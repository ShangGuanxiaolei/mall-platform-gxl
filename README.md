# 汉薇商城基础开发流程

## 目录结构

各目录功能说明

* mall-platform-hds
    - biz [业务包, 依赖service]
    - dal [数据库Mapper]
    - frontend [统一前端]
        - pc [后台pc模块]
        - restapi [公共rest模块]
        - sms [短信模块]
        - task [定时任务]
        - web [H5网页，支付]
    - service [服务层]
    - service-common [服务层的辅助包]
    - utils [工具包]

## 项目启动

### 开发准备

开发准备可以参考 [开发环境配置](http://47.99.216.55:3000/F_1rIdG1RRCEaDgpJy2q9A)

### 如何导入项目

选择IDEA的open来打开项目，不要使用import, 打开后会自动下载依赖

### 环境配置

目前主要有如下环境

* dev
* uat
* comp
* prod
* prod-service

以下说明以 **{env}** 指代具体的环境

环境配置在顶级的 `pom.xml` 文件中, 编译时使用 `mvn -P {env} compile` 可以指定编译环境,

默认的环境为开发环境

如: 以下命令会编译生产环境代码

```shell=
mvn -P prod compile
```

每个环境分别对应的配置文件在 `dal/src/main/resources/env/config-{env}-hds.properties` 中

### IDEA开发调试

在IDEA中配置好tomcat, 需要注意的是tomcat中有部分依赖的lib，需要使用特定版本

![tomcat1.png](https://i.loli.net/2019/03/19/5c9055593d3a0.png)

配置启动的war包, 在artifacts中选择 `*:war exploded` 结尾的包, 各个主要包的说明:

* xquark-pc 对应后台服务
* xquark-rest-api 对应公共的rest服务
* xquark-web 对应前端web服务
* xquark-task 对应定时任务
* xquark-sms 对应短信服务

其他的包目前没有使用

![tomcat2.png](https://i.loli.net/2019/03/19/5c90555a44471.png)


注意修改tomcat中的 **war包路径映射**

* xquark-pc -> /sellerpc
* xqaurk-rest-api -> /v2
* xquark-web -> /
* xquark-task -> /task

![tomcat-path.png](https://i.loli.net/2019/03/19/5c90566a5a976.png)

修改完成后可以点击 `debug` 或者 `run` 按钮启动项目

### 域名映射及端口转发

项目里面配置了跨域规则, 如果直接使用 *localhost:8080* 访问会导致登录错误

建议使用 [switchhost](https://github.com/oldj/SwitchHosts) 把 *hwsc-dev.handeson.com* 这个域名映射到本机

> hwsc-dev.handeson.com 这个域名本身不存在, 只是用于本地开发虚拟的域名,
> 与config-dev-hds.properties中配置的一致即可

![switchhost.png](https://i.loli.net/2019/03/19/5c90587d8f39c.png)

修改后使用 `ping hwsc-api.handeson.com` 检验配置是否正确

#### 端口转发

做了域名映射后默认的域名会转发到 80 端口, 类Unix的操作系统(windows不是很了解) 对1024以下的端口限制了只允许root权限开放, 所以需要想办法把80端口的请求转发到8080端口上, 可以使用以下方法:

1. 直接使用root权限启动IDEA (不推荐, 会导致写入的文件权限异常)
2. 使用防火墙配置转发规则
    - Linux: `sudo iptables -t nat -A OUTPUT -o lo -p tcp --dport 80 -j REDIRECT --to-port 8080`
    - MacOS: `echo "rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080" | sudo pfctl -ef -`
3. 使用转发软件如 nginx、authbind

服务启动后后台地址为 hwsc-dev.handeson.com/sellerpc/pc/login.html 开发帐号密码为 *admin/123456*

## 基础业务开发

以下以新增一个基础的CRUD业务为例, 简单描述如何开发新功能

首先参考 [mybatis-gererator-gui使用说明](http://47.99.216.55:3000/YBD3hQaETDq2zjEXe3sdCQ)

可以自动生成通用的CRUD代码

### 注册Mybatis组件

记得检查生成的 \*Mapper.java 跟 \*Mapper.xml 文件所在目录是否正确

* mall-platform-hds/dal/src/main/java/com/xquark/dal/model : entity
* mall-platform-hds/dal/src/main/java/com/xquark/dal/mapper : Java
* mall-platform-hds/dal/src/main/resources/com/xquark/dal/mapper : xml

以下以商品推荐, Recommend功能为例:

自动生成后应该产生三个文件:

* mall-platform-hds/dal/src/main/java/com/xquark/dal/model/Recommend.java
* mall-platform-hds/dal/src/main/java/com/xquark/dal/mapper/RecommendMapper.java
* mall-platform-hds/dal/src/main/resources/com/xquark/dal/mapper/RecommendMapper.xml

确认文件生成正确后在 `DalCofig.java` 类中注册Java的Mapper接口:

```java=
@Bean
public RecommendMapper recommendMapper() throws Exception {
	return newMapperFactoryBean(RecommendMapper.class).getObject();
}
```

再在 `MapperConfig.xml` 中注册xml的Mapper接口:

```xml=
<mapper resource="com/xquark/dal/mapper/RecommendMapper.xml"/>
```

尝试在Service中注入 `RecommendMapper`, 如果IDE的 `@Autowired` 没有报错,
则说明配置正确

```java=
private final RecommendMapper recommendMapper;

@Autowired
public RecommendServiceImpl(RecommendMapper recommendMapper) {
  this.recommendMapper = recommendMapper;
}
```

再在对应的 service 及 controller 层进行业务开发就可以了

## IdTypeHandler说明

为了防止数据库Id暴露, 项目中有一个IdTypeHandler类, 用来在数据库读写时对id进行加解密

如:

```xml=
<resultMap id="BaseResultMap" type="com.xquark.dal.model.Product">
		<id column="id" property="id" typeHandler="idHandler" />
		<result column="name" property="name" jdbcType="VARCHAR" />
		<result column="code" property="code" jdbcType="VARCHAR" />
```

对商品对象读取时会使用 `IdTypeHandler::encode` 这个方法对数据库中存储的Long类型Id加密为String类型的Id, 写入时会使用 `IdTypeHandler::decode` 这个方法对对象中String类型的Id解密为Long类型

### IdTypeHandler调试

为了方便调试, 写了一个跟项目无关的IdTypeHandler类:

```java=
/**
 * 实体层的id主键使用String，db schema则使用long自增。本类则负责其间的可逆加密转换。
 *
 * @author jamesp
 */
public final class IdTypeHandler {

  public static void main(String[] args) {
    if (args.length < 2) {
      throw new IllegalArgumentException("请指定正确的参数");
    }
    String operation = args[0];
    String code = args[1];
    if ("encode".equals(operation)) {
      Long lCode;
      try {
        lCode = Long.parseLong(code);
      } catch (Exception e) {
        throw new IllegalArgumentException("编码值请指定为数字");
      }
      System.out.println("编码值 " + encode(lCode));
    } else if ("decode".equals(operation)) {
      long decode = decode(code);
      System.out.println("解码值: " + decode);
    }

  }

  /**
   * long -> string 混淆加密 如果是特殊Id(带负号)，则原值返回 by yangjian
   */
  public static String encode(long l) {
    if (l < 0) {
      return Long.toString(l);
    } else {
      l = mix(l);
      return Long.toString(l, 36);
    }
  }

  /**
   * string -> long 解密 如果是特殊Id(带负号)，则原值返回 by yangjian
   */
  public static long decode(String s) {
    if (s.startsWith("-")) {
      return Long.parseLong(s);
    } else {
      return demix(Long.parseLong(s, 36));
    }
  }

  /**
   * 带版本的混淆
   */
  private static long mix(long l) {
    final long[] vs = doMix(l);
    return setVersion(vs);
  }

  /**
   * 当前版本的mix算法. <b>注意不要数值越界成负数</b>
   */
  private static long[] doMix(long l) {
    final long version = 1L; // 当前混淆算法版本号

    // 8进制位
    long ret = l;
    int digit = 0;
    while (ret > 0) {
      digit++;
      ret = ret >> 3;
    }
    // 每5位插值, 插值位
    int i = 0, md = (digit - 1) / 5 + 1;
    final int mix = (int) (l & ((1 << (3 * md)) - 1));
    ret = 0;
    while (digit > 0) {
      ret += (((l & ((1 << 15) - 1)) + ((mix & (((1 << 3) - 1) << (3 * --md))) << (15 - 3 * md)))
          << i);
      l = (l >> 15);
      digit -= 5;
      i += 18;
    }
    l = ret;

    return new long[]{version, l};
  }

  private static long demix(long l) {
    final long[] vs = getVersion(l);
    l = vs[1];
    switch ((int) vs[0]) {
      case 1:
        long dig = 0,
            ret = 0;
        while (l > 0) {
          ret += ((l & ((1 << 15) - 1)) << dig);
          l = (l >> 18);
          dig += 15;
        }
        l = ret;
        break;
    }
    return l;
  }

  /**
   * 16进制下，将版本号保留在百位数
   *
   * @param [version, value]
   */
  private static long setVersion(long[] vs) {
    // return vs[1] / 256 * 4096 + vs[0] * 256 + vs[1] % 256;
    return ((vs[1] >> 8) << 12) + (vs[0] << 8) + (vs[1] & 255);
  }

  /**
   * 16进制下，将版本号保留在百位
   *
   * @return [version, value]
   */
  private static long[] getVersion(long l) {
    // return new long[] { (l / 256) % 16, (l / 4096) * 256 + l % 256 };
    return new long[]{(l >> 8) & 15, ((l >> 12) << 8) + (l & 255)};
  }

}
```

可以把它编译成class, 进行调用, 解码同理, 使用decode命令

![](https://i.loli.net/2019/03/19/5c90604e991fb.png)
