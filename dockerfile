#!/bin/sh  
  
FROM java8 
  
MAINTAINER jrain cjw20098@sina.com 

#设置环境变量 ENV命令 DIR_WEBAPP变量名 /usr/local/tomcat/webapps变量的值  
ENV DIR_WEBAPP /usr/local/tomcat7/webapps  
ENV DB_CONFIG_FILE config-dev-hds.properties
  
#删除webapps下所有文件  
RUN rm -rf $DIR_WEBAPP/*   
  
#在webapps下添加ecloud.war并改名ROOT.war  
ADD sellerpc.war $DIR_WEBAPP/sellerpc.war   
ADD v2.war $DIR_WEBAPP/v2.war  
  
#解压webapps下的ROOT.war到ROOT目录下  
RUN unzip $DIR_WEBAPP/sellerpc.war -d $DIR_WEBAPP/sellerpc/  
RUN unzip $DIR_WEBAPP/v2.war -d $DIR_WEBAPP/v2/ 
  
#拷贝assist下的config-dev-hds.properties、start.sh文件到/home/目录下   
#注意： COPY命令前面的参数是源地址（相对地址） 后面参数是目的地址（绝对地址）  
COPY   ./config-dev-hds.properties /root/
EXPOSE 8080
#执行/home/start.sh  
#ENTRYPOINT  '/home/start.sh'
CMD /usr/local/tomcat7/bin/catalina.sh run 
#RUN sudo docker run -d  --name docker_test -p 8111:8080  docker_test 
