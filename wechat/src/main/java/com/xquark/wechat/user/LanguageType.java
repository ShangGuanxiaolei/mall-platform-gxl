package com.xquark.wechat.user;

/**
 * 语言种类
 *
 * @version 2015-7-5
 */
public enum LanguageType {
  /**
   * 简体
   */
  zh_CN,
  /**
   * 繁体
   */
  zh_TW,
  /**
   * 英语
   */
  en;

}
