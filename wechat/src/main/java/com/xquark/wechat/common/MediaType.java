/**
 *
 */
package com.xquark.wechat.common;

/**
 * @date 2015年1月26日
 */
public enum MediaType {
  image,
  voice,
  video,
  thumb
}
