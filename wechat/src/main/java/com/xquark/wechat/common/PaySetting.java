package com.xquark.wechat.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 企业支付商户信息
 *
 * Created by liangfan on 05/22/2016.
 */
public class PaySetting {

  private static Logger log = LoggerFactory.getLogger(PaySetting.class);

  private static PaySetting paySetting = null;

  public static void setDefault(PaySetting paySetting) {
    PaySetting.paySetting = paySetting;
  }

  public static PaySetting defaultSetting() {

    if (paySetting == null) {
      log.error("当前系统没有设置缺省的商户信息,请使用setDefault方法初始化商户信息.");
    }
    return paySetting;
  }

  /**
   * 商户ID
   */
  private String mchId;

  /**
   * 商户的appId
   */
  private String appId;

  /**
   * 秘钥
   */
  private String key;

  /**
   * 证书位置
   */
  private String certPath;

  /**
   * 证书密码
   */
  private String certPassword;

  /**
   * 数据库中存储的证书byte字节码
   */
  private byte[] certFile;

  public String getMchId() {
    return mchId;
  }

  public void setMchId(String mchId) {
    this.mchId = mchId;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getCertPath() {
    return certPath;
  }

  public byte[] getCertFile() {
    return certFile;
  }

  public void setCertFile(byte[] certFile) {
    this.certFile = certFile;
  }

  public void setCertPath(String certPath) {
    this.certPath = certPath;
  }

  public String getCertPassword() {
    return certPassword;
  }

  public void setCertPassword(String certPassword) {
    this.certPassword = certPassword;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PaySetting that = (PaySetting) o;

    if (!mchId.equals(that.mchId)) {
      return false;
    }
    return !(appId != null ? !appId.equals(that.appId) : that.appId != null);

  }

  @Override
  public int hashCode() {
    int result = mchId.hashCode();
    result = 31 * result + (appId != null ? appId.hashCode() : 0);
    return result;
  }
}
