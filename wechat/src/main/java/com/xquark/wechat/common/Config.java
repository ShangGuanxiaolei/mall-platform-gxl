/**
 *
 */
package com.xquark.wechat.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @date 2014年12月8日
 */
public class Config {

  protected static Logger logger = LoggerFactory.getLogger(Config.class);

  /**
   * 公众号使用的接口配置URL
   */
  private String url;

  /**
   * 公众号使用的接口配置Token
   */
  private String token;

  /**
   * 暂时不用
   */
  private String encodingAESKey;

  /**
   * 公众账号的appID
   */
  private String appid;

  /**
   * 公众账号的appSecret
   */
  private String appSecret;

  /**
   * 商户号, 用于下单支付,订单查询,退款的业务
   */
  private String mchId;

  /**
   * 商户号key用于下单支付,订单查询,退款的业务
   */
  private String mchKey;

  /**
   * 企业账号打款使用
   */
  private String certFileContent;

  private String accessToken;

  private static Config config = new Config();

  public Config() {
    //TODO dongsongjie remove test data
//		this.url = "http://7nodmtvdlm.proxy.qqbrowser.cc/wechat/entry";
//		this.encodingAESKey = "";
//		this.token = "xquark2016";
//		this.appid = "wxd30880e8cf0ac105"; //测试账号的appID
//		this.appSecret = "391844e29f3c39ae99af00619ba63d86"; //测试账号的appsecret
//		this.mchId = ""; //支付时使用
//		this.mchKey = ""; //支付时使用
  }

  public Config(String url, String encodingAESKey, String token, String appid, String appSecret,
      String mchId, String mchKey, String certFileContent, String accessToken) {
    this.url = url;
    this.encodingAESKey = encodingAESKey;
    this.token = token;
    this.appid = appid; //测试账号的appID
    this.appSecret = appSecret; //测试账号的appsecret
    this.mchId = mchId; //支付时使用
    this.mchKey = mchKey; //支付时使用
    this.certFileContent = certFileContent;
    this.accessToken = accessToken;
  }

  public static Config instance() {
    return config;
  }

  public String getToken() {
    return token;
  }

  public String getAppid() {
    return appid;
  }

  public String getAppSecret() {
    return appSecret;
  }

  public String getMchId() {
    return mchId;
  }

  public String getMchKey() {
    return mchKey;
  }

  public String getUrl() {
    return url;
  }

  public String getEncodingAESKey() {
    return encodingAESKey;
  }

  public String getCertFileContent() {
    return certFileContent;
  }

  public void setCertFileContent(String certFileContent) {
    this.certFileContent = certFileContent;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }
}
