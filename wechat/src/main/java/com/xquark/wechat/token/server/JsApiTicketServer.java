/**
 *
 */
package com.xquark.wechat.token.server;

import com.xquark.wechat.common.Config;


/**
 * Ticket server适配器
 *
 * @date 2015年1月29日
 */
public class JsApiTicketServer extends AbsServer implements TicketServer {


  /**
   *
   */
  public String ticket() {
    return super.token();
  }

  /**
   *
   */
  @Override
  protected String getCustomerServerClass() {
    return null;
  }

  /**
   *
   */
  @Override
  public IServer defaultServer() {
    return JsApiTicketMemServer.instance();
  }

}
