/**
 *
 */
package com.xquark.wechat.token.server;

import com.xquark.wechat.common.Config;
import com.xquark.wechat.token.Ticket;
import com.xquark.wechat.token.TicketType;

/**
 * 内存控制单例
 *
 * @date 2015年1月29日
 */
@Deprecated
public class JsApiTicketMemServer implements IServer {

  private static JsApiTicketMemServer ticketServer = null;

  private Ticket jsApiTicket = new Ticket(TicketType.jsapi);

  private int requestTimes = 1;//token请求失败后重新请求的次数

  private Config config;

  /**
   * 私有构造
   */
  private JsApiTicketMemServer(Config config) {
    //获取新的token
    this.config = config;
    refresh();
  }

  /**
   * token中控服务器实例
   *
   * @return ticket服务器实例
   */
  public static JsApiTicketMemServer instance() {
    return ticketServer;
  }


  /**
   * 通过中控服务器得到accessToken
   */
  public String token() {
    //没有可用的token，则去刷新
    if (!this.jsApiTicket.isValid()) {
      refresh();
    }
    return this.jsApiTicket.getToken();
  }

  /**
   * 服务器刷新token
   */
  private void refresh() {
    for (int i = 0; i < requestTimes; i++) {
      //请求成功则退出
      if (this.jsApiTicket.request(this.config)) {
        break;
      }
    }
  }

}
