/**
 *
 */
package com.xquark.wechat.token;

import com.xquark.wechat.common.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Access token实体模型
 */
public class AccessToken extends Token {

  protected static Logger logger = LoggerFactory.getLogger(AccessToken.class);

  private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";

  @Override
  protected String tokenName() {
    return "access_token";
  }

  @Override
  protected String expiresInName() {
    return "expires_in";
  }

  /**
   * 组织accesstoken的请求url
   */
  @Override
  protected String accessTokenUrl(Config config) {
    String appid = config.getAppid();
    String appsecret = config.getAppSecret();
    String url = ACCESS_TOKEN_URL + "&appid=" + appid + "&secret=" + appsecret;
    logger.info("创建获取access_token url");
    return url;
  }

}
