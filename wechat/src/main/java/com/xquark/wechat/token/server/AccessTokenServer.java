/**
 *
 */
package com.xquark.wechat.token.server;

import com.xquark.wechat.common.Config;

/**
 * 适配器
 *
 * @date 2015年1月30日
 */
public class AccessTokenServer extends AbsServer implements TokenServer {


  /**
   *
   */
  public String token() {
    return super.token();
  }

  @Override
  protected String getCustomerServerClass() {
    return null;
  }

  @Override
  public IServer defaultServer() {
    return AccessTokenMemServer.instance();
  }

}
