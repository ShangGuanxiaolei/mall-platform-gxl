/**
 *
 */
package com.xquark.wechat.token.server;

/**
 * @date 2015年1月29日
 */
public interface TicketServer {

  public String ticket();
}
