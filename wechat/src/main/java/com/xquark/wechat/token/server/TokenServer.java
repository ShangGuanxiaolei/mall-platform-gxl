/**
 *
 */
package com.xquark.wechat.token.server;


/**
 * accessToken中控服务器
 *
 * @date 2015年1月9日
 */
public interface TokenServer {

  public String token();
}
