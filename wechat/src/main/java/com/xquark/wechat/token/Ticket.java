/**
 *
 */
package com.xquark.wechat.token;


import com.xquark.wechat.common.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 微信ticket操作类 ticket和token的逻辑在腾讯是差不多的，所以继承抽象类token
 *
 * @date 2015年1月29日
 */
public class Ticket extends Token {

  protected static Logger logger = LoggerFactory.getLogger(Ticket.class);

  private static final String TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?";
  private static final String TICKET_NAME = "ticket";
  private static final String EXPIRESIN_NAME = "expires_in";

  private String type;

  public Ticket(TicketType ticketType) {
    super();
    this.type = ticketType.name();
  }

  /* (non-Javadoc)
   * @see com.xquark.wechat.token.Token#accessTokenUrl()
   */
  @Override
  protected String accessTokenUrl(Config config) {
    String access_token = config.getAccessToken();
    String url = TICKET_URL + "access_token=" + access_token + "&type=" + this.type;
    logger.info("获取ticket,ticket类型" + this.type);
    return url;
  }

  /* (non-Javadoc)
   * @see com.xquark.wechat.token.Token#tokenName()
   */
  @Override
  protected String tokenName() {
    return TICKET_NAME;
  }

  /* (non-Javadoc)
   * @see com.xquark.wechat.token.Token#expiresInName()
   */
  @Override
  protected String expiresInName() {
    return EXPIRESIN_NAME;
  }


}
