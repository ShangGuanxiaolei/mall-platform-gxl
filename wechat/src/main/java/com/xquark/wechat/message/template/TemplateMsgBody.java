/**
 *
 */
package com.xquark.wechat.message.template;

import java.util.List;


public class TemplateMsgBody {

  private String touser;

  private String templateId;

  private String url;

  private List<TemplateMsgData> data;

  public String getTouser() {
    return touser;
  }

  public void setTouser(String touser) {
    this.touser = touser;
  }

  public String getTemplateId() {
    return templateId;
  }

  public void setTemplateId(String templateId) {
    this.templateId = templateId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<TemplateMsgData> getData() {
    return data;
  }

  public void setData(List<TemplateMsgData> data) {
    this.data = data;
  }


}
