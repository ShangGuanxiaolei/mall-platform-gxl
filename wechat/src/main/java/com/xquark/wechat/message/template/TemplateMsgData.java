/**
 *
 */
package com.xquark.wechat.message.template;

/**
 * @date 2014年12月24日
 */
public class TemplateMsgData {

  public static final String PRIMARY = "#1565C0";

  public static final String WARNING = "#FF5722";

  public static final String DANGER = "#C62828";

  public static final String SUCCESS = "#2E7D32";

  public static final String INFO = "#00838F";


  public TemplateMsgData(String name, String value, String color) {
    this.name = name;
    this.value = value;
    this.color = color;
  }

  private String name; //json中的数据名称

  private String value; //keynote value

  private String color; //data keynote color


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }
}
