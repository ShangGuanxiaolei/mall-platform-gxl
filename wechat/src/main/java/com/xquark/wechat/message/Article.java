package com.xquark.wechat.message;

/**
 * 微信图文素材内容
 */
public class Article {

  private String title;
  private String author;
  private String content;
  private String digest;

  private String thumbMediaId;

  private String thumbUrl;

  private String contentSourceUrl;

  private boolean showCover;

  private String url;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDigest() {
    return digest;
  }

  public void setDigest(String digest) {
    this.digest = digest;
  }

  public String getThumbMediaId() {
    return thumbMediaId;
  }

  public void setThumbMediaId(String thumbMediaId) {
    this.thumbMediaId = thumbMediaId;
  }

  public String getContentSourceUrl() {
    return contentSourceUrl;
  }

  public void setContentSourceUrl(String contentSourceUrl) {
    this.contentSourceUrl = contentSourceUrl;
  }

  public boolean isShowCover() {
    return showCover;
  }

  public void setShowCover(boolean showCover) {
    this.showCover = showCover;
  }

  public String getThumbUrl() {
    return thumbUrl;
  }

  public void setThumbUrl(String thumbUrl) {
    this.thumbUrl = thumbUrl;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
