package com.xquark.wechat.oauth.protocol.get_userinfo;

import com.xquark.wechat.oauth.protocol.WechatUserBean;
import java.util.Arrays;

/**
 * 响应：拉取用户信息(需scope为 snsapi_userinfo) Created by xuwen on 2015-12-11.
 */
public class GetUserinfoResponse extends WechatUserBean {

  private String appId;
  private String openid;
  private String nickname;
  private String sex;
  private String province;
  private String city;
  private String country;
  private String headimgurl;
  private String[] privilege;
  private String unionid;

  public void setOpenid(String openid) {
    this.openid = openid;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setHeadimgurl(String headimgurl) {
    this.headimgurl = headimgurl;
  }

  public void setPrivilege(String[] privilege) {
    this.privilege = privilege;
  }

  public void setUnionid(String unionid) {
    this.unionid = unionid;
  }

  @Override
  public String getOpenId() {
    return this.openid;
  }

  @Override
  public String getNickName() {
    return this.nickname;
  }

  @Override
  public String getGender() {
    return this.sex;
  }

  @Override
  public String getCity() {
    return this.city;
  }

  @Override
  public String getProvince() {
    return this.province;
  }

  @Override
  public String country() {
    return this.country;
  }

  @Override
  public String getAvatar() {
    return this.headimgurl;
  }

  @Override
  public String getUnionId() {
    return this.unionid;
  }

  @Override
  public String[] getPrivilege() {
    return Arrays.copyOf(this.privilege, this.privilege.length);
  }

  @Override
  public String getAppId() {
    return appId;
  }

  @Override
  public void setAppId(String appId) {
    this.appId = appId;
  }

  @Override
  public String toString() {
    return "GetUserinfoResponse{" +
        "openid='" + openid + '\'' +
        ", nickname='" + nickname + '\'' +
        ", sex='" + sex + '\'' +
        ", province='" + province + '\'' +
        ", city='" + city + '\'' +
        ", country='" + country + '\'' +
        ", headimgurl='" + headimgurl + '\'' +
        ", privilege=" + Arrays.toString(privilege) +
        ", unionid='" + unionid + '\'' +
        '}';
  }

}

