package com.xquark.wechat.oauth;

/**
 * Created by wangxinhua on 18-2-11. DESC:
 */
public abstract class ThirdUserBean {

  public abstract String getNickName();

  public abstract String getAvatar();

  public abstract String getPhone();

  public abstract String getUnionId();

}
