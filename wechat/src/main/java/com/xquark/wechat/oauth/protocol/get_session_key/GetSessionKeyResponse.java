package com.xquark.wechat.oauth.protocol.get_session_key;

/**
 * Created by wangxinhua on 17-10-23. DESC:
 */
public class GetSessionKeyResponse {

  private String openid;

  private String session_key;

  private String unionid;

  public String getOpenid() {
    return openid;
  }

  public void setOpenid(String openid) {
    this.openid = openid;
  }

  public String getSession_key() {
    return session_key;
  }

  public void setSession_key(String session_key) {
    this.session_key = session_key;
  }

  public String getUnionid() {
    return unionid;
  }

  public void setUnionid(String unionid) {
    this.unionid = unionid;
  }

  @Override
  public String toString() {
    return "GetSessionKeyResponse{" +
        "openid='" + openid + '\'' +
        ", session_key='" + session_key + '\'' +
        ", unionid='" + unionid + '\'' +
        '}';
  }
}
