package com.xquark.wechat.oauth;

/**
 * Created by wangxinhua on 18-2-11. DESC:
 */
public class SCrmAuth {

  // 时间戳
  private String timeStamp;

  // 签名
  private String signature;

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

}
