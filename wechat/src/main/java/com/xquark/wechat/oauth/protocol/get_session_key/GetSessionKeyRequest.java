package com.xquark.wechat.oauth.protocol.get_session_key;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

/**
 * Created by wangxinhua on 17-10-23. DESC:
 */
public class GetSessionKeyRequest extends WechatApiRequest {

  private String js_code;

  private String grant_type = "authorization_code";

  public GetSessionKeyRequest(Config config, String code) {
    super(config);
    this.js_code = code;
  }

  public String getJs_code() {
    return js_code;
  }

  public void setJs_code(String js_code) {
    this.js_code = js_code;
  }

  public String getGrant_type() {
    return grant_type;
  }

  public void setGrant_type(String grant_type) {
    this.grant_type = grant_type;
  }
}
