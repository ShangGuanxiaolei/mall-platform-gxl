package com.xquark.wechat.oauth.protocol.get_access_token;

import com.alibaba.fastjson.JSONObject;
import com.xquark.utils.AesCbcUtil;
import com.xquark.wechat.oauth.protocol.WechatUserBean;

/**
 * Created by wangxinhua on 17-10-23. DESC:
 */
public class DecryptedResponse extends WechatUserBean {

  // 响应中不会返回appId，用于参数传递
  private String appId;

  private String openId;
  private String nickName;
  private String gender;
  private String city;
  private String province;
  private String country;
  private String avatarUrl;
  private String unionId;

  public DecryptedResponse() {
  }

  public static DecryptedResponse fromEncryptData(String sessionKey, String encryptedData,
      String iv)
      throws Exception {
    String res = AesCbcUtil.decrypt(encryptedData, sessionKey, iv, "UTF-8");
    return JSONObject.parseObject(res, DecryptedResponse.class);
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  @Override
  public void setAppId(String appId) {
    this.appId = appId;
  }

  @Override
  public String getAppId() {
    return this.appId;
  }

  @Override
  public String getOpenId() {
    return this.openId;
  }

  @Override
  public String getNickName() {
    return this.nickName;
  }

  @Override
  public String getGender() {
    return this.gender;
  }

  @Override
  public String getCity() {
    return this.city;
  }

  @Override
  public String getProvince() {
    return this.province;
  }

  @Override
  public String country() {
    return this.country;
  }

  @Override
  public String getAvatar() {
    return this.avatarUrl;
  }

  @Override
  public String getUnionId() {
    return this.unionId;
  }

  @Override
  public String[] getPrivilege() {
    throw new UnsupportedOperationException("小程序返回不包含此字段");
  }
}
