package com.xquark.wechat.oauth.protocol.get_access_token;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

/**
 * 请求：通过code换取网页授权access_token
 */
public class GetAccessTokenRequest extends WechatApiRequest {

  ;

  private String code;

  private String grant_type = "authorization_code";

  public GetAccessTokenRequest(Config config, String code) {
    super(config);
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getGrant_type() {
    return grant_type;
  }

  public void setGrant_type(String grant_type) {
    this.grant_type = grant_type;
  }
}
