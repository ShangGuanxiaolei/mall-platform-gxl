package com.xquark.wechat.oauth.protocol.valid_access_token;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

/**
 * 响应：检验授权凭证（access_token）是否有效
 */
public class ValidAccessTokenRequest extends WechatApiRequest {

  public ValidAccessTokenRequest(Config config, String access_token, String openid) {
    super(config);
    this.access_token = access_token;
    this.openid = openid;
  }

  private String access_token;
  private String openid;

  public String getAccess_token() {
    return access_token;
  }

  public void setAccess_token(String access_token) {
    this.access_token = access_token;
  }

  public String getOpenid() {
    return openid;
  }

  public void setOpenid(String openid) {
    this.openid = openid;
  }

}
