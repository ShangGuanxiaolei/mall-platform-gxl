package com.xquark.wechat.oauth;

/**
 * Created by wangxinhua on 18-2-11. DESC:
 */
public class SCrmUser extends ThirdUserBean {

  private String unionId;

  private String phone;

  private String avatar;

  private String name;

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String getNickName() {
    return getName();
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
