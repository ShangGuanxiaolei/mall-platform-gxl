package com.xquark.wechat.oauth.protocol.refresh_access_token;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

/**
 * 响应：刷新access_token（如果需要）
 */
public class RefreshAccessTokenRequest extends WechatApiRequest {

  private String grant_type = "refresh_token";

  private String refresh_token;

  public RefreshAccessTokenRequest(Config config, String refresh_token) {
    super(config);
    this.refresh_token = refresh_token;
  }

  public String getGrant_type() {
    return grant_type;
  }

  public void setGrant_type(String grant_type) {
    this.grant_type = grant_type;
  }

  public String getRefresh_token() {
    return refresh_token;
  }

  public void setRefresh_token(String refresh_token) {
    this.refresh_token = refresh_token;
  }

}
