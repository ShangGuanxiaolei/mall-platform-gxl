package com.xquark.wechat.oauth.protocol.get_userinfo;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

/**
 * 请求：拉取用户信息(需scope为 snsapi_userinfo)
 */
public class GetUserinfoRequest extends WechatApiRequest {

  public GetUserinfoRequest(Config config, String access_token, String openid) {
    super(config);
    this.access_token = access_token;
    this.openid = openid;
  }

  private String access_token;
  private String openid;
  private String lang = "zh_CN";

  public String getAccess_token() {
    return access_token;
  }

  public void setAccess_token(String access_token) {
    this.access_token = access_token;
  }

  public String getOpenid() {
    return openid;
  }

  public void setOpenid(String openid) {
    this.openid = openid;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

}
