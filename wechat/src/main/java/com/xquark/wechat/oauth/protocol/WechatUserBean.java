package com.xquark.wechat.oauth.protocol;

import com.xquark.wechat.oauth.ThirdUserBean;

/**
 * Created by wangxinhua on 17-10-27. DESC: 微信小程序与公众号返回结果
 */
public abstract class WechatUserBean extends ThirdUserBean {

  public abstract void setAppId(String appId);

  public abstract String getAppId();

  public abstract String getOpenId();

  public abstract String getNickName();

  public abstract String getGender();

  public abstract String getCity();

  public abstract String getProvince();

  public abstract String country();

  public abstract String getAvatar();

  public abstract String getUnionId();

  public abstract String[] getPrivilege();

  public String getPhone() {
    return "";
  }

}
