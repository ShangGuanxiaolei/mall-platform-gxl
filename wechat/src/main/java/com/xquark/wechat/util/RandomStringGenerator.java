package com.xquark.wechat.util;

import java.util.Random;

/**
 * 随机字符串生成器
 */
public class RandomStringGenerator {

  private static final int DEFAULT_LENGTH = 32;

  public enum GenerateMode {
    CAPITAL_LETTERS("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"), // 大写字母加数字
    SMALL_LETTERS("abcdefghijklmnopqrstuvwxyz0123456789"), // 小写字母加数字,
    MIXED("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"); // 大小写字母加数字

    String chars;

    GenerateMode(String chars) {
      this.chars = chars;
    }

    private String getChars() {
      return chars;
    }

    private int charLength() {
      return this.chars.length();
    }
  }

  /**
   * 获取一定长度的随机字符串
   *
   * @param length 指定字符串长度
   * @return 一定长度的字符串
   */
  public static String generate(int length) {
    Random random = new Random();
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < length; i++) {
      int number = random.nextInt(GenerateMode.MIXED.charLength());
      sb.append(GenerateMode.MIXED.getChars().charAt(number));
    }
    return sb.toString();
  }

  /**
   * 获取一定长度的随机字符串
   *
   * @param length 指定字符串长度
   * @return 一定长度的字符串
   */
  public static String getRandomStringByLength(int length) {
    return getRandomStringByLength(GenerateMode.MIXED, length);
  }

  /**
   * 获取一定长度的随机字符串
   *
   * @param mode 生成模式
   * @param length 指定字符串长度
   * @return 生成的字符串
   */
  public static String getRandomStringByLength(GenerateMode mode, int length) {
    Random random = new Random();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < length; i++) {
      int number = random.nextInt(mode.charLength());
      sb.append(mode.getChars().charAt(number));
    }
    return sb.toString();
  }

  /**
   * 获取默认长度的随机字符串
   *
   * @return 默认长度的字符串
   */
  public static String generate() {
    return generate(DEFAULT_LENGTH);
  }

  public static void main(String[] args) {
    System.out
        .println(RandomStringGenerator.getRandomStringByLength(GenerateMode.CAPITAL_LETTERS, 6));
  }

}
