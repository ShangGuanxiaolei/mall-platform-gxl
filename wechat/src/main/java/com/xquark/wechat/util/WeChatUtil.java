package com.xquark.wechat.util;

import com.xquark.utils.MD5Util;
import com.xquark.wechat.exception.WeChatException;
import com.xquark.wechat.exception.WeChatReturnCode;

import com.alibaba.fastjson.JSONObject;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 工具类
 *
 * @version 2015-7-4
 */
public class WeChatUtil {

  /**
   * 判断是否请求成功
   */
  public static void isSuccess(String resultStr) throws WeChatException {
    JSONObject jsonObject = JSONObject.parseObject(resultStr);
    Integer errCode = jsonObject.getIntValue("errcode");
    if (errCode != null && errCode != 0) {
      String errMsg = WeChatReturnCode.getMsg(errCode);
      if (errMsg.equals("")) {
        errMsg = jsonObject.getString("errmsg");
      }
      throw new WeChatException("异常码:" + errCode + ";异常说明:" + errMsg);
    }
  }

  public static String createSign(String apiKey, String characterEncoding,
      Map<?, ?> parameters) {
    StringBuilder sb = new StringBuilder();
    Set es = parameters.entrySet();
    for (Object e : es) {
      Entry entry = (Entry) e;
      String k = (String) entry.getKey();
      Object v = entry.getValue();
      if (null != v && !"".equals(v)
          && !"pay".equals(k) && !"key".equals(k)) {
        sb.append(k).append("=").append(v).append("&");
      }
    }
    sb.append("key=").append(apiKey);
    return MD5Util.MD5Encode(sb.toString(), characterEncoding).toUpperCase();
  }
}
