package com.xquark.wechat.pay;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

import javax.xml.bind.annotation.XmlElement;

/**
 * H5调起支付API的参数对象
 * <p><a href="https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=7_7&index=6">开发文档</p>
 * <p/>
 */
public class H5PayParam extends WechatApiRequest {

  public H5PayParam(Config config) {
    super(config);
  }

  private String timeStamp;

  private String nonceStr;

  private String packageWithPrepayId; //参数名package

  private String signType = "MD5";

  private String paySign;

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getNonceStr() {
    return nonceStr;
  }

  public void setNonceStr(String nonceStr) {
    this.nonceStr = nonceStr;
  }

  @XmlElement(name = "package")
  public String getPackageWithPrepayId() {
    return packageWithPrepayId;
  }

  public void setPackageWithPrepayId(String packageWithPrepayId) {
    this.packageWithPrepayId = packageWithPrepayId;
  }

  public String getSignType() {
    return signType;
  }

  public void setSignType(String signType) {
    this.signType = signType;
  }

  public String getPaySign() {
    return paySign;
  }

  public void setPaySign(String paySign) {
    this.paySign = paySign;
  }

}
