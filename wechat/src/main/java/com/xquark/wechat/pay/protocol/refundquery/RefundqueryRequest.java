package com.xquark.wechat.pay.protocol.refundquery;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 查询退款请求对象
 * <p>参考<a href="https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_5">开发文档</p>
 * <p/>
 * Created by xuwen on 2015-12-10.
 */
@XmlRootElement(name = "xml")
public class RefundqueryRequest extends WechatApiRequest {

  public RefundqueryRequest(Config config) {
    super(config);
  }

  private String device_info;

  private String nonce_str;

  private String sign;

  private String transaction_id;

  private String out_trade_no;

  private String out_refund_no;

  private String refund_id;

  public String getDevice_info() {
    return device_info;
  }

  public void setDevice_info(String device_info) {
    this.device_info = device_info;
  }

  public String getNonce_str() {
    return nonce_str;
  }

  public void setNonce_str(String nonce_str) {
    this.nonce_str = nonce_str;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getTransaction_id() {
    return transaction_id;
  }

  public void setTransaction_id(String transaction_id) {
    this.transaction_id = transaction_id;
  }

  public String getOut_trade_no() {
    return out_trade_no;
  }

  public void setOut_trade_no(String out_trade_no) {
    this.out_trade_no = out_trade_no;
  }

  public String getOut_refund_no() {
    return out_refund_no;
  }

  public void setOut_refund_no(String out_refund_no) {
    this.out_refund_no = out_refund_no;
  }

  public String getRefund_id() {
    return refund_id;
  }

  public void setRefund_id(String refund_id) {
    this.refund_id = refund_id;
  }

}
