package com.xquark.wechat.pay.protocol.orderquery;

import com.xquark.wechat.WechatApiRequest;
import com.xquark.wechat.common.Config;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 查询订单请求对象
 * <p>参考<a href="https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_2">开发文档</p>
 * <p/>
 */
@XmlRootElement(name = "xml")
public class OrderqueryRequest extends WechatApiRequest {

  protected OrderqueryRequest(Config config) {
    super(config);
  }

  private String transaction_id;

  private String out_trade_no;

  private String nonce_str;

  private String sign;

  public String getTransaction_id() {
    return transaction_id;
  }

  public void setTransaction_id(String transaction_id) {
    this.transaction_id = transaction_id;
  }

  public String getOut_trade_no() {
    return out_trade_no;
  }

  public void setOut_trade_no(String out_trade_no) {
    this.out_trade_no = out_trade_no;
  }

  public String getNonce_str() {
    return nonce_str;
  }

  public void setNonce_str(String nonce_str) {
    this.nonce_str = nonce_str;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

}
