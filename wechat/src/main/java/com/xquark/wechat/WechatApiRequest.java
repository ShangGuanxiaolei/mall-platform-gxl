package com.xquark.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.xquark.wechat.common.Config;

/**
 * 微信接口调用请求对象
 */
public abstract class WechatApiRequest {

  @JSONField(serialize = false)
  private Config config;

  private String appId;

  private String mchId;

  private String mchKey;

  private String secret;

  public Config getConfig() {
    return config;
  }

  public void setConfig(Config config) {
    this.config = config;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getMchId() {
    return mchId;
  }

  public void setMchId(String mchId) {
    this.mchId = mchId;
  }

  public String getMchKey() {
    return mchKey;
  }

  public void setMchKey(String mchKey) {
    this.mchKey = mchKey;
  }

  protected WechatApiRequest(Config config) {
    this.config = config;
    this.appId = config.getAppid();
    this.mchId = config.getMchId();
    this.mchKey = config.getMchKey();
    this.secret = config.getAppSecret();
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }
}
