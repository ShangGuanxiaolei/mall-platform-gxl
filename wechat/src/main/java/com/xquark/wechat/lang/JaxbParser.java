/**
 *
 */
package com.xquark.wechat.lang;


import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;

/**
 * @date 2014年12月7日
 */
public class JaxbParser {

  protected static Logger logger = LoggerFactory.getLogger(JaxbParser.class);

  private Class clazz;
  private String[] cdataNode;

  /**
   *
   * @param clazz
   */
  public JaxbParser(Class clazz) {
    this.clazz = clazz;
  }

  /**
   * 设置需要包含CDATA的节点
   */
  public void setCdataNode(String[] cdataNode) {
    this.cdataNode = cdataNode;
  }

  /**
   * 转为xml串
   */
  public String toXML(Object obj) {
    String ret = null;
    try {
      JAXBContext context = JAXBContext.newInstance(obj.getClass());
      Marshaller m = context.createMarshaller();
      m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      m.setProperty(Marshaller.JAXB_FRAGMENT, true);// 去掉报文头
      StringWriter writer = new StringWriter(256);
      m.marshal(obj, writer);
      ret = writer.toString();
    } catch (Exception e) {
      logger.error("serialize obj error. obj: " + JSON.toJSONString(obj), e);
    }

    logger.info("response text:" + ret);

    return ret;
  }


  /**
   * 转为对象
   */
  public Object toObj(InputStream is) {
    JAXBContext context;
    try {
      context = JAXBContext.newInstance(clazz);
      Unmarshaller um = context.createUnmarshaller();
      Object obj = um.unmarshal(is);
      return obj;
    } catch (Exception e) {
      logger.error("post data parse error");
      e.printStackTrace();
    }
    return null;
  }

  /**
   * XML转为对象
   */
  public Object toObj(String xmlStr) {
    InputStream is = new ByteArrayInputStream(xmlStr.getBytes());
    return toObj(is);
  }

  /**
   * 适配cdata tag
   */
  private void formatCDataTag() {
    for (int i = 0; i < cdataNode.length; i++) {
      cdataNode[i] = "^" + cdataNode[i];
    }
  }
}
