/**
 *
 */
package com.xquark.wechat.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @date 2014年12月11日
 */
public class StreamUtils {

  protected static Logger logger = LoggerFactory.getLogger(StreamUtils.class);

  /**
   * stream to string
   */
  public static String streamToString(HttpServletRequest request) {
    String ret = null;
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      InputStream ins = request.getInputStream();
      int i = -1;
      while ((i = ins.read()) != -1) {
        out.write(i);
      }
      ret = out.toString();
    } catch (IOException e) {
      logger.error("streaming request data error", e);
    }
    return ret;
  }

  /**
   * string to stream
   */
  public static InputStream strToStream(String str) {
    InputStream is = new ByteArrayInputStream(str.getBytes());
    return is;
  }

}
