DROP TABLE IF EXISTS `xquark_promotion_bargain`;
CREATE TABLE `xquark_promotion_bargain` (
  `id`           BIGINT         NOT NULL AUTO_INCREMENT,
  `promotion_id` BIGINT         NOT NULL
  COMMENT '活动id',
  `product_id`   BIGINT         NOT NULL
  COMMENT '活动商品id',
  `max_discount` DECIMAL(18, 2) NOT NULL
  COMMENT '最大折扣',
  `max_times`    INT            NOT NULL
  COMMENT '最大砍价次数',
  `created_at`   DATETIME                DEFAULT NULL,
  `updated_at`   DATETIME                DEFAULT NULL,
  `archive`      TINYINT(4)     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '砍价活动表';

DROP TABLE `xquark_promotion_bargain_detail`;
CREATE TABLE `xquark_promotion_bargain_detail` (
  `id`            BIGINT         NOT NULL AUTO_INCREMENT,
  `bargain_id`    BIGINT         NOT NULL
  COMMENT '砍价表id',
  `user_id`       BIGINT         NOT NULL
  COMMENT '砍价发起人id',
  `curr_discount` DECIMAL(18, 2) DEFAULT 0
  COMMENT '当前折扣',
  `curr_times`    INT            DEFAULT 0
  COMMENT '当前砍价次数',
  `created_at`    DATETIME                DEFAULT NULL,
  `updated_at`    DATETIME                DEFAULT NULL,
  `archive`       TINYINT(4)     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '砍价明细表';

DROP TABLE `xquark_promotion_bargain_history`;
CREATE TABLE xquark_promotion_bargain_history (
  `id`                BIGINT         NOT NULL AUTO_INCREMENT,
  `bargain_detail_id` BIGINT         NOT NULL
  COMMENT '砍价明细表id',
  `user_id`           BIGINT         NOT NULL
  COMMENT '参与用户id',
  `discount`          DECIMAL(18, 2) NOT NULL
  COMMENT '砍价金额',
  `created_at`        DATETIME                DEFAULT NULL,
  `updated_at`        DATETIME                DEFAULT NULL,
  `archive`           TINYINT(4)     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '砍价历史表';

ALTER TABLE xquark_promotion_bargain ADD COLUMN `price_start` DECIMAL(18, 2) NOT NULL COMMENT '砍价金额起始数' AFTER `max_times`;
ALTER TABLE xquark_promotion_bargain ADD COLUMN `price_end` DECIMAL(18, 2) NOT NULL COMMENT '砍价金额结束数' AFTER `price_start`;

ALTER TABLE xquark_promotion_bargain_detail MODIFY COLUMN curr_discount DECIMAL(18, 2) NOT NULL DEFAULT 0.00;
