ALTER TABLE xquark_health_test_module ADD COLUMN `required`     TINYINT NOT NULL DEFAULT FALSE COMMENT '是否必做' AFTER `required_sex`;

ALTER TABLE xquark_health_test_module ADD COLUMN `required_sex` INT NOT NULL DEFAULT 0 COMMENT '题目性别限制 0 - 不限 1 - 男 2 - 女' AFTER sort_no;

ALTER TABLE xquark_health_test_question DROP COLUMN point;
ALTER TABLE xquark_health_test_question ADD COLUMN `required_sex` INT NOT NULL DEFAULT 0 COMMENT '题目性别限制 0 - 不限 1 - 男 2 - 女' AFTER type;
ALTER TABLE xquark_health_test_module ADD COLUMN `static_name` VARCHAR(50) DEFAULT '' COMMENT '静态名称' AFTER name;

ALTER TABLE xquark_health_test_answer DROP COLUMN `option` ;
