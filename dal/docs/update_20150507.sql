ALTER TABLE `xquark_product` ADD COLUMN `original_price`  decimal(14,2) NULL COMMENT '原始价格（用户录入价格）' AFTER `price`;
ALTER TABLE `xquark_sku` ADD COLUMN `original_price`  decimal(14,2) NULL COMMENT '原始价格（用户录入价格）' AFTER `price`;
ALTER TABLE `xquark_sku`	ADD COLUMN `in_activity` TINYINT NOT NULL DEFAULT '0' COMMENT '是否正在活动优惠中(0/1) 当前由活动开始和结束时 改写';
ALTER TABLE `xquark_product`	ADD COLUMN `in_activity` TINYINT NOT NULL DEFAULT '0' COMMENT '是否正在活动优惠中(0/1) 当前由活动开始和结束时 改写';
ALTER TABLE `xquark_activity_ticket` ADD COLUMN `updated_at`  datetime NULL AFTER `audit_reason`;

ALTER TABLE `xquark_activity`
DROP INDEX `idx_act_name_date`,
ADD INDEX `idx_activity_type_status` (`type`, `status`) ;

ALTER TABLE `xquark_activity_ticket`
ADD INDEX `idx_act_ticket_index` (`activity_id`, `start_time`, `end_time`, `status`) ;

ALTER TABLE `xquark_campaign_product`
DROP INDEX `idx1` ,
ADD UNIQUE INDEX `idx1_campaing_index` (`ticket_id`, `product_id`, `activity_id`) USING BTREE ;