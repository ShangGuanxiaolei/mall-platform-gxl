ALTER TABLE xquark_sku
  ADD height double DEFAULT 0 NOT NULL
COMMENT '商品的高度';
ALTER TABLE xquark_sku
  ADD width double DEFAULT 0 NOT NULL
COMMENT '商品宽度';
ALTER TABLE xquark_sku
  ADD length double DEFAULT 0 NOT NULL
COMMENT '商品长度';
-- ALTER TABLE xquark_product ADD length_unit varchar(8) DEFAULT 'CM' NOT NULL COMMENT '商品的长度单位';
ALTER TABLE xquark_sku ADD num_in_package int DEFAULT 0 not  null COMMENT '每箱多少个';
ALTER TABLE xquark_sku ADD weight int(11) DEFAULT 0
COMMENT '商品重量';
