/*
  Tables for new spider
*/

DROP TABLE IF EXISTS `desc`;
CREATE TABLE `desc` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `desc_url` varchar(200) DEFAULT NULL,
  `details` mediumtext,
  `fragments` mediumtext,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=263212 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `http_request_error`;
CREATE TABLE `http_request_error` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(200) NOT NULL,
  `status_code` int(10) NOT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status_code` (`status_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `img`;
CREATE TABLE `img` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `img_url` varchar(256) DEFAULT NULL,
  `img` varchar(256) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `order_num` smallint(5) unsigned DEFAULT '1',
  `md5` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_item_img_list` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=46025 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `price` double(10,2) unsigned DEFAULT NULL,
  `amount` int(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_id` varchar(64) DEFAULT NULL,
  `item_url` varchar(256) DEFAULT NULL,
  `shop_type` varchar(8) DEFAULT NULL,
  `item_id` varchar(32) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT '1' COMMENT '1-正常，2-item信息需要补全，3-下架',
  `req_from` varchar(8) DEFAULT '1' COMMENT '1-KKKD,2-XIANGQU',
  `user_id` varchar(32) DEFAULT NULL COMMENT '第三方店铺用户ID',
  `shop_id` varchar(32) DEFAULT NULL COMMENT '第三方店铺店铺ID',
  `details` mediumtext,
  `decode_shop_id` bigint(20) DEFAULT NULL,
  `imgs_count` int(20) DEFAULT NULL,
  `completed` tinyint(4) DEFAULT '0',
  `desc` varchar(64) DEFAULT NULL,
  `sku_props` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_item_id` (`ouer_shop_id`, `item_id`) USING BTREE,
  KEY `idx_user_shop_item_list` (`req_from`,`ouer_user_id`,`ouer_shop_id`,`shop_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2146 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `item_failed`;
CREATE TABLE `item_failed` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned NOT NULL,
  `item_failed` bit(1) NOT NULL DEFAULT b'0',
  `desc_failed` bit(1) NOT NULL DEFAULT b'0',
  `sku_failed` bit(1) NOT NULL DEFAULT b'0',
  `group_img_failes` int(10) unsigned DEFAULT NULL,
  `sku_img_failes` int(10) unsigned DEFAULT NULL,
  `detail_img_failes` int(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=261868 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `item_process`;
CREATE TABLE `item_process` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned DEFAULT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `desc_parsed` bit(1) NOT NULL DEFAULT b'0',
  `sku_parsed` bit(1) NOT NULL DEFAULT b'0',
  `group_img_count` int(10) unsigned DEFAULT NULL,
  `sku_img_count` int(10) unsigned DEFAULT NULL,
  `detail_img_count` int(10) unsigned DEFAULT NULL,
  `cur_group_img_count` int(10) unsigned DEFAULT NULL,
  `cur_sku_img_count` int(10) unsigned DEFAULT NULL,
  `cur_detail_img_count` int(10) unsigned DEFAULT NULL,
  `type` tinyint(4) unsigned DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shop_id` (`shop_id`) USING BTREE,
  UNIQUE KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=263224 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `item_task`;
CREATE TABLE `item_task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` varchar(32) NOT NULL,
  `item_url` varchar(100) DEFAULT NULL,
  `shop_url` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `t_user_id` varchar(32) DEFAULT NULL,
  `shop_type` varchar(16) DEFAULT NULL,
  `parser_type` varchar(16) DEFAULT NULL,
  `request_url` varchar(100) DEFAULT NULL,
  `req_from` varchar(16) DEFAULT NULL,
  `device_type` varchar(16) DEFAULT NULL,
  `retry` bit(1) NOT NULL DEFAULT b'1',
  `retry_incr` bit(1) NOT NULL DEFAULT b'1',
  `retry_times` int(10) unsigned DEFAULT NULL,
  `status` tinyint(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=263383 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `item_task_audit`;
CREATE TABLE `item_task_audit` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` varchar(32) NOT NULL,
  `item_url` varchar(100) DEFAULT NULL,
  `shop_url` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `t_user_id` varchar(32) DEFAULT NULL,
  `shop_type` varchar(16) DEFAULT NULL,
  `parser_type` varchar(16) DEFAULT NULL,
  `request_url` varchar(100) DEFAULT NULL,
  `req_from` varchar(16) DEFAULT NULL,
  `device_type` varchar(16) DEFAULT NULL,
  `retry` bit(1) NOT NULL DEFAULT b'1',
  `retry_incr` bit(1) NOT NULL DEFAULT b'1',
  `retry_times` int(10) unsigned DEFAULT NULL,
  `status` tinyint(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=263383 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `itemlist_process`;
CREATE TABLE `itemlist_process` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` bigint(20) unsigned NOT NULL,
  `item_count` int(10) unsigned DEFAULT NULL,
  `cur_item_count` int(10) unsigned DEFAULT NULL,
  `partially` bit(1) NOT NULL DEFAULT b'0',
  `type` tinyint(4) unsigned DEFAULT '0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_id` (`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=261881 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `itemlist_task`;
CREATE TABLE `itemlist_task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_url` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `t_user_id` varchar(32) DEFAULT NULL,
  `shop_type` varchar(16) DEFAULT NULL,
  `parser_type` varchar(16) DEFAULT NULL,
  `request_url` varchar(100) DEFAULT NULL,
  `req_from` varchar(16) DEFAULT NULL,
  `device_type` varchar(16) DEFAULT NULL,
  `retry` bit(1) NOT NULL DEFAULT b'1',
  `retry_incr` bit(1) NOT NULL DEFAULT b'1',
  `retry_times` int(10) unsigned DEFAULT NULL,
  `status` tinyint(10) unsigned DEFAULT NULL,
  `partially` bit(1) NOT NULL DEFAULT b'0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ouer_shop_id` (`ouer_shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=261886 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `req_from` varchar(8) DEFAULT '1' COMMENT '1-KKKD,2-XIANGQU',
  `user_id` varchar(32) DEFAULT NULL COMMENT '第三方店铺用户ID',
  `shop_id` varchar(32) DEFAULT NULL COMMENT '第三方店铺店铺ID',
  `shop_url` varchar(256) DEFAULT NULL COMMENT '第三方店铺的URL',
  `shop_type` varchar(8) DEFAULT NULL COMMENT '1-TAOBAO;2-TMALL',
  `name` varchar(64) DEFAULT NULL COMMENT '店铺名称',
  `nickname` varchar(64) DEFAULT NULL COMMENT '掌柜昵称',
  `score` varchar(64) DEFAULT NULL COMMENT '店铺评分',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ouer_user_shop_list` (`req_from`,`ouer_user_id`,`ouer_shop_id`) USING BTREE,
  KEY `idx_user_shop` (`req_from`,`shop_type`,`user_id`,`shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `shop_task`;
CREATE TABLE `shop_task` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_url` varchar(100) DEFAULT NULL,
  `ouer_shop_id` varchar(32) DEFAULT NULL,
  `ouer_user_id` varchar(32) DEFAULT NULL,
  `t_user_id` varchar(32) DEFAULT NULL,
  `shop_type` varchar(16) DEFAULT NULL,
  `parser_type` varchar(16) DEFAULT NULL,
  `request_url` varchar(100) DEFAULT NULL,
  `req_from` varchar(16) DEFAULT NULL,
  `device_type` varchar(16) DEFAULT NULL,
  `retry` bit(1) NOT NULL DEFAULT b'1',
  `retry_incr` bit(1) NOT NULL DEFAULT b'1',
  `retry_times` int(10) unsigned DEFAULT NULL,
  `status` tinyint(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ouer_shop_id` (`ouer_shop_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=261890 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `sku`;
CREATE TABLE `sku` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) unsigned DEFAULT NULL,
  `spec` varchar(256) DEFAULT NULL,
  `price` double(10,2) unsigned DEFAULT NULL,
  `amount` int(10) unsigned DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `orig_spec` varchar(256) DEFAULT NULL,
  `img_url` varchar(200) DEFAULT NULL,
  `sku_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_item_sku_list` (`item_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13414 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `proxy`;
CREATE TABLE `proxy` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) DEFAULT NULL,
  `port` int(11) unsigned NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  `check_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cur_page` int(11) unsigned NOT NULL,
  `source` varchar(32) DEFAULT NULL,
  `check_time_2_cur` bit(1) NOT NULL DEFAULT b'0',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `item_id` (`ip`, `port`) USING BTREE,
  KEY `check_time` (`check_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=263212 DEFAULT CHARSET=utf8mb4;

