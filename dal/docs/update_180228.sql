ALTER TABLE xquark_product ADD COLUMN `model` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '产品型号' AFTER `encode`;

ALTER TABLE xquark_product MODIFY COLUMN `model` VARCHAR(50) DEFAULT '' COMMENT '产品型号' AFTER `encode`;
