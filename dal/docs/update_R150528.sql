alter table `xquark_coupon_activity` add column `type` varchar(64) default 'REDUCTION' after title;
update `xquark_coupon_activity` set type = 'CODE' where act_code like '%COUPONCODE';

ALTER TABLE `xquark_coupon_activity`
ADD COLUMN `partner`  varchar(20) NULL AFTER `id`;

ALTER TABLE `xquark_coupon_activity`
DROP COLUMN `grant_rule`,
ADD COLUMN `grant_rule`  varchar(20) NULL COMMENT '发放规则，SINGLE用户单次，MULTIPLE设备单次 DEVICE_SINGLE用户多次' AFTER `valid`;

insert into xquark_coupon_activity(partner, act_code, title, type, default_select, min_price, discount, valid, 
grant_rule, auto_use, valid_from, valid_to, partner_id, created_at, updated_at)
values('xiangqu', 'XQ.61', '大儿童专属红包', 'REDUCTION', 1, 100, 10, 1, 
'DEVICE_SINGLE', 0, '2015-05-29 00:00:00', '2015-06-01 23:59:59', 16630961, now(), now());

/* 需要先插入xq.61活动 */
alter table `xquark_coupon_activity` add column `type` varchar(64) default 'REDUCTION' after title;
alter table `xquark_coupon_activity` add column `default_select` tinyint default 0 after type;
update `xquark_coupon_activity` set type = 'CODE' where act_code like '%COUPONCODE';
update `xquark_coupon_activity` set default_select = 1 where act_code in ('XQ.FIRST', 'XQ.61');


ALTER TABLE `xquark_coupon_activity` ADD INDEX `idx_coupon_partner` (`partner`) ;

update xquark_coupon_activity set partner = 'xiangqu';

ALTER TABLE `xquark_coupon_activity`
MODIFY COLUMN `partner`  varchar(20) NOT NULL AFTER `id`;

ALTER TABLE `xquark_coupon`
ADD COLUMN `device_id`  varchar(100) NULL COMMENT '设备id' AFTER `pay_no`;

ALTER TABLE `xquark_coupon`
DROP INDEX `code`,
ADD INDEX `idx_coupon_did` (`activity_id`, `device_id`) ;




