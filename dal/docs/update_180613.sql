/*
  2018.6.19
  产品表增加了条形码和仓库编号字段
*/

/*
  2018.6.27
  删除了发货产品表子单号的约束
 */
/*
 2018.6.28
 stock_sku从int改为了varchar
 */
/**
 2018.6.29
 增加了brand_name字段,修改积分德分字段，端口号，商城号，立减，运费减免，其他调整
 */
CREATE TABLE `xquark_wms_product` (
  `id`             bigint(20)  NOT NULL AUTO_INCREMENT
  COMMENT '主键ID',
  `code`           varchar(40) NOT NULL
  COMMENT '产品编号',
  `name`           varchar(40) NOT NULL
  COMMENT '产品名称',
  `num_in_package` int(10) COMMENT '装箱数',
  `edi_status`     int(1)      NOT NULL
  COMMENT 'EDI状态',
  `updated_by`     varchar(40) NOT NULL
  COMMENT '更新方',
  `bar_code`       varchar(64)          DEFAULT ''
  COMMENT '条形码',
  `ware_house_id`  bigint(20) COMMENT '仓库编号',
  `brand_name`     varchar(30) COMMENT '品牌名称',
  `created_at`     datetime    NOT NULL,
  `updated_at`     datetime    NULL     DEFAULT NULL,
  `archive`        tinyint(1)  NOT NULL DEFAULT '0'
  COMMENT '是否逻辑删除',
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS产品表';

CREATE TABLE `xquark_wms_inventory` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
 `warehouse_code` varchar(40) NOT NULL COMMENT '仓库编号',
 `batch_id` varchar(20) NOT NULL COMMENT '批次编号',
 `code` varchar(40) NOT NULL COMMENT '产品货号',
 `amount` int(10) NOT NULL COMMENT '库存数',
 `edi_status` int(1) NOT NULL COMMENT 'EDI状态',
 `created_at`  datetime NOT NULL ,
 `updated_at`  datetime NULL DEFAULT NULL,
`archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS库存表';

CREATE TABLE `xquark_wms_batch` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
 `batch_no` varchar(20) NOT NULL COMMENT '批次号',
 `tpl_status` varchar(10) NOT NULL COMMENT '3PL标识',
 `hds_status` varchar(10) NOT NULL COMMENT 'HDS标识',
 `edi_status` int(1) NOT NULL COMMENT 'EDI状态',
 `created_at`  datetime NOT NULL ,
 `updated_at`  datetime NULL DEFAULT NULL,
`archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS批次表';

CREATE TABLE `xquark_wms_order` (
  `id`                  bigint(20)   NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
  `order_no`            varchar(30)  NOT NULL COMMENT '订单号',
  `type`                varchar(20)  NOT NULL
  COMMENT '订单类型',
  `warehouse_code`      varchar(40)  NOT NULL
  COMMENT '发货仓库',
  `order_time`          datetime     NOT NULL
  COMMENT '订单时间',
  `name`                varchar(20)  NOT NULL
  COMMENT '订货人',
  `buyer_id`            varchar(20)  NOT NULL
  COMMENT '订货人卡号',
  `consignee`           varchar(20)  NOT NULL
  COMMENT '收货人',
  `phone`               varchar(20)  NOT NULL
  COMMENT '收货人手机号',
  `pay_type`            varchar(20) COMMENT '支付方式',
  `total_fee`           decimal(20)  NOT NULL
  COMMENT '总计金额',
  `discount_fee`        decimal(20)  NOT NULL
  COMMENT '扣除',
  `paid_fee`            decimal(20) COMMENT '应付金额',
  `consumption_points`  decimal(10, 2) COMMENT '积分消费',
  `paid_point`          decimal(10, 2) COMMENT '德分消费',
  `logistics_fee`       decimal(20)  NOT NULL
  COMMENT '实算运费',
  `freight_discount`    decimal(20)  NOT NULL
  COMMENT '运费折扣',
  `weight`              decimal(10)  NOT NULL
  COMMENT '订单重量',
  `volume`              decimal(10)  NOT NULL
  COMMENT '订单体积',
  `address`             varchar(200) NOT NULL
  COMMENT '收货地址',
  `zipcode`             varchar(20)  NOT NULL
  COMMENT '邮编',
  `status`              int(10)      NOT NULL
  COMMENT '发货状态',
  `remark`              varchar(400) COMMENT '发货说明',
  `logistics_order_no`  varchar(30) COMMENT '端口号(包裹号)',
  `main_order_id`       varchar(30) COMMENT '商城号(主订单号)',
  `logistic_discount`   decimal(18, 2)        DEFAULT 0
  COMMENT '配送费减免',
  `reduction`           decimal(18, 2)        DEFAULT 0
  COMMENT '立减',
  `other_adjustments`   decimal(18, 2)        DEFAULT 0
  COMMENT '其他调整',
  `wms_edi_status`      int(1)       NOT NULL
  COMMENT 'MWSEDI状态',
  `local_edi_status`    int(1)       NOT NULL
  COMMENT 'MYEDI状态',
  `created_at`          datetime     NOT NULL,
  `updated_at`          datetime     NULL     DEFAULT NULL,
  `recently_updated_at` datetime     NULL     DEFAULT NULL
  COMMENT '最近一次更新时间',
  `edi_status_for_wuyou` int(1),
  `archive`             tinyint(1)   NOT NULL DEFAULT '0'
  COMMENT '是否逻辑删除',
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS订单表';

CREATE TABLE `xquark_wms_order_nts_product` (
  `id`                  bigint(20)     NOT NULL AUTO_INCREMENT
  COMMENT '主键ID',
  `order_no`            varchar(30)    NOT NULL
  COMMENT '订单号',
  `sub_order_no`        varchar(30) COMMENT '子单号',
  `stock_sku`           varchar(30)    NOT NULL
  COMMENT '库存编码',
  `product_name`        varchar(200)   NOT NULL
  COMMENT '产品名称',
  `type`                varchar(20) COMMENT '产品类别',
  `amount`              int(10)        NOT NULL
  COMMENT '订购数量',
  `market_price`        decimal(20, 2) NOT NULL
  COMMENT '销售单价',
  `discount`            decimal(20) COMMENT '折扣',
  `combination_no`      varchar(40) COMMENT '套装编号',
  `combination_name`    varchar(200) COMMENT '套装名称',
  `combination_num`     int(10) COMMENT '套装数量',
  `show_combination`    bit(1) COMMENT '是否在订单上显示',
  `is_combination`      bit(1) COMMENT '是否为套装',
  `remark`              varchar(400) COMMENT '发货说明',
  `edi_status`          int(1)         NOT NULL
  COMMENT 'EDI状态',
  `created_at`          datetime       NOT NULL ,
  `updated_at`          datetime       NULL DEFAULT NULL ,
  `recently_updated_at` datetime       NULL DEFAULT NULL COMMENT '最近一次更新时间',
  `archive`             tinyint(1)     NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS发货通知单产品表';

CREATE TABLE `xquark_wms_order_package` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
 `order_no` varchar(30) NOT NULL COMMENT '订单号',
 `package_type` varchar(40) NOT NULL COMMENT '推荐包装箱规格',
 `package_name` varchar(40) NOT NULL COMMENT '箱型名称',
 `ordering_qty` int(10) NOT NULL COMMENT '推荐包装箱数量',
 `warehouse_qty` int(10) NOT NULL COMMENT '实际包装箱数量',
 `edi_status` int(1) NOT NULL COMMENT 'EDI状态',
 `created_at`  datetime NOT NULL ,
 `updated_at`  datetime NULL DEFAULT NULL ,
 `recently_updated_at`  datetime NULL DEFAULT NULL COMMENT '最近一次更新时间',
`archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS订单包装信息表';

CREATE TABLE `xquark_wms_express` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
 `order_no` varchar(30) NOT NULL COMMENT '订单号',
 `time` datetime NOT NULL COMMENT '时间',
 `express_company_name` varchar(40) NOT NULL COMMENT '快递公司',
 `express_no` varchar(20) NOT NULL COMMENT '快递单号',
 `route` varchar(400) NOT NULL COMMENT '路由',
 `edi_status` int(1) NOT NULL COMMENT 'EDI状态',
 `created_at`  datetime NOT NULL ,
 `updated_at`  datetime NULL DEFAULT NULL ,
`archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='WMS快递信息表';


CREATE TABLE `xquark_package` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT  COMMENT '主键ID',
 `name` varchar(30) NOT NULL COMMENT '箱型名称',
 `weight` int(10) NOT NULL COMMENT '重量',
 `length` int(10) NOT NULL COMMENT '长度',
 `width` int(10) NOT NULL COMMENT '宽度',
 `height` int(10) NOT NULL COMMENT '高度',
 `price` decimal(20,3) NOT NULL COMMENT '价格',
 `created_at`  datetime NOT NULL ,
 `updated_at`  datetime NULL DEFAULT NULL ,
`archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
 PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品包装箱表';
