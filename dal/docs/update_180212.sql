CREATE TABLE `xquark_product_filter` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT(20) NOT NULL
  COMMENT '商品id',
  `filter_id`  BIGINT(20) NOT NULL
  COMMENT '滤芯id',
  `created_at` DATETIME            DEFAULT NULL
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='商品滤芯关联表';

CREATE TABLE `xquark_product_user` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT(20) NOT NULL
  COMMENT '商品id',
  `user_id`  BIGINT(20) NOT NULL
  COMMENT '用户id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='商品用户关联表';
