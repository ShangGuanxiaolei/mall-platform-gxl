ALTER TABLE xquark_user
  ADD INDEX (union_id, archive);
ALTER TABLE xquark_user
  ADD INDEX (phone, archive);

ALTER TABLE xquark_product_user
  ADD INDEX (user_id, product_id, archive);
ALTER TABLE xquark_product_user
  ADD INDEX (user_id, archive);
ALTER TABLE xquark_product_user
  ADD INDEX (product_id, archive);

ALTER TABLE xquark_address
  ADD INDEX (ext_id, archive);
ALTER TABLE xquark_address
  ADD INDEX (user_id, archive);

ALTER TABLE xquark_zone ADD INDEX (name, archive);
ALTER TABLE xquark_zone ADD INDEX (parent_id, archive);
ALTER TABLE xquark_address_tmp ADD INDEX (union_id);
ALTER TABLE xquark_address_tmp ADD INDEX (city_name);
ALTER TABLE xquark_address_tmp ADD INDEX (area_name, city_name);