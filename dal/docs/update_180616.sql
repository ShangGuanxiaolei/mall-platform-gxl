ALTER TABLE xquark_product
  ADD totalcount int(8) DEFAULT 1 NOT NULL
COMMENT '外部商家编码';
ALTER TABLE xquark_product
  ADD outer_id varchar(16) DEFAULT "" NOT NULL
COMMENT '外部商家编码';
ALTER TABLE xquark_product
  ADD num int DEFAULT 0 NOT NULL
COMMENT '商品数量';
ALTER TABLE xquark_product
  ADD whse_code varchar(16) DEFAULT "" NOT NULL
COMMENT '商品所在仓库编号';
ALTER TABLE xquark_product
  ADD sku_id varchar(32) DEFAULT "" NOT NULL
COMMENT '平台规格ID';
ALTER TABLE xquark_product
  ADD sku_outer_id varchar(32) DEFAULT "" NOT NULL
COMMENT '规格外部商家编码';
ALTER TABLE xquark_product
  ADD sku_price decimal DEFAULT 0 NOT NULL
COMMENT '规格价格';
ALTER TABLE xquark_product
  ADD sku_quantity int DEFAULT 0 NOT NULL
COMMENT '规格数量';
ALTER TABLE xquark_product
  ADD sku_name varchar(16) DEFAULT "" NOT NULL
COMMENT '规格名称';
ALTER TABLE xquark_product
  ADD sku_property varchar(16) DEFAULT "" NOT NULL
COMMENT '规格属性';
ALTER TABLE xquark_product
  ADD sku_picture_url varchar(108) DEFAULT "" NOT NULL
COMMENT '规格图片URL';
alter table xquark_product drop totalcount;