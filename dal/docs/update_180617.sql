alter table xquark_order
  add column `paid_point` DECIMAL(10, 2) DEFAULT 0
COMMENT '下单使用德分'
  after paid_yundou;
alter table xquark_order
  add column `paid_commission` DECIMAL(10, 2) DEFAULT 0
COMMENT '下单使用佣金'
  after paid_point;
