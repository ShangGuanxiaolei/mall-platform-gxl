DROP TABLE IF EXISTS `xquark_domain_login_strategy`;
CREATE TABLE `xquark_domain_login_strategy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) unsigned NOT NULL COMMENT '域id',
  `profile` varchar(16) NOT NULL COMMENT '运行时环境',
  `module_name` varchar(16) NOT NULL COMMENT '运行时模块',
  `domain_name` varchar(64) NOT NULL COMMENT '域名',
  `login_strategy_type` varchar(16) NOT NULL COMMENT 'login strategy 类型',
  `login_strategy_value` varchar(128) NOT NULL COMMENT 'login strategy 值',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_dpmd` (`domain_id`, `profile`, `module_name`, `domain_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;