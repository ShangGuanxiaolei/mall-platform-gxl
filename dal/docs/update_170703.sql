DROP TABLE IF EXISTS `xquark_health_test_module`;
CREATE TABLE `xquark_health_test_module` (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `parent_id`   BIGINT               DEFAULT NULL
  COMMENT '父标题id',
  `name`        VARCHAR(50) NOT NULL
  COMMENT '标题名称',
  `type`        VARCHAR(50) NOT NULL
  COMMENT '算分方式',
  `icon`        VARCHAR(255)         DEFAULT NULL
  COMMENT '图标',
  `is_leaf`     TINYINT     NOT NULL DEFAULT FALSE
  COMMENT '是否子节点',
  `description` VARCHAR(255)         DEFAULT NULL
  COMMENT '标题描述',
  `sort_no`     INT         NOT NULL
  COMMENT '排序号',
  `created_at`  DATETIME             DEFAULT NULL,
  `updated_at`  DATETIME             DEFAULT NULL,
  `archive`     TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康自测标题表';

DROP TABLE IF EXISTS `xquark_health_test_question`;
CREATE TABLE `xquark_health_test_question` (
  `id`         BIGINT      NOT NULL AUTO_INCREMENT,
  `module_id`  BIGINT      NOT NULL
  COMMENT '菜单id',
  `answer_id`  BIGINT               DEFAULT NULL
  COMMENT '正确答案的id',
  `group_id`   BIGINT      NOT NULL
  COMMENT '答案模板的id',
  `name`       VARCHAR(50) NOT NULL
  COMMENT '题目名称',
  `type`       VARCHAR(20) NOT NULL
  COMMENT '题目类型',
  `point`      DOUBLE               DEFAULT NULL
  COMMENT '答对的分值',
  `created_at` DATETIME             DEFAULT NULL,
  `updated_at` DATETIME             DEFAULT NULL,
  `archive`    TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康题目表';

DROP TABLE IF EXISTS `xquark_health_test_answer_group`;
CREATE TABLE `xquark_health_test_answer_group` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(50)         DEFAULT NULL
  COMMENT '题目组名称',
  `share`      TINYINT    NOT NULL DEFAULT FALSE
  COMMENT '是否共享',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康题目组表';

DROP TABLE IF EXISTS `xquark_health_test_answer`;
CREATE TABLE `xquark_health_test_answer` (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `group_id`   BIGINT       NOT NULL
  COMMENT '答案组id',
  `option`     VARCHAR(20)           DEFAULT NULL
  COMMENT '选项序号',
  `score`      DOUBLE                DEFAULT NULL
  COMMENT '分数',
  `alias`      INT          NOT NULL
  COMMENT '答案别名 统计用',
  `content`    VARCHAR(255) NOT NULL
  COMMENT '答案内容',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康题目选项表';

DROP TABLE IF EXISTS `xquark_health_test_result`;
CREATE TABLE xquark_health_test_result (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT,
  `module_id`   BIGINT      NOT NULL
  COMMENT '菜单id',
  `physique_id` BIGINT               DEFAULT NULL
  COMMENT '体质表id， 没有表示非体质测试',
  `start`       DOUBLE               DEFAULT NULL
  COMMENT '分数范围开始',
  `end`         DOUBLE               DEFAULT NULL
  COMMENT '分数范围结束',
  `type`        VARCHAR(20) NOT NULL
  COMMENT '计算类型',
  `description` TEXT                 DEFAULT NULL
  COMMENT '答案描述',
  `created_at`  DATETIME             DEFAULT NULL,
  `updated_at`  DATETIME             DEFAULT NULL,
  `archive`     TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康题目结果表';

DROP TABLE IF EXISTS `xquark_health_test_physique`;
CREATE TABLE xquark_health_test_physique (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(50) NOT NULL
  COMMENT '体质名称',
  `description` TEXT        NOT NULL
  COMMENT '体制描述',
  `created_at`  DATETIME             DEFAULT NULL,
  `updated_at`  DATETIME             DEFAULT NULL,
  `archive`     TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康测试体质表';

DROP TABLE IF EXISTS `xquark_health_test_profile`;
CREATE TABLE `xquark_health_test_profile` (
  `id`         BIGINT      NOT NULL AUTO_INCREMENT,
  `user_id`    BIGINT      NOT NULL
  COMMENT '用户id',
  `gender`     VARCHAR(20) NOT NULL
  COMMENT '用户性别',
  `height`     INT         NOT NULL
  COMMENT '身高',
  `weight`     INT         NOT NULL
  COMMENT '体重',
  `pregnant`   TINYINT     NOT NULL
  COMMENT '是否怀孕',
  `bmi`        DOUBLE               DEFAULT NULL
  COMMENT 'BMI指数',
  `medicine`   VARCHAR(100)         DEFAULT NULL
  COMMENT '当前服用药物',
  `created_at` DATETIME             DEFAULT NULL,
  `updated_at` DATETIME             DEFAULT NULL,
  `archive`    TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '健康测试用户信息表';


