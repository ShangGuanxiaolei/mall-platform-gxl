# 会员序列生成表
DROP TABLE IF EXISTS xquark_member_sequence_generator;
CREATE TABLE xquark_member_sequence_generator (
  id   bigint(20) unsigned NOT NULL auto_increment,
  stub char(1)            NOT NULL default '',
  PRIMARY KEY (id),
  UNIQUE KEY stub (stub)
)
  ENGINE = MyISAM;

# 银行序列生成表
DROP TABLE IF EXISTS xquark_bank_sequence_generator;
CREATE TABLE xquark_bank_sequence_generator (
  id   bigint(20) unsigned NOT NULL auto_increment,
  stub char(1)            NOT NULL default '',
  PRIMARY KEY (id),
  UNIQUE KEY stub (stub)
)
  ENGINE = MyISAM;

# 地址序列生成表
DROP TABLE IF EXISTS xquark_address_sequence_generator;
CREATE TABLE xquark_address_sequence_generator (
  id   bigint(20) unsigned NOT NULL auto_increment,
  stub char(1)            NOT NULL default '',
  PRIMARY KEY (id),
  UNIQUE KEY stub (stub)
)
  ENGINE = MyISAM;

INSERT INTO xquark_member_sequence_generator value (3000000, 'a');
INSERT INTO xquark_address_sequence_generator value (30000, 'a');
INSERT INTO xquark_bank_sequence_generator value (30000, 'a');
