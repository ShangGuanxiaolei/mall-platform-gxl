/*
type 拼团类型:PIECE_TO_VIP
 */
create table promotion_to_vip
(
  id         bigint unsigned auto_increment
    primary key,
  title      varchar(40) charset utf8mb4          not null comment '标题',
  describes  varchar(40) charset utf8mb4          not null comment '升级vip描述',
  state      tinyint(1) default '0'               not null comment '状态 0 - 停用, 1 - 启用',
  to_number  int        default '0'               null comment '转变为vip所需的次数',
  type       varchar(20)                          null comment '活动类型',
  archive    tinyint(1) default '0'                 null comment '0有效，1已删除',
  updater    varchar(16) charset utf8mb4          null comment '更新人',
  created_at timestamp  default CURRENT_TIMESTAMP null,
  updated_at timestamp                            null on update CURRENT_TIMESTAMP
)
  collate = utf8mb4_unicode_ci comment '后台配置白人升级vip表';

create table promotion_to_conserve
(
  id         bigint(20) unsigned auto_increment primary key,
  cpId       bigint(20)                            not null
    comment '记录无vip资格的白人拼主',
  pg_num     int(11)    default '0'                not null
    comment '新人团的成团次数',
  created_at timestamp  default CURRENT_TIMESTAMP  null,
  updated_at timestamp on update CURRENT_TIMESTAMP null,
  archive    tinyint(1) default '0'                not null
    comment '0有效，1已删除'
)
  collate = utf8mb4_unicode_ci comment '白人成团次数记录表';