ALTER TABLE xquark_order_refund ADD wms_check_status varchar(32) DEFAULT 'UNCHECKED' NULL COMMENT '仓库的审核状态';
ALTER TABLE xquark_order_refund ADD wms_check_remark varchar(512) DEFAULT '' NULL;
ALTER TABLE xquark_order_refund ADD wms_check_time datetime DEFAULT null NULL COMMENT '审核时间';