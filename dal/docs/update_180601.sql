# 增加商品来源字段, 默认自营
ALTER TABLE xquark_product ADD COLUMN `source_type` VARCHAR(24) DEFAULT 'SELF';

ALTER TABLE xquark_product DROP COLUMN `source_type`;

ALTER TABLE xquark_product MODIFY COLUMN `img` VARCHAR(255) DEFAULT '' COMMENT '商品图片';
ALTER TABLE xquark_product_image MODIFY COLUMN `img` VARCHAR(255) DEFAULT '' COMMENT '商品图片';

ALTER TABLE xquark_category ADD COLUMN `source` VARCHAR(48) DEFAULT 'CLIENT';

DROP TABLE IF EXISTS `xquark_brand`;
CREATE TABLE `xquark_brand` (
  `id`          bigint(20)   NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `name`        varchar(50)  NOT NULL
  COMMENT '品牌名',
  `site_host`   varchar(255) NOT NULL
  COMMENT '品牌主页',
  `description` varchar(255)          DEFAULT NULL
  COMMENT '品牌描述',
  `sort_order`  INTEGER               DEFAULT 0
  COMMENT '排序字段',
  `logo`        varchar(255)          DEFAULT 0
  COMMENT '品牌图片',
  `source`      varchar(50)  NOT NULL
  COMMENT '品牌来源',
  `source_id`   varchar(20)           DEFAULT NULL
  COMMENT '第三方品牌id',
  `is_show`     TINYINT COMMENT '是否展示',
  `created_at`  datetime              DEFAULT NULL,
  `updated_at`  datetime              DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='品牌表';

DROP TABLE IF EXISTS `xquark_brand_product`;
CREATE TABLE `xquark_brand_product` (
  `id`         bigint(20) NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `product_id` bigint(20) NOT NULL
  COMMENT '商品id',
  `brand_id`   bigint(20) NOT NULL
  COMMENT '品牌id',
  `created_at` datetime            DEFAULT NULL,
  `updated_at` datetime            DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='商品品牌关联表';
