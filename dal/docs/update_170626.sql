DROP TABLE IF EXISTS `xquark_member_card`;
CREATE TABLE `xquark_member_card` (
  `id`                 BIGINT           NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `name`               VARCHAR(255)     NOT NULL
  COMMENT '会员卡名称',
  `bg_color`           VARCHAR(100)     NOT NULL
  COMMENT '会员卡颜色',
  `background`         VARCHAR(255)              DEFAULT NULL
  COMMENT '会员卡背景色',
  `level`              INTEGER UNSIGNED NOT NULL
  COMMENT '会员卡等级值',
  `discount`           DECIMAL(4, 2)    NOT NULL
  COMMENT '会员折扣',
  `free_delivery`      TINYINT          NOT NULL DEFAULT FALSE
  COMMENT '是否享受包邮特权, 默认不享受',
  `upgrade_type`       VARCHAR(20)      NOT NULL DEFAULT 'AUTO'
  COMMENT '升级类型， AUTO: 自动， MANUAL： 手动, BUY 购买',
  `upgrade_deal_no`    BIGINT                    DEFAULT NULL
  COMMENT '升级条件1 - 交易笔数',
  `upgrade_consume_no` DECIMAL(10, 2)            DEFAULT NULL
  COMMENT '升级条件2 - 消费金额',
  `upgrade_point`      BIGINT                    DEFAULT NULL
  COMMENT '升级条件3 - 积分数量',
  `created_at`         DATETIME                  DEFAULT NULL,
  `updated_at`         DATETIME                  DEFAULT NULL,
  `archive`            TINYINT(4)       NOT NULL DEFAULT FALSE,
  `remark`             VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '会员卡表';
