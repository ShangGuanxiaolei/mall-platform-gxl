INSERT INTO hvmall.xquark_module (id, parent_id, name, url, icon_name, is_leaf, is_auto_expand, sort_no, created_at, archive, updated_at, module, page)
VALUES
  (36, null, '积分、德分导入', '/mall/pointImport', 'icon-stats-bars', 0, 0, 36, '2018-07-30 14:24:13', 0,
       '2018-07-30 16:10:04', 'pointImport', 'reports');
INSERT INTO hvmall.xquark_module (id, parent_id, name, url, icon_name, is_leaf, is_auto_expand, sort_no, created_at, archive, updated_at, module, page)
VALUES
  (35, null, '收入报表-按订单', '/mall/exportByOrder', 'icon-pie-chart7', 0, 0, 35, '2018-07-30 14:18:37',
       0, '2018-07-30 16:09:44', 'exportByOrder', 'reports');
INSERT INTO hvmall.xquark_module (id, parent_id, name, url, icon_name, is_leaf, is_auto_expand, sort_no, created_at, archive, updated_at, module, page)
VALUES (34, null, '收入报表-按客户', '/mall/exportByCustomer', 'icon-stats-bars2', 0, 0, 34,
            '2018-07-30 14:17:58', 0, '2018-07-30 16:09:32', 'exportByCustomer', 'reports');
INSERT INTO hvmall.xquark_module (id, parent_id, name, url, icon_name, is_leaf, is_auto_expand, sort_no, created_at, archive, updated_at, module, page)
VALUES
  (33, null, '综合报表', '/mall/exportByCustomer', 'icon-stats-dots', 0, 0, 33, '2018-07-30 14:16:21',
       0, '2018-07-30 16:09:05', 'reports', 'mall');