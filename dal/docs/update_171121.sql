DROP TABLE IF EXISTS `xquark_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xquark_comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '评价的主键',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `content` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '评论的内容',
  `type` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PRODUCT' COMMENT '评论对象的类型',
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `obj_id` bigint(20) unsigned NOT NULL COMMENT '被评论对象的id',
  `like_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '点赞的数量',
  `user_id` bigint(20) DEFAULT NULL COMMENT '评论用户的id',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `xquark_comment_obj_id_index` (`obj_id`),
  KEY `xquark_comment_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



DROP TABLE IF EXISTS `xquark_comment_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xquark_comment_like` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '点赞的主键',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL COMMENT '点赞用户的id',
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `comment_id` bigint(20) NOT NULL COMMENT '被点赞的评论的id',
  PRIMARY KEY (`id`),
  KEY `xquark_comment_like_comment_id_index` (`comment_id`),
  KEY `xquark_comment_like_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `xquark_comment_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xquark_comment_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) NOT NULL COMMENT '回复评论的id',
  `from_user_id` bigint(20) NOT NULL COMMENT '谁发起的回复',
  `to_user_id` bigint(20) NOT NULL COMMENT '被回复的用户id',
  `content` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '回复的内容',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `xquark_comment_reply_comment_id_index` (`comment_id`),
  KEY `xquark_comment_reply_from_user_id_index` (`from_user_id`),
  KEY `xquark_comment_reply_to_user_id_index` (`to_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;