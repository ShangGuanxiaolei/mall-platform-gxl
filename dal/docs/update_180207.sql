ALTER TABLE xquark_product ADD COLUMN `p_id` BIGINT DEFAULT NULL COMMENT '商品id, (用于滤芯)' AFTER `code`;
ALTER TABLE xquark_product ADD COLUMN `encode` VARCHAR(50) DEFAULT '' COMMENT '商品编码' AFTER `p_id` ;
ALTER TABLE xquark_product ADD COLUMN `type` VARCHAR(50) NOT NULL DEFAULT 'NORMAL' COMMENT '商品类型' AFTER `encode`;
ALTER TABLE xquark_product ADD COLUMN `level` INTEGER DEFAULT NULL COMMENT '滤芯等级' AFTER `type`;
