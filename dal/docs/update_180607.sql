rename table
    xquark_member_sequence_generator to memberGenerator;
rename table
    xquark_bank_sequence_generator to bankGenerator;
rename table
    xquark_address_sequence_generator to addressGenerator;

drop table `customerMessage`;
create table `customerMessage` (
  `id`          bigint(20) NOT NULL auto_increment
  COMMENT '主键',
  `cpId`        bigint,
  `msgCode`     varchar(20) COMMENT 'login',
  `message`     nvarchar(200),
  `source`      tinyint comment '1 hds, 2 vivilife 3 ecommerce',
  `createdDate` datetime,
  `updatedDate` datetime,
  `isRead`      bit                 default 0
  comment '0 unread, 1 read',
  PRIMARY KEY (`id`),
  key cpId_msgCode_source (cpId, msgCode, source)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8
  COMMENT ='customer message';