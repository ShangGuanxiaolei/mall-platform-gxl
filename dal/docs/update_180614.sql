ALTER TABLE xquark_product DROP morality_point_rate;
ALTER TABLE xquark_product ADD support_refund tinyint(1) DEFAULT 0 NOT NULL COMMENT '是否支持退款';
ALTER TABLE xquark_product ADD num_in_package int DEFAULT 0 not  null COMMENT '每箱多少个';
ALTER TABLE xquark_product ADD bar_code varchar(64) DEFAULT '' NOT NULL COMMENT '商品条形码';
ALTER TABLE xquark_product ADD ware_house_id bigint(20) default NULL COMMENT '商品所属仓库';
ALTER TABLE xquark_product ADD package_id bigint(20) DEFAULT null  COMMENT '装箱类型';
