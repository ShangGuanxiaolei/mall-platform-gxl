CREATE TABLE `xquark_order_user_device` (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_ip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `server_ip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ����Ƭ��������� 2015-04-10
CREATE INDEX idx_fragment_id ON xquark_fragment(id);
CREATE INDEX idx_fragment_id ON xquark_fragment_image(fragment_id);
CREATE INDEX idx_fragment_id ON xquark_product_fragment(fragment_id);
CREATE INDEX idx_product_id ON xquark_product_fragment(product_id);

