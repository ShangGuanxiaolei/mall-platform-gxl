-- 新增部门组织表
DROP TABLE IF EXISTS `xquark_org`;
CREATE TABLE `xquark_org` (
  -- AUTO_INCREMENT
  `id`             BIGINT(20)   NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `name`           VARCHAR(255) NOT NULL
  COMMENT '组织名称',
  `parent_id`      VARCHAR(20)           DEFAULT NULL
  COMMENT '父节点id',
  `is_leaf`        TINYINT(4)   NOT NULL DEFAULT FALSE
  COMMENT '是否叶子节点',
  `is_auto_expand` TINYINT(4)   NOT NULL DEFAULT FALSE
  COMMENT '是否自动展开',
  `icon_name`      VARCHAR(255)          DEFAULT NULL
  COMMENT '节点图标文件名称',
  `sort_no`        INT(10)               DEFAULT NULL
  COMMENT '排序号',
  `remark`         VARCHAR(4000)         DEFAULT NULL
  COMMENT '备注',
  `created_at`     DATETIME              DEFAULT NULL,
  `archive`        TINYINT(4)   NOT NULL DEFAULT '0',
  `updated_at`     DATETIME              DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '部门组织表';

-- 用户角色表新增org_id字段
ALTER TABLE `xquark_merchant`
  ADD COLUMN `org_id` BIGINT NOT NULL
COMMENT '所属部门id';

-- 增加菜单表
DROP TABLE IF EXISTS `xquark_module`;
CREATE TABLE `xquark_module` (
  `id`             BIGINT       NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `parent_id`      VARCHAR(20)           DEFAULT NULL
  COMMENT '父节点id',
  `name`           VARCHAR(255) NOT NULL
  COMMENT '功能模块名称',
  `url`            VARCHAR(255)          DEFAULT NULL
  COMMENT '主页面URL',
  `icon_name`      VARCHAR(255)          DEFAULT NULL
  COMMENT '节点图标文件名称',
  `is_leaf`        TINYINT      NOT NULL
  COMMENT '是否叶子节点',
  `is_auto_expand` TINYINT      NOT NULL
  COMMENT '是否自动展开',
  `sort_no`        INT(10)               DEFAULT NULL
  COMMENT '排序号',
  `created_at`     DATETIME     NULL,
  `archive`        TINYINT      NULL
  COMMENT '是否删除',
  `updated_at`     DATETIME     NULL,
  `module`         VARCHAR(255) NOT NULL
  COMMENT 'module名称',
  `page`           VARCHAR(255) NOT NULL
  COMMENT 'page名称',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '菜单表';

-- 增加菜单角色关联表
DROP TABLE IF EXISTS `xquark_merchant_role_module`;
CREATE TABLE `xquark_merchant_role_module` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `role_id`    BIGINT(20) NOT NULL
  COMMENT '角色id',
  `module_id`  BIGINT(20) NOT NULL
  COMMENT '菜单id',
  `created_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4)          DEFAULT NULL
  COMMENT '是否删除',
  `updated_at` DATETIME            DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '菜单-角色关联表';

-- 增加用户角色关联表
DROP TABLE IF EXISTS `xquark_merchant_role_merchant`;
CREATE TABLE `xquark_merchant_role_merchant` (
  `id`         BIGINT   NOT NULL
  COMMENT 'id' AUTO_INCREMENT,
  `user_id`    BIGINT   NOT NULL
  COMMENT '用户id',
  `role_id`    BIGINT   NOT NULL
  COMMENT '角色id',
  `created_at` DATETIME NULL,
  `archive`    TINYINT  NULL
  COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户-角色关联表';

-- 增加菜单-按钮关联表
DROP TABLE IF EXISTS `xquark_module_function`;
CREATE TABLE `xquark_module_function` (
  `id`          BIGINT       NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `function_id` VARCHAR(255) NOT NULL
  COMMENT '按钮在html中的id',
  `module_id`   BIGINT       NOT NULL
  COMMENT '菜单id',
  `created_at`  DATETIME     NULL,
  `archive`     TINYINT      NULL
  COMMENT '是否删除',
  `updated_at`  DATETIME     NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '菜单-按钮关联表';

-- 增加角色按钮关联表
DROP TABLE IF EXISTS `xquark_role_function`;
CREATE TABLE `xquark_role_function` (
  `id`          BIGINT       NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `function_id` VARCHAR(255) NOT NULL
  COMMENT '按钮在html中的id',
  `role_id`     BIGINT       NOT NULL
  COMMENT '角色id',
  `is_show`     TINYINT               DEFAULT TRUE
  COMMENT '是否可见',
  `is_enable`   TINYINT               DEFAULT TRUE
  COMMENT '是否可用',
  `created_at`  DATETIME     NULL,
  `archive`     TINYINT      NULL
  COMMENT '是否删除',
  `updated_at`  DATETIME     NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '菜单-角色关联表';

-- 按钮表
DROP TABLE IF EXISTS `xquark_function`;
CREATE TABLE `xquark_function` (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `module_id`  BIGINT       NOT NULL
  COMMENT '菜单id',
  `name`       VARCHAR(255) NOT NULL
  COMMENT '按钮名称',
  `html_id`    VARCHAR(255) NOT NULL
  COMMENT '按钮在html中的id',
  `created_at` DATETIME     NULL,
  `archive`    TINYINT      NULL
  COMMENT '是否删除',
  `updated_at` DATETIME     NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '按钮表';

-- 角色-按钮关联表
DROP TABLE IF EXISTS `xquark_role_function`;
CREATE TABLE `xquark_role_function` (
  `id`          BIGINT   NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `function_id` BIGINT   NOT NULL
  COMMENT '按钮表id',
  `role_id`     BIGINT   NOT NULL
  COMMENT '角色id',
  `is_show`     TINYINT           DEFAULT TRUE
  COMMENT '是否可见',
  `is_enable`   TINYINT           DEFAULT TRUE
  COMMENT '是否可用',
  `created_at`  DATETIME NULL,
  `archive`     TINYINT  NULL
  COMMENT '是否删除',
  `updated_at`  DATETIME NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '按钮-角色关联表';

-- 添加基础角色
INSERT INTO `xquark_merchant_role` (parent_id, role_name, created_at, role_desc, permissions, archive, updated_at, creater_id)
VALUES
  (0, 'BASIC', now(), '基础角色', '', FALSE, now(), 0);

-- 除ROOT外其他用户都增加BASIC角色
UPDATE xquark_merchant
SET roles = 'AGENT,BASIC'
WHERE roles = 'AGENT';
UPDATE xquark_merchant
SET roles = 'SHIP,BASIC'
WHERE roles = 'SHIP';
UPDATE xquark_merchant
SET roles = 'FINANCE,BASIC'
WHERE roles = 'FINANCE';
UPDATE xquark_merchant
SET roles = 'ADMIN,BASIC'
WHERE roles = 'ADMIN';
UPDATE xquark_merchant
SET roles = 'CUSTOMER,BASIC'
WHERE roles = 'CUSTOMER';

-- 初始用户 为ROOT角色
-- 暂时不使用 role_merchant表
# INSERT INTO `xquark_merchant_role_merchant` VALUES (1, 17496624, 5, now(), 0, now());

-- 初始菜单数据
INSERT INTO `xquark_module` VALUES
  (23, NULL, '帐号管理', '', 'icon-people', 0, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'profile,password,admin', 'merchant,profile'),
  (24, '23', '个人设置', '/profile', 'icon-cog5', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'profile', 'merchant,profile'),
  (25, '23', '修改密码', '/profile/password', 'icon-pen6', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'password', 'merchant,profile'),
  (26, '23', '管理员设置', '/merchant/admin', 'icon-user', 1, 0, 3, '2017-04-27 12:35:07', 1, '2017-04-27 12:35:07', 'admin', 'merchant,profile'),
  (27, NULL, '商品管理', '', ' icon-bucket', 0, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'product,category', 'mall,merchant,profile'),
  (28, '27', '商品', '/mall/product', 'icon-dropbox', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'product', 'mall'),
  (29, '27', '商品类别设置', '/category/list', 'icon-info22', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'category', 'merchant,profile'),
  (30, NULL, '代理管理', '', 'icon-tree6', 0, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'summary,member,tree', 'agent'),
  (31, NULL, '佣金管理', '', 'icon-price-tag', 0, 0, 4, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'commission', 'agent'),
  (32, NULL, '审核管理', '', 'icon-checkmark4', 0, 0, 5, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'apply,agent,auditRule', 'agent'),
  (33, NULL, '订单管理', '', 'icon-clipboard4', 0, 0, 6, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'order,auditRule', 'mall'),
  (34, NULL, '二维码申请', '', 'icon-qrcode', 0, 0, 7, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'generalQrcode,generalDirectorQrcode,generalSecondQrcode,generalFirstQrcode', 'agent'),
  (35, NULL, '防伪管理', '', 'icon-cog4', 0, 0, 8, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', '', ''),
  (36, NULL, '数据统计', '', 'icon-stats-growth', 0, 0, 9, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'trade,flow,guest', 'datacenter'),
  (37, NULL, '系统设置', '', 'icon-cog4', 0, 0, 10, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'role,org,module,homeItem,contact,version,function', 'org_list,module_manage,org_edit,merchant,mall,function_manage'),
  (38, NULL, '代理设置', '', 'icon-cog7', 0, 0, 11, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'roleAgent,rolePrice,rulePrice,ruleProduct', 'mall,agent'),
  (39, NULL, '微信设置', '', 'icon-bubbles4', 0, 0, 12, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'summary,config,templateMessage,autoReply,customizedMenus', 'wechat'),
  (40, '30', '代理概况', '/agent/summary', 'icon-statistics', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'summary', 'agent'),
  (41, '30', '代理会员', '/agent/member', 'icon-insert-template', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'member', 'agent'),
  (42, '30', '树状图', '/agent/tree', 'icon-insert-template', 1, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'tree', 'agent'),
  (43, '31', '代理佣金', '/agent/commission', 'icon-insert-template', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'commission', 'agent'),
  (44, '32', '代理审核', '/agent/apply', 'icon-check', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'apply', 'agent'),
  (45, '32', '代理审核规则', '/agent/auditRule', 'icon-dropbox', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'auditRule', 'agent'),
  (46, '33', '订单', '/mall/order', 'icon-stack', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'order', 'mall'),
  (47, '33', '订单审核规则', '/auditRule/list', 'icon-dropbox', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'auditRule', 'mall'),
  (48, '34', '董事二维码', '/agent/generalDirectorQrcode', 'icon-insert-template', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'generalDirectorQrcode', 'agent'),
  (49, '34', '总代二维码', '/agent/generalQrcode', 'icon-insert-template', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'generalQrcode', 'agent'),
  (50, '34', '二星二维码', '/agent/generalSecondQrcode', 'icon-insert-template', 1, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'generalSecondQrcode', 'agent'),
  (51, '34', '一星二维码', '/agent/generalFirstQrcode', 'icon-insert-template', 1, 0, 4, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'generalFirstQrcode', 'agent'),
  (52, '35', '防伪码查询', '~/antifake/query', 'icon-users4', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', '', ''),
  (53, '36', '交易概况', '/datacenter/trade', 'icon-statistics', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'trade', 'datacenter'),
  (54, '36', '流量概况', '/datacenter/flow', 'icon-user', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'flow', 'datacenter'),
  (55, '36', '访客分析', '/datacenter/guest', 'icon-insert-template', 1, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'guest', 'datacenter'),
  (56, '37', '管理员角色', '/merchant/role', ' icon-users2', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'role', 'merchant,profile'),
  (57, '37', '首页模块设置', '/homeItem/list', 'icon-dropbox', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'homeItem', 'mall'),
  (58, '37', '部门管理', '/org/list', 'icon-dropbox', 1, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'org', 'org_list'),
  (59, '37', '部门人员管理', '/org/edit', 'icon-users4', 1, 0, 4, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'org', 'org_edit'),
  (60, '37', '配置菜单', '/module/manage', 'icon-person', 1, 0, 5, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'module', 'module_manage'),
  (61, '38', '新增代理', '~/userAgent/apply4Admin', 'icon-dropbox', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', '', ''),
  (62, '38', '代理角色设置', '/role/list', 'icon-dropbox', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'roleAgent', 'mall'),
  (63, '38', '角色价格设置', '/rolePrice/list', 'icon-dropbox', 1, 0, 3, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'rolePrice', 'mall'),
  (64, '38', '佣金规则设置', '/rule/list', 'icon-insert-template', 1, 0, 4, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'rulePrice', 'agent'),
  (65, '38', '商品佣金设置', '/ruleProduct/list', 'icon-insert-template', 1, 0, 5, '2017-04-27 12:35:07', 0,
       '2017-04-27 12:35:07', 'ruleProduct', 'agent'),
  (66, '39', '微信概况', '/wechat/summary', 'icon-statistics', 1, 0, 1, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'summary', 'wechat'),
  (67, '39', '公众号设置', '/wechat/config', 'icon-user', 1, 0, 2, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07', 'config',
   'wechat'), (68, '39', '消息模板设置', '/wechat/templateMessage', 'icon-insert-template', 1, 0, 3, '2017-04-27 12:35:07', 0,
                   '2017-04-27 12:35:07', 'templateMessage', 'wechat'),
  (69, '39', '自动回复', '/wechat/autoReply', 'icon-reply', 1, 0, 4, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'autoReply', 'wechat'),
  (70, '39', '自定义菜单', '/wechat/customizedMenus', 'icon-menu6', 1, 0, 5, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'customizedMenus', 'wechat'),
  (71, '37', '配置按钮', '/function/manage', 'icon-insert-template', 1, 0, 6, '2017-04-27 12:35:07', 0,
       '2017-04-27 12:35:07', 'function', 'function_manage'),
  (74, '37', 'app更新设置', '/version/list', 'icon-dropbox', 1, 0, 7, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07',
   'version', 'mall');

-- 菜单角色关联数据
INSERT INTO `xquark_merchant_role_module` VALUES (308, 7, 24, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (309, 7, 25, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (310, 5, 24, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (311, 5, 25, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (312, 5, 26, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (313, 5, 43, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (314, 5, 44, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (315, 5, 45, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (316, 5, 46, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (317, 5, 47, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (318, 5, 48, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (319, 5, 49, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (320, 5, 50, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (321, 5, 51, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (322, 5, 52, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (323, 5, 53, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (324, 5, 54, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (325, 5, 55, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (326, 5, 56, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (327, 5, 57, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (328, 5, 58, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (329, 5, 59, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (330, 5, 60, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (331, 5, 61, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (332, 5, 62, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (333, 5, 63, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (334, 5, 64, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (335, 5, 65, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (336, 5, 66, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (337, 5, 67, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (338, 5, 68, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (339, 5, 69, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (340, 5, 70, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (341, 5, 71, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (342, 5, 40, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (343, 5, 28, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (344, 5, 29, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (345, 5, 41, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (346, 5, 42, '2017-04-27 12:35:07', 0, '2017-04-27 12:35:07'),
  (347, 5, 74, '2017-04-27 12:35:08', 0, '2017-04-27 12:35:08'),
  (358, 1, 40, '2017-04-28 10:34:57', 0, '2017-04-28 10:34:57'),
  (359, 1, 41, '2017-04-28 10:34:57', 0, '2017-04-28 10:34:57'),
  (360, 1, 42, '2017-04-28 10:34:57', 0, '2017-04-28 10:34:57'),
  (361, 1, 44, '2017-04-28 10:34:57', 0, '2017-04-28 10:34:57'),
  (362, 1, 45, '2017-04-28 10:34:57', 0, '2017-04-28 10:34:57'),
  (370, 4, 43, '2017-04-28 10:40:47', 0, '2017-04-28 10:40:47'),
  (398, 1, 48, '2017-04-28 11:05:56', 0, '2017-04-28 11:05:56'),
  (399, 1, 49, '2017-04-28 11:05:56', 0, '2017-04-28 11:05:56'),
  (400, 1, 50, '2017-04-28 11:05:56', 0, '2017-04-28 11:05:56'),
  (401, 1, 51, '2017-04-28 11:05:56', 0, '2017-04-28 11:05:56'),
  (402, 2, 46, '2017-04-28 11:07:59', 0, '2017-04-28 11:07:59'),
  (404, 3, 40, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (405, 3, 28, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (406, 3, 29, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (407, 3, 41, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (408, 3, 42, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (409, 3, 43, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (410, 3, 44, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (411, 3, 45, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (414, 3, 48, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (415, 3, 49, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (416, 3, 50, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (417, 3, 51, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (418, 3, 53, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (419, 3, 54, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (420, 3, 55, '2017-04-28 11:11:15', 0, '2017-04-28 11:11:15'),
  (421, 3, 46, '2017-04-28 11:11:56', 0, '2017-04-28 11:11:56'),
  (422, 3, 47, '2017-04-28 11:11:56', 0, '2017-04-28 11:11:56'),
  (426, 6, 40, '2017-04-28 11:14:19', 0, '2017-04-28 11:14:19'),
  (427, 6, 41, '2017-04-28 11:14:19', 0, '2017-04-28 11:14:19');

-- 初始化按钮数据
INSERT INTO `xquark_function` VALUES (1, 71, '添加按钮', 'add_button', '2017-04-24 09:49:14', 0, '2017-04-24 09:49:14'),
  (2, 71, '可见角色', 'set_visible', '2017-04-24 09:50:10', 0, '2017-04-24 09:50:10'),
  (3, 71, '可用角色', 'set_enable', '2017-04-24 10:04:00', 0, '2017-04-24 10:04:00'),
  (5, 28, '下架', 'off_shelves', '2017-04-24 10:32:45', 0, '2017-04-24 10:32:45'),
  (6, 28, '编辑', 'edit_product', '2017-04-24 10:33:35', 0, '2017-04-24 10:33:35'),
  (7, 28, '删除', 'delete_product', '2017-04-24 10:33:51', 0, '2017-04-24 10:33:51'),
  (8, 28, '团购', 'group_buy', '2017-04-24 10:34:36', 0, '2017-04-24 10:34:36'),
  (9, 45, '发布新审核规则', 'publish_new_rules', '2017-04-24 10:36:20', 0, '2017-04-24 10:36:20'),
  (10, 45, '编辑', 'edit_audit', '2017-04-24 10:36:44', 0, '2017-04-24 10:36:44'),
  (11, 45, '删除', 'delete_audit', '2017-04-24 10:36:55', 0, '2017-04-24 10:36:55'),
  (12, 47, '发布新审核规则', 'publish_new_rules_order', '2017-04-24 10:37:48', 0, '2017-04-24 10:37:48'),
  (13, 47, '编辑', 'edit_audit_order', '2017-04-24 10:38:17', 0, '2017-04-24 10:38:17'),
  (14, 47, '删除', 'delete_audit_order', '2017-04-24 10:38:26', 0, '2017-04-24 10:38:26'),
  (15, 56, '发布新角色', 'add_new_role', '2017-04-24 10:39:12', 0, '2017-04-24 10:39:12'),
  (16, 56, '编辑菜单', 'edit_modules', '2017-04-24 10:39:35', 0, '2017-04-24 10:39:35'),
  (17, 56, '编辑', 'edit_role', '2017-04-24 10:39:50', 0, '2017-04-24 10:39:50'),
  (18, 56, '删除', 'delete_role', '2017-04-24 10:40:03', 0, '2017-04-24 10:40:03'),
  (19, 57, '新增', 'add_item', '2017-04-24 10:40:46', 0, '2017-04-24 10:40:46'),
  (20, 57, '编辑', 'edit_item', '2017-04-24 10:40:59', 0, '2017-04-24 10:40:59'),
  (21, 57, '删除', 'delete_item', '2017-04-24 10:41:07', 0, '2017-04-24 10:41:07'),
  (22, 58, '添加部门', 'add_org', '2017-04-24 10:41:29', 0, '2017-04-24 10:41:29'),
  (23, 58, '编辑', 'edit_org', '2017-04-24 10:41:38', 0, '2017-04-24 10:41:38'),
  (24, 58, '删除', 'delete_org', '2017-04-24 10:41:49', 0, '2017-04-24 10:41:49'),
  (25, 59, '新增管理员', 'add_merchant', '2017-04-24 10:42:14', 0, '2017-04-24 10:42:14'),
  (26, 59, '编辑', 'edit_merchant', '2017-04-24 10:42:35', 0, '2017-04-24 10:42:35'),
  (27, 59, '部门', 'edit_merchant_org', '2017-04-24 10:42:53', 0, '2017-04-24 10:42:53'),
  (28, 59, '角色', 'edit_merchant_role', '2017-04-24 10:43:06', 0, '2017-04-24 10:43:06'),
  (29, 59, '删除', 'delete_merchant', '2017-04-24 10:43:23', 0, '2017-04-24 10:43:23'),
  (30, 60, '添加菜单', 'add_module', '2017-04-24 10:43:50', 0, '2017-04-24 10:43:50'),
  (31, 60, '刷新缓存', 'refresh_module_cache', '2017-04-24 10:44:04', 0, '2017-04-24 10:44:04'),
  (32, 60, '删除当前菜单', 'delete_current_module', '2017-04-24 10:44:22', 0, '2017-04-24 10:44:22'),
  (33, 60, '编辑', 'edit_module', '2017-04-24 10:44:44', 0, '2017-04-24 10:44:44'),
  (34, 60, '删除', 'delete_module', '2017-04-24 10:44:51', 0, '2017-04-24 10:44:51'),
  (35, 71, '刷新缓存', 'refresh_button_cache', '2017-04-24 10:45:31', 0, '2017-04-24 10:45:31'),
  (36, 71, '删除', 'delete_button', '2017-04-24 10:47:24', 0, '2017-04-24 10:47:24'),
  (37, 74, '发布新app更新规则', 'publish_new_app_rule', '2017-04-24 10:48:03', 0, '2017-04-24 10:48:03'),
  (38, 74, '编辑', 'edit_app_rule', '2017-04-24 10:48:45', 0, '2017-04-24 10:48:45'),
  (39, 74, '删除', 'delete_app_rule', '2017-04-24 10:48:55', 0, '2017-04-24 10:48:55'),
  (40, 62, '发布新角色', 'publish_new_agent_role', '2017-04-24 10:50:23', 0, '2017-04-24 10:50:23'),
  (41, 62, '编辑', 'edit_agent_role', '2017-04-24 10:50:38', 0, '2017-04-24 10:50:38'),
  (42, 62, '删除', 'delete_agent_role', '2017-04-24 10:50:47', 0, '2017-04-24 10:50:47'),
  (43, 63, '发布新角色价格', 'publish_new_agent_price', '2017-04-24 10:51:22', 0, '2017-04-24 10:51:22'),
  (44, 63, '编辑', 'edit_agent_price', '2017-04-24 10:51:39', 0, '2017-04-24 10:51:39'),
  (45, 63, '删除', 'delete_agent_price', '2017-04-24 10:51:50', 0, '2017-04-24 10:51:50'),
  (46, 64, '发布新规则', 'publish_new_price_rule', '2017-04-24 10:53:09', 0, '2017-04-24 10:53:09'),
  (47, 64, '编辑', 'edit_price_rule', '2017-04-24 10:53:51', 0, '2017-04-24 10:53:51'),
  (48, 64, '删除', 'delete_price_rule', '2017-04-24 10:54:09', 0, '2017-04-24 10:54:09'),
  (50, 41, '冻结', 'freeze_agent', '2017-04-24 10:58:20', 0, '2017-04-24 10:58:20'),
  (51, 41, '启用', 'enable_agent', '2017-04-24 10:58:31', 0, '2017-04-24 10:58:31'),
  (52, 41, '改变上级', 'edit_agent_superior', '2017-04-24 10:59:30', 0, '2017-04-24 10:59:30'),
  (54, 42, '清空', 'empty_agent_tree', '2017-04-24 11:01:43', 0, '2017-04-24 11:01:43'),
  (56, 44, '删除申请', 'delete_agent_apply', '2017-04-24 11:03:21', 0, '2017-04-24 11:03:21'),
  (57, 44, '审核通过', 'pass_agent', '2017-04-24 11:03:47', 0, '2017-04-24 11:03:47'),
  (58, 28, '发布新商品', 'publish_new_goods', '2017-04-24 11:09:14', 0, '2017-04-24 11:09:14'),
  (60, 28, '上架', 'up_shelves', '2017-04-24 11:11:48', 0, '2017-04-24 11:11:48'),
  (61, 41, '升级', 'rise_agent', '2017-04-24 11:14:15', 0, '2017-04-24 11:14:15'),
  (62, 44, '冻结账户', 'account_freeze', '2017-04-24 11:16:30', 0, '2017-04-24 11:16:30'),
  (64, 44, '解冻账户', 'account_unfreeze', '2017-04-24 11:17:11', 0, '2017-04-24 11:17:11'),
  (65, 46, '审核通过', 'pass_order', '2017-04-24 11:20:58', 0, '2017-04-24 11:20:58'),
  (66, 46, '修改数量', 'edit_amount', '2017-04-24 11:21:20', 0, '2017-04-24 11:21:20'),
  (67, 46, '取消', 'cancel_order', '2017-04-24 11:21:58', 0, '2017-04-24 11:21:58'),
  (68, 46, '删除', 'delete_order', '2017-04-24 11:22:27', 0, '2017-04-24 11:22:27'),
  (69, 65, '发布新商品佣金设置', 'publish_new_product_price_rule', '2017-04-24 12:26:47', 0, '2017-04-24 12:26:47'),
  (70, 65, '编辑', 'edit_product_price_rule', '2017-04-24 12:27:07', 0, '2017-04-24 12:27:07'),
  (71, 65, '删除', 'delete_product_price_rule', '2017-04-24 12:27:17', 0, '2017-04-24 12:27:17'),
  (72, 43, '批量导出', 'role_price_export', '2017-04-25 15:03:46', 0, '2017-04-25 15:03:46'),
  (73, 41, '批量导出', 'export_agent', '2017-04-25 15:05:21', 0, '2017-04-25 15:05:21'),
  (74, 46, '批量导出', 'export_order', '2017-04-25 15:08:57', 0, '2017-04-25 15:08:57'),
  (75, 46, '发货', 'delivery_order', '2017-04-25 15:22:50', 0, '2017-04-25 15:22:50'),
  (76, 44, '查看详情', 'account_detail', '2017-04-25 15:39:32', 0, '2017-04-25 15:39:32');