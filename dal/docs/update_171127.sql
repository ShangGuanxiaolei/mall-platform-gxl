CREATE TABLE `xquark_sign_in_rule` (
  `id`               BIGINT      NOT NULL AUTO_INCREMENT,
  `init`             BIGINT      NOT NULL
  COMMENT '签到积分初始值',
  `operation`        VARCHAR(20) NOT NULL
  COMMENT '签到计算方式',
  `operation_number` INTEGER     NOT NULL
  COMMENT '签到运算数',
  `max`              BIGINT               DEFAULT NULL
  COMMENT '签到获得的最大积分',
  `created_at`       DATETIME             DEFAULT NULL,
  `updated_at`       DATETIME             DEFAULT NULL,
  `archive`          TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '签到规则表';
