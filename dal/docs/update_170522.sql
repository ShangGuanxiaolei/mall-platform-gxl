-- user表去掉phone唯一索引
DROP INDEX unique_phone ON xquark_user;

-- 系统设置菜单下增加代理白名单
UPDATE xquark_module
SET page = 'org_list,module_manage,org_edit,merchant,mall,function_manage,white_list'
WHERE name = '系统设置';
UPDATE xquark_module
SET module = 'role,org,module,homeItem,contact,version,function,agent'
WHERE name = '系统设置';

-- 增加代理白名单页面
INSERT INTO `xquark_module` VALUES
  (80, '37', '代理白名单', '/agent/whiteList', 'icon-insert-template', 1, 0, 8, now(), 0, now(), 'agent', 'white_list ');

-- 增加代理白名单页面按钮
INSERT INTO `xquark_function` VALUES (
  77,80,'添加白名单','add_white_list','2017-05-24 11:46:10',0,'2017-05-24 11:46:10'),
  (78,80,'编辑','edit_white_list','2017-05-24 11:46:47',0,'2017-05-24 11:46:47'),
  (79,80,'禁用','change_white_list','2017-05-24 11:47:16',0,'2017-05-24 11:47:16'),
  (80,80,'删除','delete_white_list','2017-05-24 11:47:34',0,'2017-05-24 11:47:34'),
  (81,80,'查看详情','view_agent_detail','2017-05-24 11:48:00',0,'2017-05-24 11:48:00');

-- 增加批量发放佣金按钮
INSERT INTO xquark_function (id, module_id, name, html_id, created_at, archive, updated_at) VALUES (82, 43, '批量发放佣金', 'role_offered', '2017-05-24 11:54:35', 0, '2017-05-24 11:54:35');
