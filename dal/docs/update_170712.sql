ALTER TABLE xquark_health_test_result
  MODIFY COLUMN start DOUBLE NOT NULL;
ALTER TABLE xquark_health_test_result
  MODIFY COLUMN end DOUBLE NOT NULL;

DROP TABLE `xquark_health_test_result_product`;
CREATE TABLE `xquark_health_test_result_product` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `result_id`   BIGINT     NOT NULL
  COMMENT '结果id',
  `product_id`  BIGINT     NOT NULL
  COMMENT '产品id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
);
