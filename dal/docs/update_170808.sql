ALTER TABLE xquark_promotion_bargain_detail
  ADD COLUMN `status` VARCHAR(50) NOT NULL DEFAULT ''
COMMENT '状态';

DROP TABLE IF EXISTS `xquark_promotion_bargain_order`;
CREATE TABLE `xquark_promotion_bargain_order` (
  `id`                BIGINT     NOT NULL AUTO_INCREMENT,
  `bargain_detail_id` BIGINT     NOT NULL,
  `order_id`          BIGINT     NOT NULL,
  `created_at`        DATETIME            DEFAULT NULL,
  `updated_at`        DATETIME            DEFAULT NULL,
  `archive`           TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '砍价-订单关联表';

ALTER TABLE xquark_yundou_operation_type
  MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT;

DROP TABLE IF EXISTS `xquark_yundou_order`;
CREATE TABLE `xquark_yundou_order` (
  `id`                BIGINT     NOT NULL AUTO_INCREMENT,
  `yundou_product_id` BIGINT     NOT NULL
  COMMENT '积分商品id',
  `order_id`          BIGINT     NOT NULL,
  `created_at`        DATETIME            DEFAULT NULL,
  `updated_at`        DATETIME            DEFAULT NULL,
  `archive`           TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '积分-订单关联表';
