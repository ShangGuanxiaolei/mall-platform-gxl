/*
  元宵节活动所需数据库脚本
 */

INSERT INTO hvmall.promotion_list (id, state, trigger_start_time, trigger_end_time, created_at, updated_at, archive)
VALUES (2, 1, '2019-02-19 00:00:00', '2019-02-19 23:59:59', NOW(), NOW(), 0);

INSERT INTO hvmall.promotion_packet_rain (id,
                                          promotion_list_id,
                                          promotion_date,
                                          point_total,
                                          normal_limit,
                                          vip_limit,
                                          created_at,
                                          updated_at,
                                          archive,
                                          morning,
                                          afternoon,
                                          evening,
                                          time)
VALUES (13, 2, '2019-02-19 00:00:00', 1000000, 1000, 750, NOW(), NOW(), 0, '11:00:00', '16:00:00', '21:00:00', 1);