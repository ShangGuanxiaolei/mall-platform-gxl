alter table `xquark_sku` add column partner_product_id bigint(20) default null;
alter table `xquark_product` add column partner_product_id bigint(20) default null;

alter table `xquark_activity` add column `tag_type` int default 1;
alter table `xquark_activity` add column `apply_desc` varchar(2048);

alter table `xquark_campaign_product` add column `product_brand` varchar(1024);
alter table `xquark_campaign_product` add column `sort` int default 0;
alter table `xquark_campaign_product` add column `image_pc` varchar(512);
alter table `xquark_campaign_product` add column `image_app` varchar(512);
alter table `xquark_activity_ticket` add column `audit_reason` varchar(512);
alter table `xquark_activity_ticket` add column `auditor` varchar(64) default null;