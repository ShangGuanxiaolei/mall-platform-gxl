CREATE TABLE `xquark_domain` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '域名称',
  `memo` varchar(512) DEFAULT NULL COMMENT '备注',
  `code` varchar(40) NOT NULL COMMENT 'partner代码',
  `admin_user_id` bigint(20) NOT NULL COMMENT '对应user表中的userId',
  `archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否逻辑删除',
  `root` bigint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_domain_code` (`code`) USING BTREE
) ENGINE=InnoDB;

insert into xquark_domain(name, code, admin_user_id, root, created_at, updated_at)
values('快店', 'kkkd', -101, 1, now(), now());
insert into xquark_domain(name, code, admin_user_id, root, created_at, updated_at)
values('想去', 'xiangqu', 16630961, 0, now(), now());
insert into xquark_domain(name, code, admin_user_id, root, created_at, updated_at)
values('奶牛家', 'nnj', 17435037, 0, now(), now());

CREATE TABLE `xquark_distribution_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `master_id` bigint(20) NOT NULL COMMENT '域id',
  `promoter` bigint(20) DEFAULT NULL COMMENT '分销者 userId',
  `cps_rate` decimal(3,2) NOT NULL COMMENT '分销比例',
  `cps_max_age` int(11) DEFAULT NULL COMMENT '有效期，妙为单位',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;


ALTER TABLE `xquark_order` ADD COLUMN `partner` varchar(40) NULL COMMENT '合作方，同user表里的partner，非枚举' AFTER `partner_type`;

ALTER TABLE `xquark_commission`
ADD COLUMN `order_id`  bigint(20) NULL AFTER `id`,
ADD COLUMN `type` varchar(255) NULL COMMENT '佣金类型 CPS/PLATFORM' AFTER `status`;

ALTER TABLE `xquark_domain`
ADD INDEX `idx_domain_code` (`code`) ;

ALTER TABLE `xquark_order`
ADD COLUMN `tu_id`  bigint(20) NULL COMMENT '三方分佣ID' AFTER `union_id`;

update xquark_order set partner = 'xiangqu' where partner_type = 'XIANGQU';
update xquark_order set partner = 'kkkd' where partner_type = 'KKKD';

--初始化语句 
update xquark_commission a inner join xquark_order_item b on a.order_item_id = b.id
set a.order_id = b.order_id where a.order_id is null;