ALTER TABLE hgw.xquark_user
  ADD COLUMN `yundou` BIGINT NOT NULL DEFAULT 0
COMMENT '云豆数量';

DROP TABLE IF EXISTS `xquark_yundou_operation_type`;
CREATE TABLE `xquark_yundou_operation_type` (
  `id`         BIGINT       NOT NULL,
  `name`       VARCHAR(255) NOT NULL
  COMMENT '操作类型名称',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆操作类型表';

DROP TABLE IF EXISTS `xquark_yundou_operation`;
CREATE TABLE `xquark_yundou_operation` (
  `id`          BIGINT       NOT NULL AUTO_INCREMENT,
  `type_id`     BIGINT       NOT NULL
  COMMENT '操作类型id',
  `code`        INTEGER      NOT NULL
  COMMENT '操作代码',
  `name`        VARCHAR(255) NOT NULL
  COMMENT '操作名称',
  `description` VARCHAR(255)          DEFAULT NULL
  COMMENT '操作描述',
  `is_enable`   TINYINT      NOT NULL DEFAULT TRUE
  COMMENT '操作是否禁用',
  `created_at`  DATETIME              DEFAULT NULL,
  `updated_at`  DATETIME              DEFAULT NULL,
  `archive`     TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆操作表';

DROP TABLE IF EXISTS `xquark_yundou_rule`;
CREATE TABLE `xquark_yundou_rule` (
  `id`           BIGINT       NOT NULL AUTO_INCREMENT,
  `operation_id` BIGINT       NOT NULL
  COMMENT '操作id',
  `code`         INTEGER      NOT NULL
  COMMENT '规则代码',
  `name`         VARCHAR(255) NOT NULL
  COMMENT '规则名称',
  `description`  VARCHAR(255)          DEFAULT NULL
  COMMENT '规则描述',
  `is_enable`    TINYINT      NOT NULL DEFAULT TRUE
  COMMENT '规则是否禁用',
  `direction`    TINYINT               DEFAULT NULL
  COMMENT '规则对云豆的加减方向(0减1加)',
  `created_at`   DATETIME              DEFAULT NULL,
  `updated_at`   DATETIME              DEFAULT NULL,
  `archive`      TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆规则表';

DROP TABLE IF EXISTS `xquark_yundou_condition`;
CREATE TABLE `xquark_yundou_condition` (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `ruleId`     BIGINT       NOT NULL
  COMMENT '规则id',
  `code`       INTEGER      NOT NULL
  COMMENT '条件代码',
  `name`       VARCHAR(255) NOT NULL
  COMMENT '条件名称',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆条件表';

DROP TABLE IF EXISTS `xquark_yundou_operation_rule`;

DROP TABLE IF EXISTS `xquark_yundou_rule_detail`;
CREATE TABLE `xquark_yundou_rule_detail` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `role_id`    BIGINT     NOT NULL
  COMMENT '角色id',
  `rule_id`    BIGINT     NOT NULL
  COMMENT '规则id',
  `amount`     BIGINT     NOT NULL
  COMMENT '云豆数量',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆角色-规则关联表';

DROP TABLE IF EXISTS `xquark_operation_detail`;
CREATE TABLE `xquark_operation_detail` (
  `id`         BIGINT  NOT NULL AUTO_INCREMENT,
  `user_id`    BIGINT  NOT NULL
  COMMENT '用户id',
  `amount`     BIGINT  NOT NULL
  COMMENT '操作总云豆数量',
  `rule_id`    BIGINT  NOT NULL
  COMMENT '规则id',
  `biz_id`     BIGINT  NOT NULL
  COMMENT '业务id',
  `created_at` DATETIME         DEFAULT NULL,
  `updated_at` DATETIME         DEFAULT NULL,
  `archive`    TINYINT NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆操作明细表';

DROP TABLE IF EXISTS `xquark_achieve_goal`;
CREATE TABLE `xquark_achieve_goal` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `userId`     BIGINT     NOT NULL
  COMMENT '用户id',
  `range`      TINYINT(4) NOT NULL
  COMMENT '实效范围 0-月, 1-季, 2-年',
  `goal`       BIGINT     NOT NULL
  COMMENT '销售目标',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '云豆目标表';

DROP TABLE IF EXISTS `xquark_archive_rule`;
CREATE TABLE `xquark_archive_rule` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `userId`     BIGINT     NOT NULL
  COMMENT '用户id',
  `increase`   FLOAT      NOT NULL
  COMMENT '期望增幅',
  `range`      TINYINT(4) NOT NULL
  COMMENT '实效范围 0-月, 1-季, 2-年',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)

