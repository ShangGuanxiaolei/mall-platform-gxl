/*
  银联支付脚本
*/

DROP TABLE IF EXISTS `xquark_payment_switch_config`;
create table xquark_payment_switch_config
(
  id         bigint auto_increment
    primary key,
  profile    varchar(20) NULL,
  payment_id bigint      NOT NULL,
  status     tinyint(1)  NOT NULL,
  archive    tinyint(1)  NOT NULL,
  created_at datetime    NOT NULL,
  updated_at datetime    NOT NULL,
  code       varchar(20) NOT NULL,
  constraint profile
  unique (profile, payment_id)
)
  collate = utf8mb4_unicode_ci;

INSERT INTO hvmall.xquark_payment_switch_config  VALUES (NULL, 'prod', 5, 1, 0, NOW(), NOW(), 'EPS');

INSERT INTO hvmall.xquark_payment_config VALUES (NULL, 'EPS', 'EPS', '', '', 0, NOW(), NOW());

INSERT INTO hvmall.xquark_payment_merchant VALUES (NULL, 1, '', 'prod', 5, 'MD5', 'APP', '80128709207024', NULL, NULL, 'https://hwapi.handeson.com/pay', 'https://hwapi.handeson.com/pay/tenpayApp/notify', 'wxad2aff8b2919a3c2', '', '4641D1335D281E65CB925BD356296477', NOW(), NOW());
INSERT INTO hvmall.xquark_payment_merchant VALUES (NULL, 1, '', 'prod', 5, 'MD5', 'MINIPROGRAM', '80135370662564', NULL, NULL, 'https://hwapi.handeson.com', 'https://hwapi.handeson.com', 'wxa4a1fc9ae9458aa9', '', 'EB5C06ABECE709EDE2F19FD634808199', NOW(), NOW());
INSERT INTO hvmall.xquark_payment_merchant VALUES (NULL, 1, '', 'prod', 5, 'MD5', 'ALIPAY', '80128709207024', NULL, NULL, 'https://hwapi.handeson.com', 'https://hwapi.handeson.com/pay/alipay/notify', 'wxad2aff8b2919a3c2', '', '4641D1335D281E65CB925BD356296477', NOW(), NOW());