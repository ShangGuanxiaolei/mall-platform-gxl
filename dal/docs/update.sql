-- 2014-07-07 by ahlon
alter table `xquark_user` add UNIQUE KEY `loginname` (`name`);
alter table `xquark_user` add UNIQUE KEY `loginname` (`loginname`);

ALTER TABLE `xquark_product`
MODIFY COLUMN `price`  decimal(12,2) NOT NULL AFTER `status`;

ALTER TABLE `xquark_product`
MODIFY COLUMN `price`  decimal(12,2) NOT NULL AFTER `status`;

ALTER TABLE `xquark_sku`
MODIFY COLUMN `price`  decimal(12,2) NOT NULL AFTER `spec`;

ALTER TABLE `xquark_order_item`
MODIFY COLUMN `price`  decimal(12,2) NOT NULL AFTER `product_img`;