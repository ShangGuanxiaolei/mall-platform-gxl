set SQL_SAFE_UPDATES = 0;
delete from xquark_order;
delete from xquark_order_address;
delete from xquark_order_item;
delete from xquark_order_refund;
delete from xquark_shop_tree where descendant_shop_id != 6102910;
delete from xquark_shop where id != 6102910;
delete from xquark_shop_access;
delete from xquark_shop_access_log;
delete from xquark_user where id != 17481953;
delete from xquark_user_partner;
delete from xquark_user_twitter;
delete from xquark_user_team;
delete from xquark_user_signin_log;

delete from xquark_address;
delete from xquark_product_user;

ALTER TABLE xquark_user
  ADD INDEX (union_id, archive);
ALTER TABLE xquark_user
  ADD INDEX (phone, archive);

ALTER TABLE xquark_product_user
  ADD INDEX (user_id, product_id, archive);
ALTER TABLE xquark_product_user
  ADD INDEX (user_id, archive);
ALTER TABLE xquark_product_user
  ADD INDEX (product_id, archive);

ALTER TABLE xquark_address
  ADD INDEX (ext_id, archive);
ALTER TABLE xquark_address
  ADD INDEX (user_id, archive);

ALTER TABLE xquark_zone ADD INDEX (name, archive);
ALTER TABLE xquark_zone ADD INDEX (parent_id, archive);
