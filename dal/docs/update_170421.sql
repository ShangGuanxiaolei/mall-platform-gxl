-- 角色-按钮关联表
DROP TABLE IF EXISTS `xquark_function`;
CREATE TABLE `xquark_function` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `module_id` BIGINT NOT NULL COMMENT '菜单id',
  `name` varchar(255) NOT NULL COMMENT '按钮名称',
  `html_id` VARCHAR(255) NOT NULL COMMENT '按钮在html中的id',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='按钮表';

-- 菜单-按钮关联表
# DROP TABLE IF EXISTS `xquark_module_function`;
# CREATE TABLE `xquark_module_function` (
#   `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
#   `function_id` BIGINT NOT NULL COMMENT '按钮表id',
#   `module_id` bigint NOT NULL COMMENT '菜单id',
#   `created_at` DATETIME NULL,
#   `archive` TINYINT NULL COMMENT '是否删除',
#   `updated_at` DATETIME NULL,
#   PRIMARY KEY (`id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单-按钮关联表';

-- 角色-按钮关联表
DROP TABLE IF EXISTS `xquark_role_function`;
CREATE TABLE `xquark_role_function` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `function_id` BIGINT NOT NULL COMMENT '按钮表id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `is_show` TINYINT DEFAULT TRUE COMMENT '是否可见',
  `is_enable` TINYINT DEFAULT TRUE COMMENT '是否可用',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='按钮-角色关联表';



