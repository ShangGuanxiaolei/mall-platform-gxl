DROP TABLE xquark_promotion_apply;
CREATE TABLE xquark_promotion_apply (
  `id`           BIGINT      NOT NULL AUTO_INCREMENT,
  `promotion_product_id` BIGINT      NOT NULL
  COMMENT '活动商品id',
  `order_id`     BIGINT      NOT NULL
  COMMENT '关联订单',
  `type`         VARCHAR(20) NOT NULL
  COMMENT '活动类型',
  `status`       VARCHAR(20) NOT NULL
  COMMENT '审核状态',
  `created_at`   DATETIME             DEFAULT NULL,
  `updated_at`   DATETIME             DEFAULT NULL,
  `archive`      TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '需后台审核的活动申请';

