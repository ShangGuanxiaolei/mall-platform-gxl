alter table xquark_user
  add column `free_logistic_used` tinyint(4) not null default 0
comment '包邮资格, 默认老用户已使用';

-- 非必要
update xquark_user
set free_logistic_used = 0;

update xquark_user
set free_logistic_used = 1
-- 下单成功过的更新为老用户
where id in (select distinct buyer_id
             from xquark_order
             where archive = false
               and status in ('PAID', 'DELIVERY', 'SHIPPED', 'SUCCESS', 'COMMENT'))
--  有身份的人更新为老用户
and cpId in (
  select cpId from customercareerlevel
  where (hds_type is not null and hds_type != '') or vivilife_type = 'SP' or vivilife_type = 'DS'
);

alter table xquark_order
  add column `logistic_discount_type` varchar(25) comment '运费优惠类型' default null ;

update xquark_order set logistic_discount_type = 'FREE_POSTAGE' where logistic_discount > 0;
