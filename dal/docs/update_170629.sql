DROP TABLE IF EXISTS `xquark_product_card`;
CREATE TABLE `xquark_product_card` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `product_id` BIGINT     NOT NULL
  COMMENT '产品id',
  `card_id`    BIGINT     NOT NULL
  COMMENT '会员卡id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)