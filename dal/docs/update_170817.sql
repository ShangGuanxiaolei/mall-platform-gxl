ALTER TABLE xquark_health_test_result MODIFY COLUMN type VARCHAR(20) DEFAULT NULL COMMENT '统计类型-弃用';

DROP TABLE IF EXISTS `xquark_sign_in`;
CREATE TABLE `xquark_sign_in` (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `user_id`    BIGINT     NOT NULL
  COMMENT '用户id',
  `sign_count` BIGINT       NOT NULL DEFAULT 0
  COMMENT '连续签到数',
  `sign_sum`   BIGINT       NOT NULL DEFAULT 0
  COMMENT '签到总天数',
  `created_at` DATETIME            NOT NULL,
  `updated_at` DATETIME            NOT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '签到表';

# ALTER TABLE xquark_sign_in ADD INDEX index_created_at (created_at);

TRUNCATE xquark_yundou_operation_type;
INSERT INTO uplus.xquark_yundou_operation_type (name, created_at, updated_at, archive) VALUES ('社交类', '2017-08-09 16:31:07', '2017-08-09 16:31:10', 0);
INSERT INTO uplus.xquark_yundou_operation_type (name, created_at, updated_at, archive) VALUES ('交易类', '2017-08-09 16:31:24', '2017-08-09 16:31:25', 0);
INSERT INTO uplus.xquark_yundou_operation_type (name, created_at, updated_at, archive) VALUES ('活跃类', '2017-08-18 10:04:07', '2017-08-18 10:04:09', 0);

TRUNCATE xquark_yundou_operation;
INSERT INTO uplus.xquark_yundou_operation (type_id, code, name, description, is_enable, created_at, updated_at, archive) VALUES (1, 1001, '完善信息', '完善信息送积分', 1, '2017-08-09 16:32:11', '2017-08-09 16:32:11', 0);
INSERT INTO uplus.xquark_yundou_operation (type_id, code, name, description, is_enable, created_at, updated_at, archive) VALUES (2, 2001, '确认收货', '确认收获送积分', 1, '2017-08-09 16:33:15', '2017-08-09 16:34:15', 0);
INSERT INTO uplus.xquark_yundou_operation (type_id, code, name, description, is_enable, created_at, updated_at, archive) VALUES (2, 2002, '完成订单', '完成订单按订单金额送积分', 1, '2017-08-09 16:34:03', '2017-08-09 16:34:03', 0);
INSERT INTO uplus.xquark_yundou_operation (type_id, code, name, description, is_enable, created_at, updated_at, archive) VALUES (2, 2003, '订单退款', '订单退款返回相应积分', 1, '2017-08-10 18:48:04', '2017-08-10 18:48:04', 0);
INSERT INTO uplus.xquark_yundou_operation (type_id, code, name, description, is_enable, created_at, updated_at, archive) VALUES (3, 3001, '签到', '签到送积分', 1, '2017-08-18 10:04:47', '2017-08-18 10:04:47', 0);

TRUNCATE xquark_yundou_rule;
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (1, 10011, '完善手机送积分', '完善手机送20积分', 1, 1, '2017-08-09 16:56:14', '2017-08-17 19:12:58', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (2, 20011, '确认收获送积分', '确认收获送积分', 1, 1, '2017-08-09 17:00:45', '2017-08-09 17:00:45', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (3, 20021, '完成订单送积分', '完成订单默认送10积分', 1, 1, '2017-08-09 17:02:03', '2017-08-17 19:11:23', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (3, 20022, '订单金额达到100', '订单金额满100送20积分', 1, 1, '2017-08-09 18:49:43', '2017-08-09 18:49:43', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (3, 20023, '订单金额达到200', '订单金额满200送100积分', 1, 1, '2017-08-09 18:50:19', '2017-08-09 18:50:19', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (4, 20031, '订单退款', '云豆数量为下单时送的数量', 1, 1, '2017-08-10 18:49:38', '2017-08-17 19:12:52', 0);
INSERT INTO uplus.xquark_yundou_rule (operation_id, code, name, description, is_enable, direction, created_at, updated_at, archive) VALUES (5, 30011, '签到送积分', '根据天数累计，1-6天每天累计2积分，第7天加10积分，最大每天22积分', 1, 1, '2017-08-18 10:06:20', '2017-08-18 10:06:20', 0);
