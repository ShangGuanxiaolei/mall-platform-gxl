ALTER TABLE xquark_product ADD net_worth decimal(10,2) DEFAULT '0.00' NULL COMMENT '净值';
ALTER TABLE xquark_sku ADD net_worth decimal(10,2) DEFAULT '0.00' NOT NULL COMMENT '商品净值';
CREATE TABLE xquark_supplier
(
    id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    archive tinyint(1) DEFAULT '0' NOT NULL,
    updated_at datetime DEFAULT now() NOT NULL,
    created_at datetime DEFAULT now() NOT NULL,
    name varchar(32) DEFAULT '' NOT NULL
);


CREATE TABLE xquark_supplier_warehouse
(
    id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    archive tinyint(1) DEFAULT '0' NOT NULL,
    updated_at datetime DEFAULT now() NOT NULL,
    created_at datetime DEFAULT now() NOT NULL,
    warehouse_id bigint(20)  NOT NULL ,
    supplier_id bigint(20)  NOT NULL
);

ALTER TABLE xquark_supplier ADD code varchar(64) NOT NULL COMMENT '供应商编码';
