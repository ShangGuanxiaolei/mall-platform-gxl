DROP TABLE IF EXISTS `xquark_sku_combine`;
CREATE TABLE `xquark_sku_combine` (
  `id`         bigint(20) not null auto_increment
  comment 'id',
  `product_id` bigint(20) not null
  comment '套餐商品的productId',
  `sku_id`     bigint(20) not null
  comment '套餐商品的skuId',
  `valid_from` datetime            default null,
  `valid_to`   datetime            default null,
  `created_at` datetime            default null,
  `updated_at` datetime            default null,
  `archive`    tinyint(4) not null
  comment '逻辑删除字段',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='组合套装表';

DROP TABLE IF EXISTS `xquark_sku_combine_extra`;
CREATE TABLE `xquark_sku_combine_extra` (
  `id`               bigint(20) not null auto_increment
  comment 'id',
  `master_id`        bigint(20) not null
  comment '套装id',
  `slave_id`         bigint(20) not null
  comment '套餐商品对应的实际skuId',
  `slave_product_id` bigint(20) default null
  comment '套餐商品对应的商品id',
  `amount`           INTEGER    not null
  comment '套餐商品数量',
  `created_at`       datetime            default null,
  `updated_at`       datetime            default null,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='组合套装关系表';

select * from xquark_sku_combine;

select * from xquark_sku_combine_extra;