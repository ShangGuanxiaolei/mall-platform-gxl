alter table xquark_product add column promo_amt decimal(18, 2) not null default 0 comment '推广费';
alter table xquark_sku add column promo_amt decimal(18, 2) not null default 0 comment '推广费';
alter table xquark_order_item add column promo_amt decimal(18, 2) not null default 0 comment '推广费';

alter table xquark_product add column server_amt decimal(18, 2) not null default 0 comment '服务费';
alter table xquark_sku add column server_amt decimal(18, 2) not null default 0 comment '服务费';
alter table xquark_order_item add column server_amt decimal(18, 2) not null default 0 comment '服务费';
alter table order_detail add column server_amt decimal(18, 2) not null default 0 comment '服务费';
