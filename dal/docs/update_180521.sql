CREATE TABLE xquark_bill_personal (
  `id`  BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT '发票id',
  `order_id`  BIGINT(20)  NOT NULL COMMENT '订单id',
  `type`        varchar(32) NOT NULL COMMENT '发票类型',
  `receive_phone`   char(11)  COMMENT '接受手机号',
  `receive_email`   varchar(64)  COMMENT '接受邮箱',
  `name`   varchar(64)  COMMENT '姓名',
  `exchange_id`   char(20)  COMMENT '请求流水号',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '个人开票请求';


CREATE TABLE xquark_bill_company (
  `id`  BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT '发票id',
  `order_id`  BIGINT(20)  NOT NULL COMMENT '订单id',
  `product_detail` VARCHAR(128)  default  '' COMMENT '商品明细',
  `identify_number` VARCHAR(128) NOT NULL  COMMENT '纳税人识别号',
  `name`   varchar(64) NOT NULL COMMENT '公司名称',
  `receive_phone`   char(11)  COMMENT '接受手机号',
  `receive_mail`   varchar(64)  COMMENT '接受邮箱',
  `type`        varchar(32) NOT NULL COMMENT '发票类型',
  `exchange_id`   char(20)  COMMENT '请求流水号',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '公司开票请求';

CREATE TABLE xquark_bill_electronic_result (
  `id`  BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT '发票id',
  `order_id`  BIGINT(20)  NOT NULL COMMENT '订单id',
  `return_code`  BIGINT(20)  NOT NULL COMMENT '返回代码',
  `exchange_id`   char(20)  COMMENT '请求流水号',
  `bill_code` VARCHAR(128)   COMMENT '发票代码',
  `bill_number` VARCHAR(128)   COMMENT '发票号码',
  `date`   datetime  COMMENT '开票日期',
  `pdf_url`   varchar(256)  COMMENT 'pdf地址',
  `recevive_url`   varchar(128)  COMMENT '收票地址',
  `validete_code`   varchar(12)  COMMENT '验证码',
  `created_at` DATETIME            DEFAULT now(),
  `updated_at` DATETIME            DEFAULT now(),
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '电子发票开票结果';



CREATE TABLE xquark_bill_added_tax(
  `id`  BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT '发票id',
  `order_id`  BIGINT(20)  NOT NULL COMMENT '订单id',
  `identify_number` VARCHAR(128) NOT NULL  COMMENT '纳税人识别号',
  `receive_mail`   varchar(64)  COMMENT '接受邮箱',
  `name`   varchar(64) NOT NULL COMMENT '公司名称',
  `attorney_img` VARCHAR(256) NOT NULL  COMMENT '委托书图片',
  `company_phone` VARCHAR(32) NOT NULL  COMMENT '公司电话',
  `company_address` VARCHAR(128) NOT NULL  COMMENT '公司地址',
  `bank` VARCHAR(32) NOT NULL  COMMENT '开户银行',
  `bank_account` VARCHAR(32) NOT NULL  COMMENT '银行账号',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '增值税开票请求';
