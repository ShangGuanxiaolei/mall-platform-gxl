alter table xquark_promotion
  add column `closed` tinyint(4) not null default false;


create table `xquark_promotion_pre_order` (
  `id`             bigint         not null auto_increment,
  `promotion_id`   bigint         not null
  comment '活动id',
  `product_id`     bigint         not null
  comment '商品id',
  `pre_order_price` decimal(18, 2) not null
  comment '预购价',
  `discount`       decimal(18, 2) not null
  comment '活动价',
  `amount`         bigint         not null
  comment '活动库存',
  `sales`          bigint         not null
  comment '活动销量',
  `created_at`     datetime                default null,
  `updated_at`     datetime                default null,
  `archive`        tinyint(4)     not null default false,
  primary key (`id`)
)
  engine = innodb
  default charset = utf8
  comment = '预购活动表';

create table `xquark_promotion_flash_sale` (
  `id`             bigint         not null auto_increment,
  `promotion_id`   bigint         not null
  comment '活动id',
  `product_id`     bigint         not null
  comment '商品id',
  `discount`       decimal(18, 2) not null
  comment '活动价',
  `amount`         bigint         not null
  comment '活动库存',
  `sales`          bigint         not null
  comment '活动销量',
  `created_at`     datetime                default null,
  `updated_at`     datetime                default null,
  `archive`        tinyint(4)     not null default false,
  primary key (`id`)
)
  engine = innodb
  default charset = utf8
  comment = '抢购活动表';

alter table xquark_promotion_pre_order add column `free_points` bigint default null comment '达到多少积分免定金' after `sales`;