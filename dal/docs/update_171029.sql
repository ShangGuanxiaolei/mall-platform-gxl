ALTER TABLE xquark_promotion_full_cut_discount MODIFY COLUMN discount_type VARCHAR(30) DEFAULT NULL COMMENT '折扣方式';
ALTER TABLE xquark_promotion_full_pieces_discount MODIFY COLUMN discount_type VARCHAR(30) DEFAULT NULL COMMENT '折扣方式';
ALTER TABLE xquark_promotion_coupon MODIFY COLUMN apply_limit INT(8) NOT NULL COMMENT '优惠券最大发放人数';
