DROP TABLE IF EXISTS `xquark_point_info`;
CREATE TABLE `xquark_point_info` (
  `id`                      bigint(20)  NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `user_id`                 bigint(20)  NOT NULL
  COMMENT '用户id',
  `total_point`             bigint(20)  NOT NULL DEFAULT 0
  COMMENT '总积分',
  `total_point_hds`         bigint(20)  NOT NULL DEFAULT 0
  COMMENT 'hds总积分',
  `total_point_hwsc`        bigint(20)  NOT NULL DEFAULT 0
  COMMENT '汉薇商城总积分',
  `total_point_vivi_life`   bigint(20)  NOT NULL DEFAULT 0
  COMMENT 'viviLife总积分',
  `current_point`           bigint(20)  NOT NULL DEFAULT 0
  COMMENT '当前积分',
  `freezed_point`           bigint(20)  NOT NULL DEFAULT 0
  COMMENT '已冻结积分',
  `total_expense`           bigint(20)  NOT NULL DEFAULT 0
  COMMENT '总消费',
  `total_expense_hds`       bigint(20)  NOT NULL DEFAULT 0
  COMMENT 'hds总冻结积分',
  `total_expense_hwsc`      bigint(20)  NOT NULL DEFAULT 0
  COMMENT '汉薇商城总冻结积分',
  `total_expense_vivi_life` bigint(20)  NOT NULL DEFAULT 0
  COMMENT 'viviLife商城总冻结积分',
  `create_user_id`          bigint(20)  NOT NULL
  COMMENT '创建用户id',
  `modify_user_id`          bigint(20)  NOT NULL
  COMMENT '修改用户id',
  `status`                  varchar(20) NOT NULL
  COMMENT '积分状态',
  `created_at`              datetime             DEFAULT NULL,
  `updated_at`              datetime             DEFAULT NULL,
  `archive`                 tinyint(4)  NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='用户积分信息表';

DROP TABLE IF EXISTS `xquark_point_grade`;
CREATE TABLE `xquark_point_grade` (
  `id`                BIGINT(20)   NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `function_code`     VARCHAR(200) NOT NULL
  COMMENT '功能代码',
  `point_type`        VARCHAR(30)  NOT NULL
  COMMENT '积分类型 GRANT - 发放, CONSUME - 消费, ROLLBACK - 回退, FREEZE - 冻结',
  `priority`          INTEGER      NOT NULL
  COMMENT '优先级, 从1开始计数',
  `is_global`         TINYINT(4)   NOT NULL DEFAULT TRUE
  COMMENT '是否全局',
  `is_active_use`     TINYINT(4)   NOT NULL DEFAULT FALSE
  COMMENT '是否活动使用',
  `point`             BIGINT       NOT NULL
  COMMENT '积分',
  `is_use_for_mula`   TINYINT(4)   NOT NULL DEFAULT FALSE
  COMMENT '是否应用公式',
  `formula`           VARCHAR(255) NOT NULL DEFAULT ''
  COMMENT '公式',
  `description`       TEXT         NOT NULL
  COMMENT '描述',
  `day_upper_limit`   BIGINT       NOT NULL DEFAULT 0
  COMMENT '日上限',
  `month_upper_limit` BIGINT       NOT NULL DEFAULT 0
  COMMENT '月上限',
  `total_upper_limit` BIGINT       NOT NULL DEFAULT 0
  COMMENT '总上限',
  `create_user_id`    BIGINT(20)   NOT NULL
  COMMENT '创建用户id',
  `modify_user_id`    BIGINT(20)   NOT NULL
  COMMENT '修改用户id',
  `status`            VARCHAR(20)  NOT NULL
  COMMENT '状态 ENABLE, DISABLE ',
  `created_at`        DATETIME              DEFAULT NULL,
  `updated_at`        DATETIME              DEFAULT NULL,
  `archive`           TINYINT(4)   NOT NULL DEFAULT 0,
  INDEX `idx_function_code` (function_code),
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT ='用户积分规则表';

DROP TABLE IF EXISTS `xquark_point_record`;
CREATE TABLE `xquark_point_record` (
  `id`                    bigint(20)   NOT NULL AUTO_INCREMENT,
  `user_id`               bigint(20)   NOT NULL
  COMMENT '用户id',
  `point_grade_id`        bigint(20)   NOT NULL
  COMMENT '规则id',
  `function_code`         varchar(255) NOT NULL
  COMMENT '功能代码',
  `business_id`           varchar(32)  NOT NULL
  COMMENT '业务id',
  `current_point`         bigint(20)   NOT NULL
  COMMENT '本次积分',
  `current_freezed_point` bigint(20)   NOT NULL
  COMMENT '本次冻结积分',
  `platform`              varchar(20)  NOT NULL
  COMMENT '积分来源平台',
  `rollbacked`            tinyint(4)   NOT NULL DEFAULT 0
  COMMENT '是否已回退',
  `un_freeze_id`          bigint(20)            DEFAULT NULL
  COMMENT '解冻记录id',
  `rollback_id`           bigint(20)            DEFAULT NULL
  COMMENT '回退记录id',
  `created_at`            datetime              DEFAULT NULL,
  `updated_at`            datetime              DEFAULT NULL,
  `freezed_at`            datetime              DEFAULT NULL
  COMMENT '冻结起始时间',
  `freezed_to`            datetime              DEFAULT NULL
  COMMENT '冻结结束时间',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8
  COMMENT ='用户积分记录表';

# 用户表增加全局唯一id
ALTER TABLE xquark_user
  ADD COLUMN `guid` VARCHAR(200) NOT NULL DEFAULT ''
  AFTER `union_id`;
