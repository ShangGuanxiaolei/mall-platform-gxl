ALTER TABLE xquark_user
  ADD COLUMN `cpId` BIGINT(20) NOT NULL;
ALTER TABLE xquark_user
  ADD COLUMN `customer_number` VARCHAR(20) NOT NULL;

ALTER TABLE xquark_product
  DROP COLUMN `deduction_d_point`;
ALTER TABLE xquark_product
  ADD COLUMN `deduction_d_point` DECIMAL(10, 2) DEFAULT 0;
