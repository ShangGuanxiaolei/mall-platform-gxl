ALTER TABLE xquark_helper ADD COLUMN `content` TEXT NOT NULL COMMENT '问答内容' AFTER url;

CREATE TABLE `xquark_feedback_tags` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `tag_id`     BIGINT(20) NOT NULL
  COMMENT '标签id',
  `feedback_id` BIGINT(20) NOT NULL
  COMMENT '商品id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='反馈标签关联表';
