ALTER TABLE xquark_product
  ADD COLUMN `yundou_scale` TINYINT(1) DEFAULT 0;
COMMENT '积分兑换比例'
  AFTER attributes;

ALTER TABLE xquark_product
  ADD COLUMN `min_yundou_price` DECIMAL(18, 2) NOT NULL DEFAULT 0
COMMENT '最小积分抵扣价格'
  AFTER yundou_scale;

DROP TABLE xquark_user_notification;
CREATE TABLE xquark_user_notification (
  `id`         BIGINT     NOT NULL AUTO_INCREMENT,
  `to_user_id` BIGINT     NOT NULL
  COMMENT '发送用户id',
  `msg_id`     BIGINT     NOT NULL
  COMMENT '消息id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户消息表 - 缓存用户消息';
