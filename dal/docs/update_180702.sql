CREATE TABLE xquark_merchant_supplier
(
    id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    archive tinyint(1) DEFAULT '0' NOT NULL,
    updated_at datetime DEFAULT now() NOT NULL,
    created_at datetime DEFAULT now() NOT NULL,
    merchant_id bigint(20) not null,
    supplier_id bigint(20) not null
) comment '后台用户与供应商的关系';