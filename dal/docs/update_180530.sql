DROP TABLE IF EXISTS `xquark_sf_app_config`;
CREATE TABLE `xquark_sf_app_config` (
  `id`                      bigint(20)  NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `client_id`               varchar(50) NOT NULL
  COMMENT '顺丰clientId',
  `grant_type`              varchar(50) NOT NULL
  COMMENT '顺丰grantType',
  `profile`                 varchar(20) NOT NULL
  COMMENT '环境配置',
  `client_secret`           varchar(50) NOT NULL
  COMMENT '顺丰secret',
  `access_token`            varchar(200)         DEFAULT NULL
  COMMENT 'token',
  `access_token_expired_at` datetime             DEFAULT NULL
  COMMENT '下次过期时间',
  `created_at`              datetime             DEFAULT NULL,
  `updated_at`              datetime             DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  COMMENT ='sf配置表';
CREATE TABLE `xquark_combination` (
  `id`         bigint(20)  NOT NULL AUTO_INCREMENT
  COMMENT '组合表 id',
  `name`       varchar(50) NOT NULL
  COMMENT '组合名称',
  `status`     varchar(15) NOT NULL
  COMMENT '组合状态',
  `created_at` datetime    NOT NULL,
  `updated_at` datetime    NULL     DEFAULT NULL,
  `archive`    tinyint(1)  NOT NULL DEFAULT '0'
  COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT ='组合表';

CREATE TABLE `xquark_combination_item` (
  `id`             bigint(20)     NOT NULL AUTO_INCREMENT
  COMMENT '组合商品表 id',
  `combination_id` bigint(20)     NOT NULL
  COMMENT '组合表 id',
  `product_id`     bigint(20)     NOT NULL
  COMMENT '商品 id',
  `price`          decimal(12, 2) NOT NULL
  COMMENT '商品价格',
  `num`            int            NOT NULL
  COMMENT '商品数量',
  `created_at`     datetime       NOT NULL,
  `updated_at`     datetime       NULL     DEFAULT NULL,
  `archive`        tinyint(1)     NOT NULL DEFAULT '0'
  COMMENT '是否逻辑删除',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT ='组合明细表';
