ALTER TABLE `xquark_order`
ADD COLUMN `buyer_archive` tinyint(1) NULL DEFAULT 0 AFTER `remark_admin`;

ALTER TABLE `xquark_outpay`
ADD COLUMN `payment_merchant_id`  bigint(20) NULL COMMENT '支付商户ID' AFTER `trade_no`;

DROP TABLE IF EXISTS `xquark_payment`;

CREATE TABLE `xquark_payment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) NOT NULL,
  `domain_code` varchar(20) COLLATE DEFAULT NULL,
  `payment_id` bigint(20) NOT NULL,
  `device` varchar(20) NOT NULL COMMENT '设备(ios,android,pc,ipad,wap)',
  `icon` varchar(200) NOT NULL,
  `payment_order` int(4) NOT NULL,
  `description` varchar(40) NOT NULL,
  `description_ext` varchar(100) NOT NULL,
  `max_fee` decimal(10,0) DEFAULT NULL COMMENT '最大使用金额',
  `archive` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `xquark_payment` VALUES ('1', '2', 'xiangqu', '1', 'ANDROID', 'qn|ixp-other|FqD_T-7WZangJpzZoOQu4Xut8biG', '1', '推荐支付宝用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('2', '2', 'xiangqu', '2', 'ANDROID', 'qn|ixp-other|Fqh2JKmpQUQLj6y33XKeWVAu6lkA', '2', '推荐微信用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('3', '2', 'xiangqu', '3', 'ANDROID', 'qn|ixp-other|FmPLRN_uO4d-80YU1FwvY0tMp5CL', '3', '支持信用卡，储蓄卡支付', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('4', '2', 'xiangqu', '1', 'IOS', 'qn|ixp-other|FqD_T-7WZangJpzZoOQu4Xut8biG', '1', '推荐支付宝用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('5', '2', 'xiangqu', '2', 'IOS', 'qn|ixp-other|Fqh2JKmpQUQLj6y33XKeWVAu6lkA', '2', '推荐微信用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('6', '2', 'xiangqu', '3', 'IOS', 'qn|ixp-other|FmPLRN_uO4d-80YU1FwvY0tMp5CL', '3', '支持信用卡，储蓄卡支付', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('7', '2', 'xiangqu', '1', 'IPAD', 'qn|ixp-other|FqD_T-7WZangJpzZoOQu4Xut8biG', '1', '推荐支付宝用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('8', '2', 'xiangqu', '3', 'IPAD', 'qn|ixp-other|FmPLRN_uO4d-80YU1FwvY0tMp5CL', '3', '支持信用卡，储蓄卡支付', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('9', '2', 'xiangqu', '1', 'PC', 'qn|ixp-other|FqD_T-7WZangJpzZoOQu4Xut8biG', '1', '推荐支付宝用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('10', '2', 'xiangqu', '2', 'PC', 'qn|ixp-other|Fqh2JKmpQUQLj6y33XKeWVAu6lkA', '2', '推荐微信用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('11', '2', 'xiangqu', '3', 'PC', 'qn|ixp-other|FmPLRN_uO4d-80YU1FwvY0tMp5CL', '3', '支持信用卡，储蓄卡支付', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('12', '2', 'xiangqu', '1', 'WAP', 'qn|ixp-other|FqD_T-7WZangJpzZoOQu4Xut8biG', '1', '推荐支付宝用户使用', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');
INSERT INTO `xquark_payment` VALUES ('13', '2', 'xiangqu', '3', 'WAP', 'qn|ixp-other|FmPLRN_uO4d-80YU1FwvY0tMp5CL', '2', '支持信用卡，储蓄卡支付', '', null, '0', '2015-07-12 22:34:10', '2015-07-12 22:34:10');



DROP TABLE IF EXISTS `xquark_payment_config`;
CREATE TABLE `xquark_payment_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '支付宝,微信，银行卡,',
  `code` varchar(20) NOT NULL,
  `default_icon` varchar(200) NOT NULL COMMENT '默认icon，如果domain_payment没指定则使用该icon',
  `description` varchar(200) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


INSERT INTO `xquark_payment_config` VALUES ('1', '支付宝', 'ALIPAY', 'www.kkkd.com/_resources/images/pay/pay_12.jpg', '推荐支付宝用户使用', '0', '2015-07-12 22:31:48', '2015-07-12 22:31:48');
INSERT INTO `xquark_payment_config` VALUES ('2', '微信支付', 'WEIXIN', 'http://www.kkkd.com/resources/images/pay_06.jpg', '推荐微信用户使用', '0', '2015-07-12 22:31:48', '2015-07-12 22:31:48');
INSERT INTO `xquark_payment_config` VALUES ('3', '银行卡支付', 'UNION', 'http://www.kkkd.com/resources/images/pay_09.jpg', '支持信用卡，储蓄卡支付', '0', '2015-07-12 22:31:48', '2015-07-12 22:31:48');
INSERT INTO `xquark_payment_config` VALUES ('4', 'U付', 'UMPAY', '', '', '0', '2015-07-12 22:31:48', '2015-07-12 22:31:48');




DROP TABLE IF EXISTS `xquark_payment_merchant`;
CREATE TABLE `xquark_payment_merchant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) NOT NULL,
  `domain_code` varchar(20) NOT NULL,
  `profile` varchar(20) DEFAULT NULL,
  `payment_id` bigint(20) NOT NULL,
  `sign_type` varchar(20) DEFAULT NULL,
  `trade_type` varchar(20) NOT NULL COMMENT 'APP,WEB',
  `merchant_id` varchar(128) NOT NULL,
  `merchant_account` varchar(128) DEFAULT NULL COMMENT '支付宝email',
  `merchant_name` varchar(128) DEFAULT NULL,
  `callback_site` varchar(200) DEFAULT NULL,
  `notify_site` varchar(200) DEFAULT NULL,
  `app_id` varchar(128) DEFAULT NULL,
  `pub_key` varchar(512) DEFAULT NULL,
  `secret_key` varchar(2048) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_pay_mer_domain` (`domain_id`)
) ENGINE=InnoDB;

ALTER TABLE `xquark_payment_merchant`
ADD UNIQUE INDEX `idx_payment_mer` (`domain_id`, `profile`, `payment_id`, `trade_type`, `merchant_id`) ;


-- ----------------------------
-- Records of xquark_payment_merchant
-- ----------------------------
INSERT INTO `xquark_payment_merchant` VALUES ('1', '2', 'xiangqu', 'dev', '1', 'MD5', 'WEB', '2088611036448823', 'xiangqu@ixiaopu.com', null, 'http://localhost:8080', 'http://localhost:8080', null, '96lxp5317689pdy5hmuqvwml8d0gruzu', '96lxp5317689pdy5hmuqvwml8d0gruzu', '2015-07-13 01:09:01', '2015-07-13 01:09:04');
INSERT INTO `xquark_payment_merchant` VALUES ('2', '2', 'xiangqu', 'dev', '1', '0001', 'APP', '2088611036448823', 'xiangqu@ixiaopu.com', null, 'http://localhost:8080', 'http://localhost:8080', null, 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALys+oYaxqv4FYju8C1poM6qmHLjWPnXzqEJT0NxyFXgdaK/Qe9DcpcASod9mIAdlLIxJEyYNlWeonAJVYW8pQ+pTVGwI9n0iaT71ldWQzcMN3Dvi/+zpgw3HxxO7HJtEIlR84pvILv1yceCZCqqQ4O/4SemsG00oTiTyD3SM2ZvAgMBAAECgYBLToeX6ywNC7Icu7Hljll+45yBjri+0CJLKFoYw1uA21xYnxoEE9my54zX04uA502oafDhGYfmWLDhIvidrpP6oaluURb/gbV5Bdcm98gGGVgm6lpK+G5N/eawXDjP0ZjxXb114Y/Hn/oVFVM9OqcujFSV+Wg4JgJ4Mmtdr35gYQJBAPbhx030xPcep8/dL5QQMc7ddoOrfxXewKcpDmZJi2ey381X+DhuphQ5gSVBbbunRiDCEcuXFY+R7xrgnP+viWcCQQDDpN8DfqRRl+cUhc0z/TbnSPJkMT/IQoFeFOE7wMBcDIBoQePEDsr56mtc/trIUh/L6evP9bkjLzWJs/kb/i25AkEAtoOf1k/4NUEiipdYjzuRtv8emKT2ZPKytmGx1YjVWKpyrdo1FXMnsJf6k9JVD3/QZnNSuJJPTD506AfZyWS6TQJANdeF2Hxd1GatnaRFGO2y0mvs6U30c7R5zd6JLdyaE7sNC6Q2fppjmeu9qFYq975CKegykYTacqhnX4I8KEwHYQJAby60iHMAYfSUpu//f5LMMRFK2sVif9aqlYbepJcAzJ6zbiSG5E+0xg/MjEj/Blg9rNsqDG4RECGJG2nPR72O8g==', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('3', '2', 'xiangqu', 'dev', '2', 'MD5', 'WEB', '1248880601', null, null, 'http://localhost:8080', 'http://localhost:8080', 'wxaafb7e6c6d0bc90d', null, 'lfsea4lkjsfsdf10dsflklaidowfksed', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('4', '2', 'xiangqu', 'dev', '2', 'MD5', 'APP', '1248744501', null, null, 'http://localhost:8080', 'http://localhost:8080', 'wxe4564ca5ef3c23ef', null, 'jfoi34lkjsfsdf10dsflklaidoweqlsk', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('6', '2', 'xiangqu', 'dev', '3', 'RSA', 'WEB', '9854', null, null, 'http://localhost:8080', 'http://localhost:8080', null, 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('7', '2', 'xiangqu', 'dev', '3', 'RSA', 'APP', '9854', null, null, 'http://localhost:8080', 'http://localhost:8080', null, 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('8', '2', 'xiangqu', 'test', '1', 'MD5', 'WEB', '2088611036448823', 'xiangqu@ixiaopu.com', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', '96lxp5317689pdy5hmuqvwml8d0gruzu', '96lxp5317689pdy5hmuqvwml8d0gruzu', '2015-07-13 01:09:01', '2015-07-13 01:09:04');
INSERT INTO `xquark_payment_merchant` VALUES ('9', '2', 'xiangqu', 'test', '1', '0001', 'APP', '2088611036448823', 'xiangqu@ixiaopu.com', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALys+oYaxqv4FYju8C1poM6qmHLjWPnXzqEJT0NxyFXgdaK/Qe9DcpcASod9mIAdlLIxJEyYNlWeonAJVYW8pQ+pTVGwI9n0iaT71ldWQzcMN3Dvi/+zpgw3HxxO7HJtEIlR84pvILv1yceCZCqqQ4O/4SemsG00oTiTyD3SM2ZvAgMBAAECgYBLToeX6ywNC7Icu7Hljll+45yBjri+0CJLKFoYw1uA21xYnxoEE9my54zX04uA502oafDhGYfmWLDhIvidrpP6oaluURb/gbV5Bdcm98gGGVgm6lpK+G5N/eawXDjP0ZjxXb114Y/Hn/oVFVM9OqcujFSV+Wg4JgJ4Mmtdr35gYQJBAPbhx030xPcep8/dL5QQMc7ddoOrfxXewKcpDmZJi2ey381X+DhuphQ5gSVBbbunRiDCEcuXFY+R7xrgnP+viWcCQQDDpN8DfqRRl+cUhc0z/TbnSPJkMT/IQoFeFOE7wMBcDIBoQePEDsr56mtc/trIUh/L6evP9bkjLzWJs/kb/i25AkEAtoOf1k/4NUEiipdYjzuRtv8emKT2ZPKytmGx1YjVWKpyrdo1FXMnsJf6k9JVD3/QZnNSuJJPTD506AfZyWS6TQJANdeF2Hxd1GatnaRFGO2y0mvs6U30c7R5zd6JLdyaE7sNC6Q2fppjmeu9qFYq975CKegykYTacqhnX4I8KEwHYQJAby60iHMAYfSUpu//f5LMMRFK2sVif9aqlYbepJcAzJ6zbiSG5E+0xg/MjEj/Blg9rNsqDG4RECGJG2nPR72O8g==', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('10', '2', 'xiangqu', 'test', '2', 'MD5', 'WEB', '1248880601', '', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', 'wxaafb7e6c6d0bc90d', '', 'lfsea4lkjsfsdf10dsflklaidowfksed', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('11', '2', 'xiangqu', 'test', '2', 'MD5', 'APP', '1248744501', '', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', 'wxe4564ca5ef3c23ef', '', 'jfoi34lkjsfsdf10dsflklaidoweqlsk', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('12', '2', 'xiangqu', 'test', '3', 'RSA', 'WEB', '9854', '', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('13', '2', 'xiangqu', 'test', '3', 'RSA', 'APP', '9854', '', null, 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('14', '2', 'xiangqu', 'test', '4', 'RSA', 'APP', '9854', '', '', 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('15', '2', 'xiangqu', 'test', '4', 'RSA', 'WEB', '9854', '', '', 'http://xiangqu.kkkdtest.com', 'http://kkkd.xiangqutest.com:8888', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('16', '2', 'xiangqu', 'dev', '4', 'RSA', 'WEB', '9854', '', '', 'http://localhost:8080', 'http://localhost:8080', '', 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('17', '2', 'xiangqu', 'dev', '4', 'RSA', 'APP', '9854', '', '', 'http://localhost:8080', 'http://localhost:8080', '', 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('18', '2', 'xiangqu', 'preprod', '1', 'MD5', 'WEB', '2088611036448823', 'xiangqu@ixiaopu.com', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', '96lxp5317689pdy5hmuqvwml8d0gruzu', '96lxp5317689pdy5hmuqvwml8d0gruzu', '2015-07-13 01:09:01', '2015-07-13 01:09:04');
INSERT INTO `xquark_payment_merchant` VALUES ('19', '2', 'xiangqu', 'preprod', '1', '0001', 'APP', '2088611036448823', 'xiangqu@ixiaopu.com', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALys+oYaxqv4FYju8C1poM6qmHLjWPnXzqEJT0NxyFXgdaK/Qe9DcpcASod9mIAdlLIxJEyYNlWeonAJVYW8pQ+pTVGwI9n0iaT71ldWQzcMN3Dvi/+zpgw3HxxO7HJtEIlR84pvILv1yceCZCqqQ4O/4SemsG00oTiTyD3SM2ZvAgMBAAECgYBLToeX6ywNC7Icu7Hljll+45yBjri+0CJLKFoYw1uA21xYnxoEE9my54zX04uA502oafDhGYfmWLDhIvidrpP6oaluURb/gbV5Bdcm98gGGVgm6lpK+G5N/eawXDjP0ZjxXb114Y/Hn/oVFVM9OqcujFSV+Wg4JgJ4Mmtdr35gYQJBAPbhx030xPcep8/dL5QQMc7ddoOrfxXewKcpDmZJi2ey381X+DhuphQ5gSVBbbunRiDCEcuXFY+R7xrgnP+viWcCQQDDpN8DfqRRl+cUhc0z/TbnSPJkMT/IQoFeFOE7wMBcDIBoQePEDsr56mtc/trIUh/L6evP9bkjLzWJs/kb/i25AkEAtoOf1k/4NUEiipdYjzuRtv8emKT2ZPKytmGx1YjVWKpyrdo1FXMnsJf6k9JVD3/QZnNSuJJPTD506AfZyWS6TQJANdeF2Hxd1GatnaRFGO2y0mvs6U30c7R5zd6JLdyaE7sNC6Q2fppjmeu9qFYq975CKegykYTacqhnX4I8KEwHYQJAby60iHMAYfSUpu//f5LMMRFK2sVif9aqlYbepJcAzJ6zbiSG5E+0xg/MjEj/Blg9rNsqDG4RECGJG2nPR72O8g==', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('20', '2', 'xiangqu', 'preprod', '2', 'MD5', 'WEB', '1248880601', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', 'wxaafb7e6c6d0bc90d', '', 'lfsea4lkjsfsdf10dsflklaidowfksed', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('21', '2', 'xiangqu', 'preprod', '2', 'MD5', 'APP', '1248744501', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', 'wxe4564ca5ef3c23ef', '', 'jfoi34lkjsfsdf10dsflklaidoweqlsk', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('22', '2', 'xiangqu', 'preprod', '3', 'RSA', 'WEB', '9854', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('23', '2', 'xiangqu', 'preprod', '3', 'RSA', 'APP', '9854', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('24', '2', 'xiangqu', 'prod', '1', 'MD5', 'WEB', '2088611036448823', 'xiangqu@ixiaopu.com', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', '96lxp5317689pdy5hmuqvwml8d0gruzu', '96lxp5317689pdy5hmuqvwml8d0gruzu', '2015-07-13 01:09:01', '2015-07-13 01:09:04');
INSERT INTO `xquark_payment_merchant` VALUES ('25', '2', 'xiangqu', 'prod', '1', '0001', 'APP', '2088611036448823', 'xiangqu@ixiaopu.com', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALys+oYaxqv4FYju8C1poM6qmHLjWPnXzqEJT0NxyFXgdaK/Qe9DcpcASod9mIAdlLIxJEyYNlWeonAJVYW8pQ+pTVGwI9n0iaT71ldWQzcMN3Dvi/+zpgw3HxxO7HJtEIlR84pvILv1yceCZCqqQ4O/4SemsG00oTiTyD3SM2ZvAgMBAAECgYBLToeX6ywNC7Icu7Hljll+45yBjri+0CJLKFoYw1uA21xYnxoEE9my54zX04uA502oafDhGYfmWLDhIvidrpP6oaluURb/gbV5Bdcm98gGGVgm6lpK+G5N/eawXDjP0ZjxXb114Y/Hn/oVFVM9OqcujFSV+Wg4JgJ4Mmtdr35gYQJBAPbhx030xPcep8/dL5QQMc7ddoOrfxXewKcpDmZJi2ey381X+DhuphQ5gSVBbbunRiDCEcuXFY+R7xrgnP+viWcCQQDDpN8DfqRRl+cUhc0z/TbnSPJkMT/IQoFeFOE7wMBcDIBoQePEDsr56mtc/trIUh/L6evP9bkjLzWJs/kb/i25AkEAtoOf1k/4NUEiipdYjzuRtv8emKT2ZPKytmGx1YjVWKpyrdo1FXMnsJf6k9JVD3/QZnNSuJJPTD506AfZyWS6TQJANdeF2Hxd1GatnaRFGO2y0mvs6U30c7R5zd6JLdyaE7sNC6Q2fppjmeu9qFYq975CKegykYTacqhnX4I8KEwHYQJAby60iHMAYfSUpu//f5LMMRFK2sVif9aqlYbepJcAzJ6zbiSG5E+0xg/MjEj/Blg9rNsqDG4RECGJG2nPR72O8g==', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('26', '2', 'xiangqu', 'prod', '2', 'MD5', 'WEB', '1248880601', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', 'wxaafb7e6c6d0bc90d', '', 'lfsea4lkjsfsdf10dsflklaidowfksed', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('27', '2', 'xiangqu', 'prod', '2', 'MD5', 'APP', '1248744501', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', 'wxe4564ca5ef3c23ef', '', 'jfoi34lkjsfsdf10dsflklaidoweqlsk', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('28', '2', 'xiangqu', 'prod', '3', 'RSA', 'WEB', '9854', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('29', '2', 'xiangqu', 'prod', '3', 'RSA', 'APP', '9854', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('30', '2', 'xiangqu', 'prod', '4', 'RSA', 'APP', '9854', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('31', '2', 'xiangqu', 'prod', '4', 'RSA', 'WEB', '9854', '', '', 'http://kkkd.xiangqu.com', 'http://kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('32', '2', 'xiangqu', 'preprod', '4', 'RSA', 'WEB', '9854', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('33', '2', 'xiangqu', 'preprod', '4', 'RSA', 'APP', '9854', '', '', 'http://pre.kkkd.xiangqu.com', 'http://pre.kkkd.xiangqu.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9854_xiangquwang.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('34', '1', 'kkkd', 'dev', '1', 'MD5', 'WEB', '2088511131123172', 'admin@ixiaopu.com', null, 'http://localhost:8080', 'http://localhost:8080', null, '9hlgt8g075dq4k62i3n170c5bnqd74l9', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('35', '1', 'kkkd', 'dev', '2', 'MD5', 'WEB', '1221040401', null, null, 'http://localhost:8080', 'http://localhost:8080', 'wx12f9b23232474a10', '53d4e921f44d050cd6e53470bed1af13', '53d4e921f44d050cd6e53470bed1af13', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('36', '1', 'kkkd', 'dev', '3', 'RSA', 'WEB', '9335', null, null, 'http://localhost:8080', 'http://localhost:8080', null, 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('37', '1', 'kkkd', 'test', '1', 'MD5', 'WEB', '2088511131123172', 'admin@ixiaopu.com', '', 'http://www.kkkdtest.com', 'http://www.kkkdtest.com', '', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('38', '1', 'kkkd', 'test', '2', 'MD5', 'WEB', '1221040401', '', '', 'http://www.kkkdtest.com', 'http://www.kkkdtest.com', 'wx12f9b23232474a10', '53d4e921f44d050cd6e53470bed1af13', '53d4e921f44d050cd6e53470bed1af13', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('39', '1', 'kkkd', 'test', '3', 'RSA', 'WEB', '9335', '', '', 'http://www.kkkdtest.com', 'http://www.kkkdtest.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('40', '1', 'kkkd', 'preprod', '1', 'MD5', 'WEB', '2088511131123172', 'admin@ixiaopu.com', '', 'http://pre.www.kkkd.com', 'http://pre.www.kkkd.com', '', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('41', '1', 'kkkd', 'preprod', '2', 'MD5', 'WEB', '1221040401', '', '', 'http://pre.www.kkkd.com', 'http://pre.www.kkkd.com', 'wx12f9b23232474a10', '53d4e921f44d050cd6e53470bed1af13', '53d4e921f44d050cd6e53470bed1af13', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('42', '1', 'kkkd', 'preprod', '3', 'RSA', 'WEB', '9335', '', '', 'http://pre.www.kkkd.com', 'http://pre.www.kkkd.com', '', 'D:\\projects\\umcert\\cert_2d59.crt', 'D:\\projects\\umcert\\9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('43', '1', 'kkkd', 'prod', '1', 'MD5', 'WEB', '2088511131123172', 'admin@ixiaopu.com', '', 'http://www.kkkd.com', 'http://www.kkkd.com', '', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '9hlgt8g075dq4k62i3n170c5bnqd74l9', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('44', '1', 'kkkd', 'prod', '2', 'MD5', 'WEB', '1221040401', '', '', 'http://www.kkkd.com', 'http://www.kkkd.com', 'wx12f9b23232474a10', '53d4e921f44d050cd6e53470bed1af13', '53d4e921f44d050cd6e53470bed1af13', '2015-07-13 01:09:38', '2015-07-13 01:09:38');
INSERT INTO `xquark_payment_merchant` VALUES ('45', '1', 'kkkd', 'prod', '3', 'RSA', 'WEB', '9335', '', '', 'http://www.kkkd.com', 'http://www.kkkd.com', '', '/ouer/data/payCert/umpay/cert_2d59.crt', '/ouer/data/payCert/umpay/9335_KuaiKuaiKaiDian.key.p8', '2015-07-13 01:09:38', '2015-07-13 01:09:38');



CREATE TABLE `xquark_domain_login_strategy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `domain_id` bigint(20) unsigned NOT NULL COMMENT '域id',
  `profile` varchar(16) NOT NULL COMMENT '运行时环境',
  `module_name` varchar(16) NOT NULL COMMENT '运行时模块',
  `domain_name` varchar(64) NOT NULL COMMENT '域名',
  `login_strategy_type` varchar(16) NOT NULL COMMENT 'login strategy 类型',
  `login_strategy_value` varchar(128) NOT NULL COMMENT 'login strategy 值',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_dpmd` (`domain_id`,`profile`,`module_name`,`domain_name`) USING BTREE
) ENGINE=InnoDB;

-- ----------------------------
-- Records of xquark_domain_login_strategy
-- ----------------------------
INSERT INTO `xquark_domain_login_strategy` VALUES ('5', '2', 'prod', 'xquark-web', 'kkkd.xiangqu.com', 'customized-ref', 'xiangquLogin', '2015-07-06 19:04:02', '2015-07-06 19:09:36');
INSERT INTO `xquark_domain_login_strategy` VALUES ('6', '2', 'prod', 'xquark-web', 'xiangqu.kkkd.com', 'customized-ref', 'xiangquLogin', '2015-07-06 19:04:11', '2015-07-06 19:09:35');
INSERT INTO `xquark_domain_login_strategy` VALUES ('7', '2', 'dev', 'xquark-web', 'kkkd.xiangqutest.com', 'customized-ref', 'xiangquLogin', '2015-07-06 19:09:22', '2015-07-06 19:09:44');
INSERT INTO `xquark_domain_login_strategy` VALUES ('8', '2', 'dev', 'xquark-web', 'xiangqu.kkkdtest.com', 'customized-ref', 'xiangquLogin', '2015-07-06 19:09:59', '2015-07-06 19:10:13');
INSERT INTO `xquark_domain_login_strategy` VALUES ('9', '1', 'dev', 'xquark-web', 'www.kkkdtest.com', 'customized-ref', 'kkkdLogin', '2015-07-06 19:10:28', '2015-07-06 19:10:53');
INSERT INTO `xquark_domain_login_strategy` VALUES ('10', '1', 'prod', 'xquark-web', 'www.kkkd.com', 'customized-ref', 'kkkdLogin', '2015-07-06 19:11:12', '2015-07-06 19:11:46');
INSERT INTO `xquark_domain_login_strategy` VALUES ('11', '1', 'dev', 'xquark-web', 'localhost', 'customized-ref', 'kkkdLogin', '2015-07-06 19:12:13', '2015-07-06 19:47:10');
INSERT INTO `xquark_domain_login_strategy` VALUES ('12', '2', 'dev', 'xquark-rest-api', 'kkkd.xiangqutest.com', 'customized-ref', 'xiangquLogin', '2015-07-06 19:46:53', '2015-07-17 14:54:06');
INSERT INTO `xquark_domain_login_strategy` VALUES ('13', '2', 'test', 'xquark-web', 'xiangqu.kkkdtest.com', 'customized-ref', 'xiangquLogin', '2015-07-17 14:53:46', '2015-07-17 14:54:33');
INSERT INTO `xquark_domain_login_strategy` VALUES ('14', '2', 'test', 'xquark-web', 'kkkd.xiangqutest.com', 'customized-ref', 'xiangquLogin', '2015-07-17 14:54:19', '2015-07-17 14:54:34');
INSERT INTO `xquark_domain_login_strategy` VALUES ('15', '1', 'test', 'xquark-web', 'www.kkkdtest.com', 'customized-ref', 'kkkdLogin', '2015-07-17 15:09:17', '2015-07-17 15:09:41');
INSERT INTO `xquark_domain_login_strategy` VALUES ('18', '2', 'preprod', 'xquark-web', 'pre.kkkd.xiangqu.com', 'customized-ref', 'xiangquLogin', '2015-07-20 10:11:15', '2015-07-20 10:12:05');
INSERT INTO `xquark_domain_login_strategy` VALUES ('21', '2', 'preprod', 'xquark-web', 'pre.xiangqu.kkkd.com', 'customized-ref', 'xiangquLogin', '2015-07-20 10:12:12', '2015-07-20 10:12:21');
INSERT INTO `xquark_domain_login_strategy` VALUES ('22', '1', 'preprod', 'xquark-web', 'pre.www.kkkd.com', 'customized-ref', 'kkkdLogin', '2015-07-20 10:12:27', '2015-07-20 10:12:48');

