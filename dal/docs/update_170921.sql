ALTER TABLE xquark_promotion
  ADD COLUMN `is_share` TINYINT NOT NULL DEFAULT 0
COMMENT '是否与其他活动共享' AFTER `valid_to`;

ALTER TABLE xquark_promotion
  ADD COLUMN `original_price_only` TINYINT NOT NULL DEFAULT 0
COMMENT '是否只有原价享受折扣' AFTER `is_share`;

ALTER TABLE xquark_promotion ADD COLUMN `scope` VARCHAR(30) DEFAULT 'PRODUCT' NOT NULL COMMENT '活动范围' AFTER `type`;

DROP TABLE IF EXISTS xquark_promotion_full_cut;
CREATE TABLE xquark_promotion_full_cut (
  `id`                  BIGINT      NOT NULL AUTO_INCREMENT,
  `promotion_id`        BIGINT      NOT NULL
  COMMENT '活动id',
  `product_id`        BIGINT      NOT NULL
  COMMENT '商品id',
  `created_at`          DATETIME             DEFAULT NULL,
  `updated_at`          DATETIME             DEFAULT NULL,
  `archive`             TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '满就减活动商品关联表';

DROP TABLE IF EXISTS xquark_promotion_full_cut_discount;
CREATE TABLE xquark_promotion_full_cut_discount (
  `id`            BIGINT         NOT NULL AUTO_INCREMENT,
  `full_cut_id`   BIGINT         NOT NULL
  COMMENT
    '满减活动表id',
  `level`         INT            NOT NULL
  COMMENT '层级',
  `min_consume`   DECIMAL(18, 2) NOT NULL
  COMMENT '最低消费',
  `discount_type` VARCHAR(30)    NOT NULL
  COMMENT '优惠方式',
  `created_at`    DATETIME                DEFAULT NULL,
  `updated_at`    DATETIME                DEFAULT NULL,
  `archive`       TINYINT(4)     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '满就减活动折扣表';

DROP TABLE IF EXISTS xquark_promotion_full_pieces;
CREATE TABLE xquark_promotion_full_pieces (
  `id`                  BIGINT      NOT NULL AUTO_INCREMENT,
  `promotion_id`        BIGINT      NOT NULL
  COMMENT '活动id',
  `product_id`        BIGINT      NOT NULL
  COMMENT '商品id',
  `created_at`          DATETIME             DEFAULT NULL,
  `updated_at`          DATETIME             DEFAULT NULL,
  `archive`             TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '满件数活动商品关联表';

DROP TABLE IF EXISTS xquark_promotion_full_pieces_discount;
CREATE TABLE xquark_promotion_full_pieces_discount (
  `id`            BIGINT         NOT NULL AUTO_INCREMENT,
  `promotion_id`   BIGINT         NOT NULL
  COMMENT
    '满件活动表id(promotionId)',
  `level`         INT            NOT NULL
  COMMENT '层级',
  `min_amount`   DECIMAL(18, 2) NOT NULL
  COMMENT '最低件数',
  `discount_type` VARCHAR(30)    NOT NULL
  COMMENT '优惠方式',
  `created_at`    DATETIME                DEFAULT NULL,
  `updated_at`    DATETIME                DEFAULT NULL,
  `archive`       TINYINT(4)     NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '满件活动折扣表';


ALTER TABLE xquark_promotion_full_cut_discount ADD COLUMN `discount` DECIMAL(18, 2) DEFAULT NULL COMMENT '折扣' AFTER `discount_type`;
ALTER TABLE xquark_promotion_full_cut_discount ADD COLUMN `cash` DECIMAL(18, 2) DEFAULT NULL COMMENT '现金优惠' AFTER `discount`;
ALTER TABLE xquark_promotion_full_cut_discount ADD COLUMN `free_delivery` TINYINT DEFAULT FALSE COMMENT '是否包邮' AFTER `cash`;

ALTER TABLE xquark_promotion_full_pieces_discount ADD COLUMN `discount` DECIMAL(18, 2) DEFAULT NULL COMMENT '折扣' AFTER `discount_type`;
ALTER TABLE xquark_promotion_full_pieces_discount ADD COLUMN `cash` DECIMAL(18, 2) DEFAULT NULL COMMENT '现金优惠' AFTER `discount`;
ALTER TABLE xquark_promotion_full_pieces_discount ADD COLUMN `free_delivery` TINYINT DEFAULT FALSE COMMENT '是否包邮' AFTER `cash`;

DROP TABLE `xquark_promotion_full_reduce_order`;
CREATE TABLE `xquark_promotion_full_reduce_order` (
  `id`                  BIGINT      NOT NULL AUTO_INCREMENT,
  `promotion_id`        BIGINT      NOT NULL
  COMMENT '活动id',
  `order_id`        BIGINT      NOT NULL
  COMMENT '订单id',
  `discount_fee`        DECIMAL      NOT NULL
  COMMENT '优惠金额',
  `created_at`          DATETIME             DEFAULT NULL,
  `updated_at`          DATETIME             DEFAULT NULL,
  `archive`             TINYINT(4)  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '满减、满件活动优惠订单关联表';

ALTER TABLE xquark_product ADD COLUMN gift TINYINT NOT NULL DEFAULT FALSE COMMENT '是否为赠品' AFTER template_value;

ALTER TABLE xquark_promotion_full_cut_discount ADD COLUMN `gift` TINYINT NOT NULL DEFAULT FALSE COMMENT '是否有赠品' AFTER `cash`;
ALTER TABLE xquark_promotion_full_pieces_discount ADD COLUMN `gift` TINYINT NOT NULL DEFAULT FALSE COMMENT '是否有赠品' AFTER `cash`;

ALTER TABLE xquark_promotion_full_cut_discount ADD COLUMN `gift_ids` VARCHAR(255) DEFAULT '' COMMENT '赠品id' AFTER `gift`;
ALTER TABLE xquark_promotion_full_pieces_discount ADD COLUMN `gift_ids` VARCHAR(255) DEFAULT '' COMMENT '赠品id' AFTER `gift`;
