alter table `xquark_shop` add column code varchar(100) default null;
alter table `xquark_sku` add column code varchar(100) default null;
alter table `xquark_user` add column code varchar(100) default null;