-- 新增部门组织表
CREATE TABLE `xquark_org` (
  -- AUTO_INCREMENT
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT '组织名称',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '父节点id',
  `is_leaf` tinyint(4) NOT NULL DEFAULT FALSE COMMENT '是否叶子节点',
  `is_auto_expand` tinyint(4) NOT NULL DEFAULT FALSE COMMENT '是否自动展开',
  `icon_name` varchar(255) DEFAULT NULL COMMENT '节点图标文件名称',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `remark` varchar(4000) DEFAULT NULL COMMENT '备注',
  `created_at` datetime DEFAULT NULL,
  `archive` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门组织表';

-- 用户角色表新增org_id字段
ALTER TABLE `xquark_merchant` ADD COLUMN `org_id` bigint NOT NULL COMMENT '所属部门id';

-- 增加菜单表
CREATE TABLE `xquark_module` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '父节点id',
  `name` varchar(255) NOT NULL COMMENT '功能模块名称',
  `url` varchar(255) DEFAULT NULL COMMENT '主页面URL',
  `icon_name` varchar(255) DEFAULT NULL COMMENT '节点图标文件名称',
  `is_leaf` TINYINT NOT NULL COMMENT '是否叶子节点',
  `is_auto_expand` TINYINT NOT NULL COMMENT '是否自动展开',
  `sort_no` int(10) DEFAULT NULL COMMENT '排序号',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  `module` VARCHAR(255) NOT NULL COMMENT 'module名称',
  `page` VARCHAR(255) NOT NULL COMMENT 'page名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- 增加菜单角色关联表
CREATE TABLE `xquark_merchant_role_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` bigint(20) NOT NULL COMMENT '角色id',
  `module_id` bigint(20) NOT NULL COMMENT '菜单id',
  `created_at` datetime DEFAULT NULL,
  `archive` tinyint(4) DEFAULT NULL COMMENT '是否删除',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8 COMMENT='菜单-角色关联表';

-- 增加用户角色关联表
CREATE TABLE `xquark_merchant_role_merchant` (
  `id` bigint NOT NULL COMMENT 'id' AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色关联表';

-- 增加菜单-按钮关联表
CREATE TABLE `xquark_module_function` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `function_id` varchar(255) NOT NULL COMMENT '按钮在html中的id',
  `module_id` bigint NOT NULL COMMENT '菜单id',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单-按钮关联表';

-- 增加角色按钮关联表
CREATE TABLE `xquark_role_function` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `function_id` varchar(255) NOT NULL COMMENT '按钮在html中的id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  `is_show` TINYINT DEFAULT TRUE COMMENT '是否可见',
  `is_enable` TINYINT DEFAULT TRUE COMMENT '是否可用',
  `created_at` DATETIME NULL,
  `archive` TINYINT NULL COMMENT '是否删除',
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单-角色关联表';

-- 初始u用户 为ROOT角色
INSERT INTO `xquark_merchant_role_merchant` VALUES (1,17496624,5,now(),0,now());

-- 添加基础角色
INSERT INTO `xquark_merchant_role` (parent_id, role_name, created_at, role_desc, permissions, archive, updated_at, creater_id) VALUES
  (0, 'BASIC', now(), '基础角色', '', FALSE, now(), 0);

-- 设置基础角色可操作的菜单
INSERT INTO `xquark_merchant_role_module` (role_id, module_id, created_at, archive, updated_at)
VALUES (7,24, now() ,0, now()),(7,25, now() ,0, now());

-- 初始菜单数据
LOCK TABLES `xquark_module` WRITE;
/*!40000 ALTER TABLE `xquark_module` DISABLE KEYS */;
INSERT INTO `xquark_module` VALUES
  (23,NULL,'帐号管理','','icon-people',0,0,1,now(),0,now(),'profile,password,admin','merchant,profile'),
  (24,'23','个人设置','/profile','icon-cog5',1,0,1,now(),0,now(),'profile','merchant,profile'),
  (25,'23','修改密码','/profile/password','icon-pen6',1,0,2,NOW(),0,NOW(),'password','merchant,profile'),(26,'23','管理员设置','/merchant/admin','icon-user',1,0,3,NOW(),0,NOW(),'admin','merchant,profile'),(27,NULL,'商品管理','',' icon-bucket',0,0,2,NOW(),0,NOW(),'product,category','mall,merchant,profile'),(28,'27','商品','/mall/product','icon-dropbox',1,0,1,NOW(),0,NOW(),'product','mall'),(29,'27','商品类别设置','/category/list','icon-info22',1,0,2,NOW(),0,NOW(),'category','merchant,profile'),(30,NULL,'代理管理','','icon-tree6',0,0,3,NOW(),0,NOW(),'summary,member,tree','agent'),(31,NULL,'佣金管理','','icon-price-tag',0,0,4,NOW(),0,NOW(),'commission','agent'),(32,NULL,'审核管理','','icon-checkmark4',0,0,5,NOW(),0,NOW(),'apply,agent,auditRole','agent'),(33,NULL,'订单管理','','icon-clipboard4',0,0,6,NOW(),0,NOW(),'order,auditRole','mall'),(34,NULL,'二维码申请','','icon-qrcode',0,0,7,NOW(),0,NOW(),'generalQrcode,generalDirectorQrcode,generalSecondQrcode,generalFirstQrcode','agent'),(35,NULL,'防伪管理','','icon-cog4',0,0,8,NOW(),0,NOW(),'',''),(36,NULL,'数据统计','','icon-stats-growth',0,0,9,NOW(),0,NOW(),'trade,flow,guest','datacenter'),(37,NULL,'系统设置','','icon-cog4',0,0,10,NOW(),0,NOW(),'role,org,module,homeItem,contact,function','org_list,module_manage,org_edit,merchant,mall,function_manage'),(38,NULL,'代理设置','','icon-cog7',0,0,11,NOW(),0,NOW(),'role,rolePrice,roleProduct','mall,agent'),(39,NULL,'微信设置','','icon-bubbles4',0,0,12,NOW(),0,NOW(),'summary,config,templateMessage,autoReply,customizedMenus','wechat'),(40,'30','代理概况','/agent/summary','icon-statistics',1,0,1,NOW(),0,NOW(),'summary','agent'),(41,'30','代理会员','/agent/member','icon-insert-template',1,0,2,NOW(),0,NOW(),'member','agent'),(42,'30','树状图','/agent/tree','icon-insert-template',1,0,3,NOW(),0,NOW(),'tree','agent'),(43,'31','代理佣金','/agent/commission','icon-insert-template',1,0,1,NOW(),0,NOW(),'commission','agent'),(44,'32','代理审核','/agent/apply','icon-check',1,0,1,NOW(),0,NOW(),'apply','agent'),(45,'32','代理审核规则','/agent/auditRule','icon-dropbox',1,0,2,NOW(),0,NOW(),'auditRole','agent'),(46,'33','订单','/mall/order','icon-stack',1,0,1,NOW(),0,NOW(),'order','mall'),(47,'33','订单审核规则','/auditRule/list','icon-dropbox',1,0,2,NOW(),0,NOW(),'auditRole','mall'),(48,'34','董事二维码','/agent/generalDirectorQrcode','icon-insert-template',1,0,1,NOW(),0,NOW(),'generalDirectorQrcode','agent'),(49,'34','总代二维码','/sellerpc/agent/generalQrcode','icon-insert-template',1,0,2,NOW(),0,NOW(),'generalQrcode','agent'),(50,'34','二星二维码','/sellerpc/agent/generalSecondQrcode','icon-insert-template',1,0,3,NOW(),0,NOW(),'generalSecondQrcode','agent'),(51,'34','一星二维码','/sellerpc/agent/generalFirstQrcode','icon-insert-template',1,0,4,NOW(),0,NOW(),'generalFirstQrcode','agent'),(52,'35','防伪码查询','/antifake/query','icon-user4',1,0,1,NOW(),0,NOW(),'',''),(53,'36','交易概况','/datacenter/trade','icon-statistics',1,0,1,NOW(),0,NOW(),'trade','datacenter'),(54,'36','流量概况','/datacenter/flow','icon-user',1,0,2,NOW(),0,NOW(),'flow','datacenter'),(55,'36','访客分析','/sellerpc/datacenter/guest','icon-insert-template',1,0,3,NOW(),0,NOW(),'guest','datacenter'),(56,'37','管理员角色','/merchant/role',' icon-users2',1,0,1,NOW(),0,NOW(),'role','merchant,profile'),(57,'37','首页模块设置','/homeItem/list','icon-dropbox',1,0,2,NOW(),0,NOW(),'homeItem','mall'),(58,'37','部门管理','/org/list','icon-dropbox',1,0,3,NOW(),0,NOW(),'org','org_list'),(59,'37','部门人员管理','/org/edit','icon-users4',1,0,4,NOW(),0,NOW(),'org','org_edit'),(60,'37','配置菜单','/module/manage','icon-person',1,0,5,NOW(),0,NOW(),'module','module_manage'),(61,'38','新增代理','/userAgent/apply4Admin','icon-dropbox',1,0,1,NOW(),0,NOW(),'',''),(62,'38','代理角色设置','/role/list','icon-dropbox',1,0,2,NOW(),0,NOW(),'role','mall'),(63,'38','角色价格设置','/rolePrice/list','icon-dropbox',1,0,3,NOW(),0,NOW(),'rolePrice','mall'),(64,'38','佣金规则设置','/rule/list','icon-insert-template',1,0,4,NOW(),0,NOW(),'role','agent'),(65,'38','商品佣金设置','/ruleProduct/list','icon-insert-template',1,0,5,NOW(),0,NOW(),'ruleProduct','agent'),(66,'39','微信概况','/wechat/summary','icon-statistics',1,0,1,NOW(),0,NOW(),'summary','wechat'),(67,'39','公众号设置','/wechat/config','icon-user',1,0,2,NOW(),0,NOW(),'config','wechat'),(68,'39','消息模板设置','/wechat/templateMessage','icon-insert-template',1,0,3,NOW(),0,NOW(),'templateMessage','wechat'),(69,'39','自动回复','/wechat/autoReply','icon-reply',1,0,4,NOW(),0,NOW(),'autoReply','wechat'),(70,'39','自定义菜单','/wechat/customizedMenus','icon-menu6',1,0,5,NOW(),0,NOW(),'customizedMenus','wechat'),(71,'37','配置按钮','/function/manage','icon-insert-template',1,0,6,NOW(),0,NOW(),'function','function_manage');
/*!40000 ALTER TABLE `xquark_module` ENABLE KEYS */;
UNLOCK TABLES;

-- 菜单角色关联数据
LOCK TABLES `xquark_merchant_role_module` WRITE;
/*!40000 ALTER TABLE `xquark_merchant_role_module` DISABLE KEYS */;
-- 配置ROOT角色默认所有菜单可见
INSERT INTO `xquark_merchant_role_module` (role_id, module_id, created_at, archive, updated_at)
VALUES
  (5,24,NOW(),0,NOW()),(5,25,NOW(),0,NOW()),(5,26,NOW(),0,NOW()),
  (5,43,NOW(),0,NOW()),(5,44,NOW(),0,NOW()),(5,45,NOW(),0,NOW()),
  (5,46,NOW(),0,NOW()),(5,47,NOW(),0,NOW()),(5,48,NOW(),0,NOW()),
  (5,49,NOW(),0,NOW()),(5,50,NOW(),0,NOW()),(5,51,NOW(),0,NOW()),
  (5,52,NOW(),0,NOW()),(5,53,NOW(),0,NOW()),(5,54,NOW(),0,NOW()),
  (5,55,NOW(),0,NOW()),(5,56,NOW(),0,NOW()),(5,57,NOW(),0,NOW()),
  (5,58,NOW(),0,NOW()),(5,59,NOW(),0,NOW()),(5,60,NOW(),0,NOW()),
  (5,61,NOW(),0,NOW()),(5,62,NOW(),0,NOW()),(5,63,NOW(),0,NOW()),
  (5,64,NOW(),0,NOW()),(5,65,NOW(),0,NOW()),(5,66,NOW(),0,NOW()),
  (5,67,NOW(),0,NOW()),(5,68,NOW(),0,NOW()),(5,69,NOW(),0,NOW()),
  (5,70,NOW(),0,NOW()),(5,71,NOW(),0,NOW()),(5,40,NOW(),0,NOW()),
  (5,28,NOW(),0,NOW()),(5,29,NOW(),0,NOW()),(5,41,NOW(),0,NOW()),
  (5,42,NOW(),0,NOW());
/*!40000 ALTER TABLE `xquark_merchant_role_module` ENABLE KEYS */;
UNLOCK TABLES;

-- 增加app配置页面
INSERT INTO xquark_module VALUES (74, 37, 'app更新设置', '/version/list', 'icon-dropbox', TRUE, FALSE, 7, now(), FALSE, now(), 'version', 'mall');
INSERT xquark_merchant_role_module (role_id, module_id, created_at, archive, updated_at) VALUES
  (5, 74, now(), FALSE, now());

UPDATE xquark_module SET module = 'role,org,module,homeItem,contact,version,function' WHERE name = '系统设置';

UPDATE xquark_module SET url = '~/antifake/query' WHERE name = '防伪码查询';
UPDATE xquark_module SET url = '~/userAgent/apply4Admin' WHERE name = '新增代理';

UPDATE xquark_module SET module = 'roleAgent' WHERE id = 62;
UPDATE xquark_module SET module = 'rulePrice' WHERE id = 64;
UPDATE xquark_module set module = 'roleAgent,rolePrice,rulePrice,ruleProduct' WHERE id = 38;





