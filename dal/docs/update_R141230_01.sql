/**
 *	再次部署时重新执行的sql语句
 */

--更新pay_no，兼容老数据
update xquark_order set pay_no = order_no where paid_at is not null and pay_no is null;
update xquark_cashieritem set batch_biz_nos = biz_no where batch_biz_nos is null;

update xquark_cart_item ci, xquark_product p
set ci.shop_id = p.shop_id, ci.seller_id = p.user_id
where ci.product_id = p.id;

update xquark_activity_ticket t INNER JOIN xquark_activity a on t.activity_id=a.id 
set t.preferential_type=a.preferential_type, t.discount=a.discount, t.reduction=a.reduction;

update xquark_order set logistics_company = '顺丰速运' where logistics_company = '顺丰';
update xquark_order set logistics_company = '顺丰速运' where logistics_company = '顺丰快递';
update xquark_order set logistics_company = '顺丰速运' where logistics_company = 'SF_EXPRESS';
update xquark_order set logistics_company = '圆通速递' where logistics_company = '圆通';
update xquark_order set logistics_company = '圆通速递' where logistics_company = '圆通快递';
update xquark_order set logistics_company = '圆通速递' where logistics_company = 'YTO';
update xquark_order set logistics_company = '申通快递' where logistics_company = '申通';
update xquark_order set logistics_company = '申通快递' where logistics_company = 'STO';
update xquark_order set logistics_company = '中通快递' where logistics_company = '中通';
update xquark_order set logistics_company = '中通快递' where logistics_company = 'ZTO';
update xquark_order set logistics_company = '汇通快运' where logistics_company = '百世汇通';
update xquark_order set logistics_company = '汇通快运' where logistics_company = 'BESTEX';
update xquark_order set logistics_company = '韵达快递' where logistics_company = '韵达';
update xquark_order set logistics_company = '天天快递' where logistics_company = '天天';
update xquark_order set logistics_company = '全峰快递' where logistics_company = '全峰';
update xquark_order set logistics_company = 'EMS' where logistics_company = '邮政EMS';
update xquark_order set logistics_company = 'EMS' where logistics_company = '中国邮政';
update xquark_order set logistics_company = '其他物流' where logistics_company = 'OTHER';

update xquark_activity_ticket t INNER JOIN xquark_activity a on t.activity_id=a.id 
set t.preferential_type=a.preferential_type, t.discount=a.discount, t.reduction=a.reduction;

update xquark_activity set channel='PRIVATE';
