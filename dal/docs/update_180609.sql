ALTER TABLE xquark_product
  ADD height double DEFAULT 0 NOT NULL
COMMENT '商品的高度';
ALTER TABLE xquark_product
  ADD width double DEFAULT 0 NOT NULL
COMMENT '商品宽度';
ALTER TABLE xquark_product
  ADD length double DEFAULT 0 NOT NULL
COMMENT '商品长度';
-- ALTER TABLE xquark_product ADD length_unit varchar(8) DEFAULT 'CM' NOT NULL COMMENT '商品的长度单位';
ALTER TABLE xquark_product
  ADD kind varchar(16) DEFAULT '' NOT NULL
COMMENT '商品类型';
-- ALTER TABLE xquark_product ADD weight_unit varchar(8) DEFAULT 'G' NOT NULL COMMENT '商品的重量单位';
ALTER TABLE xquark_product
  ADD point decimal DEFAULT 0 NOT NULL
COMMENT '商品购买返利积分';
ALTER TABLE xquark_product
  ADD morality_point_rate decimal DEFAULT 0 NOT NULL
COMMENT '商品的德分比例';