ALTER TABLE xquark_order ADD COLUMN `full_yundou` TINYINT(1) NOT NULL DEFAULT FALSE COMMENT '是否为全积分兑换订单';
ALTER TABLE xquark_order ADD COLUMN `paid_yundou` DECIMAL(10, 2) NOT NULL DEFAULT 0 COMMENT '使用的积分数量';
