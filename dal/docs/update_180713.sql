CREATE DATABASE /*!32312 IF NOT EXISTS*/`hds` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hds`;

/*Table structure for table `xquark_order_send` */

DROP TABLE IF EXISTS `xquark_order_send`;

CREATE TABLE `xquark_order_send` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `send_type` varchar(255) DEFAULT NULL COMMENT '订单发货类别',
  `logistic_name` varchar(255) NOT NULL COMMENT '快递名称',
  `logistic_type` varchar(255) NOT NULL COMMENT '快递类别',
  `logistic_no` varchar(255) NOT NULL COMMENT '快递运单号',
  `plat_order_no` varchar(255) NOT NULL COMMENT '平台订单号',
  `is_split` int(10) NOT NULL COMMENT '是否拆单发货(拆单=1 ，不拆单=0)',
  `sub_plat_order_no` varchar(4000) NOT NULL COMMENT '平台订单号',
  `sender_name` varchar(255) DEFAULT NULL COMMENT '发货人姓名',
  `sender_tel` varchar(255) DEFAULT NULL COMMENT '发货人联系电话',
  `sender_address` varchar(4000) DEFAULT NULL COMMENT '发货人地址',
  `is_hwg_flag` int(10) DEFAULT NULL COMMENT '是否为海外购(是=1；否=0)',
  `created_at` datetime DEFAULT NULL,
  `archive` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='菠萝派物流信息详情表';