CREATE TABLE `xquark_tags` (
  `id`          BIGINT(20)  NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(50) NOT NULL
  COMMENT '标签名称',
  `description` VARCHAR(200)         DEFAULT NULL
  COMMENT '标签描述',
  `created_at`  DATETIME             DEFAULT NULL,
  `updated_at`  DATETIME             DEFAULT NULL,
  `archive`     TINYINT(4)  NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='标签表';

CREATE TABLE `xquark_product_tags` (
  `id`         BIGINT(20) NOT NULL AUTO_INCREMENT,
  `tag_id`     BIGINT(20) NOT NULL
  COMMENT '标签id',
  `product_id` BIGINT(20) NOT NULL
  COMMENT '商品id',
  `created_at` DATETIME            DEFAULT NULL,
  `updated_at` DATETIME            DEFAULT NULL,
  `archive`    TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='商品标签关联表';

ALTER TABLE xquark_tags DROP COLUMN `score`;
ALTER TABLE xquark_tags ADD COLUMN `score` INTEGER NOT NULL DEFAULT 0 COMMENT '标签热度' AFTER description;

ALTER TABLE xquark_tags ADD COLUMN `category` VARCHAR(20) NOT NULL DEFAULT 'PRODUCT' COMMENT '标签分类' AFTER description;
