ALTER TABLE `xquark_coupon` ADD INDEX `idx_coupon_ext_id` (`ext_coupon_id`) ;

ALTER TABLE `xquark_coupon` ADD COLUMN `pay_no` varchar(40) NULL AFTER `status`;
ALTER TABLE `xquark_cashieritem` ADD INDEX `idx_cashieritem_user` (`user_id`) ;

