--新增spider更新时间字段 2015-03-25
ALTER TABLE spider.item
ADD COLUMN update_at timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER create_at;

--新增shop依附表 2015-03-26
CREATE TABLE xquark_shop_prop (
  shop_id bigint(20) NOT NULL,
  third_shop_url varchar(256) NOT NULL,
  auto_updated tinyint(1) NOT NULL DEFAULT '0',
  auto_updated_at datetime DEFAULT NULL,
  created_at datetime NOT NULL,
  updated_at datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--初始配置想去自营店
insert into xquark_shop_prop(shop_id,third_shop_url,auto_updated,created_at) values 
(6098667,'http://jiwenbo.taobao.com/',true,now()),
(6098667,'http://shangulideshaonian.taobao.com/',true,now()),
(6098667,'http://luanyimu.taobao.com/',true,now()),
(6098667,'http://y-vison.taobao.com/',true,now()),
(6098667,'http://s-wv.taobao.com/',true,now()),
(6098667,'http://feng-ren.taobao.com/',true,now()),
(6098667,'http://taciturnli.taobao.com/',true,now()),
(6098667,'http://triplemajor.taobao.com/',true,now()),
(6098667,'http://onebyone-studio.taobao.com/',true,now()),
(6098667,'http://morbi.taobao.com/',true,now()),
(6098667,'http://unncc.taobao.com/',true,now()),
(6098667,'http://folopo.taobao.com/',true,now()),
(6098667,'http://tiyozi.taobao.com/',true,now());

-- 创建product表third_item_id唯一索引
-- 2015/4/2
CREATE UNIQUE INDEX idx_product_third ON xquark_product(third_item_id)