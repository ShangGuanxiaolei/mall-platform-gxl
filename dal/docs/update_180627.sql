ALTER TABLE xquark_product ADD supplier_id bigint(20) NULL COMMENT '供应商';
ALTER TABLE xquark_product ALTER COLUMN support_refund SET DEFAULT '1';
ALTER TABLE xquark_product MODIFY point decimal(10) DEFAULT '0' COMMENT '商品购买返利积分';
ALTER TABLE xquark_sku ADD bar_code varchar(64) NULL COMMENT '商品条形码';

