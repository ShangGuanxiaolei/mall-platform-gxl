ALTER TABLE xquark_promotion_pre_order ADD COLUMN `can_pay_time` TIMESTAMP NOT NULL COMMENT '付尾款时间' AFTER updated_at;

ALTER TABLE xquark_order ADD COLUMN `is_pay_rest` TINYINT DEFAULT FALSE COMMENT '是否为付尾款订单' AFTER paid_yundou;