ALTER TABLE xquark_promotion_coupon
  ADD COLUMN `scope` VARCHAR(30) NOT NULL
  DEFAULT 'SHOP'
  AFTER `status`;

ALTER TABLE xquark_product
  ADD COLUMN `u8_encode` VARCHAR(50) default ''
  AFTER encode;

DROP TABLE IF EXISTS xquark_inside_employee_lhlh;
CREATE TABLE xquark_inside_employee_lhlh (
  `id`  BIGINT  NOT NULL
  COMMENT '内部员工id',
  `english_name` VARCHAR(100) NOT NULL
  COMMENT '员工英文名',
  `name`        varchar(100) NOT NULL
  COMMENT '员工名',
  UNIQUE idx_name (id, name(100)),
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '沁园内部员工表-联合利华';

DROP TABLE IF EXISTS xquark_inside_employee_qinyuan;
CREATE TABLE xquark_inside_employee_qinyuan (
  `id`  BIGINT  NOT NULL
  COMMENT '内部员工id',
  `english_name` VARCHAR(100) NOT NULL
  COMMENT '员工英文名',
  `name`        varchar(100) NOT NULL
  COMMENT '员工名',
  UNIQUE idx_name (id, name(100)),
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '沁园内部员工表-沁园';

DROP TABLE IF EXISTS xquark_inside_user_employee;
CREATE TABLE xquark_inside_user_employee (
  `id`  BIGINT  NOT NULL AUTO_INCREMENT
  COMMENT 'id',
  `user_id` BIGINT NOT NULL UNIQUE
  COMMENT '用户id',
  `employee_id`        BIGINT NOT NULL UNIQUE
  COMMENT '员工id',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '员工用户关联表';

ALTER TABLE xquark_promotion ADD COLUMN `user_scope` VARCHAR(30) NOT NULL DEFAULT 'ALL' AFTER `scope`;

ALTER TABLE xquark_promotion ADD COLUMN `discount` DECIMAL(18, 2) DEFAULT NULL AFTER details;

ALTER TABLE xquark_promotion_full_cut ADD COLUMN `discount` DECIMAL(18, 2) NOT NULL AFTER product_id;

ALTER TABLE xquark_comment ADD COLUMN `star` INTEGER NOT NULL DEFAULT 5 AFTER type;

ALTER TABLE xquark_order modify COLUMN FULL_YUNDOU TINYINT DEFAULT FALSE ;

ALTER TABLE xquark_order MODIFY COLUMN paid_yundou BIGINT(20) DEFAULT 0;

ALTER TABLE xquark_order_item ADD COLUMN `discount` DECIMAL(18, 2) DEFAULT 0 COMMENT '优惠金额' AFTER `market_price`;
ALTER TABLE xquark_order_item ADD COLUMN `discount_price` DECIMAL(18, 2) DEFAULT 0 COMMENT '优惠价' AFTER `discount`;

ALTER TABLE xquark_order_item_promotion ADD COLUMN `type` VARCHAR(50) NOT NULL AFTER `promotion_id`;

UPDATE xquark_order_item SET discount_price = price;

ALTER TABLE xquark_promotion ADD COLUMN `is_free_delivery` TINYINT DEFAULT FALSE COMMENT '是否包邮, 默认不包邮';

ALTER TABLE xquark_order_item ADD COLUMN `promotion_type` VARCHAR(50) DEFAULT NULL COMMENT '使用活动类型' AFTER `discount_price`;
