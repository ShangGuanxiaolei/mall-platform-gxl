# 清空商品、订单表
truncate xquark_product;
truncate xquark_sku;

truncate xquark_main_order;
truncate xquark_order;
truncate xquark_order_item;
truncate xquark_order_address;
truncate xquark_address;
truncate xquark_promotion;
truncate xquark_promotion_flash_sale;
truncate xquark_promotion_bargain;
truncate pointTotal;
truncate pointTotalAudit;
truncate pointSuspending;
truncate commissionTotal;
truncate commissionTotalAudit;
truncate commissionSuspending;

# 删除除总店外用户
# delete from xquark_shop
# where id != 6102910;
# 删除前校验该用户是否为总店用户
delete from xquark_user
where id != 17481953;
