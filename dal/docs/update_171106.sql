DROP TABLE IF EXISTS xquark_fan_card_apply;
CREATE TABLE xquark_fan_card_apply (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `user_id`    BIGINT       NOT NULL
  COMMENT '申请人用户id',
  `name`       VARCHAR(50)  NOT NULL
  COMMENT '申请人姓名',
  `gender`     VARCHAR(10)  NOT NULL
  COMMENT '申请人性别',
  `age`        INTEGER(3)   NOT NULL
  COMMENT '年龄',
  `phone`      VARCHAR(100) NOT NULL
  COMMENT '手机号',
  `email`      VARCHAR(100) NOT NULL
  COMMENT '邮箱',
  `wechat_no`  VARCHAR(100) NOT NULL
  COMMENT '微信号',
  `address`    VARCHAR(255) NOT NULL
  COMMENT '住址',
  `status`     VARCHAR(20)  NOT NULL
  COMMENT '申请状态 APPLYING REJECTED SUCCEED',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '范卡申请表';

# 用户表添加范卡id字段
ALTER TABLE xquark_user
  ADD COLUMN `fan_card_no` VARCHAR(10) DEFAULT ''
  AFTER yundou;
