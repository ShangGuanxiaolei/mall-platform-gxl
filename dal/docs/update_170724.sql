ALTER TABLE xquark_promotion_bargain ADD COLUMN `amount` DECIMAL(18, 0) NOT NULL DEFAULT 0 AFTER `price_end`;
ALTER TABLE xquark_promotion_bargain ADD COLUMN `sales` DECIMAL(18, 0) NOT NULL DEFAULT 0 AFTER `amount`;
