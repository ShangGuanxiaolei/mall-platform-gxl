DROP TABLE IF EXISTS xquark_member_promotion;
CREATE TABLE xquark_member_promotion (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `title`      VARCHAR(100) NOT NULL
  COMMENT '活动标题',
  `banner`     VARCHAR(255) NOT NULL
  COMMENT '介绍图',
  `status`     VARCHAR(20)  NOT NULL
  COMMENT '状态',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '用户消息表 - 缓存用户消息';
