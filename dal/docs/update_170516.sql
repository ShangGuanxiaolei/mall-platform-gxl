DROP TABLE IF EXISTS `xquark_agent_whitelist`;
CREATE TABLE `xquark_agent_whitelist` (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `phone`      VARCHAR(255) NOT NULL,
  `is_enable`  TINYINT      NOT NULL DEFAULT TRUE,
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT      NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '代理白名单表';

ALTER TABLE xquark_commission ADD COLUMN offered TINYINT DEFAULT FALSE COMMENT '佣金是否发放';