alter table xquark_order_item
  add column `paid_point` decimal(10, 2) DEFAULT 0;
alter table xquark_order_item
  add column `paid_commission` decimal(10, 2) DEFAULT 0;
