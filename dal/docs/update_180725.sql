ALTER TABLE xquark_order ADD capable_to_refund tinyint(1) DEFAULT false NOT NULL COMMENT '是否能申请售后服务';
ALTER TABLE xquark_order ADD update_refund_entry_at datetime NULL COMMENT '更新售后入口的时间';