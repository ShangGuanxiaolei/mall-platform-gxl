/*
  红包雨 and 抽奖活动数据库脚本 modify 2019-01-28 23:26:43
 */

create table promotion_list
(
  id                 bigint primary key,
  state              int               default 0
  comment '0-未开始 1-进行中 2-已结束',
  trigger_start_time datetime not null
  comment '开始触发事件时间',
  trigger_end_time   datetime not null
  comment '结束触发事件时间',
  created_at         datetime null,
  updated_at         datetime null,
  archive            tinyint  not null default 0
  comment '逻辑删除 0-正常  1-已删除'
)
  comment '活动清单';

create table promotion_packet_rain
(
  id                  int auto_increment
    primary key,
  promotion_list_id   bigint              not null
  comment '对应的活动列表的id',
  promotion_date      datetime            not null
  comment '活动日期',
  point_total         int                 not null
  comment '德分总数',
  normal_limit        int                 not null
  comment '白人每日上限',
  vip_limit           int                 not null
  comment '非白人每日上限',
  created_at          datetime            null,
  updated_at          datetime            null,
  archive             tinyint default '0' not null
  comment '逻辑删除 0-正常  1-已删除',
  morning           varchar(20) default '11:00:00' not null
  comment '上午活动开始时间(格式必须为00:00:00)',
  afternoon         varchar(20) default '16:00:00' not null
  comment '下午活动开始时间(格式必须为00:00:00)',
  evening           varchar(20) default '22:00:00' not null
  comment '晚上活动开始时间(格式必须为00:00:00)',
  time              int default '1'                not null
  comment '每个时间段活动持续时间(单位小时)'
)
  comment '当天红包雨配置';

create index idx_promotion_packet_rain_promotion_list_id
  on promotion_packet_rain (promotion_list_id);


create table promotion_lottery
(
  id                 int                    auto_increment primary key,
  promotion_list_id  bigint        not null
  comment '对应的活动列表的id',
  promotion_date     datetime      not null
  comment '活动日期',
  first_score        int           not null
  comment '第一等级德分数',
  first_probability  double(16, 4) not null
  comment '第一等级中奖概率',
  second_score       int           not null
  comment '第二等级德分数',
  second_probability double(16, 4) not null
  comment '第二等级中奖概率',
  third_score        int           not null
  comment '第三等级德分数',
  third_probability  double(16, 4) not null
  comment '第三等级中奖概率',
  fourth_score        int         not null
  comment '第四等级德分数',
  fourth_probability  double(16, 4)  not null
  comment '第四等级中奖概率',
  first_rank         int           not null default 0
  comment '一等奖份数',
  second_rank        int           not null default 0
  comment '二等奖份数',
  third_rank         int           not null default 0
  comment '三等奖份数',
  created_at         datetime      null,
  updated_at         datetime      null,
  archive            tinyint       not null default 0
  comment '逻辑删除 0-正常  1-已删除'
)
  comment '当天抽奖配置';
create index idx_promotion_lottery_promotion_list_id
  on promotion_lottery (promotion_list_id);

create table winning_list
(
  id               int AUTO_INCREMENT                  not null
    primary key,
  biz_id           VARCHAR(64)                         not null
  comment '业务编号',
  winning_time     datetime                            not null
  comment '中奖时间',
  winning_cpid     bigint                              not null
  comment '中奖人cpid',
  winning_identity varchar(20)                         not null
  comment 'RS-没身份 NOT_RS-有身份',
  winning_rank     int                                 not null
  comment '中奖类型 1-一等奖 2-二等奖 3-三等奖',
  address_id       bigint                              null
  comment '中奖地址id',
  state            int default '0'                     not null
  comment '领取状态 0-未领取 1-已领取 2-已失效',
  created_at       datetime                            null,
  updated_at       datetime                            null,
  archive          tinyint default '0'                 not null
  comment '逻辑删除 0-正常  1-已删除'
)
  comment '中奖列表';

create index idx_promotion_winnig_list_address_id
  on winning_list (address_id);

create index idx_promotion_winnig_list_winning_cpid
  on winning_list (winning_cpid);
