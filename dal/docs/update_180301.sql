DROP TABLE IF EXISTS xquark_scrm_http_log;
CREATE TABLE xquark_scrm_http_log (
  `id`         BIGINT       NOT NULL AUTO_INCREMENT,
  `url`        VARCHAR(150) NOT NULL
  COMMENT '调用地址',
  `params`     TINYTEXT     NOT NULL
  COMMENT '调用参数',
  `is_success`  TINYINT      NOT NULL
  COMMENT '是否成功',
  `error`      VARCHAR(200)          DEFAULT ''
  COMMENT '错误信息',
  `result`     TINYTEXT              DEFAULT NULL
  COMMENT '返回结果',
  `created_at` DATETIME              DEFAULT NULL,
  `updated_at` DATETIME              DEFAULT NULL,
  `archive`    TINYINT(4)   NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT = '记录与scrm对接日志';
