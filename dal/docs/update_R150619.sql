ALTER TABLE `xquark_product`
ADD COLUMN `enable_desc` tinyint(1) NULL COMMENT '是否启用富文本';

CREATE TABLE `xquark_product_desc` (
  `id`  bigint(20) NOT NULL AUTO_INCREMENT ,
  `product_id` bigint(20) NOT NULL COMMENT '商品 id',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容，商品详情',
  `archive` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1逻辑删除',
  `created_at`  datetime NOT NULL ,
  `updated_at`  datetime NULL DEFAULT NULL ,
  PRIMARY KEY (`id`,`product_id`)
) 
