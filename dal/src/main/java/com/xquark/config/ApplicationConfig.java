package com.xquark.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
public class ApplicationConfig {

  /**
   * 线上环境的profile名称
   */
  public final static String PROFILE_NAME_PROD = "prod";

  public final static String PROFILE_NAME_PROD_SERVICE = "prodservice";

  public final static String PROFILE_NAME_PREPROD = "preprod";

  @Profile("dev")
  @Configuration
  @PropertySource("classpath:env/config-dev-hds.properties")
  static class AppDev {

  }

  @Profile("sit")
  @Configuration
  @PropertySource("classpath:env/config-sit-hds.properties")
  static class AppSit {

  }

  @Profile("uat")
  @Configuration
  @PropertySource("classpath:env/config-uat-hds.properties")
  static class AppUat {

  }

  @Profile("uat2")
  @Configuration
  @PropertySource("classpath:env/config-uat2-hds.properties")
  static class AppUat2 {

  }

  @Profile("test")
  @Configuration
  @PropertySource("classpath:env/config-test-hds.properties")
  static class AppTest {

  }

  @Profile("innertest")
  @Configuration
  @PropertySource("classpath:env/config-innertest-hds.properties")
  static class AppInnerTest {

  }

  @Profile("stage")
  @Configuration
  @PropertySource("classpath:env/config-stage-hds.properties")
  static class AppStage {

  }

  @Profile(PROFILE_NAME_PROD)
  @Configuration
  @PropertySource("classpath:env/config-prod-hds.properties")
  static class AppProd {

  }

  @Profile(PROFILE_NAME_PROD_SERVICE)
  @Configuration
  @PropertySource("classpath:env/config-prod-service-hds.properties")
  static class AppProdService {

  }
}