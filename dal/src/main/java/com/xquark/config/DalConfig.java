package com.xquark.config;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.XquarkOrderThirdNoerp;
import com.xquark.dal.mybatis.DomainTypeHandler;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.mybatis.ObjectRangeHandler;
import com.xquark.dal.mybatis.PromotionActionTypeHandler;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

@Configuration
@ImportResource("classpath:/META-INF/applicationContext-dal.xml")
public class DalConfig {

  @Value("classpath:config/MapperConfig.xml")
  Resource mybatisMapperConfig;

  @Autowired
  DataSource dataSource;

  @Autowired
  DataSource dataSource2;

  // 自动注入 UAT VIVILIFE 数据源
  @Autowired
  DataSource dataSource3;

  // 自动注入 UAT HDS 数据源
  @Autowired
  DataSource dataSource4;
  //DataSource dataSource2;
  // @Autowired
  // Environment env;

  @Bean
  public PromotionConfigMapper promotionConfigMapper() throws Exception {
    return newMapperFactoryBean(PromotionConfigMapper.class).getObject();
  }

  @Bean
  public PromotionBaseInfoMapper promotionBaseInfoMapper() throws Exception {
    return newMapperFactoryBean(PromotionBaseInfoMapper.class).getObject();
  }

  @Bean
  public PromotionSkusMapper promotionSkuMapper() throws Exception {
    return newMapperFactoryBean(PromotionSkusMapper.class).getObject();
  }

  @Bean
  public PromotionSkuGiftMapper promotionSkuGiftMapper() throws Exception {
    return newMapperFactoryBean(PromotionSkuGiftMapper.class).getObject();
  }

  @Bean
  public PromotionInviteCodeMapper promotionInviteCodeMapper() throws Exception {
    return newMapperFactoryBean(PromotionInviteCodeMapper.class).getObject();
  }

  @Bean
  public PromotionWhitelistMapper promotionWhitelistMapper() throws Exception {
    return newMapperFactoryBean(PromotionWhitelistMapper.class).getObject();
  }

  @Bean
  public PromotionListMapper PromotionListMapper() throws Exception {
    return newMapperFactoryBean(PromotionListMapper.class).getObject();
  }

  @Bean
  public ActivityPtOrderStatusMapper ptOrderStatusMapper() throws Exception {
    return newMapperFactoryBean(ActivityPtOrderStatusMapper.class).getObject();
  }

  @Bean
  public ActivityPtOutTImeMapper ptOutTImeMapper() throws Exception {
    return newMapperFactoryBean(ActivityPtOutTImeMapper.class).getObject();
  }

  @Bean
  public ActivityEndMapper endMapper() throws Exception {
    return newMapperFactoryBean(ActivityEndMapper.class).getObject();
  }

  @Bean
  public ActivityPtCancelMapper activityPtCancelMapper() throws Exception {
    return newMapperFactoryBean(ActivityPtCancelMapper.class).getObject();
  }

  @Bean
  public ActivityPushMapper activityPushMapper() throws Exception {
    return newMapperFactoryBean(ActivityPushMapper.class).getObject();
  }

  @Bean
  public ActivityPushDetailMapper activityPushDetailMapper() throws Exception {
    return newMapperFactoryBean(ActivityPushDetailMapper.class).getObject();
  }


  @Bean
  public AccountMapper accountMapper() throws Exception {
    return newMapperFactoryBean(AccountMapper.class).getObject();
  }

  @Bean
  public PromotionTranInfoMapper promotionTranInfoMapper() throws Exception {
    return newMapperFactoryBean(PromotionTranInfoMapper.class).getObject();
  }

  @Bean
  public CollagePushMessageMapper collagePushMessageMapper() throws Exception {
    return newMapperFactoryBean(CollagePushMessageMapper.class).getObject();
  }

  @Bean
  public CollageSmsMessageMapper collageSmsMessageMapper() throws Exception {
    return newMapperFactoryBean(CollageSmsMessageMapper.class).getObject();
  }

  @Bean
  public DealMapper dealMapper() throws Exception {
    return newMapperFactoryBean(DealMapper.class).getObject();
  }

  @Bean
  public DealLogMapper dealLogMapper() throws Exception {
    return newMapperFactoryBean(DealLogMapper.class).getObject();
  }

  @Bean
  public AddressMapper addressMapper() throws Exception {
    return newMapperFactoryBean(AddressMapper.class).getObject();
  }

  @Bean
  public CartItemMapper cartItemMapper() throws Exception {
    return newMapperFactoryBean(CartItemMapper.class).getObject();
  }

  @Bean
  public CashierItemMapper cashierItemMapper() throws Exception {
    return newMapperFactoryBean(CashierItemMapper.class).getObject();
  }

  @Bean
  public MainOrderMapper mainOrderMapper() throws Exception {
    return newMapperFactoryBean(MainOrderMapper.class).getObject();
  }

  @Bean
  public OrderMapper orderMapper() throws Exception {
    return newMapperFactoryBean(OrderMapper.class).getObject();
  }

  @Bean
  public OrderAddressMapper orderAddressMapper() throws Exception {
    return newMapperFactoryBean(OrderAddressMapper.class).getObject();
  }

  @Bean
  public OrderItemMapper orderItemMapper() throws Exception {
    return newMapperFactoryBean(OrderItemMapper.class).getObject();
  }

  @Bean
  public OrderItemCommentMapper orderItemCommentMapper() throws Exception {
    return newMapperFactoryBean(OrderItemCommentMapper.class).getObject();
  }

  @Bean
  public ActivityOrderMapper activityOrderMapper() throws Exception {
    return newMapperFactoryBean(ActivityOrderMapper.class).getObject();
  }

  @Bean
  public ProductMapper productMapper() throws Exception {
    return newMapperFactoryBean(ProductMapper.class).getObject();
  }

  @Bean
  public RegionMapper RegionMapper() throws Exception {
    return newMapperFactoryBean(RegionMapper.class).getObject();
  }

  @Bean
  public ShippingMapper shippingMapper() throws Exception {
    return newMapperFactoryBean(ShippingMapper.class).getObject();
  }

  @Bean
  public ActivityEndMapper activityEndMapper() throws Exception {
    return newMapperFactoryBean(ActivityEndMapper.class).getObject();
  }

  @Bean
  public StockCheckMapper stockCheckMapper() throws Exception {
    return newMapperFactoryBean(StockCheckMapper.class).getObject();
  }

  @Bean
  public RecordPromotionPgMemberInfoMapper recordPromotionPgMemberInfoMapper() throws Exception {
    return newMapperFactoryBean(RecordPromotionPgMemberInfoMapper.class).getObject();
  }

  @Bean
  public PromotionGoodsDetailMapper promotionGoodsDetailMapper() throws Exception {
    return newMapperFactoryBean(PromotionGoodsDetailMapper.class).getObject();
  }

  @Bean
  public PromotionPgMemberInfoMapper promotionPgMemberInfoMapper() throws Exception {
    return newMapperFactoryBean(PromotionPgMemberInfoMapper.class).getObject();
  }

  @Bean
  public RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper() throws Exception {
    return newMapperFactoryBean(RecordPromotionPgTranInfoMapper.class).getObject();
  }

  @Bean
  public CheckStatusAndTimesMapper checkStatusAndTimesMapper() throws Exception {
    return newMapperFactoryBean(CheckStatusAndTimesMapper.class).getObject();
  }

  @Bean
  public ActivityRecycleMapper activityRecycleMapper() throws Exception {
    return newMapperFactoryBean(ActivityRecycleMapper.class).getObject();
  }

  @Bean
  public StockInfoMapper stockInfoMapper() throws Exception {
    return newMapperFactoryBean(StockInfoMapper.class).getObject();
  }

  @Bean
  public PloyProductMapper ployProductMapper() throws Exception {
    return newMapperFactoryBean(PloyProductMapper.class).getObject();
  }

  @Bean
  public ThirdPartySupplierMapper thirdPartySupplierMapper() throws Exception {
    return newMapperFactoryBean(ThirdPartySupplierMapper.class).getObject();
  }

  @Bean
  public ProductDescMapper productDescMapper() throws Exception {
    return newMapperFactoryBean(ProductDescMapper.class).getObject();
  }

  @Bean
  public ShopMapper shopMapper() throws Exception {
    return newMapperFactoryBean(ShopMapper.class).getObject();
  }

  @Bean
  public CombinationMapper combinationMapper() throws Exception {
    return newMapperFactoryBean(CombinationMapper.class).getObject();
  }

  @Bean
  public CombinationItemMapper combinationItemMapper() throws Exception {
    return newMapperFactoryBean(CombinationItemMapper.class).getObject();
  }

  @Bean
  public FragmentMapper fragmentMapper() throws Exception {
    return newMapperFactoryBean(FragmentMapper.class).getObject();
  }

  @Bean
  public FragmentImageMapper fragmentImageMapper() throws Exception {
    return newMapperFactoryBean(FragmentImageMapper.class).getObject();
  }

  @Bean
  public ProductFragmentMapper productFragmentMapper() throws Exception {
    return newMapperFactoryBean(ProductFragmentMapper.class).getObject();
  }

  @Bean
  public SkuMapper skuMapper() throws Exception {
    return newMapperFactoryBean(SkuMapper.class).getObject();
  }

  @Bean
  public SkuMappingMapper skuMappingMapper() throws Exception {
    return newMapperFactoryBean(SkuMappingMapper.class).getObject();
  }

  @Bean
  public ZoneMapper zoneMapper() throws Exception {
    return newMapperFactoryBean(ZoneMapper.class).getObject();
  }

  @Bean
  public AppVersionMapper appVersionMapper() throws Exception {
    return newMapperFactoryBean(AppVersionMapper.class).getObject();
  }

  @Bean
  public UserMapper userMapper() throws Exception {
    return newMapperFactoryBean(UserMapper.class).getObject();
  }

  @Bean
  public UserAlipayMapper userAlipayMapper() throws Exception {
    return newMapperFactoryBean(UserAlipayMapper.class).getObject();
  }

  @Bean
  public ImageMapper imageMapper() throws Exception {
    return newMapperFactoryBean(ImageMapper.class).getObject();
  }

  @Bean
  public FeedbackMapper feedbackMapper() throws Exception {
    return newMapperFactoryBean(FeedbackMapper.class).getObject();
  }

  @Bean
  public PromotionCouponMapper promotionCouponMapper() throws Exception {
    return newMapperFactoryBean(PromotionCouponMapper.class).getObject();
  }

  @Bean
  public UserCouponMapper userCouponMapper() throws Exception {
    return newMapperFactoryBean(UserCouponMapper.class).getObject();
  }

  @Bean
  public TagMapper tagMapper() throws Exception {
    return newMapperFactoryBean(TagMapper.class).getObject();
  }

  @Bean
  public ProductTagMapper productTagMapper() throws Exception {
    return newMapperFactoryBean(ProductTagMapper.class).getObject();
  }

  @Bean
  public ProductImageMapper productImageMapper() throws Exception {
    return newMapperFactoryBean(ProductImageMapper.class).getObject();
  }

  @Bean
  public TinyUrlMapper tinyUrlMapper() throws Exception {
    return newMapperFactoryBean(TinyUrlMapper.class).getObject();
  }

  @Bean
  public PaymentMapper paymentMapper() throws Exception {
    return newMapperFactoryBean(PaymentMapper.class).getObject();
  }

  @Bean
  public ShopAccessMapper shopAccessMapper() throws Exception {
    return newMapperFactoryBean(ShopAccessMapper.class).getObject();
  }

  @Bean
  public AccessReportMapper accessReportMapper() throws Exception {
    return newMapperFactoryBean(AccessReportMapper.class).getObject();
  }

  @Bean
  public MessageMapper messageMapper() throws Exception {
    return newMapperFactoryBean(MessageMapper.class).getObject();
  }

  @Bean
  public UserMessageMapper userMessageMapper() throws Exception {
    return newMapperFactoryBean(UserMessageMapper.class).getObject();
  }

  @Bean
  public UserBankMapper userBankMapper() throws Exception {
    return newMapperFactoryBean(UserBankMapper.class).getObject();
  }

  @Bean
  public WithdrawApplyMapper withdrawApplyMapper() throws Exception {
    return newMapperFactoryBean(WithdrawApplyMapper.class).getObject();
  }

  @Bean
  public PushMessageMapper pushMessageMapper() throws Exception {
    return newMapperFactoryBean(PushMessageMapper.class).getObject();
  }

  @Bean
  public PersonnelMapper personnelMapper() throws Exception {
    return newMapperFactoryBean(PersonnelMapper.class).getObject();
  }

  @Bean
  public SmsMessageMapper smsMessageMapper() throws Exception {
    return newMapperFactoryBean(SmsMessageMapper.class).getObject();
  }

  @Bean
  public SubAccountLogMapper subAccountLogMapper() throws Exception {
    return newMapperFactoryBean(SubAccountLogMapper.class).getObject();
  }

  @Bean
  public SupplierMapper supplierMapper() throws Exception {
    return newMapperFactoryBean(SupplierMapper.class).getObject();
  }

  @Bean
  public SupplierWarehouseMapper supplierWareHouse() throws Exception {
    return newMapperFactoryBean(SupplierWarehouseMapper.class).getObject();
  }

  @Bean
  public SubAccountMapper subAccountMapper() throws Exception {
    return newMapperFactoryBean(SubAccountMapper.class).getObject();
  }

  @Bean
  public SubAccountSnapshotMapper subAccountSnapshotMapper() throws Exception {
    return newMapperFactoryBean(SubAccountSnapshotMapper.class).getObject();
  }

  @Bean
  public OutpayMapper outpayMapper() throws Exception {
    return newMapperFactoryBean(OutpayMapper.class).getObject();
  }

  @Bean
  public PayRequestMapper payRequestMapper() throws Exception {
    return newMapperFactoryBean(PayRequestMapper.class).getObject();
  }

  @Bean
  public CommissionRuleMapper commissionRuleMapper() throws Exception {
    return newMapperFactoryBean(CommissionRuleMapper.class).getObject();
  }

  @Bean
  public CommissionProductRuleConfigMapper commissionProductRuleConfigMapper() throws Exception {
    return newMapperFactoryBean(CommissionProductRuleConfigMapper.class).getObject();
  }

  @Bean
  public ActivityMapper activityMapper() throws Exception {
    return newMapperFactoryBean(ActivityMapper.class).getObject();
  }

  @Bean
  public CategoryMapper categoryMapper() throws Exception {
    return newMapperFactoryBean(CategoryMapper.class).getObject();
  }

  @Bean
  public CategoryActivityMapper categoryActivityMapper() throws Exception {
    return newMapperFactoryBean(CategoryActivityMapper.class).getObject();
  }

  @Bean
  public TermMapper termMapper() throws Exception {
    return newMapperFactoryBean(TermMapper.class).getObject();
  }

  @Bean
  public TermRelationshipMapper termRelationshipMapper() throws Exception {
    return newMapperFactoryBean(TermRelationshipMapper.class).getObject();
  }

  @Bean
  public PosterMapper posterMapper() throws Exception {
    return newMapperFactoryBean(PosterMapper.class).getObject();
  }

  @Bean
  public PayBankMapper payBankMapper() throws Exception {
    return newMapperFactoryBean(PayBankMapper.class).getObject();
  }

  @Bean
  public PosterTagMapper posterTagMapper() throws Exception {
    return newMapperFactoryBean(PosterTagMapper.class).getObject();
  }

  @Bean
  public ShopStyleMapper shopStyleMapper() throws Exception {
    return newMapperFactoryBean(ShopStyleMapper.class).getObject();
  }

  @Bean
  public OutpayAgreementMapper outpayAgreementMapper() throws Exception {
    return newMapperFactoryBean(OutpayAgreementMapper.class).getObject();
  }

  @Bean
  public CustomerServiceMapper customerServiceMapper() throws Exception {
    return newMapperFactoryBean(CustomerServiceMapper.class).getObject();
  }

  @Bean
  public PromotionTitleMapper promotionTitleMapper() throws Exception {
    return newMapperFactoryBean(PromotionTitleMapper.class).getObject();
  }

  @Bean
  public PromotionMapper promotionMapper() throws Exception {
    return newMapperFactoryBean(PromotionMapper.class).getObject();
  }

  @Bean
  public ProdSyncMapper prodSyncMapper() throws Exception {
    return newMapperFactoryBean(ProdSyncMapper.class).getObject();
  }

  @Bean
  public ApiVisitorLogMapper apiVisitorLogMapper() throws Exception {
    return newMapperFactoryBean(ApiVisitorLogMapper.class).getObject();
  }

  @Bean
  public UserSigninLogMapper userSigninLogMapper() throws Exception {
    return newMapperFactoryBean(UserSigninLogMapper.class).getObject();
  }

  @Bean
  public MerchantSigninLogMapper merchantSigninLogMapper() throws Exception {
    return newMapperFactoryBean(MerchantSigninLogMapper.class).getObject();
  }

  @Bean
  public CouponMapper couponMapper() throws Exception {
    return newMapperFactoryBean(CouponMapper.class).getObject();
  }

  @Bean
  public CouponActivityMapper couponActivityMapper() throws Exception {
    return newMapperFactoryBean(CouponActivityMapper.class).getObject();
  }

  @Bean
  public OrderMessageMapper orderMessageMapper() throws Exception {
    return newMapperFactoryBean(OrderMessageMapper.class).getObject();
  }

  @Bean
  public SmsSendRecordMapper smsSendRecordMapper() throws Exception {
    return newMapperFactoryBean(SmsSendRecordMapper.class).getObject();
  }

  @Bean
  public SmsTplMapper smsTplMapper() throws Exception {
    return newMapperFactoryBean(SmsTplMapper.class).getObject();
  }

  @Bean
  public SmsVarTplMapper smsVarTplMapper() throws Exception {
    return newMapperFactoryBean(SmsVarTplMapper.class).getObject();
  }

  @Bean
  public CampaignProductMapper campaignProductMapper() throws Exception {
    return newMapperFactoryBean(CampaignProductMapper.class).getObject();
  }

  @Bean
  public ActivityTicketMapper activityTicketMapper() throws Exception {
    return newMapperFactoryBean(ActivityTicketMapper.class).getObject();
  }

  @Bean
  public ActivitySPMapper activitySPMapper() throws Exception {
    return newMapperFactoryBean(ActivitySPMapper.class).getObject();
  }

  @Bean
  public PostAgeSetMapper postAgeSetMapper() throws Exception {
    return newMapperFactoryBean(PostAgeSetMapper.class).getObject();
  }

  @Bean
  public OrderRefundMapper orderRefundMapper() throws Exception {
    return newMapperFactoryBean(OrderRefundMapper.class).getObject();
  }

  @Bean
  public OrderRefundAttachMapper orderRefundAttachMapper() throws Exception {
    return newMapperFactoryBean(OrderRefundAttachMapper.class).getObject();
  }

  //	@Bean
  //    public HelperMessageMapper helperMessageMapper() throws Exception {
  //        return newMapperFactoryBean(HelperMessageMapper.class).getObject();
  //    }

  @Bean
  public OrderUserDeviceMapper orderUserDeviceMapper() throws Exception {
    return newMapperFactoryBean(OrderUserDeviceMapper.class).getObject();
  }

  @Bean
  public TestMapper testMapper() throws Exception {
    return newMapperFactoryBean(TestMapper.class).getObject();
  }

  private <T> MapperFactoryBean<T> newMapperFactoryBean(Class<T> clazz) throws Exception {
    final MapperFactoryBean<T> b = new MapperFactoryBean<>();
    b.setMapperInterface(clazz);
    b.setSqlSessionFactory(sqlSessionFactory());
    return b;
  }

  private <T> MapperFactoryBean<T> newMapperFactoryBean2(Class<T> clazz) throws Exception {
    final MapperFactoryBean<T> b = new MapperFactoryBean<>();
    b.setMapperInterface(clazz);
    b.setSqlSessionFactory(sqlSessionFactory2());
    return b;
  }

  private <T> MapperFactoryBean<T> newMapperFactoryBean3(Class<T> clazz) throws Exception {
    final MapperFactoryBean<T> b = new MapperFactoryBean<T>();
    b.setMapperInterface(clazz);
    b.setSqlSessionFactory(sqlSessionFactory3());
    return b;
  }

  private <T> MapperFactoryBean<T> newMapperFactoryBean4(Class<T> clazz) throws Exception {
    final MapperFactoryBean<T> b = new MapperFactoryBean<T>();
    b.setMapperInterface(clazz);
    b.setSqlSessionFactory(sqlSessionFactory4());
    return b;
  }

  @Bean
  public PromotionModelMapper promotionModelMapper() throws Exception {
    return newMapperFactoryBean(PromotionModelMapper.class).getObject();
  }

  @Bean
  public OrderRefundRecordMapper orderRefundRecordMapper() throws Exception {
    return newMapperFactoryBean(OrderRefundRecordMapper.class).getObject();
  }

  @Bean
  public DomainMapper domainMapper() throws Exception {
    return newMapperFactoryBean(DomainMapper.class).getObject();
  }

  @Bean
  public DistributionConfigMapper distributionConfigMapper() throws Exception {
    return newMapperFactoryBean(DistributionConfigMapper.class).getObject();
  }

  @Bean
  public DomainLoginStrategyMapper domainLoginStrategyMapper() throws Exception {
    return newMapperFactoryBean(DomainLoginStrategyMapper.class).getObject();
  }

  @Bean
  public PaymentMerchantMapper paymentMerchantMapper() throws Exception {
    return newMapperFactoryBean(PaymentMerchantMapper.class).getObject();
  }

  @Bean
  public ActivityProductMapper activityProductMapper() throws Exception {
    return newMapperFactoryBean(ActivityProductMapper.class).getObject();
  }

  @Bean
  public WeixinJSTicketMapper weixinJSTicketMapper() throws Exception {
    return newMapperFactoryBean(WeixinJSTicketMapper.class).getObject();
  }

  @Bean
  public WechatAppConfigMapper wechatAppConfigMapper() throws Exception {
    return newMapperFactoryBean(WechatAppConfigMapper.class).getObject();
  }

  @Bean
  public WechatCustomizedMenuMapper wechatCustomizedMenuMapper() throws Exception {
    return newMapperFactoryBean(WechatCustomizedMenuMapper.class).getObject();
  }

  @Bean
  public WechatTemplateMessageMapper wechatTemplateMessageMapper() throws Exception {
    return newMapperFactoryBean(WechatTemplateMessageMapper.class).getObject();
  }

  @Bean
  public WechatAutoReplyMapper wechatAutoReplyMapper() throws Exception {
    return newMapperFactoryBean(WechatAutoReplyMapper.class).getObject();
  }

  @Bean
  public MerchantMapper merchantMapper() throws Exception {
    return newMapperFactoryBean(MerchantMapper.class).getObject();
  }

  @Bean
  public MemberMapper memberMapper() throws Exception {
    return newMapperFactoryBean(MemberMapper.class).getObject();
  }

  @Bean
  public MemberActionRecordMapper memberActionRecordMapper() throws Exception {
    return newMapperFactoryBean(MemberActionRecordMapper.class).getObject();
  }

  @Bean
  public MerchantRoleMapper merchantRoleMapper() throws Exception {
    return newMapperFactoryBean(MerchantRoleMapper.class).getObject();
  }

  @Bean
  public MerchantPermissionMapper merchantPermissionMapper() throws Exception {
    return newMapperFactoryBean(MerchantPermissionMapper.class).getObject();
  }

  @Bean
  public ShopTreeMapper shopTreeMapper() throws Exception {
    return newMapperFactoryBean(ShopTreeMapper.class).getObject();
  }

  @Bean
  public ShopWechatSettingMapper shopWechatSettingMapper() throws Exception {
    return newMapperFactoryBean(ShopWechatSettingMapper.class).getObject();
  }

  @Bean
  public TwitterProductCommissionMapper twitterProductCommissionMapper() throws Exception {
    return newMapperFactoryBean(TwitterProductCommissionMapper.class).getObject();
  }

  @Bean
  public TwitterShopCommissionMapper twitterShopCommissionMapper() throws Exception {
    return newMapperFactoryBean(TwitterShopCommissionMapper.class).getObject();
  }

  @Bean
  public UpdatePgMemberInfoMapper updatePgMemberInfoMapper() throws Exception {
    return newMapperFactoryBean(UpdatePgMemberInfoMapper.class).getObject();
  }

  @Bean
  public UpdatePgTranInfoMapper updatePgTranInfoMapper() throws Exception {
    return newMapperFactoryBean(UpdatePgTranInfoMapper.class).getObject();
  }

  //	@Bean
  //	public CommissionAreaPlanMapper commissionAreaPlanMapper() throws Exception {
  //		return newMapperFactoryBean(CommissionAreaPlanMapper.class).getObject();
  //	}
  //
  //	@Bean
  //	public  CommissionOrderPartnerMapper commissionOrderPartnerMapper() throws Exception {
  //		return newMapperFactoryBean(CommissionOrderPartnerMapper.class).getObject();
  //	}
  //
  //	@Bean
  //	public CommissionTeamPlanMapper commissionTeamPlanMapper() throws Exception {
  //		return newMapperFactoryBean(CommissionTeamPlanMapper.class).getObject();
  //	}
  //
  //	@Bean
  //	public CommissionPartnerPlanMapper commissionPartnerPlanMapper() throws Exception {
  //		return newMapperFactoryBean(CommissionPartnerPlanMapper.class).getObject();
  //	}
  //
  //	@Bean
  //	public  CommissionCustomAreaPlanMapper commissionCustomAreaPlanMapper() throws Exception {
  //		return newMapperFactoryBean(CommissionCustomAreaPlanMapper.class).getObject();
  //	}

  @Bean
  public UserCardMapper userCardMapper() throws Exception {
    return newMapperFactoryBean(UserCardMapper.class).getObject();
  }

  @Bean
  public UserFamilyMapper userFamilyMapper() throws Exception {
    return newMapperFactoryBean(UserFamilyMapper.class).getObject();
  }

  @Bean
  public FamilyCardSettingMapper familyCardSettingMapper() throws Exception {
    return newMapperFactoryBean(FamilyCardSettingMapper.class).getObject();
  }

  @Bean
  public FamilySettingMapper familySettingMapper() throws Exception {
    return newMapperFactoryBean(FamilySettingMapper.class).getObject();
  }

  @Bean
  public PartnerSettingMapper partnerSettingMapper() throws Exception {
    return newMapperFactoryBean(PartnerSettingMapper.class).getObject();
  }

  @Bean
  public UserPartnerMapper userPartnerMapper() throws Exception {
    return newMapperFactoryBean(UserPartnerMapper.class).getObject();
  }

  @Bean
  public UserTwitterMapper userTwitterMapper() throws Exception {
    return newMapperFactoryBean(UserTwitterMapper.class).getObject();
  }

  @Bean
  public ShopTreeChangeApplicationMapper shopTreeChangeApplicationMapper() throws Exception {
    return newMapperFactoryBean(ShopTreeChangeApplicationMapper.class).getObject();
  }

  @Bean
  public MallMapper mallMapper() throws Exception {
    return newMapperFactoryBean(MallMapper.class).getObject();
  }

  @Bean
  public PromotionGrouponMapper promotionGrouponMapper() throws Exception {
    return newMapperFactoryBean(PromotionGrouponMapper.class).getObject();
  }

  @Bean
  public ActivityGrouponMapper activityGrouponMapper() throws Exception {
    return newMapperFactoryBean(ActivityGrouponMapper.class).getObject();
  }

  @Bean
  public ActivityGrouponDetailMapper activityGrouponDetailMapper() throws Exception {
    return newMapperFactoryBean(ActivityGrouponDetailMapper.class).getObject();
  }

  @Bean
  public OrderItemPromotionMapper orderItemPromotionMapper() throws Exception {
    return newMapperFactoryBean(OrderItemPromotionMapper.class).getObject();
  }

  @Bean
  public MarketingTweetMapper marketingTweetMapper() throws Exception {
    return newMapperFactoryBean(MarketingTweetMapper.class).getObject();
  }

  @Bean
  public ViewComponentMapper viewComponentMapper() throws Exception {
    return newMapperFactoryBean(ViewComponentMapper.class).getObject();
  }

  @Bean
  public ViewPageComponentMapper viewPageComponentMapper() throws Exception {
    return newMapperFactoryBean(ViewPageComponentMapper.class).getObject();
  }

  @Bean
  public ViewPageMapper viewPageMapper() throws Exception {
    return newMapperFactoryBean(ViewPageMapper.class).getObject();
  }

  @Bean
  public TweetImageMapper tweetImageMapper() throws Exception {
    return newMapperFactoryBean(TweetImageMapper.class).getObject();
  }

  @Bean
  public PromotionDiamondMapper promotionDiamondMapper() throws Exception {
    return newMapperFactoryBean(PromotionDiamondMapper.class).getObject();
  }

  @Bean
  public ProductDistributorMapper productDistributorMapper() throws Exception {
    return newMapperFactoryBean(ProductDistributorMapper.class).getObject();
  }

  @Bean
  public HomeItemMapper homeItemMapper() throws Exception {
    return newMapperFactoryBean(HomeItemMapper.class).getObject();
  }

  @Bean
  public UserAgentMapper userAgentMapper() throws Exception {
    return newMapperFactoryBean(UserAgentMapper.class).getObject();
  }

  @Bean
  public RoleMapper roleMapper() throws Exception {
    return newMapperFactoryBean(RoleMapper.class).getObject();
  }

  @Bean
  public RolePriceMapper rolePriceMapper() throws Exception {
    return newMapperFactoryBean(RolePriceMapper.class).getObject();
  }

  @Bean
  public AuditRuleMapper auditRuleMapper() throws Exception {
    return newMapperFactoryBean(AuditRuleMapper.class).getObject();
  }

  @Bean
  public VersionMapper versionMapper() throws Exception {
    return newMapperFactoryBean(VersionMapper.class).getObject();
  }

  @Bean
  public OrgMapper orgMapper() throws Exception {
    return newMapperFactoryBean(OrgMapper.class).getObject();
  }

  @Bean
  public ModuleMapper moduleMapper() throws Exception {
    return newMapperFactoryBean(ModuleMapper.class).getObject();
  }

  @Bean
  public RoleFunctionMapper roleFunctionMapper() throws Exception {
    return newMapperFactoryBean(RoleFunctionMapper.class).getObject();
  }

  @Bean
  public FunctionMapper functionMapper() throws Exception {
    return newMapperFactoryBean(FunctionMapper.class).getObject();
  }

  @Bean
  public YundouOperationMapper yundouOperationMapper() throws Exception {
    return newMapperFactoryBean(YundouOperationMapper.class).getObject();
  }

  @Bean
  public YundouRuleMapper yundouRuleMapper() throws Exception {
    return newMapperFactoryBean(YundouRuleMapper.class).getObject();
  }

  @Bean
  public YundouOperationDetailMapper yundouOperationDetailMapper() throws Exception {
    return newMapperFactoryBean(YundouOperationDetailMapper.class).getObject();
  }

  @Bean
  public UserAgentWhiteListMapper userAgentWhiteListMapper() throws Exception {
    return newMapperFactoryBean(UserAgentWhiteListMapper.class).getObject();
  }

  @Bean
  public BonusMapper bonusMapper() throws Exception {
    return newMapperFactoryBean(BonusMapper.class).getObject();
  }

  @Bean
  public AntifakeMapper antifakeMapper() throws Exception {
    return newMapperFactoryBean(AntifakeMapper.class).getObject();
  }

  @Bean
  public MemberCardMapper memberCardMapper() throws Exception {
    return newMapperFactoryBean(MemberCardMapper.class).getObject();
  }

  @Bean
  public TradeMapper tradeMapper() throws Exception {
    return newMapperFactoryBean(TradeMapper.class).getObject();
  }

  @Bean
  public TwitterLevelMapper twitterLevelMapper() throws Exception {
    return newMapperFactoryBean(TwitterLevelMapper.class).getObject();
  }

  @Bean
  public TwitterLevelRelationMapper twitterLevelRelationMapper() throws Exception {
    return newMapperFactoryBean(TwitterLevelRelationMapper.class).getObject();
  }

  @Bean
  public HealthTestModuleMapper healthTestModuleMapper() throws Exception {
    return newMapperFactoryBean(HealthTestModuleMapper.class).getObject();
  }

  @Bean
  public HealthTestQuestionMapper healthTestQuestionMapper() throws Exception {
    return newMapperFactoryBean(HealthTestQuestionMapper.class).getObject();
  }

  @Bean
  public HealthTestAnswerGroupMapper healthTestAnswerGroupMapper() throws Exception {
    return newMapperFactoryBean(HealthTestAnswerGroupMapper.class).getObject();
  }

  @Bean
  public HealthTestAnswerMapper healthTestAnswerMapper() throws Exception {
    return newMapperFactoryBean(HealthTestAnswerMapper.class).getObject();
  }

  @Bean
  public HealthTestResultMapper healthTestResultMapper() throws Exception {
    return newMapperFactoryBean(HealthTestResultMapper.class).getObject();
  }

  @Bean
  public HealthTestPhysiqueMapper healthTestPhysiqueMapper() throws Exception {
    return newMapperFactoryBean(HealthTestPhysiqueMapper.class).getObject();
  }

  @Bean
  public HealthTestProfileMapper healthTestProfileMapper() throws Exception {
    return newMapperFactoryBean(HealthTestProfileMapper.class).getObject();
  }

  @Bean
  public HealthTestResultProductMapper healthTestResultProductMapper() throws Exception {
    return newMapperFactoryBean(HealthTestResultProductMapper.class).getObject();
  }

  @Bean
  public PartnerShopCommissionMapper partnerShopCommissionMapper() throws Exception {
    return newMapperFactoryBean(PartnerShopCommissionMapper.class).getObject();
  }

  @Bean
  public PartnerProductCommissionMapper partnerProductCommissionMapper() throws Exception {
    return newMapperFactoryBean(PartnerProductCommissionMapper.class).getObject();
  }

  @Bean
  public PartnerProductCommissionDeMapper partnerProductCommissionDeMapper() throws Exception {
    return newMapperFactoryBean(PartnerProductCommissionDeMapper.class).getObject();
  }

  @Bean
  public PartnerTypeMapper partnerTypeMapper() throws Exception {
    return newMapperFactoryBean(PartnerTypeMapper.class).getObject();
  }

  @Bean
  public PartnerTypeRelationMapper partnerTypeRelationMapper() throws Exception {
    return newMapperFactoryBean(PartnerTypeRelationMapper.class).getObject();
  }

  @Bean
  public TeamShopCommissionMapper teamShopCommissionMapper() throws Exception {
    return newMapperFactoryBean(TeamShopCommissionMapper.class).getObject();
  }

  @Bean
  public TeamShopCommissionDeMapper teamShopCommissionDeMapper() throws Exception {
    return newMapperFactoryBean(TeamShopCommissionDeMapper.class).getObject();
  }

  @Bean
  public TeamProductCommissionMapper teamProductCommissionMapper() throws Exception {
    return newMapperFactoryBean(TeamProductCommissionMapper.class).getObject();
  }

  @Bean
  public TeamMapper teamMapper() throws Exception {
    return newMapperFactoryBean(TeamMapper.class).getObject();
  }

  @Bean
  public UserTeamMapper userTeamMapper() throws Exception {
    return newMapperFactoryBean(UserTeamMapper.class).getObject();
  }

  @Bean
  public YundouSettingMapper yundouSettingMapper() throws Exception {
    return newMapperFactoryBean(YundouSettingMapper.class).getObject();
  }

  @Bean
  public YundouProductMapper yundouProductMapper() throws Exception {
    return newMapperFactoryBean(YundouProductMapper.class).getObject();
  }

  @Bean
  public YundouOrderMapper yundouOrderMapper() throws Exception {
    return newMapperFactoryBean(YundouOrderMapper.class).getObject();
  }

  @Bean
  public ProductTopMapper productTopMapper() throws Exception {
    return newMapperFactoryBean(ProductTopMapper.class).getObject();
  }

  @Bean
  public CarouselMapper carouselMapper() throws Exception {
    return newMapperFactoryBean(CarouselMapper.class).getObject();
  }

  @Bean
  public CarouselImageMapper carouselImageMapper() throws Exception {
    return newMapperFactoryBean(CarouselImageMapper.class).getObject();
  }

  @Bean
  public StoreMapper storeMapper() throws Exception {
    return newMapperFactoryBean(StoreMapper.class).getObject();
  }

  @Bean
  public StoreMemberMapper storeMemberMapper() throws Exception {
    return newMapperFactoryBean(StoreMemberMapper.class).getObject();
  }

  @Bean
  public TeamApplyMapper teamApplyMapper() throws Exception {
    return newMapperFactoryBean(TeamApplyMapper.class).getObject();
  }

  @Bean
  public ShopAccessLogMapper shopAccessLogMapper() throws Exception {
    return newMapperFactoryBean(ShopAccessLogMapper.class).getObject();
  }

  @Bean
  public ProductCollectionMapper productCollectionMapper() throws Exception {
    return newMapperFactoryBean(ProductCollectionMapper.class).getObject();
  }

  @Bean
  public ProductBlackListMapper productBlackListMapper() throws Exception {
    return newMapperFactoryBean(ProductBlackListMapper.class).getObject();
  }

  @Bean
  public SignInMapper signInMapper() throws Exception {
    return newMapperFactoryBean(SignInMapper.class).getObject();
  }

  @Bean
  public PromotionFullReduceDiscountMapper promotionFullReduceDiscountMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullReduceDiscountMapper.class).getObject();
  }

  @Bean
  public PromotionFullCutMapper promotionFullCutMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullCutMapper.class).getObject();
  }

  @Bean
  public PromotionFullPiecesMapper promotionFullPiecesMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullPiecesMapper.class).getObject();
  }

  @Bean
  public PromotionFullCutDiscountMapper promotionFullCutDiscountMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullCutDiscountMapper.class).getObject();
  }

  @Bean
  public PromotionFullPiecesDiscountMapper promotionFullPiecesDiscountMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullPiecesDiscountMapper.class).getObject();
  }

  @Bean
  public PromotionFullReduceOrderMapper promotionFullReduceOrderMapper() throws Exception {
    return newMapperFactoryBean(PromotionFullReduceOrderMapper.class).getObject();
  }

  @Bean
  public TwitterAccessLogMapper twitterAccessLogMapper() throws Exception {
    return newMapperFactoryBean(TwitterAccessLogMapper.class).getObject();
  }

  @Bean
  public FanCardApplyMapper fanCardApplyMapper() throws Exception {
    return newMapperFactoryBean(FanCardApplyMapper.class).getObject();
  }

  @Bean
  public CommentMapper commentMapper() throws Exception {
    return newMapperFactoryBean(CommentMapper.class).getObject();
  }

  @Bean
  public CommentReplyMapper commentReplyMapper() throws Exception {
    return newMapperFactoryBean(CommentReplyMapper.class).getObject();
  }

  @Bean
  public CommentLikeMapper commentLikeMapper() throws Exception {
    return newMapperFactoryBean(CommentLikeMapper.class).getObject();
  }

  @Bean
  public TagsMapper tagsMapper() throws Exception {
    return newMapperFactoryBean(TagsMapper.class).getObject();
  }

  @Bean
  public ProductTagsMapper productTagsMapper() throws Exception {
    return newMapperFactoryBean(ProductTagsMapper.class).getObject();
  }

  @Bean
  public PromotionPreOrderMapper promotionPreOrderMapper() throws Exception {
    return newMapperFactoryBean(PromotionPreOrderMapper.class).getObject();
  }

  @Bean
  public PromotionFlashSaleMapper promotionFlashSaleMapper() throws Exception {
    return newMapperFactoryBean(PromotionFlashSaleMapper.class).getObject();
  }

  @Bean
  public SkuAttributeMapper skuAttributeMapper() throws Exception {
    return newMapperFactoryBean(SkuAttributeMapper.class).getObject();
  }

  @Bean
  public SkuAttributeItemMapper skuAttributeItemMapper() throws Exception {
    return newMapperFactoryBean(SkuAttributeItemMapper.class).getObject();
  }

  @Bean
  public SignInRuleMapper signInRuleMapper() throws Exception {
    return newMapperFactoryBean(SignInRuleMapper.class).getObject();
  }

  @Bean
  public UserNotificationMapper userNotificationMapper() throws Exception {
    return newMapperFactoryBean(UserNotificationMapper.class).getObject();
  }

  @Bean
  public PromotionApplyMapper promotionApplyMapper() throws Exception {
    return newMapperFactoryBean(PromotionApplyMapper.class).getObject();
  }

  @Bean
  public HelperMapper helperMapper() throws Exception {
    return newMapperFactoryBean(HelperMapper.class).getObject();
  }

  @Bean
  public FeedbackTagsMapper feedbackTagsMapper() throws Exception {
    return newMapperFactoryBean(FeedbackTagsMapper.class).getObject();
  }

  @Bean
  public PrivilegeCodeMapper privilegeCodeMapper() throws Exception {
    return newMapperFactoryBean(PrivilegeCodeMapper.class).getObject();
  }

  @Bean
  public PrivilegeCodeUserMapper privilegeCodeUserMapper() throws Exception {
    return newMapperFactoryBean(PrivilegeCodeUserMapper.class).getObject();
  }

  @Bean
  public PrivilegeProductMapper privilegeProductMapper() throws Exception {
    return newMapperFactoryBean(PrivilegeProductMapper.class).getObject();
  }

  @Bean
  public PrivilegeCodeOrderMapper privilegeCodeOrderMapper() throws Exception {
    return newMapperFactoryBean(PrivilegeCodeOrderMapper.class).getObject();
  }

  @Bean
  public ProductUserMapper productUserMapper() throws Exception {
    return newMapperFactoryBean(ProductUserMapper.class).getObject();
  }

  @Bean
  public ProductFilterMapper productFilterMapper() throws Exception {
    return newMapperFactoryBean(ProductFilterMapper.class).getObject();
  }

  @Bean
  public SCrmHttpLogMapper sCrmHttpLogMapper() throws Exception {
    return newMapperFactoryBean(SCrmHttpLogMapper.class).getObject();
  }

  @Bean
  public MemberPromotionMapper memberPromotionMapper() throws Exception {
    return newMapperFactoryBean(MemberPromotionMapper.class).getObject();
  }

  @Bean
  public InsideEmployeeMapper insideEmployeeMapper() throws Exception {
    return newMapperFactoryBean(InsideEmployeeMapper.class).getObject();
  }

  @Bean
  public InsideUserEmployeeMapper insideUserEmployeeMapper() throws Exception {
    return newMapperFactoryBean(InsideUserEmployeeMapper.class).getObject();
  }

  @Bean
  public SequenceGeneratorMapper sequenceGeneratorMapper() throws Exception {
    return newMapperFactoryBean(SequenceGeneratorMapper.class).getObject();
  }

  @Bean
  public CustomerluckylistMapper customerluckylistMapper() throws Exception {
    return newMapperFactoryBean(CustomerluckylistMapper.class).getObject();
  }

  @Bean
  public PersonalBillMapper personalBillMapper() throws Exception {
    return newMapperFactoryBean(PersonalBillMapper.class).getObject();
  }

  @Bean
  public CompanyBillMapper companyBillMapper() throws Exception {
    return newMapperFactoryBean(CompanyBillMapper.class).getObject();
  }

  @Bean
  public AddedTaxBillMapper addedTaxBillMapper() throws Exception {
    return newMapperFactoryBean(AddedTaxBillMapper.class).getObject();
  }

  @Bean
  public ElectronicBillResultMapper electronicBillResultMapper() throws Exception {
    return newMapperFactoryBean(ElectronicBillResultMapper.class).getObject();
  }

  @Bean
  public BrandMapper brandMapper() throws Exception {
    return newMapperFactoryBean(BrandMapper.class).getObject();
  }

  @Bean
  public BrandProductMapper brandProductMapper() throws Exception {
    return newMapperFactoryBean(BrandProductMapper.class).getObject();
  }

  @Bean
  public SfAppConfigMapper sfAppConfigMapper() throws Exception {
    return newMapperFactoryBean(SfAppConfigMapper.class).getObject();
  }

  @Bean
  public SfProductLibraryMapper sfProductLibraryMapper() throws Exception {
    return newMapperFactoryBean(SfProductLibraryMapper.class).getObject();
  }

  @Bean
  public CustomerCareerLevelMapper customerCareerLevelMapper() throws Exception {
    return newMapperFactoryBean(CustomerCareerLevelMapper.class).getObject();
  }

  @Bean
  public CustomerCareerLevelAuditMapper customerCareerLevelAuditMapper() throws Exception {
    return newMapperFactoryBean(CustomerCareerLevelAuditMapper.class).getObject();
  }

  @Bean
  public CustomerMessageMapper customerMessageMapper() throws Exception {
    return newMapperFactoryBean(CustomerMessageMapper.class).getObject();
  }

  @Bean
  public CommissionMapper commissionMapper() throws Exception {
    return newMapperFactoryBean(CommissionMapper.class).getObject();
  }

  @Bean
  public WmsExpressMapper WmsExpressMapper() throws Exception {
    return newMapperFactoryBean2(WmsExpressMapper.class).getObject();
  }

  @Bean
  public WmsInventoryMapper WmsInventoryMapper() throws Exception {
    return newMapperFactoryBean2(WmsInventoryMapper.class).getObject();
  }

  @Bean
  public WmsOrderMapper WmsOrderMapper() throws Exception {
    return newMapperFactoryBean2(WmsOrderMapper.class).getObject();
  }

  @Bean
  public WmsOrderNtsProductMapper WmsOrderNtsProductMapper() throws Exception {
    return newMapperFactoryBean2(WmsOrderNtsProductMapper.class).getObject();
  }

  @Bean
  public WmsOrderPackageMapper WmsOrderPackageMapper() throws Exception {
    return newMapperFactoryBean2(WmsOrderPackageMapper.class).getObject();
  }

  @Bean
  public WmsProductMapper WmsProductMapper() throws Exception {
    return newMapperFactoryBean2(WmsProductMapper.class).getObject();
  }

  @Bean
  public WmsBatchMapper WmsBatchMapper() throws Exception {
    return newMapperFactoryBean2(WmsBatchMapper.class).getObject();
  }

  @Bean
  public PackageMapper PackageMapper() throws Exception {
    return newMapperFactoryBean(PackageMapper.class).getObject();
  }

  @Bean
  public CustomerProfileMapper CustomerProfileMapper() throws Exception {
    return newMapperFactoryBean(CustomerProfileMapper.class).getObject();
  }

  @Bean
  public CustomerProfileAuditMapper CustomerProfileAuditMapper() throws Exception {
    return newMapperFactoryBean(CustomerProfileAuditMapper.class).getObject();
  }

  @Bean
  public CustomerWechartUnionMapper CustomerWechartUnionMapper() throws Exception {
    return newMapperFactoryBean(CustomerWechartUnionMapper.class).getObject();
  }

  @Bean
  public WareHouseMapper WareHouseMapper() throws Exception {
    return newMapperFactoryBean(WareHouseMapper.class).getObject();
  }

  @Bean
  public MerchantSupplierMapper merchantSupplierMapper() throws Exception {
    return newMapperFactoryBean(MerchantSupplierMapper.class).getObject();
  }

  @Bean
  public PolyapiOrderVOMapper PolyapiOrderVOMapper() throws Exception {
    return newMapperFactoryBean(PolyapiOrderVOMapper.class).getObject();
  }

  @Bean
  public OrderSendMapper OrderSendMapper() throws Exception {
    return newMapperFactoryBean(OrderSendMapper.class).getObject();
  }

  @Bean
  public OrderDetailMapper OrderDetailMapper() throws Exception {
    return newMapperFactoryBean(OrderDetailMapper.class).getObject();
  }

  @Bean
  public OrderHeaderMapper OrderHeaderMapper() throws Exception {
    return newMapperFactoryBean(OrderHeaderMapper.class).getObject();
  }

  @Bean
  public MessageNotifyMapper messageNotifyMapper() throws Exception {
    return newMapperFactoryBean(MessageNotifyMapper.class).getObject();
  }

  @Bean
  public QueryOpenIdByCpidMapper queryOpenIdByCpidMapper() throws Exception {
    return newMapperFactoryBean(QueryOpenIdByCpidMapper.class).getObject();
  }

  @Bean
  public JobSchedulerLogMapper jobSchedulerLogMapper() throws Exception {
    return newMapperFactoryBean(JobSchedulerLogMapper.class).getObject();
  }

  @Bean
  public JugleIsGroupMasterMapper jugleIsGroupMasterMapper() throws Exception {
    return newMapperFactoryBean(JugleIsGroupMasterMapper.class).getObject();
  }

  @Bean
  public SystemRegionMapper SystemRegionMapper() throws Exception {
    return newMapperFactoryBean(SystemRegionMapper.class).getObject();
  }

  @Bean
  public CustomerAddressMapper CustomerAddressMapper() throws Exception {
    return newMapperFactoryBean(CustomerAddressMapper.class).getObject();
  }

  @Bean
  public ConsumerToCustomerMapper consumerToCustomerMapper() throws Exception {
    return newMapperFactoryBean(ConsumerToCustomerMapper.class).getObject();
  }

  @Bean
  public ConsumerToConsumerMapper consumerToConsumerMapper() throws Exception {
    return newMapperFactoryBean(ConsumerToConsumerMapper.class).getObject();
  }

  @Bean
  public PointBalanceMapper pointBalanceMapper() throws Exception {
    return newMapperFactoryBean(PointBalanceMapper.class).getObject();
  }

  @Bean
  public PromotionGoodsMapper promotionGoodsMapper() throws Exception {
    return newMapperFactoryBean(PromotionGoodsMapper.class).getObject();
  }

  @Bean
  public FileHeaderMapper fileHeaderMapper() throws Exception {
    return newMapperFactoryBean(FileHeaderMapper.class).getObject();
  }

  @Bean
  public CustomerBankMapper customerBankMapper() throws Exception {
    return newMapperFactoryBean(CustomerBankMapper.class).getObject();
  }

  @Bean
  public CustomerBankAuditMapper customerBankAuditMapper() throws Exception {
    return newMapperFactoryBean(CustomerBankAuditMapper.class).getObject();
  }

  @Bean
  public TimeSettingsMapper timeSettingsMapper() throws Exception {
    return newMapperFactoryBean(TimeSettingsMapper.class).getObject();
  }

  @Bean
  public SkuCombineMapper skuCombineMapper() throws Exception {
    return newMapperFactoryBean(SkuCombineMapper.class).getObject();
  }

  @Bean
  public SkuCombineExtraMapper skuCombineExtraMapper() throws Exception {
    return newMapperFactoryBean(SkuCombineExtraMapper.class).getObject();
  }

  @Bean
  public PromotionOrderDetailMapper promotionOrderDetailMapper() throws Exception {
    return newMapperFactoryBean(PromotionOrderDetailMapper.class).getObject();
  }

  @Bean
  public MsgListMapper msgListMapper() throws Exception {
    return newMapperFactoryBean(MsgListMapper.class).getObject();
  }

  @Bean
  public PromotionPgDetailMapper PromotionPgDetailMapper() throws Exception {
    return newMapperFactoryBean(PromotionPgDetailMapper.class).getObject();
  }

  @Bean
  public PromotionPgMapper PromotionPgMapper() throws Exception {
    return newMapperFactoryBean(PromotionPgMapper.class).getObject();
  }

  @Bean
  public PromotionGroupDetailMapper PromotionGroupDetailMapper() throws Exception {
    return newMapperFactoryBean(PromotionGroupDetailMapper.class).getObject();
  }

  @Bean
  public PromotionRewardsMapper promotionRewardsMapper() throws Exception {
    return newMapperFactoryBean(PromotionRewardsMapper.class).getObject();
  }

  @Bean
  public PoscodeMapper poscodeMapper() throws Exception {
    return newMapperFactoryBean(PoscodeMapper.class).getObject();
  }

  @Bean
  public LogisticsOrderMapper logisticsOrderMapper() throws Exception {
    return newMapperFactoryBean(LogisticsOrderMapper.class).getObject();
  }

  @Bean
  public LogisticsOrderSendMapper logisticsOrderSendMapper() throws Exception {
    return newMapperFactoryBean(LogisticsOrderSendMapper.class).getObject();
  }

  @Bean
  public LogisticsProductAndStockMapper logisticsProductAndStockMapper() throws Exception {
    return newMapperFactoryBean(LogisticsProductAndStockMapper.class).getObject();
  }

  @Bean
  public SqlSessionFactory sqlSessionFactory() throws Exception {
    final SqlSessionFactoryBean fb = new SqlSessionFactoryBean();
    fb.setConfigLocation(mybatisMapperConfig);
    fb.setDataSource(dataSource);
    // env.acceptsProfiles("prod") ? IdTypeHandler.class : IdTypeNullHandler.class
    fb.setTypeAliases(
        new Class<?>[] {
          IdTypeHandler.class,
          ObjectRangeHandler.class,
          PromotionActionTypeHandler.class,
          DomainTypeHandler.class
        });

    return fb.getObject();
  }

  @Bean
  public SqlSessionFactory sqlSessionFactory2() throws Exception {
    final SqlSessionFactoryBean fb = new SqlSessionFactoryBean();
    fb.setConfigLocation(mybatisMapperConfig);
    fb.setDataSource(dataSource2);
    fb.setTypeAliases(new Class<?>[]{IdTypeHandler.class, ObjectRangeHandler.class,
        PromotionActionTypeHandler.class, DomainTypeHandler.class});

    return fb.getObject();
  }

  /**
   *
   * @return
   * @throws Exception
   */
  @Bean
  public SqlSessionFactory sqlSessionFactory3() throws Exception {
    final SqlSessionFactoryBean fb = new SqlSessionFactoryBean();
    fb.setConfigLocation(mybatisMapperConfig);
    fb.setDataSource(dataSource3);
    fb.setTypeAliases(new Class<?>[]{IdTypeHandler.class, ObjectRangeHandler.class,
            PromotionActionTypeHandler.class, DomainTypeHandler.class});

    return fb.getObject();
  }


  @Bean
  public SqlSessionFactory sqlSessionFactory4() throws Exception {
    final SqlSessionFactoryBean fb = new SqlSessionFactoryBean();
    fb.setConfigLocation(mybatisMapperConfig);
    fb.setDataSource(dataSource4);
    // env.acceptsProfiles("prod") ? IdTypeHandler.class : IdTypeNullHandler.class
    fb.setTypeAliases(new Class<?>[]{IdTypeHandler.class, ObjectRangeHandler.class,
            PromotionActionTypeHandler.class, DomainTypeHandler.class});

    return fb.getObject();
  }

  @Bean
  public PromotionVipsuitMapper promotionVipsuitMapper() throws Exception {
    return newMapperFactoryBean(PromotionVipsuitMapper.class).getObject();
  }

  @Bean
  public PromotionTempStockMapper PromotionTempStockMapper() throws Exception {
    return newMapperFactoryBean(PromotionTempStockMapper.class).getObject();
  }

  @Bean
  public PromotionExclusiveMapper promotionExclusiveMapper() throws Exception {
    return newMapperFactoryBean(PromotionExclusiveMapper.class).getObject();
  }

  @Bean
  public PacketWhitelistMapper packetWhitelistMapper() throws Exception {
    return newMapperFactoryBean(PacketWhitelistMapper.class).getObject();
  }

  @Bean
  public FootprintMapper footprintMapper() throws Exception {
    return newMapperFactoryBean(FootprintMapper.class).getObject();
  }

  @Bean
  public PointGiftMapper pointGiftMapper() throws Exception {
    return newMapperFactoryBean(PointGiftMapper.class).getObject();
  }

  @Bean
  public PointGiftRecordMapper pointGiftRecordMapper() throws Exception {
    return newMapperFactoryBean(PointGiftRecordMapper.class).getObject();
  }

  @Bean
  public PointGiftDetailMapper pointGiftDetailMapper() throws Exception {
    return newMapperFactoryBean(PointGiftDetailMapper.class).getObject();
  }

  @Bean
  public WeakLinkMapper weakLinkMapper() throws Exception {
    return newMapperFactoryBean(WeakLinkMapper.class).getObject();
  }

  @Bean
  public CustomerModifyLinkDetailMapper customerModifyLinkDetailMapper() throws Exception {
    return newMapperFactoryBean(CustomerModifyLinkDetailMapper.class).getObject();
  }

  @Bean
  public CustomerQuestionMapper customerQuestionMapper() throws Exception {
    return newMapperFactoryBean(CustomerQuestionMapper.class).getObject();
  }

  @Bean
  public PromotionWhiteMapper promotionWhiteMapper() throws Exception {
    return newMapperFactoryBean(PromotionWhiteMapper.class).getObject();
  }

  @Bean
  public ServMessageMapper servMessageMapper() throws Exception {
    return newMapperFactoryBean(ServMessageMapper.class).getObject();
  }

  @Bean
  public PaymentSwitchConfigMapper paymentSwitchConfigMapper() throws Exception {
    return newMapperFactoryBean(PaymentSwitchConfigMapper.class).getObject();
  }

  @Bean
  public XquarkProductMapper xquarkProductMapper() throws Exception {
    return newMapperFactoryBean(XquarkProductMapper.class).getObject();
  }

  @Bean
  public WinningMapper winningMapper() throws Exception {
    return newMapperFactoryBean(WinningMapper.class).getObject();
  }

  @Bean
  public PromotionPacketRainMapper promotionPacketRainMapper() throws Exception {
    return newMapperFactoryBean(PromotionPacketRainMapper.class).getObject();
  }

  @Bean
  public PromotionLotteryMapper promotionLotteryMapper() throws Exception {
    return newMapperFactoryBean(PromotionLotteryMapper.class).getObject();
  }

  @Bean
  public PromotionLotteryProbabilityMapper promotionLotteryProbabilityMapper() throws Exception {
    return newMapperFactoryBean(PromotionLotteryProbabilityMapper.class).getObject();
  }

  @Bean
  public PromotionPacketRainTimeMapper promotionPacketRainTimeMapper() throws Exception {
    return newMapperFactoryBean(PromotionPacketRainTimeMapper.class).getObject();
  }

  @Bean
  public OfReportMapper ofReportMapper() throws Exception {
    return newMapperFactoryBean(OfReportMapper.class).getObject();
  }

  @Bean
  public IndexIconbarMapper indexIconbarMapper() throws Exception {
    return newMapperFactoryBean(IndexIconbarMapper.class).getObject();
  }

  @Bean
  public FreshmanRuleMapper freshmanRuleMapper() throws Exception {
    return newMapperFactoryBean(FreshmanRuleMapper.class).getObject();
  }

  @Bean
  public FreshmanLotteryMapper freshmanLotteryMapper() throws Exception {
    return newMapperFactoryBean(FreshmanLotteryMapper.class).getObject();
  }

  @Bean
  public FreshmanTrackMapper freshmanTrackMapper() throws Exception {
    return newMapperFactoryBean(FreshmanTrackMapper.class).getObject();
  }

  @Bean
  public FreshManProductMapper freshManProductMapper() throws Exception {
    return newMapperFactoryBean(FreshManProductMapper.class).getObject();
  }

  @Bean
  public FreshManConfigMapper freshManConfigMapper() throws Exception {
    return newMapperFactoryBean(FreshManConfigMapper.class).getObject();
  }

  @Bean
  public FreshManBannerMapper freshManBannerMapper() throws Exception {
    return newMapperFactoryBean(FreshManBannerMapper.class).getObject();
  }

  @Bean
  public FreshManAreatTabMapper freshManAreatTabMapper() throws Exception {
    return newMapperFactoryBean(FreshManAreatTabMapper.class).getObject();
  }

  @Bean
  public FirstOrderMapper firstOrderMapper() throws Exception {
    return newMapperFactoryBean(FirstOrderMapper.class).getObject();
  }

  @Bean
  public FreshmanRecommendMapper freshmanRecommendMapper() throws Exception {
    return newMapperFactoryBean(FreshmanRecommendMapper.class).getObject();
  }

  @Bean
  public FreshManFlagMapper freshManFlagMapper() throws Exception {
    return newMapperFactoryBean(FreshManFlagMapper.class).getObject();
  }

  @Bean
  public FreshManModuleMapper freshManModuleMapper() throws Exception {
    return newMapperFactoryBean(FreshManModuleMapper.class).getObject();
  }

  @Bean
  public FreshManUpGradeMapper freshManUpGradeMapper() throws Exception {
    return newMapperFactoryBean(FreshManUpGradeMapper.class).getObject();
  }

  @Bean
  public PromotionToConserveMapper promotionToConserveMapper() throws Exception {
    return newMapperFactoryBean(PromotionToConserveMapper.class).getObject();
  }

  @Bean
  public PromotionToVipMapper promotionToVipMapper() throws Exception {
    return newMapperFactoryBean(PromotionToVipMapper.class).getObject();
  }

  @Bean
	public RecommendMapper recommendMapper() throws Exception {
		return newMapperFactoryBean(RecommendMapper.class).getObject();
	}
  @Bean
  public GrandSaleKvMapper grandSaleKvMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleKvMapper.class).getObject();
  }

  @Bean
  public GrandSaleModuleMapper grandSaleModuleMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleModuleMapper.class).getObject();
  }

  @Bean
  public GrandSalePromotionMapper grandSalePromotionMapper() throws Exception {
    return newMapperFactoryBean(GrandSalePromotionMapper.class).getObject();
  }

  @Bean
  public GrandSaleTimelineMapper grandSaleTimelineMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleTimelineMapper.class).getObject();
  }

  @Bean
  public GrandSaleStadiumMapper grandSaleStadiumMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleStadiumMapper.class).getObject();
  }

  @Bean
  public GrandSaleStadiumTypeMapper grandSaleStadiumTypeMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleStadiumTypeMapper.class).getObject();
  }

  @Bean
  public GrandSaleShareMapper grandSaleShareMapper() throws Exception {
    return newMapperFactoryBean(GrandSaleShareMapper.class).getObject();
  }

  @Bean
  public MemberInfoMapper memberInfoMapper() throws Exception {
    return newMapperFactoryBean(MemberInfoMapper.class).getObject();
  }

  @Bean
  public PromotionPgPriceMapper promotionPgPriceMapper() throws Exception {
    return newMapperFactoryBean(PromotionPgPriceMapper.class).getObject();
  }

  @Bean
  public PromotionPgMasterPriceMapper promotionPgMasterPriceMapper() throws Exception {
    return newMapperFactoryBean(PromotionPgMasterPriceMapper.class).getObject();
  }

  @Bean
  public IndexBannerMapper indexBannerMapper() throws Exception {
    return newMapperFactoryBean(IndexBannerMapper.class).getObject();
  }

  @Bean
  public PayNoLogMapper payNoLogMapper() throws Exception {
      return newMapperFactoryBean(PayNoLogMapper.class).getObject();
  }


  @Bean
  public CustomerProfileHDSMapper customerProfileHDSMapper() throws Exception {
    return newMapperFactoryBean4(CustomerProfileHDSMapper.class).getObject();
  }

  @Bean
  public CustomerProfileVvlifeMapper customerProfileVvlifeMapper() throws Exception {
    return newMapperFactoryBean3(CustomerProfileVvlifeMapper.class).getObject();
  }
  @Bean
  public UserLogMapper userLogMapper() throws Exception {
    return newMapperFactoryBean(UserLogMapper.class).getObject();
  }
  @Bean
  public PromotionBargainMapper promotionBargainMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainMapper.class).getObject();
  }

  @Bean
  public PromotionBargainDetailMapper promotionBargainDetailMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainDetailMapper.class).getObject();
  }

  @Bean
  public PromotionBargainRuleMapper promotionBargainRuleMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainRuleMapper.class).getObject();
  }

  @Bean
  public PromotionBargainProductMapper promotionBargainProductMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainProductMapper.class).getObject();
  }

  @Bean
  public PromotionBargainOrderMapper promotionBargainOrderMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainOrderMapper.class).getObject();
  }

  @Bean
  public PromotionBargainHistoryMapper promotionBargainHistoryMapper() throws Exception {
    return newMapperFactoryBean(PromotionBargainHistoryMapper.class).getObject();
  }

  @Bean
  public PromotionReserveMapper promotionReserveMapper() throws Exception {
    return newMapperFactoryBean(PromotionReserveMapper.class).getObject();
  }

  @Bean
  public OrderIdentityLogMapper orderIdentityLogMapper() throws Exception {
    return newMapperFactoryBean(OrderIdentityLogMapper.class).getObject();
  }

  @Bean
  public XquarkOrderThirdErpMapper xquarkOrderThirdErpMapper() throws  Exception{
    return newMapperFactoryBean(XquarkOrderThirdErpMapper.class).getObject();

  }
  @Bean
  public XquarkOrderThirdCheckMapper xquarkOrderThirdCheckMapper() throws  Exception{

    return newMapperFactoryBean(XquarkOrderThirdCheckMapper.class).getObject();
  }
  @Bean
  public  XquarkOrderThirdInfoDetailMapper xquarkOrderThirdInfoDetailMapper() throws Exception{

    return newMapperFactoryBean(XquarkOrderThirdInfoDetailMapper.class)  .getObject();
  }
  @Bean
  public XquarkOrderThirdInfoMapper xquarkOrderThirdInfoMapper() throws Exception{

    return  newMapperFactoryBean(XquarkOrderThirdInfoMapper.class).getObject();
  }
  @Bean
  public XquarkOrderThirdNoerpMapper xquarkOrderThirdNoerpMapper() throws Exception{
    return  newMapperFactoryBean(XquarkOrderThirdNoerpMapper.class).getObject();
  }


  @Bean
  public SystemConfigMapper systemConfigMapper() throws Exception {
    return newMapperFactoryBean(SystemConfigMapper.class).getObject();
  }
}
