package com.xquark.dal.page;

import java.util.List;
import java.util.Map;

/**
 * 分页类
 *
 * @author huxaya
 */
public class PageHelper<T> implements java.io.Serializable {

  private static final long serialVersionUID = 1L;
  private int page = 0;// 当前页
  private int pageCount;//总条数
  private int pageSize = 10;// 每页显示记录数
  private int startNo;
  private int totalPage;//总页数
  private String sort;// 排序字段
  private String order;// asc/desc
  private List<T> result;//要在页面上显示的信息
  //附加字段
  private Map<String, String> commentMap;

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public List<T> getResult() {
    return result;
  }

  public void setResult(List<T> result) {
    this.result = result;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public String getSort() {
    return sort;
  }

  public int getStartNo() {
    this.startNo = this.getPage() * this.getPageSize();
    return startNo;
  }

  public void setStartNo(int startNo) {
    this.startNo = startNo;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }
  public int getPageCount() {
    return pageCount;
  }

  public void setPageCount(int pageCount) {
    this.pageCount = pageCount;
  }


  public Map<String, String> getCommentMap() {
    return commentMap;
  }

  public void setCommentMap(Map<String, String> commentMap) {
    this.commentMap = commentMap;
  }
}
