package com.xquark.dal.voex;

import com.google.common.base.Optional;
import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.vo.OrderFeeVO;
import com.xquark.dal.vo.OrderVO;

import java.math.BigDecimal;
import java.util.List;

public class OrderVOEx extends OrderVO {

    private static final long serialVersionUID = 1L;

    List<OrderFeeVO> orderFees;

    boolean hadGift;

    Integer giftCount;

    private PieceGroupVO pieceGroup;

    public OrderVOEx(OrderVO order, List<OrderFeeVO> fees) {
        super(order);
        this.orderFees = fees;
    }

    private List<DiscountDetailVO> discountDetailStr;

    public List<DiscountDetailVO> getDiscountDetailStr() {
        return discountDetailStr;
    }

    public void setDiscountDetailStr(List<DiscountDetailVO> discountDetailStr) {
        this.discountDetailStr = discountDetailStr;
    }

    public List<OrderFeeVO> getOrderFees() {
        return orderFees;
    }

    public void setOrderFees(List<OrderFeeVO> orderFees) {
        this.orderFees = orderFees;
    }

    public boolean isHadGift() {
        return hadGift;
    }

    public void setHadGift(boolean hadGift) {
        this.hadGift = hadGift;
    }

    public Integer getGiftCount() {
        return giftCount;
    }

    public void setGiftCount(Integer giftCount) {
        this.giftCount = giftCount;
    }

    public PieceGroupVO getPieceGroup() {
        return pieceGroup;
    }

    public void setPieceGroup(PieceGroupVO pieceGroup) {
        this.pieceGroup = pieceGroup;
    }

    /**
     * 除去积分、德分、立减的折扣外, 活动的优惠价
     */
    public BigDecimal getPromotionDiscount() {
        BigDecimal pointDiscount = Optional.fromNullable(getPaidPoint())
                .or(BigDecimal.ZERO).divide(PointConstants.POINT_SCALA, 2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal commissionDiscount = Optional.fromNullable(getPaidCommission())
                .or(BigDecimal.ZERO).divide(PointConstants.COMMISSION_SCALA, 2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal cutDown = Optional.fromNullable(getReduction()).or(BigDecimal.ZERO);
        return getDiscountFee().subtract(pointDiscount)
                .subtract(commissionDiscount)
                .subtract(cutDown);
    }

    /**
     * 展示时取reduction
     */
    @Override
    public BigDecimal getCutDown() {
        return getReduction();
    }
}
