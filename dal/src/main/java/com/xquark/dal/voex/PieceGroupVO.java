package com.xquark.dal.voex;

import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.status.ExecStatus;
import com.xquark.dal.vo.PromotionGroupDetailVO;

import java.util.List;

/**
 * @author wangxinhua.
 * @date 2018/11/4
 */
public class PieceGroupVO {

  private final String tranCode;

  private final String pCode;

  private final int memberCount;

  private final int beginCount;

  private final ExecStatus groupStatus;

  private final List<PromotionGroupDetailVO> memberInfo;

  private final Integer isGroupHead;

  private final boolean isCutLine;

  public Integer getIsGroupHead() {
    return isGroupHead;
  }

  public PieceGroupVO(PromotionOrderDetail detail,
      int memberCount, int beginCount, ExecStatus groupStatus,
      List<PromotionGroupDetailVO> memberInfo, Integer isGroupHead) {
    this.memberCount = memberCount;
    this.beginCount = beginCount;
    this.groupStatus = groupStatus;
    if (detail != null) {
      this.tranCode = detail.getPiece_group_tran_code();
      this.pCode = detail.getP_code();
      this.isCutLine = detail.getIs_cut_line();
    } else {
      this.tranCode = "";
      this.pCode = "";
      this.isCutLine = false;
    }
    this.memberInfo = memberInfo;
    this.isGroupHead = isGroupHead;
  }

  public List<PromotionGroupDetailVO> getMemberInfo() {
    return memberInfo;
  }

  public String getTranCode() {
    return tranCode;
  }

  public String getpCode() {
    return pCode;
  }

  public int getMemberCount() {
    return memberCount;
  }

  public ExecStatus getGroupStatus() {
    return groupStatus;
  }

  public int getBeginCount() {
    return beginCount;
  }

  public boolean getIsCutLine() {
    return isCutLine;
  }
}
