package com.xquark.dal.voex;

import com.google.common.base.Optional;
import com.xquark.dal.type.PromotionType;
import com.xquark.utils.functional.MapLike;

import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 */
public class DiscountDetailVO implements MapLike<PromotionType, BigDecimal>, Comparable<DiscountDetailVO> {

    private PromotionType discountType;

    private BigDecimal discountFee;

    public PromotionType getDiscountType() {
        return discountType;
    }

    public DiscountDetailVO() {
    }

    public DiscountDetailVO(PromotionType discountType, BigDecimal discountFee) {
        this.discountType = discountType;
        this.discountFee = discountFee;
    }

    public String getDiscountFee() {
        BigDecimal discountFee = Optional.fromNullable(this.discountFee)
                .or(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        String prefix = (discountType != PromotionType.LOGISTICS ? "- " : "") + "¥";
        return prefix + discountFee.abs();
    }

    public String getDiscountStr() {
        return discountType == PromotionType.RESERVE ?"活动优惠":discountType.getcName();
    }

    public void setDiscountType(PromotionType discountType) {
        this.discountType = discountType;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    @Override
    public void setKey(PromotionType promotionType) {
        this.setDiscountType(promotionType);
    }

    @Override
    public void setVal(BigDecimal bigDecimal) {
        this.setDiscountFee(bigDecimal);
    }

    @Override
    public int compareTo(DiscountDetailVO o) {
        if (o == null) {
            return -1;
        }
        return this.getDiscountType().getSortNum().compareTo(o.getDiscountType().getSortNum());
    }
}
