package com.xquark.dal.status;

public enum WithdrawApplyStatus {
  NEW, SUCCESS, CLOSE, PENDING, FAILED
}
