package com.xquark.dal.status;

public enum ActivityGrouponStatus {
  CREATED, // 进行中
  PAID, // 已付款  未发货
  REFUNDING, // 退款申请中
  SHIPPED, // 已发货
  SUCCESS, // 交易成功,已收货
  CLOSED // 未成团
}
