package com.xquark.dal.status;

public enum MessageStatus {
  VALID, BLOCKED
}
