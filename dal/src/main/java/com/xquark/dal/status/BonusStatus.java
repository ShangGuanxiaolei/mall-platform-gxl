package com.xquark.dal.status;

/**
 * 分红状态
 *
 * @author chh 2017-6-7
 */
public enum BonusStatus {
  NEW, //新增
  SUCCESS //已发放
}
