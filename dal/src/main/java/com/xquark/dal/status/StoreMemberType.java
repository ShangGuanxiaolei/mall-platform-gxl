package com.xquark.dal.status;

/**
 * 线下门店人员类型
 *
 * @author chh 2017-07-12
 */
public enum StoreMemberType {
  LEADER, // 店长
  SENIOR, // 特约店员
  MEMBER // 普通店员
}
