package com.xquark.dal.status;

/**
 * Created by wangxinhua on 17-7-25. DESC:
 */
public enum PromotionStatus {

  NORMAL(0, ""),
  END(1, "活动已结束"),
  SELL_OUT(2, "活动商品已抢完"),
  NOT_IN_PROMOTION(3, "不在活动中"),
  UNDER_STOCKS(4, "库存不足"),
  IN_STOCK(5, "商品已下架"),
  NOT_EXISTS(6, "活动不存在"),
  APPLIED(7, "您已参与过该活动，不能重复参与"),
  REJECTED(7, "您的购买申请未通过，请联系客服"),
  CANT_PAY_NOW(8, "预购商品还未开放购买，请耐心等待"),
  NOT_AUDIT_YET(9, "您的购买申请还未审核，请耐心等待");

  PromotionStatus(int code, String message) {
    this.code = code;
    this.message = message;
  }

  private int code;

  private String message;

  public String getMessage() {
    return message;
  }

  public int getCode() {
    return code;
  }
}
