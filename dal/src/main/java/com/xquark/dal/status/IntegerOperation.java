package com.xquark.dal.status;

/**
 * Created by wangxinhua on 17-11-24. DESC: {@code Integer} 类型的符号运算枚举
 */
public enum IntegerOperation implements Operation<Integer> {

  PLUS("+") {
    @Override
    public Integer apply(Integer t1, Integer t2) {
      return t1 + t2;
    }

  },
  MINUS("-") {
    @Override
    public Integer apply(Integer t1, Integer t2) {
      return t1 - t2;
    }
  },
  MULTIPLY("*") {
    @Override
    public Integer apply(Integer t1, Integer t2) {
      return t1 * t2;
    }
  };

  private final String symbol;

  IntegerOperation(String symbol) {
    this.symbol = symbol;
  }

  @Override
  public String getSymbol() {
    return this.symbol;
  }
}
