package com.xquark.dal.status;

/**
 * Created by wangxinhua on 17-11-24. DESC:
 */
public enum LongOperation implements Operation<Long> {

  PLUS("+") {
    @Override
    public Long apply(Long t1, Long t2) {
      return t1 + t2;
    }

  },
  MINUS("-") {
    @Override
    public Long apply(Long t1, Long t2) {
      return t1 - t2;
    }
  },
  MULTIPLY("*") {
    @Override
    public Long apply(Long t1, Long t2) {
      return t1 * t2;
    }
  };

  private final String symbol;

  LongOperation(String symbol) {
    this.symbol = symbol;
  }

  @Override
  public String getSymbol() {
    return null;
  }
}
