package com.xquark.dal.status;

/**
 * 轮播图类型，首页，其他页面等
 *
 * @author chh 2017-07-10
 */
public enum CarouselType {
  HOME, //首页
  HOME_COUPON, //首页优惠券
  HOME_ACTIVITY //首页活动
}
