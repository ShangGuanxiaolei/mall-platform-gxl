package com.xquark.dal.status;

/**
 * app模类型
 *
 * @author chh 2017-04-01
 */
public enum AppType {
  IOS, // IOS
  ANDROID // ANDROID
}
