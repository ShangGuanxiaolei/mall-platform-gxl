package com.xquark.dal.status;

public enum ServMessageStatus {

    /**
     * 拼团成功
     */
    SUCCESSPIECECONTENT("您参加的拼团活动已组团成功，订单号ESO，我们会尽快安排发货。"),

    /**
     * 拼团失败
     */
    FAILPIECECONTENT("您参加的拼团活动未成团，订单号ESO。"),

    /**
     * 退款申请通过(已收到货)
     */
    REFUNDAPPLICATIONPASS("【汉薇商城】您的订单ESO申请售后审核已通过，退货需知\n" +
            "1、退货商品需产品完好，仍具销售价值，不影响二次销售\n" +
            "2、汉薇商城目前仅接受整单退货，需将订单中所有产品如数寄回\n" +
            "3、包装箱内要含购物清单，包括订货人信息、订单号、订货及退货产品信息（原则上所退产品应该与清单上完全一致）\n" +
            "4、退货申请人发货后必须在汉薇APP我的订单中填写退货的运单号，以方便仓库收货时准确完成收货信息的系统录入\n" +
            "5、所有寄回仓库的货品都由退货人预先支付运费，仓库不接受到付的退货\n" +
            "6、如果收到退货单据及相关信息不全，公司将不受理该笔退货\n" +
            "7、退货仓库收货人及地址：\n" +
            "收货人：葛俊平\n" +
            "联系电话：021-39950003\n" +
            "地址：上海市嘉定区嘉定工业园区兴邦路333号无忧物流"),


    /**
     * 未到货，同意退款
     */
    REFUNDAPPLICATIONPASSFAIL("【汉薇商城】您的订单ESO申请售后审核已通过，公司将尽快退款，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     * 退款申请拒绝
     */
    REFUNDAPPLICATIONFAIL("【汉薇商城】您的订单ESO申请售后审核未通过，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     * 退款完成
     */
    REFUNDSUCCESS("【汉薇商城】您的订单ESO申请售后已完成，感谢您的支持，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     * 换货申请通过(收到货物)
     */
    REPLACEAPPLICATIONPASS("【汉薇商城】您的订单ESO申请售后审核已通过，换货须知：\n" +
            "1、换货的商品需如数寄回\n" +
            "2、包装箱内要含购物清单，包括订货人信息、订单号、订货及换货产品信息（原则上所换产品应该与清单上完全一致）\n" +
            "3、换货申请人发货后必须在汉薇APP我的订单中填写退货的运单号，以方便仓库收货时准确完成收货信息的系统录入，逾期未填写，售后申请将被撤销。\n" +
            "4、所有寄回仓库的货品都由换货人预先支付运费，仓库不接受到付的换货\n" +
            "5、如果收到换货单据及相关信息不全，公司将不受理该笔换货\n" +
            "6、换货仓库收货人及地址：\n" +
            "收货人：葛俊平\n" +
            "联系电话：021-39950003\n" +
            "地址：上海市嘉定区嘉定工业园区兴邦路333号无忧物流"),


    /**
     * 换货申请通过（不需要入库）
     */
    REPLACEAPPLICATIONPASSFAIL("【汉薇商城】您的订单ESO申请售后审核已通过，公司将尽快发货，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     *补货申请通过
     */
    REISSUEPASS("【汉薇商城】您的订单ESO申请售后审核已通过，公司将尽快补发，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     * 换货申请拒绝
     */
    REPLACEAPPLICATIONFAIL("【汉薇商城】您的订单ESO申请售后审核未通过，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     *补货申请拒绝
     */
    REISSUEFAIL("【汉薇商城】您的订单ESO申请售后审核未通过，如有任何问题可在工作日联系售后服务热线4000186266"),

    /**
     * 换货出库
     */
    REPLACEOUT("【汉薇商城】您的订单ESO申请售后已完成，换货商品已出库，感谢您的支持，如有任何问题可在工作日联系售后服务热线4000186266"),




    /**
     * 消息类型
     */
    TYPEMSG("SUCCESS"),

    /**
     * 消息未读
     */
    STATUS("0"),

    /**
     * 服务消息
     */
    TYPESYSTEM("SYSTEM"),
    /**
     * 系统消息
     */
    TYPESERVICE("SERVICE");


    public String getWord(String orderNo) {
        String replaceWord ="";
        if(word.contains("ESO")){

            replaceWord= word.replace("ESO", orderNo);
        }
        return replaceWord;
    }
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    private String word;
    private String orderNo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    ServMessageStatus(String word) {
        this.word = word;
    }
}
