package com.xquark.dal.status;

public enum CouponStatus {
  VALID, // 可使用
  OVERDUE, // 已过期
  LOCKED, //已有订单使用该优惠券
  USED, // 已使用
  CLOSED //关闭
}
