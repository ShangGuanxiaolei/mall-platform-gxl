package com.xquark.dal.status;

/**
 * 规则类型(订单审核或代理审核规则)
 *
 * @author chh 2017-02-27
 */
public enum AuditRuleType {
  ORDER, //订单审核
  AGENT //代理审核
}
