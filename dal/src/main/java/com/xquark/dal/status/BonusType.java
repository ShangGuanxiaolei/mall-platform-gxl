package com.xquark.dal.status;

/**
 * 分红类型
 *
 * @author chh 2017-6-7
 */
public enum BonusType {
  MONTH, //月度分红
  YEAR //年度分红
}
