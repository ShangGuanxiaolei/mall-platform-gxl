package com.xquark.dal.status;


public enum PartnerStatus {

  ACTIVE, // 已生效
  APPLY // 申请中
}
