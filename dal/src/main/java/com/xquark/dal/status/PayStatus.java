package com.xquark.dal.status;

public enum PayStatus {
  SUCCESS,
  FAIL,
  PAID
}
