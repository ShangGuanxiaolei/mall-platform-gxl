package com.xquark.dal.status;

/**
 * 代理审核类型（由上级审核或平台审核等）
 *
 * @author chh 2016-12-23
 */
public enum AuditType {
  PARENT, //上级
  PLATFORM, //平台
  ALL //上级平台均能审核
}
