package com.xquark.dal.status;

public enum BargainOrderStatus {

    /**
     *已提交 ，未付款
     */
    SUBMITTED,
    /**
     * 已付款
     */
    PAID

}
