package com.xquark.dal.status;

/**
 * Created by wangxinhua on 17-7-6. DESC:
 */
public enum QuestionType {
  SINGLE_CHOICE, // 单选
  MULTIPLE_CHOICE, // 多选
  BLANK_FILLING, // 填空
  TRUE_FALSE // 判断
}
