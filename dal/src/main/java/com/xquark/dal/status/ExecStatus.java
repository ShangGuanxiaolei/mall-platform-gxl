package com.xquark.dal.status;

/**
 * @author wangxinhua
 * @since 18-11-3
 * 通用表示执行状态
 */
public enum ExecStatus {

  /**
   * 状态初始化
   */
  NORMAL,

  /**
   * 成功
   */
  SUCCESS,

  /**
   * 失败
   */
  FAILED,

}
