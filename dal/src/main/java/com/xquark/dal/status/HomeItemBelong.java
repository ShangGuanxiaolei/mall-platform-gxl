package com.xquark.dal.status;

public enum HomeItemBelong {
  B2C, // B2C用户
  B2B, // B2B用户
  DEFAULT // 默认
}
