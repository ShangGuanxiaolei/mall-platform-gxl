package com.xquark.dal.status;

import java.util.HashMap;
import java.util.Map;

public enum WinningRank {

	ILLEGAL(0,"非法参数"),

	FIRST(1,"一等奖","Ya6302"),

	SECOND(2,"二等奖","Aa0104"),

	THIRD(3,"三等奖","Ab0201");

	private static final Map<Integer, WinningRank> WINNING_RANK_MAP;
	static {
		WINNING_RANK_MAP = new HashMap<>();
		for (WinningRank rank : values()) {
			WINNING_RANK_MAP.put(rank.value,rank);
		}
	}

	public static String getSkuCode(int index) {
		for (WinningRank state : values()) {
			if (state.value == index) {
				return state.skuCode;
			}
		}
		return null;
	}

	private WinningRank rank;

	private int value;

	private String message;

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	private String skuCode;

	public int getWinningRank() {
		return winningRank;
	}

	public void setWinningRank(int winningRank) {
		this.winningRank = winningRank;
	}

	private int winningRank;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	WinningRank(int value, String message,String skuCode){
		this.skuCode = skuCode;
		this.value = value;
		this.message = message;
	}

	WinningRank(int value, String message){
		this.value = value;
		this.message = message;
	}

	public static WinningRank getEnum(int index) {
		for (WinningRank state : values()) {
			if (state.getValue()== index) {
				return state;
			}
		}
		return null;
	}
}
