package com.xquark.dal.status;

public enum CommissionStatus {
  NEW,
  SUCCESS,
  CLOSED
}
