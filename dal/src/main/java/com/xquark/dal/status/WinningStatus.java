package com.xquark.dal.status;

import java.util.HashMap;
import java.util.Map;

public enum WinningStatus {
  UNCLAIMED(0, "未领取"),

  CLAIMED(1, "已领取"),

  INVALID(2, "已失效"),

  ILLEGAL(3, "非法参数");

  private static final Map<Integer, WinningStatus> WINNING_STATUS_MAP;

  static {
    WINNING_STATUS_MAP = new HashMap<>();
    for (WinningStatus state : values()) {
      WINNING_STATUS_MAP.put(state.value, state);
    }
  }

  private int value;

  private String message;

  private WinningStatus state;

  public WinningStatus getState() {
    return state;
  }

  public void setState(WinningStatus state) {
    this.state = state;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  WinningStatus(int value, String message) {
    this.value = value;
    this.message = message;
  }

  public static WinningStatus valueOf(int value) {
    switch (value) {
      case 0:
        return UNCLAIMED;
      case 1:
        return CLAIMED;
      case 2:
        return INVALID;
      default:
        return ILLEGAL;
    }
  }

  public static WinningStatus getEnum(int index) {
    WinningStatus status = WINNING_STATUS_MAP.get(index);
    if (status == null) {
      throw new IllegalArgumentException("枚举没有对应的映射值");
    }
    return status;
//    return Optional.ofNullable(WINNING_STATUS_MAP.get(index))
//        .orElseThrow(IllegalArgumentException::new);
  }
}
