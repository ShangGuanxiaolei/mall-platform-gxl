package com.xquark.dal.status;

public enum ShopStatus {
  ACTIVE,
  FROZEN
}
