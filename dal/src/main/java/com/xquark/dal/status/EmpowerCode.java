package com.xquark.dal.status;

public enum EmpowerCode {

    /**
     *邀请码相关说明
     */
    EmpowerText("1.邀请码是您在汉薇商城中的唯一标识，您可方便地分享汉薇商城给身边的小伙伴；\n"
    +"2.长按图片可保存二维码到本地或通过微信分享给好友；\n"
    +"3.好友扫码后即可进入汉薇商城。"
    );

    private String word;

    

    EmpowerCode(String word) {
        this.word=word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
