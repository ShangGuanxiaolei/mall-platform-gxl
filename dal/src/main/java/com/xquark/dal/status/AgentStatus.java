package com.xquark.dal.status;

/**
 * 代理申请状态
 *
 * @author chh 2016-12-06
 */
public enum AgentStatus {
  ACTIVE,
  FROZEN,
  APPLYING
}
