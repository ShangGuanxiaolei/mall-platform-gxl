package com.xquark.dal.status;

/**
 * @author wangxinhua on 2018/5/18.
 * DESC: 积分规则状态
 */
public enum PointGradeStatus {

  /**
   * 启用
   */
  ENABLE,

  /**
   * 禁用
   */
  DISABLE

}
