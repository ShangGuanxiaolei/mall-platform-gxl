package com.xquark.dal.status;

/**
 * Created by chh on 16/12/07.
 */
public enum UserType {
  B2B,      //B2B用户
  B2C,       //B2C用户
  B2C_APPLYING,       //B2C正在审核中用户
  B2B_APPLYING, //B2B正在审核中用户
  B2B_FROZEN, //B2B被冻结用户
  DEFAULT //初始用户
}
