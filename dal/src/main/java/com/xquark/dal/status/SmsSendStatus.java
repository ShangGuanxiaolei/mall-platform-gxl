package com.xquark.dal.status;

public enum SmsSendStatus {
  SUCCESS, FAIL, PENDING
}
