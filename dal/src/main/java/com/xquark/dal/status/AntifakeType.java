package com.xquark.dal.status;

/**
 * 物流码类型
 *
 * @author chh 2017-6-12
 */
public enum AntifakeType {
  TRUNK, //箱标
  BOX //盒标
}
