package com.xquark.dal.status;

public enum MainOrderStatus {
  // PREORDER, // 预订
  SUBMITTED(OrderStatus.SUBMITTED), // 已提交 ，未付款
  CANCELLED(OrderStatus.CANCELLED), // 取消
  PAID(OrderStatus.PAID), // 已付款  未发货
  SHIPPED(OrderStatus.SHIPPED), // 已发货
  SUCCESS(OrderStatus.SUCCESS), // 交易成功
  REFUNDING(OrderStatus.REFUNDING), // 退款申请中
  CLOSED(OrderStatus.CLOSED), // 交易关闭
  PAID_NO_STOCK(OrderStatus.PAIDNOSTOCK),//已支付无库存
  PENDING(OrderStatus.PENDING); // 支付中

  private final OrderStatus orderStatus;

  MainOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }
}
