package com.xquark.dal.status;

/**
 * 代理申请类型
 *
 * @author chh 2016-12-06
 */
public enum AgentType {
  ALL, //所有
  FOUNDER, //联合创始人
  DIRECTOR, //董事
  GENERAL, //总顾问
  FIRST, //一星顾问
  SECOND, //二星顾问
  SPECIAL //特约
}
