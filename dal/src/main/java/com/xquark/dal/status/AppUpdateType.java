package com.xquark.dal.status;

/**
 * app更新类型
 *
 * @author chh 2017-04-01
 */
public enum AppUpdateType {
  SUGGEST, // 建议更新
  FORCE, // 强制更新
  NO // 不更新
}
