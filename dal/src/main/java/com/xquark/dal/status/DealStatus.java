package com.xquark.dal.status;

public enum DealStatus {
  NEW,
  IN_PROGRESS,
  SUCCESS,
  CLOSED
}
