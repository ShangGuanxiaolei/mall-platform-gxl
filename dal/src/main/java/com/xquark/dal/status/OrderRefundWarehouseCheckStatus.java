package com.xquark.dal.status;

/***
 * 退款订单的仓库检查状态
 */
public enum OrderRefundWarehouseCheckStatus {
    UNCHECKED,//未检查
    CHECKED_PASS,//检查通过
    CHECKED_FAILED,//未检查
}
