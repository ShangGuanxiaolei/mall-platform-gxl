package com.xquark.dal.status;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

/**
 * created by
 *
 * @author wangxinhua at 18-6-30 下午1:29 处理本平台订单与中台订单之间的映射关系
 */
public class OrderHeaderStatusMapping {

  public static int SUBMITTED = 0;
  public static int CANCELLED = 2;
  public static int PAID = 1;
  public static int SHIPPED = 4;
  public static int SUCCESS = 5;
  public static int REFUNDING = 9;
  public static int COMMENT = -1;
  public static int CLOSED = 11;
  public static int DELIVERY = 3;
  public static int NOT_MATCH = -1;
  /**
   * 特殊处理拼团的PAIDNOSTOCK状态, 给结算中心不认识的状态
   */
  public static int PIECE_PAID_NOSTOCK = 20;

  private final static Map<OrderStatus, Integer> INNER_MAP =
      ImmutableMap.<OrderStatus, Integer>builder()
          .put(OrderStatus.SUBMITTED, SUBMITTED)
          .put(OrderStatus.CANCELLED, CANCELLED)
          .put(OrderStatus.PAID, PAID)
          .put(OrderStatus.PAIDNOSTOCK, PAID)
          .put(OrderStatus.SHIPPED, SHIPPED)
          .put(OrderStatus.SUCCESS, SUCCESS)
          .put(OrderStatus.REFUNDING, REFUNDING)
          .put(OrderStatus.COMMENT, NOT_MATCH) // 未实现
          .put(OrderStatus.CLOSED, NOT_MATCH) // 未匹配
          .put(OrderStatus.DELIVERY,DELIVERY) //待发货
          .build();

  public static int mapping(OrderStatus orderStatus) {
    Integer ret = INNER_MAP.get(orderStatus);
    Preconditions.checkNotNull(ret, "订单状态对应关系未初始化:the relation of the order status had not been init...ed");
    return ret;
  }

}
