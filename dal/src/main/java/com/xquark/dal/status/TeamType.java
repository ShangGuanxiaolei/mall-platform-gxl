package com.xquark.dal.status;

/**
 * 战队分红类型
 *
 * @author chh 2017-7-4
 */
public enum TeamType {
  WEEK, //自然周
  MONTH //自然月
}
