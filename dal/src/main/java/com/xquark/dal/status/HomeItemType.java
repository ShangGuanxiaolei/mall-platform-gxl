package com.xquark.dal.status;

/**
 * app模块配置类型
 *
 * @author chh 2017-02-23
 */
public enum HomeItemType {
  H5, //H5页面
  Native //原生应用
}
