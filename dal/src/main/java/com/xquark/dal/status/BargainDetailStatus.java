package com.xquark.dal.status;

/**
 * Created by wangxinhua on 17-8-8. DESC:
 */
public enum BargainDetailStatus {
  CREATED, // 进行中
  PAID, // 已付款  未发货
  REFUNDING, // 退款申请中
  SHIPPED, // 已发货
  SUCCESS, // 交易成功,已收货
  CLOSED // 活动结束未交易
}
