package com.xquark.dal.status;

/**
 * 推客等级类型
 *
 * @author chh 2017-06-27
 */
public enum TwitterLevelType {
  COMMISSION, //按佣金
  CUSTOM //自定义
}
