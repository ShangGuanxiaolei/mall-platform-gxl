package com.xquark.dal.status;

/**
 * Created by quguangming on 16/3/2.
 */
public enum UserStatus {
  ANONYMOUS,      //匿名用户
  DETACHED,       //游离用户
  LOGINED         //登录验证过的
}
