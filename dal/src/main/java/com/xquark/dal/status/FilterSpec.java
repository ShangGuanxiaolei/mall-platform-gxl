package com.xquark.dal.status;

/**
 * Created by wangxinhua on 18-3-15. DESC:滤芯规格
 */
public enum FilterSpec {

  SINGLE, // 单品
  DOZEN // 套装
}
