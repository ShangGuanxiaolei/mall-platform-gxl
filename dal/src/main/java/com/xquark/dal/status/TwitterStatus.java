package com.xquark.dal.status;


public enum TwitterStatus {

  ACTIVE,
  FROZEN,
  APPLYING
}
