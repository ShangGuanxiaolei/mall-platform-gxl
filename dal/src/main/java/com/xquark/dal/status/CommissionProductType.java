package com.xquark.dal.status;

/**
 * 分佣类型，按百分比或者固定金额
 *
 * @author chh 2017-06-28
 */
public enum CommissionProductType {
  PER, //百分比
  AMOUNT //固定金额
}
