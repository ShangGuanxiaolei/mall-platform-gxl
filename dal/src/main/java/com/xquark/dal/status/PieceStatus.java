package com.xquark.dal.status;

import java.util.HashMap;
import java.util.Map;

import static com.xquark.dal.status.ExecStatus.*;

/**
 * @author wangxinhua
 * @since 18-11-2
 */
public enum PieceStatus {

  /**
   * 已提交, 未支付
   */
  SUBMITTED(0, NORMAL),

  /**
   * 已开团
   */
  OPENED(1, NORMAL),

  /**
   * 已成团
   */
  GROUPED(2, SUCCESS),

  /**
   * 库存不足取消
   */
  STOCK_CANCELED(3, FAILED),

  /**
   * 过期取消
   */
  EXPIRE_CANCELED(4, FAILED);

  private final static Map<Integer, PieceStatus> CODE_MAPPING;

  static {
    CODE_MAPPING = new HashMap<>();
    for (PieceStatus status : PieceStatus.values()) {
      CODE_MAPPING.put(status.getCode(), status);
    }
  }

  private final int code;

  private final ExecStatus ret;

  PieceStatus(int code, ExecStatus ret) {
    this.code = code;
    this.ret = ret;
  }

  public int getCode() {
    return code;
  }

  public ExecStatus getRet() {
    return ret;
  }

  public static PieceStatus fromCode(int code) {
    return CODE_MAPPING.get(code);
  }

  public static PieceStatus fromCode(String status) {
    int val;
    try {
      val = Integer.valueOf(status);
    } catch (Exception e) {
      throw new IllegalArgumentException("status " + status + " can not be cast to PieceStatus");
    }
    return fromCode(val);
  }

}
