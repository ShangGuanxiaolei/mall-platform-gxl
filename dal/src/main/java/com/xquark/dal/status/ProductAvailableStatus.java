package com.xquark.dal.status;

/**
 * 商品的库存状态
 */
public enum ProductAvailableStatus {
    NOTIFY_OF_ARRIVAL,//到货通知
    NORMAL;//正常
}
