package com.xquark.dal.status;

public enum HomeItemCode {
  FIND, // 发现
  SALE, // 精选特卖
  VIP, // 钻石专享
  GROUPON, // 拼团
  CHOOSE, // 选品上架
  MINE, // 我的店铺
  ORDER, // 订单管理
  PROFIT, // 我的收益
  CUSTOMER, // 业绩管理
  SETTING, // 设置
  CLASS, // 在线课堂
  PURCHASE, // 进货
  AGENT // 代理管理
}
