package com.xquark.dal.status;

/**
 * 推客设置引导消费者有效期类型
 *
 * @author chh 2017-06-27
 */
public enum ValidType {
  ONE, //一次: 以用户最后访问的小店链接为准有效
  ALWAYS, //永久: 以用户最先访问的小店链接为准有效
  DAY, //1天: 访问小店链接一天内有效
  WEEK, //1周: 访问小店链接一周内有效
  MONTH //1月: 访问小店链接一月内有效
}
