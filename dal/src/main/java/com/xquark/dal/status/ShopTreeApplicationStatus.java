package com.xquark.dal.status;


public enum ShopTreeApplicationStatus {

  ACTIVE,
  APPLYING
}
