package com.xquark.dal.status;

public enum CategoryStatus {
  ONSALE, // 上架
  INSTOCK // 下架
}
