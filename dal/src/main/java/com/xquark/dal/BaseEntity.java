package com.xquark.dal;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

  String getId();

}
