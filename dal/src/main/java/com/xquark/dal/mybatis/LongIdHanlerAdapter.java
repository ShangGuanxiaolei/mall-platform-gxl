package com.xquark.dal.mybatis;

/**
 * @author wangxinhua
 * @date 2019-04-26
 * @since 1.0
 */
public interface LongIdHanlerAdapter extends IdHandleAdapter<Long> {

    @Override
    default Long decode(String id) {
        return IdTypeHandler.decode(id);
    }

}
