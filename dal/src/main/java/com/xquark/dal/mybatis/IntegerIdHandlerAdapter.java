package com.xquark.dal.mybatis;

/**
 * @author wangxinhua
 * @date 2019-04-26
 * @since 1.0
 */
public interface IntegerIdHandlerAdapter extends IdHandleAdapter<Integer> {

    @Override
    default Integer decode(String id) {
        return Long.valueOf(IdTypeHandler.decode(id)).intValue();
    }
}
