package com.xquark.dal.mybatis;

import static com.xquark.dal.mybatis.IdTypeHandler.encode;

/**
 * 处理没有使用idHandler的类型
 * @author wangxinhua
 * @date 2019-04-26
 * @since 1.0
 */
public interface IdHandleAdapter<T extends Number> {

    T getId();

    void setId(T id);

    T decode(String id);

    default String getIdEncoded() {
        final T idRaw = getId();
        if (idRaw == null) {
            return null;
        }
        return encode((Long) idRaw);
    }

    default void setIdEncoded(String id) {
        T decode = decode(id);
        setId(decode);
    }

}
