package com.xquark.dal.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.Alias;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.xquark.dal.model.User;
import com.xquark.utils.cache.ThreadLocalCache;

@Alias("domainTypeHandler")
public final class DomainTypeHandler extends BaseTypeHandler<Object> {

  final static Logger log = LoggerFactory.getLogger(DomainTypeHandler.class);

  @Override
  public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType)
      throws SQLException {
    String domain = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Object principal = auth.getPrincipal();
    if (principal != null && principal instanceof User) {
      User user = (User) principal;
      domain = user.getPartner();
    }

    //TODO
    //获取domain为空从缓存变量中取
    if (StringUtils.isBlank(domain)) {
      domain = (String) ObjectUtils.defaultIfNull(ThreadLocalCache.get("domain"), "");
      log.info("domain from ThreadLocalCache" + domain);
    }

    //TODO 暂时先放xiangqu, 防止出错，15年7月底删除
    if (StringUtils.isBlank(domain)) {
      domain = StringUtils.defaultIfBlank(domain, "xiangqu");
      log.error("domain error");
    }

    if (StringUtils.isBlank(domain)) {
      log.error("domain is null");
    } else {
      ps.setString(i, domain.toLowerCase());
    }
  }

  @Override
  public String getNullableResult(ResultSet rs, String columnName)
      throws SQLException {
    return null;
  }

  @Override
  public String getNullableResult(ResultSet rs, int columnIndex)
      throws SQLException {
    return null;
  }

  @Override
  public String getNullableResult(CallableStatement cs, int columnIndex)
      throws SQLException {
    return null;
  }

}
