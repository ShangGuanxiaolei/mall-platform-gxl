package com.xquark.dal.type;

/**
 * @author luqig
 * @since 2019-04-29
 */
public enum ActivityModule {
  /**
   * 秒杀
   */
  SPIKE{
    @Override
    String description() {
      return "秒杀";
    }
  },

  /**
   * 预售
   */
  ADVANCE_SALE {
    @Override
    String description() {
      return "预售";
    }
  },

  /**
   * 拼团
   */
  ASSEMBLE {
    @Override
    String description() {
      return "拼团";
    }
  },

  /**
   * 砍价
   */
  BARGAIN {
    @Override
    String description() {
      return "砍价";
    }
  },

  /**
   * 红包雨
   */
  MONEY_RAIN {
    @Override
    String description() {
      return "红包雨";
    }
  },

  /**
   * 抽奖
   */
  LUCK_DRAW {
    @Override
    String description() {
      return "抽奖";
    }
  };

  abstract String description();
}
