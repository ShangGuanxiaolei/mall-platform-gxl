package com.xquark.dal.type;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public enum CareerLevelType {
  PW(true),

  RC(true),

  DS(false),

  SP(false);

  /**
   * 是否属于新人类型
   */
  private final boolean isNewUserType;

  /**
   * @deprecated
   * 该方法默认值返回RC，已经不能再单独使用，需要再根据弱关系区分是白人还是纯白人
   * 建议使用 CustomerProfileService::getLevelType 这个包装方法
   */
  CareerLevelType(boolean isNewUserType) {
    this.isNewUserType = isNewUserType;
  }

  /**
   * @deprecated
   * 该方法默认值返回RC，已经不能再单独使用，需要再根据弱关系区分是白人还是纯白人
   * 建议使用 CustomerProfileService::getLevelType 这个包装方法
   */
  public static CareerLevelType getFinalLevelType(CareerLevelType level1, CareerLevelType level2) {
    level1 = Optional.ofNullable(level1).orElse(RC);
    level2 = Optional.ofNullable(level2).orElse(RC);
    return level1.compareTo(level2) > 0 ? level1 : level2;
  }

  /**
   * @deprecated
   * 该方法默认值返回RC，已经不能再单独使用，需要再根据弱关系区分是白人还是纯白人
   * 建议使用 CustomerProfileService::getLevelType 这个包装方法
   */
  @Deprecated
  public static CareerLevelType getFinalLevelType(String level1, String level2) {
    CareerLevelType levelType1;
    CareerLevelType levelType2;
    try {
      levelType1 = StringUtils.isBlank(level1) ? null : CareerLevelType.valueOf(level1);
      levelType2 = StringUtils.isBlank(level2) ? null : CareerLevelType.valueOf(level2);
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
    return getFinalLevelType(levelType1, levelType2);
  }

  public boolean isNewUserType() {
    return isNewUserType;
  }
}
