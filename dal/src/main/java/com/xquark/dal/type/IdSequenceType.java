package com.xquark.dal.type;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午4:18
 */
public enum IdSequenceType {

  MEMBER,
  ADDRESS,
  BANK,
  ORDER

}
