package com.xquark.dal.type;

/**
 * Created by dongsongjie on 15/12/3.
 */
public enum ProductFeaturesKeyType {

  NOT_SEND_TO_ERP(1);

  private int index;

  ProductFeaturesKeyType(int index) {
    if (index == 0) {
      throw new RuntimeException("ProductFeaturesKeyType index start with 1.");
    }
    this.index = index;
  }

  public int getIndex() {
    return index;
  }
}
