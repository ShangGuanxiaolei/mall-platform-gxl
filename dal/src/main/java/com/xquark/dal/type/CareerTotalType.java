package com.xquark.dal.type;

/**
 * created by
 *
 * @author wangxinhua at 18-6-25 下午7:53 业绩统计枚举类型
 */
public enum CareerTotalType {
  /**
   * 零售业绩
   */
  RETAIL,

  // 个人业绩(单网汉薇)
  PPV_HVMALL,

  // 个人业绩(三网)
  PPV_ALL,

  // 小组业绩(单网汉薇)
  PV_HVMALL,

  // 小组业绩(三网)
  PV_ALL
}
