package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-7-20. DESC: 积分专区类别
 */
public enum YundouCategoryType {

  NO_LIMIT(0, 4, "名品兑换"), // 无门槛
  TWO_HUNDRED(200, 3, "200专区"), // 200以上
  FOUR_HUNDRED(400, 3, "400专区"),
  SIX_HUNDRED(600, 3, "600专区"),
  EIGHT_HUNDRED(800, 3, "800专区");

  private int point; // 积分
  private int limit;
  private String name;

  YundouCategoryType(int point, int limit, String name) {
    this.point = point;
    this.limit = limit;
    this.name = name;
  }

  public int getPoint() {
    return point;
  }

  public int getLimit() {
    return limit;
  }

  public String getName() {
    return name;
  }
}
