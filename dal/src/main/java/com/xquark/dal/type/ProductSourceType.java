package com.xquark.dal.type;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午3:24
 */
public enum ProductSourceType {

  /**
   * 自营
   */
  SELF,

  /**
   * 顺丰优选
   */
  SF

}
