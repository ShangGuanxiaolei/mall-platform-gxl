package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-12-4. DESC:
 */
public enum PromotionApplyStatus {
  APPLYING("您的申请已提交审核, 请勿重复提交"), // APPLYING 已经付了定金
  PASS("后台审核已通过，可以购买"), // 已审核通过, 但不能支付尾款
  REJECTED("审核未通过，请联系客服"), // 审核拒绝
  SUCCESS("购买成功"), // 尾款已支付，待发货
  END("活动已结束"); // 活动已结束，需退还定金

  private final String desc;

  PromotionApplyStatus(String desc) {
    this.desc = desc;
  }

  public String getDesc() {
    return desc;
  }
}
