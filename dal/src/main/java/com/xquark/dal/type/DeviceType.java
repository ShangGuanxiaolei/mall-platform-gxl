package com.xquark.dal.type;

public enum DeviceType {
  ANDROID, IOS, IPAD, PC, WAP
}
