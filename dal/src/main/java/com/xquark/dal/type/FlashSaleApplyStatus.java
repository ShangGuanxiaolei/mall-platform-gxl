package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-12-5. DESC:
 */
public enum FlashSaleApplyStatus {
  APPLYING, SUCCESS, REJECTED
}
