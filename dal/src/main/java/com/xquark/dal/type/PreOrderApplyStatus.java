package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-12-5. DESC:
 */
public enum PreOrderApplyStatus {
  APPLYING, // APPLYING 已经付了定金
  PASS, // 已审核通过, 但不能支付尾款
  REJECTED, // 审核拒绝，退还定金
  SUCCESS, // 尾款已支付，待发货
  END // 活动已结束，需退还定金
}
