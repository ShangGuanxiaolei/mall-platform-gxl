package com.xquark.dal.type;

/**
 * @author wangxinhua on 2018/7/27. DESC:
 */
public enum JobStatus {

  CREATED(1),

  IN_PROGRESSING(2),

  COMPLETED(3),

  ERROR(4);

  private final int code;

  JobStatus(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
