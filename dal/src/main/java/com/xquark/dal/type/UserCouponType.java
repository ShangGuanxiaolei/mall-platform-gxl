package com.xquark.dal.type;

import static com.google.common.base.Preconditions.checkState;

import com.xquark.dal.vo.BasePromotionCouponVO;
import org.apache.commons.lang3.StringUtils;

/**
 * 用户优惠券类型（针对店铺或者商品）
 *
 * @author chh 2017-07-25
 */
public enum UserCouponType {

  SHOP {
    @Override
    public boolean inScope(BasePromotionCouponVO toCompare, String productId,
        String categoryId) {
      return true;
    }
  },
  PRODUCT {
    @Override
    public boolean inScope(BasePromotionCouponVO toCompare, String productId,
        String categoryId) {
      checkState(StringUtils.isNotBlank(productId), "优惠券指定商品不正确");
      String dbProductId = toCompare.getProductId();
      return dbProductId.contains(productId);
    }
  }, // 商品
  CATEGORY {
    @Override
    public boolean inScope(BasePromotionCouponVO toCompare, String productId,
        String categoryId) {
      // 商品没有分类
      if (StringUtils.isBlank(categoryId)) {
        return false;
      }
      String dbCategoryId = toCompare.getCategoryId();
      return dbCategoryId.contains(categoryId);
    }
  }; // 商品分类

  public abstract boolean inScope(BasePromotionCouponVO toCompare, String productId,
      String categoryId);
}
