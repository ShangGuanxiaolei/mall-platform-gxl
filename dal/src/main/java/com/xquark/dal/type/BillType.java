package com.xquark.dal.type;

import com.xquark.dal.validation.group.bill.AddedTaxGroup;
import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;

/**
 * Created by IntelliJ IDEA. User: huangjie Date: 2018/5/21. Time: 下午3:09 发票的类型
 */
public enum BillType {

  NORMAL("普通发票", NormalGroup.class),
  ELECTRONIC("电子发票", ElectronicGroup.class),
  ADDED("增值税发票", AddedTaxGroup.class);

  private String name;

  private Class<?> validationGroup;

  BillType(String name, Class<?> validationGroup) {
    this.name = name;
    this.validationGroup = validationGroup;
  }

  public String getName() {
    return name;
  }

  public Class<?> getValidationGroup() {
    return validationGroup;
  }
}
