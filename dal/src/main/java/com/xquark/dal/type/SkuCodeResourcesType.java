package com.xquark.dal.type;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Skucode
 */
@Component
public class SkuCodeResourcesType {

  private  static  Map<String,SkuType> map = new ConcurrentHashMap<>(16);
  static {
    map.put("ECMOHO", new SkuType("ECMOHO", "0", "A", null));
    map.put("SENMIAO", new SkuType("SENMIAO", "1", "B", null));
    map.put("CLIENT", new SkuType("CLIENT", "CLIENT", "D", "01"));
    map.put("BLP", new SkuType("BLP", "BLP", "E", "JK"));
  }
  public static SkuType valueResOf(String res) {
    return  map.get(res);
  }

  public static SkuType valueSupplierCode(String s) {
    for (Map.Entry<String, SkuType> stringSkuTypeEntry : map.entrySet()) {
      String supplierCode = stringSkuTypeEntry.getValue().getSupplierCode();
      if (supplierCode != null) {
        if (supplierCode.equals(s)) {
          return stringSkuTypeEntry.getValue();
        }
      }
    }
    return null;
  }

  public static SkuType put(String res,SkuType skuType) {
    return  map.put(res,skuType);
  }

  public static void delete(String key) {
    map.remove(key);
  }


  public static Optional<SkuType>
  getSkuCodeResourceBySupplierCode(String code) {

    SkuType skuType = valueResOf(code);
    if (skuType != null) {
      return Optional.of(skuType);
    }
    for (Map.Entry<String, SkuType> stringSkuTypeEntry : map.entrySet()) {
      String supplierCode = stringSkuTypeEntry.getValue().getSupplierCode();
      if (supplierCode != null) {
        if (supplierCode.equals(code)) {
          return Optional.of(stringSkuTypeEntry.getValue());
        }
      }
    }
    return Optional.ofNullable(null);
  }

}
