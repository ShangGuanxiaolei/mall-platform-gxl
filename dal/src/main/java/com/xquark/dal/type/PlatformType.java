package com.xquark.dal.type;

import com.google.common.base.Optional;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangxinhua on 2018/5/19. DESC: 积分平台类型
 */
public enum PlatformType {

  /**
   * 汉德森
   */
  H(1, "Hds", "汉德森", "440372988966", "withdraw_hds"),

  /**
   * ViViLife
   */
  V(2, "ViviLife", "viviLife", "455972148095", "withdraw_vivi"),

  /**
   * 汉薇商城
   */
  E(3, "Ecomm", "汉薇", "444275564696", "withdraw_hv"),

  /**
   * 系统
   */
  S(4, "System", "系统", "", "");

  private static final Map<Integer, PlatformType> STATIC_MAP;

  static {
    STATIC_MAP = new HashMap<>();
    for (PlatformType type : PlatformType.values()) {
      STATIC_MAP.put(type.getCode(), type);
    }
  }

  private final int code;

  private final String fullName;

  private final String nameCN;

  /**
   * 提现账户
   */
  private final String withdrawAccount;

  /**
   * 定时任务key
   */
  private final String timeKey;

  PlatformType(int code, String fullName, String nameCN, String withdrawAccount,
      String timeKey) {
    this.code = code;
    this.fullName = fullName;
    this.nameCN = nameCN;
    this.withdrawAccount = withdrawAccount;
    this.timeKey = timeKey;
  }

  public int getCode() {
    return code;
  }

  public String getFullName() {
    return fullName;
  }

  public String getWithdrawAccount() {
    return withdrawAccount;
  }

  public String getTimeKey() {
    return timeKey;
  }

  public String getNameCN() {
    return nameCN;
  }

  public static Optional<PlatformType> fromCodeOp(int code) {
    return Optional.fromNullable(STATIC_MAP.get(code));
  }

  public static PlatformType fromCode(int code) {
    return STATIC_MAP.get(code);
  }

}
