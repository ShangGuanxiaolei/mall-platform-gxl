package com.xquark.dal.type;

/**
 * Created by wangxinhua. Date: 2018/8/28 Time: 下午3:29
 * 微信第三方服务
 */
public enum WechatServiceType {

  /**
   * 原生类型
   */
  RAW,

  /**
   * 易办事
   */
  EPS

}
