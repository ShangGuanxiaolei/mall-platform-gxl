package com.xquark.dal.type;

public enum Taxonomy {
  CATEGORY,   // 后台类目
  GOODS,     // 后台类目
  FILTER,    // 滤芯
  CATALOG,   // 前台类目
  BRAND,     // 品牌
  ACTIVITY,  // 促销活动
}
