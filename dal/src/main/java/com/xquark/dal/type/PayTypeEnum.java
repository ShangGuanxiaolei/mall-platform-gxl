package com.xquark.dal.type;

import org.apache.commons.lang3.StringUtils;

public enum PayTypeEnum {

    ALIPAY_APP("alipay_app", "支付宝APP"),

    ALIPAY_SCAN("alipay_scan", "支付宝扫码支付"),

    ALIPAY_WAP("alipay_wap", "支付宝手机网站支付"),

    ALIPAY_WEB("alipay_web", "支付宝PC网站支付"),

    WECHAT_APP("wechat_app", "微信APP支付"),

    WECHAT_JSAPI("wechat_jsapi", "微信公众号"),

    WECHAT_MINIAPP("wechat_miniapp", "微信小程序"),

    WECHAT_NATIVE("wechat_native", "微信扫码支付"),

    WECHAT_MWEB("wechat_mweb", "微信手机网站H5支付");

    private String payType;
    private String typeName;

    PayTypeEnum(String payType, String typeName) {
        this.payType = payType;
        this.typeName = typeName;
    }

    public String getPayType() {
        return payType;
    }

    public String getTypeName() {
        return typeName;
    }
    
    public static PayTypeEnum getPayType(String payType) {
    	if(StringUtils.isBlank(payType))
    		return null;
    	for(PayTypeEnum type : PayTypeEnum.values()) {
    		if(type.payType.equalsIgnoreCase(payType))
    			return type;
    	}
    	return null;
    }
}


