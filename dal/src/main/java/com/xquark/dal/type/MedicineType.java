package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
public enum MedicineType {
  ASPIRIN, // 阿司匹灵
  DIGITALIS, // 洋地黄
  NEO, // 新霉素
  ANTIBIOTIC, // 抗生素
  HORMONAL // 激素药物
}
