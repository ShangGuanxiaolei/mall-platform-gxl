package com.xquark.dal.type;

public enum MainOrderActionType {
  SUBMIT, // 下单
  CANCEL, // 取消订单
  PAY, // 订单支付
  FINISH, // 订单完成
  REFUND, // 卖家退款
  REQUEST_REFUND, // 申请退款
  ACCEPT_REFUND,  // 接受退款申请
  REJECT_REFUND,   // 拒绝退款申请
  CANCEL_REFUND,   // 取消退款申请
  DELAYSIGN//延期签约
}
