package com.xquark.dal.type;

public enum SyncEvType {

  /**
   * 事件类型从1开始, 保持原值不变
   */
  EV_BASE,

  /**
   * 店铺事件
   */
  EV_SHOP,

  /**
   * 商品事件
   */
  EV_PRODUCT,

  /**
   * 活动事件
   */
  EV_ACTIVITY

}
