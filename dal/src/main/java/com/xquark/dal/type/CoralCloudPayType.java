package com.xquark.dal.type;

import java.util.HashMap;
import java.util.Map;

public enum CoralCloudPayType {

  alipay("ALIPAY", 10), // 支付宝
  bankpay("网银支付", 20),
  wenxinpay("WEIXIN", 30),// 微信
  tenpay("财付通", 40),
  remaidpay("账户余额支付", 50),
  pointpay("积分支付", 60),
  otherpay("其他", 90);

  private String key;
  private int value;

  private CoralCloudPayType(String key, int value) {
    this.key = key;
    this.value = value;
  }

  public static final Map<String, CoralCloudPayType> values = new HashMap<String, CoralCloudPayType>();

  static {
    for (CoralCloudPayType ec : CoralCloudPayType.values()) {
      values.put(ec.key, ec);
    }
  }

  public String geKey() {
    return key;
  }

  public int getValue() {
    return value;
  }



}
