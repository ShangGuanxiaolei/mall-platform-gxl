package com.xquark.dal.type;

import com.google.common.base.Optional;
import java.util.regex.Pattern;

/**
 * Created by wangxinhua on 17-10-20. DESC:
 */
public enum FileUploadType {

    UNKNOWN("未知", new String[]{}, 5),

    VIDEO("视频", new String[]{"mp4", "webm"}, 500) {
        @Override
        protected Optional<Pattern> getPattern() {
            return Optional.of(Pattern.compile("([^\\s]+(\\.(?i)(MP4|WEBM))$)"));
        }
    },
    AUDIO("音频", new String[]{"mp3", "wav"}, 100) {
        @Override
        protected Optional<Pattern> getPattern() {
            return Optional.of(Pattern.compile("([^\\s]+(\\.(?i)(MP3|WAV))$)"));
        }
    },
    DOC("文档", new String[]{"doc", "docx","pdf","xls","xlsx"}, 50) {
        @Override
        protected Optional<Pattern> getPattern() {
            return Optional.of(Pattern.compile("([^\\s]+(\\.(?i)(doc|docx|pdf|xls|xlsx))$)"));
        }
    },
    ARTICLE("图文", new String[]{}, 5),
    IMAGE("图片", new String[]{"png", "jpg", "jpeg"}, 20) {
        @Override
        protected Optional<Pattern> getPattern() {
            return Optional.of(Pattern.compile(".+(.JPEG|.jpeg|.JPG|.jpg|.png|.PNG|.gif|.GIF|.bmp|.BMP)$"));
        }
    };

    /**
     * 中文名称
     */
    private final String str;

    /*允许的后缀名*/
    private final String[] postfix;

    /**
     * 文件大小限制
     */
    private final int limitSize;

    FileUploadType(String str, String[] postfix, int limitSize) {
        this.str = str;
        this.postfix = postfix;
        this.limitSize = limitSize;
    }

    public String getStr() {
        return str;
    }

    public int getLimitSize() {
        return limitSize;
    }

    public String[] getPostfix() {
        return postfix;
    }

    /**
     * 校验文件名格式
     *
     * @param fileName 文件名
     * @return 格式是否匹配
     */
    public final boolean matches(String fileName) {
        Optional<Pattern> pattern = this.getPattern();
        if (!pattern.isPresent()) {
            return Boolean.TRUE;
        }
        return pattern.get().matcher(fileName).matches();
    }

    /**
     * 获取正则 {@code Pattern} 对象, 默认不需要校验
     *
     * @return {@code Optional} 封装的Pattern对象
     */
    protected Optional<Pattern> getPattern() {
        return Optional.absent();
    }
}
