package com.xquark.dal.type;

/**
 * @author huxaya
 */
public enum PaymentMode {
  WEIXIN, //
  WEIXIN_APP, //
  WEIXIN_MINIPROGRAM,
  UNION, //
  //	TENPAY, //
  ALIPAY, //
  ALIPAY_PC,
  UMPAY,
  PINGANPAY,
  SUMPAY,
  XIANGQU,
  SHANGOU,
  EPS, NNJ
}
