package com.xquark.dal.type;

public enum CouponGrantRule {
  SINGLE,
  MULTIPLE,
  DEVICE_SINGLE
}
