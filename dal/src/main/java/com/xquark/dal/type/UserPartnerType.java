package com.xquark.dal.type;

public enum UserPartnerType {
  KKKD,
  XIANGQU,
  SHANGOU,
  KUCHUAN,
  NNJ,
  ALISMS
}
