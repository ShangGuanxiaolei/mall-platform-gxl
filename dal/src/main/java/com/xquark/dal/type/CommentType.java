package com.xquark.dal.type;

/**
 * Created by jitre on 11/9/17. 评论对象的枚举
 */
public enum CommentType {
  PRODUCT,// 产品
  ARTICLE,// 文章
  ORDER_ITEM // 订单商品
}
