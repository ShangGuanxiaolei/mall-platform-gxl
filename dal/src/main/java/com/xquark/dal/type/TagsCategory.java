package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-11-16. DESC:
 */
public enum TagsCategory {
  PRODUCT("商品"),
  HELPER("帮助");

  private final String str;

  TagsCategory(String str) {
    this.str = str;
  }

  public String getStr() {
    return str;
  }
}
