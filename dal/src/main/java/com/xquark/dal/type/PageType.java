package com.xquark.dal.type;

/**
 * 活动页面类型
 *
 * @author chh
 */
public enum PageType {
  TWEET,  // 发现
  GROUPON, // 拼团
  FLASHSALE  // 特卖
}
