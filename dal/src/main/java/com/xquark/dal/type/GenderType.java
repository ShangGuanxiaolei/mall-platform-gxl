package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-7-13. DESC:
 */
public enum GenderType {
  N(0),
  M(1),
  FM(2);
  private int code;

  GenderType(int code) {
    this.code = code;
  }

  public int getCode() {
    return this.code;
  }

}
