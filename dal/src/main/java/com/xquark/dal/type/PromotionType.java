package com.xquark.dal.type;

import java.util.Comparator;
import java.util.Objects;

/**
 * 活动类型
 */
public enum PromotionType {

  GROUPON("catalog/product_groupon", "", "", 1),  //团购
  FLASHSALE("catalog/product_flashsale", "flashSalePromotionProductService", "活动优惠", 4), // 限时抢购
  SALEZONE("","","活动优惠",4),// 一元专区
  NORMAL("","","普通商品",1),// 普通商品
  COUPON("", "", "优惠券", 1),
  BARGAIN("catalog/product_bargain", "", "疯狂砍价", 1), // 疯狂砍价
  RESERVE("catalog/product_reserve", "", "预约", 111), // 预售
  //    POINT("catalog/product_yundou"), // 积分商城
  DIAMOND("catalog/product_diamond", "", "积分兑换", 1) {
    @Override
    public boolean canUseCoupon() {
      return true;
    }
  }, // 钻石专享
  FULLCUT("", "", "会员专享优惠", 1), //满减
  INSIDE_BUY("", "", "员工内部优惠", 0),
  PREORDER("catelog/product_preorder", "preOrderPromotionProductService", "预购", 1),
  PREORDER_PAY_REST("catelog/product_preorder", "preOrderPromotionProductService", "预购付尾款", 1),
  POINT("", "", "德分", 2),
  POINT_PACKET("", "", "德分红包", 2),
  COMMISSION("", "", "收益", 3),
  FULLPIECES("", "", "满件", 1), //满件
  CUT_DOWN("", "", "立减", 1),
  PIECE("", "", "活动优惠", 5),
  /**
   * 在活动中还是使用PIECE_GROUP来兼容代码 订单中用的是PIECE, 只能通过复写toString来处理
   */
  @Deprecated
  PIECE_GROUP("", "", "活动优惠", 5) {
    /**
     * 复写toString, 保存时作为PIECE保存
     */
    @Override
    public String toString() {
      return "PIECE";
    }
  },
  PIECE_DOUBLE_DISCOUNT("", "", "拼团折上折", 1),
  VIP_RIGHT("", "", "VIP资格", 1),
  MEETING_GIFT("", "", "大会售卖赠品", 1),
  MEETING("", "", "大会售卖商品", 1),
  APPLY_CHANGE_GOODS("", "", "换货选择商品", 1),
  APPLY_REISSUE_GOODS("", "", "补货选择商品", 1),

  OTHERS("", "", "其他调整", 5),
  LOGISTICS("", "", "配送费", 6),
  LOGISTICS_DISCOUNT("", "", "配送费减免", 7),
  FRESHMAN("", "", "新人专区商品", 1),
  PRODUCT_SALES("", "", "商品折扣促销", 1),
  PACKET_RAIN("", "", "红包雨", 1),
  LOTTERY_DRAW("", "", "抽奖", 1),
  GIFT("", "", "抽奖", 1),
  STADIUM("", "", "分会场", 1),
  MULTIPLE("","","综合",1);



  private String url;

  private String cName;

  private String serviceName;

  /**
   * 排序优先级
   */
  private Integer sortNum;

  /**
   * 根据sortNum排序的比较器
   */
  public static Comparator<PromotionType> SORT_NUM_COMPARATOR =
      new Comparator<PromotionType>() {
        @Override
        public int compare(PromotionType o1, PromotionType o2) {
          if (o1 == null) {
            return -1;
          }
          if (o2 == null) {
            return 1;
          }
          return o1.getSortNum().compareTo(o2.sortNum);
        }
      };

  PromotionType(String url, String serviceName, String cName, int sortNum) {
    this.url = url;
    this.serviceName = serviceName;
    this.cName = cName;
    this.sortNum = sortNum;
  }

  public String getUrl() {
    return url;
  }

  public String getServiceName() {
    return serviceName;
  }

  public boolean canUseCoupon() {
    return false;
  }

  public String getcName() {
    return cName;
  }

  public Integer getSortNum() {
    return sortNum;
  }

  public static boolean isPiece(PromotionType type) {
    return Objects.equals(type, PromotionType.PIECE)
        || Objects.equals(type, PromotionType.PIECE_GROUP);
  }
}
