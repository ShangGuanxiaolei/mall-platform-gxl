package com.xquark.dal.type;

/**
 * @author wangxinhua on 2018/7/27. DESC:
 */
public enum JobType {

  RELEASE_COMMISSION(1, "解冻积分"),

  RELEASE_POINT(2, "解冻德分"),

  IMPORT_COMMISSION(101, "导入积分"),

  IMPORT_POINT(102, "导入德分"),

  IMPORT_POINT_COMM(103, "导入德分积分"),

  IMPORT_WITHDRAW(104, "导入落账反馈"),

  EXPORT_COMMISSION(201, "导出积分"),

  EXPORT_POINT(202, "导出德分"),

  EXPORT_WITHDRAW(203, "导出积分提现"),

  EXPORT_WITHDRAW_DETAIL(204, "导出积分提现详情"),

  EXPORT_LABOR_EXPENSES(205, "导出劳务报酬");

  private final int code;

  private final String desc;

  JobType(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  public int getCode() {
    return code;
  }

  public String getDesc() {
    return desc;
  }
}
