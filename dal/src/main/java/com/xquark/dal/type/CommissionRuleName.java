package com.xquark.dal.type;

/**
 * 分佣规则定义
 */
public enum CommissionRuleName {
  GENERAL_DIRECT_TO_DIRECTOR, //董事的直接下级总顾问订单分润给董事(董事->(直招)总顾问)
  GENERAL_INDIRECT_TO_DIRECTOR, //董事的间接下级总顾问订单分润给董事(董事->(直招)总顾问->(间招)总顾问)
  DIRECTORS_TEAM_TO_DIRECTORS, //董事下属团队(包括董事本身)订单需要分润给上级和上上级董事(无论中间包含多少其他人,只到找到总部为止)
  TEAM_ORDER, //针对联合创始人的封地政策，订单为该区域的所有订单，负责本区域的联合创始人每一盒订单都可以奖励1元
  AREA_ORDER, //联合创始人所发展出的团队，系统下单每一盒奖励1元--无层级限制
  GENERAL_DIRECT_TO_GENERAL //成为总顾问后，直接推荐一位总顾问，一次性返利2000元
}
