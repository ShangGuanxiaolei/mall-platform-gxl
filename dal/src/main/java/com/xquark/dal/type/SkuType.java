package com.xquark.dal.type;

public  class SkuType{
    private String name;
    private String res;
    private String des;
    private String supplierCode = "";

    public SkuType() {
    }

    public SkuType(String name, String res, String des, String supplierCode) {
      this.name = name;
      this.res = res;
      this.des = des;
      this.supplierCode = supplierCode;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getRes() {
      return res;
    }

    public void setRes(String res) {
      this.res = res;
    }

    public String getDes() {
      return des;
    }

    public void setDes(String des) {
      this.des = des;
    }

    public String getSupplierCode() {
      return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
      this.supplierCode = supplierCode;
    }
  }