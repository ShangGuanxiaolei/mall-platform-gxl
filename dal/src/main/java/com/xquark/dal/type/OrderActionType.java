package com.xquark.dal.type;

public enum OrderActionType {
  SUBMIT, // 下单
  CANCEL, // 取消订单
  // PREORDER, // 支付订单预付款
  // CONFIRM, // 确认预订单
  PAY, // 订单支付
  SHIP, // 订单发货
  SIGN, // 订单签收
  REFUND, // 卖家退款
  REQUEST_REFUND, // 申请退款
  ACCEPT_REFUND,  // 接受退款申请
  REJECT_REFUND,   // 拒绝退款申请
  CANCEL_REFUND,   // 取消退款申请
  DELAYSIGN,//延期签约
  PAY_NO_STOCK,//订单支付无库存
  CHANGE, // 卖家换货
  REQUEST_CHANGE, // 申请换货
  ACCEPT_CHANGE, // 接受换货申请
  REJECT_CHANGE, // 拒绝换货申请
  CANCEL_CHANGE, //取消换货申请
  REQUEST_REISSUE, // 申请补货
  ACCEPT_REISSUE, // 接受补货申请
  REJECT_REISSUE, // 拒绝补货申请
  CANCEL_REISSUE, //取消补货申请
}
