package com.xquark.dal.type;

public enum AvatarStyle {
  CIRCLE,
  SQUARE
}
