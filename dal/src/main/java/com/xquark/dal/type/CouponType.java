package com.xquark.dal.type;

public enum CouponType {
  /**
   * 优惠减免
   */
  REDUCTION,

  /**
   * 优惠码
   */
  CODE

}
