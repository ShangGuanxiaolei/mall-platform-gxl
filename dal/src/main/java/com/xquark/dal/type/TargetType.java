package com.xquark.dal.type;

/**
 * 发现活动页面跳转类型
 *
 * @author chh
 */
public enum TargetType {
  PRODUCT,  // 单个商品
  GROUPON,  // 团购
  ACTIVITY, // 活动页面
  CATEGORY,  // 商品类别
  CUSTOMIZED,  // 自定义
  SECOND_CATEGORY //二级分类商品列表
}
