package com.xquark.dal.type;

public enum OrderSingleType {
  /**
   * 购物车下单
   */
  CART,
  /**
   * 直接下单
   */
  SUBMIT,
}
