package com.xquark.dal.type;

/**
 * @author wangxinhua on 2018/8/1. DESC:
 * 邮费减免类型
 */
public enum LogisticDiscountType {

  /**
   * 包邮
   */
  FREE_POSTAGE,

  /**
   * 首次购买包邮
   */
  FREE_FIRST_PAY

}
