package com.xquark.dal.type;

import java.util.Objects;

public enum OrderSortType {

  /**
   * 普通订单
   */
  NORMAL,

  /**
   * 团购订单
   */
  GROUPON,

  /**
   * 进货订单
   */
  PURCHASE,

  /**
   * 积分订单
   */
  YUNDOU,

  /**
   * 限时抢购订单
   */
  FLASHSALE,

  /**
   * 一元专区订单
   */
  SALEZONE,

  /**
   * 品牌专题订单
   */
  DIAMOND,

  /**
   * 砍价订单
   */
  BARGAIN,

  /**
   * 满减订单
   */
  FULLCUT,

  INSIDE_BUY,

  /**
   * 预购订单
   */
  PREORDER,

  /**
   * 预购订单付尾款
   */
  PREORDER_PAY_REST,

  /**
   * 特权活动订单
   */
  PRIVILEGE,

  /**
   * 优惠券
   */
  COUPON,
  /**
   * 拼团交易
   */
  PIECE,
  /**
   * 杭州大会活动
   */
  MEETING,
  /**
   * vip资格商品
   */
  VIP_RIGHT,

  /**
   * 换货订单
   */
  EXCHANGE,

  /**
   * 基因产品订单
   */
  DNA,

  /**
   * 团购订单
   * 使用PIECE
   */
  @Deprecated
  PIECE_GROUP,

  /**
   * 预售
   */
  RESERVE,

  /**
   * 新客引导订单
   */
  FRESHMAN,

  /**
   * 促销活动订单
   */

  PRODUCT_SALES,
  /**
   *分会场
   */
  STADIUM,

  GIFT,

  /**
   * 补货订单
   */
  REISSUE,
  /**
   * 多个活动的综合订单
   */
  MULTIPLE,
  IMPORT
  ;

  /**
   * 尝试转为 {@link PromotionType} 类型
   *
   * @return 转换成功后的类型或null转换失败
   */
  public PromotionType tryCastToPromotion() {
    PromotionType type = null;
    try {
      type = PromotionType.valueOf(this.name());
    } catch (IllegalArgumentException e) {
      // do noting
    }
    return type;
  }

  /**
   * 命名混乱, 都需要兼容, 后期应该都是PIECE
   */
  public static boolean isPiece(OrderSortType orderSortType) {
    return Objects.equals(orderSortType, PIECE) || Objects.equals(orderSortType, PIECE_GROUP);
  }

}
