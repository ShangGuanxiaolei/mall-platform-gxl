package com.xquark.dal.type;

public enum PaymentChannel {
  PLATFORM, // 支付平台
  CREDITCARD, // 信用卡
  DEBITCARD, // 储蓄卡
  PARTNER,    //合作平台
  EBS //易办事 通道
}
