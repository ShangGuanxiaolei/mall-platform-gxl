package com.xquark.dal.type;

import com.xquark.dal.mapper.ActivityPushDetailMapper;
import com.xquark.dal.model.ActivityPush;
import com.xquark.dal.model.ActivityPushDetail;
import com.xquark.utils.DateUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.xquark.dal.type.ActivityModule.ASSEMBLE;
import static com.xquark.dal.type.ActivityModule.BARGAIN;
import static com.xquark.utils.MessageCastUtils.castCpIdList;
import static com.xquark.utils.MessageCastUtils.castParamMap;

/**
 * @author luqing
 * @since 2019-04-29
 */
@Service
public enum ActionType {

  /**
   * 预热期整体活动上线预告
   * Date : 预热期前一日21点
   * need param X元/折 : X：商品最低价/最低折扣
   */
  SPIKE_PRE_WHOLE_NOTICE(
      ActivityModule.SPIKE,"秒杀活动预告",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 提醒单场活动开始
   * Date : 开始前10分钟
   * need param  C:商品名,B:秒杀价
   */
  SPIKE_SINGLE_START(
      ActivityModule.SPIKE,"上线提醒",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-10 * 60);
    }
  },

  /**
   * 单场活动倒计时提醒
   * Date: 结束前15分钟
   * need param X:最低折扣
   */
  SPIKE_SINGLE_COUNTDOWN(
      ActivityModule.SPIKE,"倒计时提醒",
      1,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-15 * 60);
    }
  },

  /**
   * 库存告急
   * Date : 消耗80%的库存
   * need param B:秒杀价,C:商品名,D:库存数量
   */
  SPIKE_STOCK_EMERGENCY(
      ActivityModule.SPIKE,"库存告急",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },


  /**
   * 预热期整体活动上线预告
   * Date : 预热期前一日21点
   * need param X元/折 : X：商品最低价/最低折扣
   */
  ADVANCE_SALE_PRE_WHOLE_NOTICE(
      ActivityModule.ADVANCE_SALE,"预热活动预告",
        1,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 预售开始提醒
   * Date : 开始前十分钟
   * need param X：最大降价金额
   */
  ADVANCE_SALE_START(
      ActivityModule.ADVANCE_SALE,"上线提醒",
      3,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime, -10 * 60);
    }
  },

  /**
   * 预约期结束前提醒
   * date : 最后一天10点
   */
  ADVANCE_SALE_END(
      ActivityModule.ADVANCE_SALE,"倒计时提醒",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 正式开售提醒
   * Date : 开始前10分钟/首日10点
   * need param A:订购用户名 ,B订购商品
   */
  ADVANCE_SALE_OFFICIAL(
      ActivityModule.ADVANCE_SALE,"正式开售提醒",
      2,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-10 * 60);
    }
  },

  /**
   * 尾款倒计时提醒
   * Date : 结束前15分钟/爆发日晚10点
   * need param A:订购用户名 ,B订购商品
   */
  ADVANCE_SALE_FINAL_COUNTDOWN(
      ActivityModule.ADVANCE_SALE,"尾款倒计时提醒",
      1,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime, -15 * 60);
    }
  },


  /**
   * 爆发期整体活动上线预告
   * Date : 爆发期前一日21点
   * need param Y:活动最低价
   */
  ASSEMBLE_BUR_WHOLE_NOTICE(
      ASSEMBLE,"爆发活动预告",
      1,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 单日活动上线预告
   * Date : 品类日每天10点
   * need param ZZ:品类名,X：活动商品件数
   */
  ASSEMBLE_SINGLE_COUNTDOWN(ASSEMBLE,"拼团倒计时",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 拼团倒计时提醒
   * Date : 结束前15分钟
   * need param  A：拼团发起人用户名 B：拼团产品,Z：拼团达成剩余人数
   */
  ASSEMBLE_COUNTDOWN(
      ASSEMBLE, "拼团倒计时提醒", 1,
       2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-15 * 60);
    }
  },

  /**
   * pre预热期整体活动上线预告
   * Date : pre-预热期前一日21点
   * Y：最高可砍金额,X：参与商品最低折扣
   */
  BARGAIN_PRE_WHOLE_NOTICE(
      BARGAIN,"砍价活动预告",
      1,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 超级品类日整体活动上线预告
   * Date : 品类日上线前一日晚21点
   * need param ZZ:品类名,X：活动商品件数
   */
  BARGAIN_WHOLE_NOTICE(
      BARGAIN,"品类日活动上线预告",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 单日活动上线预告
   * Date : 品类日每天10点
   * need param ZZ:品类名,X：活动商品件数
   */
  BARGAIN_SINGLE_START(
      BARGAIN,"上线提醒",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 砍价倒计时
   * Date : 结束前15分钟
   * need param  A：用户 B：拼团产品,
   */
  BARGAIN_COUNTDOWN(
      BARGAIN,"砍价倒计时",
      1,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-15 * 60);
    }
  },

  /**
   * 砍价结果通知
   * Date : 砍价结束
   * need param  A：用户名 B：拼团产品
   */
  BARGAIN_RESULT(BARGAIN,"砍价结束",
      2,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 每场红包雨开始前提醒
   * Date : 开始前5分钟
   */
  MONEY_RAIN_START(BARGAIN,"红包雨开始提醒",
      1,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,-5 * 60);
    }
  },

  /**
   * 爆发日红包雨提醒
   * Date : 20号9点
   */
  MONEY_RAIN_BURST(BARGAIN,"爆发日红包雨提醒",
      2,1
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  },

  /**
   * 引导兑奖
   * Date : 中奖后半小时
   * need param  A：用户名 B：奖品/奖金
   */
  LUCK_DRAW_GUIDE(BARGAIN,"引导兑奖",
      2,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return DateUtils.addSeconds(estimatePushTime,10);
    }
  },

  /**
   * 活动倒计时提醒
   * Date : 爆发日21：30
   * need param  A：用户名 B：抽奖剩余次数
   */
  LUCK_DRAW_COUNTDOWN(BARGAIN,"抽奖倒计时",
      3,2
  ) {
    @Override
    Date calEstimatePushTime(Date estimatePushTime) {
      return estimatePushTime;
    }
  };

  ActionType(
      ActivityModule activityModule,String abbreviation,
      Integer priority, Integer pushUserType){
    this.activityModule = activityModule;
    this.abbreviation = abbreviation;
    this.priority = priority;

    this.pushUserType = pushUserType;
  }

  private final ActivityModule activityModule;
  private final String abbreviation;
  private final Integer priority;
  private final Integer pushUserType;
  private String content = "";

  public String getAbbreviation() {
    return abbreviation;
  }

  public Integer getPriority() {
    return priority;
  }

  public String getContent(){
    return content;
  }


  public ActivityPush createActivityPush(Date activityTime, String customUrl){
    return createActivityPush(activityTime,customUrl,null,null);
  }

  public ActivityPush createActivityPush(Date activityTime, String customUrl, Map<String, String> needParam){
    return createActivityPush(activityTime,customUrl,needParam,null);
  }

  private static ActivityPushDetailMapper activityPushDetailMapper;

  public static void setPushMapper(ActivityPushDetailMapper pushDetailMapper) {
    ActionType.activityPushDetailMapper = pushDetailMapper;
  }

  public ActivityPush createActivityPush(Date activityTime, String customUrl, Map<String, String> needParam, List<Long> cpIdList){
    ActivityPush ap = new ActivityPush();

    String needParamStr = castParamMap(needParam);
    String cpIds = castCpIdList(cpIdList);

    ap.setActivityModule(activityModule.description());
    ap.setAbbreviation(abbreviation);
    ap.setPriority(priority);
    ap.setPushUserType(pushUserType);
    Date date = calEstimatePushTime(activityTime);

    ActivityPushDetail activityPushDetail = activityPushDetailMapper.loadByTimeAndActionType(date, this);

    if (activityPushDetail != null){
      ap.setContent(activityPushDetail.getContent());
      this.content = activityPushDetail.getContent();
    }

    ap.setCustomUrl(customUrl);
    ap.setEstimatePushTime(date);

    ap.setNeedParam(needParamStr);
    ap.setPushUserCpIds(cpIds);

    return ap;
  }

  abstract Date calEstimatePushTime(Date estimatePushTime);
}
