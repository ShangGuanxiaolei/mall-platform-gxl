package com.xquark.dal.type;

/**
 * 分佣规则集定义
 */
public enum CommissionPolicy {
  DEFAULT, //默认设置分佣规则集
  USER_AGENT_DEFAULT, // 代理商默认分佣规则集
}
