package com.xquark.dal.type;

/**
 * @ClassName: IndexBannerDropStatusType
 * @Description: TODO
 * @Author: Kwonghom
 * @CreateDate: 2019/5/7 16:12
 * @Version: 1.0
 **/
public enum IndexBannerDropStatusType {

    ALL(1, "所有状态"),
    ONGOING(2, "进行中"),
    ABOUT_TO_BEGIN(3, "即将开始"),
    ENDED(4, "已结束");

    private int index;
    private String dropDesc;

    IndexBannerDropStatusType(int index, String dropDesc) {
        this.index = index;
        this.dropDesc = dropDesc;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDropDesc() {
        return dropDesc;
    }

    public void setDropDesc(String dropDesc) {
        this.dropDesc = dropDesc;
    }
}
