package com.xquark.dal.type;

public enum InviteCodeStatus {
	  NONE(-1), // 没有查到这个邀请码
	  NEVER(1), // 有邀请码，且未绑定
	  ALREADY(2), // 有邀请码，且已绑定自己
	  USED(-2); // 有邀请码，但绑定他人
	 

	  private int code;

	  InviteCodeStatus(int code) {
	    this.code = code;
	  }

	  public int getCode() {
	    return this.code;
	  }
}
