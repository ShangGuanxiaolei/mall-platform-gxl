package com.xquark.dal.type;

public enum RulesType {

    RULE_FRESHMAN(1, "新人升级规则"),
    RULE_POINT(2, "德分规则");

    private int index;
    private String name;

    RulesType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
