package com.xquark.dal.type;

public enum EmpowerCode {

    /**
     *邀请码相关说明
     */
    EmpowerText("邀请码相关说明");

    private String word;

    

    EmpowerCode(String word) {
        this.word=word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
