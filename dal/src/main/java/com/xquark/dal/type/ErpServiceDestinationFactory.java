package com.xquark.dal.type;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Jack Zhu
 * @date 2019/02/28
 */
public class ErpServiceDestinationFactory {
    private static Map<String, String> set = new ConcurrentHashMap<>();
    static {
        set.put("NOT_REFUNDABLE", "NOT_REFUNDABLE");
        set.put("REFUNDABLE", "REFUNDABLE");
        set.put("SENMIAO_WANGDIAN", "SENMIAO_WANGDIAN");
        set.put("SENMIAO_SHANHUYUN", "SENMIAO_SHANHUYUN");
        set.put("NO_DESTINATION", "NO_DESTINATION");
        set.put("01_REFUNDABLE", "CLIENT_REFUNDABLE");
        set.put("01_NOT_REFUNDABLE", "CLIENT_NOT_REFUNDABLE");
        set.put("JK_REFUNDABLE", "BLP_REFUNDABLE");
        set.put("JK_NOT_REFUNDABLE", "BLP_NOT_REFUNDABLE");
    }

    public static void delete(String key) {
        String enableKey = key + "_REFUNDABLE";
        String unableKey = key + "_NOT_REFUNDABLE";
        set.remove(enableKey);
        set.remove(unableKey);
    }
    public static String getErp(String erp) {
       return set.get(erp);
    }

    public static void  put(String key, String value ) {
        set.put(key, value);
    }

}
