package com.xquark.dal.type;

/**
 * 支付渠道
 * @author
 */
public class PayChannelConst {

    //支付宝
    public static final String ALIPAY = "alipay";

    //微信支付
    public static final String WECHAT = "wechat";

    //汇付天下
    public static final String PNRPAY = "pnrpay";

    //易办事
    public static final String EBANKPAY = "ebs";
    
    public static boolean checkPayChannel(String payChannel) {
    	if(ALIPAY.equalsIgnoreCase(payChannel) 
    			|| WECHAT.equalsIgnoreCase(payChannel)
    			|| PNRPAY.equalsIgnoreCase(payChannel)
    			|| EBANKPAY.equalsIgnoreCase(payChannel))
    		return true;
    	else return false;
    }
    
    public static final String DEFAULT_FROM_SYSTEM = "hw";

}

