package com.xquark.dal.type;

public enum MessageType {
  /**
   * 系统消息
   */
  SYSTEM,
  /**
   * 业务信息
   */
  BUSSINESS,
  /**
   * 用户消息
   */
  USER,

  /**
   * 通知
   */
  NOTIFICATION,

  /**
   * 全局通知
   */
  NOTIFICATION_ALL,
}
