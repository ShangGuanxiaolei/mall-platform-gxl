package com.xquark.dal.type;

/**
 * Created by IntelliJ IDEA. User: huangjie Date: 2018/6/8. Time: 下午4:28 重量单位
 */
public enum WeightUnit {
  KG,//千克
  G,//克
  T;//吨
}
