package com.xquark.dal.type;

public enum ActivityStatus {
  /*将发布和创建活动分开的时候，需要添加的类型
  TO_AUDIT,// 待审核
  PASS, // 审核通过
    */
  NOT_STARTED, // 未开始
  IN_PROGRESS,  // 进行中
  CLOSED // 结束
}
