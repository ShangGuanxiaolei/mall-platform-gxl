package com.xquark.dal.type;

/**
 * @ClassName: IndexBannerJumpType
 * @Description: 首页 banner 跳转类型
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 13:32
 * @Version: 1.0
 **/
public enum IndexBannerJumpType {
    PRODUCT, //单个商品
    SECOND_CATEGORY, //二级分类
    CUSTOMER, //自定义
    MODULE, //模块ID
    NO_JUMP //不跳转
}
