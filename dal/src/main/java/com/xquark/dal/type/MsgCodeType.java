package com.xquark.dal.type;

import java.util.ArrayList;
import java.util.List;

/**
 * created by
 *
 * @author wangxinhua at 18-6-11 下午8:53
 */
public enum MsgCodeType {
  LIGHTEN;

  public final static List<String> VALUES_LOWER;

  static {
    MsgCodeType[] values = MsgCodeType.values();
    VALUES_LOWER = new ArrayList<>(values.length);
    for (MsgCodeType val : values) {
      VALUES_LOWER.add(val.name().toLowerCase());
    }
  }
}
