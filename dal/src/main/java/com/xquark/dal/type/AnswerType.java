package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-7-11. DESC:
 */
public enum AnswerType {

  NO(0), // 否
  YES(1), // 是
  NEVER(2), // 没有
  FEW(3), // 很少
  SOMETIMES(4), // 有时
  OFTEN(5), // 经常
  ALWAYS(6); // 总是

  private int code;

  AnswerType(int code) {
    this.code = code;
  }

  public int getCode() {
    return this.code;
  }

}
