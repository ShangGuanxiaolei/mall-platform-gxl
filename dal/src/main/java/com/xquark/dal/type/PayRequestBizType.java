package com.xquark.dal.type;

public enum PayRequestBizType {
  ORDER, // 订单
  REFUND, // 订单退款
  BATCH_PAY, // 批量付款
  WITHDRAW, // 提现`
  COMMISSION //佣金
}
