package com.xquark.dal.type;

public enum CommissionType {
  KKKD,             //偶尔佣金
  PLATFORM,         //平台合伙人佣金
  CPS,           // CPS佣金
  PARTNER,         //合伙人佣金
  PRODUCT,         //推客购物佣金
  ONECOMMISSION,     //一级分佣佣金
  TWOCOMMISSION,     //二级分佣佣金
  THREECOMMISSION,   //三级分佣佣金
  TEAM,              //团队合伙人佣金
  SHAREHOLDER,       //股东合伙人佣金
  AREA,              //区域合伙人佣金
  CUSTOMAREA,         //自定义区域

  TEAM_LEADER,       //战队队长佣金
  TEAM_MEMBER,       //战队成员佣金

  SILVER,
  GLOD,
  GENERAL_DIRECT_TO_DIRECTOR, //董事的直接下级总顾问订单分润给董事(董事->(直招)总顾问)
  GENERAL_INDIRECT_TO_DIRECTOR, //董事的间接下级总顾问订单分润给董事(董事->(直招)总顾问->(间招)总顾问)
  DIRECTORS_TEAM_TO_DIRECTORS, //董事下属团队(包括董事本身)订单需要分润给上级和上上级董事(无论中间包含多少其他人,只到找到总部为止)
  TEAM_ORDER, //针对联合创始人的封地政策，订单为该区域的所有订单，负责本区域的联合创始人每一盒订单都可以奖励1元
  AREA_ORDER,
  GENERAL_DIRECT_TO_GENERAL //成为总顾问后，直接推荐一位总顾问，一次性返利2000元

}
