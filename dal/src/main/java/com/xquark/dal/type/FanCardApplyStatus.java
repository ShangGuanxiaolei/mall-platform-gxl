package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-11-6. DESC:
 */
public enum FanCardApplyStatus {
  APPLYING, REJECTED, SUCCEED,  // 申请成功
  USED // 该范卡已被兑换;
}
