package com.xquark.dal.type;

public enum PushMessageType {
  MESSAGE, NOTIFICATION
}
