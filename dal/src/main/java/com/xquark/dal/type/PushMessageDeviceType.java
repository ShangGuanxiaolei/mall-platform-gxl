package com.xquark.dal.type;

public enum PushMessageDeviceType {
  ANDROID,
  IOS,
  WEB,
  PC,
  WP,
  ALL
}
