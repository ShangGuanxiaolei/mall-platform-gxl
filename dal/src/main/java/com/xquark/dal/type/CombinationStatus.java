package com.xquark.dal.type;

/**
 * Created by IntelliJ IDEA. User: byy Date: 18-6-1. Time: 下午7:44 ${DESCRIPTION}
 */
public enum CombinationStatus {
    ONSALE, // 上架
    INSTOCK, // 下架
    UNDETERMINED //待定
}
