package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
public enum PromotionDiscountType {
  CASH, // 现金折扣
  PERCENT, // 按比例折扣
}
