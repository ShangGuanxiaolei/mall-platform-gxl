package com.xquark.dal.type;

/**
 * Created by wangxinhua on 2018/4/12. DESC: 活动针对人群
 */
public enum PromotionUserScope {

  ALL, EMPLOYEE

}
