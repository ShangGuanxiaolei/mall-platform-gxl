package com.xquark.dal.type;

public enum PromotionScope {
  SHOP, ALL, PRODUCT
}
