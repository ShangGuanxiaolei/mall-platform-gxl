package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-5-2. DESC:
 */
public enum YundouOperationType {
  // 完善信息
  IMPROVE_INFORMATION(1001),
  // 确认收货
  CONFIRM_RECEIPT(2001),
  // 完成订单
  FINISH_ORDER(2002),
  // 订单退款
  REFUND_ORDER(2003),

  // 签到
  SIGN_IN(3001),

  // 范卡积分兑换
  FANCARD_EXCHANGE(4001);

  private int code;

  YundouOperationType(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }
}
