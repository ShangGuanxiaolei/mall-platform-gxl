package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-5-2. DESC: 已停止使用
 */
@Deprecated
public class YundouRuleType {

  // 完善手机信息送积分
  public static final int IMPROVE_PHONE_INFORMATION = 10011;
  // 确认收货送积分
  public static final int CONFIRM_RECEIPT = 20011;
  // 完成订单送积分
  public static final int FINISH_ORDER = 20021;
  // 订单金额满100
  public static final int FINISH_ORDER_SUM_100 = 20022;
  // 订单金额满200
  public static final int FINISH_ORDER_SUM_200 = 20023;
  // 订单退款
  public static final int REFUND_ORDER = 20031;

  // 签到送积分
  public static final int SIGN_IN = 30011;

}
