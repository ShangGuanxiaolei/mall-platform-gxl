package com.xquark.dal.type;

/**
 * Created by wangxinhua on 17-7-14. DESC:
 */
public enum BMIMessage {

  TOO_FAT("肥胖", "请改善身体状态哦"),
  TOO_THIN("偏瘦", "请改善身体状态哦"),
  REGULAR("正常", "请保持身体状态哦");

  private String message;
  private String status;

  public String getMessage() {
    return this.message;
  }

  public String getStatus() {
    return status;
  }

  private BMIMessage(String status, String message) {
    this.status = status;
    this.message = message;
  }

  public static BMIMessage valueOf(double bmi) {
    if (bmi <= 18) {
      return TOO_THIN;
    } else if (bmi > 18 && bmi < 29) {
      return REGULAR;
    } else {
      return TOO_FAT;
    }
  }

}
