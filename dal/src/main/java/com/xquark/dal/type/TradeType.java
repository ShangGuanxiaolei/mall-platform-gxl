package com.xquark.dal.type;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 上午9:52
 */
public enum TradeType {

  /**
   * 小程序
   */
  MINIPROGRAM,

  /**
   * APP
   */
  APP,

  /**
   * h5
   */
  H5,

  /**
   * TODO 暂时兼容老的支付宝配置
   */
  WEB,

  /**
   * 支付宝
   */
  ALIPAY

}
