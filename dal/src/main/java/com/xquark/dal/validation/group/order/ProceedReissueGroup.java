package com.xquark.dal.validation.group.order;

import javax.validation.groups.Default;

/**
 * 补货进行中校验分组
 */
public interface ProceedReissueGroup extends Default {

}
