package com.xquark.dal.validation.group.order;

import javax.validation.groups.Default;

/**
 * 申请补货校验分组
 */
public interface ApplyForReissueGroup extends Default {

}
