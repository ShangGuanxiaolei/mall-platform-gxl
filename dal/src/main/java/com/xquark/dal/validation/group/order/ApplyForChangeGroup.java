package com.xquark.dal.validation.group.order;

import javax.validation.groups.Default;

/**
 * 申请换货校验分组
 */
public interface ApplyForChangeGroup extends Default {

}
