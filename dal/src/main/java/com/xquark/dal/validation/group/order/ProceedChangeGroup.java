package com.xquark.dal.validation.group.order;

import javax.validation.groups.Default;

/**
 * 换货进行中校验分组
 */
public interface ProceedChangeGroup extends Default {

}
