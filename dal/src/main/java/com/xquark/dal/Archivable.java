package com.xquark.dal;

public interface Archivable extends BaseEntity {

  Boolean getArchive();

  void setArchive(Boolean archive);

}
