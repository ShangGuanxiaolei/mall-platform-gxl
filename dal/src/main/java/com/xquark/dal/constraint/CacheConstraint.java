package com.xquark.dal.constraint;

/**
 * Created by wangxinhua on 17-11-16. DESC: 缓存静态常量类
 */
public class CacheConstraint {

  /**
   * 商品的标签zSet键名称
   */
  public static final String P_TAG_Z_SET_KEY = "productTags";

  private static final String TAG_P_KEY_PREFIX = "tag:product:";

  public static String getTagProductKey(String tagName) {
    return TAG_P_KEY_PREFIX.concat(tagName);
  }

}
