package com.xquark.dal;

/**
 * Created by wangxinhua on 17-12-15. DESC:
 */
public interface Shareable {

  String getShareTitle();

  String getShareImg();

}
