package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.FanCardApplyStatus;
import com.xquark.dal.type.GenderType;

import java.util.Date;

/**
 * @author wangxinhua
 */
public class FanCardApply extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = -8100351416460042862L;

  private String id;

  /**
   * 申请人用户id
   */
  private String userId;

  /**
   * 申请人姓名
   */
  private String name;

  /**
   * 申请人性别
   */
  private GenderType gender;

  /**
   * 年龄
   */
  private Integer age;

  /**
   * 手机号
   */
  private String phone;

  /**
   * 身份证
   */
  private String idCard;

  /**
   * 邮箱
   */
  private String email;

  /**
   * 微信号
   */
  private String wechatNo;

  /**
   * 住址
   */
  private String address;

  /**
   * 申请状态 APPLYING REJECTED SUCCEED
   */
  private FanCardApplyStatus status;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getIdCard() {
    return idCard;
  }

  public void setIdCard(String idCard) {
    this.idCard = idCard;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getWechatNo() {
    return wechatNo;
  }

  public void setWechatNo(String wechatNo) {
    this.wechatNo = wechatNo;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public FanCardApplyStatus getStatus() {
    return status;
  }

  public void setStatus(FanCardApplyStatus status) {
    this.status = status;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}