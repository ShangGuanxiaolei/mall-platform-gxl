package com.xquark.dal.model;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/13
 * Time:17:38
 * Des:
 */
public class FreshManUpGradeVo {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT,
//    `serial_no` bigint(20) NOT NULL COMMENT '新客引导配置号',
//    `grade` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '等级',
//    `amount` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '累计消费金额（首次达到）',
//    `point` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '赠送德分数',
//    `gift` int(11) NOT NULL DEFAULT '1' COMMENT '是否赠送德分，1是，0否',
//    `guide_text` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '引导文字，顶部大字',
//    `step` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '节点文字',
//    `tip` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附加提示文字',
//    `ext` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '扩展文字信息',
    private Long id;
    private int serialNo;
    private String grade;
    private String amount;
    private String point;
    private int gift;
    private String guideText;
    private String step;
    private String tip;
    private String ext;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(int serialNo) {
        this.serialNo = serialNo;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public int getGift() {
        return gift;
    }

    public void setGift(int gift) {
        this.gift = gift;
    }

    public String getGuideText() {
        return guideText;
    }

    public void setGuideText(String guideText) {
        this.guideText = guideText;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}