package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * 首页新icon实体类
 */
public class Iconbar extends BaseEntityImpl {

    private static final long serialVersionUID = 5915467280490764274L;

    private String position; //icon显示位置

    private String name; //显示名称

    private String iconUrl; //icon来源

    private Integer type; //icon类型

    private Integer optional; //icon多选一(根据当前用户身份判断)

    private Integer sort; //icon排序

    private Integer status; //icon状态

    private String searchKey; //搜索关键字

    private String upgrade; //权益身份

    private String jumpUrl;

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public String getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOptional() {
        return optional;
    }

    public void setOptional(Integer optional) {
        this.optional = optional;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
