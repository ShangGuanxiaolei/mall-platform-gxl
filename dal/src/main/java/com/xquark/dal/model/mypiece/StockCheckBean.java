package com.xquark.dal.model.mypiece;

/**
 *  @author  gx
 * @date 2018-09-04
 */
public class StockCheckBean {
    /**
     * 活动编码
     */
    private String p_code;
    /**
     * Sku编码
     */
    private String sku_code;
    /**
     * 库存检查所需库存
     */
    private int p_sku_num;
    /**
     * 拼团成功消耗库存
     */
    private int resume_sku_num;

    private String tranCode;

    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public String getP_code() {
        return p_code;
    }

    public void setP_code(String p_code) {
        this.p_code = p_code;
    }

    public String getSku_code() {
        return sku_code;
    }

    public void setSku_code(String sku_code) {
        this.sku_code = sku_code;
    }

    public int getP_sku_num() {
        return p_sku_num;
    }

    public void setP_sku_num(int p_sku_num) {
        this.p_sku_num = p_sku_num;
    }

    public int getResume_sku_num() {
        return resume_sku_num;
    }

    public void setResume_sku_num(int resume_sku_num) {
        this.resume_sku_num = resume_sku_num;
    }
}
