package com.xquark.dal.model;

import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午3:16
 * 个人发票
 */
public class PersonalBill extends  BaseBill{

    /**
     * 接受手机号
     */
    @NotBlank(groups = {ElectronicGroup.class}, message = "手机号不能为空")
    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$", groups = {
        ElectronicGroup.class}, message = "手机号的格式不符合规范")
    private String receivePhone;


    /**
     * 接受邮箱
     */
    @Email(groups = {ElectronicGroup.class}, message = "邮箱的格式不符合规范")
    @NotBlank(groups = {ElectronicGroup.class}, message = "接受邮箱不能为空")
    private String receiveMail;

    /**
     * 姓名
     */
    @NotBlank(groups = {NormalGroup.class}, message = "姓名不能为空")
    private String name;

    public void setReceivePhone(String receivePhone) {
        this.receivePhone = receivePhone;
    }

    public void setReceiveMail(String receiveMail) {
        this.receiveMail = receiveMail;
    }

    public String getReceivePhone() {
        return receivePhone;
    }

    public String getReceiveMail() {
        return receiveMail;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return super.toString()+"PersonalBill{" +
                "receivePhone='" + receivePhone + '\'' +
                ", receiveMail='" + receiveMail + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
