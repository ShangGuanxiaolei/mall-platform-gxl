package com.xquark.dal.model.pay;

/**
 * Created by wangxinhua. Date: 2018/8/27 Time: 上午10:04
 */
public interface PaymentConfig {

  String getAppId();

  String getAppSecret();

  /**
   * 商户id
   */
  String getMchId();

  String getPaymentId();

  String getCallbackSite();

  /**
   * 回调通知地址
   */
  String getNotifySite();

  /**
   * 商户证书
   */
  String getMchSecret();

}
