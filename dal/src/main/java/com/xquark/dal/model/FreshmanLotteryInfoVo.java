package com.xquark.dal.model;

import java.io.Serializable;
import java.util.List;

public class FreshmanLotteryInfoVo implements Serializable {

    private static final long serialVersionUID = -7064315390221630935L;

    private int lotteryCount; //剩余抽奖次数

    private List<Integer> pointList; //抽奖德分

    public int getLotteryCount() {
        return lotteryCount;
    }

    public void setLotteryCount(int lotteryCount) {
        this.lotteryCount = lotteryCount;
    }

    public List<Integer> getPointList() {
        return pointList;
    }

    public void setPointList(List<Integer> pointList) {
        this.pointList = pointList;
    }
}
