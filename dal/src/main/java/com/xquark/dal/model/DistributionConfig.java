package com.xquark.dal.model;

import java.math.BigDecimal;
import java.util.Date;

import com.xquark.dal.BaseEntityImpl;

public class DistributionConfig extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String masterId;

  private String promoter;

  private BigDecimal cpsRate;

  private Integer cpsMaxAge;

  private Date createdAt;

  private Date updatedAt;

  public String getMasterId() {
    return masterId;
  }

  public void setMasterId(String masterId) {
    this.masterId = masterId;
  }

  public String getPromoter() {
    return promoter;
  }

  public void setPromoter(String promoter) {
    this.promoter = promoter;
  }

  public BigDecimal getCpsRate() {
    return cpsRate;
  }

  public void setCpsRate(BigDecimal cpsRate) {
    this.cpsRate = cpsRate;
  }

  public Integer getCpsMaxAge() {
    return cpsMaxAge;
  }

  public void setCpsMaxAge(Integer cpsMaxAge) {
    this.cpsMaxAge = cpsMaxAge;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }
}