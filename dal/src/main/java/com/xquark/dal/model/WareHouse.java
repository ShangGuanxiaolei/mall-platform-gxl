package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: shihao Date: 18-6-14. Time: 上午10:06 发货仓库
 */
public class WareHouse extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  @NotBlank
  private String name;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;

  }

  @Override
  public String toString() {
    return "WareHouse{" +
        "archive=" + archive +
        ", name='" + name + '\'' +
        '}';
  }
}
