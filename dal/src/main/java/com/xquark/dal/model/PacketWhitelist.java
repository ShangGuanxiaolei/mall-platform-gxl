package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/** @author wangxinhua */
public class PacketWhitelist implements Serializable {

  private Long id;

  /** cpId */
  private Long cpId;

  private Date createdAt;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
}
