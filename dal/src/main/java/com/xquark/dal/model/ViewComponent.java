package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * Created by chh on 16-10-17.
 */
public class ViewComponent extends BaseEntityImpl implements Archivable {

  private String vcName;
  private String description;
  private String content;
  private String contentParamDefinition;
  private boolean archive;

  public String getVcName() {
    return vcName;
  }

  public void setVcName(String vcName) {
    this.vcName = vcName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContentParamDefinition() {
    return contentParamDefinition;
  }

  public void setContentParamDefinition(String contentParamDefinition) {
    this.contentParamDefinition = contentParamDefinition;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
