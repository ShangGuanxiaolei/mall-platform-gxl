package com.xquark.dal.model;

import java.util.Date;
import java.util.Objects;

/**
 * @author wangxinhua
 * @date 2019-05-18
 * @since 1.0
 */
public class WinningItem implements Comparable<WinningItem> {

    private final int prize;

    private final Date expectTime;

    public WinningItem(int prize, Date expectTime) {
        this.prize = prize;
        this.expectTime = expectTime;
    }

    public int getPrize() {
        return prize;
    }

    public Date getExpectTime() {
        return expectTime;
    }

    @Override
    public String toString() {
        return "WinningItem{" +
                "prize=" + prize +
                ", expectTime=" + expectTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WinningItem that = (WinningItem) o;
        return prize == that.prize &&
                Objects.equals(expectTime, that.expectTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prize, expectTime);
    }

    @Override
    public int compareTo(WinningItem o) {
        if (o == null) {
            return -1;
        }
        return this.getExpectTime().compareTo(o.getExpectTime());
    }
}

