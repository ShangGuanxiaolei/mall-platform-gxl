package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * timesettings
 *
 * @author wangxinhua
 */
public class TimeSettings implements Serializable {

  private String name;

  private String timeValue;

  private Date startDate;

  private Date endDate;

  private String description;

  private static final long serialVersionUID = 1L;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTimeValue() {
    return timeValue;
  }

  public void setTimeValue(String timeValue) {
    this.timeValue = timeValue;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    TimeSettings other = (TimeSettings) that;
    return
        (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getTimeValue() == null ? other.getTimeValue() == null
            : this.getTimeValue().equals(other.getTimeValue()))
            && (this.getStartDate() == null ? other.getStartDate() == null
            : this.getStartDate().equals(other.getStartDate()))
            && (this.getEndDate() == null ? other.getEndDate() == null
            : this.getEndDate().equals(other.getEndDate()))
            && (this.getDescription() == null ? other.getDescription() == null
            : this.getDescription().equals(other.getDescription()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
    result = prime * result + ((getTimeValue() == null) ? 0 : getTimeValue().hashCode());
    result = prime * result + ((getStartDate() == null) ? 0 : getStartDate().hashCode());
    result = prime * result + ((getEndDate() == null) ? 0 : getEndDate().hashCode());
    result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", name=").append(name);
    sb.append(", timeValue=").append(timeValue);
    sb.append(", startDate=").append(startDate);
    sb.append(", endDate=").append(endDate);
    sb.append(", description=").append(description);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}