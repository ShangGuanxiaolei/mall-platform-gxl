package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Function;

/** @author hongwentao */
public class PromotionBargainDetail extends BaseEntityArchivableImpl {

  /**
   * 根据用户id，规则及商品初始化一个砍价详情
   *
   * @param userId 用户id
   * @param product 商品对象
   * @param discountFunc 砍价折扣的方法
   * @return 构造的初始化砍价详情
   */
  public static PromotionBargainDetail init(
      String userId,
      PromotionBargainProduct product,
      Function<BigDecimal, BigDecimal> discountFunc) {
    PromotionBargainDetail detail = new PromotionBargainDetail();
    BigDecimal priceStart = product.getPriceStart();
    BigDecimal launchDiscount = discountFunc.apply(priceStart);
    detail.setBargainId(product.getBargainId());
    detail.setSkuId(product.getSkuId());
    detail.setUserId(userId);
    detail.setLaunchDiscount(launchDiscount);
    detail.setCurrTimes(1);
    detail.setCurrDiscount(priceStart.subtract(launchDiscount));
    return detail;
  }

  private String id;

  /** 砍价活动表id */
  private String bargainId;

  /** 砍价商品sku_id */
  private String skuId;

  /** 砍价发起人id */
  private String userId;

  /** 发起折扣 */
  private BigDecimal launchDiscount;

  /** 当前砍价次数 */
  private Integer currTimes;

  /** 当前金额 */
  private BigDecimal currDiscount;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public BigDecimal getLaunchDiscount() {
    return launchDiscount;
  }

  public void setLaunchDiscount(BigDecimal launchDiscount) {
    this.launchDiscount = launchDiscount;
  }

  public Integer getCurrTimes() {
    return currTimes;
  }

  public void setCurrTimes(Integer currTimes) {
    this.currTimes = currTimes;
  }

  public BigDecimal getCurrDiscount() {
    return currDiscount;
  }

  public void setCurrDiscount(BigDecimal currDiscount) {
    this.currDiscount = currDiscount;
  }

  @JsonIgnore
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @JsonIgnore
  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @JsonIgnore
  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
