package com.xquark.dal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HealthTestProfileExample {

  protected String orderByClause;

  protected boolean distinct;

  protected List<Criteria> oredCriteria;

  private Integer limit;

  private Integer offset;

  public HealthTestProfileExample() {
    oredCriteria = new ArrayList<Criteria>();
  }

  public void setOrderByClause(String orderByClause) {
    this.orderByClause = orderByClause;
  }

  public String getOrderByClause() {
    return orderByClause;
  }

  public void setDistinct(boolean distinct) {
    this.distinct = distinct;
  }

  public boolean isDistinct() {
    return distinct;
  }

  public List<Criteria> getOredCriteria() {
    return oredCriteria;
  }

  public void or(Criteria criteria) {
    oredCriteria.add(criteria);
  }

  public Criteria or() {
    Criteria criteria = createCriteriaInternal();
    oredCriteria.add(criteria);
    return criteria;
  }

  public Criteria createCriteria() {
    Criteria criteria = createCriteriaInternal();
    if (oredCriteria.size() == 0) {
      oredCriteria.add(criteria);
    }
    return criteria;
  }

  protected Criteria createCriteriaInternal() {
    Criteria criteria = new Criteria();
    return criteria;
  }

  public void clear() {
    oredCriteria.clear();
    orderByClause = null;
    distinct = false;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public Integer getOffset() {
    return offset;
  }

  protected abstract static class GeneratedCriteria {

    protected List<Criterion> idCriteria;

    protected List<Criterion> userIdCriteria;

    protected List<Criterion> allCriteria;

    protected List<Criterion> criteria;

    protected GeneratedCriteria() {
      super();
      criteria = new ArrayList<Criterion>();
      idCriteria = new ArrayList<Criterion>();
      userIdCriteria = new ArrayList<Criterion>();
    }

    public List<Criterion> getIdCriteria() {
      return idCriteria;
    }

    protected void addIdCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      idCriteria.add(new Criterion(condition, value, "idHandler"));
      allCriteria = null;
    }

    protected void addIdCriterion(String condition, String value1, String value2, String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      idCriteria.add(new Criterion(condition, value1, value2, "idHandler"));
      allCriteria = null;
    }

    public List<Criterion> getUserIdCriteria() {
      return userIdCriteria;
    }

    protected void addUserIdCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      userIdCriteria.add(new Criterion(condition, value, "idHandler"));
      allCriteria = null;
    }

    protected void addUserIdCriterion(String condition, String value1, String value2,
        String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      userIdCriteria.add(new Criterion(condition, value1, value2, "idHandler"));
      allCriteria = null;
    }

    public boolean isValid() {
      return criteria.size() > 0
          || idCriteria.size() > 0
          || userIdCriteria.size() > 0;
    }

    public List<Criterion> getAllCriteria() {
      if (allCriteria == null) {
        allCriteria = new ArrayList<Criterion>();
        allCriteria.addAll(criteria);
        allCriteria.addAll(idCriteria);
        allCriteria.addAll(userIdCriteria);
      }
      return allCriteria;
    }

    public List<Criterion> getCriteria() {
      return criteria;
    }

    protected void addCriterion(String condition) {
      if (condition == null) {
        throw new RuntimeException("Value for condition cannot be null");
      }
      criteria.add(new Criterion(condition));
      allCriteria = null;
    }

    protected void addCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      criteria.add(new Criterion(condition, value));
      allCriteria = null;
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      criteria.add(new Criterion(condition, value1, value2));
      allCriteria = null;
    }

    public Criteria andIdIsNull() {
      addCriterion("id is null");
      return (Criteria) this;
    }

    public Criteria andIdIsNotNull() {
      addCriterion("id is not null");
      return (Criteria) this;
    }

    public Criteria andIdEqualTo(String value) {
      addIdCriterion("id =", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotEqualTo(String value) {
      addIdCriterion("id <>", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdGreaterThan(String value) {
      addIdCriterion("id >", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
      addIdCriterion("id >=", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdLessThan(String value) {
      addIdCriterion("id <", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
      addIdCriterion("id <=", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdIn(List<String> values) {
      addIdCriterion("id in", values, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotIn(List<String> values) {
      addIdCriterion("id not in", values, "id");
      return (Criteria) this;
    }

    public Criteria andIdBetween(String value1, String value2) {
      addIdCriterion("id between", value1, value2, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
      addIdCriterion("id not between", value1, value2, "id");
      return (Criteria) this;
    }

    public Criteria andUserIdIsNull() {
      addCriterion("user_id is null");
      return (Criteria) this;
    }

    public Criteria andUserIdIsNotNull() {
      addCriterion("user_id is not null");
      return (Criteria) this;
    }

    public Criteria andUserIdEqualTo(String value) {
      addUserIdCriterion("user_id =", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdNotEqualTo(String value) {
      addUserIdCriterion("user_id <>", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdGreaterThan(String value) {
      addUserIdCriterion("user_id >", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdGreaterThanOrEqualTo(String value) {
      addUserIdCriterion("user_id >=", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdLessThan(String value) {
      addUserIdCriterion("user_id <", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdLessThanOrEqualTo(String value) {
      addUserIdCriterion("user_id <=", value, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdIn(List<String> values) {
      addUserIdCriterion("user_id in", values, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdNotIn(List<String> values) {
      addUserIdCriterion("user_id not in", values, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdBetween(String value1, String value2) {
      addUserIdCriterion("user_id between", value1, value2, "userId");
      return (Criteria) this;
    }

    public Criteria andUserIdNotBetween(String value1, String value2) {
      addUserIdCriterion("user_id not between", value1, value2, "userId");
      return (Criteria) this;
    }

    public Criteria andGenderIsNull() {
      addCriterion("gender is null");
      return (Criteria) this;
    }

    public Criteria andGenderIsNotNull() {
      addCriterion("gender is not null");
      return (Criteria) this;
    }

    public Criteria andGenderEqualTo(String value) {
      addCriterion("gender =", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderNotEqualTo(String value) {
      addCriterion("gender <>", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderGreaterThan(String value) {
      addCriterion("gender >", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderGreaterThanOrEqualTo(String value) {
      addCriterion("gender >=", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderLessThan(String value) {
      addCriterion("gender <", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderLessThanOrEqualTo(String value) {
      addCriterion("gender <=", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderLike(String value) {
      addCriterion("gender like", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderNotLike(String value) {
      addCriterion("gender not like", value, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderIn(List<String> values) {
      addCriterion("gender in", values, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderNotIn(List<String> values) {
      addCriterion("gender not in", values, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderBetween(String value1, String value2) {
      addCriterion("gender between", value1, value2, "gender");
      return (Criteria) this;
    }

    public Criteria andGenderNotBetween(String value1, String value2) {
      addCriterion("gender not between", value1, value2, "gender");
      return (Criteria) this;
    }

    public Criteria andHeightIsNull() {
      addCriterion("height is null");
      return (Criteria) this;
    }

    public Criteria andHeightIsNotNull() {
      addCriterion("height is not null");
      return (Criteria) this;
    }

    public Criteria andHeightEqualTo(Integer value) {
      addCriterion("height =", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightNotEqualTo(Integer value) {
      addCriterion("height <>", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightGreaterThan(Integer value) {
      addCriterion("height >", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightGreaterThanOrEqualTo(Integer value) {
      addCriterion("height >=", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightLessThan(Integer value) {
      addCriterion("height <", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightLessThanOrEqualTo(Integer value) {
      addCriterion("height <=", value, "height");
      return (Criteria) this;
    }

    public Criteria andHeightIn(List<Integer> values) {
      addCriterion("height in", values, "height");
      return (Criteria) this;
    }

    public Criteria andHeightNotIn(List<Integer> values) {
      addCriterion("height not in", values, "height");
      return (Criteria) this;
    }

    public Criteria andHeightBetween(Integer value1, Integer value2) {
      addCriterion("height between", value1, value2, "height");
      return (Criteria) this;
    }

    public Criteria andHeightNotBetween(Integer value1, Integer value2) {
      addCriterion("height not between", value1, value2, "height");
      return (Criteria) this;
    }

    public Criteria andWeightIsNull() {
      addCriterion("weight is null");
      return (Criteria) this;
    }

    public Criteria andWeightIsNotNull() {
      addCriterion("weight is not null");
      return (Criteria) this;
    }

    public Criteria andWeightEqualTo(Integer value) {
      addCriterion("weight =", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightNotEqualTo(Integer value) {
      addCriterion("weight <>", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightGreaterThan(Integer value) {
      addCriterion("weight >", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightGreaterThanOrEqualTo(Integer value) {
      addCriterion("weight >=", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightLessThan(Integer value) {
      addCriterion("weight <", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightLessThanOrEqualTo(Integer value) {
      addCriterion("weight <=", value, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightIn(List<Integer> values) {
      addCriterion("weight in", values, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightNotIn(List<Integer> values) {
      addCriterion("weight not in", values, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightBetween(Integer value1, Integer value2) {
      addCriterion("weight between", value1, value2, "weight");
      return (Criteria) this;
    }

    public Criteria andWeightNotBetween(Integer value1, Integer value2) {
      addCriterion("weight not between", value1, value2, "weight");
      return (Criteria) this;
    }

    public Criteria andPregnantIsNull() {
      addCriterion("pregnant is null");
      return (Criteria) this;
    }

    public Criteria andPregnantIsNotNull() {
      addCriterion("pregnant is not null");
      return (Criteria) this;
    }

    public Criteria andPregnantEqualTo(Boolean value) {
      addCriterion("pregnant =", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantNotEqualTo(Boolean value) {
      addCriterion("pregnant <>", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantGreaterThan(Boolean value) {
      addCriterion("pregnant >", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantGreaterThanOrEqualTo(Boolean value) {
      addCriterion("pregnant >=", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantLessThan(Boolean value) {
      addCriterion("pregnant <", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantLessThanOrEqualTo(Boolean value) {
      addCriterion("pregnant <=", value, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantIn(List<Boolean> values) {
      addCriterion("pregnant in", values, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantNotIn(List<Boolean> values) {
      addCriterion("pregnant not in", values, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantBetween(Boolean value1, Boolean value2) {
      addCriterion("pregnant between", value1, value2, "pregnant");
      return (Criteria) this;
    }

    public Criteria andPregnantNotBetween(Boolean value1, Boolean value2) {
      addCriterion("pregnant not between", value1, value2, "pregnant");
      return (Criteria) this;
    }

    public Criteria andBmiIsNull() {
      addCriterion("bmi is null");
      return (Criteria) this;
    }

    public Criteria andBmiIsNotNull() {
      addCriterion("bmi is not null");
      return (Criteria) this;
    }

    public Criteria andBmiEqualTo(Double value) {
      addCriterion("bmi =", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiNotEqualTo(Double value) {
      addCriterion("bmi <>", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiGreaterThan(Double value) {
      addCriterion("bmi >", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiGreaterThanOrEqualTo(Double value) {
      addCriterion("bmi >=", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiLessThan(Double value) {
      addCriterion("bmi <", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiLessThanOrEqualTo(Double value) {
      addCriterion("bmi <=", value, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiIn(List<Double> values) {
      addCriterion("bmi in", values, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiNotIn(List<Double> values) {
      addCriterion("bmi not in", values, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiBetween(Double value1, Double value2) {
      addCriterion("bmi between", value1, value2, "bmi");
      return (Criteria) this;
    }

    public Criteria andBmiNotBetween(Double value1, Double value2) {
      addCriterion("bmi not between", value1, value2, "bmi");
      return (Criteria) this;
    }

    public Criteria andMedicineIsNull() {
      addCriterion("medicine is null");
      return (Criteria) this;
    }

    public Criteria andMedicineIsNotNull() {
      addCriterion("medicine is not null");
      return (Criteria) this;
    }

    public Criteria andMedicineEqualTo(String value) {
      addCriterion("medicine =", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineNotEqualTo(String value) {
      addCriterion("medicine <>", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineGreaterThan(String value) {
      addCriterion("medicine >", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineGreaterThanOrEqualTo(String value) {
      addCriterion("medicine >=", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineLessThan(String value) {
      addCriterion("medicine <", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineLessThanOrEqualTo(String value) {
      addCriterion("medicine <=", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineLike(String value) {
      addCriterion("medicine like", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineNotLike(String value) {
      addCriterion("medicine not like", value, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineIn(List<String> values) {
      addCriterion("medicine in", values, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineNotIn(List<String> values) {
      addCriterion("medicine not in", values, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineBetween(String value1, String value2) {
      addCriterion("medicine between", value1, value2, "medicine");
      return (Criteria) this;
    }

    public Criteria andMedicineNotBetween(String value1, String value2) {
      addCriterion("medicine not between", value1, value2, "medicine");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIsNull() {
      addCriterion("created_at is null");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIsNotNull() {
      addCriterion("created_at is not null");
      return (Criteria) this;
    }

    public Criteria andCreatedAtEqualTo(Date value) {
      addCriterion("created_at =", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotEqualTo(Date value) {
      addCriterion("created_at <>", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtGreaterThan(Date value) {
      addCriterion("created_at >", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtGreaterThanOrEqualTo(Date value) {
      addCriterion("created_at >=", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtLessThan(Date value) {
      addCriterion("created_at <", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtLessThanOrEqualTo(Date value) {
      addCriterion("created_at <=", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIn(List<Date> values) {
      addCriterion("created_at in", values, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotIn(List<Date> values) {
      addCriterion("created_at not in", values, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtBetween(Date value1, Date value2) {
      addCriterion("created_at between", value1, value2, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotBetween(Date value1, Date value2) {
      addCriterion("created_at not between", value1, value2, "createdAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIsNull() {
      addCriterion("updated_at is null");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIsNotNull() {
      addCriterion("updated_at is not null");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtEqualTo(Date value) {
      addCriterion("updated_at =", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotEqualTo(Date value) {
      addCriterion("updated_at <>", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtGreaterThan(Date value) {
      addCriterion("updated_at >", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtGreaterThanOrEqualTo(Date value) {
      addCriterion("updated_at >=", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtLessThan(Date value) {
      addCriterion("updated_at <", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtLessThanOrEqualTo(Date value) {
      addCriterion("updated_at <=", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIn(List<Date> values) {
      addCriterion("updated_at in", values, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotIn(List<Date> values) {
      addCriterion("updated_at not in", values, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtBetween(Date value1, Date value2) {
      addCriterion("updated_at between", value1, value2, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotBetween(Date value1, Date value2) {
      addCriterion("updated_at not between", value1, value2, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andArchiveIsNull() {
      addCriterion("archive is null");
      return (Criteria) this;
    }

    public Criteria andArchiveIsNotNull() {
      addCriterion("archive is not null");
      return (Criteria) this;
    }

    public Criteria andArchiveEqualTo(Boolean value) {
      addCriterion("archive =", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotEqualTo(Boolean value) {
      addCriterion("archive <>", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveGreaterThan(Boolean value) {
      addCriterion("archive >", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveGreaterThanOrEqualTo(Boolean value) {
      addCriterion("archive >=", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveLessThan(Boolean value) {
      addCriterion("archive <", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveLessThanOrEqualTo(Boolean value) {
      addCriterion("archive <=", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveIn(List<Boolean> values) {
      addCriterion("archive in", values, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotIn(List<Boolean> values) {
      addCriterion("archive not in", values, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveBetween(Boolean value1, Boolean value2) {
      addCriterion("archive between", value1, value2, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotBetween(Boolean value1, Boolean value2) {
      addCriterion("archive not between", value1, value2, "archive");
      return (Criteria) this;
    }
  }

  /**
   */
  public static class Criteria extends GeneratedCriteria {

    protected Criteria() {
      super();
    }
  }

  public static class Criterion {

    private String condition;

    private Object value;

    private Object secondValue;

    private boolean noValue;

    private boolean singleValue;

    private boolean betweenValue;

    private boolean listValue;

    private String typeHandler;

    public String getCondition() {
      return condition;
    }

    public Object getValue() {
      return value;
    }

    public Object getSecondValue() {
      return secondValue;
    }

    public boolean isNoValue() {
      return noValue;
    }

    public boolean isSingleValue() {
      return singleValue;
    }

    public boolean isBetweenValue() {
      return betweenValue;
    }

    public boolean isListValue() {
      return listValue;
    }

    public String getTypeHandler() {
      return typeHandler;
    }

    protected Criterion(String condition) {
      super();
      this.condition = condition;
      this.typeHandler = null;
      this.noValue = true;
    }

    protected Criterion(String condition, Object value, String typeHandler) {
      super();
      this.condition = condition;
      this.value = value;
      this.typeHandler = typeHandler;
      if (value instanceof List<?>) {
        this.listValue = true;
      } else {
        this.singleValue = true;
      }
    }

    protected Criterion(String condition, Object value) {
      this(condition, value, null);
    }

    protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
      super();
      this.condition = condition;
      this.value = value;
      this.secondValue = secondValue;
      this.typeHandler = typeHandler;
      this.betweenValue = true;
    }

    protected Criterion(String condition, Object value, Object secondValue) {
      this(condition, value, secondValue, null);
    }
  }
}