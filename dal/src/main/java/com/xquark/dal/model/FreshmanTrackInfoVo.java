package com.xquark.dal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 新人轨迹进度条信息
 */
public class FreshmanTrackInfoVo implements Serializable {

    private static final long serialVersionUID = -657397277703691558L;

    private BigDecimal consumedAmount; //已消费金额

    private String title; //进度条顶部文案

    private String identityEffectTime; //身份生效时间(11:30)

    private String activityEffectTime; //活动生效时间(2019年3月22日)

    private List<Map<String, Integer>> progressor; //节点信息

    public BigDecimal getConsumedAmount() {
        return consumedAmount;
    }

    public void setConsumedAmount(BigDecimal consumedAmount) {
        this.consumedAmount = consumedAmount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentityEffectTime() {
        return identityEffectTime;
    }

    public void setIdentityEffectTime(String identityEffectTime) {
        this.identityEffectTime = identityEffectTime;
    }

    public String getActivityEffectTime() {
        return activityEffectTime;
    }

    public void setActivityEffectTime(String activityEffectTime) {
        this.activityEffectTime = activityEffectTime;
    }

    public List<Map<String, Integer>> getProgressor() {
        return progressor;
    }

    public void setProgressor(List<Map<String, Integer>> progressor) {
        this.progressor = progressor;
    }
}
