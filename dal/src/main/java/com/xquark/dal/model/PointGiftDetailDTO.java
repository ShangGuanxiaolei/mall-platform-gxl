package com.xquark.dal.model;

/**
 * @author Jack Zhu
 * @since 2018/12/17
 */
public class PointGiftDetailDTO extends PointGiftDetail{
    private String packetType;

    public String getPacketType() {
        return packetType;
    }

    public void setPacketType(String packetType) {
        this.packetType = packetType;
    }
}
