package com.xquark.dal.model;

/**
 * @author gxl
 */
public enum  PromotionToVipType {

  /**
   * 拼团达到次数升级VIP
   */
  PIECE_TO_VIP
}