package com.xquark.dal.model.mypiece;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xquark.dal.status.PieceStatus;

import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/12 11:28
 */
public class PGTranInfoVo {

  private String pieceGroupTranCode;//拼团编码
  private String groupHeadId;//团长会员id
  private String groupOpenMemberId;//开团会员id
  private String groupSkuCode;//组团sku编码
  private String pCode;//活动编码
  private String pDetailCode;//活动详情编码
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date groupOpenTime;//组团时间
  private String shareLink;//分享短链
  private String pieceStatus;//拼团交易状态，0:开团未支付 1:已开团 2:已成团 3:库存不足取消 4:失效取消',
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date createdAt;//创建时间
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date updatedAt;//修改时间
  private int isDeleted;//删除标记
  private Date groupFinishTime; //成团时间

  public String getPieceGroupTranCode() {
    return pieceGroupTranCode;
  }

  public void setPieceGroupTranCode(String pieceGroupTranCode) {
    this.pieceGroupTranCode = pieceGroupTranCode;
  }

  public String getGroupHeadId() {
    return groupHeadId;
  }

  @JsonProperty("groupHeadId")
  public void setGroupHeadId(String groupHeadId) {
    this.groupHeadId = groupHeadId;
  }

  @JsonIgnore
  public void setGroupHeadId(Long groupHeadId) {
    if (groupHeadId == null) {
      this.groupHeadId = null;
    }
    this.groupHeadId = String.valueOf(groupHeadId);
  }

  public String getGroupOpenMemberId() {
    return groupOpenMemberId;
  }

  @JsonProperty("groupOpenMemberId")
  public void setGroupOpenMemberId(String groupOpenMemberId) {
    this.groupOpenMemberId = groupOpenMemberId;
  }

  @JsonIgnore
  public void setGroupOpenMemberId(Long groupOpenMemberId) {
    if (groupOpenMemberId == null) {
      this.groupOpenMemberId = null;
    }
    setGroupOpenMemberId(String.valueOf(groupOpenMemberId));
  }

  public String getGroupSkuCode() {
    return groupSkuCode;
  }

  public void setGroupSkuCode(String groupSkuCode) {
    this.groupSkuCode = groupSkuCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }

  public String getShareLink() {
    return shareLink;
  }

  public void setShareLink(String shareLink) {
    this.shareLink = shareLink;
  }

  public String getPieceStatus() {
    return pieceStatus;
  }

  @JsonProperty("pieceStatus")
  public void setPieceStatus(String pieceStatus) {
    this.pieceStatus = pieceStatus;
  }

  @JsonIgnore
  public void setPieceStatus(PieceStatus status) {
    setPieceStatus(String.valueOf(status.getCode()));
  }

  public int getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(int isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Date getGroupFinishTime() {
    return groupFinishTime;
  }

  public void setGroupFinishTime(Date groupFinishTime) {
    this.groupFinishTime = groupFinishTime;
  }
}
