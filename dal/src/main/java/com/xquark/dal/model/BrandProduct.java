package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * xquark_brand_product
 *
 * @author wangxinhua
 */
public class BrandProduct extends BaseEntityImpl {

  public BrandProduct() {
  }

  public BrandProduct(String productId, String brandId) {
    this.productId = productId;
    this.brandId = brandId;
  }

  /**
   * id
   */
  private String id;

  /**
   * 商品id
   */
  private String productId;

  /**
   * 品牌id
   */
  private String brandId;

  private Date createdAt;

  private Date updatedAt;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getBrandId() {
    return brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    BrandProduct other = (BrandProduct) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getProductId() == null ? other.getProductId() == null
        : this.getProductId().equals(other.getProductId()))
        && (this.getBrandId() == null ? other.getBrandId() == null
        : this.getBrandId().equals(other.getBrandId()))
        && (this.getCreatedAt() == null ? other.getCreatedAt() == null
        : this.getCreatedAt().equals(other.getCreatedAt()))
        && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null
        : this.getUpdatedAt().equals(other.getUpdatedAt()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
    result = prime * result + ((getBrandId() == null) ? 0 : getBrandId().hashCode());
    result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
    result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(id);
    sb.append(", productId=").append(productId);
    sb.append(", brandId=").append(brandId);
    sb.append(", createdAt=").append(createdAt);
    sb.append(", updatedAt=").append(updatedAt);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }

}