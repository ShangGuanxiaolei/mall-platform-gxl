package com.xquark.dal.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class UserPointDTO {
    private String name;
    private Date createAt ;
    private BigDecimal amount ;
    private String avatar;
    private String bizType;
    private String leaveWord;

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public int getAmount() {
        return amount.intValue();
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "UserPointDTO{" +
                "name='" + name + '\'' +
                ", createAt=" + createAt +
                ", amount=" + amount +
                '}';
    }
}
