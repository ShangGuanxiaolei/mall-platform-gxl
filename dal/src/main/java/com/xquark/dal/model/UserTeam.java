package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ShopStatus;


public class UserTeam extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String shopId;

  private String teamId;

  private ShopStatus status;

  private String userId;

  private Boolean archive;

  public String getTeamId() {
    return teamId;
  }

  public void setTeamId(String teamId) {
    this.teamId = teamId;
  }

  public ShopStatus getStatus() {
    return status;
  }

  public void setStatus(ShopStatus status) {
    this.status = status;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}