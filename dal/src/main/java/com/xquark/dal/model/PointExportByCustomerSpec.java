package com.xquark.dal.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author liuwei
 * @date 18-7-26 下午3:02
 */
public class PointExportByCustomerSpec {

    public static final String SHEET_STR = "收入报表-按客户";

    private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

    public static String[] KEY_SET;
    public static String[] VALUE_SET;

    static {
        EXPORT_FORMAT.put("ordermonth", "getOrderMonth");
        EXPORT_FORMAT.put("ID", "getCustomerNumber");
        EXPORT_FORMAT.put("Name", "getNamecn");
        EXPORT_FORMAT.put("Title","getTypeName");
        EXPORT_FORMAT.put("Company Name", "getSubTypeName");
        EXPORT_FORMAT.put("Type", "getPromotionType");
        EXPORT_FORMAT.put("Category", "getSubType");
        EXPORT_FORMAT.put("City of Store", "getStore");
        EXPORT_FORMAT.put("Total Earnings Amount", "getEarnAmout");
        EXPORT_FORMAT.put("Rebate Amount", "getRebateAmout");
        EXPORT_FORMAT.put("Other Amount","getOtherAmout");
        EXPORT_FORMAT.put("Other Amount2","getOtherAmout2");
        EXPORT_FORMAT.put("DF", "getDF");
        EXPORT_FORMAT.put("Bank Account", "getBankAccount");
        EXPORT_FORMAT.put("Account Name", "getAccountName");
        EXPORT_FORMAT.put("Bank Branch Name", "getBankFullName");
        EXPORT_FORMAT.put("Bank City", "");
        EXPORT_FORMAT.put("Bank Province", "getBankProvince");
        EXPORT_FORMAT.put("Customer NationalID", "getCustomerNationalId");
        EXPORT_FORMAT.put("Customer Gender", "getGender");
        EXPORT_FORMAT.put("Spouse Name", "getSpouseName");
        EXPORT_FORMAT.put("Spouse NationalID", "getSpouseNationalId");
        EXPORT_FORMAT.put("IdCardNumber", "getTinCode");
        KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
        VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
    }

    private static String[][] getArrayFromHash(Map<String, String> data) {
        String[][] str;
        {
            Object[] keys = data.keySet().toArray();
            Object[] values = data.values().toArray();
            str = new String[keys.length][values.length];
            for (int i = 0; i < keys.length; i++) {
                str[0][i] = (String) keys[i];
                str[1][i] = (String) values[i];
            }
        }
        return str;
    }
}
