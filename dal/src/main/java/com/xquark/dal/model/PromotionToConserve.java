package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

/**
 * @author gxl
 */
public class PromotionToConserve extends BaseEntityArchivableImpl {

    /**
     * 记录无vip资格的白人拼主
     */
    private Long cpId;

    /**
     * 新人团的成团次数
     */
    private Integer pgNum;

    private static final long serialVersionUID = 1L;

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Integer getPgNum() {
        return pgNum;
    }

    public void setPgNum(Integer pgNum) {
        this.pgNum = pgNum;
    }

}