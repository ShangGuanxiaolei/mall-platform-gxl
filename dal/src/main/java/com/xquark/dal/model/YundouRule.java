package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public class YundouRule extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = -1987062643524962729L;

  private String id;
  private String operationId;
  private Integer code;
  private String name;
  private String description;
  private Boolean direction;
  private Boolean isEnable;
  private Boolean archive;
  private Date createdAt;
  private Date updatedAt;

  public YundouRule() {
  }

  public YundouRule(String id, String operationId, Integer code, String name, String description,
      Boolean direction) {
    this.id = id;
    this.operationId = operationId;
    this.code = code;
    this.name = name;
    this.description = description;
    this.direction = direction;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getOperationId() {
    return operationId;
  }

  public void setOperationId(String operationId) {
    this.operationId = operationId;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getDirection() {
    return direction;
  }

  public void setDirection(Boolean direction) {
    this.direction = direction;
  }

  public Boolean getEnable() {
    return isEnable;
  }

  public void setEnable(Boolean enable) {
    isEnable = enable;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }
}
