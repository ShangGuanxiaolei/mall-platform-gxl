package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;

/**
 * @author 
 */
public class PointGiftRecord extends BaseEntityArchivableImpl {

    /**
     * 红包编号
     */
    private String pointGiftNo;

    /**
     * 发放人身份id
     */
    private String grantCpid;

    /**
     * 领取人身份id
     */
    private String receiveCpid;

    /**
     * 领取金额
     */
    private BigDecimal amount;

    private static final long serialVersionUID = 1L;

     private PointGiftRecord(Builder builder) {
        this.pointGiftNo = builder.pointGiftNo;
        this.grantCpid = builder.grantCpid;
        this.receiveCpid = builder.receiveCpid;
        this.amount = builder.amount;
    }

    public PointGiftRecord(){}

    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public String getGrantCpid() {
        return grantCpid;
    }

    public void setGrantCpid(String grantCpid) {
        this.grantCpid = grantCpid;
    }

    public String getReceiveCpid() {
        return receiveCpid;
    }

    public void setReceiveCpid(String receiveCpid) {
        this.receiveCpid = receiveCpid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public static class Builder{
        private String pointGiftNo;
        private String grantCpid;
        private String receiveCpid;
        private BigDecimal amount;

        public Builder pointGiftNo(String pointGiftNo){
            this.pointGiftNo = pointGiftNo;
            return this;
        }

        public Builder grantCpid(String grantCpid){
            this.grantCpid = grantCpid;
            return this;
        }

        public Builder receiveCpid(String receiveCpid){
            this.receiveCpid = receiveCpid;
            return this;
        }

        public Builder amount(BigDecimal amount){
            this.amount = amount;
            return this;
        }

        public PointGiftRecord build(){
            return new PointGiftRecord(this);
        }
    }
}