package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @auther liuwei
 * @date 2018/6/16 23:33
 */
public class PSyncStockRequsetParam {
  @JsonProperty("PlatProductID")
  private String PlatProductID;
  @JsonProperty("SkuID")
  private String SkuID;
  @JsonProperty("OuterID")
  private String OuterID;
  @JsonProperty("Quantity")
  private int Quantity;
  @JsonProperty("OutSkuID")
  private String OutSkuID;

  public String getPlatProductID() {
    return PlatProductID;
  }

  public void setPlatProductID(String platProductID) {
    PlatProductID = platProductID;
  }

  public String getSkuID() {
    return SkuID;
  }

  public void setSkuID(String skuID) {
    SkuID = skuID;
  }

  public String getOuterID() {
    return OuterID;
  }

  public void setOuterID(String outerID) {
    OuterID = outerID;
  }

  public int getQuantity() {
    return Quantity;
  }

  public void setQuantity(int quantity) {
    Quantity = quantity;
  }

  public String getOutSkuID() {
    return OutSkuID;
  }

  public void setOutSkuID(String outSkuID) {
    OutSkuID = outSkuID;
  }

  @Override
  public String toString() {
    return "PSyncStockRequsetParam{" +
        "PlatProductID='" + PlatProductID + '\'' +
        ", SkuID='" + SkuID + '\'' +
        ", OuterID='" + OuterID + '\'' +
        ", Quantity=" + Quantity +
        ", OutSkuID='" + OutSkuID + '\'' +
        '}';
  }
}
