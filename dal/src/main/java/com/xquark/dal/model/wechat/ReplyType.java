package com.xquark.dal.model.wechat;

/**
 * 自定义回复内容的类型
 */
public enum ReplyType {
  TEXT, //纯文本
  WECHAT_MEDIA // 微信富文本信息
}
