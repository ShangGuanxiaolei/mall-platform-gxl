package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class ProductFilter extends BaseEntityImpl implements Archivable {

  public ProductFilter() {
  }

  public ProductFilter(String productId, String filterId) {
    this.productId = productId;
    this.filterId = filterId;
  }

  private String id;

  /**
   * 商品id
   */
  private String productId;

  /**
   * 滤芯id
   */
  private String filterId;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getFilterId() {
    return filterId;
  }

  public void setFilterId(String filterId) {
    this.filterId = filterId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}