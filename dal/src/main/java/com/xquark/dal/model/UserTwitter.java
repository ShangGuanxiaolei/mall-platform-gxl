package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.TwitterStatus;

public class UserTwitter extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerShopId;

  private String userId;

  private Boolean archive;

  private TwitterStatus status;

  private String parentUserId;

  public String getParentUserId() {
    return parentUserId;
  }

  public void setParentUserId(String parentUserId) {
    this.parentUserId = parentUserId;
  }

  public TwitterStatus getStatus() {
    return status;
  }

  public void setStatus(TwitterStatus status) {
    this.status = status;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}