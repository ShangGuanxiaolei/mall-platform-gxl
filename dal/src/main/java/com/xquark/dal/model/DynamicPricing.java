package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/11
 * 根据用户身份动态变更价格
 */
public interface DynamicPricing {

  void setPrice(BigDecimal price);

  BigDecimal getPrice();

  BigDecimal getServerAmt();

  void setServerAmt(BigDecimal serverAmt);

  BigDecimal getReduction();

  void setReduction(BigDecimal point);

  void setNetWorth(BigDecimal netWorth);

  void setPromoAmt(BigDecimal promoAmt);

  BigDecimal getConversionPrice();

  void setMemberPrice(BigDecimal memberPrice);

  void setProxyPrice(BigDecimal proxyPrice);

  void setChangePrice(BigDecimal changePrice);

  BigDecimal getChangePrice();

  void setReducedPrice(BigDecimal reducedPrice);

}
