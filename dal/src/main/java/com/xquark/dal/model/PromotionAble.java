package com.xquark.dal.model;

import com.xquark.dal.type.PromotionType;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Optional;

/**
 * 活动接口
 *
 * @author wangxinhua
 * @date 2019-04-06
 * @since 1.0
 */
public interface PromotionAble {

  /** 获取活动标题 */
  String getTitle();

  /** 活动开始时间 */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  Date getValidFrom();

  /** 活动结束时间 */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  Date getValidTo();

  /**
   * 校验活动时间是否符合条件
   * 若 {@code validFrom}, {@code validTo} 中有一项为空则返回false
   * @param date 若传入则以该时间计算，否则取当前时间
   * @return true or false
   */
  default boolean isValid(Date date) {
    final Date now = Optional.ofNullable(date).orElseGet(Date::new);
    return Optional.ofNullable(getValidFrom())
        .flatMap(
            from -> Optional.ofNullable(getValidTo()).map(to -> now.after(from) && now.before(to)))
        .orElse(false);
  }

  /**
   * 校验活动时间是否符合条件
   *
   * @return true or false
   */
  default boolean isValid() {
    return this.isValid(null);
  }

  /**
   * 获取当前的活动类型
   * @return 活动类型
   */
  PromotionType getPromotionType();
}
