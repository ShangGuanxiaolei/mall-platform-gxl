package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/4/21
 * Time:15:27
 * Des:
 */
public class UserLogVo {
//   `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
//   `cpId` bigint(20) DEFAULT NULL,
//   `from_cpId` bigint(20) DEFAULT NULL,
//   `log_info` mediumtext COMMENT '日志内容',
//   `type` tinyint(4) DEFAULT NULL COMMENT '日志来源:0app，1小程序，2h5',
//   `created_at` timestamp NULL DEFAULT NULL COMMENT '日志时间',
    private Long id;
    private Long cpId;
    private Long fromCpId;
    private String logInfo;
    private int type;
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Long getFromCpId() {
        return fromCpId;
    }

    public void setFromCpId(Long fromCpId) {
        this.fromCpId = fromCpId;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "UserLogVo{" +
                "id=" + id +
                ", cpId=" + cpId +
                ", fromCpId=" + fromCpId +
                ", logInfo='" + logInfo + '\'' +
                ", type=" + type +
                ", createdAt=" + createdAt +
                '}';
    }
}