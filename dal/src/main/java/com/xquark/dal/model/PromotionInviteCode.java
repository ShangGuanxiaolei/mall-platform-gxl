package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
/**
 * 邀请码
 *
 */
public class PromotionInviteCode extends BaseEntityImpl implements Archivable {
	
	private static final long serialVersionUID = 1L;
	//邀请码所属活动的活动编码
	private  String pCode ;
	//邀请码
	private String code;
	//会员的custom_profile_id
	private String cpId;

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	@Override
	public Boolean getArchive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArchive(Boolean archive) {
		// TODO Auto-generated method stub
		
	}


}
