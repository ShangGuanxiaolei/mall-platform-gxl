package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.type.CommissionType;

import java.math.BigDecimal;

/**
 * 佣金
 *
 * @author ahlon
 */
public class Commission extends BaseEntityImpl {

  private static final long serialVersionUID = -6408188940728651337L;

  private String orderId;

  private String orderItemId;
  private String skuId;
  private BigDecimal price;
  private double rate; // 分佣比例
  private Integer amount;
  private BigDecimal fee; // 分拥金额
  private String userId;//  分佣用户
  private Boolean withdrawn;
  private CommissionStatus status;

  private CommissionType type;

  private String ruleLog; // 分佣计算的日志
  private Boolean offered; // 佣金是否发放

  public Commission(String orderId, String orderItemId, String skuId, BigDecimal price, double rate,
      CommissionType type, Integer amount, BigDecimal fee, String userId, CommissionStatus status) {
    this.orderId = orderId;
    this.orderItemId = orderItemId;
    this.skuId = skuId;
    this.price = price;
    this.rate = rate;
    this.type = type;
    this.amount = amount;
    this.fee = fee;
    this.userId = userId;
    this.status = status;
  }

  public Commission(String orderId, String orderItemId, String skuId, BigDecimal price, double rate,
      CommissionType type, Integer amount, BigDecimal fee, String userId, CommissionStatus status,
      Boolean withdrawn) {
    this.orderId = orderId;
    this.orderItemId = orderItemId;
    this.skuId = skuId;
    this.price = price;
    this.rate = rate;
    this.type = type;
    this.amount = amount;
    this.fee = fee;
    this.userId = userId;
    this.status = status;
    this.withdrawn = withdrawn;
  }

  public Commission(String orderId, String orderItemId, String skuId, BigDecimal price, double rate,
      CommissionType type, Integer amount, BigDecimal fee, String userId, CommissionStatus status,
      Boolean withdrawn, String ruleLog) {
    this.orderId = orderId;
    this.orderItemId = orderItemId;
    this.skuId = skuId;
    this.price = price;
    this.rate = rate;
    this.type = type;
    this.amount = amount;
    this.fee = fee;
    this.userId = userId;
    this.status = status;
    this.withdrawn = withdrawn;
    this.ruleLog = ruleLog;
  }

  public Commission() {

  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public double getRate() {
    return rate;
  }

  public void setRate(double rate) {
    this.rate = rate;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public CommissionStatus getStatus() {
    return status;
  }

  public void setStatus(CommissionStatus status) {
    this.status = status;
  }

  public Boolean getWithdrawn() {
    return withdrawn;
  }

  public void setWithdrawn(Boolean withdrawn) {
    this.withdrawn = withdrawn;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public CommissionType getType() {
    return type;
  }

  public void setType(CommissionType type) {
    this.type = type;
  }

  public String getRuleLog() {
    return ruleLog;
  }

  public void setRuleLog(String ruleLog) {
    this.ruleLog = ruleLog;
  }

  public Boolean getOffered() {
    return offered;
  }

  public void setOffered(Boolean offered) {
    this.offered = offered;
  }
}
