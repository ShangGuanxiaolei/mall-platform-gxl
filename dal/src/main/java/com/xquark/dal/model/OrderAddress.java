package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.util.ArrayList;
import java.util.List;

public class OrderAddress extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  /**
   * 主订单ID
   */
  private String orderId;

  /**
   * 地区ID
   */
  private String zoneId;
  /**
   * 收货人
   */
  private String consignee;

  /**
   * 详细地址
   */
  private String street;

  /**
   * 电话号码
   */
  private String phone;

  /**
   * 微信号
   */
  private String weixinId;
  /**
   * 身份证
   */
  private String certificateId;

  private Boolean isDefault;

  private List<SystemRegion> systemRegions = new ArrayList<SystemRegion>();

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getCertificateId() {
    return certificateId;
  }

  public void setCertificateId(String certificateId) {
    this.certificateId = certificateId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getWeixinId() {
    return weixinId;
  }

  public void setWeixinId(String weixinId) {
    this.weixinId = weixinId;
  }

  public List<SystemRegion> getSystemRegions() {
    return systemRegions;
  }

  public void setSystemRegions(List<SystemRegion> systemRegions) {
    this.systemRegions = systemRegions;
  }
}