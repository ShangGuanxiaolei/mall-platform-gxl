package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.status.OrderStatus;

import java.math.BigDecimal;

/**
 * 预售子订单记录
 *
 * @author: yyc
 * @date: 19-4-21 下午5:25
 */
public class PromotionReserveOrderItem extends BaseEntityArchivableImpl {

  /** 订单id */
  private String orderId;

  /** 子订单状态 */
  private OrderStatus orderStatus;

  /** 用户id */
  private String userId;

  /** 关联的sku */
  private String skuId;

  /** 关联的productId */
  private String productId;

  /** 关联的活动id */
  private String promotionId;

  /** 预约活动价 */
  private BigDecimal promotionPrice;

  /** 商品原价 */
  private BigDecimal price;

  /** 预约数量 */
  private Integer amount;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public BigDecimal getPromotionPrice() {
    return promotionPrice;
  }

  public void setPromotionPrice(BigDecimal promotionPrice) {
    this.promotionPrice = promotionPrice;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }
}
