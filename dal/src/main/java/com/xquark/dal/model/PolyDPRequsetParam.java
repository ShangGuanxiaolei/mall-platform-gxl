package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @auther liuwei
 * @date 2018/6/16 20:15
 */
public class PolyDPRequsetParam {
  @JsonProperty("ProductId")
  private String ProductId;
  @JsonProperty("ProductName")
  private String ProductName;
  @JsonProperty("Status")
  private String Status;
  @JsonProperty("PageIndex")
  private Integer PageIndex;
  @JsonProperty("PageSize")
  private Integer PageSize;

  public String getProductId() {
    return ProductId;
  }

  public void setProductId(String productId) {
    ProductId = productId;
  }

  public String getProductName() {
    return ProductName;
  }

  public void setProductName(String productName) {
    ProductName = productName;
  }

  public String getStatus() {
    return Status;
  }

  public void setStatus(String status) {
    Status = status;
  }

  public Integer getPageIndex() {
    return PageIndex;
  }

  public void setPageIndex(Integer pageIndex) {
    PageIndex = pageIndex;
  }

  public Integer getPageSize() {
    return PageSize;
  }

  public void setPageSize(Integer pageSize) {
    PageSize = pageSize;
  }

  @Override
  public String toString() {
    return "PolyDPRequsetParam{" +
        "ProductId='" + ProductId + '\'' +
        ", ProductName='" + ProductName + '\'' +
        ", Status='" + Status + '\'' +
        ", PageIndex=" + PageIndex +
        ", PageSize=" + PageSize +
        '}';
  }
}
