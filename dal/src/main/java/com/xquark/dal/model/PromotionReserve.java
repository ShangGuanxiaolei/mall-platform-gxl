package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.type.PromotionType;

import java.util.Date;

/**
 * @author: yyc
 * @date: 19-4-17 下午4:15
 */
public class PromotionReserve extends BaseEntityArchivableImpl implements PromotionAble {

  private static final long serialVersionUID = 1L;

  /** 活动id */
  private String promotionId;

  /** 状态 */
  private Integer status;

  /** 预约开始时间 */
  private Date effectFromTime;

  /** 预约结束时间 */
  private Date effectToTime;

  /** 支付开始时间 */
  private Date payFromTime;

  /** 支付结束时间 */
  private Date payToTime;

  /** 活动名称 */
  private String promotionName;

  /** 定向人群(1不限，2非会员(新人+白人),3(Vip+店主)) */
  private Integer targetPerson;

  /** 是否计算收益 */
  private Boolean isCountEarning;

  /** 是否包邮 */
  private Boolean isDeliveryReduction;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Date getEffectFromTime() {
    return effectFromTime;
  }

  public void setEffectFromTime(Date effectFromTime) {
    this.effectFromTime = effectFromTime;
  }

  public Date getEffectToTime() {
    return effectToTime;
  }

  public void setEffectToTime(Date effectToTime) {
    this.effectToTime = effectToTime;
  }

  public Date getPayFromTime() {
    return payFromTime;
  }

  public void setPayFromTime(Date payFromTime) {
    this.payFromTime = payFromTime;
  }

  public Date getPayToTime() {
    return payToTime;
  }

  public void setPayToTime(Date payToTime) {
    this.payToTime = payToTime;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public Integer getTargetPerson() {
    return targetPerson;
  }

  public void setTargetPerson(Integer targetPerson) {
    this.targetPerson = targetPerson;
  }

  public Boolean getIsCountEarning() {
    return isCountEarning;
  }

  public void setIsCountEarning(Boolean countEarning) {
    isCountEarning = countEarning;
  }

  public Boolean getIsDeliveryReduction() {
    return isDeliveryReduction;
  }

  public void setIsDeliveryReduction(Boolean deliveryReduction) {
    isDeliveryReduction = deliveryReduction;
  }

  /**
   * 能够预约
   *
   * @return
   */
  public boolean canOrder() {
    Date now = new Date();
    return effectFromTime.before(now) && effectToTime.after(now) && status < 3;
  }

  /**
   * 能够支付
   *
   * @return
   */
  public boolean canPay() {
    Date now = new Date();
    return payFromTime.before(now) && payToTime.after(now) && status < 4;
  }

  @Override
  public String getTitle() {
    return promotionName;
  }

  @Override
  public Date getValidFrom() {
    return getEffectFromTime();
  }

  @Override
  public Date getValidTo() {
    return getEffectToTime();
  }

  @Override
  public PromotionType getPromotionType() {
    return PromotionType.RESERVE;
  }
}
