package com.xquark.dal.model;

import com.xquark.dal.model.pay.PaymentConfig;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.xquark.dal.BaseEntityImpl;
import org.apache.http.MethodNotSupportedException;

public class PaymentMerchant extends BaseEntityImpl implements PaymentConfig {

  private static final long serialVersionUID = 1L;

  private String domainId;

  private String domainCode;

  private String paymentId;

  private String signType;

  private String tradeType;

  private String merchantId;

  private String merchantAccount;

  private String merchantName;

  private String appId;

  private String callbackSite;

  private String notifySite;

  private String pubKey;

  private String secretKey;

  public String getDomainId() {
    return domainId;
  }

  public void setDomainId(String domainId) {
    this.domainId = domainId;
  }

  public String getDomainCode() {
    return domainCode;
  }

  public void setDomainCode(String domainCode) {
    this.domainCode = domainCode == null ? null : domainCode.trim();
  }

  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public String getSignType() {
    return signType;
  }

  public void setSignType(String signType) {
    this.signType = signType;
  }

  public String getTradeType() {
    return tradeType;
  }

  public void setTradeType(String tradeType) {
    this.tradeType = tradeType == null ? null : tradeType.trim();
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId == null ? null : merchantId.trim();
  }

  public String getMerchantAccount() {
    return merchantAccount;
  }

  public void setMerchantAccount(String merchantAccount) {
    this.merchantAccount = merchantAccount == null ? null : merchantAccount.trim();
  }

  public String getMerchantName() {
    return merchantName;
  }

  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }

  public String getAppId() {
    return appId;
  }

  @Override
  public String getAppSecret() {
    throw new NotImplementedException("支付宝支付无需appSecret");
  }

    @Override
  public String getMchId() {
    return getMerchantId();
  }

  public void setAppId(String appId) {
    this.appId = appId == null ? null : appId.trim();
  }

  public String getCallbackSite() {
    return callbackSite;
  }

  public void setCallbackSite(String callbackSite) {
    this.callbackSite = callbackSite;
  }

  public String getNotifySite() {
    return notifySite;
  }

  @Override
  public String getMchSecret() {
    return secretKey;
  }

  public void setNotifySite(String notifySite) {
    this.notifySite = notifySite;
  }

  public String getPubKey() {
    return pubKey;
  }

  public void setPubKey(String pubKey) {
    this.pubKey = pubKey == null ? null : pubKey.trim();
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey == null ? null : secretKey.trim();
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }
}