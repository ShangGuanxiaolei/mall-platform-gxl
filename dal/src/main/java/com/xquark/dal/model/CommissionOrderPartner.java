package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;


public class CommissionOrderPartner extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String userId;

  private String ownShopId;

  private String teamPlanId;

  private String parterPlanId;

  private String areaPlanId;

  private String customAreaPlanId;

  private Boolean archive;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(String ownShopId) {
    this.ownShopId = ownShopId;
  }

  public String getTeamPlanId() {
    return teamPlanId;
  }

  public void setTeamPlanId(String teamPlanId) {
    this.teamPlanId = teamPlanId;
  }

  public String getParterPlanId() {
    return parterPlanId;
  }

  public void setParterPlanId(String parterPlanId) {
    this.parterPlanId = parterPlanId;
  }

  public String getAreaPlanId() {
    return areaPlanId;
  }

  public void setAreaPlanId(String areaPlanId) {
    this.areaPlanId = areaPlanId;
  }

  public String getCustomAreaPlanId() {
    return customAreaPlanId;
  }

  public void setCustomAreaPlanId(String customAreaPlanId) {
    this.customAreaPlanId = customAreaPlanId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}