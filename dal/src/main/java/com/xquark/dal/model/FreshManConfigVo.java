package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:11:45
 * Des:新人页面表
 */
public class FreshManConfigVo {
//`id` bigint(20) NOT NULL AUTO_INCREMENT,
//`name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数名称',
//`value` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0.00' COMMENT '参数值',
//`type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '参数类型',
//`remark` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数说明',
//`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    private Long id;
    private String name;
    private String value;
    private String type;
    private String remark;
    private Date createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}