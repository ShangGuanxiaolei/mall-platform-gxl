package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.math.BigDecimal;

/**
 * User: byy Date: 18-8-31. Time: 上午10:50
 */
public class HotProduct extends BaseEntityImpl {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  private String name;

  private BigDecimal price;

  private BigDecimal deductionDPoint;

  private String sName;

  private Integer amount;

  private String cName;

  private String aName;

  private Integer sales;

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public String getsName() {
    return sName;
  }

  public void setsName(String sName) {
    this.sName = sName;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getcName() {
    return cName;
  }

  public void setcName(String cName) {
    this.cName = cName;
  }

  public String getaName() {
    return aName;
  }

  public void setaName(String aName) {
    this.aName = aName;
  }

  public Integer getSales() {
    return sales;
  }

  public void setSales(Integer sales) {
    this.sales = sales;
  }
}
