package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class Poscode implements Serializable {

  /**
   * id
   */
  private Long id;

  private String snCode;

  private String homeCity;

  private Date createdDate;

  /**
   * 1 可用，0 不可用
   */
  private Boolean idDeleted;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSnCode() {
    return snCode;
  }

  public void setSnCode(String snCode) {
    this.snCode = snCode;
  }

  public String getHomeCity() {
    return homeCity;
  }

  public void setHomeCity(String homeCity) {
    this.homeCity = homeCity;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Boolean getIdDeleted() {
    return idDeleted;
  }

  public void setIdDeleted(Boolean idDeleted) {
    this.idDeleted = idDeleted;
  }
}