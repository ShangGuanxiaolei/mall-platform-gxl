package com.xquark.dal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HealthTestModuleExample {

  protected String orderByClause;

  protected boolean distinct;

  protected List<Criteria> oredCriteria;

  private Integer limit;

  private Integer offset;

  public HealthTestModuleExample() {
    oredCriteria = new ArrayList<Criteria>();
  }

  public void setOrderByClause(String orderByClause) {
    this.orderByClause = orderByClause;
  }

  public String getOrderByClause() {
    return orderByClause;
  }

  public void setDistinct(boolean distinct) {
    this.distinct = distinct;
  }

  public boolean isDistinct() {
    return distinct;
  }

  public List<Criteria> getOredCriteria() {
    return oredCriteria;
  }

  public void or(Criteria criteria) {
    oredCriteria.add(criteria);
  }

  public Criteria or() {
    Criteria criteria = createCriteriaInternal();
    oredCriteria.add(criteria);
    return criteria;
  }

  public Criteria createCriteria() {
    Criteria criteria = createCriteriaInternal();
    if (oredCriteria.size() == 0) {
      oredCriteria.add(criteria);
    }
    return criteria;
  }

  protected Criteria createCriteriaInternal() {
    Criteria criteria = new Criteria();
    return criteria;
  }

  public void clear() {
    oredCriteria.clear();
    orderByClause = null;
    distinct = false;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public Integer getOffset() {
    return offset;
  }

  protected abstract static class GeneratedCriteria {

    protected List<Criterion> idCriteria;

    protected List<Criterion> parentIdCriteria;

    protected List<Criterion> allCriteria;

    protected List<Criterion> criteria;

    protected GeneratedCriteria() {
      super();
      criteria = new ArrayList<Criterion>();
      idCriteria = new ArrayList<Criterion>();
      parentIdCriteria = new ArrayList<Criterion>();
    }

    public List<Criterion> getIdCriteria() {
      return idCriteria;
    }

    protected void addIdCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      idCriteria.add(new Criterion(condition, value, "idHandler"));
      allCriteria = null;
    }

    protected void addIdCriterion(String condition, String value1, String value2, String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      idCriteria.add(new Criterion(condition, value1, value2, "idHandler"));
      allCriteria = null;
    }

    public List<Criterion> getParentIdCriteria() {
      return parentIdCriteria;
    }

    protected void addParentIdCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      parentIdCriteria.add(new Criterion(condition, value, "idHandler"));
      allCriteria = null;
    }

    protected void addParentIdCriterion(String condition, String value1, String value2,
        String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      parentIdCriteria.add(new Criterion(condition, value1, value2, "idHandler"));
      allCriteria = null;
    }

    public boolean isValid() {
      return criteria.size() > 0
          || idCriteria.size() > 0
          || parentIdCriteria.size() > 0;
    }

    public List<Criterion> getAllCriteria() {
      if (allCriteria == null) {
        allCriteria = new ArrayList<Criterion>();
        allCriteria.addAll(criteria);
        allCriteria.addAll(idCriteria);
        allCriteria.addAll(parentIdCriteria);
      }
      return allCriteria;
    }

    public List<Criterion> getCriteria() {
      return criteria;
    }

    protected void addCriterion(String condition) {
      if (condition == null) {
        throw new RuntimeException("Value for condition cannot be null");
      }
      criteria.add(new Criterion(condition));
      allCriteria = null;
    }

    protected void addCriterion(String condition, Object value, String property) {
      if (value == null) {
        throw new RuntimeException("Value for " + property + " cannot be null");
      }
      criteria.add(new Criterion(condition, value));
      allCriteria = null;
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
      if (value1 == null || value2 == null) {
        throw new RuntimeException("Between values for " + property + " cannot be null");
      }
      criteria.add(new Criterion(condition, value1, value2));
      allCriteria = null;
    }

    public Criteria andIdIsNull() {
      addCriterion("id is null");
      return (Criteria) this;
    }

    public Criteria andIdIsNotNull() {
      addCriterion("id is not null");
      return (Criteria) this;
    }

    public Criteria andIdEqualTo(String value) {
      addIdCriterion("id =", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotEqualTo(String value) {
      addIdCriterion("id <>", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdGreaterThan(String value) {
      addIdCriterion("id >", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
      addIdCriterion("id >=", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdLessThan(String value) {
      addIdCriterion("id <", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
      addIdCriterion("id <=", value, "id");
      return (Criteria) this;
    }

    public Criteria andIdIn(List<String> values) {
      addIdCriterion("id in", values, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotIn(List<String> values) {
      addIdCriterion("id not in", values, "id");
      return (Criteria) this;
    }

    public Criteria andIdBetween(String value1, String value2) {
      addIdCriterion("id between", value1, value2, "id");
      return (Criteria) this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
      addIdCriterion("id not between", value1, value2, "id");
      return (Criteria) this;
    }

    public Criteria andParentIdIsNull() {
      addCriterion("parent_id is null");
      return (Criteria) this;
    }

    public Criteria andParentIdIsNotNull() {
      addCriterion("parent_id is not null");
      return (Criteria) this;
    }

    public Criteria andParentIdEqualTo(String value) {
      addParentIdCriterion("parent_id =", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdNotEqualTo(String value) {
      addParentIdCriterion("parent_id <>", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdGreaterThan(String value) {
      addParentIdCriterion("parent_id >", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdGreaterThanOrEqualTo(String value) {
      addParentIdCriterion("parent_id >=", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdLessThan(String value) {
      addParentIdCriterion("parent_id <", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdLessThanOrEqualTo(String value) {
      addParentIdCriterion("parent_id <=", value, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdIn(List<String> values) {
      addParentIdCriterion("parent_id in", values, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdNotIn(List<String> values) {
      addParentIdCriterion("parent_id not in", values, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdBetween(String value1, String value2) {
      addParentIdCriterion("parent_id between", value1, value2, "parentId");
      return (Criteria) this;
    }

    public Criteria andParentIdNotBetween(String value1, String value2) {
      addParentIdCriterion("parent_id not between", value1, value2, "parentId");
      return (Criteria) this;
    }

    public Criteria andNameIsNull() {
      addCriterion("name is null");
      return (Criteria) this;
    }

    public Criteria andNameIsNotNull() {
      addCriterion("name is not null");
      return (Criteria) this;
    }

    public Criteria andNameEqualTo(String value) {
      addCriterion("name =", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameNotEqualTo(String value) {
      addCriterion("name <>", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameGreaterThan(String value) {
      addCriterion("name >", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameGreaterThanOrEqualTo(String value) {
      addCriterion("name >=", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameLessThan(String value) {
      addCriterion("name <", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameLessThanOrEqualTo(String value) {
      addCriterion("name <=", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameLike(String value) {
      addCriterion("name like", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameNotLike(String value) {
      addCriterion("name not like", value, "name");
      return (Criteria) this;
    }

    public Criteria andNameIn(List<String> values) {
      addCriterion("name in", values, "name");
      return (Criteria) this;
    }

    public Criteria andNameNotIn(List<String> values) {
      addCriterion("name not in", values, "name");
      return (Criteria) this;
    }

    public Criteria andNameBetween(String value1, String value2) {
      addCriterion("name between", value1, value2, "name");
      return (Criteria) this;
    }

    public Criteria andNameNotBetween(String value1, String value2) {
      addCriterion("name not between", value1, value2, "name");
      return (Criteria) this;
    }

    public Criteria andStaticNameIsNull() {
      addCriterion("static_name is null");
      return (Criteria) this;
    }

    public Criteria andStaticNameIsNotNull() {
      addCriterion("static_name is not null");
      return (Criteria) this;
    }

    public Criteria andStaticNameEqualTo(String value) {
      addCriterion("static_name =", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameNotEqualTo(String value) {
      addCriterion("static_name <>", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameGreaterThan(String value) {
      addCriterion("static_name >", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameGreaterThanOrEqualTo(String value) {
      addCriterion("static_name >=", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameLessThan(String value) {
      addCriterion("static_name <", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameLessThanOrEqualTo(String value) {
      addCriterion("static_name <=", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameLike(String value) {
      addCriterion("static_name like", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameNotLike(String value) {
      addCriterion("static_name not like", value, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameIn(List<String> values) {
      addCriterion("static_name in", values, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameNotIn(List<String> values) {
      addCriterion("static_name not in", values, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameBetween(String value1, String value2) {
      addCriterion("static_name between", value1, value2, "staticName");
      return (Criteria) this;
    }

    public Criteria andStaticNameNotBetween(String value1, String value2) {
      addCriterion("static_name not between", value1, value2, "staticName");
      return (Criteria) this;
    }

    public Criteria andTypeIsNull() {
      addCriterion("type is null");
      return (Criteria) this;
    }

    public Criteria andTypeIsNotNull() {
      addCriterion("type is not null");
      return (Criteria) this;
    }

    public Criteria andTypeEqualTo(String value) {
      addCriterion("type =", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeNotEqualTo(String value) {
      addCriterion("type <>", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeGreaterThan(String value) {
      addCriterion("type >", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeGreaterThanOrEqualTo(String value) {
      addCriterion("type >=", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeLessThan(String value) {
      addCriterion("type <", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeLessThanOrEqualTo(String value) {
      addCriterion("type <=", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeLike(String value) {
      addCriterion("type like", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeNotLike(String value) {
      addCriterion("type not like", value, "type");
      return (Criteria) this;
    }

    public Criteria andTypeIn(List<String> values) {
      addCriterion("type in", values, "type");
      return (Criteria) this;
    }

    public Criteria andTypeNotIn(List<String> values) {
      addCriterion("type not in", values, "type");
      return (Criteria) this;
    }

    public Criteria andTypeBetween(String value1, String value2) {
      addCriterion("type between", value1, value2, "type");
      return (Criteria) this;
    }

    public Criteria andTypeNotBetween(String value1, String value2) {
      addCriterion("type not between", value1, value2, "type");
      return (Criteria) this;
    }

    public Criteria andIconIsNull() {
      addCriterion("icon is null");
      return (Criteria) this;
    }

    public Criteria andIconIsNotNull() {
      addCriterion("icon is not null");
      return (Criteria) this;
    }

    public Criteria andIconEqualTo(String value) {
      addCriterion("icon =", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconNotEqualTo(String value) {
      addCriterion("icon <>", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconGreaterThan(String value) {
      addCriterion("icon >", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconGreaterThanOrEqualTo(String value) {
      addCriterion("icon >=", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconLessThan(String value) {
      addCriterion("icon <", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconLessThanOrEqualTo(String value) {
      addCriterion("icon <=", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconLike(String value) {
      addCriterion("icon like", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconNotLike(String value) {
      addCriterion("icon not like", value, "icon");
      return (Criteria) this;
    }

    public Criteria andIconIn(List<String> values) {
      addCriterion("icon in", values, "icon");
      return (Criteria) this;
    }

    public Criteria andIconNotIn(List<String> values) {
      addCriterion("icon not in", values, "icon");
      return (Criteria) this;
    }

    public Criteria andIconBetween(String value1, String value2) {
      addCriterion("icon between", value1, value2, "icon");
      return (Criteria) this;
    }

    public Criteria andIconNotBetween(String value1, String value2) {
      addCriterion("icon not between", value1, value2, "icon");
      return (Criteria) this;
    }

    public Criteria andIsLeafIsNull() {
      addCriterion("is_leaf is null");
      return (Criteria) this;
    }

    public Criteria andIsLeafIsNotNull() {
      addCriterion("is_leaf is not null");
      return (Criteria) this;
    }

    public Criteria andIsLeafEqualTo(Boolean value) {
      addCriterion("is_leaf =", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafNotEqualTo(Boolean value) {
      addCriterion("is_leaf <>", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafGreaterThan(Boolean value) {
      addCriterion("is_leaf >", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafGreaterThanOrEqualTo(Boolean value) {
      addCriterion("is_leaf >=", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafLessThan(Boolean value) {
      addCriterion("is_leaf <", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafLessThanOrEqualTo(Boolean value) {
      addCriterion("is_leaf <=", value, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafIn(List<Boolean> values) {
      addCriterion("is_leaf in", values, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafNotIn(List<Boolean> values) {
      addCriterion("is_leaf not in", values, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafBetween(Boolean value1, Boolean value2) {
      addCriterion("is_leaf between", value1, value2, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andIsLeafNotBetween(Boolean value1, Boolean value2) {
      addCriterion("is_leaf not between", value1, value2, "isLeaf");
      return (Criteria) this;
    }

    public Criteria andDescriptionIsNull() {
      addCriterion("description is null");
      return (Criteria) this;
    }

    public Criteria andDescriptionIsNotNull() {
      addCriterion("description is not null");
      return (Criteria) this;
    }

    public Criteria andDescriptionEqualTo(String value) {
      addCriterion("description =", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionNotEqualTo(String value) {
      addCriterion("description <>", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionGreaterThan(String value) {
      addCriterion("description >", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
      addCriterion("description >=", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionLessThan(String value) {
      addCriterion("description <", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionLessThanOrEqualTo(String value) {
      addCriterion("description <=", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionLike(String value) {
      addCriterion("description like", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionNotLike(String value) {
      addCriterion("description not like", value, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionIn(List<String> values) {
      addCriterion("description in", values, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionNotIn(List<String> values) {
      addCriterion("description not in", values, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionBetween(String value1, String value2) {
      addCriterion("description between", value1, value2, "description");
      return (Criteria) this;
    }

    public Criteria andDescriptionNotBetween(String value1, String value2) {
      addCriterion("description not between", value1, value2, "description");
      return (Criteria) this;
    }

    public Criteria andSortNoIsNull() {
      addCriterion("sort_no is null");
      return (Criteria) this;
    }

    public Criteria andSortNoIsNotNull() {
      addCriterion("sort_no is not null");
      return (Criteria) this;
    }

    public Criteria andSortNoEqualTo(Integer value) {
      addCriterion("sort_no =", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoNotEqualTo(Integer value) {
      addCriterion("sort_no <>", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoGreaterThan(Integer value) {
      addCriterion("sort_no >", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoGreaterThanOrEqualTo(Integer value) {
      addCriterion("sort_no >=", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoLessThan(Integer value) {
      addCriterion("sort_no <", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoLessThanOrEqualTo(Integer value) {
      addCriterion("sort_no <=", value, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoIn(List<Integer> values) {
      addCriterion("sort_no in", values, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoNotIn(List<Integer> values) {
      addCriterion("sort_no not in", values, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoBetween(Integer value1, Integer value2) {
      addCriterion("sort_no between", value1, value2, "sortNo");
      return (Criteria) this;
    }

    public Criteria andSortNoNotBetween(Integer value1, Integer value2) {
      addCriterion("sort_no not between", value1, value2, "sortNo");
      return (Criteria) this;
    }

    public Criteria andRequiredSexIsNull() {
      addCriterion("required_sex is null");
      return (Criteria) this;
    }

    public Criteria andRequiredSexIsNotNull() {
      addCriterion("required_sex is not null");
      return (Criteria) this;
    }

    public Criteria andRequiredSexEqualTo(Integer value) {
      addCriterion("required_sex =", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexNotEqualTo(Integer value) {
      addCriterion("required_sex <>", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexGreaterThan(Integer value) {
      addCriterion("required_sex >", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexGreaterThanOrEqualTo(Integer value) {
      addCriterion("required_sex >=", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexLessThan(Integer value) {
      addCriterion("required_sex <", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexLessThanOrEqualTo(Integer value) {
      addCriterion("required_sex <=", value, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexIn(List<Integer> values) {
      addCriterion("required_sex in", values, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexNotIn(List<Integer> values) {
      addCriterion("required_sex not in", values, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexBetween(Integer value1, Integer value2) {
      addCriterion("required_sex between", value1, value2, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredSexNotBetween(Integer value1, Integer value2) {
      addCriterion("required_sex not between", value1, value2, "requiredSex");
      return (Criteria) this;
    }

    public Criteria andRequiredIsNull() {
      addCriterion("required is null");
      return (Criteria) this;
    }

    public Criteria andRequiredIsNotNull() {
      addCriterion("required is not null");
      return (Criteria) this;
    }

    public Criteria andRequiredEqualTo(Boolean value) {
      addCriterion("required =", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredNotEqualTo(Boolean value) {
      addCriterion("required <>", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredGreaterThan(Boolean value) {
      addCriterion("required >", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredGreaterThanOrEqualTo(Boolean value) {
      addCriterion("required >=", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredLessThan(Boolean value) {
      addCriterion("required <", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredLessThanOrEqualTo(Boolean value) {
      addCriterion("required <=", value, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredIn(List<Boolean> values) {
      addCriterion("required in", values, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredNotIn(List<Boolean> values) {
      addCriterion("required not in", values, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredBetween(Boolean value1, Boolean value2) {
      addCriterion("required between", value1, value2, "required");
      return (Criteria) this;
    }

    public Criteria andRequiredNotBetween(Boolean value1, Boolean value2) {
      addCriterion("required not between", value1, value2, "required");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIsNull() {
      addCriterion("created_at is null");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIsNotNull() {
      addCriterion("created_at is not null");
      return (Criteria) this;
    }

    public Criteria andCreatedAtEqualTo(Date value) {
      addCriterion("created_at =", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotEqualTo(Date value) {
      addCriterion("created_at <>", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtGreaterThan(Date value) {
      addCriterion("created_at >", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtGreaterThanOrEqualTo(Date value) {
      addCriterion("created_at >=", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtLessThan(Date value) {
      addCriterion("created_at <", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtLessThanOrEqualTo(Date value) {
      addCriterion("created_at <=", value, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtIn(List<Date> values) {
      addCriterion("created_at in", values, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotIn(List<Date> values) {
      addCriterion("created_at not in", values, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtBetween(Date value1, Date value2) {
      addCriterion("created_at between", value1, value2, "createdAt");
      return (Criteria) this;
    }

    public Criteria andCreatedAtNotBetween(Date value1, Date value2) {
      addCriterion("created_at not between", value1, value2, "createdAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIsNull() {
      addCriterion("updated_at is null");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIsNotNull() {
      addCriterion("updated_at is not null");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtEqualTo(Date value) {
      addCriterion("updated_at =", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotEqualTo(Date value) {
      addCriterion("updated_at <>", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtGreaterThan(Date value) {
      addCriterion("updated_at >", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtGreaterThanOrEqualTo(Date value) {
      addCriterion("updated_at >=", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtLessThan(Date value) {
      addCriterion("updated_at <", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtLessThanOrEqualTo(Date value) {
      addCriterion("updated_at <=", value, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtIn(List<Date> values) {
      addCriterion("updated_at in", values, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotIn(List<Date> values) {
      addCriterion("updated_at not in", values, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtBetween(Date value1, Date value2) {
      addCriterion("updated_at between", value1, value2, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andUpdatedAtNotBetween(Date value1, Date value2) {
      addCriterion("updated_at not between", value1, value2, "updatedAt");
      return (Criteria) this;
    }

    public Criteria andArchiveIsNull() {
      addCriterion("archive is null");
      return (Criteria) this;
    }

    public Criteria andArchiveIsNotNull() {
      addCriterion("archive is not null");
      return (Criteria) this;
    }

    public Criteria andArchiveEqualTo(Boolean value) {
      addCriterion("archive =", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotEqualTo(Boolean value) {
      addCriterion("archive <>", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveGreaterThan(Boolean value) {
      addCriterion("archive >", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveGreaterThanOrEqualTo(Boolean value) {
      addCriterion("archive >=", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveLessThan(Boolean value) {
      addCriterion("archive <", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveLessThanOrEqualTo(Boolean value) {
      addCriterion("archive <=", value, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveIn(List<Boolean> values) {
      addCriterion("archive in", values, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotIn(List<Boolean> values) {
      addCriterion("archive not in", values, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveBetween(Boolean value1, Boolean value2) {
      addCriterion("archive between", value1, value2, "archive");
      return (Criteria) this;
    }

    public Criteria andArchiveNotBetween(Boolean value1, Boolean value2) {
      addCriterion("archive not between", value1, value2, "archive");
      return (Criteria) this;
    }
  }

  /**
   */
  public static class Criteria extends GeneratedCriteria {

    protected Criteria() {
      super();
    }
  }

  public static class Criterion {

    private String condition;

    private Object value;

    private Object secondValue;

    private boolean noValue;

    private boolean singleValue;

    private boolean betweenValue;

    private boolean listValue;

    private String typeHandler;

    public String getCondition() {
      return condition;
    }

    public Object getValue() {
      return value;
    }

    public Object getSecondValue() {
      return secondValue;
    }

    public boolean isNoValue() {
      return noValue;
    }

    public boolean isSingleValue() {
      return singleValue;
    }

    public boolean isBetweenValue() {
      return betweenValue;
    }

    public boolean isListValue() {
      return listValue;
    }

    public String getTypeHandler() {
      return typeHandler;
    }

    protected Criterion(String condition) {
      super();
      this.condition = condition;
      this.typeHandler = null;
      this.noValue = true;
    }

    protected Criterion(String condition, Object value, String typeHandler) {
      super();
      this.condition = condition;
      this.value = value;
      this.typeHandler = typeHandler;
      if (value instanceof List<?>) {
        this.listValue = true;
      } else {
        this.singleValue = true;
      }
    }

    protected Criterion(String condition, Object value) {
      this(condition, value, null);
    }

    protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
      super();
      this.condition = condition;
      this.value = value;
      this.secondValue = secondValue;
      this.typeHandler = typeHandler;
      this.betweenValue = true;
    }

    protected Criterion(String condition, Object value, Object secondValue) {
      this(condition, value, secondValue, null);
    }
  }
}