package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class CommissionAreaPlan extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name;

  private String ownShopId;

  private double provinceRate;

  private double cityRate;

  private double areaRate;

  private Boolean defaultStatus;

  private Boolean archive;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(String ownShopId) {
    this.ownShopId = ownShopId;
  }

  public double getProvinceRate() {
    return provinceRate;
  }

  public void setProvinceRate(double provinceRate) {
    this.provinceRate = provinceRate;
  }

  public double getCityRate() {
    return cityRate;
  }

  public void setCityRate(double cityRate) {
    this.cityRate = cityRate;
  }

  public double getAreaRate() {
    return areaRate;
  }

  public void setAreaRate(double areaRate) {
    this.areaRate = areaRate;
  }

  public Boolean getDefaultStatus() {
    return defaultStatus;
  }

  public void setDefaultStatus(Boolean defaultStatus) {
    this.defaultStatus = defaultStatus;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}