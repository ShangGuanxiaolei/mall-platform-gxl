package com.xquark.dal.model;

import com.xquark.dal.type.JobStatus;
import com.xquark.dal.type.JobType;
import java.io.Serializable;
import java.util.Date;

/**
 * jobScheduler
 *
 * @author wangxinhua
 */
public class JobSchedulerLog implements Serializable {

  private Long jobId;

  /**
   * 解冻积分job
   */
  private Integer jobType;

  private String jobName;

  private Date startDate;

  /**
   * 0 开始，1 是成功，4 不成功
   */
  private Integer jobStatus;

  private Date updatedDate;

  private Date completedDate;

  private String exceptionMsg;

  private static final long serialVersionUID = 1L;

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public Integer getJobType() {
    return jobType;
  }

  public void setJobType(Integer jobType) {
    this.jobType = jobType;
  }

  public String getJobName() {
    return jobName;
  }

  public void setJobName(String jobName) {
    this.jobName = jobName;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Integer getJobStatus() {
    return jobStatus;
  }

  public void setJobStatus(Integer jobStatus) {
    this.jobStatus = jobStatus;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public Date getCompletedDate() {
    return completedDate;
  }

  public void setCompletedDate(Date completedDate) {
    this.completedDate = completedDate;
  }

  public String getExceptionMsg() {
    return exceptionMsg;
  }

  public void setExceptionMsg(String exceptionMsg) {
    this.exceptionMsg = exceptionMsg;
  }

  public void complete(JobStatus status, String msg) {
    this.setCompletedDate(new Date());
    this.setJobStatus(status.getCode());
    this.setExceptionMsg(msg);
  }

  /**
   * 积分job日志
   */
  public static JobSchedulerLog empty(JobType type) {
    JobSchedulerLog log = new JobSchedulerLog();
    int code = type.getCode();
    log.setJobType(code);
    log.setJobName(type.getDesc());
    log.setStartDate(new Date());
    log.setJobStatus(JobStatus.CREATED.getCode());
    return log;
  }

}