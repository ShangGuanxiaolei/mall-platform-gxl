package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AgentStatus;


public class TeamApply extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private String name;

  private String phone;

  private AgentStatus status;

  private String userId;

  private String teamId;

  private String parentUserId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public AgentStatus getStatus() {
    return status;
  }

  public void setStatus(AgentStatus status) {
    this.status = status;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTeamId() {
    return teamId;
  }

  public void setTeamId(String teamId) {
    this.teamId = teamId;
  }

  public String getParentUserId() {
    return parentUserId;
  }

  public void setParentUserId(String parentUserId) {
    this.parentUserId = parentUserId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}