package com.xquark.dal.model.promotion;

import com.google.common.base.Optional;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.model.PromotionFlashSale;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public abstract class PromotionProduct extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = -4416918346258397820L;

  private final static BigDecimal HUNDRED_SCALA = BigDecimal.valueOf(100);

  /**
   * 活动id
   */
  private String promotionId;

  /**
   * 活动商品id
   */
  private String productId;

  /**
   * 活动优惠价
   */
  private BigDecimal discount;

  /**
   * 是否删除
   */
  private Boolean archive;

  /**
   * 活动商品库存
   */
  private Long amount;

  /**
   * 活动商品销量
   */
  private Long sales = 0L;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public Long getSales() {
    return sales;
  }

  public void setSales(Long sales) {
    this.sales = sales;
  }

  public BigDecimal getPercent() {
    amount = Optional.fromNullable(amount).or(0L);
    sales = Optional.fromNullable(sales).or(0L);
    BigDecimal bSales = BigDecimal.valueOf(sales);
    BigDecimal bAmount = BigDecimal.valueOf(amount);
    BigDecimal total = bSales.add(bAmount);
    if (total.signum() == 0) {
      return BigDecimal.ZERO;
    }
    return bSales.divide(total, 2, BigDecimal.ROUND_HALF_EVEN);
  }

  public String getPercentStr() {
    return new DecimalFormat("#.##").format(getPercent()
        .multiply(HUNDRED_SCALA)).concat("%");
  }

  public static void main(String[] args) {
    PromotionFlashSale f = new PromotionFlashSale();
    f.setSales(2L);
    f.setAmount(11L);
//    f.setAmount(1);
    System.out.println(f.getPercentStr());
  }

}
