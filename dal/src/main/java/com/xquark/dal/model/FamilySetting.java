package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class FamilySetting extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name;

  private String ownerShopId;

  private Integer subordinateAmount;

  private Integer greaterProbationSumDemand;

  private Integer lessProbationSumDemand;

  private BigDecimal probationCm;

  private Integer greaterPermanentSumDemand;

  private Integer lessPermanentSumDemand;

  private BigDecimal permanentCm;


  private Boolean archive;


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public Integer getSubordinateAmount() {
    return subordinateAmount;
  }

  public void setSubordinateAmount(Integer subordinateAmount) {
    this.subordinateAmount = subordinateAmount;
  }

  public Integer getGreaterProbationSumDemand() {
    return greaterProbationSumDemand;
  }

  public void setGreaterProbationSumDemand(Integer greaterProbationSumDemand) {
    this.greaterProbationSumDemand = greaterProbationSumDemand;
  }

  public Integer getLessProbationSumDemand() {
    return lessProbationSumDemand;
  }

  public void setLessProbationSumDemand(Integer lessProbationSumDemand) {
    this.lessProbationSumDemand = lessProbationSumDemand;
  }

  public BigDecimal getProbationCm() {
    return probationCm;
  }

  public void setProbationCm(BigDecimal probationCm) {
    this.probationCm = probationCm;
  }

  public Integer getGreaterPermanentSumDemand() {
    return greaterPermanentSumDemand;
  }

  public void setGreaterPermanentSumDemand(Integer greaterPermanentSumDemand) {
    this.greaterPermanentSumDemand = greaterPermanentSumDemand;
  }

  public Integer getLessPermanentSumDemand() {
    return lessPermanentSumDemand;
  }

  public void setLessPermanentSumDemand(Integer lessPermanentSumDemand) {
    this.lessPermanentSumDemand = lessPermanentSumDemand;
  }

  public BigDecimal getPermanentCm() {
    return permanentCm;
  }

  public void setPermanentCm(BigDecimal permanentCm) {
    this.permanentCm = permanentCm;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}