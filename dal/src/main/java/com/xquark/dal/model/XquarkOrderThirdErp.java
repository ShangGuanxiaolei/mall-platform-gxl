package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * xquark_order_third_erp
 * @author 
 */
public class XquarkOrderThirdErp implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * erp订单编号
     */
    private String erpOrderNo;

    /**
     * 运单号
     */
    private String wayNum;

    /**
     * 快递公司
     */
    private String company;

    /**
     * 快递模板名称
     */
    private String orderModel;

    /**
     * 仓库
     */
    private String wareHouse;

    /**
     * 买家名称
     */
    private String buyName;

    /**
     * 收件人
     */
    private String receiver;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 详细地址
     */
    private String street;

    /**
     * 订单编号
     */
    private String thirdOrderNo;

    /**
     * 插旗颜色
     */
    private String flagColor;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 卖家备注
     */
    private String sellerRemark;

    /**
     * 买家留言
     */
    private String buyerRemark;

    /**
     * 快递单备注
     */
    private String orderRemark;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 子订单编号
     */
    private String orderItemNo;

    /**
     * 商品名称
     */
    private String goodName;

    /**
     * 商品简称
     */
    private String nameSing;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * 商品sku编码
     */
    private String skuCode;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 商品价格
     */
    private String price;

    /**
     * 支付金额
     */
    private String payMoney;

    /**
     * 优惠金额
     */
    private String discounts;

    /**
     * 运费
     */
    private String carriage;

    /**
     * 下单时间
     */
    private Date orderTime;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 打发货单时间
     */
    private Date printSend;

    /**
     * 打快递单时间
     */
    private Date printNo;

    /**
     * 打发货单操作人
     */
    private String printSendMan;

    /**
     * 打发货单操作人
     */
    private String printNoMan;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String area;

    /**
     * 原始单号
     */
    private String firstOrderNo;

    /**
     * 批次号
     */
    private String lot;

    /**
     * 是否删除0表示删除1表示没删除，默认为1
     */
    private String achieve;

    /**
     * 创建时间
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getErpOrderNo() {
        return erpOrderNo;
    }

    public void setErpOrderNo(String erpOrderNo) {
        this.erpOrderNo = erpOrderNo;
    }

    public String getWayNum() {
        return wayNum;
    }

    public void setWayNum(String wayNum) {
        this.wayNum = wayNum;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(String orderModel) {
        this.orderModel = orderModel;
    }

    public String getWareHouse() {
        return wareHouse;
    }

    public void setWareHouse(String wareHouse) {
        this.wareHouse = wareHouse;
    }

    public String getBuyName() {
        return buyName;
    }

    public void setBuyName(String buyName) {
        this.buyName = buyName;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getThirdOrderNo() {
        return thirdOrderNo;
    }

    public void setThirdOrderNo(String thirdOrderNo) {
        this.thirdOrderNo = thirdOrderNo;
    }

    public String getFlagColor() {
        return flagColor;
    }

    public void setFlagColor(String flagColor) {
        this.flagColor = flagColor;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSellerRemark() {
        return sellerRemark;
    }

    public void setSellerRemark(String sellerRemark) {
        this.sellerRemark = sellerRemark;
    }

    public String getBuyerRemark() {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark) {
        this.buyerRemark = buyerRemark;
    }

    public String getOrderRemark() {
        return orderRemark;
    }

    public void setOrderRemark(String orderRemark) {
        this.orderRemark = orderRemark;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderItemNo() {
        return orderItemNo;
    }

    public void setOrderItemNo(String orderItemNo) {
        this.orderItemNo = orderItemNo;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getNameSing() {
        return nameSing;
    }

    public void setNameSing(String nameSing) {
        this.nameSing = nameSing;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getDiscounts() {
        return discounts;
    }

    public void setDiscounts(String discounts) {
        this.discounts = discounts;
    }

    public String getCarriage() {
        return carriage;
    }

    public void setCarriage(String carriage) {
        this.carriage = carriage;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getPrintSend() {
        return printSend;
    }

    public void setPrintSend(Date printSend) {
        this.printSend = printSend;
    }

    public Date getPrintNo() {
        return printNo;
    }

    public void setPrintNo(Date printNo) {
        this.printNo = printNo;
    }

    public String getPrintSendMan() {
        return printSendMan;
    }

    public void setPrintSendMan(String printSendMan) {
        this.printSendMan = printSendMan;
    }

    public String getPrintNoMan() {
        return printNoMan;
    }

    public void setPrintNoMan(String printNoMan) {
        this.printNoMan = printNoMan;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFirstOrderNo() {
        return firstOrderNo;
    }

    public void setFirstOrderNo(String firstOrderNo) {
        this.firstOrderNo = firstOrderNo;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getAchieve() {
        return achieve;
    }

    public void setAchieve(String achieve) {
        this.achieve = achieve;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkOrderThirdErp other = (XquarkOrderThirdErp) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getErpOrderNo() == null ? other.getErpOrderNo() == null : this.getErpOrderNo().equals(other.getErpOrderNo()))
            && (this.getWayNum() == null ? other.getWayNum() == null : this.getWayNum().equals(other.getWayNum()))
            && (this.getCompany() == null ? other.getCompany() == null : this.getCompany().equals(other.getCompany()))
            && (this.getOrderModel() == null ? other.getOrderModel() == null : this.getOrderModel().equals(other.getOrderModel()))
            && (this.getWareHouse() == null ? other.getWareHouse() == null : this.getWareHouse().equals(other.getWareHouse()))
            && (this.getBuyName() == null ? other.getBuyName() == null : this.getBuyName().equals(other.getBuyName()))
            && (this.getReceiver() == null ? other.getReceiver() == null : this.getReceiver().equals(other.getReceiver()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getStreet() == null ? other.getStreet() == null : this.getStreet().equals(other.getStreet()))
            && (this.getThirdOrderNo() == null ? other.getThirdOrderNo() == null : this.getThirdOrderNo().equals(other.getThirdOrderNo()))
            && (this.getFlagColor() == null ? other.getFlagColor() == null : this.getFlagColor().equals(other.getFlagColor()))
            && (this.getShopName() == null ? other.getShopName() == null : this.getShopName().equals(other.getShopName()))
            && (this.getSellerRemark() == null ? other.getSellerRemark() == null : this.getSellerRemark().equals(other.getSellerRemark()))
            && (this.getBuyerRemark() == null ? other.getBuyerRemark() == null : this.getBuyerRemark().equals(other.getBuyerRemark()))
            && (this.getOrderRemark() == null ? other.getOrderRemark() == null : this.getOrderRemark().equals(other.getOrderRemark()))
            && (this.getOrderStatus() == null ? other.getOrderStatus() == null : this.getOrderStatus().equals(other.getOrderStatus()))
            && (this.getOrderType() == null ? other.getOrderType() == null : this.getOrderType().equals(other.getOrderType()))
            && (this.getOrderItemNo() == null ? other.getOrderItemNo() == null : this.getOrderItemNo().equals(other.getOrderItemNo()))
            && (this.getGoodName() == null ? other.getGoodName() == null : this.getGoodName().equals(other.getGoodName()))
            && (this.getNameSing() == null ? other.getNameSing() == null : this.getNameSing().equals(other.getNameSing()))
            && (this.getSkuName() == null ? other.getSkuName() == null : this.getSkuName().equals(other.getSkuName()))
            && (this.getSkuCode() == null ? other.getSkuCode() == null : this.getSkuCode().equals(other.getSkuCode()))
            && (this.getCount() == null ? other.getCount() == null : this.getCount().equals(other.getCount()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getPayMoney() == null ? other.getPayMoney() == null : this.getPayMoney().equals(other.getPayMoney()))
            && (this.getDiscounts() == null ? other.getDiscounts() == null : this.getDiscounts().equals(other.getDiscounts()))
            && (this.getCarriage() == null ? other.getCarriage() == null : this.getCarriage().equals(other.getCarriage()))
            && (this.getOrderTime() == null ? other.getOrderTime() == null : this.getOrderTime().equals(other.getOrderTime()))
            && (this.getPayTime() == null ? other.getPayTime() == null : this.getPayTime().equals(other.getPayTime()))
            && (this.getSendTime() == null ? other.getSendTime() == null : this.getSendTime().equals(other.getSendTime()))
            && (this.getPrintSend() == null ? other.getPrintSend() == null : this.getPrintSend().equals(other.getPrintSend()))
            && (this.getPrintNo() == null ? other.getPrintNo() == null : this.getPrintNo().equals(other.getPrintNo()))
            && (this.getPrintSendMan() == null ? other.getPrintSendMan() == null : this.getPrintSendMan().equals(other.getPrintSendMan()))
            && (this.getPrintNoMan() == null ? other.getPrintNoMan() == null : this.getPrintNoMan().equals(other.getPrintNoMan()))
            && (this.getProvince() == null ? other.getProvince() == null : this.getProvince().equals(other.getProvince()))
            && (this.getCity() == null ? other.getCity() == null : this.getCity().equals(other.getCity()))
            && (this.getArea() == null ? other.getArea() == null : this.getArea().equals(other.getArea()))
            && (this.getFirstOrderNo() == null ? other.getFirstOrderNo() == null : this.getFirstOrderNo().equals(other.getFirstOrderNo()))
            && (this.getLot() == null ? other.getLot() == null : this.getLot().equals(other.getLot()))
            && (this.getAchieve() == null ? other.getAchieve() == null : this.getAchieve().equals(other.getAchieve()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getErpOrderNo() == null) ? 0 : getErpOrderNo().hashCode());
        result = prime * result + ((getWayNum() == null) ? 0 : getWayNum().hashCode());
        result = prime * result + ((getCompany() == null) ? 0 : getCompany().hashCode());
        result = prime * result + ((getOrderModel() == null) ? 0 : getOrderModel().hashCode());
        result = prime * result + ((getWareHouse() == null) ? 0 : getWareHouse().hashCode());
        result = prime * result + ((getBuyName() == null) ? 0 : getBuyName().hashCode());
        result = prime * result + ((getReceiver() == null) ? 0 : getReceiver().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getStreet() == null) ? 0 : getStreet().hashCode());
        result = prime * result + ((getThirdOrderNo() == null) ? 0 : getThirdOrderNo().hashCode());
        result = prime * result + ((getFlagColor() == null) ? 0 : getFlagColor().hashCode());
        result = prime * result + ((getShopName() == null) ? 0 : getShopName().hashCode());
        result = prime * result + ((getSellerRemark() == null) ? 0 : getSellerRemark().hashCode());
        result = prime * result + ((getBuyerRemark() == null) ? 0 : getBuyerRemark().hashCode());
        result = prime * result + ((getOrderRemark() == null) ? 0 : getOrderRemark().hashCode());
        result = prime * result + ((getOrderStatus() == null) ? 0 : getOrderStatus().hashCode());
        result = prime * result + ((getOrderType() == null) ? 0 : getOrderType().hashCode());
        result = prime * result + ((getOrderItemNo() == null) ? 0 : getOrderItemNo().hashCode());
        result = prime * result + ((getGoodName() == null) ? 0 : getGoodName().hashCode());
        result = prime * result + ((getNameSing() == null) ? 0 : getNameSing().hashCode());
        result = prime * result + ((getSkuName() == null) ? 0 : getSkuName().hashCode());
        result = prime * result + ((getSkuCode() == null) ? 0 : getSkuCode().hashCode());
        result = prime * result + ((getCount() == null) ? 0 : getCount().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getPayMoney() == null) ? 0 : getPayMoney().hashCode());
        result = prime * result + ((getDiscounts() == null) ? 0 : getDiscounts().hashCode());
        result = prime * result + ((getCarriage() == null) ? 0 : getCarriage().hashCode());
        result = prime * result + ((getOrderTime() == null) ? 0 : getOrderTime().hashCode());
        result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
        result = prime * result + ((getSendTime() == null) ? 0 : getSendTime().hashCode());
        result = prime * result + ((getPrintSend() == null) ? 0 : getPrintSend().hashCode());
        result = prime * result + ((getPrintNo() == null) ? 0 : getPrintNo().hashCode());
        result = prime * result + ((getPrintSendMan() == null) ? 0 : getPrintSendMan().hashCode());
        result = prime * result + ((getPrintNoMan() == null) ? 0 : getPrintNoMan().hashCode());
        result = prime * result + ((getProvince() == null) ? 0 : getProvince().hashCode());
        result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
        result = prime * result + ((getArea() == null) ? 0 : getArea().hashCode());
        result = prime * result + ((getFirstOrderNo() == null) ? 0 : getFirstOrderNo().hashCode());
        result = prime * result + ((getLot() == null) ? 0 : getLot().hashCode());
        result = prime * result + ((getAchieve() == null) ? 0 : getAchieve().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", erpOrderNo=").append(erpOrderNo);
        sb.append(", wayNum=").append(wayNum);
        sb.append(", company=").append(company);
        sb.append(", orderModel=").append(orderModel);
        sb.append(", wareHouse=").append(wareHouse);
        sb.append(", buyName=").append(buyName);
        sb.append(", receiver=").append(receiver);
        sb.append(", phone=").append(phone);
        sb.append(", street=").append(street);
        sb.append(", thirdOrderNo=").append(thirdOrderNo);
        sb.append(", flagColor=").append(flagColor);
        sb.append(", shopName=").append(shopName);
        sb.append(", sellerRemark=").append(sellerRemark);
        sb.append(", buyerRemark=").append(buyerRemark);
        sb.append(", orderRemark=").append(orderRemark);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", orderType=").append(orderType);
        sb.append(", orderItemNo=").append(orderItemNo);
        sb.append(", goodName=").append(goodName);
        sb.append(", nameSing=").append(nameSing);
        sb.append(", skuName=").append(skuName);
        sb.append(", skuCode=").append(skuCode);
        sb.append(", count=").append(count);
        sb.append(", price=").append(price);
        sb.append(", payMoney=").append(payMoney);
        sb.append(", discounts=").append(discounts);
        sb.append(", carriage=").append(carriage);
        sb.append(", orderTime=").append(orderTime);
        sb.append(", payTime=").append(payTime);
        sb.append(", sendTime=").append(sendTime);
        sb.append(", printSend=").append(printSend);
        sb.append(", printNo=").append(printNo);
        sb.append(", printSendMan=").append(printSendMan);
        sb.append(", printNoMan=").append(printNoMan);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", firstOrderNo=").append(firstOrderNo);
        sb.append(", lot=").append(lot);
        sb.append(", achieve=").append(achieve);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}