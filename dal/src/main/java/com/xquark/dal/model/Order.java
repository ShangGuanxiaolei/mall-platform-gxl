package com.xquark.dal.model;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.*;
import org.apache.commons.lang3.ObjectUtils;

import java.math.BigDecimal;
import java.util.Date;

public class Order extends BaseEntityImpl implements Cloneable, PaidAble {
  //物流专用dest
  private String destForLogistics;

  /** 订单可取消时间 */
  private static final int CANCELABLE_SECONDS = 30 * 60;

  private static final long serialVersionUID = 1L;

  // 订单类型缩写
  public static final String SUB_ORDER_TYPE = "s";

  private String buyerId; // 注册用户ID
  private String shopId; // 卖家店铺ID
  private Long shareCpId; // 直接分享人cpId，与fromCpId区分
  private String rootShopId; // 总店店铺ID

  private String sellerId; // 卖家ID
  private Boolean vip; // 是否vip买家
  private String mainOrderId; // 主订单ID

  @ApiModelProperty(value = "交易订单号")
  private String orderNo; // 交易订单号

  @ApiModelProperty(value = "交易类型")
  private OrderType type; // 交易类型

  @ApiModelProperty(value = "订单状态")
  private OrderStatus status; // 订单状态

  @ApiModelProperty(value = "支付类型")
  private PaymentMode payType; // 支付类型

  @ApiModelProperty(
      value = "订单总金额 totalFee = goodsFee + logisticsFee - discountFee totalFee = paidFee")
  private BigDecimal
      totalFee; // 订单总金额 totalFee = goodsFee + logisticsFee - discountFee totalFee = paidFee

  @ApiModelProperty(value = "订单商品总金额")
  private BigDecimal goodsFee; // 订单商品总金额

  @ApiModelProperty(value = "订单物流金额")
  private BigDecimal logisticsFee; // 订单物流金额

  private BigDecimal logisticDiscount;

  /**积分抵扣的邮费值*/
  private BigDecimal commissionLogisticsDiscount = BigDecimal.ZERO;

  private LogisticDiscountType logisticDiscountType;

  @ApiModelProperty(value = "订单折扣金额")
  private BigDecimal discountFee; // 订单折扣金额

  @ApiModelProperty(value = "订单付款总金额")
  private BigDecimal paidFee; // 订单付款总金额

  @ApiModelProperty(value = "付款状态")
  private PayStatus paidStatus; // 付款状态

  @ApiModelProperty(value = "付款时间")
  private Date paidAt; // 付款时间

  @ApiModelProperty(value = "支付单号")
  private String payNo; // 支付单号

  @ApiModelProperty(value = "订单成功时间")
  private Date succeedAt; // 订单成功时间

  @ApiModelProperty(value = "退款金额")
  private BigDecimal refundFee; // 退款金额，对于新接口为 商品额退款+运费退款

  @ApiModelProperty(value = "退款（商品额）")
  private BigDecimal refundGoodsFee; // 退款（商品额）

  @ApiModelProperty(value = "退款（运费）")
  private BigDecimal refundLogisticsFee; // 退款（运费）

  @ApiModelProperty(value = "退平台优惠")
  private BigDecimal refundPlatformFee; // 退平台优惠

  @ApiModelProperty(value = "退款时间")
  private Date refundAt; // 退款时间

  @ApiModelProperty(value = "订单取消时间")
  private Date cancelledAt; // 订单取消时间

  @ApiModelProperty(value = "订单发货时间")
  private Date shippedAt; // 订单发货时间

  @ApiModelProperty(value = "物流公司")
  private String logisticsCompany; // 物流公司

  @ApiModelProperty(value = "物流公司订单号")
  private String logisticsOrderNo; // 物流公司订单号

  @ApiModelProperty(value = "物流公司官网")
  private String logisticsOfficial; // 物流公司官网

  @ApiModelProperty("是否为全积分兑换订单")
  private Boolean fullYundou;

  @ApiModelProperty("该订单使用的积分")
  private Long paidYundou;

  private BigDecimal paidPoint = BigDecimal.ZERO;
  private BigDecimal paidPointPacket = BigDecimal.ZERO;

  private BigDecimal paidCommission = BigDecimal.ZERO; // 总得分

  private Long fromCpId;

  /** 是否开具发票 */
  private Boolean needInvoice;

  private ProductSource source; // 商品来源

  private Date latestSignAt; // 预计最晚收货时间 (延迟收货)
  private Date remindShipAt; // 上次提醒卖家发货时间

  @ApiModelProperty(value = "备注")
  private String remark; // 备注

  private String remarkAdmin; // 备注-管理端

  private Boolean archive; // 订单是否被删除
  private String unionId; // 第三方ID
  private String tuId; // 多方分佣
  private RefundType refundType;
  private String promotionId; // 店铺使用的优惠
  private String couponId; // 店铺使用的优惠券
  private Date checkingAt; // 订单对账时间
  private String partner; // 订单类别，用xquark_domain中的code表示
  private UserPartnerType partnerType; // 订单类型  //暂时不删除，待稳定后再删除
  private String partnerOrderNo; // 合作方订单编号
  @Deprecated // 移到主订单了
  private OrderSingleType orderSingleType; // 下单方式

  private String destName; // 订单投递的目的地名称
  private String dest; // 该订单对应商品的合作方
  private String warehouseName; // 推送仓库名称
  private String shopName; // 推送店铺名称

  private Date sendErpAt; // erp订单发送时间
  private SendErpStatus sendErpStatus; // erp订单发送状态
  private SendErpDest sendErpDest; // 发送erp目的地
  private String sendErpLog; // erp发送log
  private Date sysnErpAt; // erp物流同步时间
  private SysnErpStatus sysnErpStatus; // erp物流同步状态
  private String sysnErpLog; // erp同步log

  @ApiModelProperty(value = "是否自提")
  private Integer isPickup; // 是否自提

  @ApiModelProperty(value = "订单是否确认")
  private Integer isConfirm; // 订单是否确认

  @ApiModelProperty(value = "是否是付尾款订单")
  private Boolean isPayRest;

  private BigDecimal reduction;

  /** 是否具备申请售后的能力 */
  private Boolean capableToRefund;

  /** 上一次改变capableToRefund的时间 */
  private Date updateRefundEntryAt;

  private Date orderMonth;

  private String preOrderNo; // 预约订单号
  /**
   * 订单是否在一小时内
   *
   * @return
   */
  public boolean isInOneHour() {
    Optional<Date> dateOptional = Optional.fromNullable(this.getPaidAt());
    final boolean inOneHour =
        dateOptional
            .transform(
                new Function<Date, Boolean>() {
                  @Override
                  public Boolean apply(Date input) {
                    long t = System.currentTimeMillis() - input.getTime();
                    return t <= 3600000;
                  }
                })
            .or(false);
    return inOneHour;
  }

  public String getDestForLogistics() {
    return destForLogistics;
  }

  public void setDestForLogistics(String destForLogistics) {
    this.destForLogistics = destForLogistics;
  }

  public Date getOrderMonth() {
    return orderMonth;
  }

  public void setOrderMonth(Date orderMonth) {
    this.orderMonth = orderMonth;
  }

  public Integer getIsConfirm() {
    return isConfirm;
  }

  public void setIsConfirm(Integer isConfirm) {
    this.isConfirm = isConfirm;
  }

  public Integer getIsPickup() {
    if (isPickup == null) {
      return 0;
    }
    return isPickup;
  }

  public void setIsPickup(Integer isPickup) {
    this.isPickup = isPickup;
  }

  private OrderSortType orderType; // 订单类型

  public OrderSortType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderSortType orderType) {
    this.orderType = orderType;
  }

  public Date getSendErpAt() {
    return sendErpAt;
  }

  public void setSendErpAt(Date sendErpAt) {
    this.sendErpAt = sendErpAt;
  }

  public SendErpStatus getSendErpStatus() {
    return sendErpStatus;
  }

  public void setSendErpStatus(SendErpStatus sendErpStatus) {
    this.sendErpStatus = sendErpStatus;
  }

  public SendErpDest getSendErpDest() {
    return sendErpDest;
  }

  public void setSendErpDest(SendErpDest sendErpDest) {
    this.sendErpDest = sendErpDest;
  }

  public String getSendErpLog() {
    return sendErpLog;
  }

  public void setSendErpLog(String sendErpLog) {
    this.sendErpLog = sendErpLog;
  }

  public Date getSysnErpAt() {
    return sysnErpAt;
  }

  public void setSysnErpAt(Date sysnErpAt) {
    this.sysnErpAt = sysnErpAt;
  }

  public SysnErpStatus getSysnErpStatus() {
    return sysnErpStatus;
  }

  public void setSysnErpStatus(SysnErpStatus sysnErpStatus) {
    this.sysnErpStatus = sysnErpStatus;
  }

  public String getSysnErpLog() {
    return sysnErpLog;
  }

  public void setSysnErpLog(String sysnErpLog) {
    this.sysnErpLog = sysnErpLog;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getTuId() {
    return tuId;
  }

  public void setTuId(String tuId) {
    this.tuId = tuId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public OrderType getType() {
    return type;
  }

  public void setType(OrderType type) {
    this.type = type;
  }

  public PaymentMode getPayType() {
    return payType;
  }

  public void setPayType(PaymentMode payType) {
    this.payType = payType;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public void setBuyerId(String buyerId) {
    this.buyerId = buyerId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getSellerId() {
    return sellerId;
  }

  public void setSellerId(String sellerId) {
    this.sellerId = sellerId;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public BigDecimal getLogisticDiscount() {
    return logisticDiscount;
  }

  public void setLogisticDiscount(BigDecimal logisticDiscount) {
    this.logisticDiscount = logisticDiscount;
  }

  public LogisticDiscountType getLogisticDiscountType() {
    return logisticDiscountType;
  }

  public void setLogisticDiscountType(LogisticDiscountType logisticDiscountType) {
    this.logisticDiscountType = logisticDiscountType;
  }

  public BigDecimal getCommissionLogisticsDiscount() {
    return commissionLogisticsDiscount;
  }

  public void setCommissionLogisticsDiscount(BigDecimal commissionLogisticsDiscount) {
    this.commissionLogisticsDiscount = commissionLogisticsDiscount;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getPaidFee() {
    return paidFee;
  }

  public void setPaidFee(BigDecimal paidFee) {
    this.paidFee = paidFee;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public Date getPaidAt() {
    return paidAt;
  }

  public void setPaidAt(Date paidAt) {
    this.paidAt = paidAt;
  }

  public PayStatus getPaidStatus() {
    return paidStatus;
  }

  public void setPaidStatus(PayStatus paidStatus) {
    this.paidStatus = paidStatus;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public Date getCancelledAt() {
    return cancelledAt;
  }

  public void setCancelledAt(Date cancelledAt) {
    this.cancelledAt = cancelledAt;
  }

  public Date getShippedAt() {
    return shippedAt;
  }

  public void setShippedAt(Date shippedAt) {
    this.shippedAt = shippedAt;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getLogisticsOfficial() {
    return logisticsOfficial;
  }

  public void setLogisticsOfficial(String logisticsOfficial) {
    this.logisticsOfficial = logisticsOfficial;
  }

  public Date getSucceedAt() {
    return succeedAt;
  }

  public void setSucceedAt(Date succeedAt) {
    this.succeedAt = succeedAt;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getRemarkAdmin() {
    return remarkAdmin;
  }

  public void setRemarkAdmin(String remarkAdmin) {
    this.remarkAdmin = remarkAdmin;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public RefundType getRefundType() {
    return refundType;
  }

  public void setRefundType(RefundType refundType) {
    this.refundType = refundType;
  }

  public Boolean getVip() {
    return vip;
  }

  public void setVip(Boolean vip) {
    this.vip = vip;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public UserPartnerType getPartnerType() {
    return partnerType;
  }

  public void setPartnerType(UserPartnerType partnerType) {
    this.partnerType = partnerType;
  }

  public String getPayNo() {
    return payNo;
  }

  public void setPayNo(String payNo) {
    this.payNo = payNo;
  }

  public BigDecimal getRefundFee() {
    return ObjectUtils.defaultIfNull(refundFee, BigDecimal.ZERO);
  }

  public void setRefundFee(BigDecimal refundFee) {
    this.refundFee = refundFee;
  }

  public BigDecimal getRefundGoodsFee() {
    return ObjectUtils.defaultIfNull(refundGoodsFee, BigDecimal.ZERO);
  }

  public void setRefundGoodsFee(BigDecimal refundGoodsFee) {
    this.refundGoodsFee = refundGoodsFee;
  }

  public BigDecimal getRefundLogisticsFee() {
    return ObjectUtils.defaultIfNull(refundLogisticsFee, BigDecimal.ZERO);
  }

  public void setRefundLogisticsFee(BigDecimal refundLogisticsFee) {
    this.refundLogisticsFee = refundLogisticsFee;
  }

  /** 需统一移除 */
  public BigDecimal getRefundPlatformFee() {
    return ObjectUtils.defaultIfNull(refundPlatformFee, BigDecimal.ZERO);
  }

  public void setRefundPlatformFee(BigDecimal refundPlatformFee) {
    this.refundPlatformFee = refundPlatformFee;
  }

  public Date getRefundAt() {
    return refundAt;
  }

  public void setRefundAt(Date refundAt) {
    this.refundAt = refundAt;
  }

  public Date getCheckingAt() {
    return checkingAt;
  }

  public void setCheckingAt(Date checkingAt) {
    this.checkingAt = checkingAt;
  }

  public Boolean getFullYundou() {
    return fullYundou;
  }

  public void setFullYundou(Boolean fullYundou) {
    this.fullYundou = fullYundou;
  }

  public Long getPaidYundou() {
    return paidYundou;
  }

  public void setPaidYundou(Long paidYundou) {
    this.paidYundou = paidYundou;
  }

  public BigDecimal getPaidPoint() {
    return paidPoint;
  }

  public void setPaidPoint(BigDecimal paidPoint) {
    this.paidPoint = paidPoint;
  }

  public BigDecimal getPaidPointPacket() {
    return paidPointPacket;
  }

  public void setPaidPointPacket(BigDecimal paidPointPacket) {
    this.paidPointPacket = paidPointPacket;
  }

  public BigDecimal getPaidCommission() {
    return paidCommission;
  }

  public void setPaidCommission(BigDecimal paidCommission) {
    this.paidCommission = paidCommission;
  }

  public Date getLatestSignAt() {
    return latestSignAt;
  }

  public void setLatestSignAt(Date latestSignAt) {
    this.latestSignAt = latestSignAt;
  }

  public Date getRemindShipAt() {
    return remindShipAt;
  }

  public void setRemindShipAt(Date remindShipAt) {
    this.remindShipAt = remindShipAt;
  }

  @Deprecated
  public OrderSingleType getOrderSingleType() {
    return orderSingleType;
  }

  @Deprecated
  public void setOrderSingleType(OrderSingleType orderSingleType) {
    this.orderSingleType = orderSingleType;
  }

  public String getMainOrderId() {
    return mainOrderId;
  }

  public void setMainOrderId(String mainOrderId) {
    this.mainOrderId = mainOrderId;
  }

  public String getDestName() {
    return destName;
  }

  public void setDestName(String destName) {
    this.destName = destName;
  }

  public String getDest() {
    return dest;
  }

  public void setDest(String dest) {
    this.dest = dest;
  }

  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getRootShopId() {
    return rootShopId;
  }

  public void setRootShopId(String rootShopId) {
    this.rootShopId = rootShopId;
  }

  public Boolean isPayRest() {
    return isPayRest;
  }

  public void setIsPayRest(Boolean payRest) {
    isPayRest = payRest;
  }

  public Boolean getNeedInvoice() {
    return needInvoice;
  }

  public void setNeedInvoice(Boolean needInvoice) {
    this.needInvoice = needInvoice;
  }

  public ProductSource getSource() {
    return source;
  }

  public void setSource(ProductSource source) {
    this.source = source;
  }

  public String getPartnerOrderNo() {
    return partnerOrderNo;
  }

  public void setPartnerOrderNo(String partnerOrderNo) {
    this.partnerOrderNo = partnerOrderNo;
  }

  public BigDecimal getReduction() {
    return reduction;
  }

  public void setReduction(BigDecimal reduction) {
    this.reduction = reduction;
  }

  public Long getFromCpId() {
    return fromCpId;
  }

  public void setFromCpId(Long fromCpId) {
    this.fromCpId = fromCpId;
  }

  public Long getShareCpId() {
    return shareCpId;
  }

  public void setShareCpId(Long shareCpId) {
    this.shareCpId = shareCpId;
  }

  public Boolean getCapableToRefund() {
    return capableToRefund;
  }

  public void setCapableToRefund(Boolean capableToRefund) {
    this.capableToRefund = capableToRefund;
  }

  public Date getUpdateRefundEntryAt() {
    return updateRefundEntryAt;
  }

  public void setUpdateRefundEntryAt(Date updateRefundEntryAt) {
    this.updateRefundEntryAt = updateRefundEntryAt;
  }

  public String getPreOrderNo() {
    return preOrderNo;
  }

  public void setPreOrderNo(String preOrderNo) {
    this.preOrderNo = preOrderNo;
  }

  /** 是否一小时后退款 */
  public boolean isRefundAfterOneHour() {
    if (getStatus() != OrderStatus.CLOSED) {
      return false;
    }
    if (paidAt == null || refundAt == null) {
      return false;
    }
    return (refundAt.getTime() - paidAt.getTime()) > 3600;
  }

  @Override
  public Object clone()  {
    try {
      return super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    throw new RuntimeException("clone is fail");
  }
}
