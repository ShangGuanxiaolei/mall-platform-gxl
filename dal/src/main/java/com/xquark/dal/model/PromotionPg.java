package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.utils.DateUtils;

import java.util.Date;

public class PromotionPg extends BaseEntityImpl implements Archivable {

  private String pCode;
  private int pieceEffectTime;
  private String orgGroupQua;
  private String joinGroupQua;
  private int skuLimit;
  private int isWithPoint;
  private int isAchiev;
  private int isDeleted;
  private int jumpQueueTime;
  private String creator;
  private String updator;

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public int getPieceEffectTime() {
    return pieceEffectTime;
  }

  public void setPieceEffectTime(int pieceEffectTime) {
    this.pieceEffectTime = pieceEffectTime;
  }

  public String getOrgGroupQua() {
    return orgGroupQua;
  }

  public void setOrgGroupQua(String orgGroupQua) {
    this.orgGroupQua = orgGroupQua;
  }

  public String getJoinGroupQua() {
    return joinGroupQua;
  }

  public void setJoinGroupQua(String joinGroupQua) {
    this.joinGroupQua = joinGroupQua;
  }

  public int getSkuLimit() {
    return skuLimit;
  }

  public void setSkuLimit(int skuLimit) {
    this.skuLimit = skuLimit;
  }

  public int getIsWithPoint() {
    return isWithPoint;
  }

  public void setIsWithPoint(int isWithPoint) {
    this.isWithPoint = isWithPoint;
  }

  public int getIsAchiev() {
    return isAchiev;
  }

  public void setIsAchiev(int isAchiev) {
    this.isAchiev = isAchiev;
  }

  public int getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(int isDeleted) {
    this.isDeleted = isDeleted;
  }

  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }

  @Override
  public Boolean getArchive() {
    return null;
  }

  @Override
  public void setArchive(Boolean archive) {
  }

  public int getJumpQueueTime() {
    return jumpQueueTime;
  }

  public void setJumpQueueTime(int jumpQueueTime) {
    this.jumpQueueTime = jumpQueueTime;
  }

  /**
   * 是否在插队有效期内
   * @param groupFinishedTime 成团时间
   */
  public boolean isInCutLine(Date groupFinishedTime) {
    if (groupFinishedTime == null) {
      return false;
    }
    // 活动创建时间
    Date now = new Date();

    // 允许插队小时
    int cutLineHour = this.getJumpQueueTime();
    // 插队拼团有效期, 创建后按小时计算
    Date cutLineAbleTime = DateUtils.addSeconds(groupFinishedTime, cutLineHour * 3600);
    // 当前还在插队有效期内则允许
    return now.before(cutLineAbleTime);
  }
}
