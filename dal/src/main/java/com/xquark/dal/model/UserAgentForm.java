package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.status.AgentType;

/**
 * Created by root on 16-12-06.
 */
public class UserAgentForm extends UserAgent {

  @ApiModelProperty(value = "上级代理手机号")
  private String parentPhone;

  @ApiModelProperty(value = "上级代理用户id")
  private String parentId;

  @ApiModelProperty(value = "申请代理级别")
  private AgentType agentType;

  @ApiModelProperty(value = "省市区的区id", required = true)
  private String district;

  @ApiModelProperty(value = "不需要传入")
  private String addressDetail;

  private boolean isFromApp;

  @ApiModelProperty(value = "微信开放平台unionId")
  private String unionId;

  private String avatar;

  private String wechatName;

  public String getWechatName() {
    return wechatName;
  }

  public void setWechatName(String wechatName) {
    this.wechatName = wechatName;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public boolean getIsFromApp() {
    return isFromApp;
  }

  public void setIsFromApp(boolean fromApp) {
    isFromApp = fromApp;
  }

  public String getAddressDetail() {
    return addressDetail;
  }

  public void setAddressDetail(String addressDetail) {
    this.addressDetail = addressDetail;
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public AgentType getAgentType() {
    return agentType;
  }

  public void setAgentType(AgentType agentType) {
    this.agentType = agentType;
  }

  public String getParentPhone() {
    return parentPhone;
  }

  public void setParentPhone(String parentPhone) {
    this.parentPhone = parentPhone;
  }
}
