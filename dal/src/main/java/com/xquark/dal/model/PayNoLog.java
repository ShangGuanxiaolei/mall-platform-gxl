package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * 记录payNo变更的日志表
 * @author wangxinhua
 */
public class PayNoLog extends BaseEntityImpl {

    public PayNoLog() {
    }

    public PayNoLog(String mainOrderNo, String payNoFrom, String payNoTo,String payTypeFrom,String payTypeTo) {
        this.mainOrderNo = mainOrderNo;
        this.payNoFrom = payNoFrom;
        this.payNoTo = payNoTo;
        this.payTypeFrom = payTypeFrom;
        this.payTypeTo = payTypeTo;
    }

    /**
     * id
     */
    private String id;

    /**
     * 主订单编号
     */
    private String mainOrderNo;

    /**
     * 变更前pay_no
     */
    private String payNoFrom;

    private String payTypeFrom;

    private String payTypeTo;


    /**
     * 变更后pay_no
     */
    private String payNoTo;

    private Date createdAt;

    private Date updatedAt;


    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainOrderNo() {
        return mainOrderNo;
    }

    public void setMainOrderNo(String mainOrderNo) {
        this.mainOrderNo = mainOrderNo;
    }

    public String getPayNoFrom() {
        return payNoFrom;
    }

    public void setPayNoFrom(String payNoFrom) {
        this.payNoFrom = payNoFrom;
    }

    public String getPayNoTo() {
        return payNoTo;
    }

    public void setPayNoTo(String payNoTo) {
        this.payNoTo = payNoTo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPayTypeFrom() {
        return payTypeFrom;
    }

    public void setPayTypeFrom(String payTypeFrom) {
        this.payTypeFrom = payTypeFrom;
    }

    public String getPayTypeTo() {
        return payTypeTo;
    }

    public void setPayTypeTo(String payTypeTo) {
        this.payTypeTo = payTypeTo;
    }
}