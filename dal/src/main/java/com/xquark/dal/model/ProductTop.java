package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

/**
 * 爆款商品 2017-07-10 chh
 */
public class ProductTop extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String shopId;

  private String productId;

  private Boolean archive;

  private int idx;

  private String logo;

  private BigDecimal memberPrice = BigDecimal.ZERO;//会员价
  private BigDecimal proxyPrice = BigDecimal.ZERO;//代理价
  private BigDecimal changePrice = BigDecimal.ZERO;//兑换价现金部分

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public int getIdx() {
    return idx;
  }

  public void setIdx(int idx) {
    this.idx = idx;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public BigDecimal getMemberPrice() {
    return memberPrice;
  }

  public void setMemberPrice(BigDecimal memberPrice) {
    this.memberPrice = memberPrice;
  }

  public BigDecimal getProxyPrice() {
    return proxyPrice;
  }

  public void setProxyPrice(BigDecimal proxyPrice) {
    this.proxyPrice = proxyPrice;
  }

  public BigDecimal getChangePrice() {
    return changePrice;
  }

  public void setChangePrice(BigDecimal changePrice) {
    this.changePrice = changePrice;
  }
}