package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializerSync;

import java.util.Date;

public class PaymentSwitchConfig extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String profile;

  private String code;

  private Integer paymentId;

  private boolean status;

  private Date createdAt;

  private Date updatedAt;

  private boolean archive;

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(Integer paymentId) {
    this.paymentId = paymentId;
  }

  public boolean getStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public Boolean getArchive() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setArchive(Boolean archive) {
    // TODO Auto-generated method stub
  }
}