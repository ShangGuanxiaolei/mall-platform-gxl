package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;
import java.util.Date;

/** @author wangxinhua */
public class PromotionBargainHistory extends BaseEntityArchivableImpl {

  /**
   * 静态方法, 构造一个砍价历史对象
   * @param userId 帮砍用户id
   * @param detailId 发起记录id
   * @param discount 帮砍折扣
   * @return 砍价记录对象
   */
  public static PromotionBargainHistory init(String userId, String detailId, BigDecimal discount) {
    PromotionBargainHistory history = new PromotionBargainHistory();
    history.setUserId(userId);
    history.setBargainDetailId(detailId);
    history.setDiscount(discount);
    return history;
  }

  private String id;

  /** 砍价明细表id */
  private String bargainDetailId;

  /** 参与用户id */
  private String userId;

  /** 砍价金额 */
  private BigDecimal discount;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBargainDetailId() {
    return bargainDetailId;
  }

  public void setBargainDetailId(String bargainDetailId) {
    this.bargainDetailId = bargainDetailId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @JsonIgnore
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @JsonIgnore
  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @JsonIgnore
  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
