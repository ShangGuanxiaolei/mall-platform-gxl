package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * User: huangjie Date: 2018/7/2. Time: 下午8:05 后台账户与supplier的关联
 */
public class MerchantSupplier extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String merchantId;

  private String supplierId;

  private String userName;

  private String supplierName;

  private String supplierCode;

  private Boolean archive;


  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public String getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(String supplierId) {
    this.supplierId = supplierId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "MerchantSupplier{" +
        "merchantId='" + merchantId + '\'' +
        ", supplierId='" + supplierId + '\'' +
        ", userName='" + userName + '\'' +
        ", supplierName='" + supplierName + '\'' +
        ", supplierCode='" + supplierCode + '\'' +
        ", archive=" + archive +
        '}';
  }
}