package com.xquark.dal.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-7-25. DESC: 活动商品接口 积分商品暂时没有活动，不实现该接口
 */
public interface PromotionProduct {

  String getId();

  String getProductId();

  String getPromotionId();

  Date getValidFrom();

  Date getValidTo();

  Boolean getPromotionArchive();

  BigDecimal getAmount();

  boolean getIsSellOut();

}
