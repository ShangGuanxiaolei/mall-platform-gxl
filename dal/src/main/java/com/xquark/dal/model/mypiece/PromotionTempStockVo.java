package com.xquark.dal.model.mypiece;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PromotionTempStockVo {
    private long id;
    /**
     * 活动编码
     */
    private String p_code;
    /**
     * Sku编码
     */
    private String sku_code;
    /**
     * 活动Sku数量
     */
    private int p_sku_num;
    /**
     * 可用Sku数量
     */
    private int p_usable_sku_num;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date created_at;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updated_at;
    private Integer is_deleted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getP_code() {
        return p_code;
    }

    public void setP_code(String p_code) {
        this.p_code = p_code;
    }

    public String getSku_code() {
        return sku_code;
    }

    public void setSku_code(String sku_code) {
        this.sku_code = sku_code;
    }

    public int getP_sku_num() {
        return p_sku_num;
    }

    public void setP_sku_num(int p_sku_num) {
        this.p_sku_num = p_sku_num;
    }

    public int getP_usable_sku_num() {
        return p_usable_sku_num;
    }

    public void setP_usable_sku_num(int p_usable_sku_num) {
        this.p_usable_sku_num = p_usable_sku_num;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }
}
