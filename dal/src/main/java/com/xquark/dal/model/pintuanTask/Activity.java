package com.xquark.dal.model.pintuanTask;

import java.sql.Timestamp;
import java.util.Date;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.ActivityChannel;
import com.xquark.dal.type.ActivityStatus;
import com.xquark.dal.type.ActivityType;

/**
 * 活动实体类
 */
public class Activity {

    private static final long serialVersionUID = 1L;
    private String pcode;//活动编码
    private String name; // 活动名称
    private String type; // 活动类型
    private String status; // 活动状态
    private Timestamp startTime; // 活动开始时间
    private Timestamp endTime; // 活动结束时间
    private String creatorId; // 创建人
    private String auditor;  //审核人
    private String updator;  //修改人
    private Timestamp creatTime;//创建时间
    private Timestamp updateTime;//修改时间
    private int idDelete;//删除标记

    public Activity() {
    }

    public Activity(String pcode, String name, String type, String status, Timestamp startTime, Timestamp endTime, String creatorId, String auditor, String updator, Timestamp creatTime, Timestamp updateTime, int idDelete) {
        this.pcode = pcode;
        this.name = name;
        this.type = type;
        this.status = status;
        this.startTime = startTime;
        this.endTime = endTime;
        this.creatorId = creatorId;
        this.auditor = auditor;
        this.updator = updator;
        this.creatTime = creatTime;
        this.updateTime = updateTime;
        this.idDelete = idDelete;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Timestamp getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Timestamp creatTime) {
        this.creatTime = creatTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public int getIdDelete() {
        return idDelete;
    }

    public void setIdDelete(int idDelete) {
        this.idDelete = idDelete;
    }
}