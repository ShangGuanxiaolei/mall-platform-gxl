package com.xquark.dal.model.wechat;

/**
 * 自定义回复消息的所有类型
 */
public enum ConditionType {
  SUBSCRIBED, // 关注后
  REQUEST_TEXT, // 用户输入文本
  REQUEST_LOCATION, // 用户分享位置
}
