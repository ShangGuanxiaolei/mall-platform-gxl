package com.xquark.dal.model;

import java.util.Objects;

public class PromotionPgTranInfo {

  private String pieceGroupTranCode;//拼团编码
  private String groupHeadId;//团长会员id
  private String groupOpenId;//开团会员id
  private String groupSkuCode;//组团sku编码
  private String pCode;//活动编码
  private String pDetailCode;//活动详情编码
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date groupOpenTime;//组团时间
  private String shareLink;//分享短链
  private String pieceStatus;//拼团交易状态
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date createdAt;//创建时间
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date updatedAt;//修改时间
  private int isDeleted;//删除标记


  public String getPieceGroupTranCode() {
    return pieceGroupTranCode;
  }

  public void setPieceGroupTranCode(String pieceGroupTranCode) {
    this.pieceGroupTranCode = pieceGroupTranCode;
  }

  public String getGroupHeadId() {
    return groupHeadId;
  }

  public void setGroupHeadId(String groupHeadId) {
    this.groupHeadId = groupHeadId;
  }

  public String getGroupOpenId() {
    return groupOpenId;
  }

  public void setGroupOpenId(String groupOpenId) {
    this.groupOpenId = groupOpenId;
  }

  public String getGroupSkuCode() {
    return groupSkuCode;
  }

  public void setGroupSkuCode(String groupSkuCode) {
    this.groupSkuCode = groupSkuCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }
//
//  public Date getGroupOpenTime() {
//    return groupOpenTime;
//  }
//
//  public void setGroupOpenTime(Date groupOpenTime) {
//    this.groupOpenTime = groupOpenTime;
//  }

  public String getShareLink() {
    return shareLink;
  }

  public void setShareLink(String shareLink) {
    this.shareLink = shareLink;
  }

  public String getPieceStatus() {
    return pieceStatus;
  }

  public void setPieceStatus(String pieceStatus) {
    this.pieceStatus = pieceStatus;
  }
//
//  public Date getCreatedAt() {
//    return createdAt;
//  }
//
//  public void setCreatedAt(Date createdAt) {
//    this.createdAt = createdAt;
//  }
//
//  public Date getUpdatedAt() {
//    return updatedAt;
//  }
//
//  public void setUpdatedAt(Date updatedAt) {
//    this.updatedAt = updatedAt;
//  }

  public int getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(int isDeleted) {
    this.isDeleted = isDeleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PromotionPgTranInfo that = (PromotionPgTranInfo) o;
    return Objects.equals(pieceGroupTranCode, that.pieceGroupTranCode) &&
        Objects.equals(groupHeadId, that.groupHeadId) &&
        Objects.equals(groupOpenId, that.groupOpenId) &&
        Objects.equals(groupSkuCode, that.groupSkuCode) &&
        Objects.equals(pCode, that.pCode) &&
        Objects.equals(pDetailCode, that.pDetailCode) &&
        Objects.equals(shareLink, that.shareLink) &&
        Objects.equals(pieceStatus, that.pieceStatus) &&
        Objects.equals(isDeleted, that.isDeleted);
  }

  @Override
  public int hashCode() {

    return Objects
        .hash(pieceGroupTranCode, groupHeadId, groupOpenId, groupSkuCode, pCode, pDetailCode,
            shareLink, pieceStatus, isDeleted);
  }
}
