package com.xquark.dal.model;

/**
 * Created by wangxinhua on 17-7-10. DESC:
 */
public interface IExample {

  void setOrderByClause(String orderByClause);

  void setLimit(Integer limit);

  void setOffset(Integer offset);

}
