package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.RoleBelong;

/**
 * Created by chh on 16-12-13.
 */
public class Role extends BaseEntityImpl implements Archivable {

  private String code;
  private String name;
  private RoleBelong belong;
  private boolean archive;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public RoleBelong getBelong() {
    return belong;
  }

  public void setBelong(RoleBelong belong) {
    this.belong = belong;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
