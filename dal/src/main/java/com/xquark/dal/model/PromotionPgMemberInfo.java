package com.xquark.dal.model;

import java.util.Objects;

public class PromotionPgMemberInfo {
  private String pieceGroupDetailCode;//详情编码
  private String pieceGroupTranCode;//拼团编码
  private String memberId;//团员会员ID
  private String wxNickname;//微信昵称
  private String wxHeaderUrl;//微信头像
  private String wxOpenId;//微信OpenId
  private int isGroupHead;//是否团长
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date joinTime;//参加拼团时间
  private String shareUrl;//分享短链
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date createdAt;//创建时间
  //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  //private Date updatedAt;//修改时间
  private int isDeleted;//删除标记
  //是否白人
  private int isNew;
  //团支付状态
  private int status;

  public int getIsNew() {
    return isNew;
  }

  public void setIsNew(int isNew) {
    this.isNew = isNew;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getPieceGroupDetailCode() {
    return pieceGroupDetailCode;
  }

  public void setPieceGroupDetailCode(String pieceGroupDetailCode) {
    this.pieceGroupDetailCode = pieceGroupDetailCode;
  }

  public String getPieceGroupTranCode() {
    return pieceGroupTranCode;
  }

  public void setPieceGroupTranCode(String pieceGroupTranCode) {
    this.pieceGroupTranCode = pieceGroupTranCode;
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  public String getWxNickname() {
    return wxNickname;
  }

  public void setWxNickname(String wxNickname) {
    this.wxNickname = wxNickname;
  }

  public String getWxHeaderUrl() {
    return wxHeaderUrl;
  }

  public void setWxHeaderUrl(String wxHeaderUrl) {
    this.wxHeaderUrl = wxHeaderUrl;
  }

  public String getWxOpenId() {
    return wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public int getIsGroupHead() {
    return isGroupHead;
  }

  public void setIsGroupHead(int isGroupHead) {
    this.isGroupHead = isGroupHead;
  }
//
//  public Date getJoinTime() {
//    return joinTime;
//  }
//
//  public void setJoinTime(Date joinTime) {
//    this.joinTime = joinTime;
//  }

  public String getShareUrl() {
    return shareUrl;
  }

  public void setShareUrl(String shareUrl) {
    this.shareUrl = shareUrl;
  }

//  public Date getCreatedAt() {
//    return createdAt;
//  }
//
//  public void setCreatedAt(Date createdAt) {
//    this.createdAt = createdAt;
//  }
//
//  public Date getUpdatedAt() {
//    return updatedAt;
//  }
//
//  public void setUpdatedAt(Date updatedAt) {
//    this.updatedAt = updatedAt;
//  }

  public int getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(int isDeleted) {
    this.isDeleted = isDeleted;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PromotionPgMemberInfo that = (PromotionPgMemberInfo) o;
    return Objects.equals(pieceGroupDetailCode, that.pieceGroupDetailCode) &&
        Objects.equals(pieceGroupTranCode, that.pieceGroupTranCode) &&
        Objects.equals(memberId, that.memberId) &&
        Objects.equals(wxNickname, that.wxNickname) &&
        Objects.equals(wxHeaderUrl, that.wxHeaderUrl) &&
        Objects.equals(wxOpenId, that.wxOpenId) &&
        Objects.equals(isGroupHead, that.isGroupHead) &&
        Objects.equals(shareUrl, that.shareUrl) &&
        Objects.equals(isDeleted, that.isDeleted);
  }

  @Override
  public int hashCode() {

    return Objects
        .hash(pieceGroupDetailCode, pieceGroupTranCode, memberId, wxNickname, wxHeaderUrl, wxOpenId,
            isGroupHead, shareUrl, isDeleted);
  }
}
