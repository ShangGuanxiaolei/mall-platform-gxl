package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * @author
 */
public class PromotionFullCutDiscount<T extends BigDecimal> extends
    PromotionFullReduceDiscount<T> implements Comparable<PromotionFullCutDiscount<T>> {

  /**
   * 满减活动表id TODO wangxinhua 修改为promotionId、移动到父类
   */
  private String fullCutId;

  /**
   * 最低消费
   */
  private BigDecimal minConsume;

  public String getFullCutId() {
    return fullCutId;
  }

  public void setFullCutId(String fullCutId) {
    this.fullCutId = fullCutId;
  }

  public BigDecimal getMinConsume() {
    return minConsume;
  }

  public void setMinConsume(BigDecimal minConsume) {
    this.minConsume = minConsume;
  }

  @Override
  public boolean isSatisfied(T targetValue) {
    if (targetValue == null) {
      throw new IllegalArgumentException("传入参数不能为空");
    }
    if (this.minConsume == null) {
      return false;
    }
    return this.minConsume.compareTo(targetValue) <= 0;
  }

  @Override
  public int compareTo(PromotionFullCutDiscount<T> tPromotionFullCutDiscount) {
    if (tPromotionFullCutDiscount == null || tPromotionFullCutDiscount.minConsume == null) {
      return -1;
    }
    return this.minConsume.compareTo(tPromotionFullCutDiscount.minConsume);
  }
}