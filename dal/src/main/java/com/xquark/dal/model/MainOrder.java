package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.OrderSingleType;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.RefundType;

import java.math.BigDecimal;
import java.util.Date;

public class MainOrder extends BaseEntityImpl implements PaidAble {

  private static final long serialVersionUID = 1L;

  //订单类型缩写
  public static final String MAIN_ORDER_TYPE = "m";

  private OrderSingleType orderSingleType; //下单方式

  private String orderNo; // 交易订单号


  private String payNo; // 支付单号


  private OrderType type; // 交易类型


  private PaymentMode payType; // 支付类型


  private String buyerId; // 注册用户ID


  private BigDecimal totalFee; // 订单总金额 totalFee = goodsFee + logisticsFee - discountFee | totalFee = paidFee


  private BigDecimal paidFee; // 订单付款总金额


  private BigDecimal discountFee; // 订单折扣金额

  private Date paidAt;  // 付款时间


  private PayStatus paidStatus;// 付款状态


  private Date refundAt; // 退款时间


  private MainOrderStatus status;// 订单状态


  private String remark; // 备注


  private Boolean archive; // 订单是否被删除


  private RefundType refundType;


  private Date checkingAt; // 订单对账时间


  private Date cancelledAt; // 订单取消时间


  private Date succeedAt; // 订单成功时间


  private String remarkAdmin; // 备注-管理端


  private String devStatus; // 主订单状态，开发使用


  private String devLog; // 主订单append only log

  private String activityGrouponId;

//  private OrderSortType orderType;

  public String getActivityGrouponId() {
    return activityGrouponId;
  }

  public void setActivityGrouponId(String activityGrouponId) {
    this.activityGrouponId = activityGrouponId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getPayNo() {
    return payNo;
  }

  public void setPayNo(String payNo) {
    this.payNo = payNo;
  }

  public OrderType getType() {
    return type;
  }

  public void setType(OrderType type) {
    this.type = type;
  }

  public PaymentMode getPayType() {
    return payType;
  }

  public void setPayType(PaymentMode payType) {
    this.payType = payType;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public void setBuyerId(String buyerId) {
    this.buyerId = buyerId;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getPaidFee() {
    return paidFee;
  }

  public void setPaidFee(BigDecimal paidFee) {
    this.paidFee = paidFee;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public Date getPaidAt() {
    return paidAt;
  }

  public void setPaidAt(Date paidAt) {
    this.paidAt = paidAt;
  }

  public PayStatus getPaidStatus() {
    return paidStatus;
  }

  public void setPaidStatus(PayStatus paidStatus) {
    this.paidStatus = paidStatus;
  }

  public Date getRefundAt() {
    return refundAt;
  }

  public void setRefundAt(Date refundAt) {
    this.refundAt = refundAt;
  }

  public MainOrderStatus getStatus() {
    return status;
  }

  public void setStatus(MainOrderStatus status) {
    this.status = status;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public RefundType getRefundType() {
    return refundType;
  }

  public void setRefundType(RefundType refundType) {
    this.refundType = refundType;
  }

  public Date getCheckingAt() {
    return checkingAt;
  }

  public void setCheckingAt(Date checkingAt) {
    this.checkingAt = checkingAt;
  }

  public Date getCancelledAt() {
    return cancelledAt;
  }

  public void setCancelledAt(Date cancelledAt) {
    this.cancelledAt = cancelledAt;
  }

  public Date getSucceedAt() {
    return succeedAt;
  }

  public void setSucceedAt(Date succeedAt) {
    this.succeedAt = succeedAt;
  }

  public String getRemarkAdmin() {
    return remarkAdmin;
  }

  public void setRemarkAdmin(String remarkAdmin) {
    this.remarkAdmin = remarkAdmin;
  }

  public String getDevStatus() {
    return devStatus;
  }

  public void setDevStatus(String devStatus) {
    this.devStatus = devStatus;
  }

  public String getDevLog() {
    return devLog;
  }

  public void setDevLog(String devLog) {
    this.devLog = devLog;
  }

  public OrderSingleType getOrderSingleType() {
    return orderSingleType;
  }

  public void setOrderSingleType(OrderSingleType orderSingleType) {
    this.orderSingleType = orderSingleType;
  }

//  public OrderSortType getOrderType() {
//    return orderType;
//  }
//
//  public void setOrderType(OrderSortType orderType) {
//    this.orderType = orderType;
//  }
}