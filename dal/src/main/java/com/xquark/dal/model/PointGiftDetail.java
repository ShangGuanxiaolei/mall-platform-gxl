package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.math.BigDecimal;

/** @author wangxinhua */
public class PointGiftDetail extends BaseEntityImpl implements Archivable {

  private Long cpId;

  /** 业务id */
  private String bizId;

  /** 业务类型 */
  private String bizType;

  /** 操作值 */
  private BigDecimal amount;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getBizId() {
    return bizId;
  }

  public void setBizId(String bizId) {
    this.bizId = bizId;
  }

  public String getBizType() {
    return bizType;
  }

  public void setBizType(String bizType) {
    this.bizType = bizType;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
