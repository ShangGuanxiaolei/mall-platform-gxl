package com.xquark.dal.model;

import com.xquark.dal.status.LongOperation;
import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class SignInRule implements Serializable {

  private String id;

  /**
   * 签到积分初始值
   */
  private Long init;

  /**
   * 签到计算方式
   */
  private LongOperation operation;

  /**
   * 签到运算数
   */
  private Integer operationNumber;

  /**
   * 签到获得的最大积分
   */
  private Long max;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getInit() {
    return init;
  }

  public void setInit(Long init) {
    this.init = init;
  }

  public LongOperation getOperation() {
    return operation;
  }

  public void setOperation(LongOperation operation) {
    this.operation = operation;
  }

  public Integer getOperationNumber() {
    return operationNumber;
  }

  public void setOperationNumber(Integer operationNumber) {
    this.operationNumber = operationNumber;
  }

  public Long getMax() {
    return max;
  }

  public void setMax(Long max) {
    this.max = max;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}