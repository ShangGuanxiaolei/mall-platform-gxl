package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.status.BargainOrderStatus;

import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionBargainOrder extends BaseEntityArchivableImpl {

  private String id;

  private String bargainDetailId;

  private String orderId;

  private BargainOrderStatus status; // 订单状态

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBargainDetailId() {
    return bargainDetailId;
  }

  public void setBargainDetailId(String bargainDetailId) {
    this.bargainDetailId = bargainDetailId;
  }

  public String getOrderId() {
    return orderId;
  }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
