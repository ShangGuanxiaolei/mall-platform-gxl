package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleTimeline implements Serializable {
    private Integer id;

    /**
     * 大型促销活动表id
     */
    private Integer grandSaleId;

    /**
     * 活动节点名称
     */
    private String name;

    /**
     * 活动时间节点
     */
    private Date timeNode;

    /**
     * 活动节点持续时间
     */
    private String duration;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTimeNode() {
        return timeNode;
    }

    public void setTimeNode(Date timeNode) {
        this.timeNode = timeNode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    @Override
    public String toString() {
        return "GrandSaleTimeline{" +
                "grandSaleId=" + grandSaleId +
                ", name='" + name + '\'' +
                ", timeNode=" + timeNode +
                ", duration='" + duration + '\'' +
                '}';
    }
}