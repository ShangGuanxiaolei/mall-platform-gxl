package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * @author jitre
 *
 * 对评论的回复
 */
@ApiModel("回复对象")
public class CommentReply extends BaseEntityImpl implements Archivable {

  /**
   * 回复评论的id
   */
  @ApiModelProperty("被回复的评论的id")
  private String commentId;

  /**
   * 谁发起的回复
   */
  @ApiModelProperty("发起回复用户的id,无需传入,默认取当前用户的id")
  private String fromUserId;

  /**
   * 被回复的用户id
   */
  @ApiModelProperty("被回复用户的id")
  private String toUserId;

  /**
   * 回复的内容
   */
  @ApiModelProperty("回复的内容")
  private String content;


  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getCommentId() {
    return commentId;
  }

  public void setCommentId(String commentId) {
    this.commentId = commentId;
  }

  public String getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(String fromUserId) {
    this.fromUserId = fromUserId;
  }

  public String getToUserId() {
    return toUserId;
  }

  public void setToUserId(String toUserId) {
    this.toUserId = toUserId;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    CommentReply other = (CommentReply) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getCommentId() == null) ? 0 : getCommentId().hashCode());
    result = prime * result + ((getFromUserId() == null) ? 0 : getFromUserId().hashCode());
    result = prime * result + ((getToUserId() == null) ? 0 : getToUserId().hashCode());
    result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
    result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
    result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
    result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(super.getId());
    sb.append(", commentId=").append(commentId);
    sb.append(", fromUserId=").append(fromUserId);
    sb.append(", toUserId=").append(toUserId);
    sb.append(", content=").append(content);
    sb.append(", createdAt=").append(super.getCreatedAt());
    sb.append(", updatedAt=").append(super.getUpdatedAt());
    sb.append(", archive=").append(archive);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}