package com.xquark.dal.model;

/**
 * Created by chh on 17-07-13.
 */
public class StoreMemberForm extends StoreMember {

  private String code;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
