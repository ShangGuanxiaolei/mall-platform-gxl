package com.xquark.dal.model;

/**
 * @类名: PromotionNameListVO
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/2/18 16:29
 * @版本号: V1.0 .
 */
public class PromotionNameListVO {

    private String promotionName;

    private String pCode;

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }
}
