package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class HealthTestModule implements Serializable {

  // 菜单最大层级
  public static int MAX_LEVEL = 2;

  /**
   * id
   */
  private String id;

  /**
   * 父标题id
   */
  private String parentId;

  /**
   * 标题名称
   */
  private String name;

  /**
   * 静态名称
   */
  private String staticName;

  private String type;

  /**
   * 图标
   */
  private String icon;

  /**
   * 是否子节点
   */
  private Boolean isLeaf;

  /**
   * 标题描述
   */
  private String description;

  /**
   * 排序号
   */
  private Integer sortNo;

  /**
   * 题目性别限制 0 - 不限 1 - 男 2 - 女
   */
  private Integer requiredSex;

  /**
   * 是否必做
   */
  private Boolean required;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStaticName() {
    return staticName;
  }

  public void setStaticName(String staticName) {
    this.staticName = staticName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Boolean getIsLeaf() {
    return isLeaf;
  }

  public void setIsLeaf(Boolean isLeaf) {
    this.isLeaf = isLeaf;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getSortNo() {
    return sortNo;
  }

  public void setSortNo(Integer sortNo) {
    this.sortNo = sortNo;
  }

  public Integer getRequiredSex() {
    return requiredSex;
  }

  public void setRequiredSex(Integer requiredSex) {
    this.requiredSex = requiredSex;
  }

  public Boolean getRequired() {
    return required;
  }

  public void setRequired(Boolean required) {
    this.required = required;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}