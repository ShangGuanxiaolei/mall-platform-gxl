package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * customerMessage
 *
 * 中台表结构, 不继承原 {@link com.xquark.dal.BaseEntityImpl} !!!
 *
 * @author wangxinhua
 */
public class CustomerMessage implements Serializable {

  public CustomerMessage() {
  }

  public CustomerMessage(Long cpId, String msgCode, String message, Integer source) {
    this.cpId = cpId;
    this.msgCode = msgCode;
    this.message = message;
    this.source = source;
  }

  /**
   * 主键
   */
  private Long id;

  private Long cpId;

  /**
   * login
   */
  private String msgCode;

  private String message;

  /**
   * 1 hds, 2 vivilife 3 ecommerce
   */
  private Integer source;

  private Date createdDate;

  private Date updatedDate;

  /**
   * 0 unread, 1 read
   */
  private Boolean isRead;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getMsgCode() {
    return msgCode;
  }

  public void setMsgCode(String msgCode) {
    this.msgCode = msgCode;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getSource() {
    return source;
  }

  public void setSource(Integer source) {
    this.source = source;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public Boolean getIsRead() {
    return isRead;
  }

  public void setIsRead(Boolean isRead) {
    this.isRead = isRead;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    CustomerMessage other = (CustomerMessage) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getCpId() == null ? other.getCpId() == null
        : this.getCpId().equals(other.getCpId()))
        && (this.getMsgCode() == null ? other.getMsgCode() == null
        : this.getMsgCode().equals(other.getMsgCode()))
        && (this.getMessage() == null ? other.getMessage() == null
        : this.getMessage().equals(other.getMessage()))
        && (this.getSource() == null ? other.getSource() == null
        : this.getSource().equals(other.getSource()))
        && (this.getCreatedDate() == null ? other.getCreatedDate() == null
        : this.getCreatedDate().equals(other.getCreatedDate()))
        && (this.getUpdatedDate() == null ? other.getUpdatedDate() == null
        : this.getUpdatedDate().equals(other.getUpdatedDate()))
        && (this.getIsRead() == null ? other.getIsRead() == null
        : this.getIsRead().equals(other.getIsRead()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getCpId() == null) ? 0 : getCpId().hashCode());
    result = prime * result + ((getMsgCode() == null) ? 0 : getMsgCode().hashCode());
    result = prime * result + ((getMessage() == null) ? 0 : getMessage().hashCode());
    result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
    result = prime * result + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
    result = prime * result + (((getUpdatedDate()) == null) ? 0 : getUpdatedDate().hashCode());
    result = prime * result + ((getIsRead() == null) ? 0 : getIsRead().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(id);
    sb.append(", cpId=").append(cpId);
    sb.append(", msgCode=").append(msgCode);
    sb.append(", message=").append(message);
    sb.append(", source=").append(source);
    sb.append(", createdDate=").append(createdDate);
    sb.append(", updatedDate=").append(updatedDate);
    sb.append(", isRead=").append(isRead);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}