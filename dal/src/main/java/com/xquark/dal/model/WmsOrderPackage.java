package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * User: kong Date: 18-6-9. Time: 上午10:21 WMS订单包装信息表
 */
public class WmsOrderPackage extends BaseEntityImpl implements Archivable {

  //订单号
  private String orderNo;
  //推荐包装箱规格
  private String packageType;
  //箱型名称
  private String packageName;
  //推荐包装箱数量
  private Long orderingQty;
  //实际包装箱数量
  private Long warehouseQty;
  //EDI状态
  private Integer ediStatus;
  //逻辑删除
  private Boolean archive;

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getPackageType() {
    return packageType;
  }

  public void setPackageType(String packageType) {
    this.packageType = packageType;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public Long getOrderingQty() {
    return orderingQty;
  }

  public void setOrderingQty(Long orderingQty) {
    this.orderingQty = orderingQty;
  }

  public Long getWarehouseQty() {
    return warehouseQty;
  }

  public void setWarehouseQty(Long warehouseQty) {
    this.warehouseQty = warehouseQty;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "WmsOrderPackage{" +
        "orderNo='" + orderNo + '\'' +
        ", packageType='" + packageType + '\'' +
        ", packageName='" + packageName + '\'' +
        ", orderingQty=" + orderingQty +
        ", warehouseQty=" + warehouseQty +
        ", ediStatus=" + ediStatus +
        ", archive=" + archive +
        '}';
  }
}
