package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.util.Date;
import java.util.List;

/**
 * 红包雨实体类
 */
public class PacketRain extends BaseEntityArchivableImpl {

    public static PacketRain state(Integer state) {
        final PacketRain ret = new PacketRain();
        ret.setState(state);
        return ret;
    }

    /**
     * 活动状态
     */
    private Integer state;

    /**
     * 活动开始时间
     */
    private Date triggerStartTime;

    /**
     * 活动结束时间
     */
    private Date triggerEndTime;

    /**
     * 下个时间活动
     */
    private Date nextStartTime;

    private Date thisEndTime;

    /**
     * 活动id
     */
    private String promotionListId;

    /**
     * 活动日期
     */
    private Date promotionDate;

    /**
     * 白人每日上限
     */
    private Integer normalLimit;

    /**
     * 非白人每日上限
     */
    private Integer vipLimit;

    /**
     * 每个时间段的活动有效时间(单位：毫秒)
     */
    private Long effectiveTime;

    /**
     * 是否参与过红包雨活动
     */
    private boolean isJoinedPacketRain;

    /**
     * 每个时间段的活动有效时间(单位：小时)(数据库字段)
     */
    private long time;

    /**
     * 活动攻略
     */
    private String activityStrategy;

    /**
     * 活动规则
     */
    private String activityRule;

    /**
     * kv主图
     */
    private String banner;

    private List<PromotionPacketRainTime> rainTimes;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    public Date getNextStartTime() {
        return nextStartTime;
    }

    public void setNextStartTime(Date nextStartTime) {
        this.nextStartTime = nextStartTime;
    }

    public Date getThisEndTime() {
        return thisEndTime;
    }

    public void setThisEndTime(Date thisEndTime) {
        this.thisEndTime = thisEndTime;
    }

    public String getPromotionListId() {
        return promotionListId;
    }

    public void setPromotionListId(String promotionListId) {
        this.promotionListId = promotionListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public Integer getNormalLimit() {
        return normalLimit;
    }

    public void setNormalLimit(Integer normalLimit) {
        this.normalLimit = normalLimit;
    }

    public Integer getVipLimit() {
        return vipLimit;
    }

    public void setVipLimit(Integer vipLimit) {
        this.vipLimit = vipLimit;
    }


    public Long getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(Long effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public boolean isJoinedPacketRain() {
        return isJoinedPacketRain;
    }

    public void setJoinedPacketRain(boolean joinedPacketRain) {
        isJoinedPacketRain = joinedPacketRain;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getActivityStrategy() {
        return activityStrategy;
    }

    public void setActivityStrategy(String activityStrategy) {
        this.activityStrategy = activityStrategy;
    }

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public List<PromotionPacketRainTime> getRainTimes() {
        return rainTimes;
    }

    public void setRainTimes(List<PromotionPacketRainTime> rainTimes) {
        this.rainTimes = rainTimes;
    }
}
