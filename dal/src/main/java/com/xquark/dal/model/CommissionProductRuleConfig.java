package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;

import java.math.BigDecimal;

/**
 * 商品在交易中的分佣规则配置参数
 */
public class CommissionProductRuleConfig extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String commissionRuleId;

  private String productId;

  private String extConditions; // 附加条件, 不能定义与实体对象productId冲突的条件

  private int priority; // 规则优先级, 默认值是0, 为最低优先级, 如果extConditions设置了附加规则, 需要自定义优先级1-10

  private BigDecimal commissionFee; // 优先使用用户设置的fee计算佣金, 如果该值为NULL, 则使用Rate计算

  private BigDecimal commissionRate; // 分佣比例, 通常要结合规则中的上下文选择用什么价格作为被乘数计算

  private Boolean archive;

  public String getCommissionRuleId() {
    return commissionRuleId;
  }

  public void setCommissionRuleId(String commissionRuleId) {
    this.commissionRuleId = commissionRuleId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getExtConditions() {
    return extConditions;
  }

  public void setExtConditions(String extConditions) {
    this.extConditions = extConditions;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public BigDecimal getCommissionFee() {
    return commissionFee;
  }

  public void setCommissionFee(BigDecimal commissionFee) {
    this.commissionFee = commissionFee;
  }

  public BigDecimal getCommissionRate() {
    return commissionRate;
  }

  public void setCommissionRate(BigDecimal commissionRate) {
    this.commissionRate = commissionRate;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
