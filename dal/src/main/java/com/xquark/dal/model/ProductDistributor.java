package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductFeaturesKeyType;
import com.xquark.dal.type.ProductSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * 分销商商品model类
 */

public class ProductDistributor extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;
  private String shopId;
  private String productId;
  private ProductStatus status;
  private Boolean archive;

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}