package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionLottery extends BaseEntityArchivableImpl {

    /**
     * 对应的活动列表的id
     */
    private String promotionListId;

    /**
     * 活动日期
     */
    private Date promotionDate;

    /**
     * 当天抽奖次数
     */
    private Integer lotteryTimes;

    /**
     * 展示样式
     */
    private String showStyle;

    /**
     * 抽奖消耗的德分
     */
    private Integer consumptionScore;

    private Date createdAt;

    private Date updatedAt;

    /**
     * 逻辑删除 0-正常  1-已删除
     */
    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public String getPromotionListId() {
        return promotionListId;
    }

    public void setPromotionListId(String promotionListId) {
        this.promotionListId = promotionListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public Integer getLotteryTimes() {
        return lotteryTimes;
    }

    public void setLotteryTimes(Integer lotteryTimes) {
        this.lotteryTimes = lotteryTimes;
    }

    public String getShowStyle() {
        return showStyle;
    }

    public void setShowStyle(String showStyle) {
        this.showStyle = showStyle;
    }

    public Integer getConsumptionScore() {
        return consumptionScore;
    }

    public void setConsumptionScore(Integer consumptionScore) {
        this.consumptionScore = consumptionScore;
    }

}