package com.xquark.dal.model;


import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.CarouselType;
import java.util.List;

@ApiModel
public class Carousel extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;
  @ApiModelProperty(value = "标题", example = "")
  private String title;

  @ApiModelProperty(value = "描述", example = "")
  private String description;

  @ApiModelProperty(value = "类型", example = "")
  private CarouselType type;

  private String shopId;

  private Boolean archive;

  private List<CarouselImage> images;

  public List<CarouselImage> getImages() {
    return images;
  }

  public void setImages(List<CarouselImage> images) {
    this.images = images;
  }

  public CarouselType getType() {
    return type;
  }

  public void setType(CarouselType type) {
    this.type = type;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}