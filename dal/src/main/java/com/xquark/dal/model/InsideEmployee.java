package com.xquark.dal.model;

import java.io.Serializable;

/**
 * @author wangxinhua
 */
public class InsideEmployee implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 内部员工id
   */
  private Long id;

  private String englishName;

  /**
   * 员工名
   */
  private String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEnglishName() {
    return englishName;
  }

  public void setEnglishName(String englishName) {
    this.englishName = englishName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}