package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ActivityGrouponStatus;
import org.apache.commons.lang3.StringUtils;

/**
 * 团购活动表
 *
 * @author chh
 */
public class ActivityGroupon extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String grouponId;
  private String userId;
  private ActivityGrouponStatus status;
  private Boolean archive;

  public String getGrouponId() {
    return grouponId;
  }

  public void setGrouponId(String grouponId) {
    this.grouponId = grouponId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public ActivityGrouponStatus getStatus() {
    return status;
  }

  public void setStatus(ActivityGrouponStatus status) {
    this.status = status;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}