package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleStadium implements Serializable {
    private Integer id;

    /**
     * 关联大促活动id
     */
    private Integer grandSaleId;

    /**
     * 分会场标题
     */
    private String title;

    /**
     * 分会场活动开始时间
     */
    private Date beginTime;

    /**
     * 分会场活动结束时间
     */
    private Date endTime;

    /**
     * 分会场前端排序
     */
    private Integer sort;

    /**
     * 分会场按钮图片地址
     */
    private String iconUrl;

    /**
     * 分会场banner图片地址
     */
    private String bannerUrl;

    /**
     * 是否包邮，0->不包邮，1->包邮
     */
    private Boolean isFreePostage = Boolean.FALSE;

    /**
     * 是否计算收益，0->不计算，1->计算
     */
    private Boolean isCountEarning = Boolean.TRUE;

    /**
     * 分会场类型
     */
    private String type;

    /**
     * 分会场路径
     */
    private String path;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Boolean getIsFreePostage() {
        return isFreePostage;
    }

    public void setIsFreePostage(Boolean isFreePostage) {
        this.isFreePostage = isFreePostage;
    }

    public Boolean getIsCountEarning() {
        return isCountEarning;
    }

    public void setIsCountEarning(Boolean isCountEarning) {
        this.isCountEarning = isCountEarning;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}