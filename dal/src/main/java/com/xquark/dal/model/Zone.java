package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * 地区
 */
public class Zone extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name; // 名称

  private String zipCode;

  private String path;

  private String parentId; // 父Id 为 0 代表省份

  private String zoneTag;

  private Boolean archive; // 是否逻辑删除

  private String extId;// 汇购网对应的zoneid

  public String getExtId() {
    return extId;
  }

  public void setExtId(String extId) {
    this.extId = extId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getZoneTag() {
    return zoneTag;
  }

  public void setZoneTag(String zoneTag) {
    this.zoneTag = zoneTag;
  }
}
