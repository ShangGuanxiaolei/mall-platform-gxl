package com.xquark.dal.model;

import java.io.Serializable;

/**
 * xquark_order_third_info
 * @author 
 */
public class XquarkOrderThirdInfoWithBLOBs extends XquarkOrderThirdInfo implements Serializable {
    /**
     * 导入批次成功的类容
     */
    private String infoSuccess;

    /**
     * 导入批次失败的内容
     */
    private String infoLoss;

    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    private static final long serialVersionUID = 1L;

    public String getInfoSuccess() {
        return infoSuccess;
    }

    public void setInfoSuccess(String infoSuccess) {
        this.infoSuccess = infoSuccess;
    }

    public String getInfoLoss() {
        return infoLoss;
    }

    public void setInfoLoss(String infoLoss) {
        this.infoLoss = infoLoss;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkOrderThirdInfoWithBLOBs other = (XquarkOrderThirdInfoWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getImportTime() == null ? other.getImportTime() == null : this.getImportTime().equals(other.getImportTime()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getLot() == null ? other.getLot() == null : this.getLot().equals(other.getLot()))
            && (this.getResult() == null ? other.getResult() == null : this.getResult().equals(other.getResult()))
            && (this.getCreateAt() == null ? other.getCreateAt() == null : this.getCreateAt().equals(other.getCreateAt()))
            && (this.getUpdateAt() == null ? other.getUpdateAt() == null : this.getUpdateAt().equals(other.getUpdateAt()))
            && (this.getInfoSuccess() == null ? other.getInfoSuccess() == null : this.getInfoSuccess().equals(other.getInfoSuccess()))
            && (this.getInfoLoss() == null ? other.getInfoLoss() == null : this.getInfoLoss().equals(other.getInfoLoss()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getImportTime() == null) ? 0 : getImportTime().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getLot() == null) ? 0 : getLot().hashCode());
        result = prime * result + ((getResult() == null) ? 0 : getResult().hashCode());
        result = prime * result + ((getCreateAt() == null) ? 0 : getCreateAt().hashCode());
        result = prime * result + ((getUpdateAt() == null) ? 0 : getUpdateAt().hashCode());
        result = prime * result + ((getInfoSuccess() == null) ? 0 : getInfoSuccess().hashCode());
        result = prime * result + ((getInfoLoss() == null) ? 0 : getInfoLoss().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", infoSuccess=").append(infoSuccess);
        sb.append(", infoLoss=").append(infoLoss);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}