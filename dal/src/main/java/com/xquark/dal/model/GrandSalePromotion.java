package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSalePromotion implements Serializable {
    private Integer id;

    /**
     * 促销活动名称
     */
    private String name;

    /**
     * 活动开始时间
     */
    private Date beginTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 首页图片地址
     */
    private String homePageBanner;

    /**
     * 详情图片地址
     */
    private String detailsPageBanner;

    /**
     * 支付成功页图片地址
     */
    private String successPageBanner;

    /**
     * 首页活动飘窗Icon图片地址
     */
    private String homePageGiftIconBanner;

    /**
     * 活动状态：0->未开始,1->进行中,2->已结束
     */
    private Integer status;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getHomePageBanner() {
        return homePageBanner;
    }

    public void setHomePageBanner(String homePageBanner) {
        this.homePageBanner = homePageBanner;
    }

    public String getDetailsPageBanner() {
        return detailsPageBanner;
    }

    public void setDetailsPageBanner(String detailsPageBanner) {
        this.detailsPageBanner = detailsPageBanner;
    }

    public String getSuccessPageBanner() {
        return successPageBanner;
    }

    public void setSuccessPageBanner(String successPageBanner) {
        this.successPageBanner = successPageBanner;
    }

    public String getHomePageGiftIconBanner() {
        return homePageGiftIconBanner;
    }

    public void setHomePageGiftIconBanner(String homePageGiftIconBanner) {
        this.homePageGiftIconBanner = homePageGiftIconBanner;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}