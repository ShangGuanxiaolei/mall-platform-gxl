package com.xquark.dal.model;

import com.xquark.dal.type.PlatformType;
import java.io.Serializable;
import java.util.Date;

/**
 * customerCareerLevel
 *
 * @author wangxinhua
 */
public class CustomerCareerLevelAudit implements Serializable {

  private Long auditId;

  /**
   * 资格号
   */
  private Long cpId;

  /**
   * 身份类型
   */
  private String hdsType;

  /**
   * 子类型
   */
  private String hdsSubType;

  /**
   * 经销商类型
   */
  private String hdsSpType;

  /**
   * 特级服务商星级
   */
  private String hdsStar;

  /**
   * 身份类型
   */
  private String viviLifeType;

  /**
   * 子类型
   */
  private String viviLifeSubType;

  /**
   * 经销商类型
   */
  private String viviLifeSpType;

  /**
   * 0 为不点亮，1为点亮
   */
  private Boolean isLightening;

  /**
   * 点亮时间
   */
  private Date lightenDate;

  /**
   * 时间
   */
  private Date createdDate;

  /**
   * 更新时间
   */
  private Date updateDate;

  private Integer auditType;

  private PlatformType auditUser;

  private Date auditDate;

  private static final long serialVersionUID = 1L;

  public Long getAuditId() {
    return auditId;
  }

  public void setAuditId(Long auditId) {
    this.auditId = auditId;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getHdsType() {
    return hdsType;
  }

  public void setHdsType(String hdsType) {
    this.hdsType = hdsType;
  }

  public String getHdsSubType() {
    return hdsSubType;
  }

  public void setHdsSubType(String hdsSubType) {
    this.hdsSubType = hdsSubType;
  }

  public String getHdsSpType() {
    return hdsSpType;
  }

  public void setHdsSpType(String hdsSpType) {
    this.hdsSpType = hdsSpType;
  }

  public String getHdsStar() {
    return hdsStar;
  }

  public void setHdsStar(String hdsStar) {
    this.hdsStar = hdsStar;
  }

  public String getViviLifeType() {
    return viviLifeType;
  }

  public void setViviLifeType(String viviLifeType) {
    this.viviLifeType = viviLifeType;
  }

  public String getViviLifeSubType() {
    return viviLifeSubType;
  }

  public void setViviLifeSubType(String viviLifeSubType) {
    this.viviLifeSubType = viviLifeSubType;
  }

  public String getViviLifeSpType() {
    return viviLifeSpType;
  }

  public void setViviLifeSpType(String viviLifeSpType) {
    this.viviLifeSpType = viviLifeSpType;
  }

  public Boolean getIsLightening() {
    return isLightening;
  }

  public void setIsLightening(Boolean isLightening) {
    this.isLightening = isLightening;
  }

  public Date getLightenDate() {
    return lightenDate;
  }

  public void setLightenDate(Date lightenDate) {
    this.lightenDate = lightenDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public Integer getAuditType() {
    return auditType;
  }

  public void setAuditType(Integer auditType) {
    this.auditType = auditType;
  }

  public PlatformType getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(PlatformType auditUser) {
    this.auditUser = auditUser;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }
}