package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.ActionType;

import java.util.Date;

/**
 * @author luqig
 * @since 2019-05-02
 */
public class ActivityPushDetail extends BaseEntityImpl implements Archivable {

  /**
   * 主键
   */
  private String id;

  /**
   * 动作类型
   */
  private ActionType actionType;

  /**
   * 推送日期
   */
  private Date time;

  /**
   * 推送日期对应的文案
   */
  private String content;

  private Boolean archive;

  public ActivityPushDetail() {
  }

  public ActivityPushDetail(ActionType actionType, Date time, String content) {
    this.actionType = actionType;
    this.time = time;
    this.content = content;
  }

  public ActionType getActionType() {
    return actionType;
  }

  public void setActionType(ActionType actionType) {
    this.actionType = actionType;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
