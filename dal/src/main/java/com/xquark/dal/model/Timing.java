package com.xquark.dal.model;

public class Timing {
    private String pType;
    private String pCode;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpType() {

        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }
}
