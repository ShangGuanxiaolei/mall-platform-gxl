package com.xquark.dal.model;

import com.xquark.dal.type.PromotionType;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleModule implements Serializable {
    private Integer id;

    /**
     * 大型促销活动表id
     */
    private Integer grandSaleId;

    /**
     * 关联的活动id
     */
    private String promotionId;

    /**
     * 关联的活动类型
     */
    private PromotionType promotionType;

    /**
     * 前端模块排序
     */
    private Integer sort;

    /**
     * 活动模块状态:0->,未开始,1->活动中,2->自动结束,3->人工下线
     */
    private Integer status;

    /**
     * 请求接口路径
     */
    private String targetUrl;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}