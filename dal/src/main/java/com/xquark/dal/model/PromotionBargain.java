package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.type.PromotionType;

import java.util.Date;

/** @author hongwentao */
public class PromotionBargain extends BaseEntityArchivableImpl implements PromotionAble {

  private String id;

  /**
   * 商品id
   */
  private String productId;

  /** 活动开始时间 */
  private Date startTime;

  /** 活动结束时间 */
  private Date endTime;

  /** 活动名称 */
  private String promotionName;

  /** 活动状态 0-未开始 1-已生效 2-已过期 3-人工下线 */
  private Integer state;

  /** 是否有限制，0无限制，1只限新人 */
  private Byte supportLimit;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public Byte getSupportLimit() {
    return supportLimit;
  }

  public void setSupportLimit(Byte supportLimit) {
    this.supportLimit = supportLimit;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String getTitle() {
      return promotionName;
  }

  @Override
  public Date getValidFrom() {
      return getStartTime();
  }

  @Override
  public Date getValidTo() {
      return getEndTime();
  }

  @Override
  public PromotionType getPromotionType() {
      return PromotionType.BARGAIN;
  }
}
