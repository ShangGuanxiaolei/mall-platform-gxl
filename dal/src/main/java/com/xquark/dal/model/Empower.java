package com.xquark.dal.model;

public class Empower {

    private Long cpid;
    private String memberName;
    private String identityType;
    private String identityName;
    private String shopName;
    private String qrCode;
    private String empowerText;
    private int type;
    private String wechatShareTitle; // 微信分享标题

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    private long shopId;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEmpowerText() {
        return empowerText;
    }

    public void setEmpowerText(String empowerText) {
        this.empowerText = empowerText;
    }

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(Long cpid) {
        this.cpid = cpid;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityName() {
        return identityName;
    }

    public void setIdentityName(String identityName) {
        this.identityName = identityName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getWechatShareTitle() {
        return wechatShareTitle;
    }

    public void setWechatShareTitle(String wechatShareTitle) {
        this.wechatShareTitle = wechatShareTitle;
    }

    @Override
    public String toString() {
        return "Empower{" +
                "cpid=" + cpid +
                ", memberName='" + memberName + '\'' +
                ", identityType='" + identityType + '\'' +
                ", identityName='" + identityName + '\'' +
                ", shopName='" + shopName + '\'' +
                ", qrCode='" + qrCode + '\'' +
                ", empowerText='" + empowerText + '\'' +
                ", type=" + type +
                ", wechatShareTitle='" + wechatShareTitle + '\'' +
                ", shopId=" + shopId +
                '}';
    }
}
