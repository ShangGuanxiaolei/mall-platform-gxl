package com.xquark.dal.model;

import java.util.Date;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class PromotionConfig extends BaseEntityImpl implements Archivable {
	private String configName;
	private String configValue;
	private String configType;
	private Date createdAt;
	
	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	@Override
	public Boolean getArchive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArchive(Boolean archive) {
		// TODO Auto-generated method stub
		
	}

}
