package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介: Module菜单映射类
 */
public class Module extends BaseEntityImpl implements Archivable {

  private String id;
  private String parentId;
  private String name;
  private String url;
  private String iconName;
  private Boolean isLeaf;
  private Boolean isAutoExpand;
  private Integer sortNo;
  private Date createdAt;
  private Boolean archive;
  private Date updatedAt;
  private String module;
  private String page;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getIconName() {
    return iconName;
  }

  public void setIconName(String iconName) {
    this.iconName = iconName;
  }

  public Boolean getIs_Leaf() {
    return isLeaf;
  }

  public void setIs_Leaf(Boolean leaf) {
    isLeaf = leaf;
  }

  public Boolean getIs_AutoExpand() {
    return isAutoExpand;
  }

  public void setIs_AutoExpand(Boolean autoExpend) {
    isAutoExpand = autoExpend;
  }

  public Integer getSortNo() {
    return sortNo;
  }

  public void setSortNo(Integer sortNo) {
    this.sortNo = sortNo;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getModule() {
    return module;
  }

  public void setModule(String module) {
    this.module = module;
  }

  public String getPage() {
    return page;
  }

  public void setPage(String page) {
    this.page = page;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Module module = (Module) o;

    return getId() != null ? getId().equals(module.getId()) : module.getId() == null;
  }

  @Override
  public int hashCode() {
    return getId() != null ? getId().hashCode() : 0;
  }
}
