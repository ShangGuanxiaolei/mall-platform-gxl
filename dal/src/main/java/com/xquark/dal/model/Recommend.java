package com.xquark.dal.model;

import java.util.Date;

/**
 * 商品推荐
 */
public class Recommend {

	private long id ;

	private String name;

	private String code;

	private int nummber;

	private Date createdAt;

	private Date updatedAt;

	private boolean archive;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNummber() {
		return nummber;
	}

	public void setNummber(int nummber) {
		this.nummber = nummber;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}
}
