package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.math.BigDecimal;
import java.util.Date;

/**
 * User: kong Date: 18-6-9. Time: 上午9:52 WMS订单表
 */
public class WmsOrder extends BaseEntityImpl implements Archivable {

  //订单编号
  private String orderNo;
  //订单类型
  private String type;
  //订单活动类型
  private String orderType;
  //仓库编号
  private String warehouseCode;
  //订单创建时间
  private Date orderTime;
  //订货人
  private String name;
  //订货人卡号
  private String buyerId;
  //收货人
  private String consignee;
  //收货人手机号
  private String phone;
  //支付方式
  private String payType;
  //总计金额
  private BigDecimal totalFee;
  //扣除
  private BigDecimal discountFee;
  //应付金额
  private BigDecimal paidFee;
  //积分
  private BigDecimal consumptionPoints;
  //德分
  private BigDecimal paidPoint;
  //实算运费
  private BigDecimal logisticsFee;
  //运费折扣
  private BigDecimal freightDiscount;
  //订单重量
  private BigDecimal weight;
  //订单体积
  private BigDecimal volume;
  //  //收货地址-省
//  private String province;
//  //收货地址-市
//  private String city;
//  //收货地址-区
//  private String district;
  //收货地址
  private String address;
  //邮编
  private String zipcode;
  //发货状态
  private Long status;
  //发货说明
  private String remark;
  //WMSEDI状态
  private Integer wmsEdiStatus;
  //LOCALEDI状态
  private Integer localEdiStatus;
  //逻辑删除
  private Boolean archive;
  //最近一次更新时间
  private Date recentlyUpdatedAt;

  //端口号(包裹号)
  private String logisticsOrderNo;

  //商城号(主订单号)
  private String mainOrderId;

  //配送费减免
  private BigDecimal logisticDiscount;

  //立减
  private BigDecimal reduction;

  //其他调整
  private BigDecimal otherAdjustments;

  //地区id
  private String zoneId;

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getWarehouseCode() {
    return warehouseCode;
  }

  public void setWarehouseCode(String warehouseCode) {
    this.warehouseCode = warehouseCode;
  }

  public Date getOrderTime() {
    return orderTime;
  }

  public void setOrderTime(Date orderTime) {
    this.orderTime = orderTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public void setBuyerId(String buyerId) {
    this.buyerId = buyerId;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getPayType() {
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public BigDecimal getPaidFee() {
    return paidFee;
  }

  public void setPaidFee(BigDecimal paidFee) {
    this.paidFee = paidFee;
  }

  public BigDecimal getConsumptionPoints() {
    return consumptionPoints;
  }

  public void setConsumptionPoints(BigDecimal consumptionPoints) {
    this.consumptionPoints = consumptionPoints;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public BigDecimal getFreightDiscount() {
    return freightDiscount;
  }

  public void setFreightDiscount(BigDecimal freightDiscount) {
    this.freightDiscount = freightDiscount;
  }

  public BigDecimal getWeight() {
    return weight;
  }

  public void setWeight(BigDecimal weight) {
    this.weight = weight;
  }

  public BigDecimal getVolume() {
    return volume;
  }

  public void setVolume(BigDecimal volume) {
    this.volume = volume;
  }

//  public String getProvince() {
//    return province;
//  }
//
//  public void setProvince(String province) {
//    this.province = province;
//  }
//
//  public String getCity() {
//    return city;
//  }
//
//  public void setCity(String city) {
//    this.city = city;
//  }
//
//  public String getDistrict() {
//    return district;
//  }
//
//  public void setDistrict(String district) {
//    this.district = district;
//  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getWmsEdiStatus() {
    return wmsEdiStatus;
  }

  public void setWmsEdiStatus(Integer wmsEdiStatus) {
    this.wmsEdiStatus = wmsEdiStatus;
  }

  public Integer getLocalEdiStatus() {
    return localEdiStatus;
  }

  public void setLocalEdiStatus(Integer localEdiStatus) {
    this.localEdiStatus = localEdiStatus;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public Date getRecentlyUpdateAt() {
    return recentlyUpdatedAt;
  }

  public void setRecentlyUpdateAt(Date recentlyUpdateAt) {
    this.recentlyUpdatedAt = recentlyUpdateAt;
  }

  public BigDecimal getPaidPoint() {
    return paidPoint;
  }

  public void setPaidPoint(BigDecimal paidPoint) {
    this.paidPoint = paidPoint;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getMainOrderId() {
    return mainOrderId;
  }

  public void setMainOrderId(String mainOrderId) {
    this.mainOrderId = mainOrderId;
  }

  public BigDecimal getLogisticDiscount() {
    return logisticDiscount;
  }

  public void setLogisticDiscount(BigDecimal logisticDiscount) {
    this.logisticDiscount = logisticDiscount;
  }

  public BigDecimal getReduction() {
    return reduction;
  }

  public void setReduction(BigDecimal reduction) {
    this.reduction = reduction;
  }

  public BigDecimal getOtherAdjustments() {
    return otherAdjustments;
  }

  public void setOtherAdjustments(BigDecimal otherAdjustments) {
    this.otherAdjustments = otherAdjustments;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  @Override
  public String toString() {
    return "WmsOrder{" +
        "orderNo='" + orderNo + '\'' +
        ", type='" + type + '\'' +
        ", warehouseCode='" + warehouseCode + '\'' +
        ", orderTime=" + orderTime +
        ", name='" + name + '\'' +
        ", buyerId='" + buyerId + '\'' +
        ", consignee='" + consignee + '\'' +
        ", phone='" + phone + '\'' +
        ", payType='" + payType + '\'' +
        ", totalFee=" + totalFee +
        ", discountFee=" + discountFee +
        ", paidFee=" + paidFee +
        ", consumptionPoints=" + consumptionPoints +
        ", logisticsFee=" + logisticsFee +
        ", freightDiscount=" + freightDiscount +
        ", weight=" + weight +
        ", volume=" + volume +
//        ", province='" + province + '\'' +
//        ", city='" + city + '\'' +
//        ", district='" + district + '\'' +
        ", address='" + address + '\'' +
        ", zipcode='" + zipcode + '\'' +
        ", status=" + status +
        ", remark='" + remark + '\'' +
        ", wmsEdiStatus=" + wmsEdiStatus +
        ", localEdiStatus=" + localEdiStatus +
        ", archive=" + archive +
        ", recently=" + recentlyUpdatedAt +
        '}';
  }
}
