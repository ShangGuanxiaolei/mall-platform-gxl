package com.xquark.dal.model;

import com.xquark.dal.validation.group.bill.AddedTaxGroup;
import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午4:02
 * 公司发票
 */
public class CompanyBill extends BaseBill{

    /**
     * 公司名称
     */
    @NotBlank(groups = {NormalGroup.class, ElectronicGroup.class})
    private String name;

    /**
     * 商品明细
     */
    @Length(max = 128, groups = {NormalGroup.class, ElectronicGroup.class})
    private String productDetail="";

    /**
     * 纳税人识别号
     */
    @NotBlank(groups = {NormalGroup.class, ElectronicGroup.class})
    private String identifyNumber;

    /**
     * 接受手机号
     */
    @NotBlank(groups = {ElectronicGroup.class})
    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$" ,groups = {ElectronicGroup.class})
    private String receivePhone;

    /**
     * 接受邮箱
     */
    @Email(groups = {ElectronicGroup.class})
    @NotBlank(groups = {ElectronicGroup.class,AddedTaxGroup.class})
    private String receiveMail;

    public String getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }


  public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifyNumber() {
        return identifyNumber;
    }

    public void setIdentifyNumber(String identifyNumber) {
        this.identifyNumber = identifyNumber;
    }

    public String getReceivePhone() {
        return receivePhone;
    }

    public void setReceivePhone(String receivePhone) {
        this.receivePhone = receivePhone;
    }

    public String getReceiveMail() {
        return receiveMail;
    }

    public void setReceiveMail(String receiveMail) {
        this.receiveMail = receiveMail;
    }

    @Override
    public String toString() {
        return super.toString()+"CompanyBill{" +
                "name='" + name + '\'' +
                ", productDetail='" + productDetail + '\'' +
                ", identifyNumber='" + identifyNumber + '\'' +
                ", receivePhone='" + receivePhone + '\'' +
                ", receiveMail='" + receiveMail + '\'' +
                '}';
    }
}
