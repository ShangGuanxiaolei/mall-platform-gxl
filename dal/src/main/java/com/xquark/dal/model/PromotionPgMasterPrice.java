package com.xquark.dal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author gxl
 */
public class PromotionPgMasterPrice implements Serializable {
    private Integer id;

    /**
     * 拼团活动详情编码
     */
    private String pDetailCode;

    /**
     * 拼主价,设置类型为统一设置时使用
     */
    private BigDecimal memberPrice = BigDecimal.ZERO;

    /**
     * 拼主价次数
     */
    private Integer frequency = 0;

    /**
     * 纯白人
     */
    private String pureWhite;

    /**
     * 纯白人拼主价
     */
    private BigDecimal pureMemberPrice;

    /**
     * 纯白人拼主价次数
     */
    private Integer pureFrequency = 0;

    /**
     * 白人
     */
    private String white;

    /**
     * 白人拼主价
     */
    private BigDecimal whiteMemberPrice;

    /**
     * 白人拼主价次数
     */
    private Integer whiteFrequency = 0;

    /**
     * VIP
     */
    private String vip;

    /**
     * VIP拼主价
     */
    private BigDecimal vipMemberPrice;

    /**
     * VIP拼主价次数
     */
    private Integer vipFrequency = 0;

    /**
     * 店主
     */
    private String merchant;

    /**
     * 店主拼主价
     */
    private BigDecimal merchantMemberPrice;

    /**
     * 店主拼主价次数
     */
    private Integer merchantFrequency = 0;

    private Boolean archive;

    private Date createdAt;

    private Date updatedAt;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public String getPureWhite() {
        return pureWhite;
    }

    public void setPureWhite(String pureWhite) {
        this.pureWhite = pureWhite;
    }

    public BigDecimal getPureMemberPrice() {
        return pureMemberPrice;
    }

    public void setPureMemberPrice(BigDecimal pureMemberPrice) {
        this.pureMemberPrice = pureMemberPrice;
    }

    public Integer getPureFrequency() {
        return pureFrequency;
    }

    public void setPureFrequency(Integer pureFrequency) {
        this.pureFrequency = pureFrequency;
    }

    public String getWhite() {
        return white;
    }

    public void setWhite(String white) {
        this.white = white;
    }

    public BigDecimal getWhiteMemberPrice() {
        return whiteMemberPrice;
    }

    public void setWhiteMemberPrice(BigDecimal whiteMemberPrice) {
        this.whiteMemberPrice = whiteMemberPrice;
    }

    public Integer getWhiteFrequency() {
        return whiteFrequency;
    }

    public void setWhiteFrequency(Integer whiteFrequency) {
        this.whiteFrequency = whiteFrequency;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public BigDecimal getVipMemberPrice() {
        return vipMemberPrice;
    }

    public void setVipMemberPrice(BigDecimal vipMemberPrice) {
        this.vipMemberPrice = vipMemberPrice;
    }

    public Integer getVipFrequency() {
        return vipFrequency;
    }

    public void setVipFrequency(Integer vipFrequency) {
        this.vipFrequency = vipFrequency;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public BigDecimal getMerchantMemberPrice() {
        return merchantMemberPrice;
    }

    public void setMerchantMemberPrice(BigDecimal merchantMemberPrice) {
        this.merchantMemberPrice = merchantMemberPrice;
    }

    public Integer getMerchantFrequency() {
        return merchantFrequency;
    }

    public void setMerchantFrequency(Integer merchantFrequency) {
        this.merchantFrequency = merchantFrequency;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "PromotionPgMasterPrice{" +
                "id=" + id +
                ", pDetailCode='" + pDetailCode + '\'' +
                ", memberPrice=" + memberPrice +
                ", frequency=" + frequency +
                ", pureWhite='" + pureWhite + '\'' +
                ", pureMemberPrice=" + pureMemberPrice +
                ", pureFrequency=" + pureFrequency +
                ", white='" + white + '\'' +
                ", whiteMemberPrice=" + whiteMemberPrice +
                ", whiteFrequency=" + whiteFrequency +
                ", vip='" + vip + '\'' +
                ", vipMemberPrice=" + vipMemberPrice +
                ", vipFrequency=" + vipFrequency +
                ", merchant='" + merchant + '\'' +
                ", merchantMemberPrice=" + merchantMemberPrice +
                ", merchantFrequency=" + merchantFrequency +
                ", archive=" + archive +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}