package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class InsideUserEmployee implements Serializable {

  /**
   * id
   */
  private String id;

  /**
   * 用户id
   */
  private String userId;

  public InsideUserEmployee() {
  }

  public InsideUserEmployee(String userId, Long employeeId) {
    this.userId = userId;
    this.employeeId = employeeId;
  }

  /**
   * 员工id
   */
  private Long employeeId;

  private Date createdAt;

  private Date updatedAt;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Long employeeId) {
    this.employeeId = employeeId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }
}