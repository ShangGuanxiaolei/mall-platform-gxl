package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author
 */
public class PromotionFullCut extends BaseEntityImpl implements Archivable {

    public PromotionFullCut() {
    }

    public PromotionFullCut(String promotionId, String productId, BigDecimal discount) {
        this.promotionId = promotionId;
        this.productId = productId;
        this.discount = discount;
    }

    public PromotionFullCut(String productId, BigDecimal discount) {
        this.productId = productId;
        this.discount = discount;
    }

    private String id;

    /**
     * 活动id
     */
    private String promotionId;

    /**
     * 商品id
     */
    private String productId;

    private BigDecimal discount;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    /**
     * 小数形式的折扣
     */
    public BigDecimal getRealDiscount() {
        if (discount != null) {
            return discount;
        }
        return BigDecimal.ZERO;
    }
}