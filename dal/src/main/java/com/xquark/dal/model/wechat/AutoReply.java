package com.xquark.dal.model.wechat;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * 自定义自动回复消息
 */
public class AutoReply extends BaseEntityImpl implements Archivable {

  /**
   * 自定义回复消息名称
   */
  private String name;

  /**
   * 自定义回复消息条件类型
   */
  private ConditionType conditionType;

  /**
   * 自定义回复消息的输入匹配类型
   */
  private MatchType matchType;

  /**
   * 自定义回复消息的条件
   */
  private String condition;

  /**
   * 回复内容的类型
   */
  private ReplyType replyType;

  /**
   * 回复内容
   */
  private String replyContent;

  /**
   * 此自定义回复规则所属的店铺
   */
  private String shopId;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ConditionType getConditionType() {
    return conditionType;
  }

  public void setConditionType(ConditionType conditionType) {
    this.conditionType = conditionType;
  }

  public String getCondition() {
    return condition;
  }

  public void setCondition(String condition) {
    this.condition = condition;
  }

  public ReplyType getReplyType() {
    return replyType;
  }

  public void setReplyType(ReplyType replyType) {
    this.replyType = replyType;
  }

  public String getReplyContent() {
    return replyContent;
  }

  public void setReplyContent(String replyContent) {
    this.replyContent = replyContent;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  /**
   * 此自定义回复规则所属的微信公众号
   */
  private String wechatAppId;

  private Boolean archive;

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getWechatAppId() {
    return wechatAppId;
  }

  public void setWechatAppId(String wechatAppId) {
    this.wechatAppId = wechatAppId;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public void setMatchType(MatchType matchType) {
    this.matchType = matchType;
  }
}
