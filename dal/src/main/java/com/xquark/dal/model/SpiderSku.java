package com.xquark.dal.model;

public class SpiderSku {

  private Long id;
  private Long itemId;
  private Double price;
  private Integer amount;
  private String spec;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public String getOrigSpec() {
    return origSpec;
  }

  public void setOrigSpec(String origSpec) {
    this.origSpec = origSpec;
  }

  private String origSpec;
}
