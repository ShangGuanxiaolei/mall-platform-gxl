package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.TargetType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by chh on 16-10-17.
 */
public class MarketingTweet extends BaseEntityImpl implements Archivable {

  private String title;
  private String description;
  private String bloggerIcon;
  private String bloggerName;
  private String target;
  private TargetType targetType;
  private Date tweetAt;
  private boolean archive;

  private String targetName;

  public String getTargetName() {
    return targetName;
  }

  public void setTargetName(String targetName) {
    this.targetName = targetName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getBloggerIcon() {
    return bloggerIcon;
  }

  public void setBloggerIcon(String bloggerIcon) {
    this.bloggerIcon = bloggerIcon;
  }

  public String getBloggerName() {
    return bloggerName;
  }

  public void setBloggerName(String bloggerName) {
    this.bloggerName = bloggerName;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public TargetType getTargetType() {
    return targetType;
  }

  public void setTargetType(TargetType targetType) {
    this.targetType = targetType;
  }

  public Date getTweetAt() {
    return tweetAt;
  }

  public void setTweetAt(Date tweetAt) {
    this.tweetAt = tweetAt;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
