package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PromotionType;

/**
 * @author sgxl
 */
public class PromotionTitle extends BaseEntityImpl {

  private static final long serialVersionUID = 4485622304685973150L;

  /**
   * 活动标题
   */
  private String promotionTitle;

  /**
   * 活动类型（团购，限时抢购等）
   */
  private PromotionType promotionType;

  public String getPromotionTitle() {
    return promotionTitle;
  }

  public void setPromotionTitle(String promotionTitle) {
    this.promotionTitle = promotionTitle;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  public void setPromotionType(PromotionType promotionType) {
    this.promotionType = promotionType;
  }

  private PromotionTitle(){}

  private static class PromotionTitleInstance {
    private static final PromotionTitle INSTANCE = new PromotionTitle();
  }

  public static PromotionTitle getInstance(){
    return PromotionTitleInstance.INSTANCE;
  }

}
