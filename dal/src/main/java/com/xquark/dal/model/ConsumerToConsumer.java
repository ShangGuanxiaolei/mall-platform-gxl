package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class ConsumerToConsumer implements Serializable {

    public ConsumerToConsumer() {
    }

    public ConsumerToConsumer(Long consumerCpId, Long fromConsumerCpId, Long customerCpId) {
        this.consumerCpId = consumerCpId;
        this.fromConsumerCpId = fromConsumerCpId;
        this.customerCpId = customerCpId;
    }

    /**
     * id
     */
    private Long id;

    /**
     * 购买白人ID
     */
    private Long consumerCpId;

    /**
     * 分享白人ID
     */
    private Long fromConsumerCpId;

    /**
     * 找到白人上级有身份ID、若无为公司
     */
    private Long customerCpId;

    /**
     * 创建时间
     */
    private Date createdDate;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsumerCpId() {
        return consumerCpId;
    }

    public void setConsumerCpId(Long consumerCpId) {
        this.consumerCpId = consumerCpId;
    }

    public Long getFromConsumerCpId() {
        return fromConsumerCpId;
    }

    public void setFromConsumerCpId(Long fromConsumerCpId) {
        this.fromConsumerCpId = fromConsumerCpId;
    }

    public Long getCustomerCpId() {
        return customerCpId;
    }

    public void setCustomerCpId(Long customerCpId) {
        this.customerCpId = customerCpId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}