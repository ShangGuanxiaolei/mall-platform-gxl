package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.IUser;
import com.xquark.dal.status.UserStatus;
import com.xquark.utils.UniqueNoUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public class Merchant extends BaseEntityImpl implements IUser {

  private static final SimpleGrantedAuthority role_auth_admin = new SimpleGrantedAuthority(
      "ROLE_ADMIN");

  private String loginname;
  private String password;
  private String name;
  private String phone;
  private String email;
  private String shopId;
  private Boolean archive;
  private String avatar;
  private Date createAt;
  private Date updateAt;
  private Boolean isAdmin;
  private String sex;
  private String roles;
  private Long count;
  private Date lastLoginAt;
  private Date loginAt;
  private String shopRight;
  private String permissions;
  private String createrId;
  private String wechat;
  private String adminId;
  private String userId;
  private String partner;
  private UserStatus userStatus;

  private String rolesDesc;
  private String orgId;


  private List<String> roleList;

  public void setRoleList(List<String> roleList) {
    this.roleList = roleList;
  }

  public String getRolesDesc() {
    return rolesDesc;
  }

  public void setRolesDesc(String rolesDesc) {
    this.rolesDesc = rolesDesc;
  }

  public UserStatus getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public String getLoginname() {
    return loginname;
  }

  public void setLoginname(String loginname) {
    this.loginname = loginname;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getAdminId() {
    return adminId;
  }

  public void setAdminId(String adminId) {
    this.adminId = adminId;
  }

  public String getCreaterId() {
    return createrId;
  }

  public void setCreaterId(String createrId) {
    this.createrId = createrId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  public Boolean getAdmin() {
    return isAdmin;
  }

  public void setAdmin(Boolean admin) {
    isAdmin = admin;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getRoles() {
    return roles;
  }

  public void setRoles(String roles) {
    this.roles = roles;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public Date getLastLoginAt() {
    return lastLoginAt;
  }

  public void setLastLoginAt(Date lastLoginAt) {
    this.lastLoginAt = lastLoginAt;
  }

  public Date getLoginAt() {
    return loginAt;
  }

  public void setLoginAt(Date loginAt) {
    this.loginAt = loginAt;
  }

  public String getShopRight() {
    return shopRight;
  }

  public void setShopRight(String shopRight) {
    this.shopRight = shopRight;
  }

  public String getPermissions() {
    return permissions;
  }

  public void setPermissions(String permissions) {
    this.permissions = permissions;
  }

  public String getOrgId() {
    return orgId;
  }

  public void setOrgId(String orgId) {
    this.orgId = orgId;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public String getUsername() {
    return this.getLoginname();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  public boolean hasRole(String role) {
    return roles != null && roles.indexOf(role) != -1;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {

    Collection<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
//        for (String role:getRoles().split(",")) {
//            //权限如果前缀是ROLE_，security就会认为这是个角色信息，而不是权限，例如ROLE_MENBER就是MENBER角色，CAN_SEND就是CAN_SEND权限
//            auths.add(new SimpleGrantedAuthority("ROLE_"+role));
//        }
//
//        if (auths.size() == 0)
//            auths.add(role_auth_admin);
    // TODO 兼容原来的ROLES
    if (roleList != null && roleList.size() > 0) {
      for (String role : roleList) {
        auths.add(new SimpleGrantedAuthority(role));
      }
    } else if (roles != null) {
      for (String role : roles.split(",")) {
        auths.add(new SimpleGrantedAuthority("ROLE_" + role));
      }
    }
    return auths;
  }

  public boolean isAnonymous() {
    return this.loginname.startsWith(UniqueNoUtils.UniqueNoType.CID.name());
  }
}
