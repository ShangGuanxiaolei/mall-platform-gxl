package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CombinationStatus;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

//组合实体类
public class Combination extends BaseEntityImpl implements Archivable {

  //版本号
  private static final long serialVersionUID = 1L;
  //组合名称
  @NotBlank
  private String name;
  //组合状态
  @NotNull
  private CombinationStatus status;
  //逻辑删除
  private Boolean archive;

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getName() {
    return name;
  }

  public CombinationStatus getStatus() {
    return status;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStatus(CombinationStatus status) {
    this.status = status;
  }


  @Override
  public String toString() {
    return super.toString()+"Combination{" +
        "name='" + name + '\'' +
        ", status='" + status + '\'' +
        ", archive=" + archive +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Combination that = (Combination) o;
    return Objects.equals(name, that.name) &&
        Objects.equals(status, that.status) &&
        Objects.equals(archive, that.archive);
  }

  @Override
  public int hashCode() {

    return Objects.hash(name, status, archive);
  }
}
