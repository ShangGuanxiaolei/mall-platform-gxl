package com.xquark.dal.model;

import java.util.Date;

/**
 * 店铺的样式
 *
 * @author xuebowen
 */
public class ShopStyle {

  private String shopId; // 店铺id
  private String backgroundColor; // 店铺背景颜色
  private String fontFamily; // 店铺名称字体
  private String fontColor; // 店铺名称颜色
  private String avatarStyle; // 店铺头像样式：方形/圆形
  private String listView; // 商品列表视图ProductListView
  private Date createdAt;
  private Date updatedAt;

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getBackgroundColor() {
    return backgroundColor;
  }

  public void setBackgroundColor(String backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  public String getFontFamily() {
    return fontFamily;
  }

  public void setFontFamily(String fontFamily) {
    this.fontFamily = fontFamily;
  }

  public String getFontColor() {
    return fontColor;
  }

  public void setFontColor(String fontColor) {
    this.fontColor = fontColor;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getAvatarStyle() {
    return avatarStyle;
  }

  public void setAvatarStyle(String avatarStyle) {
    this.avatarStyle = avatarStyle;
  }

  public String getListView() {
    return listView;
  }

  public void setListView(String listView) {
    this.listView = listView;
  }

}
