package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.status.WinningRank;
import com.xquark.dal.status.WinningStatus;

import java.util.Date;

public class WinningList extends BaseEntityArchivableImpl {

	private Date winningTime;

	private Long winningCpId;

	private String addressId;

	private int winningRank;

	private String bizId;

	private String winningIdentity;

	private int state;

	public Date getWinningTime() {
		return winningTime;
	}

	public void setWinningTime(Date winningTime) {
		this.winningTime = winningTime;
	}

	public Long getWinningCpId() {
		return winningCpId;
	}

	public void setWinningCpId(Long winningCpId) {
		this.winningCpId = winningCpId;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public int getWinningRank() {
		return winningRank;
	}

	public void setWinningRank(int winningRank) {
		this.winningRank = winningRank;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getWinningIdentity() {
		return winningIdentity;
	}

	public void setWinningIdentity(String winningIdentity) {
		this.winningIdentity = winningIdentity;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
