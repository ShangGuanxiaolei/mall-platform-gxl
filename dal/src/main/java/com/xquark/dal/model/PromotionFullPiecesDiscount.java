package com.xquark.dal.model;

/**
 * @author
 */
public class PromotionFullPiecesDiscount<T extends Integer> extends
    PromotionFullReduceDiscount<T> implements Comparable<PromotionFullPiecesDiscount<T>> {

  /**
   * 满减活动表id
   */
  private String promotionId;

  private Integer minAmount;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Integer getMinAmount() {
    return minAmount;
  }

  public void setMinAmount(Integer minAmount) {
    this.minAmount = minAmount;
  }

  /**
   * 比较件数是否满足条件
   *
   * @param targetValue 所需件数
   * @return 是否满足
   * @throws IllegalArgumentException 如果传入参数为空
   */
  @Override
  public boolean isSatisfied(T targetValue) {
    if (targetValue == null) {
      throw new IllegalArgumentException("传入参数不能为空");
    }
    if (this.minAmount == null) {
      return false;
    }
    return this.minAmount.compareTo(targetValue) <= 0;
  }

  @Override
  public int compareTo(PromotionFullPiecesDiscount<T> tPromotionFullPiecesDiscount) {
    if (tPromotionFullPiecesDiscount == null || tPromotionFullPiecesDiscount.minAmount == null) {
      return -1;
    }
    return this.minAmount.compareTo(tPromotionFullPiecesDiscount.getMinAmount());
  }
}