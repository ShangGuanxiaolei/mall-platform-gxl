package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * xquark_order_third_check
 * @author 
 */
public class XquarkOrderThirdCheck implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 批次
     */
    private String lot;

    /**
     * 导入时间
     */
    private Date importTime;

    /**
     * 审核人
     */
    private String checkUser;

    /**
     * 提交人
     */
    private String submitUser;

    /**
     * 订单状态0:待审核1审核通过2审核不通过
     */
    private String checkState;

    /**
     * 批次来源
     */
    private String sourceFrom;

    /**
     * 审核通过时间
     */
    private Date checkTime;

    private Date createAt;

    private Date updateAt;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public Date getImportTime() {
        return importTime;
    }

    public void setImportTime(Date importTime) {
        this.importTime = importTime;
    }

    public String getCheckUser() {
        return checkUser;
    }

    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public String getSubmitUser() {
        return submitUser;
    }

    public void setSubmitUser(String submitUser) {
        this.submitUser = submitUser;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkOrderThirdCheck other = (XquarkOrderThirdCheck) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getLot() == null ? other.getLot() == null : this.getLot().equals(other.getLot()))
            && (this.getImportTime() == null ? other.getImportTime() == null : this.getImportTime().equals(other.getImportTime()))
            && (this.getCheckUser() == null ? other.getCheckUser() == null : this.getCheckUser().equals(other.getCheckUser()))
            && (this.getSubmitUser() == null ? other.getSubmitUser() == null : this.getSubmitUser().equals(other.getSubmitUser()))
            && (this.getCheckState() == null ? other.getCheckState() == null : this.getCheckState().equals(other.getCheckState()))
            && (this.getSourceFrom() == null ? other.getSourceFrom() == null : this.getSourceFrom().equals(other.getSourceFrom()))
            && (this.getCheckTime() == null ? other.getCheckTime() == null : this.getCheckTime().equals(other.getCheckTime()))
            && (this.getCreateAt() == null ? other.getCreateAt() == null : this.getCreateAt().equals(other.getCreateAt()))
            && (this.getUpdateAt() == null ? other.getUpdateAt() == null : this.getUpdateAt().equals(other.getUpdateAt()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getLot() == null) ? 0 : getLot().hashCode());
        result = prime * result + ((getImportTime() == null) ? 0 : getImportTime().hashCode());
        result = prime * result + ((getCheckUser() == null) ? 0 : getCheckUser().hashCode());
        result = prime * result + ((getSubmitUser() == null) ? 0 : getSubmitUser().hashCode());
        result = prime * result + ((getCheckState() == null) ? 0 : getCheckState().hashCode());
        result = prime * result + ((getSourceFrom() == null) ? 0 : getSourceFrom().hashCode());
        result = prime * result + ((getCheckTime() == null) ? 0 : getCheckTime().hashCode());
        result = prime * result + ((getCreateAt() == null) ? 0 : getCreateAt().hashCode());
        result = prime * result + ((getUpdateAt() == null) ? 0 : getUpdateAt().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", lot=").append(lot);
        sb.append(", importTime=").append(importTime);
        sb.append(", checkUser=").append(checkUser);
        sb.append(", submitUser=").append(submitUser);
        sb.append(", checkState=").append(checkState);
        sb.append(", sourceFrom=").append(sourceFrom);
        sb.append(", checkTime=").append(checkTime);
        sb.append(", createAt=").append(createAt);
        sb.append(", updateAt=").append(updateAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}