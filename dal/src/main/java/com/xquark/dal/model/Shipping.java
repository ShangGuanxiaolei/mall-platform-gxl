package com.xquark.dal.model;

/**
 * @auther liuwei
 * @date 2018/6/8 17:15
 */
public class Shipping {

  private int id;
  private int productId;
  private String sfshipping;
  private String sfairline;

  public Shipping(int id, int productId, String sfshipping, String sfairline) {
    this.id = id;
    this.productId = productId;
    this.sfshipping = sfshipping;
    this.sfairline = sfairline;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public String getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(String sfshipping) {
    this.sfshipping = sfshipping;
  }

  public String getSfairline() {
    return sfairline;
  }

  public void setSfairline(String sfairline) {
    this.sfairline = sfairline;
  }

  @Override
  public String toString() {
    return "Shipping{" +
        "id=" + id +
        ", productId=" + productId +
        ", sfshipping='" + sfshipping + '\'' +
        ", sfairline='" + sfairline + '\'' +
        '}';
  }
}
