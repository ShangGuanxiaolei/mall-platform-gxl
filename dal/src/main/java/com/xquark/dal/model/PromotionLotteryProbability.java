package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionLotteryProbability extends BaseEntityArchivableImpl {

    /**
     * 关联抽奖活动id
     */
    private String promotionLotteryId;

    /**
     * 奖品 根据抽奖类型确定 0 - 多少德分 1 - 几等奖
     */
    private Integer prize;

    /**
     * 中奖概率
     */
    private BigDecimal probability;

    /**
     * 份数
     */
    private Integer amount;

    /**
     * 0 - 德分类型 1 - 奖品类型
     */
    private Integer lotteryType;

    /**
     * 关联skuId
     */
    private Long skuId;

    private String name;

    /**
     * 前端展示顺序
     */
    private Integer sortOrder;

    /**
     * 商品或者红包图片
     */
    private String image;

    private Date createdAt;

    private Date updatedAt;

    /**
     * 逻辑删除 0-正常  1-已删除
     */
    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public String getPromotionLotteryId() {
        return promotionLotteryId;
    }

    public void setPromotionLotteryId(String promotionLotteryId) {
        this.promotionLotteryId = promotionLotteryId;
    }

    public Integer getPrize() {
        return prize;
    }

    public void setPrize(Integer prize) {
        this.prize = prize;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(Integer lotteryType) {
        this.lotteryType = lotteryType;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}