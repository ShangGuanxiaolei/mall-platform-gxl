package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class SkuCombine extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  /**
   * 冗余字段，方便根据商品查询
   */
  private String productId;

  /**
   * 套餐商品的skuId
   */
  private String skuId;

  private Date validFrom;

  private Date validTo;

  public SkuCombine() {
  }

  public SkuCombine(String productId, String skuId, Date validFrom, Date validTo) {
    this.productId = productId;
    this.skuId = skuId;
    this.validFrom = validFrom;
    this.validTo = validTo;
  }

  /**
   * 逻辑删除字段
   */
  private Boolean archive;

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

}