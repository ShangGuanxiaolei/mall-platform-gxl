package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * @author
 */
public class HealthTestAnswer extends BaseEntityImpl implements Archivable,
    Comparable<HealthTestAnswer> {

  private String id;

  /**
   * 答案组id
   */
  private String groupId;

  private Double score;

  /**
   * 答案别名 统计用
   */
  private Integer alias;

  /**
   * 答案内容
   */
  private String content;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public Double getScore() {
    return score;
  }

  public void setScore(Double score) {
    this.score = score;
  }

  public Integer getAlias() {
    return alias;
  }

  public void setAlias(Integer alias) {
    this.alias = alias;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public int compareTo(HealthTestAnswer answer) {
    if (answer == null) {
      return -1;
    }
    return this.getAlias() - answer.getAlias();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    HealthTestAnswer answer = (HealthTestAnswer) o;

    return id.equals(answer.id) && groupId.equals(answer.groupId) && content.equals(answer.content);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}