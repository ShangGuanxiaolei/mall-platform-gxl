package com.xquark.dal.model.mypiece;

import java.sql.DatabaseMetaData;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/22 0:02
 */
public class PromotionRewardsVo {
    private Long id;
    private String pCode;
    private String pType;
    private String pDetailCode;
    private String rewardCode;
    private String rewardCondition;
    private String rewardRule;
    private String rewardType;
    private String rewardSubType;
    private String creator;
    private String auditor;
    private String updator;
    private Date createdAt;
    private Date updatedAt;
    private int is_deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getRewardCondition() {
        return rewardCondition;
    }

    public void setRewardCondition(String rewardCondition) {
        this.rewardCondition = rewardCondition;
    }

    public String getRewardRule() {
        return rewardRule;
    }

    public void setRewardRule(String rewardRule) {
        this.rewardRule = rewardRule;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardSubType() {
        return rewardSubType;
    }

    public void setRewardSubType(String rewardSubType) {
        this.rewardSubType = rewardSubType;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }
}
