package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * @author luqig
 * @since 2019-04-28
 */
public class ActivityPush extends BaseEntityImpl implements Archivable {
  /**
   * 活动模块
   */
  private String activityModule;

  /**
   * 动作类型
   */
  private String actionType;

  /**
   * 简称
   */
  private String abbreviation;

  /**
   * 预计推送时间
   */
  private Date estimatePushTime;

  /**
   * 实际推送时间
   */
  private Date pushTime;

  /**
   * 推送优先级
   */
  private Integer priority;

  /**
   * 落地页url
   */
  private String customUrl;

  /**
   * 文案内容
   */
  private String content;

  /**
   * 推送用户类型
   * 1:推所有,2:推部分用户
   */
  private Integer pushUserType;

  /**
   * 推送用户id
   * id使用,隔开
   */
  private String pushUserCpIds;

  /**
   * 需要的参数
   */
  private String needParam;

  /**
   * 推送状态
   * 0.暂未推送 1.已推送
   */
  private Integer pushState;

  private Boolean archive;

  public String getActivityModule() {
    return activityModule;
  }

  public void setActivityModule(String activityModule) {
    this.activityModule = activityModule;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public Date getEstimatePushTime() {
    return estimatePushTime;
  }

  public void setEstimatePushTime(Date estimatePushTime) {
    this.estimatePushTime = estimatePushTime;
  }

  public Date getPushTime() {
    return pushTime;
  }

  public void setPushTime(Date pushTime) {
    this.pushTime = pushTime;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public String getCustomUrl() {
    return customUrl;
  }

  public void setCustomUrl(String customUrl) {
    this.customUrl = customUrl;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getPushUserType() {
    return pushUserType;
  }

  public void setPushUserType(Integer pushUserType) {
    this.pushUserType = pushUserType;
  }

  public String getPushUserCpIds() {
    return pushUserCpIds;
  }

  public void setPushUserCpIds(String pushUserCpIds) {
    this.pushUserCpIds = pushUserCpIds;
  }

  public String getNeedParam() {
    return needParam;
  }

  public void setNeedParam(String needParam) {
    this.needParam = needParam;
  }

  public Integer getPushState() {
    return pushState;
  }

  public void setPushState(Integer pushState) {
    this.pushState = pushState;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  @Override
  public String toString() {
    return "ActivityPush{" +
        "activityModule='" + activityModule + '\'' +
        ", actionType='" + actionType + '\'' +
        ", abbreviation='" + abbreviation + '\'' +
        ", estimatePushTime=" + estimatePushTime +
        ", pushTime=" + pushTime +
        ", priority=" + priority +
        ", customUrl='" + customUrl + '\'' +
        ", content='" + content + '\'' +
        ", pushUserType=" + pushUserType +
        ", pushUserCpIds='" + pushUserCpIds + '\'' +
        ", needParam='" + needParam + '\'' +
        ", pushState=" + pushState +
        ", archive=" + archive +
        '}';
  }
}
