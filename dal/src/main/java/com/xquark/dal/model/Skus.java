package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * @auther liuwei
 * @date 2018/6/16 15:48
 */
public class Skus {
  private String SkuID;//平台规格ID
  private String skuOuterID;//规格外部商家编码
  private BigDecimal skuprice = new BigDecimal(0);//规格价格
  private int skuQuantity;//规格数量
  private String skuname;//规格名称
  private String skuproperty;//规格属性
  private String skupictureurl;//规格图片URL

  public Skus() {
  }

  public Skus(String skuID, String skuOuterID, BigDecimal skuprice, int skuQuantity,
      String skuname, String skuproperty, String skupictureurl) {
    SkuID = skuID;
    this.skuOuterID = skuOuterID;
    this.skuprice = skuprice;
    this.skuQuantity = skuQuantity;
    this.skuname = skuname;
    this.skuproperty = skuproperty;
    this.skupictureurl = skupictureurl;
  }

  public String getSkuID() {
    return SkuID;
  }

  public void setSkuID(String skuID) {
    SkuID = skuID;
  }

  public String getSkuOuterID() {
    return skuOuterID;
  }

  public void setSkuOuterID(String skuOuterID) {
    this.skuOuterID = skuOuterID;
  }

  public BigDecimal getSkuprice() {
    return skuprice;
  }

//  public void setSkuprice(BigDecimal skuprice) {
//    this.skuprice = skuprice;
//  }

  public int getSkuQuantity() {
    return skuQuantity;
  }

  public void setSkuQuantity(int skuQuantity) {
    this.skuQuantity = skuQuantity;
  }

  public String getSkuname() {
    return skuname;
  }

  public void setSkuname(String skuname) {
    this.skuname = skuname;
  }

  public String getSkuproperty() {
    return skuproperty;
  }

  public void setSkuproperty(String skuproperty) {
    this.skuproperty = skuproperty;
  }

  public String getSkupictureurl() {
    return skupictureurl;
  }

  public void setSkupictureurl(String skupictureurl) {
    this.skupictureurl = skupictureurl;
  }

  @Override
  public String toString() {
    return "Skus{" +
        "SkuID='" + SkuID + '\'' +
        ", skuOuterID='" + skuOuterID + '\'' +
        ", skuprice=" + skuprice +
        ", skuQuantity=" + skuQuantity +
        ", skuname='" + skuname + '\'' +
        ", skuproperty='" + skuproperty + '\'' +
        ", skupictureurl='" + skupictureurl + '\'' +
        '}';
  }
}
