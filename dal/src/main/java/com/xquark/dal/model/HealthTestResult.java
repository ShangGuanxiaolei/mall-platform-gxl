package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class HealthTestResult implements Serializable {

  private String id;

  private String moduleId;

  private String physiqueId;

  private Double start;

  private Double end;

  /**
   * 计算类型
   */
  private String type;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private String description;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getModuleId() {
    return moduleId;
  }

  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  public String getPhysiqueId() {
    return physiqueId;
  }

  public void setPhysiqueId(String physiqueId) {
    this.physiqueId = physiqueId;
  }

  public Double getStart() {
    return start;
  }

  public void setStart(Double start) {
    this.start = start;
  }

  public Double getEnd() {
    return end;
  }

  public void setEnd(Double end) {
    this.end = end;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}