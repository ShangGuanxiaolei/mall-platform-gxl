package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class UserCard extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  public UserCard() {
  }

  public UserCard(String userId, String cardId) {
    this.userId = userId;
    this.cardId = cardId;
  }

  public UserCard(String ownerShopId, String userId, String cardId) {
    this(userId, cardId);
    this.ownerShopId = ownerShopId;
  }

  private String ownerShopId;

  private String userId;

  private String cardId;


  private Boolean archive;


  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCardId() {
    return cardId;
  }

  public void setCardId(String cardId) {
    this.cardId = cardId;
  }


  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}