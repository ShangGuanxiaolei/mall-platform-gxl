package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.TwitterLevelType;

import java.math.BigDecimal;

public class TwitterLevel extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Long ownShopId;

  private String name;

  private double firstLevelRate;

  private double secondLevelRate;

  private double thirdLevelRate;

  private Boolean archive;

  private TwitterLevelType type;

  private BigDecimal conditions;

  private String description;

  public TwitterLevelType getType() {
    return type;
  }

  public void setType(TwitterLevelType type) {
    this.type = type;
  }

  public BigDecimal getConditions() {
    return conditions;
  }

  public void setConditions(BigDecimal conditions) {
    this.conditions = conditions;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(Long ownShopId) {
    this.ownShopId = ownShopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getFirstLevelRate() {
    return firstLevelRate;
  }

  public void setFirstLevelRate(double firstLevelRate) {
    this.firstLevelRate = firstLevelRate;
  }

  public double getSecondLevelRate() {
    return secondLevelRate;
  }

  public void setSecondLevelRate(double secondLevelRate) {
    this.secondLevelRate = secondLevelRate;
  }

  public double getThirdLevelRate() {
    return thirdLevelRate;
  }

  public void setThirdLevelRate(double thirdLevelRate) {
    this.thirdLevelRate = thirdLevelRate;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}