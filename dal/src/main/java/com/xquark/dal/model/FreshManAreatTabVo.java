package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:15:51
 * Des:新人页面tab表
 */
public class FreshManAreatTabVo {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT,
//    `tab_id` int(11) NOT NULL COMMENT 'tabID定死（0，1，2，3...）',
//    `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'tab名称',
//    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '模块排序',
//    `status` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1启用，0冻结',
//    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '专区创建时间',
//    `updated_at` timestamp NULL DEFAULT NULL,
    private Long id;
    private int tabId;
    private String name;
    private int sort;
    private int status;
    private Date createdAt;
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTabId() {
        return tabId;
    }

    public void setTabId(int tabId) {
        this.tabId = tabId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}