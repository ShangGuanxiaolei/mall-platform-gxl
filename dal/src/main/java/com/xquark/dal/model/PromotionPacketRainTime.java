package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.time.Duration;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionPacketRainTime extends BaseEntityArchivableImpl {

    /**
     * 红包雨活动id
     */
    private String promotionPacketRainId;

    /**
     * 开始活动时间
     */
    private Date startTime;

    /**
     * 结束活动时间
     */
    private Date endTime;

    private static final long serialVersionUID = 1L;

    public String getPromotionPacketRainId() {
        return promotionPacketRainId;
    }

    public void setPromotionPacketRainId(String promotionPacketRainId) {
        this.promotionPacketRainId = promotionPacketRainId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Duration getBetween() {
        if (startTime == null || endTime == null) {
            return Duration.ZERO;
        }
        return Duration.between(startTime.toInstant(), endTime.toInstant());
    }

}