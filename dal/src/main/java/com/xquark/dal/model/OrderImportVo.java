package com.xquark.dal.model;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/6/5
 * Time: 17:33
 * Description: 物流导入订单扩展
 */
public class OrderImportVo  extends Order {

    private  String  thirdOrderNo;
    private  String  erpOrderNo;
    private  String  lot;
    private  String  thirdOrderItemNo;
    private  String  firstOrderNo;
    private  String  islead;

    public String getIslead() {
        return islead;
    }

    public void setIslead(String islead) {
        this.islead = islead;
    }

    public String getThirdOrderNo() {
        return thirdOrderNo;
    }

    public void setThirdOrderNo(String thirdOrderNo) {
        this.thirdOrderNo = thirdOrderNo;
    }

    public String getErpOrderNo() {
        return erpOrderNo;
    }

    public void setErpOrderNo(String erpOrderNo) {
        this.erpOrderNo = erpOrderNo;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getThirdOrderItemNo() {
        return thirdOrderItemNo;
    }

    public void setThirdOrderItemNo(String thirdOrderItemNo) {
        this.thirdOrderItemNo = thirdOrderItemNo;
    }

    public String getFirstOrderNo() {
        return firstOrderNo;
    }

    public void setFirstOrderNo(String firstOrderNo) {
        this.firstOrderNo = firstOrderNo;
    }
}
