package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AntifakeType;
import com.xquark.dal.status.BonusType;

import java.util.Date;

public class Antifake extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private String orderId;

  private String userId;

  private int isSystem;

  private String code;

  private AntifakeType codeType;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public int getIsSystem() {
    return isSystem;
  }

  public void setIsSystem(int isSystem) {
    this.isSystem = isSystem;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public AntifakeType getCodeType() {
    return codeType;
  }

  public void setCodeType(AntifakeType codeType) {
    this.codeType = codeType;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}