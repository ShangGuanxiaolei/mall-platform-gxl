package com.xquark.dal.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author liuwei
 * @date 18-7-26 下午3:02
 */
public class PointExportByOrderSpec {

    public static final String SHEET_STR = "收入报表-按订单";

    private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

    public static String[] KEY_SET;
    public static String[] VALUE_SET;

    static {
        EXPORT_FORMAT.put("ordermonth", "getOrderMonth");
        EXPORT_FORMAT.put("Payee Id", "getPayeeId");
        EXPORT_FORMAT.put("Payee Name", "getPayeeName");
        EXPORT_FORMAT.put("Purchase Id", "getPurchaseId");
        EXPORT_FORMAT.put("Purchase Name", "getPurchaseName");
        EXPORT_FORMAT.put("Order", "getOrderId");
        EXPORT_FORMAT.put("ctryearn", "getCtryEarn");
        EXPORT_FORMAT.put("trancd", "getTrancd");
        EXPORT_FORMAT.put("earnbase", "getEarnBase");
        EXPORT_FORMAT.put("%earn", "getEarn");
        EXPORT_FORMAT.put("amt-earn", "getAmtEarn");
        EXPORT_FORMAT.put("ctry-paid", "getCrtyPaid");
        EXPORT_FORMAT.put("amt-paid","getAmtEarn");
        EXPORT_FORMAT.put("DistrType", "getDistrType");
        EXPORT_FORMAT.put("paid_date", "");
        EXPORT_FORMAT.put("remark","getRemark");
        KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
        VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
    }

    private static String[][] getArrayFromHash(Map<String, String> data) {
        String[][] str;
        {
            Object[] keys = data.keySet().toArray();
            Object[] values = data.values().toArray();
            str = new String[keys.length][values.length];
            for (int i = 0; i < keys.length; i++) {
                str[0][i] = (String) keys[i];
                str[1][i] = (String) values[i];
            }
        }
        return str;
    }
}
