package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * 作者: wangxh 创建日期: 17-4-7 简介: 按钮数据库映射类
 */
public class ModuleFunction extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1307333281763914051L;

  public ModuleFunction() {
  }

  public ModuleFunction(String functionId, String moduleId) {
    this.functionId = functionId;
    this.moduleId = moduleId;
  }

  private String id;
  private String functionId;
  private String moduleId;

  private Date createdAt;
  private Date updatedAt;
  private Boolean archive;

  public String getFunctionId() {
    return functionId;
  }

  public void setFunctionId(String functionId) {
    this.functionId = functionId;
  }

  public String getModuleId() {
    return moduleId;
  }

  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Date getCreatedAt() {
    return this.createdAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public Date getUpdatedAt() {
    return this.updatedAt;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String getId() {
    return this.id;
  }
}
