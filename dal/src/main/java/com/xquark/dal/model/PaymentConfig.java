package com.xquark.dal.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializerSync;

public class PaymentConfig extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name;

  private String code;

  @JsonSerialize(using = JsonResourceUrlSerializerSync.class)
  private String icon;

  private String payAgreementId;

  private Date createdAt;

  private Date updatedAt;

  private String description;

  private String descriptionExt;

  public String getDescriptionExt() {
    return descriptionExt;
  }

  public void setDescriptionExt(String descriptionExt) {
    this.descriptionExt = descriptionExt;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon == null ? null : icon.trim();
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getPayAgreementId() {
    return payAgreementId;
  }

  public void setPayAgreementId(String payAgreementId) {
    this.payAgreementId = payAgreementId;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description == null ? null : description.trim();
  }

  @Override
  public Boolean getArchive() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setArchive(Boolean archive) {
    // TODO Auto-generated method stub

  }
}