package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

public class FragmentImage extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String fragmentId;
  private String img;
  private Integer idx;

  public String getFragmentId() {
    return fragmentId;
  }

  public void setFragmentId(String fragmentId) {
    this.fragmentId = fragmentId;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public Integer getIdx() {
    return idx;
  }

  public void setIdx(Integer idx) {
    this.idx = idx;
  }

}
