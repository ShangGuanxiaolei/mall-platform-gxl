package com.xquark.dal.model;

import org.apache.commons.lang3.StringUtils;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * 商品分销池
 *
 * @author wyx
 */
public class ActivityProduct extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 7649425392293716086L;

  private String ticketId;
  private String productId;
  private String activityId;
  private Integer productOrder;
  private Boolean archive;

  public String getActivityId() {
    return activityId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public String getProductId() {
    return productId;
  }

  public Integer getProductOrder() {
    return productOrder;
  }

  public String getTicketId() {
    return ticketId;
  }

  public void setActivityId(String activityId) {
    if (StringUtils.isBlank(activityId)) {
      activityId = "0";
    }
    this.activityId = activityId;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }


  public void setProductOrder(Integer productOrder) {
    if (null == productOrder) {
      productOrder = 999;
    }
    this.productOrder = productOrder;
  }

  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }

}