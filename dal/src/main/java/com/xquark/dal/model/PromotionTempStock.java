package com.xquark.dal.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

public class PromotionTempStock implements Serializable {

  private Long id;

  /**
   * 活动编码
   */
  private String pCode;

  /**
   * sku编码
   */
  private String skuCode;

  /**
   * 活动sku数量
   */
  private String pSkuNum;

  /**
   * 可用sku数量
   */
  private String pUsableSkuNum;

  /**
   * 创建者
   */
  private String creator;

  private Date createdAt;

  private Date updatedAt;

  /**
   * 删除标记,1有效，0已删除
   */
  private Boolean isDeleted;

  /**
   * 商品限购数量，0表示无限制
   */
  private Integer restriction;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getpSkuNum() {
    return pSkuNum;
  }

  public void setpSkuNum(String pSkuNum) {
    this.pSkuNum = pSkuNum;
  }

  public String getpUsableSkuNum() {
    return pUsableSkuNum;
  }

  public Integer getUsableSkuNum() {
    return Optional.ofNullable(getpUsableSkuNum())
            .filter(StringUtils::isNotBlank)
            .map(Integer::valueOf)
            .orElse(0);
  }

  public void setpUsableSkuNum(String pUsableSkuNum) {
    this.pUsableSkuNum = pUsableSkuNum;
  }

  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Integer getRestriction() {
    return restriction;
  }

  public void setRestriction(Integer restriction) {
    this.restriction = restriction;
  }
}