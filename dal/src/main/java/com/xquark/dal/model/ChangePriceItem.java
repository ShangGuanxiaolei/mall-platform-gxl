package com.xquark.dal.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author wangxinhua
 * @since 1.0
 */
public interface ChangePriceItem {

    default BigDecimal getConversionPrice() {
        final BigDecimal price = getPrice();
        final BigDecimal point = getPoint();
        if (price == null || point == null) {
            return BigDecimal.ZERO;
        }
        return price.subtract(point.divide(BigDecimal.TEN, 2, RoundingMode.HALF_EVEN));
    }

    BigDecimal getPrice();

    BigDecimal getPoint();

    BigDecimal getServerAmt();

    BigDecimal getReduction();
}
