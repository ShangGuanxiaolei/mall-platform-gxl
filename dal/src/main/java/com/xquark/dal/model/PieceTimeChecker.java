package com.xquark.dal.model;

import java.util.Date;
import java.util.Optional;

/**
 * @author wangxinhua
 * @version 1.0.0 $ 2019/4/3
 */
public interface PieceTimeChecker {

  Date getGroupFinishTime();

  Date getGroupOpenTime();

  Integer getJumpQueueTime();

  Long getPieceEffectTime();

  default Optional<Date> getTranEndTime() {
    Date groupFinishTime = getGroupFinishTime();
    Date groupOpenTime = getGroupOpenTime();
    Integer jumpQueueTime = getJumpQueueTime();
    Long pieceEffectTime = getPieceEffectTime();
    if (groupFinishTime != null) {
      // 已成团，返回插队时间
      return Optional.ofNullable(jumpQueueTime)
          .flatMap(jt -> Optional.of(groupFinishTime)
              .map(Date::getTime)
              .map(fTime -> jt * 3600 * 1000 + fTime)
              .map(Date::new));
    }
    // 未成团，返回成团有效时间
    return Optional.ofNullable(groupOpenTime)
        .map(Date::getTime)
        .flatMap(gt -> Optional.ofNullable(pieceEffectTime)
            .map(eft -> gt + eft * 3600 * 1000)
            .map(Date::new));
  }

}
