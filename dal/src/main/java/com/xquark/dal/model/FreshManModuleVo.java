package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/13
 * Time:13:26
 * Des:新人页面模块
 */
public class FreshManModuleVo {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT,
//   `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '模块名称',
//    `type` int(11) NOT NULL COMMENT '1:banner 2：新人专享 3：进度条',
//    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
//    `status` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1启用，0冻结',
//    `cpid` bigint(20) NOT NULL COMMENT '操作人',
//    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//    `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    private Long id;
    private String name;
    private int type; //BANNER:1 新人专区:2 3:新客引导
    private int sort;
    private int status;
    private Long cpId;
    private Date createdAt;
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}