package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * 新人专区商品价格
 *
 * @author tanggb
 * @date 2019/03/06 15:34
 */
public class FreshmanProduct {
    //专区价
    private BigDecimal price;
    //专区兑换德分
    private BigDecimal exchangePoint;
    //专区兑换价
    private BigDecimal exchangePrice;
    //统计商品是否存在
    private Integer countProduct;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExchangePoint() {
        return exchangePoint;
    }

    public void setExchangePoint(BigDecimal exchangePoint) {
        this.exchangePoint = exchangePoint;
    }

    public BigDecimal getExchangePrice() {
        return exchangePrice;
    }

    public void setExchangePrice(BigDecimal exchangePrice) {
        this.exchangePrice = exchangePrice;
    }

    public Integer getCountProduct() {
        return countProduct;
    }

    public void setCountProduct(Integer countProduct) {
        this.countProduct = countProduct;
    }
}
