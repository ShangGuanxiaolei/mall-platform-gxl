package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

public class TweetImage extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String tweetId;

  private String img;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  private Boolean archive;

  private int imgOrder;

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getTweetId() {
    return tweetId;
  }

  public void setTweetId(String tweetId) {
    this.tweetId = tweetId;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public int getImgOrder() {
    return imgOrder;
  }

  public void setImgOrder(int imgOrder) {
    this.imgOrder = imgOrder;
  }

}