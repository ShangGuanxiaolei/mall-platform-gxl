package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.TagsCategory;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class Tags extends BaseEntityImpl implements Archivable, IndexAble, Comparable<Tags> {

  private static final long serialVersionUID = 7437565654746377410L;

  public Tags() {
  }

  public Tags(String name) {
    this.name = name;
  }

  private String id;

  /**
   * 标签名称
   */
  private String name;

  /**
   * 热度
   */
  private Integer score = 0;

  /**
   * 标签分类
   */
  private TagsCategory category;

  /**
   * 标签描述
   */
  private String description;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getScore() {
    return score;
  }

  public void setScore(Integer score) {
    this.score = score;
  }

  public TagsCategory getCategory() {
    return category;
  }

  public void setCategory(TagsCategory category) {
    this.category = category;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getCategoryStr() {
    return category == null ? "" : category.getStr();
  }

  @Override
  public String toString() {
    return "Tags{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", score=" + score +
        ", description='" + description + '\'' +
        ", createdAt=" + createdAt +
        ", updatedAt=" + updatedAt +
        ", archive=" + archive +
        '}';
  }

  @Override
  public int compareTo(Tags o) {
    if (o == null) {
      return 1;
    }
    return this.getScore() - o.getScore();
  }
}