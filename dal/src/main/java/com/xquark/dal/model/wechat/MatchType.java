package com.xquark.dal.model.wechat;

/**
 * 自定义回复的匹配模式
 */
public enum MatchType {
  LITERAL,  //精确匹配
  LIKE,  //包含该关键字
  ADD,  //被添加自动回复
  AUTO,  //消息自动回复
}
