package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * customerBankAudit
 *
 */
public class CustomerBankAudit implements Serializable {

  /**
   * 主键
   */
  private Long cpId;

  /**
   * 1 insert, 2 update, 3 delete, 5 update under insert
   */
  private Byte auditType;

  private int isDeleted;

  private String bankAccount;


  private static final long serialVersionUID = 1L;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public Byte getAuditType() {
    return auditType;
  }

  public void setAuditType(Byte auditType) {
    this.auditType = auditType;
  }

  public int getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(int isDeleted) {
    this.isDeleted = isDeleted;
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }
}