package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * Created by quguangming on 16/5/23.
 */
public class MerchantPermission extends BaseEntityImpl implements Archivable {

  private String parentId;
  private String permission;
  private Date createAt;
  private Date updateAt;
  private String menuFlag;
  private String description;
  private Boolean archive;
  private Integer level;
  private String menuModule;

  public String getMenuModule() {
    return menuModule;
  }

  public void setMenuModule(String menuModule) {
    this.menuModule = menuModule;
  }

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getPermission() {
    return permission;
  }

  public void setPermission(String permission) {
    this.permission = permission;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public String getMenuFlag() {
    return menuFlag;
  }

  public void setMenuFlag(String menuFlag) {
    this.menuFlag = menuFlag;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
