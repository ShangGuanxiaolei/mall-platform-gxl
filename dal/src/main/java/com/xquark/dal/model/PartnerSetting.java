package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.FamilyCardStatus;

public class PartnerSetting extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerShopId;

  private String name;

  private double cmRate;

  private Integer amountDemand;

  private Boolean archive;

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getCmRate() {
    return cmRate;
  }

  public void setCmRate(double cmRate) {
    this.cmRate = cmRate;
  }


  public Integer getAmountDemand() {
    return amountDemand;
  }

  public void setAmountDemand(Integer amountDemand) {
    this.amountDemand = amountDemand;
  }


  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}