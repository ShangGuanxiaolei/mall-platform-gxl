package com.xquark.dal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * order_header
 *
 * @author kong
 */
public class OrderHeader implements Serializable {

  /**
   * 订单号，种子表生成
   */
  private String orderid;

  /**
   * 资格证号
   */
  private Long cpid;

  /**
   * 分享
   */
  private Long fromCpId;

  /**
   * 订单月，201609
   */
  private Integer ordermonth;

  private BigDecimal reductionAmt;

  /**
   * 产品金额
   */
  private BigDecimal productamt;

  /**
   * 税费
   */
  private BigDecimal taxamt;

  /**
   * 总VP值
   */
  private BigDecimal vp;

  /**
   * 计酬金额
   */
  private BigDecimal commissionProductamt;

  /**
   * 折扣金额
   */
  private BigDecimal discountamt;

  private BigDecimal useDePoints;

  /**
   * 积分抵扣
   */
  private BigDecimal usePoints;

  /**
   * 运费
   */
  private BigDecimal freightamt;

  /**
   * 实际运费
   */
  private BigDecimal freightamtActual;

  /**
   * 推广费
   */
  private BigDecimal promoAmt;

  /**
   * 应付
   */
  private BigDecimal totaldue;

  /**
   * 实付
   */
  private BigDecimal totalpaid;

  /**
   * 状态（0=>待付，1=>完成，2=>取消，3=>待发货，4=>发货中，5=>已发货，6 => 退货）
   */
  private Integer status;

  /**
   * 订单是否可用(0=>否，1是)
   */
  private Integer isActive;

  /**
   * 订单创建时间
   */
  private Date createtime;

  /**
   * 订单过期时间
   */
  private Date deadline;

  /**
   * 申请支付时间
   */
  private Date paytime;

  /**
   * 支付完成时间
   */
  private Date paytimeFinish;

  /**
   * 发货完成时间
   */
  private Date sendtimeFinish;

  /**
   * 完成时间
   */
  private Date finishtime;

  /**
   * 配送方式，0单独配送，1随首单一起配送。
   */
  private Integer deliverytype;

  /**
   * 外键ID，店铺表
   */
  private String storeId;

  /**
   * 外键ID，用户表
   */
  private String orderinguserId;

  /**
   * motion是否计算标志，0未算，1已算
   */
  private Integer iscalculated;

  /**
   * 其他调整
   */
  private BigDecimal adjustAmount;

  /**
   * 保险费，产品总金额 * 保险费费率
   */
  private BigDecimal insurance;

  /**
   * 配送费,保险费 + 运费
   */
  private BigDecimal totalFreight;

  /**
   * 备注
   */
  private String remark;

  /**
   * 0：后台下单；1：补单；
   */
  private Integer isSuppleOrder;

  /**
   * 来源平台 1 hds, 2 vivilife, 3 ecommerce
   */
  private Byte source;

  /**
   * 订单明细记录数，用于核对header和detail数据是否完整
   */
  private Integer detailCount;

  /**
   * 创建人
   */
  private String createdBy;

  /**
   * 创建时间
   */
  private Date createdAt;

  /**
   * 更新人
   */
  private String updatedBy;

  /**
   * 更新时间
   */
  private Date updatedAt;

  /**
   * 加入类型（判断是否改变身份）
   */
  private Integer joinType;

  /**
   * 逻辑删除标记，0未删，1已删。
   */
  private Integer isdeleted;

  private static final long serialVersionUID = 1L;

  public String getOrderid() {
    return orderid;
  }

  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  public Long getCpid() {
    return cpid;
  }

  public void setCpid(Long cpid) {
    this.cpid = cpid;
  }

  public Integer getOrdermonth() {
    return ordermonth;
  }

  public void setOrdermonth(Integer ordermonth) {
    this.ordermonth = ordermonth;
  }

  public BigDecimal getProductamt() {
    return productamt;
  }

  public void setProductamt(BigDecimal productamt) {
    this.productamt = productamt;
  }

  public BigDecimal getTaxamt() {
    return taxamt;
  }

  public void setTaxamt(BigDecimal taxamt) {
    this.taxamt = taxamt;
  }

  public BigDecimal getVp() {
    return vp;
  }

  public void setVp(BigDecimal vp) {
    this.vp = vp;
  }

  public BigDecimal getCommissionProductamt() {
    return commissionProductamt;
  }

  public void setCommissionProductamt(BigDecimal commissionProductamt) {
    this.commissionProductamt = commissionProductamt;
  }

  public BigDecimal getDiscountamt() {
    return discountamt;
  }

  public void setDiscountamt(BigDecimal discountamt) {
    this.discountamt = discountamt;
  }

  public BigDecimal getUsePoints() {
    return usePoints;
  }

  public void setUsePoints(BigDecimal usePoints) {
    this.usePoints = usePoints;
  }

  public BigDecimal getUseDePoints() {
    return useDePoints;
  }

  public void setUseDePoints(BigDecimal useDePoints) {
    this.useDePoints = useDePoints;
  }

  public BigDecimal getFreightamt() {
    return freightamt;
  }

  public void setFreightamt(BigDecimal freightamt) {
    this.freightamt = freightamt;
  }

  public BigDecimal getFreightamtActual() {
    return freightamtActual;
  }

  public void setFreightamtActual(BigDecimal freightamtActual) {
    this.freightamtActual = freightamtActual;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }

  public BigDecimal getTotaldue() {
    return totaldue;
  }

  public void setTotaldue(BigDecimal totaldue) {
    this.totaldue = totaldue;
  }

  public BigDecimal getTotalpaid() {
    return totalpaid;
  }

  public void setTotalpaid(BigDecimal totalpaid) {
    this.totalpaid = totalpaid;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getIsActive() {
    return isActive;
  }

  public void setIsActive(Integer isActive) {
    this.isActive = isActive;
  }

  public Date getCreatetime() {
    return createtime;
  }

  public void setCreatetime(Date createtime) {
    this.createtime = createtime;
  }

  public Date getDeadline() {
    return deadline;
  }

  public void setDeadline(Date deadline) {
    this.deadline = deadline;
  }

  public Date getPaytime() {
    return paytime;
  }

  public void setPaytime(Date paytime) {
    this.paytime = paytime;
  }

  public Date getPaytimeFinish() {
    return paytimeFinish;
  }

  public void setPaytimeFinish(Date paytimeFinish) {
    this.paytimeFinish = paytimeFinish;
  }

  public Date getSendtimeFinish() {
    return sendtimeFinish;
  }

  public void setSendtimeFinish(Date sendtimeFinish) {
    this.sendtimeFinish = sendtimeFinish;
  }

  public Date getFinishtime() {
    return finishtime;
  }

  public void setFinishtime(Date finishtime) {
    this.finishtime = finishtime;
  }

  public Integer getDeliverytype() {
    return deliverytype;
  }

  public void setDeliverytype(Integer deliverytype) {
    this.deliverytype = deliverytype;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getOrderinguserId() {
    return orderinguserId;
  }

  public void setOrderinguserId(String orderinguserId) {
    this.orderinguserId = orderinguserId;
  }

  public Integer getIscalculated() {
    return iscalculated;
  }

  public void setIscalculated(Integer iscalculated) {
    this.iscalculated = iscalculated;
  }

  public BigDecimal getAdjustAmount() {
    return adjustAmount;
  }

  public void setAdjustAmount(BigDecimal adjustAmount) {
    this.adjustAmount = adjustAmount;
  }

  public BigDecimal getInsurance() {
    return insurance;
  }

  public void setInsurance(BigDecimal insurance) {
    this.insurance = insurance;
  }

  public BigDecimal getTotalFreight() {
    return totalFreight;
  }

  public void setTotalFreight(BigDecimal totalFreight) {
    this.totalFreight = totalFreight;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getIsSuppleOrder() {
    return isSuppleOrder;
  }

  public void setIsSuppleOrder(Integer isSuppleOrder) {
    this.isSuppleOrder = isSuppleOrder;
  }

  public Byte getSource() {
    return source;
  }

  public void setSource(Byte source) {
    this.source = source;
  }

  public Integer getDetailCount() {
    return detailCount;
  }

  public void setDetailCount(Integer detailCount) {
    this.detailCount = detailCount;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Integer getIsdeleted() {
    return isdeleted;
  }

  public void setIsdeleted(Integer isdeleted) {
    this.isdeleted = isdeleted;
  }

  public BigDecimal getReductionAmt() {
    return reductionAmt;
  }

  public void setReductionAmt(BigDecimal reductionAmt) {
    this.reductionAmt = reductionAmt;
  }

  public Long getFromCpId() {
    return fromCpId;
  }

  public void setFromCpId(Long fromCpId) {
    this.fromCpId = fromCpId;
  }

  public Integer getJoinType() {
    return joinType;
  }

  public void setJoinType(Integer joinType) {
    this.joinType = joinType;
  }

  @Override
  public String toString() {
    return "OrderHeader{" +
        "orderid='" + orderid + '\'' +
        ", cpid=" + cpid +
        ", status=" + status +
        ", createtime=" + createtime +
        ", paytime=" + paytime +
        '}';
  }
}