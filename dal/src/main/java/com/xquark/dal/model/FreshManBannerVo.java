package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:14:41
 * Des:新人页面banner
 */
public class FreshManBannerVo {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
//    `img_url` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片地址',
//    `start_at` timestamp NULL DEFAULT NULL COMMENT '开始时间',
//    `end_at` timestamp NULL DEFAULT NULL COMMENT '结束时间',
//    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
//    `redirect_type` int(11) NOT NULL DEFAULT '0' COMMENT '跳转类型，0自定义，1商品，2分类',
//    `redirect_url` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转地址0(存地址)1(存商品ID)2(存分类列表)',
//    `status` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1启用，0冻结',
//    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//    `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    private Long id;
    private String imgUrl;
    private Date startAt;
    private Date endAt;
    private int sort;
    private int redirectType;
    private String redirectUrl;
    private int status;
    private Date createdAt;
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}