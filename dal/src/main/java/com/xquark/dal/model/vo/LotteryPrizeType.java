package com.xquark.dal.model.vo;

public enum LotteryPrizeType {

    /***
     * 德分类型
     */
    SOCRE(0),
    /**
     * 奖品
     */
    PRODUC(1);

    private final int code;

    LotteryPrizeType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
