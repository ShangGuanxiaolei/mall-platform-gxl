package com.xquark.dal.model.mypiece;

public class RedisConfig {
    public static final int EXPIRE_TIME = 30*1000;//锁过期时间设置为30秒
    public static final int WAIT_TIME = 30*1000;//锁等待时间设置为10秒
}
