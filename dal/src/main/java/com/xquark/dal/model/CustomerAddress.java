package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;

/**
 * customeraddress
 *
 * @author
 */
public class CustomerAddress implements Serializable {
    private static final long serialVersionUID = 3972412721243171906L;
    private Long addressid; //主键id
    private Long cpid;
    private String nickname; //别称
    private String receivername;  //收货人姓名
    private Integer type;  //默认为2   类型，1通讯地址，2收货地址
    private Long provinceId;  // 一级省份
    private Long cityId;//二级城市
    private Long districtId; // 三级区域
    private String address; // 详细地址
    private String zipcode; // 邮编
    private String phoneno;//手机号
    private Integer status; //状态
    private String createdBy; // 创建人  设置为null
    private Date createdAt;//创建时间，插入逻辑在 mysql 实现，只查询
    private String updatedBy; //更新人 设置为null
    private Date updatedAt;//更新时间，更新逻辑在 mysql 实现，只查询
    private Long orderinguserId; //外键ID，用户表
    private Boolean isdefault;  //是否为默认地址，0否，1是
    private Boolean isdeleted;//逻辑删除标记，0未删，1已删。
    private String dtsSyncSource;//
    private String dtsSyncUid; //解决DTS双向同步问题,该值相等，就同步，不相等且DTS_SYNC_SOURCE 不等，同步

    public Long getAddressid() {
        return addressid;
    }

    public void setAddressid(Long addressid) {
        this.addressid = addressid;
    }

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(Long cpid) {
        this.cpid = cpid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getOrderinguserId() {
        return orderinguserId;
    }

    public void setOrderinguserId(Long orderinguserId) {
        this.orderinguserId = orderinguserId;
    }

    public Boolean getIsdefault() {
        return isdefault;
    }

    public void setIsdefault(Boolean isdefault) {
        this.isdefault = isdefault;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getDtsSyncSource() {
        return dtsSyncSource;
    }

    public void setDtsSyncSource(String dtsSyncSource) {
        this.dtsSyncSource = dtsSyncSource;
    }

    public String getDtsSyncUid() {
        return dtsSyncUid;
    }

    public void setDtsSyncUid(String dtsSyncUid) {
        this.dtsSyncUid = dtsSyncUid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CustomerAddress that = (CustomerAddress) o;

        return new EqualsBuilder()
                .append(addressid, that.addressid)
                .append(cpid, that.cpid)
                .append(nickname, that.nickname)
                .append(receivername, that.receivername)
                .append(type, that.type)
                .append(provinceId, that.provinceId)
                .append(cityId, that.cityId)
                .append(districtId, that.districtId)
                .append(address, that.address)
                .append(zipcode, that.zipcode)
                .append(phoneno, that.phoneno)
                .append(status, that.status)
                .append(createdBy, that.createdBy)
                .append(createdAt, that.createdAt)
                .append(updatedBy, that.updatedBy)
                .append(updatedAt, that.updatedAt)
                .append(orderinguserId, that.orderinguserId)
                .append(isdefault, that.isdefault)
                .append(isdeleted, that.isdeleted)
                .append(dtsSyncSource, that.dtsSyncSource)
                .append(dtsSyncUid, that.dtsSyncUid)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(addressid)
                .append(cpid)
                .append(nickname)
                .append(receivername)
                .append(type)
                .append(provinceId)
                .append(cityId)
                .append(districtId)
                .append(address)
                .append(zipcode)
                .append(phoneno)
                .append(status)
                .append(createdBy)
                .append(createdAt)
                .append(updatedBy)
                .append(updatedAt)
                .append(orderinguserId)
                .append(isdefault)
                .append(isdeleted)
                .append(dtsSyncSource)
                .append(dtsSyncUid)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "CustomerAddress{" +
                "addressid=" + addressid +
                ", cpid=" + cpid +
                ", nickname='" + nickname + '\'' +
                ", receivername='" + receivername + '\'' +
                ", type=" + type +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", districtId=" + districtId +
                ", address='" + address + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", phoneno='" + phoneno + '\'' +
                ", status=" + status +
                ", createdBy='" + createdBy + '\'' +
                ", createdAt=" + createdAt +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedAt=" + updatedAt +
                ", orderinguserId=" + orderinguserId +
                ", isdefault=" + isdefault +
                ", isdeleted=" + isdeleted +
                ", dtsSyncSource='" + dtsSyncSource + '\'' +
                ", dtsSyncUid='" + dtsSyncUid + '\'' +
                '}';
    }
}