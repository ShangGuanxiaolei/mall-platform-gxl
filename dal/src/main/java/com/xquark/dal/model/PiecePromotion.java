package com.xquark.dal.model;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 */
public class PiecePromotion extends Promotion {

  private boolean isNew;

  public boolean getIsNew() {
    return isNew;
  }

  public void setIsNew(boolean aNew) {
    isNew = aNew;
  }
}
