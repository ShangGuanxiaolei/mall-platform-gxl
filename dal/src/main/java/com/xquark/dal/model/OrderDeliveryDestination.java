package com.xquark.dal.model;

import java.lang.String;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.SkuType;
import java.util.Objects;

/**
 * 订单投递的目的地
 */
public class OrderDeliveryDestination {

  /**
   * 投递点名称
   */
  private String name;

  /**
   * 商品来源于哪个合作方
   */
  private SkuType skuCodeResourcesType;

  /**
   * 合作方ERP系统
   */
  private String dest;

  /**
   * 订单投递的仓库
   */
  private String warehouseName;

  /**
   * 订单投递的店铺
   */
  private String shopName;

  private ProductSource source; // 商品来源

  public OrderDeliveryDestination(String name, SkuType skuCodeResourcesType,
                                  String dest, String wareHouseName, String shopName) {
    this.name = name;
    this.skuCodeResourcesType = skuCodeResourcesType;
    this.dest = dest;
    this.warehouseName = wareHouseName;
    this.shopName = shopName;

    if (skuCodeResourcesType.getRes().equals("SF")) {
      source = ProductSource.SF;
    } else if (skuCodeResourcesType.getRes().equals("CLIENT")) {
      source = ProductSource.CLIENT;
    }
  }


  public SkuType getSkuCodeResourcesType() {
    return skuCodeResourcesType;
  }

  public void setSkuCodeResourcesType(SkuType skuCodeResourcesType) {
    this.skuCodeResourcesType = skuCodeResourcesType;
  }


  public String getWarehouseName() {
    return warehouseName;
  }

  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDest() {
    return dest;
  }

  public void setDest(String dest) {
    this.dest = new String(dest);
  }

  public ProductSource getSource() {
    return source;
  }

  public void setSource(ProductSource source) {
    this.source = source;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof OrderDeliveryDestination)) {
      return false;
    }
    OrderDeliveryDestination that = (OrderDeliveryDestination) o;
    return Objects.equals(name, that.name) &&
        skuCodeResourcesType == that.skuCodeResourcesType &&
        dest == that.dest &&
        Objects.equals(warehouseName, that.warehouseName) &&
        Objects.equals(shopName, that.shopName) &&
        source == that.source;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, skuCodeResourcesType, dest, warehouseName, shopName, source);
  }

  @Override
  public String toString() {
    return "OrderDeliveryDestination{" +
            "name='" + name + '\'' +
            ", skuCodeResourcesType=" + skuCodeResourcesType +
            ", dest=" + dest +
            ", warehouseName='" + warehouseName + '\'' +
            ", shopName='" + shopName + '\'' +
            ", source=" + source +
            '}';
  }
}
