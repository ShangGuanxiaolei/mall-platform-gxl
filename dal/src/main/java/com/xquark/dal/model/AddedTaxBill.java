package com.xquark.dal.model;

import org.hibernate.validator.constraints.NotBlank;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午4:09
 * 公司增值税发票
 */
public class AddedTaxBill extends  CompanyBill {

    /**
     * 授权人委托书
     */
    @NotBlank(groups = {AddedTaxBill.class})
    private String attorneyImg;

    /***
     * 公司电话
     */
    @NotBlank(groups = {AddedTaxBill.class})
    private String companyPhone;

    /***
     * 公司地址
     */
    @NotBlank(groups = {AddedTaxBill.class})
    private String companyAddress;

    /**
     * 开户银行
     */
    @NotBlank(groups = {AddedTaxBill.class})
    private String bank;

    /**
     * 银行账号
     */
    @NotBlank(groups = {AddedTaxBill.class})
    private String bankAccount;

    public String getAttorneyImg() {
        return attorneyImg;
    }

    public void setAttorneyImg(String attorneyImg) {
        this.attorneyImg = attorneyImg;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public String toString() {
        return super.toString()+"AddedTaxBill{" +
                "attorneyImg='" + attorneyImg + '\'' +
                ", companyPhone='" + companyPhone + '\'' +
                ", companyAddress='" + companyAddress + '\'' +
                ", bank='" + bank + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                '}';
    }
}
