package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.List;

/**
 * User: kong Date: 18-6-9. Time: 上午9:38 WMS产品库存表
 */
public class WmsInventory extends BaseEntityImpl implements Archivable {

  //仓库编号
  private String warehouseCode;
  //批次编号
  private String batchId;
  //产品货号
  private String code;
  //库存数
  private Long amount;
  //edi状态
  private Integer ediStatus;
  private String ids;
  //逻辑删除
  private Boolean archive;

  public String getWarehouseCode() {
    return warehouseCode;
  }

  public void setWarehouseCode(String warehouseCode) {
    this.warehouseCode = warehouseCode;
  }

  public String getBatchId() {
    return batchId;
  }

  public void setBatchId(String batchId) {
    this.batchId = batchId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  public String getIds() {
    return ids;
  }

  public void setIds(String ids) {
    this.ids = ids;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "WmsInventory{" +
        "warehouseCode='" + warehouseCode + '\'' +
        ", batchId='" + batchId + '\'' +
        ", code='" + code + '\'' +
        ", amount=" + amount +
        ", ediStatus=" + ediStatus +
        ", archive=" + archive +
        '}';
  }
}
