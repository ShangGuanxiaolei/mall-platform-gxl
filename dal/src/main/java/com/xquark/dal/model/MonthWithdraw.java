package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * 月收益 Created by huangjianbo on 2016/12/13.
 */
public class MonthWithdraw {

  private String month;
  private BigDecimal withdraw;

  public String getDate() {
    return month;
  }

  public void setDate(String date) {
    this.month = date;
  }

  public BigDecimal getWithdraw() {
    return withdraw;
  }

  public void setWithdraw(BigDecimal withdraw) {
    this.withdraw = withdraw;
  }
}
