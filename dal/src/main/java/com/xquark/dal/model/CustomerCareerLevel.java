package com.xquark.dal.model;

import com.xquark.dal.type.CareerLevelType;
import java.io.Serializable;
import java.util.Date;

/**
 * customerCareerLevel
 *
 * @author wangxinhua
 */
public class CustomerCareerLevel implements Serializable {

  /**
   * 资格号
   */
  private Long cpId;
  private String orgGroupQua;

    public String getOrgGroupQua() {
        return orgGroupQua;
    }

    public void setOrgGroupQua(String orgGroupQua) {
        this.orgGroupQua = orgGroupQua;
    }

    /**
   * 身份类型
   */
  private String hdsType;

  /**
   * 子类型
   */
  private String hdsSubType;

  /**
   * 经销商类型
   */
  private String hdsSpType;

  /**
   * 特级服务商星级
   */
  private String hdsStar;

  /**
   * 身份类型
   */
  private String viviLifeType;

  /**
   * 子类型
   */
  private String viviLifeSubType;

  /**
   * 经销商类型
   */
  private String viviLifeSpType;

  /**
   * 0 为不点亮，1为点亮
   */
  private Boolean isLightening = false;

  /**
   * 点亮时间
   */
  private Date lightenDate;

  /**
   * 时间
   */
  private Date createdDate;

  /**
   * 更新时间
   */
  private Date updateDate;

  private static final long serialVersionUID = 1L;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getHdsType() {
    return hdsType;
  }

  public void setHdsType(String hdsType) {
    this.hdsType = hdsType;
  }

  public String getHdsSubType() {
    return hdsSubType;
  }

  public void setHdsSubType(String hdsSubType) {
    this.hdsSubType = hdsSubType;
  }

  public String getHdsSpType() {
    return hdsSpType;
  }

  public void setHdsSpType(String hdsSpType) {
    this.hdsSpType = hdsSpType;
  }

  public String getHdsStar() {
    return hdsStar;
  }

  public void setHdsStar(String hdsStar) {
    this.hdsStar = hdsStar;
  }

  public String getViviLifeType() {
    return viviLifeType;
  }

  public void setViviLifeType(String viviLifeType) {
    this.viviLifeType = viviLifeType;
  }

  public String getViviLifeSubType() {
    return viviLifeSubType;
  }

  public void setViviLifeSubType(String viviLifeSubType) {
    this.viviLifeSubType = viviLifeSubType;
  }

  public String getViviLifeSpType() {
    return viviLifeSpType;
  }

  public void setViviLifeSpType(String viviLifeSpType) {
    this.viviLifeSpType = viviLifeSpType;
  }

  public Boolean getIsLightening() {
    return isLightening;
  }

  public void setIsLightening(Boolean isLightening) {
    this.isLightening = isLightening;
  }

  public Date getLightenDate() {
    return lightenDate;
  }

  public void setLightenDate(Date lightenDate) {
    this.lightenDate = lightenDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    CustomerCareerLevel other = (CustomerCareerLevel) that;
    return
        (this.getCpId() == null ? other.getCpId() == null : this.getCpId().equals(other.getCpId()))
            && (this.getHdsType() == null ? other.getHdsType() == null
            : this.getHdsType().equals(other.getHdsType()))
            && (this.getHdsSubType() == null ? other.getHdsSubType() == null
            : this.getHdsSubType().equals(other.getHdsSubType()))
            && (this.getHdsSpType() == null ? other.getHdsSpType() == null
            : this.getHdsSpType().equals(other.getHdsSpType()))
            && (this.getHdsStar() == null ? other.getHdsStar() == null
            : this.getHdsStar().equals(other.getHdsStar()))
            && (this.getViviLifeType() == null ? other.getViviLifeType() == null
            : this.getViviLifeType().equals(other.getViviLifeType()))
            && (this.getViviLifeSubType() == null ? other.getViviLifeSubType() == null
            : this.getViviLifeSubType().equals(other.getViviLifeSubType()))
            && (this.getViviLifeSpType() == null ? other.getViviLifeSpType() == null
            : this.getViviLifeSpType().equals(other.getViviLifeSpType()))
            && (this.getIsLightening() == null ? other.getIsLightening() == null
            : this.getIsLightening().equals(other.getIsLightening()))
            && (this.getLightenDate() == null ? other.getLightenDate() == null
            : this.getLightenDate().equals(other.getLightenDate()))
            && (this.getCreatedDate() == null ? other.getCreatedDate() == null
            : this.getCreatedDate().equals(other.getCreatedDate()))
            && (this.getUpdateDate() == null ? other.getUpdateDate() == null
            : this.getUpdateDate().equals(other.getUpdateDate()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getCpId() == null) ? 0 : getCpId().hashCode());
    result = prime * result + ((getHdsType() == null) ? 0 : getHdsType().hashCode());
    result = prime * result + ((getHdsSubType() == null) ? 0 : getHdsSubType().hashCode());
    result = prime * result + ((getHdsSpType() == null) ? 0 : getHdsSpType().hashCode());
    result = prime * result + ((getHdsStar() == null) ? 0 : getHdsStar().hashCode());
    result = prime * result + ((getViviLifeType() == null) ? 0 : getViviLifeType().hashCode());
    result =
        prime * result + ((getViviLifeSubType() == null) ? 0 : getViviLifeSubType().hashCode());
    result = prime * result + ((getViviLifeSpType() == null) ? 0 : getViviLifeSpType().hashCode());
    result = prime * result + ((getIsLightening() == null) ? 0 : getIsLightening().hashCode());
    result = prime * result + ((getLightenDate() == null) ? 0 : getLightenDate().hashCode());
    result = prime * result + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
    result = prime * result + ((getUpdateDate() == null) ? 0 : getUpdateDate().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", cpId=").append(cpId);
    sb.append(", hdsType=").append(hdsType);
    sb.append(", hdsSubType=").append(hdsSubType);
    sb.append(", hdsSpType=").append(hdsSpType);
    sb.append(", hdsStar=").append(hdsStar);
    sb.append(", viviLifeType=").append(viviLifeType);
    sb.append(", viviLifeSubType=").append(viviLifeSubType);
    sb.append(", viviLifeSpType=").append(viviLifeSpType);
    sb.append(", isLightening=").append(isLightening);
    sb.append(", lightenDate=").append(lightenDate);
    sb.append(", createdDate=").append(createdDate);
    sb.append(", updateDate=").append(updateDate);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }

  public CareerLevelType getFinalType() {
    return CareerLevelType.getFinalLevelType(this.getHdsType(), this.getViviLifeType());
  }
}