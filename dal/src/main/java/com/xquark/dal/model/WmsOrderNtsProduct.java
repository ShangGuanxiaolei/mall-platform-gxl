package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.mybatis.IdTypeHandler;
import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;

/**
 * User: kong Date: 18-6-9. Time: 上午10:11 WMS发货通知单产品表
 */
public class WmsOrderNtsProduct extends BaseEntityImpl implements Archivable {

  //订单号
  private String orderNo;
  //子单号
  private String subOrderNo;

  // skuId 非同步字段
  private Long skuId;

  // sku描述
  private String skuSpec;

  // 商品id 非同步字段
  private Long productId;

  //库存编码
  private String stockSku;
  //产品名称
  private String productName;
  //产品类别
  private String type;
  //订购数量
  private Long amount;
  //销售单价
  private BigDecimal marketPrice;
  //折扣
  private BigDecimal discount;
  //套装编号
  private String combinationNo;
  //套装名称
  private String combinationName;
  //套装数量
  private Long combinationNum;
  //是否在订单上显示
  private Integer showCombination;
  //是否为套装
  private Integer isCombination;
  //发货说明
  private String remark;
  //EDI状态
  private Integer ediStatus;
  //逻辑删除
  private Boolean archive;

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getSubOrderNo() {
    return subOrderNo;
  }

  public void setSubOrderNo(String subOrderNo) {
    this.subOrderNo = subOrderNo;
  }

  public String getSkuId() {
    if (skuId == null) {
      return null;
    }
    return IdTypeHandler.encode(skuId);
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getSkuSpec() {
    return skuSpec;
  }

  public void setSkuSpec(String skuSpec) {
    this.skuSpec = skuSpec;
  }

  public String getProductId() {
    if (productId == null) {
      return null;
    }
    return IdTypeHandler.encode(productId);
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getStockSku() {
    return stockSku;
  }

  public void setStockSku(String stockSku) {
    this.stockSku = stockSku;
  }

  public String getName() {
    if (StringUtils.isNotBlank(skuSpec) && !StringUtils.equals("无", skuSpec)) {
      return productName + "[" + skuSpec + "]";
    }
    return productName;
  }

  public void setName(String name) {
    this.productName = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getQty() {
    return amount;
  }

  public void setQty(Long qty) {
    this.amount = qty;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public BigDecimal getPrice() {
    return discount;
  }

  public void setPrice(BigDecimal price) {
    this.discount = price;
  }

  public String getSetNo() {
    return combinationNo;
  }

  public void setSetNo(String setNo) {
    this.combinationNo = setNo;
  }

  public String getSetName() {
    return combinationName;
  }

  public void setSetName(String setName) {
    this.combinationName = setName;
  }

  public Long getSetQty() {
    return combinationNum;
  }

  public void setSetQty(Long setQty) {
    this.combinationNum = setQty;
  }

  public Integer getShowSet() {
    return showCombination;
  }

  public void setShowSet(Integer showSet) {
    this.showCombination = showSet;
  }

  public Integer getIsSet() {
    return isCombination;
  }

  public void setIsSet(Integer isSet) {
    this.isCombination = isSet;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public String getCombinationNo() {
    return combinationNo;
  }

  public void setCombinationNo(String combinationNo) {
    this.combinationNo = combinationNo;
  }

  public String getCombinationName() {
    return combinationName;
  }

  public void setCombinationName(String combinationName) {
    this.combinationName = combinationName;
  }

  public Long getCombinationNum() {
    return combinationNum;
  }

  public void setCombinationNum(Long combinationNum) {
    this.combinationNum = combinationNum;
  }

  public Integer getShowCombination() {
    return showCombination;
  }

  public void setShowCombination(Integer showCombination) {
    this.showCombination = showCombination;
  }

  public Integer getIsCombination() {
    return isCombination;
  }

  public void setIsCombination(Integer isCombination) {
    this.isCombination = isCombination;
  }

  @Override
  public String toString() {
    return "WmsOrderNtsProduct{" +
        "orderNo='" + orderNo + '\'' +
        ", subOrderNo='" + subOrderNo + '\'' +
        ", stockSku=" + stockSku +
        ", name='" + productName + '\'' +
        ", type='" + type + '\'' +
        ", qty=" + amount +
        ", marketPrice=" + marketPrice +
        ", discount=" + discount +
        ", setNo='" + combinationNo + '\'' +
        ", setName='" + combinationName + '\'' +
        ", setQty=" + combinationNum +
        ", showSet=" + showCombination +
        ", isSet=" + isCombination +
        ", remark='" + remark + '\'' +
        ", ediStatus=" + ediStatus +
        ", archive=" + archive +
        '}';
  }
}
