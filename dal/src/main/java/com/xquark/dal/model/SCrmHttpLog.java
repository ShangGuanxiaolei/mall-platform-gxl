package com.xquark.dal.model;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.utils.http.po.SCrmParam;
import java.util.Date;
import org.apache.http.HttpStatus;

/**
 * @author wangxinhua
 */
public class SCrmHttpLog extends BaseEntityImpl implements Archivable {

  /**
   * 请求失败日志对象
   */
  public static SCrmHttpLog httpErrorLog(String url, SCrmParam params, String error,
      int errorCode) {
    return new Builder()
        .withUrl(url)
        .withParams(JSON.toJSONString(params))
        .withError(error)
        .withIsSuccess(false)
        .withStatusCode(errorCode)
        .withResult("")
        .build();
  }

  /**
   * 异常日志对象
   */
  public static SCrmHttpLog exceptionLog(String url, SCrmParam params, Throwable e) {
    return new Builder()
        .withUrl(url)
        .withParams(JSON.toJSONString(params))
        .withError("本地回调错误" + e.getMessage())
        .withIsSuccess(false)
        .withStatusCode(HttpStatus.SC_OK)
        .withResult("")
        .build();
  }

  /**
   * 返回结果业务异常
   */
  public static SCrmHttpLog bizLog(String url, SCrmParam params, String error) {
    return new Builder()
        .withUrl(url)
        .withParams(JSON.toJSONString(params))
        .withError(error)
        .withIsSuccess(false)
        .withStatusCode(HttpStatus.SC_OK)
        .withResult("")
        .build();
  }

  /**
   * 成功日志对象
   */
  public static SCrmHttpLog successLog(String url, SCrmParam params, Object result) {
    return new Builder()
        .withUrl(url)
        .withParams(JSON.toJSONString(params))
        .withError("")
        .withIsSuccess(true)
        .withResult(JSON.toJSONString(result))
        .withStatusCode(HttpStatus.SC_OK)
        .build();
  }

  /**
   * 默认构造器, 供框架初始化
   */
  public SCrmHttpLog() {
  }

  private String id;

  /**
   * 调用地址
   */
  private String url;

  /**
   * 调用参数
   */
  private String params;

  /**
   * 是否成功
   */
  private Boolean isSuccess;

  /**
   * 错误信息
   */
  private String error;

  /**
   * http状态码
   */
  private int statusCode;

  /**
   * 返回结果
   */
  private String result;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  public Boolean getIsSuccess() {
    return isSuccess;
  }

  public void setIsSuccess(Boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  public static final class Builder {

    private SCrmHttpLog sCrmHttpLog;

    private Builder() {
      sCrmHttpLog = new SCrmHttpLog();
    }

    public static Builder aSCrmHttpLog() {
      return new Builder();
    }

    public Builder withUrl(String url) {
      sCrmHttpLog.setUrl(url);
      return this;
    }

    public Builder withParams(String params) {
      sCrmHttpLog.setParams(params);
      return this;
    }

    public Builder withIsSuccess(Boolean isSuccess) {
      sCrmHttpLog.setIsSuccess(isSuccess);
      return this;
    }

    public Builder withError(String error) {
      sCrmHttpLog.setError(error);
      return this;
    }

    public Builder withStatusCode(int statusCode) {
      sCrmHttpLog.setStatusCode(statusCode);
      return this;
    }

    public Builder withResult(String result) {
      sCrmHttpLog.setResult(result);
      return this;
    }

    public SCrmHttpLog build() {
      return sCrmHttpLog;
    }
  }
}