package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * 订单信息
 * @author tanggb
 * @date 2019/03/09 14:46
 */
public class FreshManOrderData {
    private BigDecimal price;
    private BigDecimal discount;
    private BigDecimal reduction;
    private Integer amount;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}