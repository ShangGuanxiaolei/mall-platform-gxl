package com.xquark.dal.model;

import java.math.BigDecimal;

import com.xquark.dal.BaseEntityImpl;

/**
 * 临时存储表
 *
 * @author ZHUYIN
 */
public class OrderRefundRecord extends BaseEntityImpl {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private String batch_no;
  private String order_no;
  private String trade_no;
  private String pay_no;
  private String status;
  private BigDecimal refund_fee;
  private String source;

  public String getBatch_no() {
    return batch_no;
  }

  public void setBatch_no(String batch_no) {
    this.batch_no = batch_no;
  }

  public String getOrder_no() {
    return order_no;
  }

  public void setOrder_no(String order_no) {
    this.order_no = order_no;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getTrade_no() {
    return trade_no;
  }

  public void setTrade_no(String trade_no) {
    this.trade_no = trade_no;
  }

  public String getPay_no() {
    return pay_no;
  }

  public void setPay_no(String pay_no) {
    this.pay_no = pay_no;
  }

  public BigDecimal getRefund_fee() {
    return refund_fee;
  }

  public void setRefund_fee(BigDecimal refund_fee) {
    this.refund_fee = refund_fee;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }


}
