package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.math.BigInteger;

public class SkuExtend extends Sku {

  private static final long serialVersionUID = 1L;

  /**
   * 是否有赠品
   */
  boolean hadGift;

  String giftType;

  public String getGiftType() {
    return giftType;
  }

  public void setGiftType(String giftType) {
    this.giftType = giftType;
  }

  /**
   * 赠品数量
   */
  int giftCount;

  public boolean isHadGift() {
    return hadGift;
  }

  public void setHadGift(boolean hadGift) {
    this.hadGift = hadGift;
  }

  public int getGiftCount() {
    return giftCount;
  }

  public void setGiftCount(int giftCount) {
    this.giftCount = giftCount;
  }
}
