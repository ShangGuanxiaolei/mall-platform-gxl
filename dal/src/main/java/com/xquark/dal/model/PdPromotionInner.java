package com.xquark.dal.model;

/**
 * @author wangxinhua on 2018/5/25. DESC:
 */
public class PdPromotionInner {

  private final String productId;

  private final String promotionId;

  public PdPromotionInner(String productId, String promotionId) {
    this.productId = productId;
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public String getPromotionId() {
    return promotionId;
  }
}
