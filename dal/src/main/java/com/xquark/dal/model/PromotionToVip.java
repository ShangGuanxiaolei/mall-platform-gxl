package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.util.Date;

/**
 * @author gxl
 */
public class PromotionToVip extends BaseEntityArchivableImpl {

    /**
     * 标题
     */
    private String title;

    /**
     * 升级vip描述
     */
    private String describes;

    /**
     * 状态 0 - 停用, 1 - 启用
     */
    private Boolean state;

    /**
     * 转变为vip所需的次数
     */
    private Integer toNumber;

    /**
     * 活动类型
     */
    private String type;

    /**
     * 更新人
     */
    private String updater;

    /**
     * 有效期开始时间
     */
    private Date effectiveBegin;

    /**
     *  有效期结束时间
     */
    private Date effectiveEnd;

    private static final long serialVersionUID = 1L;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Integer getToNumber() {
        return toNumber;
    }

    public void setToNumber(Integer toNumber) {
        this.toNumber = toNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Date getEffectiveBegin() {
        return effectiveBegin;
    }

    public void setEffectiveBegin(Date effectiveBegin) {
        this.effectiveBegin = effectiveBegin;
    }

    public Date getEffectiveEnd() {
        return effectiveEnd;
    }

    public void setEffectiveEnd(Date effectiveEnd) {
        this.effectiveEnd = effectiveEnd;
    }
}