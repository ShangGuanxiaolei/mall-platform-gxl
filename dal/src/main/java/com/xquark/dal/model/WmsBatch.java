package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * User: kong Date: 18-6-9. Time: 上午9:46 WMS批次表
 */
public class WmsBatch extends BaseEntityImpl implements Archivable {

  //批次编号
  private String batchNo;
  //3PL标识
  private String tplStatus;
  //HDS标识
  private String hdsStatus;
  //edi状态
  private Integer ediStatus;
  //逻辑删除
  private Boolean archive;

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public String getTplStatus() {
    return tplStatus;
  }

  public void setTplStatus(String tplStatus) {
    this.tplStatus = tplStatus;
  }

  public String getDsStatus() {
    return hdsStatus;
  }

  public void setDsStatus(String dsStatus) {
    this.hdsStatus = dsStatus;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "wmsBatch{" +
        "batchNo='" + batchNo + '\'' +
        ", tplStatus='" + tplStatus + '\'' +
        ", dsStatus='" + hdsStatus + '\'' +
        ", ediStatus=" + ediStatus +
        ", archive=" + archive +
        '}';
  }
}
