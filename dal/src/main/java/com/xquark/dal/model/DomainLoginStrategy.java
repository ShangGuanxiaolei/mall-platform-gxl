package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * @author: chenxi
 */

public class DomainLoginStrategy extends BaseEntityImpl {

  /**
   *
   */
  private static final long serialVersionUID = -4646943303612474750L;

  private String domainId;
  private String profile; // dev
  private String moduleName; // xquark-web
  private String domainName; // 51shop.mobi
  private String loginStrategyType; // customized-ref
  private String loginStrategyValue; // xiangquLogin

  public String getDomainId() {
    return domainId;
  }

  public void setDomainId(String domainId) {
    this.domainId = domainId;
  }

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public String getLoginStrategyType() {
    return loginStrategyType;
  }

  public void setLoginStrategyType(String loginStrategyType) {
    this.loginStrategyType = loginStrategyType;
  }

  public String getLoginStrategyValue() {
    return loginStrategyValue;
  }

  public void setLoginStrategyValue(String loginStrategyValue) {
    this.loginStrategyValue = loginStrategyValue;
  }

}
