package com.xquark.dal.model;

/**
 * Created by wangxinhua on 17-11-16. DESC: 标识一个对象可以被索引，通过score来比较排序
 */
public interface IndexAble {

  Integer getScore();

}
