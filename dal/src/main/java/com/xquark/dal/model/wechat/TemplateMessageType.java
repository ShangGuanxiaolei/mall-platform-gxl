package com.xquark.dal.model.wechat;

/**
 * 自定义回复内容的类型
 */
public enum TemplateMessageType {

  financeEvent("OPENTM207453441"), // 资金变更通知
  pointEvent("TM00335"), // 积分变动通知
  orderEvent("OPENTM205109409"), // 订单通知
  orderDeliveryEvent("OPENTM200565259"), // 订单发货通知
  refundEvent("TM00431"), // 退款通知
  distributorApplicationEvent("OPENTM203787411"), // 分销商申请通知
  commissionEvent("OPENTM201812627"), // 佣金通知
  productDetailEvent("OPENTM207331564"), // 商详通知
  newPartnerEvent("OPENTM400045760"), // 新合伙人通知
  partnerObtainCommissionEvent("OPENTM207568117"); // 合伙人获得佣金通知

  private String templateMessageNo;

  TemplateMessageType(String templateMessageNo) {
    this.templateMessageNo = templateMessageNo;
  }

  public String getTemplateMessageNo() {
    return templateMessageNo;
  }

  public static TemplateMessageType find(String templateMessageNo) {

    if (financeEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return financeEvent;
    } else if (pointEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return pointEvent;
    } else if (orderEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return orderEvent;
    } else if (orderDeliveryEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return orderDeliveryEvent;
    } else if (refundEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return refundEvent;
    } else if (distributorApplicationEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return distributorApplicationEvent;
    } else if (commissionEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return commissionEvent;
    } else if (productDetailEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return productDetailEvent;
    } else if (newPartnerEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return newPartnerEvent;
    } else if (partnerObtainCommissionEvent.getTemplateMessageNo().equals(templateMessageNo)) {
      return partnerObtainCommissionEvent;
    } else {
      throw new RuntimeException("未找到对应的自定义回复内容NO");
    }
  }
}
