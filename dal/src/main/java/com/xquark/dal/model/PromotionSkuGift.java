package com.xquark.dal.model;

import java.util.Date;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class PromotionSkuGift extends BaseEntityImpl implements Archivable{

	private static final long serialVersionUID = 1L;
	
	private String pCode ;
	private String mainSkuCode;
	private String giftSkuCode;
	private String mainProductId;
	private String giftProductId;
	private Date createdAt;
	private Date updatedAt;
	
	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getMainSkuCode() {
		return mainSkuCode;
	}

	public void setMainSkuCode(String mainSkuCode) {
		this.mainSkuCode = mainSkuCode;
	}

	public String getGiftSkuCode() {
		return giftSkuCode;
	}

	public void setGiftSkuCode(String giftSkuCode) {
		this.giftSkuCode = giftSkuCode;
	}

	public String getMainProductId() {
		return mainProductId;
	}

	public void setMainProductId(String mainProductId) {
		this.mainProductId = mainProductId;
	}

	public String getGiftProductId() {
		return giftProductId;
	}

	public void setGiftProductId(String giftProductId) {
		this.giftProductId = giftProductId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	@Override
	public Boolean getArchive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArchive(Boolean archive) {
		// TODO Auto-generated method stub
		
	}

}
