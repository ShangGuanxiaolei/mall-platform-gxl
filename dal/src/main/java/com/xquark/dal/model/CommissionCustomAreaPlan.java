package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class CommissionCustomAreaPlan extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name;

  private String ownShopId;

  private String areaId;

  private double areaRate;

  private String areaPlanId;

  private Boolean defaultStatus;

  private Boolean archive;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(String ownShopId) {
    this.ownShopId = ownShopId;
  }

  public String getAreaId() {
    return areaId;
  }

  public void setAreaId(String areaId) {
    this.areaId = areaId;
  }

  public double getAreaRate() {
    return areaRate;
  }

  public void setAreaRate(double areaRate) {
    this.areaRate = areaRate;
  }

  public String getAreaPlanId() {
    return areaPlanId;
  }

  public void setAreaPlanId(String areaPlanId) {
    this.areaPlanId = areaPlanId;
  }

  public Boolean getDefaultStatus() {
    return defaultStatus;
  }

  public void setDefaultStatus(Boolean defaultStatus) {
    this.defaultStatus = defaultStatus;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}