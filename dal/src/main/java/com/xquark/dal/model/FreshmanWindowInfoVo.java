package com.xquark.dal.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 新人弹窗 pojo
 */
public class FreshmanWindowInfoVo implements Serializable {

    private static final long serialVersionUID = 8005829580366362946L;

    private int type; // 2.首单完成弹窗，3.消费满200弹窗，4.VIP弹窗, 5.满500弹窗

    private BigDecimal amount; //再消费金额

    private int point; //发放德分数量

    private String effectTime; //VIP生效时间

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime;
    }
}
