package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Optional;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;

public class OrderItem extends BaseEntityImpl implements DynamicPricingItem {

  private static final long serialVersionUID = 1L;
  private String orderId; // 所属订单
  private String productId; // 商品
  private String proLiteralId; // 商品展示id，用于识别代销商品
  private String skuId;
  private String productName;
  private String skuStr;
  private String productImg;
  private BigDecimal price;
  private BigDecimal marketPrice;
  private BigDecimal discount; // item折扣
  private BigDecimal discountPrice; // item折后价
  private PromotionType promotionType; //使用的优惠类型
  private Integer amount;
  private Boolean special;
  private String promotionId;

  private String AddressDetails;

  private Date succeedAt;// 订单成功时间

  private Date shippedAt; // 订单发货时间

  private String logisticsCompany; // 物流公司
  private String logisticsOrderNo; // 物流公司订单号

  private Product product;

  private Sku sku;

  // 是否已经评价过
  private boolean isComment;

  private BigDecimal serverAmt; // 服务费
  private BigDecimal promoAmt; // 推广费

  private BigDecimal paidPoint = BigDecimal.ZERO;
  private BigDecimal paidPointPacket = BigDecimal.ZERO;

  private BigDecimal paidCommission = BigDecimal.ZERO;

  // 立减
  private BigDecimal reduction = BigDecimal.ZERO;
  //是否新人专区商品
  private boolean freshmanProduct;

  public boolean getFreshmanProduct() {
    return freshmanProduct;
  }

  public void setFreshmanProduct(boolean freshmanProduct) {
    this.freshmanProduct = freshmanProduct;
  }

  private int selfOperated;//自营标签

  private Date promotionValidFrom;

  private Date promotionValidTo;

  private Date promotionPayFrom;

  private Date promotionPayTo;

  private Boolean canPay = true;

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public boolean getIsComment() {
    return isComment;
  }

  public void setIsComment(boolean comment) {
    isComment = comment;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getAddressDetails() {
    return AddressDetails;
  }

  public void setAddressDetails(String addressDetails) {
    AddressDetails = addressDetails;
  }

  public Date getSucceedAt() {
    return succeedAt;
  }

  public void setSucceedAt(Date succeedAt) {
    this.succeedAt = succeedAt;
  }

  public Date getShippedAt() {
    return shippedAt;
  }

  public void setShippedAt(Date shippedAt) {
    this.shippedAt = shippedAt;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImgUrl;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public BigDecimal getPrice() {
    return price;
  }

  @Override
  public DynamicPricing getDynamicPricing() {
    return sku;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getProductImgUrl() {
    return productImg;
  }

  public void setProductImgUrl(String productImgUrl) {
    this.productImgUrl = productImgUrl;
  }

  public String getTitle() {
    return this.productName;
  }

  public Boolean getSpecial() {
    return special;
  }

  public void setSpecial(Boolean special) {
    this.special = special;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProLiteralId() {
    return proLiteralId;
  }

  public void setProLiteralId(String proLiteralId) {
    this.proLiteralId = proLiteralId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public BigDecimal getDiscountPrice() {
    return discountPrice;
  }

  /**
   * 总的折扣最终金额
   */
  public BigDecimal getTotalDiscountPrice() {
    BigDecimal amount = BigDecimal.valueOf(Optional.fromNullable(getAmount()).or(1));
    BigDecimal discountPrice = Optional.fromNullable(getDiscountPrice()).or(getPrice());
    return discountPrice.multiply(amount);
  }

  public void setDiscountPrice(BigDecimal discountPrice) {
    this.discountPrice = discountPrice;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  public void setPromotionType(PromotionType promotionType) {
    this.promotionType = promotionType;
  }

  public BigDecimal getPaidPoint() {
    return paidPoint;
  }

  public void setPaidPoint(BigDecimal paidPoint) {
    this.paidPoint = paidPoint;
  }

  public BigDecimal getPaidPointPacket() {
    return paidPointPacket;
  }

  public void setPaidPointPacket(BigDecimal paidPointPacket) {
    this.paidPointPacket = paidPointPacket;
  }

  public BigDecimal getPaidCommission() {
    return paidCommission;
  }

  public void setPaidCommission(BigDecimal paidCommission) {
    this.paidCommission = paidCommission;
  }

  public Sku getSku() {
    return sku;
  }

  public void setSku(Sku sku) {
    this.sku = sku;
  }

  public BigDecimal getReduction() {
    return reduction;
  }

  public void setReduction(BigDecimal reduction) {
    this.reduction = reduction;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }


  public Date getPromotionValidFrom() {
    return promotionValidFrom;
  }

  public void setPromotionValidFrom(Date promotionValidFrom) {
    this.promotionValidFrom = promotionValidFrom;
  }

  public Date getPromotionValidTo() {
    return promotionValidTo;
  }

  public void setPromotionValidTo(Date promotionValidTo) {
    this.promotionValidTo = promotionValidTo;
  }

  public Date getPromotionPayFrom() {
    return promotionPayFrom;
  }

  public void setPromotionPayFrom(Date promotionPayFrom) {
    this.promotionPayFrom = promotionPayFrom;
  }

  public Date getPromotionPayTo() {
    return promotionPayTo;
  }

  public void setPromotionPayTo(Date promotionPayTo) {
    this.promotionPayTo = promotionPayTo;
  }

  public Boolean getCanPay() {
    return canPay;
  }

  public void setCanPay(Boolean canPay) {
    this.canPay = canPay;
  }

}