package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.YundouCategoryType;


public class YundouProduct extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String shopId;

  private String productId;

  private Boolean archive;

  private int level;

  private YundouCategoryType category;

  private Boolean isForce;

  private int canDeduction;

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public Boolean getIsForce() {
    return isForce;
  }

  public void setIsForce(Boolean force) {
    isForce = force;
  }

  public int getCanDeduction() {
    return canDeduction;
  }

  public void setCanDeduction(int canDeduction) {
    this.canDeduction = canDeduction;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public YundouCategoryType getCategory() {
    return category;
  }

  public void setCategory(YundouCategoryType category) {
    this.category = category;
  }
}