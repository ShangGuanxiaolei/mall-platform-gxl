package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.ShopType;

import java.math.BigDecimal;
import java.util.Date;

public class Shop extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerId; // 店铺拥有者
  private String wechat; // 微信号
  private String img; // 店铺主图
  private String name; // 店铺名称
  private String banner; // 店铺招牌
  private String bannerUrl;
  private String description; // 店铺说明
  private Long provinceId; // 店铺所在省份
  private Long cityId; // 店铺所在城市
  private ShopStatus status; // 店铺状态
  private BigDecimal commisionRate; // 佣金比例

  private String bulletin; // 店铺公告
  private Date bulletinAt; //公告日期

  private Boolean danbao; //是否开通担保交易，默认为不开通
  private Boolean archive; // 是否是逻辑删除
  private Boolean fragmentStatus; //是否使用片段，true:使用、false:不使用，默认false

  private String activityId;

  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  // 字段废弃 TODO---改为  ShopPostAge 存储邮费信息
  private Boolean postageStatus; // 是否设置了邮费
  private BigDecimal postage;   // 邮费
  private String freeZone; // 免邮地区
  private Long freeZoneId;

  private ShopType shopType;

  private String servicePhone;

  public String getBannerUrl() {
    return bannerUrl;
  }

  public void setBannerUrl(String bannerUrl) {
    this.bannerUrl = bannerUrl;
  }

  public String getServicePhone() {
    return servicePhone;
  }

  public void setServicePhone(String servicePhone) {
    this.servicePhone = servicePhone;
  }

  public ShopType getShopType() {
    return shopType;
  }

  public void setShopType(ShopType shopType) {
    this.shopType = shopType;
  }

  // 缺少字段：库存提醒
  public Long getFreeZoneId() {
    return freeZoneId;
  }

  public void setFreeZoneId(Long freeZoneId) {
    this.freeZoneId = freeZoneId;
  }

  public String getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(String ownerId) {
    this.ownerId = ownerId;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBulletin() {
    return bulletin;
  }

  public void setBulletin(String bulletin) {
    this.bulletin = bulletin;
  }

  public Date getBulletinAt() {
    return bulletinAt;
  }

  public void setBulletinAt(Date bulletinAt) {
    this.bulletinAt = bulletinAt;
  }

  public ShopStatus getStatus() {
    return status;
  }

  public void setStatus(ShopStatus status) {
    this.status = status;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }

  public String getBanner() {
    return banner;
  }

  public void setBanner(String banner) {
    this.banner = banner;
  }

  public BigDecimal getCommisionRate() {
    return commisionRate;
  }

  public void setCommisionRate(BigDecimal commisionRate) {
    this.commisionRate = commisionRate;
  }

  public Boolean getDanbao() {
    return danbao;
  }

  public void setDanbao(Boolean danbao) {
    this.danbao = danbao;
  }

  public Long getProvinceId() {
    return provinceId;
  }

  public void setProvinceId(Long provinceId) {
    this.provinceId = provinceId;
  }

  public Long getCityId() {
    return cityId;
  }

  public void setCityId(Long cityId) {
    this.cityId = cityId;
  }

  public Boolean getPostageStatus() {
    return postageStatus;
  }

  public void setPostageStatus(Boolean postageStatus) {
    this.postageStatus = postageStatus;
  }

  public String getFreeZone() {
    return freeZone;
  }

  public void setFreeZone(String freeZone) {
    this.freeZone = freeZone;
  }

  public BigDecimal getPostage() {
    return postage;
  }

  public void setPostage(BigDecimal postage) {
    this.postage = postage;
  }

  public Boolean getFragmentStatus() {
    return fragmentStatus;
  }

  public void setFragmentStatus(Boolean fragmentStatus) {
    this.fragmentStatus = fragmentStatus;
  }

  @Override
  public String toString() {
    return this.getId();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    Shop other = (Shop) obj;

    if (name == null && other.name != null) {
      return false;
    }

    if (!name.equals(other.name)) {
      return false;
    }

    return true;
  }

}