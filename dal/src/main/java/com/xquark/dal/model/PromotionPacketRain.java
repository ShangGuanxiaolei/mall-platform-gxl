package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionPacketRain extends BaseEntityArchivableImpl {

    /**
     * 对应的活动列表的id
     */
    private String promotionListId;

    /**
     * 活动日期
     */
    private Date promotionDate;

    /**
     * 德分总数
     */
    private Integer pointTotal;

    /**
     * 白人每日上限
     */
    private Integer normalLimit;

    /**
     * 非白人每日上限
     */
    private Integer vipLimit;

    private static final long serialVersionUID = 1L;

    public String getPromotionListId() {
        return promotionListId;
    }

    public void setPromotionListId(String promotionListId) {
        this.promotionListId = promotionListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public Integer getPointTotal() {
        return pointTotal;
    }

    public void setPointTotal(Integer pointTotal) {
        this.pointTotal = pointTotal;
    }

    public Integer getNormalLimit() {
        return normalLimit;
    }

    public void setNormalLimit(Integer normalLimit) {
        this.normalLimit = normalLimit;
    }

    public Integer getVipLimit() {
        return vipLimit;
    }

    public void setVipLimit(Integer vipLimit) {
        this.vipLimit = vipLimit;
    }
}