package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.MemberPromotionStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.FmtQiNiuImgVO;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class MemberPromotion extends BaseEntityImpl implements Archivable, FmtQiNiuImgVO {

  private String id;

  /**
   * 活动标题
   */
  private String title;

  /**
   * 介绍图
   */
  private String banner;

  /**
   * 状态
   */
  private MemberPromotionStatus status;

  /**
   * 跳转链接
   */
  private String url;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getBanner() {
    return banner;
  }

  public void setBanner(String banner) {
    this.banner = banner;
  }

  public MemberPromotionStatus getStatus() {
    return status;
  }

  public void setStatus(MemberPromotionStatus status) {
    this.status = status;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getSourceImg() {
    return banner;
  }

  @Override
  public String getTinyImgUrl() {
    return banner;
  }

  @Override
  public String getMiddleImgUrl() {
    return banner;
  }

}