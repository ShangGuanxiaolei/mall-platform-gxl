package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * 商品申请，对应一个ticket
 */
public class CampaignProduct extends BaseEntityImpl {

  private static final long serialVersionUID = 8459673242355841783L;

  private String ticketId; // 参加活动的ticket
  private String activityId; // 活动id
  private String productId; // 商品id
  private Float discount; // 折扣，多少的折扣off
  private Float reduction; // 优惠，减多少钱
  private Integer activityAmount;// 活动库存
  private Boolean archive;
  private int sort;
  private String productBrand;
  private String shortName;
  private String imagePc;
  private String imageApp;

  public String getActivityId() {
    return activityId;
  }

  public void setActivityId(String activityId) {
    this.activityId = activityId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Float getDiscount() {
    return discount;
  }

  public void setDiscount(Float discount) {
    this.discount = discount;
  }

  public Float getReduction() {
    return reduction;
  }

  public void setReduction(Float reduction) {
    this.reduction = reduction;
  }

  public String getTicketId() {
    return ticketId;
  }

  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }

  public Integer getActivityAmount() {
    return activityAmount;
  }

  public void setActivityAmount(Integer activityAmount) {
    this.activityAmount = activityAmount;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public int getSort() {
    return sort;
  }

  public void setSort(int sort) {
    this.sort = sort;
  }

  public String getProductBrand() {
    return productBrand;
  }

  public void setProductBrand(String productBrand) {
    this.productBrand = productBrand;
  }

  public String getImagePc() {
    return imagePc;
  }

  public void setImagePc(String imagePc) {
    this.imagePc = imagePc;
  }

  public String getImageApp() {
    return imageApp;
  }

  public void setImageApp(String imageApp) {
    this.imageApp = imageApp;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

}
