package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class HealthTestQuestion implements Serializable {

  private String id;

  /**
   * 菜单id
   */
  private String moduleId;

  /**
   * 正确答案的id
   */
  private String answerId;

  /**
   * 答案模板的id
   */
  private String groupId;

  /**
   * 题目名称
   */
  private String name;

  /**
   * 题目类型
   */
  private String type;

  /**
   * 题目性别限制 0 - 不限 1 - 男 2 - 女
   */
  private Integer requiredSex;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public HealthTestQuestion() {
  }

  public HealthTestQuestion(String name, String groupId, String type) {
    this.name = name;
    this.groupId = groupId;
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getModuleId() {
    return moduleId;
  }

  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  public String getAnswerId() {
    return answerId;
  }

  public void setAnswerId(String answerId) {
    this.answerId = answerId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getRequiredSex() {
    return requiredSex;
  }

  public void setRequiredSex(Integer requiredSex) {
    this.requiredSex = requiredSex;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}