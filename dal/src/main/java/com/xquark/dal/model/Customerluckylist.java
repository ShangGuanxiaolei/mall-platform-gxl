package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * customerLuckylist
 * @author 
 */
public class Customerluckylist implements Serializable {
    /**
     * 主键
     */
    private Long luckyId;

    /**
     * 吉祥号
     */
    private Long cpId;

    /**
     * 身份证号
     */
    private String tincode;

    /**
     * unbind:未绑定，unused:未使用，used:已使用
     */
    private String status;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedAt;

    /**
     * 逻辑删除标记，0未删，1已删。
     */
    private Integer isdeleted;

    private static final long serialVersionUID = 1L;

    public Long getLuckyId() {
        return luckyId;
    }

    public void setLuckyId(Long luckyId) {
        this.luckyId = luckyId;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public String getTincode() {
        return tincode;
    }

    public void setTincode(String tincode) {
        this.tincode = tincode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Integer isdeleted) {
        this.isdeleted = isdeleted;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}