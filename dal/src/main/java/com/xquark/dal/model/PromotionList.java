package com.xquark.dal.model;

import com.hds.xquark.service.error.BizException;
import com.hds.xquark.service.error.GlobalErrorCode;
import com.xquark.dal.BaseEntityArchivableImpl;

import java.time.Duration;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionList extends BaseEntityArchivableImpl {

    /**
     * 0-未开始 1-进行中 2-已结束
     */
    private Integer state;

    /**
     * 开始触发事件时间
     */
    private Date triggerStartTime;

    /**
     * 结束触发事件时间
     */
    private Date triggerEndTime;

    /**
     * 创建者
     */
    private String createUser;

    /**
     * 活动类型 0-红包雨 1-抽奖
     */
    private Integer promotionType;

    /**
     * 活动攻略
     */
    private String activityStrategy;

    /**
     * 活动规则
     */
    private String activityRule;

    /**
     * kv主图
     */
    private String banner;

    private static final long serialVersionUID = 1L;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    /**
     * 活动持续天数
     */
    public long getLastDays() {
        return getLast().toDays();
    }

    public Duration getLast() {
        final Date start = getTriggerStartTime();
        final Date end = getTriggerEndTime();
        if (start == null || end == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不存在");
        }
        return Duration.between(start.toInstant(), end.toInstant());
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

    public String getActivityStrategy() {
        return activityStrategy;
    }

    public void setActivityStrategy(String activityStrategy) {
        this.activityStrategy = activityStrategy;
    }

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}