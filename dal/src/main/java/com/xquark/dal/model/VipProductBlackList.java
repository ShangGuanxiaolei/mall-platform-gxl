package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * 用于封装后台VIP套装实体
 */
public class VipProductBlackList {

    private static final long serialVersionUID = 1L;
    private String productName; // vip套装商品名称
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img; // 商品图片url
    private BigDecimal marketPrice; // 市场价(原价)
    private Integer skuNum; // vip套装商品库存
    private String productId;
    private Integer vipsuitStatus; // vip套装商品状态
    private String supplierName; // 供应商名称

    @ApiModelProperty(value = "商品是否已经删除")
    private Boolean archive;

    public Integer getVipsuitStatus() {
        return vipsuitStatus;
    }

    public void setVipsuitStatus(Integer vipsuitStatus) {
        this.vipsuitStatus = vipsuitStatus;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }
}
