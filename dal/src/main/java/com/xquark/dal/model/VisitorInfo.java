package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

public class VisitorInfo extends BaseEntityImpl {

    private static final long serialVersionUID = 1L;

    private Long cpId;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "头像")
    private String headimgurl;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "买家Id")
    private Long buyerId;

    @ApiModelProperty(value = "订单数")
    private Integer orderNumber;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "常用收件人")
    //取默认收货地址中的收件人
    private String  addressee;

    @ApiModelProperty(value = "贡献总收益")
    private BigDecimal totalIncome;

    @ApiModelProperty(value = "顾客订单时间")
    private String orderTime;

    @ApiModelProperty(value = "是否可以赠送德分")
    private Boolean isPresentDPoint;

    @ApiModelProperty(value = "是否显示收益Button")
    private Boolean isShowIncome;

    public Boolean getIsPresentDPoint() {
        return isPresentDPoint;
    }

    public void setPresentDPoint(Boolean presentDPoint) {
        isPresentDPoint = presentDPoint;
    }

    public Boolean getIsShowIncome() {
        return isShowIncome;
    }

    public void setShowIncome(Boolean showIncome) {
        isShowIncome = showIncome;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    @ApiModelProperty(value = "收件人")
    //取默认收货地址中的收件人
    private String  receivername;

    public String getCorrelationTime() {
        return correlationTime;
    }

    public void setCorrelationTime(String correlationTime) {
        this.correlationTime = correlationTime;
    }

    @ApiModelProperty(value = "分享人关联时间")
    private String correlationTime;

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }
}
