package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class ShopWechatSetting extends BaseEntityImpl implements Archivable {


  private String ownerId;

  private String name;

  private String appId;

  private String mchId;

  private String key;

  private String certPwd;

  private String certPath;

  public String getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(String ownerId) {
    this.ownerId = ownerId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getMchId() {
    return mchId;
  }

  public void setMchId(String mchId) {
    this.mchId = mchId;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getCertPwd() {
    return certPwd;
  }

  public void setCertPwd(String certPwd) {
    this.certPwd = certPwd;
  }

  public String getCertPath() {
    return certPath;
  }

  public void setCertPath(String certPath) {
    this.certPath = certPath;
  }

  @Override
  public Boolean getArchive() {
    return null;
  }

  @Override
  public void setArchive(Boolean archive) {

  }
}