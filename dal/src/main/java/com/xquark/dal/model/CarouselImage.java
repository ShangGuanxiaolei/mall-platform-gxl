package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.TargetType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.FmtQiNiuImgVO;

@ApiModel
public class CarouselImage extends BaseEntityImpl implements Archivable, FmtQiNiuImgVO {

  private static final long serialVersionUID = 1L;

  private String carouselId;

  private String img;

  private static final String PRODUCT_URI="hanwei://product/";
  private static final String CATEGORY_URI="hanwei://category/";

  @ApiModelProperty(value = "图片地址", example = "")
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  private Boolean archive;

  private int idx;

  @ApiModelProperty(value = "标题", example = "")
  private String title;
  private String description;
  private String target;
  private TargetType targetType;

  @ApiModelProperty(value = "跳转地址", example = "")
  private String targetUrl;

  private String targetName;

  public String getTargetName() {
    return targetName;
  }

  public void setTargetName(String targetName) {
    this.targetName = targetName;
  }

  public String getTargetUrl() {
    String targetUrl = "";
    switch (this.getTargetType()) {
      case PRODUCT:
        if (!this.getTarget().contains(PRODUCT_URI))
          targetUrl = PRODUCT_URI + this.getTarget();
        break;
      case SECOND_CATEGORY:
        if (!this.getTarget().contains(CATEGORY_URI))
          targetUrl = CATEGORY_URI + this.getTarget();
        break;
      default:
        targetUrl = this.getTarget();
        break;
    }
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public String getCarouselId() {
    return carouselId;
  }

  public void setCarouselId(String carouselId) {
    this.carouselId = carouselId;
  }

  public int getIdx() {
    return idx;
  }

  public void setIdx(int idx) {
    this.idx = idx;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public TargetType getTargetType() {
    return targetType;
  }

  public void setTargetType(TargetType targetType) {
    this.targetType = targetType;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String getTinyImgUrl() {
    return imgUrl;
  }

  @Override
  public String getMiddleImgUrl() {
    return imgUrl;
  }
}