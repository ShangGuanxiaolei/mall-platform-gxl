package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * User: huangjie
 * Date: 2018/5/22.
 * Time: 下午2:56
 * 电子发票开票结果
 */
public class ElectronicBillResult extends BaseEntityImpl implements Archivable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 返回代码
     */
    private String returnCode;

    /**
     * 请求处理流水号
     */
    private String exchangeId;

    /**
     * 发票代码
     */
    private String billCode;

    /**
     * 发票号码
     */
    private String billNumber;

    /**
     * 开票日期
     */
    private Date date;

    /**
     * 电子发票pdf下载地址
     */
    private String pdfUrl;

    /**
     * 收票地址
     */
    private String receiveUrl;

    /**
     * 收票地址
     */
    private String validateCode;


    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public String getBillCode() {
        return billCode;
    }


    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getReceiveUrl() {
        return receiveUrl;
    }

    public void setReceiveUrl(String receiveUrl) {
         this.receiveUrl = receiveUrl;
    }

    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    private Boolean archive;

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @Override
    public String toString() {
        return super.toString()+"ElectronicBillResult{" +
                "orderId='" + orderId + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", exchangeId='" + exchangeId + '\'' +
                ", billCode='" + billCode + '\'' +
                ", billNumber='" + billNumber + '\'' +
                ", date=" + date +
                ", pdfUrl='" + pdfUrl + '\'' +
                ", receiveUrl='" + receiveUrl + '\'' +
                ", validateCode='" + validateCode + '\'' +
                ", archive=" + archive +
                '}';
    }
}