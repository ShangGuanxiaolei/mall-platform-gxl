package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * xquark_sf_app_config
 *
 * @author wangxinhua
 */
public class SfAppConfig extends BaseEntityImpl {

  /**
   * id
   */
  private String id;

  /**
   * 顺丰clientId
   */
  private String clientId;

  /**
   * 顺丰grantType
   */
  private String grantType;

  /**
   * 环境配置
   */
  private String profile;

  /**
   * 顺丰secret
   */
  private String clientSecret;

  /**
   * token
   */
  private String accessToken;

  /**
   * 下次过期时间
   */
  private Date accessTokenExpiredAt;

  private Date createdAt;

  private Date updatedAt;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getGrantType() {
    return grantType;
  }

  public void setGrantType(String grantType) {
    this.grantType = grantType;
  }

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Date getAccessTokenExpiredAt() {
    return accessTokenExpiredAt;
  }

  public void setAccessTokenExpiredAt(Date accessTokenExpiredAt) {
    this.accessTokenExpiredAt = accessTokenExpiredAt;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    SfAppConfig other = (SfAppConfig) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getClientId() == null ? other.getClientId() == null
        : this.getClientId().equals(other.getClientId()))
        && (this.getProfile() == null ? other.getProfile() == null
        : this.getProfile().equals(other.getProfile()))
        && (this.getClientSecret() == null ? other.getClientSecret() == null
        : this.getClientSecret().equals(other.getClientSecret()))
        && (this.getAccessToken() == null ? other.getAccessToken() == null
        : this.getAccessToken().equals(other.getAccessToken()))
        && (this.getAccessTokenExpiredAt() == null ? other.getAccessTokenExpiredAt() == null
        : this.getAccessTokenExpiredAt().equals(other.getAccessTokenExpiredAt()))
        && (this.getCreatedAt() == null ? other.getCreatedAt() == null
        : this.getCreatedAt().equals(other.getCreatedAt()))
        && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null
        : this.getUpdatedAt().equals(other.getUpdatedAt()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getClientId() == null) ? 0 : getClientId().hashCode());
    result = prime * result + ((getProfile() == null) ? 0 : getProfile().hashCode());
    result = prime * result + ((getClientSecret() == null) ? 0 : getClientSecret().hashCode());
    result = prime * result + ((getAccessToken() == null) ? 0 : getAccessToken().hashCode());
    result = prime * result + ((getAccessTokenExpiredAt() == null) ? 0
        : getAccessTokenExpiredAt().hashCode());
    result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
    result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(id);
    sb.append(", clientId=").append(clientId);
    sb.append(", profile=").append(profile);
    sb.append(", clientSecret=").append(clientSecret);
    sb.append(", accessToken=").append(accessToken);
    sb.append(", accessTokenExpiredAt=").append(accessTokenExpiredAt);
    sb.append(", createdAt=").append(createdAt);
    sb.append(", updatedAt=").append(updatedAt);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}