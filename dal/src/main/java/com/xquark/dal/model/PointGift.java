package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class PointGift extends BaseEntityArchivableImpl {
    /**
     * 主键
     */
//    private String id;

    /**
     * 红包编号
     */
    private String pointGiftNo;

    /**
     * 红包数量金额
     */
    private BigDecimal aggregateAmount;

    /**
     * 发给的人数
     */
    private Integer peopleAmount;

    /**
     * 红包发放对象
     */
    private String requireIdentity;

    /**
     * 红包类型 1 - 定向 2 - 非定向
     */
    private Integer packetType;

    /**
     * 红包状态 1 - 可领取 2 - 已过期 3 - 已领完
     */
    private Integer status;

    /**
     * 用户留言
     */
    private String leaveWord;


    /**
     * 0 - 等额 1 - 非等额
     */
    private int divisionType;
    /**
     * 过期时间
     */
    private Date expireAt;

    private Long cpid;

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(long cpid) {
        this.cpid = cpid;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private PointGift(Builder builder){
        this.pointGiftNo = builder.pointGiftNo;
        this.aggregateAmount = builder.aggregateAmount;
        this.peopleAmount = builder.peopleAmount;
        this.requireIdentity = builder.requireIdentity;
        this.packetType = builder.packetType;
        this.status = builder.status;
        this.leaveWord = builder.leaveWord;
        this.expireAt = builder.expireAt;
        this.cpid = builder.cpid;
    }

    public PointGift() {
    }

    private static final long serialVersionUID = 1L;


    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public void setId(String id) {
        super.setId(id);
    }

    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public BigDecimal getAggregateAmount() {
        return aggregateAmount;
    }

    public void setAggregateAmount(BigDecimal aggregateAmount) {
        this.aggregateAmount = aggregateAmount;
    }

    public Integer getPeopleAmount() {
        return peopleAmount;
    }

    public void setPeopleAmount(Integer peopleAmount) {
        this.peopleAmount = peopleAmount;
    }

    public String getRequireIdentity() {
        return requireIdentity;
    }

    public void setRequireIdentity(String requireIdentity) {
        this.requireIdentity = requireIdentity;
    }

    public Integer getPacketType() {
        return packetType;
    }

    public void setPacketType(Integer packetType) {
        this.packetType = packetType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }

    public int getDivisionType() {
        return divisionType;
    }

    public void setDivisionType(int divisionType) {
        this.divisionType = divisionType;
    }

    public static class Builder {
        private String pointGiftNo;

        private BigDecimal aggregateAmount;

        private Integer peopleAmount;

        private String requireIdentity;

        private Integer packetType;

        private Integer status;

        private String leaveWord;

        private Date expireAt;

        private Long cpid;

        public Builder pointGiftNo(String pointGiftNo) {
            this.pointGiftNo = pointGiftNo;
            return this;
        }

        public Builder aggregateAmount(BigDecimal aggregateAmount) {
            this.aggregateAmount = aggregateAmount;
            return this;
        }

        public Builder peopleAmount(Integer peopleAmount) {
            this.peopleAmount = peopleAmount;
            return this;
        }

        public Builder requireIdentity(String requireIdentity) {
            this.requireIdentity = requireIdentity;
            return this;
        }

        public Builder packetType(Integer packetType) {
            this.packetType = packetType;
            return this;
        }

        public Builder status(Integer status) {
            this.status = status;
            return this;
        }

        public Builder leaveWord(String leaveWord) {
            this.leaveWord = leaveWord;
            return this;
        }

        public Builder expireAt(Date expireAt) {
            this.expireAt = expireAt;
            return this;
        }

        public Builder cpid(Long cpid){
            this.cpid = cpid;
            return this;
        }

        public PointGift build(){
            return new PointGift(this);
        }
    }
}
