package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;


public class ShopAccessLog extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private String userId;

  private String shopId;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}