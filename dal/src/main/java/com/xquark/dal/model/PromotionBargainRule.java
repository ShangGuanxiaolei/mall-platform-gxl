package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.type.CareerLevelType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiFunction;

/**
 * @author wangxinhua
 */
public class PromotionBargainRule extends BaseEntityArchivableImpl {

    /**
     * 折扣跟数据库配置的比例
     */
    private final static BigDecimal RULE_SCALA = BigDecimal.valueOf(100);

    private String id;

    private Integer maxCutTime;

    /**
     *  砍价活动表id
     */
    private String bargainId;

    /**
     * 砍价最小折扣 
     */
    private Integer launchDiscountMin;

    /**
     * 砍价最大折扣 
     */
    private Integer launchDiscountMax;

    /**
     * 助力砍价最小折扣 
     */
    private Integer supportDiscountMin;

    /**
     * 助力砍价最大折扣 
     */
    private Integer supportDiscountMax;

    /**
     * 助力得分奖励 
     */
    private Integer supportPoint;

    /**
     * 身份类型
     */
    private CareerLevelType identityType;

    private Date createAt;

    private Date updateAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;


    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Integer getMaxCutTime() {
        return maxCutTime;
    }

    public void setMaxCutTime(Integer maxCutTime) {
        this.maxCutTime = maxCutTime;
    }

    public String getBargainId() {
        return bargainId;
    }

    public void setBargainId(String bargainId) {
        this.bargainId = bargainId;
    }

    public Integer getLaunchDiscountMin() {
        return launchDiscountMin;
    }

    public void setLaunchDiscountMin(Integer launchDiscountMin) {
        this.launchDiscountMin = launchDiscountMin;
    }

    public Integer getLaunchDiscountMax() {
        return launchDiscountMax;
    }

    public void setLaunchDiscountMax(Integer launchDiscountMax) {
        this.launchDiscountMax = launchDiscountMax;
    }

    public Integer getSupportDiscountMin() {
        return supportDiscountMin;
    }

    public void setSupportDiscountMin(Integer supportDiscountMin) {
        this.supportDiscountMin = supportDiscountMin;
    }

    public Integer getSupportDiscountMax() {
        return supportDiscountMax;
    }

    public void setSupportDiscountMax(Integer supportDiscountMax) {
        this.supportDiscountMax = supportDiscountMax;
    }

    public Integer getSupportPoint() {
        return supportPoint;
    }

    public void setSupportPoint(Integer supportPoint) {
        this.supportPoint = supportPoint;
    }

    public CareerLevelType getIdentityType() {
        return identityType;
    }

    public void setIdentityType(CareerLevelType identityType) {
        this.identityType = identityType;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    /**
     * 给自己砍价
     * @param price 原价
     * @return 随机砍价金额
     */
    public BigDecimal genDiscountSelf(BigDecimal price) {
        return genDiscount(price).apply(launchDiscountMin, launchDiscountMax);
    }

    /**
     * 帮忙砍价
     * @param price 原价
     * @return 随机砍价金额
     */
    public BigDecimal genDiscountSupport(BigDecimal price) {
        return genDiscount(price).apply(supportDiscountMin, supportDiscountMax);
    }

    /**
     * 封装随机砍价金额方法
     * @param source 原价
     * @return (min, max) -> discount
     */
    private static BiFunction<Integer, Integer, BigDecimal> genDiscount(final BigDecimal source) {
        return (min, max) -> {
            final int random = ThreadLocalRandom.current().nextInt(min, max + 1);
            final BigDecimal rate = BigDecimal.valueOf(random)
                    .divide(RULE_SCALA, 2, RoundingMode.HALF_EVEN);
            return source.multiply(rate);
        };
    }
}