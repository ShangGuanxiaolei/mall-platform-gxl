package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * 作者: wangxh 创建日期: 17-3-15 简介:
 */
public class Org extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 5428688643431689884L;

  private String name;  //节点名称
  private String parent_id;  //父节点id
  private boolean is_leaf;  //是否为子节点
  private boolean is_auto_expand;  //是否自动展开
  private String icon_name;  //节点图标
  private int sort_no;  //节点顺序
  private String remark;  //备注
  private Boolean archive;  //是否已删除

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getParent_id() {
    return parent_id;
  }

  public void setParent_id(String parent_id) {
    this.parent_id = parent_id;
  }

  public boolean getIs_leaf() {
    return is_leaf;
  }

  public void setIs_leaf(boolean is_leaf) {
    this.is_leaf = is_leaf;
  }

  public boolean getIs_auto_expand() {
    return is_auto_expand;
  }

  public void setIs_auto_expand(boolean is_auto_expand) {
    this.is_auto_expand = is_auto_expand;
  }

  public String getIcon_name() {
    return icon_name;
  }

  public void setIcon_name(String icon_name) {
    this.icon_name = icon_name;
  }

  public int getSort_no() {
    return sort_no;
  }

  public void setSort_no(int sort_no) {
    this.sort_no = sort_no;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
