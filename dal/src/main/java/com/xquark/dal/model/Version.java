package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AppType;
import com.xquark.dal.status.AppUpdateType;
import com.xquark.dal.status.HomeItemType;

public class Version extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String startVersion;

  private String endVersion;

  private String version;

  private AppUpdateType type;

  private AppType appType;

  private Boolean archive;

  private String url;

  private String description;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStartVersion() {
    return startVersion;
  }

  public void setStartVersion(String startVersion) {
    this.startVersion = startVersion;
  }

  public String getEndVersion() {
    return endVersion;
  }

  public void setEndVersion(String endVersion) {
    this.endVersion = endVersion;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public AppUpdateType getType() {
    return type;
  }

  public void setType(AppUpdateType type) {
    this.type = type;
  }

  public AppType getAppType() {
    return appType;
  }

  public void setAppType(AppType appType) {
    this.appType = appType;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}