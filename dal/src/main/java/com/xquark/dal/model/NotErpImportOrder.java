package com.xquark.dal.model;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/6/10
 * Time: 14:59
 * Description: 非erp 导入模板
 */
public class NotErpImportOrder {
    //订单编号
  private  String orderNo;
    //收件人
  private  String receiver;
    //联系方式
  private  String phone;
    //收货地址
  private  String street;
    //买家名称
  private  String buyerRemark;
     //商品名称
  private  String goodName;
     //规格名称
  private  String skuName;
     //规格编码
  private  String skuCode;
     //商品数量
  private  String count;

  public String getOrderNo() {
        return orderNo;
    }

  public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

  public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuyerRemark() {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark) {
        this.buyerRemark = buyerRemark;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
