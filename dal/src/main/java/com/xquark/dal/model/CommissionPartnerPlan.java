package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class CommissionPartnerPlan extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name;

  private String ownShopId;

  private double partnerRate;

  private Boolean defaultStatus;

  private Boolean archive;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(String ownShopId) {
    this.ownShopId = ownShopId;
  }

  public double getPartnerRate() {
    return partnerRate;
  }

  public void setPartnerRate(double partnerRate) {
    this.partnerRate = partnerRate;
  }

  public Boolean getDefaultStatus() {
    return defaultStatus;
  }

  public void setDefaultStatus(Boolean defaultStatus) {
    this.defaultStatus = defaultStatus;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}