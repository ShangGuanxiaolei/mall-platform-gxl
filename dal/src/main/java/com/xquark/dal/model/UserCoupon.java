package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.CouponStatus;

import java.util.Date;

/**
 * 用户领取优惠券
 */
public class UserCoupon extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String couponId;          //优惠券id

  private String code;              //优惠券编码

  private String userId;            //用户id

  private String shopId;            //店铺id

  private String phone;             //手机号

  private String couponSerialNo;    //优惠券唯一编号

  private Date createAt;            //记录创建时间

  private Date updateAt;            //记录更新时间

  private Date acquiredAt;          //优惠券领取时间

  private Date appliedAt;           //使用时间

  private CouponStatus status;      //用户领取的优惠券状态

  private String deviceIp;          //领取设备ip

  private String deviceAgent;       //领取设备agent

  private String recommendedBy;     //领取页面的推荐来源：微信朋友圈，微博等等

  private Boolean archive;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getCouponSerialNo() {

    return couponSerialNo;
  }

  public void setCouponSerialNo(String couponSerialNo) {
    this.couponSerialNo = couponSerialNo;
  }

  public Date getCreateAt() {

    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public Date getAcquiredAt() {
    return acquiredAt;
  }

  public void setAcquiredAt(Date acquiredAt) {
    this.acquiredAt = acquiredAt;
  }

  public CouponStatus getStatus() {
    return status;
  }

  public void setStatus(CouponStatus status) {
    this.status = status;
  }

  public Date getAppliedAt() {
    return appliedAt;
  }

  public void setAppliedAt(Date appliedAt) {
    this.appliedAt = appliedAt;
  }

  public String getDeviceIp() {
    return deviceIp;
  }

  public void setDeviceIp(String deviceIp) {
    this.deviceIp = deviceIp;
  }

  public String getDeviceAgent() {
    return deviceAgent;
  }

  public void setDeviceAgent(String deviceAgent) {
    this.deviceAgent = deviceAgent;
  }

  public String getRecommendedBy() {
    return recommendedBy;
  }

  public void setRecommendedBy(String recommendedBy) {
    this.recommendedBy = recommendedBy;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}