package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.TeamType;

import java.math.BigDecimal;

public class TeamShopCommissionDe extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String parentId;

  private TeamType type;

  private BigDecimal sales;

  private BigDecimal totalAmount;

  private BigDecimal leaderAmount;

  private Boolean defaultStatus;

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public TeamType getType() {
    return type;
  }

  public void setType(TeamType type) {
    this.type = type;
  }

  public BigDecimal getSales() {
    return sales;
  }

  public void setSales(BigDecimal sales) {
    this.sales = sales;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

  public BigDecimal getLeaderAmount() {
    return leaderAmount;
  }

  public void setLeaderAmount(BigDecimal leaderAmount) {
    this.leaderAmount = leaderAmount;
  }

  public Boolean getDefaultStatus() {
    return defaultStatus;
  }

  public void setDefaultStatus(Boolean defaultStatus) {
    this.defaultStatus = defaultStatus;
  }

  private Boolean archive;

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}