package com.xquark.dal.model.wechat;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * 微信公众号自定义菜单项
 */
public class CustomizedMenuItem extends BaseEntityImpl implements Archivable {

  /**
   * 微信自定义菜单按钮名称
   */
  private String name;

  /**
   * 按钮类型 https://mp.weixin.qq.com/wiki/13/43de8269be54a0a6f64413e4dfa94f39.html
   */
  private ButtonType type;

  /**
   * 父按钮
   */
  private String parentId;

  /**
   * 在同级别菜单中的顺序,从0开始
   */
  private Integer index;

  /**
   * 菜单项的key值,用于识别菜单项,根据key值所反映的用户点击按钮与用户交互
   */
  private String key;

  /**
   * 针对于按钮类型为跳转url的按钮,保存该url
   */
  private String url;

  /**
   * 针对于按钮类型为推送永久素材时,保存该素材的ID
   */
  private String mediaId;

  /**
   * 此自定义菜单所属的店铺
   */
  private String shopId;

  /**
   * 此自定义菜单所属的微信服务号
   */
  private String wechatAppId;

  private Boolean archive;

  private String content;

  /**
   * 该菜单项是否需要更新
   */
  private Boolean renewable;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ButtonType getType() {
    return type;
  }

  public void setType(ButtonType type) {
    this.type = type;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getWechatAppId() {
    return wechatAppId;
  }

  public void setWechatAppId(String wechatAppId) {
    this.wechatAppId = wechatAppId;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Boolean getRenewable() {
    return renewable;
  }

  public void setRenewable(Boolean renewable) {
    this.renewable = renewable;
  }
}
