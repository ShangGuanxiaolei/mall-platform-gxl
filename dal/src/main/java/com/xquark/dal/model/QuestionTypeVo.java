package com.xquark.dal.model;

import java.util.List;

public class QuestionTypeVo {
    private Integer id;
    private String type;
    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    private List<CustomerQuestion> customerQuestionList;

    public List<CustomerQuestion> getCustomerQuestionList() {
        return customerQuestionList;
    }

    public void setCustomerQuestionList(List<CustomerQuestion> customerQuestionList) {
        this.customerQuestionList = customerQuestionList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

