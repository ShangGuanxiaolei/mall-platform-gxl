package com.xquark.dal.model.mypiece;

/**
 * //用于封装StockCheck(库存检查) 和 PieceConfirm(订单确认)
 * @author gx
 */
public class ConfirmAndCheckVo {
    private StockCheckBean stockCheckBean;
    private PieceConfirmOrder pieceConfirmOrder;

    public StockCheckBean getStockCheckBean() {
        return stockCheckBean;
    }

    public void setStockCheckBean(StockCheckBean stockCheckBean) {
        this.stockCheckBean = stockCheckBean;
    }

    public PieceConfirmOrder getPieceConfirmOrder() {
        return pieceConfirmOrder;
    }

    public void setPieceConfirmOrder(PieceConfirmOrder pieceConfirmOrder) {
        this.pieceConfirmOrder = pieceConfirmOrder;
    }
}
