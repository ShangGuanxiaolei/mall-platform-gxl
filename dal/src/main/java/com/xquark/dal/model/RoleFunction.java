package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * 作者: wangxh 创建日期: 17-4-7 简介:
 */
public class RoleFunction extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 6960592357669085567L;

  public RoleFunction() {
  }

  public RoleFunction(String functionId, String roleId) {
    this.functionId = functionId;
    this.roleId = roleId;
  }

  private String id;
  private String functionId;
  private String roleId;
  // 默认有可见可用权限
  private boolean isShow;
  private boolean isEnable;
  private Date createdAt;
  private Date updatedAt;
  private Boolean archive;

  public String getFunctionId() {
    return functionId;
  }

  public void setFunctionId(String functionId) {
    this.functionId = functionId;
  }

  public String getRoleId() {
    return roleId;
  }

  public void setRoleId(String roleId) {
    this.roleId = roleId;
  }

  public Boolean getIsShow() {
    return isShow;
  }

  public void setIsShow(Boolean show) {
    isShow = show;
  }

  public Boolean getIsEnable() {
    return isEnable;
  }

  public void setIsEnable(Boolean enable) {
    isEnable = enable;
  }

  @Override
  public String getId() {
    return super.getId();
  }

  @Override
  public void setId(String id) {
    super.setId(id);
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    super.setCreatedAt(createdAt);
  }

  @Override
  public Date getCreatedAt() {
    return super.getCreatedAt();
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    super.setUpdatedAt(updatedAt);
  }

  @Override
  public Date getUpdatedAt() {
    return super.getUpdatedAt();
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
