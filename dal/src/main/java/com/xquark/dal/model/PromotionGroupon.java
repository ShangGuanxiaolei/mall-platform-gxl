package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by chh on 16-9-23.
 */
public class PromotionGroupon extends BaseEntityImpl implements Archivable {

  private Boolean archive;
  private BigDecimal discount;
  private int numbers;
  private String productId;
  private BigDecimal amount;
  private BigDecimal sales;
  private String shopid;
  private String promotionId;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public BigDecimal getSales() {
    return sales;
  }

  public void setSales(BigDecimal sales) {
    this.sales = sales;
  }

  public String getShopid() {
    return shopid;
  }

  public void setShopid(String shopid) {
    this.shopid = shopid;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public int getNumbers() {
    return numbers;
  }

  public void setNumbers(int numbers) {
    this.numbers = numbers;
  }
}
