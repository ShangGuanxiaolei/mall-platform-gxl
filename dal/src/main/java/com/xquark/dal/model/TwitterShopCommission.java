package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ValidType;

public class TwitterShopCommission extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Long ownShopId;

  private String name;

  private double firstLevelRate;

  private double secondLevelRate;

  private double thirdLevelRate;

  private Boolean archive;

  private Boolean defaultStatus;

  private ValidType validType;

  private Boolean selfCommission;

  private String alias;

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public Boolean getSelfCommission() {
    return selfCommission;
  }

  public void setSelfCommission(Boolean selfCommission) {
    this.selfCommission = selfCommission;
  }

  public ValidType getValidType() {
    return validType;
  }

  public void setValidType(ValidType validType) {
    this.validType = validType;
  }

  public Long getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(Long ownShopId) {
    this.ownShopId = ownShopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getFirstLevelRate() {
    return firstLevelRate;
  }

  public void setFirstLevelRate(double firstLevelRate) {
    this.firstLevelRate = firstLevelRate;
  }

  public double getSecondLevelRate() {
    return secondLevelRate;
  }

  public void setSecondLevelRate(double secondLevelRate) {
    this.secondLevelRate = secondLevelRate;
  }

  public double getThirdLevelRate() {
    return thirdLevelRate;
  }

  public void setThirdLevelRate(double thirdLevelRate) {
    this.thirdLevelRate = thirdLevelRate;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  public Boolean getDefaultStatus() {
    return defaultStatus;
  }

  public void setDefaultStatus(Boolean defaultStatus) {
    this.defaultStatus = defaultStatus;
  }
}