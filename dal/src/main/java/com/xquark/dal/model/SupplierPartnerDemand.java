package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class SupplierPartnerDemand extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Long shopId;

  private Long distributionTypeId;

  private Long distributorAmount;

  private BigDecimal cashDemand;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private BigDecimal orderCommissionRate;

  public Long getShopId() {
    return shopId;
  }

  public void setShopId(Long shopId) {
    this.shopId = shopId;
  }

  public Long getDistributionTypeId() {
    return distributionTypeId;
  }

  public void setDistributionTypeId(Long distributionTypeId) {
    this.distributionTypeId = distributionTypeId;
  }

  public Long getDistributorAmount() {
    return distributorAmount;
  }

  public void setDistributorAmount(Long distributorAmount) {
    this.distributorAmount = distributorAmount;
  }

  public BigDecimal getCashDemand() {
    return cashDemand;
  }

  public void setCashDemand(BigDecimal cashDemand) {
    this.cashDemand = cashDemand;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public BigDecimal getOrderCommissionRate() {
    return orderCommissionRate;
  }

  public void setOrderCommissionRate(BigDecimal orderCommissionRate) {
    this.orderCommissionRate = orderCommissionRate;
  }
}