package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;

/**
 * 预售
 *
 * @author: yyc
 * @date: 19-4-17 下午4:54
 */
public class PromotionReserveProduct extends BaseEntityArchivableImpl {

  private static final long serialVersionUID = 1L;

  /** 预约活动id */
  private String promotionId;

  /** 商品id */
  private String productId;

  /** 类目名称 */
  private String categoryName;

  /** 预约活动价 */
  private BigDecimal promotionPrice;

  /** 兑换价 */
  private BigDecimal conversionPrice;

  /** 德分 */
  private Long point;

  /** 活动库存上限 */
  private Integer amountLimit;

  /** 每人限购数 */
  private Integer buyLimit;

  /** 最大可预约数量 */
  private Integer reserveLimit;

  /** skuId */
  private String skuId;

  /** 前端已预约数量(配置固定值) */
  private Integer preReserveLimit;

  /** 实际预约数量(当实际预约数量大于配置预约数量，才展示) */
  private Integer preReserveCount;

  private Integer version;

  /** 活动付款数量 */
  private Integer payAmount;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public BigDecimal getPromotionPrice() {
    return promotionPrice;
  }

  public void setPromotionPrice(BigDecimal promotionPrice) {
    this.promotionPrice = promotionPrice;
  }

  public BigDecimal getConversionPrice() {
    return conversionPrice;
  }

  public void setConversionPrice(BigDecimal conversionPrice) {
    this.conversionPrice = conversionPrice;
  }

  public Long getPoint() {
    return point;
  }

  public void setPoint(Long point) {
    this.point = point;
  }

  public Integer getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Integer amountLimit) {
    this.amountLimit = amountLimit;
  }

  public Integer getBuyLimit() {
    return buyLimit;
  }

  public void setBuyLimit(Integer buyLimit) {
    this.buyLimit = buyLimit;
  }

  public Integer getReserveLimit() {
    return reserveLimit;
  }

  public void setReserveLimit(Integer reserveLimit) {
    this.reserveLimit = reserveLimit;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public Integer getPreReserveLimit() {
    return preReserveLimit;
  }

  public void setPreReserveLimit(Integer preReserveLimit) {
    this.preReserveLimit = preReserveLimit;
  }

  public Integer getPreReserveCount() {
    return preReserveCount;
  }

  public void setPreReserveCount(Integer preReserveCount) {
    this.preReserveCount = preReserveCount;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Integer getPayAmount() {
    return payAmount;
  }

  public void setPayAmount(Integer payAmount) {
    this.payAmount = payAmount;
  }
}
