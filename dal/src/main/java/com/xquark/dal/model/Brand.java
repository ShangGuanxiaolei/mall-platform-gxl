package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.Date;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * xquark_brand
 *
 * @author wangxinhua
 */
public class Brand extends BaseEntityImpl implements Archivable {

    /**
     * 品牌名
     */
    @NotBlank
    private String name;

    /**
     * 品牌主页
     */
    private String siteHost = "";

    /**
     * 品牌描述
     */
    private String description = "";

    /**
     * 排序字段
     */
    @NotNull
    @Range(min = 0, max = 999)
    private Integer sortOrder;

    /**
     * 品牌图片
     */
    @NotBlank
    private String logo;


    /**
     * 品牌图片url
     */
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String logoUrl;


    private Boolean archive;

    /**
     * 是否展示
     */
    private Boolean isShow = true;

    private ProductSource source;

    private String sourceId;

    private Date createdAt;

    private Date updatedAt;

    private Long decodeId; //解码后的id

    private static final long serialVersionUID = 1L;

    public Long getDecodeId() {
        if(StringUtils.isBlank(this.getId())){
            return this.decodeId;
        }
        return IdTypeHandler.decode(this.getId());
    }

    public void setDecodeId(Long decodeId) {
        this.decodeId = decodeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSiteHost() {
        return siteHost;
    }

    public void setSiteHost(String siteHost) {
        this.siteHost = siteHost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getIsShow() {
        return isShow;
    }

    public void setIsShow(Boolean show) {
        isShow = show;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ProductSource getSource() {
        return source;
    }

    public void setSource(ProductSource source) {
        this.source = source;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}