package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.Objects;

public class StatusAndTimesVO implements PieceTimeChecker {

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date effectFrom;//开始时间
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date effectTo;//结束时间
  private String pStatus;//活动状态
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date groupOpenTime;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date groupFinishTime;
  private Long pieceEffectTime;

  private Integer jumpQueueTime;

  public Long getPieceEffectTime() {
    return pieceEffectTime;
  }

  public void setPieceEffectTime(Long pieceEffectTime) {
    this.pieceEffectTime = pieceEffectTime;
  }

  public Date getGroupOpenTime() {
    return groupOpenTime;
  }

  public void setGroupOpenTime(Date groupOpenTime) {
    this.groupOpenTime = groupOpenTime;
  }

  public Date getEffectFrom() {
    return effectFrom;
  }

  public void setEffectFrom(Date effectFrom) {
    this.effectFrom = effectFrom;
  }

  public Date getEffectTo() {
    return effectTo;
  }

  public void setEffectTo(Date effectTo) {
    this.effectTo = effectTo;
  }

  public String getpStatus() {
    return pStatus;
  }

  public void setpStatus(String pStatus) {
    this.pStatus = pStatus;
  }

  public Integer getJumpQueueTime() {
    return jumpQueueTime;
  }

  public void setJumpQueueTime(Integer jumpQueueTime) {
    this.jumpQueueTime = jumpQueueTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatusAndTimesVO that = (StatusAndTimesVO) o;
    return Objects.equals(effectFrom, that.effectFrom) &&
        Objects.equals(effectTo, that.effectTo) &&
        Objects.equals(pStatus, that.pStatus);
  }

  public Date getGroupFinishTime() {
    return groupFinishTime;
  }

  public void setGroupFinishTime(Date groupFinishTime) {
    this.groupFinishTime = groupFinishTime;
  }

  @Override
  public int hashCode() {

    return Objects.hash(effectFrom, effectTo, pStatus);
  }

  @Override
  public String toString() {
    return "StatusAndTimesVO{" +
            "effectFrom=" + effectFrom +
            ", effectTo=" + effectTo +
            ", pStatus='" + pStatus + '\'' +
            '}';
  }
}
