package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * @author 
 */
public class WeakLink extends BaseEntityImpl {

    private static final long serialVersionUID = 1L;
    /**
     * 汉薇cpId
     */
    private Long cpid;

    /**
     * 分享人cpId,没人分享填公司
     */
    private Long shareCpid;

    /**
     * 关系状态 1-已经绑定 2-已经解除 3-失效
     */
    private int status;

    /**
     * ORDINARY_GOODS
     * IDENTITY_RIGHTS
     * SPELL_GROUP
     * POINT_PACKET
     * SELF_MOTION
     */
    private String channel;

    public WeakLink(){}
    public WeakLink(Long cpid, Long shareCpid, int status) {
        this.cpid = cpid;
        this.shareCpid = shareCpid;
        this.status = status;
    }

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(Long cpid) {
        this.cpid = cpid;
    }

    public Long getShareCpid() {
        return shareCpid;
    }

    public void setShareCpid(Long shareCpid) {
        this.shareCpid = shareCpid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}