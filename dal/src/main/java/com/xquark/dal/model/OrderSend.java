package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;
import org.apache.ibatis.annotations.Param;

/**
 * User: a9175
 * Date: 2018/6/17.
 * Time: 14:55
 */
public class OrderSend implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sendType;//订单发货类别
    /**
     * 快递名称
     */
    private String logisticName;

    /**
     * 快递类别
     */
    private String logisticType;

    /**
     * 快递运单号
     */
    private String logisticNo;

    /**
     * 平台订单号
     */
    private String platOrderNo;

    /**
     * 是否拆单发货(拆单=1 ，不拆单=0)
     */
    private Integer isSplit;

    /**
     * 平台订单号
     */
    private String subPlatOrderNo;

    /**
     * 发货人姓名
     */
    private String senderName;

    /**
     * 发货人联系电话
     */
    private String senderTel;

    /**
     * 发货人地址
     */
    private String senderAddress;

    private Integer isHwgFlag;//是否为海外购

    private Date createdAt;//创建时间

    private Date updatedAt;//更改时间

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public String getLogisticName() {
        return logisticName;
    }

    public void setLogisticName(String logisticName) {
        this.logisticName = logisticName;
    }

    public String getLogisticType() {
        return logisticType;
    }

    public void setLogisticType(String logisticType) {
        this.logisticType = logisticType;
    }

    public String getLogisticNo() {
        return logisticNo;
    }

    public void setLogisticNo(String logisticNo) {
        this.logisticNo = logisticNo;
    }

    public String getPlatOrderNo() {
        return platOrderNo;
    }

    public void setPlatOrderNo(String platOrderNo) {
        this.platOrderNo = platOrderNo;
    }

    public Integer getIsSplit() {
        return isSplit;
    }

    public void setIsSplit(Integer isSplit) {
        this.isSplit = isSplit;
    }

    public String getSubPlatOrderNo() {
        return subPlatOrderNo;
    }

    public void setSubPlatOrderNo(String subPlatOrderNo) {
        this.subPlatOrderNo = subPlatOrderNo;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderTel() {
        return senderTel;
    }

    public void setSenderTel(String senderTel) {
        this.senderTel = senderTel;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public Integer getIsHwgFlag() {
        return isHwgFlag;
    }

    public void setIsHwgFlag(Integer isHwgFlag) {
        this.isHwgFlag = isHwgFlag;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

