package com.xquark.dal.model;

import com.xquark.dal.page.PageHelper;
import com.xquark.dal.vo.PromotionTranInfoVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class FootOrder {

    private Date paidAt;
    private String orderNo;
    private int totalNum;
    private BigDecimal totalPrice;
    private String status;
    private String dest;

    public String getLogisticsOrderNo() {
        return logisticsOrderNo;
    }

    public void setLogisticsOrderNo(String logisticsOrderNo) {
        this.logisticsOrderNo = logisticsOrderNo;
    }

    private String logisticsOrderNo;

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private List<FootOrderItem> footOrderItems;

    public List<FootOrderItem> getFootOrderItems() {
        return footOrderItems;
    }

    public void setFootOrderItems(List<FootOrderItem> footOrderItems) {
        this.footOrderItems = footOrderItems;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(Date paidAt) {
        this.paidAt = paidAt;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }




}
