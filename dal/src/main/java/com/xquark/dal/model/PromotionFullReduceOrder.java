package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author
 */
public class PromotionFullReduceOrder extends BaseEntityImpl implements Archivable {

  public PromotionFullReduceOrder() {
  }

  public PromotionFullReduceOrder(String orderId, String promotionId, PromotionType type,
      BigDecimal discountFee) {
    this.promotionId = promotionId;
    this.orderId = orderId;
    this.type = type;
    this.discountFee = discountFee;
  }

  private String id;

  /**
   * 活动id
   */
  private String promotionId;

  /**
   * 订单id
   */
  private String orderId;

  private PromotionType type;

  /**
   * 优惠金额
   */
  private BigDecimal discountFee;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public PromotionType getType() {
    return type;
  }

  public void setType(PromotionType type) {
    this.type = type;
  }
}