package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * User: huangjie Date: 2018/6/23. Time: 上午11:36 供应商与仓库的关联
 */
public class SupplierWareHouse extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  /**
   * 供应商id
   */
  private String supplierId;

  /**
   * 仓库id
   */
  private String warehouseId;

  /***
   * 仓库名称
   */
  private String houseName;

  public String getHouseName() {
    return houseName;
  }

  public void setHouseName(String houseName) {
    this.houseName = houseName;
  }

  private Boolean archive;

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "SupplierWareHouse{" +
        "supplierId='" + supplierId + '\'' +
        ", warehouseId='" + warehouseId + '\'' +
        ", archive=" + archive +
        '}';
  }

  public String getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(String supplierId) {
    this.supplierId = supplierId;
  }

  public String getWarehouseId() {
    return warehouseId;
  }

  public void setWarehouseId(String warehouseId) {
    this.warehouseId = warehouseId;
  }
}