package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializerSync;

public class Payment extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String domainId;

  private String paymentId;

  private String device;

  @JsonSerialize(using = JsonResourceUrlSerializerSync.class)
  private String icon;

  private Integer paymentOrder;

  private String description;

  private String descriptionExt;

  private Long maxFee;

  private Boolean archive;

  public String getDevice() {
    return device;
  }

  public void setDevice(String device) {
    this.device = device == null ? null : device.trim();
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon == null ? null : icon.trim();
  }

  public String getDomainId() {
    return domainId;
  }

  public void setDomainId(String domainId) {
    this.domainId = domainId;
  }

  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public Integer getPaymentOrder() {
    return paymentOrder;
  }

  public void setPaymentOrder(Integer paymentOrder) {
    this.paymentOrder = paymentOrder;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description == null ? null : description.trim();
  }

  public String getDescriptionExt() {
    return descriptionExt;
  }

  public void setDescriptionExt(String descriptionExt) {
    this.descriptionExt = descriptionExt == null ? null : descriptionExt.trim();
  }

  public Long getMaxFee() {
    return maxFee;
  }

  public void setMaxFee(Long maxFee) {
    this.maxFee = maxFee;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}