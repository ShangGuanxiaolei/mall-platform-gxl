package com.xquark.dal.model;

/**
 * @Author:  tanggb
 * @Description: 拼团活动实体类
 * @time:  2018/9/12 10:23
 * @Modified:
 */
public class PromotionPgDetail {
    /**
     * 活动编码
     */
    private String pCode;
    /**
     * 拼团详细编码
     */
    private String pDetailCode;
    /**
     * 成团人数
     */
    private int pieceGroupNum;
    /**
     * sku活动折扣
     */
    private int skuDiscount;
    /**
     * 创建人
     */
    private int creator;
    /**
     * 修改人
     */
    private int updator;
    /**
     * 创建时间
     */
    private int createdAt;
    /**
     * 修改时间
     */
    private int updatedAt;
    /**
     * 删除标记
     */
    private int isDeleted;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getUpdatedAt() {

        return updatedAt;
    }

    public void setUpdatedAt(int updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getCreatedAt() {

        return createdAt;
    }

    public void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }

    public int getUpdator() {

        return updator;
    }

    public void setUpdator(int updator) {
        this.updator = updator;
    }

    public int getCreator() {

        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public int getSkuDiscount() {

        return skuDiscount;
    }

    public void setSkuDiscount(int skuDiscount) {
        this.skuDiscount = skuDiscount;
    }

    public int getPieceGroupNum() {

        return pieceGroupNum;
    }

    public void setPieceGroupNum(int pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public String getpDetailCode() {

        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }
}
