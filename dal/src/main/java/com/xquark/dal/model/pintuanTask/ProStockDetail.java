package com.xquark.dal.model.pintuanTask;

import java.sql.Timestamp;
import java.util.Date;

public class ProStockDetail {
    private int id;
    private String pcode;
    private String  pdetail;
    private String skucode;
    private String  personid;
    private int num;
    private int type;
    private Timestamp tranTime;
    private Timestamp creatTime;
    private Timestamp updateTime;
    private int isDelete;

    public ProStockDetail() {
    }

    public ProStockDetail(int id, String pcode, String pdetail, String skucode, String personid, int num, int type, Timestamp tranTime, Timestamp creatTime, Timestamp updateTime, int isDelete) {
        this.id = id;
        this.pcode = pcode;
        this.pdetail = pdetail;
        this.skucode = skucode;
        this.personid = personid;
        this.num = num;
        this.type = type;
        this.tranTime = tranTime;
        this.creatTime = creatTime;
        this.updateTime = updateTime;
        this.isDelete = isDelete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPdetail() {
        return pdetail;
    }

    public void setPdetail(String pdetail) {
        this.pdetail = pdetail;
    }

    public String getSkucode() {
        return skucode;
    }

    public void setSkucode(String skucode) {
        this.skucode = skucode;
    }

    public String getPersonid() {
        return personid;
    }

    public void setPersonid(String personid) {
        this.personid = personid;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Timestamp getTranTime() {
        return tranTime;
    }

    public void setTranTime(Timestamp tranTime) {
        this.tranTime = tranTime;
    }

    public Timestamp getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Timestamp creatTime) {
        this.creatTime = creatTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }
}
