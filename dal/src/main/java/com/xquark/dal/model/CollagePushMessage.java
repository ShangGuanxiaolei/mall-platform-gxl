package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.sql.Timestamp;

/**
 * push消息实体类
 * @author tgb
 *
 */
public class CollagePushMessage extends BaseEntityImpl{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 会员ID
	 */
	private String belognTo;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 消息类型
	 */
	private String typeMsg;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 消息类型

	 */
	private String type;
	/**
	 * 消息读取状态 0:默认未读取，1：已读取
	 */
	private int status;
	/**
	 * 创建时间
	 */
	private Timestamp createAt;
	/**
	 * 创建时间
	 */
	private Timestamp updateAt;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getBelognTo() {
		return belognTo;
	}

	public void setBelognTo(String belognTo) {
		this.belognTo = belognTo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreateAt() {
		return createAt;
	}

	public Timestamp getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	public void setCreateAt(Timestamp createAt) {
		this.createAt = createAt;

	}

	public String getTypeMsg() {
		return typeMsg;
	}

	public void setTypeMsg(String typeMsg) {
		this.typeMsg = typeMsg;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
