package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.math.BigDecimal;

//组合实体明细类
public class CombinationItem extends BaseEntityImpl implements Archivable {
  //版本号
  private static final long serialVersionUID = 1L;
  //组合表id
  private String combinationId;
  //商品表id
  private String productId;
  //商品价格
  private BigDecimal price;
  //商品数量
  private Integer num;
  //逻辑删除
  private Boolean archive;

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getCombinationId() {
    return combinationId;
  }

  public void setCombinationId(String combinationId) {
    this.combinationId = combinationId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }

  @Override
  public String toString() {
    return super.toString()+"CombinationItem{" +
            "combinationId=" + combinationId +
            ", productId=" + productId +
            ", price=" + price +
            ", num=" + num +
            ", archive=" + archive +
            '}';
  }
}
