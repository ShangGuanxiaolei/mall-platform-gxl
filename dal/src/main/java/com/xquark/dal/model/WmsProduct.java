package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * User: kong Date: 18-6-9. Time: 上午9:31 WMS产品表
 */
public class WmsProduct extends BaseEntityImpl implements Archivable {

  private Long skuId;

  //产品编号
  private String code;
  //产品名称
  private String name;
  //装箱数
  private Integer numInPackage;
  //edi状态
  private Integer ediStatus;
  //更新方
  private String updatedBy;
  //条形码
  private String barCode;
  //仓库编号
  private Integer wareHouseId;
  //品牌名称
  private String brandName;
  //逻辑删除
  private Boolean archive;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSnp() {
    return numInPackage;
  }

  public void setSnp(Integer snp) {
    this.numInPackage = snp;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public Integer getWareHouseId() {
    return wareHouseId;
  }

  public void setWareHouseId(Integer wareHouseId) {
    this.wareHouseId = wareHouseId;
  }

  public Integer getNumInPackage() {
    return numInPackage;
  }

  public void setNumInPackage(Integer numInPackage) {
    this.numInPackage = numInPackage;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  @Override
  public String toString() {
    return "WmsProduct{" +
        "code='" + code + '\'' +
        ", name='" + name + '\'' +
        ", numInPackage=" + numInPackage +
        ", ediStatus=" + ediStatus +
        ", updatedBy='" + updatedBy + '\'' +
        ", archive=" + archive +
        '}';
  }
}
