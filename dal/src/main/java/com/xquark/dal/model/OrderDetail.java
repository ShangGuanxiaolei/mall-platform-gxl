package com.xquark.dal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * order_detail
 * @author kong
 */
public class OrderDetail implements Serializable {
    /**
     *订单明细主键
     */
    private String detailid;
    /**
     * 订单号，种子表生成
     */
    private String orderid;

    /**
     * 产品SKU编号
     */
    private String productSku;

    /**
     * 产品来源,1 hds, 2 vivilife, 3 ecommerce
     */
    private Long productSource;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 购买时单价
     */
    private BigDecimal unitprice;

    /**
     * 套装和套装产品差异价(+/-)
     */
    private BigDecimal priceDiff;

    /**
     * 税费
     */
    private BigDecimal taxamt;

    /**
     * 税率
     */
    private BigDecimal taxrate;

    /**
     * 购买时VP值
     */
    private BigDecimal vp;

    /**
     * 推广费
     */
    private BigDecimal promoAmt;

    /**
     * 服务费
     */
    private BigDecimal serverAmt;

    /**
     * 立省; 会员价
     */
    private BigDecimal savingAmt;

    /**
     * 购买时积分
     */
    private BigDecimal points;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedAt;

    /**
     * 逻辑删除标记，0未删，1已删。
     */
    private Integer isdeleted;

    private static final long serialVersionUID = 1L;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public Long getProductSource() {
        return productSource;
    }

    public void setProductSource(Long productSource) {
        this.productSource = productSource;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public BigDecimal getPriceDiff() {
        return priceDiff;
    }

    public void setPriceDiff(BigDecimal priceDiff) {
        this.priceDiff = priceDiff;
    }

    public BigDecimal getTaxamt() {
        return taxamt;
    }

    public void setTaxamt(BigDecimal taxamt) {
        this.taxamt = taxamt;
    }

    public BigDecimal getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(BigDecimal taxrate) {
        this.taxrate = taxrate;
    }

    public BigDecimal getVp() {
        return vp;
    }

    public void setVp(BigDecimal vp) {
        this.vp = vp;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Integer isdeleted) {
        this.isdeleted = isdeleted;
    }

    public String getDetailid() {
        return detailid;
    }

    public void setDetailid(String detailid) {
        this.detailid = detailid;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getSavingAmt() {
        return savingAmt;
    }

    public void setSavingAmt(BigDecimal savingAmt) {
        this.savingAmt = savingAmt;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OrderDetail other = (OrderDetail) that;
        return (this.getOrderid() == null ? other.getOrderid() == null : this.getOrderid().equals(other.getOrderid()))
            && (this.getProductSku() == null ? other.getProductSku() == null : this.getProductSku().equals(other.getProductSku()))
            && (this.getProductSource() == null ? other.getProductSource() == null : this.getProductSource().equals(other.getProductSource()))
            && (this.getQuantity() == null ? other.getQuantity() == null : this.getQuantity().equals(other.getQuantity()))
            && (this.getUnitprice() == null ? other.getUnitprice() == null : this.getUnitprice().equals(other.getUnitprice()))
            && (this.getPriceDiff() == null ? other.getPriceDiff() == null : this.getPriceDiff().equals(other.getPriceDiff()))
            && (this.getTaxamt() == null ? other.getTaxamt() == null : this.getTaxamt().equals(other.getTaxamt()))
            && (this.getTaxrate() == null ? other.getTaxrate() == null : this.getTaxrate().equals(other.getTaxrate()))
            && (this.getVp() == null ? other.getVp() == null : this.getVp().equals(other.getVp()))
            && (this.getPromoAmt() == null ? other.getPromoAmt() == null : this.getPromoAmt().equals(other.getPromoAmt()))
            && (this.getPoints() == null ? other.getPoints() == null : this.getPoints().equals(other.getPoints()))
            && (this.getCreatedBy() == null ? other.getCreatedBy() == null : this.getCreatedBy().equals(other.getCreatedBy()))
            && (this.getCreatedAt() == null ? other.getCreatedAt() == null : this.getCreatedAt().equals(other.getCreatedAt()))
            && (this.getUpdatedBy() == null ? other.getUpdatedBy() == null : this.getUpdatedBy().equals(other.getUpdatedBy()))
            && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null : this.getUpdatedAt().equals(other.getUpdatedAt()))
            && (this.getIsdeleted() == null ? other.getIsdeleted() == null : this.getIsdeleted().equals(other.getIsdeleted()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderid() == null) ? 0 : getOrderid().hashCode());
        result = prime * result + ((getProductSku() == null) ? 0 : getProductSku().hashCode());
        result = prime * result + ((getProductSource() == null) ? 0 : getProductSource().hashCode());
        result = prime * result + ((getQuantity() == null) ? 0 : getQuantity().hashCode());
        result = prime * result + ((getUnitprice() == null) ? 0 : getUnitprice().hashCode());
        result = prime * result + ((getPriceDiff() == null) ? 0 : getPriceDiff().hashCode());
        result = prime * result + ((getTaxamt() == null) ? 0 : getTaxamt().hashCode());
        result = prime * result + ((getTaxrate() == null) ? 0 : getTaxrate().hashCode());
        result = prime * result + ((getVp() == null) ? 0 : getVp().hashCode());
        result = prime * result + ((getPromoAmt() == null) ? 0 : getPromoAmt().hashCode());
        result = prime * result + ((getPoints() == null) ? 0 : getPoints().hashCode());
        result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
        result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
        result = prime * result + ((getUpdatedBy() == null) ? 0 : getUpdatedBy().hashCode());
        result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
        result = prime * result + ((getIsdeleted() == null) ? 0 : getIsdeleted().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderid=").append(orderid);
        sb.append(", productSku=").append(productSku);
        sb.append(", productSource=").append(productSource);
        sb.append(", quantity=").append(quantity);
        sb.append(", unitprice=").append(unitprice);
        sb.append(", priceDiff=").append(priceDiff);
        sb.append(", taxamt=").append(taxamt);
        sb.append(", taxrate=").append(taxrate);
        sb.append(", vp=").append(vp);
        sb.append(", promoAmt=").append(promoAmt);
        sb.append(", points=").append(points);
        sb.append(", createdBy=").append(createdBy);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedBy=").append(updatedBy);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", isdeleted=").append(isdeleted);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}