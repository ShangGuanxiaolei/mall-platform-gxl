package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

public class FootOrderItem {
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String productImg;
    private String productName;
    private BigDecimal price;
    private BigDecimal totalPrice;

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    private int amount;

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
