package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;

/**
 * 分佣规则定义
 */
public class CommissionRule extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private CommissionPolicy policyName; // 规则集名称

  private CommissionRuleName ruleName; // 规则名称

  private String description; // 规则描述

  private int index; // 规则的顺序

  private String params; // 规则的参数

  private Boolean archive;

  public CommissionPolicy getPolicyName() {
    return policyName;
  }

  public void setPolicyName(CommissionPolicy policyName) {
    this.policyName = policyName;
  }

  public CommissionRuleName getRuleName() {
    return ruleName;
  }

  public void setRuleName(CommissionRuleName ruleName) {
    this.ruleName = ruleName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public String getParams() {
    return params;
  }

  public void setParams(String params) {
    this.params = params;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
