package com.xquark.dal.model;


/**
 * Created by freedom on 15/11/9.
 */
public class CategoryActivity {

  private String id;

  private String name;

  private String activiyId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getActiviyId() {
    return activiyId;
  }

  public void setActiviyId(String activiyId) {
    this.activiyId = activiyId;
  }
}
