package com.xquark.dal.model.wechat;

/**
 * 微信公众号菜单按钮的类型
 */
public enum ButtonType {
  CLICK_RETURN_TEXT, // 点击推送文本消息
  CLICK_REDIRECT_VIEW, // 点击跳转URL
  CLICK_RETURN_MEDIA_ID, // 推送永久类图文消息
  HAS_SUB_BUTTON, // 包含二级菜单项的一级菜单按钮
}
