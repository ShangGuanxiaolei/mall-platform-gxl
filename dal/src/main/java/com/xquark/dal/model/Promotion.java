package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Optional;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.CouponView;
import com.xquark.utils.RandomStringUtil;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @author ahlon
 */
public class Promotion extends BaseEntityImpl implements CouponView, PromotionAble {

  private static final long serialVersionUID = 4485622304685973150L;

  /**
   * 优惠活动标题
   */
  private String title;

  /**
   * 优惠活动详情
   */
  private String details;

  /**
   * 优惠活动生效范围 默认范围为自选
   */
  private PromotionScope scope = PromotionScope.PRODUCT;

  /**
   * 活动针对人群
   */
  private PromotionUserScope userScope = PromotionUserScope.ALL;

  /**
   * 折扣
   */
  private BigDecimal discount;

  /**
   * 活动开始时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validFrom;

  /**
   * 活动结束时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validTo;

  /**
   * 支付开始时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date payFrom;

  /**
   * 支付结束时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date payTo;

  /**
   * 每个人可预约数量
   */
  private Integer buyLimit;

  /**
   * 每个人已预约的数量
   */
  private Integer buyCount;

  /**
   * 前端已有多少人预约(先取固定值，如果实际值大于固定值，再取实际值)
   */
  private Integer reserveCount;

  /**
   * 活动类型（团购，限时抢购等）
   */
  private PromotionType type;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String subImg;

  private String originImg;

  /**
   * 是否与其他活动共享
   */
  private Boolean isShare = Boolean.FALSE;

  /**
   * 是否只有原价享受优惠 沁园平台不需要该字段
   */
  @Deprecated
  private Boolean originalPriceOnly = Boolean.FALSE;

  /**
   * 标识活动是否结束
   */
  private Boolean closed = false;

  /**
   * 活动是否享受免邮
   */
  private Boolean isFreeDelivery = false;

  /**
   * 是否支持使用德分 - 默认支持
   */
  private Boolean isPointUsed = true;

  /**
   * 是否支持使用德分 - 默认支持
   */
  private Boolean isCutDownUsed = true;

  /**
   * 活动是否支持计算收益
   */
  private Boolean isCountEarning = true;

  /**
   * 附加字段
   */
  private Map<String, Object> commentMap;

  /**
   * 活动价格
   */
  private BigDecimal price;

  /**
   * 兑换价
   */
  private BigDecimal conversionPrice;

  /**
   * 兑换德分
   */
  private BigDecimal point;

  /**
   * 德分是否同步打折
   */
  private boolean isPointDiscount = true;

  /**
   * 是否显示倒计时
   */
  private Boolean isShowTime;

  private Boolean isShowProgress;

  private String identities;

  /**
   * 批次号
   */
  private String batchNo;

  /**
   * 邮费
   */
  private BigDecimal logisticsFee;

  private int selfOperated;

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  /**
   * 补贴的用户id
   */
  private String allowanceUserId;

  private Boolean archive = false;

  /**
   * 是否免单
   */
  private boolean isPayFree;

  public boolean isPayFree() {
    return isPayFree;
  }

  public void setPayFree(boolean payFree) {
    isPayFree = payFree;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public Map<String, Object> getCommentMap() {
    return commentMap;
  }

  public void setCommentMap(Map<String, Object> commentMap) {
    this.commentMap = commentMap;
  }

  public Boolean getIsShare() {
    return isShare;
  }

  public void setIsShare(Boolean isShare) {
    this.isShare = isShare;
  }

  public Boolean getOriginalPriceOnly() {
    return originalPriceOnly;
  }

  public void setOriginalPriceOnly(Boolean originalPriceOnly) {
    this.originalPriceOnly = originalPriceOnly;
  }

  public String getOriginImg() {
    return originImg;
  }

  public void setOriginImg(String originImg) {
    this.originImg = originImg;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getSubImg() {
    return subImg;
  }

  public void setSubImg(String subImg) {
    this.subImg = subImg;
  }

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public PromotionType getPromotionType() {
    return type;
  }

  public void setType(PromotionType type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public PromotionScope getScope() {
    return scope;
  }

  public void setScope(PromotionScope scope) {
    this.scope = scope;
  }

  public Boolean getCutDownUsed() {
    return isCutDownUsed;
  }

  public void setCutDownUsed(Boolean cutDownUsed) {
    isCutDownUsed = cutDownUsed;
  }

  @Override
  public String getCouponName() {
    return this.title;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  /**
   * 小数形式的折扣
   */
  public BigDecimal getRealDiscount() {
    if (discount != null) {
      return discount;
    }
    return BigDecimal.ZERO;
  }

  @Override
  public String getCouponType() {
    return getPromotionType().getcName();
  }

  @Override
  public String getUuId() {
    return RandomStringUtil.create();
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getAllowanceUserId() {
    return allowanceUserId;
  }

  public void setAllowanceUserId(String allowanceUserId) {
    this.allowanceUserId = allowanceUserId;
  }

  public Boolean getClosed() {
    return closed;
  }

  public void setClosed(Boolean closed) {
    this.closed = closed;
  }

  public PromotionUserScope getUserScope() {
    return userScope;
  }

  public void setUserScope(PromotionUserScope userScope) {
    this.userScope = userScope;
  }

  public Boolean getIsFreeDelivery() {
    return Optional.fromNullable(isFreeDelivery).or(Boolean.FALSE);
  }

  public void setIsFreeDelivery(Boolean freeDelivery) {
    isFreeDelivery = freeDelivery;
  }

  public Boolean getIsPointUsed() {
    return isPointUsed;
  }

  public void setIsPointUsed(Boolean pointUsed) {
    isPointUsed = pointUsed;
  }

  public Boolean getIsCountEarning() {
    return isCountEarning;
  }

  public void setIsCountEarning(Boolean countEarning) {
    isCountEarning = countEarning;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getConversionPrice() {
    return conversionPrice;
  }

  public void setConversionPrice(BigDecimal conversionPrice) {
    this.conversionPrice = conversionPrice;
  }

  /**
   * @deprecated 废弃，使用价格体系的德分
   */
  @Deprecated
  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public Boolean getIsShowTime() {
    return isShowTime;
  }

  public void setIsShowTime(Boolean showTime) {
    isShowTime = showTime;
  }

  public Boolean getIsShowProgress() {
    return isShowProgress;
  }

  public void setIsShowProgress(Boolean showProgress) {
    isShowProgress = showProgress;
  }

  public String getIdentities() {
    return identities;
  }

  public void setIdentities(String identities) {
    this.identities = identities;
  }

  public boolean isPointDiscount() {
    return isPointDiscount;
  }

  public void setPointDiscount(boolean pointDiscount) {
    isPointDiscount = pointDiscount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Promotion promotion = (Promotion) o;

    if (!title.equals(promotion.title)) {
      return false;
    }
    if (!validFrom.equals(promotion.validFrom)) {
      return false;
    }
    if (!validTo.equals(promotion.validTo)) {
      return false;
    }
    if (type != promotion.type) {
      return false;
    }
    return archive.equals(promotion.archive);
  }

  @Override
  public int hashCode() {
    int result = title.hashCode();
    result = 31 * result + validFrom.hashCode();
    result = 31 * result + validTo.hashCode();
    result = 31 * result + type.hashCode();
    result = 31 * result + archive.hashCode();
    return result;
  }

  @Override
  public int compareTo(CouponView o) {
    if (o == null) {
      return 1;
    }
    BigDecimal thisDiscount = this.getDiscount();
    BigDecimal oDiscount = o.getDiscount();
    if (thisDiscount == null) {
      return -1;
    }
    if (oDiscount == null) {
      return 1;
    }
    // 倒序排
    return thisDiscount.compareTo(oDiscount);
  }

  public Date getPayFrom() {
    return payFrom;
  }

  public void setPayFrom(Date payFrom) {
    this.payFrom = payFrom;
  }

  public Date getPayTo() {
    return payTo;
  }

  public void setPayTo(Date payTo) {
    this.payTo = payTo;
  }

  public Integer getBuyLimit() {
    return buyLimit;
  }

  public void setBuyLimit(Integer buyLimit) {
    this.buyLimit = buyLimit;
  }

  public Integer getBuyCount() {
    return buyCount == null ? 0 : buyCount;
  }

  public void setBuyCount(Integer buyCount) {
    this.buyCount = buyCount;
  }

  public Integer getReserveCount() {
    return reserveCount;
  }

  public void setReserveCount(Integer reserveCount) {
    this.reserveCount = reserveCount;
  }

  public PromotionType getType() {
    return type;
  }
}
