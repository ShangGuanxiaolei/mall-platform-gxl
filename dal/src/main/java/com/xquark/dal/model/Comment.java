package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CommentType;

/**
 * @author jitre 评论
 */
@ApiModel(value = "评论对象")
public class Comment extends BaseEntityImpl implements Archivable {

  /**
   * 评论的内容
   */
  @ApiModelProperty("评论的内容")
  private String content;

  /**
   * 评论对象的类型
   */
  @ApiModelProperty("评论的类型,PRODUCT")
  private CommentType type;

  private Integer star;

  private Boolean archive;

  /**
   * 被评论对象的id
   */
  @ApiModelProperty("被评论对象的id,也就是文章或产品的id")
  private String objId;

  /**
   * 评论用户的ｉｄ
   */
  @ApiModelProperty("无需传入, 默认取当前用户id")
  private String userId;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  private Integer likeCount;

  private static final long serialVersionUID = 1L;

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public CommentType getType() {
    return type;
  }

  public void setType(CommentType type) {
    this.type = type;
  }

  public Integer getStar() {
    return star;
  }

  public void setStar(Integer star) {
    this.star = star;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getObjId() {
    return objId;
  }

  public void setObjId(String objId) {
    this.objId = objId;
  }

  public Integer getLikeCount() {
    return likeCount;
  }

  public void setLikeCount(Integer likeCount) {
    this.likeCount = likeCount;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    Comment other = (Comment) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
    result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
    result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
    result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
    result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
    result = prime * result + ((getObjId() == null) ? 0 : getObjId().hashCode());
    result = prime * result + ((getLikeCount() == null) ? 0 : getLikeCount().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(this.getId());
    sb.append(", createdAt=").append(this.getCreatedAt());
    sb.append(", updatedAt=").append(this.getUpdatedAt());
    sb.append(", content=").append(content);
    sb.append(", type=").append(type);
    sb.append(", archive=").append(archive);
    sb.append(", objId=").append(objId);
    sb.append(", likeCount=").append(likeCount);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}