package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.FilterSpec;
import com.xquark.dal.status.ProductAvailableStatus;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * 商品model类 其中price为sku中最低的价格 amount为sku中总的库存 price和amout为冗余字段，为方便前端显示 descImg：存放商品描述的图片，
 * 包含img的值，以逗号相隔
 *
 * @author huxaya
 */

public class Product extends BaseEntityImpl implements Archivable, CanTag {

  private static final long serialVersionUID = 1L;
  @ApiModelProperty(value = "商品名称")
  private String name; // 商品名称
  private String encode; // 沁园商品编码
  private String u8Encode; // 新增的U8编码
  private String model; // 沁园商品型号


  private ProductType type; // 商品类型
  private Integer level; //滤芯等级
  private FilterSpec filterSpec;
  private ProductAvailableStatus avaliableStatus;//商品的库存状态

  @ApiModelProperty(value = "商品编码")
  private String code; // 商品编码
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;  // 主图code
  private String userId;
  private String shopId;
  @ApiModelProperty(value = "商品状态，上架，下架等")
  private ProductStatus status;
  private ProductReviewStatus reviewStatus;//商品审核状态
  @ApiModelProperty(value = "商品描述")
  private String description;
  @ApiModelProperty(value = "商品是否已经删除")
  private Boolean archive;
  private Boolean enableDesc;
  private String detailH5;//商品的h5详情
  @ApiModelProperty(value = "商品市场价")
  private BigDecimal marketPrice; // 市场价
  @ApiModelProperty(value = "商品价格")
  private BigDecimal price; //最低价格
  private BigDecimal point; //商品返利积分
  private BigDecimal deductionDPoint; //可用德分抵扣值
  private BigDecimal netWorth; //净值
  private BigDecimal serverAmt; // 服务费
  private BigDecimal promoAmt; // 推广费
  @ApiModelProperty(value = "原始价格")
  private BigDecimal originalPrice; //原始价格（用户录入价格）
  @ApiModelProperty(value = "商品销售额")
  private Integer sales;//产品销量
  private Boolean freePostage = false;
  private BigInteger thirdItemId; //第三方同步id
  private String source; // 商品来源
  private Integer isCrossBorder;// 是否跨境：是否跨境(1：是，0：否)
  //是否是特价商品  seker 20150114增加
  private Boolean productSP = false;
  // add start by liangfan 20150825 for distribution
  private String sourceProductId;// 分销商品id
  private String kind;// 分销商品id
  private String sourceShopId;//   分销商店铺id
  private Boolean isDistribution;//商品类别  自营 0（或者空） or 分销 1
  // add end by liangfan 20150825 for distribution
  private Boolean oneyuanPurchase;//是否一元购商品
  private BigDecimal oneyuanPostage; //一元购商品邮费
  private BigDecimal specialRate; //特殊商品佣金
  private int selfOperated; //是否自营:0非自营，1自营
  private Long decodeId; //解码后的id
  private boolean stadiumOpen;//分会场是否开馆

  public boolean isStadiumOpen() {
    return stadiumOpen;
  }

  public void setStadiumOpen(boolean stadiumOpen) {
    this.stadiumOpen = stadiumOpen;
  }

  public Long getDecodeId() {
    if(StringUtils.isBlank(this.getId())){
      return this.decodeId;
    }
    return IdTypeHandler.decode(this.getId());
  }

  public void setDecodeId(Long decodeId) {
    this.decodeId = decodeId;
  }

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public String getDeliveryRegion() {
    return deliveryRegion;
  }

  public void setDeliveryRegion(String deliveryRegion) {
    this.deliveryRegion = deliveryRegion;
  }

  private String deliveryRegion; //区域配送省份

  private Boolean supportRefund;

  private Boolean isDNAProduct = false; //是否是基因产品

    public Boolean getIsDNAProduct() {
        return isDNAProduct;
    }

    public void setDNAProduct(Boolean DNAProduct) {
        isDNAProduct = DNAProduct;
    }

    private Integer numInPackage;


  private String wareHouseId;

  private String supplierId;

  private Integer isCommission;

  private long newactivityId;

  private ProductFeatures features;

  private Long feature0;
  private Long feature1;

  public String getDetailH5() {
    return detailH5;
  }

  public void setDetailH5(String detailH5) {
    this.detailH5 = detailH5;
  }

  private String refundAddress;

  private Double height;
  private Double width;
  private Double length;
  private LengthUnit lengthUnit = LengthUnit.CM;

  private String refundTel;

  private String refundName;

  private LogisticsType logisticsType;

  private BigDecimal uniformValue;

  private String templateValue;

  private Integer priority;

  @ApiModelProperty(value = "是否赠品")
  protected Boolean gift;

  @ApiModelProperty(value = "商品重量")
  private Integer weight; //商品重量

  @ApiModelProperty(value = "商品对应的规格属性id，如颜色，尺码等,如果值为空，代表该商品不是多规格，sku只会有一条记录")
  private String attributes; // 商品对应的规格属性id，如颜色，尺码等

  @ApiModelProperty(value = "积分兑换比例")
  private Integer yundouScale;

  @ApiModelProperty(value = "最少需要的RMB数")
  private BigDecimal minYundouPrice;

  private Integer sfairline;
  private Integer sfshipping;
  private String merchantNumber;
  private String skuCode;//sku编码
  private Integer secureAmount;//安全库存
  private String barCode;//产品条形码
  //商品源productId
  private Long sourceId;

  /**
   * 限购件数
   */
  private Integer buyLimit;

  public Integer getBuyLimit() {
    return buyLimit;
  }

  public void setBuyLimit(Integer buyLimit) {
    this.buyLimit = buyLimit;
  }

  public Long getSourceId() {
    return sourceId;
  }

  public void setSourceId(Long sourceId) {
    this.sourceId = sourceId;
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  private BigDecimal memberPrice = BigDecimal.ZERO;
  private BigDecimal proxyPrice = BigDecimal.ZERO;
  private BigDecimal changePrice = BigDecimal.ZERO;

  private BigDecimal savedPrice = BigDecimal.ZERO;//已省
  private BigDecimal earnedPrice = BigDecimal.ZERO;//已赚

  public Integer getSecureAmount() {
    return secureAmount;
  }

  public void setSecureAmount(Integer secureAmount) {
    this.secureAmount = secureAmount;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getAttributes() {
    return attributes;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public Integer getYundouScale() {
    return yundouScale;
  }

  public void setYundouScale(Integer yundouScale) {
    this.yundouScale = yundouScale;
  }

  public BigDecimal getMinYundouPrice() {
    return minYundouPrice;
  }

  public void setMinYundouPrice(BigDecimal minYundouPrice) {
    this.minYundouPrice = minYundouPrice;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public Double getHeight() {
    return height;
  }

  public BigDecimal getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(BigDecimal netWorth) {
    this.netWorth = netWorth;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(Double width) {
    this.width = width;
  }

  public Double getLength() {
    return length;
  }

  public void setLength(Double length) {
    this.length = length;
  }

  public Integer getNumInPackage() {
    return numInPackage;
  }

  public void setNumInPackage(Integer numInPackage) {
    this.numInPackage = numInPackage;
  }

  public String getWareHouseId() {
    return StringUtils.isNotBlank(wareHouseId) ? wareHouseId : null;
  }

  public void setWareHouseId(String wareHouseId) {
    this.wareHouseId = wareHouseId;
  }

  public LengthUnit getLengthUnit() {
    return lengthUnit;
  }

  public void setLengthUnit(LengthUnit lengthUnit) {
    this.lengthUnit = lengthUnit;
  }

  public LogisticsType getLogisticsType() {
    return logisticsType;
  }

  public void setLogisticsType(LogisticsType logisticsType) {
    this.logisticsType = logisticsType;
  }

  public BigDecimal getUniformValue() {
    return uniformValue;
  }

  public void setUniformValue(BigDecimal uniformValue) {
    this.uniformValue = uniformValue;
  }

  public String getTemplateValue() {
    return templateValue;
  }

  public void setTemplateValue(String templateValue) {
    this.templateValue = templateValue;
  }

  public Boolean getGift() {
    return gift;
  }

  public void setGift(Boolean gift) {
    this.gift = gift;
  }

  public String getRefundAddress() {
    return refundAddress;
  }

  public void setRefundAddress(String refundAddress) {
    this.refundAddress = refundAddress;
  }

  public String getRefundTel() {
    return refundTel;
  }

  public void setRefundTel(String refundTel) {
    this.refundTel = refundTel;
  }

  public String getRefundName() {
    return refundName;
  }

  public void setRefundName(String refundName) {
    this.refundName = refundName;
  }

  public BigDecimal getSpecialRate() {
    return specialRate;
  }

  public void setSpecialRate(BigDecimal specialRate) {
    this.specialRate = specialRate;
  }

  public BigDecimal getOneyuanPostage() {
    return oneyuanPostage;
  }

  public void setOneyuanPostage(BigDecimal oneyuanPostage) {
    this.oneyuanPostage = oneyuanPostage;
  }

  public Boolean getOneyuanPurchase() {
    return oneyuanPurchase;
  }

  public void setOneyuanPurchase(Boolean oneyuanPurchase) {
    this.oneyuanPurchase = oneyuanPurchase;
  }

  public Boolean getSupportRefund() {
    return supportRefund;
  }

  public void setSupportRefund(Boolean supportRefund) {
    this.supportRefund = supportRefund;
  }

  public long getNewactivityId() {
    return newactivityId;
  }

  public void setNewactivityId(long newactivityId) {
    this.newactivityId = newactivityId;
  }

  public Integer getIsCommission() {
    return isCommission;
  }

  public void setIsCommission(Integer isCommission) {
    this.isCommission = isCommission;
  }

  public String getSourceProductId() {
    return sourceProductId;
  }

  public void setSourceProductId(String sourceProductId) {
    this.sourceProductId = sourceProductId;
  }

  public String getSourceShopId() {
    return sourceShopId;
  }

  public void setSourceShopId(String sourceShopId) {
    this.sourceShopId = sourceShopId;
  }

  public Boolean getIsDistribution() {
    if (isDistribution == null) {
      return false;
    }
    return isDistribution;
  }

  public void setIsDistribution(Boolean isDistribution) {
    this.isDistribution = isDistribution;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public Boolean getUpdateLock() {
    return updateLock;
  }

  public void setUpdateLock(Boolean updateLock) {
    this.updateLock = updateLock;
  }

  public Integer getDelayed() {
    return delayed;
  }

  public void setDelayed(Integer delayed) {
    this.delayed = delayed;
  }

  public String getSynchronousFlag() {
    return synchronousFlag;
  }

  public void setSynchronousFlag(String synchronousFlag) {
    this.synchronousFlag = synchronousFlag;
  }

  private Integer fakeSales;// 零时使用产品销量
  private Integer amount;//库存
  private Integer imgWidth;
  private Integer imgHeight;
  private Boolean recommend;
  private Date recommendAt;
  private BigDecimal commissionRate; // 分佣比例
  private Float discount; //折扣，如1.1， 2.2
  private Date forsaleAt; //待发布时间
  private Date onsaleAt;//上架时间
  private Date instockAt;//下架时间
  private Boolean updateLock; // 1 不能修改商品信息，0 能修改商品信息
  private boolean isCollected; //是否收藏

  public ProductAvailableStatus getAvaliableStatus() {
    return avaliableStatus;
  }

  public boolean getIsCollected() {
    return isCollected;
  }

  public void setCollected(boolean collected) {
    isCollected = collected;
  }

  public void setAvaliableStatus(ProductAvailableStatus avaliableStatus) {
    this.avaliableStatus = avaliableStatus;
  }

  private Integer delayAt = 0;//延迟发货时间（天）  （delayed==0的时候才有效）
  private Integer delayed;

  private String synchronousFlag;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getEncode() {
    return encode;
  }

  public void setEncode(String encode) {
    this.encode = encode;
  }

  public String getU8Encode() {
    return u8Encode;
  }

  public void setU8Encode(String u8Encode) {
    this.u8Encode = u8Encode;
  }

  public ProductType getType() {
    return type;
  }

  public void setType(ProductType type) {
    this.type = type;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Integer getSales() {
    return sales;
  }

  public void setSales(Integer sales) {
    this.sales = sales;
  }

  public Boolean getRecommend() {
    return recommend;
  }

  public void setRecommend(Boolean recommend) {
    this.recommend = recommend;
  }

  public Date getRecommendAt() {
    return recommendAt;
  }

  public void setRecommendAt(Date recommendAt) {
    this.recommendAt = recommendAt;
  }

  public Float getDiscount() {
    return discount;
  }

  public void setDiscount(Float discount) {
    this.discount = discount;
  }

  public Date getForsaleAt() {
    return forsaleAt;
  }

  public void setForsaleAt(Date forsaleAt) {
    this.forsaleAt = forsaleAt;
  }

  public Date getOnsaleAt() {
    return onsaleAt;
  }

  public void setOnsaleAt(Date onsaleAt) {
    this.onsaleAt = onsaleAt;
  }

  public Date getInstockAt() {
    return instockAt;
  }

  public void setInstockAt(Date instockAt) {
    this.instockAt = instockAt;
  }

  public BigDecimal getCommissionRate() {
    return commissionRate;
  }

  public void setCommissionRate(BigDecimal commissionRate) {
    this.commissionRate = commissionRate;
  }

  public Integer getImgWidth() {
    return imgWidth;
  }

  public void setImgWidth(Integer imgWidth) {
    this.imgWidth = imgWidth;
  }

  public Integer getImgHeight() {
    return imgHeight;
  }

  public void setImgHeight(Integer imgHeight) {
    this.imgHeight = imgHeight;
  }

  public Integer getFakeSales() {
    return fakeSales;
  }

  public void setFakeSales(Integer fakeSales) {
    this.fakeSales = fakeSales;
  }

  public Integer getDelayAt() {
    return delayAt;
  }

  public void setDelayAt(Integer delayAt) {
    this.delayAt = delayAt;
  }

  public Boolean getFreePostage() {
    return freePostage;
  }

  public void setFreePostage(Boolean freePostage) {
    this.freePostage = freePostage;
  }


  public Boolean getProductSP() {
    return productSP;
  }

  public void setProductSP(Boolean productSP) {
    this.productSP = productSP;
  }

  public BigInteger getThirdItemId() {
    return thirdItemId;
  }

  public void setThirdItemId(BigInteger thirdItemId) {
    this.thirdItemId = thirdItemId;
  }

  public Integer getLevel() {
    return level;
  }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }


  public void setLevel(Integer level) {
    this.level = level;
  }

  public FilterSpec getFilterSpec() {
    return filterSpec;
  }

  public void setFilterSpec(FilterSpec filterSpec) {
    this.filterSpec = filterSpec;
  }

  public String getCode() {
    return code;
  }


  public void setCode(String code) {
    this.code = code;
  }

  public Boolean getEnableDesc() {
    return enableDesc;
  }

  public void setEnableDesc(Boolean enableDesc) {
    this.enableDesc = enableDesc;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public void setSource(ProductSource source) {
      if (source != null) {
        this.source = source.name();
      }
  }

  public Integer getIsCrossBorder() {
    return isCrossBorder;
  }

  public ProductReviewStatus getReviewStatus() {
    return reviewStatus;
  }

  public String getKind() {
    return kind;
  }

  public void setKind(String kind) {
    this.kind = kind;
  }

  public void setReviewStatus(ProductReviewStatus reviewStatus) {
    this.reviewStatus = reviewStatus;
  }

  public String getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(String supplierId) {
    this.supplierId = supplierId;
  }

  public void setIsCrossBorder(Integer isCrossBorder) {
    this.isCrossBorder = isCrossBorder;
  }

  public Long getFeature0() {
    return feature0;
  }

  public void setFeature0(Long feature0) {
    this.feature0 = feature0;
  }

  public Long getFeature1() {
    return feature1;
  }

  public void setFeature1(Long feature1) {
    this.feature1 = feature1;
  }

  public Integer getSfairline() {
    return sfairline;
  }

  public void setSfairline(Integer sfairline) {
    this.sfairline = sfairline;
  }

  public Integer getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(Integer sfshipping) {
    this.sfshipping = sfshipping;
  }

  public String getMerchantNumber() {
    return merchantNumber;
  }

  public void setMerchantNumber(String merchantNumber) {
    this.merchantNumber = merchantNumber;
  }

  public BigDecimal getMemberPrice() {
    return memberPrice;
  }

  public void setMemberPrice(BigDecimal memberPrice) {
    this.memberPrice = memberPrice;
  }

  public BigDecimal getProxyPrice() {
    return proxyPrice;
  }

  public void setProxyPrice(BigDecimal proxyPrice) {
    this.proxyPrice = proxyPrice;
  }

  public BigDecimal getChangePrice() {
    return changePrice;
  }

  public void setChangePrice(BigDecimal changePrice) {
    this.changePrice = changePrice;
  }

  public BigDecimal getSavedPrice() {
    return savedPrice;
  }

  public void setSavedPrice(BigDecimal savedPrice) {
    this.savedPrice = savedPrice;
  }

  public BigDecimal getEarnedPrice() {
    return earnedPrice;
  }

  public void setEarnedPrice(BigDecimal earnedPrice) {
    this.earnedPrice = earnedPrice;
  }

  public boolean getFeature(ProductFeaturesKeyType featureKey) {
    if (this.features == null) {
      this.features = new ProductFeatures(feature0, feature1);
    }
    return features.getFeature(featureKey);
  }

  public void setFeature(ProductFeaturesKeyType featureKey, boolean fVal) {
    if (this.features == null) {
      this.features = new ProductFeatures(feature0, feature1);
    }
    features.setFeature(featureKey, fVal);
    //sync to primitive value for persistence
    Long word = features.getWord(featureKey);
    int wordIndex = features.getWordIndex(featureKey);
    switch (wordIndex) {
      case 0:
        feature0 = word;
        break;
      case 1:
        feature1 = word;
    }
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
        prime * result + ((getId() == null) ? 0 : getId().hashCode()) + ((getName() == null) ? 0
            : getName().hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    Product other = (Product) obj;

    // 如果比较非数据库查询出来的对象还需要其他属性
    if (getId() == null && other.getId() != null) {
      return false;
    }

  if (!getId().equals(other.getId())) {
      return false;
    }

    return true;
  }

  @Override
  public String toString() {
    return "Product{" +
            "name='" + name + '\'' +
            ", encode='" + encode + '\'' +
            ", u8Encode='" + u8Encode + '\'' +
            ", model='" + model + '\'' +
            ", type=" + type +
            ", level=" + level +
            ", filterSpec=" + filterSpec +
            ", avaliableStatus=" + avaliableStatus +
            ", code='" + code + '\'' +
            ", img='" + img + '\'' +
            ", userId='" + userId + '\'' +
            ", shopId='" + shopId + '\'' +
            ", status=" + status +
            ", reviewStatus=" + reviewStatus +
            ", description='" + description + '\'' +
            ", archive=" + archive +
            ", enableDesc=" + enableDesc +
            ", detailH5='" + detailH5 + '\'' +
            ", marketPrice=" + marketPrice +
            ", price=" + price +
            ", point=" + point +
            ", deductionDPoint=" + deductionDPoint +
            ", originalPrice=" + originalPrice +
            ", sales=" + sales +
            ", freePostage=" + freePostage +
            ", thirdItemId=" + thirdItemId +
            ", source=" + source +
            ", isCrossBorder=" + isCrossBorder +
            ", productSP=" + productSP +
            ", sourceProductId='" + sourceProductId + '\'' +
            ", kind='" + kind + '\'' +
            ", sourceShopId='" + sourceShopId + '\'' +
            ", isDistribution=" + isDistribution +
            ", oneyuanPurchase=" + oneyuanPurchase +
            ", oneyuanPostage=" + oneyuanPostage +
            ", specialRate=" + specialRate +
            ", supportRefund=" + supportRefund +
            ", numInPackage=" + numInPackage +
            ", wareHouseId='" + wareHouseId + '\'' +
            ", isCommission=" + isCommission +
            ", newactivityId=" + newactivityId +
            ", features=" + features +
            ", feature0=" + feature0 +
            ", feature1=" + feature1 +
            ", refundAddress='" + refundAddress + '\'' +
            ", height=" + height +
            ", width=" + width +
            ", length=" + length +
            ", lengthUnit=" + lengthUnit +
            ", refundTel='" + refundTel + '\'' +
            ", refundName='" + refundName + '\'' +
            ", logisticsType=" + logisticsType +
            ", uniformValue=" + uniformValue +
            ", templateValue='" + templateValue + '\'' +
            ", priority=" + priority +
            ", gift=" + gift +
            ", weight=" + weight +
            ", attributes='" + attributes + '\'' +
            ", yundouScale=" + yundouScale +
            ", minYundouPrice=" + minYundouPrice +
            ", fakeSales=" + fakeSales +
            ", amount=" + amount +
            ", imgWidth=" + imgWidth +
            ", imgHeight=" + imgHeight +
            ", recommend=" + recommend +
            ", recommendAt=" + recommendAt +
            ", commissionRate=" + commissionRate +
            ", discount=" + discount +
            ", forsaleAt=" + forsaleAt +
            ", onsaleAt=" + onsaleAt +
            ", instockAt=" + instockAt +
            ", updateLock=" + updateLock +
            ", isCollected=" + isCollected +
            ", delayAt=" + delayAt +
            ", delayed=" + delayed +
            ", deliveryRegion=" + deliveryRegion +
            ", synchronousFlag='" + synchronousFlag + '\'' +
            '}';
  }
}