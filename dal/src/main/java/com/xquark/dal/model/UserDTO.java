package com.xquark.dal.model;

/**
 * @author Jack Zhu
 * @since 2018/12/18
 */
public class UserDTO {
    private String name;
    private String avatar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}

