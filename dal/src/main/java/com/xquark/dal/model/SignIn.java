package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class SignIn extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = -4842167029348968923L;

  public SignIn() {
  }

  public SignIn(String userId) {
    this.userId = userId;
  }

  private String id;

  /**
   * 用户id
   */
  private String userId;

  /**
   * 连续签到数
   */
  private Long signCount;

  /**
   * 签到总天数
   */
  private Long signSum;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getSignCount() {
    return signCount;
  }

  public void setSignCount(Long signCount) {
    this.signCount = signCount;
  }

  public Long getSignSum() {
    return signSum;
  }

  public void setSignSum(Long signSum) {
    this.signSum = signSum;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

}