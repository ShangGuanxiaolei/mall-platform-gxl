package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: kong Date: 18-6-13. Time: 下午5:44 产品包装箱表
 */
public class Package extends BaseEntityImpl implements Archivable {

  //箱型名称
  @NotBlank
  private String name;
  //重量
  @Digits(integer = 5, fraction = 0)
  @Min(0)
  @NotNull
  private Integer weight;
  //宽度
  @Digits(integer = 5, fraction = 0)
  @Min(0)
  @NotNull
  private Integer width;
  //高度
  @Digits(integer = 5, fraction = 0)
  @Min(0)
  @NotNull
  private Integer height;
  //长度
  @Digits(integer = 5, fraction = 0)
  @Min(0)
  @NotNull
  private Integer length;
  //价格
  @DecimalMin(value = "0", inclusive = false)
  @NotNull
  private BigDecimal price;
  //逻辑删除
  private Boolean archive;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "Package{" +
        "name='" + name + '\'' +
        ", weight=" + weight +
        ", width=" + width +
        ", height=" + height +
        ", length=" + length +
        ", price=" + price +
        ", archive=" + archive +
        '}';
  }
}
