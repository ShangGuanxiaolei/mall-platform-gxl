package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ValidType;
import com.xquark.dal.type.CommissionType;

import java.math.BigDecimal;

public class PartnerShopCommission extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownShopId;

  private String name;

  private BigDecimal rate;

  private CommissionType type;

  private Boolean archive;

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public CommissionType getType() {
    return type;
  }

  public void setType(CommissionType type) {
    this.type = type;
  }

  public String getOwnShopId() {
    return ownShopId;
  }

  public void setOwnShopId(String ownShopId) {
    this.ownShopId = ownShopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

}