package com.xquark.dal.model;

import com.xquark.dal.type.PaymentMode;

/**
 * 标识可支付
 * @author wangxinhua
 * @date 2019-04-19
 * @since 1.0
 */
public interface PaidAble {

    /**
     * 获取支付单号
     * @return 支付单号
     */
    String getPayNo();

    /**
     * 获取支付类型
     * @return 支付类型枚举
     */
    PaymentMode getPayType();

}
