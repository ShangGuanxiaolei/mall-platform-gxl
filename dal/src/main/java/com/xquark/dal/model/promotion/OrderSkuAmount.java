package com.xquark.dal.model.promotion;

public class OrderSkuAmount {

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 订单商品数
   */
  private Integer amount;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }
}
