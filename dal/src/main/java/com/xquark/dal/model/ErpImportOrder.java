package com.xquark.dal.model;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/5/30
 * Time: 17:05
 * Description: erp导入实体类
 */
public class ErpImportOrder {

    /**
     * erpOrderNo  : 系统订单号
     * wayNum : 运单号
     * company : 快递公司
     * orderModel : 快递模板名称
     * warehouse : 仓库
     * buyName : 买家名称
     * receiver : 收件人
     * phone : 联系方式
     * street : 详细地址
     * thirdOrderNo : 订单编号
     * flagColor : 插旗颜色
     * shopName : 店铺名称
     * sellerRemark : 卖家备注
     * buyerRemark : 买家留言
     * orderRemark : 快递单备注
     * orderStatus : 订单状态
     * orderType : 订单类型
     * orderItemNo : 子订单编号
     * goodName : 商品名称
     * nameSing : 商品简称
     * sellerNo : 商家编码
     * skuName : 规格名称
     * skuCode : 规格编码
     * count : 商品数量
     * price : 商品价格
     * payMoney : 实收金额
     * discounts : 优惠金额
     * carriage : 运费
     * orderTime : 下单时间
     * payTime : 支付时间
     * sendTime : 发货时间
     * printSend : 打发货单时间
     * printNo : 打快递单时间
     * printSendMan : 打发货单操作人
     * printNoMan : 打发货单操作人
     * province : 省
     * city : 市
     * area : 区
     * firstOrderNo : 原始单号
     */

    private String erpOrderNo;
    private String wayNum;
    private String company;
    private String orderModel;
    private String warehouse;
    private String buyName;
    private String receiver;
    private String phone;
    private String street;
    private String thirdOrderNo;
    private String flagColor;
    private String shopName;
    private String sellerRemark;
    private String buyerRemark;
    private String orderRemark;
    private String orderStatus;
    private String orderType;
    private String orderItemNo;
    private String goodName;
    private String nameSing;
    private String sellerNo;
    private String skuName;
    private String skuCode;
    private String count;
    private String price;
    private String payMoney;
    private String discounts;
    private String carriage;
    private String orderTime;
    private String payTime;
    private String sendTime;
    private String printSend;
    private String printNo;
    private String printSendMan;
    private String printNoMan;
    private String province;
    private String city;
    private String area;
    private String firstOrderNo;

    public String getErpOrderNo() {
        return erpOrderNo;
    }

    public void setErpOrderNo(String erpOrderNo) {
        this.erpOrderNo = erpOrderNo;
    }

    public String getWayNum() {
        return wayNum;
    }

    public void setWayNum(String wayNum) {
        this.wayNum = wayNum;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(String orderModel) {
        this.orderModel = orderModel;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getBuyName() {
        return buyName;
    }

    public void setBuyName(String buyName) {
        this.buyName = buyName;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getThirdOrderNo() {
        return thirdOrderNo;
    }

    public void setThirdOrderNo(String thirdOrderNo) {
        this.thirdOrderNo = thirdOrderNo;
    }

    public String getFlagColor() {
        return flagColor;
    }

    public void setFlagColor(String flagColor) {
        this.flagColor = flagColor;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSellerRemark() {
        return sellerRemark;
    }

    public void setSellerRemark(String sellerRemark) {
        this.sellerRemark = sellerRemark;
    }

    public String getBuyerRemark() {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark) {
        this.buyerRemark = buyerRemark;
    }

    public String getOrderRemark() {
        return orderRemark;
    }

    public void setOrderRemark(String orderRemark) {
        this.orderRemark = orderRemark;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderItemNo() {
        return orderItemNo;
    }

    public void setOrderItemNo(String orderItemNo) {
        this.orderItemNo = orderItemNo;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getNameSing() {
        return nameSing;
    }

    public void setNameSing(String nameSing) {
        this.nameSing = nameSing;
    }

    public String getSellerNo() {
        return sellerNo;
    }

    public void setSellerNo(String sellerNo) {
        this.sellerNo = sellerNo;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
    }

    public String getDiscounts() {
        return discounts;
    }

    public void setDiscounts(String discounts) {
        this.discounts = discounts;
    }

    public String getCarriage() {
        return carriage;
    }

    public void setCarriage(String carriage) {
        this.carriage = carriage;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getPrintSend() {
        return printSend;
    }

    public void setPrintSend(String printSend) {
        this.printSend = printSend;
    }

    public String getPrintNo() {
        return printNo;
    }

    public void setPrintNo(String printNo) {
        this.printNo = printNo;
    }

    public String getPrintSendMan() {
        return printSendMan;
    }

    public void setPrintSendMan(String printSendMan) {
        this.printSendMan = printSendMan;
    }

    public String getPrintNoMan() {
        return printNoMan;
    }

    public void setPrintNoMan(String printNoMan) {
        this.printNoMan = printNoMan;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFirstOrderNo() {
        return firstOrderNo;
    }

    public void setFirstOrderNo(String firstOrderNo) {
        this.firstOrderNo = firstOrderNo;
    }
}
