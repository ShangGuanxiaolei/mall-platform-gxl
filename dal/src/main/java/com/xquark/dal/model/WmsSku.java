package com.xquark.dal.model;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/5/31
 * Time: 18:40
 * Description: WMS延迟推送SKU
 */
public class WmsSku {

    private String orderNo;

    private Integer amount;

    private String skuCode;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }
}