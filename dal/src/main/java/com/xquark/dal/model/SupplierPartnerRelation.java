package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

public class SupplierPartnerRelation extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;
  private Long shopId;

  private Long partnerShopId;

  private Long distributionTypeId;

  private Long zoneId;

  private Date createdAt;

  private Date updateAt;

  private Boolean archive;

  public Long getShopId() {
    return shopId;
  }

  public void setShopId(Long shopId) {
    this.shopId = shopId;
  }

  public Long getPartnerShopId() {
    return partnerShopId;
  }

  public void setPartnerShopId(Long partnerShopId) {
    this.partnerShopId = partnerShopId;
  }

  public Long getDistributionTypeId() {
    return distributionTypeId;
  }

  public void setDistributionTypeId(Long distributionTypeId) {
    this.distributionTypeId = distributionTypeId;
  }

  public Long getZoneId() {
    return zoneId;
  }

  public void setZoneId(Long zoneId) {
    this.zoneId = zoneId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}