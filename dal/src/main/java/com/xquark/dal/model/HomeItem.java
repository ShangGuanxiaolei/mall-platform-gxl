package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.HomeItemType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

public class HomeItem extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String code;

  private Integer idx;

  private String img;

  private String name;

  private String belong;

  private Boolean archive;

  private String url;

  private HomeItemType type;

  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public HomeItemType getType() {
    return type;
  }

  public void setType(HomeItemType type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getIdx() {
    return idx;
  }

  public void setIdx(Integer idx) {
    this.idx = idx;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBelong() {
    return belong;
  }

  public void setBelong(String belong) {
    this.belong = belong;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}