package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.dal.vo.SaleProduactVO;

import java.util.Date;
import java.util.List;

/**
 * @类名: Promotion4ProductSalesVO
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/28 15:52
 * @版本号: V1.0 .
 */
public class Promotion4ProductSalesVO {

    private String pCode;
    private String pName;
    private String pType;
    private Date effectFrom;
    private Date effectTo;
    private String pStatus;
    private String creator;
    private String auditor;
    private Integer isWithPoint;
    private String updator;

    private Integer isDeleted;
    private String tranCode;
    private Integer isCountEarning;
    private Integer isForcePostage;
    private Integer isDeliveryReduction;

    /**
     * 首页banner图
     */
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String bannerImgHome;

    /**
     * 二级页面banner图
     */
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String bannerImgList;

    /**
     * 是否显示倒计时，0为不显示，1为显示，默认0
     */
    private Boolean timeShow;

    /**
     * 活动类型：0为橱窗型，1为banner型，默认,0
     */
    private Integer type;

    /**
     * 是否计算收益:0为不计算，1为计算，默认为0
     */
    private Boolean calculateYield;

    /**
     * 是否包邮：0为不包邮，1为包邮，默认为0
     */
    private Boolean carriageFree;

    /**
     * 受众类型,新人(1),白人加新人(2),Vip加店主(3),不限(0)
     */
    private String customerType;

    /**
     * 角标文字，最多三个字
     */
    private String cornerTab;

    private List<SaleProduactVO> saleProduacts;

    public List<SaleProduactVO> getSaleProduacts() {
        return saleProduacts;
    }

    public void setSaleProduacts(List<SaleProduactVO> saleProduacts) {
        this.saleProduacts = saleProduacts;
    }

    public Integer getIsDeliveryReduction() {
        return isDeliveryReduction;
    }

    public void setIsDeliveryReduction(Integer isDeliveryReduction) {
        this.isDeliveryReduction = isDeliveryReduction;
    }

    public Integer getIsForcePostage() {
        return isForcePostage;
    }

    public void setIsForcePostage(Integer isForcePostage) {
        this.isForcePostage = isForcePostage;
    }

    public Integer getIsWithPoint() {
        return isWithPoint;
    }

    public void setIsWithPoint(Integer isWithPoint) {
        this.isWithPoint = isWithPoint;
    }

    public Integer getIsCountEarning() {
        return isCountEarning;
    }

    public void setIsCountEarning(Integer isCountEarning) {
        this.isCountEarning = isCountEarning;
    }

    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }



    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getBannerImgHome() {
        return bannerImgHome;
    }

    public void setBannerImgHome(String bannerImgHome) {
        this.bannerImgHome = bannerImgHome;
    }

    public String getBannerImgList() {
        return bannerImgList;
    }

    public void setBannerImgList(String bannerImgList) {
        this.bannerImgList = bannerImgList;
    }

    public Boolean getTimeShow() {
        return timeShow;
    }

    public void setTimeShow(Boolean timeShow) {
        this.timeShow = timeShow;
    }



    public Boolean getCalculateYield() {
        return calculateYield;
    }

    public void setCalculateYield(Boolean calculateYield) {
        this.calculateYield = calculateYield;
    }

    public Boolean getCarriageFree() {
        return carriageFree;
    }

    public void setCarriageFree(Boolean carriageFree) {
        this.carriageFree = carriageFree;
    }

    public String getCornerTab() {
        return cornerTab;
    }

    public void setCornerTab(String cornerTab) {
        this.cornerTab = cornerTab;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }



}
