package com.xquark.dal.model;

import java.util.Date;

public class RecommendProduct {

	private long id;

	private long productId;

	private long recommendId;

	private Date createdAt;

	private Date updatedAt;

	private boolean archive;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getRecommendId() {
		return recommendId;
	}

	public void setRecommendId(long recommendId) {
		this.recommendId = recommendId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}
}
