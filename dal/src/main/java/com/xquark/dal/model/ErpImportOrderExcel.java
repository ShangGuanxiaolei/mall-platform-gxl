package com.xquark.dal.model;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/6/10
 * Time: 10:39
 * Description: erp详情导出excel模板
 */
public class ErpImportOrderExcel extends  ErpImportOrder {
   //失败原因
  private String  reason;


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
