package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * customerWechatUnion
 *
 * @author wangxinhua
 */
public class CustomerWechatUnion implements Serializable {

  /**
   * 主键
   */
  private Long cpId;

  private String unionId;

  private String nickName;

  private String headImgurl;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getHeadImgurl() {
    return headImgurl;
  }

  public void setHeadImgurl(String headImgurl) {
    this.headImgurl = headImgurl;
  }
}