package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

/**
 * Created by chh on 16-10-12.
 */
public class PromotionDiamond extends BaseEntityImpl implements Archivable {

  private Boolean archive;
  private BigDecimal discount;
  private String productId;
  private String shopid;
  private String promotionId;

  public String getShopid() {
    return shopid;
  }

  public void setShopid(String shopid) {
    this.shopid = shopid;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

}
