package com.xquark.dal.model;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 * 商品下架查询VO
 */
public class InStockProdVO {

  private String id;

  private String name;

  private String prodCode;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProdCode() {
    return prodCode;
  }

  public void setProdCode(String prodCode) {
    this.prodCode = prodCode;
  }

}
