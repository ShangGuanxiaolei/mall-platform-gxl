
package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class Address extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String userId;
  private Integer extId;
  private String consignee;
  private String phone;
  private String zoneId;
  private String street;
  private String zipcode;
  private String weixinId;
  private String hashCode;
  private Boolean common;
  private Boolean archive = false;
  private Boolean isDefault = false;
  private String certificateId;// 身份证

  private Long cpId;
  private short type = 2;//类型，1通讯地址，2收货地址
  private Long provinceId;
  private Long cityId;
  private short status = 1;//状态
  private String createdBy;
  private String updateBy;
  private String dtsSyncSource;
  private String dtsSyncUid;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getExtId() {
    return extId;
  }

  public void setExtId(Integer extId) {
    this.extId = extId;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getWeixinId() {
    return weixinId;
  }

  public void setWeixinId(String weixinId) {
    this.weixinId = weixinId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public Boolean getCommon() {
    return common;
  }

  public void setCommon(Boolean common) {
    this.common = common;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getHashCode() {
    return hashCode;
  }

  public void setHashCode(String hashCode) {
    this.hashCode = hashCode;
  }

  public Boolean getIsDefault() {
    if (isDefault == null) {
      return Boolean.FALSE;
    }
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getCertificateId() {
    return certificateId;
  }

  public void setCertificateId(String certificateId) {
    this.certificateId = certificateId;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public short getType() {
    return type;
  }

  public void setType(short type) {
    this.type = type;
  }

  public Long getProvinceId() {
    return provinceId;
  }

  public void setProvinceId(Long provinceId) {
    this.provinceId = provinceId;
  }

  public Long getCityId() {
    return cityId;
  }

  public void setCityId(Long cityId) {
    this.cityId = cityId;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Boolean getDefault() {
    return isDefault;
  }

  public void setDefault(Boolean aDefault) {
    isDefault = aDefault;
  }

  public String getDtsSyncSource() {
    return dtsSyncSource;
  }

  public void setDtsSyncSource(String dtsSyncSource) {
    this.dtsSyncSource = dtsSyncSource;
  }

  public String getDtsSyncUid() {
    return dtsSyncUid;
  }

  public void setDtsSyncUid(String dtsSyncUid) {
    this.dtsSyncUid = dtsSyncUid;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((consignee == null) ? 0 : consignee.hashCode());
    result = prime * result + ((phone == null) ? 0 : phone.hashCode());
    result = prime * result + ((street == null) ? 0 : street.hashCode());
    result = prime * result + ((zoneId == null) ? 0 : zoneId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Address other = (Address) obj;
    if (consignee == null) {
      if (other.consignee != null) {
        return false;
      }
    } else if (!consignee.equals(other.consignee)) {
      return false;
    }
    if (phone == null) {
      if (other.phone != null) {
        return false;
      }
    } else if (!phone.equals(other.phone)) {
      return false;
    }
    if (street == null) {
      if (other.street != null) {
        return false;
      }
    } else if (!street.equals(other.street)) {
      return false;
    }
    if (zoneId == null) {
      if (other.zoneId != null) {
        return false;
      }
    } else if (!zoneId.equals(other.zoneId)) {
      return false;
    }
    return true;
  }

}