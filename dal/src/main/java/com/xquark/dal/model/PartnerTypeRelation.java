package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CommissionType;


public class PartnerTypeRelation extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private CommissionType type;

  private String shopId;

  private String userId;

  private String typeId;

  private String typeStr;

  public String getTypeStr() {
    if (this.getType() == null) {
      return "";
    }
    String str = "";
    switch (this.getType()) {
      case PLATFORM:
        str = "平台";
        break;
      case TEAM:
        str = "团队";
        break;
      case SHAREHOLDER:
        str = "股东";
        break;
      default:
        str = this.getType().toString();
        break;
    }
    return str;
  }

  public void setTypeStr(String typeStr) {
    this.typeStr = typeStr;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTypeId() {
    return typeId;
  }

  public void setTypeId(String typeId) {
    this.typeId = typeId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public CommissionType getType() {
    return type;
  }

  public void setType(CommissionType type) {
    this.type = type;
  }

  private Boolean archive;

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}