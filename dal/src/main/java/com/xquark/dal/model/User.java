package com.xquark.dal.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.IUser;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.type.GenderType;
import com.xquark.dal.type.PlatformType;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends BaseEntityImpl implements IUser {

  private static final long serialVersionUID = 1L;

  /**
   * 后台管理员
   */
  public static final long ROLE_ADMIN = 1;
  /**
   * 广告分润联盟
   */
  public static final long ROLE_UNION = 1 << 1;
//	/**运营**/
//	public static final long ROLE_OPERATION = 1 << 2;
//	/**财务**/
//	public static final long ROLE_FINANCE = 1 << 3;


  private static final SimpleGrantedAuthority role_auth_admin = new SimpleGrantedAuthority(
      "ROLE_ADMIN");
  private static final SimpleGrantedAuthority role_auth_union = new SimpleGrantedAuthority(
      "ROLE_UNION");
//	private static final SimpleGrantedAuthority role_auth_operation = new SimpleGrantedAuthority(
//			"ROLE_OPERATION");
//	private static final SimpleGrantedAuthority role_auth_finance = new SimpleGrantedAuthority(
//			"ROLE_FINANCE");

  private Long cpId;

  private String customerNumber;

  @ApiModelProperty(value = "昵称")
  private String name;//目前等于绑定银行卡时候的姓名

  /**
   * 是否后台账号，TODO 先简单实现了，有必要在支持UserRole
   */
  private Long roles;

  @ApiModelProperty(value = "头像")
  private String avatar;

  private String email;

  private String loginname;

  @ApiModelProperty(value = "手机号")
  private String phone;

  private String password;

  private Boolean archive;

  private String shopId;

  @ApiModelProperty(value = "身份证号")
  private String idCardNum;
  // 第三方用户提供方
  private String partner;
  // 第三方用户id
  private String extUserId;

  private int withdrawType;

  private long functionSet;

  private Boolean freeLogisticUsed;
  /**
   * 当前买家进入的店铺ID
   */
  private String currentSellerShopId;

  //会员数据
  private String memberCode;           //会员编号
  private String memberLevel;          //会员等级;
  private Integer memberPoints;        //会员经验值
  private Integer consumptionPoints;   //消费积分
  private String weixinCode;           //微信code
  private String weiboCode;            //微博code
  private String cid;                  //cid
  private String cid1;                 //cid1
  private String cid2;                 //cid2
  private String cid3;                 //cid3

  @JSONField(serialize = false)
  private Double balance;              //余额

  private String code;

  private String unionId;

  private String wechatName;

  private Long yundou;

  private String fanCardNo;

  // 会员等级名称
  private String levelName;

  @ApiModelProperty(value = "性别")
  private GenderType sex;  //性别

  @ApiModelProperty(value = "生日")
  private String birthday; //生日

  @ApiModelProperty(value = "个性签名")
  private String signature; //个性签名

  @ApiModelProperty(value = "常住地")
  private String zoneId; //常住地

  @ApiModelProperty(value = "性别显示名称")
  private String sexStr;

  @ApiModelProperty(value = "常住地显示名称")
  private String zoneStr;

  private Long collections;

  private Integer source;

  private Long upline;

  /**
   * 是否是店主
   */
  private boolean isShopOwner;

  /**
   * 是否是会员
   */
  private boolean isMember;

  /**
   * 是否是代理
   */
  private boolean isProxy;

  private boolean isLighten;

  private boolean hasIdentity;

  private int registerPointFlag;//是否已显示注册德分提示，1已显示，0未显示
  private String nickName; // nick_name汉薇自定义昵称

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public int getRegisterPointFlag() {
    return registerPointFlag;
  }

  public void setRegisterPointFlag(int registerPointFlag) {
    this.registerPointFlag = registerPointFlag;
  }

  /**
   * 是否为首席体验官
   */
  private int expOfficer;

  private String identityName;

  private String identityType;

  public String getIdentityType() {
    return identityType;
  }

  public void setIdentityType(String identityType) {
    this.identityType = identityType;
  }

  public int getExpOfficer() {
    return expOfficer;
  }

  public void setExpOfficer(int expOfficer) {
    this.expOfficer = expOfficer;
  }

  public String getIdentityName() {
    return identityName;
  }

  public void setIdentityName(String identityName) {
    this.identityName = identityName;
  }

  public Long getCollections() {
    return collections;
  }

  public void setCollections(Long collections) {
    this.collections = collections;
  }

  public String getSexStr() {
    if (this.getSex() == null) {
      return "";
    }
    String str = "";
    if (this.getSex() == GenderType.M) {
      str = "男";
    } else if (this.getSex() == GenderType.FM) {
      str = "女";
    } else if (this.getSex() == GenderType.N) {
      str = "保密";
    }
    return str;
  }

  public void setSexStr(String sexStr) {
    this.sexStr = sexStr;
  }

  public String getZoneStr() {
    return zoneStr;
  }

  public void setZoneStr(String zoneStr) {
    this.zoneStr = zoneStr;
  }

  public GenderType getSex() {
    return sex;
  }

  public void setSex(GenderType sex) {
    this.sex = sex;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getLevelName() {
    return levelName;
  }

  public void setLevelName(String levelName) {
    this.levelName = levelName;
  }

  public String getWechatName() {
    return wechatName;
  }

  public void setWechatName(String wechatName) {
    this.wechatName = wechatName;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Double getBalance() {
    return balance;
  }

  public void setBalance(Double balance) {
    this.balance = balance;
  }

  public Long getYundou() {
    return yundou;
  }

  public void setYundou(Long yundou) {
    this.yundou = yundou;
  }

  public String getFanCardNo() {
    return fanCardNo;
  }

  public void setFanCardNo(String fanCardNo) {
    this.fanCardNo = fanCardNo;
  }

  @Override
  public boolean hasRole(String role) {
    return true;
  }

  public void updateCid(String anonymousId) {
    //不记录重复的匿名用户Id
    if (anonymousId != null && (anonymousId.equals(cid) || anonymousId.equals(cid1) || anonymousId
        .equals(cid2) || anonymousId.equals(cid3))) {
      return;
    }
    if (this.cid == null) {
      this.cid = anonymousId;
    } else if (this.cid1 == null) {
      this.cid1 = anonymousId;
    } else if (this.cid2 == null) {
      this.cid2 = anonymousId;
    } else if (this.cid3 == null) {
      this.cid3 = anonymousId;
    } else {
      this.cid = anonymousId;
    }
  }

  public String getCid1() {
    return cid1;
  }

  public void setCid1(String cid1) {
    this.cid1 = cid1;
  }

  public String getCid2() {
    return cid2;
  }

  public void setCid2(String cid2) {
    this.cid2 = cid2;
  }

  public String getCid3() {
    return cid3;
  }

  public void setCid3(String cid3) {
    this.cid3 = cid3;
  }

  public String getCid() {
    return cid;
  }

  public void setCid(String cid) {
    this.cid = cid;
  }

  private UserStatus userStatus = UserStatus.ANONYMOUS;   //用户状态 默认为匿名用户

  public String getMemberCode() {
    return memberCode;
  }

  public void setMemberCode(String memberCode) {
    this.memberCode = memberCode;
  }

  public String getMemberLevel() {
    return memberLevel;
  }

  public void setMemberLevel(String memberLevel) {
    this.memberLevel = memberLevel;
  }

  public Integer getConsumptionPoints() {
    return consumptionPoints;
  }

  public void setConsumptionPoints(Integer consumptionPoints) {
    this.consumptionPoints = consumptionPoints;
  }

  public Integer getMemberPoints() {
    return memberPoints;
  }

  public void setMemberPoints(Integer memberPoints) {
    this.memberPoints = memberPoints;
  }

  public String getWeiboCode() {
    return weiboCode;
  }

  public void setWeiboCode(String weiboCode) {
    this.weiboCode = weiboCode;
  }

  public String getWeixinCode() {
    return weixinCode;
  }

  public void setWeixinCode(String weixinCode) {
    this.weixinCode = weixinCode;
  }

  public UserStatus getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getCustomerNumber() {
    if (StringUtils.isBlank(customerNumber)) {
      return "";
    }
    PlatformType platform = getPlatForm();
    if (platform == null || platform == PlatformType.E) {
      return customerNumber;
    }
    return customerNumber + "(" + platform.name() + ")";
  }

  public Long getUpline() {
    return upline;
  }

  public void setUpline(Long upline) {
    this.upline = upline;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getRoles() {
    return roles;
  }

  public void setRoles(Long roles) {
    this.roles = roles;
  }

  public boolean hasRole(long role) {
    return roles != null && (roles & role) != 0;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getLoginname() {
    return loginname;
  }

  public void setLoginname(String loginname) {
    this.loginname = loginname;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Collection<GrantedAuthority> auths = new ArrayList<GrantedAuthority>(2);
    if (hasRole(ROLE_ADMIN)) {
      auths.add(role_auth_admin);
    }
    if (hasRole(ROLE_UNION)) {
      auths.add(role_auth_union);
    }
    return auths;
  }

  @Override
  public String getUsername() {
    return this.getLoginname() == null ? this.getEmail() : this
        .getLoginname();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getIdCardNum() {
    return idCardNum;
  }

  public void setIdCardNum(String idCardNum) {
    this.idCardNum = idCardNum;
  }

  public String getExtUserId() {
    return extUserId;
  }

  public void setExtUserId(String extUserId) {
    this.extUserId = extUserId;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  @Override
  public String getId() {
    return super.getId();
  }

  public boolean isAnonymous() {
    return this.loginname.startsWith(UniqueNoType.CID.name());
  }

  public int getWithdrawType() {
    return withdrawType;
  }

  public void setWithdrawType(int withdrawType) {
    this.withdrawType = withdrawType;
  }

  public Long getFunctionSet() {
    return functionSet;
  }

  public void setFunctionSet(Long functionSet) {
    this.functionSet = functionSet;
  }

  public Boolean getFunctionSts(int bit) {
    long result = (1 << bit) & this.functionSet;
    return result > 0 ? true : false;
  }

  public String getCurrentSellerShopId() {
    return currentSellerShopId;
  }

  public void setCurrentSellerShopId(String currentSellerShopId) {
//    this.currentSellerShopId = currentSellerShopId;
  }

  public Boolean getFreeLogisticUsed() {
    return freeLogisticUsed;
  }

  public void setFreeLogisticUsed(Boolean freeLogisticUsed) {
    this.freeLogisticUsed = freeLogisticUsed;
  }

  public void setRootSellerShopId(String rootShopId) {
    this.currentSellerShopId = rootShopId;
  }

  public String getMemberStr() {
    return "普通会员";
  }

  public long getDecodeId() {
    String id = this.getId();
    if (StringUtils.isBlank(id)) {
      return 0L;
    }
    return IdTypeHandler.decode(id);
  }

  public Integer getSource() {
    return source;
  }

  public void setSource(Integer source) {
    this.source = source;
  }

  public PlatformType getPlatForm() {
    if (source == null) {
      return null;
    }
    return PlatformType.fromCode(source);
  }

  public boolean getIsShopOwner() {
    return isShopOwner;
  }

  public void setIsShopOwner(boolean shopOwner) {
    isShopOwner = shopOwner;
  }

  public boolean getIsMember() {
    return isMember;
  }

  public void setIsMember(boolean member) {
    isMember = member;
  }

  public boolean getIsProxy() {
    return isProxy;
  }

  public void setIsProxy(boolean proxy) {
    isProxy = proxy;
  }

  public boolean getIsLighten() {
    return isLighten;
  }

  public void setIsLighten(boolean lighten) {
    isLighten = lighten;
  }

  public boolean getHasIdentity() {
    return hasIdentity;
  }

  public void setHasIdentity(boolean hasIdentity) {
    this.hasIdentity = hasIdentity;
  }

  @Override
  public String toString() {
    return "User{" +
            "cpId=" + cpId +
            ", customerNumber='" + customerNumber + '\'' +
            ", name='" + name + '\'' +
            ", roles=" + roles +
            ", avatar='" + avatar + '\'' +
            ", email='" + email + '\'' +
            ", loginname='" + loginname + '\'' +
            ", phone='" + phone + '\'' +
            ", password='" + password + '\'' +
            ", archive=" + archive +
            ", shopId='" + shopId + '\'' +
            ", idCardNum='" + idCardNum + '\'' +
            ", partner='" + partner + '\'' +
            ", extUserId='" + extUserId + '\'' +
            ", withdrawType=" + withdrawType +
            ", functionSet=" + functionSet +
            ", freeLogisticUsed=" + freeLogisticUsed +
            ", currentSellerShopId='" + currentSellerShopId + '\'' +
            ", memberCode='" + memberCode + '\'' +
            ", memberLevel='" + memberLevel + '\'' +
            ", memberPoints=" + memberPoints +
            ", consumptionPoints=" + consumptionPoints +
            ", weixinCode='" + weixinCode + '\'' +
            ", weiboCode='" + weiboCode + '\'' +
            ", cid='" + cid + '\'' +
            ", cid1='" + cid1 + '\'' +
            ", cid2='" + cid2 + '\'' +
            ", cid3='" + cid3 + '\'' +
            ", balance=" + balance +
            ", code='" + code + '\'' +
            ", unionId='" + unionId + '\'' +
            ", wechatName='" + wechatName + '\'' +
            ", yundou=" + yundou +
            ", fanCardNo='" + fanCardNo + '\'' +
            ", levelName='" + levelName + '\'' +
            ", sex=" + sex +
            ", birthday='" + birthday + '\'' +
            ", signature='" + signature + '\'' +
            ", zoneId='" + zoneId + '\'' +
            ", sexStr='" + sexStr + '\'' +
            ", zoneStr='" + zoneStr + '\'' +
            ", collections=" + collections +
            ", source=" + source +
            ", upline=" + upline +
            ", isShopOwner=" + isShopOwner +
            ", isMember=" + isMember +
            ", isProxy=" + isProxy +
            ", isLighten=" + isLighten +
            ", hasIdentity=" + hasIdentity +
            ", registerPointFlag=" + registerPointFlag +
            ", userStatus=" + userStatus +
            '}';
  }
}