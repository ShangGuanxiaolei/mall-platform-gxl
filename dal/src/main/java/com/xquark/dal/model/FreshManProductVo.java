package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:16:12
 * Des:新人商品model
 */
public class FreshManProductVo implements Serializable {
//            `id` bigint(20) NOT NULL AUTO_INCREMENT,
//            `tab_id` bigint(20) NOT NULL COMMENT '关联专区tab_ID',
//            `product_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '商品ID',
//            `product_name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '商品名称',
//            `stock` int(11) NOT NULL COMMENT '可售库存',
//            `price` decimal(10,2) NOT NULL COMMENT '专区价格',
//            `exchange_price` decimal(10,2) NOT NULL COMMENT '兑换金额',
//            `exchange_point` int(11) NOT NULL COMMENT '兑换德分',
//            `status` int(11) NOT NULL DEFAULT '1' COMMENT '是否启用，1启用，0删除',
//            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//            `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    private Long id;
    private Long tabId;
    private Long productId;
    private String productName;
    private Integer stock;
    private BigDecimal price; //实际价格
    private BigDecimal marketPrice;//原价
    private BigDecimal exchangePrice;
    private BigDecimal exchangePoint;
    private Integer status;
    private Date createdAt;
    private Date updatedAt;
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;  // 主图code
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String imgUrl;
    private int selfOperated;//自营标签

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }
    private BigDecimal serverAmt; // 服务费
    private BigDecimal point; // 立减

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return IdTypeHandler.encode(productId);
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getTabId() {
        return tabId;
    }

    public void setTabId(Long tabId) {
        this.tabId = tabId;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExchangePrice() {
        return exchangePrice;
    }

    public void setExchangePrice(BigDecimal exchangePrice) {
        this.exchangePrice = exchangePrice;
    }

    public BigDecimal getExchangePoint() {
        return exchangePoint;
    }

    public void setExchangePoint(BigDecimal exchangePoint) {
        this.exchangePoint = exchangePoint;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}