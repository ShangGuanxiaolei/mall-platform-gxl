package com.xquark.dal.model;

/**
 * created by
 *
 * @author wangxinhua at 18-7-4 下午8:48
 */
public class SFCategoryMapping {

  private String code;

  private String name;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
