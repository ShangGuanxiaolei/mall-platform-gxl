package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.List;

public class HotSearchKey extends BaseEntityImpl implements Archivable {

  private String id;
  private String hkey;
  private String productId;
  private Integer status;
  private Integer display;
  private Boolean archive;

  private List<Product> products;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  public String getHkey() {
    return hkey;
  }

  public void setHkey(String hkey) {
    this.hkey = hkey;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getDisplay() {
    return display;
  }

  public void setDisplay(Integer display) {
    this.display = display;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
