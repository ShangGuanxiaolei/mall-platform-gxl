package com.xquark.dal.model;

public class MemberTypeAndNickName {

    private String nickName;
    private String identityType;
    private String cpid;
    private String viviType;

    public String getViviType() {
        return viviType;
    }

    public void setViviType(String viviType) {
        this.viviType = viviType;
    }

    public String getCpid() {
        return cpid;
    }

    public void setCpid(String cpid) {
        this.cpid = cpid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }
}
