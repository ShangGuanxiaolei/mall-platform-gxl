package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.model.promotion.ComparableDiscount;
import com.xquark.dal.type.PromotionDiscountType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-9-28. DESC:
 */
public abstract class PromotionFullReduceDiscount<S extends Number> extends
        BaseEntityImpl implements Archivable, ComparableDiscount<S> {

    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 层级
     */
    private Integer level;


    /**
     * 优惠方式
     */
    private PromotionDiscountType discountType;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 现金优惠
     */
    private BigDecimal cash;

    private Boolean gift = false;

    private String giftIds;

    /**
     * 是否包邮
     */
    private boolean freeDelivery = false;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public PromotionDiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(PromotionDiscountType discountType) {
        this.discountType = discountType;
    }

    /**
     * 展示形式的折扣
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * 小数形式的折扣
     */
    public BigDecimal getRealDiscount() {
        if (discount != null) {
            return discount;
        }
        return BigDecimal.ZERO;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public boolean getFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Boolean getGift() {
        return gift;
    }

    public void setGift(Boolean gift) {
        this.gift = gift;
    }

    public String getGiftIds() {
        return giftIds;
    }

    public void setGiftIds(String giftIds) {
        this.giftIds = giftIds;
    }

}
