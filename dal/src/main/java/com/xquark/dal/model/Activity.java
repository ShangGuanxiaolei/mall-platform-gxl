package com.xquark.dal.model;

import java.util.Date;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.ActivityChannel;
import com.xquark.dal.type.ActivityStatus;
import com.xquark.dal.type.ActivityType;

/**
 * 活动实体类
 *
 * @author xuebowen
 */
public class Activity extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String name; // 活动名称
  private String img; // 头像
  private String tagImage;
  private String banner; // 活动标语
  private ActivityType type; // 活动类型
  private ActivityStatus status; // 活动状态
  private Date startTime; // 活动开始时间
  private Date endTime; // 活动结束时间
  private String creatorId; // 创建用户

  private Boolean remind; // 是否有提醒
  private String applyDesc;// 活动要求
  private String summary; // 活动简述
  private String details; // 活动描述
  private ActivityChannel channel; // 活动所属平台：想去..
  private Boolean archive;

  private Integer actTagType; // 标签图位置1=下 2=上

  private Date applyStartTime; // 卖家报名参加活动系统活动的开始时间
  private Date applyEndTime; // 卖家报名的截止时间
  private String url; // 下发活动url

  // 未知属性 todo
  private Date actDate;
  private Date closedAt;
//    private Boolean repeatable;// 持续活动，还是一次性活动

  private String shopName;

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBanner() {
    return banner;
  }

  public void setBanner(String banner) {
    this.banner = banner;
  }

  public ActivityType getType() {
    return type;
  }

  public void setType(ActivityType type) {
    this.type = type;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Boolean getRemind() {
    return remind;
  }

  public void setRemind(Boolean remind) {
    this.remind = remind;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public String getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(String creatorId) {
    this.creatorId = creatorId;
  }

  public Date getActDate() {
    return actDate;
  }

  public void setActDate(Date actDate) {
    this.actDate = actDate;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

//    public Boolean getRepeatable() {
//        return repeatable;
//    }
//
//    public void setRepeatable(Boolean repeatable) {
//        this.repeatable = repeatable;
//    }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public ActivityStatus getStatus() {
    return status;
  }

  public void setStatus(ActivityStatus status) {
    this.status = status;
  }

  public Date getClosedAt() {
    return closedAt;
  }

  public void setClosedAt(Date closedAt) {
    this.closedAt = closedAt;
  }

  public Date getApplyStartTime() {
    return applyStartTime;
  }

  public void setApplyStartTime(Date applyStartTime) {
    this.applyStartTime = applyStartTime;
  }

  public Date getApplyEndTime() {
    return applyEndTime;
  }

  public void setApplyEndTime(Date applyEndTime) {
    this.applyEndTime = applyEndTime;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public ActivityChannel getChannel() {
    return channel;
  }

  public void setChannel(ActivityChannel channel) {
    this.channel = channel;
  }

  public String getTagImage() {
    return tagImage;
  }

  public void setTagImage(String tagImage) {
    this.tagImage = tagImage;
  }

  public String getApplyDesc() {
    return applyDesc;
  }

  public void setApplyDesc(String applyDesc) {
    this.applyDesc = applyDesc;
  }

  public Integer getActTagType() {
    return actTagType;
  }

  public void setActTagType(Integer actTagType) {
    this.actTagType = actTagType;
  }
}