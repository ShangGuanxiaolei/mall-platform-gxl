package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/11
 * Time:10:06
 * Des:弹框标记
 */
public class FreshManFlagVo {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT,
//    `cpid` bigint(20) NOT NULL COMMENT 'CPID',
//    `register_flag` int(11) NOT NULL DEFAULT '0' COMMENT '注册送德分弹层，1已弹，0未弹',
//    `first_flag` int(11) NOT NULL DEFAULT '0' COMMENT '首单弹窗，1已弹，0未弹',
//    `reach_flag` int(11) NOT NULL DEFAULT '0' COMMENT '消费满额弹窗，1已弹，0未弹',
//    `vip_flag` int(11) NOT NULL DEFAULT '0' COMMENT '升级VIP弹窗，1已弹，0未弹',
//    `register_flag_at` timestamp NULL DEFAULT NULL COMMENT '注册送德分弹层确认时间',
//    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//    `first_flag_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
//    `reach_flag_at` timestamp NULL DEFAULT NULL COMMENT '消费满额弹层确认时间',
//    `vip_flag_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    private Long id;
    private Long cpId;
    private Integer registerFlag; //注册送德分弹层，1已弹，0未弹
    private Integer firstFlag; //首单弹窗，1已弹，0未弹
    private Integer reachFlag; //消费满额弹窗，1已弹，0未弹
    private Integer vipFlag; //升级VIP弹窗，1已弹，0未弹
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date registerFlagAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date firstFlagAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date reachFlagAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date vipFlagAt;
    private Integer vipType;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date effectAt;

    public Integer getVipType() {
        return vipType;
    }

    public void setVipType(Integer vipType) {
        this.vipType = vipType;
    }

    public Date getEffectAt() {
        return effectAt;
    }

    public void setEffectAt(Date effectAt) {
        this.effectAt = effectAt;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public Integer getRegisterFlag() {
        return registerFlag;
    }

    public void setRegisterFlag(Integer registerFlag) {
        this.registerFlag = registerFlag;
    }

    public Integer getFirstFlag() {
        return firstFlag;
    }

    public void setFirstFlag(Integer firstFlag) {
        this.firstFlag = firstFlag;
    }

    public Integer getReachFlag() {
        return reachFlag;
    }

    public void setReachFlag(Integer reachFlag) {
        this.reachFlag = reachFlag;
    }

    public Integer getVipFlag() {
        return vipFlag;
    }

    public void setVipFlag(Integer vipFlag) {
        this.vipFlag = vipFlag;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getRegisterFlagAt() {
        return registerFlagAt;
    }

    public void setRegisterFlagAt(Date registerFlagAt) {
        this.registerFlagAt = registerFlagAt;
    }

    public Date getFirstFlagAt() {
        return firstFlagAt;
    }

    public void setFirstFlagAt(Date firstFlagAt) {
        this.firstFlagAt = firstFlagAt;
    }

    public Date getReachFlagAt() {
        return reachFlagAt;
    }

    public void setReachFlagAt(Date reachFlagAt) {
        this.reachFlagAt = reachFlagAt;
    }

    public Date getVipFlagAt() {
        return vipFlagAt;
    }

    public void setVipFlagAt(Date vipFlagAt) {
        this.vipFlagAt = vipFlagAt;
    }
}