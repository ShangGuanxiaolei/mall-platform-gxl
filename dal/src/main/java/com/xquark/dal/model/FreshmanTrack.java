package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * 新人轨迹 pojo
 */
public class FreshmanTrack extends BaseEntityImpl {

    private static final long serialVersionUID = 4024156457908530690L;

    private Long serialNo; //新客引导配置号

    private String grade; //等级

    private String amount; //累计消费金额（首次达到）

    private String point; //赠送德分数

    private Integer isGiftPoint; //是否赠送德分，1是，0否

    private String guideText; //引导文字，顶部大字

    private String step; //节点文字

    private String tip; //附加提示文字

    private String ext; //扩展文字信息

    public Long getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Long serialNo) {
        this.serialNo = serialNo;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public Integer getIsGiftPoint() {
        return isGiftPoint;
    }

    public void setIsGiftPoint(Integer isGiftPoint) {
        this.isGiftPoint = isGiftPoint;
    }

    public String getGuideText() {
        return guideText;
    }

    public void setGuideText(String guideText) {
        this.guideText = guideText;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
