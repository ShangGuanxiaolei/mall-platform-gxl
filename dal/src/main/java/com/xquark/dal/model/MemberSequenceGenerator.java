package com.xquark.dal.model;

import java.io.Serializable;

/**
 * xquark_member_sequence_generator
 *
 * @author wangxinhua
 */
public class MemberSequenceGenerator implements Serializable {

  private Long id;

  private String stub;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getStub() {
    return stub;
  }

  public void setStub(String stub) {
    this.stub = stub;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    MemberSequenceGenerator other = (MemberSequenceGenerator) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getStub() == null ? other.getStub() == null
        : this.getStub().equals(other.getStub()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getStub() == null) ? 0 : getStub().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(id);
    sb.append(", stub=").append(stub);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}