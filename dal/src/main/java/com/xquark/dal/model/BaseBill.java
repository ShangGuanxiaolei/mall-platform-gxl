package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.BillType;
import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午2:29
 * 发票基础类型
 */
public class BaseBill extends BaseEntityImpl implements Archivable {


    private Boolean archive;

    /**
     * 发票的类型
     */
    @NotNull(groups = {NormalGroup.class, ElectronicGroup.class})
    private BillType type;

    /**
     * 订单id
     */
    private String  orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BillType getType() {
        return type;
    }

    public void setType(BillType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString()+"BaseBill{" +
                "archive=" + archive +
                ", type=" + type +
                ", orderId='" + orderId + '\'' +
                '}';
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive=archive;
    }
}
