package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.ShopTreeApplicationStatus;
import com.xquark.dal.status.TwitterStatus;

public class ShopTreeChangeApplication extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerShopId;

  private String newParentShopId;

  private Boolean archive;

  private ShopTreeApplicationStatus status;

  private String shopId;

  public ShopTreeApplicationStatus getStatus() {
    return status;
  }

  public void setStatus(ShopTreeApplicationStatus status) {
    this.status = status;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getNewParentShopId() {
    return newParentShopId;
  }

  public void setNewParentShopId(String newParentShopId) {
    this.newParentShopId = newParentShopId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }
}