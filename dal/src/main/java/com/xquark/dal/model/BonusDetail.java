package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.BonusStatus;
import com.xquark.dal.status.BonusType;

import java.math.BigDecimal;
import java.util.Date;

public class BonusDetail extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private String parentId;

  private String level;

  private BigDecimal rate;

  private BigDecimal amount;

  private BigDecimal fee;

  private BonusStatus status;

  private String userId;

  private String firstNum;

  private String secondNum;

  private String thirdNum;

  private String statusStr;

  public String getStatusStr() {
    if (this.getStatus() == null) {
      return "";
    }
    String str = "";
    switch (this.getStatus()) {
      case SUCCESS:
        str = "已发放";
        break;
      case NEW:
        str = "未发放";
        break;
      default:
        str = this.getStatus().toString();
        break;
    }
    return str;
  }

  public String getFirstNum() {
    return firstNum;
  }

  public void setFirstNum(String firstNum) {
    this.firstNum = firstNum;
  }

  public String getSecondNum() {
    return secondNum;
  }

  public void setSecondNum(String secondNum) {
    this.secondNum = secondNum;
  }

  public String getThirdNum() {
    return thirdNum;
  }

  public void setThirdNum(String thirdNum) {
    this.thirdNum = thirdNum;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public BonusStatus getStatus() {
    return status;
  }

  public void setStatus(BonusStatus status) {
    this.status = status;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}