package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

public class PromotionBaseInfo extends BaseEntityImpl implements Archivable {
    private String pCode;
    private String pName;
    private String pType;
    private Date effectFrom;
    private Date effectTo;
    private String pStatus;
    private String creator;
    private String auditor;
    private Integer isWithPoint;
    private String updator;
    private Date createdAt;
    private Date updatedAt;
    private Integer isDeleted;
    private String tranCode;
    private Integer isCountEarning;
    private Integer isDeliveryReduction;




    public Integer getIsDeliveryReduction() {
        return isDeliveryReduction;
    }

    public void setIsDeliveryReduction(Integer isDeliveryReduction) {
        this.isDeliveryReduction = isDeliveryReduction;
    }

    public Integer getIsWithPoint() {
      return isWithPoint;
    }

    public void setIsWithPoint(Integer isWithPoint) {
      this.isWithPoint = isWithPoint;
    }

    public Integer getIsCountEarning() {
      return isCountEarning;
    }

    public void setIsCountEarning(Integer isCountEarning) {
      this.isCountEarning = isCountEarning;
    }

    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }




    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }
}
