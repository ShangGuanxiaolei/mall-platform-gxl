package com.xquark.dal.model.mypiece;


import java.sql.Timestamp;

/**
 * entity类--活动订单明细表(promotion_order_detail)
 * @类名: PromotionTempStock .
 * @描述: 拼团 .
 * @程序猿: guoxia .
 * @日期: 2018年9月7日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public class PromotionOrderDetail {
    private long id;
    private String order_head_code;
    private String sub_order_no;
    private String order_detail_code;
    private String p_code;
    private String p_detail_code;
    private String p_type;
    private long member_id;
    private Timestamp created_at;
    private Timestamp updated_at;
    private int is_deleted;
    private String piece_group_tran_code;
    private String piece_group_detail_code;
    private boolean is_cut_line;

    public String getSub_order_no() {
        return sub_order_no;
    }

    public void setSub_order_no(String sub_order_no) {
        this.sub_order_no = sub_order_no;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrder_head_code() {
        return order_head_code;
    }

    public void setOrder_head_code(String order_head_code) {
        this.order_head_code = order_head_code;
    }

    public String getOrder_detail_code() {
        return order_detail_code;
    }

    public void setOrder_detail_code(String order_detail_code) {
        this.order_detail_code = order_detail_code;
    }

    public String getP_code() {
        return p_code;
    }

    public void setP_code(String p_code) {
        this.p_code = p_code;
    }

    public String getP_detail_code() {
        return p_detail_code;
    }

    public void setP_detail_code(String p_detail_code) {
        this.p_detail_code = p_detail_code;
    }

    public String getP_type() {
        return p_type;
    }

    public void setP_type(String p_type) {
        this.p_type = p_type;
    }

    public long getMember_id() {
        return member_id;
    }

    public void setMember_id(long member_id) {
        this.member_id = member_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getPiece_group_tran_code() {
        return piece_group_tran_code;
    }

    public void setPiece_group_tran_code(String piece_group_tran_code) {
        this.piece_group_tran_code = piece_group_tran_code;
    }

    public String getPiece_group_detail_code() {
        return piece_group_detail_code;
    }

    public void setPiece_group_detail_code(String piece_group_detail_code) {
        this.piece_group_detail_code = piece_group_detail_code;
    }

    public boolean getIs_cut_line() {
        return is_cut_line;
    }

    public void setIs_cut_line(boolean is_cut_line) {
        this.is_cut_line = is_cut_line;
    }
}
