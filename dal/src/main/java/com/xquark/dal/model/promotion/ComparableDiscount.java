package com.xquark.dal.model.promotion;

/**
 * Created by wangxinhua on 17-10-9. DESC: 可比较的折扣接口
 */
public interface ComparableDiscount<T extends Number> {

  /**
   * 该折扣是否满足条件
   *
   * @param t 条件所需值
   * @return 是否满足
   */
  boolean isSatisfied(T t);

}
