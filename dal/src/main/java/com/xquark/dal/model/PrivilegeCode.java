package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;

import java.util.Date;

public class PrivilegeCode extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private String code;

  private int canQty;

  private int remainQty;

  private Date validFrom;

  private Date validTo;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getCanQty() {
    return canQty;
  }

  public void setCanQty(int canQty) {
    this.canQty = canQty;
  }

  public int getRemainQty() {
    return remainQty;
  }

  public void setRemainQty(int remainQty) {
    this.remainQty = remainQty;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

}