package com.xquark.dal.model;

import java.util.Date;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/13
 * Time:10:31
 * Des:dao实体类
 */
public class SystemConfigDO {
//    `id` bigint(20) NOT NULL AUTO_INCREMENT,
//    `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数名称',
//    `value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '参数值',
//    `remark` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数说明',
//    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    private int id;
    private String name;
    private String value;
    private String remark;
    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}