package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * xquark_sf_product_library
 *
 * @author wangxinhua
 */
public class SfProductLibrary extends BaseEntityImpl {

  /**
   * 商品库编码
   */
  private Long productEncode;

  private static final long serialVersionUID = 1L;

  public Long getProductEncode() {
    return productEncode;
  }

  public void setProductEncode(Long productEncode) {
    this.productEncode = productEncode;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    SfProductLibrary other = (SfProductLibrary) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getProductEncode() == null ? other.getProductEncode() == null
        : this.getProductEncode().equals(other.getProductEncode()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getProductEncode() == null) ? 0 : getProductEncode().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(getId());
    sb.append(", productEncode=").append(productEncode);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}