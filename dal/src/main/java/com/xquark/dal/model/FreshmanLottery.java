package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

/**
 * 新人首单抽奖 pojo
 */
public class FreshmanLottery extends BaseEntityImpl {

    private static final long serialVersionUID = 8124429917649790996L;

    private String serialNo; //序列号UUID

    private Integer point; //抽奖德分

    private BigDecimal probability; //抽中概率

    private Integer status; //是否生效，1生效，0失效

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
