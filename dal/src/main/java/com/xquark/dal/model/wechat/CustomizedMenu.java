package com.xquark.dal.model.wechat;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * 微信公众号自定义菜单
 */
public class CustomizedMenu {

  private List<CustomizedMenuItem> items;

  private CustomizedMenuItem[][] mapping = new CustomizedMenuItem[3][6];

  public CustomizedMenu() {
  }

  public CustomizedMenu(List<CustomizedMenuItem> items) {
    this.items = items;
    arrange();
  }

  public List<CustomizedMenuItem> getItems() {
    return items;
  }

  public void setItems(List<CustomizedMenuItem> items) {
    this.items = items;
    arrange();
  }

  private void arrange() {
    if (CollectionUtils.isEmpty(items)) {
      return;
    }

    for (int i = 0; i < items.size(); i++) {
      CustomizedMenuItem item = items.get(i);
      int index = item.getIndex();

      if (StringUtils.isEmpty(item.getParentId())) {
        mapping[index][0] = item;
      }
    }

    for (int i = 0; i < items.size(); i++) {
      CustomizedMenuItem item = items.get(i);
      String parentId = item.getParentId();

      if (StringUtils.isNotEmpty(parentId)) {
        for (int j = 0; j < 3; j++) {
          if (mapping[j][0] == null) {
            break;
          }
          String rootLevelId = mapping[j][0].getId();
          if (StringUtils.equals(rootLevelId, parentId)) {
            mapping[j][item.getIndex() + 1] = item;
          }
        }
      }
    }
  }

  public CustomizedMenuItem[][] getMapping() {
    return mapping;
  }

  public void setMapping(CustomizedMenuItem[][] mapping) {
    this.mapping = mapping;
  }

}
