package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.math.BigInteger;

public class WeixinJSTicket extends BaseEntityImpl {

  private static final long serialVersionUID = 5499098729123662464L;

  private String appId;
  private String apiSecret;
  private String accessToken;
  private String jsTicket;
  private long checkTime;
  private long renewCounter;

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appid) {
    this.appId = appid;
  }

  public String getApiSecret() {
    return apiSecret;
  }

  public void setApiSecret(String secret) {
    this.apiSecret = secret;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String access_token) {
    this.accessToken = access_token;
  }

  public String getJSTicket() {
    return jsTicket;
  }

  public void setJSTicket(String js_ticket) {
    this.jsTicket = js_ticket;
  }

  public long getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(long check_time) {
    this.checkTime = check_time;
  }

  public long getRenewCounter() {
    return renewCounter;
  }

  public void setRenewCounter(long renew_counter) {
    this.renewCounter = renew_counter;
  }
}
