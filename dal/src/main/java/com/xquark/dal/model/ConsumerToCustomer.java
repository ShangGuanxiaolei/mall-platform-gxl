package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class ConsumerToCustomer implements Serializable {

  public ConsumerToCustomer() {
  }

  public ConsumerToCustomer(Long consumerCpId, Long customerCpId) {
    this.consumerCpId = consumerCpId;
    this.customerCpId = customerCpId;
  }

  /**
   * id
   */
  private Long id;

  /**
   * 汉薇ID
   */
  private Long consumerCpId;

  /**
   * 分享人ID
   */
  private Long customerCpId;

  /**
   * 创建时间
   */
  private Date createDate;

  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getConsumerCpId() {
    return consumerCpId;
  }

  public void setConsumerCpId(Long consumerCpId) {
    this.consumerCpId = consumerCpId;
  }

  public Long getCustomerCpId() {
    return customerCpId;
  }

  public void setCustomerCpId(Long customerCpId) {
    this.customerCpId = customerCpId;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }
}