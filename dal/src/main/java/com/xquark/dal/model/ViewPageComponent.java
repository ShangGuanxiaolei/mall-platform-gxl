package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

/**
 * Created by chh on 16-10-17.
 */
public class ViewPageComponent extends BaseEntityImpl implements Archivable {

  private String componentId;
  private String componentParams;
  private String pageId;
  private BigDecimal hidx;
  private BigDecimal vidx;
  private boolean archive;

  public String getComponentId() {
    return componentId;
  }

  public void setComponentId(String componentId) {
    this.componentId = componentId;
  }

  public String getComponentParams() {
    return componentParams;
  }

  public void setComponentParams(String componentParams) {
    this.componentParams = componentParams;
  }

  public String getPageId() {
    return pageId;
  }

  public void setPageId(String pageId) {
    this.pageId = pageId;
  }

  public BigDecimal getHidx() {
    return hidx;
  }

  public void setHidx(BigDecimal hidx) {
    this.hidx = hidx;
  }

  public BigDecimal getVidx() {
    return vidx;
  }

  public void setVidx(BigDecimal vidx) {
    this.vidx = vidx;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
