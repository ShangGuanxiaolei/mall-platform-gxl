package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

public class MerchantSigninLog extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String merchantId;
  private String ip;
  private String client;
  private String browser;
  private String os;
  private String partner;
  private String deviceSN; //设备号  seker 20150202

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(String browser) {
    this.browser = browser;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }

  public String getDeviceSN() {
    return this.deviceSN;
  }

  public void setDeviceSN(String deviceSN) {
    this.deviceSN = deviceSN;
  }
}
