package com.xquark.dal.model.wechat;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * 微信模板消息
 */
public class TemplateMessage extends BaseEntityImpl implements Archivable {

  /**
   * 模板消息名称
   */
  private String templateName;

  /**
   * 模板消息编号
   */
  private String templateNo;

  /**
   * 模板消息在公众号中的ID
   */
  private String templateId;

  /**
   * 此模板消息所属的店铺
   */
  private String shopId;

  /**
   * 此模板消息所属的微信公众号
   */
  private String wechatAppId;

  private Boolean archive;

  public String getTemplateName() {
    return templateName;
  }

  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }

  public String getTemplateNo() {
    return templateNo;
  }

  public void setTemplateNo(String templateNo) {
    this.templateNo = templateNo;
  }

  public String getTemplateId() {
    return templateId;
  }

  public void setTemplateId(String templateId) {
    this.templateId = templateId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getWechatAppId() {
    return wechatAppId;
  }

  public void setWechatAppId(String wechatAppId) {
    this.wechatAppId = wechatAppId;
  }
}
