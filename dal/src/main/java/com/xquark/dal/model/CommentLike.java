package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * @author
 */
@ApiModel("点赞对象")
public class CommentLike extends BaseEntityImpl implements Archivable {

  /**
   * 点赞用户的id
   */
  @ApiModelProperty("点赞用户的id,无需传入!!默认取当前用户id")
  private String userId;

  /**
   * 评论的id
   */
  @ApiModelProperty("被点赞评论的id")
  private String commentId;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getCommentId() {
    return commentId;
  }

  public void setCommentId(String commentId) {
    this.commentId = commentId;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    CommentLike other = (CommentLike) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()));
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
    result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
    result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
    result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
    return result;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append(" [");
    sb.append("Hash = ").append(hashCode());
    sb.append(", id=").append(super.getId());
    sb.append(", createdAt=").append(super.getCreatedAt());
    sb.append(", updatedAt=").append(super.getUpdatedAt());
    sb.append(", userId=").append(userId);
    sb.append(", archive=").append(archive);
    sb.append(", serialVersionUID=").append(serialVersionUID);
    sb.append("]");
    return sb.toString();
  }
}