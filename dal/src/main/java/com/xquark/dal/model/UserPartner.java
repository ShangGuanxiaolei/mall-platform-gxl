package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.PartnerStatus;

public class UserPartner extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerShopId;

  private String userId;

  private Boolean archive;

  private PartnerStatus status;

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public PartnerStatus getStatus() {
    return status;
  }

  public void setStatus(PartnerStatus status) {
    this.status = status;
  }
}