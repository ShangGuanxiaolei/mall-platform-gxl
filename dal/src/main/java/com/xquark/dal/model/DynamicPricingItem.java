package com.xquark.dal.model;

import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/11
 * 包含动态计算的对象的对象
 */
public interface DynamicPricingItem {

  DynamicPricing getDynamicPricing();

  void setPrice(BigDecimal price);

  Integer getAmount();

}
