package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;

public class AuditRule extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Boolean archive;

  private AgentType buyerType;

  private AgentType sellerType;

  private AuditType auditType;

  private AuditRuleType type;

  public AuditRuleType getType() {
    return type;
  }

  public void setType(AuditRuleType type) {
    this.type = type;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


  public AgentType getBuyerType() {
    return buyerType;
  }

  public void setBuyerType(AgentType buyerType) {
    this.buyerType = buyerType;
  }

  public AgentType getSellerType() {
    return sellerType;
  }

  public void setSellerType(AgentType sellerType) {
    this.sellerType = sellerType;
  }

  public AuditType getAuditType() {
    return auditType;
  }

  public void setAuditType(AuditType auditType) {
    this.auditType = auditType;
  }
}