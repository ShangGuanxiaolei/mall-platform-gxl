package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * Created by quguangming on 16/6/27.
 */
public class MemberActionRecord extends BaseEntityImpl {

  private String memberId;
  private Date createdAt;
  private String shopId;
  private String actionType;
  private String operatorId;
  private Date expireAt;           /*过期时间*/
  private String actionLevelTo;    /*等级记录操作后数据 普通会员、金卡会员*/
  private String actionLevelFrom; /*等级记录操作前数据 普通会员、金卡会员*/
  private String actionPointsTo;   /*积分记录操作后数据 100*/
  private String actionPointsFrom; /*积分记录操作前数据 1000*/
  private String actionRemark;    /*记录操作备注*/
  private String actionReason;    /*记录操作原因*/


  public String getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(String operatorId) {
    this.operatorId = operatorId;
  }

  public String getActionRemark() {
    return actionRemark;
  }

  public void setActionRemark(String actionRemark) {
    this.actionRemark = actionRemark;
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public Date getExpireAt() {
    return expireAt;
  }

  public void setExpireAt(Date expireAt) {
    this.expireAt = expireAt;
  }

  public String getActionLevelTo() {
    return actionLevelTo;
  }

  public void setActionLevelTo(String actionLevelTo) {
    this.actionLevelTo = actionLevelTo;
  }

  public String getActionLevelFrom() {
    return actionLevelFrom;
  }

  public void setActionLevelFrom(String actionLevelFrom) {
    this.actionLevelFrom = actionLevelFrom;
  }

  public String getActionPointsTo() {
    return actionPointsTo;
  }

  public void setActionPointsTo(String actionPointsTo) {
    this.actionPointsTo = actionPointsTo;
  }

  public String getActionPointsFrom() {
    return actionPointsFrom;
  }

  public void setActionPointsFrom(String actionPointsFrom) {
    this.actionPointsFrom = actionPointsFrom;
  }

  public String getActionReason() {
    return actionReason;
  }

  public void setActionReason(String actionReason) {
    this.actionReason = actionReason;
  }
}
