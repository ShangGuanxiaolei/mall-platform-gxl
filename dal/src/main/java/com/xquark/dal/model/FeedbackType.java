package com.xquark.dal.model;

public enum FeedbackType {

  /**
   * 报错反馈
   */
  ERROR,
  /**
   * 咨询反馈
   */
  CONTACT,

  /**
   * 用户反馈
   */
  USER;
}
