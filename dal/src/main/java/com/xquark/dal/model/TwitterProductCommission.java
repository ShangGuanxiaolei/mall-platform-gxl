package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.CommissionProductType;


public class TwitterProductCommission extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private Long shopId;

  private String productId;

  private double firstLevelRate;

  private double secondLevelRate;

  private double thirdLevelRate;

  private Boolean archive;

  private CommissionProductType type;

  public CommissionProductType getType() {
    return type;
  }

  public void setType(CommissionProductType type) {
    this.type = type;
  }

  public Long getShopId() {
    return shopId;
  }

  public void setShopId(Long shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public double getFirstLevelRate() {
    return firstLevelRate;
  }

  public void setFirstLevelRate(double firstLevelRate) {
    this.firstLevelRate = firstLevelRate;
  }

  public double getSecondLevelRate() {
    return secondLevelRate;
  }

  public void setSecondLevelRate(double secondLevelRate) {
    this.secondLevelRate = secondLevelRate;
  }

  public double getThirdLevelRate() {
    return thirdLevelRate;
  }

  public void setThirdLevelRate(double thirdLevelRate) {
    this.thirdLevelRate = thirdLevelRate;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}