package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import org.hibernate.validator.constraints.NotBlank;

/**
 * User: huangjie Date: 2018/6/23. Time: 上午11:35 供应商
 */
public class Supplier extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  /***
   * code是否在@SkuCodeResourcesType中存在映射
   */
  private Boolean ifMappingToEnum = false;


  @NotBlank
  private String name;

  /***
   * 供应商编号
   */
  @NotBlank
  private String code;

  private Boolean archive;

  public Boolean getArchive() {
    return archive;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getMappingToEnum() {
    return ifMappingToEnum;
  }

  public void setIfMappingToEnum(Boolean ifMappingToEnum) {
    this.ifMappingToEnum = ifMappingToEnum;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  @Override
  public String toString() {
    return "supplier{" +
        "name='" + name + '\'' +
        ", archive=" + archive +
        '}';
  }
}