package com.xquark.dal.model.wechat;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.type.WechatServiceType;

import java.util.Date;

/**
 * 微信公众号配置信息
 */
public class WechatAppConfig extends BaseEntityImpl implements Archivable, PaymentConfig {

  /**
   * 公众号名称
   */
  private String appName;

  /**
   * ios版本号
   */
  private String iosVersion;

  /**
   * 开发ID
   */
  private String appId;

  /**
   * 接口加密签名
   */
  private String appSecret;

  /**
   * 访问用token
   */
  private String accessToken;

  /**
   * access token 过期时间
   */
  private Date accessTokenExpiredAt;

  /**
   * access token 更新计数
   */
  private int accessTokenRenewCounter;

  /**
   * oauth授权访问token
   */
  private String oauthAccessToken;

  /**
   * oauth授权过期时间
   */
  private Date oauthAccessTokenExpiredAt;

  /**
   * oauth更新授权token
   */
  private String oauthRenewToken;

  /**
   * oauth更新授权token计数
   */
  private int oauthAccessTokenRenewCounter;

  /**
   * js api访问凭证
   */
  private String jsapiTicket;

  /**
   * js api访问凭证过期时间
   */
  private Date jsapiTicketExpiredAt;

  /**
   * js api访问凭证更新计数
   */
  private int jsapiTicketRenewCounter;

  private Boolean archive;

  private String apiUrl;

  private String apiVerifyToken;

  private String encodingAESKey;

  private String mchId;

  private String mchKey;

  private String certFileContent;

  private String shopId;

  private byte[] cert_file;

  private String cert_password;

  /**
   * 信开放平台审核通过的应用APPID
   */
  private String appAppId;

  /**
   * 微信开放平台审核通过的应用APPsecret
   */
  private String appAppSecret;

  /**
   * 小程序appId
   */
  private String appMiniId;

  /**
   * 小程序商户号
   */
  private String appMiniSecret;

  /**
   * 微信app支付分配的商户号
   */
  private String appMchId;

  /**
   * 微信app支付分配的商户密钥
   */
  private String appMchKey;

  /**
   * 小程序商户号
   */
  private String miniMchId;

  /**
   * 小程序商户密钥
   */
  private String miniMchKey;

  private byte[] app_cert_file;

  private String app_cert_password;

  private String notifySite;

  private WechatServiceType type;

  public byte[] getApp_cert_file() {
    return app_cert_file;
  }

  public void setApp_cert_file(byte[] app_cert_file) {
    this.app_cert_file = app_cert_file;
  }

  public String getApp_cert_password() {
    return app_cert_password;
  }

  public void setApp_cert_password(String app_cert_password) {
    this.app_cert_password = app_cert_password;
  }

  public String getAppAppId() {
    return appAppId;
  }

  public void setAppAppId(String appAppId) {
    this.appAppId = appAppId;
  }

  public String getAppAppSecret() {
    return appAppSecret;
  }

  public void setAppAppSecret(String appAppSecret) {
    this.appAppSecret = appAppSecret;
  }

  public String getAppMchId() {
    return appMchId;
  }

  public void setAppMchId(String appMchId) {
    this.appMchId = appMchId;
  }

  public String getAppMchKey() {
    return appMchKey;
  }

  public void setAppMchKey(String appMchKey) {
    this.appMchKey = appMchKey;
  }

  public byte[] getCert_file() {
    return cert_file;
  }

  public String getCert_password() {
    return cert_password;
  }

  public void setCert_password(String cert_password) {
    this.cert_password = cert_password;
  }

  public void setCert_file(byte[] cert_file) {
    this.cert_file = cert_file;
  }

  public String getMchId() {
    return mchId;
  }

  @Override
  public String getPaymentId() {
      return getId();
  }

  @Override
  public String getCallbackSite() {
    return "";
  }

  @Override
  public String getNotifySite() {
    return notifySite;
  }

  public void setNotifySite(String notifySite) {
    this.notifySite = notifySite;
  }

  @Override
  public String getMchSecret() {
    return mchKey;
  }

  public void setMchId(String mchId) {
    this.mchId = mchId;
  }

  public String getMchKey() {
    return mchKey;
  }

  public void setMchKey(String mchKey) {
    this.mchKey = mchKey;
  }

  public String getCertFileContent() {
    return certFileContent;
  }

  public void setCertFileContent(String certFileContent) {
    this.certFileContent = certFileContent;
  }

  /**
   * 公众号在哪个环境下使用(dev,test,prod)
   */
  private String profile;

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getAppSecret() {
    return appSecret;
  }

  public void setAppSecret(String appSecret) {
    this.appSecret = appSecret;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public Date getAccessTokenExpiredAt() {
    return accessTokenExpiredAt;
  }

  public void setAccessTokenExpiredAt(Date accessTokenExpiredAt) {
    this.accessTokenExpiredAt = accessTokenExpiredAt;
  }

  public int getAccessTokenRenewCounter() {
    return accessTokenRenewCounter;
  }

  public void setAccessTokenRenewCounter(int accessTokenRenewCounter) {
    this.accessTokenRenewCounter = accessTokenRenewCounter;
  }

  public String getOauthAccessToken() {
    return oauthAccessToken;
  }

  public void setOauthAccessToken(String oauthAccessToken) {
    this.oauthAccessToken = oauthAccessToken;
  }

  public Date getOauthAccessTokenExpiredAt() {
    return oauthAccessTokenExpiredAt;
  }

  public void setOauthAccessTokenExpiredAt(Date oauthAccessTokenExpiredAt) {
    this.oauthAccessTokenExpiredAt = oauthAccessTokenExpiredAt;
  }

  public String getOauthRenewToken() {
    return oauthRenewToken;
  }

  public void setOauthRenewToken(String oauthRenewToken) {
    this.oauthRenewToken = oauthRenewToken;
  }

  public int getOauthAccessTokenRenewCounter() {
    return oauthAccessTokenRenewCounter;
  }

  public void setOauthAccessTokenRenewCounter(int oauthAccessTokenRenewCounter) {
    this.oauthAccessTokenRenewCounter = oauthAccessTokenRenewCounter;
  }

  public String getJsapiTicket() {
    return jsapiTicket;
  }

  public void setJsapiTicket(String jsapiTicket) {
    this.jsapiTicket = jsapiTicket;
  }

  public Date getJsapiTicketExpiredAt() {
    return jsapiTicketExpiredAt;
  }

  public void setJsapiTicketExpiredAt(Date jsapiTicketExpiredAt) {
    this.jsapiTicketExpiredAt = jsapiTicketExpiredAt;
  }

  public int getJsapiTicketRenewCounter() {
    return jsapiTicketRenewCounter;
  }

  public void setJsapiTicketRenewCounter(int jsapiTicketRenewCounter) {
    this.jsapiTicketRenewCounter = jsapiTicketRenewCounter;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getApiUrl() {
    return apiUrl;
  }

  public void setApiUrl(String apiUrl) {
    this.apiUrl = apiUrl;
  }

  public String getApiVerifyToken() {
    return apiVerifyToken;
  }

  public void setApiVerifyToken(String apiVerifyToken) {
    this.apiVerifyToken = apiVerifyToken;
  }

  public String getEncodingAESKey() {
    return encodingAESKey;
  }

  public void setEncodingAESKey(String encodingAESKey) {
    this.encodingAESKey = encodingAESKey;
  }

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getIosVersion() {
    return iosVersion;
  }

  public void setIosVersion(String iosVersion) {
    this.iosVersion = iosVersion;
  }

  public String getAppMiniId() {
    return appMiniId;
  }

  public void setAppMiniId(String appMiniId) {
    this.appMiniId = appMiniId;
  }

  public String getAppMiniSecret() {
    return appMiniSecret;
  }

  public void setAppMiniSecret(String appMiniSecret) {
    this.appMiniSecret = appMiniSecret;
  }

  public String getMiniMchId() {
    return miniMchId;
  }

  public void setMiniMchId(String miniMchId) {
    this.miniMchId = miniMchId;
  }

  public String getMiniMchKey() {
    return miniMchKey;
  }

  public void setMiniMchKey(String miniMchKey) {
    this.miniMchKey = miniMchKey;
  }

  public WechatServiceType getType() {
    return type;
  }

  public void setType(WechatServiceType type) {
    this.type = type;
  }
}
