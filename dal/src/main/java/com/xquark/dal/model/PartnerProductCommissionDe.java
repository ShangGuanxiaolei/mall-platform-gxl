package com.xquark.dal.model;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CommissionType;

import java.math.BigDecimal;


public class PartnerProductCommissionDe extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String parentId;

  private CommissionType type;

  private BigDecimal rate;

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public CommissionType getType() {
    return type;
  }

  public void setType(CommissionType type) {
    this.type = type;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  private Boolean archive;

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }


}