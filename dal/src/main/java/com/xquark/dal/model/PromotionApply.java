package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PromotionApplyStatus;
import com.xquark.dal.type.PromotionType;
import java.util.Date;

/**
 * @author
 */
public class PromotionApply extends BaseEntityImpl implements Archivable {

  public PromotionApply() {
  }

  public PromotionApply(String orderId, String promotionProductId, PromotionType type) {
    this.orderId = orderId;
    this.promotionProductId = promotionProductId;
    this.type = type;
  }

  private String id;

  /**
   * 活动商品id
   */
  private String promotionProductId;

  /**
   * 关联订单
   */
  private String orderId;

  /**
   * 活动类型
   */
  private PromotionType type;

  /**
   * 审核状态
   */
  private PromotionApplyStatus status;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionProductId() {
    return promotionProductId;
  }

  public void setPromotionProductId(String promotionProductId) {
    this.promotionProductId = promotionProductId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public PromotionType getType() {
    return type;
  }

  public void setType(PromotionType type) {
    this.type = type;
  }

  public PromotionApplyStatus getStatus() {
    return status;
  }

  public void setStatus(PromotionApplyStatus status) {
    this.status = status;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}