package com.xquark.dal.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @auther liuwei
 * @date 2018/6/16 15:48
 */
public class GoodInfo {
  private String PlatProductID;//产品id
  private String name;//商品名称
  private String OuterID;//外部商家编码
  private BigDecimal price = new BigDecimal(0);//商品价格
  private int num;//商品数量
  private String pictureurl;//图片URL
  private String whsecode;//商品所在仓库编号
  private List<Skus> skus;//规格集合
  private String SkuID;//平台规格ID

  public String getSkuID() {
    return SkuID;
  }

  public void setSkuID(String skuID) {
    SkuID = skuID;
  }

  public String getPlatProductID() {
    return PlatProductID;
  }

  public void setPlatProductID(String platProductID) {
    PlatProductID = platProductID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOuterID() {
    return OuterID;
  }

  public void setOuterID(String outerID) {
    OuterID = outerID;
  }

  public BigDecimal getPrice() {
    return price;
  }

//  public void setPrice(BigDecimal price) {
//    this.price = price;
//  }

  public int getNum() {
    return num;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public String getPictureurl() {
    return pictureurl;
  }

  public void setPictureurl(String pictureurl) {
    this.pictureurl = pictureurl;
  }

  public String getWhsecode() {
    return whsecode;
  }

  public void setWhsecode(String whsecode) {
    this.whsecode = whsecode;
  }

  public List<Skus> getSkus() {
    return skus;
  }

  public void setSkus(List<Skus> skus) {
    this.skus = skus;
  }

  @Override
  public String toString() {
    return "GoodInfo{" +
        "PlatProductID='" + PlatProductID + '\'' +
        ", name='" + name + '\'' +
        ", OuterID='" + OuterID + '\'' +
        ", price=" + price +
        ", num=" + num +
        ", pictureurl='" + pictureurl + '\'' +
        ", whsecode='" + whsecode + '\'' +
        ", skus=" + skus +
        '}';
  }
}
