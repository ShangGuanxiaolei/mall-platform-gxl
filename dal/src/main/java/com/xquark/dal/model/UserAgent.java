package com.xquark.dal.model;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.TwitterStatus;
import org.apache.commons.lang3.time.DateFormatUtils;

public class UserAgent extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "不需要传入")
  private String ownerShopId;

  @ApiModelProperty(value = "不需要传入")
  private String userId;

  @ApiModelProperty(value = "不需要传入")
  private Boolean archive;

  @ApiModelProperty(value = "不需要传入")
  private AgentStatus status;

  @ApiModelProperty(value = "不需要传入")
  private String parentUserId;

  @ApiModelProperty(value = "代理姓名", required = true)
  private String name;

  @ApiModelProperty(value = "微信号")
  private String weixin;

  @ApiModelProperty(value = "手机号", required = true)
  private String phone;

  @ApiModelProperty(value = "邮箱")
  private String email;

  @ApiModelProperty(value = "身份证号")
  private String idcard;

  @ApiModelProperty(value = "住址的明细街区信息")
  private String address;

  @ApiModelProperty(value = "不需要传入")
  private String idcardImg;

  @ApiModelProperty(value = "不需要传入")
  private String lifeImg;

  @ApiModelProperty(value = "不需要传入")
  private AgentType type;

  @ApiModelProperty(value = "不需要传入")
  private String role;

  @ApiModelProperty(value = "不需要传入")
  private String typeStr;

  @ApiModelProperty(value = "不需要传入")
  private String createdAtStr;

  @ApiModelProperty(value = "不需要传入")
  private String statusStr;

  private String contractImg;

  private String certImg;

  private String score;

  private String sex;

  private String sexStr;

  private String companyName;

  private String description;

  private String wxbotId;

  private String area;

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getWxbotId() {
    return wxbotId;
  }

  public void setWxbotId(String wxbotId) {
    this.wxbotId = wxbotId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getSexStr() {
    if (this.getSex() == null) {
      return "";
    }
    String str = "";
    if ("1".equals(this.getSex())) {
      str = "男";
    } else if ("0".equals(this.getSex())) {
      str = "女";
    }
    return str;
  }

  public void setSexStr(String sexStr) {
    this.sexStr = sexStr;
  }

  public String getScore() {
    return score;
  }

  public void setScore(String score) {
    this.score = score;
  }

  public String getCertImg() {
    return certImg;
  }

  public void setCertImg(String certImg) {
    this.certImg = certImg;
  }

  public String getContractImg() {
    return contractImg;
  }

  public void setContractImg(String contractImg) {
    this.contractImg = contractImg;
  }

  public String getStatusStr() {
    if (this.getStatus() == null) {
      return "";
    }
    String str = "";
    switch (this.getStatus()) {
      case ACTIVE:
        str = "已审核";
        break;
      case FROZEN:
        str = "已冻结";
        break;
      case APPLYING:
        str = "申请中";
        break;
      default:
        str = this.getStatus().toString();
        break;
    }
    return str;
  }

  public String getTypeStr() {
    if (this.getType() == null) {
      return "";
    }
    String str = "";
    switch (this.getType()) {
      case FOUNDER:
        str = "联合创始人";
        break;
      case DIRECTOR:
        str = "董事";
        break;
      case GENERAL:
        str = "总顾问";
        break;
      case FIRST:
        str = "一星顾问";
        break;
      case SECOND:
        str = "二星顾问";
        break;
      case SPECIAL:
        str = "特约";
        break;
      default:
        str = this.getType().toString();
        break;
    }
    return str;
  }

  public void setTypeStr(String typeStr) {
    this.typeStr = typeStr;
  }

  public void setCreatedAtStr(String createdAtStr) {
    this.createdAtStr = createdAtStr;
  }

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getParentUserId() {
    return parentUserId;
  }

  public void setParentUserId(String parentUserId) {
    this.parentUserId = parentUserId;
  }

  public AgentStatus getStatus() {
    return status;
  }

  public void setStatus(AgentStatus status) {
    this.status = status;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getWeixin() {
    return weixin;
  }

  public void setWeixin(String weixin) {
    this.weixin = weixin;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getIdcard() {
    return idcard;
  }

  public void setIdcard(String idcard) {
    this.idcard = idcard;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getIdcardImg() {
    return idcardImg;
  }

  public void setIdcardImg(String idcardImg) {
    this.idcardImg = idcardImg;
  }

  public String getLifeImg() {
    return lifeImg;
  }

  public void setLifeImg(String lifeImg) {
    this.lifeImg = lifeImg;
  }

  public AgentType getType() {
    return type;
  }

  public void setType(AgentType type) {
    this.type = type;
  }
}