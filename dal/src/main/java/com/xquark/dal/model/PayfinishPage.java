package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * 支付完成页 pojo
 */
public class PayfinishPage extends BaseEntityImpl {

    private static final long serialVersionUID = 8786949573258867088L;

    private String orderNo; //订单号

    private int payStatus; //支付状态

    private String paidFee; //支付金额

    private Boolean isWhiteCustom; //是否是白人

    private Boolean showLottery; //是否展示抽奖

    private Boolean showFreshmanTrack; //是否展示新人轨迹

    private Boolean showUpgradeVIP; //是否展示升级汉薇会员

    private FreshmanWindowInfoVo toastInfo; //弹窗类型

    private FreshmanLotteryInfoVo lottery; //抽奖数据

    private FreshmanTrackInfoVo freshmanGuide; //新人轨迹数据

    private Boolean showSpringWindow; //是否展示弹窗(满200和升VIP弹窗)

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public String getPaidFee() {
        return paidFee;
    }

    public void setPaidFee(String paidFee) {
        this.paidFee = paidFee;
    }

    public Boolean getIsWhiteCustom() {
        return isWhiteCustom;
    }

    public void setWhiteCustom(Boolean whiteCustom) {
        isWhiteCustom = whiteCustom;
    }

    public Boolean getShowLottery() {
        return showLottery;
    }

    public void setShowLottery(Boolean showLottery) {
        this.showLottery = showLottery;
    }

    public Boolean getShowFreshmanTrack() {
        return showFreshmanTrack;
    }

    public void setShowFreshmanTrack(Boolean showFreshmanTrack) {
        this.showFreshmanTrack = showFreshmanTrack;
    }

    public Boolean getShowUpgradeVIP() {
        return showUpgradeVIP;
    }

    public void setShowUpgradeVIP(Boolean showUpgradeVIP) {
        this.showUpgradeVIP = showUpgradeVIP;
    }

    public FreshmanWindowInfoVo getToastInfo() {
        return toastInfo;
    }

    public void setToastInfo(FreshmanWindowInfoVo toastInfo) {
        this.toastInfo = toastInfo;
    }

    public FreshmanLotteryInfoVo getLottery() {
        return lottery;
    }

    public void setLottery(FreshmanLotteryInfoVo lottery) {
        this.lottery = lottery;
    }

    public FreshmanTrackInfoVo getFreshmanGuide() {
        return freshmanGuide;
    }

    public void setFreshmanGuide(FreshmanTrackInfoVo freshmanGuide) {
        this.freshmanGuide = freshmanGuide;
    }

    public Boolean getShowSpringWindow() {
        return showSpringWindow;
    }

    public void setShowSpringWindow(Boolean showSpringWindow) {
        this.showSpringWindow = showSpringWindow;
    }
}
