package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

public class PostAgeSet extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String shopId;
  private String postageSet;

  private String name;

  private String type;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getPostageSet() {
    return postageSet;
  }

  public void setPostageSet(String postageSet) {
    this.postageSet = postageSet;
  }

}
