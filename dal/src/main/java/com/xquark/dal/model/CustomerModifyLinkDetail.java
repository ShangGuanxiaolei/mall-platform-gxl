package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * @author  hs
 */
public class CustomerModifyLinkDetail extends BaseEntityImpl {

    private Long cpid;

    /**
     * 变更方向
  WEAK_TO_SUBSTRONG   弱关系-次强关系
  SUBSTRONG_TO_STRONG 次强关系-强关系
  WEAK_TO_STRONG      弱关系-强关系
     */
    private String linkDirection;

    /**
     * 修改前上级cpId
     */
    private Long fromCpid;

    /**
     * 修改后上级cpId
     */
    private Long toCpid;

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(Long cpid) {
        this.cpid = cpid;
    }

    public String getLinkDirection() {
        return linkDirection;
    }

    public void setLinkDirection(String linkDirection) {
        this.linkDirection = linkDirection;
    }

    public Long getFromCpid() {
        return fromCpid;
    }

    public void setFromCpid(Long fromCpid) {
        this.fromCpid = fromCpid;
    }

    public Long getToCpid() {
        return toCpid;
    }

    public void setToCpid(Long toCpid) {
        this.toCpid = toCpid;
    }
}