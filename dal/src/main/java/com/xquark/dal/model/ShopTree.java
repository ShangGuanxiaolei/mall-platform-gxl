package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

public class ShopTree extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ancestorShopId;

  private String descendantShopId;

  private String rootShopId;

  private Integer shopDepth;

  private Boolean archive; // 是否是逻辑删除

  public String getRootShopId() {
    return rootShopId;
  }

  public void setRootShopId(String rootShopId) {
    this.rootShopId = rootShopId;
  }

  public Integer getShopDepth() {
    return shopDepth;
  }

  public void setShopDepth(int shopDepth) {
    this.shopDepth = shopDepth;
  }

  public String getAncestorShopId() {
    return ancestorShopId;
  }

  public void setAncestorShopId(String ancestorShopId) {
    this.ancestorShopId = ancestorShopId;
  }

  public String getDescendantShopId() {
    return descendantShopId;
  }

  public void setDescendantShopId(String descendantShopId) {
    this.descendantShopId = descendantShopId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}