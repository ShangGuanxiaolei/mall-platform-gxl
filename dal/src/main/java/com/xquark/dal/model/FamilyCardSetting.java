package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.FamilyCardStatus;

import java.math.BigDecimal;
import java.util.Date;

public class FamilyCardSetting extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String ownerShopId;

  private String name;

  private String level;

  private double personalCmRate;

  private double subordinateCmRate;

  private Integer amountDemand;

  private Integer levelUpAmount;

  private FamilyCardStatus status;

  private Boolean archive;

  public FamilyCardStatus getStatus() {
    return status;
  }

  public void setStatus(FamilyCardStatus status) {
    this.status = status;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public double getPersonalCmRate() {
    return personalCmRate;
  }

  public void setPersonalCmRate(double personalCmRate) {
    this.personalCmRate = personalCmRate;
  }

  public double getSubordinateCmRate() {
    return subordinateCmRate;
  }

  public void setSubordinateCmRate(double subordinateCmRate) {
    this.subordinateCmRate = subordinateCmRate;
  }

  public Integer getAmountDemand() {
    return amountDemand;
  }

  public void setAmountDemand(Integer amountDemand) {
    this.amountDemand = amountDemand;
  }

  public Integer getLevelUpAmount() {
    return levelUpAmount;
  }

  public void setLevelUpAmount(Integer levelUpAmount) {
    this.levelUpAmount = levelUpAmount;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}