package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.MessageStatus;
import com.xquark.dal.type.MessageType;

public class Message extends BaseEntityImpl {

  private static final long serialVersionUID = 5299720757613494127L;

  public Message() {
  }

  public Message(String title, String content, MessageType type) {
    this.title = title;
    this.content = content;
    this.type = type;
  }

  private String title;

  private String content;

  private MessageType type;

  private String url;

  private MessageStatus status;

  private String sessId;

  private String replyTo;

  // 返回json格式的订单数据字符串，供小程序展示消息使用
  private String data;

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public MessageStatus getStatus() {
    return status;
  }

  public void setStatus(MessageStatus status) {
    this.status = status;
  }

  public String getSessId() {
    return sessId;
  }

  public void setSessId(String sessId) {
    this.sessId = sessId;
  }

  public String getReplyTo() {
    return replyTo;
  }

  public void setReplyTo(String replyTo) {
    this.replyTo = replyTo;
  }

  public MessageType getType() {
    return type;
  }

  public void setType(MessageType type) {
    this.type = type;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
