package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * xquark_order_third_info_detail
 * @author 
 */
public class XquarkOrderThirdInfoDetail implements Serializable {
    private Long id;

    /**
     * 第三方订单号
     */
    private String thirdOrderNo;

    /**
     * 订单批次
     */
    private String lot;

    /**
     * 收件人手机号
     */
    private String phone;

    /**
     * 收货人
     */
    private String receiver;

    /**
     * 第三方订单子订单编号
     */
    private String thirdOrderItemNo;

    /**
     * 商品唯一编码
     */
    private String skuCode;

    /**
     * 导入状态（1为成功，0为失败，默认为1）
     */
    private String importStatus;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 创建时间
     */
    private Date createAt;

    /**
     * 更新时间
     */
    private Date updateAt;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getThirdOrderNo() {
        return thirdOrderNo;
    }

    public void setThirdOrderNo(String thirdOrderNo) {
        this.thirdOrderNo = thirdOrderNo;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getThirdOrderItemNo() {
        return thirdOrderItemNo;
    }

    public void setThirdOrderItemNo(String thirdOrderItemNo) {
        this.thirdOrderItemNo = thirdOrderItemNo;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkOrderThirdInfoDetail other = (XquarkOrderThirdInfoDetail) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getThirdOrderNo() == null ? other.getThirdOrderNo() == null : this.getThirdOrderNo().equals(other.getThirdOrderNo()))
            && (this.getLot() == null ? other.getLot() == null : this.getLot().equals(other.getLot()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getReceiver() == null ? other.getReceiver() == null : this.getReceiver().equals(other.getReceiver()))
            && (this.getThirdOrderItemNo() == null ? other.getThirdOrderItemNo() == null : this.getThirdOrderItemNo().equals(other.getThirdOrderItemNo()))
            && (this.getSkuCode() == null ? other.getSkuCode() == null : this.getSkuCode().equals(other.getSkuCode()))
            && (this.getImportStatus() == null ? other.getImportStatus() == null : this.getImportStatus().equals(other.getImportStatus()))
            && (this.getReason() == null ? other.getReason() == null : this.getReason().equals(other.getReason()))
            && (this.getCreateAt() == null ? other.getCreateAt() == null : this.getCreateAt().equals(other.getCreateAt()))
            && (this.getUpdateAt() == null ? other.getUpdateAt() == null : this.getUpdateAt().equals(other.getUpdateAt()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getThirdOrderNo() == null) ? 0 : getThirdOrderNo().hashCode());
        result = prime * result + ((getLot() == null) ? 0 : getLot().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getReceiver() == null) ? 0 : getReceiver().hashCode());
        result = prime * result + ((getThirdOrderItemNo() == null) ? 0 : getThirdOrderItemNo().hashCode());
        result = prime * result + ((getSkuCode() == null) ? 0 : getSkuCode().hashCode());
        result = prime * result + ((getImportStatus() == null) ? 0 : getImportStatus().hashCode());
        result = prime * result + ((getReason() == null) ? 0 : getReason().hashCode());
        result = prime * result + ((getCreateAt() == null) ? 0 : getCreateAt().hashCode());
        result = prime * result + ((getUpdateAt() == null) ? 0 : getUpdateAt().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", thirdOrderNo=").append(thirdOrderNo);
        sb.append(", lot=").append(lot);
        sb.append(", phone=").append(phone);
        sb.append(", receiver=").append(receiver);
        sb.append(", thirdOrderItemNo=").append(thirdOrderItemNo);
        sb.append(", skuCode=").append(skuCode);
        sb.append(", importStatus=").append(importStatus);
        sb.append(", reason=").append(reason);
        sb.append(", createAt=").append(createAt);
        sb.append(", updateAt=").append(updateAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}