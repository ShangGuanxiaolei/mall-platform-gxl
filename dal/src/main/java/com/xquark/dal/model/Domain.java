package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

public class Domain extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String name;

  private String memo;

  private String code;

  private String adminUserId;

  private Boolean archive;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo == null ? null : memo.trim();
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code == null ? null : code.trim();
  }

  public String getAdminUserId() {
    return adminUserId;
  }

  public void setAdminUserId(String adminUserId) {
    this.adminUserId = adminUserId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}