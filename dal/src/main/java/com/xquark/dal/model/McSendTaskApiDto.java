package com.xquark.dal.model;

import java.util.Map;

public class McSendTaskApiDto {

  private String taskNo;      //任务编号
  private long chanelTypeId;    //任务类型
  private String signatureNo;    //签名编号
  private String[] originalID;  //唯一编号
  private String[] emails;    //邮件
  private String[] mobiles;    //手机
  private Map<String, String> contentVar;//变量替换

  public long getChanelTypeId() {
    return chanelTypeId;
  }

  public Map<String, String> getContentVar() {
    return contentVar;
  }

  public String[] getEmails() {
    return emails;
  }

  public String[] getMobiles() {
    return mobiles;
  }

  public String[] getOriginalID() {
    return originalID;
  }

  public String getSignatureNo() {
    return signatureNo;
  }

  public String getTaskNo() {
    return taskNo;
  }

  public void setChanelTypeId(long chanelTypeId) {
    this.chanelTypeId = chanelTypeId;
  }

  public void setContentVar(Map<String, String> contentVar) {
    this.contentVar = contentVar;
  }

  public void setEmails(String[] emails) {
    this.emails = emails;
  }

  public void setMobiles(String[] mobiles) {
    this.mobiles = mobiles;
  }

  public void setOriginalID(String[] originalID) {
    this.originalID = originalID;
  }

  public void setSignatureNo(String signatureNo) {
    this.signatureNo = signatureNo;
  }

  public void setTaskNo(String taskNo) {
    this.taskNo = taskNo;
  }
}
