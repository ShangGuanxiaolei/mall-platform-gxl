package com.xquark.dal.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * VIP套装列表实体类
 */
public class VipProduct extends BaseEntityImpl implements Archivable, DynamicPricing {
    
    private static final long serialVersionUID = 1L;
    private String productName; // vip套装商品名称
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img; // 商品图片url
    private BigDecimal marketPrice; // 市场价(原价)
    private BigDecimal price; // 兑换价
    private BigDecimal deductionDPoint; // 德分
    private BigDecimal netWorth; // 净值
    private Integer skuNum; // vip套装商品库存
    private String productId; 
    private Integer vipsuitStatus; // vip套装商品状态
    private String supplierName; // 供应商名称
    
    @ApiModelProperty(value = "商品是否已经删除")
    private Boolean archive;

    private BigDecimal serverAmt;
    private BigDecimal point;
    private BigDecimal memberPrice;
    private BigDecimal proxyPrice;
    private BigDecimal changePrice;
    private BigDecimal reducedPrice;
    private int selfOperated; //自营标签
    private BigDecimal earnedPrice;

    public BigDecimal getEarnedPrice() {
        return earnedPrice;
    }

    public void setEarnedPrice(BigDecimal earnedPrice) {
        this.earnedPrice = earnedPrice;
    }

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }

    private BigDecimal promoAmt = BigDecimal.ZERO;

    public Integer getVipsuitStatus() {
        return vipsuitStatus;
    }

    public void setVipsuitStatus(Integer vipsuitStatus) {
        this.vipsuitStatus = vipsuitStatus;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
    
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    @Override
    public BigDecimal getReduction() {
        return getPoint();
    }

    @Override
    public void setReduction(BigDecimal point) {
        this.setPoint(point);
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    @Override
    public BigDecimal getConversionPrice() {
        return this.getPrice()
        .subtract(
            this.getDeductionDPoint().divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_DOWN));
    }

    @Override
    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    @Override
    public void setProxyPrice(BigDecimal proxyPrice) {
        this.proxyPrice = proxyPrice;
    }

    @Override
    public void setChangePrice(BigDecimal changePrice) {
        this.changePrice = changePrice;
    }

    @Override
    public void setReducedPrice(BigDecimal reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    @Override
    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public BigDecimal getProxyPrice() {
        return proxyPrice;
    }

    public BigDecimal getChangePrice() {
        return changePrice;
    }

    public BigDecimal getReducedPrice() {
        return reducedPrice;
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
