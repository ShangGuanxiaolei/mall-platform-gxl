package com.xquark.dal.model.promotion;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.type.CouponType;
import com.xquark.dal.type.UserCouponType;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 店铺优惠券
 */
public class Coupon extends BaseEntityImpl implements Archivable {


  private String code;           //优惠券编码

  private String name = "店铺优惠券";           //优惠券名称

  private BigDecimal discount;   //折扣金额

  private CouponType type;       //优惠券类型

  // 优惠券使用范围
  private UserCouponType scope;

  private Integer amount;        //发布优惠券数量

  private Integer remainder;        //剩余数量

  private BigDecimal totalDiscount; //发布总金额

  private Date distributedAt;    // 何时发行

  private Date validFrom;        //优惠券有效期开始日

  private Date validTo;          //优惠券有效期截止日

  private CouponStatus status;   //优惠券状态

  private String batchNo;        // 发行批次号

  private Integer acquireLimit;  // 优惠券的领取上限

  private Integer applyLimit;    // 下单可使用张数

  private BigDecimal applyAbove; // 满减

  private String shopId;         // 发行该优惠券的店铺

  private Date createAt;         //创建时间

  private Date updateAt;         //更新时间

  private Integer usedCount;      //使用数量

  private Boolean archive;

  private String productId;

  private String categoryId;

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Integer getUsedCount() {
    return usedCount;
  }

  public void setUsedCount(Integer usedCount) {
    this.usedCount = usedCount;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public Integer getRemainder() {
    return remainder;
  }

  public void setRemainder(Integer remainder) {
    this.remainder = remainder;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public CouponType getType() {
    return type;
  }

  public void setType(CouponType type) {
    this.type = type;
  }

  public UserCouponType getScope() {
    return scope;
  }

  public void setScope(UserCouponType scope) {
    this.scope = scope;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public BigDecimal getTotalDiscount() {
    return totalDiscount;
  }

  public void setTotalDiscount(BigDecimal totalDiscount) {
    this.totalDiscount = totalDiscount;
  }

  public Date getDistributedAt() {
    return distributedAt;
  }

  public void setDistributedAt(Date distributedAt) {
    this.distributedAt = distributedAt;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public CouponStatus getStatus() {
    return status;
  }

  public void setStatus(CouponStatus status) {
    this.status = status;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public Integer getAcquireLimit() {
    return acquireLimit;
  }

  public void setAcquireLimit(Integer acquireLimit) {
    this.acquireLimit = acquireLimit;
  }

  public Integer getApplyLimit() {
    return applyLimit;
  }

  public void setApplyLimit(Integer applyLimit) {
    this.applyLimit = applyLimit;
  }

  public BigDecimal getApplyAbove() {
    return applyAbove;
  }

  public void setApplyAbove(BigDecimal applyAbove) {
    this.applyAbove = applyAbove;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
