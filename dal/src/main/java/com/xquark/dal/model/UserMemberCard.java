package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

/**
 * Created by wangxinhua on 17-6-28. DESC:
 */
public class UserMemberCard extends BaseEntityImpl implements Archivable {

  private String id;

  private String ownerShopId;

  private String userId;

  private String cardId;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getOwnerShopId() {
    return ownerShopId;
  }

  public void setOwnerShopId(String ownerShopId) {
    this.ownerShopId = ownerShopId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCardId() {
    return cardId;
  }

  public void setCardId(String cardId) {
    this.cardId = cardId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
