package com.xquark.dal.model;

/**
 * 统计用户新春战绩VO
 */
public class PacketRainCount {

  /**用户领取的红包总数*/
  private Integer packetCount = 0;

  /**用户领取的德分红包总额*/
  private Integer totalPoint = 0;

  public Integer getPacketCount() {
    return packetCount;
  }

  public void setPacketCount(Integer packetCount) {
    this.packetCount = packetCount;
  }

  public Integer getTotalPoint() {
    return totalPoint;
  }

  public void setTotalPoint(Integer totalPoint) {
    this.totalPoint = totalPoint;
  }
}
