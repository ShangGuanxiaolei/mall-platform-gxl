package com.xquark.dal.model;

/**
 * @auther liuwei
 * @date 2018/6/16 20:19
 */
public class PolyDPParam {
  private String ProductId;
  private String ProductName;
  private String Status;
  private Integer PageIndex;
  private Integer PageSize;

  public String getProductId() {
    return ProductId;
  }

  public void setProductId(String productId) {
    ProductId = productId;
  }

  public String getProductName() {
    return ProductName;
  }

  public void setProductName(String productName) {
    ProductName = productName;
  }

  public String getStatus() {
    return Status;
  }

  public void setStatus(String status) {
    Status = status;
  }

  public Integer getPageIndex() {
    return PageIndex;
  }

  public void setPageIndex(Integer pageIndex) {
    PageIndex = pageIndex;
  }

  public Integer getPageSize() {
    return PageSize;
  }

  public void setPageSize(Integer pageSize) {
    PageSize = pageSize;
  }

  @Override
  public String toString() {
    return "PolyDPParam{" +
        "ProductId='" + ProductId + '\'' +
        ", ProductName='" + ProductName + '\'' +
        ", Status='" + Status + '\'' +
        ", PageIndex=" + PageIndex +
        ", PageSize=" + PageSize +
        '}';
  }
}
