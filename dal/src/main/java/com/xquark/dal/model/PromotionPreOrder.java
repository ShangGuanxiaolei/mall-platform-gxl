package com.xquark.dal.model;

import com.xquark.dal.model.promotion.PromotionProduct;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class PromotionPreOrder extends PromotionProduct {

  private static final long serialVersionUID = 5697675687366863175L;

  /**
   * 预购价
   */
  private BigDecimal preOrderPrice;

  /**
   * 达到多少积分免预购价格
   */
  private Long freePoints;

  /**
   * 预约最大人数
   */
  private Integer maxCount;

  private Date canPayTime;

  public Integer getMaxCount() {
    return maxCount;
  }

  public void setMaxCount(Integer maxCount) {
    this.maxCount = maxCount;
  }

  public BigDecimal getPreOrderPrice() {
    return preOrderPrice;
  }

  public void setPreOrderPrice(BigDecimal preOrderPrice) {
    this.preOrderPrice = preOrderPrice;
  }

  public Long getFreePoints() {
    return freePoints;
  }

  public void setFreePoints(Long freePoints) {
    this.freePoints = freePoints;
  }

  public Date getCanPayTime() {
    return canPayTime;
  }

  public void setCanPayTime(Date canPayTime) {
    this.canPayTime = canPayTime;
  }
}