package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class FileHeader implements Serializable {

  public FileHeader() {
  }

  public FileHeader(Long jobId, String fileName, String filePath, Integer rowCount) {
    this.jobId = jobId;
    this.fileName = fileName;
    this.filePath = filePath;
    this.rowCount = rowCount;
  }

  private Long jobId;

  private String fileName;

  private String filePath;

  private Integer rowCount;

  private Date createdDate;

  private static final long serialVersionUID = 1L;

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public Integer getRowCount() {
    return rowCount;
  }

  public void setRowCount(Integer rowCount) {
    this.rowCount = rowCount;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String toString() {
    return "FileHeader{" +
        "jobId=" + jobId +
        ", fileName='" + fileName + '\'' +
        ", filePath='" + filePath + '\'' +
        '}';
  }
}