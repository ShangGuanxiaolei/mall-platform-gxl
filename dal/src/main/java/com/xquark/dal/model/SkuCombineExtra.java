package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * @author wangxinhua
 */
public class SkuCombineExtra extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  /**
   * 套餐商品的skuId
   */
  private String masterId;

  /**
   * 套餐商品对应的实际skuId
   */
  private String slaveId;

  /**
   * 套装商品id
   */
  private String slaveProductId;

  /**
   * 套装数量
   */
  private Integer amount;

  public String getMasterId() {
    return masterId;
  }

  public void setMasterId(String masterId) {
    this.masterId = masterId;
  }

  public String getSlaveId() {
    return slaveId;
  }

  public void setSlaveId(String slaveId) {
    this.slaveId = slaveId;
  }

  public String getSlaveProductId() {
    return slaveProductId;
  }

  public void setSlaveProductId(String slaveProductId) {
    this.slaveProductId = slaveProductId;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }
}