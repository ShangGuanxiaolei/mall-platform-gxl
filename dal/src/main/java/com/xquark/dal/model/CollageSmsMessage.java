package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.SmsMessageStatus;

import java.sql.Timestamp;


/**
 * 拼团短信实体类
 *
 * @author  tanggb
 * Create in 10:55 2018/9/5
 */
public class CollageSmsMessage extends BaseEntityImpl{
    /**
     * 短信发送状态
     */

    private SmsMessageStatus pushStatus;

    /**

     * 短信内容
     */
    private  String content;

    /**
     * 短信重发次数
     */
    private int recount;

    /**
     * 手机号
     */
    private  String mobile;

    /**
     * 创建时间
     */
    private Timestamp createAt;

    /**
     * 创建时间
     */
    private Timestamp updateAt;

    public SmsMessageStatus getPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(SmsMessageStatus pushStatus) {
        this.pushStatus = pushStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRecount() {
        return recount;
    }

    public void setRecount(int recount) {
        this.recount = recount;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }

    public Timestamp getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Timestamp updateAt) {
        this.updateAt = updateAt;
    }
}
