package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * @author wangxinhua
 */
public class FeedbackTags extends BaseEntityImpl implements Archivable {

  private String id;

  /**
   * 标签id
   */
  private String tagId;

  /**
   * 商品id
   */
  private String feedbackId;

  public FeedbackTags() {
  }

  public FeedbackTags(String tagId, String feedbackId) {
    this.tagId = tagId;
    this.feedbackId = feedbackId;
  }

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTagId() {
    return tagId;
  }

  public void setTagId(String tagId) {
    this.tagId = tagId;
  }

  public String getFeedbackId() {
    return feedbackId;
  }

  public void setFeedbackId(String feedbackId) {
    this.feedbackId = feedbackId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}