package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.PageType;

/**
 * Created by chh on 16-10-17.
 */
public class ViewPage extends BaseEntityImpl implements Archivable {

  private String title;
  private String description;
  private PageType pageType;
  private String layout;
  private String backgroundImg;
  private String customizedUrl;
  private boolean archive;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public PageType getPageType() {
    return pageType;
  }

  public void setPageType(PageType pageType) {
    this.pageType = pageType;
  }

  public String getLayout() {
    return layout;
  }

  public void setLayout(String layout) {
    this.layout = layout;
  }

  public String getBackgroundImg() {
    return backgroundImg;
  }

  public void setBackgroundImg(String backgroundImg) {
    this.backgroundImg = backgroundImg;
  }

  public String getCustomizedUrl() {
    return customizedUrl;
  }

  public void setCustomizedUrl(String customizedUrl) {
    this.customizedUrl = customizedUrl;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
