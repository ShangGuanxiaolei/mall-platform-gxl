package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.CareerLevelType;

/**
 * @author wangxinhua
 */
public class OrderIdentityLog extends BaseEntityImpl {

    public OrderIdentityLog() {
    }

    public OrderIdentityLog(String orderNo, Long cpId, CareerLevelType identity) {
        this.orderNo = orderNo;
        this.cpId = cpId;
        this.identity = identity;
        this.onPayIdentity = null;
    }

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * cpId
     */
    private Long cpId;

    /**
     * 下单时用户身份
     */
    private CareerLevelType identity;

    /**
     * 支付是用户身份
     */
    private CareerLevelType onPayIdentity;

    private static final long serialVersionUID = 1L;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getCpId() {
        return cpId;
    }

    public void setCpId(Long cpId) {
        this.cpId = cpId;
    }

    public CareerLevelType getIdentity() {
        return identity;
    }

    public void setIdentity(CareerLevelType identity) {
        this.identity = identity;
    }

    public CareerLevelType getOnPayIdentity() {
        return onPayIdentity;
    }

    public void setOnPayIdentity(CareerLevelType onPayIdentity) {
        this.onPayIdentity = onPayIdentity;
    }
}