package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.type.IndexBannerDropStatusType;
import com.xquark.dal.type.IndexBannerJumpType;

import java.util.Date;

/**
 * @ClassName: IndexBanner
 * @Description: 首页优化 Banner
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 11:24
 * @Version: 1.0
 **/
public class IndexBanner extends BaseEntityImpl {

    private static final long serialVersionUID = 3454214366601612600L;

    private static final String PRODUCT_URI="hanwei://product/";
    private static final String CATEGORY_URI="hanwei://category/";

    private String title; //标题

    private String imgUrl; //图片路径

    private Integer sequence; //顺序

    private IndexBannerJumpType jumpType;

    private String jumpTypeLink;

    private Boolean status; //banner 是否开启

    private Integer dropStatus; //投放状态(1:所有状态  2:进行中  3:"即将开始  4:已结束)

    private IndexBannerDropStatusType indexBannerDropStatusType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startAt; //开始时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endAt; //结束时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt; //发布时间

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt; //更新时间

    private String description; //banner描述

    private Boolean archive; // 0:未删, 1:已删

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public IndexBannerJumpType getJumpType() {
        return jumpType;
    }

    public void setJumpType(IndexBannerJumpType jumpType) {
        this.jumpType = jumpType;
    }

    public String getJumpTypeLink() {
        switch (this.getJumpType()) {
            case PRODUCT:
                if (!jumpTypeLink.contains(PRODUCT_URI))
                    jumpTypeLink = PRODUCT_URI + jumpTypeLink;
                break;
            case SECOND_CATEGORY:
                if (!jumpTypeLink.contains(CATEGORY_URI))
                    jumpTypeLink = CATEGORY_URI + jumpTypeLink;
                break;
            default:
                break;
        }
        return jumpTypeLink;
    }

    public void setJumpTypeLink(String jumpTypeLink) {
        this.jumpTypeLink = jumpTypeLink;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Integer getDropStatus() {
        switch (dropStatus) {
            case 2:
                setIndexBannerDropStatusType(IndexBannerDropStatusType.ONGOING);
                break;
            case 3:
                setIndexBannerDropStatusType(IndexBannerDropStatusType.ABOUT_TO_BEGIN);
                break;
            case 4:
                setIndexBannerDropStatusType(IndexBannerDropStatusType.ENDED);
                break;
            default:
                break;
        }
        return dropStatus;
    }

    public void setDropStatus(Integer dropStatus) {
        this.dropStatus = dropStatus;
    }

    public String getIndexBannerDropStatusType() {
        return indexBannerDropStatusType.getDropDesc();
    }

    public void setIndexBannerDropStatusType(IndexBannerDropStatusType indexBannerDropStatusType) {
        this.indexBannerDropStatusType = indexBannerDropStatusType;
    }
}
