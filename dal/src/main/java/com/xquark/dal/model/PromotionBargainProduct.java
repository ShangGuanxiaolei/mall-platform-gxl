package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

/**
 * @author wangxinhua
 */
public class PromotionBargainProduct extends BaseEntityArchivableImpl {

    /**
     * 活动Id
     */
    private String id;

    /**
     * 商品id
     */
    private String productId;

    /**
     * 关联的skuId
     */
    private String skuId;

    /**
     * 活动id
     */
    private String bargainId;

    /**
     * 砍价金额起始数 - 默认为商品原价
     */
    private BigDecimal priceStart;

    /**
     * 砍价金额结束数 - 默认为商品兑换价
     */
    private BigDecimal priceEnd;

    /**
     * 活动库存
     */
    private Integer stock;

    private Date createdAt;

    private Date updatedAt;

    private Boolean archive;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getBargainId() {
        return bargainId;
    }

    public void setBargainId(String bargainId) {
        this.bargainId = bargainId;
    }

    public BigDecimal getPriceStart() {
        return priceStart;
    }

    public void setPriceStart(BigDecimal priceStart) {
        this.priceStart = priceStart;
    }

    public BigDecimal getPriceEnd() {
        return priceEnd;
    }

    public void setPriceEnd(BigDecimal priceEnd) {
        this.priceEnd = priceEnd;
    }

    public Integer getStock() {
        return Optional.ofNullable(stock).orElse(0);
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Boolean getArchive() {
        return archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
