package com.xquark.dal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xquark.dal.BaseEntityImpl;

import java.util.Date;

public class MessageNotify extends BaseEntityImpl {
    private String message;
    private String cause;
    private String influencesMessage;
    private Date triggerStartTime;
    private Date triggerEndTime;
    @JsonIgnore
    private long idRaw = -1;
    @JsonIgnore
    private String id;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getInfluencesMessage() {
        return influencesMessage;
    }

    public void setInfluencesMessage(String influencesMessage) {
        this.influencesMessage = influencesMessage;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
