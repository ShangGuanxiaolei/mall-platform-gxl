package com.xquark.dal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * customerProfile
 *
 * @author wangxinhua
 */
public class CustomerProfileAudit implements Serializable {

  /**
   * 主键
   */
  private Long cpId;

  /**
   * 资格证号
   */
  private String customerNumber;

  private String customerIdHds;

  private String customerIdVivi;

  /**
   * 姓名
   */
  private String nameCN;

  /**
   * 英文名
   */
  private String nameEN;

  /**
   * 性别(1=>男，2=>女)
   */
  private Byte gender;

  /**
   * 国籍
   */
  private String nationality;

  /**
   * 证件号
   */
  private String tinCode;

  /**
   * 证件类型
   */
  private String tinCodeType;

  /**
   * 生日
   */
  private Date birthday;

  /**
   * 手机号1
   */
  private String phoneNo1;

  /**
   * 手机号2
   */
  private String phoneNo2;

  /**
   * 手机号3
   */
  private String phoneNo3;

  private String type;

  private String subType;

  private String spType;

  private String star;

  /**
   * 民族
   */
  private Long ethnicityId;

  private String upline;

  private Long uplineCpId;

  /**
   * 状态(0=>禁用，1=>正常)
   */
  private String status;

  /**
   * 配偶姓名
   */
  private String spouseName;

  /**
   * 配偶身份证号
   */
  private String spouseTinCode;

  /**
   * 配偶证件类型
   */
  private String spouseTinCodeType;

  /**
   * 配偶手机号1
   */
  private String spousePhoneNo1;

  /**
   * 配偶手机号2
   */
  private String spousePhoneNo2;

  /**
   * 配偶生日
   */
  private Date spouseBirthday;

  /**
   * 配偶民族
   */
  private Long spouseEthniCityId;

  private Date dsAppDate;

  private Date spQualDate;

  private Date spRequalDate;

  private Date pcAppDate;

  /**
   * 续约时间
   */
  private Date renewDate;

  /**
   * 创建人
   */
  private String createdBy;

  /**
   * 创建时间
   */
  private Date createdAt;

  /**
   * 更新人
   */
  private String updatedBy;

  /**
   * 更新时间
   */
  private Date updatedAt;

  /**
   * 逻辑删除标记，0未删，1已删。
   */
  private Byte isDeleted;

  /**
   * 是否在册，1在册，0退出。
   */
  private Byte isActive;

  /**
   * 外键ID，店铺表
   */
  private String storeId;

  /**
   * 区域
   */
  private Long regionId;

  /**
   * 退出时间
   */
  private Date exitDate;

  /**
   * 退出原因
   */
  private String exitReason;

  /**
   * 直销员号
   */
  private String directSaleCode;

  private Integer spRenewMonth;

  private Date spRenewDate;

  /**
   * 外键ID,营业执照
   */
  private Long licenseId;

  /**
   * 职业
   */
  private Long occupationId;

  /**
   * 学历
   */
  private Long educationId;

  /**
   * 加入方式
   */
  private String platform;

  /**
   * 是否员工标识，0非，1是，员工号及下级不参与motion计算
   */
  private Byte isStaff;

  /**
   * 是否comgpany类型账号，0非，1是，company类账号默认按职级上限计算
   */
  private Byte isCompany;

  /**
   * 是否同步到积分表(0=>否，1是)
   */
  private Byte isToPoints;

  /**
   * 是否直销员，0：不是，1：是
   */
  private Byte isDirect;

  /**
   * 上线店主资格证号
   */
  private String fromCustomerId;

  /**
   * 提现方式，alipay=>支付宝, bank=>银行卡
   */
  private String withdrawType;

  /**
   * 是否同意服务协议；0=>没有同意, 1=>已同意
   */
  private Byte protocol;

  /**
   * 0 hds, 1 vivilife, 3 ecommerce
   */
  private Byte source;

  /**
   * 1 insert, 2 update, 3 delete, 5 update under insert
   */
  private Byte auditType;
  /**
   * H -> HDS, V -> vivilife, E -> ecommerce, S -> system
   */
  private String auditUser;

  private Date auditDate;

  private static final long serialVersionUID = 1L;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getCustomerIdHds() {
    return customerIdHds;
  }

  public void setCustomerIdHds(String customerIdHds) {
    this.customerIdHds = customerIdHds;
  }

  public String getCustomerIdVivi() {
    return customerIdVivi;
  }

  public void setCustomerIdVivi(String customerIdVivi) {
    this.customerIdVivi = customerIdVivi;
  }

  public String getNameCN() {
    return nameCN;
  }

  public void setNameCN(String nameCN) {
    this.nameCN = nameCN;
  }

  public String getNameEN() {
    return nameEN;
  }

  public void setNameEN(String nameEN) {
    this.nameEN = nameEN;
  }

  public Byte getGender() {
    return gender;
  }

  public void setGender(Byte gender) {
    this.gender = gender;
  }

  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getTinCode() {
    return tinCode;
  }

  public void setTinCode(String tinCode) {
    this.tinCode = tinCode;
  }

  public String getTinCodeType() {
    return tinCodeType;
  }

  public void setTinCodeType(String tinCodeType) {
    this.tinCodeType = tinCodeType;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getPhoneNo1() {
    return phoneNo1;
  }

  public void setPhoneNo1(String phoneNo1) {
    this.phoneNo1 = phoneNo1;
  }

  public String getPhoneNo2() {
    return phoneNo2;
  }

  public void setPhoneNo2(String phoneNo2) {
    this.phoneNo2 = phoneNo2;
  }

  public String getPhoneNo3() {
    return phoneNo3;
  }

  public void setPhoneNo3(String phoneNo3) {
    this.phoneNo3 = phoneNo3;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSubType() {
    return subType;
  }

  public void setSubType(String subType) {
    this.subType = subType;
  }

  public String getSpType() {
    return spType;
  }

  public void setSpType(String spType) {
    this.spType = spType;
  }

  public String getStar() {
    return star;
  }

  public void setStar(String star) {
    this.star = star;
  }

  public Long getEthnicityId() {
    return ethnicityId;
  }

  public void setEthnicityId(Long ethnicityId) {
    this.ethnicityId = ethnicityId;
  }

  public String getUpline() {
    return upline;
  }

  public void setUpline(String upline) {
    this.upline = upline;
  }

  public Long getUplineCpId() {
    return uplineCpId;
  }

  public void setUplineCpId(Long uplineCpId) {
    this.uplineCpId = uplineCpId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getSpouseName() {
    return spouseName;
  }

  public void setSpouseName(String spouseName) {
    this.spouseName = spouseName;
  }

  public String getSpouseTinCode() {
    return spouseTinCode;
  }

  public void setSpouseTinCode(String spouseTinCode) {
    this.spouseTinCode = spouseTinCode;
  }

  public String getSpouseTinCodeType() {
    return spouseTinCodeType;
  }

  public void setSpouseTinCodeType(String spouseTinCodeType) {
    this.spouseTinCodeType = spouseTinCodeType;
  }

  public String getSpousePhoneNo1() {
    return spousePhoneNo1;
  }

  public void setSpousePhoneNo1(String spousePhoneNo1) {
    this.spousePhoneNo1 = spousePhoneNo1;
  }

  public String getSpousePhoneNo2() {
    return spousePhoneNo2;
  }

  public void setSpousePhoneNo2(String spousePhoneNo2) {
    this.spousePhoneNo2 = spousePhoneNo2;
  }

  public Date getSpouseBirthday() {
    return spouseBirthday;
  }

  public void setSpouseBirthday(Date spouseBirthday) {
    this.spouseBirthday = spouseBirthday;
  }

  public Long getSpouseEthniCityId() {
    return spouseEthniCityId;
  }

  public void setSpouseEthniCityId(Long spouseEthniCityId) {
    this.spouseEthniCityId = spouseEthniCityId;
  }

  public Date getDsAppDate() {
    return dsAppDate;
  }

  public void setDsAppDate(Date dsAppDate) {
    this.dsAppDate = dsAppDate;
  }

  public Date getSpQualDate() {
    return spQualDate;
  }

  public void setSpQualDate(Date spQualDate) {
    this.spQualDate = spQualDate;
  }

  public Date getSpRequalDate() {
    return spRequalDate;
  }

  public void setSpRequalDate(Date spRequalDate) {
    this.spRequalDate = spRequalDate;
  }

  public Date getPcAppDate() {
    return pcAppDate;
  }

  public void setPcAppDate(Date pcAppDate) {
    this.pcAppDate = pcAppDate;
  }

  public Date getRenewDate() {
    return renewDate;
  }

  public void setRenewDate(Date renewDate) {
    this.renewDate = renewDate;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Byte getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Byte isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Byte getIsActive() {
    return isActive;
  }

  public void setIsActive(Byte isActive) {
    this.isActive = isActive;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public Long getRegionId() {
    return regionId;
  }

  public void setRegionId(Long regionId) {
    this.regionId = regionId;
  }

  public Date getExitDate() {
    return exitDate;
  }

  public void setExitDate(Date exitDate) {
    this.exitDate = exitDate;
  }

  public String getExitReason() {
    return exitReason;
  }

  public void setExitReason(String exitReason) {
    this.exitReason = exitReason;
  }

  public String getDirectSaleCode() {
    return directSaleCode;
  }

  public void setDirectSaleCode(String directSaleCode) {
    this.directSaleCode = directSaleCode;
  }

  public Integer getSpRenewMonth() {
    return spRenewMonth;
  }

  public void setSpRenewMonth(Integer spRenewMonth) {
    this.spRenewMonth = spRenewMonth;
  }

  public Date getSpRenewDate() {
    return spRenewDate;
  }

  public void setSpRenewDate(Date spRenewDate) {
    this.spRenewDate = spRenewDate;
  }

  public Long getLicenseId() {
    return licenseId;
  }

  public void setLicenseId(Long licenseId) {
    this.licenseId = licenseId;
  }

  public Long getOccupationId() {
    return occupationId;
  }

  public void setOccupationId(Long occupationId) {
    this.occupationId = occupationId;
  }

  public Long getEducationId() {
    return educationId;
  }

  public void setEducationId(Long educationId) {
    this.educationId = educationId;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public Byte getIsStaff() {
    return isStaff;
  }

  public void setIsStaff(Byte isStaff) {
    this.isStaff = isStaff;
  }

  public Byte getIsCompany() {
    return isCompany;
  }

  public void setIsCompany(Byte isCompany) {
    this.isCompany = isCompany;
  }

  public Byte getIsToPoints() {
    return isToPoints;
  }

  public void setIsToPoints(Byte isToPoints) {
    this.isToPoints = isToPoints;
  }

  public Byte getIsDirect() {
    return isDirect;
  }

  public void setIsDirect(Byte isDirect) {
    this.isDirect = isDirect;
  }

  public String getFromCustomerId() {
    return fromCustomerId;
  }

  public void setFromCustomerId(String fromCustomerId) {
    this.fromCustomerId = fromCustomerId;
  }

  public String getWithdrawType() {
    return withdrawType;
  }

  public void setWithdrawType(String withdrawType) {
    this.withdrawType = withdrawType;
  }

  public Byte getProtocol() {
    return protocol;
  }

  public void setProtocol(Byte protocol) {
    this.protocol = protocol;
  }

  public Byte getSource() {
    return source;
  }

  public void setSource(Byte source) {
    this.source = source;
  }

  public Byte getAuditType() {
    return auditType;
  }

  public void setAuditType(Byte auditType) {
    this.auditType = auditType;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }
}