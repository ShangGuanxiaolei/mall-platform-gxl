package com.xquark.dal.model;

import com.xquark.dal.BaseEntityImpl;

/**
 * @author  hs
 */
public class PromotionWhite extends BaseEntityImpl {

    private static final long serialVersionUID = 1L;
    /**
     * 活动编码 salezone-专区商品
     */
    private String pCode;

    /**
     * CPID
     */
    private Long cpid;

    private Byte deleted;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public Long getCpid() {
        return cpid;
    }

    public void setCpid(Long cpid) {
        this.cpid = cpid;
    }

    public Byte getDeleted() {
        return deleted;
    }

    public void setDeleted(Byte deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PromotionWhite{" +
                "pCode='" + pCode + '\'' +
                ", cpid=" + cpid +
                ", deleted=" + deleted +
                '}';
    }
}