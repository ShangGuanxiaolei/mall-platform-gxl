package com.xquark.dal.model.mypiece;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xquark.dal.status.PieceStatus;
import org.springframework.data.annotation.Id;

import javax.annotation.Generated;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/12 11:02
 */
public class PGMemberInfoVo {
    private String pieceGroupDetailCode;

    private String pieceGroupTranCode;
    private String memberId;
    private String wxNickname;
    private String wxHeaderUrl;
    private String wxOpenId;
    private Integer isGroupHead;
    private Date joinTime;
    private String shareUrl;
    private Integer status;
    private Date createAt;
    private Date updateAt;
    private Integer isDeleted;
    private Integer isNew;

    public String getPieceGroupDetailCode() {
        return pieceGroupDetailCode;
    }

    public void setPieceGroupDetailCode(String pieceGroupDetailCode) {
        this.pieceGroupDetailCode = pieceGroupDetailCode;
    }

    public String getPieceGroupTranCode() {
        return pieceGroupTranCode;
    }

    public void setPieceGroupTranCode(String pieceGroupTranCode) {
        this.pieceGroupTranCode = pieceGroupTranCode;
    }

    public String getMemberId() {
        return memberId;
    }

    @JsonProperty("memberId")
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @JsonIgnore
    public void setMemberId(Long memberId) {
      setMemberId(memberId == null ? null : String.valueOf(memberId));
    }

    public String getWxNickname() {
        return wxNickname;
    }

    public void setWxNickname(String wxNickname) {
        this.wxNickname = wxNickname;
    }

    public String getWxHeaderUrl() {
        return wxHeaderUrl;
    }

    public void setWxHeaderUrl(String wxHeaderUrl) {
        this.wxHeaderUrl = wxHeaderUrl;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public Integer getIsGroupHead() {
        return isGroupHead;
    }

    public void setIsGroupHead(Integer isGroupHead) {
        this.isGroupHead = isGroupHead;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonIgnore
    public void setStatus(PieceStatus status) {
      setStatus(status.getCode());
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }
}
