package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import java.util.Date;

/**
 * User: kong Date: 18-6-8. Time: 下午4:58 wms快递信息表
 */
public class WmsExpress extends BaseEntityImpl implements Archivable {

  //订单号
  private String orderNo;
  //订单更新时间
  private Date time;
  //快递公司
  private String expressCompanyName;
  //快递单号
  private String expressNo;
  //路由
  private String route;
  //edi状态
  private Integer ediStatus;
  //逻辑删除
  private Boolean archive;

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public String getExpressCompanyName() {
    return expressCompanyName;
  }

  public void setExpressCompanyName(String expressCompanyName) {
    this.expressCompanyName = expressCompanyName;
  }

  public String getExpressNo() {
    return expressNo;
  }

  public void setExpressNo(String expressNo) {
    this.expressNo = expressNo;
  }

  public String getRoute() {
    return route;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public Integer getEdiStatus() {
    return ediStatus;
  }

  public void setEdiStatus(Integer ediStatus) {
    this.ediStatus = ediStatus;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String toString() {
    return "wmsExpress{" +
        "orderNo='" + orderNo + '\'' +
        ", time=" + time +
        ", expressCompanyName='" + expressCompanyName + '\'' +
        ", expressNo='" + expressNo + '\'' +
        ", route='" + route + '\'' +
        ", ediStatus=" + ediStatus +
        ", archive=" + archive +
        '}';
  }

  public WmsExpress(String orderNo, Date time, String expressCompanyName, String expressNo,
      String route, Integer ediStatus) {
    this.orderNo = orderNo;
    this.time = time;
    this.expressCompanyName = expressCompanyName;
    this.expressNo = expressNo;
    this.route = route;
    this.ediStatus = ediStatus;
  }

  public WmsExpress() {

  }
}
