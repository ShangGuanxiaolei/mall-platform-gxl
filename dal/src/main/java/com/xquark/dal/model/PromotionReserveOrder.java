package com.xquark.dal.model;

import com.xquark.dal.BaseEntityArchivableImpl;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderSortType;

import java.math.BigDecimal;

/**
 * 预售订单记录
 *
 * @author: yyc
 * @date: 19-4-21 下午5:20
 */
public class PromotionReserveOrder extends BaseEntityArchivableImpl {

  /** 用户id */
  private String userId;

  /** 活动id */
  private String promotionId;

  /** 预约单号 */
  private String orderNo;

  /** 订单状态 */
  private OrderStatus orderStatus;

  /** 订单实际总金额 */
  private BigDecimal totalFee;

  /** 原商品累计总金额 */
  private BigDecimal goodsFee;

  /** 订单类型 */
  private OrderSortType orderType;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public OrderSortType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderSortType orderType) {
    this.orderType = orderType;
  }
}
