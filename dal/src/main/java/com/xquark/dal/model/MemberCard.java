package com.xquark.dal.model;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-6-26. DESC: 会员卡model类
 */
public class MemberCard extends BaseEntityImpl implements Archivable {

  public MemberCard() {
  }

  protected String id;

  protected String name;

  protected String bgColor;

  protected String background;

  protected Integer level;

  protected BigDecimal discount;

  protected Boolean freeDelivery;

  protected String upgradeType;

  protected Long upgradeDealNo;

  protected BigDecimal upgradeConsumeNo;

  protected Long upgradePoint;

  protected Date createdAt;

  protected Date updatedAt;

  protected Boolean archive;

  protected String remark;

  @Override
  public Boolean getArchive() {
    return this.archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  @Override
  public String getId() {
    return super.getId();
  }

  @Override
  public void setId(String id) {
    super.setId(id);
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    super.setCreatedAt(createdAt);
  }

  @Override
  public Date getCreatedAt() {
    return super.getCreatedAt();
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    super.setUpdatedAt(updatedAt);
  }

  @Override
  public Date getUpdatedAt() {
    return super.getUpdatedAt();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBgColor() {
    return bgColor;
  }

  public void setBgColor(String bgColor) {
    this.bgColor = bgColor;
  }

  public String getBackground() {
    return background;
  }

  public void setBackground(String background) {
    this.background = background;
  }

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Boolean getFreeDelivery() {
    return freeDelivery;
  }

  public void setFreeDelivery(Boolean freeDelivery) {
    this.freeDelivery = freeDelivery;
  }

  public String getUpgradeType() {
    return upgradeType;
  }

  public void setUpgradeType(String upgradeType) {
    this.upgradeType = upgradeType;
  }

  public Long getUpgradeDealNo() {
    return upgradeDealNo;
  }

  public void setUpgradeDealNo(Long upgradeDealNo) {
    this.upgradeDealNo = upgradeDealNo;
  }

  public BigDecimal getUpgradeConsumeNo() {
    return upgradeConsumeNo;
  }

  public void setUpgradeConsumeNo(BigDecimal upgradeConsumeNo) {
    this.upgradeConsumeNo = upgradeConsumeNo;
  }

  public Long getUpgradePoint() {
    return upgradePoint;
  }

  public void setUpgradePoint(Long upgradePoint) {
    this.upgradePoint = upgradePoint;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }
}
