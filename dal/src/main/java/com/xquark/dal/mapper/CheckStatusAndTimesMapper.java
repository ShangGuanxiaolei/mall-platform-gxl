package com.xquark.dal.mapper;


import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.StatusAndTimesVO;
import org.springframework.stereotype.Component;

@Component
public interface CheckStatusAndTimesMapper {

  StatusAndTimesVO selectBypCode(PromotionBaseInfo promotionBaseInfo);

  StatusAndTimesVO selectByTranCode(PromotionBaseInfo promotionBaseInfo);


}
