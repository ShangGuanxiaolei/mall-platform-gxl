package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManConfigVo;

import java.util.Map;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:11:40
 * Des:新人页面配置
 */
public interface FreshManConfigMapper {
    FreshManConfigVo selectByName(String name);

    Map selectPointConfig(String grade);
}