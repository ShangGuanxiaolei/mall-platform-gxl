package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductBlackList;
import com.xquark.dal.vo.ProductBlackListVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ProductBlackListMapper {

  int deleteByPrimaryKey(String id);

  int insert(ProductBlackList record);

  int insertSelective(ProductBlackList record);

  ProductBlackListVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ProductBlackList record);

  int updateByPrimaryKey(ProductBlackList record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<ProductBlackListVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 删除黑名单商品
   */
  int deleteByProductId(@Param(value = "userId") String userId,
      @Param(value = "productId") String productId);


}