package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSaleShare;

public interface GrandSaleShareMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GrandSaleShare record);

    int insertSelective(GrandSaleShare record);

    GrandSaleShare selectByPrimaryKey(Long id);

    GrandSaleShare selectByType(String type);

    int updateByPrimaryKeySelective(GrandSaleShare record);

    int updateByPrimaryKey(GrandSaleShare record);
}