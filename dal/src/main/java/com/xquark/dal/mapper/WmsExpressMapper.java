package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsExpress;
import com.xquark.dal.vo.SFOrderLogDal;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-8. Time: 下午5:11 wms快递表单
 */
public interface WmsExpressMapper {

  /**
   * 添加
   */
  int insert(WmsExpress obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsExpress> collection);

  /**
   * 通过id查找
   */
  WmsExpress getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsExpress obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsExpress> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

  /**
   * 根据edi状态和发货状态查询
   * @param ediStatus
   * @return
   */
  List<WmsExpress> getByEdiStatusAndStatus(@Param("ediStatus") Integer ediStatus,
      @Param("status") Integer status);

  List<SFOrderLogDal> getRouteByOrderNo(String orderNo);
}