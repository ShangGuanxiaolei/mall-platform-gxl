package com.xquark.dal.mapper;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullPieces;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionFullPiecesMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionFullPieces record);

  int insertSelective(PromotionFullPieces record);

  PromotionFullPieces selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionFullPieces record);

  int updateByPrimaryKey(PromotionFullPieces record);

  List<Promotion> listPromotion(@Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param(value = "page") Pageable page,
      @Param("keyword") String keyword);

  List<PromotionFullPiecesVO> listPromotionVO(@Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param(value = "page") Pageable page,
      @Param("keyword") String keyword);

  List<Product> listProductByPromotionId(@Param("id") String id, @Param("gift") Boolean gift,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  Long countProductByPromotionId(@Param("id") String id, @Param("gift") Boolean gift);

  /**
   * 查询全店有效商品的活动及非全店有效中关联该商品的活动
   *
   * @param productId 商品id
   * @param order 排序
   * @param direction 方向
   * @param page page
   */
  List<PromotionFullPiecesVO> listPromotionVOByProductId(@Param("productId") String productId,
      @Param("isShare") Boolean isShare,
      @Param("order") String order, @Param("direction") Sort.Direction direction,
      @Param(value = "page") Pageable page);

  PromotionFullPiecesVO selectById(String id);

  Long countPromotion(@Param("keyword") String keyword);

  int close(String promotionId);

  int deleteProduct(@Param("promotionId") String promotionId, @Param("productId") String productId);

  int emptyProduct(@Param("promotionId") String promotionId, @Param("gift") Boolean gift);

  int hasAdded(@Param("promotionId") String promotionId, @Param("productId") String productId);
}