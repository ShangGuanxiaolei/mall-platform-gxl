package com.xquark.dal.mapper;

import com.xquark.dal.model.IndexBanner;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: IndexBannerMapper
 * @Description: 首页优化 banner dao
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 13:57
 * @Version: 1.0
 **/
@Repository
public interface IndexBannerMapper {

    int insertIndexBanner(IndexBanner indexBanner);

    // 查询表中排在最后的顺序编号
    Integer selectLastSequence();

    List<IndexBanner> searchIndexBanner(@Param("params") Map<String, Object> map, @Param("page") Pageable pageable);

    long countIndexBannerBySearch(@Param("params") Map<String, Object> map);

    boolean selectExists(String id);

    int updateByPrimaryKey(IndexBanner indexBanner);

    IndexBanner selectByPrimaryKey(String id);

    List<IndexBanner> selectIndexBanners(@Param("page") Pageable pageable);

    int updateSequenceByPrimaryKey(IndexBanner indexBanner);

    List<IndexBanner> selectAppIndexBanners();

    int deletedByPrimaryKey(String id);

    long countIndexBannerList();
}
