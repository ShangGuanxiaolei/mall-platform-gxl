package com.xquark.dal.mapper;

import com.xquark.dal.model.SupplierWareHouse;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午4:50 供应商关联的仓库
 */
public interface SupplierWarehouseMapper {

  /**
   * 添加
   */
  int insert(SupplierWareHouse obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<SupplierWareHouse> collection);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 通过供应商的id删除
   */
  int deleteBySupplierId(@Param("supplierId") String supplierId);


}