package com.xquark.dal.mapper;

import com.xquark.dal.model.Feedback;
import com.xquark.dal.status.FeedbackStatus;
import com.xquark.dal.vo.FeedbackVO;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface FeedbackMapper {

  Feedback selectByPrimaryKey(String id);

  int insert(Feedback record);

  int updateForClosed(String id, String replay, @Param("status") FeedbackStatus status);

  List<FeedbackVO> listFeedbacksByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Long countFeedbacksByAdmin(@Param(value = "params") Map<String, Object> params);

  int delete(@Param(value = "id") String id);

  Feedback selectByContact(@Param(value = "contact") String contact);

}