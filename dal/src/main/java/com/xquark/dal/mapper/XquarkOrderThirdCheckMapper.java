package com.xquark.dal.mapper;

import com.xquark.dal.model.XquarkOrderThirdCheck;

public interface XquarkOrderThirdCheckMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkOrderThirdCheck record);

    int insertSelective(XquarkOrderThirdCheck record);

    XquarkOrderThirdCheck selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkOrderThirdCheck record);

    int updateByPrimaryKey(XquarkOrderThirdCheck record);
}