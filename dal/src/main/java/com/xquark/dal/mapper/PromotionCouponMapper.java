package com.xquark.dal.mapper;

import com.xquark.dal.model.promotion.Coupon;
import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.PromotionCouponVo;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;


public interface PromotionCouponMapper {

  PromotionCouponVo selectByPrimaryKey(@Param("id") String id);

  int insert(@Param("params") Map<String, Object> params);

  int updateStatusById(@Param("status") String status, @Param("id") String id);

  int updateRemainderById(@Param("useNumber") int useNumber, @Param("id") String id);

  List<PromotionCouponVo> listPromotionCoupon(@Param("params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  List<BasePromotionCouponVO> listBasePromotionCouponVO(@Param("params") Map<String, ?> params,
      @Param("order") String order,
      @Param("direction") Direction direction,
      @Param("page") Pageable pageable);

  int modifyPromotionCoupon(@Param("params") Map<String, Object> params, @Param("id") String id);

  Long countPromotionCoupon(@Param("params") Map<String, Object> params);

  List<PromotionCouponVo> loadShopPromotionCoupon(@Param("params") Map<String, Object> params);

  Long selectRemainder(@Param("promotionCouponId") String promotionCouponId);

  List<Coupon> selectShouldCloseCoupon();

  int closeCoupon(@Param("id") String id);

  int updateUsedCountById(@Param("id") String id, @Param("number") int number);

  Long countCouponByShopId(@Param("params") Map<String, Object> params);

  List<PromotionCouponVo> listCouponByShopId(@Param("params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  List<PromotionCouponVo> listForApp(@Param("page") Pageable pageable,
      @Param("shopId") String shopId);

  /**
   * 优惠券作废后更新使用该优惠券的未支付订单
   */
  int updateOrderDiscount(@Param("couponId") String couponId,
      @Param("discount") BigDecimal discount);

  /**
   * 优惠券作废后更新使用该优惠券的未支付主订单
   */
  int updateMainOrderDiscount(@Param("couponId") String couponId,
      @Param("discount") BigDecimal discount);

  /**
   * 找到该店铺内能使用到的优惠券
   */
  List<Coupon> selectShopCoupon(@Param("shopId") String shopId);

}
