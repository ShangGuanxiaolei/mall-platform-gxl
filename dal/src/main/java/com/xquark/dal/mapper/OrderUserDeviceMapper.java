package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderUserDevice;

public interface OrderUserDeviceMapper {

  int insert(OrderUserDevice orderUserDevice);

}
