package com.xquark.dal.mapper;

import com.xquark.dal.model.UserMessage;
import com.xquark.dal.status.UserMessageStatus;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserMessageMapper {

  void insert(UserMessage um);

  int insertList(@Param("list") List<UserMessage> list);

  UserMessage selectByPrimaryKey(String id);

  void updateUserMessageStatus(@Param("id") String id, @Param("status") UserMessageStatus status);

  List<UserMessage> selectByReceiver(String userId);

  List<UserMessage> selectBySender(String userId);

  List<UserMessage> selectByUser(String userId);

  List<UserMessage> selectNotificationByUser(String userId);

  long countUnreadByUser(@Param("userId") String userId);

  void updateUserAllRead(@Param("userId") String userId);

  void deleteBargainMessage(@Param("fromUserId") String fromUserId,
      @Param("toUserId") String toUserId, @Param("msgId") String msgId);

  long countMessageByUser(@Param("userId") String userId, @Param("msgId") String msgId,
      @Param("content") String content);

}
