package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionListMapper {

    int deleteByPrimaryKey(String id);

    int insert(PromotionList record);

    PromotionList selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionList record);

    int updateByPrimaryKey(PromotionList record);

    List<PromotionList> getTodayPromotion(Integer type);

    boolean havePromotionToday(@Param("type") Integer type);
}