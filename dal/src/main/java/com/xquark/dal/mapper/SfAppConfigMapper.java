package com.xquark.dal.mapper;

import com.xquark.dal.model.SfAppConfig;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SfAppConfigMapper {

  int deleteByPrimaryKey(String id);

  int insert(SfAppConfig record);

  int insertSelective(SfAppConfig record);

  SfAppConfig selectByPrimaryKey(String id);

  SfAppConfig selectByProfile(String profile);

  List<SfAppConfig> listAppConfig();

  List<SfAppConfig> listExpireAtConfig(
      @Param("profile") String profile,
      @Param("renewInterval") Integer renewInterval);

  int updateByPrimaryKeySelective(SfAppConfig record);

  int updateByPrimaryKey(SfAppConfig record);

  /**
   * 验证开发者账号
   * @param clientId
   * @return
   */
  int checkDevAccount(String clientId);

}