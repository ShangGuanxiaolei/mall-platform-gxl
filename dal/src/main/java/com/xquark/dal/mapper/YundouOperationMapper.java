package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.model.YundouOperationType;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface YundouOperationMapper {

  Integer insert(YundouOperation operation);

  Integer deleteById(@Param("id") String id);

  Integer deleteByCode(@Param("code") Integer code);

  Integer updateById(YundouOperation operation);

  YundouOperation selectById(@Param("id") String id);

  YundouOperation selectByName(@Param("name") String name);

  YundouOperationType selectType(@Param("operationId") String operationId);

  YundouOperation selectByCode(@Param("code") Integer code);

  List<YundouOperationType> selectTypes();

  List<YundouOperation> selectAll(@Param("order") String order,
      @Param("page") Pageable pageable,
      @Param("direction") Sort.Direction direction);

  Integer count();

  Integer countTypes();
}
