package com.xquark.dal.mapper;

import com.xquark.dal.model.TermRelationship;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TermRelationshipMapper {

  int insert(TermRelationship record);

  int delete(String id);

  TermRelationship select(String objType, String objId, String cid);

  List<TermRelationship> listInCategory(@Param("cid") String cid, @Param("objType") String objType,
      @Param("pager") Pageable pager);

  List<TermRelationship> listUnderCategory(@Param("cid") String cid, @Param("shopId") String shopId,
      @Param("objType") String objType, @Param("pager") Pageable pager);

  List<String> listCategoryId(@Param("objType") String objType, @Param("grandSaleId") Integer grandSaleId);

  long countUnderCategory(@Param("cid") String cid, @Param("objType") String objType);

  void deleteByCatAndObject(@Param("objType") String type, @Param("objId") String posterId,
      @Param("cid") String cid);

  long countProducts(@Param("id") String id, @Param("userId") String userId);

  List<TermRelationship> checkProductIdExists(@Param("objType") String objType,
      @Param("objId") String objId);

  /**
   * 得到某个商品的所有分类
   */
  List<String> getCategoryByProduct(@Param("productId") String productId);

  void updateByCode(TermRelationship termRelationship);

  List<String> getProductCode();

  void insertExcel(TermRelationship termRelationship);

  List<TermRelationship> selectByObjType(String type);
}
