package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopAccessLog;
import com.xquark.dal.model.TwitterAccessLog;
import org.apache.ibatis.annotations.Param;

public interface TwitterAccessLogMapper {

  int deleteByPrimaryKey(String id);

  int insert(TwitterAccessLog record);

  TwitterAccessLog selectByPrimaryKey(String id);

  long countByUserId(@Param("userId") String userId);

}