package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopWechatSetting;

public interface ShopWechatSettingMapper {

  int deleteByPrimaryKey(String id);

  int unDeleteByPrimaryKey(String id);

  int insert(ShopWechatSetting record);

  int insertSelective(ShopWechatSetting record);

  ShopWechatSetting selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ShopWechatSetting record);

  int updateByPrimaryKey(ShopWechatSetting record);

  ShopWechatSetting selectByUserId(String userId);
}