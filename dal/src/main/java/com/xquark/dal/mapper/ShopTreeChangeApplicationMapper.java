package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopTreeChangeApplication;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.UserTwitterApplyVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface ShopTreeChangeApplicationMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(ShopTreeChangeApplication record);

  ShopTreeChangeApplication selectByPrimaryKey(String id);

  int updateByPrimaryKey(ShopTreeChangeApplication record);

  List<ShopTreeChangeApplication> selectByOwnShopIdApplying(@Param("shopId") String shopId);
}