package com.xquark.dal.mapper;

import com.xquark.dal.model.ConsumerToConsumer;

public interface ConsumerToConsumerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ConsumerToConsumer record);

    int insertSelective(ConsumerToConsumer record);

    ConsumerToConsumer selectByPrimaryKey(Long id);

    Long selectUplineConsumerCpId(Long cpId);

    int updateByPrimaryKeySelective(ConsumerToConsumer record);

    int updateByPrimaryKey(ConsumerToConsumer record);

    Long selectCustomerCpId(Long cpId);
}