package com.xquark.dal.mapper;

import com.xquark.dal.model.User;
import com.xquark.dal.vo.UserMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by quguangming on 16/6/27.
 */
public interface MemberMapper {

  Long countMember(@Param(value = "params") Map<String, Object> params);

  Long countWechatMember(@Param(value = "params") Map<String, Object> params);

  List<UserMemberVO> getMemberList(@Param("params") Map<String, Object> params,
      @Param("pager") Pageable pageable);

  List<User> getWechatMemberList(@Param(value = "params") Map<String, Object> params,
      @Param(value = "pager") Pageable pageable);

  int setMemberLevel(@Param(value = "params") Map<String, Object> params);

  int setMemberPoints(@Param(value = "params") Map<String, Object> params);

  int countMemberByLevel(@Param(value = "params") Map<String, Object> params);

  int countMemberByWeek(@Param(value = "params") Map<String, Object> params);

  Long countMembersAll(@Param("day") Integer day);

  Long countDealMembers(@Param("day") Integer day);

  /**
   * 新增会员
   *
   * @param day 前几天
   * @return 日期与会员数量 Map
   */
  List<Map<Date, Long>> addUpMembers(@Param("day") Integer day);

  /**
   * 新增成交会员
   *
   * @param day 前几天
   * @return 日期与会员数量 Map
   */
  List<Map<Date, Long>> addUpDealMembers(@Param("day") Integer day);

}
