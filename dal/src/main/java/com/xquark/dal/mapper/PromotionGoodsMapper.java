package com.xquark.dal.mapper;

import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionGoodsVO;
import com.xquark.dal.vo.PromotionPGoodsVO;
import com.xquark.dal.vo.SaleProduactVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author LiHaoYang
 * @date 2018/9/6 0006
 */

public interface PromotionGoodsMapper {
  /**
   * 活动已开始商品查询
   */
  List<PromotionGoodsVO> selectBeginningPromotionGoods(
          @Param("grandSaleId") Integer grandSaleId,
          @Param("userLevel") String userLevel,
          @Param("firstNo") Integer firstNo,
          @Param("pageSize") Integer pageSize,
          @Param("promotionType") PromotionType promotionType);

  /**
   * 查询商品是否有赠品
   */
  Integer qualityGoods(@Param("pCode") String pCode, @Param("productId") String productId);

  /**
   * 查询活动已开始商品(废弃)
   */
  List<PromotionPGoodsVO> selectProduct(@Param("firstNo") Integer firstNo, @Param("pageSize") Integer pageSize);

  /**
   * 活动未开始商品查询
   */
  List<PromotionGoodsVO> selectNotBeginningPromotionGoods(
          @Param("grandSaleId") Integer grandSaleId,
          @Param("userLevel") String userLevel,
          @Param("firstNo") Integer firstNo,
          @Param("pageSize") Integer pageSize,
          @Param("promotionType") PromotionType promotionType);

  /**
   * 活动已开始商品总数查询
   */
  int selectBeginningGoodsCount(
          @Param("grandSaleId") Integer grandSaleId,
          @Param("userLevel") String userLevel,
          @Param("userLevel2") String userLevel2,
          @Param("promotionType") PromotionType promotionType);

  /**
   * 活动未开始商品查询
   */
  int selectNotBeginGoodsCount(@Param("grandSaleId") Integer grandSaleId,
                               @Param("userLevel") String userLevel,
                               @Param("userLevel2") String userLevel2,
                               @Param("promotionType") PromotionType promotionType);

  /**
   * 查询最低的优惠价
   * @return
   */
  BigDecimal selectMinDiscount(@Param("pCode")String pCode);

  /**
   * 查询拼团所需最低人数
   */
  Integer selectMinPeople(@Param("pCode") String pCode);

  /**
   * 查询当前登录用户的资格
   */
  PromotionGoodsVO selectUserQua(@Param("cpid") String cpid);


  /**
   * 查询商品开团所需的资格
   */
  List<String> selectOrgGroupQua();

  /**
   * 当前用户是否为纯白人（未下过订单）
   */
  int selectIsNew(@Param("cpid") String cpid);

  /**
   * 活动商品库存查询
   */
  int promotionGoodsCount(@Param("pCode") String pCode, @Param("skuCode") String skuCode);

  /**
   * 根据pCode 查询活动商品信息
   */
  List<PromotionPGoodsVO> selectProProductByPcode(@Param("pCode") String pCode);

  /**
   * 查询橱窗和非橱窗展示的所有活动商品
   */
  List<PromotionPGoodsVO> selectProProductListByPcode(
          @Param("offSet") int offSet,
          @Param("pageSize") int pageSize,
          @Param("pCode") String pCode);

  /**
   * 查询商品数量
   */
  int selectProProductCount(@Param("pCode") String pCode);

  List<SaleProduactVO> selectSalesProductsByPcode(@Param("pCode")String pCode,@Param("show")Integer show);

  List<SaleProduactVO> selectAllProductForGrandSale(@Param("pCode")String pCode);

}
