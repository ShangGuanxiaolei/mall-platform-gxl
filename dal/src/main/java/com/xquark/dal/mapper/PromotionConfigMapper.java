package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PromotionConfig;

import java.util.List;
import java.util.Map;

public interface PromotionConfigMapper {
	
	PromotionConfig selectByConfigType(@Param("configType")String configType);

    PromotionConfig selectByConfig(Map map);

    List<PromotionConfig> selectListByConfigType(Map map);

    PromotionConfig selectBySkuCodeAndConfigName(@Param("skuCode")String skuCode,@Param("configName")String configName);

    String selectPcodeBySku(@Param("skuCode")String skuCode);

}
