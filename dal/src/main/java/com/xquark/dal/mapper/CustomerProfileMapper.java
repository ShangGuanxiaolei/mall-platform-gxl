package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerProfile;
import com.xquark.dal.vo.BaseCustomerVO;
import com.xquark.dal.vo.CustomerByPhone;
import com.xquark.dal.vo.WxVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerProfileMapper {

  int deleteByPrimaryKey(Long cpId);

  int insert(CustomerProfile record);

  int add(CustomerProfile record);

  int insertSelective(CustomerProfile record);

  CustomerProfile selectByPrimaryKey(Long cpId);

  boolean selectExists(Long cpId);

  int updateByPrimaryKeySelective(CustomerProfile record);

  int updateByPrimaryKey(CustomerProfile record);

  void merge(CustomerProfile customerProfile);

  List<BaseCustomerVO> listCustomerByUnionId(String unionId);

  List<WxVO> listCusByUnionId(String unionId);

  boolean selectWechatUnionExists(String unionId);

  Integer deleteWechatUnionByCpId(Long cpId);

  Boolean updatePhone(@Param("cpId") Long cpId, @Param("phone") String phone);

  List<CustomerByPhone> getCustomerByTel(String phone);

  boolean hasUplineCpId(@Param("cpId") Long cpId);

  void updateTinCodeAndName(@Param("cpId") Long cpId, @Param("tinCode") String tinCode, @Param("accountName") String accountName);

  String selectHomeCityIdByOrderNo(@Param("orderNo")String orderNo);

  String selectHomeCityIdByOrderNoAndParentId(@Param("orderNo")String orderNo);

  String selectStoreIdByHomeCityId(@Param("homeCityId")String homeCityId);

  //boolean selectExistsStoreIdAndRegionId(@Param("cpId")long cpId);


  boolean  getPhoneCus(@Param("phone") String phone);
  boolean  getPhoneCusX(@Param("phone") String phone);

  int updatePhoneT(@Param("cpId") Long cpId, @Param("phone") String phone);

  int updatePhoneH(@Param("cpId") Long cpId, @Param("phone") String phone);

  /**
   * @author Kwonghom
   * @date 2019/4/9 15:31
   * @param [cpId]
   * @return java.util.List<com.xquark.dal.model.CustomerProfile>
   */
  List<CustomerProfile> selectCustomerProfilesByCpId(Long cpId);

    // 查询当前用户的所有下级cpId
    List<Long> selectDownCpIdByUplineCpId(Long cpId);

    // 把下级的upline 更新为上级的upline
    Integer updateDownCustomerUplineByCpIds(List<Long> downCpIds, String upline, Long uplineCpId);

    // 更新当前用户信息
    Integer updateCustomerByCpId(Long cpId);

    // 更新当前用户的微信记录
    Integer updateWechatUnionIdByCpId(Long cpId);

    Integer updateWechatUnionIdByUnionId(String unionId);

    // 更新当前用户的 user 信息
    Integer updateUserByCpId(Long cpId);

    boolean selectWechatUnionExistsByCpId(Long cpId);

    boolean selectUserExistsByCpId(Long cpId);

    int countWechatUnionByUnionId(String unionId);
}