package com.xquark.dal.mapper;

import com.xquark.dal.model.wechat.CustomizedMenuItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WechatCustomizedMenuMapper {

  List<CustomizedMenuItem> list(@Param("shopId") String shopId,
      @Param("wechatAppId") String wechatAppId);

  int insert(CustomizedMenuItem customizedMenuItem);

  int updateByPrimaryKeySelective(CustomizedMenuItem customizedMenuItem);

  CustomizedMenuItem selectByPrimaryKey(@Param("id") String id);

  int updateIndexById(@Param("id") String id, @Param("newIndex") Integer newIndex);

  int updateRenewable(@Param("shopId") String shopId, @Param("renewable") Boolean renewable);

  CustomizedMenuItem selectByButtonKey(@Param("buttonKey") String buttonKey);
}

