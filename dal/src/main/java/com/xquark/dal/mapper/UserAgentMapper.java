package com.xquark.dal.mapper;

import com.xquark.dal.model.UserAgent;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserAgentMapper {

  UserAgentVO selectByPrimaryKey(@Param("id") String id);

  int insert(UserAgent userAgent);

  int modify(UserAgent userAgent);


  /**
   * @param id
   * @return
   */
  int delete(@Param("id") String id);


  /**
   * 服务端分页查询数据
   */
  List<UserAgentVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询某个代理下级各个代理类型人数汇总
   */
  List<Map> getTotalByParentId(@Param(value = "parentId") String parentId);

  /**
   * 分页查询某个代理下级某种代理类型所有的明细人员信息
   */
  List<UserAgentVO> listByParentAndType(@Param(value = "pager") Pageable pager,
      @Param(value = "parentId") String parentId, @Param(value = "type") String type);

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  List<UserAgentVO> listApplyingByParent(@Param(value = "pager") Pageable pager,
      @Param(value = "parentId") String parentId);

  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  Long countApplyingByParent(@Param(value = "parentId") String parentId);

  /**
   * 分页查询某个代理所有代理申请人信息
   */
  List<UserAgentVO> listUserByParent(@Param(value = "pager") Pageable pager,
      @Param(value = "parentId") String parentId,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 查询某个代理下面的代理总数
   */
  Long countUserByParent(@Param(value = "parentId") String parentId,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 根据手机号查询某个代理
   */
  UserAgentVO selectByPhone(@Param("phone") String phone);

  /**
   * 根据手机号查询某个代理是否在已审核状态
   */
  UserAgentVO selectByPhoneActive(@Param("phone") String phone);

  /**
   * 根据手机号查询某个代理是否在待审核状态
   */
  UserAgentVO selectByPhoneApplying(@Param("phone") String phone);

  /**
   * 根据微信号查询某个代理
   */
  UserAgentVO selectByWeixin(@Param("weixin") String weixin);

  /**
   * 根据userId查询某个代理
   */
  UserAgentVO selectByUserId(@Param("userId") String userId);

  /**
   * 查询某个代理下面的代理总数
   */
  Long selectCntByParentId(@Param(value = "parentId") String parentId);

  /**
   * 根据手机号或微信号查询
   */
  UserAgentVO selectByPhoneOrWeixin(@Param("keys") String keys);

  /**
   * 得到经销商总店shopid
   */
  String getRootShopId();

  /**
   * 分页查询所有代理的佣金信息
   */
  List<UserAgentCommissionVO> listUserCommission(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询某个代理的佣金信息
   */
  List<UserAgentCommissionVO> searchUserCommission(@Param(value = "pager") Pageable pager,
      @Param("userId") String userId, @Param("keys") String keys);

  /**
   * 查询所有代理的佣金信息总数
   */
  Long selectCntUserCommission(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询本月新增代理(新增的直招代理人数总和)
   */
  Long countMonthByB2BDirect(@Param("userId") String userId);

  /**
   * 查询本月新增代理(对于董事，统计本月新增的所有代理人数总和（不包含直接或间接下级是董事的团队人数）)
   */
  Long countMonthByB2BDirector(@Param("userId") String userId, @Param("shopId") String shopId,
      @Param("rootShopId") String rootShopId);

  /**
   * 团队人数的统计规则：计算包括直接和间接的下级代理总数
   */
  Long countTeamByB2B(@Param("userId") String userId, @Param("shopId") String shopId);

  /**
   * 得到最近七天新增的用户数
   */
  Long getNewUser();

  /**
   * 得到总用户数
   */
  Long getAllUser();

  /**
   * 获取一周的代理交易额
   */
  List<Map> getAmount(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的代理订单
   */
  List<Map> getOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的新增代理
   */
  List<Map> getAgent(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的代理佣金
   */
  List<Map> getCommission(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取代理的合同图片
   */
  String getContractImg(@Param("userId") String userId);

  /**
   * @param phone
   * @return
   */
  int deleteShopByPhone(@Param("phone") String phone);

  /**
   * 逻辑删除shop
   */
  int deleteShopLogically(@Param("shopId") String shopId);

  /**
   * @param phone
   * @return
   */
  int deleteShopTreeByPhone(@Param("phone") String phone);

  /**
   * 逻辑删除shoptree
   */
  int deleteShopTreeLogically(@Param("shopId") String shopId);

  /**
   * @param phone
   * @return
   */
  int deleteUserByPhone(@Param("phone") String phone);

  /**
   * 逻辑删除用户
   */
  int deleteUserLogically(@Param("phone") String phone);

  /**
   * @param phone
   * @return
   */
  int deleteUserAgentByPhone(@Param("phone") String phone);

  /**
   * 逻辑删除代理
   */
  int deleteUserAgentLogically(@Param("id") String id);

  /**
   * 获取代理地区分布信息
   */
  List<Map> getZones();

  /**
   * 得到所有代理用户信息
   */
  List<UserAgentVO> getAllUsers();

  /**
   * 查询手机号对应用户的所有上级和所有直接下级代理
   */
  List<String> getAllFriends(@Param("phone") String phone);


  /**
   * 判断两个手机号是否是上下级关系
   */
  UserAgentVO selectByPhoneAndParentPhone(@Param("parentPhone") String parentPhone,
      @Param("phone") String phone);

  /**
   * 查询某个代理的团队成员信息
   */
  List<UserAgentVO> listMembers(@Param(value = "pager") Pageable pager,
      @Param(value = "parentId") String parentId);

  /**
   * 获取某个用户，当月，当季，当年销售数据
   */
  List<Map> getMemberSales(@Param("userId") String userId);

  /**
   * 获取某个用户下级代理数据
   */
  List<Map> getMemberAgents(@Param("userId") String userId);

  /**
   * 更新用户手势图片
   */
  int updateLifeImg(@Param("userId") String userId, @Param("url") String url);

  /**
   * 更新用户身份证照片
   */
  int updateIdcardImg(@Param("userId") String userId, @Param("url") String url);

  /**
   * 更新合同图片字段
   */
  int updateContractImg(@Param("userId") String userId, @Param("url") String url);

  /**
   * 根据wxbotId查询某个代理
   */
  UserAgentVO selectByWxbotId(@Param("wxbotId") String wxbotId);

  /**
   * 根据手机号或身份证号查询某个代理
   */
  UserAgentVO selectByPhoneOrIdcard(@Param("phoneOrIdcard") String phoneOrIdcard);

  /**
   * 更新wxbotId字段
   */
  int updateWxbotId(@Param("id") String id, @Param("wxbotId") String wxbotId);


  /**
   * 服务端分页查询数据
   */
  List<UserAgentVO> listFounder(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 更新area字段
   */
  int updateArea(@Param("id") String id, @Param("area") String area);

  /**
   * 查询所有联合创始人代理信息
   */
  List<UserAgentVO> getAllFounder();

  /**
   * 获取所有董事和联合创始人代理
   */
  List<UserAgent> listAllDirectorAndFounder();

}
