package com.xquark.dal.mapper;

import com.xquark.dal.model.XquarkOrderThirdNoerp;

public interface XquarkOrderThirdNoerpMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkOrderThirdNoerp record);

    int insertSelective(XquarkOrderThirdNoerp record);

    XquarkOrderThirdNoerp selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkOrderThirdNoerp record);

    int updateByPrimaryKey(XquarkOrderThirdNoerp record);
}