package com.xquark.dal.mapper;

import com.xquark.dal.model.Category;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface CategoryMapper {

  int insert(Category record);

  Category load(String id);

  Category loadByThird(@Param("thirdId") String thirdId, @Param("source") ProductSource source);

  Category selectByTermAndTaxonomy(@Param("termId") String termId, @Param("taxo") Taxonomy taxo,
      @Param("shopId") String shopId);

  List<Category> selectRootNodes(Taxonomy taxo);

  List<Category> selectByTaxonomy(Taxonomy taxo);

  List<Category> selectSiblings(String id);

  Category loadParent(String id);

  List<Category> loadSubs(String id);

  List<Category> loadDescendants(String id);

  // for shop user
  void updateCategoryTerm(String id, String termId, String term, String userId);

  List<Category> listRootsByUser(Taxonomy taxo, String userId,
      @Param("limitByUser") Boolean limitByUser);

  Category loadByTermIdAndTaxonomy(String id, Taxonomy taxo, String userId);

  int delete(String id, String userId);

  Integer selectMaxIdx(@Param("parentId") String parentId, @Param("shopId") String shopId);

  void updateIdx(String srcId, int idx);

  void updateIdxNotBeforeDest(@Param("parentId") String parentId, @Param("shopId") String shopId,
      @Param("idx") int idx, @Param("increment") int increment);

  void updateIdxAfterDest(@Param("parentId") String parentId, @Param("shopId") String shopId,
      @Param("idx") int idx, @Param("increment") int increment);

  CategoryVO loadCategoryByProductId(String productId);

  List<CategoryVO> loadCategorysByProductId(String productId);

  void deleteCategorys(@Param("userId") String userId, @Param("ids") String... ids);

  CategoryVO selectVoById(String categoryId);

  List<Category> listCategorybyAll(@Param("paramsMap") Map<String, Object> paramsMap,
      @Param("pager") Pageable pager);

  Long listCategoryCountbyAll();

  List<Category> listAllCategorys(@Param("shopId") String shopId,
      @Param("taxonomy") Taxonomy taxonomy);

  void updateImg(@Param("img") String img, @Param("id") String id);

  void updateTinyImg(@Param("img") String img, @Param("id") String id);

  void updateFontColor(@Param("fontColor") String fontColor, @Param("id") String id);

  void move(@Param("id") String id, @Param("parentid") String parentid, @Param("idx") String idx);

  void upIdx(@Param("parentId") String parentId, @Param("idx") String idx,
      @Param("sourceidx") String sourceidx);

  void downIdx(@Param("parentId") String parentId, @Param("idx") String idx,
      @Param("sourceidx") String sourceidx);

  void jumpIdx(@Param("sourceparentId") String sourceparentId,
      @Param("sourceidx") String sourceidx);

  void jumpupIdx(@Param("parentId") String parentId, @Param("idx") String idx);

  void updateTreePath(@Param("id") String id, @Param("treePath") String treePath);

  int updateForInstock(String id);

  int updateForOnsale(String id);

  Category loadBySourceIdAndShopId(@Param("id") String id, @Param("shopId") String shopId);

  List<Category> loadCategoriesByShopId(@Param("shopId") String shopId,
      @Param("taxonomy") Taxonomy taxonomy);

  List<Category> loadCategoriesByShopIdAndParentId(@Param("shopId") String shopId,
      @Param("parentId") String parentId);

  /**
   * 根据名称查找分类
   */
  Category loadByName(@Param("name") String name);

  List<Category> listCategoriesByShopAndStatus(@Param("shopId") String shopId,
      @Param("categoryStatus") CategoryStatus categoryStatus);

  List<Category> listCategoriesByShopAndStatusPage(@Param("pager") Pageable pager,
      @Param("shopId") String shopId, @Param("categoryId") String categoryId,
      @Param("categoryStatus") CategoryStatus categoryStatus);

  /**
   * 根据名称查找分类 like
   */
  List<Category> listByName(@Param("shopId") String shopId, @Param("name") String name,
      @Param("parentCategoryId") String parentCategoryId);

  /**
   * 我的店铺品牌排序
   */
  int sortShopCategorys(List<String> list);

  /**
   * 总店类别更新后，同时更新店铺上架复制出来的品牌类别
   */
  int updateCopyCategory(@Param("id") String id);

  /**
   * 总店类别删除后，同时删除店铺上架复制出来的品牌类别
   */
  int deleteCopyCategory(@Param("id") String id);

  /**
   * 供客户端调用，获取首页商品类别信息
   */
  List<Category> getHomeForApp();

   List<Category> getNameAndId();

   List<Category> getRandIdList();

   List<Category> getAllCategoryIdList();

   List<CategoryVO> selectCategoryByIds(List<String> categoryIds);

  int deleteCategoryById(String id);
}