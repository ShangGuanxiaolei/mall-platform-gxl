package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouSetting;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YundouSettingMapper {

  int deleteByPrimaryKey(String id);

  int insert(YundouSetting record);

  int insertSelective(YundouSetting record);

  YundouSetting selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(YundouSetting record);

  int updateByPrimaryKey(YundouSetting record);

  int updateForArchive(String id);

  YundouSetting selectByShopId(@Param("shopId") String shopId);

  List<YundouSetting> list();

}