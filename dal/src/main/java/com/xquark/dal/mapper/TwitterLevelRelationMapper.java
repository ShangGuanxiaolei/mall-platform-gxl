package com.xquark.dal.mapper;

import com.xquark.dal.model.TwitterLevel;
import com.xquark.dal.model.TwitterLevelRelation;
import org.apache.ibatis.annotations.Param;


public interface TwitterLevelRelationMapper {

  TwitterLevelRelation selectByPrimaryKey(@Param("id") String id);

  int insert(TwitterLevelRelation twitterLevelRelation);

  int modify(TwitterLevelRelation twitterLevelRelation);


  /**
   * @param id
   * @return
   */
  int delete(@Param("id") String id);

  /**
   * 删除用户之前所有的推客等级信息
   */
  int deleteByUserId(@Param("shopId") String shopId, @Param("userId") String userId);

  /**
   * 得到某个用户的推客等级信息
   */
  TwitterLevel selectByUserId(@Param("shopId") String shopId, @Param("userId") String userId);

  /**
   * 自动删除到期的推客会员
   */
  int autoClose();

}
