package com.xquark.dal.mapper;

import com.xquark.dal.model.PartnerProductCommission;
import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.vo.PartnerProductCommissionVO;
import com.xquark.dal.vo.TwitterProductCommissionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PartnerProductCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(PartnerProductCommission record);

  int insertSelective(PartnerProductCommission record);

  PartnerProductCommissionVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerProductCommission record);

  int updateByPrimaryKey(PartnerProductCommission record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<PartnerProductCommissionVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(@Param(value = "shopId") String shopId,
      @Param(value = "productId") String productId);

}