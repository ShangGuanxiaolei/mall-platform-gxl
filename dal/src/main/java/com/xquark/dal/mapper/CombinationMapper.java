package com.xquark.dal.mapper;

import com.xquark.dal.model.Combination;
import com.xquark.dal.vo.CombinationVO;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-5-31. Time: 下午7:25 组合商品
 */
public interface CombinationMapper {

  /**
   * 添加
   */
  int insert(Combination obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<Combination> collection);

  /**
   * 通过id查找
   */
  Combination getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(Combination obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<Combination> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

  /**
   * 根据id，查询组合的vo
   * @param id
   * @return
   */

  CombinationVO getVOByPrimaryId(@Param("id") String id,@Param("page") Pageable page, @Param("params") Map<String, Object> params);

}