package com.xquark.dal.mapper;

import com.xquark.dal.model.SkuCombine;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SkuCombineMapper {

  int deleteByPrimaryKey(String id);

  int insert(SkuCombine record);

  SkuCombine selectByPrimaryKey(String id);

  List<SkuCombine> selectByProductId(String productId);

  SkuCombine selectBySkuId(String skuId);

  int updateByPrimaryKeySelective(SkuCombine record);

  int updateByPrimaryKey(SkuCombine record);

  boolean isMaster(String skuId);

  boolean isCombineProduct(@Param("productId") String productId);

  boolean isCombineSku(@Param("skuId") String skuId);

}