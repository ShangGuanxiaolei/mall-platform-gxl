package com.xquark.dal.mapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.CashierItem;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentChannel;
import com.xquark.dal.vo.CouponInfoVO;

public interface CashierItemMapper {

  /**
   * 根据主键获取
   */
  CashierItem selectByPrimaryKey(String id);

  /**
   * 新增
   */
  int insert(CashierItem item);

  /**
   * 根据支付号获取收银列表
   */
  List<CashierItem> listByBizNo(@Param("bizNo") String bizNo);
  /**
   * 根据支付方式和主订单号查询收银列表
   * @param payType
   * @param orderNo
   * @return
   */
  List<CashierItem> listByPayTypeAndOrderNo(@Param("paymentChannel") PaymentChannel paymentChannel,@Param("payType") String payType, @Param("orderNo") String orderNo);

  /**
   * 收银明细支付完成
   */
  int paid(@Param("id") String id);

  /***
   *
   * @param bizNo
   * @return
   */
  BigDecimal loadPaidFee(@Param("bizNo") String bizNo);

  /**
   *
   * @param bizNo
   * @return
   */
  List<Map<String, Object>> loadHongBaoFee(@Param("bizNo") String bizNo);

  /**
   * 更新
   */
  int update(CashierItem item);

  List<CouponInfoVO> loadCouponInfoByOrderNo(@Param("bizNo") String bizNo);

  List<CashierItem> listByBatchBizNo(@Param("batchBizNo") String batchBizNo,
      @Param("userId") String userId, @Param("status") PaymentStatus status);
//	return cashierItemMapper.listByBatchBizNo(batchBizNo, userId, status);

  int close(CashierItem item);

}
