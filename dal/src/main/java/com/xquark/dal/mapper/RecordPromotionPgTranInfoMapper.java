package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPgTranInfo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Component
public interface RecordPromotionPgTranInfoMapper {

  //记录拼团信息
//  void save(PromotionPgTranInfo promotionPgTranInfo);
  int save(PGTranInfoVo pgTranInfoVo);

  /**
   * 使用pCode查询出开团时间
   * @param pCode
   * @return
   */
  Timestamp selectTime(String pCode);

  /**
   * 根据pcode查询团长
   * @param pCode
   * @return
   */
  String selectMaster(String pCode);

BigDecimal selectPgDiscount(@Param("pCode") String pCode, @Param("pDetailCode") String pDetailCode);

  String selectTranCode(@Param("pCode") String pCode, @Param("pDetailCode") String pDetailCode);

  void updatePieceStatus(PromotionPgTranInfo promotionPgTranInfo);

  /**
   * 根据tranCode查询组团信息
   * @param pieceGroupTranCode
   * @return
   */
  Map<String, Object> selectAllByTranCode(@Param("pieceGroupTranCode")String pieceGroupTranCode);

  PGTranInfoVo selectTranInfoVOByCode(@Param("tranCode") String tranCode);

  /**
   * 根据tranCode和memberId查询拼团成员信息
   * @param pieceGroupTranCode
   * @param memberId
   * @return
   */
  Map<String, Object> selectMemberInfoByTranCodeAndMemberId(@Param("pieceGroupTranCode")String pieceGroupTranCode, @Param("memberId")String memberId);

  /**
   * 更新团主的拼团状态
   * 只更新当前为开团状态
   */
  int updateStatusByTranCode(@Param("tranCode") String tranCode, @Param("status") int status,
      @Param("originCodeList") List<Integer> originCodeList);

  List<Map> selectAllByStatusPush();
}
