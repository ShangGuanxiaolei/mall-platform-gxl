package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderDetail;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderDetailMapper {
    int insert(OrderDetail record);

    int insertSelective(OrderDetail record);

    int batchInsert(@Param("collection") List<OrderDetail> list);
}