package com.xquark.dal.mapper;

import com.xquark.dal.SequenceId;
import org.apache.ibatis.annotations.Param;

public interface SequenceGeneratorMapper {

  /**
   * 会员自增序列
   *
   * @param sequenceId 全局id对象
   * @return 自增后id
   */
  Long replaceMemberInfo(SequenceId sequenceId);

  /**
   * 获取当前的会员id
   * @return 当前数据库中的会员id
   */
  Long getMemberId();

  /**
   * 银行自增序列
   *
   * @param sequenceId 全局id对象
   * @return 自增后id
   */
  Long replaceBankInfo(SequenceId sequenceId);

  /**
   * 获取当前的银行id
   * @return 当前数据库中的银行id
   */
  Long getBankId();

  /**
   * 地址自增序列
   *
   * @param sequenceId 全局id对象
   * @return 自增后id
   */
  Long replaceAddressInfo(SequenceId sequenceId);

  /**
   * 获取当前数据中的地址id
   * @return 当前数据库中的地址id
   */
  Long getAddressId();

  /**
   * 订单自增序列
   *
   * @param sequenceId 全局id对象
   * @return 自增后id
   */
  Long replaceOrderInfo(SequenceId sequenceId);

  /**
   * 获取当前数据中的订单id
   * @return 当前数据库中的地址id
   */
  Long getOrderId();
 /*
  * @Author chp
  * @Description //redis自增满1000进行数据库更新
  * @Date
  * @Param
  * @return
  **/
  int orderUpdate();

  int orderIdUpdate(@Param("id") Long id);
}