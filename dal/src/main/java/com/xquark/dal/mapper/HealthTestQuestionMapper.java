package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestQuestion;
import com.xquark.dal.model.HealthTestQuestionExample;
import com.xquark.dal.vo.HealthTestModuleQuestionVO;
import com.xquark.dal.vo.HealthTestQuestionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestQuestionMapper {

  long countByExample(HealthTestQuestionExample example);

  int deleteByExample(HealthTestQuestionExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestQuestion record);

  int insertSelective(HealthTestQuestion record);

  List<HealthTestQuestion> selectByExample(HealthTestQuestionExample example);

  HealthTestQuestion selectByPrimaryKey(String id);


  int updateByExampleSelective(@Param("record") HealthTestQuestion record,
      @Param("example") HealthTestQuestionExample example);

  int updateByExample(@Param("record") HealthTestQuestion record,
      @Param("example") HealthTestQuestionExample example);

  int updateByPrimaryKeySelective(HealthTestQuestion record);

  int updateByPrimaryKey(HealthTestQuestion record);

  // 分割线

  HealthTestQuestionVO selectVOByPrimaryKey(String id);

  List<HealthTestQuestionVO> selectVOByExample(HealthTestQuestionExample example);

  List<HealthTestModuleQuestionVO> selectQuestionWithModule(@Param("moduleIds") String[] ids,
      @Param("requiredSex") Integer requiredSex);
}