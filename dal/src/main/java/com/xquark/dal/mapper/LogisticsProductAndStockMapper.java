package com.xquark.dal.mapper;

import com.xquark.dal.model.GoodInfo;
import com.xquark.dal.model.Skus;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LogisticsProductAndStockMapper {

    /**
     * 统计各个供应商的产品
     * @return
     */
    int countTotalPolyProduct(@Param("supplierId") String supplierId);

    /**
     * 各个供应商产品的分页查询
     * @param offset 偏移量
     * @param PageSize 页数
     *
     * @return
     */
    List<GoodInfo> limitSelect(@Param("supplierId") String supplierId,
        @Param("offset") Integer offset, @Param("PageSize") Integer PageSize);

    /**
     * 产品的SKU
     * @return 产品Sku的集合
     */
    List<Skus> selectSku(@Param("id") String id);

  /**
   * 更新商品库存
   * @param quantity
   */
  void updateNewQuantity(@Param("quantity") Integer quantity, @Param("id") Long id);

    /**
     * 各个供应商商品库存查询
     * @param barCode 条形码
     * @return
     */
    Integer selectQuantityNewByBarCode(@Param("barCode") String barCode,
        @Param("supplierCode") String supplierCode);

  /**
   * 更新sku库存
   * @param quantity
   * @param skuId
   */
  Integer updateNewSkuQuantity(@Param("quantity") Integer quantity,
      @Param("skuId") String skuId,@Param("supplierCode")String supplierCode);

  /**
   * 检查传进来的id是否存在
   * @param id
   * @return
   */
  Integer selectIdByProductId(@Param("id") Long id);

}