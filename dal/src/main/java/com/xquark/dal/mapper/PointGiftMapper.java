package com.xquark.dal.mapper;

import com.xquark.dal.model.PointGift;
import com.xquark.dal.model.PointGiftDetail;
import com.xquark.dal.model.UserDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PointGiftMapper {
    int deleteByPrimaryKey(String id);

    int insert(PointGift record);

    int insertSelective(PointGift record);

    PointGift selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PointGift record);

    int updateByPrimaryKey(PointGift record);

    List<PointGift> selectExpiredPacket();

    void updatePointGiftToExpired(@Param("pointGiftNos") List<String> pointGiftNos);

    PointGift getByPointGiftNo(@Param("pointGiftNo") String pointGiftNo);

    List<PointGift> selectSendPacket(@Param("cpId") Long cpId);

    PointGift selectBypointGiftNo(String pointGiftNo);

    List<PointGift> listPointGiftsByGiftNo(@Param("pointGiftDetails") List<PointGiftDetail> pointGiftDetails);

    UserDTO selectByUserInfoPointGiftNo(String pointGiftNo);
}