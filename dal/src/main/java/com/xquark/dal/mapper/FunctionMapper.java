package com.xquark.dal.mapper;

import com.xquark.dal.model.Function;
import com.xquark.dal.model.ModuleFunction;
import com.xquark.dal.vo.FunctionVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * 作者: wangxh 创建日期: 17-4-21 简介:
 */
public interface FunctionMapper {

  Integer insert(Function function);

  Integer insertModuleFunction(ModuleFunction moduleFunction);

  Integer deleteById(@Param("id") String id);

  Integer deleteByHtmlId(@Param("htmlId") String htmlId);

  Integer update(Function function);

  Function selectOne(@Param("id") String id);

  Function selectByHtmlId(@Param("htmlId") String htmlId);

  List<Function> listAll();

  List<FunctionVO> listFunctionVO(@Param("moduleId") String moduleId, @Param("order") String order,
      @Param("page") Pageable page, @Param("direction") Sort.Direction direction);

  Integer count();

  Integer countByModuleId(@Param("moduleId") String moduleId);

  List<String> listRoles(@Param("functionId") String functionId);

  List<String> listEnableRoles(@Param("functionId") String functionId);

  List<String> listVisibleRoles(@Param("functionId") String functionId);

}
