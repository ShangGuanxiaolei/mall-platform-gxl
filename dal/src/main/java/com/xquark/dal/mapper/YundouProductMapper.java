package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouProduct;
import com.xquark.dal.type.YundouCategoryType;
import com.xquark.dal.vo.YundouProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Map;

public interface YundouProductMapper {

  int deleteByPrimaryKey(String id);

  int insert(YundouProduct record);

  int insertSelective(YundouProduct record);

  YundouProductVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(YundouProduct record);

  int updateByPrimaryKey(YundouProduct record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<YundouProductVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 根据类别查询，不传category则查询全部
   *
   * @param categoryType 分类类型
   * @param pageable 分页对象
   * @return 积分商品或空集合
   */
  List<YundouProductVO> listWithCategory(@Param("category") YundouCategoryType categoryType,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  /**
   * 根据用户信息查询满足用户积分数量的商品
   *
   * @param userId 用户id
   * @param pageable 分页对象
   * @return 积分商品或空集合
   */
  List<YundouProductVO> listWithUser(@Param("userId") String userId, @Param("setting") int setting,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  Long selectCntWithProducts(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long countByProductId(@Param(value = "shopId") String shopId,
      @Param(value = "productId") String productId);

  YundouProductVO selectByProductId(@Param("productId") String productId,
      @Param("shopId") String shopId);

  List<YundouProductVO> listWithCategoryAndUnion(
      @Param("categoryTypes") YundouCategoryType[] categoryTypes);

  YundouProduct selectByOrderId(String orderId);

}