package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionCustomAreaPlan;

public interface CommissionCustomAreaPlanMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int insert(CommissionCustomAreaPlan record);

  int insertSelective(CommissionCustomAreaPlan record);

  CommissionCustomAreaPlan selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CommissionCustomAreaPlan record);

  int updateByPrimaryKey(CommissionCustomAreaPlan record);

}