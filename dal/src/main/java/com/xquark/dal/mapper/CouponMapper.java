package com.xquark.dal.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Coupon;
import com.xquark.dal.status.CouponStatus;

public interface CouponMapper {

  Coupon selectByPrimaryKey(@Param("id") String id);

  void insert(Coupon coupon);

  List<Coupon> selectByUserAndStatus(@Param("userId") String userId,
      @Param("status") CouponStatus status, @Param("partner") String partner);

//    int updateStatus(@Param("id") String id, @Param("status") CouponStatus status);

  int use(@Param("id") String id, @Param("payNo") String payNo);

  int lock(@Param("id") String id, @Param("payNo") String payNo);

  int unLock(@Param("id") String id);

  int close(@Param("id") String id);

  List<Coupon> listValids(@Param("userId") String userId, CouponStatus valid);

  Coupon selectByCouponCode(@Param("actCode") String actCode,
      @Param("couponCode") String couponCode);

  /**
   * 优惠券发给指定用户
   */
  int grant(@Param("id") String id, @Param("userId") String userId);


  List<Coupon> listCouponsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Long countCouponsByAdmin(@Param(value = "params") Map<String, Object> params);

  int create(@Param(value = "codes") List<String> listCodes,
      @Param(value = "params") Map<String, Object> params);

  List<Coupon> selectByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  Boolean delete(@Param("ids") String[] ids);

  Coupon obtainCoupon(@Param("code") String code);

  Coupon selectByExtCouponId(@Param("extCouponId") String extCouponId,
      @Param("actCode") String actCode);

  List<Coupon> selectByActIdAndDeviceId(@Param("activityId") String activityId,
      @Param("deviceId") String deviceId);

  List<Coupon> selectByActIdAndUserId(@Param("activityId") String activityId,
      @Param("userId") String userId);

  int closeByActId(@Param("activityId") String activityId);
}
