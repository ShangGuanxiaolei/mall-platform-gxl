package com.xquark.dal.mapper;

import com.xquark.dal.model.CommentLike;
import com.xquark.dal.model.CommentLikeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CommentLikeMapper {

  long countByExample(CommentLikeExample example);

  int deleteByExample(CommentLikeExample example);

  int deleteByPrimaryKey(String id);

  int insert(CommentLike record);

  int insertSelective(CommentLike record);

  List<CommentLike> selectByExample(CommentLikeExample example);

  CommentLike selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") CommentLike record,
      @Param("example") CommentLikeExample example);

  int updateByExample(@Param("record") CommentLike record,
      @Param("example") CommentLikeExample example);

  int updateByPrimaryKeySelective(CommentLike record);

  int updateByPrimaryKey(CommentLike record);
}