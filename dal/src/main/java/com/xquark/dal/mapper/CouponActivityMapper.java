package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.CouponActivity;

public interface CouponActivityMapper {

  CouponActivity selectByPrimaryKey(String id);

  CouponActivity selectByActCode(@Param("actCode") String actCode);

  List<CouponActivity> selectByAdmin();

  List<CouponActivity> selectShouldCloseAct();

  int close(@Param("id") String id);

  List<CouponActivity> selectAutoGrantAct(@Param("domain") String domain);

}
