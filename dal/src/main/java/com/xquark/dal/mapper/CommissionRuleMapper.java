package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionRule;
import com.xquark.dal.type.CommissionPolicy;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommissionRuleMapper {

  int insert(CommissionRule rule);

  int updateByPrimaryKeySelective(CommissionRule rule);

  int updateByPrimaryKey(CommissionRule rule);

  /**
   * 查看一个规则集下的所有有效规则
   */
  List<CommissionRule> listByPolicy(CommissionPolicy policy);


  CommissionRule selectByPrimaryKey(@Param("id") String id);

  int modify(CommissionRule role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<CommissionRule> list(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

}
