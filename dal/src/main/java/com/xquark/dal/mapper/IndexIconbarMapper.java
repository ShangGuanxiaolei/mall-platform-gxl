package com.xquark.dal.mapper;

import com.xquark.dal.model.Iconbar;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndexIconbarMapper {

    List<Iconbar> selectIndexIconbarList(@Param("identity") Integer identity);
}
