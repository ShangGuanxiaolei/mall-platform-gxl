package com.xquark.dal.mapper;

import com.xquark.dal.model.WinningList;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface WinningMapper {

	//根据cpId来查询奖品列表
	List<WinningList> getWinningListByCpId(@Param("cpId") Long cpId,
										   @Param("start") Date start,
										   @Param("end") Date end);

	//根据id查询奖品
	WinningList getWinningListByPrimaryKey(String winningId);

	WinningList getUncollectedWinningList(String winningId);

	 //未领取改成已领取
	int updateClaimedStatus(@Param("winningId") String winningId, @Param("addressId") String addressId);

	//超过24小时自动失效
	int updateValidMapper();

	//获取中奖人列表
	List<WinningList> getWinningList();

}
