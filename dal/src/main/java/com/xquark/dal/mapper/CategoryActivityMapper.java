package com.xquark.dal.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.xquark.dal.model.CategoryActivity;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryVO;
import org.springframework.data.domain.Pageable;

/**
 * Created by freedom on 15/11/9.
 */
public interface CategoryActivityMapper {

  List<CategoryActivity> listCategoryforActivity(@Param("userId") String userId);

  List<CategoryActivity> listCategorybyActivity(@Param("paramsMap") Map<String, Object> paramsMap,
      @Param("pager") Pageable pager, @Param("activityId") String activityId);

  int createCategory(@Param("paramsMap") Map<String, String> paramsMap);

  Long existsName(@Param("paramsMap") Map<String, String> paramsMap);

  int deleteCategory(@Param("id") String id);

  int deleteCategoryRelation(@Param("id") String id);

  Long countCategorybyActivity(@Param("activityId") String activityId);

  Long existcategoryforproduct(@Param("activityId") String activityId,
      @Param("productId") String productId, @Param("categoryId") String categoryId);

  int createCategoryforProduct(@Param("activityId") String activityId,
      @Param("productId") String productId, @Param("categoryId") String categoryId);

  int updateCategoryforProduct(@Param("activityId") String activityId,
      @Param("productId") String productId, @Param("categoryId") String categoryId);

  int deleteCategoryforProduct(@Param("activityId") String activityId,
      @Param("productId") String productId, @Param("categoryId") String categoryId);
}
