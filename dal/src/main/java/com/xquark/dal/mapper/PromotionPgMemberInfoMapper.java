package com.xquark.dal.mapper;

import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionPgMemberInfoMapper
 * @date 2018/9/10 0010
 */
public interface PromotionPgMemberInfoMapper {

  /**
   * 查询用户拼团的商品详情
   */
  PromotionOrderGoodsInfoVO selectGoodsInfo(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  PromotionOrderGoodsInfoVO selectTranCodeAndpCodeByOrderNo(@Param("orderNo") String orderNo);

  String selectTranCodeAndpCodeByOrderNo2(@Param("orderNo") String orderNo);

  /**
   * 查询已拼团人数
   */
  int selectSuccessPromotionMemberCount(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  /**
   * 除了查询正常状态的信息外, 还查询自己的参团信息
   */
  int selectSuccessPromotionMemberAndSelf(@Param("tranCode") String tranCode, @Param("cpId")
      Long cpId);

  /**
   * 除了查询正常状态的信息外, 还查询自己订单信息
   */
  int selectSuccessPromotionMemberAndOrder(@Param("tranCode") String tranCode, @Param("orderNo")
      String orderNo);

  /**
   * 查询拼团总人数
   */
  int selectPgMemberCount(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  /**
   * 查询拼团成员信息
   * @param pieceGroupTranCode tranCode
   * @param orderNo 如果传了orderNo则同时带出orderNo
   * @param limit
   */
  List<PromotionPgMemberInfoVO> selectMemberInfo(
      @Param("pieceGroupTranCode") String pieceGroupTranCode,
      @Param("orderNo") String orderNo, @Param("limit") Integer limit);

  List<PromotionPgMemberInfoVO> selectMemberInfoBycpId(
          @Param("pieceGroupTranCode") String pieceGroupTranCode, @Param("cpId") Long cpId);

  /**
   * 查询成团剩余有效时间
   */
  int selectRegimentTime(@Param("pCode") String pCode);

  /**
   * 查询开团时间
   */
  Date selectGroupOpenTime(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  /**
   * 统计个人参团次数
   */
  int checkJoinTimes(@Param("pieceGroupTranCode") String pieceGroupTranCode,
      @Param("memberId") String memberId);

  /**
   * 统计参团人数
   */
  int countPerson(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  /**
   * 统计参团的纯白人人数
   */
  int countNewPerson(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  String selectStatus(@Param("pieceGroupTranCode") String pieceGroupTranCode);

  List<Integer> selectMemberStatus(@Param("pieceGroupTranCode") String pieceGroupTranCode);
}
