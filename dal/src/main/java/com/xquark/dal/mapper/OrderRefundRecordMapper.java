package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.OrderRefundRecord;

/**
 * @author zhuyin
 */
public interface OrderRefundRecordMapper {

  List<OrderRefundRecord> listOrderByBatchNo(@Param("banchNO") String banchNO);

  void saveOrderByOrderNo(OrderRefundRecord batchOrder);

  //List<OrderRefundRecord>  listOrderByBatchNoAndTradeNo(@Param("banchNO") String banchNO, @Param("tradeNo") String tradeNo);


  List<String> listBatchNoByOrderNo(@Param("orderNo") String orderNo);
}
