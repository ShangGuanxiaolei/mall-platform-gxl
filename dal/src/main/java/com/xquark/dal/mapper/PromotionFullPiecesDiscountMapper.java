package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionFullPiecesDiscount;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionFullPiecesDiscountMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionFullPiecesDiscount record);

  int insertSelective(PromotionFullPiecesDiscount record);

  PromotionFullPiecesDiscount selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionFullPiecesDiscount record);

  int updateByPrimaryKey(PromotionFullPiecesDiscount record);

  int deleteByPromotionId(String id);

  List<PromotionFullPiecesDiscount> listByPromotionId(@Param("id") String id,
      @Param("order") String order, @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  int saveList(@Param("discountList") List<? extends PromotionFullPiecesDiscount> discountList);

  long countDiscount(String promotionId);

  int deleteInPromotion(@Param("promotionId") String promotionId, @Param("id") String id);

  boolean inPromotion(String productId);

}