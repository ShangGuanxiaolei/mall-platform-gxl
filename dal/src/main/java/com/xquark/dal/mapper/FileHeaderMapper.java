package com.xquark.dal.mapper;

import com.xquark.dal.model.FileHeader;

public interface FileHeaderMapper {

  int insert(FileHeader record);

  FileHeader selectByJobId(Long jobId);

  boolean updateSelective(FileHeader fileHeader);

  boolean isFileExists(String path);

}