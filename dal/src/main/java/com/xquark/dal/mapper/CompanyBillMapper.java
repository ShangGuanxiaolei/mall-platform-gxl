package com.xquark.dal.mapper;

import com.xquark.dal.model.CompanyBill;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午7:53
 * 公司请求开票
 */
public interface CompanyBillMapper {

    /**
     * 添加
     */
    int insert(CompanyBill obj);

    /**
     * 批量插入
     *
     * @param collection
     * @return
     */
    int batchInsert(@Param("collection") List<CompanyBill> collection);

    /**
     * 通过id查找
     */
    CompanyBill getByPrimaryKey(@Param("id") String id);

    /**
     * 更新
     */
    int update(CompanyBill obj);


    /**
     * 通过id删除
     */
    int delete(@Param("id") String id);

    /**
     * 批量删除
     */
    int batchDelete(@Param("collection") List<String> collection);

    /**
     * 计算数据的数量
     */
    Long count(@Param("params") Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    List<CompanyBill> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

}