package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.TweetImage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TweetImageMapper {

  int updateForArchive(String id);

  int updateForUnArchive(String id);

  int insert(TweetImage record);

  int updateForArchiveByParam(@Param("tweetId") String tweetId);

  /**
   * 根据发现记录ID获取所有的Image列表
   */
  List<TweetImage> selectByTweetId(String tweetId);

  List<TweetImage> getImgs4Sync(@Param("productId") String productId);

  int updateImgOrder(TweetImage img);

  List<TweetImage> getProductImgs(@Param("productId") String productId);

}