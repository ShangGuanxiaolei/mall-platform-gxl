package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityPushDetail;
import com.xquark.dal.type.ActionType;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author luqig
 * @since 2019-05-02
 */
public interface ActivityPushDetailMapper {
    int deleteByPrimaryKey(@Param("id")String id);

    int insert(ActivityPushDetail record);

    int insertSelective(ActivityPushDetail record);

    ActivityPushDetail load(@Param("id")String id);

    ActivityPushDetail loadByTimeAndActionType(@Param("time") Date time, @Param("actionType") ActionType actionType);


    int updateByPrimaryKeySelective(ActivityPushDetail record);

    int updateByPrimaryKey(ActivityPushDetail record);
}