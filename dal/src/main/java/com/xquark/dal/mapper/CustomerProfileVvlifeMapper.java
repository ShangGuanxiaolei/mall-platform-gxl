package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerProfile;

import java.util.List;

public interface CustomerProfileVvlifeMapper {

    // 查询当前用户在VIVILIFE数据库是否存在
    boolean selectExists(Long cpId);

    // 获取当前用户的信息
    CustomerProfile selectByPrimaryKey(Long cpId);

    // 查询上级为当前用户的所有下级cpId
    List<Long> selectDownCpIdByUplineCpId(Long cpId);

    // 把下级的upline 更新为上级的upline
    Integer updateDownCustomerUplineByCpIds(List<Long> downCpIds, String upline, Long uplineCpId);

    // 更新当前用户信息
    Integer updateCustomerByCpId(Long cpId);

    // 删除当前用户的微信记录
    Integer deleteWechatByCpId(Long cpId);

    // 删除当前用户的微信union记录
    Integer deleteWechatUnionByCpId(Long cpId);

    // 更新当前用户的微信记录
    Integer updateWechatUnionIdByCpId(Long cpId);

    // 更新当前用户的微信记录
    Integer updateCustomerWechatByCpId(Long cpId);

    boolean selectWechatExistsByCpId(Long cpId);

    boolean selectWechatUnionExistsByCpId(Long cpId);

    Integer updateCustomerWechatByUnionId(String unionId);

    int countCustomerWechatByUnionId(String unionId);

}
