package com.xquark.dal.mapper;

import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.vo.PrivilegeCodeVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PrivilegeCodeMapper {

  int insert(PrivilegeCode rule);

  PrivilegeCode selectByPrimaryKey(@Param("id") String id);

  int modify(PrivilegeCode role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<PrivilegeCode> list(@Param(value = "pager") Pageable page,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);


  /**
   * 查找某个用户所有的特权码
   */
  List<PrivilegeCodeVO> selectByUserId(@Param(value = "userId") String userId);

  /**
   * 根据编码查询特权码
   */
  PrivilegeCode selectByCode(@Param("code") String code);

  /**
   * 返回一个用户可使用的特权码
   */
  PrivilegeCodeVO selectValidByUserId(@Param(value = "userId") String userId);

  /**
   * 将特权码的可用数量减1
   */
  void subQty(@Param("codeId") String codeId);

  /**
   * 将特权码的可用数量加1
   */
  void addQty(@Param("codeId") String codeId);

}
