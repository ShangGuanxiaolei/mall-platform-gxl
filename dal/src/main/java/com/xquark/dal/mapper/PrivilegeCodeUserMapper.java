package com.xquark.dal.mapper;

import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.model.PrivilegeCodeUser;
import com.xquark.dal.vo.PrivilegeCodeVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PrivilegeCodeUserMapper {

  int insert(PrivilegeCodeUser rule);

  PrivilegeCodeUser selectByPrimaryKey(@Param("id") String id);

  int modify(PrivilegeCodeUser role);

  int delete(@Param("id") String id);

  PrivilegeCodeUser selectByCode(@Param("code") String code);

}
