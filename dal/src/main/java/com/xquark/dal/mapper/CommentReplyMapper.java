package com.xquark.dal.mapper;

import com.xquark.dal.model.CommentReply;
import com.xquark.dal.model.CommentReplyExample;
import com.xquark.dal.vo.ReplyManagment;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CommentReplyMapper {

  long countByExample(CommentReplyExample example);

  int deleteByExample(CommentReplyExample example);

  int deleteByPrimaryKey(String id);

  int insert(CommentReply record);

  int insertSelective(CommentReply record);

  List<CommentReply> selectByExample(CommentReplyExample example);

  CommentReply selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") CommentReply record,
      @Param("example") CommentReplyExample example);

  int updateByExample(@Param("record") CommentReply record,
      @Param("example") CommentReplyExample example);

  int updateByPrimaryKeySelective(CommentReply record);

  int updateByPrimaryKey(CommentReply record);

  void blockReply(@Param("id") String id, @Param("isBlocked") int isBlocked);

  /*
   * 获取后台使用的回复数据
   * @param commentId
   * @return
   */
  public List<ReplyManagment> getReplysList(@Param("id") String commentId);

  /**
   * 获取后台使用的回复数据的条数
   */
  public Integer getReplysCount(@Param("id") String commentId);
}