package com.xquark.dal.mapper;

import com.xquark.dal.model.promotion.CouponCode;
import com.xquark.dal.model.promotion.CouponCodeStatus;

/**
 * @author: chenxi
 */

public interface CouponCodeMapper {

  public int insert(CouponCode code);

  public CouponCode selectByPrimaryKey(String id);

  public int updateCodeStatus(String id, CouponCodeStatus status);
}
