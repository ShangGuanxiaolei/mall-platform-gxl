package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestAnswer;
import com.xquark.dal.model.HealthTestAnswerExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestAnswerMapper {

  long countByExample(HealthTestAnswerExample example);

  int deleteByExample(HealthTestAnswerExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestAnswer record);

  int insertSelective(HealthTestAnswer record);

  List<HealthTestAnswer> selectByExample(HealthTestAnswerExample example);

  HealthTestAnswer selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestAnswer record,
      @Param("example") HealthTestAnswerExample example);

  int updateByExample(@Param("record") HealthTestAnswer record,
      @Param("example") HealthTestAnswerExample example);

  int updateByPrimaryKeySelective(HealthTestAnswer record);

  int updateByPrimaryKey(HealthTestAnswer record);

  // 分割线
  List<HealthTestAnswer> selectByQuestionId(@Param("questionId") String questionId,
      @Param("example") HealthTestAnswerExample example);

  Long countByQuestionId(@Param("questionId") String questionId);

  Integer countAlias(@Param("groupId") String groupId);
}