package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.type.PromotionType;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PromotionPgPriceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgPrice record);

    PromotionPgPrice selectByPrimaryKey(Integer id);

    PromotionPgPrice selectByPromotionIdAndSkuId(@Param("promotionId") String promotionId,
                                                 @Param("skuId") String skuId,
                                                 @Param("type") PromotionType type);

    List<PromotionPgPrice> listByPromotionIdAndProductId(@Param("promotionId") String promotionId,
                                                         @Param("productId") String productId, @Param("type") PromotionType type);

    int updateByPrimaryKeySelective(PromotionPgPrice record);

    int updateByPrimaryKey(PromotionPgPrice record);

    PromotionPgPrice selectPromotionPgPriceByDetailCode(String pDetailCode);

    BigDecimal selectPromotionPrice(String tranCode);
}