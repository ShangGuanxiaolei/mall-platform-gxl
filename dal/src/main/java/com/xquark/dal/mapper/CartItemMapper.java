package com.xquark.dal.mapper;

import java.util.List;

import com.xquark.dal.model.CartItem;
import org.apache.ibatis.annotations.Param;

public interface CartItemMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int deleteByUserId(String userId);

  int deleteBySku(String skuId);

  int insert(CartItem record);

  CartItem selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CartItem record);

  int updateByPrimaryKey(CartItem record);

  List<CartItem> selectByUserId(String userId);

  CartItem selectByUserIdAndSku(String userId, String skuId);

  CartItem selectByUserIdAndSkuShopId(String userId, String skuId, String shopId);

  List<CartItem> selectByUserIdAndShopId(String userId, String shopId);

  int deleteByUserIdAndSkuId(String userId, String skuId);

  int deleteByUserIdAndShopId(String id, String shopId);

  Integer selectCount(String userId);

  int deleteByUserIdAndId(String id, String userId);

  int updateCartUserId(String anonymouId, String siginId);

  /**
   * 获取用户在某个商铺下的购物车内商品总数
   */
  Integer countByShopId(@Param("userId") String userId, @Param("shopId") String shopId);
}