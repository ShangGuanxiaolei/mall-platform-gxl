package com.xquark.dal.mapper;

import com.xquark.dal.model.Supplier;
import com.xquark.dal.vo.SupplierVO;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * User: huangjie Date: 2018/6/23. Time: 下午1:16 基础Mapper
 */
public interface BaseMapper  <T>{

  /**
   * 添加
   */
  int insert(T obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<T> collection);

  /**
   * 通过id查找
   */
  T  getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(T obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<? extends T> list(@Param("params") Map<String, Object> params);

}