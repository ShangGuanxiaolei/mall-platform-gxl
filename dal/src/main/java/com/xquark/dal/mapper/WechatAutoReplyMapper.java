package com.xquark.dal.mapper;


import com.xquark.dal.model.wechat.AutoReply;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WechatAutoReplyMapper {

  List<AutoReply> list(@Param("shopId") String shopId);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param("shopId") String shopId);

  int updateByPrimaryKeySelective(AutoReply autoReply);

  int updateForArchive(@Param("id") String id);

  int updateForUnArchive(@Param("id") String id);

  int insert(AutoReply autoReply);

  AutoReply selectByPrimaryKey(@Param("id") String id);

  /**
   * 获取消息自动回复
   */
  AutoReply getAutoBack();

  /**
   * 获取被添加自动回复
   */
  AutoReply getAddBack();

  List<AutoReply> listAll(@Param("shopId") String shopId);

}

