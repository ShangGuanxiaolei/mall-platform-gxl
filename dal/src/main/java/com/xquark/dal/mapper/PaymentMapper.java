package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.Payment;
import com.xquark.dal.type.DeviceType;
import com.xquark.dal.vo.PaymentVO;

public interface PaymentMapper {

  int deleteByPrimaryKey(@Param("id") String id);

  int insert(Payment record);

  Payment selectByPrimaryKey(@Param("id") String id);

  List<PaymentVO> listByDeviceType(@Param("deviceType") DeviceType deviceType,
      @Param("domain") String... domain);

  int updateByPrimaryKeySelective(Payment record);

  List<Payment> list();

}