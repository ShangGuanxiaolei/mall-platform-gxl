package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionLotteryProbability;
import com.xquark.dal.vo.WinningProductVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionLotteryProbabilityMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionLotteryProbability record);

    PromotionLotteryProbability selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionLotteryProbability record);

    int updateByPrimaryKey(PromotionLotteryProbability record);

    /**
     * 获取当天的配置列表
     */
    List<PromotionLotteryProbability> listByPromotionLotteryId(@Param("promotionLotteryId") String promotionLotteryId,
                                                               @Param("type") Integer type);

    WinningProductVO selectWinningProduct(@Param("id") String lotteryId, @Param("prize") int prize);
    WinningProductVO selectWinningProduct2(@Param("skuCode") String skuCode);

}