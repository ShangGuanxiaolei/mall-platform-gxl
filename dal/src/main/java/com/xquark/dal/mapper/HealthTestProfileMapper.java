package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestProfile;
import com.xquark.dal.model.HealthTestProfileExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestProfileMapper {

  long countByExample(HealthTestProfileExample example);

  int deleteByExample(HealthTestProfileExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestProfile record);

  int insertSelective(HealthTestProfile record);

  List<HealthTestProfile> selectByExample(HealthTestProfileExample example);

  HealthTestProfile selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestProfile record,
      @Param("example") HealthTestProfileExample example);

  int updateByExample(@Param("record") HealthTestProfile record,
      @Param("example") HealthTestProfileExample example);

  int updateByPrimaryKeySelective(HealthTestProfile record);

  int updateByPrimaryKey(HealthTestProfile record);
}