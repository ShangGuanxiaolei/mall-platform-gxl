package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManAreatTabVo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:15:49
 * Des:新人页面商品tab
 */
public interface FreshManAreatTabMapper {
    List<FreshManAreatTabVo> selectFreshManAreatTabVoByStatus(int status);
}