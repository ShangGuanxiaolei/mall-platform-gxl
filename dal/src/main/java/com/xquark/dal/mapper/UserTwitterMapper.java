package com.xquark.dal.mapper;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.UserTwitterApplyVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserTwitterMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(UserTwitter record);

  int insertSelective(UserTwitter record);

  UserTwitter selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserTwitter record);

  int updateByPrimaryKey(UserTwitter record);

  List<UserTwitterApplyVO> selectUserTwitterApplyingByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  Long countUserTwitterApplyingByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  List<UserTwitter> selectByUserId(@Param("userId") String userId);

  UserTwitter selectByUserIdAndShopId(@Param("userId") String userId,
      @Param("shopId") String shopId);

  /**
   * 获取一周的推客交易额
   */
  List<Map> getAmount(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的推客订单
   */
  List<Map> getOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的新增推客
   */
  List<Map> getTwitter(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的推客佣金
   */
  List<Map> getCommission(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 推客结算记录列表
   */
  List<PartnerOrderCmVO> listCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  /**
   * 推客结算记录
   */
  Long countCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);


  List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  Long countChildrenCmByRootId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  List<User> getMyMembers(@Param("shopId") String shopId, @Param("page") Pageable pageable);

  int deleteByUserId(@Param(value = "userId") String userId);

}