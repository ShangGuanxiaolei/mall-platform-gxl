package com.xquark.dal.mapper;

import com.xquark.dal.model.TimeSettings;

public interface TimeSettingsMapper {

  int insert(TimeSettings record);

  int insertSelective(TimeSettings record);

  int update(TimeSettings record);

  TimeSettings selectByName(String name);

}