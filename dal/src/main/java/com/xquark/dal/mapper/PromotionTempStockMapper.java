package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionTempStock;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionTempStockMapper {

  int deleteByPrimaryKey(Long id);

  int insert(PromotionTempStock record);

  int insertSelective(PromotionTempStock record);

  PromotionTempStock selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(PromotionTempStock record);

  int updateByPrimaryKey(PromotionTempStock record);

  int updateStockNumByPCode(@Param("skuCode") String skuCode, @Param("amount") Integer amount);

  PromotionTempStock selectByPCodeAndSkuId(@Param("skuCode") String skuCode, @Param("pCode") String pCode);

  List<PromotionTempStock> listTempStockByPCode(@Param("pCode") String pCode);
}