package com.xquark.dal.mapper;

import com.xquark.dal.model.pintuanTask.ProStockDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityRecycleMapper {
    //查询sku编码,id为p_code
    List<String> selectSkuCode(String id);
    //查询sku编码对应剩余库存
    int selectSkuRestNum(String id);
    //更新活动库存表
    int updateSkuActivityStock(String id,String code);
    //更新活动库存扣减明细表
    int updateSkuStockDetail(ProStockDetail detail);
    //更新单品库存表
    int updateSkuXquarkStock(int num, String id);
}
