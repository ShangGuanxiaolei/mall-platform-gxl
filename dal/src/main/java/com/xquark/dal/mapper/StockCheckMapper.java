package com.xquark.dal.mapper;


import com.xquark.dal.model.mypiece.PromotionTempStock;
import com.xquark.dal.model.mypiece.PromotionTempStockVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StockCheckMapper {

  List<PromotionTempStockVo> findStock(PromotionTempStock promotionTempStock);

  boolean updateStock(PromotionTempStock promotionTempStock);

  boolean updateUsableStock(PromotionTempStock promotionTempStock);


  boolean updateStockList(@Param("tempStockList") List<PromotionTempStock> tempStockList);

}
