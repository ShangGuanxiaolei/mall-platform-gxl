package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargainRule;
import com.xquark.dal.type.CareerLevelType;

import java.util.List;

public interface PromotionBargainRuleMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionBargainRule record);

    PromotionBargainRule selectByPrimaryKey(String id);

    /**
     * 根据用户身份配置查询砍价规则
     * @param identity 用户的身份
     * @return 砍价规则列表
     */
    PromotionBargainRule selectByIdentity(CareerLevelType identity);

    /**
     * 查询所有配置的规则
     * @return 规则列表
     */
    List<PromotionBargainRule> listAll();

    int updateByPrimaryKeySelective(PromotionBargainRule record);

    int updateByPrimaryKey(PromotionBargainRule record);
}