package com.xquark.dal.mapper;

import com.xquark.dal.model.SCrmHttpLog;

public interface SCrmHttpLogMapper {

  int deleteByPrimaryKey(String id);

  int insert(SCrmHttpLog record);

  SCrmHttpLog selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(SCrmHttpLog record);

  int updateByPrimaryKey(SCrmHttpLog record);
}