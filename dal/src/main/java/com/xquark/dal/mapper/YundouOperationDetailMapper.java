package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouOperationDetail;
import com.xquark.dal.vo.YundouOperationDetailVO;
import com.xquark.dal.vo.YundouRuleDetailVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-5-11. DESC:
 */
public interface YundouOperationDetailMapper {

  Integer insert(YundouOperationDetail operationDetail);

  Integer delete(@Param("id") String id);

  Integer update(YundouOperationDetail operationDetail);

  YundouOperationDetail selectById(@Param("id") String id);

  List<YundouOperationDetail> selectAll(@Param("order") String order,
      @Param("page") Pageable pageable, @Param("direction") String direction);

  List<YundouOperationDetail> selectByUser(@Param("userId") String userId,
      @Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") String direction);

  List<YundouOperationDetail> selectByOperationAndBizId(@Param("operationCode") int code,
      @Param("bizId") String id);

  List<YundouOperationDetail> selectByUserAndDate(@Param("userId") String userId,
      @Param("from") Date from, @Param("to") Date to, @Param("order") String order,
      @Param("page") Pageable pageable, @Param("direction") String direction);

  List<YundouOperationDetail> selectLastWeekByUser(@Param("userId") String userId,
      @Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") String direction);

  List<YundouOperationDetail> selectLastMonthByUser(@Param("userId") String userId,
      @Param("order") String order,
      @Param("page") Pageable pageable, @Param("direction") String direction);

  List<YundouOperationDetail> selectLastYearByUser(@Param("userId") String userId,
      @Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") String direction);

  List<YundouOperationDetail> selectByOperationIdAndUser(String operationId, String userId,
      @Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") String direction);

  /**
   * 服务端分页查询数据
   */
  List<YundouOperationDetailVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, ?> params);

  /**
   * 得到总发放积分或总消耗积分
   */
  Long getTotalByDirection(@Param(value = "direction") String direction);

  // 最近一周总发放积分或总消耗积分
  List<Map> getWeekByDirection(@Param("direction") String direction, @Param("cdate") List cdate);

  /**
   * 检查是否针对某个操作执行过积分增减
   *
   * @param operationCode 操作代码
   * @param bizId 业务id
   * @return 执行过的次数
   */
  int hasDetail(@Param("operationCode") int operationCode, @Param("bizId") String bizId);

  List<YundouRuleDetailVO> listRuleDetailByUser(@Param("userId") String userId,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  Long countRuleDetailByUserId(String userId);

}
