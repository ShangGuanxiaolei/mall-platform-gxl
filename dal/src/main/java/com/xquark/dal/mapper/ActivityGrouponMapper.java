package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.PromotionGrouponVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ActivityGrouponMapper {

  ActivityGroupon selectByPrimaryKey(@Param("id") String id);

  ActivityGroupon selectUnique(@Param("userId") String userId,
      @Param("grouponId") String grouponId);

  int insert(ActivityGroupon activityGroupon);

  int updateStatus(@Param("id") String id, @Param("status") ActivityGrouponStatus status);

  /**
   * 通过订单找到对应的团购活动设置相关信息，如开团时间，开团人数等
   */
  PromotionGrouponVO findPromotionGrouponByOrderId(@Param("orderId") String orderId);

  /**
   * 通过订单统计对应的团购活动所有已支付的订单数
   */
  int countPaidOrderByOrderId(@Param("orderId") String orderId);

  /**
   * 通过id统计对应的团购活动所有已支付的订单数
   */
  int countPaidOrderById(String id);

  /**
   * 根据订单id查询团购活动表
   */
  ActivityGroupon findActivityGrouponByOrderId(@Param("orderId") String orderId);

  /**
   * 通过订单统计对应的团购活动所有指定状态的订单数
   */
  int countOrderByOrderIdAndStatus(@Param("orderId") String orderId,
      @Param("status") OrderStatus status);

  /**
   * 团购活动到了结束时间则自动更新未成团活动状态
   */
  int autoGrouponClose();

  /**
   * 根据订单id更新对应团购活动的库存，销量
   */
  int updateStock(@Param("orderId") String orderId, @Param("qty") int qty);

  /**
   * 优惠活动时间到了后自动结束
   */
  int autoPromotionClose();

  List<Long> listClosedPromotionId();

  /**
   * 获取所有未成团且已经付款的订单
   */
  List<String> needRefundOrders();

  /**
   * 得到该团购活动所有已经参与购买的买家id
   */
  List<String> getPaidUserByOrderId(@Param("orderId") String orderId);

  /**
   * 获取某个用户针对某个团购活动是否有未完成的活动
   */
  ActivityGroupon findActivityGrouponByUserIdAndGrouponId(@Param("grouponId") String grouponId,
      @Param("userId") String userId);

  /**
   * 结束拼团活动后，需要自动将该活动对应的未成团订单退款
   */
  List<String> needRefundOrdersByPromotionGrouponId(@Param("grouponId") String grouponId);

  /**
   * 得到该团购活动所有已经参与购买或退款的买家id
   */
  List<String> getPaidOrCloseUserByOrderId(@Param("orderId") String orderId);


}
