package com.xquark.dal.mapper;

import com.xquark.dal.type.ProductSource;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 下午8:04
 */
public class ThirdDetail {

  private final String thirdId;

  private final ProductSource source;

  private final String detailH5;

  public ThirdDetail(String thirdId, ProductSource source, String detailH5) {
    this.thirdId = thirdId;
    this.source = source;
    this.detailH5 = detailH5;
  }

  public String getThirdId() {
    return thirdId;
  }

  public ProductSource getSource() {
    return source;
  }

  public String getDetailH5() {
    return detailH5;
  }
}
