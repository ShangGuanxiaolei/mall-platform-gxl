package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.AppVersion;
import com.xquark.dal.type.OSType;

public interface AppVersionMapper {

  AppVersion findCurrentVersion(@Param("clientVersion") int userVersion,
      @Param("os") OSType osType);
}