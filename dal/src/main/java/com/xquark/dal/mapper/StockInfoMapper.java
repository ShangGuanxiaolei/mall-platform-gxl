package com.xquark.dal.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @auther liuwei
 * @date 2018/6/11 0:09
 */
public interface StockInfoMapper {

  /**
   * 添加
   */
  int insert(Stock obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<Stock> collection);

  /**
   * 更新
   */
  int update(Stock obj);
}
