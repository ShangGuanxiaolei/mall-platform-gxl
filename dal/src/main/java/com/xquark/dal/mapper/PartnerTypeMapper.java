package com.xquark.dal.mapper;

import com.xquark.dal.model.PartnerType;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PartnerTypeMapper {

  int deleteByPrimaryKey(String id);

  int insert(PartnerType record);

  int insertSelective(PartnerType record);

  PartnerType selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerType record);

  int updateByPrimaryKey(PartnerType record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<PartnerType> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查找所有设置的合伙人类型
   */
  List<PartnerType> listByShopId(@Param(value = "shopId") String shopId);

  /**
   * 获取某个合伙人的团队类型
   */
  PartnerType selectTeamByUserId(@Param(value = "userId") String userId);

}