package com.xquark.dal.mapper;

import com.xquark.dal.model.TwitterShopCommission;

import java.util.List;

public interface TwitterShopCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(TwitterShopCommission record);

  int insertSelective(TwitterShopCommission record);

  TwitterShopCommission selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TwitterShopCommission record);

  int updateByPrimaryKey(TwitterShopCommission record);

  int updateForArchive(String id);

  int updateDefaultCom(TwitterShopCommission record);

  TwitterShopCommission selectDefaultByshopId(String shopId);

  List<TwitterShopCommission> selectNonDefaultByshopId(String shopId);

  TwitterShopCommission selectDefault();

}