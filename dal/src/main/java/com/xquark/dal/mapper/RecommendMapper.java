package com.xquark.dal.mapper;

import com.xquark.dal.model.AccessReport;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.RecommendProductVO;
import com.xquark.dal.model.VipProduct;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface RecommendMapper {

  //查询推荐商品
	List<RecommendProductVO> getRecommendProduct(@Param("recommendCode")String recommendCode);

}