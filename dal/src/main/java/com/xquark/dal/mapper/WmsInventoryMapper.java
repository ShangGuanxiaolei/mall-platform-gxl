package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsInventory;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 上午10:51
 */
public interface WmsInventoryMapper {

  /**
   * 添加
   */
  int insert(WmsInventory obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsInventory> collection);

  /**
   * 通过id查找
   */
  WmsInventory getByPrimaryKey(@Param("id") String id);

  /**
   * 根据code跟ediStatus状态更新
   * 配合批量更新使用
   */
  int updateByCode(@Param("code") String code, @Param("status") int status);

  int updateBeforeDate(@Param("status") int status, @Param("code") String code, @Param("date") Date date);

  /**
   * 根据id批量更新
   */
  int updateByIds(@Param("ids") String ids, @Param("status") int status);

  /**
   * 更新
   */
  int update(WmsInventory obj);

  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsInventory> list(@Param("page") Pageable page,
      @Param("params") Map<String, Object> params);

  /**
   * 根据edi状态查询
   */
  List<WmsInventory> getByEdiStatus(@Param("ediStatus") Integer ediStatus, @Param("date") Date date);
}