package com.xquark.dal.mapper;

import com.xquark.dal.model.FeedbackTags;
import java.util.List;

public interface FeedbackTagsMapper {

  int deleteByPrimaryKey(String id);

  int deleteByFeedBackId(String feedBackId);

  int insert(FeedbackTags record);

  int insertMultiple(List<FeedbackTags> list);

  int insertSelective(FeedbackTags record);

  FeedbackTags selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(FeedbackTags record);

  int updateByPrimaryKey(FeedbackTags record);
}