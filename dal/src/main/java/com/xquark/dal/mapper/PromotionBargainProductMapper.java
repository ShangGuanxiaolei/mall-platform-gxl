package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargainProduct;
import com.xquark.dal.vo.PromotionBargainSku;
import com.xquark.dal.vo.PromotionBargainSkuVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionBargainProductMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionBargainProduct record);

  PromotionBargainProduct selectByPrimaryKey(String id);

  /**
   * 根据砍价活动id查询
   *
   * @param bargainId 砍价活动id
   * @param skuId 砍价skuId
   * @return 砍价商品对象
   */
  PromotionBargainProduct selectByBargainIdAndSkuId(
      @Param("bargainId") String bargainId, @Param("skuId") String skuId);

  /**
   * 根据砍价活动id查询
   *
   * @param bargainId 砍价活动id
   * @param skuId 砍价skuId
   * @return 砍价活动商品对象
   */
  PromotionBargainSku selectVOByBargainIdAndSkuId(
      @Param("bargainId") String bargainId, @Param("skuId") String skuId);

  int updateByPrimaryKeySelective(PromotionBargainProduct record);

  int updateByPrimaryKey(PromotionBargainProduct record);

  /**
   * 查询活动列表，价格显示sku最低价
   *
   * @param order 排序
   * @param direction 顺序
   * @param pageable 分页参数
   * @return 砍价商品列表
   */
  List<PromotionBargainSkuVO> listPromotion(
      @Param("order") String order,
      @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  /**
   * 查询活动列表总数
   *
   * @return 活动总数
   */
  Long countPromotion();
}
