package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManBannerVo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/6
 * Time:14:42
 * Des:
 */
public interface FreshManBannerMapper {
    List<FreshManBannerVo> selectBySortBanner(int status);

}