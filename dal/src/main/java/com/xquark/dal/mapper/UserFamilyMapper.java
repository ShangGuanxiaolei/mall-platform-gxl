package com.xquark.dal.mapper;

import com.xquark.dal.model.UserFamily;
import org.apache.ibatis.annotations.Param;

public interface UserFamilyMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(UserFamily record);

  int insertSelective(UserFamily record);

  UserFamily selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserFamily record);

  int updateByPrimaryKey(UserFamily record);
}