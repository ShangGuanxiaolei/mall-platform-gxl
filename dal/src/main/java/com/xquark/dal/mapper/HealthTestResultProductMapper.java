package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestResultProduct;
import com.xquark.dal.model.HealthTestResultProductExample;
import com.xquark.dal.model.Product;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface HealthTestResultProductMapper {

  long countByExample(HealthTestResultProductExample example);

  int deleteByExample(HealthTestResultProductExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestResultProduct record);

  int insertSelective(HealthTestResultProduct record);

  List<HealthTestResultProduct> selectByExample(HealthTestResultProductExample example);

  HealthTestResultProduct selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestResultProduct record,
      @Param("example") HealthTestResultProductExample example);

  int updateByExample(@Param("record") HealthTestResultProduct record,
      @Param("example") HealthTestResultProductExample example);

  int updateByPrimaryKeySelective(HealthTestResultProduct record);

  int updateByPrimaryKey(HealthTestResultProduct record);

  List<Product> listProductsByResultId(@Param("resultId") String resultId,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  Long countProductsByResultId(String resultId);
}