package com.xquark.dal.mapper;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.vo.PromotionDiamondProductVO;
import com.xquark.dal.vo.PromotionDiamondVO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;


public interface PromotionDiamondMapper {

  PromotionDiamondVO selectByPrimaryKey(@Param("id") String id);

  int insert(PromotionDiamond promotionDiamond);

  int modifyPromotionDiamond(PromotionDiamond promotionDiamond);

  PromotionDiamondVO selectByProductId(@Param("productId") String productId,
      @Param("date") Date date);

  int close(@Param("id") String id);

  /**
   * 列表查询限时抢购活动
   */
  List<Promotion> listPromotion(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页限时抢购活动列表接口取总数
   */
  Long selectPromotionCnt(@Param("keyword") String keyword);


  /**
   * 列表查询限时抢购活动关联商品
   */
  List<PromotionDiamondProductVO> listPromotionProduct(@Param(value = "pager") Pageable page,
      @Param("id") String id);

  /**
   * 对分页限时抢购活动关联商品列表接口取总数
   */
  Long selectPromotionProductCnt(@Param("id") String id);


  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  int delete(@Param("id") String id);

  /**
   * 查询某个商品在限时抢购活动中的个数
   */
  Long selectPromotionByProductId(@Param("promotionId") String promotionId, @Param("id") String id,
      @Param("productId") String productId);

  /**
   * 查找某个商品在其他有效限时抢购活动中的信息，用于判断这个商品有没有存在同一时段中不同活动中
   */
  List<PromotionDiamondVO> listExistPromotionProduct(@Param("promotionId") String promotionId,
      @Param("productId") String productId);

  PromotionDiamondVO selectPromotionProductInDiamond(@Param("id") String id);

  PromotionDiamondVO selectPromotionProductInUpcomeDiamond(@Param("id") String id);

  /**
   * app端列表查询钻石专享分类数据
   */
  List<PromotionDiamondProductVO> listDiamondApp(@Param("shopId") String shopId,
      @Param("categoryId") String categoryId, @Param("limit") int limit);


  /**
   * 根据promotionId查找对应的具体活动商品信息
   */
  ArrayList<PromotionDiamondProductVO> listByPromotionId(@Param("promotionId") String promotionId);

  /**
   * 根据promotionId查找对应的具体活动商品信息
   */
  ArrayList<PromotionDiamondProductVO> listByPromotionIdPage(
      @Param("promotionId") String promotionId, @Param("page") int page, @Param("size") int size);

}
