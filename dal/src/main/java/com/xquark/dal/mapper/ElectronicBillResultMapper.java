package com.xquark.dal.mapper;

import com.xquark.dal.model.ElectronicBillResult;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: huangjie
 * Date: 2018/5/23.
 * Time: 下午2:09
 * 电子发票开票结果
 */
public interface ElectronicBillResultMapper {

    /**
     * 添加
     */
    int insert(ElectronicBillResult obj);

    /**
     * 存储开票的失败记录
     */
    int insertFailBill(ElectronicBillResult obj);

    /**
     * 批量插入
     *
     * @param collection
     * @return
     */
    int batchInsert(@Param("collection") List<ElectronicBillResult> collection);

    /**
     * 通过id查找
     */
    ElectronicBillResult getByPrimaryKey(@Param("id") String id);

    /**
     * 更新
     */
    int update(ElectronicBillResult obj);


    /**
     * 通过id删除
     */
    int delete(@Param("id") String id);

    /**
     * 批量删除
     */
    int batchDelete(@Param("collection") List<String> collection);

    /**
     * 计算数据的数量
     */
    Long count(@Param("params") Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    List<ElectronicBillResult> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

}