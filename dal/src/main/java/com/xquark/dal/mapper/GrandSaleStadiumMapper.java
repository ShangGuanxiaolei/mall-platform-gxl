package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSaleStadium;

import java.util.List;
import java.util.Map;

public interface GrandSaleStadiumMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleStadium record);

    int insertSelective(GrandSaleStadium record);

    GrandSaleStadium selectByPrimaryKey(Integer id);

    List<GrandSaleStadium> selectByGrandSaleId(Integer grandSaleId);

    List<GrandSaleStadium> selectAll();

    GrandSaleStadium selectOne(Integer id);

    List<GrandSaleStadium> selectAllByGrandSaleId(Integer grandSaleId);

    String selectTitleByType(String type);

    GrandSaleStadium selectOneByType(String type);

    Map selectBannerByType(String stadiumType);

    int updateByPrimaryKeySelective(GrandSaleStadium record);

    int updateByPrimaryKey(GrandSaleStadium record);

    int updateByJob(Integer grandSaleId);
}