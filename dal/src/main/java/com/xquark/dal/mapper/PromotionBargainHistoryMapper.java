package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargainHistory;
import com.xquark.dal.vo.PromotionBargainHistoryVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.List;

public interface PromotionBargainHistoryMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionBargainHistory record);

    Boolean selectExistsByDetailIdAndUser(@Param("detailId") String detailId, @Param("userId") String userId);

    PromotionBargainHistory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionBargainHistory record);

    int updateByPrimaryKey(PromotionBargainHistory record);

    List<PromotionBargainHistoryVO> selectByUser(@Param("userId") String userId,
                                                 @Param("order") String order,
                                                 @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

    BigDecimal selectTotalByDetailId(String detailId);
}