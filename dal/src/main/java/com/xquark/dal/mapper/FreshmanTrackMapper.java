package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshmanTrack;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface FreshmanTrackMapper {

    List<FreshmanTrack> selectAllFreshmanTrack();

    FreshmanTrack selectFreshmanTrackByGrade(String grade);

    BigDecimal queryConsumedMoneyByCpid(Long currentCpid);

    Date queryIdentityActiveTime(Long currentCpid);

    Date queryFreshmanTrackStartTime();

    Integer queryIsVIPOder(Long currentCpid);

    Date queryVIPOrderPaidAtById(Integer orderId);

    Integer queryFreshmanFlagHaveVIPOrderId(Long currentCpid);

    Integer queryFreshmanFlagNotVIPOrderId(Long currentCpid, Integer consumedAmount);
}
