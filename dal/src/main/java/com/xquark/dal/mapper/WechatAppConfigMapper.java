package com.xquark.dal.mapper;


import com.xquark.dal.model.wechat.WechatAppConfig;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface WechatAppConfigMapper {

  List<WechatAppConfig> listByExpiredAt(@Param("profile") String profile,
      @Param("renewInterval") int renewInterval, @Param("now") Date now);

  int updateTokenByPrimaryKey(WechatAppConfig wechatAppConfig);

  WechatAppConfig selectByAppName(@Param("appName") String appName);

  WechatAppConfig selectByApiUrl(@Param("apiUrl") String apiUrl);

  int insert(@Param("appName") String appName, @Param("appId") String appId,
      @Param("appSecret") String appSecret,
      @Param("mchId") String mchId, @Param("mchKey") String mchKey,
      @Param("profile") String profile,
      @Param("shopId") String shopId, @Param("apiUrl") String apiUrl,
      @Param("apiVerifyToken") String apiVerifyToken, @Param("cert_password") String cert_password);

  int updateByPrimaryKeySelective(WechatAppConfig appConfig);

  WechatAppConfig selectByShopId(@Param("shopId") String shopId);

  List<WechatAppConfig> list(@Param("profile") String profile);

  List<WechatAppConfig> selectByUrlBegin(@Param("url") String url);

  WechatAppConfig selectByAppId(@Param("appId") String appId);

  WechatAppConfig selectByAppAppId(@Param("appId") String appId);

  // 没有微信配置记录时新增证书
  int insertCert(Map map);

  // 有微信配置记录时更新证书
  int updateCert(Map map);

  // 没有微信APP配置记录时新增证书
  int insertAppCert(Map map);

  // 有微信APP配置记录时更新证书
  int updateAppCert(Map map);

  WechatAppConfig selectByProfile(@Param("profile") String profile);

}

