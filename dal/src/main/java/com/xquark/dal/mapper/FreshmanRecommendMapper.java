package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManOrderData;
import com.xquark.dal.model.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * 新人专区商品推荐mapper
 *
 * @author  tanggb
 * @date 2019/03/09
 */
public interface FreshmanRecommendMapper {

    //获取用户累计消费金额
    List<FreshManOrderData> countUserSpending(Long userId);

    List<Product> findFreshmanProduct(BigDecimal price);

    Integer countByStrongPush();
    List<Product> findFreshmanProductByStrongPush(BigDecimal price);
    Integer findVipType(Long cpId);

    Integer selectFreshmanOrder(Long cpId);
}