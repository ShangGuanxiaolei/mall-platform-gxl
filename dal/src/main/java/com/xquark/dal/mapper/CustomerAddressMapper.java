package com.xquark.dal.mapper;


import com.xquark.dal.model.CustomerAddress;
import org.apache.ibatis.annotations.Param;


import java.util.List;
import java.util.Map;


public interface CustomerAddressMapper {
    int insert(CustomerAddress record);  //添加

    CustomerAddress selectById(String id); //根据主键id查找

    boolean isExists(String id); //根据id查找知否存在

    CustomerAddress selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CustomerAddress customerAddress);

    int updateByExtIdSelective(CustomerAddress record);

    CustomerAddress selectUserAddress(String userId, String addressId);  //orderinguser_id 查找  未实现

    List<CustomerAddress> selectUserAddresses(String userId);

    int updateForArchive(String addressId);

    int updateForUnArchive(String addressId);

    CustomerAddress selectOrderAddress(String orderId);

    CustomerAddress selectLatestUserAddresses(String userId);

    void updateForUnCommon(String id);

    int setDefault(@Param("userId") String addressid, @Param("id") String addressId);

    int setDefaultByPrimaryKey(@Param("id") String id);

    void emptyDefault(String orderinguserId);

    List<CustomerAddress> getDefault(String userId);

    /**
     * 以下sql是批量修改默认地址
     */
    List<Map<String, Object>> getTempResult();

    void updatempDefault(@Param("map") Map<String, Object> map);

    /**
     * consignee，phone,zoneId,street信息这些信息是否有存在的address
     */
    CustomerAddress selectByInfo(@Param("userId") String userId, @Param("consignee") String consignee,
                                 @Param("phone") String phone, @Param("zoneId") String zoneId, @Param("street") String street);

    int updateExtIdByPrimaryKey(@Param("id") String id, @Param("extId") Integer extId);

    Integer selectExtIdByPrimaryKey(String id);


}