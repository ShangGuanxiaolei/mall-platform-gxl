package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.ShopTreeKey;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.TwitterChildrenCmVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ShopTreeMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int deleteByDescendant(@Param(value = "shopId") String shopId);

  int insert(ShopTree record);

  int insertSelective(ShopTree record);

  ShopTree selectByPrimaryKey(ShopTreeKey key);

  int updateByPrimaryKeySelective(ShopTree record);

  int updateByPrimaryKey(ShopTree record);

  int insertShopTree(ShopTree record);

  int insertShopTreeDepth(ShopTree record);

  ShopTree selectRootShopByShopId(String shopId);

  List<ShopTree> selectChildren(String shopId);

  List<ShopTree> getParents(String shopId);

  Long selectParentUserIdByShopId(@Param("shopId") String shopId,
      @Param("shopDepth") Integer shopDepth);

  List<ShopTree> selectFamilyShopTreeByShopId(String shopId);

  Long countDirectChildren(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  List<ShopTree> listDirectChildren(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId, @Param("page") Pageable pageable);

  Long countChildren(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  List<ShopTree> listChildren(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  List<ShopTree> listChildrenPage(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId, @Param("page") Pageable pageable);

  List<ShopTree> listChildrenIncludingItself(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  List<ShopTree> listParentIncludingItself(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  List<ShopTree> listParentIncludingItselfDepth(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId, @Param("depth") int depth);

  List<TwitterChildrenCmVO> selectChildrenCmByRootIdAndAccountType(@Param("shopId") String shopId,
      @Param("accountType") AccountType accountType);

  List<TwitterChildrenCmDisplayVO> selectChildrenCmByRootId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  Long countChildrenCmByRootId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  List<ShopTree> listDirectChildren4Admin(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  List<ShopTree> listChildren4Admin(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  int updateParentShopDepth(@Param("rootShopId") String rootShopId,
      @Param("newParentShopId") String newParentShopId, @Param("shopId") String shopId);

  int updateParentShop(@Param("rootShopId") String rootShopId,
      @Param("newParentShopId") String newParentShopId, @Param("shopId") String shopId);

  ShopTree selectDirectParent(@Param("rootShopId") String rootShopId,
      @Param("shopId") String shopId);

  List<ShopTree> listChildrenExceptShopList(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId, @Param("shopIds") List shopIds);

  List<ShopTree> listRootShopTree();

  /**
   * b2b代理用户树状图显示
   */
  List<ShopTree> listDirectChildrenAgent(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId, @Param("page") Pageable pageable);

  /**
   * b2b代理用户树状图显示
   */
  Long countChildrenAgent(@Param("rootShopId") String rootShopId,
      @Param("currentShopId") String currentShopId);

  int trueDeleteByPrimaryKey(@Param(value = "id") String id);


  /**
   * 根据手机号或名称查询该shop的级层关系
   */
  List<ShopTree> listByPhoneOrName(@Param("rootShopId") String rootShopId,
      @Param("keys") String keys);

  /**
   * 获取某个用户的上级第一个合伙人用户(包含他自己)
   */
  ShopTree getDirectPartner(@Param("rootShopId") String rootShopId, @Param("shopId") String shopId);

  /**
   * 获取总店店铺
   */
  ShopTree getRootShop();

  int deleteByUserId(@Param(value = "userId") String userId);

}