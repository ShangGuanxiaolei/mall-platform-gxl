package com.xquark.dal.mapper;

import com.xquark.dal.model.AuditRule;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AuditRuleMapper {

  int insert(AuditRule rule);

  AuditRule selectByPrimaryKey(@Param("id") String id);

  int modify(AuditRule role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<AuditRule> list(@Param(value = "pager") Pageable page, @Param("keyword") String keyword,
      @Param("type") String type);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword, @Param("type") String type);

  /**
   * 根据订单id查询该订单的审核类型
   */
  AuditRule selectByOrderId(@Param("orderId") String orderId);

  /**
   * 判断根据买家，卖家类型查询是否已经存在记录
   */
  AuditRule selectByBuyerAndSeller(@Param("buyer") AgentType buyer,
      @Param("seller") AgentType seller, @Param("id") String id, @Param("type") AuditRuleType type);

  /**
   * 代理审核时根据申请类型与上级类型查找审核规则
   */
  AuditRule selectByAgentType(@Param("type") AgentType type,
      @Param("parentType") AgentType parentType);

}
