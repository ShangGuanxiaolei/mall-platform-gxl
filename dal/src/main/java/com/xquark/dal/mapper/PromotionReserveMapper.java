package com.xquark.dal.mapper;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.PromotionReserve;
import com.xquark.dal.model.PromotionReserveOrder;
import com.xquark.dal.model.PromotionReserveOrderItem;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author: yyc
 * @date: 19-4-17 下午3:43
 */
public interface PromotionReserveMapper {

  /**
   * 根据活动id和商品id查询活动商品
   *
   * @param promotionId String
   * @param productId String
   * @param skuId String
   * @return PromotionReserveProductVO
   */
  PromotionReserveProductVO selectVOByPidAndProductId(
      @Param("promotionId") String promotionId,
      @Param("productId") String productId,
      @Param("skuId") String skuId);

  /**
   * 分页查询预售活动
   *
   * @param pageable Pageable
   * @return List
   */
  List<ReserveVO> pageActivities(@Param("page") Pageable pageable, @Param("promotionId") String promotionId);

  /**
   * 根据活动名称查询对应的商品
   *
   * @param params ImmutableMap
   * @return List
   */
  List<ReserveProductVO> selectProductVOs(@Param("params") ImmutableMap<String, Object> params);

  ReserveProductVO selectByPromotionIdAndSkuId(@Param("promotionId") String promotionId, @Param("skuId") String skuId);

  /**
   * 更新总预约数量
   *
   * @param id String
   * @param reserveCount int
   * @return int
   */
  int updateReserveCount(
      @Param("id") String id,
      @Param("reserveCount") int reserveCount,
      @Param("version") int version);

  int updateReserveAmt(@Param("id") String id, @Param("amt") int amt);

  /**
   * 查询用户已预约的数量
   *
   * @param userId String
   * @return Integer
   */
  Integer selectUserBuyCount(
      @Param("userId") String userId,
      @Param("promotionId") String promotionId,
      @Param("productId") String productId);

  /**
   * 保存预约单
   *
   * @param order PromotionReserveOrder
   * @return int
   */
  int insertReserveOrder(PromotionReserveOrder order);

    /**
     * 更新预购订单状态
     * @param promotionId 活动id
     * @param status 状态
     * @param orgiStatus 原状态
     * @return 更新结果
     */
  int updateStatusByPromotionId(@Param("promotionId") String promotionId, @Param("status") String status,
                                @Param("orgiStatus") String orgiStatus);

  /**
   * 保存预约子订单
   *
   * @param orderItem PromotionReserveOrderItem
   * @return int
   */
  int insertReserveOrderItem(PromotionReserveOrderItem orderItem);

  /**
   * 根据参数查询对应的预约单
   *
   * @param params ImmutableMap
   * @return List
   */
  List<PromotionReserveOrderVO> selectReserveOrders(
      @Param("params") ImmutableMap<String, Object> params);

  /**
   * 根据预约单id查询预约子订单
   *
   * @param orderId String
   * @return List
   */
  List<PromotionReserveOrderItemVO> selectReserveOrderItems(@Param("orderId") String orderId);

  /**
   * 统计预约单状态的数量
   *
   * @param userId String
   * @param orderStatus OrderStatus
   * @return Integer
   */
  Integer selectReserveOrderCount(
      @Param("userId") String userId, @Param("orderStatus") OrderStatus orderStatus);

  /**
   * 更新活动库存
   *
   * @param reserveProductVO ReserveProductVO
   * @return int
   */
  int updatePayAmount(ReserveProductVO reserveProductVO);

  int updateAmtByPromotionAndSkuId(
          @Param("promotionId") String promotionId,
          @Param("skuId") String skuId, @Param("amt") int amt);



  /**
   * 更新预约单状态
   *
   * @param preOrderNo String
   * @param status OrderStatus
   * @return int
   */
  int updateReserveOrderStatus(
      @Param("preOrderNo") String preOrderNo, @Param("status") OrderStatus status);

  int updateReserveOrderStatusToPaid(
          @Param("preOrderNo") String preOrderNo);

  /**
   * 根据id查询预约活动
   *
   * @param promotionId String
   * @return PromotionReserve
   */
  PromotionReserve selectReserveById(@Param("id") String promotionId);

  //如果当前时间到达了预约开始时间，就改变预约的状态
  int updateReserveStartStatus();

  //　如果当前时间到了支付结束时间，就改变预约状态为结束状态
  int updateReserveEndStatus();

    /**
     * 查询所有已过期的预约活动
     */
    List<Long> selectExpiredPromotionId();

    /**
     * 通过id查询库存
     */
    List<ReserveAmount> selectPromotionReserveById(String promotionId);

    OrderStatus selectOrderStatus(@Param("preOrderNo") String preOrderNo);

    OrderStatus selectReserveOrderStatus(@Param("preOrderNo") String preOrderNo);

    Long selectPromotionIdByPreOrderNo(@Param("preOrderNo") String preOrderNo);

    PromotionPgPrice loadPromotionPgPrice(@Param("promotionId") String promotionId,
                                       @Param("skuId") String skuId);

}
