package com.xquark.dal.mapper;


import com.xquark.dal.vo.XquarkProduct;
import com.xquark.dal.vo.XquarkProductExample;
import com.xquark.dal.vo.XquarkProductWithBLOBs;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface XquarkProductMapper {
    long countByExample1(XquarkProductExample example);

    int deleteByExample1(XquarkProductExample example);

    int insert1(XquarkProductWithBLOBs record);

    int insertSelective1(XquarkProductWithBLOBs record);

    List<XquarkProductWithBLOBs> selectByExampleWithBLOBs1(XquarkProductExample example);

    List<XquarkProduct> selectByExample1(XquarkProductExample example);

    int updateByExampleSelective1(@Param("record") XquarkProductWithBLOBs record, @Param("example") XquarkProductExample example);

    int updateByExampleWithBLOBs1(@Param("record") XquarkProductWithBLOBs record, @Param("example") XquarkProductExample example);

    int updateByExample(@Param("record") XquarkProduct record, @Param("example") XquarkProductExample example);

    Integer updateDeliveryRegionByid(@Param("id")Integer id ,@Param("provinceid") String provinceid);

    XquarkProduct queryDeliveryByid(@Param("code")String code);

    List<Map<String,String>> queryRegionById ();

    Integer queryParentId(@Param("regionid") Integer regionid);

    List<String> querySfByCoid(@Param("code") String code);

    String getParent(@Param("provincename") String provincename);
}