package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author LiHaoYang
 * @date 2018/9/6 0006
 */

public interface PromotionWhitelistMapper {

    /**
     * 活动已开始商品查询
     */
    Integer checkIfInWhiteList(@Param("cpId")Long cpId);


    Integer checkWhitelistValidation(@Param("pCode")String pCode, @Param("cpId")Long cpId);

    /**
     * 校验是否在白名单中
     * @param cpId
     * @return
     */
    boolean checkIsWhiteExist(@Param("pCode")String pCode, @Param("cpId")Long cpId);

    /**
     * 大会是否开启
     * @param pCode
     * @param cpId
     * @return
     */
    boolean checkMettingIsOpen(@Param("pCode")String pCode, @Param("cpId")Long cpId);
}
