package com.xquark.dal.mapper;

import com.xquark.dal.model.OutPay;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface OutpayMapper {

  int deleteByPrimaryKey(String id);

  int insert(OutPay record);

  int insertSelective(OutPay record);

  OutPay selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(OutPay record);

  int updateByPrimaryKeyWithBLOBs(OutPay record);

  int updateByPrimaryKey(OutPay record);

  List<OutPay> selectByUserId(String userId);

  List<OutPay> selectByBillNO(String billNO);

  List<OutPay> selectByTradeNo(String tradeNo);

  List<OutPay> selectByAccountName(String name);

  int finishOutPay(OutPay record);

  OutPay findByOrderNo4Refund(@Param(value = "payNo") String payNo,
      @Param(value = "payType") String payType);

  OutPay findOutPayByTradeNo(@Param(value = "tradeNo") String tradeNo,
      @Param(value = "payType") String payType);

  OutPay findOutPayByBillNo(@Param(value = "billNo") String billNo,
      @Param(value = "payType") String payType);

  List<OutPay> selectRecords(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  Long countRecords(@Param(value = "params") Map<String, Object> params);

    OutPay queryOutPayBybillNo(@Param(value = "bizNo") String bizNo);

    Long countByTradeNo(String tradeNo);
}