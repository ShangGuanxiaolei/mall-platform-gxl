package com.xquark.dal.mapper;

import com.xquark.dal.model.PayNoLog;

public interface PayNoLogMapper {
    int deleteByPrimaryKey(String id);

    int insert(PayNoLog record);
    
    int insertBeforeCheck(PayNoLog record);

    PayNoLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PayNoLog record);

    int updateByPrimaryKey(PayNoLog record);
}