package com.xquark.dal.mapper;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionGroupon;
import com.xquark.dal.vo.PromotionGrouponProductVO;
import com.xquark.dal.vo.PromotionGrouponUserVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;


public interface PromotionGrouponMapper {

  PromotionGrouponVO selectByPrimaryKey(@Param("id") String id);

  int insert(PromotionGroupon promotionGroupon);

  int modifyPromotionGroupon(PromotionGroupon promotionGroupon);

  PromotionGrouponVO selectByProductId(@Param("productId") String productId);

  PromotionGrouponVO selectInPromotion(@Param("productId") String productId,
      @Param("promotionId") String promotionId);

  int close(@Param("id") String id);

  /**
   * 列表查询限时抢购活动
   */
  List<Promotion> listPromotion(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页限时抢购活动列表接口取总数
   */
  Long selectPromotionCnt(@Param("keyword") String keyword);


  /**
   * 列表查询限时抢购活动关联商品
   */
  List<PromotionGrouponProductVO> listPromotionProduct(@Param(value = "pager") Pageable page,
      @Param("id") String id);

  /**
   * 对分页限时抢购活动关联商品列表接口取总数
   */
  Long selectPromotionProductCnt(@Param("id") String id);

  /**
   * 修改活动
   */
  int modifyPromotion(PromotionGrouponVO promotionGroupon);

  /**
   * 根据团购状态查询某个用户参与的团购活动
   */
  List<PromotionGrouponUserVO> listUserPromotion(@Param("userId") String userId,
      @Param("status") String status, @Param(value = "pager") Pageable page);

  /**
   * 根据promotionId查找对应的具体活动商品信息
   */
  ArrayList<PromotionGrouponProductVO> listByPromotionId(@Param("promotionId") String promotionId,
      @Param("page") int page, @Param("size") int size);

  /**
   * 将id对应的xquark_promotion_Diamond记录archive设置为true
   */
  int delete(@Param("id") String id);

}
