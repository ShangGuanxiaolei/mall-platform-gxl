package com.xquark.dal.mapper;

import com.xquark.dal.model.UserSigninLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserSigninLogMapper {

  int insert(UserSigninLog userSigninLog);

  List<Long> selectUserSignInLog(@Param(value = "userId") String userId);
}
