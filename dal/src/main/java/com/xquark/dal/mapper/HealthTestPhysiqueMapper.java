package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestPhysique;
import com.xquark.dal.model.HealthTestPhysiqueExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestPhysiqueMapper {

  long countByExample(HealthTestPhysiqueExample example);

  int deleteByExample(HealthTestPhysiqueExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestPhysique record);

  int insertSelective(HealthTestPhysique record);

  List<HealthTestPhysique> selectByExampleWithBLOBs(HealthTestPhysiqueExample example);

  List<HealthTestPhysique> selectByExample(HealthTestPhysiqueExample example);

  HealthTestPhysique selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestPhysique record,
      @Param("example") HealthTestPhysiqueExample example);

  int updateByExampleWithBLOBs(@Param("record") HealthTestPhysique record,
      @Param("example") HealthTestPhysiqueExample example);

  int updateByExample(@Param("record") HealthTestPhysique record,
      @Param("example") HealthTestPhysiqueExample example);

  int updateByPrimaryKeySelective(HealthTestPhysique record);

  int updateByPrimaryKeyWithBLOBs(HealthTestPhysique record);

  int updateByPrimaryKey(HealthTestPhysique record);
}