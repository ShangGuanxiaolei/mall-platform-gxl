package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionTitle;
import com.xquark.dal.type.PromotionType;
import org.apache.ibatis.annotations.Param;

public interface PromotionTitleMapper {

  PromotionTitle selectByPrimaryKey(String id);

  int insert(PromotionTitle promotionTitle);

  int update(PromotionTitle promotionTitle);

  int delete(String id);

  PromotionTitle selectByType(@Param("promotionType") PromotionType promotionType);

}
