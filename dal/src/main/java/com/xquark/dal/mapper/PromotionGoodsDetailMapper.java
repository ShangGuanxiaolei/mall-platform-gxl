package com.xquark.dal.mapper;

import com.xquark.dal.model.Sku;
import com.xquark.dal.vo.PromotionGoodsDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionGoodsDetailMapper
 * @date 2018/9/7 0007
 */
public interface PromotionGoodsDetailMapper {

  /**
   * 查询人数和优惠价
   */
  List<PromotionGoodsDetailVO> selectPeopleAndDiscountPrice(@Param("pCode") String pCode,
      @Param("tranCode") String tranCode);

  /**
   * 查询剩余时间和最高拼团价格
   */
  PromotionGoodsDetailVO selectPromotionTimeAndMaxDiscount(@Param("productCode") String productCode,
                                                           @Param("pCode") String pCode);

  List<Sku> selectSku(@Param("code") String code);

  Integer selectPromotionPriceLimit(String pDetailCode);
}
