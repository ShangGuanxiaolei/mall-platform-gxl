package com.xquark.dal.mapper;

import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.PartnerSetting;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartnerSettingMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(PartnerSetting record);

  int insertSelective(PartnerSetting record);

  PartnerSetting selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerSetting record);

  int updateByPrimaryKey(PartnerSetting record);

  PartnerSetting selectByShopId(String shopId);
}