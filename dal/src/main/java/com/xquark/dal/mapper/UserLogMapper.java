package com.xquark.dal.mapper;

import com.xquark.dal.model.UserLogVo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/4/21
 * Time:15:25
 * Des:用户日志
 */
public interface UserLogMapper {
    boolean insert(UserLogVo userLogVo);
}