package com.xquark.dal.mapper;

import com.xquark.dal.model.JobSchedulerLog;
import com.xquark.dal.vo.JobFileVO;

public interface JobSchedulerLogMapper {

  int deleteByPrimaryKey(Long jobId);

  int insert(JobSchedulerLog record);

  int insertSelective(JobSchedulerLog record);

  JobSchedulerLog selectByPrimaryKey(Long jobId);

  JobFileVO selectJobFileVO(Long jobId);

  int updateByPrimaryKeySelective(JobSchedulerLog record);

  int updateByPrimaryKey(JobSchedulerLog record);
}