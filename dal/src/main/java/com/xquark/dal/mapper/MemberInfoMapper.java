package com.xquark.dal.mapper;

import com.xquark.dal.model.MemberTypeAndNickName;
import org.apache.ibatis.annotations.Param;

public interface MemberInfoMapper {


    /**
     * 根据cpid查询上级的cpid
     * @param cpId
     * @return
     */
    Long selectShareCpid(@Param("cpId")Long cpId);


    /**
     * 根据cpid查询强关系的cpid
     * @param cpId
     * @return
     */
    Long selectStrogeCpId(@Param("cpId")Long cpId);

    /**
     * 根据cpid查询最新次强关系的cpid
     * @param cpId
     * @return
     */
    Long selectSubStrogeCpId(@Param("cpId")Long cpId);


    /**
     * 根据cpId查询最新绑定次强关系的cpid
     * @param cpId
     * @return
     */
    Long selectToCpId(@Param("cpId")Long cpId);

    /**
     * 根据cpid查询用户的昵称、身份
     * @param cpId
     * @return
     */
    MemberTypeAndNickName selectMemberInfo(@Param("cpId")Long cpId);



    /**
     * 修改用户昵称
     * @param nickName
     * @param cpId
     * @return
     */
    boolean updateNickName(@Param("nickName")String nickName,@Param("cpId")Long cpId);



    /**
     * 查询customer中的用户昵称
     * @param cpId
     * @return
     */
    String selectNickName(@Param("cpId")Long cpId);


    /**
     * 查询user表中的nickName是否为null
     * @param cpId
     * @return
     */
    String selectUserNickNameIsNull(@Param("cpId")Long cpId);


    /**
     * 查询调用安畅接口是否为开启状态
     * @return
     */
    String selectSystemConfig();

    /**查询sku编码
     *
     */

    String selectSkuSystemConfig();

    /**查询sku可推WMS库存
     *
     */

    String selectAmountSystemConfig();

    /**查询sku开始下单时间
     *
     */

    String selectStartTimeSystemConfig();

    /**查询sku下单结束时间
     *
     */

    String selectEndTimeSystemConfig();
}
