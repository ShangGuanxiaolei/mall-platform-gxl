package com.xquark.dal.mapper;


import com.xquark.dal.model.CustomerQuestion;
import com.xquark.dal.model.QuestionTypeVo;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName CustomerQuestionMapper
 * @date 2019/1/14 0014
 */
public interface CustomerQuestionMapper {

    /**
     * 查询问题类型
     * @return
     */
    List<QuestionTypeVo> selectQueType();


    /**
     * 查询当前类型下的问题详情
     * @param typeId
     * @return
     */
    List<CustomerQuestion> selectQuestionAll(@Param("typeId") String typeId);



    /**
     * 根据问题详情id查询问题内容
     * @param id
     * @return
     */
    String selectContentById(@Param("id")String id);



    /**
     * 修改有帮助、无帮助
     * @param id
     * @return
     */
    int updateHelpful(@Param("id")int id);

    /**
     * 修改有帮助、无帮助
     * @param id
     * @return
     */
    int updateUnHelpful(@Param("id")int id);

}
