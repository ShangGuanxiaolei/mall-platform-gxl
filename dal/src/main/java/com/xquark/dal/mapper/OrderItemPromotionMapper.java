package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderItemPromotion;
import org.apache.ibatis.annotations.Param;


public interface OrderItemPromotionMapper {

  OrderItemPromotion selectByPrimaryKey(@Param("id") String id);

  int insert(OrderItemPromotion orderItemPromotion);

  OrderItemPromotion selectByOrderItemId(@Param("orderItemId") String orderItemId);

  int delete(@Param("id") String id);

  int deleteByOrderItemIdAndPromotionId(@Param("promotionId") String promotionId,
      @Param("orderItemId") String orderItemId);

}
