package com.xquark.dal.mapper;

import com.xquark.dal.model.FanCardApply;
import com.xquark.dal.type.FanCardApplyStatus;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public interface FanCardApplyMapper {

  int deleteByPrimaryKey(String id);

  int insert(FanCardApply record);

  int insertSelective(FanCardApply record);

  FanCardApply selectByPrimaryKey(String id);

  FanCardApply selectByUserId(String userId);

  int updateByPrimaryKeySelective(FanCardApply record);

  int updateByPrimaryKey(FanCardApply record);

  boolean checkCodeExist(String code);

  int updateStatus(@Param("UserId") String userId, @Param("status") FanCardApplyStatus status);

  List<FanCardApply> list(@Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);
}