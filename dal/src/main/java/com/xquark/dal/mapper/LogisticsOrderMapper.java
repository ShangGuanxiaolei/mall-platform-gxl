package com.xquark.dal.mapper;

import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * author: LiuWei Date: 18-9-19. Time: 上午11:36
 */
public interface LogisticsOrderMapper {

  /**
   * 根据状态查询订单数量
   */
  String getNumTotalOrder(@Param("status") String status,
                          @Param("code") String code,
                          @Param("startTime")Date startTime,
                          @Param("endTime")Date endTime);

  /**
   * 查询所有订单数量
   */
  String getNumTotalAllOrder(@Param("code") String code);

  /**
   * 根据订单状态查询订单
   */
  List<PolyapiOrderItemVO> getOrderItem(@Param("pageIndex") Integer pageIndex,
      @Param("pageSize") Integer pageSize, @Param("startTime") Date startTime,
      @Param("endTime") Date endTime,@Param("orderStatus") String orderStatus,
      @Param("code") String code);

  Long countOrderItem(@Param("startTime") Date startTime,
      @Param("endTime") Date endTime,@Param("orderStatus") String orderStatus,
      @Param("code") String code);

  /**
   * 查询订单
   */
  List<PolyapiOrderItemVO> getAllOrderItem(@Param("pageIndex") Integer pageIndex,
      @Param("pageSize") Integer pageSize,
      @Param("startTime") Date startTime,
      @Param("endTime") Date endTime,
      @Param("code") String code);

  /**
   * 根据订单号查找商品
   */
  List<PolyapiGoodInfoVO> getGoodInfoByOrderNo(@Param("orderNo") String orderNo,
      @Param("code") String code);

}
