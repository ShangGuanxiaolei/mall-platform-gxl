package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopAccessLog;
import org.apache.ibatis.annotations.Param;

public interface ShopAccessLogMapper {

  int deleteByPrimaryKey(String id);

  int insert(ShopAccessLog record);

  int insertSelective(ShopAccessLog record);

  ShopAccessLog selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ShopAccessLog record);

  int updateByPrimaryKey(ShopAccessLog record);

  int updateForArchive(String id);

  /**
   * 统计某个店铺的总访问量
   */
  Long countByShopId(@Param(value = "shopId") String shopId);

  /**
   * 获取用户最后一次访问的店铺
   */
  ShopAccessLog getLastShop(@Param(value = "userId") String userId);

  /**
   * 获取用户第一次访问的店铺
   */
  ShopAccessLog getFirstShop(@Param(value = "userId") String userId);

  /**
   * 将店铺访问记录表中的该店铺信息都删掉
   */
  void deleteByShopId(@Param(value = "shopId") String shopId);

}