package com.xquark.dal.mapper;

import com.xquark.dal.model.*;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.vo.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ProductMapper {

  int insert(Product record);

  int insertDistribution(Product recourd);

  boolean exists(String id);

  Product selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Product record);

  int updateCopyProductByPrimaryKey(Product record);

  int updateAmountAndSales(String id, Integer amount);

  int updateAmountForRefund(String id, Integer amount);

  int updateForArchive(String id);

  int updateForUnArchive(String id);

  int updateReviewStatus(@Param("id") String id, @Param("status") ProductReviewStatus status);

  int updateForInstock(String id);

  int updateForOnsale(String id);

  int updateForForsale(String id, Date forsaleAt);

  int unlockProductByShopId(@Param(value = "shopId") String shopId);

  int lockProductByShopId(@Param(value = "shopId") String shopId);

  int unlockProductByIdsRevs(@Param("ids") String[] ids, @Param(value = "shopId") String shopId);

  int unlockProductByIds(@Param("ids") String[] ids);

  List<ProductBasicVO> listProductWithParams(@Param("page") Pageable pageable,
      @Param("params") Map<String, Object> params);

  Long countWithParams(@Param("params") Map<String, Object> params);

  List<Product> listAllActivityProdsByShop(@Param(value = "shopId") String shopId);

  /**
   * 获取DNA基因产品唯一code
   */
  List<String> getDNAProductCode();

  /**
   * 同步列表
   */
  List<Product> listProducts4Sync(@Param(value = "shopId") String shopId,
      @Param(value = "page") Pageable page);


  /**
   * 上架列表
   */
  List<Product> listProductsByOnsaleAt(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category, @Param(value = "is_groupon") String isGroupon,
      @Param(value = "page") Pageable page, @Param(value = "params") Map<String, Object> params);

  /**
   * 销量列表
   */
  List<Product> listProductsBySales(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "direction") Direction direction,
      @Param(value = "sellerShopId") String sellerShopId,
      @Param(value = "subCategoryIds") List<String> subCategoryIds,
      @Param(value = "priceStart") String priceStart,
      @Param(value = "priceEnd") String priceEnd,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus,
      @Param(value = "selfOperated") Integer selfOperated,
      @Param(value = "types") ProductType... type);

  /**
   * 上架时间列表
   */
  List<Product> listProductsBySaleAt(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "direction") Direction direction,
      @Param(value = "sellerShopId") String sellerShopId,
      @Param(value = "subCategoryIds") List<String> subCategoryIds,
      @Param(value = "priceStart") String priceStart,
      @Param(value = "priceEnd") String priceEnd,
      @Param(value = "selfOperated") Integer selfOperated,
      @Param(value = "types") ProductType... type);

  /**
   * 销量列表
   */
  List<Product> listProductsByCm(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category, @Param(value = "is_groupon") String isGroupon,
      @Param(value = "page") Pageable page, @Param(value = "direction") Direction direction,
      @Param(value = "sellerShopId") String sellerShopId);

  /**
   * 发布时间列表
   */
  List<Product> listProductsByOnsaleDate(@Param(value = "shopId") String shopId,
      @Param(value = "page") Pageable page, @Param(value = "direction") Direction direction);

  List<FilterBasicVO> listFilterByProductId(@Param("productId") String productId,
      @Param("page") Pageable pageable,
      @Param("params") Map<String, ?> params);

  Long countFilterByProductId(@Param("productId") String productId,
      @Param("params") Map<String, ?> params);

  Long countProductsBySales(@Param(value = "shopId") String shopId);

  /**
   * 获取某个类别下商品总数
   */
  Long countCategoryProducts(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category);

  /**
   * 库存列表
   */
  List<Product> listProductsByAmount(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "direction") Direction direction,
      @Param(value = "type") ProductType type,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus);


  /**
   * 根据价格进行排序by zzd
   */
  List<Product> listProductsByPrice(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "direction") Direction direction,
      @Param(value = "sellerShopId") String sellerShopId,
      @Param(value = "subCategoryIds") List<String> subCategoryIds,
      @Param(value = "priceStart") String priceStart,
      @Param(value = "priceEnd") String priceEnd,
      @Param(value = "selfOperated") Integer selfOperated,
      @Param(value = "types") ProductType... type);


  /**
   * 下架列表
   */
  List<Product> listProductsBySoldout(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "direction") Direction direction,
      @Param(value = "type") ProductType type,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus);

  /**
   * 查询
   */
  List<Product> listProductsBySearch(@Deprecated @Param(value = "shopId") String shopId,
      @Param(value = "kwd") String kwd, @Param(value = "page") Pageable page,
      @Param(value = "type") ProductType type);

  /**
   * 汉薇自营商品, 默认排序(上架时间倒序)
   * @param shopId
   * @param page
   * @param type
   * @return
   */
  List<Product> listProductsBySelfSupport(@Deprecated @Param(value = "shopId") String shopId,
                                     @Param(value = "page") Pageable page, @Param(value = "type") ProductType type);

    /**
   * 查询
   */
  List<Product> listProductsBySearchWithNoStatus(@Deprecated @Param(value = "shopId") String shopId,
      @Param(value = "kwd") String kwd, @Param(value = "page") Pageable page,
      @Param(value = "type") ProductType type, @Param(value = "productId") String productId, @Param("category") String category);

  /**
   * 查询
   */
  List<Product> listProductsBySearchAll(@Param(value = "shopId") String shopId,
      @Param(value = "kwd") String kwd, @Param(value = "page") Pageable page);

  /**
   * 查询
   */
  List<Product> listProductsBySearchAndPrice(@Deprecated @Param(value = "shopId") String shopId,
      @Param(value = "kwd") String kwd, @Param(value = "page") Pageable page,
      @Param(value = "direction") String direction,
      @Param(value = "type") ProductType type);

  /**
   * 汉薇自营商品列表, 按照价格排序
   * @param shopId
   * @param page
   * @param direction
   * @param type
   * @return
   */
  List<Product> listProductsBySelfSupportAndPrice(@Deprecated @Param(value = "shopId") String shopId,
                                             @Param(value = "page") Pageable page,
                                             @Param(value = "direction") String direction,
                                             @Param(value = "type") ProductType type);

  /**
   * 草稿列表
   */
  List<Product> listProductsByStatusDraft(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "type") ProductType type,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus);

  /**
   * 计划发布列表
   */
  List<Product> listProductsByForSale(@Param(value = "shopId") String shopId,
      @Param(value = "page") Pageable page);

  /**
   * 缺货列表
   */
  List<Product> listProductsByOutOfStock(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "type") ProductType type,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus);


  /**
   * 计划发布列表
   */
  List<Product> listProductsByDelay(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
      @Param(value = "type") ProductType type,
      @Param("status") ProductStatus status,
      @Param("reviewStatus") ProductReviewStatus reviewStatus);


  List<Product> listFilterBySpec(@Param("order") String order,
      @Param("direction") Direction direction,
      @Param("page") Pageable pageable,
      @Param("params") Map<String, ?> params);

  Long countFilterBySpec(@Param("params") Map<String, ?> params);

  /**
   * 店长推荐
   */
  List<Product> listProductsByRecommend(@Param(value = "shopId") String shopId,
      @Param(value = "page") Pageable page);

  Long countByRecommend(@Param(value = "shopId") String shopId);

  /**
   * 相关商品，根据tag
   */
  List<Product> listProductsByRelated(String shopId, String id);

  List<Product> listGiftProduct(@Param("category") String category, @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  Long countGiftProduct(@Param("category") String category);

  /**
   * 根据发布计划自动上架
   */
  int updateForSaleToOnSale();

  /**
   * 最近发布商品
   */
  List<Map<String, Object>> listProductByRecently(@Param(value = "shopId") String shopId,
      @Param(value = "date") Date date, @Param(value = "days") int days);

  /**
   * 每种状态商品的数量
   */
  Long countProductsByStatus(@Param(value = "shopId") String shopId,
      @Param(value = "status") ProductStatus status);

  /**
   * 缺货商品的数量
   */
  Long countProductsByOutofStock(@Param(value = "shopId") String shopId);

  /**
   * 自动上架
   */
  int autoOnSaleByTask();

  /**
   * 用于后台商品管理
   */
  List<ProductAdmin> listProductsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Long countProductsByAdmin(@Param(value = "params") Map<String, Object> params);

  /**
   *
   * @param id
   * @return
   */
  Product selectByAdmin(String id);

  List<Product> selectAll(@Param(value = "shopId") String shopId);

  Boolean instockByAdmin(@Param("ids") String[] ids);

  int deleteByAdmin(@Param("ids") String[] ids);

  int undeleteByAdmin(@Param("ids") String[] ids);

  List<Product> selectByIds(@Param("ids") String[] ids);

  /**
   * 对分页商品列表接口取总数
   *
   * @param shopId 店铺ID
   * @param catType 商品排序分类类型  { 'sales', 'amount', 'soldout', 'statusDraft', 'outofstock } +
   * default
   */
  Long selectLastCnt(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category, @Param(value = "is_groupon") String isGroupon,
      @Param(value = "catType") String catType,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 根据关键字进行搜索，返回总记录数据  by zzd
   */
  Long CountTotalByName(@Deprecated @Param(value = "shopId") String shopId, @Param(value = "kwd") String kwd);

  /**
   * 统计汉德森自营商品数量
   * @param shopId
   * @return
   */
  Long CountTotalBySelfSupport(@Deprecated @Param(value = "shopId") String shopId);

  Long CountTotalByNameWithNoStatus(@Deprecated @Param(value = "shopId") String shopId,
                                    @Param("productId") String productId,
                                    @Param(value = "kwd") String kwd,
                                    @Param("category") String category);


  /**
   * 根据活动取商品列表
   */
  List<Product> listProductsByActId(@Param(value = "actId") String actId,
      @Param("sort") String sortStr);

  List<Product> listActivityProducts(@Param("pager") Pageable pager, @Param("sort") String sortStr);

  List<Product> listActivityByProductsCateid(@Param("pager") Pageable pager,
      @Param("sort") String sortStr, @Param("cateid") int cateid);

  List<Product> listCategoryActivityProducts(@Param("pager") Pageable pager,
      @Param("sort") String sortStr, @Param("activity_id") String activity_id);

  List<Product> listCategoryActivityByProductsCateid(@Param("pager") Pageable pager,
      @Param("sort") String sortStr, @Param("cateid") int cateid,
      @Param("activity_id") String activity_id);

  int updateFakeSales(String id, int count);

  List<Product> getForsaleNow();

  Long countDelayProduct(String shopId);

  List<Product> listDelayProduct(@Param("shopId") String shopId, @Param("page") Pageable page);

  List<Product> listUnCategoriedProductsInShop(@Param("shopId") String shopId,
      @Param("pager") Pageable pager, @Param("cid") String cid);

  List<Product> listUnCategoriedProductsInShopInstock(@Param("shopId") String shopId,
      @Param("pager") Pageable pager, @Param("cid") String cid);

  List<Product> listUnCategoryProductsInShop(@Param("shopId") String shopId,
      @Param("pager") Pageable pager, @Param("cid") String cid);

  long countUnCategoriedProductsInShop(@Param("shopId") String shopId);

  /**
   * seker 修改优惠和优惠结束时的方法 名称 2015-05-07
   */
  // 仅在活动的时候使用，注意有update_lock设置
  //int updatePriceDiscountByShop(@Param("shopId") String shopId, @Param("discount") Float discount);
  int discountByShop(@Param("shopId") String shopId, @Param("discount") Float discount);

  // 仅在活动的时候使用，注意有update_lock设置
  //int updatePriceDiscount(@Param("id") String id, @Param("discount") Float discount);
  int discountByProduct(@Param("id") String id, @Param("discount") Float discount);

  // 仅在活动的时候使用，注意有update_lock设置
  //int updatePriceReduction(@Param("id") String id, @Param("reduction") Float reduction);
  int reductionByProduct(@Param("id") String id, @Param("reduction") Float reduction);

  // 仅在活动的时候使用，注意有update_lock设置
  //int updatePriceFromMarketPriceByShop(@Param("shopId") String shopId);
  int closeDiscountByShop(@Param("shopId") String shopId);

  // 仅在活动的时候使用，注意有update_lock设置
  //int updatePriceFromMarketPrice(@Param("id") String id);
  int closeDiscountByProduct(@Param("id") String id);

  int synchronousSource(@Param("id") String id, @Param("sourceVal") String sourceVal);

  List<String> obtainDbSynchronous(@Param("ids") List<String> ids);

  //获取活动下商品列表 (xquark_campaign_product)
  List<Product> listProductByActivityId(@Param("activityId") String activityId);

  List<Product> listProductsAvailableByChannel(@Param("activityId") String activityId,
      @Param("shopId") String shopId, @Param("pager") Pageable pager,
      @Param("channel") String channel, @Param("key") String key);

  Long countProductsAvailableByChannel(@Param("activityId") String activityId,
      @Param("shopId") String shopId, @Param("channel") String channel, @Param("key") String key);

  /**
   * 根据thirdItemId返回商品id
   */
  Long selectProductIdByThirdItemId(@Param("shopId") String shopId,
      @Param("thirdItemId") BigInteger thirdItemId);

  Long selectProductIdByProName(@Param("shopId") String shopId,
      @Param("productName") String productName);


  void addCode(String id);

  List<Product> listNoCodeProducts();

  /**
   * 4无
   */
  List<Product> selectUnCartProdByShop(@Param("shopId") String shopId);

  Long countOrderProdById(@Param("productId") String productId);

  Long countCommentProdById(@Param("productId") String productId);

  Long countFaverProdById(@Param("productId") String productId);

  int clear4noneProduct(@Param("productId") String productId);

  Product select4SyncFixById(String id);

  List<Product> select4SyncFix();

  List<Product> listAllProductsByOnsaleAt(@Param("shopId") String shopId,
      @Param("page") Pageable pageable);

  List<Product> listCategoriedProductsByCategoryId(@Param("cid") String cid,
      @Param("page") Pageable pageable);

  long countAllShopProduct(@Param("shopId") String shopId);

  Boolean updateCommisionById(@Param("ids") String[] ids,
      @Param("commissionRate") BigDecimal commissionRate);

  List<ProductAdmin> listProds2ActivityByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Long countProds2ActivityByAdmin(@Param(value = "params") Map<String, Object> params);

  int updateCommisionRateById(@Param("id") String id,
      @Param("commissionRate") BigDecimal commissionRate);

  List<Product> listCrossProductsBySales(@Param(value = "shopId") String shopId,
      @Param(value = "page") Pageable page, @Param(value = "direction") Direction direction);

  int updateCommisionAndRateById(@Param("id") String id,
      @Param("commissionRate") BigDecimal commissionRate,
      @Param("isCommission") Integer isCommission);

  int updateForStatusBySourceId(@Param("sourceId") String sourceId, @Param("status") String status);

  List<Product> listProductsBySourceId(@Param("sourceId") String sourceId);

  List<Product> findProductsBySkuCode(@Param("shopId") String shopId,
      @Param("skuCode") String skuCode);

  List<ProductAdmin> listProductsByCategory(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page, @Param("activityId") String activityId,
      @Param("categoryId") String categoryId);

  List<Product> listProductsByCategoryIds(@Param("objType") String objType,
                                          @Param("categoryId") String categoryId,
                                          @Param("page") Pageable pageable);

  int countProductsByCategoryIds(@Param("objType") String objType,
                                 @Param("categoryId") String categoryId);

  Long countProductsByCategory(@Param(value = "params") Map<String, Object> params,
      @Param("activityId") String activityId, @Param("categoryId") String categoryId);

  Long countProductsSellerById(@Param("id") String id);

  Product selectProductBySourceProductIdAndShopId(@Param("id") String id,
      @Param("shopId") String shopId);

  List<Product> list2ProductsByCategoryId(@Param("categoryId") String categoryId,
      @Param("shopId") String shopId);

  List<Product> listDistributeProductsBySales(@Param("rootShopIdz") String rootShopId,
      @Param("categoryId") String categoryId, @Param("shopId") String shopId,
      @Param(value = "page") Pageable page);

  List<ProductCardsVO> listProductWithCards(@Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  Long countProductWithCards();

  int deleteDiscountProduct(@Param("productId") String productId);

  /**
   * 自动下架
   */
  int autoInstockByTask();

  String selectConcatNameByIds(@Param("ids") String[] ids, @Param("giftOnly") Boolean giftOnly);

  /**
   * 查询商品的积分兑换比例
   *
   * @param id 商品id
   * @return 积分兑换比例
   */
  Integer selectYundouScaleById(String id);

  /**
   * 根据分类过滤条件，统计该过滤条件下总商品数
   */
  Long countProductsByCategoryAndPrice(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category,
      @Param(value = "subCategoryIds") List<String> subCategoryIds,
      @Param(value = "priceStart") String priceStart,
      @Param(value = "priceEnd") String priceEnd,
      @Param(value = "types") ProductType... type);

  boolean selectIsProduct(String id);

  /**
   * 根据商品id查询编码
   */
  String selectEncodeById(@Param("productId") String productId);

  String selectU8EncodeById(@Param("productId") String productId);

  /**
   * 根据商品编码查找id
   */
  Long selectIdByEncode(@Param("encode") String encode);

  boolean selectExistsByModel(@Param("model") String model);

  List<Product> listFilterByProductModel(@Param("model") String model,
      @Param("page") Pageable pageable);

  int updatePriority(@Param("id") String id, @Param("priority") Integer priority);

  boolean selectThirdExists(@Param("thirdId") Long thirdId, @Param("source") ProductSource source);

  Product selectThirdProduct(@Param("thirdId") Long thirdId, @Param("source") ProductSource source);

  Product selectThirdProductByEncode(@Param("encode") String encode, @Param("source") ProductSource source);

  List<String> listSFProductEncode();

  int updateThirdDetail(ThirdDetail detail);

  int batchUpdateThirdDetail(@Param("details") List<ThirdDetail> details);

  /**
   * 更新第三方商品详情
   */
  int updateMultipleThirdDetail(@Param("details") List<ThirdDetail> details);

  /**
   * 获取Wms产品
   */
  List<WmsProduct> getWmsProduct();

  int updateByCode(WmsInventory wmsInventory);

  List<String> getCode();

  Long insertByExcel(Product product);

  Integer updateByExcel(Product product);

  String getIdByCode(String code);

  List<Product> listProductsByDiscount(@Param(value = "shopId") String shopId,
                                    @Param(value = "category") String category,
                                    @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
                                    @Param(value = "direction") Direction direction,
                                    @Param(value = "sellerShopId") String sellerShopId,
                                    @Param(value = "subCategoryIds") List<String> subCategoryIds,
                                    @Param(value = "priceStart") String priceStart,
                                    @Param(value = "priceEnd") String priceEnd,
                                    @Param(value = "type") ProductType type);

  List<Product> listProductsByPoint(@Param(value = "shopId") String shopId,
                                       @Param(value = "category") String category,
                                       @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
                                       @Param(value = "direction") Direction direction,
                                       @Param(value = "sellerShopId") String sellerShopId,
                                       @Param(value = "subCategoryIds") List<String> subCategoryIds,
                                       @Param(value = "priceStart") String priceStart,
                                       @Param(value = "priceEnd") String priceEnd,
                                       @Param(value = "types") ProductType... type);

  Product getSupIdById(@Param("id") String id);

  String getSupplierCodeByIdHandler(@Param("id") String id);

  Integer productForSale(@Param("id") String id, @Param("time") Date time);

  List<Map<String, Object>> findInstock();

  /**
   * 查询所有库存小于安全库存的商品
   * 如果有多sku, 则按最大的那个sku算, 所有sku的库存都小于安全库存才下架
   */
  List<InStockProdVO> findInStockProd();

  Integer productInstock(@Param("product") List<String> product);

  Integer updateProductAmountByMaxSkuAmount(@Param("productId") String productId);

  List<OrderItemsDal> getLocOrderItems(String orderNo);

  Long selectProductIdBySkuCode(@Param("skuCode") String skuCode);

  Product selectProductByProductId(@Param("productId") Long productId);

  void insertSkuByExcel(@Param("skuCode")String skuCode,@Param("barCode")String barCode,
      @Param("productId") Long productId, @Param("amount") Integer amount,
      @Param("secureAmount") Integer secureAmount, @Param("price") BigDecimal price,
      @Param("deductionDPoint") BigDecimal deductionDPoint,
      @Param("point") BigDecimal point,@Param("netWorth") BigDecimal netWorth,
      @Param("promoAmt") BigDecimal promoAmt,@Param("serverAmt") BigDecimal serverAmt,
      @Param("length") Double length, @Param("height") Double height,
      @Param("width") Double width, @Param("weight") Integer weight,
      @Param("numInPackage") Integer numInPackage);

  void updateSkuByExcel(@Param("amount") Integer amount,
      @Param("secureAmount") Integer secureAmount, @Param("skuCode") String skuCode,
      @Param("barCode") String barCode, @Param("price") BigDecimal price,
      @Param("deductionDPoint") BigDecimal deductionDPoint,
      @Param("point") BigDecimal point, @Param("netWorth") BigDecimal netWorth,
      @Param("promoAmt") BigDecimal promoAmt, @Param("serverAmt") BigDecimal serverAmt,
      @Param("length") Double length, @Param("height") Double height,
      @Param("width") Double width, @Param("weight") Integer weight,
      @Param("numInPackage") Integer numInPackage);

  void insertCategoryRelationship(@Param("productId") String productId);

  void updateCategoryRelationship(@Param("productId") String productId);

  void updateProductCodeById(@Param("code") String code,@Param("id") Long id);


  List<String> listHotSearchedKeys(int displayNum);

  Integer countHotSearchedProducts(@Param("pids")List<String> pids);

  List<Product> selectHotSearchedProducts(@Param("pids")List<String> pids,
                                          @Param("order")String order,
                                          @Param("direction")String direction,
                                          @Param("page")Pageable page);

  String selectProductIdByKey(@Param("key") String key);

  Integer checkKeyExists(@Param("key") String key, @Param("id") String id);

  Integer checkHotsearchKeyExists(@Param("key") String key);

  Integer updateHotKeyState(@Param("id") String id, @Param("state") Integer state);

  Integer updateDisplayState(@Param("id") String id, @Param("display") Integer display);

  Integer newHotSearchKey(@Param("key") String key, @Param("pids") String pids);

  Integer updateHotSearchKey(@Param("id") String id, @Param("key") String key,
      @Param("pids") String pids);

  List<HotSearchKey> findValidHotSearchedKeys(@Param("key") String key, @Param("page") Pageable page);

  Integer countTotalByKey(@Param("key") String key);

  String findHotSearchKeyDisplayLimit();

  Integer updateHotKeyArchive(@Param("ids")List<String> ids);

  HotSearchKey getHotSearchKeyById(@Param("id") String id);

  Integer updateHotSearchKeyDisplayLimit(@Param("value") String value);

  List<Product> listProductsBySearchOrdered(@Deprecated @Param("shopId")String shopId, @Param("key")String key, @Param("page")Pageable page,
                                            @Param("direction")String direction, @Param("type")ProductType type, @Param("order")String order);

  List<Product> listProductsBySelfSupportAndOrdered(@Deprecated @Param("shopId")String shopId, @Param("page")Pageable page,
                                            @Param("direction")String direction, @Param("type")ProductType type, @Param("order")String order);

  List<HotProduct> getHotProduct(@Param("page") Pageable page, @Param("category") String category,
      @Param("brand") String brand, @Param("supplier") String supplier
      , @Param("keyword") String keyword, @Param("productId") String productId,
      @Param("ids") List ids);

  Integer countHotProduct();

  Integer getHotSearchKeyDisplayLimit();

  Integer getNumOfHot();

  /**
   * FIXME 该方法不适用多sku
   * @param productId 商品id
   * @return 组合子商品
   */
  List<ProductBasicVO> listCombineProduct(@Param("productId") String productId, @Param("page") Pageable pageable);

  /**
   * 查询组合商品总数
   * @param productId 商品id
   * @return 组合商品数量
   */
  Long countCombineProduct(@Param("productId") String productId);

  /**
   * FIXME 该方法不适用多sku
   * @param productId 商品id
   * @return 非组合商品
   */
  List<ProductBasicVO> listUnCombineProduct(@Param("productId") String productId, @Param("page") Pageable pageable);

  Long countUnCombineProduct(@Param("productId") String productId);

  /**
   * 销量排序取最多
   */
  Product productsBySale(@Param(value = "category") String category, @Param(value = "shopId") String shopId);

  Product selectProductSoldOutByProductIdIdHandler(@Param("productId")String productId);

  /**
   * 查询顺丰商品
   * @return
   */
  List<Product> selectSfProduct();

  List<Product> listProductsBySoldOutNotSf(@Param(value = "shopId") String shopId,
                                           @Param(value = "category") String category,
                                           @Param(value = "is_groupon") String isGroupon, @Param(value = "page") Pageable page,
                                           @Param(value = "direction") Direction direction,
                                           @Param(value = "type") ProductType type,
                                           @Param("status") ProductStatus status,
                                           @Param("reviewStatus") ProductReviewStatus reviewStatus,
                                           @Param("list") List<String> List);
  /**
   * 确认商品状态
   * @param productId
   * @param status
   * @return
 */
  boolean checkProductStatus(@Param("productId") String productId, @Param("status") String status);

    List<Product> selectCopyProduct(@Param("Id") String Id);
  /**
   * 是否是汉薇自营
   * @param productId
   * @return
   */
  boolean checkIsSelfOperated(@Param("productId") String productId, @Param("selfOperated") String selfOperated);

  /**
   * 修改顺丰商品状态
   * @param lists
   * @return
   */
  int updateSFProductStatus(@Param("lists")List<String> lists,@Param("status") String status);

}
