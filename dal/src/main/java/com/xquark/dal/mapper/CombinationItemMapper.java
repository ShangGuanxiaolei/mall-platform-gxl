package com.xquark.dal.mapper;

import com.xquark.dal.model.CombinationItem;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: byy Date: 18-6-1. Time: 上午8:48 组合商品明细
 */
public interface CombinationItemMapper {

  /**
   * 添加
   */
  int insert(CombinationItem obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<CombinationItem> collection);

  /**
   * 通过id查找
   */
  CombinationItem getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(CombinationItem obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<CombinationItem> list(@Param("page") Pageable page,
      @Param("params") Map<String, Object> params);

}