package com.xquark.dal.mapper;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author liuyandong
 * @date 2018/9/10 9:18
 */
public interface ActivityPtOutTImeMapper {
    //查询有效活动
    List<String> selectActivityCode();

    //查询失效的拼团编码
    List<String> selectOutTimePt(Timestamp time, String pCode);

    //查询库存不足的拼团
    List<String> selectOutStock(String pCode);

    List<String> selectOutStockDetail(String pCode);

    //查询提前失效活动的拼团
    List<String> selectTimeOver(String pCode);

    //查询每个拼团编码对应的所有成员的订单明细编码
    List<String> selectOrderId(String groupCode,String pCode);

    List<String> selectOrderIdByDetailCode(String detailCode,String pCode);

    int selectEffectTime(String pCode);

    List<String> findUnrefundPieceOrders();

}
