package com.xquark.dal.mapper;

import com.xquark.dal.model.ViewPage;
import com.xquark.dal.vo.ViewPageComponentVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface ViewPageMapper {

  ViewPage selectByPrimaryKey(@Param("id") String id);

  int insert(ViewPage viewPage);

  int modifyViewPage(ViewPage viewPage);

  int delete(@Param("id") String id);

  /**
   * 列表查询页面渲染组件
   */
  List<ViewPage> list(@Param(value = "pager") Pageable page, @Param("keyword") String keyword);

  /**
   * 对分页页面渲染组件列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

  /**
   * 列表查询自定义页面关联页面组件
   */
  List<ViewPageComponentVO> listComponent(@Param(value = "pager") Pageable page,
      @Param("id") String id);

  /**
   * 对分页自定义页面关联页面组件列表接口取总数
   */
  Long selectComponentCnt(@Param("id") String id);

}
