package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshmanProduct;

import java.util.List;

/**
 * 验证首单Mapper
 *
 * @author tanggb
 * @date 2019/03/05 14:02
 */
public interface FirstOrderMapper {

    //通过cpId统计用户订单
    Long countOrder(Long cpId);
    //查询新人商品信息
    List<FreshmanProduct> findProductByProcductId(Long productId);
    //通过productId查询否为新人专区商品
    Integer countFreshmanProduct(Long productId);
    //查询userId查询新人专区购买数
    Integer countAmount(Long userId);
    //根据userId查询新人专区购买订单数
    Integer mainOrderCount(Long userId);
    //查询出新人商品订单限购数
    Integer selectOrderLimit();
    //查询出新人商品限购数
    Integer findfreshmanValue();

    //用于支付完成页的首单校验逻辑
    Long countMainOrder(String buyerId, String mainOrderId);
    // 支付完成页, 校验首单之前是否有拼团订单
    Long countMainOrderByPiece(String buyerId, String mainOrderId);

    //统计购物车内新人商品数
    Integer countCart(Long cpId);
    //检测新人专区库存
    Integer checkStock(Long productId);
}
