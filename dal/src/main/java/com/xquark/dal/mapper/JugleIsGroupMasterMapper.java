package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author qiuchuyi
 * @date 2018/9/13 19:26
 */
public interface JugleIsGroupMasterMapper {
    /**
     * 根据orderHeadCode查询是否是团长；1:团长，0:不是团长
     * @param OrderHeadCode
     * @return
     */
    Integer jugleIsGroupMasterByOrderHeadCode(String OrderHeadCode);
}
