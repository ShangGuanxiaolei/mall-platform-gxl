package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;


/**
 * @author qiuchuyi
 * @date 2018/9/12 15:17
 */
public interface UpdatePgMemberInfoMapper {
//    @Update("UPDATE promotion_pg_tran_info set status=#{status} where pieceGroupDetailCode=#{pieceGroupDetailCode}")
    Integer updatePgMemberInfopieceGroupDetailCode(@Param("pieceGroupDetailCode")String pieceGroupDetailCode,@Param("status")Integer status);
}
