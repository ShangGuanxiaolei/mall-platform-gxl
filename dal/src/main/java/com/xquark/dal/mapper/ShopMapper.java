package com.xquark.dal.mapper;

import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.ShopAdmin;
import com.xquark.dal.vo.ShopProp;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface ShopMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "opRemark") String opRemark);

  int undeleteByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "opRemark") String opRemark);

  int insert(Shop record);

  Shop selectByPrimaryKey(String id);

  Shop selectByShopName(@Param(value = "name") String name);

  int updateByPrimaryKeySelective(Shop record);

  Shop selectByUserId(String userId);

  int openDanbao(@Param(value = "id") String id);

  int closeDanbao(@Param(value = "id") String id);

  List<Shop> selectAll(@Param(value = "page") Pageable pageable);

  Long countByShop();

  int updateImgByPrimaryKey(@Param(value = "id") String id, @Param(value = "img") String img);

  int updateNameByPrimaryKey(@Param(value = "id") String id, @Param(value = "name") String name);

  int updateWechatByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "wechat") String wechat);

  int updateDescByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "description") String description);

  int updateBulletinByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "bulletin") String bulletin);

  int updateLocalByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "provinceId") Long provinceId, @Param(value = "cityId") Long cityId);

  int updatePostageStatusByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "postageStatus") Boolean postageStatus);

  int updatePostageByPrimaryKey(@Param(value = "id") String id,
      @Param(value = "freeZone") Long freeZone, @Param(value = "postage") BigDecimal postage);

  /**
   * 用于后台店铺管理
   */
  List<ShopAdmin> listShopsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);


  List<ShopAdmin> listShopsByNewActivity(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page, @Param(value = "activityid") String activityid);

  Long countShopsByAdmin(@Param(value = "params") Map<String, Object> params);

  Long countShopByActivity(@Param(value = "params") Map<String, Object> params,
      @Param(value = "activityID") String activityID);

  int saveFragmentById(@Param(value = "id") String id,
      @Param(value = "fragmentStatus") boolean fragmentStatus);

  List<ShopProp> selectAutoUpdatedShops();

  Long isPropUpdated(@Param(value = "shopId") String shopId,
      @Param(value = "shopUrl") String shopUrl);

  int activityUpdated(@Param(value = "shopId") String shopId,
      @Param(value = "activityId") String activityId);

  int updateAutoShopPropAt(@Param(value = "shopId") String shopId);

  Long selectShopOwner(@Param(value = "shopId") String shopId);

  List<Shop> listNoCodeShops();

  List<Shop> listAllShops();

  void addCode(String id);

  List<Shop> listShopByActivity(@Param(value = "activityID") String activityID);

  List<Shop> listShopIdsByActivity(@Param(value = "activityID") String activityID);

  /**
   * 我的店铺单品排序
   */
  int sortShopProducts(@Param(value = "list") List<String> list,
      @Param(value = "shopId") String shopId);

  int deleteByUserId(@Param(value = "userId") String userId);

  Shop selectRootShop();
}