package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PromotionInviteCode;
public interface PromotionInviteCodeMapper {
	
	List<PromotionInviteCode> selectByCodeAndPcode(@Param("code")String code ,@Param("pCode")String pCode);
	
	int updateRelation ( @Param("code")String code, @Param("cpid")String cpid, @Param("pCode")String pCode);
	
	String selectInviteCodeByCpid(@Param("cpid")String cpid, @Param("pCode")String pCode);

}
