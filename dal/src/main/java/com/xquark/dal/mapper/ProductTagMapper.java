package com.xquark.dal.mapper;

import java.util.List;

import com.xquark.dal.model.ProductTag;

public interface ProductTagMapper {

  int updateForArchive(String id);

  int updateForUnArchive(String id);

  int insert(ProductTag record);

  ProductTag selectByPrimaryKey(String id);

  /**
   * 根据商品ID获取所有的Tag列表
   */
  List<ProductTag> selectByProductId(String productId);

  /**
   * 根据店铺ID获取所有的Tag列表
   */
  List<ProductTag> selectByShopId(String shopId);

}