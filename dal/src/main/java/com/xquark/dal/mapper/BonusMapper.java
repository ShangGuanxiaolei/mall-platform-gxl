package com.xquark.dal.mapper;

import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.vo.BonusVO;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface BonusMapper {


  List<BonusVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 更新分红发放状态
   */
  int updateStatus(@Param("ids") String[] ids, @Param("status") String status);

  /**
   * 根据年度，月度获取月度分红记录
   */
  Bonus getBonusMonthByPeriod(@Param("year") String year, @Param("month") String month);

  /**
   * 根据年度获取年度分红记录
   */
  Bonus getBonusYearByPeriod(@Param("year") String year);

  /**
   * 删除分红记录
   */
  int deleteBonus(@Param("id") String id);

  /**
   * 新增分红记录
   */
  int insertBonus(Bonus bonus);

  /**
   * 新增分红明细记录
   */
  int insertBonusDetail(BonusDetail bonusDetail);

  /**
   * 更新分红记录的end_time
   */
  int updateBonusEndTime(@Param("id") String id);

  /**
   * 获取某个代理某年月度分红各个级别数量
   */
  List<Map> getBonusMonthCount(@Param("year") String year, @Param("userId") String userId);


  /**
   * 获取当前用户的分红记录
   */
  List<BonusVO> getMyBonus(@Param("userId") String userId);

  /**
   * 得到某个代理某年某个级别下所有分红的月份
   */
  List<String> getBonusMonthByType(@Param("userId") String userId, @Param("level") String level,
      @Param("year") String year);

  /**
   * 获取某年某个月份每个达标等级对应的代理总数
   */
  List<Map> getTotalBonusMonthCount(@Param("year") String year, @Param("month") String month);

  /**
   * 获取某年份每个达标等级对应的代理总数
   */
  List<Map> getTotalBonusYearCount(@Param("year") String year);

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的月度分红金额
   */
  int updateBonusMonthFee(@Param("year") String year, @Param("month") String month,
      @Param("level") String level, @Param("count") String count);

  /**
   * 根据每个级别达标的代理人数，平均算出每个代理能分的年度分红金额
   */
  int updateBonusYearFee(@Param("year") String year, @Param("level") String level,
      @Param("count") String count);

}
