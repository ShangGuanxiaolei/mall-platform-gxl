package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerBank;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerBankMapper {

    void addCustomerBankInfo(CustomerBank customerBank);

    List<CustomerBank> queryCustomerBankListByCpId(@Param("cpId") Long cpId);
}