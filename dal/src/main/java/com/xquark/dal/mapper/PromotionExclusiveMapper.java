package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityExclusive;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionExclusiveMapper {

    /**
     * 添加活动商品时，同时将活动商品保存到活动表中
     *
     * @param activityExclusive
     */
    void insertActivityProduct(ActivityExclusive activityExclusive);

    /**
     * 更新活动商品状态(在普通商品中是否展示)
     *
     * @param productId
     * @param status
     */
    void updateActivityExclusive(@Param("productId") Integer productId, @Param("status") Integer status);

    int updateStatus(@Param("productId") Long productId, @Param("pCode") String pCode,
                     @Param("status") int status);
}
