package com.xquark.dal.mapper;

import com.xquark.dal.model.ViewPageComponent;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ViewPageComponentMapper {

  ViewPageComponent selectByPrimaryKey(@Param("id") String id);

  int insert(ViewPageComponent viewPageComponent);

  int modifyViewPageComponent(ViewPageComponent viewPageComponent);

  int delete(@Param("id") String id);

  List<ViewPageComponent> findByPageId(@Param("pageId") String pageId);
}
