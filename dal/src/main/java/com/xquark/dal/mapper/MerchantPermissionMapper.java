package com.xquark.dal.mapper;

import com.xquark.dal.model.MerchantPermission;
import com.xquark.dal.vo.MerchantPermissionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public interface MerchantPermissionMapper {

  List<MerchantPermissionVo> selectAll();

  MerchantPermissionVo selectByPrimaryKey(@Param(value = "id") String id);

  List<MerchantPermissionVo> listByParentId(@Param(value = "pId") String pId);

  List<MerchantPermissionVo> listByIds(@Param(value = "ids") String[] ids);

  int updateByPrimaryKeySelective(MerchantPermission permission);

  int deleteByPrimaryKey(@Param(value = "id") String id);

  Long insert(MerchantPermission permission);
}
