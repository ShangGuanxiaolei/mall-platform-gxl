package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionPartnerPlan;
import java.util.List;

public interface CommissionPartnerPlanMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int insert(CommissionPartnerPlan record);

  int insertSelective(CommissionPartnerPlan record);

  CommissionPartnerPlan selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CommissionPartnerPlan record);

  int updateByPrimaryKey(CommissionPartnerPlan record);

  List<CommissionPartnerPlan> selectByShopId(String ownShopId);

  CommissionPartnerPlan selectDefaultByShopId(String ownShopId);
}