package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionProductRuleConfig;
import com.xquark.dal.vo.CommissionProductRuleConfigVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommissionProductRuleConfigMapper {

  int insert(CommissionProductRuleConfig rule);

  int updateByPrimaryKey(CommissionProductRuleConfig rule);

  int updateByPrimaryKeySelective(CommissionProductRuleConfig rule);

  List<CommissionProductRuleConfig> listByCommissionRuleAndProduct(
      @Param("commissionRuleId") String commissionRuleId, @Param("productId") String productId);


  CommissionProductRuleConfigVO selectByPrimaryKey(@Param("id") String id);

  int modify(CommissionProductRuleConfig role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<CommissionProductRuleConfigVO> list(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

}
