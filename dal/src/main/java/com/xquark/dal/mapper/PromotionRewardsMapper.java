package com.xquark.dal.mapper;

import com.xquark.dal.model.mypiece.PromotionRewardsVo;

/**
 * @author qiuchuyi
 * @date 2018/9/22 0:00
 */
public interface PromotionRewardsMapper {
    /**
     * 根据p_detail_code查询拼团活动是否有折上折奖励
     * @param pTranCode
     * @return
     */
    Integer queryPromotionRewardsZSZInfoByPTranCode(String pTranCode);
}
