package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionToVip;
import com.xquark.dal.model.PromotionToVipType;
import org.apache.ibatis.annotations.Param;

import org.springframework.data.domain.Pageable;
import java.util.List;

public interface PromotionToVipMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionToVip record);

    int insertSelective(PromotionToVip record);

    PromotionToVip selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionToVip record);

    int updateByPrimaryKey(PromotionToVip record);

    PromotionToVip selectByType(@Param("type") PromotionToVipType type);

    List<PromotionToVip> selectList(@Param("page") Pageable page);

    Integer countList();

    int updateAndUse(PromotionToVip promotionToVip);

    int changeState(PromotionToVip promotionToVip);

    int autoPromotionClose();

    int autoPromotionBegin();
}