package com.xquark.dal.mapper;

import com.xquark.dal.model.XquarkOrderThirdInfoDetail;

public interface XquarkOrderThirdInfoDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkOrderThirdInfoDetail record);

    int insertSelective(XquarkOrderThirdInfoDetail record);

    XquarkOrderThirdInfoDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkOrderThirdInfoDetail record);

    int updateByPrimaryKey(XquarkOrderThirdInfoDetail record);
}