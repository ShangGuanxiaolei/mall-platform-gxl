package com.xquark.dal.mapper;

import com.xquark.dal.model.Order;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author liuyandong
 * @date 2018/9/11 11:00
 */
public interface ActivityPtOrderStatusMapper {
    int updateOrderStatus(String orderId);
    List<String> selectPtPcode();
    List<String> selectPtGroupCode(String pCode);
    int selectMainOrderId(String orderNo);
    Order selectOrderId(int id);
}
