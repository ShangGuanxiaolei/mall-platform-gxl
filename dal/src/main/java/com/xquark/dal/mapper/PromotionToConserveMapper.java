package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionToConserve;

public interface PromotionToConserveMapper {
    int deleteByPrimaryKey(Long id);

    int insert(String cpId);

    int insertSelective(PromotionToConserve record);

    PromotionToConserve selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionToConserve record);

    int updateByPrimaryKey(PromotionToConserve record);

    Integer selectByCpId(String cpId);
}