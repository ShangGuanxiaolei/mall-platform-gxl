package com.xquark.dal.mapper;

import com.xquark.dal.model.CollagePushMessage;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.annotation.Id;

/**
 * @Author: zzl
 * @Date: 2018/9/6 16:56
 * @Version
 */
public interface MsgListMapper {

  /**
   * push消息列表查询
   * @param belognTo
   * @param pageNumber
   * @param pageCount 每页显示数量
   * @return List<CollagePushMessage>
   */
  List<CollagePushMessage> selectMsgList(@Param("belognTo")Long belognTo,@Param("TypeMsg")String TypeMsg,@Param("pageNumber")Integer pageNumber,@Param("pageCount")Integer pageCount);

  /**
   * push未读消息列表查询
   * @param belognTo
   * @param pageNumber
   * @param pageCount 每页显示数量
   * @return List<CollagePushMessage>
   */
  List<CollagePushMessage> selectMsgUnReadList(@Param("belognTo")String belognTo,@Param("pageNumber")Integer pageNumber,@Param("pageCount")Integer pageCount);

  /**
   * push消息状态修改
   * @param id
   * @return 修改成功与否
   */
  Boolean updateMsgStatus(@Param("id") String id);


  /**
   *根据cpid查询push消息
   * @param cpid
   * @return
   */
  int selectMsgByCpId(Long cpid);

  /**
   * 根据类型查询数量
   * @param cpid
   * @param type
   * @return
   */
  int selectTypeCount(@Param("cpid") Long cpid,@Param("type") String type);

  /**
   * 查询第一条消息
   * @param cpid
   * @param type
   * @return
   */
  String selectContent(@Param("cpid") Long cpid,@Param("type") String type);

  /**
   * 删除消息
   * @param MsgId
   * @return
   */
  boolean deleteMsg(@Param("MsgId")Integer MsgId);
}
