package com.xquark.dal.mapper;

import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.model.promotion.OrderSkuAmount;
import com.xquark.dal.vo.PromotionOrderDetailVo;
import com.xquark.dal.vo.PromotionSkuVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 拼团活动实体映射接口
 *
 * @author tanggb
 */
public interface PromotionOrderDetailMapper {

  void insert(PromotionOrderDetail promotionOrderDetail);

  /**
   * 通过订单号查找pTranCode
   */
  String findPTranCode(String orderId);

  /**
   * 通过订单号查找pTranOrderDetail
   */
  String findPTranDetailCode(String orderId);

  /**
   * 通过订单号查找pTranDetailCode
   */
  List<Map> findOrderMapList(String pieceGroupTranCode);

  /**
   * 查找出拼团订单数组
   */
  List<String> findOrders(String pCode);

  /**
   * 根据pcode查询订单信息列表
   */
  List<PromotionSkuVo> findSkus(String pCode);

  List<Map> findSkus2(String pCode);


  /**
   * 根据pcode查询订单信息列表
   */
  List<PromotionOrderDetailVo> findOrderInfos(String pTranCode);

  /**
   * 查询活动订单信息
   */
  Map<String, Object> findPgOrderInfo(@Param("orderId") String orderId,
      @Param("subOrderId") String subOrderId);

  List<Map<String, Object>> findPieceGroupInfo();

  /**
   * 查询当前会员历史活动订单总金额 cpid 会员编码 pCode 活动编码
   */
  double findSumMoneyHitoryOrder(@Param("cpid") String cpid, @Param("pCode") String pCode);

  Integer findSumAmountHitoryOrder(@Param("cpid") String cpid, @Param("pCode") String pCode,
      @Param("skuId") String promotionSkuId);

  /**
   * 注意是子订单id
   */
  String selectTranCodeByOrderNo(String orderNo);

  PromotionOrderDetail selectByOrderNo(String orderNo);

  PromotionOrderDetail selectByMainOrderNo(@Param("mainOrderNo") String mainOrderNo,
                                           @Param("pType") String pType);

  Integer batchUpdateTranOrderStatusToPaid(@Param("orderNos") List<String> orderNos);

  Integer batchUpdateTranOrderStatusToPaidByOrderNO(@Param("orderNoList") List<String> orderNoList);

  List<String> selectTranOrderNos(@Param("tranCode") String tranCode);

  List<String> selectTranOrderIds(@Param("tranCode") String tranCode);

  List<String> selectTranOrderNoList(@Param("tranCode") String tranCode);

  List<OrderSkuAmount> loadDetailByTranCode(@Param("tranCode") String tranCode);

  /**
   * 查询当前团需要扣减的总库存
   * 该查询报错, 拆分为两个查询
   * {@link PromotionOrderDetailMapper#selectOrderIdByTranCode(String)}
   * {@link PromotionOrderDetailMapper#selectItemAmtByOrderId(String...)} (String)}
   */
  @Deprecated
  int sumTranTotalAmount(@Param("tranCode") String tranCode, @Param("orderId") String orderId);

  /**
   * 查询团下面的所有有效订单
   */
  List<String> selectOrderIdByTranCode(@Param("tranCode") String tranCode);

  /**
   * 查询参团的所有订单
   */
  String selectOrderIdByDetailCode(@Param("detailCode") String detailCode);

  /**
   * 查询订单下的item总和
   */
  List<OrderSkuAmount> selectItemAmtByOrderId(@Param("ids") String... ids);

  int sumTranDetailAmount(@Param("detailCode") String detailCode, @Param("orderId") String orderId);

  int countUnPaidTranOrder(@Param("tranCode") String tranCode, @Param("userId") String userId);

  List<PromotionOrderDetail> selectByTranCode(String tranCode);
}
