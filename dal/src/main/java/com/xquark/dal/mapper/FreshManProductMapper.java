package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManProductVo;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:16:06
 * Des:新人商品列表
 */
public interface FreshManProductMapper {
    /**
     * 依据商品tab查询列表
     * @param tabId
     * @return
     */
    List<FreshManProductVo> selectListByTabId(@Param("tabId") Integer tabId);

    /**
     * 根据productid查询新人专区商品
     * @param productId
     * @return
     */
    List<FreshManProductVo> selectByProductId(String productId);

    BigDecimal selectFreshmanLogisticsFee();

    /**
     * 功能描述:根据skuId查询新人专区商品
     * @author Luxiaoling
     * @date 2019/3/11 15:06
     * @param
     * @return
     */

    List<FreshManProductVo> selectBySkuId(String skuId);


    List<FreshManProductVo> selectByProductIdIgnoreStatus(String productId);
}