package com.xquark.dal.mapper;

import com.xquark.dal.model.MarketingTweet;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;


public interface MarketingTweetMapper {

  MarketingTweet selectByPrimaryKey(@Param("id") String id);

  int insert(MarketingTweet marketingTweet);

  int modifyMarketingTweet(MarketingTweet marketingTweet);

  int delete(@Param("id") String id);

  /**
   * 列表查询发现
   */
  List<MarketingTweet> list(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页发现列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

  /**
   * app端发现列表查询发现
   */
  List<MarketingTweet> listApp(@Param(value = "pager") Pageable page);

  /**
   * 供客户端调用，获取首页品牌/专题
   */
  List<MarketingTweet> getHomeForApp();

}
