package com.xquark.dal.mapper;

import com.xquark.dal.model.PrivilegeProduct;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.vo.PrivilegeProductVO;
import com.xquark.dal.vo.ProductTopVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PrivilegeProductMapper {

  int deleteByPrimaryKey(String id);

  int insert(PrivilegeProduct record);

  int insertSelective(PrivilegeProduct record);

  PrivilegeProductVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PrivilegeProduct record);

  int updateByPrimaryKey(PrivilegeProduct record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<PrivilegeProductVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(@Param(value = "shopId") String shopId,
      @Param(value = "productId") String productId, @Param(value = "id") String id);

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  List<PrivilegeProductVO> getHomeForApp(@Param(value = "pager") Pageable pager,
      @Param(value = "shopId") String shopId);

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  long getCount();

}