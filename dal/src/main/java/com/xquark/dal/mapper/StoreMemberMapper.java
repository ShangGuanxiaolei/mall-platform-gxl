package com.xquark.dal.mapper;

import com.xquark.dal.model.StoreMember;
import com.xquark.dal.vo.StoreMemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface StoreMemberMapper {

  int deleteByPrimaryKey(String id);

  int insert(StoreMember record);

  int insertSelective(StoreMember record);

  StoreMember selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(StoreMember record);

  int updateByPrimaryKey(StoreMember record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<StoreMemberVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询同一个门店审核通过的店长个数
   */
  StoreMember selectActiveLeader(@Param(value = "storeId") String storeId);

  /**
   * 通过手机号查询店员信息
   */
  StoreMember selectByPhone(@Param(value = "phone") String phone);

  /**
   * 通过userId查询店员信息
   */
  StoreMember selectByUserId(@Param(value = "userId") String userId);

  /**
   * 查找某个门店下所有特约店员
   */
  List<StoreMember> selectSeniorByStoreId(@Param(value = "storeId") String storeId);

  /**
   * 查找某个门店下所有店员
   */
  List<StoreMember> selectByStoreId(@Param(value = "storeId") String storeId);

}