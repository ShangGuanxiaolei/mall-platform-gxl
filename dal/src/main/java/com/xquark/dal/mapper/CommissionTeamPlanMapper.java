package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionPartnerPlan;
import com.xquark.dal.model.CommissionTeamPlan;

import java.util.List;

public interface CommissionTeamPlanMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int insert(CommissionTeamPlan record);

  int insertSelective(CommissionTeamPlan record);

  CommissionTeamPlan selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CommissionTeamPlan record);

  int updateByPrimaryKey(CommissionTeamPlan record);

  List<CommissionTeamPlan> selectByShopId(String ownShopId);

  CommissionTeamPlan selectDefaultByShopId(String ownShopId);

}