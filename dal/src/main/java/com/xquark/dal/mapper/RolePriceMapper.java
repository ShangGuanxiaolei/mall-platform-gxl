package com.xquark.dal.mapper;

import com.xquark.dal.model.Role;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.vo.RolePriceVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface RolePriceMapper {

  RolePriceVO selectByPrimaryKey(@Param("id") String id);

  int insert(RolePrice role);

  int modify(RolePrice role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<RolePriceVO> list(@Param(value = "pager") Pageable page, @Param("keyword") String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

  RolePriceVO selectByProductAndRole(@Param("productId") String productId,
      @Param("roleId") String roleId);

}
