package com.xquark.dal.mapper;

import com.xquark.dal.vo.PolyapiGoodInfoVO;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * User: kong Date: 2018/6/16. Time: 15:16 菠萝派订单下载
 */
public interface PolyapiOrderVOMapper {

  /**
   * 根据状态查询订单数量
   */
  String getNumTotalOrder(@Param("status") String status);

  /**
   * 查询所有订单数量
   */
  String getNumTotalAllOrder();

  /**
   * 查询订单
   */
  List<PolyapiOrderItemVO> getPolyapiOrderItem(@Param("pageIndex") Integer pageIndex,
      @Param("pageSize") Integer pageSize,
      @Param("startTime") Date startTime,
      @Param("endTime") Date endTime,@Param("orderStatus") String orderStatus);

  /**
   * 查询订单
   */
  List<PolyapiOrderItemVO> getPolyapiAllOrderItem(@Param("pageIndex") Integer pageIndex,
      @Param("pageSize") Integer pageSize,
      @Param("startTime") Date startTime,
      @Param("endTime") Date endTime);

  /**
   * 根据订单号查找商品
   */
  List<PolyapiGoodInfoVO> getPolyapiGoodInfoByOrderNo(String orderNo);

}