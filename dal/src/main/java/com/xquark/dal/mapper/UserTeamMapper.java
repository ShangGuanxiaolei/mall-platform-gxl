package com.xquark.dal.mapper;

import com.xquark.dal.model.UserTeam;
import com.xquark.dal.vo.UserTeamVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface UserTeamMapper {

  int deleteByPrimaryKey(String id);

  int insert(UserTeam record);

  int insertSelective(UserTeam record);

  UserTeam selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserTeam record);

  int updateByPrimaryKey(UserTeam record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<UserTeam> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  UserTeam selectByUserId(@Param(value = "userId") String userId);

  /**
   * 服务端分页查询数据
   */
  List<UserTeamVO> listByTeamId(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCntByTeamId(@Param(value = "params") Map<String, Object> params);

  /**
   * 得到某战队所有成员信息
   */
  List<String> getAllUserByTeamId(@Param(value = "teamId") String teamId);

  int deleteByUserId(@Param(value = "userId") String userId);

}