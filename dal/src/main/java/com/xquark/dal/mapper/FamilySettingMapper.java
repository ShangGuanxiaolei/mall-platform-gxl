package com.xquark.dal.mapper;

import com.xquark.dal.model.FamilySetting;
import org.apache.ibatis.annotations.Param;

public interface FamilySettingMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(FamilySetting record);

  int insertSelective(FamilySetting record);

  FamilySetting selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(FamilySetting record);

  int updateByPrimaryKey(FamilySetting record);
}