package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsOrderPackage;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午4:21
 */
public interface WmsOrderPackageMapper {

  /**
   * 添加
   */
  int insert(WmsOrderPackage obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsOrderPackage> collection);

  /**
   * 通过id查找
   */
  WmsOrderPackage getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsOrderPackage obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsOrderPackage> list(@Param("page") Pageable page,
      @Param("params") Map<String, Object> params);

  /**
   * 根据orderNo查询
   */
  WmsOrderPackage getByOrderNo(String orderNo);

  /**
   * 获取orderNo
   */
  Set<String> getOrderNo();
}