package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderIdentityLog;

public interface OrderIdentityLogMapper {
    int deleteByPrimaryKey(String id);

    int insert(OrderIdentityLog record);

    boolean selectOrderNoExists(String orderNo);

    int updateByOrderNo(OrderIdentityLog record);

    OrderIdentityLog selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OrderIdentityLog record);

    int updateByPrimaryKey(OrderIdentityLog record);
}