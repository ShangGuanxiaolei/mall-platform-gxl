package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.vo.PromotionSkuVo;
/**
 * 
 * @author luxiaoling
 *
 */

public interface PromotionSkusMapper {
	/**
	 * 根据skuCode查询活动sku
	 * @param skuCode
	 * @return
	 */
	List<PromotionSkuVo>  getPromotionSkus(@Param("skuCode")String skuCode);

	/**
	 * 根据活动编码查询活动sku
	 * @param pCode
	 * @return
	 */
	List<PromotionSkuVo>  getPromotionSkusByPcode(@Param("pCode")String pCode);

	//	List<Integer> getPSkuUseableNum(@Param("skuCode")String skuCode, @Param("skuCode")String pType);
	List<Integer> getPSkuUseableNum(@Param("skuCode")String skuCode, @Param("pType")String pType);

	/**
	 * 根据productid查询skuCode
	 */
	String querySkuCodeByProductid(@Param("productId") String productId,@Param("pCode") String pCode);
	/**
	 * 根据skucode和pCode查询活动商品信息
	 */
	PromotionSkuVo selectProSkuBySkuCodeAndPcode (@Param("skuCode") String skuCode,@Param("pCode") String pCode);

	List<String> selectPCodeBySkuCodeList(@Param("list")List<String> promotionskus);

	Integer getUseableNumBySkuCodeAndPcode(@Param("skuCode")String skuCode, @Param("pCode")String pCode);

    Integer getGift(@Param("skuCode")String skuCode, @Param("pCode")String pCode);


	List<PromotionSkuVo>  getPromotionSkusAllStatus(@Param("skuCode")String skuCode);

	/**
	 * 查詢需要从redis同步库存的大会活动商品
	 * @return
	 */
	List<PromotionSkuVo> getPromotionSkuStockToDB();

	List<PromotionSkuVo> getPromotionByProductId(String productId);
	/**
	 * @Author chp
	 * @Description //查询购物车商品是否是促销包邮商品
	 **/
	 Integer  countBySku(@Param("skuCode")String skuCode,@Param("productId") String productId);

	Integer  countSaleSku(@Param("skuCode")String skuCode,@Param("productId") String productId);

	/**
	 * @Author chp
	 * @Description    查询商品购买限制
	 **/
    Integer  getBuyCountlimit(@Param("skuCode")  String skuCode);

    Integer  getBuyCountLimitBySkuId(@Param("skuId")  String skuId);

	PromotionSkuVo getProductSalesSkus(@Param("skuCode")String skuCode,@Param("pCode")String pCode);


	/**
	 * @Author chp
	 * @Description    查询用户已有订单中活动商品的数量
	 **/

    Integer  getBuyCount(@Param("buyerId") String buyerId,@Param("skuCode")  String skuCode);

	Integer  getBuyCountBuySkuId(@Param("buyerId") String buyerId,@Param("skuId")  String skuId);

	//查看商品库存信息
	Integer  getGoodCount(@Param("skuCode") String skuCode);
}
