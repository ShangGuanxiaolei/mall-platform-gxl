package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionFullReduceOrder;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionReduceDetailVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionFullReduceOrderMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionFullReduceOrder record);

  int insertSelective(PromotionFullReduceOrder record);

  PromotionFullReduceOrder selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionFullReduceOrder record);

  int updateByPrimaryKey(PromotionFullReduceOrder record);

  List<PromotionReduceOrderVO> listOrderDetail(@Param("type") PromotionType type,
      @Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  List<PromotionReduceOrderVO> listByOrderId(@Param("orderId") String orderId);

  List<PromotionReduceDetailVO> listDetailByOrderId(@Param("orderId") String orderId);

  Long countOrderDetail(@Param("type") PromotionType type);

}