package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.FragmentImage;
import com.xquark.dal.vo.FragmentImageVO;

public interface FragmentImageMapper {

  int insert(FragmentImage fragmentImage);

  int deleteById(@Param(value = "id") String id);

  int deleteByFragmentId(@Param(value = "fragmentId") String fragmentId);

  FragmentImageVO selectById(@Param(value = "id") String id);

  Integer selectMaxByFragmentId(@Param(value = "fragmentId") String fragmentId);

  List<FragmentImageVO> selectByFragmentId(@Param(value = "fragmentId") String fragmentId);

  void incAllBeforeDest(@Param(value = "fragmentId") String fragmentId,
      @Param(value = "idx") int idx, @Param(value = "increment") int increment);

  void decAllAfterDest(@Param(value = "fragmentId") String fragmentId,
      @Param(value = "idx") int idx, @Param(value = "increment") int increment);

  int updateSrcIdx(@Param(value = "id") String id, @Param(value = "idx") int idx);

  boolean batchDeleteByFragmentIds(@Param(value = "fragmentIds") List<String> fragmentIds);
}
