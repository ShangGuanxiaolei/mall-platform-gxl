package com.xquark.dal.mapper;

import com.xquark.dal.model.Poscode;
import org.apache.ibatis.annotations.Param;

public interface PoscodeMapper {

  int deleteByPrimaryKey(Long id);

  int insert(Poscode record);

  int insertSelective(Poscode record);

  Poscode selectByPrimaryKey(Long id);

  boolean selectSnCodeExists(@Param("snCode") String snCode);

  int updateByPrimaryKeySelective(Poscode record);

  int updateByPrimaryKey(Poscode record);
}