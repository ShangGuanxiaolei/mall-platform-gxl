package com.xquark.dal.mapper;

import com.xquark.dal.model.ModuleFunction;
import com.xquark.dal.vo.ModuleFunctionVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * 作者: wangxh 创建日期: 17-4-7 简介:
 */
public interface ModuleFunctionMapper {

  Integer insert(ModuleFunction moduleFunction);

  Integer updateById(ModuleFunction moduleFunction);

  Integer deleteById(@Param("id") String id);

  Integer deleteByModuleId(@Param("moduleId") String moduleId);

  Integer deleteByModuleIdAndEnable(@Param("moduleId") String moduleId);

  Integer deleteByModuleAndShow(@Param("ModuleId") String moduleId);

  ModuleFunction selectOne(@Param("id") String id);

  List<ModuleFunction> list(@Param("order") String order, @Param("page") Pageable page,
      @Param("direction") Sort.Direction direction);

  List<ModuleFunction> listByModuleId(@Param("moduleId") String moduleId,
      @Param("order") String order, @Param("page") Pageable page,
      @Param("direction") Sort.Direction direction);

  List<ModuleFunction> listByModuleName(@Param("moduleName") String moduleName,
      @Param("order") String order, @Param("page") Pageable page,
      @Param("direction") Sort.Direction direction);

  Boolean checkExists(@Param("functionId") String functionId, @Param("moduleId") String moduleId);

  Integer selectCount(@Param("moduleId") String moduleId);

  List<ModuleFunctionVO> listModuleFunctionVO(@Param("moduleId") String moduleId,
      @Param("order") String order, @Param("page") Pageable page,
      @Param("direction") Sort.Direction direction);

}
