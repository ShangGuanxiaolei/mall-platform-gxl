package com.xquark.dal.mapper;

import com.xquark.dal.model.SFCategoryMapping;
import com.xquark.dal.model.Shipping;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.MapKey;

/**
 * @auther liuwei
 * @date 2018/6/8 15:20
 */
public interface ShippingMapper {

  boolean selectShippingByProductId(int productId);

  int insertSFShipping(Shipping shipping);

  /**
   * 查询顺丰类目
   * @return
   */
  List<SFCategoryMapping> selectSFCategoryMap();

}
