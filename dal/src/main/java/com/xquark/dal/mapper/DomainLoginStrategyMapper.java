package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.DomainLoginStrategy;

/**
 * @author: chenxi
 */

public interface DomainLoginStrategyMapper {

  public List<DomainLoginStrategy> listByProfile(@Param(value = "profile") String profile);

  public List<DomainLoginStrategy> listByProfileAndModule(@Param(value = "profile") String profile,
      @Param(value = "module") String module);

  public int insert(@Param(value = "dls") DomainLoginStrategy dls);

  public int update(DomainLoginStrategy dls);

  public int delete(DomainLoginStrategy dls);
}
