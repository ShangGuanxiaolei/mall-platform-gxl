package com.xquark.dal.mapper;

import java.util.List;

import com.xquark.dal.model.SmsVarTpl;

public interface SmsVarTplMapper {

  SmsVarTpl load(String id);

  List<SmsVarTpl> loadAll();
}
