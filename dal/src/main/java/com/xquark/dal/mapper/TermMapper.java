package com.xquark.dal.mapper;

import com.xquark.dal.model.Term;

public interface TermMapper {

  int insert(Term record);

  Term load(String id);

  Term loadByName(String term);

  int deleteTermById(String id);

}
