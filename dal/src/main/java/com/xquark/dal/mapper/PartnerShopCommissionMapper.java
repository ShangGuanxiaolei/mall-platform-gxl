package com.xquark.dal.mapper;

import com.xquark.dal.model.PartnerShopCommission;
import com.xquark.dal.model.TwitterShopCommission;

import java.util.List;

public interface PartnerShopCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(PartnerShopCommission record);

  int insertSelective(PartnerShopCommission record);

  PartnerShopCommission selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerShopCommission record);

  int updateByPrimaryKey(PartnerShopCommission record);

  int updateForArchive(String id);

  List<PartnerShopCommission> selectByShopId(String shopId);

}