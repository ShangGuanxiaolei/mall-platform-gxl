package com.xquark.dal.mapper;

import com.xquark.dal.model.SkuAttributeItem;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface SkuAttributeItemMapper {

  int insert(SkuAttributeItem rule);

  SkuAttributeItem selectByPrimaryKey(@Param("id") String id);

  int modify(SkuAttributeItem role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<SkuAttributeItem> list(@Param(value = "pager") Pageable page,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查找某个属性规格的所有属性值
   */
  List<SkuAttributeItem> findAllByAttributeId(@Param(value = "attributeId") String attributeId);

  /**
   * 查找所有属性值
   */
  List<SkuAttributeItem> findAll();

}
