package com.xquark.dal.mapper;

import com.xquark.dal.model.MainOrder;
import com.xquark.dal.status.MainOrderStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MainOrderMapper {

  int insert(MainOrder record);

  MainOrder selectByPrimaryKey(String id);

  MainOrder selectByPrimaryKeyAndStatus(@Param("id") String id, @Param("status") MainOrderStatus... status);

  Long selectUnPaid(@Param("payNo") String payNo);
  
  List<MainOrder> listPendingByCashierItem(int delaySeconds);
  
  List<MainOrder> listPending(int delaySeconds);

  int updateByPrimaryKeySelective(MainOrder record);

  int updateByPrimaryKeyAndStatus(@Param("order") MainOrder record, @Param("status") MainOrderStatus... from);

  MainOrder selectByOrderNo(String orderNo);

  int updateOrderByPay(@Param("params") Map<String, Object> params,
      @Param("order") MainOrder order);

  List<String> getOrderNoByMain(@Param("orderNo")String orderNo);

  int updateStatusByOrderNo(@Param("orderNo")String orderNo,@Param("status")MainOrderStatus status);

  MainOrder queryMainOrderByPayNo(@Param("bizNo") String bizNo);

  int updateByPrimaryKeySelectiveByNo(MainOrder record);

  String findEsByEso(String Eso);
  
  int cancelMainOrderIfOrderCancelled(@Param("id") String id);
}