package com.xquark.dal.mapper;


import com.xquark.dal.model.MerchantSigninLog;

public interface MerchantSigninLogMapper {

  int insert(MerchantSigninLog merchantSigninLog);
}
