package com.xquark.dal.mapper;

import com.xquark.dal.model.PartnerTypeRelation;
import com.xquark.dal.vo.PartnerTypeRelationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartnerTypeRelationMapper {

  int deleteByPrimaryKey(String id);

  int insert(PartnerTypeRelation record);

  int insertSelective(PartnerTypeRelation record);

  PartnerTypeRelation selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerTypeRelation record);

  int updateByPrimaryKey(PartnerTypeRelation record);

  int updateForArchive(String id);

  /**
   * 获取某个合伙人的类型设置
   */
  List<PartnerTypeRelation> selectByUserId(@Param(value = "shopId") String shopId,
      @Param(value = "userId") String userId);

  /**
   * 删除某个合伙人所有的类型设置
   */
  int deleteByUserId(@Param(value = "shopId") String shopId,
      @Param(value = "userId") String userId);

  /**
   * 得到系统内所有的股份合伙人和平台合伙人
   */
  List<PartnerTypeRelationVO> getAllPlatformAndHolder();

}