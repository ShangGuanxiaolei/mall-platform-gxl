package com.xquark.dal.mapper;

import java.util.List;
import java.util.Map;

import com.xquark.dal.model.Carousel;
import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PostAgeSet;
import org.springframework.data.domain.Pageable;

public interface PostAgeSetMapper {

  PostAgeSet selectByPrimaryKey(String id);

  List<PostAgeSet> selectByShopId(@Param("shopId") String shopId);

  Long insert(PostAgeSet record);

  Long update(PostAgeSet record);

  int deleteByPrimaryKey(String id);

  /**
   * 服务端分页查询数据
   */
  List<PostAgeSet> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

}