package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.ActivityProduct;

public interface ActivityProductMapper {

  ActivityProduct loadById(@Param("id") String id);

  List<ActivityProduct> selectByproductId(@Param("productId") String productId);

  int insertBatch(List<ActivityProduct> list);

  int update(ActivityProduct entity);

  int delete(@Param("id") String id);

  int deleteByIds(@Param("ids") String[] ids);
}