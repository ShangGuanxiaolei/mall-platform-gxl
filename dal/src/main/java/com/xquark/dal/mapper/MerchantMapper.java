package com.xquark.dal.mapper;

import com.xquark.dal.model.Merchant;
import com.xquark.dal.vo.MerchantOrgVO;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

/**
 * Created by quguangming on 16/5/23.
 */
public interface MerchantMapper {

  Merchant loadByLoginName(@Param(value = "loginName") String loginName);

  Merchant selectByPrimaryKey(@Param(value = "id") String id);

  Long insert(Merchant merchant);

  List<Merchant> listByMerchantId(@Param("params") Map<String, Object> params,
      @Param(value = "pager") Pageable pageable);

  List<Merchant> listAll();

  int updateByPrimaryKey(Merchant merchant);

  int deleteByPrimaryKey(@Param(value = "id") String id);

  List<Merchant> listByKeyword(@Param(value = "params") Map<String, Object> params,
      @Param(value = "pager") Pageable pageable);

  Long countMerchants(@Param(value = "params") Map<String, Object> params);

  int countRegistered(@Param(value = "loginName") String loginName);

  int changePwd(@Param("id") String id, @Param("newPwd") String newPwd);

  Long countPhone(@Param(value = "phone") String phone);

  List<MerchantOrgVO> listByOrgId(@Param("orgId") String orgId, @Param("page") Pageable pageable,
      @Param("order") String order,
      @Param("keyword") String keyword, @Param("direction") Direction direction,
      @Param("pageable")boolean ifPage); Integer countByOrgId(@Param("orgId") String orgId);

  Boolean checkPhoneExits(@Param("phone") String phone);

}
