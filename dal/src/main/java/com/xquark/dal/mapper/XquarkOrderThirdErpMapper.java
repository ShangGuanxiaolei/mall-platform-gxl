package com.xquark.dal.mapper;

import com.xquark.dal.model.XquarkOrderThirdErp;
import org.springframework.stereotype.Repository;


public interface XquarkOrderThirdErpMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkOrderThirdErp record);

    int insertSelective(XquarkOrderThirdErp record);

    XquarkOrderThirdErp selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkOrderThirdErp record);

    int updateByPrimaryKey(XquarkOrderThirdErp record);
}