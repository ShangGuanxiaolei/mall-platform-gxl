package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductDesc;


public interface ProductDescMapper {

  int insert(ProductDesc productDesc);

  ProductDesc selectByPrimaryKey(String productId);

  int update(ProductDesc productDesc);

}
