package com.xquark.dal.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.SpiderItem;
import com.xquark.dal.model.SpiderItemImg;
import com.xquark.dal.model.SpiderSku;

public interface TestMapper {

  List<Product> selectProductByThirdNull();

  List<SpiderItem> selectSpiderItemByProductName(@Param("shopId") String shopId,
      @Param("productName") String productName);

  int updateProductThird(@Param("itemId") String itemId, @Param("productId") String productId);

  List<SpiderItem> selectSpiderItem();

  int updateDecodeShopId(@Param("id") Long id, @Param("decodeShopId") Long shopId);

  int updateEncodeShopId(@Param("id") Long id, @Param("encodeShopId") String shopId);

  List<Product> selectProductThirdIds();

  List<Product> selectDuplicateByThird(@Param("shopId") String shopId,
      @Param("thirdId") Long thirdId);

  long selectIsXQ(@Param("productId") String productId);

  long selectIsXQPool(@Param("productId") String productId);

  int delDuplicate(@Param("productId") String productId);

  List<Long> selectProductForEncode();

  int encodeProductId(@Param("encodeId") String encodeId, @Param("productId") Long productId);

  List<Product> selectProductsErrorImg(@Param("itemId") String itemId);

  List<Product> selectReSpiderProducts(@Param("type") String type);

  List<SpiderItem> selectSpiderItemByName(@Param("shopId") String shopId,
      @Param("name") String name);

  List<SpiderItemImg> selectSpiderItemImgs(@Param("spiderId") Long spiderId);

  List<SpiderSku> selectSpiderItemSkus(@Param("spiderId") Long spiderId);

  List<Long> selectxquarkShopIdsByThirdNull();

  List<SpiderItem> selectSpiderItemByShopId(@Param("shopId") String shopId);

  int updateSourceByShopId(@Param("shopId") Long shopId);

  List<String> selectSpiderShop();

  List<SpiderItemImg> selectImgByItemId(@Param("itemId") Long itemId);

  List<Product> selectProductByThirdId(@Param("shopId") String shopId,
      @Param("thirdId") String thirdId);

  ProductImage selectImgByProductId(@Param("productId") String productId);

  long selectShopIdByAdmin(@Param("from") Integer from, @Param("to") Integer to);

  List<SpiderItem> listItemsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  long countItemsByAdmin(@Param(value = "params") Map<String, Object> params);

  List<Product> listProductsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  long countProductsByAdmin(@Param(value = "params") Map<String, Object> params);

  int isSpider(@Param("thirdId") String thirdId);

  int isxquark(@Param("thirdId") String thirdId);

  int updateThirdId(@Param("productId") String productId, @Param("thirdId") String thirdId);

  long checkSpiderShop(@Param("shopId") String shopId);
}
