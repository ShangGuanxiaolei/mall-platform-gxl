package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.RangeIdPair;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface PromotionModelMapper {

  PromotionModel selectByRangeAndObjId(RangeIdPair pair);

  // need order by range desc
  List<PromotionModel> selectByRangesAndObjIds(@Param("pairs") List<RangeIdPair> pairs);

  List<PromotionModel> selectByObjId(String objId);

  List<PromotionModel> selectBySellerId(String sellerId);

  List<PromotionModel> selectByPartner(String sellerId);

  int insert(PromotionModel model);

  int update(PromotionModel model);

  int deleteById(String id);

  int updatePeriodicityDate(String id);
//	int deleteByRangeAndObjId(RuleRange range, String objId);
}
