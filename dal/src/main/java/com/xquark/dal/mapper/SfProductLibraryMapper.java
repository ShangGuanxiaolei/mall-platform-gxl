package com.xquark.dal.mapper;

import com.xquark.dal.model.SfProductLibrary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SfProductLibraryMapper {

  int deleteByPrimaryKey(String id);

  int insert(SfProductLibrary record);

  int insertSelective(SfProductLibrary record);

  SfProductLibrary selectByPrimaryKey(String id);

  List<String> loadAllEncodes();

  int updateByPrimaryKeySelective(SfProductLibrary record);

  int updateByPrimaryKey(SfProductLibrary record);

  void updateProductTemperature(@Param("productId") String productId,@Param("sfairline") String sfairline,
                                @Param("sfshipping") String sfshipping,@Param("merchantNumber") String merchantNumber);
}