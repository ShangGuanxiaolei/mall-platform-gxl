package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductFilter;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductFilterMapper {

  int deleteByPrimaryKey(String id);

  int insert(ProductFilter record);

  int insertSelective(ProductFilter record);

  ProductFilter selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ProductFilter record);

  int updateByPrimaryKey(ProductFilter record);

  boolean selectIsBounded(@Param("productId") String productId, @Param("filterId") String filterId);

  int deleteBounded(@Param("productId") String productId, @Param("filterId") String filterId);

  int deleteByProductId(@Param("productId") String productId);

  List<Long> selectBoundedFilterIds(@Param("productId") String productId);
}