package com.xquark.dal.mapper;

import com.xquark.dal.model.HomeItem;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.vo.HomeItemVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;


public interface HomeItemMapper {

  HomeItemVO selectByPrimaryKey(@Param("id") String id);

  int insert(HomeItem homeItem);

  int modify(HomeItem homeItem);


  /**
   * @param id
   * @return
   */
  int delete(@Param("id") String id);


  /**
   * @return
   */
  List<HomeItemVO> listByApp(@Param("belong") HomeItemBelong belong);

  /**
   * 服务端分页查询数据
   */
  List<HomeItemVO> list(@Param(value = "pager") Pageable pager, @Param("belong") String belong);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param("belong") String belong);

}
