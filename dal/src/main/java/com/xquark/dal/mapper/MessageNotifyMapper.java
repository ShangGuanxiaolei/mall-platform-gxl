package com.xquark.dal.mapper;

import com.xquark.dal.model.MessageNotify;

public interface MessageNotifyMapper {
    MessageNotify selectMessageNotifyByTime();
}
