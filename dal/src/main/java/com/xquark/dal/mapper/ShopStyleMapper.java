package com.xquark.dal.mapper;

import com.xquark.dal.model.ShopStyle;

public interface ShopStyleMapper {

  ShopStyle selectByPrimaryKey(String id);

  int insert(ShopStyle ss);

  int update(ShopStyle ss);

}
