package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionFullCutDiscount;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionFullCutDiscountMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionFullCutDiscount record);

  int insertSelective(PromotionFullCutDiscount record);

  PromotionFullCutDiscount selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionFullCutDiscount record);

  int updateByPrimaryKey(PromotionFullCutDiscount record);

  int deleteByPromotionId(String id);

  List<PromotionFullCutDiscount> listByPromotionId(@Param("id") String id,
      @Param("order") String order, @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  int saveList(@Param("discountList") List<? extends PromotionFullCutDiscount> discountList);

  long countDiscount(String promotionId);

  int deleteInPromotion(@Param("promotionId") String promotionId, @Param("id") String id);

}