package com.xquark.dal.mapper;

import com.xquark.dal.vo.PromotionGroupDetailVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionGroupDetailMapper
 * @date 2018/9/15 0015
 */
public interface PromotionGroupDetailMapper {
    /**
     * 我的拼团详情
      * @param pieceGroupTranCode
     * @return
     */
    List<PromotionGroupDetailVO> selectGroupDetail(
        @Param("pieceGroupTranCode") String pieceGroupTranCode);

    /**
     * 根据pCode查询成团人数
     * @param pCode
     * @return
     */
    Integer findPieceGroupNum(@Param("pCode")String pCode);

    /**
     * 根据pCode查询拼团每人限购数量
     * @param pCode
     * @return
     */
    Integer findPieceSkuLimit(@Param("pCode")String pCode);

    BigDecimal selectOpenMemberPrice(@Param("pDetailCode") String pDetailCode);

    boolean selectFreshPromotionByPCode(@Param("pDetailCode")String pDetailCode);
}
