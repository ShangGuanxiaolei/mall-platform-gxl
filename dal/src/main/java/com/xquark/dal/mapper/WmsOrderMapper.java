package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsOrder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午3:22
 */
public interface WmsOrderMapper {

  /**
   * 添加
   */
  int insert(WmsOrder obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsOrder> collection);

  /**
   * 通过id查找
   */
  WmsOrder getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsOrder obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsOrder> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

  /**
   * 通过orderNo查找
   */
  WmsOrder getByOrderNo(@Param("orderNo") String orderNo);

  /**
   * 通过EdiStatus查询
   */
  List<WmsOrder> getByWmsEdiStatus(Integer ediStatus);

  /**
   * 获取所有的OrderNo
   */
  Set<String> getOrderNo();
}