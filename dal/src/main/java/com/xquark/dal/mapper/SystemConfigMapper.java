package com.xquark.dal.mapper;

import com.xquark.dal.model.SystemConfigDO;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/6/13
 * Time:10:27
 * Des:汉薇配置表
 */
public interface SystemConfigMapper {
    SystemConfigDO getByName(String name);
    String getValueByName(String name);
    boolean existByName(String name);
}