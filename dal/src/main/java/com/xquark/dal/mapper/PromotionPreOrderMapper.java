package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPreOrder;
import com.xquark.dal.vo.PreOrderPromotionOrderVO;
import com.xquark.dal.vo.PreOrderPromotionProductVO;
import com.xquark.dal.vo.PreOrderPromotionUserVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public interface PromotionPreOrderMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionPreOrder record);

  int insertSelective(PromotionPreOrder record);

  boolean inPromotion(@Param("productId") String productId);

  boolean exists(@Param("promotionId") String promotionId, @Param("productId") String productId);

  PromotionPreOrder selectByPrimaryKey(String id);

  PreOrderPromotionUserVO selectUserVOByPrimaryKey(@Param("id") String id,
      @Param("userId") String userId);

  List<PreOrderPromotionOrderVO> listOrderVO(@Param("userId") String userId,
      @Param("order") String order, @Param("direction") Direction direction,
      @Param("page") Pageable pageable);

  Long countOrderVO(String userId);

  PromotionPreOrder selectByPromotionIdAndProductId(@Param("promotionId") String promotionId,
      @Param("productId") String productId);

  PreOrderPromotionProductVO selectVOByPromotionIdAndProductId(
      @Param("promotionId") String promotionId,
      @Param("productId") String productId);

  PreOrderPromotionProductVO selectVOByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionPreOrder record);

  int updateByPrimaryKey(PromotionPreOrder record);

  List<PromotionPreOrder> listPromotionPreOrder(@Param("promotionId") String promotionId,
      @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  List<PreOrderPromotionProductVO> listPromotionPreOrderVO(@Param("promotionId") String promotionId,
      @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  /**
   * 查询所有限购商品
   *
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页对象
   * @param isAvailable 活动目前是否有效
   * @return 限购商品集合
   */
  List<PreOrderPromotionProductVO> listAllPromotionPreOrderVO(@Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable,
      @Param("isAvailable") Boolean isAvailable);

  /**
   * 根据指定条件查询限购商品数量
   *
   * @param promotionId 活动id，传null则查询所有限购商品数量
   * @return 限购商品数量
   */
  Long count(String promotionId);

  Long countAll(@Param("isAvailable") Boolean isAvailable);

  int updateStock(@Param("orderId") String orderId, @Param("qty") int qty);

}