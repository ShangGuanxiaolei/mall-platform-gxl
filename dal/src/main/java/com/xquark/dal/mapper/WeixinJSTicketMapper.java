package com.xquark.dal.mapper;

import com.xquark.dal.model.WeixinJSTicket;
import org.apache.ibatis.annotations.Param;

import java.math.BigInteger;

public interface WeixinJSTicketMapper {

  WeixinJSTicket selectByAppId(@Param("app_id") String app_id);

  int updateTicket(@Param("app_id") String app_id, @Param("access_token") String access_token,
      @Param("js_ticket") String js_ticket, @Param("check_time") long check_time,
      @Param("renew_counter") long renew_counter);
}