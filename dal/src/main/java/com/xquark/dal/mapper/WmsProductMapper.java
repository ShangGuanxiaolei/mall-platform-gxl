package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsProduct;
import com.xquark.dal.vo.LogisticsStockParam;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 上午10:29
 */
public interface WmsProductMapper {

  /**
   * 添加
   */
  int insert(WmsProduct obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsProduct> collection);

  /**
   * 通过id查找
   */
  WmsProduct getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsProduct obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsProduct> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

  /**
   * 根据code获取产品
   */
  WmsProduct getByCode(@Param("code") String code);

  /**
   * 获取code集合
   */
  Set<String> getCodeSet();

  List<LogisticsStockParam> getSFInStock(String orderNo);
}