package com.xquark.dal.mapper;

import com.xquark.dal.model.Timing;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

public interface ActivityEndMapper {
    //查询所有失效的拼团活动的id
    List<Timing> selectActivityCode(Timestamp time);
    //查询是否还有未结束的拼团存在
    int selectPtIsLive(String pcode);
    //下架活动
    int updateActivityStatus(String pcode);

    int updateExclusiveStatus(@Param("pCode")String pCode);

    int updateTempStock(@Param("pCode")String pCode);
}
