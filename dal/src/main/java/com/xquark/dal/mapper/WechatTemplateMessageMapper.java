package com.xquark.dal.mapper;

import com.xquark.dal.model.wechat.TemplateMessage;
import com.xquark.dal.model.wechat.TemplateMessageType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WechatTemplateMessageMapper {

  List<TemplateMessage> list(@Param("shopId") String shopId,
      @Param("wechatAppId") String wechatAppId);

  int insert(TemplateMessage templateMessage);

  int updateByPrimaryKeySelective(TemplateMessage templateMessage);

  TemplateMessage selectByPrimaryKey(@Param("id") String id);

  int updateTemplateId(TemplateMessage templateMessage);

  TemplateMessage selectByTemplateName(@Param("templateName") TemplateMessageType templateName,
      @Param("shopId") String shopId);
}

