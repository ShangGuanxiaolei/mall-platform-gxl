package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.DistributionConfig;

public interface DistributionConfigMapper {

  int insert(DistributionConfig record);

  DistributionConfig selectByPrimaryKey(@Param(value = "id") String id);

  DistributionConfig selectByUser(@Param(value = "masterId") String masterId,
      @Param(value = "promoter") String promoter);

  int updateByPrimaryKeySelective(DistributionConfig record);
}