package com.xquark.dal.mapper;

import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.CarouselImage;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface CarouselImageMapper {

  int deleteByPrimaryKey(String id);

  int insert(CarouselImage record);

  int insertSelective(CarouselImage record);

  CarouselImage selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CarouselImage record);

  int updateByPrimaryKey(CarouselImage record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<CarouselImage> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 获取轮播图明细信息
   */
  List<CarouselImage> getByCarouselId(@Param(value = "carouselId") String carouselId);

  /**
   * 查询某种类型的优惠券的明细个数
   */
  Long countByCarouselId(@Param(value = "carouselId") String carouselId);

}