package com.xquark.dal.mapper;

import com.xquark.dal.model.CollageSmsMessage;

import java.util.List;

/**
 * 拼团消息映射
 *
 * @author tanggb
 * Create in 11:41 2018/9/5
 */
public interface CollageSmsMessageMapper {

    /**
     * 保存短信消息
     *
     * @param collageSmsMessage 拼团短信实体类
     * @return int
     */
    int insert(CollageSmsMessage collageSmsMessage);

    /**
     * 更新重发短信发送状态
     * @param collageSmsMessage 拼团短信实体类
     * @return
     */
    int reUpdate(CollageSmsMessage collageSmsMessage);

    /**
     * 通过id查询短信表信息
     *
     * @param id 短信表id
     * @return 短信实体类
     */
    CollageSmsMessage findById(long id);
}
