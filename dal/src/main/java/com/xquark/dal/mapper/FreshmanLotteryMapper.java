package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshmanLottery;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FreshmanLotteryMapper {

    List<FreshmanLottery> getAllFreshmanLotteryIsOpen();

    int insertFreshmanLotteryLog(Integer currentCpid, Integer point);

    Integer queryFreshmanLotteryCountByCpid(Long currentCpid);
}
