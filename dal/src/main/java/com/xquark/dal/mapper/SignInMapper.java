package com.xquark.dal.mapper;

import com.xquark.dal.model.SignIn;

public interface SignInMapper {

  int deleteByPrimaryKey(String id);

  int insert(SignIn record);

  int insertSelective(SignIn record);

  SignIn selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(SignIn record);

  int updateByPrimaryKey(SignIn record);

  /**
   * 获取用户最后一次签到
   *
   * @param userId 用户id
   * @return 最后一次签到数据
   */
  SignIn selectLastModifiedByUser(String userId);

  /**
   * 获取用户所有签到周期中的最后一个周期的第一次签到
   *
   * @param userId 用户id
   * @return 最后一次签到数据
   */
  SignIn selectLastCycleBegin(String userId);
}