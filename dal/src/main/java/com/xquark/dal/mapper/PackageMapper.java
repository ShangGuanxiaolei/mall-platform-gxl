package com.xquark.dal.mapper;

import com.xquark.dal.model.Package;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-13. Time: 下午5:52 产品包装箱
 */
public interface PackageMapper {

  /**
   * 添加
   */
  int insert(Package obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<Package> collection);

  /**
   * 通过id查找
   */
  Package getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(Package obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<Package> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

    Integer updateByName(Package packageTo);

    List<String> getName();
}