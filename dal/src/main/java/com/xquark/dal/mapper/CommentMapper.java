package com.xquark.dal.mapper;

import com.xquark.dal.model.Comment;
import com.xquark.dal.model.CommentExample;
import com.xquark.dal.type.CommentType;
import com.xquark.dal.vo.CommentManagment;
import com.xquark.dal.vo.CommentVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface CommentMapper {

  long countByExample(CommentExample example);

  int deleteByExample(CommentExample example);

  int deleteByPrimaryKey(String id);

  int insert(Comment record);

  int insertSelective(Comment record);

  List<Comment> selectByExample(CommentExample example);

  Comment selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") Comment record,
      @Param("example") CommentExample example);

  int updateByExample(@Param("record") Comment record, @Param("example") CommentExample example);

  int updateByPrimaryKeySelective(Comment record);

  int updateByPrimaryKey(Comment record);

  void subtractLikeCount(String commentId);

  void plusLikeCount(String commentId);

  /**
   * 获取移动端页面上的评论数据
   */
  List<CommentVO> getCommentVOS(@Param("objId") String objId, @Param("userId") String userId,
      @Param("start") Integer start, @Param("offset") int offset);

  //List<CommentManagment> manageComments(@Param("type")CommentType type, @Param("blocked")Boolean blocked, @Param("desc")Boolean desc, @Param("start")Integer start, @Param("offset") Integer offset);

  void blockComment(@Param("id") String id, @Param("isBlocked") int isBlocked);

  /**
   * 获取pc后台使用的评论数据
   */
  List<CommentManagment> getCommentList(@Param("type") CommentType type,
      @Param("blocked") Boolean blocked, @Param("desc") Boolean descByTime,
      @Param("page") Pageable page);

  /**
   * 获取返回给后台的评论数据的条数
   */
  Integer getCommentCount(@Param("type") CommentType type, @Param("blocked") Boolean blocked,
      @Param("desc") Boolean descByTime, @Param("page") Pageable page);

  List<CommentVO> listCommentByProductIdAndStar(@Param("productId") String productId,
      @Param("star") Integer star, @Param("page") Pageable pageable);

  Long countCommentByProductIdAndStar(@Param("productId") String productId,
      @Param("star") Integer star);
}
