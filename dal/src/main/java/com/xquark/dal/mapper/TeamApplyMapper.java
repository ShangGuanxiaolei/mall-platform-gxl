package com.xquark.dal.mapper;

import com.xquark.dal.model.TeamApply;
import com.xquark.dal.vo.TeamApplyVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TeamApplyMapper {

  int deleteByPrimaryKey(String id);

  int insert(TeamApply record);

  int insertSelective(TeamApply record);

  TeamApply selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamApply record);

  int updateByPrimaryKey(TeamApply record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<TeamApplyVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 战队禁用，启用
   */
  int updateStatus(@Param(value = "id") String id, @Param(value = "status") String status);

  /**
   * 根据userid查询意向用户信息
   */
  TeamApply selectByUserId(@Param(value = "userId") String userId);

}