package com.xquark.dal.mapper;

import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.vo.UserCouponVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserCouponMapper {


  int insert(UserCoupon userCoupon);

  List<UserCouponVo> listUserCoupon(@Param("params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  Long countUserCoupons(@Param("params") Map<String, Object> params);

  Long countPromotionCoupons(@Param("statusArr") String[] statusArr,
      @Param("promotionCouponId") String promotionCouponId);

  List<UserCoupon> selectCouponListByUserId(@Param("uId") String uId);

  Long countAcquirePromotionCoupons(@Param("userId") String userId,
      @Param("promotionCouponId") String promotionCouponId);

  List<UserCouponVo> selectValidCouponList(@Param("params") Map<String, Object> params);

  int closeByPromotionId(@Param("promotionCouponId") String promotionCouponId,
      @Param("mStatus") String mStatus, @Param("statusArr") String[] statusArr);

  int updateUserCouponById(UserCoupon userCoupon);

  UserCoupon selectByPrimaryKey(@Param("id") String id);

  List<UserCouponVo> selectUserCouponList(@Param("params") Map<String, Object> params,
      @Param("statusArr") String[] statusArr);

  List<UserCouponVo> selectUserCouponListPage(@Param("page") Pageable pageable,
      @Param("params") Map<String, Object> params, @Param("statusArr") String[] statusArr);

  Long updateStatusByCouponId(@Param("promotionCouponId") String promotionCouponId,
      @Param("status") String status);

  void addCode(@Param("id") String id, @Param("code") String code);
}
