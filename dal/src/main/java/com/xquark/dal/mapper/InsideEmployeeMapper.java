package com.xquark.dal.mapper;

import com.xquark.dal.model.InsideEmployee;
import org.apache.ibatis.annotations.Param;

public interface InsideEmployeeMapper {

  int deleteByPrimaryKey(Long id);

  int insert(InsideEmployee record);

  int insertSelective(InsideEmployee record);

  InsideEmployee selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(InsideEmployee record);

  int updateByPrimaryKey(InsideEmployee record);

  boolean isInsideEmployee(@Param("id") Long id, @Param("name") String name);

  int updateEmployeeId(@Param("id") Long id, @Param("newId") Long newId);

  int updateEmployeeIdQinyuan(@Param("id") Long id, @Param("newId") Long newId);
}