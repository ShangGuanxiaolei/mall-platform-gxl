package com.xquark.dal.mapper;

import com.xquark.dal.model.UserCard;
import com.xquark.dal.status.FamilyCardStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserCardMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(UserCard record);

  int insertSelective(UserCard record);

  UserCard selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserCard record);

  int updateByPrimaryKey(UserCard record);

  List<UserCard> selectUserCardsByStatus(FamilyCardStatus status);

  UserCard selectUserCardsByUserIdAndShopId(@Param("userId") String userId,
      @Param("shopId") String shopId);

  UserCard selectUserCardsByUserId(String userId);

  List<UserCard> listUserCardsByShopId(@Param("shopId") String shopId);

  String selectCardIdByUserId(@Param("userId") String userId);
}