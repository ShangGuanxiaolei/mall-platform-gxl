package com.xquark.dal.mapper;

import com.xquark.dal.model.MemberPromotion;
import com.xquark.dal.type.MemberPromotionStatus;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface MemberPromotionMapper {

  int deleteByPrimaryKey(String id);

  int insert(MemberPromotion record);

  MemberPromotion selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(MemberPromotion record);

  int updateByPrimaryKey(MemberPromotion record);

  List<MemberPromotion> list(@Param("page") Pageable pageable,
      @Param("status") MemberPromotionStatus status);

  Long count(MemberPromotionStatus status);
}