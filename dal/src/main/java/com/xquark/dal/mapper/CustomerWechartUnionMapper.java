package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerWechatUnion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerWechartUnionMapper {

  int insert(CustomerWechatUnion customerWechatUnion);

  void update(CustomerWechatUnion customerWechatUnion);

  CustomerWechatUnion isExsitWeChat(CustomerWechatUnion customerWechatUnion);

  String selectAvatarByUnionIdAndCpId(@Param("cpId") Long cpId, @Param("unionId") String unionId);

  List<String> selectKeysByCpId(@Param("cpId") Long cpId);

  CustomerWechatUnion selectRandomOne(@Param("cpId") Long cpId);
}