package com.xquark.dal.mapper;

import com.xquark.dal.model.WareHouse;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA. User: shihao Date: 18-6-14. Time: 上午10:06 ${DESCRIPTION}
 */
public interface WareHouseMapper {


  /**
   * 添加
   */
  int insert(WareHouse record); //插入仓库名


  /**
   * 通过id查找
   */
  WareHouse getByPrimaryKey(@Param("id") String id);  //查询仓库名

  WareHouse getByName(@Param("name") String name);


  int updateByPrimaryKeySelective(WareHouse house);

  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);  // 删除仓库

    /**
     * 分页查询
     */
    List<WareHouse> list(@Param("params") Map<String, Object> params);


    /**
     * 计算数据的数量
     */
    Long count(@Param("params") Map<String, Object> params);

  List<WareHouse> getNameAndId();
}
