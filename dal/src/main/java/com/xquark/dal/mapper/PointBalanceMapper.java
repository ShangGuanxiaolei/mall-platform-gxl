package com.xquark.dal.mapper;

import com.xquark.dal.vo.PointExportByCustomerVO;
import com.xquark.dal.vo.PointExportByOrderVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuwei
 * @date 18-7-26 下午7:13
 */
public interface PointBalanceMapper {
    /**
     * 查询订单对应视图的收入列表
     * @return
     */
    List<PointExportByOrderVO> selectPointExportByOrder(@Param("month")Integer month);

    /**
     * 查询客户对应视图的收入列表
     * @return
     */
    List<PointExportByCustomerVO> selectPointExportByCustomer(@Param("month")Integer month);
}
