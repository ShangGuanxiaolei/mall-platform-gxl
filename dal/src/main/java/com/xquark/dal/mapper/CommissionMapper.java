package com.xquark.dal.mapper;

import com.xquark.dal.model.Commission;
import com.xquark.dal.model.MonthWithdraw;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.CommissionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface CommissionMapper {

  int insert(Commission record);

  int insert4Partner(Commission record);

  int finish(@Param("orderId") String orderId);

  void updateStatus(@Param("orderId") String orderId, @Param("status") CommissionStatus status);

  int updateOffered(@Param("ids") String[] ids, @Param("offered") Boolean offered);

  Commission selectByOrderItem(String id);

  Commission loadByOrderItemAndUserId(@Param("id") String id, @Param("userId") String userId);

  List<Commission> listByOrderId(String orderId);

  List<Commission> listCanWithdraw(@Param("day") Integer day);

  List<Commission> listCanWithdrawTest();

  Long countCommissionsByAdmin(@Param(value = "params") Map<String, Object> params);

  List<CommissionVO> listCommissionsByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  void cleanCommissionByOrderId(@Param("orderId") String orderId);

  void cleanCommissionByOrderIdAndType(@Param("orderId") String orderId,
      @Param("type") CommissionType type);

  List<Commission> listByOrderIdAndType(String orderId, CommissionType commissionType);

  /***
   * 查询某个用户当月佣金
   * @param userId
   * @return
   */
  BigDecimal getFeeByMonth(@Param("userId") String userId);

  List<CommissionVO> listOrderNoByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Commission getOrderItemCommission(String orderId, String orderItemId, String userId);

  /**
   * 查询用户本年度每个月的佣金
   */
  List<MonthWithdraw> getFeeOfMonths(String userId);

  /**
   * 2b获取累计收益
   */
  BigDecimal getWithdrawAll(String userId);

  /**
   * 2b获取指定月份的收益
   */
  BigDecimal getWithdrawOfMonth(String userId, String time);

  /**
   * 2b查询收益订单列表的orderNo
   */
  List<CommissionVO> listSuccessOrderNo(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  /**
   * 获取有收益的订单数
   */
  long countNumberByDate(@Param(value = "params") Map<String, Object> params);

  /**
   * 获取卖出的商品数量
   */
  long getSaleProductsByDate(@Param(value = "params") Map<String, Object> params);

  /**
   * 今日收益
   */
  BigDecimal getTodayWithdraw(@Param("userId") String userId);

  /**
   * 本月收益
   */
  BigDecimal getMonthWithdraw(@Param("userId") String userId);

  /**
   * 我的收益，根据类型分页获取明细收益信息
   */
  List<CommissionVO> listByApp(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);
}
