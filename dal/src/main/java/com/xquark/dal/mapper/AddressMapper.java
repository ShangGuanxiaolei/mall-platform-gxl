package com.xquark.dal.mapper;

import com.xquark.dal.model.Address;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AddressMapper {

  int insert(Address record);

  Address selectById(String id);

  boolean isExists(String id);

  Address selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Address record);

  int updateByExtIdSelective(Address record);

  Address selectUserAddress(String userId, String addressId);

  Address selectUserDefault(Long cpId);

  Address selectUserFirst(Long cpId);

  List<Address> selectUserAddresses(String userId);

  int updateForArchive(String addressId);

  int updateForUnArchive(String addressId);

  Address selectOrderAddress(String orderId);

  Address selectLatestUserAddresses(String userId);

  void updateForUnCommon(String id);

  int setDefault(@Param("userId") String userId, @Param("id") String addressId);

  int setDefaultByPrimaryKey(@Param("id") String id);

  void emptyDefault(String userId);

  List<Address> getDefault(String userId);

  /**
   * 以下sql是批量修改默认地址
   */
  List<Map<String, Object>> getTempResult();

  void updatempDefault(@Param("map") Map<String, Object> map);

  /**
   * consignee，phone,zoneId,street信息这些信息是否有存在的address
   */
  Address selectByInfo(@Param("userId") String userId, @Param("consignee") String consignee,
      @Param("phone") String phone, @Param("zoneId") String zoneId, @Param("street") String street);

  int updateExtIdByPrimaryKey(@Param("id") String id, @Param("extId") Integer extId);

  Integer selectExtIdByPrimaryKey(String id);

  List<Address> getAddressById(String id);
}