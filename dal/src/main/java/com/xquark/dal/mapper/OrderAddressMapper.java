package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderAddress;

public interface OrderAddressMapper {

  int insert(OrderAddress record);

  OrderAddress selectByPrimaryKey(String id);

  OrderAddress selectByOrderId(String orderId);

  //int updateForArchive(String addressId);

  int update(OrderAddress orderAddress);

}