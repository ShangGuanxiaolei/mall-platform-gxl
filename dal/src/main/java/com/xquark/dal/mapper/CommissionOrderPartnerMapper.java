package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionOrderPartner;

import java.util.List;

public interface CommissionOrderPartnerMapper {

  int deleteByPrimaryKey(String id);

  int insert(CommissionOrderPartner record);

  int insertSelective(CommissionOrderPartner record);

  CommissionOrderPartner selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CommissionOrderPartner record);

  int updateByPrimaryKey(CommissionOrderPartner record);

  List<CommissionOrderPartner> selectByShopId(String ownShopId);

}