package com.xquark.dal.mapper;


import com.xquark.dal.model.CustomerProfileAudit;

public interface CustomerProfileAuditMapper {

  int insert(CustomerProfileAudit record);
}