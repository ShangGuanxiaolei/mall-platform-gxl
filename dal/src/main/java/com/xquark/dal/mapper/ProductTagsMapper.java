package com.xquark.dal.mapper;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductTags;
import com.xquark.dal.model.Tags;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface ProductTagsMapper {

  int deleteByPrimaryKey(String id);

  int deleteByProductId(String productId);

  int insert(ProductTags record);

  int insertSelective(ProductTags record);

  ProductTags selectByPrimaryKey(String id);

  List<Tags> listTagsByProductId(@Param("productId") String productId,
      @Param("page") Pageable pageable);

  List<Product> listProductByTagIds(@Param("page") Pageable pageable, @Param("ids") String... ids);

  List<Product> listProductByTagNames(@Param("page") Pageable pageable,
      @Param("ids") String... ids);

  int updateByPrimaryKeySelective(ProductTags record);

  int updateByPrimaryKey(ProductTags record);

  boolean selectTagged(@Param("productId") String productId, @Param("tagId") String tagId);

  boolean unTag(@Param("productId") String productId, @Param("name") String name);
}