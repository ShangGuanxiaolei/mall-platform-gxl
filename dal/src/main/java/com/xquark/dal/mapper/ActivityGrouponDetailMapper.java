package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.vo.ActivityGrouponDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ActivityGrouponDetailMapper {

  ActivityGrouponDetail selectByPrimaryKey(@Param("id") String id);

  int insert(ActivityGrouponDetail activityGrouponDetail);

  List<ActivityGrouponDetailVO> selectGrouponUsers(String activityGrouponId);

  int selectUserInGroupon(String userId);

}
