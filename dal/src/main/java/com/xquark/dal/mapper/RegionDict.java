package com.xquark.dal.mapper;

import com.xquark.dal.BaseEntityImpl;
import org.apache.ibatis.annotations.Param;

/**
 * @auther liuwei
 * @date 2018/6/7 0:20
 */
public class RegionDict {

  private String showName;//展示名称
  private int regionId;//区域id
  private boolean regionType;//区域类型
  private String regionName;//区域名称
  private int parentId;//父区域id

  public RegionDict(String showName, int regionId,
      boolean regionType, String regionName,
      int parentId) {
    this.showName = showName;
    this.regionId = regionId;
    this.regionType = regionType;
    this.regionName = regionName;
    this.parentId = parentId;
  }

  public RegionDict() {
  }

  public String getShowName() {
    return showName;
  }

  public void setShowName(String showName) {
    this.showName = showName;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  public boolean isRegionType() {
    return regionType;
  }

  public void setRegionType(boolean regionType) {
    this.regionType = regionType;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public int getParentId() {
    return parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }
}
