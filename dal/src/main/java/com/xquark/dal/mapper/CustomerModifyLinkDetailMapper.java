package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerModifyLinkDetail;
import org.apache.ibatis.annotations.Param;

public interface CustomerModifyLinkDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CustomerModifyLinkDetail record);

    int insertSelective(CustomerModifyLinkDetail record);

    CustomerModifyLinkDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CustomerModifyLinkDetail record);

    int updateByPrimaryKey(CustomerModifyLinkDetail record);

    CustomerModifyLinkDetail selectUpgradeDetail(@Param("cpid") Long cpid,
                                                 @Param("toDay") String toDay);
}