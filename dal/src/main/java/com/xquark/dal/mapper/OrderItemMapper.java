package com.xquark.dal.mapper;

import com.xquark.dal.model.ErpImportOrderExcel;
import com.xquark.dal.model.NotErpImportOrderExcel;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.AmountVo;
import com.xquark.dal.vo.OrderItemVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface OrderItemMapper {

  int deleteByPrimaryKey(String id);

  int insert(OrderItem record);

  int insertChangeGoods(OrderItem record);

  OrderItem selectByPrimaryKey(String id);

  OrderItem selectByPrimaryKey2(String id);

  int updateByPrimaryKeySelective(OrderItem record);

  int updateByPrimaryKey(OrderItem record);

  List<OrderItem> selectByOrderId(String orderId);

  List<OrderItem> selectByOrderIdAndPT(String orderId);

  List<OrderItemVO> selectVOByOrderId(@Param(value = "orderId") String orderId);

  List<OrderItemVO> selectVOByOrderIdAndUserId(@Param(value = "orderId") String orderId,
      @Param(value = "userId") String userId);
  /*
   * @Author chp
   * @Description //查询导入订单列表
   * @Date
   * @Param
   * @return
   **/
 List<Map<String,Object>>  getOrderList(
         @Param("orderStatus")    String   orderStatus, //订单状态
         @Param("thirdOrderNo")   String   thirdOrderNo,//第三方订单号
         @Param("hvOrderNo")      String   hvOrderNo,//hv订单号
         @Param("source")         String   source ,//source
         @Param("startTime")      Date startTime,//查询起始时间
         @Param("endTime")        Date   endTime , //查询结束时间
         @Param("page")  Pageable page
 );

 int    getOrderListCount(
         @Param("orderStatus")    String   orderStatus, //订单状态
         @Param("thirdOrderNo")   String   thirdOrderNo,//第三方订单号
         @Param("hvOrderNo")      String   hvOrderNo,//hv订单号
         @Param("source")         String   source ,//source
         @Param("startTime")      Date startTime,//查询起始时间
         @Param("endTime")        Date   endTime , //查询结束时间
         @Param("page")  Pageable page
    );

 List<Map<String,Object>> getInfoDetail(String lot);
  /*
   * @Author chp
   * @Description //查询订单审核列表
   * @Date
   * @Param
   * @return
   **/
  List<Map<String,Object>>  getCheckList(
          @Param("orderStatus")    String   orderStatus, //订单状态
          @Param("source")         String   source ,//source
          @Param("startTime")      Date startTime,//查询起始时间
          @Param("endTime")        Date   endTime , //查询结束时间
          @Param("page")  Pageable page
  );
    int  getCheckListCount(
            @Param("orderStatus")    String   orderStatus, //订单状态
            @Param("source")         String   source ,//source
            @Param("startTime")      Date startTime,//查询起始时间
            @Param("endTime")        Date   endTime , //查询结束时间
            @Param("page")  Pageable page
    );

  int updateCheck(@Param("array") String[] array,@Param("status") String status);

  int updateOrderStatue(@Param("array") String[] array);

  List<Map<String,Object>>  getInfoList(
          @Param("result")         String   result ,//source
          @Param("startTime")      Date startTime,//查询起始时间
          @Param("endTime")        Date   endTime , //查询结束时间
          @Param("page")  Pageable page);

  int  getInfoListCount(
            @Param("result")         String   result ,//source
            @Param("startTime")      Date startTime,//查询起始时间
            @Param("endTime")        Date   endTime , //查询结束时间
            @Param("page")  Pageable page);

  List<Map<String,Object>> getInfoListErp(
          @Param("lot") String lot,@Param("page") Pageable page
  );

    List<Map<String,Object>> getInfoListNotErp(
            @Param("lot") String lot,@Param("page") Pageable page
    );

   String getErp(@Param("lot") String lot);


   int getInfoListErpCount(
            @Param("lot") String lot
    );
    int getInfoListNotErpCount(
            @Param("lot") String lot,@Param("page") Pageable page
    );



    /*
     * @Author chp
     * @Description //fei  erp 查看详情
     * @Date
     * @Param
     * @return
     **/
    List<NotErpImportOrderExcel> getInfoListNotErpExcel(
            @Param("lot") String lot
    );

    List<ErpImportOrderExcel> getInfoListErpExcel(
            @Param("lot") String lot
    );


    List<Map<String,Object>>  getErpWl(String[] array);
    List<Map<String,Object>>  getErpWlAll();
 /*
  * @Author chp
  * @Description  导出非erp的物流信息
  * @Date
  * @Param
  * @return
  **/
  List<Map<String,Object>>  getNotErpWl(String[] array);
    List<Map<String,Object>>  getNotErpWlAll();
    /*
     * @Author chp
     * @Description     查询订单是否重复传入
     * @Date
     * @Param
     * @return
     **/
   List<String>     getThirdItemList(@Param("orderList") List<String>  orderList);

  /*
   * @Author chp
   * @Description   查询导入商品的sku是否存在
   * @Date
   * @Param
   * @return
   **/
    List<String>     getSkuList(@Param("skuList") List<String>  skuList);
      int getSkuList2(String skuCode);
    /*
     * @Author chp
     * @Description  查询sku库存是否满足要求
     * @Date
     * @Param
     * @return
     **/
   List<String>     getAmountSku(@Param("skuCode") String skuCode,@Param("amount")Integer amount);

   /*
    * @Author chp
    * @Description
    * @Date
    * @Param
    * @return
    **/
    List<String>    getAmountCheck(@Param("skuCode") String skuCode,@Param("amount")Integer damount);

    List<Map<String,Object>> getSkuCount(@Param("lot") String lot);

     int  updateSkuAmount(@Param("skuId") Long skuId,@Param("amount") Integer  amount);
     int getSkuAmountCheck(@Param("skuId") Long skuId);
    int  updateOrderErp(@Param("array") String[] array);

    int  updateOrderErpMain(@Param("array") String[] array);

    Boolean getCpid(Long cpid);
}

