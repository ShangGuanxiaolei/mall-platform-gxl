package com.xquark.dal.mapper;

import com.xquark.dal.model.FootOrder;
import com.xquark.dal.model.FootOrderItem;
import com.xquark.dal.model.VisitorInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface FootprintMapper {

    List<VisitorInfo> selectCustomerList(Integer cpId, String date);

    //根据分享人cpid和日期查询所有的下级用户
    List<String> selectConsumersByDate(String cpId, String date);

    //根据上级的cpid和日期查询所有的下级用户
    List<String> selectConsumersByDateAndCpid(String cpId, String date);

    //根据用户的cpid查询订单判断该用户是否是新人
    Long countOrderNumberByCpid(@Param("cpId") String cpId);

    //根据用户的cpid查询级别判断该用户是否白人
    String selectCareerlevel(@Param("cpId") String cpId);

    //根据下级用户的cpid查询常用收件人
    String selectReceiverName(@Param("cpid") String cpid);

    //根据下级用户的cpid查询游客信息
    VisitorInfo selectVisitorInfoByCpid(@Param("cpid") String cpid);

    //根据当前用户的cpid查询三个月内所有的下级用户
    List<String> selectAllConsumerCpid(String cpId, Date month);

    //根据当前用户的cpid和下级用户id获取游客日期
    List<String> selectDateByCpid(@Param("currentCpid") String currentCpid, @Param("consumerCpids") List<String> consumerCpids);

    //查询下级的buyerid
    List<String> selectLowerConsumerByCustomer(@Param("cpId") String cpId);

    List<String> selectOrderTimeByBuyerIds(@Param("buyerIds") List<String> buyerIds,@Param("month") Date month,@Param("currentCpid") String currentCpid);

    VisitorInfo selectConsumerDetail(@Param("cpId")String cpId,@Param("currentCpid")String currentCpid);
    //根据当前登入人的身份查询该顾客所产生的收益
    BigDecimal selectTotalIncome(@Param("consumerCpid")String consumerCpid, @Param("hasServerCharge")Boolean hasServerCharge,@Param("currentCpid")String currentCpid);


    /**
     * 根据用户id和orderNo查询用户购买的订单list
     */
    List<FootOrder> selectOrderInfoByCpIdAndOrderNo(@Param("cpId")Long cpId,@Param("fromCpId")Long fromCpId,@Param("paidAt")String paidAt, @Param("page")int page, @Param("pageSize")int pageSize);

    /**
     * 顾客订单列表, 当前顾客的所有订单list
     * @param cpId
     * @param fromCpId
     * @param page
     * @param pageSize
     * @return
     */
    List<FootOrder> selectOrderInfoByCpId(@Param("cpId")Long cpId,@Param("fromCpId")Long fromCpId,@Param("page")int page, @Param("pageSize")int pageSize);
    /**
     * 根据用户id和orderNo查询用户购买的订单的商品信息
     */
    List<FootOrderItem> selectOrderProductInfoByCpIdAndOrderNo(@Param("cpId")Long cpId, @Param("orderNo")String orderNo);

    /**
     * 顾客订单列表, 当前顾客的所有订单项List
     * @param cpId
     * @return
     */
    List<FootOrderItem> selectOrderProductInfoByCpId(@Param("cpId")Long cpId);
    /**
     * 根据用户id和orderNo查询用户购买的订单总数
     */
    int selectOrderNumByCpIdAndOrderNo(@Param("cpId")Long cpId,@Param("fromCpId")Long fromCpId,@Param("paidAt")String paidAt);

    /**
     * 顾客订单列表, 统计当前顾客的所有订单数
     * @param cpId
     * @param fromCpId
     * @return
     */
    int selectOrderNumByCpId(@Param("cpId")Long cpId,@Param("fromCpId")Long fromCpId);

    /**
     * 根据cpid查询所有支付完成的订单数量
     * @param cpId
     * @param date
     * @return
     */
    int selectOrderAllCount(Integer cpId, String date);

    List<VisitorInfo> selectOrderCountIfNUll(Integer cpId, String date);

    Long alreadyBuyOrderCount(@Param("cpId") String cpId);


}
