package com.xquark.dal.mapper;

import com.xquark.dal.model.XquarkOrderThirdInfo;
import com.xquark.dal.model.XquarkOrderThirdInfoWithBLOBs;

public interface XquarkOrderThirdInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkOrderThirdInfoWithBLOBs record);

    int insertSelective(XquarkOrderThirdInfoWithBLOBs record);

    XquarkOrderThirdInfoWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkOrderThirdInfoWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(XquarkOrderThirdInfoWithBLOBs record);

    int updateByPrimaryKey(XquarkOrderThirdInfo record);
}