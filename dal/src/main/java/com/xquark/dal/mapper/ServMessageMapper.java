
package com.xquark.dal.mapper;

import com.xquark.dal.status.ServMessageStatus;

import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface ServMessageMapper {

    /**
     * 拼团成功添加消息至数据库中
     * @param content
     * @param typeMsg
     * @param status
     * @param cpId
     * @param type
     * @return
     */

    Integer message(@Param("content") String content, @Param("typeMsg") String typeMsg, @Param("status") String status, @Param("cpId") String cpId, @Param("type") String type);






    List<String> memberList(@Param("tranCode") String tranCode);

    String selectMemberId(@Param("buyerId") String buyerId);


    /**
     * 根据orderId查询cpid
     * @param orderNo
     */
    String selectCpidByOrderNo(@Param("orderNo") String orderNo);


    /**
     * 通过cpid和团code查询订单号
     * @param memberId
     * @param tranCode
     * @return
     */
    String selectOrderNo(@Param("memberId")String memberId,@Param("tranCode")String tranCode);


}