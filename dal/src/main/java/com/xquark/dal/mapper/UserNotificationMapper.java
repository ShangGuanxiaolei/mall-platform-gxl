package com.xquark.dal.mapper;

import com.xquark.dal.model.UserNotification;
import java.util.List;

public interface UserNotificationMapper {

  int deleteByPrimaryKey(String id);

  int insert(UserNotification record);

  int insertSelective(UserNotification record);

  UserNotification selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserNotification record);

  int updateByPrimaryKey(UserNotification record);

  List<String> listUSerIdByMsgId(String msgId);
}