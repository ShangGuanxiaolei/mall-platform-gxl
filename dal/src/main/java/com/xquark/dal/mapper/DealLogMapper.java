package com.xquark.dal.mapper;

import java.util.List;

import com.xquark.dal.model.Deal;
import com.xquark.dal.model.DealLog;

public interface DealLogMapper {

  int insert(DealLog record);

  Deal selectByPrimaryKey(String id);

  List<DealLog> selectByDealId(String dealId);

}
