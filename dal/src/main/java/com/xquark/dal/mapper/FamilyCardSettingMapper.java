package com.xquark.dal.mapper;

import com.xquark.dal.model.FamilyCardSetting;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FamilyCardSettingMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(FamilyCardSetting record);

  int insertSelective(FamilyCardSetting record);

  FamilyCardSetting selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(FamilyCardSetting record);

  int updateByPrimaryKey(FamilyCardSetting record);

  List<FamilyCardSetting> selectByShopId(String shopId);

  FamilyCardSetting selectSilverCardByShopId(String shopId);
}