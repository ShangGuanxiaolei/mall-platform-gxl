package com.xquark.dal.mapper;

import com.xquark.dal.model.Customerluckylist;

public interface CustomerluckylistMapper {
    int deleteByPrimaryKey(Long luckyid);

    int insert(Customerluckylist record);

    int insertSelective(Customerluckylist record);

    Customerluckylist selectByPrimaryKey(Long luckyid);

    Customerluckylist selectByTinCode(String tincode);

    boolean selectCpIdExists(Long cpid);

    int updateByPrimaryKeySelective(Customerluckylist record);

    int updateByPrimaryKey(Customerluckylist record);
}