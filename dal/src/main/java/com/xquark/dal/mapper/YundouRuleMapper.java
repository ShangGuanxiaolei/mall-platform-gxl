package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import com.xquark.dal.vo.YundouRuleVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
public interface YundouRuleMapper {

  Integer insert(YundouRule yundouRule);

  Integer insertDetail(YundouRuleDetail ruleDetail);

  Integer deleteById(@Param("id") String id);

  Integer deleteByCode(@Param("code") Integer code);

  Integer deleteDetail(@Param("ruleId") String ruleId);

  Integer deleteByOperationId(@Param("operationId") String operationId);

  Integer updateById(YundouRule yundouRule);

  Integer updateDetail(YundouRuleDetail ruleDetail);

  YundouRule selectById(@Param("id") String id);

  YundouRuleVO selectYundouRuleVO(@Param("id") String id);

  YundouRuleDetail selectDetail(@Param("ruleId") String ruleId);

  YundouRuleDetail selectAmount(@Param("ruleId") String ruleId, @Param("roleId") String roleId);

  YundouRule selectByName(@Param("name") String name);

  List<YundouRule> selectAll(@Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") Sort.Direction direction);

  List<YundouRuleVO> selectDetailList(@Param("operationId") String operationId,
      @Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") Sort.Direction direction);

  List<YundouRule> selectByOperationId(@Param("operationId") String operationId,
      @Param("order") String order,
      @Param("page") Pageable pageable, @Param("direction") Sort.Direction direction);

  List<YundouRule> selectByOperationCode(@Param("code") Integer code, @Param("order") String order,
      @Param("page") Pageable pageable, @Param("direction") Sort.Direction direction);

  Integer count();

  Integer countByOperation(String operationId);
}
