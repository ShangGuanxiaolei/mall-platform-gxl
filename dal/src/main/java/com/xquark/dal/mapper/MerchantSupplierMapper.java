package com.xquark.dal.mapper;

import com.xquark.dal.model.MerchantSupplier;
import org.apache.ibatis.annotations.Param;

/**
 * Created by IntelliJ IDEA. User: huangjie Date: 2018/7/2. Time: 下午8:10 ${DESCRIPTION}
 */
public interface MerchantSupplierMapper extends  BaseMapper<MerchantSupplier>{

   MerchantSupplier getByMerchantId(@Param("id") String id);

}
