package com.xquark.dal.mapper;

import com.xquark.dal.model.ViewComponent;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface ViewComponentMapper {

  ViewComponent selectByPrimaryKey(@Param("id") String id);

  int insert(ViewComponent viewComponent);

  int modifyViewComponent(ViewComponent viewComponent);

  int delete(@Param("id") String id);

  /**
   * 列表查询页面渲染组件
   */
  List<ViewComponent> list(@Param(value = "pager") Pageable page, @Param("keyword") String keyword);

  /**
   * 对分页页面渲染组件列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

}
