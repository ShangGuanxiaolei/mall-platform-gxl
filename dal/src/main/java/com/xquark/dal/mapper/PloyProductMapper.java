package com.xquark.dal.mapper;

import com.xquark.dal.model.GoodInfo;
import com.xquark.dal.model.Skus;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PloyProductMapper {
//    int deleteByPrimaryKey(Long id);

//    int insert(PloyProduct record);

    /**
     * 统计菠萝派的产品
     * @return
     */
    int countTotalPolyProduct();

//    int insertSelective(PloyProduct record);

    /**
     * 菠萝派产品的分页查询
     * @param offset 偏移量
     * @param PageSize 页数
     *
     * @return
     */
    List<GoodInfo> limitSelect(@Param("offset") Integer offset,
        @Param("PageSize") Integer PageSize);

    /**
     * 产品的SKU
     * @return 产品Sku的集合
     */
    List<Skus> selectSku(@Param("id") String id);

  /**
   * 更新商品库存
   * @param quantity
   */
  void updateNewQuantity(@Param("quantity") Integer quantity,@Param("id") Integer id);

    /**
     * 菠萝派商品库存查询
     * @param id 商品id
     * @return
     */
    Integer selectQuantityNewById(@Param("id") Integer id);

  /**
   * 更新sku库存
   * @param quantity
   * @param skuId
   */
  void updateNewSkuQuantity(@Param("quantity") Integer quantity,@Param("skuId") String skuId);

  /**
   * 检查传进来的id是否存在
   * @param id
   * @return
   */
  Integer selectIdByProductId(@Param("id")Integer id);

//    int updateByPrimaryKeySelective(PloyProduct record);

//    int updateByPrimaryKey(PloyProduct record);
}