package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.ProductImage;

public interface ProductImageMapper {

  int updateForArchive(String id);

  int updateForUnArchive(String id);

  int insert(ProductImage record);

  int updateForArchiveByParam(@Param("productId") String productId, @Param("type") Integer type);

  /**
   * 根据商品ID获取所有的Image列表
   */
  List<ProductImage> selectByProductId(String productId);

  List<ProductImage> getImgs4Sync(@Param("productId") String productId);

  int updateImgOrder(ProductImage img);

  List<ProductImage> getProductImgs(@Param("productId") String productId,
      @Param("type") String type);

}