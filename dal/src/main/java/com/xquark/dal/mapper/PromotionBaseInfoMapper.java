package com.xquark.dal.mapper;

import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionNameListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface PromotionBaseInfoMapper {

  List<PromotionBaseInfo> selectBySkuCode(
      @Param("skuCode") String skuCode, @Param("pType") String pType);

  List<PromotionBaseInfo> selectEffectiveBySkuCode(@Param("skuCode") String skuCode);

  PromotionBaseInfo selectByPtype(@Param("pType") String pType);

  List<PromotionBaseInfo> selectByPush();

  List<PromotionBaseInfo> selectByPushEnd();

  PromotionBaseInfo selectByPCode(@Param("pCode") String pCode);

  Integer selectIsWithUsedPoint(@Param("pCode") String pCode);

  PromotionBaseInfo selectByPromotionPCode(@Param("pCode") String pCode);

  Boolean isWhthInTime(@Param("pType") String pType, @Param("pCode") String pCode);

  List<PromotionBaseInfo> selectByPstatus(
      @Param("offSet") Integer offSet, @Param("pageSize") Integer pageSize);

  int selectCountByPstatus();
  //
  List<PromotionBaseInfo> selectByProductId(
      @Param("productId") String productId, @Param("pType") String pType);

  /**
   * 查看是否超过最大购买限制
   *
   * @param pCode
   * @param userId
   * @return
   */
  Boolean selectIsOverBuyLimit(@Param("pCode") String pCode, @Param("userId") String userId);

  Integer selectBuyAmounts(@Param("pCode") String pCode, @Param("userId") String userId);

  Integer selectBuyLimits(@Param("pCode") String pCode);

  List<PromotionBaseInfo> selectEffectiveBySkuid(@Param("skuId") String skuId);

  List<PromotionNameListVO> selectPromotionName();

  List<Promotion4ProductSalesVO> selectSalesPromotion(@Param("pCode")String pCode);

  List<Promotion4ProductSalesVO> selectSalesPromotionByProductId(@Param("productId")String productId);

  List<Promotion4ProductSalesVO> selectExsitSalesPromotionByProductId(@Param("productId")String productId);

  List<Promotion4ProductSalesVO> selectSalesPromotionBySkuId(@Param("skuId")String skuId);


  List<Promotion4ProductSalesVO> selectGrandSalesPromotion(@Param("grandSaleId")String grandSaleId);




}
