package com.xquark.dal.mapper;

import com.xquark.dal.model.Carousel;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface CarouselMapper {

  int deleteByPrimaryKey(String id);

  int insert(Carousel record);

  int insertSelective(Carousel record);

  Carousel selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Carousel record);

  int updateByPrimaryKey(Carousel record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<Carousel> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该轮播图类型的设置
   */
  Long selectByType(@Param(value = "shopId") String shopId, @Param(value = "type") String type);

  /**
   * 供客户端调用，获取首页轮播图
   */
  Carousel getHomeForApp();

  /**
   * 供客户端调用，获取首页优惠券图片信息
   */
  Carousel getCouponForApp();

  /**
   * 供客户端调用，获取首页活动
   */
  Carousel getHomeActivityForApp();

}