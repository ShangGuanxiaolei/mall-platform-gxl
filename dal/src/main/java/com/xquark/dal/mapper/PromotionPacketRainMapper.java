package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPacketRain;
import com.xquark.dal.vo.PromotionPacketRainVO;
import org.apache.ibatis.annotations.Param;

public interface PromotionPacketRainMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionPacketRain record);

    PromotionPacketRain selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionPacketRain record);

    int updateByPrimaryKey(PromotionPacketRain record);

    /**
     * 选择当天激活的配置
     */
    PromotionPacketRain selectTodayActivityConfigLimit();

    PromotionPacketRainVO selectTodayActivityConfigLimitVO();

    boolean selectIsInPromotion(@Param("type") int type);

    boolean selectIsInTimePromotion(@Param("type") int type,@Param("promotionId")String promotionId);

    boolean selectIsInLottery();

    boolean selectIsInTimeLottery(@Param("promotionId")String promotionId);

}