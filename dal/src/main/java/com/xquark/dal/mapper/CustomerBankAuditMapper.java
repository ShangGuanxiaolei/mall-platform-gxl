package com.xquark.dal.mapper;


import com.xquark.dal.model.CustomerBankAudit;

public interface CustomerBankAuditMapper {

  int insert(CustomerBankAudit record);
}