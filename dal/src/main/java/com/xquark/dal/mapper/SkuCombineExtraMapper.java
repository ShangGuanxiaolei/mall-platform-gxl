package com.xquark.dal.mapper;

import com.xquark.dal.model.SkuCombineExtra;
import java.util.Collection;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SkuCombineExtraMapper {

  int deleteByPrimaryKey(String id);

  int insert(SkuCombineExtra record);

  int insertSelective(SkuCombineExtra record);

  SkuCombineExtra selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(SkuCombineExtra record);

  int updateByPrimaryKey(SkuCombineExtra record);

  boolean selectIsMasterExist(@Param("masterId") String masterId);

  int deleteByMasterId(String masterId);

  int batchInsert(@Param("collection") Collection<? extends SkuCombineExtra> collection);

  List<SkuCombineExtra> listSlaveBySkuId(@Param("skuId") String skuId);

  SkuCombineExtra loadExtraBySkuIdAndSlaveId(@Param("skuId") String skuId,
      @Param("slaveId") String slaveId);

  /**
   * 取消组合关系
   */
  int deleteCombine(@Param("masterSkuId") String masterSkuId, @Param("slaveId") String slaveId);

  /**
   * 查询商品是否被绑定到了套装 [一个商品可能会被绑定到多个套装]
   */
  Boolean selectIsBindedSlave(@Param("skuId") String skuId);

  /**
   * 查询某个sku被绑定到套装的库存数量
   * 单个库存 * 套装库存
   */
  Integer sumBindedSlaveAmount(@Param("skuId") String skuId);

}