package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPgMemberInfo;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RecordPromotionPgMemberInfoMapper {

  //记录拼团成员详细信息
//  void save(PromotionPgMemberInfo promotionPgMemberInfo);
  int save(PGMemberInfoVo pgMemberInfoVo);

  /**
   * 更改参团状态
   */
  int updateStatus(PromotionPgMemberInfo promotionPgMemberInfo);

  int countLegalMember(@Param("tranCode") String tranCode);

  PGMemberInfoVo selectByMemberCode(String memberCode);

  /**
   * 根据成员编码更新, 更新记录应唯一
   */
  int updateStatusByMemberCode(@Param("memberCode") String memberCode,
      @Param("status") int status);

  /**
   * 根据团编码更新, 批量更新整个团的状态
   */
  int updateStatusByTranCode(@Param("tranCode") String tranCode,
      @Param("status") int status, @Param("originCodeList") List<Integer> originCodeList);

  /**
   * 更新团中某个人的状态
   */
  int updateStatusByTranCodeAndMemberId(@Param("detailCode") String detailCode,
                                        @Param("status") int status,
                                        @Param("originCodeList") List<Integer> originCodeList);

  /**
   * 根据tranCode查询开团人的订单编号
   * @param tranCode tranCode
   * @return orderNo
   */
  String selectOrderNoByTranCode(@Param("tranCode") String tranCode);

  /**
   * 根据tranCode查询开团人cpId
   * @param tranCode tranCode
   * @return memberId
   */
  String selectGroupHeadMemberId(@Param("tranCode") String tranCode);
}
