package com.xquark.dal.mapper;

import com.xquark.dal.vo.OfReport;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/2/22
 * Time: 14:28
 * Description: 欧飞Mapper
 */
@Repository
public interface OfReportMapper {

    List<Map<String,String>> queryQuestionnaire ();

    int insertOfReport(OfReport ofReport);

    Integer selectSubjectPeopleByName(Integer cpid, String name);

    OfReport selectBindedOfreportByName(Integer cpid, String name);

    Integer checkSampleCode(String code);
}