package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerMessage;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CustomerMessageMapper {

  int deleteByPrimaryKey(Long id);

  int insert(CustomerMessage record);

  int insertMultiple(List<CustomerMessage> list);

  CustomerMessage selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(CustomerMessage record);

  int updateByPrimaryKey(CustomerMessage record);

  List<CustomerMessage> listUnRead(
      @Param("cpId") Long cpId, @Param("msgCode") String msgCode,
      @Param("source") Integer source, @Param("limit") Integer limit);

  int updateReadStatusMultiple(@Param("messages") List<CustomerMessage> messages);

}