package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionFlashSale;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.FlashSalePromotionOrderVO;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.dal.vo.FlashSalePromotionUserVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import java.util.Date;
import java.util.List;

public interface PromotionFlashSaleMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionFlashSale record);

  int insertSelective(PromotionFlashSale record);

  PromotionFlashSale selectByPrimaryKey(String id);

  FlashSalePromotionUserVO selectUserVOByPrimaryKey(@Param("id") String id,
      @Param("userId") String userId);

  List<FlashSalePromotionOrderVO> listOrderVO(@Param("userId") String userId,
      @Param("order") String order, @Param("direction") Direction direction,
      @Param("page") Pageable pageable);

  Long countOrderVO(String userId);

  int updateByPrimaryKeySelective(PromotionFlashSale record);

  int updateByPrimaryKey(PromotionFlashSale record);

  boolean inPromotion(@Param("productId") String productId);

  boolean exists(@Param("promotionId") String promotionId, @Param("productId") String productId);

  FlashSalePromotionProductVO selectVOByPrimaryKey(String id);

  PromotionFlashSale selectByPromotionIdAndProductId(@Param("promotionId") String promotionId,
      @Param("productId") String productId);

  /**
   * 限时抢购每个商品只允许单商品
   */
  FlashSalePromotionProductVO selectVOByProductId(@Param("productId") String productId,
                                                  @Param("validOnly") Boolean validOnly);

  /**
   * 限时抢购每个商品只允许单商品
   */
  FlashSalePromotionProductVO selectPromotionProductByProductId(@Param("productId") String productId,
                                                                @Param("validOnly") Boolean validOnly);


  FlashSalePromotionProductVO selectVOBySaleZoneProductId(@Param("productId") String productId,
                                                          @Param("validOnly") Boolean validOnly);

  Date selectPromotionValidFrom(String productId);

  boolean selectIsInPromotion(String productId);

  Long selectActivePromotionId(String productId);

  FlashSalePromotionProductVO selectVOByPromotionIdAndProductId(
      @Param("promotionId") String promotionId,
      @Param("productId") String productId);

  List<PromotionFlashSale> listPromotionFlashSale(@Param("promotionId") String promotionId,
      @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  List<FlashSalePromotionProductVO> listPromotionFlashSaleVO(
      @Param("promotionId") String promotionId, @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  /**
   * 查询所有限购商品
   *
   * @param pageable 分页对象
   * @return 限购商品集合
   */
  List<FlashSalePromotionProductVO> listAllPromotionFlashSaleVO(@Param("page") Pageable pageable);

  /**
   * 根据关键字搜索活动
   * @param batchNo 批次号
   * @param promotionType 活动类型
   * @param promotionStatus 活动状态
   * @param pageable 分页数据
   * @return List<FlashSalePromotionProductVO>
   */
  List<FlashSalePromotionProductVO> searchByKeyWord(@Param("batchNo")String batchNo,
                                                    @Param("promotionType")PromotionType promotionType,
                                                    @Param("promotionStatus")String promotionStatus,
                                                    @Param("page") Pageable pageable);


  /**
   * 查询秒杀商品
   * @param career 身份信息
   * @param grandSaleId 大型促销活动id
   * @param promotionType 活动类型
   * @return 限购商品集合
   */
  List<FlashSalePromotionProductVO> listFlashSaleVO(
          @Param("career") CareerLevelType career,
          @Param("grandSaleId") Integer grandSaleId,
          @Param("promotionType") PromotionType promotionType);

  /**
   * 查询一元专区商品
   *
   * @return 专区商品列表
   */
  List<FlashSalePromotionProductVO> listAllPromotionSaleZoneVO();

  /**
   * 根据指定条件查询限购商品数量
   *
   * @param promotionId 活动id，传null则查询所有限购商品数量
   * @return 限购商品数量
   */
  Long count(String promotionId);

  Long countAll();

  Long countAllByKey(@Param("batchNo") String batchNo,
                     @Param("promotionType") PromotionType promotionType,
                     @Param("promotionStatus") String promotionStatus);

  Long countFlashSaleAll();

  Long countSaleZoneAll(@Param("isAvailable") Boolean isAvailable);

  int updateStock(@Param("orderId") String orderId, @Param("qty") int qty);

  List<Long> listClosedProductId();
}