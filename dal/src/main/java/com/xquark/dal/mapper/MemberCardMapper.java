package com.xquark.dal.mapper;

import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserMemberCard;
import com.xquark.dal.type.MemberUpgradeType;
import com.xquark.dal.vo.MemberCardUserVO;
import com.xquark.dal.vo.MemberCardVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-6-26. DESC:
 */
public interface MemberCardMapper {

  MemberCard selectByPrimaryKey(@Param("id") String id);

  int insert(MemberCard memberCard);

  int update(MemberCard memberCard);

  List<MemberCard> selectList(@Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  List<MemberCardVO> selectListWithProductId(@Param("productId") String productId,
      @Param("order") String order, @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  List<MemberCard> listByUpgradeType(MemberUpgradeType type);

  List<MemberCardVO> selectListWithMemberCounts(@Param("order") String order,
      @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

  Long count();

  int insertUserCard(@Param("userId") String userId, @Param("cardId") String cardId);

  int hasBinded(@Param("userId") String userId);

  UserMemberCard selectUserMemberCard(@Param("userId") String userId,
      @Param("cardId") String cardId);

  int updateBindInfo(UserMemberCard userMemberCard);

  int insertProductCards(@Param("productId") String productId, @Param("cardIds") String[] cardIds);

  int deleteProductCard(@Param("productId") String productId, @Param("cardId") String cardI);

  int deleteByProductId(@Param("productId") String productId);

  List<User> listUserSatisfiedCard(MemberCard card);

  List<MemberCardUserVO> listMemberCardUsersByUpgradeType(MemberUpgradeType type);

  MemberCard selectMemberCardByUserIdAndProductId(@Param("userId") String suerId,
      @Param("productId") String productId);
}
