package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionLottery;
import com.xquark.dal.vo.PromotionLotteryVO;

public interface PromotionLotteryMapper {

    int deleteByPrimaryKey(String id);

    int insert(PromotionLottery record);

    PromotionLottery selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionLottery record);

    int updateByPrimaryKey(PromotionLottery record);

    PromotionLottery selectTodayLotteryActivityLimit();

    PromotionLotteryVO selectTodayLotteryActivityLimitVO();

    PromotionLotteryVO selectClosestLotteryActivityLimitVO();
}