package com.xquark.dal.mapper;

import com.xquark.dal.model.WeakLink;

public interface WeakLinkMapper {
    int insert(WeakLink record);

    int insertSelective(WeakLink record);

    Long selectCount(Long cpid);

    WeakLink selectWeakLinkBound(WeakLink weakLink);

    Long selectShareCpId(Long cpId);

    boolean isExist(Long cpId);
}