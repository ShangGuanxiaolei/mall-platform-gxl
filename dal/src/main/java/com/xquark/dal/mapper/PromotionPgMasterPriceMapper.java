package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPgMasterPrice;

public interface PromotionPgMasterPriceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgMasterPrice record);

    int insertSelective(PromotionPgMasterPrice record);

    PromotionPgMasterPrice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PromotionPgMasterPrice record);

    int updateByPrimaryKey(PromotionPgMasterPrice record);

    PromotionPgMasterPrice selectPromotionPgMasterPriceByDetailCode(String pDetailCode);
}