package com.xquark.dal.mapper;

import com.xquark.dal.model.Org;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import java.util.List;

public interface OrgMapper {

  /**
   * 新增部门
   *
   * @param org 部门对象
   * @return id
   */
  int insert(Org org);

  /**
   * 根据id查找部门
   *
   * @param id 部门编号
   * @return 部门对象
   */
  Org select(@Param("id") String id);

  /**
   * 根据名称查找部门
   *
   * @param name 部门名称
   * @return 部门对象
   */
  Org selectByName(@Param("name") String name);

  /**
   * 根据name查找部门下的子部门
   *
   * @param name 部门名称
   * @return 部门对象
   */
  Org selectChild(@Param("parentId") String parentId, @Param("name") String name);

  /**
   * 根据id删除部门
   *
   * @param id 部门编号
   */
  void deleteByPrimaryKey(@Param("id") String id);

  /**
   * 删除所有部门
   *
   * @param ids 部门id集合
   */
  void deleteOrgs(@Param("ids") String... ids);

  /**
   * 更新部门
   */
  Integer update(@Param("id") String id, @Param("name") String name,
      @Param("remark") String remark);

  /**
   * 查找所有节点记录
   */
  List<Org> listAll();

  /**
   * 按sortNum排序查询
   *
   * @param parentId 父节点id
   * @param page 分页对象
   * @param direction 排序方向
   */
  List<Org> listBySortNo(@Param("parentId") String parentId, @Param("page") Pageable page,
      @Param("direction") Direction direction);


  /**
   * 按名称排序
   */
  List<Org> listByName(@Param("parentId") String parentId, @Param("page") Pageable page,
      @Param("direction") Direction direction);

  /**
   * 按创建时间排序
   */
  List<Org> listByTime(@Param("parentId") String parentId, @Param("page") Pageable page,
      @Param("direction") Direction direction);

  /**
   * 查找所有根节点
   */
  List<Org> listRoots();

  /**
   * 查找节点下所有子节点
   */
  List<Org> loadSubs(@Param("id") String id);

  /**
   * 查询父节点下子节点数目
   *
   * @param parentId 父节点id
   * @return 节点总数
   */
  Integer loadSubCounts(@Param("parentId") String parentId);

  /**
   * 查询总数
   */
  Integer loadCounts();


}
