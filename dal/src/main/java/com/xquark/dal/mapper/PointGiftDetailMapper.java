package com.xquark.dal.mapper;

import com.xquark.dal.model.PacketRainCount;
import com.xquark.dal.model.PointGiftDetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.List;

public interface PointGiftDetailMapper {
  int deleteByPrimaryKey(String id);

  int insert(PointGiftDetail record);

  BigDecimal selectPointPacketByOrderNo(String bizNo);

  PointGiftDetail selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PointGiftDetail record);

  int updateByPrimaryKey(PointGiftDetail record);

  List<PointGiftDetail> listPointPacketDetailByCpid(@Param("cpId") Long cpId, @Param("bizTypes") List<String> bizTypes, @Param("pageRequest") PageRequest pageRequest);

  List<PointGiftDetail> listPointRandomPacketSend(@Param("cpId") Long cpId, @Param("pageRequest") PageRequest pageRequest);

  PacketRainCount countRecord(@Param("cpId") Long cpId);

}
