package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerCareerLevelAudit;

public interface CustomerCareerLevelAuditMapper {

  int deleteByPrimaryKey(Long cpId);

  int insert(CustomerCareerLevelAudit record);

  CustomerCareerLevelAudit selectByPrimaryKey(Long cpId);

  int updateByPrimaryKeySelective(CustomerCareerLevelAudit record);

  int updateByPrimaryKey(CustomerCareerLevelAudit record);

}