package com.xquark.dal.mapper;

import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Zone;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: a9175
 * Date: 2018/6/19.
 * Time: 15:19
 * ${DESCRIPTION}
 */
public interface SystemRegionMapper {

    SystemRegion selectByPrimaryKey(Long id); //根据主键查询

    List<SystemRegion> listRoots();

    List<SystemRegion> listChildren(String zoneId);

    SystemRegion findParent(String zoneId);

    List<SystemRegion> selectByIds(@Param("ids") String... ids);

    List<SystemRegion> findByName(String name);

    List<PostConf> findPostArea(String name);

    List<PostConf> selectPostConf();

    void updatePath(String id, String path);//--->未实现功能

    boolean isDescendant(String id, String decendantId);//-->未实现功能

    List<SystemRegion> listSiblings(String zoneId); //-->未实现功能

    List<SystemRegion> listPostAgeArea();

    List<SystemRegion> selectCity4PostAge();

    List<SystemRegion> listAll();


    /**
     * 通过汇购网对应的zoneid，查找代理系统的zone信息
     */
    SystemRegion selectByExtId(@Param("extId") String extId);// 未实现功能

    /**
     * 查找某个地区下面所有的下级地区，包含自己
     */
    List<SystemRegion> listAllChildrenIncludeSelf(String zoneId, String likeZoneId);//未实现功能

  List<SystemRegion> queryByIdList(@Param("list") List<Long> list);

  List<SystemRegion> listParents(@Param("regionId") String regionId);
  String selectHomeCityIdByOrderNo(@Param("orderNo")String orderNo);
  String selectHomeCityIdByOrderNoAndParentId(@Param("orderNo")String orderNo);
  String selectStoreIdByHomeCityId(@Param("homeCityId")String homeCityId);

    /**
     * 通过名字和parentId查询
     * @param name
     * @return
     */
    SystemRegion selectByNameAndParentId(@Param("name")String name,@Param("parentId") String parentId);
}
