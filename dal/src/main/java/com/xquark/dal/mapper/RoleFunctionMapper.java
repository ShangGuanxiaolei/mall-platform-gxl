package com.xquark.dal.mapper;

import com.xquark.dal.model.RoleFunction;
import org.apache.ibatis.annotations.Param;

/**
 * 作者: wangxh 创建日期: 17-4-7 简介:
 */
public interface RoleFunctionMapper {

  /**
   * 通过functionId和roleId可以确定关联表的唯一性
   *
   * @param functionId 按钮id
   * @param roleId 角色id
   */
  RoleFunction selectUnique(@Param("functionId") String functionId, @Param("roleId") String roleId);

  Integer insert(RoleFunction roleFunction);

  Integer updateUnique(RoleFunction roleFunction);

  Integer removeEnable(@Param("functionId") String functionId, @Param("roleIds") String[] roleIds);

  Integer removeShow(@Param("functionId") String functionId, @Param("roleIds") String[] roleIds);

  Integer delete(@Param("id") String id);

  Integer deleteByFunctionId(@Param("functionId") String functionId);

}
