package com.xquark.dal.mapper;

import com.xquark.dal.model.SkuAttribute;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface SkuAttributeMapper {

  int insert(SkuAttribute rule);

  SkuAttribute selectByPrimaryKey(@Param("id") String id);

  int modify(SkuAttribute role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<SkuAttribute> list(@Param(value = "pager") Pageable page,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 返回系统设置的所有属性规格
   */
  List<SkuAttribute> findAll();

}
