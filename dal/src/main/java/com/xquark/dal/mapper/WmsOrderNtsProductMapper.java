package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsOrderNtsProduct;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午3:59
 */
public interface WmsOrderNtsProductMapper {

  /**
   * 添加
   */
  int insert(WmsOrderNtsProduct obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsOrderNtsProduct> collection);

  /**
   * 通过id查找
   */
  WmsOrderNtsProduct getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsOrderNtsProduct obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsOrderNtsProduct> list(@Param("page") Pageable page,
      @Param("params") Map<String, Object> params);

  WmsOrderNtsProduct getByOrderNo(String orderNo);

  Set<String> getOrderNo();


  long countByOrderNoAndSku(@Param("orderNo")String orderNo,@Param("sku")String sku);
}