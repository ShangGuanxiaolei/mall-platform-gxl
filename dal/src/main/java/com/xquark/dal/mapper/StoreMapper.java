package com.xquark.dal.mapper;

import com.xquark.dal.model.Store;
import com.xquark.dal.vo.StoreVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface StoreMapper {

  int deleteByPrimaryKey(String id);

  int insert(Store record);

  int insertSelective(Store record);

  Store selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Store record);

  int updateByPrimaryKey(Store record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<StoreVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 根据code查询门店信息
   */
  Store selectByCode(@Param(value = "code") String code);

}