package com.xquark.dal.mapper;

import com.xquark.dal.vo.PromotionMemberImgVO;
import com.xquark.dal.vo.PromotionTranInfoVO;
import com.xquark.dal.vo.PromotionTranOrderVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionTranInfoMapper
 * @date 2018/9/13 0013
 */
public interface PromotionTranInfoMapper {
    /**
     * 查询当前用户为团长的拼团信息
     * @param cpid
     * @return
     */
    List<PromotionTranOrderVO> selectMyGroupInfo(@Param("cpid")String cpid,@Param("firstNo")Integer firstNo, @Param("pageSize") Integer pageSize);

    /**
     * 我的开团数量
     * @param cpid
     * @return
     */
    int selectMyTranCount(@Param("cpid")String cpid);

    /**
     * 查询当前用户不为团长的拼团信息
     * @param cpid
     * @return
     */
    List<PromotionTranOrderVO> selectMyNoGroupInfo(@Param("cpid")String cpid,@Param("firstNo")Integer firstNo, @Param("pageSize") Integer pageSize);

    /**
     * 我的参团数量
     * @param cpid
     * @return
     */
    int selectMyNoTranCount(@Param("cpid")String cpid);

    /**
     * 查询当前拼团用户头像
     * @param tranCode
     * @return
     */
    List<PromotionMemberImgVO> selectPromotioMyTranMemberImg(@Param("tranCode")String tranCode);

    /**
     * 查询当前拼团traninfo信息
     * @param tranCode
     * @return
     */
    PromotionTranInfoVO selectPromotionPgTranCode(String tranCode);

    /**
     * 查询当前pCode对应的剩余活动库存
     * 仅支持拼团活动与sku一对一的关系
     */
    int selectRestStock(@Param("pCode") String pCode);

    /**
     * 查询当前skuCode对应的剩余活动库存
     * 仅支持拼团活动与sku一对一的关系
     */
    int selectRestStockBySkuCode(@Param("skuCode") String skuCode, @Param("pCode") String pCode);

    /**
     * 修改拼团状态
     * @param tranCode
     * @return
     */
    int updatePieceStatusByTranCode(String tranCode);

}
