package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PostConf;
import com.xquark.dal.model.Zone;

public interface ZoneMapper {

  Zone selectByPrimaryKey(String id);

  Zone selectByZipCode(String zipCode);

  List<Zone> listRoots();

  List<Zone> listChildren(String zoneId);

  Zone findParent(String zoneId);

  List<Zone> selectByIds(@Param("ids") String... ids);

  List<Zone> findByName(String name);

  List<PostConf> findPostArea(String name);

  List<PostConf> selectPostConf();

  void updatePath(String id, String path);

  boolean isDescendant(String id, String decendantId);

  List<Zone> listSiblings(String zoneId);

  List<Zone> listPostAgeArea();

  List<Zone> selectCity4PostAge();

  List<Zone> listAll();

  /**
   * 通过汇购网对应的zoneid，查找代理系统的zone信息
   */
  Zone selectByExtId(@Param("extId") String extId);

  /**
   * 查找某个地区下面所有的下级地区，包含自己
   */
  List<Zone> listAllChildrenIncludeSelf(String zoneId, String likeZoneId);

}