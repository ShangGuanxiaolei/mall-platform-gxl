package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestResult;
import com.xquark.dal.model.HealthTestResultExample;
import com.xquark.dal.vo.HealthTestResultVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public interface HealthTestResultMapper {

  long countByExample(HealthTestResultExample example);

  int deleteByExample(HealthTestResultExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestResult record);

  int insertSelective(HealthTestResult record);

  List<HealthTestResult> selectByExampleWithBLOBs(HealthTestResultExample example);

  List<HealthTestResult> selectByExample(HealthTestResultExample example);

  HealthTestResult selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestResult record,
      @Param("example") HealthTestResultExample example);

  int updateByExampleWithBLOBs(@Param("record") HealthTestResult record,
      @Param("example") HealthTestResultExample example);

  int updateByExample(@Param("record") HealthTestResult record,
      @Param("example") HealthTestResultExample example);

  int updateByPrimaryKeySelective(HealthTestResult record);

  int updateByPrimaryKeyWithBLOBs(HealthTestResult record);

  int updateByPrimaryKey(HealthTestResult record);

  List<HealthTestResultVO> selectVOInPage(@Param("moduleId") String moduleId,
      @Param("number") Double number, @Param("order") String order,
      @Param("direction") Direction direction, @Param("page") Pageable pageable);

  // 分割线
  List<HealthTestResultVO> selectVOByExample(@Param("number") double number,
      @Param("moduleId") String moduleId, @Param("example") HealthTestResultExample example);
}