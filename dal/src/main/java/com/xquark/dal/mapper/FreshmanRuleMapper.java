package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshmanRule;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FreshmanRuleMapper {

    FreshmanRule selectFreshmanRuleByName(@Param("name") String name);
}
