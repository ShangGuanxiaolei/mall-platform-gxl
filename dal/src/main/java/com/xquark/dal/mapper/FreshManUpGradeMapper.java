package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManUpGradeVo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/13
 * Time:17:38
 * Des:
 */
public interface FreshManUpGradeMapper {
    FreshManUpGradeVo selectByGrade(String grade);
}