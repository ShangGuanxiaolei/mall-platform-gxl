package com.xquark.dal.mapper;

import com.xquark.dal.model.MerchantRole;
import com.xquark.dal.vo.MerchantRoleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public interface MerchantRoleMapper {

  MerchantRole selectByPrimaryKey(@Param("id") String id);

  MerchantRole selectByRoleName(@Param("roleName") String roleName);

  int updateByPrimaryKey(MerchantRole merchantRole);

  List<MerchantRole> listByCreaterId(@Param(value = "createId") String createId);

  List<MerchantRole> listAll();

  List<MerchantRole> listRolesByMerchantId(@Param("merchantId") String merchantId);

  List<MerchantRole> listRolesByMerchantLoginName(@Param("loginName") String loginName);

  Long insert(MerchantRole merchantRole);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<MerchantRoleVO> list(@Param(value = "pager") Pageable page,
      @Param("keyword") String keyword);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

  /**
   * 取出不包含ROOT和BASIC的角色总数
   */
  Long selectNormalCnt();

  /**
   * 根据角色编码获取角色名称
   */
  String getRoleDesc(List name);

}
