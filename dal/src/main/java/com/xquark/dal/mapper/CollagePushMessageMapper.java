package com.xquark.dal.mapper;

import com.xquark.dal.model.CollagePushMessage;

/**
 * 拼团push消息映射接口
 * @author  tanggb
 */
public interface CollagePushMessageMapper {
	/**
	 * 拼团push消息保存入数据库
	 * @param collagePushMessage
	 * @return
	 */
	Boolean insert(CollagePushMessage collagePushMessage);

}
