package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSaleModule;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionModuleVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GrandSaleModuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleModule record);

    int insertSelective(GrandSaleModule record);

    GrandSaleModule selectByPrimaryKey(Integer id);

    List<GrandSaleModule> selectByGrandSaleId(Integer grandSaleId);

    List<GrandSaleModule> selectByGrandSaleIdAndPromotionType(@Param("grandSaleId") Integer grandSaleId,
                                                              @Param("promotionType") PromotionType promotionType);

    List<GrandSaleModule> selectAll();

    int updateByPrimaryKeySelective(GrandSaleModule record);

    int updateByPrimaryKey(GrandSaleModule record);

    int updateStatusByPromotionId(@Param("promotionId") String promotionId, @Param("status") Integer status);

    List<PromotionModuleVO> selectEffectivePiece();

    List<PromotionModuleVO> selectEffectiveFlash();

    List<PromotionModuleVO> selectEffectiveReserve();

    List<PromotionModuleVO> selectEffectiveSales();
}