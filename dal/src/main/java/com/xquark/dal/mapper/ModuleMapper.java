package com.xquark.dal.mapper;

import com.xquark.dal.model.MerchantRoleModule;
import com.xquark.dal.model.Module;
import com.xquark.dal.vo.UrlRoleVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
public interface ModuleMapper {

  /**
   * 添加菜单
   */
  Integer insert(Module module);

  /**
   * 查找菜单
   */
  Module select(@Param("id") String id);

  /**
   * 查找指定父节点下的字节点
   */
  Module selectChild(@Param("parentId") String parentId, @Param("name") String name);

  /**
   * 查找父节点下所有节点
   */
  List<Module> loadSubs(@Param("parentId") String parentId);

  /**
   * 查找指定节点下子节点数量
   */
  Integer loadSubCounts(@Param("parentId") String parentId);

  /**
   * 查询总数据
   */
  Integer loadCounts();

  /**
   * 根据主键删除
   */
  Integer deleteByPrimaryKey(@Param("id") String id);

  /**
   * 根据指定的一串id删除所有菜单
   */
  Integer deleteModules(@Param("ids") String... id);

  /**
   * 根据传入的对象信息更新数据
   */
  Integer update(Module module);

  /**
   * 查询所有菜单
   */
  List<Module> listAll();

  /**
   * 根据传入的order排序
   */
  List<Module> listByOrder(@Param("parentId") String parentId, @Param("order") String order,
      @Param("page") Pageable page, @Param("direction") Sort.Direction direction);

  /**
   * @param roleId
   * @return
   */
  List<Module> listByRole(@Param("roleId") String roleId);

  List<Module> listByRoleName(@Param("roleName") String roleName);

  /**
   * 绑定角色跟菜单关系
   */
  Integer insertRoleModule(@Param("roleModuleList") List<MerchantRoleModule> roleModuleList);

  /**
   * 解除绑定角色跟菜单关系
   */
  Integer deleteRoleModule(@Param("moduleIdList") String[] moduleIdList,
      @Param("roleId") String roleId);

  /**
   * 获取可访问某个URL的角色
   */
  List<UrlRoleVO> selectUrlRoles(@Param("url") String url);

  /**
   * 获取所有url与角色的对应关系
   */
  List<UrlRoleVO> selectAllUrlRoles();

}
