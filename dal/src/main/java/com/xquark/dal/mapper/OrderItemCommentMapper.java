package com.xquark.dal.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.OrderItemComment;

public interface OrderItemCommentMapper {

  int deleteByPrimaryKey(String id);

  int insert(OrderItemComment record);

  OrderItemComment selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(OrderItemComment record);

  List<OrderItemComment> selectByProductId(@Param("productId") String productId,
      @Param("pager") Pageable pager);

  Long countByProductId(@Param("productId") String productId);

  OrderItemComment selectLastestByProductId(@Param("productId") String productId);

  Long countByBuyerIdAndSellerId(@Param("buyerId") String buyerId,
      @Param("sellerId") String sellerId);

  List<OrderItemComment> selectByEntityWithPage(
      @Param("orderItemComment") OrderItemComment orderItemComment, @Param("pager") Pageable pager);

  List<OrderItemComment> selectUnreadBySellerId(@Param("sellerId") String sellerId,
      @Param("pager") Pageable pager);

  List<OrderItemComment> selectByOrderItemId(@Param("orderItemId") String orderItemId);

  Long countNewOrderItemReplyBySellerId(@Param("sellerId") String sellerId,
      @Param("startTime") Date startTime);

  BigDecimal selectProcuctScoreAvg(@Param("productId") String productId);

  Long checkExistsByOrderId(@Param("orderId") String orderId);

  int emptyReply(@Param("id") String id);


}