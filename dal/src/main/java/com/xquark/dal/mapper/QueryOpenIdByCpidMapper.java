package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author qiuchuyi
 * @date 2018/9/15 15:22
 */
public interface QueryOpenIdByCpidMapper {
    String queryOpenIdByCpid(@Param("cpid") String cpid);
}
