package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderSend;
import org.apache.ibatis.annotations.Param;

/**
 * author: liuwei
 * Date: 2018/9/19.
 * Time: 15:12
 */
public interface LogisticsOrderSendMapper {
    int deleteByPrimaryKey(Long id);

    /**
     * 插入发来的发货信息
     * @param record
     * @return
     */
    int insert(OrderSend record);

    /**
     * 对order表中的物流及订单状态进行改变
     * @param orderNo
     * @param logisticsCompany
     * @param logisticsOrderNo
     */
    Integer updateLogisticsInfo(@Param("orderNo") String orderNo,
        @Param("logisticsCompany") String logisticsCompany,
        @Param("logisticsOrderNo") String logisticsOrderNo,
        @Param("supplierCode") String supplierCode);

    int insertSelective(OrderSend record);

    OrderSend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderSend record);

    int updateByPrimaryKey(OrderSend record);
}
