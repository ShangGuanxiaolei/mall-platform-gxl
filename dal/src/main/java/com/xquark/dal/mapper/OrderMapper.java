package com.xquark.dal.mapper;

import com.xquark.dal.model.*;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.Customer;
import com.xquark.dal.vo.OrderItemWithOrderVO;
import com.xquark.dal.vo.OrderVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderMapper {

  int insert(Order record);

  int  insert2(Order record);

  int insertOrder(Order record);

  Order selectByPrimaryKey(String id);

  Order selectByOrderNo(String orderNo);

  String selectStatusByOrderNo(String orderNo);

  int updateByPrimaryKeySelective(Order record);

  int updateByPrimaryKeyAndStatus(@Param("order") Order record, @Param("status") OrderStatus... from);

  int updateStatusByMainOrderId(@Param("mainOrderId") String mainOrderId,
                                @Param("payType") PaymentMode payType,
                                @Param("to") OrderStatus to,
                                @Param("from") OrderStatus... from);

  int updateOrderStatusByOrderNo(@Param("status") String status, @Param("orderNo") String orderNo);

  List<OrderVO> selectByBuyerAndStatus(String buyerId, @Param("status") OrderStatus status,
      @Param("pager") Pageable pageable);

  BigDecimal sumByBuyerAndStatus(String buyerId, @Param("start") Date start,
      @Param("end") Date end);

  Long countByBuyerAndStatus(String buyerId, @Param("status") OrderStatus status,
      @Param("start") Date start, @Param("end") Date end);

  Long countBySellerAndStatus(String sellerId, @Param("key") String key,
      @Param("status") OrderStatus... status);

  List<OrderVO> selectBySellerAndStatus(String sellerId, @Param("status") OrderStatus status,
      @Param("pager") Pageable pageable);

  /**
   * 2019-04-19 新增PENDING
   */
  List<OrderVO> selectBySellerAndStatus2(String sellerId, @Param("status") OrderStatus status,
                                        @Param("pager") Pageable pageable);

  List<OrderVO> selectBySellerAndStatusAll(String sellerId, @Param("status") OrderStatus status,
      @Param("pager") Pageable pageable, @Param("dateStr") String dateStr);

  List<OrderVO> selectBySellerAndStatusAll2(String sellerId, @Param("status") OrderStatus status,
                                           @Param("pager") Pageable pageable, @Param("dateStr") String dateStr);

  List<OrderVO> selectUnCommentOrders(String sellerId, @Param("status") OrderStatus status,
      @Param("pager") Pageable pageable);

  Boolean selectIsOrderCommented(@Param("orderId") String orderId, @Param("userId") String userId);

  List<OrderVO> selectBySellerAndStatusArr(String sellerId, @Param("status") OrderStatus[] status,
      @Param("pager") Pageable pageable);

  List<OrderItemWithOrderVO> selectBySellerAndStatusKey(String sellerId,
      @Param("status") OrderStatus status, @Param("key") String key, @Param("page") Pageable page,
      @Param("params") Map<String, Object> params);

  List<Customer> selectCustomersByShopId(@Param("shopId") String shopId,
      @Param("sort") String sortStr, @Param("page") Pageable pageable);

  Long countByAdmin(@Param(value = "params") Map<String, Object> params);

  Long countByAdminForSuccess(@Param(value = "params") Map<String, Object> params);

  Map<String, Object> countMapByAdmin(@Param(value = "params") Map<String, Object> params);

  /**
   * 店铺订单统计
   */
  int countByShopStatistics(@Param(value = "params") Map<String, Object> params);

  List<OrderVO> selectByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  List<OrderVO> selectByAdminForSuccess(@Param(value = "params") Map<String, Object> params,
                              @Param("page") Pageable pageable);

  List<OrderVO> selectByAdmin4Export(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  List<Customer> selectCustomersByShopIdAndKey(@Param("shopId") String shopId,
      @Param("key") String key, @Param("sort") String sortStr, @Param("page") Pageable pageable);

  Customer selectCustomerByShopIdAndBuyerId(String shopId, String buyerId);

  Customer selectCustomerByShopIdAndConsignee(String shopId, String name, String phone);

  List<OrderVO> selectByShopIdAndCustomerId(String shopId, String customerId);

  List<Order> selectAutoCancel(@Param("minutes") Integer minutes);

  /**
   * 查询拼团待取消订单
   */
  List<Order> selectPieceAutoCancel(@Param("minutes") Integer minutes);

  List<Order> selectAutoSignRemind();

  List<Order> selectAutoSign();

  //int updateOrderStatus4Buyer(String orderId, OrderStatus orderStatus, String buyerId);

  int updateOrderStatus4Seller(String orderId, OrderStatus orderStatus, String sellerId);

  int updateOrderByRequestRefund(@Param("id") String orderId);

  int updateOrderByRequestChange(@Param("id") String orderId);

  int updateOrderByRequestReissue(@Param("id") String orderId);

  int updateOrderByCancelRefund(@Param("id") String orderId,
      @Param("status") OrderStatus orderStatus);

  int updateOrderByCancelChange(@Param("id") String orderId,
                                @Param("status") OrderStatus orderStatus);

  int updateOrderByCancelReissue(@Param("id") String orderId,
                                 @Param("status") OrderStatus orderStatus);

  int updateOrderStatus(@Param("orderId") String orderId, @Param("status") OrderStatus orderStatus);

  Order selectBySeller(@Param("id") String orderId, @Param("sellerId") String sellerId);

  List<Order> selectByShopIdAndConsignee(String shopId, String name, String phone);

  int updateOrderByCancel(@Param("params") Map<String, Object> params, @Param("order") Order order);

  int updateOrderByPay(@Param("params") Map<String, Object> params, @Param("order") Order order);

  int updateOrderByShip(@Param("params") Map<String, Object> params, @Param("order") Order order);

  int updateOrderBySign(@Param("params") Map<String, Object> params, @Param("order") Order order);

  int updateOrderStatusWithPreCondition(@Param("params") Map<String, Object> params,
      @Param("order") Order order);

  int updateOrderByRefund(@Param("params") Map<String, Object> params,
      @Param("order") Order record);

  Long selectOrderSeqByShopId(String shopId);

  int updateOrderRefundByAdmin(String orderId, RefundType refundType);

  List<Customer> selectCustomersByVip(@Param("shopId") String shopId,
      @Param("value") Boolean value);

  Order selectLatestByBuyerId(@Param("shopId") String shopId, @Param("buyerId") String buyerId);

  List<Order> selectByBuyer(@Param(value = "params") Map<String, Object> params,
      @Param("buyerId") String userId);

  List<Order> selectBySellerAndStatus(@Param("sellerId") String sellerId,
      @Param("status") OrderStatus status);

  /**
   * 修改订单价格（老接口）
   */
  int updateTotalPrice(@Param("orderId") String orderId, @Param("totalFee") BigDecimal totalFee,
      @Param("sellerId") String sellerId);

  /**
   * 修改订单价格（新 接口141230）
   */
  int updatePrice(@Param("orderId") String orderId, @Param("totalFee") BigDecimal totalFee,
      @Param("logisticsFee") BigDecimal logisticsFee, @Param("sellerId") String sellerId);

  /**
   * 修改订单价格（不需要传入uerid，系统管理员修改金额）
   */
  int updatePriceSystem(@Param("orderId") String orderId, @Param("totalFee") BigDecimal totalFee,
      @Param("goodsFee") BigDecimal goodsFee);

  List<Order> selectByShopId(@Param("shopId") String shopId);

  Long selectCountByStatus4Buyer(@Param("buyerId") String userId,
      @Param("status") OrderStatus status, @Param("shopId") String shopId);

  Long selectCountByMoreStatus4Buyer(@Param("buyerId") String userId,
      @Param("statusArr") String[] statusArr, @Param("shopId") String shopId);

  int countNoVisitOrderBySellerId(@Param("userId") String userId,
      @Param("lastVisitTime") Date lastVisitTime);

  int updateVipByCustomer(@Param("consignee") String consignee, @Param("phone") String phone,
      @Param("shopId") String shopId, @Param("vip") Boolean vip);

  int updRemarkByAdmin(@Param(value = "orderId") String id, @Param(value = "remark") String remark);

  String obtainPaymentChannel(@Param(value = "orderNo") String orderNo);

  int delete(@Param("ids") String[] ids);

  int deleteByBuyer(@Param("orderId") String orderId);

  List<Order> selectByBuyer(@Param("buyerId") String buyerId, @Param("pager") Pageable pager);

  Long countByBuyer(@Param("buyerId") String buyerId);

  List<OrderVO> selectOrders4Export(@Param("params") Map<String, Object> params);

  List<Order> selectAutoOrderStatus();

  void updateOrderStatus(@Param("orderNo") String orderNo, @Param("status") OrderStatus status,
      @Param("logisticsCompany") String logisticsCompany,
      @Param("logisticsOrderNo") String logisticsOrderNo);

  int updateOrderSendErpStatus(@Param("id") String orderId, @Param("sendErpAt") Date sendErpAt,
      @Param("sendErpStatus") SendErpStatus sendErpStatus,
      @Param("sendErpDest") SendErpDest sendErpDest, @Param("sendErpLog") String sendErpLog);

  int updateOrderLogisticsErpStatus(@Param("id") String orderId, @Param("sysnErpAt") Date sysnErpAt,
      @Param("sysnErpStatus") SysnErpStatus sysnErpStatus, @Param("sysnErpLog") String sysnErpLog);

  List<Order> selectOrderList2Jooge(@Param("pager") Pageable pager);

  List<Order> selectByMainOrderId(@Param("mainOrderId") String mainOrderId);

  List<Order> selectStatusByMainOrderId(@Param("mainOrderId") String mainOrderId);

  int selectOrderCountByMainOrderId(@Param("mainOrderId") String mainOrderId,
      @Param("orderStatus") String orderStatus);


  List<OrderVO> selectByMemberIdAndStatus(@Param("statusArr") String[] statusArr,
      @Param("userId") String userId, @Param("shopId") String shopId,
      @Param("pager") Pageable pager, @Param("params") Map<String, Object> params);

  long countByMemberIdAndStatus(@Param("statusArr") String[] statusArr,
      @Param("userId") String userId, @Param("shopId") String shopId,
      @Param("params") Map<String, Object> params);

  List<OrderVO> selectSellerByMemberIdAndStatus(@Param("statusArr") String[] statusArr,
      @Param("sellerShopId") String sellerShopId, @Param("shopId") String shopId,
      @Param("pager") Pageable pager, @Param("params") Map<String, Object> params);

  List<Order> selectAutoPay();

  List<Order> selectSilverSellerIdAndSum();

  List<Order> selectGoldSellerIdAndSumByShopId(String shopId);

  Long countPartnerByAdmin(@Param(value = "params") Map<String, Object> params);

  List<OrderVO> selectPartnerByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  List<OrderVO> selectOrders4WxExport(@Param("params") Map<String, Object> params);

  Order selectLastMonthTotalFeeByShopId(String shopId);

  List<OrderVO> selectLastMonthOrderByShop(@Param("rootShopId") String rootShopId,
      @Param("shopId") String shopId);

  long countOrder(@Param("params") Map<String, Object> userId);

  long countB2BOrder(@Param("params") Map<String, Object> params);

  BigDecimal countB2BSale(@Param("params") Map<String, Object> params);

  long countGeneralOrder(@Param("params") Map<String, Object> params);

  long getSaleProductsByDate(@Param("params") Map<String, Object> params);

  long getDirectorSaleProductsByDate(@Param("params") Map<String, Object> params);

  long getGeneralSaleProductsByDate(@Param("params") Map<String, Object> params);

  long getFirstAndSecondSaleProductsByDate(@Param("params") Map<String, Object> params);

  /**
   * 获取某人上个月完成订单总数量
   */
  long countSelfLastMonthOrder(@Param("userId") String userId);

  /**
   * 获取某人一级直营上个月完成订单总数量
   */
  long countDirectLastMonthOrder(@Param("userId") String userId);

  /**
   * 获取某人二级直营上个月完成订单总数量
   */
  long countIndirectLastMonthOrder(@Param("userId") String userId);

  /**
   * 统计某代理整个团队上月完成订单的总金额
   */
  BigDecimal countLastMonthOrderAmount(@Param("userId") String userId);

  /**
   * 统计某代理整个团队上一个年度完成订单的总金额
   */
  BigDecimal countLastYearOrderAmount(@Param("userId") String userId);


  /**
   * 查询是否是该用户第一笔成交成功的订单
   */
  long countBeforeSuccessOrder(@Param("id") String id, @Param("userId") String userId);


  /**
   * 获取某人上个月完成订单董事姓名和各个董事完成订单数量
   */
  List<Map> getSelfLastMonthOrder(@Param("userId") String userId, @Param("dateStr") String dateStr);

  /**
   * 获取某人一级直营上个月完成订单董事姓名和各个董事完成订单数量
   */
  List<Map> getDirectLastMonthOrder(@Param("userId") String userId,
      @Param("dateStr") String dateStr);

  /**
   * 获取某人二级直营上个月完成订单董事姓名和各个董事完成订单数量
   */
  List<Map> getIndirectLastMonthOrder(@Param("userId") String userId,
      @Param("dateStr") String dateStr);


  /**
   * 团队订单查询
   */
  List<OrderVO> selectTeamByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  Long countTeamByAdmin(@Param(value = "params") Map<String, Object> params);

  /**
   * 得到该战队所有成员上周的所有订单总收益
   */
  BigDecimal countLastWeekTeamOrderAmount(@Param("teamId") String teamId);

  /**
   * 得到该战队所有成员上周的每个队员对应的订单总收益
   */
  List<Map> getLastWeekTeamOrderAmount(@Param("teamId") String teamId);

  /**
   * 得到该战队所有成员上月的所有订单总收益
   */
  BigDecimal countLastMonthTeamOrderAmount(@Param("teamId") String teamId);

  /**
   * 得到该战队所有成员上月的每个队员对应的订单总收益
   */
  List<Map> getLastMonthTeamOrderAmount(@Param("teamId") String teamId);

  /**
   * 统计某个店铺的总会员数，即在这个店铺下过订单的用户数
   */
  Long countMemberByShopId(@Param(value = "shopId") String shopId);

  /**
   * 查询某个店铺所有卖出的商品总金额
   */
  BigDecimal sumBySellerShopId(@Param("shopId") String shopId);

  /**
   * 确认订单
   */
  int confirmOrder(@Param(value = "orderId") String orderId);

  /**
   * 根据活动类型查询我的活动订单
   *
   * @param pType 活动类型
   * @param userId 用户id
   * @param shopId shopId
   * @param pageable 分页
   * @return list of {@link OrderVO}
   */
  List<OrderVO> listByPromotionTypeAndUserId(@Param("pType") PromotionType pType,
      @Param("userId") String userId,
      @Param("shopId") String shopId, @Param("page") Pageable pageable);

  /**
   * 查询我的活动订单数量
   *
   * @param pType 活动类型
   * @param userId 用户id
   * @param shopId shopId
   * @return 数量
   */
  Long countByPromotionTypeAndUserId(@Param("pType") PromotionType pType,
      @Param("userId") String userId,
      @Param("shopId") String shopId);

  int countOrderItem(String orderId);

  Long countTypeOrderCount(@Param("userId") String userId, @Param("status") String status);

  /**
   * 根据OrderNo更新数据
   */
  int updateByOrderNo(WmsExpress wmsExpress);

  /**
   * 获取Wms发货信息
   */
  List<WmsOrderNtsProduct> getWmsOrderNtsProduct(@Param("wmsInterval") Integer wmsInterval,
      @Param("orderNo") String orderNo);

  /**
   * 获取Wms订单包装箱信息
   */
  List<WmsOrderPackage> getWmsOrderPackage(@Param("wmsInterval") Integer wmsInterval,
      @Param("orderNo") String orderNo);

  /**
   * 获取Wms订单信息
   */
  List<WmsOrder> getWmsOrder(@Param("wmsInterval") Integer wmsInterval);
  List<WmsOrder>  getWmsOrder2(@Param("wmsInterval") Integer wmsInterval);
  List<WmsSku> getWmsOrderSku(@Param("starttime")String starttime,@Param("skuCode")String skuCode);

  int updateStatusByOrderNo(@Param("status") String status, @Param("orderNo") String orderNo);

  int updateStatusByOrderId(@Param("status") String status, @Param("orderId") String orderId);

  void updatePayNoByOrderNo(@Param("orderNo") String orderNo, @Param("payNo") String payNo);

  void updateOrderByOrderNo(Order order);

  Order getLogsticsByOrderNo(@Param("orderNo") String orderNo);

  int getOrderByStatusAndUser(@Param("buyerId") String buyerId,
      @Param("status") OrderStatus orderStatus);

  int getOrderByStatusAndUser2(@Param("buyerId") String buyerId,
      @Param("status") OrderStatus orderStatus);

  List<String> getOrderNoByParentNo(@Param("orderNo") List<String> orderNo);

  int updateCapableToRefundById(@Param("capableToRefund") boolean capableToRefund,
      @Param("orderId") String orderId,@Param("openTime") Date openTime);

  List<String> getOrderNoByLoc();

  List<Order> queryOrderCanPush();

  List<Order> loadByPayNo(@Param("bizNo") String bizNo);

  /**
   * 判断用户是否在汉薇成功下单过
   */
  boolean hasSucceedPay(@Param("buyerId") String buyerId);

  /**
   * 判断用户是否在中台下过单
   * */
  boolean hasSucceedPayInOrderHeader(@Param("cpId") Long cpId);

  List<Order> queryOrderByMainOrderId(@Param("id") String id);

  List<Order> selectByTypeAndBuyerId(@Param("buyerId")String buyerId,@Param("orderType")String orderType);

  Integer checkPhoneNoValid(@Param("cpId") Long cpId);

  Integer nextval();

  /**
   * 根据订单号查询运单号
   */
  Map selectBillNoByOrderNo(@Param("orderNo")String orderNo);


  String hasSucceedPay4Hv(@Param("buyerId")String buyerId);

  Map queryLogistics(@Param("orderNo")String orderNo);

  Map queryAdress(@Param("orderNo")String orderNo);

  String queryDest(@Param("orderNo")String orderNo);

  /**
   * 多个订单状态查询
   * @param buyerId
   * @param statusList
   * @param pageable
   * @param consultApplyDateFlag 14天内可以申请售后
   * @param modifyAdressDateFlag 30分钟内可以修改地址
   * @return
   */
  List<OrderVO> selectByBuyerAndStatusList(@Param("buyerId")String buyerId, @Param("statusList") List<String> statusList,
                                       @Param("pager") Pageable pageable,@Param("modifyAdressDateFlag") Integer modifyAdressDateFlag
                                      ,@Param("consultApplyDateFlag") Integer consultApplyDateFlag);

  OrderVO selectOrderVOByOrderNo(@Param("orderNo") String orderNo, @Param("modifyAdressDateFlag") Integer modifyAdressDateFlag);


  boolean checkSaleZone(@Param("buyerId")String buyerId);

  boolean selectCallBackStatus(@Param("orderId") Long orderId);

  List<Long> selectIdByMainOrderId(@Param("orderId") String orderId);

  /**
   * 验证顺丰推送的订单号是否存在
   * @param orderNo
   * @return
   */
  int checkOrderNo(String orderNo);

  /**
   * 保存顺丰拆单的物流号
   * @param logisticsNo
   * @return
   */
  int saveSFlogistics(@Param("logisticsNo")String logisticsNo,@Param("orderNo")String orderNo);

  /**
   * 保存顺丰拆单的订单号
   * @param partnerOrderNo
   * @return
   */
  int saveSFsplitOrder(@Param("partnerOrderNo")String partnerOrderNo,@Param("orderNo")String orderNo);


  /**
   * 更改顺丰订单状态
   * @param partnerOrderNo
   * @param status
   * @return
   */
  int updateSFOrderStatus(@Param("partnerOrderNo")String partnerOrderNo,@Param("status")String status);

  List<Integer> selectProductBuyAmount(@Param("buyerId") String buyerId, @Param("productId") String productId);

  List <WmsSku> selectWmsOrderNo(@Param("skuId") String skuId);

  List<String> selectWmsSku(@Param("orderNo")String orderNo);
}