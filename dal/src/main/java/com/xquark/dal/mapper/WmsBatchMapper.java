package com.xquark.dal.mapper;

import com.xquark.dal.model.WmsBatch;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: kong Date: 18-6-9. Time: 下午2:56
 */
public interface WmsBatchMapper {

  /**
   * 添加
   */
  int insert(WmsBatch obj);

  /**
   * 批量插入
   */
  int batchInsert(@Param("collection") List<WmsBatch> collection);

  /**
   * 通过id查找
   */
  WmsBatch getByPrimaryKey(@Param("id") String id);

  /**
   * 更新
   */
  int update(WmsBatch obj);


  /**
   * 通过id删除
   */
  int delete(@Param("id") String id);

  /**
   * 批量删除
   */
  int batchDelete(@Param("collection") List<String> collection);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

  /**
   * 分页查询
   */
  List<WmsBatch> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

  /**
   * 根据edi状态获取数据
   */
  List<WmsBatch> getByEdiStatus(Integer ediStatus);
}