package com.xquark.dal.mapper;

import com.xquark.dal.model.TeamShopCommissionDe;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TeamShopCommissionDeMapper {

  int deleteByPrimaryKey(String id);

  int insert(TeamShopCommissionDe record);

  int insertSelective(TeamShopCommissionDe record);

  TeamShopCommissionDe selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamShopCommissionDe record);

  int updateByPrimaryKey(TeamShopCommissionDe record);

  int updateForArchive(String id);

  List<TeamShopCommissionDe> selectByParentId(@Param(value = "parentId") String parentId);

  /**
   * 删除战队默认设置
   */
  int deleteByDefaultByshopId(@Param(value = "shopId") String shopId);

  /**
   * 根据parentid删除信息
   */
  int deleteByParentId(@Param(value = "parentId") String parentId);

  /**
   * 获取勾选了的战队分组设置
   */
  List<TeamShopCommissionDe> selectActivityByParentId(@Param(value = "parentId") String parentId,
      @Param(value = "type") String type);

}