package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestAnswerGroup;
import com.xquark.dal.model.HealthTestAnswerGroupExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestAnswerGroupMapper {

  long countByExample(HealthTestAnswerGroupExample example);

  int deleteByExample(HealthTestAnswerGroupExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestAnswerGroup record);

  int insertSelective(HealthTestAnswerGroup record);

  List<HealthTestAnswerGroup> selectByExample(HealthTestAnswerGroupExample example);

  HealthTestAnswerGroup selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestAnswerGroup record,
      @Param("example") HealthTestAnswerGroupExample example);

  int updateByExample(@Param("record") HealthTestAnswerGroup record,
      @Param("example") HealthTestAnswerGroupExample example);

  int updateByPrimaryKeySelective(HealthTestAnswerGroup record);

  int updateByPrimaryKey(HealthTestAnswerGroup record);
}