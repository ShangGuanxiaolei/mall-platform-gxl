package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.model.OrderRefundImg;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.vo.OrderRefundVO;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface OrderRefundMapper {

  List<OrderRefundVO> list(@Param("sellerId") String sellerId, @Param("page") Pageable pageable);

  int deleteByPrimaryKey(Long id);

  int deleteByPrimaryKey2(String id);

  int insert(OrderRefund record);

  OrderRefund selectByPrimaryKey(String id);

  OrderRefundVO selectVOByPrimaryKey(String id);

  int updateByPrimaryKeySelective(OrderRefund record);

  int updateStatusByOrderId(@Param("orderId") String orderId,
      @Param("status") OrderRefundStatus status,
      @Param("params")Map<String, Object> params);

  int updateByModifyRequest(OrderRefund record);

  List<OrderRefund> listByOrderId(@Param("orderId") String orderId);

  int confirmSellerByAdmin(@Param("id") String id, @Param("adminStatus") String adminStatus,
      @Param("adminRemark") String adminRemark);

  int reject(OrderRefund orderRefund);

  int rejectSign(OrderRefund orderRefund);

  int confirm(OrderRefund orderRefund);

  /**
   * 同意退款申请<br> 1.1 买家第一次申请<br> 1.2 买家已被拒绝，又同意退款
   */
  int success(OrderRefund orderRefund);

  int ship(OrderRefund orderRefund);

  int cancel(OrderRefund orderRefund);

  int sign(OrderRefund orderRefund);

  List<OrderRefundVO> listOrderRefundByAdmin(@Param(value = "params") Map<String, Object> params,
      @Param(value = "page") Pageable page);

  Long countOrderRefundByAdmin(@Param(value = "params") Map<String, Object> params);

  List<OrderRefund> selectAutoSign(@Param(value = "day") int day);

  List<OrderRefund> selectAutoSuccessWithBuyerNoShip(@Param(value = "day") int day);

  List<OrderRefund> selectAutoClosedWithSellerNoSign(@Param(value = "day") int day);

  /**
   * 根据orderid将退款申请记录状态更新为success
   */
  int successByOrderId(@Param(value = "orderid") String orderid);

  int insertRefundImg(@Param(value = "refundId") String refundId,
      @Param(value = "image") String image);

  List<OrderRefundImg> listRefundImg(@Param(value = "refundId") String refundId);

  OrderRefund loadOrderRefund(@Param(value="orderId") String orderId);
}