package com.xquark.dal.mapper;

import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.Version;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.vo.HomeItemVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface VersionMapper {

  Version selectByPrimaryKey(@Param("id") String id);

  int insert(Version homeItem);

  int modify(Version homeItem);


  /**
   * @param id
   * @return
   */
  int delete(@Param("id") String id);


  /**
   * 服务端分页查询数据
   */
  List<Version> list(@Param(value = "pager") Pageable pager, @Param("appType") String appType);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param("appType") String appType);

  /**
   * 根据传入的app类型与版本号，查询app更新类型
   */
  List<Version> selectByVersion(@Param("appType") String appType, @Param("version") String version);

}
