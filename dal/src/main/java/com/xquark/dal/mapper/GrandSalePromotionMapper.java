package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSalePromotion;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface GrandSalePromotionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSalePromotion record);

    int insertSelective(GrandSalePromotion record);

    GrandSalePromotion selectByPrimaryKey(Integer id);

    GrandSalePromotion selectGrandSale();

    List<GrandSalePromotion> selectAll();

    GrandSalePromotion selectNotStated();

    List<GrandSalePromotion> selectNeedEnded();

    int updateByPrimaryKeySelective(GrandSalePromotion record);

    int updateByPrimaryKey(GrandSalePromotion record);

    int updateStatusByJob(@Param("status") Integer status, @Param("id") Integer id);

    boolean selectExistsOpeningSale(@Param("date") Date date);
}