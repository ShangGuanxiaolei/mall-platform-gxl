package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionWhite;
import org.apache.ibatis.annotations.Param;

public interface PromotionWhiteMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionWhite record);

    int insertSelective(PromotionWhite record);

    PromotionWhite selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionWhite record);

    int updateByPrimaryKey(PromotionWhite record);

    Boolean isExistWhiteList(@Param("cpid") Long cpid);

    Boolean isExistByPacket(@Param("cpId") Long cpId);

    Boolean isExistWhiteListBypCode(Long cpId);

    Boolean isExistsByPCode(@Param("cpId") Long cpid, @Param("pCode") String pCode);
}