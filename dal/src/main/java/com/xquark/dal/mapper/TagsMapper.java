package com.xquark.dal.mapper;

import com.xquark.dal.model.Tags;
import com.xquark.dal.type.TagsCategory;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface TagsMapper {

  int deleteByPrimaryKey(String id);

  int deleteByName(String name);

  int insert(Tags record);

  int insertSelective(Tags record);

  Tags selectByPrimaryKey(String id);

  Tags selectByName(String name);

  List<Tags> listTags(@Param("page") Pageable pageable, @Param("category") TagsCategory category);

  List<Tags> selectMultiple(@Param("ids") List<String> ids,
      @Param("category") TagsCategory category);

  long count(@Param("category") TagsCategory category);

  int updateByPrimaryKeySelective(Tags record);

  int updateByPrimaryKey(Tags record);

  boolean exist(String id);

  boolean existName(String name);

}