package com.xquark.dal.mapper;

import com.xquark.dal.model.YundouOrder;

public interface YundouOrderMapper {

  int deleteByPrimaryKey(String id);

  int insert(YundouOrder record);

  int insertSelective(YundouOrder record);

  YundouOrder selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(YundouOrder record);

  int updateByPrimaryKey(YundouOrder record);
}