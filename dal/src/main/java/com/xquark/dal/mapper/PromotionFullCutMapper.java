package com.xquark.dal.mapper;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullCut;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.vo.PromotionFullCutProductInfoVO;
import com.xquark.dal.vo.PromotionFullCutProductVO;
import com.xquark.dal.vo.PromotionFullCutVO;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface PromotionFullCutMapper {

    int deleteByPrimaryKey(String id);

    int insert(PromotionFullCut record);

    int insertSelective(PromotionFullCut record);

    PromotionFullCut selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionFullCut record);

    int updateByPrimaryKey(PromotionFullCut record);

    List<Promotion> listPromotion(@Param("order") String order,
                                  @Param("direction") Sort.Direction direction, @Param(value = "page") Pageable page,
                                  @Param("keyword") String keyword);

    List<PromotionFullCutVO> listPromotionVO(@Param("order") String order,
                                             @Param("direction") Sort.Direction direction, @Param(value = "page") Pageable page,
                                             @Param("keyword") String keyword);

    List<Product> listProductByPromotionId(@Param("id") String id, @Param("gift") Boolean gift,
                                           @Param("order") String order,
                                           @Param("direction") Sort.Direction direction, @Param("page") Pageable pageable);

    Long countProductByPromotionId(@Param("id") String id, @Param("gift") Boolean gift);

    /**
     * 查询全店有效商品的活动及非全店有效中关联该商品的活动
     *
     * @param productId 商品id
     * @param order     排序
     * @param direction 方向
     * @param page      page
     */
    List<PromotionFullCutVO> listPromotionVOByProductId(@Param("productId") String productId,
                                                        @Param("isShare") Boolean isShare,
                                                        @Param("order") String order, @Param("direction") Sort.Direction direction,
                                                        @Param(value = "page") Pageable page);

    PromotionFullCutVO selectById(String id);

    Long countPromotion(@Param("keyword") String keyword);

    int close(String promotionId);

    int deleteProduct(@Param("promotionId") String promotionId, @Param("productId") String productId);

    int emptyProduct(@Param("promotionId") String promotionId, @Param("gift") Boolean gift);

    int hasAdded(@Param("promotionId") String promotionId, @Param("productId") String productId);

    boolean inPromotion(@Param("productId") String productId,
                        @Param("excludePromotionId") String excludePromotionId, @Param("type") PromotionType type,
                        @Param("userScope") PromotionUserScope userScope);

    /**
     * 后台控制统一商品只能参与一种活动, 折扣应该唯一 前提: 单个商品在某个定向人群范围内只能同时参与一个活动
     *
     * @param productId 商品id
     * @param userScope 定向人群
     * @return 活动集合, 防止因为错误数据导致有多条记录, 在service层控制取最高折扣
     */
    List<Promotion> selectCanUsePromotion(@Param("productId") String productId,
                                          @Param("userScope") PromotionUserScope userScope);

    Boolean selectHasCanUsePromotion(@Param("userScope") PromotionUserScope userScope);

    /**
     * 查询活动
     */
    PromotionFullCutProductVO selectProductVOById(String id);

    /**
     * 查询带商品参数的折扣
     */
    List<PromotionFullCutProductInfoVO> listVOWithProductInfo(@Param("id") String id,
                                                              @Param("page") Pageable pageable);

    Long countVOWithProductInfo(@Param("id") String id);

}