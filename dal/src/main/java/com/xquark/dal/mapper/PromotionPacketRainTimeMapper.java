package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPacketRainTime;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PromotionPacketRainTimeMapper {

    int deleteByPrimaryKey(String id);

    int insert(PromotionPacketRainTime record);

    PromotionPacketRainTime selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PromotionPacketRainTime record);

    int updateByPrimaryKey(PromotionPacketRainTime record);

    List<PromotionPacketRainTime> listByPackRainId(@Param("id") String id);

    /**
     * 获取当前时间段的红包雨活动
     */
    PromotionPacketRainTime selectCurrentTimePromotion(@Param("promotionId") String promotionId);

    PromotionPacketRainTime selectClosestTimePromotion(@Param("promotionId") String promotionId);

    /**
     * 查询当前场次的下一场
     */
    PromotionPacketRainTime selectNextAfter(@Param("promotionId") String promotionId, @Param("currEnd") Date currEnd);
}