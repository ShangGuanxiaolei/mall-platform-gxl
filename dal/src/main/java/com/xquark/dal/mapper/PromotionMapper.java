package com.xquark.dal.mapper;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionSimpleReduceVO;
import com.xquark.dal.vo.PromotionVO;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface PromotionMapper {

  Promotion selectByPrimaryKey(String id);

  List<Promotion> selectAvailables();

  int insert(Promotion promotion);

  int update(Promotion promotion);

  int delete(String id);

  int deleteLogically(String id);

  int close(String id);

  Promotion selectByNameAndType(@Param("name") String name, @Param("type") PromotionType type);

  Promotion selectByProductId(@Param("productId") String productId, @Param("date") Date date);

  List<Promotion> listReducePromotionByProductId(@Param("productId") String productId,
      @Param("date") Date date, @Param("isShare") Boolean isShare);

  List<PromotionSimpleReduceVO> listSimpleReducePromotionByProductId(
      @Param("productId") String productId);

  Long countReducePromotionByProductId(@Param("productId") String productId,
      @Param("date") Date date, @Param("isShare") Boolean isShare);

  List<String> listReducePromotionTitleByProductId(@Param("productId") String productId,
      @Param("date") Date date, @Param("isShare") Boolean isShare);


  /**
   * 活动列表，分页获取某个类型活动的所有信息
   */
  List<PromotionVO> getPromotionForApp(@Param(value = "pager") Pageable pager,
      @Param(value = "type") String type);

  /**
   * 查询某种类型的活动是否有已开启的
   */
  Boolean selectHasActivate(PromotionType type);

  /**
   * 根据批次号查询活动
   * @param batchNo
   * @return
   */
  Promotion findDateByBatchNo(String batchNo);

  /**
   * 根据活动时间查询活动
   * @return
   */
  Promotion findPromotionByDate(String batchNo);

  List<String> selectPcodeByTypeOrStatusOrPid(@Param("productId") String productId, @Param("type") String type,
                                        @Param("statusList") List<String> statusList);
}
