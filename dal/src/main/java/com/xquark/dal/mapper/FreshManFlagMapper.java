package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManFlagVo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/11
 * Time:9:59
 * Des:新人注册、首单、vip等弹框相关
 */
public interface FreshManFlagMapper {
    /**
     * 是否存在
     * @param cpId
     * @return
     */
    boolean selectIsExistByCpId(Long cpId);

    /**
     * 更新
     * @param freshManFlagVo
     * @return
     */
    boolean update(FreshManFlagVo freshManFlagVo);

    /**
     * 插入
     * @param freshManFlagVo
     * @return
     */
    boolean insert(FreshManFlagVo freshManFlagVo);

    FreshManFlagVo selectFlag4Toast(Long cpId);
}