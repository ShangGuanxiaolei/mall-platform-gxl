package com.xquark.dal.mapper;

import com.xquark.dal.model.PacketWhitelist;

public interface PacketWhitelistMapper {
  int deleteByPrimaryKey(Long id);

  int insert(PacketWhitelist record);

  int insertSelective(PacketWhitelist record);

  PacketWhitelist selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(PacketWhitelist record);

  int updateByPrimaryKey(PacketWhitelist record);

  boolean selectExists(Long cpId);
}
