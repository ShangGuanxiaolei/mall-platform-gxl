package com.xquark.dal.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.vo.CampaignProductEX;
import com.xquark.dal.vo.XQHomeActProductVO;

public interface CampaignProductMapper {

  int insert(CampaignProduct prod);

  List<CampaignProduct> selectByTicket(@Param("ticketId") String ticketId);

  List<CampaignProduct> selectByProdcut(@Param("productId") String productId);

  CampaignProduct selectByTicketAndProduct(@Param("ticketId") String ticketId,
      @Param("productId") String productId);

  int updateByTicketAndProduct(CampaignProduct prod);

  int updateDisCountByTicket(@Param("ticketId") String ticketId, @Param("discount") Float discount);

  void deleteActivityProducts(@Param("activityId") String activityId,
      @Param("productIds") String... productIds);

  List<CampaignProductEX> loadCampaignProductEX(@Param("activityId") String activityId,
      @Param("shopId") String shopId);

  //partner
  Long countCampaignProductByQuery(@Param("paramsMap") Map<String, Object> paramsMap);

  List<CampaignProductEX> listCampaignProductByQuery(
      @Param("paramsMap") Map<String, Object> paramsMap, @Param("pager") Pageable pager);

  Long countCampaignProduct4Home(@Param("paramsMap") Map<String, Object> paramsMap);

  List<XQHomeActProductVO> listCampaignProduct4Home(
      @Param("paramsMap") Map<String, Object> paramsMap, @Param("page") Pageable page);

  // TODO: fix me
  void auditTicketProduct(@Param("newTicketId") String newTicketId,
      @Param("activityId") String activityId, @Param("oldTicketId") String oldTicketId,
      @Param("productId") String productId, @Param("brand") String brand,
      @Param("shortName") String shortName,
      @Param("sort") Integer sort, @Param("imagePc") String imagePc,
      @Param("imageApp") String imageApp);

  Long selectCountByRange(@Param("from") Date from, @Param("to") Date to,
      @Param("excludeActId") String excludeActId, @Param("productId") String productId);
}
