package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.SmsSendRecord;

public interface SmsSendRecordMapper {

  int insert(SmsSendRecord record);

  int updateSendResult(@Param("mobile") String mobile, @Param("thirdBatchId") String thirdBatchId,
      @Param("status") String status, @Param("thirdResult") String thirdResult);
}
