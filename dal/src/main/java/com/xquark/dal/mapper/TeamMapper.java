package com.xquark.dal.mapper;

import com.xquark.dal.model.Team;
import com.xquark.dal.vo.TeamRecordVO;
import com.xquark.dal.vo.TeamVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface TeamMapper {

  int deleteByPrimaryKey(String id);

  int insert(Team record);

  int insertSelective(Team record);

  TeamVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Team record);

  int updateByPrimaryKey(Team record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<TeamVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 战队禁用，启用
   */
  int updateStatus(@Param(value = "id") String id, @Param(value = "status") String status);

  /**
   * 战队结算记录列表
   */
  List<TeamRecordVO> listCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  /**
   * 战队结算记录
   */
  Long countCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  // 最近一周成交订单数量与金额
  List<Map> getWeekOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 总战队数
   */
  Long countTotalTeam(@Param("shopId") String shopId);

  /**
   * 上月总交易额
   */
  BigDecimal getLastMonthAmount(@Param("shopId") String shopId);

  // 各个战队交易额汇总，取前三名
  List<Map> getTeamAmounts(@Param("shopId") String shopId);

  /**
   * 总交易额
   */
  BigDecimal getTotalAmount(@Param("shopId") String shopId);

  /**
   * 查询用户在哪个战队中
   */
  TeamVO selectByUserId(@Param("userId") String userId);

  /**
   * 根据线下门店查询对应的战队信息
   */
  Team selectByStoreId(@Param("storeId") String storeId);


  /**
   * 获取系统中所有的战队数据
   */
  List<Team> getAllTeam();

  /**
   * 删除线下门店，同时删掉对应的战队
   */
  int deleteByStoreId(@Param("storeId") String storeId);
}