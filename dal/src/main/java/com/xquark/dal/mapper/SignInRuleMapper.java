package com.xquark.dal.mapper;

import com.xquark.dal.model.SignInRule;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public interface SignInRuleMapper {

  int deleteByPrimaryKey(String id);

  int insert(SignInRule record);

  int insertSelective(SignInRule record);

  SignInRule selectByPrimaryKey(String id);

  SignInRule selectOne();

  List<SignInRule> listSignInRule(String order, Direction direction, Pageable pageable);

  int updateByPrimaryKeySelective(SignInRule record);

  int updateByPrimaryKey(SignInRule record);
}