package com.xquark.dal.mapper;

import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.vo.TwitterProductCommissionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TwitterProductCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(TwitterProductCommission record);

  int insertSelective(TwitterProductCommission record);

  TwitterProductCommissionVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TwitterProductCommission record);

  int updateByPrimaryKey(TwitterProductCommission record);

  int updateForArchive(String id);

  TwitterProductCommission selectComByProductIdAndShopId(
      @Param(value = "productId") String productId, @Param(value = "shopId") String shopId);

  /**
   * 服务端分页查询数据
   */
  List<TwitterProductCommission> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(@Param(value = "shopId") String shopId,
      @Param(value = "productId") String productId);

}