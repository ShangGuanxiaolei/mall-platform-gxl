package com.xquark.dal.mapper;

import com.xquark.dal.model.PersonalBill;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午6:45
 * 个人发票的数据访问对象
 */
public interface PersonalBillMapper {

    /**
     * 添加
     */
    int insert(PersonalBill obj);

    /**
     * 批量插入
     *
     * @param collection
     * @return
     */
    int batchInsert(@Param("collection") List<PersonalBill> collection);

    /**
     * 通过id查找
     */
    PersonalBill getByPrimaryKey(@Param("id") String id);

    /**
     * 更新
     */
    int update(PersonalBill obj);


    /**
     * 通过id删除
     */
    int delete(@Param("id") String id);

    /**
     * 批量删除
     */
    int batchDelete(@Param("collection") List<String> collection);

    /**
     * 计算数据的数量
     */
    Long count(@Param("params") Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    List<PersonalBill> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

    Integer haveBill(@Param("orderId") String orderId);
}