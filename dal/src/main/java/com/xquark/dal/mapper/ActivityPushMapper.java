package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityPush;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author luqig
 * @since 2019-04-29
 */
public interface ActivityPushMapper {
  int deleteById(@Param("id") String id);

  int insert(ActivityPush record);

  ActivityPush loadById(@Param("id") String id);

  int updateByPrimaryKeySelective(ActivityPush record);

  int updateById(ActivityPush record);

  List<ActivityPush> findNotTimeOut(Integer minute);
}