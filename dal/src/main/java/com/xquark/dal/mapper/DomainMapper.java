package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.Domain;

public interface DomainMapper {

  int deleteByPrimaryKey(@Param("id") String id);

  int insert(Domain domain);

  Domain selectByPrimaryKey(@Param("id") String id);

  int updateByPrimaryKeySelective(Domain record);

  Domain selectByCode(@Param("code") String code);

  Domain selectRootDomain();

}