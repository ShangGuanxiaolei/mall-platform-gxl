package com.xquark.dal.mapper;

import com.xquark.dal.model.Brand;
import com.xquark.dal.type.ProductSource;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BrandMapper {

  int deleteByPrimaryKey(String id);

  int batchDelete(@Param("collection") List<String> ids);

  int insert(Brand record);

  int insertRaw(Brand record);

  Brand selectByPrimaryKey(String id);

  Brand selectByThird(@Param("thirdId") String thirdId, @Param("source") ProductSource source);

  /**
   * 硬编码查询顺丰品牌
   */
  Brand selectSFBrand();

  int updateByPrimaryKeySelective(Brand record);

  int updateByPrimaryKey(Brand record);

  /**
   * 分页查询
   */
  List<Brand> list(@Param("params") Map<String, Object> params);


  /**
   * 查询商品关联的品牌
   */
  List<Brand> listRelatedBrand(@Param("productId") String productId);

  /**
   * 计算数据的数量
   */
  Long count(@Param("params") Map<String, Object> params);

    List<Brand> getNameAndId();
}