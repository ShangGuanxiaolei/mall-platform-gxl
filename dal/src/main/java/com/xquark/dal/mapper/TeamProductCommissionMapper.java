package com.xquark.dal.mapper;

import com.xquark.dal.model.TeamProductCommission;
import com.xquark.dal.vo.TeamProductCommissionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TeamProductCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(TeamProductCommission record);

  int insertSelective(TeamProductCommission record);

  TeamProductCommissionVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamProductCommission record);

  int updateByPrimaryKey(TeamProductCommission record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<TeamProductCommissionVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(@Param(value = "shopId") String shopId,
      @Param(value = "productId") String productId);
}