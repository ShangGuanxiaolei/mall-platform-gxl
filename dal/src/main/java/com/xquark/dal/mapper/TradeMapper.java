package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


public interface TradeMapper {

  /**
   * 获取总收入
   */
  BigDecimal getTotalIn();

  /**
   * 获取总支出
   */
  BigDecimal getTotalOut();

  /**
   * 获取总待结算
   */
  BigDecimal getTotalWaiting();

  /**
   * 获取一周的交易趋势
   */
  List<Map> getAmount(@Param("cdate") List cdate);

}