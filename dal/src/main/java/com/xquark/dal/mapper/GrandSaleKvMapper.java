package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSaleKv;

import java.util.List;

public interface GrandSaleKvMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleKv record);

    int insertSelective(GrandSaleKv record);

    GrandSaleKv selectByPrimaryKey(Integer id);

    List<GrandSaleKv> selectByGrandSaleId(Integer grandSaleId);

    String selectMainKV();

    int updateByPrimaryKeySelective(GrandSaleKv record);

    int updateByPrimaryKey(GrandSaleKv record);
}