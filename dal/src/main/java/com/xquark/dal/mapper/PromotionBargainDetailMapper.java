package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.vo.PromotionBargainDetailUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionBargainDetailMapper {
  int deleteByPrimaryKey(String id);

  int insert(PromotionBargainDetail record);

  /**
   * 查询同一场活动，用户是否已经发起过砍价
   *
   * @param bargainId 活动id
   * @param userId 用户id
   * @return true or false
   */
  boolean selectBargainExists(@Param("bargainId") String bargainId, @Param("userId") String userId);

  /**
   * 查询userId
   *
   * @param userId
   * @return
   */
  boolean selectUserExists(@Param("userId") String userId);

  PromotionBargainDetail selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionBargainDetail record);

  int updateByPrimaryKey(PromotionBargainDetail record);

  Long countMyPromotion();

  List<PromotionBargainDetailUserVO> selectDetailUserById(String userId);

  /**
   * 根据活动id和skuid以及userId关联查询用户是否发起活动及下单状态
   *
   * @param skuId skuId
   * @param bargainId 活动id
   * @param userId 用户id
   * @return 是否发起砍价
   */
  boolean isLaunch(
      @Param("skuId") String skuId,
      @Param("bargainId") String bargainId,
      @Param("userId") String userId);

  PromotionBargainDetailUserVO loadMySchedule(
      @Param("bargainDetailId") String bargainDetailId);

  /**
   * 通过活动id和skuId查询活动已发起数量
   *
   * @param bargainId 活动id
   * @param skuId sku id
   * @return 已发起的该活动数量
   */
  int selectlaunchNumByBargainIdAndSkuId(
      @Param("bargainId") String bargainId, @Param("skuId") String skuId);
}
