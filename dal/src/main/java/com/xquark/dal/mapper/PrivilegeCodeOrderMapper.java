package com.xquark.dal.mapper;

import com.xquark.dal.model.PrivilegeCodeOrder;
import com.xquark.dal.model.PrivilegeCodeUser;
import org.apache.ibatis.annotations.Param;

public interface PrivilegeCodeOrderMapper {

  int insert(PrivilegeCodeOrder rule);

  PrivilegeCodeOrder selectByPrimaryKey(@Param("id") String id);

  int modify(PrivilegeCodeOrder role);

  int delete(@Param("id") String id);

  int deleteByOrderId(@Param("orderId") String orderId);

  PrivilegeCodeOrder selectByOrderId(@Param("orderId") String orderId);

}
