package com.xquark.dal.mapper;

import com.xquark.dal.type.TradeType;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PaymentMerchant;
import com.xquark.dal.type.PaymentMode;

public interface PaymentMerchantMapper {

  int insert(PaymentMerchant record);

  PaymentMerchant selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PaymentMerchant record);

  List<PaymentMerchant> list(@Param("profile") String profile,
      @Param("payType") PaymentMode payType, @Param("domain") String... domain);

  /**
   * 获取当前环境当前支付方式的配置信息
   *
   * @param tradeType WEB/APP
   */
  PaymentMerchant selectByTradeType(@Param("profile") String profile,
      @Param("tradeType") String tradeType, @Param("payType") PaymentMode payType,
      @Param("domain") String... domain);
}