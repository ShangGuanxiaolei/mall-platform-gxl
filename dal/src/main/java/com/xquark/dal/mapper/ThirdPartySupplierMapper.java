package com.xquark.dal.mapper;

import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SupplierLogisticsExport;
import com.xquark.dal.vo.SupplierSkuExportVO;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @auther liuwei
 * @date 2018-06-29 21:22
 */
public interface ThirdPartySupplierMapper {

  /**
   * 订单查询
   * @param params
   * @return
   */
  List<OrderVO> selectOrders4WxExport(String params);

  /**
   * sku查询
   * @param supplierCode
   * @return
   */
  List<SupplierSkuExportVO> selectSkus4Export(String supplierCode);

  /**
   * 物流查询
   * @param orderNo
   * @return
   */
  SupplierLogisticsExport selectLogistics(String orderNo);

  /**
   * 更新sku库存信息
   * @param skuCode
   * @param barCode
   * @param amount
   */
  void updateSkuAmount(@Param("skuCode") String skuCode,@Param("barCode") String barCode,
      @Param("amount") Integer amount);

  /**
   * 更新物流信息
   * @param orderNo
   * @param logisticsCompany
   * @param logisticsOrderNo
   */
  void updateLogisticsInfo(@Param("orderNo") String orderNo,
      @Param("logisticsCompany") String logisticsCompany,
      @Param("logisticsOrderNo") String logisticsOrderNo);

  /**
   * 查询订单物品关联的SKU编码和条形码
   */
  String selectSkuCodeBySkuId(@Param("skuId") String skuId);

  String selectBarCodeBySkuId(@Param("skuId") String skuId);

}
