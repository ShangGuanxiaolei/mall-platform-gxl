package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductDistributor;
import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.ShopAdmin;
import com.xquark.dal.vo.ShopProp;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ProductDistributorMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int undeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(ProductDistributor record);

  ProductDistributor selectByPrimaryKey(String id);

  ProductDistributor selectByProductIdAndShopId(@Param(value = "productId") String productId,
      @Param(value = "shopId") String shopId);

  List<ProductDistributor> listByShopId(@Param(value = "shopId") String shopId,
      @Param(value = "category") String category, @Param(value = "page") Pageable page);

  int updateByPrimaryKeySelective(Shop record);

  int updateForInstock(String id);

  int updateForOnsale(String id);

  long countProductsSellerByProductId(@Param(value = "productId") String productId);


}