package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPgDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionPgDetailMapper {

    /**
     * 查询拼团配置
     */
    PromotionPgDetail seleceAllByCode(@Param("pDetailCode")String pDetailCode,@Param("pCode")String pCode);

    PromotionPgDetail selectByDetailCode(@Param("detailCode") String detailCode);

    /**
     * 查询所有拼团配置
     * @param pCode 活动编码
     * @return 活动配置
     */
    List<PromotionPgDetail> selectAllByPCode(@Param("pCode") String pCode);

}
