package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargain;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import com.xquark.dal.vo.PromotionBargainProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface PromotionBargainMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionBargain record);

  PromotionBargain selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PromotionBargain record);

  int updateByPrimaryKey(PromotionBargain record);

  /**
   * 查询所有会场砍价活动，此处未关联订单
   *
   * @param order 默认值为create_at
   * @param direction dir
   * @param pageable page
   * @return 会场活动
   */
  List<PromotionBargainPlaceVO> selectAllBargain(
      @Param("order") String order,
      @Param("direction") Sort.Direction direction,
      @Param("page") Pageable pageable);

  /**
   * 查询活动表所有活动，用来给砍价定时器更改活动状态
   *
   * @return PromotionBargain对象
   */
  List<PromotionBargain> selectAllBargainForJob();

  /**
   * 砍价会场，查询所有活动，关联查询出订单状态
   *
   * @param skuId 商品skuid
   * @param bargainId 活动id
   * @param userId 用户id
   * @return 返回单个sku
   */
  PromotionBargainPlaceVO selectAllBargainByUserId(
      @Param("skuId") String skuId,
      @Param("bargainId") String bargainId,
      @Param("userId") String userId);

  List<PromotionBargainPlaceVO> selectMyBargainRecordHelp(String userId);

  /**
   * 通过活动id跟商品id查询砍价商品VO
   * @param promotionId 活动id
   * @param productId 商品id
   * @return 砍价活动商品对象
   */
  PromotionBargainProductVO selectVOByPIdAndProduct(@Param("promotionId") String promotionId, @Param("productId") String productId);
}
