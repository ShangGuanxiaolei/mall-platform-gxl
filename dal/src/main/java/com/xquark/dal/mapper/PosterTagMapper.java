package com.xquark.dal.mapper;

import com.xquark.dal.model.PosterTag;

public interface PosterTagMapper {

  PosterTag load(String id);

  int insert(PosterTag po);

  PosterTag delete(String posterId, String id);

}
