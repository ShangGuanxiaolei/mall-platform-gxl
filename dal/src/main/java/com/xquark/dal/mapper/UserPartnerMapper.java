package com.xquark.dal.mapper;

import com.xquark.dal.model.UserPartner;
import com.xquark.dal.vo.PartnerCmVO;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.UserPartnerApplyVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface UserPartnerMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int unDeleteByPrimaryKey(@Param(value = "id") String id);

  int insert(UserPartner record);

  int insertSelective(UserPartner record);

  UserPartner selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(UserPartner record);

  int updateByPrimaryKey(UserPartner record);

  List<PartnerCmVO> selectUserPartnersCmByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  Long countUserPartnersCmByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  UserPartner selectActiveUserPartnersByUserIdAndShopId(@Param("userId") String userId,
      @Param("ownerShopId") String ownerShopId);

  int autoAudit();

  /**
   * 获取一周的合伙人交易额
   */
  List<Map> getAmount(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的合伙人订单
   */
  List<Map> getOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的新增合伙人
   */
  List<Map> getPartner(@Param("shopId") String shopId, @Param("cdate") List cdate);

  /**
   * 获取一周的合伙人佣金
   */
  List<Map> getCommission(@Param("shopId") String shopId, @Param("cdate") List cdate);

  List<UserPartner> selectUserPartnersByShopId(@Param("shopId") String shopId);

  List<PartnerOrderCmVO> listPartnerCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params, @Param("page") Pageable pageable);

  Long countPartnerCommissionByShopId(@Param("shopId") String shopId,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 合伙人申请列表
   */
  List<UserPartnerApplyVO> listPartnerApply(@Param(value = "params") Map<String, Object> params,
      @Param("page") Pageable pageable);

  Long countPartnerApply(@Param(value = "params") Map<String, Object> params);

  UserPartner selectUserPartnersByUserIdAndShopId(@Param("userId") String userId,
      @Param("shopId") String shopId);

  UserPartner selectUserPartnersByUserId(@Param("userId") String userId);

  int deleteByUserId(@Param(value = "userId") String userId);

}