package com.xquark.dal.mapper;

import com.xquark.dal.model.InsideUserEmployee;
import org.apache.ibatis.annotations.Param;

public interface InsideUserEmployeeMapper {

  int deleteByPrimaryKey(Long id);

  int insert(InsideUserEmployee record);

  InsideUserEmployee selectByPrimaryKey(String id);

  InsideUserEmployee selectByUserId(String userId);

  int updateByPrimaryKeySelective(InsideUserEmployee record);

  int updateByUserIdSelective(InsideUserEmployee record);

  int updateByPrimaryKey(InsideUserEmployee record);

  int deleteByEmployeeId(Long id);

  boolean selectIsUserExists(@Param("userId") String userId);

  boolean selectIsEmployeeIdInUser(@Param("employeeId") Long employeeId);
}