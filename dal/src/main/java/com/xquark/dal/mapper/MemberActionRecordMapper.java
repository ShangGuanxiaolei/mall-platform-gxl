package com.xquark.dal.mapper;

import com.xquark.dal.model.MemberActionRecord;

/**
 * Created by quguangming on 16/6/27.
 */
public interface MemberActionRecordMapper {

  int insert(MemberActionRecord record);
}
