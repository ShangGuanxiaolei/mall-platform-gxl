package com.xquark.dal.mapper;

import com.xquark.dal.model.BrandProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BrandProductMapper {

  int deleteByPrimaryKey(String id);

  int insert(BrandProduct record);

  BrandProduct selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(BrandProduct record);

  int updateByPrimaryKey(BrandProduct record);

  boolean selectExistsByProductIdAndBrandId(
      @Param("productId") String productId, @Param("brandId") String brandId);

  int deleteRelatedBrand(@Param("productId") String productId);

  int batchInsertBrandWithProduct(@Param("productId") String productId,
      @Param("collection") List<String> ids);

  List<String> getProductCode();

  void updateByCode(BrandProduct brandProduct);

  void insertExcel(BrandProduct brandProduct);

  /**
   * 硬编码　查询欧飞品牌下的所有产品ID
   * @return
   */
  List<Long> selectOFBrandProduct();
}