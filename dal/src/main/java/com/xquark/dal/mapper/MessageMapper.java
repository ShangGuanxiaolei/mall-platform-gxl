package com.xquark.dal.mapper;

import com.xquark.dal.model.Message;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface MessageMapper {

  int insert(Message record);

  int updateByPrimaryKeySelective(Message record);

  Message selectByPrimaryKey(String id);

  int delete(String id);

  List<Message> listMessageByAdmin(@Param(value = "params") Map<String, ?> params,
      @Param(value = "page") Pageable page);

  Long countMessageByAdmin(@Param(value = "params") Map<String, ?> params);
}
