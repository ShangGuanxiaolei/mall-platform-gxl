package com.xquark.dal.mapper;

import com.xquark.dal.model.User;
import com.xquark.dal.vo.UserEmployeeVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface UserMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int insert(User record);

  User selectByPrimaryKey(String id);

  User selectByCpId(Long cpId);

  UserEmployeeVO selectEmployeeVOByPrimaryKey(String id);

  String selectUnionIdByPrimaryKey(String id);

  int updateByPrimaryKeySelective(User record);

  int updateSCrmUserInfoByUnionId(User record);

  int updateByPrimaryKeySelectiveNoMatterArchived(User record);

  int updateByPrimaryKey(User record);

  int countRegistered(String mobile);

  User loadByLoginname(String loginname);

  int changePwd(@Param("id") String uid, @Param("newPwd") String newPwd);

  int updateYundou(@Param("id") String uid, @Param("newAmount") Long amount);

  boolean emptyUserPassword(@Param("id") String userId);

  boolean emptyUserPasswordByLoginname(@Param("loginname") String loginname);

  int updateNameAndIdCardNumByPrimaryKey(@Param("id") String id, @Param("name") String name,
      @Param("idCardNum") String idCardNum);

  int saveWithDrawType(@Param("id") String id, @Param("type") int type);

  User selectByPKAndAdmin(String id);

  List<User> loadByRoles(Long roles);

  User selectByDomainAndExtUid(@Param("domain") String domain, @Param("extUid") String extUid);

  User selectByExtUid(@Param("extUid") String extUid);

  void addCode(String p);

  List<User> listNoCodeUsers();

  User loadAnonymousByPhone(@Param("phone") String phone);

  int updateAnonymousPhone(@Param("cid") String cid, @Param("phone") String phone);

  int updateByCidSelective(User user);

  int updateByUnionIdSelective(User user);

  int updateByCpIdSelective(User user);

  List<User> loadByMoreParam(@Param("loginname") String loginname,
      @Param("weixinCode") String weixinCode, @Param("weiboCode") String weiboCode,
      @Param("cid") String cid);

  User loadByWechatParam(@Param("loginName") String loginName,
      @Param("weixinCode") String weixinCode, @Param("archive") Boolean archive);

  List<User> listUsersByRootShopId(@Param("rootShopId") String rootShopId,
      @Param("name") String name, @Param("phone") String phone, @Param("page") Pageable pageable);

  Long countUsersByRootShopId(@Param("rootShopId") String rootShopId, @Param("name") String name,
      @Param("phone") String phone);

  int updateWxQRcodeCard(String id, String qrcode);

  String getWxQRcodeCard(String id);

  int updateCid2Null(@Param("id") String id);

  int updateCid3Null(@Param("id") String id);

  /**
   * 根据手机号查询用户FunctionSet字段，为99代表登陆不需要短信验证码
   */
  long getFunctionSet(@Param("phone") String phone);

  User loadByUnionId(String unionId);

  int updateFanCardNo(@Param("userId") String userId, @Param("code") String code);

  Long selectCurrentYundou(String userId);

  int updateYundouAdd(@Param("userId") String userId, @Param("yundou") String yundou);

  boolean selectUserExistsByUserId(@Param("userId") String userId);

  boolean selectUserExistsByUnionId(@Param("unionId") String unionId);

  boolean selectUserExistsByCpId(@Param("cpId") Long cpId);

  Long selectCpIdByUserId(String userId);

  boolean selectIsFreeLogisticUsed(Long cpId);

  List<User> queryUserByPhone(@Param("phone") String phone);

  void updateTinCode(@Param("cpId") Long cpId, @Param("tinCode") String tinCode);

  User selectUserIdByCpId(String cpId);

  User selectUserByUserId(@Param("userId")String userId);
  Long selectCpIdNotEncodingByUserId(@Param("id")String id);

    boolean updateRegisterPointFlag(@Param("cpId") Long cpId, @Param("flag") int flag);

  String selectIdType(@Param("cpId")String cpId);

  String selectViViType(@Param("cpId")String cpId);

  String selectIdTypeByPhone(@Param("phone")String phone);

  String selectIdTypeByUnionId(@Param("unionId")String unionId);

  String selectIdTypeByExtUid(@Param("extUid")String extUid);

  int selectExpOfficerByCpId(@Param("cpId")Long cpId);

  /**
   * 校验是否已经发送德分
   * @param cpId
   * @param name
   * @return
   */
  boolean checkGrantPoint(@Param("cpId") Long cpId, @Param("name") String name);


  void updateHeadImageByUnionId(@Param("headImage") String headImage, @Param("unionId") String unionId);

  void updateWechatHeadImageByUnionId(@Param("headImage") String headImage, @Param("unionId") String unionId);

  void updateWechatNickNameByUnionId( @Param("unionId") String unionId,@Param("nickName") String nickName);


  boolean checkGrantCommission(@Param("cpId") Long cpId, @Param("name") String name);


  /**
   * 更新用户来源
   * @param cpId
   * @return
   */
  boolean updateFromSource(@Param("cpId")Long cpId,@Param("fromSource") int fromSource);

    void updateGrantCommission(@Param("cpId") Long cpId, @Param("name") String name);
}