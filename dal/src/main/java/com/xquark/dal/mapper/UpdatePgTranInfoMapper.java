package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author qiuchuyi
 * @date 2018/9/12 15:08
 */
public interface UpdatePgTranInfoMapper {
//    @Update("UPDATE promotion_pg_tran_info set piece_status=#{pieceStatus} where pieceGroupTranCode=#{pieceGroupTranCode}")
    Integer UpdatePgTranInfoPieceGroupTranCode(@Param("pieceGroupTranCode") String pieceGroupTranCode, @Param("pieceStatus") String pieceStatus);
}
