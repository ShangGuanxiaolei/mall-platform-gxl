package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductCollection;
import com.xquark.dal.vo.ProductCollectionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ProductCollectionMapper {

  int deleteByPrimaryKey(String id);

  int insert(ProductCollection record);

  int insertSelective(ProductCollection record);

  ProductCollection selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ProductCollection record);

  int updateByPrimaryKey(ProductCollection record);

  int updateForArchive(String id);

  /**
   * 服务端分页查询数据
   */
  List<ProductCollectionVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 用户取消某个商品收藏
   */
  int deleteByProductId(@Param(value = "userId") String userId,
      @Param(value = "productId") String productId);

  /**
   * 查询用户是否收藏某个商品
   */
  ProductCollection selectByProductId(@Param(value = "userId") String userId,
      @Param(value = "productId") String productId);

  /**
   * 用户我的收藏商品列表
   */
  List<ProductCollectionVO> listByApp(@Param(value = "pager") Pageable pager,
      @Param(value = "userId") String userId);

  /**
   * 我的收藏总数
   */
  Long selectCntByUserId(@Param(value = "userId") String userId);

}