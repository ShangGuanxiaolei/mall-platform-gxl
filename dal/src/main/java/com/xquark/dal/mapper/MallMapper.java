package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by chh on 16-9-13.
 */
public interface MallMapper {

  // 今日交易额
  BigDecimal getTodayAmount(@Param("shopId") String shopId);

  // 今日订单数
  BigDecimal getTodayOrder(@Param("shopId") String shopId);

  // 待付款订单
  BigDecimal getSubmitOrder(@Param("shopId") String shopId);

  // 待发货订单
  BigDecimal getPaidOrder(@Param("shopId") String shopId);

  // 维权订单
  BigDecimal getRefundOrder(@Param("shopId") String shopId);

  // 本周总订单
  List<Map> getWeekOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  // 本周已成功订单
  List<Map> getWeekSuccessOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  // 本周已发货订单
  List<Map> getWeekShipOrder(@Param("shopId") String shopId, @Param("cdate") List cdate);

  // 销量排行商品
  List<Map> getTopProduct(@Param("shopId") String shopId);

  // 本周交易额
  BigDecimal getWeekAmount(@Param("shopId") String shopId);

  // 本月交易额
  BigDecimal getMonthAmount(@Param("shopId") String shopId);

  // 本年交易额
  BigDecimal getYearAmount(@Param("shopId") String shopId);

  // 本年总订单
  List<Map> getYearOrder(@Param("shopId") String shopId);

  // 本年已成功订单
  List<Map> getYearSuccessOrder(@Param("shopId") String shopId);

  // 本年已发货订单
  List<Map> getYearShipOrder(@Param("shopId") String shopId);

}
