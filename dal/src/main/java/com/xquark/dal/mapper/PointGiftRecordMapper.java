package com.xquark.dal.mapper;

import com.xquark.dal.model.PointGiftRecord;
import com.xquark.dal.model.UserPointDTO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface PointGiftRecordMapper {
    int deleteByPrimaryKey(String id);

    int insert(PointGiftRecord record);

    int insertSelective(PointGiftRecord record);

    PointGiftRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PointGiftRecord record);

    int updateByPrimaryKey(PointGiftRecord record);

    List<PointGiftRecord> selectPointPacketDetailByCpid(@Param("cpId") Long cpId);

    BigDecimal selectTotalAmountByCpid(@Param("cpId") Long cpId);

    List<UserPointDTO> getPointPacketDetailByGiftNo(String pointGiftNo);

    PointGiftRecord selectByUserGiftNo(PointGiftRecord record);

    BigDecimal periodGiftPoint(@Param(value = "startTime") Long startTime,
                               @Param(value = "endTime") Long endTime,
                               @Param(value = "receiveCpid") String receiveCpid);
    UserPointDTO getPointReceviceDetailByPointGiftNo(@Param(value = "pointGitNo") String pointGitNo,
                                                              @Param(value = "cpid") Long cpid,
                                                              @Param(value = "fromCpid") Long fromCpid);
}