package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderHeader;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface OrderHeaderMapper {

  int insert(OrderHeader record);

  int insertSelective(OrderHeader record);

  int batchInsert(@Param("collection") List<OrderHeader> list);

  BigDecimal selectRetailPerformance(@Param("cpId") Long cpId, @Param("month") String month);

  int updateStatusByOrderNo(@Param("status") String status, @Param("orderNo") String orderNo);

  int updateOtherMonthByOrderNo(@Param("otherMonth") int otherMonth, @Param("orderNo") String orderNo);

  int updateStatusSelective(OrderHeader order);

  Long selectFromCpId(@Param("orderNo") String orderNo);

  int batchUpdateTranOrderStatusToPaidByOrderNo(@Param("orderNos") List<String> orderNos);

  BigDecimal selectPPVPerformanceByHvmall(@Param("cpId") Long cpId, @Param("month") Integer month);

  BigDecimal selectPPVPerformanceByAll(@Param("cpId") Long cpId, @Param("month") Integer month);

  BigDecimal selectPVPerformanceByHvmall(@Param("cpId") Long cpId, @Param("month") Integer month);

  BigDecimal selectPVPerformanceByAll(@Param("cpId") Long cpId, @Param("month") Integer month);

  List<OrderHeader> selectWhiteOrderList(@Param("toDay") String toDay);

  int updateJoinTypeByOrderNo(@Param("joinType")int joinType,@Param("orderNo")String orderNo);

  int updateJoinTypeByOrderNoForPiece(@Param("orderNo") String orderNo, @Param("cpId")String cpId);

  Integer selectJoinTypeByOrderNo(@Param("orderNo") String orderNo);
}