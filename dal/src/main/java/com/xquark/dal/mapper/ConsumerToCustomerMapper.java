package com.xquark.dal.mapper;

import com.xquark.dal.model.ConsumerToCustomer;
import org.apache.ibatis.annotations.Param;

public interface ConsumerToCustomerMapper {

  int deleteByPrimaryKey(Long id);

  int insert(ConsumerToCustomer record);

  ConsumerToCustomer selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(ConsumerToCustomer record);

  int updateByPrimaryKey(ConsumerToCustomer record);

  /**
   * 查询绑定关系, 查询是否有分享人(upline)
   */
  Long selectCustomerIdByConsumeId(@Param("consumeId") Long consumeId);
}