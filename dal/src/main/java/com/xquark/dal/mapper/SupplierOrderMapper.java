package com.xquark.dal.mapper;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.WmsExpress;
import com.xquark.dal.model.WmsOrder;
import com.xquark.dal.model.WmsOrderNtsProduct;
import com.xquark.dal.model.WmsOrderPackage;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.RefundType;
import com.xquark.dal.type.SendErpDest;
import com.xquark.dal.type.SendErpStatus;
import com.xquark.dal.type.SysnErpStatus;
import com.xquark.dal.vo.Customer;
import com.xquark.dal.vo.OrderItemWithOrderVO;
import com.xquark.dal.vo.OrderVO;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface SupplierOrderMapper {

  List<OrderVO> selectOrders4WxExport(@Param("params") Map<String, Object> params);

}