package com.xquark.dal.mapper;

import com.xquark.dal.model.ProductUser;
import com.xquark.dal.vo.ProductBasicVO;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface ProductUserMapper {

  int deleteByPrimaryKey(String id);

  int insert(ProductUser record);

  int insertSelective(ProductUser record);

  ProductUser selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(ProductUser record);

  int updateByPrimaryKey(ProductUser record);

  boolean selectIsBounded(@Param("productId") String productId, @Param("userId") String userId);

  int deleteBounded(@Param("productId") String productId, @Param("userId") String userId);

  List<ProductBasicVO> listBoundedProducts(
      @Param("userId") String userId, @Param("pageable") Pageable pageable);

  Long countBoundedProducts(@Param("userId") String userId);
}