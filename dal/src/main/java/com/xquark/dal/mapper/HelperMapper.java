package com.xquark.dal.mapper;

import com.xquark.dal.model.Helper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

public interface HelperMapper {

  int deleteByPrimaryKey(String id);

  int insert(Helper record);

  int insertSelective(Helper record);

  Helper selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(Helper record);

  int updateByPrimaryKeyWithBLOBs(Helper record);

  int updateByPrimaryKey(Helper record);

  List<Helper> list(@Param("page") Pageable pageable);

  List<Helper> listWithOutBlob(@Param("page") Pageable pageable);

  Long count();
}