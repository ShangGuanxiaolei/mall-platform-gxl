package com.xquark.dal.mapper;

import com.xquark.dal.model.TwitterLevel;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TwitterLevelMapper {

  int deleteByPrimaryKey(@Param(value = "id") String id);

  int insert(TwitterLevel record);

  TwitterLevel selectByPrimaryKey(@Param(value = "id") String id);

  int updateByPrimaryKey(TwitterLevel record);

  /**
   * 服务端分页查询数据
   */
  List<TwitterLevel> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 查询某种类型的所有推客等级
   */
  List<TwitterLevel> getByType(@Param(value = "shopId") String shopId,
      @Param(value = "type") String type);

  /**
   * 获取某个用户的推客等级
   */
  TwitterLevel selectByUserId(@Param(value = "shopId") String shopId,
      @Param(value = "userId") String userId);

}