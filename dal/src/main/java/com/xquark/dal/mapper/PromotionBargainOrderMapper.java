package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionBargainOrder;

public interface PromotionBargainOrderMapper {
    int deleteByPrimaryKey(String id);

    int insert(PromotionBargainOrder record);

    int updateByPrimaryKeySelective(PromotionBargainOrder record);

    int updateByPrimaryKey(PromotionBargainOrder record);
}