package com.xquark.dal.mapper;

public interface ActivityPtCancelMapper {
    void updatePtStatus(String status,String groupCode);
}
