package com.xquark.dal.mapper;

import com.xquark.dal.model.ActivityExclusive;
import com.xquark.dal.model.PromotionVipsuit;
import com.xquark.dal.model.VipProduct;
import com.xquark.dal.model.VipProductBlackList;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionVipsuitMapper {

    /**
     * 查询所有VIP商品套装列表，并根据发布时间排序
     * @return List<VipProduct>
     */
    List<VipProduct> selectByReleaseTime();

    /**
    * 查询所有VIP推荐商品
    */
    List<VipProduct> selectVIPRecommends(@Param("productId") String productId);

    /**
     * 根据商品ID查询VIP套装表中数据是否存在
     * @param productId
     * @return int
     */
    Boolean findVipProductByProductId(@Param("productId") String productId);

    /**
     * 查询所有VIP套装列表，用于后台列表展示
     * @return List<VipProduct>
     */
    List<VipProductBlackList> selectListUseforBackstage(@Param(value = "page") Pageable page);

    /**
     * 添加普通商品到VIP套装表中
     * @param promotionVipsuit
     * @return
     */
    int insertVipProductByProductId(PromotionVipsuit promotionVipsuit);

    /**
     * 将VIP套装的商品添加到活动商品表中
     * @param activityExclusive
     * @return
     */
    int insertVipProductToExclusive(ActivityExclusive activityExclusive);

    /**
     * 查询指定的商品数据
     * @param productCode
     * @return
     */
    PromotionVipsuit selectByProductId(@Param("productCode") String productCode);

    //根据id查询vip套装
    PromotionVipsuit selectVipSuitByid(@Param("productid") Integer productid);

    //更新vip套装状态
    void updateVipStatu(@Param("productid") Integer productid, @Param("status") Integer status);

    //更新活动表商品状态
    void updateActivityExclusive(@Param("productid") Integer productid, @Param("status") Integer status);

    /**
     * VIP套装总记录数
     * @return
     */
    Long countVipsuits();

    /**
     * 查询套装表和商品表都存在的套装信息
     * @param ids
     * @return
     */
    List<PromotionVipsuit> selectProductIdFromVipsuit(@Param("ids") String... ids);

    /**
     * 查询活动表和VIP套装表中的活动商品信息
     * @return
     */
    List<ActivityExclusive> selectExclusiveByStatus();

}
