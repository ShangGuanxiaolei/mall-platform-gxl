package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionPg;
import org.apache.ibatis.annotations.Param;

public interface PromotionPgMapper {

  PromotionPg selectPromotionPgBypCode(String pCode);

  String selectCanJoinGroup(@Param("pCode")String pCode);

  int newPg(@Param("pCode")String pCode);

}
