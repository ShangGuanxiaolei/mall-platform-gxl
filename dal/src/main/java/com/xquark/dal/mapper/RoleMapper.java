package com.xquark.dal.mapper;

import com.xquark.dal.model.Role;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;


public interface RoleMapper {

  Role selectByPrimaryKey(@Param("id") String id);

  int insert(Role role);

  int modify(Role role);

  int delete(@Param("id") String id);

  /**
   * 列表查询
   */
  List<Role> list(@Param(value = "pager") Pageable page, @Param("keyword") String keyword);

  List<Role> listAll();

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(@Param("keyword") String keyword);

  /**
   * 根据编码和归属获取对应角色信息
   */
  Role selectByCodeAndBelong(@Param("code") String code, @Param("belong") String belong);

  /**
   * 查询某个归属内的所有角色信息
   */
  List<Role> listByBelong(@Param("belong") String belong);

}
