package com.xquark.dal.mapper;

import com.xquark.dal.model.FreshManModuleVo;

import org.apache.ibatis.annotations.Param;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/13
 * Time:13:22
 * Des新人页面模块
 */
public interface FreshManModuleMapper {

    /**
     * 状态查询
     * @param status
     * @param type
     * @return
     */
    boolean selectExistByStatusAndType(@Param("status") int status, @Param("type") int type);
}