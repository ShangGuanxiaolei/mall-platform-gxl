package com.xquark.dal.mapper;

import com.xquark.dal.model.CustomerCareerLevel;

public interface CustomerCareerLevelMapper {

  int deleteByPrimaryKey(Long cpId);

  int insert(CustomerCareerLevel record);

  CustomerCareerLevel selectByPrimaryKey(Long cpId);

  int updateByPrimaryKeySelective(CustomerCareerLevel record);

  int updateByPrimaryKey(CustomerCareerLevel record);

}