package com.xquark.dal.mapper;

import com.xquark.dal.model.Antifake;
import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.vo.AntifakeVO;
import com.xquark.dal.vo.BonusVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface AntifakeMapper {

  /**
   * 新增物流码记录
   */
  int insertAntifake(Antifake antifake);


  List<AntifakeVO> list(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(@Param(value = "params") Map<String, Object> params);

  /**
   * 检查同一个物流码不能由同一个人发货多次
   */
  List<String> checkExist(@Param(value = "userId") String userId,
      @Param(value = "codes") List codes);

}
