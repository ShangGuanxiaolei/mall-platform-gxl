package com.xquark.dal.mapper;

import com.xquark.dal.model.Sku;
import com.xquark.dal.model.WmsInventory;
import com.xquark.dal.vo.SkuBasicVO;
import com.xquark.dal.vo.SkuCombineVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SkuMapper {

  int insert(Sku record);

  Sku selectByPrimaryKey(String id);

  SkuBasicVO selectBasicVOByPrimaryKey(String id);

  int countBySkuCode(@Param("skuCode") String skuCode);

  Sku select(String productId, String skuId);

  List<Sku> getAllSkuByproductId(String  productId);

  int updateForArchive(String id);

  int updateByPrimaryKeySelective(Sku record);

  int updateAmount(String id, Integer amount);

  void updateSkuCodeResources(@Param("newCode") String newCode, @Param("resourceCode") String resourceCode);

  List<Sku> selectByProductId(String id);

  Sku selectMinPriceSkuByProductId(String id);

  void deductAmountByOrderId(String orderId);

  void restoreAmountByOrderId(String orderId);

  /**
   * seker 修改优惠和优惠结束时的方法 名称 2015-05-07
   */
  //int updatePriceDiscountByShop(@Param("shopId") String shopId, @Param("discount") Float discount);
  int discountByShop(@Param("shopId") String shopId, @Param("discount") Float discount);


  //int updatePriceDiscount(@Param("productId") String productId, @Param("discount") Float discount);
  int discountByProduct(@Param("productId") String productId, @Param("discount") Float discount);

  //int updatePriceReduction(@Param("productId") String productId, @Param("reduction") Float reduction);
  int reductionByProduct(@Param("productId") String productId, @Param("reduction") Float reduction);

  //int updatePriceFromMarketPriceByShop(@Param("shopId") String shopId);
  int closeDiscountByShop(@Param("shopId") String shopId);

  //int updatePriceFromMarketPrice(@Param("productId") String productId);
  int closeDiscountByProduct(@Param("productId") String productId);

  List<Sku> selectByIds(@Param("ids") String... ids);

  /**
   * 后期删除
   */
  int delDuplicateSku(@Param("productId") String productId, @Param("spec") String spec);

  int updDuplicateSku(@Param("productId") String productId, @Param("spec") String spec);

  int delDuplicateSkuMapping(@Param("productId") String productId, @Param("specKey") String spec);

  int updDuplicateSkuMapping(@Param("productId") String productId, @Param("specKey") String spec);

  List<Sku> listNoCodeSkus();

  void addCode(String id);

  List<Sku> findBySkuCode4SkuResources(@Param("skuCode") String skuCode,
      @Param("skuResources") String skuResources);

  Long countbySkuActivity(@Param("skuId") String skuId, @Param("activityId") String activityId,
      @Param("productId") String productId);

  int updateSkuActivity(@Param("skuId") String skuId, @Param("activityId") String activityId,
      @Param("productId") String productId, @Param("price") String price,
      @Param("rate") String rate);

  int createSkuActivity(@Param("skuId") String skuId, @Param("activityId") String activityId,
      @Param("productId") String productId, @Param("price") String price,
      @Param("rate") String rate);

  /**
   * 找到某个商品对应的sku的所有属性值
   */
  List<String> findAttributeByProductId(@Param("productId") String productId);

  /**
   * 计算商品所有规格叠加的总库存
   * @param productId
   * @return
   */
  Integer countTotalAmount(@Param("productId") String productId);

  void updateByCode(WmsInventory wmsInventory);

  Sku selectBySkuCode(@Param("code") String code);

  List<Sku> listByResourceCode(@Param("resourceCode") String resourceCode);

  Sku selectByCode(@Param("code") String code);

  void updateSkuByCode(Sku sku);

  List<SkuCombineVO> listCombine(@Param("skuId") String skuId,
      @Param("categoryId") Long categoryId,
      @Param("supplierId") String supplierId,
      @Param("selectedIds") List<String> selectedIds,
      @Param("keyword") String keyword,
      @Param("page") Pageable pageable);

  List<SkuCombineVO> listUnCombine(@Param("skuId") String skuId,
      @Param("supplierId") String supplierId,
      @Param("categoryId") Long categoryId,
      @Param("selectedIds") List<String> selectedIds,
      @Param("keyword") String keyword, @Param("page") Pageable pageable);

  Long countCombine(@Param("skuId") String skuId, @Param("categoryId") Long categoryId,
      @Param("supplierId") String supplierId, @Param("selectedIds") List<String> selectedIds,
      @Param("keyword") String keyword);

  Long countUnCombine(@Param("skuId") String skuId, @Param("supplierId") String supplierId,
      @Param("categoryId") Long categoryId, @Param("selectedIds") List<String> selectedIds,
      @Param("keyword") String keyword);

  /**
   * 通过商品id和skuCode校验是否有库存，有返回true
   * @param productId
   * @return
   */
  boolean checkCommonProductStock(@Param("productId") String productId, @Param("skuCode") String skuCode);
  /**
   * 功能描述:
   * @author Luxiaoling
   * @date 2019/4/11 19:45
   * @param  skuCode
   * @return  java.lang.Integer
   */
  Integer countAmountBuySourceSkuCode(@Param("skuCode") String skuCode);

    Sku selectMinPriceSku4FreshMan(String productId);

  List<Sku> selectFreshManSku(@Param("productId")String productId);

	/**
	 * 修改预约预售库存
	 */
	int updateSkuAmount(@Param("skuId")String skuId,@Param("amount")int pAmount);


  List<Long> selectCopyProductId(@Param("productId")String productId);

  Integer  updateCopyProductSku(Sku record);
}