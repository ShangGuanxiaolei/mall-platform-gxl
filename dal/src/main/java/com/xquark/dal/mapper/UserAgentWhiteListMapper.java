package com.xquark.dal.mapper;

import com.xquark.dal.model.UserAgentWhiteList;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by wangxinhua on 17-5-16. DESC:
 */
public interface UserAgentWhiteListMapper {

  int insert(UserAgentWhiteList whiteList);

  int delete(@Param("id") String id);

  int deleteByPhone(@Param("phone") String phone);

  int update(UserAgentWhiteList whiteList);

  UserAgentWhiteList selectByPrimaryKey(@Param("id") String id);

  UserAgentWhiteList selectByPhone(@Param("phone") String phone);

  List<UserAgentWhiteList> selectAll(@Param("order") String order, @Param("page") Pageable pageable,
      @Param("direction") Sort.Direction direction, @Param("keyword") String keyword);

  int selectTotal();
}
