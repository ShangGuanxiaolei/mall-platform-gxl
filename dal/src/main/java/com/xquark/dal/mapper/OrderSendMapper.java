package com.xquark.dal.mapper;

import com.xquark.dal.model.OrderSend;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by IntelliJ IDEA.
 * User: a9175
 * Date: 2018/6/17.
 * Time: 15:12
 * ${DESCRIPTION}
 */
public interface OrderSendMapper {
    int deleteByPrimaryKey(Long id);

    /**
     * 插入菠萝派发来的发货信息
     * @param record
     * @return
     */
    int insert(OrderSend record);

    /**
     * 对order表中的物流及订单状态进行改变
     * @param orderNo
     * @param logisticsCompany
     * @param logisticsOrderNo
     */
    void updateLogisticsInfo(@Param("orderNo") String orderNo,
        @Param("logisticsCompany") String logisticsCompany,
        @Param("logisticsOrderNo") String logisticsOrderNo);

    int insertSelective(OrderSend record);

    OrderSend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderSend record);

    int updateByPrimaryKey(OrderSend record);
}
