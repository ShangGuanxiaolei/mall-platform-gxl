package com.xquark.dal.mapper;

import com.xquark.dal.model.AddedTaxBill;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

/**
 * User: huangjie
 * Date: 2018/5/21.
 * Time: 下午7:58
 * 增值税开票请求数据访问mapper
 */
public interface AddedTaxBillMapper {

    /**
     * 添加
     */
    int insert(AddedTaxBill obj);

    /**
     * 批量插入
     *
     * @param collection
     * @return
     */
    int batchInsert(@Param("collection") List<AddedTaxBill> collection);

    /**
     * 通过id查找
     */
    AddedTaxBill getByPrimaryKey(@Param("id") String id);

    /**
     * 更新
     */
    int update(AddedTaxBill obj);


    /**
     * 通过id删除
     */
    int delete(@Param("id") String id);

    /**
     * 批量删除
     */
    int batchDelete(@Param("collection") List<String> collection);

    /**
     * 计算数据的数量
     */
    Long count(@Param("params") Map<String, Object> params);

    /**
     * 分页查询
     *
     * @param page
     * @return
     */
    List<AddedTaxBill> list(@Param("page") Pageable page, @Param("params") Map<String, Object> params);

}