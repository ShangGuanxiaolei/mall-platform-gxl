package com.xquark.dal.mapper;

import org.apache.ibatis.annotations.Param;

import com.xquark.dal.model.PaymentConfig;

public interface PaymentConfigMapper {

  int deleteByPrimaryKey(@Param("id") String id);

  int insert(PaymentConfig record);

  PaymentConfig selectByPrimaryKey(@Param("id") String id);

  int updateByPrimaryKeySelective(PaymentConfig record);

}