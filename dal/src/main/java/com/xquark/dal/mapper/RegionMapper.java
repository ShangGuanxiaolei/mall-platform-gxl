package com.xquark.dal.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @auther liuwei
 * @date 2018/6/7 0:53
 */
public interface RegionMapper {

  /**
   * 批量插入区域信息
   */
  int insertRegion(@Param("regionDictList") List<RegionDict> regionDictList);

  /**
   * 查询指定region的所有父级及自已
   */
  List<RegionDict> queryAllParentAndSelf(int regionId);

  /**
   * 根据id查询对应的顺丰信息
   */
  Long selectsfid(@Param("zoneid") Long zoneid);
}
