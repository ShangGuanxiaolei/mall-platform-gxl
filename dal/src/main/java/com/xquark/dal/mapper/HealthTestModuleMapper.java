package com.xquark.dal.mapper;

import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestModuleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HealthTestModuleMapper {

  long countByExample(HealthTestModuleExample example);

  int deleteByExample(HealthTestModuleExample example);

  int deleteByPrimaryKey(String id);

  int insert(HealthTestModule record);

  int insertSelective(HealthTestModule record);

  List<HealthTestModule> selectByExample(HealthTestModuleExample example);

  HealthTestModule selectByPrimaryKey(String id);

  int updateByExampleSelective(@Param("record") HealthTestModule record,
      @Param("example") HealthTestModuleExample example);

  int updateByExample(@Param("record") HealthTestModule record,
      @Param("example") HealthTestModuleExample example);

  int updateByPrimaryKeySelective(HealthTestModule record);

  int updateByPrimaryKey(HealthTestModule record);


  // --- 分割线 ---
  HealthTestModule selectChild(@Param("parentId") String parentId, @Param("name") String name);

  Integer loadSubCounts(@Param("parentId") String parentId);

  int deleteModules(@Param("ids") String... ids);

  HealthTestModule selectParent(@Param("id") String id);
}