package com.xquark.dal.mapper;

import com.xquark.dal.model.PartnerProductCommissionDe;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PartnerProductCommissionDeMapper {

  int deleteByPrimaryKey(String id);

  int insert(PartnerProductCommissionDe record);

  int insertSelective(PartnerProductCommissionDe record);

  PartnerProductCommissionDe selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(PartnerProductCommissionDe record);

  int updateByPrimaryKey(PartnerProductCommissionDe record);

  int updateForArchive(String id);

  /**
   * 找到某个商品所有佣金明细信息
   */
  List<PartnerProductCommissionDe> getByParentId(@Param(value = "parentId") String parentId);

  /**
   * 找到某个商品所有佣金明细信息
   */
  List<PartnerProductCommissionDe> getByProductId(@Param(value = "productId") String productId);

}