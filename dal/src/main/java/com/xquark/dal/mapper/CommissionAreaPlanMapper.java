package com.xquark.dal.mapper;

import com.xquark.dal.model.CommissionAreaPlan;

public interface CommissionAreaPlanMapper {

  int deleteByPrimaryKey(String id);

  int undeleteByPrimaryKey(String id);

  int insert(CommissionAreaPlan record);

  int insertSelective(CommissionAreaPlan record);

  CommissionAreaPlan selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(CommissionAreaPlan record);

  int updateByPrimaryKey(CommissionAreaPlan record);
}