package com.xquark.dal.mapper;

import com.xquark.dal.model.GrandSaleTimeline;

import java.util.List;

public interface GrandSaleTimelineMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleTimeline record);

    int insertSelective(GrandSaleTimeline record);

    GrandSaleTimeline selectByPrimaryKey(Integer id);

    List<GrandSaleTimeline> selectByGrandSaleId(Integer grandSaleId);

    int updateByPrimaryKeySelective(GrandSaleTimeline record);

    int updateByPrimaryKey(GrandSaleTimeline record);
}