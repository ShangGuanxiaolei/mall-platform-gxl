package com.xquark.dal.mapper;

import com.xquark.dal.model.PromotionApply;
import com.xquark.dal.type.FlashSaleApplyStatus;
import com.xquark.dal.type.PreOrderApplyStatus;
import com.xquark.dal.type.PromotionType;
import org.apache.ibatis.annotations.Param;

public interface PromotionApplyMapper {

  int deleteByPrimaryKey(String id);

  int insert(PromotionApply record);

  int insertSelective(PromotionApply record);

  PromotionApply selectByPrimaryKey(String id);

  PromotionApply selectByPromotionProductIdAndUserId(@Param("pPid") String promotionProductId,
      @Param("userId") String userId, @Param("type") PromotionType type);

  int updateByPrimaryKeySelective(PromotionApply record);

  int updateByPrimaryKey(PromotionApply record);

  boolean isApplied(@Param("userId") String userId, @Param("pPid") String promotionProductId,
      @Param("type") PromotionType type);

  int updateFlashSaleStatus(@Param("id") String id, @Param("status") FlashSaleApplyStatus status);

  int updateFlashSaleStatusByOrderId(@Param("orderId") String orderId,
      @Param("status") FlashSaleApplyStatus status);

  int updatePreOrderStatus(@Param("id") String id, @Param("status") PreOrderApplyStatus status);

  int updatePreOrderStatusByOrderId(@Param("orderId") String orderId,
      @Param("status") PreOrderApplyStatus status);

  /**
   * 查询用户对应该预购活动是否可以支付尾款
   *
   * @param promotionProductId 活动商品id
   * @param userId 用户id
   * @return trur or false
   */
  boolean canPayRestPreOrder(@Param("pPid") String promotionProductId,
      @Param("userId") String userId);

  /**
   * 查询预购支付定金的订单id
   *
   * @param promotionId 活动id
   * @param orderId 当前订单id
   * @return 订单id
   */
  Long selectFirstPayOrderId(@Param("promotionId") String promotionId,
      @Param("orderId") String orderId);
}