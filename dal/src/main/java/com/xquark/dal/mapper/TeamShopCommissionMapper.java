package com.xquark.dal.mapper;

import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.vo.TeamShopCommissionVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TeamShopCommissionMapper {

  int deleteByPrimaryKey(String id);

  int insert(TeamShopCommission record);

  int insertSelective(TeamShopCommission record);

  TeamShopCommissionVO selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(TeamShopCommission record);

  int updateByPrimaryKey(TeamShopCommission record);

  int updateForArchive(String id);

  TeamShopCommission selectDefaultByshopId(String shopId);

  List<TeamShopCommission> selectNonDefaultByshopId(String shopId);

  /**
   * 删除战队默认设置
   */
  int deleteByDefaultByshopId(@Param(value = "shopId") String shopId);

  List<TeamShopCommission> selectAllByshopId(String shopId);


  List<TeamShopCommissionVO> ListNonDefaultByshopId(@Param(value = "pager") Pageable pager,
      @Param(value = "params") Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCntNonDefault(@Param(value = "params") Map<String, Object> params);

}