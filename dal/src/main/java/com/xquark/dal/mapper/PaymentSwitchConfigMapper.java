package com.xquark.dal.mapper;

import com.xquark.dal.model.PaymentSwitchConfig;
import org.apache.ibatis.annotations.Param;

public interface PaymentSwitchConfigMapper {

  PaymentSwitchConfig selectByPrimaryKey(@Param("id") String id);

  PaymentSwitchConfig selectByCodeAndProfile(@Param("code") String code, @Param("profile") String profile);

}