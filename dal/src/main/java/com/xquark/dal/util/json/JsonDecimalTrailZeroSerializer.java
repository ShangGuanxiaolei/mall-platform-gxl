package com.xquark.dal.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 18-4-4. DESC:
 */
public class JsonDecimalTrailZeroSerializer extends JsonSerializer<BigDecimal> {

  @Override
  public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException {
    if (value == null || value.signum() == 0) {
      jgen.writeString("0");
      return;
    }
    BigDecimal formatted = value.setScale(2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
    jgen.writeString(formatted.toPlainString());
  }

}
