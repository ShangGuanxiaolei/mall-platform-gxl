package com.xquark.dal.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.base.Optional;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/26
 * 金额格式化
 */
public class JsonPricingViewSerialize extends JsonSerializer<BigDecimal> {

  @Override
  public void serialize(BigDecimal bigDecimal, JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider) throws IOException {
    bigDecimal = Optional.fromNullable(bigDecimal).or(BigDecimal.ZERO);
    String prefix = (bigDecimal.signum() < 0 ? "- " : "") + "¥";
    jsonGenerator.writeString(prefix + bigDecimal.abs());
  }

}
