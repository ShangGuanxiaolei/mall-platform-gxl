package com.xquark.dal.util;

import io.vavr.Function1;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContextUtil implements ApplicationContextAware {

  private static ApplicationContext context;// 声明一个静态变量保存

  private static Function1<? super Class, Object> lazyGetBean;

  static {
    lazyGetBean = Function1.of((Class c) -> SpringContextUtil.getBean(c, context))
            .memoized();
  }

  @Override
  public void setApplicationContext(ApplicationContext contex) throws BeansException {
    context = contex;
  }

  public static ApplicationContext getContext() {
    return context;
  }

  /**
   * 直接获取bean
   */
  public static Object getBean(String beanName) {
    return context.getBean(beanName);
  }

  private static <T> T getBean(Class<T> requireType, ApplicationContext ct) {
    return ct.getBean(requireType);
  }

  @SuppressWarnings("unchecked")
  public static <T> T getBean(Class<T> requiredType) {
      return (T) lazyGetBean.apply(requiredType);
  }

  public static <T> T getBean(String beanName, Class<T> requiredType) {
    return context.getBean(beanName, requiredType);
  }
}

