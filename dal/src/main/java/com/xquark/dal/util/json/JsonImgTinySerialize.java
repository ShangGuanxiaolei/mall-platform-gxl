package com.xquark.dal.util.json;

/**
 * Created by wangxinhua on 18-3-29. DESC:
 */
public class JsonImgTinySerialize extends AbstractJsonImgQuailtySerialize {

  @Override
  protected String getExtFormat() {
    return "?imageMogr2/auto-orient/thumbnail/320x/quality/50";
  }

}
