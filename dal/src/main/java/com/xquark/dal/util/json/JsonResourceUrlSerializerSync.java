package com.xquark.dal.util.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.utils.ResourceResolver;

public class JsonResourceUrlSerializerSync extends JsonSerializer<String> {

  //	private final static ThreadLocal<Map<String, String>> imageSize = new ThreadLocal<Map<String, String>>();
  @Override
  public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String url = resourceFacade.resolveUrl2Orig(value);
    jgen.writeString(url);
  }
}
