package com.xquark.dal.vo;

import com.xquark.dal.model.HealthTestModule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-4. DESC:
 */
public class HealthTestModuleVO {

  private final HealthTestModule healthTestModule;
  private List<HealthTestModuleVO> children;
  private State state = new State();

  private static class State {

    private boolean opened;
    private boolean disabled;
    private boolean selected;

    public boolean isOpened() {
      return opened;
    }

    public void setOpened(boolean opened) {
      this.opened = opened;
    }

    public boolean isDisabled() {
      return disabled;
    }

    public void setDisabled(boolean disabled) {
      this.disabled = disabled;
    }

    public boolean isSelected() {
      return selected;
    }

    public void setSelected(boolean selected) {
      this.selected = selected;
    }
  }

  public HealthTestModuleVO(HealthTestModule healthTestModule, List<HealthTestModuleVO> children) {
    this.healthTestModule = healthTestModule;
    if (children == null) {
      this.children = new ArrayList<HealthTestModuleVO>();
    } else {
      this.children = children;
    }
  }

  public List<HealthTestModuleVO> getChildren() {
    return children;
  }

  public String getId() {
    return healthTestModule.getId();
  }

  public String getParentId() {
    return healthTestModule.getParentId();
  }

  public String getName() {
    return healthTestModule.getName();
  }

  public String getIcon() {
    return healthTestModule.getIcon();
  }

  public Boolean getIsLeaf() {
    return healthTestModule.getIsLeaf();
  }

  public String getDescription() {
    return healthTestModule.getDescription();
  }

  public Date getCreatedAt() {
    return healthTestModule.getCreatedAt();
  }

  public Date getUpdatedAt() {
    return healthTestModule.getUpdatedAt();
  }

  public Boolean getArchive() {
    return healthTestModule.getArchive();
  }

  public String getText() {
    return this.getName();
  }

  public void setOpened(boolean opened) {
    this.state.setOpened(opened);
  }

  public void setDisabled(boolean disabled) {
    this.state.setDisabled(disabled);
  }

  public void setSelected(boolean selected) {
    this.state.setSelected(selected);
  }

  public boolean getRequired() {
    return healthTestModule.getRequired();
  }

  public void setRequired(boolean required) {
    healthTestModule.setRequired(required);
  }

  public State getState() {
    return state;
  }

  public String getStaticName() {
    return healthTestModule.getStaticName();
  }

  public void setStaticName(String staticName) {
    this.healthTestModule.setStaticName(staticName);
  }
}
