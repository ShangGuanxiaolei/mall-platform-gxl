package com.xquark.dal.vo;

import com.xquark.dal.model.SignIn;

/**
 * Created by wangxinhua on 17-9-1. DESC:
 */
public class SignInVO {

  private SignIn signIn;

  private Long modifiedAmount;

  private Long userAmount;

  public SignInVO(SignIn signIn, Long modifiedAmount, Long userAmount) {
    this.signIn = signIn;
    this.modifiedAmount = modifiedAmount;
    this.userAmount = userAmount;
  }

  public Long getModifiedAmount() {
    return modifiedAmount;
  }

  public Long getUserAmount() {
    return userAmount;
  }

  public String getId() {
    return this.signIn.getId();
  }

  public String getUserId() {
    return this.signIn.getUserId();
  }

  public Long getSignCount() {
    return this.signIn.getSignCount();
  }

  public Long getSignSum() {
    return this.signIn.getSignSum();
  }

}
