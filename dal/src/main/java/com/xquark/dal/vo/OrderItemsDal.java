package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.util.List;

/**
 * User: kong Date: 18-7-24. Time: 上午11:13 顺丰订单明细
 */
public class OrderItemsDal {

  //产品信息
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  //物流单号
  private String shippingSn;

  //订单状态字符串
  private String shippingStatusStr;

  //订单编号
  private String orderSn;

  //物流公司名称
  private String shippingTypeName;

//  public List<OrderProductDal> getOrderProducts() {
//    return OrderProducts;
//  }
//
//  public void setSfOrderProducts(List<OrderProductDal> OrderProducts) {
//    this.OrderProducts = OrderProducts;
//  }

  public String getShippingSn() {
    return shippingSn;
  }

  public void setShippingSn(String shippingSn) {
    this.shippingSn = shippingSn;
  }

  public String getShippingStatusStr() {
    return shippingStatusStr;
  }

  public void setShippingStatusStr(String shippingStatusStr) {
    this.shippingStatusStr = shippingStatusStr;
  }

  public String getOrderSn() {
    return orderSn;
  }

  public void setOrderSn(String orderSn) {
    this.orderSn = orderSn;
  }

  public String getShippingTypeName() {
    return shippingTypeName;
  }

  public void setShippingTypeName(String shippingTypeName) {
    this.shippingTypeName = shippingTypeName;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }
}
