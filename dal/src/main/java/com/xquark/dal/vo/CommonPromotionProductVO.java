package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFlashSale;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
public class CommonPromotionProductVO {

  private static final long serialVersionUID = 4788160701515287419L;

  private Long amount;

  private String skuCode;

  private String pCode;

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }
}
