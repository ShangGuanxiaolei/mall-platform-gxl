package com.xquark.dal.vo;

import com.xquark.dal.model.Supplier;
import com.xquark.dal.model.SupplierWareHouse;
import java.util.List;

/**
 * User: huangjie Date: 2018/6/23. Time: 下午12:04 供应商的视图对象
 */
public class SupplierVO extends Supplier {

  /**
   * 供应商下面管理的仓库
   */
  private List<SupplierWareHouse> wareHouseList;

  public List<SupplierWareHouse> getWareHouseList() {
    return wareHouseList;
  }

  public void setWareHouseList(List<SupplierWareHouse> wareHouseList) {
    this.wareHouseList = wareHouseList;
  }
}
