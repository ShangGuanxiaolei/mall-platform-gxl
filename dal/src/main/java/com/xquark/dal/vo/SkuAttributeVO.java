package com.xquark.dal.vo;

import com.xquark.dal.model.SkuAttribute;
import com.xquark.dal.model.SkuAttributeItem;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by root on 17-11-23.
 */
public class SkuAttributeVO extends SkuAttribute {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private List<SkuAttributeItem> items;

  public List<SkuAttributeItem> getItems() {
    return items;
  }

  public void setItems(List<SkuAttributeItem> items) {
    this.items = items;
  }
}
