package com.xquark.dal.vo;

import com.xquark.dal.model.ViewPageComponent;

/**
 * Created by chh on 16-10-18.
 */
public class ViewPageComponentVO extends ViewPageComponent {

  private String vcName;

  public String getVcName() {
    return vcName;
  }

  public void setVcName(String vcName) {
    this.vcName = vcName;
  }
}
