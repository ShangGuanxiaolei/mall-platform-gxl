package com.xquark.dal.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * @类名: SaleProduactVO
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/28 10:03
 * @版本号: V1.0 .
 */
public class SaleProduactVO {

    private String name;

    private String productId;

    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;

    private Long limitAmount;
    //原价
    private BigDecimal originalPrice;
    //单价
    private BigDecimal price;
    //现金兑换价
    private BigDecimal cashExchange;
    //德分
    private Long deductionPoint;
    //立减
    private BigDecimal point;
    //推广费
    private BigDecimal promoAmt;
    //服务费
    private BigDecimal serverAmt;
    //净值
    private BigDecimal netWorth;
    //会员价
    private BigDecimal memberPrice;
    //代理价
    private BigDecimal proxyPrice;

    //自营标签
    private int selfOperated;

    //产品库存
    private Long amount;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getProxyPrice() {
        return proxyPrice;
    }

    public void setProxyPrice(BigDecimal proxyPrice) {
        this.proxyPrice = proxyPrice;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCashExchange() {
        return cashExchange;
    }

    public void setCashExchange(BigDecimal cashExchange) {
        this.cashExchange = cashExchange;
    }

    public Long getDeductionPoint() {
        return deductionPoint;
    }

    public void setDeductionPoint(Long deductionPoint) {
        this.deductionPoint = deductionPoint;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public Long getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(Long limitAmount) {
        this.limitAmount = limitAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
