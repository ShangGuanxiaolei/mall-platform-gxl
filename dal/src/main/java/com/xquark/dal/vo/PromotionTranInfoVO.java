package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PieceTimeChecker;
import com.xquark.dal.status.ExecStatus;
import com.xquark.dal.status.PieceStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang @ClassName PromotionTranInfoVO
 * @date 2018/9/13 0013
 */
public class PromotionTranInfoVO implements PieceTimeChecker {
  private int id;
  //是否是新人团
  private Boolean isNewPg;
  private String pieceGroupTranCode;
  private int pieceGroupNum;
  private String pieceStatus;
  private String pDetailCode;
  private boolean isCutLine;
  private String groupOpenMemberId;
  private Date joinTime;
  private long pieceEffectTime;
  private Date groupOpenTime;
  private Date groupFinishTime;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;
  private String name;
  private String code;
  private String pCode;
  private Integer jumpQueueTime;
  private int page = 0; // 当前页
  private int pageCount; // 总条数
  private int pageSize = 10; // 每页显示记录数
  private int startNo;
  private int totalPage;//总页数
  private List<PromotionTranInfoVO> result;//要在页面上显示的信息
  private int selfOperated;//自营标签
  private boolean isShowInvitation;

  public int getSelfOperated() {
    return selfOperated;
  }
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }
  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }
  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public List<PromotionTranInfoVO> getResult() {
    return result;
  }

  public void setResult(List<PromotionTranInfoVO> result) {
    this.result = result;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getPageCount() {
    return pageCount;
  }

  public void setPageCount(int pageCount) {
    this.pageCount = pageCount;
  }

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getStartNo() {
    return startNo;
  }

  public void setStartNo(int startNo) {
    this.startNo = startNo;
  }

  public int getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  private BigDecimal marketPrice;
  private double skuDiscount;
  private Double disCountPrice;

  public Double getDisCountPrice() {
    return disCountPrice;
  }

  public void setDisCountPrice(Double disCountPrice) {
    this.disCountPrice = disCountPrice;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public double getSkuDiscount() {
    return skuDiscount;
  }

  public void setSkuDiscount(double skuDiscount) {
    this.skuDiscount = skuDiscount;
  }

  private List<PromotionMemberImgVO> memberImgList;

  public List<PromotionMemberImgVO> getMemberImgList() {
    return memberImgList;
  }

  public void setMemberImgList(List<PromotionMemberImgVO> memberImgList) {
    this.memberImgList = memberImgList;
  }

  public String getPieceGroupTranCode() {
    return pieceGroupTranCode;
  }

  public void setPieceGroupTranCode(String pieceGroupTranCode) {
    this.pieceGroupTranCode = pieceGroupTranCode;
  }

  public int getPieceGroupNum() {
    return pieceGroupNum;
  }

  public void setPieceGroupNum(int pieceGroupNum) {
    this.pieceGroupNum = pieceGroupNum;
  }

  public String getPieceStatus() {
    return pieceStatus;
  }

  public void setPieceStatus(String pieceStatus) {
    this.pieceStatus = pieceStatus;
  }

  public Date getJoinTime() {
    return joinTime;
  }

  public void setJoinTime(Date joinTime) {
    this.joinTime = joinTime;
  }

  public Long getPieceEffectTime() {
    return pieceEffectTime;
  }

  public void setPieceEffectTime(Long pieceEffectTime) {
    this.pieceEffectTime = pieceEffectTime;
  }

  public Date getGroupOpenTime() {
    return groupOpenTime;
  }

  public void setGroupOpenTime(Date groupOpenTime) {
    this.groupOpenTime = groupOpenTime;
  }

  public Boolean getIsNewPg() {
    return isNewPg;
  }

  public void setNewPg(Boolean newPg) {
    isNewPg = newPg;
  }

  public boolean getIsShowInvitation() {
    // 开团且未成团状态才可以邀请
    // 前提条件是代码层面控制好了拼图状态
    return isShowInvitation;//StringUtils.equals(this.pieceStatus, String.valueOf(PieceStatus.OPENED.getCode()));
  }

  public void setIsShowInvitation(boolean isShowInvitation) {
    this.isShowInvitation = isShowInvitation;
  }

  /** 获取成功、失败状态 */
  public ExecStatus getGroupStatus() {
    if (StringUtils.isBlank(pieceStatus)) {
      return ExecStatus.FAILED;
    }
    return PieceStatus.fromCode(pieceStatus).getRet();
  }

  public boolean getIsCutLine() {
    return isCutLine;
  }

  public void setIsCutLine(boolean cutLine) {
    isCutLine = cutLine;
  }

  public Date getGroupFinishTime() {
    return groupFinishTime;
  }

  public void setGroupFinishTime(Date groupFinishTime) {
    this.groupFinishTime = groupFinishTime;
  }

  public String getGroupOpenMemberId() {
    return groupOpenMemberId;
  }

  public void setGroupOpenMemberId(String groupOpenMemberId) {
    this.groupOpenMemberId = groupOpenMemberId;
  }

  @Override
  public Integer getJumpQueueTime() {
    return jumpQueueTime;
  }

  public void setJumpQueueTime(Integer jumpQueueTime) {
    this.jumpQueueTime = jumpQueueTime;
  }
}
