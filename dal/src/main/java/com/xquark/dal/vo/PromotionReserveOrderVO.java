package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionReserveOrder;

import java.util.Date;

/**
 * 预售活动订单相关
 *
 * @author: yyc
 * @date: 19-4-19 下午5:31
 */
public class PromotionReserveOrderVO extends PromotionReserveOrder {

  /** 预约开始 */
  private Date validFrom;

  /** 预约结束 */
  private Date validTo;

  /** 支付开始 */
  private Date payFrom;

  /** 支付结束 */
  private Date payTo;

  /** 活动状态 */
  private Integer status;

  /** skuId */
  private String skuId;

  /** 活动id */
  private String promotionId;

  /** 商品数量 */
  private Integer amount;

  public boolean getCanPay() {
    Date now = new Date();
    return now.after(payFrom) && now.before(payTo) && status < 3;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Date getPayFrom() {
    return payFrom;
  }

  public void setPayFrom(Date payFrom) {
    this.payFrom = payFrom;
  }

  public Date getPayTo() {
    return payTo;
  }

  public void setPayTo(Date payTo) {
    this.payTo = payTo;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
}
