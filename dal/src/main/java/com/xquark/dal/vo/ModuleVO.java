package com.xquark.dal.vo;

import com.xquark.dal.model.Module;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者: wangxh 创建日期: 17-3-28 简介:
 */
public class ModuleVO implements Serializable {

  public ModuleVO(Module module) {
    this.module = module;
  }

  private final Module module;
  private List<ModuleVO> children = new ArrayList<ModuleVO>();

  public String getId() {
    return this.module.getId();
  }

  public String getParentId() {
    return this.module.getParentId();
  }

  public String getName() {
    return this.module.getName();
  }

  public String getUrl() {
    return this.module.getUrl();
  }

  public String getIconName() {
    return this.module.getIconName();
  }

  public Boolean getIs_Leaf() {
    return this.module.getIs_Leaf();
  }

  public Boolean getIs_AutoExpand() {
    return this.module.getIs_AutoExpand();
  }

  public Integer getSortNo() {
    return this.module.getSortNo();
  }

  public Date getCreatedAt() {
    return this.module.getCreatedAt();
  }

  public Date getUpdatedAt() {
    return this.module.getUpdatedAt();
  }

  public String getModule() {
    return this.module.getModule();
  }

  public String getPage() {
    return this.module.getPage();
  }

  public Boolean getArchive() {
    return this.module.getArchive();
  }

  public List<ModuleVO> getChildren() {
    return children;
  }

  public void setChildren(List<ModuleVO> children) {
    this.children = children;
  }
}
