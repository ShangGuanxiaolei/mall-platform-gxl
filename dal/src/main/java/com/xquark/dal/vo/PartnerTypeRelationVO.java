package com.xquark.dal.vo;

import com.xquark.dal.model.PartnerTypeRelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 17/07/18.
 */
public class PartnerTypeRelationVO extends PartnerTypeRelation {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal rate;

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }
}
