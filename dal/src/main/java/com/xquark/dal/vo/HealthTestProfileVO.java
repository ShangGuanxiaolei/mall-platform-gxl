package com.xquark.dal.vo;

import com.xquark.dal.model.HealthTestProfile;
import com.xquark.dal.type.BMIMessage;

/**
 * Created by wangxinhua on 17-7-14. DESC:
 */
public class HealthTestProfileVO {

  private HealthTestProfile profile;

  private String status;
  private String message;

  public HealthTestProfileVO(HealthTestProfile profile) {
    this.profile = profile;
    Double bmi = profile.getBmi();
    if (bmi == null) {
      status = "";
      message = "";
    } else {
      BMIMessage message = BMIMessage.valueOf(bmi);
      this.status = message.getStatus();
      this.message = message.getMessage();
    }
  }

  public String getGender() {
    return profile.getGender();
  }

  public Integer getHeight() {
    return profile.getHeight();
  }

  public Integer getWeight() {
    return profile.getWeight();
  }

  public Boolean getPregnant() {
    return profile.getPregnant();
  }

  public Double getBmi() {
    return profile.getBmi();
  }

  public String getMedicine() {
    return profile.getMedicine();
  }

  public String getStatus() {
    return this.status;
  }

  public String getMessage() {
    return this.message;
  }
}
