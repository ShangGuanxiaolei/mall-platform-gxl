package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PartnerProductCommission;
import com.xquark.dal.model.PartnerProductCommissionDe;
import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.status.CommissionProductType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 17/07/03.
 */
public class PartnerProductCommissionVO extends PartnerProductCommission {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  // 平台佣金
  PartnerProductCommissionDe platformDe;

  // 团队佣金
  PartnerProductCommissionDe teamDe;

  // 股东佣金
  PartnerProductCommissionDe shareholderDe;

  public PartnerProductCommissionDe getPlatformDe() {
    return platformDe;
  }

  public void setPlatformDe(PartnerProductCommissionDe platformDe) {
    this.platformDe = platformDe;
  }

  public PartnerProductCommissionDe getTeamDe() {
    return teamDe;
  }

  public void setTeamDe(PartnerProductCommissionDe teamDe) {
    this.teamDe = teamDe;
  }

  public PartnerProductCommissionDe getShareholderDe() {
    return shareholderDe;
  }

  public void setShareholderDe(PartnerProductCommissionDe shareholderDe) {
    this.shareholderDe = shareholderDe;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }
}
