package com.xquark.dal.vo;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-9-4. DESC: 订单 - 用户统计vo
 */
public class UserOrderDataVO {

  /**
   * 用户id
   */
  private String id;

  /**
   * 用户名称
   */
  private String name;

  /**
   * 积分数量
   */
  private Long yundou;

  /**
   * 交易笔数
   */
  private Long orderCount;

  /**
   * 消费金额
   */
  private BigDecimal totalFee;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getYundou() {
    return yundou;
  }

  public void setYundou(Long yundou) {
    this.yundou = yundou;
  }

  public Long getOrderCount() {
    return orderCount;
  }

  public void setOrderCount(Long orderCount) {
    this.orderCount = orderCount;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }
}
