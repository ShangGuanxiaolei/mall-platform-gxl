package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.CommissionProductRuleConfig;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.type.CommissionPolicy;
import com.xquark.dal.type.CommissionRuleName;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

/**
 * Created by chh on 16-12-21.
 */
public class CommissionProductRuleConfigVO extends CommissionProductRuleConfig {

  private CommissionPolicy policyName; // 规则集名称
  private CommissionRuleName ruleName; // 规则名称
  private String productName;
  private String productPrice;
  private String productImg;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImgUrl;

  public CommissionPolicy getPolicyName() {
    return policyName;
  }

  public void setPolicyName(CommissionPolicy policyName) {
    this.policyName = policyName;
  }

  public CommissionRuleName getRuleName() {
    return ruleName;
  }

  public void setRuleName(CommissionRuleName ruleName) {
    this.ruleName = ruleName;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(String productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getProductImgUrl() {
    return productImgUrl;
  }

  public void setProductImgUrl(String productImgUrl) {
    this.productImgUrl = productImgUrl;
  }
}
