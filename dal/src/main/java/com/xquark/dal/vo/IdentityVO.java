package com.xquark.dal.vo;

/**
 * @author wangxinhua.
 * @date 2018/11/11
 */
public class IdentityVO {

  public IdentityVO(boolean isProxy, boolean isMember) {
    this.isProxy = isProxy;
    this.isMember = isMember;
  }

  private boolean isProxy;

  private boolean isMember;

  public boolean getIsProxy() {
    return isProxy;
  }

  public void setIsProxy(boolean proxy) {
    isProxy = proxy;
  }

  public boolean getIsMember() {
    return isMember;
  }

  public void setIsMember(boolean member) {
    isMember = member;
  }
}
