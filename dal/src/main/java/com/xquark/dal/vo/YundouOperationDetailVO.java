package com.xquark.dal.vo;

import com.xquark.dal.model.YundouOperationDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/08.
 */
public class YundouOperationDetailVO extends YundouOperationDetail {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String userName;

  private String userPhone;

  private String content;

  private String ruleName;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }
}
