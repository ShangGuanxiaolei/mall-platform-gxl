package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import com.xquark.utils.EcmohoErpUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

/**
 * Created by chh on 16/12/06.
 */
public class UserAgentVO extends UserAgent {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private boolean isLeaf;

  private BigDecimal commissionFee;

  // 用户类型名称
  private String agentTypeName;

  // 授权编码
  private String certNum;

  // 授权开始时间
  private String startDate;

  // 授权结束时间
  private String endDate;

  private String addressStreet;

  // 显示*号的身份证号
  private String hiddenIdcard;

  // 已结算佣金
  private BigDecimal withdrawFee;

  // 未结算佣金
  private BigDecimal unWithdrawFee;

  private String parentName;

  // 是否由上级或者平台审核
  private AuditType auditType;

  // 省市区中文名称
  private String provinceName;

  private String cityName;

  private String districtName;

  // 详细地址
  private String street;

  private String areaName;

  // 授权编码
  private String certNumView;

  public String getCertNumView() {
    return certNumView;
  }

  public void setCertNumView(String certNumView) {
    this.certNumView = certNumView;
  }

  // 联合创始人封地名称
  public String getAreaName() {
    return areaName;
  }

  public void setAreaName(String areaName) {
    this.areaName = areaName;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public void setProvinceName(String provinceName) {
    this.provinceName = provinceName;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getDistrictName() {
    return districtName;
  }

  public void setDistrictName(String districtName) {
    this.districtName = districtName;
  }

  public AuditType getAuditType() {
    return auditType;
  }

  public void setAuditType(AuditType auditType) {
    this.auditType = auditType;
  }

  public String getParentName() {
    return parentName;
  }

  public void setParentName(String parentName) {
    this.parentName = parentName;
  }

  public BigDecimal getWithdrawFee() {
    return withdrawFee;
  }

  public void setWithdrawFee(BigDecimal withdrawFee) {
    this.withdrawFee = withdrawFee;
  }

  public BigDecimal getUnWithdrawFee() {
    return unWithdrawFee;
  }

  public void setUnWithdrawFee(BigDecimal unWithdrawFee) {
    this.unWithdrawFee = unWithdrawFee;
  }

  public String getHiddenIdcard() {
    return hiddenIdcard;
  }

  public void setHiddenIdcard(String hiddenIdcard) {
    this.hiddenIdcard = hiddenIdcard;
  }

  public String getAddressStreet() {
    return addressStreet;
  }

  public void setAddressStreet(String addressStreet) {
    this.addressStreet = addressStreet;
  }

  public String getAgentTypeName() {
    return agentTypeName;
  }

  public void setAgentTypeName(String agentTypeName) {
    this.agentTypeName = agentTypeName;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getCertNum() {
    if (certNum != null) {
      // 授权编码逻辑为六位字母加上六位数字
      // 六位字母为代理创建时分秒的大写字母，六位数字为代理id后三位加代理id进行md5加密成纯数字的前三位
      StringBuffer certNumStr = new StringBuffer();
      SimpleDateFormat sf = new SimpleDateFormat("HHmmss");
      if (this.getCreatedAt() != null) {
        String createdStr = sf.format(this.getCreatedAt());
        for (int i = 0; i < createdStr.length(); i++) {
          char item = createdStr.charAt(i);
          char c1 = (char) (item + 64);
          certNumStr.append(c1);
        }
      }
      if (certNum.length() >= 3) {
        certNumStr.append(certNum.substring(certNum.length() - 3, certNum.length()));
      } else {
        certNumStr.append(certNum);
      }

      try {
        // 生成一个MD5加密计算摘要根据代理id进行md5加密成纯数字
        String signAfter = getMD5(certNum.getBytes());
        certNumStr.append(signAfter.substring(0, 3));
      } catch (Exception e) {
        log.error("生成授权编码出错：", e);
      }
      return certNumStr.toString().toUpperCase();
    } else {
      return "";
    }
  }

  public static String getMD5(byte[] source) {
    String s = null;
    char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(source);
      byte tmp[] = md.digest();
      char str[] = new char[16];
      int k = 0;
      for (int i = 0; i < 16; i++) {
        byte byte0 = tmp[i];
        //只取高位
        str[k++] = hexDigits[(byte0 >>> 4 & 0xf) % 10];
        // str[k++] = hexDigits[byte0 & 0xf];
      }
      s = new String(str);  // 换后的结果转换为字符串
    } catch (Exception e) {
      e.printStackTrace();
    }
    return s;
  }

  public static void main(String args[]) {
    String certNum = "100015";
    try {
      // 生成一个MD5加密计算摘要
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      byte[] byteSigh = md5.digest(certNum.getBytes());
      String signAfter = getMD5(certNum.getBytes());
      System.out.println(signAfter.substring(0, 3));
    } catch (Exception e) {

    }
  }

  public void setCertNum(String certNum) {
    this.certNum = certNum;
  }

  private UserAgentVO parentVo;

  public UserAgentVO getParentVo() {
    return parentVo;
  }

  public void setParentVo(UserAgentVO parentVo) {
    this.parentVo = parentVo;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String idcardImgUrl;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String lifeImgUrl;

  public String getIdcardImgUrl() {
    return idcardImgUrl;
  }

  public void setIdcardImgUrl(String idcardImgUrl) {
    this.idcardImgUrl = idcardImgUrl;
  }

  public String getLifeImgUrl() {
    return lifeImgUrl;
  }

  public void setLifeImgUrl(String lifeImgUrl) {
    this.lifeImgUrl = lifeImgUrl;
  }

  public boolean getIsLeaf() {
    return isLeaf;
  }

  public void setIsLeaf(boolean leaf) {
    isLeaf = leaf;
  }

  public BigDecimal getCommissionFee() {
    return commissionFee;
  }

  public void setCommissionFee(BigDecimal commissionFee) {
    this.commissionFee = commissionFee;
  }
}
