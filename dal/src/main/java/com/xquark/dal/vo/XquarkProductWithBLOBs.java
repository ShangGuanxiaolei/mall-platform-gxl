package com.xquark.dal.vo;

public class XquarkProductWithBLOBs extends XquarkProduct {
    private String description;

    private String detailH5;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getDetailH5() {
        return detailH5;
    }

    public void setDetailH5(String detailH5) {
        this.detailH5 = detailH5 == null ? null : detailH5.trim();
    }
}