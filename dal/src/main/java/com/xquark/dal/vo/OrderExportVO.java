package com.xquark.dal.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;

import com.xquark.dal.type.RefundType;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.status.PayStatus;

/**
 * 新建用于bos导出订单excel
 *
 * @author anubis
 */
public class OrderExportVO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String idLong;
  private String orderNo;
  private String payNo;
  private UserPartnerType partnerType;
  private String outTradeNo;
  private String typeStr;
  private String payTypeStr;
  private String statusStr;
  private PayStatus paidStatus;
  private String shopName;
  private String buyerPhone;
  private String sellerPhone;
  private BigDecimal totalFee;
  private BigDecimal goodsFee;
  private BigDecimal logisticsFee;
  private BigDecimal discountFee;
  private BigDecimal paidFee;
  private String paidAtStr;
  private String logisticsCompany;
  private String logisticsOrderNo;
  private String createdAtStr;
  private String updatedAtStr;
  private String checkAtStr;
  private BigDecimal refundFee;
  private BigDecimal refundPlatformFee;
  private RefundType refundType;
  private String refundAtStr;
  private String doRefundAtStr;
  private String addressDetails;
  private String productName;
  private BigDecimal productPrice;
  private Integer productAmount;
  private String consignee;
  private String skuStr;

  public OrderExportVO() {
  }

  public OrderExportVO(OrderVO vo) {
    BeanUtils.copyProperties(vo, this);
    this.createdAtStr = vo.getCreatedAtStr();
    this.updatedAtStr = vo.getUpdatedAtStr();
    this.payTypeStr = vo.getPayTypeStr();
    this.statusStr = vo.getStatusStr();
    this.paidAtStr = vo.getPaidAtStr();
    this.checkAtStr = vo.getCheckingAtStr();
    this.refundAtStr = vo.getRefundAtStr();
    this.doRefundAtStr = vo.getDoRefundAtStr();
    this.consignee = vo.getOrderAddress().getConsignee();
  }

  public String getIdLong() {
    return idLong;
  }

  public void setIdLong(String idLong) {
    this.idLong = idLong;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getPayNo() {
    return payNo;
  }

  public void setPayNo(String payNo) {
    this.payNo = payNo;
  }

  public UserPartnerType getPartnerType() {
    return partnerType;
  }

  public void setPartnerType(UserPartnerType partnerType) {
    this.partnerType = partnerType;
  }

  public String getOutTradeNo() {
    return outTradeNo;
  }

  public void setOutTradeNo(String outTradeNo) {
    this.outTradeNo = outTradeNo;
  }

  public String getTypeStr() {
    return typeStr;
  }

  public void setTypeStr(String typeStr) {
    this.typeStr = typeStr;
  }

  public String getPayTypeStr() {
    return payTypeStr;
  }

  public void setPayTypeStr(String payTypeStr) {
    this.payTypeStr = payTypeStr;
  }

  public String getStatusStr() {
    return statusStr;
  }

  public void setStatusStr(String statusStr) {
    this.statusStr = statusStr;
  }

  public PayStatus getPaidStatus() {
    return paidStatus;
  }

  public void setPaidStatus(PayStatus paidStatus) {
    this.paidStatus = paidStatus;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public BigDecimal getPaidFee() {
    return paidFee;
  }

  public void setPaidFee(BigDecimal paidFee) {
    this.paidFee = paidFee;
  }

  public String getPaidAtStr() {
    return paidAtStr;
  }

  public void setPaidAtStr(String paidAtStr) {
    this.paidAtStr = paidAtStr;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getcreatedAtStr() {
    return createdAtStr;
  }

  public void setcreatedAtStr(String createdAtStr) {
    this.createdAtStr = createdAtStr;
  }

  public String getupdatedAtStr() {
    return updatedAtStr;
  }

  public void setupdatedAtStr(String updatedAtStr) {
    this.updatedAtStr = updatedAtStr;
  }

  public String getCheckAtStr() {
    return checkAtStr;
  }

  public void setCheckAtStr(String checkAtStr) {
    this.checkAtStr = checkAtStr;
  }

  public BigDecimal getRefundFee() {
    return refundFee;
  }

  public void setRefundFee(BigDecimal refundFee) {
    this.refundFee = refundFee;
  }

  public BigDecimal getRefundPlatformFee() {
    return refundPlatformFee;
  }

  public void setRefundPlatformFee(BigDecimal refundPlatformFee) {
    this.refundPlatformFee = refundPlatformFee;
  }

  public RefundType getRefundType() {
    return refundType;
  }

  public void setRefundType(RefundType refundType) {
    this.refundType = refundType;
  }

  public String getRefundAtStr() {
    return refundAtStr;
  }

  public void setRefundAtStr(String refundAtStr) {
    this.refundAtStr = refundAtStr;
  }

  public String getDoRefundAtStr() {
    return doRefundAtStr;
  }

  public void setDoRefundAtStr(String doRefundAtStr) {
    this.doRefundAtStr = doRefundAtStr;
  }

  public String getAddressDetails() {
    return addressDetails;
  }

  public void setAddressDetails(String addressDetails) {
    this.addressDetails = addressDetails;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public Integer getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(Integer productAmount) {
    this.productAmount = productAmount;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }

}
