package com.xquark.dal.vo;

import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/18 15:48
 */
public class PromotionOrderDetailVo {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String orderHeadCode;
    private String orderDetailCode;
    private String pCode;
    private String pDetailCode;
    private String pType;
    private Integer memberId;
    private Date createAt;
    private Date updateAt;
    private Integer isDeleted;
    private String pieceGroupTranCode;
    private String pieceGroupDetailCode;

    public String getOrderHeadCode() {
        return orderHeadCode;
    }

    public void setOrderHeadCode(String orderHeadCode) {
        this.orderHeadCode = orderHeadCode;
    }

    public String getOrderDetailCode() {
        return orderDetailCode;
    }

    public void setOrderDetailCode(String orderDetailCode) {
        this.orderDetailCode = orderDetailCode;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getPieceGroupTranCode() {
        return pieceGroupTranCode;
    }

    public void setPieceGroupTranCode(String pieceGroupTranCode) {
        this.pieceGroupTranCode = pieceGroupTranCode;
    }

    public String getPieceGroupDetailCode() {
        return pieceGroupDetailCode;
    }

    public void setPieceGroupDetailCode(String pieceGroupDetailCode) {
        this.pieceGroupDetailCode = pieceGroupDetailCode;
    }
}
