package com.xquark.dal.vo;

import java.util.Date;

/**
 * Created by jitre on 11/17/17. 用于在后台管理回复数据
 */
public class ReplyManagment {

  private String id;

  private String content;

  private Date createdAt;

  private String loginname;

  private Boolean isBlocked;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getLoginname() {
    return loginname;
  }

  public void setLoginname(String loginname) {
    this.loginname = loginname;
  }

  public Boolean getBlocked() {
    return isBlocked;
  }

  public void setBlocked(Boolean blocked) {
    isBlocked = blocked;
  }
}
