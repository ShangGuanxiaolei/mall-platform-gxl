package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionAble;
import com.xquark.dal.model.PromotionBargainProduct;
import com.xquark.dal.type.PromotionType;

import java.util.Date;

/**
 * @author wangxinhua
 * @date 2019-04-06
 * @since 1.0
 */
public class PromotionBargainSku extends PromotionBargainProduct implements PromotionAble {

  private boolean supportLimit;

  private String title;
  private Date validFrom;
  private Date validTo;

  public boolean getSupportLimit() {
    return supportLimit;
  }

  public void setSupportLimit(boolean supportLimit) {
    this.supportLimit = supportLimit;
  }

  @Override
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  @Override
  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  @Override
  public PromotionType getPromotionType() {
    return PromotionType.BARGAIN;
  }
}
