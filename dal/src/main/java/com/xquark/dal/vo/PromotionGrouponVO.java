package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionGroupon;
import com.xquark.dal.model.PromotionProduct;
import java.math.BigDecimal;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by chh on 16-10-12.
 */
public class PromotionGrouponVO extends PromotionGroupon implements PromotionProduct {

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validFrom;
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validTo;

  private String activityGrouponId;

  private Boolean promotionArchive;

  private Long productAmount;

  // 页面展示用, 不从数据库中读取
  private int needNumbers;

  public Long getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(Long productAmount) {
    this.productAmount = productAmount;
  }

  private List<ActivityGrouponDetailVO> users;

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidFrom() {
    return validFrom;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidTo() {
    return validTo;
  }

  @Override
  public Boolean getPromotionArchive() {
    return this.promotionArchive;
  }

  @Override
  public boolean getIsSellOut() {
    return getAmount() == null || getAmount().compareTo(BigDecimal.ZERO) <= 0
        || getProductAmount() == null || getProductAmount() <= 0;
  }

  public void setPromotionArchive(Boolean promotionArchive) {
    this.promotionArchive = promotionArchive;
  }

  public List<ActivityGrouponDetailVO> getUsers() {
    return users;
  }

  public void setUsers(List<ActivityGrouponDetailVO> users) {
    this.users = users;
  }

  public String getActivityGrouponId() {
    return activityGrouponId;
  }

  public void setActivityGrouponId(String activityGrouponId) {
    this.activityGrouponId = activityGrouponId;
  }

  public int getNeedNumbers() {
    return needNumbers;
  }

  public void setNeedNumbers(int needNumbers) {
    this.needNumbers = needNumbers;
  }
}
