package com.xquark.dal.vo;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/6/3
 * Time: 15:08
 * Description: 库存查询
 */
public class AmountVo {
  private  Integer  amount;
  private  String  skuCode;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }
}
