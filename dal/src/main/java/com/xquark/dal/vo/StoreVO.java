package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.Store;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

/**
 * Created by chh on 17/07/14.
 */
public class StoreVO extends Store {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private int memberNum;

  public int getMemberNum() {
    return memberNum;
  }

  public void setMemberNum(int memberNum) {
    this.memberNum = memberNum;
  }
}
