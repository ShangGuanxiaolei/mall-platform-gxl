package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.*;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.type.RefundType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class OrderVO extends Order {

  private static final long serialVersionUID = 1L;
  @ApiModelProperty(value = "买家电话")
  private String buyerPhone;
  @ApiModelProperty(value = "卖家电话")
  private String sellerPhone;
  private String shopName;
  private String paymentChannel; //付款渠道
  private List<CouponInfoVO> orderCoupons;//订单红包信息,需要手动查询
  private BigDecimal refundableFee;// 可退款的金额（不包含运费）
  private Date doRefundAt;//退款时间
  @ApiModelProperty(value = "详细地址")
  private String addressDetails;
  @ApiModelProperty(value = "订单地址")
  private OrderAddress orderAddress;  //订单地址
  private String outTradeNo;  //外部订单号
  private boolean showRefundBtn;  //是否显示退款按钮
  private boolean showCancelRefundBtn;//是否显示取消退款的按钮
  private List<OrderItemVO> orderItemVOs;  //订单列表
  private List<OrderItem> orderItems;  //订单列表
  private Long seq;  //客户在店铺的第X笔订单
  private BigDecimal hongbaoAmount;  //红包数量
  private BigDecimal commissionFee;  //?
  private String notes;
  private int defDelayDate;
  private Integer isCrossBorder;// 是否跨境
  private String certificateId;// 身份证
  private String mainOrderNo;
  private boolean haveBill;//是否有发票
  private boolean checkLoc;//是否可以查看物流
  private boolean checkRef;//是否可以退款
  private boolean checkCancelOrder;//是否可以取消定单
  private List<SubOrderDal> subOrder;//第三方子订单详情
  private boolean checkAddress; // 是否可以修改地址

  /**
   * 仓库的审核状态
   */
  private OrderRefundWarehouseCheckStatus wmsCheckStatus;

  /**
    *退款关联的原始订单状态
    */
  private OrderStatus orderStatus;

  /***
   * 审核备注
   */
  private String wmsCheckRemark;

  /**
   * 审核时间
   */
  private Date wmsCheckTime;

  /**
   * 立省
   */
  private BigDecimal cutDown;

  private String sellerName;
  private String buyerName;
  private String buyerImgUrl;

  private String partnerId;       //合伙人ID
  private String partnerPhone;       //合伙人手机号
  private String partnerName;       //合伙人名字

  private String refundMemo;      // 退款说明
  private String refundStatus;

  private String fromType;

  // 买家用户级别（b2b经销商订单需要显示用户级别）
  private AgentType agentType;

  private BigDecimal amount;

  private BigDecimal price;

  // 订单的审核类型，主要是进货订单使用，用于判断哪些订单需要平台审核或上级审核
  private AuditType auditType;

  private Shop buyerShop;

  private Shop sellerShop;

  private PromotionCouponVo promotionCouponVo;

  private boolean isCommented;

  @ApiModelProperty(value = "退款原因")
  String refundReason;  //退款原因

  @ApiModelProperty(value = "退货物流公司")
  private String refundLogisticsCompany; // 物流公司
  @ApiModelProperty(value = "退货物流编号")
  private String refundLogisticsNo; // 物流编号

  // 退款或退货处理
  @ApiModelProperty(value = "退款或退货处理")
  private String buyerRequire;

  private List<OrderRefundImg> refundImgList;

  // 是否确认收货过了7天
  private boolean is7DaysOut;
  // 是否确认收货过了30天
  private boolean is30DaysOut;

  private String skuCode;

  private String skuStr;

  private String barCode;

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public boolean isShowCancelRefundBtn() {
    if (Objects.equals(OrderSortType.GIFT, getOrderType())) {
      return false;
    }
    return showCancelRefundBtn;
  }

  public void setShowCancelRefundBtn(boolean showCancelRefundBtn) {
    this.showCancelRefundBtn = showCancelRefundBtn;
  }

  public List<OrderRefundImg> getRefundImgList() {
    return refundImgList;
  }

  public void setRefundImgList(List<OrderRefundImg> refundImgList) {
    this.refundImgList = refundImgList;
  }

  public boolean getIs30DaysOut() {
    Date checkingAt = getCheckingAt();
    if (checkingAt != null) {
      int days = (int) ((new Date().getTime() - checkingAt.getTime()) / (1000 * 3600 * 24));
      return days > 30;
    } else {
      return false;
    }
  }

  public void setIs30DaysOut(boolean is30DaysOut) {
    this.is30DaysOut = is30DaysOut;
  }

  public boolean getIs7DaysOut() {
    Date checkingAt = getCheckingAt();
    if (checkingAt != null) {
      int days = (int) ((new Date().getTime() - checkingAt.getTime()) / (1000 * 3600 * 24));
      return days > 7;
    } else {
      return false;
    }
  }

  public void setIs7DaysOut(boolean is7DaysOut) {
    this.is7DaysOut = is7DaysOut;
  }

  public String getBuyerRequire() {
    return buyerRequire;
  }

  public void setBuyerRequire(String buyerRequire) {
    this.buyerRequire = buyerRequire;
  }

  public String getRefundReason() {
    return refundReason;
  }

  public void setRefundReason(String refundReason) {
    this.refundReason = refundReason;
  }

  public String getRefundLogisticsCompany() {
    return refundLogisticsCompany;
  }

  public void setRefundLogisticsCompany(String refundLogisticsCompany) {
    this.refundLogisticsCompany = refundLogisticsCompany;
  }

  public String getRefundLogisticsNo() {
    return refundLogisticsNo;
  }

  public void setRefundLogisticsNo(String refundLogisticsNo) {
    this.refundLogisticsNo = refundLogisticsNo;
  }

  public PromotionCouponVo getPromotionCouponVo() {
    return promotionCouponVo;
  }

  public void setPromotionCouponVo(PromotionCouponVo promotionCouponVo) {
    this.promotionCouponVo = promotionCouponVo;
  }

  public Shop getBuyerShop() {
    return buyerShop;
  }

  public void setBuyerShop(Shop buyerShop) {
    this.buyerShop = buyerShop;
  }

  public Shop getSellerShop() {
    return sellerShop;
  }

  public void setSellerShop(Shop sellerShop) {
    this.sellerShop = sellerShop;
  }

  public AuditType getAuditType() {
    return auditType;
  }

  public void setAuditType(AuditType auditType) {
    this.auditType = auditType;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public AgentType getAgentType() {
    return agentType;
  }

  public void setAgentType(AgentType agentType) {
    this.agentType = agentType;
  }

  public String getBuyerImgUrl() {
    return buyerImgUrl;
  }

  public void setBuyerImgUrl(String buyerImgUrl) {
    this.buyerImgUrl = buyerImgUrl;
  }

  public String getFromType() {
    return fromType;
  }

  public void setFromType(String fromType) {
    this.fromType = fromType;
  }

  private ActivityGrouponStatus grouponStatus; //团购状态

  public ActivityGrouponStatus getGrouponStatus() {
    return grouponStatus;
  }

  public void setGrouponStatus(ActivityGrouponStatus grouponStatus) {
    this.grouponStatus = grouponStatus;
  }

  public String getRefundStatus() {
    return refundStatus;
  }

  public void setRefundStatus(String refundStatus) {
    this.refundStatus = refundStatus;
  }

  public String getRefundMemo() {
    return refundMemo;
  }

  public void setRefundMemo(String refundMemo) {
    this.refundMemo = refundMemo;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  private BigDecimal couponAmount = BigDecimal.ZERO;

  private String itemProductName;

  private BigDecimal itemPrice;

  private Integer itemAmount;

  public OrderVO() {

  }


  public String getItemProductName() {
    return itemProductName;
  }

  public void setItemProductName(String itemProductName) {
    this.itemProductName = itemProductName;
  }

  public BigDecimal getItemPrice() {
    return itemPrice;
  }

  public void setItemPrice(BigDecimal itemPrice) {
    this.itemPrice = itemPrice;
  }

  public Integer getItemAmount() {
    return itemAmount;
  }

  public void setItemAmount(Integer itemAmount) {
    this.itemAmount = itemAmount;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getMainOrderNo() {
    return mainOrderNo;
  }

  public void setMainOrderNo(String mainOrderNo) {
    this.mainOrderNo = mainOrderNo;
  }

  public OrderVO(Order order) {
    BeanUtils.copyProperties(order, this);
  }

  public List<OrderItemVO> getOrderItemVOs() {
    return orderItemVOs;
  }

  public void setOrderItemVOs(List<OrderItemVO> orderItemVOs) {
    this.orderItemVOs = orderItemVOs;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(List<OrderItem> orderItems) {
    this.orderItems = orderItems;
  }


  public OrderAddress getOrderAddress() {
    return orderAddress;
  }

  public void setOrderAddress(OrderAddress orderAddress) {
    this.orderAddress = orderAddress;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getAddressDetails() {
    return addressDetails;
  }


  public boolean isShowRefundBtn() {
    if (Objects.equals(OrderSortType.GIFT, getOrderType())) {
      return false;
    }
    return showRefundBtn;
  }

  public void setShowRefundBtn(boolean showRefundBtn) {
    this.showRefundBtn = showRefundBtn;
  }

  public String getOutTradeNo() {
    return outTradeNo;
  }

  public void setOutTradeNo(String outTradeNo) {
    this.outTradeNo = outTradeNo;
  }

  public void setAddressDetails(String addressDetails) {
    this.addressDetails = addressDetails;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public Long getSeq() {
    return seq;
  }

  public void setSeq(Long seq) {
    this.seq = seq;
  }

  public Date getAutoSignAt() {
    if (this.getShippedAt() != null) {
      Calendar c = Calendar.getInstance();
      c.setTime(this.getShippedAt());
      c.add(Calendar.DATE, 7);
      return c.getTime();
    } else {
      return null;
    }

  }

  public String getIdLong() {
    try {
      return IdTypeHandler.decode(this.getId()) + "";
    } catch (Exception e) {
      return this.getId();
    }
  }

  public String getPaidAtStr() {
    if (this.getPaidAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getPaidAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getShippedAtStr() {
    if (this.getShippedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getShippedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getUpdatedAtStr() {
    if (this.getUpdatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getUpdatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getCheckingAtStr() {
    if (this.getCheckingAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCheckingAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getRefundAtStr() {
    if (this.getRefundAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getRefundAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getDoRefundAtStr() {
    if (this.getRefundType() == RefundType.SUCCESS) {
      return this.getUpdatedAtStr();
    }
    return "";
  }

  public Date getDoRefundAt() {
    if (this.getRefundType() == RefundType.SUCCESS) {
      this.doRefundAt = this.getUpdatedAt();
    }
    return this.doRefundAt;
  }

  public void setDoRefundAt(Date doRefundAt) {
    if (this.getRefundType() == RefundType.SUCCESS) {
      this.doRefundAt = this.getUpdatedAt();
    } else {
      this.doRefundAt = null;
    }
  }

  public String getTypeStr() {
    if (this.getType() == null) {
      return "";
    }
    String str = "";
    switch (this.getType()) {
      case DIRECT:
        str = "即时到账";
        break;
      case DANBAO:
        str = "担保交易";
        break;
      case PREORDER:
        str = "预定模式";
        break;
      case COD:
        str = "货到付款";
        break;
      default:
        str = this.getType().toString();
        break;
    }
    return str;
  }

  public String getPayTypeStr() {
    if (this.getPayType() == null) {
      return "";
    }
    String str = "";
    switch (this.getPayType()) {
      case WEIXIN:
        str = "微信支付";
        break;
      case WEIXIN_APP:
        str = "微信支付";
        break;
      case UNION:
        str = "银联";
        break;
      case ALIPAY:
        str = "支付宝";
        break;
      case ALIPAY_PC:
        str = "支付宝";
        break;
      case UMPAY:
        str = "U付";
        break;
      default:
        str = this.getPayType().toString();
        break;
    }
    return str;
  }

  public String getStatusStr() {
    if (this.getStatus() == null) {
      return "";
    }
    String str = "";
    switch (this.getStatus()) {
      case SUBMITTED:
        str = "待付款";
        break;
      case CANCELLED:
        str = "订单取消";
        break;
      case PAID:
        str = "待发货";
        break;
      case DELIVERY:
        str = "待发货";
        break;
      case SHIPPED:
        str = "待收货";
        break;
      case SUCCESS:
        str = "交易成功";
        break;
      case REFUNDING:
        str = "退款申请中";
        break;
      case CHANGING:
        str = "换货申请中";
        break;
      case REISSUING:
        str = "补货申请中";
        break;
      case CLOSED:
        str = "交易关闭";
        break;
      default:
        str = this.getStatus().toString();
        break;
    }
    return str;
  }

  public BigDecimal getHongbaoAmount() {
    return hongbaoAmount;
  }

  public void setHongbaoAmount(BigDecimal hongbaoAmount) {
    this.hongbaoAmount = hongbaoAmount;
  }

  public BigDecimal getCommissionFee() {
    return commissionFee;
  }

  public void setCommissionFee(BigDecimal commissionFee) {
    this.commissionFee = commissionFee;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public BigDecimal getCouponAmount() {
    return couponAmount;
  }

  public void setCouponAmount(BigDecimal couponAmount) {
    this.couponAmount = couponAmount;
  }

  public int getDefDelayDate() {
    return defDelayDate;
  }

  public void setDefDelayDate(int defDelayDate) {
    this.defDelayDate = defDelayDate;
  }

  public String getPaymentChannel() {
    return paymentChannel;
  }

  public void setPaymentChannel(String paymentChannel) {
    this.paymentChannel = paymentChannel;
  }

  public List<CouponInfoVO> getOrderCoupons() {
    return orderCoupons;
  }

  public void setOrderCoupons(List<CouponInfoVO> orderCoupons) {
    this.orderCoupons = orderCoupons;
  }

  public BigDecimal getRefundableFee() {
    return refundableFee;
  }

  public void setRefundableFee(BigDecimal refundableFee) {
    this.refundableFee = refundableFee;
  }

  public Integer getIsCrossBorder() {
    return isCrossBorder;
  }

  public void setIsCrossBorder(Integer isCrossBorder) {
    this.isCrossBorder = isCrossBorder;
  }

  public String getCertificateId() {
    return certificateId;
  }

  public void setCertificateId(String certificateId) {
    this.certificateId = certificateId;
  }

  public String getPartnerName() {
    return partnerName;
  }

  public void setPartnerName(String partnerName) {
    this.partnerName = partnerName;
  }

  public String getPartnerId() {
    return partnerId;
  }

  public void setPartnerId(String partnerId) {
    this.partnerId = partnerId;
  }

  public String getPartnerPhone() {
    return partnerPhone;
  }

  public void setPartnerPhone(String partnerPhone) {
    this.partnerPhone = partnerPhone;
  }

  public boolean getIsCommented() {
    return isCommented;
  }

  public void setCommented(boolean commented) {
    isCommented = commented;
  }

  public OrderRefundWarehouseCheckStatus getWmsCheckStatus() {
    return wmsCheckStatus;
  }

  public void setWmsCheckStatus(OrderRefundWarehouseCheckStatus wmsCheckStatus) {
    this.wmsCheckStatus = wmsCheckStatus;
  }

  public String getWmsCheckRemark() {
    return wmsCheckRemark;
  }

  public void setWmsCheckRemark(String wmsCheckRemark) {
    this.wmsCheckRemark = wmsCheckRemark;
  }

  public Date getWmsCheckTime() {
    return wmsCheckTime;
  }

  public void setWmsCheckTime(Date wmsCheckTime) {
    this.wmsCheckTime = wmsCheckTime;
  }

  public boolean isHaveBill() {
    return haveBill;
  }

  public void setHaveBill(boolean haveBill) {
    this.haveBill = haveBill;
  }

  public boolean isCheckLoc() {
    return checkLoc;
  }

  public void setCheckLoc(boolean checkLoc) {
    this.checkLoc = checkLoc;
  }

  public boolean isCheckRef() {
    return checkRef;
  }

  public void setCheckRef(boolean checkRef) {
    this.checkRef = checkRef;
  }

  public boolean getCheckCancelOrder() {
    if (Objects.equals(OrderSortType.GIFT, getOrderType())) {
      return false;
    }
    return checkCancelOrder;
  }

  public void setCheckCancelOrder(boolean checkCancelOrder) {
    this.checkCancelOrder = checkCancelOrder;
  }

  public List<SubOrderDal> getSubOrder() {
    return subOrder;
  }

  public void setSubOrder(List<SubOrderDal> subOrder) {
    this.subOrder = subOrder;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public BigDecimal getCutDown() {
    return cutDown;
  }

  public void setCutDown(BigDecimal cutDown) {
    this.cutDown = cutDown;
  }

  public boolean isCheckAddress() {
    return checkAddress;
  }

  public void setCheckAddress(boolean checkAddress) {
    this.checkAddress = checkAddress;
  }

}
