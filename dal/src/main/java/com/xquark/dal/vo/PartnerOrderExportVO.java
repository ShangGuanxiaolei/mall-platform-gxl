package com.xquark.dal.vo;


import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;

public class PartnerOrderExportVO implements Serializable {

  private static final long serialVersionUID = 1L;
  private String idLong;
  private String orderNo;
  private String partnerPhone;
  private String shopName;
  private String buyerPhone;
  private String sellerPhone;
  private BigDecimal commissionFee;
  private String createdAtStr;

  public PartnerOrderExportVO(OrderVO vo) {
    BeanUtils.copyProperties(vo, this);
    this.createdAtStr = vo.getCreatedAtStr();
    this.orderNo = vo.getOrderNo();
    this.partnerPhone = vo.getPartnerPhone();
    this.shopName = vo.getShopName();
    this.buyerPhone = vo.getBuyerPhone();
    this.sellerPhone = vo.getSellerPhone();
    this.commissionFee = vo.getCommissionFee();
    this.createdAtStr = vo.getCreatedAtStr();
  }

  public String getIdLong() {
    return idLong;
  }

  public void setIdLong(String idLong) {
    this.idLong = idLong;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getPartnerPhone() {
    return partnerPhone;
  }

  public void setPartnerPhone(String partnerPhone) {
    this.partnerPhone = partnerPhone;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public BigDecimal getCommissionFee() {
    return commissionFee;
  }

  public void setCommissionFee(BigDecimal commissionFee) {
    this.commissionFee = commissionFee;
  }

  public String getCreatedAtStr() {
    return createdAtStr;
  }

  public void setCreatedAtStr(String createdAtStr) {
    this.createdAtStr = createdAtStr;
  }
}
