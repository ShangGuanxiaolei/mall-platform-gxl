package com.xquark.dal.vo;

import com.xquark.dal.model.FragmentImage;

public class FragmentImageVO extends FragmentImage {


  private static final long serialVersionUID = -7103611964750928485L;

  private String imgUrl;

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  private int width;

  private int height;

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
}
