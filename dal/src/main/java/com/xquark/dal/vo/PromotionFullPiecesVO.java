package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionFullReduceDiscount;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

/**
 * Created by wangxinhua on 17-9-26. DESC:
 */
public class PromotionFullPiecesVO extends PromotionFullReduceVO<Integer> {

  private List<PromotionFullPiecesDiscountVO> discounts;

  @Override
  public List<PromotionFullPiecesDiscountVO> getDiscounts() {
    return discounts;
  }

  public void setDiscounts(List<PromotionFullPiecesDiscountVO> discounts) {
    this.discounts = discounts;
  }

  @Override
  public PromotionFullReduceDiscount<Integer> getSatisfiedDiscount(Integer number) {
    List<PromotionFullPiecesDiscountVO> discountVOS = this.getDiscounts();
    if (CollectionUtils.isEmpty(discountVOS)) {
      return null;
    }
    Iterator<PromotionFullPiecesDiscountVO> iterator = discountVOS.iterator();
    // 删除不满足条件的折扣
    while (iterator.hasNext()) {
      PromotionFullPiecesDiscountVO discount = iterator.next();
      @SuppressWarnings("unchecked")
      boolean isSatisfied = discount.isSatisfied(number);
      if (!isSatisfied) {
        iterator.remove();
      }
    }
    // 返回最大折扣
    return Collections.max(discountVOS);
  }
}
