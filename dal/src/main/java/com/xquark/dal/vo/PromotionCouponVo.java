package com.xquark.dal.vo;

import com.xquark.dal.model.promotion.Coupon;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * Created by  on 15-11-6.
 */
public class PromotionCouponVo extends Coupon {

  private String shopName;            //店铺名称

  private Boolean isAcquired = false; // 当前用户是否已领取优惠券

  private String strValidFrom;        //转换时间格式后的开始时间 yyyy-MM-dd

  private String strValidTo;           //转换时间格式后的结束时间 yyyy-MM-dd

  private int intDiscount;

  private int intApplyAbove;

  private String productName;

  // 商品名截取字符串
  private String productNameLimit;

  public String getProductNameLimit() {
    String value = getProductName();
    if (StringUtils.isNotEmpty(value) && value.length() > 12) {
      value = value.substring(0, 12) + "...";
    }
    return value;
  }

  public void setProductNameLimit(String productNameLimit) {
    this.productNameLimit = productNameLimit;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public int getIntApplyAbove() {
    return intApplyAbove;
  }

  public void setIntApplyAbove(int intApplyAbove) {
    this.intApplyAbove = intApplyAbove;
  }

  public int getIntDiscount() {
    return intDiscount;
  }

  public void setIntDiscount(int intDiscount) {
    this.intDiscount = intDiscount;
  }

  public String getStrValidFrom() {
    return strValidFrom;
  }

  public void setStrValidFrom(String strValidFrom) {
    this.strValidFrom = strValidFrom;
  }

  public String getStrValidTo() {
    if (this.getValidTo() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidTo(), "yyyy-MM-dd");
  }

  public void setStrValidTo(String strValidTo) {
    this.strValidTo = strValidTo;
  }

  public Boolean getIsAcquired() {
    return isAcquired;
  }

  public void setIsAcquired(Boolean isAcquired) {
    this.isAcquired = isAcquired;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getShopName() {
    return shopName;

  }


}

