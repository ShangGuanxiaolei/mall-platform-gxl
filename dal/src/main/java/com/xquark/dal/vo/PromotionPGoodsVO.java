package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2018/10/8
 * Time: 10:39
 * Description: 产品实体
 */
public class PromotionPGoodsVO extends BaseEntityImpl implements Archivable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "商品名称")
    private String name; // 商品名称
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;
    private String productId;
    private Double marketPrice;
    private Date effectTo;
    private Date effectFrom;
    private String pCode;
    private Boolean  qualityGoods; //是否有赠品
    //是否显示倒计时
    private Boolean showCountDown;
    //赠品方式
    private int gift;
    private String giftType;
    private int selfOperated; //是否自营

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }

    public String getGiftType() {
        return giftType;
    }

    public void setGiftType(String giftType) {
        this.giftType = giftType;
    }

    public int getGift() {
        return gift;
    }

    public void setGift(int gift) {
        this.gift = gift;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public Boolean getQualityGoods() {
        return qualityGoods;
    }

    public void setQualityGoods(Boolean qualityGoods) {
        this.qualityGoods = qualityGoods;
    }

    public Boolean getShowCountDown() {
        return showCountDown;
    }

    public void setShowCountDown(Boolean showCountDown) {
        this.showCountDown = showCountDown;
    }
    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }
}
