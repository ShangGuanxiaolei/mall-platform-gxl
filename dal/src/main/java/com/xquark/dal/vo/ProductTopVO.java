package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/10.
 */
public class ProductTopVO extends ProductTop implements FmtQiNiuImgVO {

  protected Logger log = LoggerFactory.getLogger(getClass());


  private BigDecimal point; //商品返利积分
  private BigDecimal deductionDPoint; //可用德分抵扣值
  private BigDecimal netWorth; //净值

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String logoUrl;

  private Long productAmount;

  private String siteHost = "http://nffclub.com";

  private String targetUrl;

  private int selfOperated; //自营标签，0：非自营，1：自营

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }

  public String getTargetUrl() {
    String targetUrl = siteHost + "/p/" + this.getProductId();
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public Long getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(Long productAmount) {
    this.productAmount = productAmount;
  }

  public boolean getIsSellOut() {
    return this.productAmount == null || this.productAmount == 0L;
  }

  @Override
  public String getTinyImgUrl() {
    return logoUrl;
  }

  @Override
  public String getMiddleImgUrl() {
    return logoUrl;
  }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public BigDecimal getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(BigDecimal netWorth) {
    this.netWorth = netWorth;
  }
}
