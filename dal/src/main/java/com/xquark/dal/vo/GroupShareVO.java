package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/5
 * 拼团分享VO
 */
public class GroupShareVO {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImgURL;

  private String productName;

  private BigDecimal price;

  private BigDecimal originalPrice;

  private int totalMember;

  private int currMember;

  private String tranCode;

  public String getProductImgURL() {
    return productImgURL;
  }

  public void setProductImgURL(String productImgURL) {
    this.productImgURL = productImgURL;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public int getNumberOfResetPeople() {
    return totalMember > currMember ? totalMember - currMember : 0;
  }

  public String getTranCode() {
    return tranCode;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }

  public int getTotalMember() {
    return totalMember;
  }

  public void setTotalMember(int totalMember) {
    this.totalMember = totalMember;
  }

  public int getCurrMember() {
    return currMember;
  }

  public void setCurrMember(int currMember) {
    this.currMember = currMember;
  }
}
