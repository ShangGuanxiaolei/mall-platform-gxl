package com.xquark.dal.vo;

import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.ClientModuleType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author wangxinhua.
 * @date 2018/12/18
 */
public class UserIdentityVO {

  private final Long cpId;
  private final boolean isNew;
  private final CareerLevelType levelType;

  /**
   * 大部分用户没有白名单, 使用emptyList(), 需要的时候再初始化
   */
  private List<ClientModuleType> blackModules = Collections.emptyList();

  public UserIdentityVO(Long cpId, boolean isNew, CareerLevelType levelType) {
    this.cpId = cpId;
    this.isNew = isNew;
    this.levelType = levelType;
  }

  public Long getCpId() {
    return cpId;
  }

  public boolean getIsNew() {
    return isNew;
  }

  public CareerLevelType getLevelType() {
    return levelType;
  }

  public void setBlackModules(ClientModuleType ...modules) {
    blackModules = new ArrayList<>(Arrays.asList(modules));
  }

  public List<ClientModuleType> getBlackModules() {
    return blackModules;
  }
}

