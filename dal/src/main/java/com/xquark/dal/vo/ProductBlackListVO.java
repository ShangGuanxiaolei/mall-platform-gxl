package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.ProductBlackList;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 17/08/09.
 */
public class ProductBlackListVO extends ProductBlackList {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String productName;

  private BigDecimal productPrice;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private String siteHost = "http://nffclub.com";

  private String targetUrl;

  public String getTargetUrl() {
    String targetUrl = siteHost + "/p/" + this.getProductId();
    return targetUrl;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }
}
