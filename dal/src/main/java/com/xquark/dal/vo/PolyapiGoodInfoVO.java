package com.xquark.dal.vo;

import java.math.BigDecimal;

/**
 * User: kong Date: 2018/6/16. Time: 14:04 菠萝派订单商品集合
 */
public class PolyapiGoodInfoVO {
  //商品id
  private String productId;
  //子订单号
  private String subOrderNo;
  //商家编码
  private String tradeGoodsNo;
  //商品交易名称
  private String tradeGoodsName;
  //商品交易规格
  private String tradeGoodsSpec;
  //商品数量
  private String goodsCount;
  //单价
  private BigDecimal price = new BigDecimal(0);
  //子订单优惠金额
  private BigDecimal discountMoney;
  //商品税费
  private BigDecimal taxAmount;
  //退款状态
  private String refundStatus;
  //子订单交易状态
  private String status;
  //货品备注
  private String remark;


  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getSubOrderNo() {
    return subOrderNo;
  }

  public void setSubOrderNo(String subOrderNo) {
    this.subOrderNo = subOrderNo;
  }

  public String getTradeGoodsNo() {
    return tradeGoodsNo;
  }

  public void setTradeGoodsNo(String tradeGoodsNo) {
    this.tradeGoodsNo = tradeGoodsNo;
  }

  public String getTradeGoodsName() {
    return tradeGoodsName;
  }

  public void setTradeGoodsName(String tradeGoodsName) {
    this.tradeGoodsName = tradeGoodsName;
  }

  public String getTradeGoodsSpec() {
    return tradeGoodsSpec;
  }

  public void setTradeGoodsSpec(String tradeGoodsSpec) {
    this.tradeGoodsSpec = tradeGoodsSpec;
  }

  public String getGoodsCount() {
    return goodsCount;
  }

  public void setGoodsCount(String goodsCount) {
    this.goodsCount = goodsCount;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigDecimal getDiscountMoney() {
    return discountMoney;
  }

  public void setDiscountMoney(BigDecimal discountMoney) {
    this.discountMoney = discountMoney;
  }

  public BigDecimal getTaxAmount() {
    return taxAmount;
  }

  public void setTaxAmount(BigDecimal taxAmount) {
    this.taxAmount = taxAmount;
  }

  public String getRefundStatus() {
    return refundStatus;
  }

  public void setRefundStatus(String refundStatus) {
    this.refundStatus = refundStatus;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @Override
  public String toString() {
    return "GoodInfo{" +
        "productId='" + productId + '\'' +
        ", subOrderNo='" + subOrderNo + '\'' +
        ", tradeGoodsNo='" + tradeGoodsNo + '\'' +
        ", tradeGoodsName='" + tradeGoodsName + '\'' +
        ", tradeGoodsSpec='" + tradeGoodsSpec + '\'' +
        ", goodsCount='" + goodsCount + '\'' +
        ", price=" + price +
        ", discountMoney=" + discountMoney +
        ", taxAmount=" + taxAmount +
        ", refundStatus='" + refundStatus + '\'' +
        ", status='" + status + '\'' +
        ", remark='" + remark + '\'' +
        '}';
  }
}
