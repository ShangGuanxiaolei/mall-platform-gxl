package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.Map;

/**
 * Created by chh on 16-9-20.
 */
public class CategoryTreeVO {

  private String id;
  private String text;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String icon;

  private Object children;

  private Map state;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String tinyImg;

  private String fontColor;

  public String getTinyImg() {
    return tinyImg;
  }

  public void setTinyImg(String tinyImg) {
    this.tinyImg = tinyImg;
  }

  public String getFontColor() {
    return fontColor;
  }

  public void setFontColor(String fontColor) {
    this.fontColor = fontColor;
  }

  public Map getState() {
    return state;
  }

  public void setState(Map state) {
    this.state = state;
  }

  public Object getChildren() {
    return children;
  }

  public void setChildren(Object children) {
    this.children = children;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }
}