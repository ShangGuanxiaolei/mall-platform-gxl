package com.xquark.dal.vo;

/**
 * @author wangxinhua.
 * @date 2018/11/4
 */
public class PromotionTranOrderVO extends PromotionTranInfoVO {

  private String orderId;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

}
