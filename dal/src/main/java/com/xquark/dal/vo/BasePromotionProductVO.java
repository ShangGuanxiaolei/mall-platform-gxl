package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.type.PromotionType;

import java.util.Date;
import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-16
 * @since 1.0
 */
public abstract class BasePromotionProductVO implements PromotionProductVO {

  private String id;

  private ProductBasicVO product;

  private Promotion promotion;

  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public ProductBasicVO getProduct() {
    return product;
  }

  public void setProduct(ProductBasicVO product) {
    this.product = product;
  }

  @Override
  public Promotion getPromotion() {
    return promotion;
  }

  public void setPromotion(Promotion promotion) {
    this.promotion = promotion;
  }

  @Override
  public Boolean getPromotionClosed() {
    return Optional.ofNullable(promotion.getClosed()).orElse(false) || !isValid();
  }

  @Override
  public PromotionType getPromotionType() {
      return promotion.getPromotionType();
  }

  @Override
  public String getTitle() {
      return promotion.getTitle();
  }

  @Override
  public Date getValidFrom() {
      return promotion.getValidFrom();
  }

  @Override
  public Date getValidTo() {
    return promotion.getValidTo();
  }
}
