package com.xquark.dal.vo;

import java.math.BigDecimal;

/**
 * @author liuwei
 * @date 18-7-26 下午3:06
 */
public class PointExportByCustomerVO {
    private int orderMonth;
    private Long payeeCpid;
    private BigDecimal earnAmout;
    private BigDecimal rebateAmout;
    private BigDecimal otherAmout;
    private BigDecimal otherAmout2;
    private BigDecimal DF;
    private String namecn;
    private String customerNumber;
    private String tinCode;
    private int gender;
    private String spouseName;
    private String customerNationalId;
    private String spouseNationalId;
    private String store;
    private String subTypeName;
    private String subType;
    private String typeName;
    private String type;
    private String accountName;
    private String bankAccount;
    private String bankName;
    private String bankFullName;
    private String bankProvince;

    public BigDecimal getOtherAmout2() {
        return otherAmout2;
    }

    public void setOtherAmout2(BigDecimal otherAmout2) {
        this.otherAmout2 = otherAmout2;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDF() {
        if(DF != null) {
            return String.format("%.2f", DF);
        }
        return null;
    }

    public void setDF(BigDecimal DF) {
        this.DF = DF;
    }

    public String getCustomerNationalId() {
        return customerNationalId;
    }

    public void setCustomerNationalId(String customerNationalId) {
        this.customerNationalId = customerNationalId;
    }

    public String getSpouseNationalId() {
        return spouseNationalId;
    }

    public void setSpouseNationalId(String spouseNationalId) {
        this.spouseNationalId = spouseNationalId;
    }

    public int getOrderMonth() {
        return orderMonth;
    }

    public void setOrderMonth(int orderMonth) {
        this.orderMonth = orderMonth;
    }

    public Long getPayeeCpid() {
        return payeeCpid;
    }

    public void setPayeeCpid(Long payeeCpid) {
        this.payeeCpid = payeeCpid;
    }

    public String getEarnAmout() {
        if(earnAmout != null) {
            return String.format("%.2f", earnAmout);
        }
        return null;
    }

    public void setEarnAmout(BigDecimal earnAmout) {
        this.earnAmout = earnAmout;
    }

    public String getRebateAmout() {
        if(rebateAmout != null) {
            return String.format("%.2f", rebateAmout);
        }
        return null;
    }

    public void setRebateAmout(BigDecimal rebateAmout) {
        this.rebateAmout = rebateAmout;
    }

    public String getOtherAmout() {
        if(otherAmout != null) {
            return String.format("%.2f", otherAmout);
        }
        return null;
    }

    public void setOtherAmout(BigDecimal otherAmout) {
        this.otherAmout = otherAmout;
    }

    public String getNamecn() {
        return namecn;
    }

    public void setNamecn(String namecn) {
        this.namecn = namecn;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getTinCode() {
        return tinCode;
    }

    public void setTinCode(String tinCode) {
        this.tinCode = tinCode;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getSubTypeName() {
        return subTypeName;
    }

    public void setSubTypeName(String subTypeName) {
        this.subTypeName = subTypeName;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankFullName() {
        return bankFullName;
    }

    public void setBankFullName(String bankFullName) {
        this.bankFullName = bankFullName;
    }

    public String getBankProvince() {
        return bankProvince;
    }

    public void setBankProvince(String bankProvince) {
        this.bankProvince = bankProvince;
    }
}
