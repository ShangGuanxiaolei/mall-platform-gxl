package com.xquark.dal.vo;


import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.PartnerStatus;

import java.math.BigDecimal;
import java.util.Date;

public class PartnerCmVO extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String phone;

  private BigDecimal fee;

  private String name;

  private String status;

  private Date createdAt;

  private String shopId;

  private String userId;

  // 合伙模式
  private String typeStr;

  // 是否有黑名单商品
  private boolean isBlackList;

  public boolean isBlackList() {
    return isBlackList;
  }

  public void setBlackList(boolean blackList) {
    isBlackList = blackList;
  }

  public String getTypeStr() {
    return typeStr;
  }

  public void setTypeStr(String typeStr) {
    this.typeStr = typeStr;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  private PartnerStatus partner_status;

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public PartnerStatus getPartner_status() {
    return partner_status;
  }

  public void setPartner_status(PartnerStatus partner_status) {
    this.partner_status = partner_status;
  }
}
