package com.xquark.dal.vo;

import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

/**
 * @author wangxinhua
 * @date 2019-04-24
 * @since 1.0
 */
public class PromotionProductBasicVO extends ProductBasicVO {

    /**
     * 活动价格
     */
    private final BigDecimal promotionPrice;

    private final BigDecimal promotionPoint;

    private final BigDecimal promotionConversionPrice;

    /**
     * 活动库存
     */
    private final Long promotionAmount;

    /**
     * 活动已售数量
     */
    private final Long promotionSales;

    public PromotionProductBasicVO(BigDecimal promotionPrice, Long promotionAmount, Long promotionSales,
                                   BigDecimal promotionPoint, BigDecimal promotionConversionPrice, ProductBasicVO source) {
        this.promotionPrice = promotionPrice;
        this.promotionAmount = promotionAmount;
        this.promotionSales = promotionSales;
        this.promotionPoint = promotionPoint;
        this.promotionConversionPrice = promotionConversionPrice;
        BeanUtils.copyProperties(source, this);
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public BigDecimal getPromotionPoint() {
        return promotionPoint;
    }

    public BigDecimal getPromotionConversionPrice() {
        return promotionConversionPrice;
    }

    public Long getPromotionAmount() {
        return promotionAmount;
    }

    public Long getPromotionSales() {
        return promotionSales;
    }

    public BigDecimal getPercent() {
        final BigDecimal sales = Optional.ofNullable(promotionSales)
                .map(BigDecimal::valueOf)
                .orElse(BigDecimal.ZERO);
        final BigDecimal amt = Optional.ofNullable(promotionAmount)
                .map(BigDecimal::valueOf)
                .orElse(BigDecimal.ZERO);
        final BigDecimal total = sales.add(amt);
        if (total.signum() == 0) {
            return BigDecimal.ZERO;
        }
        return sales
                .divide(total, 2, RoundingMode.HALF_EVEN);
    }
}
