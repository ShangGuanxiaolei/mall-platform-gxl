package com.xquark.dal.vo;

import com.xquark.dal.BaseEntityImpl;

import com.xquark.dal.status.ExecStatus;
import com.xquark.dal.status.PieceStatus;
import java.util.Date;

public class PromotionGroupDetailVO extends BaseEntityImpl {

    public String getPieceGroupTranCode() {
        return pieceGroupTranCode;
    }

    public void setPieceGroupTranCode(String pieceGroupTranCode) {
        this.pieceGroupTranCode = pieceGroupTranCode;
    }

    private String pieceGroupTranCode;
    private String headimgurl;
    private Date joinTime;
    private String nickname;
    private int isGroupHead;
    private String phone;
    private int pieceStatus;

    public int getPieceStatus() {
        return pieceStatus;
    }

    public void setPieceStatus(int pieceStatus) {
        this.pieceStatus = pieceStatus;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getIsGroupHead() {
        return isGroupHead;
    }

    public void setIsGroupHead(int isGroupHead) {
        this.isGroupHead = isGroupHead;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCpid() {
        return cpid;
    }

    public void setCpid(int cpid) {
        this.cpid = cpid;
    }

    public String getHeadimgurl() {

        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    private int cpid;

  /**
   * 获取groupStatus
   * 展示用
   */
  public ExecStatus getGroupStatus() {
      return PieceStatus.fromCode(pieceStatus).getRet();
  }

}
