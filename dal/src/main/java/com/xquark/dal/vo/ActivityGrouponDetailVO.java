package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.ActivityGrouponDetail;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

/**
 * Created by wangxinhua on 17-7-28. DESC:
 */
public class ActivityGrouponDetailVO extends ActivityGrouponDetail {

  private String userId;
  private String username;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String avatar;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }
}
