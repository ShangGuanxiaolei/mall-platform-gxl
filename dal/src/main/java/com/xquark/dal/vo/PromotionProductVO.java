package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionAble;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public interface PromotionProductVO extends PromotionAble {

  /**
   * 获取活动商品id
   *
   * @return 活动商品id
   */
  String getId();

  /**
   * 获取关联商品
   *
   * @return {@link ProductBasicVO} 关联商VO
   */
  ProductBasicVO getProduct();

  /**
   * 获取关联活动
   *
   * @return 关联活动对象
   */
  Promotion getPromotion();

  /**
   * 查询关联活动是否已经结束
   *
   * @return close or not
   */
  Boolean getPromotionClosed();

  /**
   * 获取活动开始日期
   *
   * @return 开始日期
   */
  Date getValidFrom();

  /**
   * 获取活动结束日期
   *
   * @return 结束日期
   */
  Date getValidTo();

  /**
   * 获取活动库存
   *
   * @return 活动库存
   */
  Long getAmount();

  /**
   * 获取活动价格
   *
   * @return 活动价格
   */
  BigDecimal getPromotionPrice();

  /**
   * 获取活动兑换价
   */
  BigDecimal getPromotionConversionPrice();

  /**
   * 获取活动德分
   */
  BigDecimal getPromotionPoint();

  /**
   * 获取活动类型
   *
   * @return instance of {@link PromotionType}
   */
  PromotionType getPromotionType();

  /**
   * 获取价格体系 - 多规格取所有sku里面最小的值
   * @return 价格体系列表
   */
  PromotionPgPrice getPricing();

}
