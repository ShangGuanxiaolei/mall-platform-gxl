package com.xquark.dal.vo;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.PromotionFullCutDiscount;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wangxinhua on 17-9-28. DESC:
 */
public class PromotionFullCutDiscountVO extends PromotionFullCutDiscount<BigDecimal> {

  private List<Product> giftProducts;
  private String productNames;

  public List<Product> getGiftProducts() {
    return giftProducts;
  }

  public void setGiftProducts(List<Product> giftProducts) {
    this.giftProducts = giftProducts;
  }

  public String getProductNames() {
    return productNames;
  }

  public void setProductNames(String productNames) {
    this.productNames = productNames;
  }
}
