package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.math.BigDecimal;

/**
 * Created by chh on 16-10-24.
 */
public class PromotionGrouponUserVO extends PromotionGrouponVO {

  private String title;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;
  private int paidCount;
  private int needNumbers;
  ActivityGrouponStatus status;
  private String dayDesc;

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private String activityGrouponId;

  private String targetUrl;

  private String shareUrl;

  public String getValidFromStr() {
    if (this.getValidFrom() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidFrom(), "yyyy-MM-dd HH:mm");
  }

  public String getValidToStr() {
    if (this.getValidTo() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidTo(), "yyyy-MM-dd HH:mm");
  }

  public String getShareUrl() {
    return shareUrl;
  }

  public void setShareUrl(String shareUrl) {
    this.shareUrl = shareUrl;
  }

  public String getTargetUrl() {
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public String getActivityGrouponId() {
    return activityGrouponId;
  }

  public void setActivityGrouponId(String activityGrouponId) {
    this.activityGrouponId = activityGrouponId;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getDayDesc() {
    return dayDesc;
  }

  public void setDayDesc(String dayDesc) {
    this.dayDesc = dayDesc;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public int getPaidCount() {
    return paidCount;
  }

  public void setPaidCount(int paidCount) {
    this.paidCount = paidCount;
  }

  public ActivityGrouponStatus getStatus() {
    return status;
  }

  public void setStatus(ActivityGrouponStatus status) {
    this.status = status;
  }

  public int getNeedNumbers() {
    return this.getNumbers() - this.paidCount;
  }
}
