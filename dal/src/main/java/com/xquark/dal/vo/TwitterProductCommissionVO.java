package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.status.CommissionProductType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 17/06/28.
 */
public class TwitterProductCommissionVO extends TwitterProductCommission {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private BigDecimal totalCommission;

  /**
   * 商品总佣金，如果是百分比类型，则是价格乘以三级分佣佣金，如果是固定金额，则是三级分佣相加总和
   */
  public BigDecimal getTotalCommission() {
    BigDecimal result = BigDecimal.ZERO;
    CommissionProductType type = getType();
    double firstLevelRate = getFirstLevelRate();
    double secondLevelRate = getSecondLevelRate();
    double thirdLevelRate = getThirdLevelRate();
    BigDecimal price = getProductPrice();
    BigDecimal totalRate = new BigDecimal(firstLevelRate).add(new BigDecimal(secondLevelRate))
        .add(new BigDecimal(thirdLevelRate));
    if (type == CommissionProductType.PER) {
      result = price.multiply(totalRate.divide(new BigDecimal("100"), 4, BigDecimal.ROUND_HALF_UP))
          .setScale(2, BigDecimal.ROUND_HALF_UP);
    } else {
      result = totalRate;
    }
    return result;
  }

  public void setTotalCommission(BigDecimal totalCommission) {
    this.totalCommission = totalCommission;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }
}
