package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionFullCutDiscount;
import com.xquark.dal.model.PromotionFullReduceDiscount;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

/**
 * Created by wangxinhua on 17-9-25. DESC:
 */
public class PromotionFullCutVO extends PromotionFullReduceVO<BigDecimal> {


  private List<PromotionFullCutDiscountVO> discounts;

  @Override
  public List<PromotionFullCutDiscountVO> getDiscounts() {
    return discounts;
  }

  @Override
  public PromotionFullReduceDiscount<BigDecimal> getSatisfiedDiscount(BigDecimal value) {
    List<PromotionFullCutDiscountVO> discountVOS = this.getDiscounts();
    if (CollectionUtils.isNotEmpty(discountVOS)) {
      Iterator<PromotionFullCutDiscountVO> iterator = discountVOS.iterator();
      // 删除不满足条件的折扣
      while (iterator.hasNext()) {
        PromotionFullCutDiscount discount = iterator.next();
        @SuppressWarnings("unchecked")
        boolean isSatisfied = discount.isSatisfied(value);
        if (!isSatisfied) {
          iterator.remove();
        }
      }
      // 返回最大折扣
    }
    return CollectionUtils.isEmpty(discountVOS) ? null : Collections.max(discountVOS);
  }

  public void setDiscounts(List<PromotionFullCutDiscountVO> discounts) {
    this.discounts = discounts;
  }

}
