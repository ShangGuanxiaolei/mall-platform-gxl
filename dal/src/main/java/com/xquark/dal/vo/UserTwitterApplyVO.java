package com.xquark.dal.vo;


import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.status.TwitterStatus;

import java.math.BigDecimal;
import java.util.Date;

public class UserTwitterApplyVO extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String phone;

  private String name;

  private String status;

  private String userName;

  private String avatar;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


}
