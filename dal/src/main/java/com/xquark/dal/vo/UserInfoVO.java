package com.xquark.dal.vo;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.model.UserTeam;
import com.xquark.dal.model.UserTwitter;

public class UserInfoVO {

  private User user;

  private UserTwitter twitter;

  private UserPartner partner;

  private UserTeam team;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserTwitter getTwitter() {
    return twitter;
  }

  public void setTwitter(UserTwitter twitter) {
    this.twitter = twitter;
  }

  public UserPartner getPartner() {
    return partner;
  }

  public void setPartner(UserPartner partner) {
    this.partner = partner;
  }

  public UserTeam getTeam() {
    return team;
  }

  public void setTeam(UserTeam team) {
    this.team = team;
  }
}
