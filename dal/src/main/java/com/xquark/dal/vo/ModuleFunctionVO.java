package com.xquark.dal.vo;

import com.xquark.dal.model.ModuleFunction;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 作者: wangxh 创建日期: 17-4-7 简介:
 */
public class ModuleFunctionVO extends ModuleFunction {

  // Id对应xquark_module_function表的id
  private String id;
  private String functionId;
  private String moduleId;
  private String moduleName;
  private String moduleUrl;
  private String visRoles;
  private String visRolesDesc;
  private String avaRoles;
  private String avaRolesDesc;
  private Date updateAt;
  private Date createdAt;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getFunctionId() {
    return functionId;
  }

  @Override
  public void setFunctionId(String functionId) {
    this.functionId = functionId;
  }

  @Override
  public String getModuleId() {
    return moduleId;
  }

  @Override
  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public String getModuleUrl() {
    return moduleUrl;
  }

  public void setModuleUrl(String moduleUrl) {
    this.moduleUrl = moduleUrl;
  }

  public String getVisRoles() {
    return visRoles;
  }

  public String getVisRoles(String prefix) {
    if (this.visRoles == null) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    for (String role : this.visRoles.split(",")) {
      builder.append(prefix).append(role).append(",");
    }
    return StringUtils.substringBeforeLast(builder.toString(), ",");
  }

  public String getAvaRoles(String prefix) {
    if (this.avaRoles == null) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    for (String role : this.avaRoles.split(",")) {
      builder.append(prefix).append(role).append(",");
    }
    return StringUtils.substringBeforeLast(builder.toString(), ",");
  }

  public void setVisRoles(String visRoles) {
    this.visRoles = visRoles;
  }

  public String getAvaRoles() {
    return avaRoles;
  }

  public void setAvaRoles(String avaRoles) {
    this.avaRoles = avaRoles;
  }

  public String getVisRolesDesc() {
    return visRolesDesc;
  }

  public void setVisRolesDesc(String visRolesDesc) {
    this.visRolesDesc = visRolesDesc;
  }

  public String getAvaRolesDesc() {
    return avaRolesDesc;
  }

  public void setAvaRolesDesc(String avaRolesDesc) {
    this.avaRolesDesc = avaRolesDesc;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
}

