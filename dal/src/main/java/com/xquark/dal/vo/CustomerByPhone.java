package com.xquark.dal.vo;

import java.util.Date;

/**
 * User: kong Date: 18-7-19. Time: 下午4:29 根据电话查询到用户
 */
public class CustomerByPhone {

  //会员ID
  private String customerNumber;

  //微信号
  private String wechat;

  //姓名
  private String name;

  //身份证号
  private String tinCode;

  //级别
  private String level;

  //vivilife
  private String vivilevel;

  //手机号
  private String tel;

  //电话号
  private String phone;

  //上级会员Id
  private String firstId;

  //上级姓名
  private String firstName;

  //是否在册
  private Integer isActive;

  //加入时间
  private Date createdAt;

  //所属店铺
  private String storeId;

  //出生日期
  private Date birthday;

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTinCode() {
    return tinCode;
  }

  public void setTinCode(String tinCode) {
    this.tinCode = tinCode;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public String getTel() {
    return tel;
  }

  public void setTel(String tel) {
    this.tel = tel;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getFirstId() {
    return firstId;
  }

  public void setFirstId(String firstId) {
    this.firstId = firstId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Integer getIsActive() {
    return isActive;
  }

  public void setIsActive(Integer isActive) {
    this.isActive = isActive;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getVivilevel() {
    return vivilevel;
  }

  public void setVivilevel(String vivilevel) {
    this.vivilevel = vivilevel;
  }

  @Override
  public String toString() {
    return "getCustomerByPhone{" +
        "customerNumber='" + customerNumber + '\'' +
        ", wechat='" + wechat + '\'' +
        ", name='" + name + '\'' +
        ", tinCode='" + tinCode + '\'' +
        ", level='" + level + '\'' +
        ", tel='" + tel + '\'' +
        ", phone='" + phone + '\'' +
        ", firstId='" + firstId + '\'' +
        ", firstName='" + firstName + '\'' +
        ", isActive=" + isActive +
        ", createdAt=" + createdAt +
        ", storeId='" + storeId + '\'' +
        ", birthday=" + birthday +
        '}';
  }
}
