package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * Created by chh on 16-10-17.
 */
public class PromotionGrouponProductVO extends PromotionGrouponVO {

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;
  private String productId;
  private ProductStatus productStatus;

  private String siteHost = "http://nffclub.com";

  private String targetUrl;

  public String getTargetUrl() {
    targetUrl =
        siteHost + "/p/promotion/" + this.getProductId() + "?promotionId=" + super.getPromotionId();
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  @Override
  public String getProductId() {
    return productId;
  }

  @Override
  public void setProductId(String productId) {
    this.productId = productId;
  }

  public ProductStatus getProductStatus() {
    return productStatus;
  }

  public void setProductStatus(ProductStatus productStatus) {
    this.productStatus = productStatus;
  }

  public static void main(String[] args) {
    PromotionGrouponProductVO productVO = new PromotionGrouponProductVO();
    System.out.println(productVO.getTargetUrl());
  }
}
