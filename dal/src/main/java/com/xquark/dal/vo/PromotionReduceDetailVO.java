package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-10-10. DESC:
 */
public class PromotionReduceDetailVO extends Promotion {

  private BigDecimal discountFee; // 活动折扣金额

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }
}
