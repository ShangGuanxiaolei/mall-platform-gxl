package com.xquark.dal.vo;

public class TwitterChildRenShopVO {

  private static final long serialVersionUID = 1L;

  private String shopId;

  private String name;

  private Boolean archive;

  private String parentShopId;

  private String parentName;

  private Boolean parentArchive;

  public String getParentShopId() {
    return parentShopId;
  }

  public void setParentShopId(String parentShopId) {
    this.parentShopId = parentShopId;
  }

  public Boolean getParentArchive() {
    return parentArchive;
  }

  public void setParentArchive(Boolean parentArchive) {
    this.parentArchive = parentArchive;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getParentName() {
    return parentName;
  }

  public void setParentName(String parentName) {
    this.parentName = parentName;
  }
}
