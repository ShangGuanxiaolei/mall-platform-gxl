package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.Antifake;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.management.Agent;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

/**
 * Created by chh on 17/6/13.
 */
public class AntifakeVO extends Antifake {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String orderNo;

  private String sellerName;

  private String sellerPhone;

  private AgentType sellerType;

  private String buyerName;

  private String buyerPhone;

  private AgentType buyerType;

  private String userName;

  public AgentType getSellerType() {
    return sellerType;
  }

  public void setSellerType(AgentType sellerType) {
    this.sellerType = sellerType;
  }

  public AgentType getBuyerType() {
    return buyerType;
  }

  public void setBuyerType(AgentType buyerType) {
    this.buyerType = buyerType;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

}
