package com.xquark.dal.vo;

import com.xquark.dal.model.TeamApply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/19.
 */
public class TeamApplyVO extends TeamApply {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String teamName;

  private String parentName;

  private String parentPhone;

  public String getTeamName() {
    return teamName;
  }

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }

  public String getParentName() {
    return parentName;
  }

  public void setParentName(String parentName) {
    this.parentName = parentName;
  }

  public String getParentPhone() {
    return parentPhone;
  }

  public void setParentPhone(String parentPhone) {
    this.parentPhone = parentPhone;
  }
}
