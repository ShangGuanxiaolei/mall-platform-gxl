package com.xquark.dal.vo;

import com.xquark.dal.model.MerchantPermission;

import java.util.Date;
import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public class MerchantPermissionVo extends MerchantPermission {

  private boolean isCheck;

  private List<MerchantPermissionVo> merchantPermissionVos;

  public List<MerchantPermissionVo> getMerchantPermissionVos() {
    return merchantPermissionVos;
  }

  public void setMerchantPermissionVos(List<MerchantPermissionVo> merchantPermissionVos) {
    this.merchantPermissionVos = merchantPermissionVos;
  }

  public boolean isCheck() {
    return isCheck;
  }

  public void setCheck(boolean check) {
    isCheck = check;
  }
}
