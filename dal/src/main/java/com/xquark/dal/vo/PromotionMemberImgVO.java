package com.xquark.dal.vo;

public class PromotionMemberImgVO {
    private String headimgurl;
    private String piece_group_tran_code;
    private String piece_status;

    public String getPiece_group_tran_code() {
        return piece_group_tran_code;
    }

    public void setPiece_group_tran_code(String piece_group_tran_code) {
        this.piece_group_tran_code = piece_group_tran_code;
    }

    public String getPiece_status() {
        return piece_status;
    }

    public void setPiece_status(String piece_status) {
        this.piece_status = piece_status;
    }

    public String getHeadimgurl() {

        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }
}
