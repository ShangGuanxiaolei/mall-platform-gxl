package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.Team;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/05.
 */
public class TeamVO extends Team {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String userName;

  private String userPhone;

  private int teamNum;

  private int num;

  private String groupName;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String logoUrl;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public int getTeamNum() {
    return teamNum;
  }

  public void setTeamNum(int teamNum) {
    this.teamNum = teamNum;
  }

  public int getNum() {
    return num;
  }

  public void setNum(int num) {
    this.num = num;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }
}
