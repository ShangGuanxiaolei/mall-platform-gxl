package com.xquark.dal.vo;

import com.xquark.dal.model.Function;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by wangxinhua on 17-4-22. DESC:
 */
public class FunctionVO extends Function {

  private static final long serialVersionUID = -5491855884474557914L;

  private String id;
  private String name;
  private String moduleId;
  private String htmlId;

  private String moduleName;
  private String moduleUrl;
  private String visRoles;
  private String avaRoles;
  private String visRolesDesc;
  private String avaRolesDesc;

  private Date updatedAt;
  private Date createdAt;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getModuleId() {
    return moduleId;
  }

  @Override
  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  @Override
  public String getHtmlId() {
    return htmlId;
  }

  @Override
  public void setHtmlId(String htmlId) {
    this.htmlId = htmlId;
  }

  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public String getModuleUrl() {
    return moduleUrl;
  }

  public void setModuleUrl(String moduleUrl) {
    this.moduleUrl = moduleUrl;
  }

  public String getVisRoles() {
    return visRoles;
  }

  public String getVisRoles(String prefix) {
    if (StringUtils.isBlank(this.visRoles)) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    for (String role : visRoles.split(",")) {
      builder.append(prefix).append(role).append(",");
    }
    return StringUtils.substringBeforeLast(builder.toString(), ",");
  }

  public void setVisRoles(String visRoles) {
    this.visRoles = visRoles;
  }

  public String getAvaRoles() {
    return avaRoles;
  }

  public void setAvaRoles(String avaRoles) {
    this.avaRoles = avaRoles;
  }

  public String getAvaRoles(String prefix) {
    if (StringUtils.isBlank(this.avaRoles)) {
      return null;
    }
    StringBuilder builder = new StringBuilder();
    for (String role : avaRoles.split(",")) {
      builder.append(prefix).append(role).append(",");
    }
    return StringUtils.substringBeforeLast(builder.toString(), ",");
  }

  public String getVisRolesDesc() {
    return visRolesDesc;
  }

  public void setVisRolesDesc(String visRolesDesc) {
    this.visRolesDesc = visRolesDesc;
  }

  public String getAvaRolesDesc() {
    return avaRolesDesc;
  }

  public void setAvaRolesDesc(String avaRolesDesc) {
    this.avaRolesDesc = avaRolesDesc;
  }

  @Override
  public Date getUpdatedAt() {
    return updatedAt;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
}
