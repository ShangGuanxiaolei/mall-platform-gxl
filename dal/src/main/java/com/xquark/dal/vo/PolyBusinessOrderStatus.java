package com.xquark.dal.vo;

import com.xquark.dal.status.OrderStatus;
import java.util.HashMap;
import java.util.Map;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;

/**
 * @auther liuwei
 * @date 2018-06-28 14:13
 */
public enum PolyBusinessOrderStatus {
    JH_02(OrderStatus.PAID),//已支付
    JH_03(OrderStatus.SHIPPED),//已发货
    JH_04(OrderStatus.SUCCESS),//成功
    JH_05(OrderStatus.CLOSED),//已关闭
    JH_16(OrderStatus.REFUNDING ),//退款中
    JH_17(OrderStatus.SUBMITTED),//已下单
    JH_98(OrderStatus.CANCELLED);//已取消
   private static final Map<OrderStatus, PolyBusinessOrderStatus> STATUS_POLY_STATUS_MAP;

   static {
     STATUS_POLY_STATUS_MAP = new HashMap<>();
     PolyBusinessOrderStatus[] values = PolyBusinessOrderStatus.values();
     for (PolyBusinessOrderStatus polyBusinessOrderStatus : values) {
       STATUS_POLY_STATUS_MAP.put(polyBusinessOrderStatus.getOrderStatus(), polyBusinessOrderStatus);
     }
   }

   private final OrderStatus orderStatus;

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  PolyBusinessOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public static PolyBusinessOrderStatus getPolyStatusByOrderStatus(OrderStatus orderStatus) {
    return STATUS_POLY_STATUS_MAP.get(orderStatus);
  }
}
