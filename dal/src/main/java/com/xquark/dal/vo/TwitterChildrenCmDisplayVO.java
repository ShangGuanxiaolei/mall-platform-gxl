package com.xquark.dal.vo;


import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;
import java.util.Date;

public class TwitterChildrenCmDisplayVO extends BaseEntityImpl implements Archivable {

  private static final long serialVersionUID = 1L;

  private String phone;

  private BigDecimal cmFee;

  private BigDecimal unCmFee;

  private String name;

  private String status;

  private Boolean archive; // 是否是逻辑删除

  private Date createdAt;

  private String shopId;

  private String userId;

  private String levelId;

  private String levelName;

  private String twitterId;

  public String getTwitterId() {
    return twitterId;
  }

  public void setTwitterId(String twitterId) {
    this.twitterId = twitterId;
  }

  public String getLevelId() {
    return levelId;
  }

  public void setLevelId(String levelId) {
    this.levelId = levelId;
  }

  public String getLevelName() {
    return levelName;
  }

  public void setLevelName(String levelName) {
    this.levelName = levelName;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public BigDecimal getCmFee() {
    return cmFee;
  }

  public void setCmFee(BigDecimal cmFee) {
    this.cmFee = cmFee;
  }

  public BigDecimal getUnCmFee() {
    return unCmFee;
  }

  public void setUnCmFee(BigDecimal unCmFee) {
    this.unCmFee = unCmFee;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}
