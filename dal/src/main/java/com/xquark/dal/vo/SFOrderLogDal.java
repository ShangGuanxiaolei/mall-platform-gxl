package com.xquark.dal.vo;

import java.util.Date;

/**
 * User: kong Date: 18-7-24. Time: 下午2:23 顺丰订单路由
 */
public class SFOrderLogDal {

  //操作时间
  private Date time;

  //物流信息
  private String commont;

  public String getCommont() {
    return commont;
  }

  public void setCommont(String commont) {
    this.commont = commont;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}
