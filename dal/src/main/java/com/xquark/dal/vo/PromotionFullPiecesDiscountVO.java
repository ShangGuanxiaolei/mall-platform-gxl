package com.xquark.dal.vo;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.PromotionFullPiecesDiscount;

import java.util.List;

/**
 * Created by wangxinhua on 17-9-28. DESC:
 */
public class PromotionFullPiecesDiscountVO extends PromotionFullPiecesDiscount<Integer> {

  private List<Product> giftProducts;

  private String productNames;

  public List<Product> getGiftProducts() {
    return giftProducts;
  }

  public void setGiftProducts(List<Product> giftProducts) {
    this.giftProducts = giftProducts;
  }

  public String getProductNames() {
    return productNames;
  }

  public void setProductNames(String productNames) {
    this.productNames = productNames;
  }
}
