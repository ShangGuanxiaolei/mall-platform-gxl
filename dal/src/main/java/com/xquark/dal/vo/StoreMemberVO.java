package com.xquark.dal.vo;

import com.xquark.dal.model.StoreMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/17.
 */
public class StoreMemberVO extends StoreMember {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String parentName;

  private String parentPhone;

  public String getParentName() {
    return parentName;
  }

  public void setParentName(String parentName) {
    this.parentName = parentName;
  }

  public String getParentPhone() {
    return parentPhone;
  }

  public void setParentPhone(String parentPhone) {
    this.parentPhone = parentPhone;
  }
}
