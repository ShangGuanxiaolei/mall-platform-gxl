package com.xquark.dal.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.WithdrawApplyStatus;

public class WithdrawAdmin extends WithdrawApply {

  private static final long serialVersionUID = 1L;

  private String phone;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getIdLong() {
    try {
      return IdTypeHandler.decode(this.getId()) + "";
    } catch (Exception e) {
      return this.getId();
    }
  }

  public String getTypeStr() {
    int type = getType();
    if (type == 1) {
      return "银行卡";
    } else if (type == 2) {
      return "支付宝";
    } else {
      return "";
    }
  }

  public String getCreatedAtStr() {
    Date date = this.getCreatedAt();
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
          "yyyy-MM-dd HH:mm:ss");
      return simpleDateFormat.format(date);
    }
  }

  public String getStatusStr() {
    WithdrawApplyStatus status = this.getStatus();
    String str = "";
    if (status == WithdrawApplyStatus.NEW) {
      str = "待审核";
    } else if (status == WithdrawApplyStatus.CLOSE) {
      str = "已关闭";
    } else if (status == WithdrawApplyStatus.FAILED) {
      str = "支付失败";
    } else if (status == WithdrawApplyStatus.PENDING) {
      str = "支付中";
    } else if (status == WithdrawApplyStatus.SUCCESS) {
      str = "已支付";
    }
    return str;
  }

  public String getUpdatedAtStr() {
    Date date = this.getUpdatedAt();
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
          "yyyy-MM-dd HH:mm:ss");
      return simpleDateFormat.format(date);
    }
  }

  public String getPayAtStr() {
    Date date = this.getPayAt();
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
          "yyyy-MM-dd HH:mm:ss");
      return simpleDateFormat.format(date);
    }
  }
}
