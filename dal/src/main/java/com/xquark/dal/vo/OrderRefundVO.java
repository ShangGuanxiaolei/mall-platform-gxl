package com.xquark.dal.vo;

import java.util.ArrayList;
import java.util.List;

import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.status.OrderStatus;

public class OrderRefundVO extends OrderRefund {

  private static final long serialVersionUID = 1L;

  private String orderStatusName;

  private String refundReasonName;  //退款原因

  //    @JsonSerialize(using = JsonOrderRefundStatusSerializer.class)
  private String statusName;

  private String orderNo;

  private String orderFee;

  private String buyerPhone;

  List<String> refuseEvidences;  //拒绝凭证, 图片链接地址

  List<OrderRefundOperateDetail> opDetails; //操作记录

  public String getStatusName() {
    String statusName = null;
    switch (super.getStatus()) {
      case SUBMITTED:
        statusName = "买家已提交申请";
        break;
      case CANCELLED:
        statusName = "申请已取消";
        break;
      case AGREE_RETURN:
        statusName = "卖家同意退款";
        break;
      case REJECT_REFUND:
        statusName = "拒绝买家退款";
        break;
      case REJECT_RETURN:
        statusName = "拒绝买家退货";
        break;
      case RETURN_ING:
        statusName = "退货中";
        break;
      case SUCCESS:
        statusName = "退款成功";
        break;
      default:
        statusName = "未知";
        break;
    }
    this.statusName = statusName;
    return this.statusName;
  }

  public void setStatusName(String statusName) {
    this.statusName = statusName;
  }

  public String getOrderStatusName() {

    String orderStatusName = null;
    switch (super.getOrderStatus()) {
      case PAID:
        orderStatusName = "已付款";
        break;
      case SHIPPED:
        orderStatusName = "已发货";
        break;
      case REFUNDING:
        orderStatusName = "退款申请中";
        break;
      case CLOSED:
        orderStatusName = "交易关闭";
        break;
      default:
        statusName = "未知";
        break;
    }
    this.orderStatusName = orderStatusName;

    return this.orderStatusName;
  }

  public String getRefundReasonName() {
    this.refundReasonName = loadRefundReasonNameByCode(this.getRefundReason());
    return this.refundReasonName;
  }

  public void setRefundReasonName(String refundReasonName) {
    this.refundReasonName = refundReasonName;
  }

  public void setOrderStatusName(String orderStatusName) {
    this.orderStatusName = orderStatusName;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getOrderFee() {
    return orderFee;
  }

  public void setOrderFee(String orderFee) {
    this.orderFee = orderFee;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public List<String> getRefuseEvidences() {
    return refuseEvidences;
  }

  public void setRefuseEvidences(List<String> refuseEvidences) {
    this.refuseEvidences = refuseEvidences;
  }

  public List<OrderRefundOperateDetail> getOpDetails() {
    return opDetails;
  }

  public void setOpDetails(List<OrderRefundOperateDetail> opDetails) {
    this.opDetails = opDetails;
  }

  public String loadRefundReasonNameByCode(String reason) {
    List<PropertyDict> reasons = loadRefundReasonDict(null);
    String reasonName = null;
    for (PropertyDict dict : reasons) {
      if (dict.getKey().equals(reason)) {
        reasonName = dict.getValue();
        break;
      }
    }
    return reasonName;
  }

  public static List<PropertyDict> loadRefundReasonDict(OrderStatus orderStatus) {
    List<PropertyDict> reasons = new ArrayList<PropertyDict>();
    if (orderStatus == OrderStatus.PAID || orderStatus == null) {
      reasons.add(new PropertyDict(null, "1", "缺货", null));
      reasons.add(new PropertyDict(null, "2", "协商一致退款", null));
      reasons.add(new PropertyDict(null, "3", "未按约定时间发货", null));
      reasons.add(new PropertyDict(null, "4", "拍错/多拍/不想要", null));
      reasons.add(new PropertyDict(null, "5", "其他", null));
    }

    if (orderStatus == OrderStatus.SHIPPED || orderStatus == null) {
      reasons.add(new PropertyDict(null, "101", "虚假发货", null));
      reasons.add(new PropertyDict(null, "102", "快递问题", null));
      reasons.add(new PropertyDict(null, "103", "空包裹/少货", null));
      reasons.add(new PropertyDict(null, "104", "未按约定时间发货", null));
      reasons.add(new PropertyDict(null, "105", "卖家发错货", null));
      reasons.add(new PropertyDict(null, "106", "拍错/多拍/不想要", null));
      reasons.add(new PropertyDict(null, "107", "其他", null));
    }

    return reasons;
  }

}
