package com.xquark.dal.vo;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Map;

public class MainOrderVO extends MainOrder {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "明细订单VO")
  private List<OrderVO> orders;

  public MainOrderVO(MainOrder mainOrder, List<OrderVO> orders) {
    BeanUtils.copyProperties(mainOrder, this);
    this.orders = orders;
  }

  public List<OrderVO> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderVO> orders) {
    this.orders = orders;
  }
}
