package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class XquarkProductExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public XquarkProductExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andEncodeIsNull() {
            addCriterion("encode is null");
            return (Criteria) this;
        }

        public Criteria andEncodeIsNotNull() {
            addCriterion("encode is not null");
            return (Criteria) this;
        }

        public Criteria andEncodeEqualTo(String value) {
            addCriterion("encode =", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeNotEqualTo(String value) {
            addCriterion("encode <>", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeGreaterThan(String value) {
            addCriterion("encode >", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeGreaterThanOrEqualTo(String value) {
            addCriterion("encode >=", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeLessThan(String value) {
            addCriterion("encode <", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeLessThanOrEqualTo(String value) {
            addCriterion("encode <=", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeLike(String value) {
            addCriterion("encode like", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeNotLike(String value) {
            addCriterion("encode not like", value, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeIn(List<String> values) {
            addCriterion("encode in", values, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeNotIn(List<String> values) {
            addCriterion("encode not in", values, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeBetween(String value1, String value2) {
            addCriterion("encode between", value1, value2, "encode");
            return (Criteria) this;
        }

        public Criteria andEncodeNotBetween(String value1, String value2) {
            addCriterion("encode not between", value1, value2, "encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeIsNull() {
            addCriterion("u8_encode is null");
            return (Criteria) this;
        }

        public Criteria andU8EncodeIsNotNull() {
            addCriterion("u8_encode is not null");
            return (Criteria) this;
        }

        public Criteria andU8EncodeEqualTo(String value) {
            addCriterion("u8_encode =", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeNotEqualTo(String value) {
            addCriterion("u8_encode <>", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeGreaterThan(String value) {
            addCriterion("u8_encode >", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeGreaterThanOrEqualTo(String value) {
            addCriterion("u8_encode >=", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeLessThan(String value) {
            addCriterion("u8_encode <", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeLessThanOrEqualTo(String value) {
            addCriterion("u8_encode <=", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeLike(String value) {
            addCriterion("u8_encode like", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeNotLike(String value) {
            addCriterion("u8_encode not like", value, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeIn(List<String> values) {
            addCriterion("u8_encode in", values, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeNotIn(List<String> values) {
            addCriterion("u8_encode not in", values, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeBetween(String value1, String value2) {
            addCriterion("u8_encode between", value1, value2, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andU8EncodeNotBetween(String value1, String value2) {
            addCriterion("u8_encode not between", value1, value2, "u8Encode");
            return (Criteria) this;
        }

        public Criteria andModelIsNull() {
            addCriterion("model is null");
            return (Criteria) this;
        }

        public Criteria andModelIsNotNull() {
            addCriterion("model is not null");
            return (Criteria) this;
        }

        public Criteria andModelEqualTo(String value) {
            addCriterion("model =", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotEqualTo(String value) {
            addCriterion("model <>", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThan(String value) {
            addCriterion("model >", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelGreaterThanOrEqualTo(String value) {
            addCriterion("model >=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThan(String value) {
            addCriterion("model <", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLessThanOrEqualTo(String value) {
            addCriterion("model <=", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelLike(String value) {
            addCriterion("model like", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotLike(String value) {
            addCriterion("model not like", value, "model");
            return (Criteria) this;
        }

        public Criteria andModelIn(List<String> values) {
            addCriterion("model in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotIn(List<String> values) {
            addCriterion("model not in", values, "model");
            return (Criteria) this;
        }

        public Criteria andModelBetween(String value1, String value2) {
            addCriterion("model between", value1, value2, "model");
            return (Criteria) this;
        }

        public Criteria andModelNotBetween(String value1, String value2) {
            addCriterion("model not between", value1, value2, "model");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Integer value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Integer value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Integer value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Integer value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Integer value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Integer> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Integer> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Integer value1, Integer value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNull() {
            addCriterion("priority is null");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNotNull() {
            addCriterion("priority is not null");
            return (Criteria) this;
        }

        public Criteria andPriorityEqualTo(Integer value) {
            addCriterion("priority =", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotEqualTo(Integer value) {
            addCriterion("priority <>", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThan(Integer value) {
            addCriterion("priority >", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThanOrEqualTo(Integer value) {
            addCriterion("priority >=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThan(Integer value) {
            addCriterion("priority <", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThanOrEqualTo(Integer value) {
            addCriterion("priority <=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityIn(List<Integer> values) {
            addCriterion("priority in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotIn(List<Integer> values) {
            addCriterion("priority not in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityBetween(Integer value1, Integer value2) {
            addCriterion("priority between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotBetween(Integer value1, Integer value2) {
            addCriterion("priority not between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andFilterSpecIsNull() {
            addCriterion("filter_spec is null");
            return (Criteria) this;
        }

        public Criteria andFilterSpecIsNotNull() {
            addCriterion("filter_spec is not null");
            return (Criteria) this;
        }

        public Criteria andFilterSpecEqualTo(String value) {
            addCriterion("filter_spec =", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecNotEqualTo(String value) {
            addCriterion("filter_spec <>", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecGreaterThan(String value) {
            addCriterion("filter_spec >", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecGreaterThanOrEqualTo(String value) {
            addCriterion("filter_spec >=", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecLessThan(String value) {
            addCriterion("filter_spec <", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecLessThanOrEqualTo(String value) {
            addCriterion("filter_spec <=", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecLike(String value) {
            addCriterion("filter_spec like", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecNotLike(String value) {
            addCriterion("filter_spec not like", value, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecIn(List<String> values) {
            addCriterion("filter_spec in", values, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecNotIn(List<String> values) {
            addCriterion("filter_spec not in", values, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecBetween(String value1, String value2) {
            addCriterion("filter_spec between", value1, value2, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andFilterSpecNotBetween(String value1, String value2) {
            addCriterion("filter_spec not between", value1, value2, "filterSpec");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Long value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Long value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Long value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Long value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Long value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Long value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Long> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Long> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Long value1, Long value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Long value1, Long value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andShopIdIsNull() {
            addCriterion("shop_id is null");
            return (Criteria) this;
        }

        public Criteria andShopIdIsNotNull() {
            addCriterion("shop_id is not null");
            return (Criteria) this;
        }

        public Criteria andShopIdEqualTo(Long value) {
            addCriterion("shop_id =", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdNotEqualTo(Long value) {
            addCriterion("shop_id <>", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdGreaterThan(Long value) {
            addCriterion("shop_id >", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdGreaterThanOrEqualTo(Long value) {
            addCriterion("shop_id >=", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdLessThan(Long value) {
            addCriterion("shop_id <", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdLessThanOrEqualTo(Long value) {
            addCriterion("shop_id <=", value, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdIn(List<Long> values) {
            addCriterion("shop_id in", values, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdNotIn(List<Long> values) {
            addCriterion("shop_id not in", values, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdBetween(Long value1, Long value2) {
            addCriterion("shop_id between", value1, value2, "shopId");
            return (Criteria) this;
        }

        public Criteria andShopIdNotBetween(Long value1, Long value2) {
            addCriterion("shop_id not between", value1, value2, "shopId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andImgIsNull() {
            addCriterion("img is null");
            return (Criteria) this;
        }

        public Criteria andImgIsNotNull() {
            addCriterion("img is not null");
            return (Criteria) this;
        }

        public Criteria andImgEqualTo(String value) {
            addCriterion("img =", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotEqualTo(String value) {
            addCriterion("img <>", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThan(String value) {
            addCriterion("img >", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgGreaterThanOrEqualTo(String value) {
            addCriterion("img >=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThan(String value) {
            addCriterion("img <", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLessThanOrEqualTo(String value) {
            addCriterion("img <=", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgLike(String value) {
            addCriterion("img like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotLike(String value) {
            addCriterion("img not like", value, "img");
            return (Criteria) this;
        }

        public Criteria andImgIn(List<String> values) {
            addCriterion("img in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotIn(List<String> values) {
            addCriterion("img not in", values, "img");
            return (Criteria) this;
        }

        public Criteria andImgBetween(String value1, String value2) {
            addCriterion("img between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andImgNotBetween(String value1, String value2) {
            addCriterion("img not between", value1, value2, "img");
            return (Criteria) this;
        }

        public Criteria andImgWidthIsNull() {
            addCriterion("img_width is null");
            return (Criteria) this;
        }

        public Criteria andImgWidthIsNotNull() {
            addCriterion("img_width is not null");
            return (Criteria) this;
        }

        public Criteria andImgWidthEqualTo(Integer value) {
            addCriterion("img_width =", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthNotEqualTo(Integer value) {
            addCriterion("img_width <>", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthGreaterThan(Integer value) {
            addCriterion("img_width >", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthGreaterThanOrEqualTo(Integer value) {
            addCriterion("img_width >=", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthLessThan(Integer value) {
            addCriterion("img_width <", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthLessThanOrEqualTo(Integer value) {
            addCriterion("img_width <=", value, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthIn(List<Integer> values) {
            addCriterion("img_width in", values, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthNotIn(List<Integer> values) {
            addCriterion("img_width not in", values, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthBetween(Integer value1, Integer value2) {
            addCriterion("img_width between", value1, value2, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgWidthNotBetween(Integer value1, Integer value2) {
            addCriterion("img_width not between", value1, value2, "imgWidth");
            return (Criteria) this;
        }

        public Criteria andImgHeightIsNull() {
            addCriterion("img_height is null");
            return (Criteria) this;
        }

        public Criteria andImgHeightIsNotNull() {
            addCriterion("img_height is not null");
            return (Criteria) this;
        }

        public Criteria andImgHeightEqualTo(Integer value) {
            addCriterion("img_height =", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightNotEqualTo(Integer value) {
            addCriterion("img_height <>", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightGreaterThan(Integer value) {
            addCriterion("img_height >", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("img_height >=", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightLessThan(Integer value) {
            addCriterion("img_height <", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightLessThanOrEqualTo(Integer value) {
            addCriterion("img_height <=", value, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightIn(List<Integer> values) {
            addCriterion("img_height in", values, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightNotIn(List<Integer> values) {
            addCriterion("img_height not in", values, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightBetween(Integer value1, Integer value2) {
            addCriterion("img_height between", value1, value2, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andImgHeightNotBetween(Integer value1, Integer value2) {
            addCriterion("img_height not between", value1, value2, "imgHeight");
            return (Criteria) this;
        }

        public Criteria andDescImgIsNull() {
            addCriterion("desc_img is null");
            return (Criteria) this;
        }

        public Criteria andDescImgIsNotNull() {
            addCriterion("desc_img is not null");
            return (Criteria) this;
        }

        public Criteria andDescImgEqualTo(String value) {
            addCriterion("desc_img =", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgNotEqualTo(String value) {
            addCriterion("desc_img <>", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgGreaterThan(String value) {
            addCriterion("desc_img >", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgGreaterThanOrEqualTo(String value) {
            addCriterion("desc_img >=", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgLessThan(String value) {
            addCriterion("desc_img <", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgLessThanOrEqualTo(String value) {
            addCriterion("desc_img <=", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgLike(String value) {
            addCriterion("desc_img like", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgNotLike(String value) {
            addCriterion("desc_img not like", value, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgIn(List<String> values) {
            addCriterion("desc_img in", values, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgNotIn(List<String> values) {
            addCriterion("desc_img not in", values, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgBetween(String value1, String value2) {
            addCriterion("desc_img between", value1, value2, "descImg");
            return (Criteria) this;
        }

        public Criteria andDescImgNotBetween(String value1, String value2) {
            addCriterion("desc_img not between", value1, value2, "descImg");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIsNull() {
            addCriterion("market_price is null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIsNotNull() {
            addCriterion("market_price is not null");
            return (Criteria) this;
        }

        public Criteria andMarketPriceEqualTo(BigDecimal value) {
            addCriterion("market_price =", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotEqualTo(BigDecimal value) {
            addCriterion("market_price <>", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceGreaterThan(BigDecimal value) {
            addCriterion("market_price >", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price >=", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceLessThan(BigDecimal value) {
            addCriterion("market_price <", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("market_price <=", value, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceIn(List<BigDecimal> values) {
            addCriterion("market_price in", values, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotIn(List<BigDecimal> values) {
            addCriterion("market_price not in", values, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price between", value1, value2, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andMarketPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("market_price not between", value1, value2, "marketPrice");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNull() {
            addCriterion("original_price is null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIsNotNull() {
            addCriterion("original_price is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceEqualTo(BigDecimal value) {
            addCriterion("original_price =", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotEqualTo(BigDecimal value) {
            addCriterion("original_price <>", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThan(BigDecimal value) {
            addCriterion("original_price >", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price >=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThan(BigDecimal value) {
            addCriterion("original_price <", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("original_price <=", value, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceIn(List<BigDecimal> values) {
            addCriterion("original_price in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotIn(List<BigDecimal> values) {
            addCriterion("original_price not in", values, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_price not between", value1, value2, "originalPrice");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(Long value) {
            addCriterion("amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(Long value) {
            addCriterion("amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(Long value) {
            addCriterion("amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(Long value) {
            addCriterion("amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(Long value) {
            addCriterion("amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(Long value) {
            addCriterion("amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<Long> values) {
            addCriterion("amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<Long> values) {
            addCriterion("amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(Long value1, Long value2) {
            addCriterion("amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(Long value1, Long value2) {
            addCriterion("amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andSalesIsNull() {
            addCriterion("sales is null");
            return (Criteria) this;
        }

        public Criteria andSalesIsNotNull() {
            addCriterion("sales is not null");
            return (Criteria) this;
        }

        public Criteria andSalesEqualTo(Long value) {
            addCriterion("sales =", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotEqualTo(Long value) {
            addCriterion("sales <>", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesGreaterThan(Long value) {
            addCriterion("sales >", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesGreaterThanOrEqualTo(Long value) {
            addCriterion("sales >=", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesLessThan(Long value) {
            addCriterion("sales <", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesLessThanOrEqualTo(Long value) {
            addCriterion("sales <=", value, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesIn(List<Long> values) {
            addCriterion("sales in", values, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotIn(List<Long> values) {
            addCriterion("sales not in", values, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesBetween(Long value1, Long value2) {
            addCriterion("sales between", value1, value2, "sales");
            return (Criteria) this;
        }

        public Criteria andSalesNotBetween(Long value1, Long value2) {
            addCriterion("sales not between", value1, value2, "sales");
            return (Criteria) this;
        }

        public Criteria andArchiveIsNull() {
            addCriterion("archive is null");
            return (Criteria) this;
        }

        public Criteria andArchiveIsNotNull() {
            addCriterion("archive is not null");
            return (Criteria) this;
        }

        public Criteria andArchiveEqualTo(Boolean value) {
            addCriterion("archive =", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveNotEqualTo(Boolean value) {
            addCriterion("archive <>", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveGreaterThan(Boolean value) {
            addCriterion("archive >", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveGreaterThanOrEqualTo(Boolean value) {
            addCriterion("archive >=", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveLessThan(Boolean value) {
            addCriterion("archive <", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveLessThanOrEqualTo(Boolean value) {
            addCriterion("archive <=", value, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveIn(List<Boolean> values) {
            addCriterion("archive in", values, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveNotIn(List<Boolean> values) {
            addCriterion("archive not in", values, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveBetween(Boolean value1, Boolean value2) {
            addCriterion("archive between", value1, value2, "archive");
            return (Criteria) this;
        }

        public Criteria andArchiveNotBetween(Boolean value1, Boolean value2) {
            addCriterion("archive not between", value1, value2, "archive");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNull() {
            addCriterion("recommend is null");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNotNull() {
            addCriterion("recommend is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendEqualTo(Byte value) {
            addCriterion("recommend =", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotEqualTo(Byte value) {
            addCriterion("recommend <>", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThan(Byte value) {
            addCriterion("recommend >", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThanOrEqualTo(Byte value) {
            addCriterion("recommend >=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThan(Byte value) {
            addCriterion("recommend <", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThanOrEqualTo(Byte value) {
            addCriterion("recommend <=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendIn(List<Byte> values) {
            addCriterion("recommend in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotIn(List<Byte> values) {
            addCriterion("recommend not in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendBetween(Byte value1, Byte value2) {
            addCriterion("recommend between", value1, value2, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotBetween(Byte value1, Byte value2) {
            addCriterion("recommend not between", value1, value2, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendAtIsNull() {
            addCriterion("recommend_at is null");
            return (Criteria) this;
        }

        public Criteria andRecommendAtIsNotNull() {
            addCriterion("recommend_at is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendAtEqualTo(Date value) {
            addCriterion("recommend_at =", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtNotEqualTo(Date value) {
            addCriterion("recommend_at <>", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtGreaterThan(Date value) {
            addCriterion("recommend_at >", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtGreaterThanOrEqualTo(Date value) {
            addCriterion("recommend_at >=", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtLessThan(Date value) {
            addCriterion("recommend_at <", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtLessThanOrEqualTo(Date value) {
            addCriterion("recommend_at <=", value, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtIn(List<Date> values) {
            addCriterion("recommend_at in", values, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtNotIn(List<Date> values) {
            addCriterion("recommend_at not in", values, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtBetween(Date value1, Date value2) {
            addCriterion("recommend_at between", value1, value2, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andRecommendAtNotBetween(Date value1, Date value2) {
            addCriterion("recommend_at not between", value1, value2, "recommendAt");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNull() {
            addCriterion("discount is null");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNotNull() {
            addCriterion("discount is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountEqualTo(BigDecimal value) {
            addCriterion("discount =", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotEqualTo(BigDecimal value) {
            addCriterion("discount <>", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThan(BigDecimal value) {
            addCriterion("discount >", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("discount >=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThan(BigDecimal value) {
            addCriterion("discount <", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("discount <=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountIn(List<BigDecimal> values) {
            addCriterion("discount in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotIn(List<BigDecimal> values) {
            addCriterion("discount not in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("discount between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("discount not between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andIsCommissionIsNull() {
            addCriterion("is_commission is null");
            return (Criteria) this;
        }

        public Criteria andIsCommissionIsNotNull() {
            addCriterion("is_commission is not null");
            return (Criteria) this;
        }

        public Criteria andIsCommissionEqualTo(Boolean value) {
            addCriterion("is_commission =", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionNotEqualTo(Boolean value) {
            addCriterion("is_commission <>", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionGreaterThan(Boolean value) {
            addCriterion("is_commission >", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_commission >=", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionLessThan(Boolean value) {
            addCriterion("is_commission <", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionLessThanOrEqualTo(Boolean value) {
            addCriterion("is_commission <=", value, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionIn(List<Boolean> values) {
            addCriterion("is_commission in", values, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionNotIn(List<Boolean> values) {
            addCriterion("is_commission not in", values, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionBetween(Boolean value1, Boolean value2) {
            addCriterion("is_commission between", value1, value2, "isCommission");
            return (Criteria) this;
        }

        public Criteria andIsCommissionNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_commission not between", value1, value2, "isCommission");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIsNull() {
            addCriterion("commission_rate is null");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIsNotNull() {
            addCriterion("commission_rate is not null");
            return (Criteria) this;
        }

        public Criteria andCommissionRateEqualTo(BigDecimal value) {
            addCriterion("commission_rate =", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotEqualTo(BigDecimal value) {
            addCriterion("commission_rate <>", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateGreaterThan(BigDecimal value) {
            addCriterion("commission_rate >", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commission_rate >=", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateLessThan(BigDecimal value) {
            addCriterion("commission_rate <", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commission_rate <=", value, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateIn(List<BigDecimal> values) {
            addCriterion("commission_rate in", values, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotIn(List<BigDecimal> values) {
            addCriterion("commission_rate not in", values, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commission_rate between", value1, value2, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andCommissionRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commission_rate not between", value1, value2, "commissionRate");
            return (Criteria) this;
        }

        public Criteria andForsaleAtIsNull() {
            addCriterion("forsale_at is null");
            return (Criteria) this;
        }

        public Criteria andForsaleAtIsNotNull() {
            addCriterion("forsale_at is not null");
            return (Criteria) this;
        }

        public Criteria andForsaleAtEqualTo(Date value) {
            addCriterion("forsale_at =", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtNotEqualTo(Date value) {
            addCriterion("forsale_at <>", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtGreaterThan(Date value) {
            addCriterion("forsale_at >", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtGreaterThanOrEqualTo(Date value) {
            addCriterion("forsale_at >=", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtLessThan(Date value) {
            addCriterion("forsale_at <", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtLessThanOrEqualTo(Date value) {
            addCriterion("forsale_at <=", value, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtIn(List<Date> values) {
            addCriterion("forsale_at in", values, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtNotIn(List<Date> values) {
            addCriterion("forsale_at not in", values, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtBetween(Date value1, Date value2) {
            addCriterion("forsale_at between", value1, value2, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andForsaleAtNotBetween(Date value1, Date value2) {
            addCriterion("forsale_at not between", value1, value2, "forsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtIsNull() {
            addCriterion("onsale_at is null");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtIsNotNull() {
            addCriterion("onsale_at is not null");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtEqualTo(Date value) {
            addCriterion("onsale_at =", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtNotEqualTo(Date value) {
            addCriterion("onsale_at <>", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtGreaterThan(Date value) {
            addCriterion("onsale_at >", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtGreaterThanOrEqualTo(Date value) {
            addCriterion("onsale_at >=", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtLessThan(Date value) {
            addCriterion("onsale_at <", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtLessThanOrEqualTo(Date value) {
            addCriterion("onsale_at <=", value, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtIn(List<Date> values) {
            addCriterion("onsale_at in", values, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtNotIn(List<Date> values) {
            addCriterion("onsale_at not in", values, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtBetween(Date value1, Date value2) {
            addCriterion("onsale_at between", value1, value2, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andOnsaleAtNotBetween(Date value1, Date value2) {
            addCriterion("onsale_at not between", value1, value2, "onsaleAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtIsNull() {
            addCriterion("instock_at is null");
            return (Criteria) this;
        }

        public Criteria andInstockAtIsNotNull() {
            addCriterion("instock_at is not null");
            return (Criteria) this;
        }

        public Criteria andInstockAtEqualTo(Date value) {
            addCriterion("instock_at =", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtNotEqualTo(Date value) {
            addCriterion("instock_at <>", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtGreaterThan(Date value) {
            addCriterion("instock_at >", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtGreaterThanOrEqualTo(Date value) {
            addCriterion("instock_at >=", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtLessThan(Date value) {
            addCriterion("instock_at <", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtLessThanOrEqualTo(Date value) {
            addCriterion("instock_at <=", value, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtIn(List<Date> values) {
            addCriterion("instock_at in", values, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtNotIn(List<Date> values) {
            addCriterion("instock_at not in", values, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtBetween(Date value1, Date value2) {
            addCriterion("instock_at between", value1, value2, "instockAt");
            return (Criteria) this;
        }

        public Criteria andInstockAtNotBetween(Date value1, Date value2) {
            addCriterion("instock_at not between", value1, value2, "instockAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("created_at is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("created_at is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Date value) {
            addCriterion("created_at =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Date value) {
            addCriterion("created_at <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Date value) {
            addCriterion("created_at >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("created_at >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Date value) {
            addCriterion("created_at <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Date value) {
            addCriterion("created_at <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Date> values) {
            addCriterion("created_at in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Date> values) {
            addCriterion("created_at not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Date value1, Date value2) {
            addCriterion("created_at between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Date value1, Date value2) {
            addCriterion("created_at not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("updated_at is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("updated_at is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Date value) {
            addCriterion("updated_at =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Date value) {
            addCriterion("updated_at <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Date value) {
            addCriterion("updated_at >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Date value) {
            addCriterion("updated_at >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Date value) {
            addCriterion("updated_at <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Date value) {
            addCriterion("updated_at <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Date> values) {
            addCriterion("updated_at in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Date> values) {
            addCriterion("updated_at not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Date value1, Date value2) {
            addCriterion("updated_at between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Date value1, Date value2) {
            addCriterion("updated_at not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andFakeSalesIsNull() {
            addCriterion("fake_sales is null");
            return (Criteria) this;
        }

        public Criteria andFakeSalesIsNotNull() {
            addCriterion("fake_sales is not null");
            return (Criteria) this;
        }

        public Criteria andFakeSalesEqualTo(Long value) {
            addCriterion("fake_sales =", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesNotEqualTo(Long value) {
            addCriterion("fake_sales <>", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesGreaterThan(Long value) {
            addCriterion("fake_sales >", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesGreaterThanOrEqualTo(Long value) {
            addCriterion("fake_sales >=", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesLessThan(Long value) {
            addCriterion("fake_sales <", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesLessThanOrEqualTo(Long value) {
            addCriterion("fake_sales <=", value, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesIn(List<Long> values) {
            addCriterion("fake_sales in", values, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesNotIn(List<Long> values) {
            addCriterion("fake_sales not in", values, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesBetween(Long value1, Long value2) {
            addCriterion("fake_sales between", value1, value2, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andFakeSalesNotBetween(Long value1, Long value2) {
            addCriterion("fake_sales not between", value1, value2, "fakeSales");
            return (Criteria) this;
        }

        public Criteria andIsdelayIsNull() {
            addCriterion("isDelay is null");
            return (Criteria) this;
        }

        public Criteria andIsdelayIsNotNull() {
            addCriterion("isDelay is not null");
            return (Criteria) this;
        }

        public Criteria andIsdelayEqualTo(Byte value) {
            addCriterion("isDelay =", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayNotEqualTo(Byte value) {
            addCriterion("isDelay <>", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayGreaterThan(Byte value) {
            addCriterion("isDelay >", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayGreaterThanOrEqualTo(Byte value) {
            addCriterion("isDelay >=", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayLessThan(Byte value) {
            addCriterion("isDelay <", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayLessThanOrEqualTo(Byte value) {
            addCriterion("isDelay <=", value, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayIn(List<Byte> values) {
            addCriterion("isDelay in", values, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayNotIn(List<Byte> values) {
            addCriterion("isDelay not in", values, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayBetween(Byte value1, Byte value2) {
            addCriterion("isDelay between", value1, value2, "isdelay");
            return (Criteria) this;
        }

        public Criteria andIsdelayNotBetween(Byte value1, Byte value2) {
            addCriterion("isDelay not between", value1, value2, "isdelay");
            return (Criteria) this;
        }

        public Criteria andDelaydaysIsNull() {
            addCriterion("delayDays is null");
            return (Criteria) this;
        }

        public Criteria andDelaydaysIsNotNull() {
            addCriterion("delayDays is not null");
            return (Criteria) this;
        }

        public Criteria andDelaydaysEqualTo(Integer value) {
            addCriterion("delayDays =", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysNotEqualTo(Integer value) {
            addCriterion("delayDays <>", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysGreaterThan(Integer value) {
            addCriterion("delayDays >", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysGreaterThanOrEqualTo(Integer value) {
            addCriterion("delayDays >=", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysLessThan(Integer value) {
            addCriterion("delayDays <", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysLessThanOrEqualTo(Integer value) {
            addCriterion("delayDays <=", value, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysIn(List<Integer> values) {
            addCriterion("delayDays in", values, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysNotIn(List<Integer> values) {
            addCriterion("delayDays not in", values, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysBetween(Integer value1, Integer value2) {
            addCriterion("delayDays between", value1, value2, "delaydays");
            return (Criteria) this;
        }

        public Criteria andDelaydaysNotBetween(Integer value1, Integer value2) {
            addCriterion("delayDays not between", value1, value2, "delaydays");
            return (Criteria) this;
        }

        public Criteria andUpdateLockIsNull() {
            addCriterion("update_lock is null");
            return (Criteria) this;
        }

        public Criteria andUpdateLockIsNotNull() {
            addCriterion("update_lock is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateLockEqualTo(Boolean value) {
            addCriterion("update_lock =", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockNotEqualTo(Boolean value) {
            addCriterion("update_lock <>", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockGreaterThan(Boolean value) {
            addCriterion("update_lock >", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockGreaterThanOrEqualTo(Boolean value) {
            addCriterion("update_lock >=", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockLessThan(Boolean value) {
            addCriterion("update_lock <", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockLessThanOrEqualTo(Boolean value) {
            addCriterion("update_lock <=", value, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockIn(List<Boolean> values) {
            addCriterion("update_lock in", values, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockNotIn(List<Boolean> values) {
            addCriterion("update_lock not in", values, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockBetween(Boolean value1, Boolean value2) {
            addCriterion("update_lock between", value1, value2, "updateLock");
            return (Criteria) this;
        }

        public Criteria andUpdateLockNotBetween(Boolean value1, Boolean value2) {
            addCriterion("update_lock not between", value1, value2, "updateLock");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagIsNull() {
            addCriterion("synchronousFlag is null");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagIsNotNull() {
            addCriterion("synchronousFlag is not null");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagEqualTo(String value) {
            addCriterion("synchronousFlag =", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagNotEqualTo(String value) {
            addCriterion("synchronousFlag <>", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagGreaterThan(String value) {
            addCriterion("synchronousFlag >", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagGreaterThanOrEqualTo(String value) {
            addCriterion("synchronousFlag >=", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagLessThan(String value) {
            addCriterion("synchronousFlag <", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagLessThanOrEqualTo(String value) {
            addCriterion("synchronousFlag <=", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagLike(String value) {
            addCriterion("synchronousFlag like", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagNotLike(String value) {
            addCriterion("synchronousFlag not like", value, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagIn(List<String> values) {
            addCriterion("synchronousFlag in", values, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagNotIn(List<String> values) {
            addCriterion("synchronousFlag not in", values, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagBetween(String value1, String value2) {
            addCriterion("synchronousFlag between", value1, value2, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andSynchronousflagNotBetween(String value1, String value2) {
            addCriterion("synchronousFlag not between", value1, value2, "synchronousflag");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdIsNull() {
            addCriterion("third_item_id is null");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdIsNotNull() {
            addCriterion("third_item_id is not null");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdEqualTo(Long value) {
            addCriterion("third_item_id =", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdNotEqualTo(Long value) {
            addCriterion("third_item_id <>", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdGreaterThan(Long value) {
            addCriterion("third_item_id >", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdGreaterThanOrEqualTo(Long value) {
            addCriterion("third_item_id >=", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdLessThan(Long value) {
            addCriterion("third_item_id <", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdLessThanOrEqualTo(Long value) {
            addCriterion("third_item_id <=", value, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdIn(List<Long> values) {
            addCriterion("third_item_id in", values, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdNotIn(List<Long> values) {
            addCriterion("third_item_id not in", values, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdBetween(Long value1, Long value2) {
            addCriterion("third_item_id between", value1, value2, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andThirdItemIdNotBetween(Long value1, Long value2) {
            addCriterion("third_item_id not between", value1, value2, "thirdItemId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdIsNull() {
            addCriterion("partner_product_id is null");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdIsNotNull() {
            addCriterion("partner_product_id is not null");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdEqualTo(Long value) {
            addCriterion("partner_product_id =", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdNotEqualTo(Long value) {
            addCriterion("partner_product_id <>", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdGreaterThan(Long value) {
            addCriterion("partner_product_id >", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdGreaterThanOrEqualTo(Long value) {
            addCriterion("partner_product_id >=", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdLessThan(Long value) {
            addCriterion("partner_product_id <", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdLessThanOrEqualTo(Long value) {
            addCriterion("partner_product_id <=", value, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdIn(List<Long> values) {
            addCriterion("partner_product_id in", values, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdNotIn(List<Long> values) {
            addCriterion("partner_product_id not in", values, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdBetween(Long value1, Long value2) {
            addCriterion("partner_product_id between", value1, value2, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andPartnerProductIdNotBetween(Long value1, Long value2) {
            addCriterion("partner_product_id not between", value1, value2, "partnerProductId");
            return (Criteria) this;
        }

        public Criteria andSourceIsNull() {
            addCriterion("source is null");
            return (Criteria) this;
        }

        public Criteria andSourceIsNotNull() {
            addCriterion("source is not null");
            return (Criteria) this;
        }

        public Criteria andSourceEqualTo(String value) {
            addCriterion("source =", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotEqualTo(String value) {
            addCriterion("source <>", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThan(String value) {
            addCriterion("source >", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceGreaterThanOrEqualTo(String value) {
            addCriterion("source >=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThan(String value) {
            addCriterion("source <", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLessThanOrEqualTo(String value) {
            addCriterion("source <=", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceLike(String value) {
            addCriterion("source like", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotLike(String value) {
            addCriterion("source not like", value, "source");
            return (Criteria) this;
        }

        public Criteria andSourceIn(List<String> values) {
            addCriterion("source in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotIn(List<String> values) {
            addCriterion("source not in", values, "source");
            return (Criteria) this;
        }

        public Criteria andSourceBetween(String value1, String value2) {
            addCriterion("source between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andSourceNotBetween(String value1, String value2) {
            addCriterion("source not between", value1, value2, "source");
            return (Criteria) this;
        }

        public Criteria andInActivityIsNull() {
            addCriterion("in_activity is null");
            return (Criteria) this;
        }

        public Criteria andInActivityIsNotNull() {
            addCriterion("in_activity is not null");
            return (Criteria) this;
        }

        public Criteria andInActivityEqualTo(Byte value) {
            addCriterion("in_activity =", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityNotEqualTo(Byte value) {
            addCriterion("in_activity <>", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityGreaterThan(Byte value) {
            addCriterion("in_activity >", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityGreaterThanOrEqualTo(Byte value) {
            addCriterion("in_activity >=", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityLessThan(Byte value) {
            addCriterion("in_activity <", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityLessThanOrEqualTo(Byte value) {
            addCriterion("in_activity <=", value, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityIn(List<Byte> values) {
            addCriterion("in_activity in", values, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityNotIn(List<Byte> values) {
            addCriterion("in_activity not in", values, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityBetween(Byte value1, Byte value2) {
            addCriterion("in_activity between", value1, value2, "inActivity");
            return (Criteria) this;
        }

        public Criteria andInActivityNotBetween(Byte value1, Byte value2) {
            addCriterion("in_activity not between", value1, value2, "inActivity");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderIsNull() {
            addCriterion("is_cross_border is null");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderIsNotNull() {
            addCriterion("is_cross_border is not null");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderEqualTo(Boolean value) {
            addCriterion("is_cross_border =", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderNotEqualTo(Boolean value) {
            addCriterion("is_cross_border <>", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderGreaterThan(Boolean value) {
            addCriterion("is_cross_border >", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_cross_border >=", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderLessThan(Boolean value) {
            addCriterion("is_cross_border <", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderLessThanOrEqualTo(Boolean value) {
            addCriterion("is_cross_border <=", value, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderIn(List<Boolean> values) {
            addCriterion("is_cross_border in", values, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderNotIn(List<Boolean> values) {
            addCriterion("is_cross_border not in", values, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderBetween(Boolean value1, Boolean value2) {
            addCriterion("is_cross_border between", value1, value2, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andIsCrossBorderNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_cross_border not between", value1, value2, "isCrossBorder");
            return (Criteria) this;
        }

        public Criteria andEnableDescIsNull() {
            addCriterion("enable_desc is null");
            return (Criteria) this;
        }

        public Criteria andEnableDescIsNotNull() {
            addCriterion("enable_desc is not null");
            return (Criteria) this;
        }

        public Criteria andEnableDescEqualTo(Boolean value) {
            addCriterion("enable_desc =", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescNotEqualTo(Boolean value) {
            addCriterion("enable_desc <>", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescGreaterThan(Boolean value) {
            addCriterion("enable_desc >", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescGreaterThanOrEqualTo(Boolean value) {
            addCriterion("enable_desc >=", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescLessThan(Boolean value) {
            addCriterion("enable_desc <", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescLessThanOrEqualTo(Boolean value) {
            addCriterion("enable_desc <=", value, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescIn(List<Boolean> values) {
            addCriterion("enable_desc in", values, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescNotIn(List<Boolean> values) {
            addCriterion("enable_desc not in", values, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescBetween(Boolean value1, Boolean value2) {
            addCriterion("enable_desc between", value1, value2, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andEnableDescNotBetween(Boolean value1, Boolean value2) {
            addCriterion("enable_desc not between", value1, value2, "enableDesc");
            return (Criteria) this;
        }

        public Criteria andIsDistributionIsNull() {
            addCriterion("is_distribution is null");
            return (Criteria) this;
        }

        public Criteria andIsDistributionIsNotNull() {
            addCriterion("is_distribution is not null");
            return (Criteria) this;
        }

        public Criteria andIsDistributionEqualTo(Boolean value) {
            addCriterion("is_distribution =", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionNotEqualTo(Boolean value) {
            addCriterion("is_distribution <>", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionGreaterThan(Boolean value) {
            addCriterion("is_distribution >", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_distribution >=", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionLessThan(Boolean value) {
            addCriterion("is_distribution <", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionLessThanOrEqualTo(Boolean value) {
            addCriterion("is_distribution <=", value, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionIn(List<Boolean> values) {
            addCriterion("is_distribution in", values, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionNotIn(List<Boolean> values) {
            addCriterion("is_distribution not in", values, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionBetween(Boolean value1, Boolean value2) {
            addCriterion("is_distribution between", value1, value2, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andIsDistributionNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_distribution not between", value1, value2, "isDistribution");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdIsNull() {
            addCriterion("source_product_id is null");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdIsNotNull() {
            addCriterion("source_product_id is not null");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdEqualTo(Long value) {
            addCriterion("source_product_id =", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdNotEqualTo(Long value) {
            addCriterion("source_product_id <>", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdGreaterThan(Long value) {
            addCriterion("source_product_id >", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdGreaterThanOrEqualTo(Long value) {
            addCriterion("source_product_id >=", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdLessThan(Long value) {
            addCriterion("source_product_id <", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdLessThanOrEqualTo(Long value) {
            addCriterion("source_product_id <=", value, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdIn(List<Long> values) {
            addCriterion("source_product_id in", values, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdNotIn(List<Long> values) {
            addCriterion("source_product_id not in", values, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdBetween(Long value1, Long value2) {
            addCriterion("source_product_id between", value1, value2, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceProductIdNotBetween(Long value1, Long value2) {
            addCriterion("source_product_id not between", value1, value2, "sourceProductId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdIsNull() {
            addCriterion("source_shop_id is null");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdIsNotNull() {
            addCriterion("source_shop_id is not null");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdEqualTo(Long value) {
            addCriterion("source_shop_id =", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdNotEqualTo(Long value) {
            addCriterion("source_shop_id <>", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdGreaterThan(Long value) {
            addCriterion("source_shop_id >", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdGreaterThanOrEqualTo(Long value) {
            addCriterion("source_shop_id >=", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdLessThan(Long value) {
            addCriterion("source_shop_id <", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdLessThanOrEqualTo(Long value) {
            addCriterion("source_shop_id <=", value, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdIn(List<Long> values) {
            addCriterion("source_shop_id in", values, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdNotIn(List<Long> values) {
            addCriterion("source_shop_id not in", values, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdBetween(Long value1, Long value2) {
            addCriterion("source_shop_id between", value1, value2, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andSourceShopIdNotBetween(Long value1, Long value2) {
            addCriterion("source_shop_id not between", value1, value2, "sourceShopId");
            return (Criteria) this;
        }

        public Criteria andFeature0IsNull() {
            addCriterion("feature0 is null");
            return (Criteria) this;
        }

        public Criteria andFeature0IsNotNull() {
            addCriterion("feature0 is not null");
            return (Criteria) this;
        }

        public Criteria andFeature0EqualTo(Long value) {
            addCriterion("feature0 =", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0NotEqualTo(Long value) {
            addCriterion("feature0 <>", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0GreaterThan(Long value) {
            addCriterion("feature0 >", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0GreaterThanOrEqualTo(Long value) {
            addCriterion("feature0 >=", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0LessThan(Long value) {
            addCriterion("feature0 <", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0LessThanOrEqualTo(Long value) {
            addCriterion("feature0 <=", value, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0In(List<Long> values) {
            addCriterion("feature0 in", values, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0NotIn(List<Long> values) {
            addCriterion("feature0 not in", values, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0Between(Long value1, Long value2) {
            addCriterion("feature0 between", value1, value2, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature0NotBetween(Long value1, Long value2) {
            addCriterion("feature0 not between", value1, value2, "feature0");
            return (Criteria) this;
        }

        public Criteria andFeature1IsNull() {
            addCriterion("feature1 is null");
            return (Criteria) this;
        }

        public Criteria andFeature1IsNotNull() {
            addCriterion("feature1 is not null");
            return (Criteria) this;
        }

        public Criteria andFeature1EqualTo(Long value) {
            addCriterion("feature1 =", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1NotEqualTo(Long value) {
            addCriterion("feature1 <>", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1GreaterThan(Long value) {
            addCriterion("feature1 >", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1GreaterThanOrEqualTo(Long value) {
            addCriterion("feature1 >=", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1LessThan(Long value) {
            addCriterion("feature1 <", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1LessThanOrEqualTo(Long value) {
            addCriterion("feature1 <=", value, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1In(List<Long> values) {
            addCriterion("feature1 in", values, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1NotIn(List<Long> values) {
            addCriterion("feature1 not in", values, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1Between(Long value1, Long value2) {
            addCriterion("feature1 between", value1, value2, "feature1");
            return (Criteria) this;
        }

        public Criteria andFeature1NotBetween(Long value1, Long value2) {
            addCriterion("feature1 not between", value1, value2, "feature1");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageIsNull() {
            addCriterion("oneyuan_postage is null");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageIsNotNull() {
            addCriterion("oneyuan_postage is not null");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageEqualTo(BigDecimal value) {
            addCriterion("oneyuan_postage =", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageNotEqualTo(BigDecimal value) {
            addCriterion("oneyuan_postage <>", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageGreaterThan(BigDecimal value) {
            addCriterion("oneyuan_postage >", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("oneyuan_postage >=", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageLessThan(BigDecimal value) {
            addCriterion("oneyuan_postage <", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageLessThanOrEqualTo(BigDecimal value) {
            addCriterion("oneyuan_postage <=", value, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageIn(List<BigDecimal> values) {
            addCriterion("oneyuan_postage in", values, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageNotIn(List<BigDecimal> values) {
            addCriterion("oneyuan_postage not in", values, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("oneyuan_postage between", value1, value2, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPostageNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("oneyuan_postage not between", value1, value2, "oneyuanPostage");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseIsNull() {
            addCriterion("oneyuan_purchase is null");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseIsNotNull() {
            addCriterion("oneyuan_purchase is not null");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseEqualTo(Boolean value) {
            addCriterion("oneyuan_purchase =", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseNotEqualTo(Boolean value) {
            addCriterion("oneyuan_purchase <>", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseGreaterThan(Boolean value) {
            addCriterion("oneyuan_purchase >", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseGreaterThanOrEqualTo(Boolean value) {
            addCriterion("oneyuan_purchase >=", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseLessThan(Boolean value) {
            addCriterion("oneyuan_purchase <", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseLessThanOrEqualTo(Boolean value) {
            addCriterion("oneyuan_purchase <=", value, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseIn(List<Boolean> values) {
            addCriterion("oneyuan_purchase in", values, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseNotIn(List<Boolean> values) {
            addCriterion("oneyuan_purchase not in", values, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseBetween(Boolean value1, Boolean value2) {
            addCriterion("oneyuan_purchase between", value1, value2, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andOneyuanPurchaseNotBetween(Boolean value1, Boolean value2) {
            addCriterion("oneyuan_purchase not between", value1, value2, "oneyuanPurchase");
            return (Criteria) this;
        }

        public Criteria andSpecialrateIsNull() {
            addCriterion("specialRate is null");
            return (Criteria) this;
        }

        public Criteria andSpecialrateIsNotNull() {
            addCriterion("specialRate is not null");
            return (Criteria) this;
        }

        public Criteria andSpecialrateEqualTo(BigDecimal value) {
            addCriterion("specialRate =", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateNotEqualTo(BigDecimal value) {
            addCriterion("specialRate <>", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateGreaterThan(BigDecimal value) {
            addCriterion("specialRate >", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("specialRate >=", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateLessThan(BigDecimal value) {
            addCriterion("specialRate <", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("specialRate <=", value, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateIn(List<BigDecimal> values) {
            addCriterion("specialRate in", values, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateNotIn(List<BigDecimal> values) {
            addCriterion("specialRate not in", values, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("specialRate between", value1, value2, "specialrate");
            return (Criteria) this;
        }

        public Criteria andSpecialrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("specialRate not between", value1, value2, "specialrate");
            return (Criteria) this;
        }

        public Criteria andIsGrouponIsNull() {
            addCriterion("is_groupon is null");
            return (Criteria) this;
        }

        public Criteria andIsGrouponIsNotNull() {
            addCriterion("is_groupon is not null");
            return (Criteria) this;
        }

        public Criteria andIsGrouponEqualTo(Boolean value) {
            addCriterion("is_groupon =", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponNotEqualTo(Boolean value) {
            addCriterion("is_groupon <>", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponGreaterThan(Boolean value) {
            addCriterion("is_groupon >", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_groupon >=", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponLessThan(Boolean value) {
            addCriterion("is_groupon <", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponLessThanOrEqualTo(Boolean value) {
            addCriterion("is_groupon <=", value, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponIn(List<Boolean> values) {
            addCriterion("is_groupon in", values, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponNotIn(List<Boolean> values) {
            addCriterion("is_groupon not in", values, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponBetween(Boolean value1, Boolean value2) {
            addCriterion("is_groupon between", value1, value2, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andIsGrouponNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_groupon not between", value1, value2, "isGroupon");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIsNull() {
            addCriterion("refund_address is null");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIsNotNull() {
            addCriterion("refund_address is not null");
            return (Criteria) this;
        }

        public Criteria andRefundAddressEqualTo(String value) {
            addCriterion("refund_address =", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotEqualTo(String value) {
            addCriterion("refund_address <>", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressGreaterThan(String value) {
            addCriterion("refund_address >", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressGreaterThanOrEqualTo(String value) {
            addCriterion("refund_address >=", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLessThan(String value) {
            addCriterion("refund_address <", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLessThanOrEqualTo(String value) {
            addCriterion("refund_address <=", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressLike(String value) {
            addCriterion("refund_address like", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotLike(String value) {
            addCriterion("refund_address not like", value, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressIn(List<String> values) {
            addCriterion("refund_address in", values, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotIn(List<String> values) {
            addCriterion("refund_address not in", values, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressBetween(String value1, String value2) {
            addCriterion("refund_address between", value1, value2, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundAddressNotBetween(String value1, String value2) {
            addCriterion("refund_address not between", value1, value2, "refundAddress");
            return (Criteria) this;
        }

        public Criteria andRefundTelIsNull() {
            addCriterion("refund_tel is null");
            return (Criteria) this;
        }

        public Criteria andRefundTelIsNotNull() {
            addCriterion("refund_tel is not null");
            return (Criteria) this;
        }

        public Criteria andRefundTelEqualTo(String value) {
            addCriterion("refund_tel =", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelNotEqualTo(String value) {
            addCriterion("refund_tel <>", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelGreaterThan(String value) {
            addCriterion("refund_tel >", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelGreaterThanOrEqualTo(String value) {
            addCriterion("refund_tel >=", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelLessThan(String value) {
            addCriterion("refund_tel <", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelLessThanOrEqualTo(String value) {
            addCriterion("refund_tel <=", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelLike(String value) {
            addCriterion("refund_tel like", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelNotLike(String value) {
            addCriterion("refund_tel not like", value, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelIn(List<String> values) {
            addCriterion("refund_tel in", values, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelNotIn(List<String> values) {
            addCriterion("refund_tel not in", values, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelBetween(String value1, String value2) {
            addCriterion("refund_tel between", value1, value2, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundTelNotBetween(String value1, String value2) {
            addCriterion("refund_tel not between", value1, value2, "refundTel");
            return (Criteria) this;
        }

        public Criteria andRefundNameIsNull() {
            addCriterion("refund_name is null");
            return (Criteria) this;
        }

        public Criteria andRefundNameIsNotNull() {
            addCriterion("refund_name is not null");
            return (Criteria) this;
        }

        public Criteria andRefundNameEqualTo(String value) {
            addCriterion("refund_name =", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameNotEqualTo(String value) {
            addCriterion("refund_name <>", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameGreaterThan(String value) {
            addCriterion("refund_name >", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameGreaterThanOrEqualTo(String value) {
            addCriterion("refund_name >=", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameLessThan(String value) {
            addCriterion("refund_name <", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameLessThanOrEqualTo(String value) {
            addCriterion("refund_name <=", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameLike(String value) {
            addCriterion("refund_name like", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameNotLike(String value) {
            addCriterion("refund_name not like", value, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameIn(List<String> values) {
            addCriterion("refund_name in", values, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameNotIn(List<String> values) {
            addCriterion("refund_name not in", values, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameBetween(String value1, String value2) {
            addCriterion("refund_name between", value1, value2, "refundName");
            return (Criteria) this;
        }

        public Criteria andRefundNameNotBetween(String value1, String value2) {
            addCriterion("refund_name not between", value1, value2, "refundName");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeIsNull() {
            addCriterion("logistics_type is null");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeIsNotNull() {
            addCriterion("logistics_type is not null");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeEqualTo(String value) {
            addCriterion("logistics_type =", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeNotEqualTo(String value) {
            addCriterion("logistics_type <>", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeGreaterThan(String value) {
            addCriterion("logistics_type >", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeGreaterThanOrEqualTo(String value) {
            addCriterion("logistics_type >=", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeLessThan(String value) {
            addCriterion("logistics_type <", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeLessThanOrEqualTo(String value) {
            addCriterion("logistics_type <=", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeLike(String value) {
            addCriterion("logistics_type like", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeNotLike(String value) {
            addCriterion("logistics_type not like", value, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeIn(List<String> values) {
            addCriterion("logistics_type in", values, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeNotIn(List<String> values) {
            addCriterion("logistics_type not in", values, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeBetween(String value1, String value2) {
            addCriterion("logistics_type between", value1, value2, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andLogisticsTypeNotBetween(String value1, String value2) {
            addCriterion("logistics_type not between", value1, value2, "logisticsType");
            return (Criteria) this;
        }

        public Criteria andUniformValueIsNull() {
            addCriterion("uniform_value is null");
            return (Criteria) this;
        }

        public Criteria andUniformValueIsNotNull() {
            addCriterion("uniform_value is not null");
            return (Criteria) this;
        }

        public Criteria andUniformValueEqualTo(BigDecimal value) {
            addCriterion("uniform_value =", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueNotEqualTo(BigDecimal value) {
            addCriterion("uniform_value <>", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueGreaterThan(BigDecimal value) {
            addCriterion("uniform_value >", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("uniform_value >=", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueLessThan(BigDecimal value) {
            addCriterion("uniform_value <", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("uniform_value <=", value, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueIn(List<BigDecimal> values) {
            addCriterion("uniform_value in", values, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueNotIn(List<BigDecimal> values) {
            addCriterion("uniform_value not in", values, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("uniform_value between", value1, value2, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andUniformValueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("uniform_value not between", value1, value2, "uniformValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueIsNull() {
            addCriterion("template_value is null");
            return (Criteria) this;
        }

        public Criteria andTemplateValueIsNotNull() {
            addCriterion("template_value is not null");
            return (Criteria) this;
        }

        public Criteria andTemplateValueEqualTo(Long value) {
            addCriterion("template_value =", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueNotEqualTo(Long value) {
            addCriterion("template_value <>", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueGreaterThan(Long value) {
            addCriterion("template_value >", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueGreaterThanOrEqualTo(Long value) {
            addCriterion("template_value >=", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueLessThan(Long value) {
            addCriterion("template_value <", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueLessThanOrEqualTo(Long value) {
            addCriterion("template_value <=", value, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueIn(List<Long> values) {
            addCriterion("template_value in", values, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueNotIn(List<Long> values) {
            addCriterion("template_value not in", values, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueBetween(Long value1, Long value2) {
            addCriterion("template_value between", value1, value2, "templateValue");
            return (Criteria) this;
        }

        public Criteria andTemplateValueNotBetween(Long value1, Long value2) {
            addCriterion("template_value not between", value1, value2, "templateValue");
            return (Criteria) this;
        }

        public Criteria andGiftIsNull() {
            addCriterion("gift is null");
            return (Criteria) this;
        }

        public Criteria andGiftIsNotNull() {
            addCriterion("gift is not null");
            return (Criteria) this;
        }

        public Criteria andGiftEqualTo(Byte value) {
            addCriterion("gift =", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftNotEqualTo(Byte value) {
            addCriterion("gift <>", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftGreaterThan(Byte value) {
            addCriterion("gift >", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftGreaterThanOrEqualTo(Byte value) {
            addCriterion("gift >=", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftLessThan(Byte value) {
            addCriterion("gift <", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftLessThanOrEqualTo(Byte value) {
            addCriterion("gift <=", value, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftIn(List<Byte> values) {
            addCriterion("gift in", values, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftNotIn(List<Byte> values) {
            addCriterion("gift not in", values, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftBetween(Byte value1, Byte value2) {
            addCriterion("gift between", value1, value2, "gift");
            return (Criteria) this;
        }

        public Criteria andGiftNotBetween(Byte value1, Byte value2) {
            addCriterion("gift not between", value1, value2, "gift");
            return (Criteria) this;
        }

        public Criteria andWeightIsNull() {
            addCriterion("weight is null");
            return (Criteria) this;
        }

        public Criteria andWeightIsNotNull() {
            addCriterion("weight is not null");
            return (Criteria) this;
        }

        public Criteria andWeightEqualTo(Integer value) {
            addCriterion("weight =", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotEqualTo(Integer value) {
            addCriterion("weight <>", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThan(Integer value) {
            addCriterion("weight >", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("weight >=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThan(Integer value) {
            addCriterion("weight <", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightLessThanOrEqualTo(Integer value) {
            addCriterion("weight <=", value, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightIn(List<Integer> values) {
            addCriterion("weight in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotIn(List<Integer> values) {
            addCriterion("weight not in", values, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightBetween(Integer value1, Integer value2) {
            addCriterion("weight between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andWeightNotBetween(Integer value1, Integer value2) {
            addCriterion("weight not between", value1, value2, "weight");
            return (Criteria) this;
        }

        public Criteria andAttributesIsNull() {
            addCriterion("attributes is null");
            return (Criteria) this;
        }

        public Criteria andAttributesIsNotNull() {
            addCriterion("attributes is not null");
            return (Criteria) this;
        }

        public Criteria andAttributesEqualTo(String value) {
            addCriterion("attributes =", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotEqualTo(String value) {
            addCriterion("attributes <>", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesGreaterThan(String value) {
            addCriterion("attributes >", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesGreaterThanOrEqualTo(String value) {
            addCriterion("attributes >=", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLessThan(String value) {
            addCriterion("attributes <", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLessThanOrEqualTo(String value) {
            addCriterion("attributes <=", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesLike(String value) {
            addCriterion("attributes like", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotLike(String value) {
            addCriterion("attributes not like", value, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesIn(List<String> values) {
            addCriterion("attributes in", values, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotIn(List<String> values) {
            addCriterion("attributes not in", values, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesBetween(String value1, String value2) {
            addCriterion("attributes between", value1, value2, "attributes");
            return (Criteria) this;
        }

        public Criteria andAttributesNotBetween(String value1, String value2) {
            addCriterion("attributes not between", value1, value2, "attributes");
            return (Criteria) this;
        }

        public Criteria andYundouScaleIsNull() {
            addCriterion("yundou_scale is null");
            return (Criteria) this;
        }

        public Criteria andYundouScaleIsNotNull() {
            addCriterion("yundou_scale is not null");
            return (Criteria) this;
        }

        public Criteria andYundouScaleEqualTo(Boolean value) {
            addCriterion("yundou_scale =", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleNotEqualTo(Boolean value) {
            addCriterion("yundou_scale <>", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleGreaterThan(Boolean value) {
            addCriterion("yundou_scale >", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleGreaterThanOrEqualTo(Boolean value) {
            addCriterion("yundou_scale >=", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleLessThan(Boolean value) {
            addCriterion("yundou_scale <", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleLessThanOrEqualTo(Boolean value) {
            addCriterion("yundou_scale <=", value, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleIn(List<Boolean> values) {
            addCriterion("yundou_scale in", values, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleNotIn(List<Boolean> values) {
            addCriterion("yundou_scale not in", values, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleBetween(Boolean value1, Boolean value2) {
            addCriterion("yundou_scale between", value1, value2, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andYundouScaleNotBetween(Boolean value1, Boolean value2) {
            addCriterion("yundou_scale not between", value1, value2, "yundouScale");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceIsNull() {
            addCriterion("min_yundou_price is null");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceIsNotNull() {
            addCriterion("min_yundou_price is not null");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceEqualTo(BigDecimal value) {
            addCriterion("min_yundou_price =", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceNotEqualTo(BigDecimal value) {
            addCriterion("min_yundou_price <>", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceGreaterThan(BigDecimal value) {
            addCriterion("min_yundou_price >", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("min_yundou_price >=", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceLessThan(BigDecimal value) {
            addCriterion("min_yundou_price <", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("min_yundou_price <=", value, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceIn(List<BigDecimal> values) {
            addCriterion("min_yundou_price in", values, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceNotIn(List<BigDecimal> values) {
            addCriterion("min_yundou_price not in", values, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_yundou_price between", value1, value2, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andMinYundouPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_yundou_price not between", value1, value2, "minYundouPrice");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusIsNull() {
            addCriterion("available_status is null");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusIsNotNull() {
            addCriterion("available_status is not null");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusEqualTo(String value) {
            addCriterion("available_status =", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusNotEqualTo(String value) {
            addCriterion("available_status <>", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusGreaterThan(String value) {
            addCriterion("available_status >", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusGreaterThanOrEqualTo(String value) {
            addCriterion("available_status >=", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusLessThan(String value) {
            addCriterion("available_status <", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusLessThanOrEqualTo(String value) {
            addCriterion("available_status <=", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusLike(String value) {
            addCriterion("available_status like", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusNotLike(String value) {
            addCriterion("available_status not like", value, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusIn(List<String> values) {
            addCriterion("available_status in", values, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusNotIn(List<String> values) {
            addCriterion("available_status not in", values, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusBetween(String value1, String value2) {
            addCriterion("available_status between", value1, value2, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andAvailableStatusNotBetween(String value1, String value2) {
            addCriterion("available_status not between", value1, value2, "availableStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusIsNull() {
            addCriterion("review_status is null");
            return (Criteria) this;
        }

        public Criteria andReviewStatusIsNotNull() {
            addCriterion("review_status is not null");
            return (Criteria) this;
        }

        public Criteria andReviewStatusEqualTo(String value) {
            addCriterion("review_status =", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusNotEqualTo(String value) {
            addCriterion("review_status <>", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusGreaterThan(String value) {
            addCriterion("review_status >", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusGreaterThanOrEqualTo(String value) {
            addCriterion("review_status >=", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusLessThan(String value) {
            addCriterion("review_status <", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusLessThanOrEqualTo(String value) {
            addCriterion("review_status <=", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusLike(String value) {
            addCriterion("review_status like", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusNotLike(String value) {
            addCriterion("review_status not like", value, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusIn(List<String> values) {
            addCriterion("review_status in", values, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusNotIn(List<String> values) {
            addCriterion("review_status not in", values, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusBetween(String value1, String value2) {
            addCriterion("review_status between", value1, value2, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andReviewStatusNotBetween(String value1, String value2) {
            addCriterion("review_status not between", value1, value2, "reviewStatus");
            return (Criteria) this;
        }

        public Criteria andHeightIsNull() {
            addCriterion("height is null");
            return (Criteria) this;
        }

        public Criteria andHeightIsNotNull() {
            addCriterion("height is not null");
            return (Criteria) this;
        }

        public Criteria andHeightEqualTo(Double value) {
            addCriterion("height =", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotEqualTo(Double value) {
            addCriterion("height <>", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThan(Double value) {
            addCriterion("height >", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightGreaterThanOrEqualTo(Double value) {
            addCriterion("height >=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThan(Double value) {
            addCriterion("height <", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightLessThanOrEqualTo(Double value) {
            addCriterion("height <=", value, "height");
            return (Criteria) this;
        }

        public Criteria andHeightIn(List<Double> values) {
            addCriterion("height in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotIn(List<Double> values) {
            addCriterion("height not in", values, "height");
            return (Criteria) this;
        }

        public Criteria andHeightBetween(Double value1, Double value2) {
            addCriterion("height between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andHeightNotBetween(Double value1, Double value2) {
            addCriterion("height not between", value1, value2, "height");
            return (Criteria) this;
        }

        public Criteria andWidthIsNull() {
            addCriterion("width is null");
            return (Criteria) this;
        }

        public Criteria andWidthIsNotNull() {
            addCriterion("width is not null");
            return (Criteria) this;
        }

        public Criteria andWidthEqualTo(Double value) {
            addCriterion("width =", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthNotEqualTo(Double value) {
            addCriterion("width <>", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthGreaterThan(Double value) {
            addCriterion("width >", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthGreaterThanOrEqualTo(Double value) {
            addCriterion("width >=", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthLessThan(Double value) {
            addCriterion("width <", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthLessThanOrEqualTo(Double value) {
            addCriterion("width <=", value, "width");
            return (Criteria) this;
        }

        public Criteria andWidthIn(List<Double> values) {
            addCriterion("width in", values, "width");
            return (Criteria) this;
        }

        public Criteria andWidthNotIn(List<Double> values) {
            addCriterion("width not in", values, "width");
            return (Criteria) this;
        }

        public Criteria andWidthBetween(Double value1, Double value2) {
            addCriterion("width between", value1, value2, "width");
            return (Criteria) this;
        }

        public Criteria andWidthNotBetween(Double value1, Double value2) {
            addCriterion("width not between", value1, value2, "width");
            return (Criteria) this;
        }

        public Criteria andLengthIsNull() {
            addCriterion("length is null");
            return (Criteria) this;
        }

        public Criteria andLengthIsNotNull() {
            addCriterion("length is not null");
            return (Criteria) this;
        }

        public Criteria andLengthEqualTo(Double value) {
            addCriterion("length =", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthNotEqualTo(Double value) {
            addCriterion("length <>", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthGreaterThan(Double value) {
            addCriterion("length >", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthGreaterThanOrEqualTo(Double value) {
            addCriterion("length >=", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthLessThan(Double value) {
            addCriterion("length <", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthLessThanOrEqualTo(Double value) {
            addCriterion("length <=", value, "length");
            return (Criteria) this;
        }

        public Criteria andLengthIn(List<Double> values) {
            addCriterion("length in", values, "length");
            return (Criteria) this;
        }

        public Criteria andLengthNotIn(List<Double> values) {
            addCriterion("length not in", values, "length");
            return (Criteria) this;
        }

        public Criteria andLengthBetween(Double value1, Double value2) {
            addCriterion("length between", value1, value2, "length");
            return (Criteria) this;
        }

        public Criteria andLengthNotBetween(Double value1, Double value2) {
            addCriterion("length not between", value1, value2, "length");
            return (Criteria) this;
        }

        public Criteria andKindIsNull() {
            addCriterion("kind is null");
            return (Criteria) this;
        }

        public Criteria andKindIsNotNull() {
            addCriterion("kind is not null");
            return (Criteria) this;
        }

        public Criteria andKindEqualTo(String value) {
            addCriterion("kind =", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindNotEqualTo(String value) {
            addCriterion("kind <>", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindGreaterThan(String value) {
            addCriterion("kind >", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindGreaterThanOrEqualTo(String value) {
            addCriterion("kind >=", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindLessThan(String value) {
            addCriterion("kind <", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindLessThanOrEqualTo(String value) {
            addCriterion("kind <=", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindLike(String value) {
            addCriterion("kind like", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindNotLike(String value) {
            addCriterion("kind not like", value, "kind");
            return (Criteria) this;
        }

        public Criteria andKindIn(List<String> values) {
            addCriterion("kind in", values, "kind");
            return (Criteria) this;
        }

        public Criteria andKindNotIn(List<String> values) {
            addCriterion("kind not in", values, "kind");
            return (Criteria) this;
        }

        public Criteria andKindBetween(String value1, String value2) {
            addCriterion("kind between", value1, value2, "kind");
            return (Criteria) this;
        }

        public Criteria andKindNotBetween(String value1, String value2) {
            addCriterion("kind not between", value1, value2, "kind");
            return (Criteria) this;
        }

        public Criteria andPointIsNull() {
            addCriterion("point is null");
            return (Criteria) this;
        }

        public Criteria andPointIsNotNull() {
            addCriterion("point is not null");
            return (Criteria) this;
        }

        public Criteria andPointEqualTo(Long value) {
            addCriterion("point =", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotEqualTo(Long value) {
            addCriterion("point <>", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThan(Long value) {
            addCriterion("point >", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointGreaterThanOrEqualTo(Long value) {
            addCriterion("point >=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThan(Long value) {
            addCriterion("point <", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointLessThanOrEqualTo(Long value) {
            addCriterion("point <=", value, "point");
            return (Criteria) this;
        }

        public Criteria andPointIn(List<Long> values) {
            addCriterion("point in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotIn(List<Long> values) {
            addCriterion("point not in", values, "point");
            return (Criteria) this;
        }

        public Criteria andPointBetween(Long value1, Long value2) {
            addCriterion("point between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andPointNotBetween(Long value1, Long value2) {
            addCriterion("point not between", value1, value2, "point");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointIsNull() {
            addCriterion("deduction_d_point is null");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointIsNotNull() {
            addCriterion("deduction_d_point is not null");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointEqualTo(BigDecimal value) {
            addCriterion("deduction_d_point =", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointNotEqualTo(BigDecimal value) {
            addCriterion("deduction_d_point <>", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointGreaterThan(BigDecimal value) {
            addCriterion("deduction_d_point >", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("deduction_d_point >=", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointLessThan(BigDecimal value) {
            addCriterion("deduction_d_point <", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointLessThanOrEqualTo(BigDecimal value) {
            addCriterion("deduction_d_point <=", value, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointIn(List<BigDecimal> values) {
            addCriterion("deduction_d_point in", values, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointNotIn(List<BigDecimal> values) {
            addCriterion("deduction_d_point not in", values, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deduction_d_point between", value1, value2, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andDeductionDPointNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deduction_d_point not between", value1, value2, "deductionDPoint");
            return (Criteria) this;
        }

        public Criteria andSupportRefundIsNull() {
            addCriterion("support_refund is null");
            return (Criteria) this;
        }

        public Criteria andSupportRefundIsNotNull() {
            addCriterion("support_refund is not null");
            return (Criteria) this;
        }

        public Criteria andSupportRefundEqualTo(Boolean value) {
            addCriterion("support_refund =", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundNotEqualTo(Boolean value) {
            addCriterion("support_refund <>", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundGreaterThan(Boolean value) {
            addCriterion("support_refund >", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundGreaterThanOrEqualTo(Boolean value) {
            addCriterion("support_refund >=", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundLessThan(Boolean value) {
            addCriterion("support_refund <", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundLessThanOrEqualTo(Boolean value) {
            addCriterion("support_refund <=", value, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundIn(List<Boolean> values) {
            addCriterion("support_refund in", values, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundNotIn(List<Boolean> values) {
            addCriterion("support_refund not in", values, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundBetween(Boolean value1, Boolean value2) {
            addCriterion("support_refund between", value1, value2, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andSupportRefundNotBetween(Boolean value1, Boolean value2) {
            addCriterion("support_refund not between", value1, value2, "supportRefund");
            return (Criteria) this;
        }

        public Criteria andNumInPackageIsNull() {
            addCriterion("num_in_package is null");
            return (Criteria) this;
        }

        public Criteria andNumInPackageIsNotNull() {
            addCriterion("num_in_package is not null");
            return (Criteria) this;
        }

        public Criteria andNumInPackageEqualTo(Integer value) {
            addCriterion("num_in_package =", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageNotEqualTo(Integer value) {
            addCriterion("num_in_package <>", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageGreaterThan(Integer value) {
            addCriterion("num_in_package >", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageGreaterThanOrEqualTo(Integer value) {
            addCriterion("num_in_package >=", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageLessThan(Integer value) {
            addCriterion("num_in_package <", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageLessThanOrEqualTo(Integer value) {
            addCriterion("num_in_package <=", value, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageIn(List<Integer> values) {
            addCriterion("num_in_package in", values, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageNotIn(List<Integer> values) {
            addCriterion("num_in_package not in", values, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageBetween(Integer value1, Integer value2) {
            addCriterion("num_in_package between", value1, value2, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andNumInPackageNotBetween(Integer value1, Integer value2) {
            addCriterion("num_in_package not between", value1, value2, "numInPackage");
            return (Criteria) this;
        }

        public Criteria andBarCodeIsNull() {
            addCriterion("bar_code is null");
            return (Criteria) this;
        }

        public Criteria andBarCodeIsNotNull() {
            addCriterion("bar_code is not null");
            return (Criteria) this;
        }

        public Criteria andBarCodeEqualTo(String value) {
            addCriterion("bar_code =", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotEqualTo(String value) {
            addCriterion("bar_code <>", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThan(String value) {
            addCriterion("bar_code >", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThanOrEqualTo(String value) {
            addCriterion("bar_code >=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThan(String value) {
            addCriterion("bar_code <", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThanOrEqualTo(String value) {
            addCriterion("bar_code <=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLike(String value) {
            addCriterion("bar_code like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotLike(String value) {
            addCriterion("bar_code not like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeIn(List<String> values) {
            addCriterion("bar_code in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotIn(List<String> values) {
            addCriterion("bar_code not in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeBetween(String value1, String value2) {
            addCriterion("bar_code between", value1, value2, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotBetween(String value1, String value2) {
            addCriterion("bar_code not between", value1, value2, "barCode");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdIsNull() {
            addCriterion("ware_house_id is null");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdIsNotNull() {
            addCriterion("ware_house_id is not null");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdEqualTo(Long value) {
            addCriterion("ware_house_id =", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdNotEqualTo(Long value) {
            addCriterion("ware_house_id <>", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdGreaterThan(Long value) {
            addCriterion("ware_house_id >", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("ware_house_id >=", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdLessThan(Long value) {
            addCriterion("ware_house_id <", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdLessThanOrEqualTo(Long value) {
            addCriterion("ware_house_id <=", value, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdIn(List<Long> values) {
            addCriterion("ware_house_id in", values, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdNotIn(List<Long> values) {
            addCriterion("ware_house_id not in", values, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdBetween(Long value1, Long value2) {
            addCriterion("ware_house_id between", value1, value2, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andWareHouseIdNotBetween(Long value1, Long value2) {
            addCriterion("ware_house_id not between", value1, value2, "wareHouseId");
            return (Criteria) this;
        }

        public Criteria andPackageIdIsNull() {
            addCriterion("package_id is null");
            return (Criteria) this;
        }

        public Criteria andPackageIdIsNotNull() {
            addCriterion("package_id is not null");
            return (Criteria) this;
        }

        public Criteria andPackageIdEqualTo(Long value) {
            addCriterion("package_id =", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdNotEqualTo(Long value) {
            addCriterion("package_id <>", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdGreaterThan(Long value) {
            addCriterion("package_id >", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdGreaterThanOrEqualTo(Long value) {
            addCriterion("package_id >=", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdLessThan(Long value) {
            addCriterion("package_id <", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdLessThanOrEqualTo(Long value) {
            addCriterion("package_id <=", value, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdIn(List<Long> values) {
            addCriterion("package_id in", values, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdNotIn(List<Long> values) {
            addCriterion("package_id not in", values, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdBetween(Long value1, Long value2) {
            addCriterion("package_id between", value1, value2, "packageId");
            return (Criteria) this;
        }

        public Criteria andPackageIdNotBetween(Long value1, Long value2) {
            addCriterion("package_id not between", value1, value2, "packageId");
            return (Criteria) this;
        }

        public Criteria andOuterIdIsNull() {
            addCriterion("outer_id is null");
            return (Criteria) this;
        }

        public Criteria andOuterIdIsNotNull() {
            addCriterion("outer_id is not null");
            return (Criteria) this;
        }

        public Criteria andOuterIdEqualTo(String value) {
            addCriterion("outer_id =", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdNotEqualTo(String value) {
            addCriterion("outer_id <>", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdGreaterThan(String value) {
            addCriterion("outer_id >", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdGreaterThanOrEqualTo(String value) {
            addCriterion("outer_id >=", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdLessThan(String value) {
            addCriterion("outer_id <", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdLessThanOrEqualTo(String value) {
            addCriterion("outer_id <=", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdLike(String value) {
            addCriterion("outer_id like", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdNotLike(String value) {
            addCriterion("outer_id not like", value, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdIn(List<String> values) {
            addCriterion("outer_id in", values, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdNotIn(List<String> values) {
            addCriterion("outer_id not in", values, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdBetween(String value1, String value2) {
            addCriterion("outer_id between", value1, value2, "outerId");
            return (Criteria) this;
        }

        public Criteria andOuterIdNotBetween(String value1, String value2) {
            addCriterion("outer_id not between", value1, value2, "outerId");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andWhseCodeIsNull() {
            addCriterion("whse_code is null");
            return (Criteria) this;
        }

        public Criteria andWhseCodeIsNotNull() {
            addCriterion("whse_code is not null");
            return (Criteria) this;
        }

        public Criteria andWhseCodeEqualTo(String value) {
            addCriterion("whse_code =", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeNotEqualTo(String value) {
            addCriterion("whse_code <>", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeGreaterThan(String value) {
            addCriterion("whse_code >", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeGreaterThanOrEqualTo(String value) {
            addCriterion("whse_code >=", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeLessThan(String value) {
            addCriterion("whse_code <", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeLessThanOrEqualTo(String value) {
            addCriterion("whse_code <=", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeLike(String value) {
            addCriterion("whse_code like", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeNotLike(String value) {
            addCriterion("whse_code not like", value, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeIn(List<String> values) {
            addCriterion("whse_code in", values, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeNotIn(List<String> values) {
            addCriterion("whse_code not in", values, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeBetween(String value1, String value2) {
            addCriterion("whse_code between", value1, value2, "whseCode");
            return (Criteria) this;
        }

        public Criteria andWhseCodeNotBetween(String value1, String value2) {
            addCriterion("whse_code not between", value1, value2, "whseCode");
            return (Criteria) this;
        }

        public Criteria andSkuIdIsNull() {
            addCriterion("sku_id is null");
            return (Criteria) this;
        }

        public Criteria andSkuIdIsNotNull() {
            addCriterion("sku_id is not null");
            return (Criteria) this;
        }

        public Criteria andSkuIdEqualTo(String value) {
            addCriterion("sku_id =", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdNotEqualTo(String value) {
            addCriterion("sku_id <>", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdGreaterThan(String value) {
            addCriterion("sku_id >", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdGreaterThanOrEqualTo(String value) {
            addCriterion("sku_id >=", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdLessThan(String value) {
            addCriterion("sku_id <", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdLessThanOrEqualTo(String value) {
            addCriterion("sku_id <=", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdLike(String value) {
            addCriterion("sku_id like", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdNotLike(String value) {
            addCriterion("sku_id not like", value, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdIn(List<String> values) {
            addCriterion("sku_id in", values, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdNotIn(List<String> values) {
            addCriterion("sku_id not in", values, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdBetween(String value1, String value2) {
            addCriterion("sku_id between", value1, value2, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuIdNotBetween(String value1, String value2) {
            addCriterion("sku_id not between", value1, value2, "skuId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdIsNull() {
            addCriterion("sku_outer_id is null");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdIsNotNull() {
            addCriterion("sku_outer_id is not null");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdEqualTo(String value) {
            addCriterion("sku_outer_id =", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdNotEqualTo(String value) {
            addCriterion("sku_outer_id <>", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdGreaterThan(String value) {
            addCriterion("sku_outer_id >", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdGreaterThanOrEqualTo(String value) {
            addCriterion("sku_outer_id >=", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdLessThan(String value) {
            addCriterion("sku_outer_id <", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdLessThanOrEqualTo(String value) {
            addCriterion("sku_outer_id <=", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdLike(String value) {
            addCriterion("sku_outer_id like", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdNotLike(String value) {
            addCriterion("sku_outer_id not like", value, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdIn(List<String> values) {
            addCriterion("sku_outer_id in", values, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdNotIn(List<String> values) {
            addCriterion("sku_outer_id not in", values, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdBetween(String value1, String value2) {
            addCriterion("sku_outer_id between", value1, value2, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuOuterIdNotBetween(String value1, String value2) {
            addCriterion("sku_outer_id not between", value1, value2, "skuOuterId");
            return (Criteria) this;
        }

        public Criteria andSkuPriceIsNull() {
            addCriterion("sku_price is null");
            return (Criteria) this;
        }

        public Criteria andSkuPriceIsNotNull() {
            addCriterion("sku_price is not null");
            return (Criteria) this;
        }

        public Criteria andSkuPriceEqualTo(Long value) {
            addCriterion("sku_price =", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceNotEqualTo(Long value) {
            addCriterion("sku_price <>", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceGreaterThan(Long value) {
            addCriterion("sku_price >", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceGreaterThanOrEqualTo(Long value) {
            addCriterion("sku_price >=", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceLessThan(Long value) {
            addCriterion("sku_price <", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceLessThanOrEqualTo(Long value) {
            addCriterion("sku_price <=", value, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceIn(List<Long> values) {
            addCriterion("sku_price in", values, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceNotIn(List<Long> values) {
            addCriterion("sku_price not in", values, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceBetween(Long value1, Long value2) {
            addCriterion("sku_price between", value1, value2, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuPriceNotBetween(Long value1, Long value2) {
            addCriterion("sku_price not between", value1, value2, "skuPrice");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityIsNull() {
            addCriterion("sku_quantity is null");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityIsNotNull() {
            addCriterion("sku_quantity is not null");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityEqualTo(Integer value) {
            addCriterion("sku_quantity =", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityNotEqualTo(Integer value) {
            addCriterion("sku_quantity <>", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityGreaterThan(Integer value) {
            addCriterion("sku_quantity >", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityGreaterThanOrEqualTo(Integer value) {
            addCriterion("sku_quantity >=", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityLessThan(Integer value) {
            addCriterion("sku_quantity <", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityLessThanOrEqualTo(Integer value) {
            addCriterion("sku_quantity <=", value, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityIn(List<Integer> values) {
            addCriterion("sku_quantity in", values, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityNotIn(List<Integer> values) {
            addCriterion("sku_quantity not in", values, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityBetween(Integer value1, Integer value2) {
            addCriterion("sku_quantity between", value1, value2, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuQuantityNotBetween(Integer value1, Integer value2) {
            addCriterion("sku_quantity not between", value1, value2, "skuQuantity");
            return (Criteria) this;
        }

        public Criteria andSkuNameIsNull() {
            addCriterion("sku_name is null");
            return (Criteria) this;
        }

        public Criteria andSkuNameIsNotNull() {
            addCriterion("sku_name is not null");
            return (Criteria) this;
        }

        public Criteria andSkuNameEqualTo(String value) {
            addCriterion("sku_name =", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotEqualTo(String value) {
            addCriterion("sku_name <>", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameGreaterThan(String value) {
            addCriterion("sku_name >", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameGreaterThanOrEqualTo(String value) {
            addCriterion("sku_name >=", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLessThan(String value) {
            addCriterion("sku_name <", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLessThanOrEqualTo(String value) {
            addCriterion("sku_name <=", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameLike(String value) {
            addCriterion("sku_name like", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotLike(String value) {
            addCriterion("sku_name not like", value, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameIn(List<String> values) {
            addCriterion("sku_name in", values, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotIn(List<String> values) {
            addCriterion("sku_name not in", values, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameBetween(String value1, String value2) {
            addCriterion("sku_name between", value1, value2, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuNameNotBetween(String value1, String value2) {
            addCriterion("sku_name not between", value1, value2, "skuName");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyIsNull() {
            addCriterion("sku_property is null");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyIsNotNull() {
            addCriterion("sku_property is not null");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyEqualTo(String value) {
            addCriterion("sku_property =", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyNotEqualTo(String value) {
            addCriterion("sku_property <>", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyGreaterThan(String value) {
            addCriterion("sku_property >", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyGreaterThanOrEqualTo(String value) {
            addCriterion("sku_property >=", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyLessThan(String value) {
            addCriterion("sku_property <", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyLessThanOrEqualTo(String value) {
            addCriterion("sku_property <=", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyLike(String value) {
            addCriterion("sku_property like", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyNotLike(String value) {
            addCriterion("sku_property not like", value, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyIn(List<String> values) {
            addCriterion("sku_property in", values, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyNotIn(List<String> values) {
            addCriterion("sku_property not in", values, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyBetween(String value1, String value2) {
            addCriterion("sku_property between", value1, value2, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPropertyNotBetween(String value1, String value2) {
            addCriterion("sku_property not between", value1, value2, "skuProperty");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlIsNull() {
            addCriterion("sku_picture_url is null");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlIsNotNull() {
            addCriterion("sku_picture_url is not null");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlEqualTo(String value) {
            addCriterion("sku_picture_url =", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlNotEqualTo(String value) {
            addCriterion("sku_picture_url <>", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlGreaterThan(String value) {
            addCriterion("sku_picture_url >", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlGreaterThanOrEqualTo(String value) {
            addCriterion("sku_picture_url >=", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlLessThan(String value) {
            addCriterion("sku_picture_url <", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlLessThanOrEqualTo(String value) {
            addCriterion("sku_picture_url <=", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlLike(String value) {
            addCriterion("sku_picture_url like", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlNotLike(String value) {
            addCriterion("sku_picture_url not like", value, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlIn(List<String> values) {
            addCriterion("sku_picture_url in", values, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlNotIn(List<String> values) {
            addCriterion("sku_picture_url not in", values, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlBetween(String value1, String value2) {
            addCriterion("sku_picture_url between", value1, value2, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andSkuPictureUrlNotBetween(String value1, String value2) {
            addCriterion("sku_picture_url not between", value1, value2, "skuPictureUrl");
            return (Criteria) this;
        }

        public Criteria andNetWorthIsNull() {
            addCriterion("net_worth is null");
            return (Criteria) this;
        }

        public Criteria andNetWorthIsNotNull() {
            addCriterion("net_worth is not null");
            return (Criteria) this;
        }

        public Criteria andNetWorthEqualTo(BigDecimal value) {
            addCriterion("net_worth =", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotEqualTo(BigDecimal value) {
            addCriterion("net_worth <>", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthGreaterThan(BigDecimal value) {
            addCriterion("net_worth >", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("net_worth >=", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthLessThan(BigDecimal value) {
            addCriterion("net_worth <", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("net_worth <=", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthIn(List<BigDecimal> values) {
            addCriterion("net_worth in", values, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotIn(List<BigDecimal> values) {
            addCriterion("net_worth not in", values, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_worth between", value1, value2, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_worth not between", value1, value2, "netWorth");
            return (Criteria) this;
        }

        public Criteria andSupplierIdIsNull() {
            addCriterion("supplier_id is null");
            return (Criteria) this;
        }

        public Criteria andSupplierIdIsNotNull() {
            addCriterion("supplier_id is not null");
            return (Criteria) this;
        }

        public Criteria andSupplierIdEqualTo(Long value) {
            addCriterion("supplier_id =", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotEqualTo(Long value) {
            addCriterion("supplier_id <>", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdGreaterThan(Long value) {
            addCriterion("supplier_id >", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdGreaterThanOrEqualTo(Long value) {
            addCriterion("supplier_id >=", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdLessThan(Long value) {
            addCriterion("supplier_id <", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdLessThanOrEqualTo(Long value) {
            addCriterion("supplier_id <=", value, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdIn(List<Long> values) {
            addCriterion("supplier_id in", values, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotIn(List<Long> values) {
            addCriterion("supplier_id not in", values, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdBetween(Long value1, Long value2) {
            addCriterion("supplier_id between", value1, value2, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSupplierIdNotBetween(Long value1, Long value2) {
            addCriterion("supplier_id not between", value1, value2, "supplierId");
            return (Criteria) this;
        }

        public Criteria andSfairlineIsNull() {
            addCriterion("sfairline is null");
            return (Criteria) this;
        }

        public Criteria andSfairlineIsNotNull() {
            addCriterion("sfairline is not null");
            return (Criteria) this;
        }

        public Criteria andSfairlineEqualTo(Integer value) {
            addCriterion("sfairline =", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineNotEqualTo(Integer value) {
            addCriterion("sfairline <>", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineGreaterThan(Integer value) {
            addCriterion("sfairline >", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineGreaterThanOrEqualTo(Integer value) {
            addCriterion("sfairline >=", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineLessThan(Integer value) {
            addCriterion("sfairline <", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineLessThanOrEqualTo(Integer value) {
            addCriterion("sfairline <=", value, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineIn(List<Integer> values) {
            addCriterion("sfairline in", values, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineNotIn(List<Integer> values) {
            addCriterion("sfairline not in", values, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineBetween(Integer value1, Integer value2) {
            addCriterion("sfairline between", value1, value2, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfairlineNotBetween(Integer value1, Integer value2) {
            addCriterion("sfairline not between", value1, value2, "sfairline");
            return (Criteria) this;
        }

        public Criteria andSfshippingIsNull() {
            addCriterion("sfshipping is null");
            return (Criteria) this;
        }

        public Criteria andSfshippingIsNotNull() {
            addCriterion("sfshipping is not null");
            return (Criteria) this;
        }

        public Criteria andSfshippingEqualTo(Integer value) {
            addCriterion("sfshipping =", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingNotEqualTo(Integer value) {
            addCriterion("sfshipping <>", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingGreaterThan(Integer value) {
            addCriterion("sfshipping >", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingGreaterThanOrEqualTo(Integer value) {
            addCriterion("sfshipping >=", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingLessThan(Integer value) {
            addCriterion("sfshipping <", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingLessThanOrEqualTo(Integer value) {
            addCriterion("sfshipping <=", value, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingIn(List<Integer> values) {
            addCriterion("sfshipping in", values, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingNotIn(List<Integer> values) {
            addCriterion("sfshipping not in", values, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingBetween(Integer value1, Integer value2) {
            addCriterion("sfshipping between", value1, value2, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andSfshippingNotBetween(Integer value1, Integer value2) {
            addCriterion("sfshipping not between", value1, value2, "sfshipping");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberIsNull() {
            addCriterion("merchant_number is null");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberIsNotNull() {
            addCriterion("merchant_number is not null");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberEqualTo(Integer value) {
            addCriterion("merchant_number =", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberNotEqualTo(Integer value) {
            addCriterion("merchant_number <>", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberGreaterThan(Integer value) {
            addCriterion("merchant_number >", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("merchant_number >=", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberLessThan(Integer value) {
            addCriterion("merchant_number <", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberLessThanOrEqualTo(Integer value) {
            addCriterion("merchant_number <=", value, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberIn(List<Integer> values) {
            addCriterion("merchant_number in", values, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberNotIn(List<Integer> values) {
            addCriterion("merchant_number not in", values, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberBetween(Integer value1, Integer value2) {
            addCriterion("merchant_number between", value1, value2, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andMerchantNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("merchant_number not between", value1, value2, "merchantNumber");
            return (Criteria) this;
        }

        public Criteria andPromoAmtIsNull() {
            addCriterion("promo_amt is null");
            return (Criteria) this;
        }

        public Criteria andPromoAmtIsNotNull() {
            addCriterion("promo_amt is not null");
            return (Criteria) this;
        }

        public Criteria andPromoAmtEqualTo(BigDecimal value) {
            addCriterion("promo_amt =", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtNotEqualTo(BigDecimal value) {
            addCriterion("promo_amt <>", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtGreaterThan(BigDecimal value) {
            addCriterion("promo_amt >", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promo_amt >=", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtLessThan(BigDecimal value) {
            addCriterion("promo_amt <", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promo_amt <=", value, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtIn(List<BigDecimal> values) {
            addCriterion("promo_amt in", values, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtNotIn(List<BigDecimal> values) {
            addCriterion("promo_amt not in", values, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promo_amt between", value1, value2, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andPromoAmtNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promo_amt not between", value1, value2, "promoAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtIsNull() {
            addCriterion("server_amt is null");
            return (Criteria) this;
        }

        public Criteria andServerAmtIsNotNull() {
            addCriterion("server_amt is not null");
            return (Criteria) this;
        }

        public Criteria andServerAmtEqualTo(BigDecimal value) {
            addCriterion("server_amt =", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtNotEqualTo(BigDecimal value) {
            addCriterion("server_amt <>", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtGreaterThan(BigDecimal value) {
            addCriterion("server_amt >", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("server_amt >=", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtLessThan(BigDecimal value) {
            addCriterion("server_amt <", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtLessThanOrEqualTo(BigDecimal value) {
            addCriterion("server_amt <=", value, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtIn(List<BigDecimal> values) {
            addCriterion("server_amt in", values, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtNotIn(List<BigDecimal> values) {
            addCriterion("server_amt not in", values, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("server_amt between", value1, value2, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andServerAmtNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("server_amt not between", value1, value2, "serverAmt");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionIsNull() {
            addCriterion("delivery_region is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionIsNotNull() {
            addCriterion("delivery_region is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionEqualTo(String value) {
            addCriterion("delivery_region =", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionNotEqualTo(String value) {
            addCriterion("delivery_region <>", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionGreaterThan(String value) {
            addCriterion("delivery_region >", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionGreaterThanOrEqualTo(String value) {
            addCriterion("delivery_region >=", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionLessThan(String value) {
            addCriterion("delivery_region <", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionLessThanOrEqualTo(String value) {
            addCriterion("delivery_region <=", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionLike(String value) {
            addCriterion("delivery_region like", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionNotLike(String value) {
            addCriterion("delivery_region not like", value, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionIn(List<String> values) {
            addCriterion("delivery_region in", values, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionNotIn(List<String> values) {
            addCriterion("delivery_region not in", values, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionBetween(String value1, String value2) {
            addCriterion("delivery_region between", value1, value2, "deliveryRegion");
            return (Criteria) this;
        }

        public Criteria andDeliveryRegionNotBetween(String value1, String value2) {
            addCriterion("delivery_region not between", value1, value2, "deliveryRegion");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}