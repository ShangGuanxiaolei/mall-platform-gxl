package com.xquark.dal.vo;

import java.util.List;

/**
 * 预售活动类目
 *
 * @author: yyc
 * @date: 19-4-16 下午3:15
 */
public class ReserveCategoryVO {

  /** 预售活动类目名 */
  private String categoryName;
  /** 该类目下的商品 */
  private List<ReserveProductVO> reserveProductVOS;

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public List<ReserveProductVO> getReserveProductVOS() {
    return reserveProductVOS;
  }

  public void setReserveProductVOS(List<ReserveProductVO> reserveProductVOS) {
    this.reserveProductVOS = reserveProductVOS;
  }
}
