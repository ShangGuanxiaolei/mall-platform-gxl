package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.model.PromotionBargainHistory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PromotionBargainDetailUserVO extends PromotionBargainDetail {

    private String img;

    private String productName;

    private Date validFrom;

    private Date validTo;

    /**
     * 活动库存
     */
    private int stock;

    /**
     * 原价
     */
    private BigDecimal originalPrice;

    /**
     * 德分抵扣
     */
    private BigDecimal deductionDPoint;

    private List<PromotionBargainHistory> bargainHistoryList;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public List<PromotionBargainHistory> getBargainHistoryList() {
        return bargainHistoryList;
    }

    public void setBargainHistoryList(List<PromotionBargainHistory> bargainHistoryList) {
        this.bargainHistoryList = bargainHistoryList;
    }
}
