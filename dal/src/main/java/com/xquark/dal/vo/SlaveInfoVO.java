package com.xquark.dal.vo;

/**
 * @author wangxinhua.
 * @date 2018/10/7
 */
public class SlaveInfoVO {

  private String id;

  private Integer number;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }
}
