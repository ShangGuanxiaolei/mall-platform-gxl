package com.xquark.dal.vo;

import com.xquark.dal.model.Combination;
import java.util.List;

/**
 * User: byy Date: 18-6-1. Time: 上午10:35 组合商品VO
 */
public class CombinationVO extends Combination {
  //组合商品
  private List<CombinationItemVO> combinations;

  public List<CombinationItemVO> getCombinations() {
    return combinations;
  }

  public void setCombinations(List<CombinationItemVO> combinations) {
    this.combinations = combinations;
  }

  @Override
  public String toString() {
    return super.toString()+"CombinationVO{" +
        "combinations=" + combinations +
        '}';
  }
}
