package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.Date;

public class XQHomeActProductVO {

  private String productId;  //	商品ID
  private BigDecimal oldPrice;  // 原价
  private BigDecimal actPrice;  //	活动价
  private String productTitle; //	商品名称
  private String productBrand;//	商品品牌
  private String shortName;//	商品短名称
  private String auditReason; //	推荐理由
  private Date startTime; //	开始时间
  private Date endTime; //	结束时间
  private Integer soldOut; //	1=在售，2=售罄
  private Integer sort;
  private Integer actTagType;
  private String actTagImage; // 活动TAG
  private String imagePc;
  private String imageApp;

  public String getImageApp() {
    return imageApp;
  }

  public void setImageApp(String imageApp) {
    this.imageApp = imageApp;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getOldPrice() {
    return oldPrice;
  }

  public void setOldPrice(BigDecimal oldPrice) {
    this.oldPrice = oldPrice;
  }

  public BigDecimal getActPrice() {
    return actPrice;
  }

  public void setActPrice(BigDecimal actPrice) {
    this.actPrice = actPrice;
  }

  public String getProductTitle() {
    return productTitle;
  }

  public void setProductTitle(String productTitle) {
    this.productTitle = productTitle;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Integer getSoldOut() {
    return soldOut;
  }

  public void setSoldOut(Integer soldOut) {
    this.soldOut = soldOut;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getProductBrand() {
    return productBrand;
  }

  public void setProductBrand(String productBrand) {
    this.productBrand = productBrand;
  }

  public String getActTagImage() {
    return actTagImage;
  }

  public void setActTagImage(String actTagImage) {
    this.actTagImage = actTagImage;
  }

  public Integer getActTagType() {
    return actTagType;
  }

  public void setActTagType(Integer actTagType) {
    this.actTagType = actTagType;
  }

  public String getAuditReason() {
    return auditReason;
  }

  public void setAuditReason(String auditReason) {
    this.auditReason = auditReason;
  }

  public String getImagePc() {
    return imagePc;
  }

  public void setImagePc(String imagePc) {
    this.imagePc = imagePc;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

}
