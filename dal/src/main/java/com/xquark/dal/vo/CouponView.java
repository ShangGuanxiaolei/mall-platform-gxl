package com.xquark.dal.vo;

import com.xquark.dal.type.PromotionType;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/16. DESC: 供展示用的优惠券列表
 */
public interface CouponView extends Comparable<CouponView> {

  String getId();

  String getCouponName();

  BigDecimal getDiscount();

  PromotionType getPromotionType();

  /**
   * 显示优惠中文名称
   */
  String getCouponType();

  String getUuId();

  /**
   * 使用该优惠是否包邮
   */
  Boolean getIsFreeDelivery();

}
