package com.xquark.dal.vo;

import com.xquark.dal.model.Commission;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.math.BigDecimal;

public class CommissionVO extends Commission {

  private static final long serialVersionUID = 1L;

  private String orderNo;
  private String orderStatus;
  private String productName;
  private String skuName;
  private String userName;
  private String partner;
  private String userPhone;

  private BigDecimal orderFee;

  private String createdAtStr;

  private String typeStr;

  private String statusStr;

  public String getStatusStr() {
    if (this.getStatus() == null) {
      return "";
    }
    String str = "";
    switch (this.getStatus()) {
      case NEW:
        str = "未结算";
        break;
      case SUCCESS:
        str = "已结算";
        break;
      case CLOSED:
        str = "已关闭";
        break;
      default:
        str = this.getType().toString();
        break;
    }
    return str;
  }

  public String getTypeStr() {
    if (this.getType() == null) {
      return "";
    }
    String str = "";
    switch (this.getType()) {
      case PRODUCT:
        str = "推客购物佣金";
        break;
      case ONECOMMISSION:
        str = "一级分佣佣金";
        break;
      case TWOCOMMISSION:
        str = "二级分佣佣金";
        break;
      case THREECOMMISSION:
        str = "三级分佣佣金";
        break;
      case TEAM:
        str = "团队合伙人佣金";
        break;
      case SHAREHOLDER:
        str = "股东合伙人佣金";
        break;
      case TEAM_LEADER:
        str = "战队队长佣金";
        break;
      case TEAM_MEMBER:
        str = "战队成员佣金";
        break;
      default:
        str = this.getType().toString();
        break;
    }
    return str;
  }

  public void setTypeStr(String typeStr) {
    this.typeStr = typeStr;
  }

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public BigDecimal getOrderFee() {
    return orderFee;
  }

  public void setOrderFee(BigDecimal orderFee) {
    this.orderFee = orderFee;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getSkuName() {
    return skuName;
  }

  public void setSkuName(String skuName) {
    this.skuName = skuName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getPartner() {
    return partner;
  }

  public void setPartner(String partner) {
    this.partner = partner;
  }
}
