package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PromotionBargainHistory;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-7-18. DESC:
 */
public class PromotionBargainHistoryVO extends PromotionBargainHistory {

  private String userId;

  private String userName;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String avatar;

  private String skuId;

  private BigDecimal priceStart;

  private BigDecimal priceEnd;

  private Date validFrom;

  private Date validTo;

  private String img;

  private String productName;

  @Override
  public String getUserId() {
    return userId;
  }

  @Override
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public BigDecimal getPriceStart() {
    return priceStart;
  }

  public void setPriceStart(BigDecimal priceStart) {
    this.priceStart = priceStart;
  }

  public BigDecimal getPriceEnd() {
    return priceEnd;
  }

  public void setPriceEnd(BigDecimal priceEnd) {
    this.priceEnd = priceEnd;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }
}
