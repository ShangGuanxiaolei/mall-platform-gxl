package com.xquark.dal.vo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/2/22
 * Time: 15:40
 * Description: 用户带手机号
 */
public class WxVO extends BaseCustomerVO {
    private  String phone;
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

}
