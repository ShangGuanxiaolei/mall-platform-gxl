package com.xquark.dal.vo;

import java.math.BigDecimal;

/**
 * @author liuwei
 * @date 18-7-26 下午3:06
 */
public class PointExportByOrderVO {
    private Integer orderMonth;
    private String payeeId;
    private String payeeName;
    private String purchaseName;
    private String purchaseId;
    private String orderId;
    private String ctryEarn;
    private String trancd;
    private BigDecimal earnBase;
    private BigDecimal earn;
    private BigDecimal amtEarn;
    private String distrType;
    private String ctryPaid;
    private BigDecimal amtPaid;
    private String remark;

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Integer getOrderMonth() {
        return orderMonth;
    }

    public void setOrderMonth(Integer orderMonth) {
        this.orderMonth = orderMonth;
    }

    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPurchaseName() {
        return purchaseName;
    }

    public void setPurchaseName(String purchaseName) {
        this.purchaseName = purchaseName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCtryEarn() {
        return ctryEarn;
    }

    public void setCtryEarn(String ctryEarn) {
        this.ctryEarn = ctryEarn;
    }

    public String getTrancd() {
        return trancd;
    }

    public void setTrancd(String trancd) {
        this.trancd = trancd;
    }

    public String getEarnBase() {
        if(earnBase != null){
            return String.format("%.2f",earnBase);
        }
        return null;
    }

    public void setEarnBase(BigDecimal earnBase) {
        this.earnBase = earnBase;
    }

    public String getEarn() {
        if(earn != null) {
            return String.format("%.2f", earn);
        }
        return null;
    }

    public void setEarn(BigDecimal earn) {
        this.earn = earn;
    }

    public String getAmtEarn() {
        if(amtEarn != null) {
            return String.format("%.2f", amtEarn);
        }
        return null;
    }

    public void setAmtEarn(BigDecimal amtEarn) {
        this.amtEarn = amtEarn;
    }

    public String getDistrType() {
        return distrType;
    }

    public void setDistrType(String distrType) {
        this.distrType = distrType;
    }

    public String getCrtyPaid() {
        return ctryPaid;
    }

    public void setCrtyPaid(String crtyPaid) {
        this.ctryPaid = crtyPaid;
    }

    public String getAmtPaid() {
        if(amtPaid != null) {
            return String.format("%.2f", amtPaid);
        }
        return null;
    }

    public void setAmtPaid(BigDecimal amtPaid) {
        this.amtPaid = amtPaid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
