package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * @author wangxinhua
 * @date 2019-05-17
 * @since 1.0
 */
public class WinningProductVO {

    private String productId;

    private String skuId;

    private Long winningCpId;

    private String name;

    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;

    private BigDecimal price;

    private String skuCodeResources;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Long getWinningCpId() {
        return winningCpId;
    }

    public void setWinningCpId(Long winningCpId) {
        this.winningCpId = winningCpId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSkuCodeResources() {
        return skuCodeResources;
    }

    public void setSkuCodeResources(String skuCodeResources) {
        this.skuCodeResources = skuCodeResources;
    }
}
