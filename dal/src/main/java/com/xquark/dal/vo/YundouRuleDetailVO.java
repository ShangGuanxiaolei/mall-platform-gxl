package com.xquark.dal.vo;

import com.xquark.dal.model.YundouOperationDetail;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * Created by wangxinhua on 17-8-16. DESC: 规则明细VO
 */
public class YundouRuleDetailVO extends YundouOperationDetail {

  private String ruleName;

  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getAmountStr() {
    Long amount = this.getAmount();
    if (this.getAmount() != null) {
      return String.valueOf(amount);
    }
    return "0";
  }
}
