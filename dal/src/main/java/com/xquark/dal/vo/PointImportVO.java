package com.xquark.dal.vo;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author wangxinhua on 2018/7/28. DESC:
 */
public class PointImportVO {

  private String customerNumber;

  private BigDecimal commission;

  private Integer commSource;

  private BigDecimal point;

  private Integer pointSource;

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public BigDecimal getCommission() {
    return commission;
  }

  public void setCommission(BigDecimal commission) {
    this.commission = commission;
  }

  public Integer getCommSource() {
    return commSource;
  }

  public void setCommSource(Integer commSource) {
    this.commSource = commSource;
  }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public Integer getPointSource() {
    return pointSource;
  }

  public void setPointSource(Integer pointSource) {
    this.pointSource = pointSource;
  }

  public Long getCpId() {
    if (StringUtils.isBlank(customerNumber)) {
      return null;
    }
    Long cpId = null;
    try {
      // trim space first
      String trimed = StringUtils.trim(customerNumber);
      cpId = Long.valueOf(StringUtils.stripStart(trimed, "0"));
    } catch (Exception e) {
      // do nothing
      e.printStackTrace();
    }
    return cpId;
  }

  @Override
  public String toString() {
    return "PointImportVO{" +
        "customerNumber='" + customerNumber + '\'' +
        ", commission=" + commission +
        ", commSource=" + commSource +
        ", point=" + point +
        ", pointSource=" + pointSource +
        '}';
  }
}
