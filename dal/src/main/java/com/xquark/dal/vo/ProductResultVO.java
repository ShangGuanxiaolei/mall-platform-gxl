package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-7-13. DESC:
 */
public class ProductResultVO implements Serializable {

  private String id;
  private String name; // 商品名称

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;  // 主图code

  private ProductStatus status;
  private BigDecimal price; //最低价格
  private Integer sales;//产品销
  private Integer amount;//库存

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Integer getSales() {
    return sales;
  }

  public void setSales(Integer sales) {
    this.sales = sales;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getTargetUrl() {
    return "http://nffclub.com/p/" + this.id;
  }
}
