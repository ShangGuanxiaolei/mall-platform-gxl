package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.type.UserCouponType;
import com.xquark.dal.util.json.JsonDecimalTrailZeroSerializer;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 18-4-4. DESC:
 */
public class BasePromotionCouponVO {

  private String id;

  private String name;

  private UserCouponType scope;

  private CouponStatus status;

  private Integer acquireLimit;

  private Long acquiredNum;

  @JsonSerialize(using = JsonDecimalTrailZeroSerializer.class)
  private BigDecimal discount;

  @JsonSerialize(using = JsonDecimalTrailZeroSerializer.class)
  private BigDecimal applyAbove;

  private String productId;

  private String categoryId;

  private Date validFrom;

  private Date validTo;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UserCouponType getScope() {
    return scope;
  }

  public void setScope(UserCouponType scope) {
    this.scope = scope;
  }

  public CouponStatus getStatus() {
    return status;
  }

  public void setStatus(CouponStatus status) {
    this.status = status;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public BigDecimal getApplyAbove() {
    return applyAbove;
  }

  public void setApplyAbove(BigDecimal applyAbove) {
    this.applyAbove = applyAbove;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }


  /**
   * 是否已领取该优惠券
   */
  public Boolean getIsAcquired() {
    return acquiredNum > 0;
  }

  public Boolean getReachLimit() {
    if (acquireLimit == null || acquiredNum == null) {
      return false;
    }
    return Long.valueOf(acquireLimit).equals(acquiredNum);
  }

  public String getApplyAboveStr() {
    BigDecimal applyAbove = getApplyAbove();
    if (applyAbove == null) {
      applyAbove = BigDecimal.ZERO;
    }
    BigDecimal formatted = applyAbove.setScale(2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
    return String.format("满%s使用", formatted.toPlainString());
  }

  public Integer getAcquireLimit() {
    return acquireLimit;
  }

  public void setAcquireLimit(Integer acquireLimit) {
    this.acquireLimit = acquireLimit;
  }

  public Long getAcquiredNum() {
    return acquiredNum;
  }

  public void setAcquiredNum(Long acquiredNum) {
    this.acquiredNum = acquiredNum;
  }

}
