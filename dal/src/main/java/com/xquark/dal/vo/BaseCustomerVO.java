package com.xquark.dal.vo;

import com.xquark.dal.type.PlatformType;
import org.apache.commons.lang3.StringUtils;

/**
 * created by
 *
 * @author wangxinhua at 18-6-25 下午11:17
 */
public class BaseCustomerVO {

  private Long cpId;

  private String customerNumber;

  private String nickName;

  private Integer source;

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getCustomerNumber() {
    if (StringUtils.isBlank(customerNumber)) {
      return "";
    }
    PlatformType platform = getPlatForm();
    if (platform == null || platform == PlatformType.E) {
      return customerNumber;
    }
    return customerNumber + "(" + platform.name() + ")";
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public Integer getSource() {
    return source;
  }

  public void setSource(Integer source) {
    this.source = source;
  }

  public PlatformType getPlatForm() {
    if (source == null) {
      return null;
    }
    return PlatformType.fromCode(source);
  }
}
