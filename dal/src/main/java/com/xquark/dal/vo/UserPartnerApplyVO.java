package com.xquark.dal.vo;

import com.xquark.dal.model.UserPartner;

/**
 * Created by chh on 16-11-3.
 */
public class UserPartnerApplyVO extends UserPartner {

  private String name;
  private String phone;
  private String shopName;

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
