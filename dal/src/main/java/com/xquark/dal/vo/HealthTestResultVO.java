package com.xquark.dal.vo;

import com.xquark.dal.model.HealthTestResult;
import com.xquark.dal.model.Product;

import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
public class HealthTestResultVO extends HealthTestResult {

  private String moduleName;

  private List<Product> products;

  @Override
  public String getId() {
    return super.getId();
  }

  @Override
  public void setId(String id) {
    super.setId(id);
  }

  @Override
  public String getModuleId() {
    return super.getModuleId();
  }

  @Override
  public void setModuleId(String moduleId) {
    super.setModuleId(moduleId);
  }

  @Override
  public String getPhysiqueId() {
    return super.getPhysiqueId();
  }

  @Override
  public void setPhysiqueId(String physiqueId) {
    super.setPhysiqueId(physiqueId);
  }

  @Override
  public Double getStart() {
    return super.getStart();
  }

  @Override
  public void setStart(Double start) {
    super.setStart(start);
  }

  @Override
  public Double getEnd() {
    return super.getEnd();
  }

  @Override
  public void setEnd(Double end) {
    super.setEnd(end);
  }

  @Override
  public String getType() {
    return super.getType();
  }

  @Override
  public void setType(String type) {
    super.setType(type);
  }

  @Override
  public Date getCreatedAt() {
    return super.getCreatedAt();
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    super.setCreatedAt(createdAt);
  }

  @Override
  public Date getUpdatedAt() {
    return super.getUpdatedAt();
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    super.setUpdatedAt(updatedAt);
  }

  @Override
  public Boolean getArchive() {
    return super.getArchive();
  }

  @Override
  public void setArchive(Boolean archive) {
    super.setArchive(archive);
  }

  @Override
  public String getDescription() {
    return super.getDescription();
  }

  @Override
  public void setDescription(String description) {
    super.setDescription(description);
  }

  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public List<Product> getProducts() {
    return products;
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }
}
