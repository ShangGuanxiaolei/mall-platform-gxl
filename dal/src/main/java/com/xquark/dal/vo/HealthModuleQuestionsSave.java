package com.xquark.dal.vo;

import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestQuestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-12. DESC:
 */
public class HealthModuleQuestionsSave {

  private HealthTestModule module;
  private List<HealthTestQuestion> questionList = new ArrayList<HealthTestQuestion>();

  public HealthModuleQuestionsSave() {
  }

  public HealthModuleQuestionsSave(HealthTestModule module) {
    this.module = module;
  }

  public HealthTestModule getModule() {
    return module;
  }

  public void setModule(HealthTestModule module) {
    this.module = module;
  }

  public List<HealthTestQuestion> getQuestionList() {
    return questionList;
  }

  public void setQuestionList(List<HealthTestQuestion> questionList) {
    this.questionList = questionList;
  }
}
