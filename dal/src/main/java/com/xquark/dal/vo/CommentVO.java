package com.xquark.dal.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by jitre on 11/9/17. 用于在web端展示的评论数据
 */
public class CommentVO {

  private String id;
  private String userId;
  private String avatar;
  private String loginname;
  private Date createdAt;
  private String content;
  private Integer likeCount;
  private Integer isLike;
  private List<CommentReplyVO> replyList;

  public Integer getIsLike() {
    return isLike;
  }

  public void setIsLike(Integer isLike) {
    this.isLike = isLike;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getLoginname() {
    return loginname;
  }

  public void setLoginname(String loginname) {
    this.loginname = loginname;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getLikeCount() {
    return likeCount;
  }

  public void setLikeCount(Integer likeCount) {
    this.likeCount = likeCount;
  }

  public List<CommentReplyVO> getReplyList() {
    return replyList;
  }

  public void setReplyList(List<CommentReplyVO> replyList) {
    this.replyList = replyList;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
