package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;

/**
 * Created by chh on 18/01/16.
 */
public class PrivilegeCodeVO extends PrivilegeCode {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String validFromStr;

  private String validToStr;

  public String getValidFromStr() {
    if (this.getValidFrom() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidFrom(), "yyyy-MM-dd");
  }

  public void setValidFromStr(String validFromStr) {
    this.validFromStr = validFromStr;
  }

  public String getValidToStr() {
    if (this.getValidTo() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidTo(), "yyyy-MM-dd");
  }

  public void setValidToStr(String validToStr) {
    this.validToStr = validToStr;
  }

}
