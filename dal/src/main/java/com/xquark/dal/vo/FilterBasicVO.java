/**
 * Copyright (C), 2015-2018, XXX有限公司 FileName: FilterBasicVO Author:   rwx Date:     18-2-27 下午2:19
 * Description: History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.xquark.dal.vo;

import com.xquark.dal.status.FilterSpec;

/**
 * @author rwx
 * @create 18-2-27
 * @since 1.0.0
 */
public class FilterBasicVO extends ProductBasicVO {
  // 滤芯专有属性

  private Integer level;

  private FilterSpec filterSpec;

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  public FilterSpec getFilterSpec() {
    return filterSpec;
  }

  public void setFilterSpec(FilterSpec filterSpec) {
    this.filterSpec = filterSpec;
  }
}