package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * User: kong Date: 2018/6/16. Time: 14:24 菠萝派订单
 */
public class PolyapiOrderItemVO {
  //订单id
  private String orderId;
  //订单号
  private String platOrderNo;
  //订单交易状态
  private String tradeStatus;
  //订单交易说明
  private String tradeStatusDescription;
  //交易时间
  private String tradeTime;
  //支付单号
  private String payOrderNo;
  //州/省
  private String province;
  //城市
  private String city;
  //区县
  private String area;
  //镇/街道
  private String town;
  //地址
  private String address;
  //邮编
  private String zip;
  //电话（电话、手机必填一个）
  private String phone;
  //手机（电话、手机必填一个）
  private String mobile;
  //Email
  private String email;
  //买家备注
  private String customerRemark;
  //卖家备注
  private String sellerRemark;
  //邮资
  private BigDecimal postFee;
  //货款金额
  private BigDecimal goodsfee = new BigDecimal(0);
  //合计应收（针对卖家）
  private BigDecimal totalMoney = new BigDecimal(0);
  //订单总优惠金额
  private BigDecimal favourableMoney = new BigDecimal(0);
  //佣金
  private BigDecimal commissionValue;
  //订单税费总额
  private BigDecimal taxAmount;
  //订单关税金额
  private BigDecimal tariffAmount;
  //订单增值税金额
  private BigDecimal addedValueAmount;
  //订单消费税金额
  private BigDecimal consumptionDutyAmount;
  //货运方式
  private String sendStyle;
  //支付时间
  private String payTime;
  //发票抬头
  private String invoiceTitle;
  //纳税人识别号
  private String taxPayerIdent;
  //COD服务费
  private String codServiceFee;
  //货币类型
  private String currencyCode;
  //证件类型
  private String cardType;
  //证件号码
  private String idCard;
  //证件真实姓名
  private String idCardTruename;
  //收货人真实姓名
  private String receiverName;
  //买家昵称
  private String nick;
  //是否为海外购
  private Integer isHwgFlag;
  //结算方式
  private String shouldPayType;
  //商品信息集合
  private List<PolyapiGoodInfoVO> goodInfos;

  public String getPlatOrderNo() {
    return platOrderNo;
  }

  public void setPlatOrderNo(String platOrderNo) {
    this.platOrderNo = platOrderNo;
  }

  public String getTradeStatus() {
    return tradeStatus;
  }

  public void setTradeStatus(String tradeStatus) {
    this.tradeStatus = tradeStatus;
  }

  public String getTradeStatusDescription() {
    return tradeStatusDescription;
  }

  public void setTradeStatusDescription(String tradeStatusDescription) {
    this.tradeStatusDescription = tradeStatusDescription;
  }

  public BigDecimal getGoodsfee() {
    return goodsfee;
  }

  public String getTradeTime() {
    return tradeTime;
  }

  public void setTradeTime(String tradeTime) {
    this.tradeTime = tradeTime.substring(0,19);
  }

  public String getPayOrderNo() {
    return payOrderNo;
  }

  public void setPayOrderNo(String payOrderNo) {
    this.payOrderNo = payOrderNo;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCustomerRemark() {
    return customerRemark;
  }

  public void setCustomerRemark(String customerRemark) {
    this.customerRemark = customerRemark;
  }

  public String getSellerRemark() {
    return sellerRemark;
  }

  public void setSellerRemark(String sellerRemark) {
    this.sellerRemark = sellerRemark;
  }

  public BigDecimal getPostFee() {
    return postFee;
  }

  public void setPostFee(BigDecimal postFee) {
    this.postFee = postFee;
  }

  public BigDecimal getTotalMoney() {
    return totalMoney;
  }

  public BigDecimal getFavourableMoney() {
    return favourableMoney;
  }

  public BigDecimal getCommissionValue() {
    return commissionValue;
  }

  public void setCommissionValue(BigDecimal commissionValue) {
    this.commissionValue = commissionValue;
  }

  public BigDecimal getTaxAmount() {
    return taxAmount;
  }

  public void setTaxAmount(BigDecimal taxAmount) {
    this.taxAmount = taxAmount;
  }

  public BigDecimal getTariffAmount() {
    return tariffAmount;
  }

  public void setTariffAmount(BigDecimal tariffAmount) {
    this.tariffAmount = tariffAmount;
  }

  public BigDecimal getAddedValueAmount() {
    return addedValueAmount;
  }

  public void setAddedValueAmount(BigDecimal addedValueAmount) {
    this.addedValueAmount = addedValueAmount;
  }

  public BigDecimal getConsumptionDutyAmount() {
    return consumptionDutyAmount;
  }

  public void setConsumptionDutyAmount(BigDecimal consumptionDutyAmount) {
    this.consumptionDutyAmount = consumptionDutyAmount;
  }

  public String getSendStyle() {
    return sendStyle;
  }

  public void setSendStyle(String sendStyle) {
    this.sendStyle = sendStyle;
  }

  public String getPayTime() {
    return payTime;
  }

  public void setPayTime(String payTime) {
    this.payTime = payTime.substring(0,19);
  }

  public String getInvoiceTitle() {
    return invoiceTitle;
  }

  public void setInvoiceTitle(String invoiceTitle) {
    this.invoiceTitle = invoiceTitle;
  }

  public String getTaxPayerIdent() {
    return taxPayerIdent;
  }

  public void setTaxPayerIdent(String taxPayerIdent) {
    this.taxPayerIdent = taxPayerIdent;
  }

  public String getCodServiceFee() {
    return codServiceFee;
  }

  public void setCodServiceFee(String codServiceFee) {
    this.codServiceFee = codServiceFee;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public String getIdCard() {
    return idCard;
  }

  public void setIdCard(String idCard) {
    this.idCard = idCard;
  }

  public String getIdCardTruename() {
    return idCardTruename;
  }

  public void setIdCardTruename(String idCardTruename) {
    this.idCardTruename = idCardTruename;
  }

  public String getReceiverName() {
    return receiverName;
  }

  public void setReceiverName(String receiverName) {
    this.receiverName = receiverName;
  }

  public String getNick() {
    return nick;
  }

  public void setNick(String nick) {
    this.nick = nick;
  }

  public Integer getIsHwgFlag() {
    return isHwgFlag;
  }

  public void setIsHwgFlag(Integer isHwgFlag) {
    this.isHwgFlag = isHwgFlag;
  }

  public String getShouldPayType() {
    return shouldPayType;
  }

  public void setShouldPayType(String shouldPayType) {
    this.shouldPayType = shouldPayType;
  }

  public List<PolyapiGoodInfoVO> getGoodInfos() {
    return goodInfos;
  }

  public void setGoodInfos(List<PolyapiGoodInfoVO> goodInfos) {
    this.goodInfos = goodInfos;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

}
