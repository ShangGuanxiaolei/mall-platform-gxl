package com.xquark.dal.vo;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.beans.BeanUtils;

/**
 * Created by wangxinhua on 2018/4/12. DESC: 供后台商品选择列表使用的数据结构
 */
public class ProductSelectVO extends ProductBasicVO {

  public ProductSelectVO(ProductBasicVO source, boolean isSelect) {
    checkNotNull(source);
    BeanUtils.copyProperties(source, this);
    this.isSelect = isSelect;
  }

  private boolean isSelect;

  public boolean getIsSelect() {
    return isSelect;
  }

  public void setIsSelect(boolean select) {
    isSelect = select;
  }
}
