package com.xquark.dal.vo;

import com.xquark.dal.type.OrderType;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-9-27. DESC:
 */
public class PromotionReduceOrderVO extends PromotionReduceDetailVO {

  private String buyId;
  private String buyerName;

  private String sellerId;
  private String sellerName;

  private String buyerPhone;
  private String sellerPhone;

  private String orderId;
  private String orderNo; // 交易订单号
  private OrderType orderType;
  private BigDecimal goodsFee; // 订单原价
  private BigDecimal totalFee; // 订单总金额 totalFee = goodsFee + logisticsFee - discountFee totalFee = paidFee

  public String getSellerId() {
    return sellerId;
  }

  public void setSellerId(String sellerId) {
    this.sellerId = sellerId;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public String getBuyId() {
    return buyId;
  }

  public void setBuyId(String buyId) {
    this.buyId = buyId;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public OrderType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

}
