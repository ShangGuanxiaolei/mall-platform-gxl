package com.xquark.dal.vo;

import com.xquark.dal.model.MerchantPermission;
import com.xquark.dal.model.MerchantRole;

import java.util.List;

/**
 * Created by chh on 16/12/30.
 */
public class MerchantRoleVO extends MerchantRole {

  private boolean isSelect;

  private List<MerchantPermission> merchantPermissions;

  public List<MerchantPermission> getMerchantPermissions() {
    return merchantPermissions;
  }

  public void setMerchantPermissions(List<MerchantPermission> merchantPermissions) {
    this.merchantPermissions = merchantPermissions;
  }

  public boolean getIsSelect() {
    return isSelect;
  }

  public void setIsSelect(boolean select) {
    isSelect = select;
  }

}
