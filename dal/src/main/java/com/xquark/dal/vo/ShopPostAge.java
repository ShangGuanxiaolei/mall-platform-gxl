package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.List;

public class ShopPostAge {

  private String shopId;
  private Boolean postageStatus; // 店铺邮费开关
  private BigDecimal postage; // 店铺全局邮费
  private int defaultFirst;
  private int defaultSecond;
  private BigDecimal defaultFirstFee;
  private BigDecimal defaultSecondFee;
  private String id;
  private String name;

  private String type;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getDefaultFirst() {
    return defaultFirst;
  }

  public void setDefaultFirst(int defaultFirst) {
    this.defaultFirst = defaultFirst;
  }

  public int getDefaultSecond() {
    return defaultSecond;
  }

  public void setDefaultSecond(int defaultSecond) {
    this.defaultSecond = defaultSecond;
  }

  public BigDecimal getDefaultFirstFee() {
    return defaultFirstFee;
  }

  public void setDefaultFirstFee(BigDecimal defaultFirstFee) {
    this.defaultFirstFee = defaultFirstFee;
  }

  public BigDecimal getDefaultSecondFee() {
    return defaultSecondFee;
  }

  public void setDefaultSecondFee(BigDecimal defaultSecondFee) {
    this.defaultSecondFee = defaultSecondFee;
  }

  private BigDecimal freeShippingPrice; // 指定包邮订单金额
  private Boolean freeShippingFirstPay; // 首次付款包邮
  private List<String> freeShippingGoods; // 指定包邮商品列表
  private List<AreaPostagePair> customizedPostage; // 自定义邮费地区设置

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public BigDecimal getFreeShippingPrice() {
    return freeShippingPrice;
  }

  public void setFreeShippingPrice(BigDecimal freeShippingPrice) {
    this.freeShippingPrice = freeShippingPrice;
  }

  public List<String> getFreeShippingGoods() {
    return freeShippingGoods;
  }

  public void setFreeShippingGoods(List<String> freeShippingGoods) {
    this.freeShippingGoods = freeShippingGoods;
  }

  public List<AreaPostagePair> getCustomizedPostage() {
    return customizedPostage;
  }

  public void setCustomizedPostage(List<AreaPostagePair> customizedPostage) {
    this.customizedPostage = customizedPostage;
  }

  public Boolean getPostageStatus() {
    return postageStatus;
  }

  public void setPostageStatus(Boolean postageStatus) {
    this.postageStatus = postageStatus;
  }

  public BigDecimal getPostage() {
    return postage;
  }

  public void setPostage(BigDecimal postage) {
    this.postage = postage;
  }

  public Boolean getFreeShippingFirstPay() {
    if (freeShippingFirstPay == null) {
      return false;
    }
    return freeShippingFirstPay;
  }

  public void setFreeShippingFirstPay(Boolean freeShippingFirstPay) {
    this.freeShippingFirstPay = freeShippingFirstPay;
  }
}
