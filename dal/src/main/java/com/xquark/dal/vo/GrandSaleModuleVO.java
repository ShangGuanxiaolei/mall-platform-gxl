package com.xquark.dal.vo;

import com.xquark.dal.type.PromotionType;

/**
 * @author gxl
 */
public class GrandSaleModuleVO {
    /**
     * 关联的活动id
     */
    private String promotionId;

    /**
     * 关联的活动类型
     */
    private PromotionType promotionType;

    /**
     * 前端模块排序
     */
    private Integer sort;

    /**
     * 活动模块状态:0->,未开始,1->活动中,2->自动结束,3->人工下线
     */
    private Integer status;

    /**
     * 请求接口路径
     */
    private String targetUrl;

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }
}