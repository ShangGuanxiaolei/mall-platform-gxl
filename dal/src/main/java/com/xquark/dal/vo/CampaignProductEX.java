package com.xquark.dal.vo;

import java.util.Date;

import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.status.ActivityTicketAuditStatus;
import com.xquark.dal.type.ActivityStatus;

public class CampaignProductEX extends CampaignProduct {

  private static final long serialVersionUID = 1L;

  private ActivityStatus status;

  private ActivityTicketAuditStatus auditStatus;

  private Date activityStartTime;

  private Date activityEndTime;

  private String feedback;

  private String auditReason;

  private String reason;

  private String auditor;

  public ActivityStatus getStatus() {
    return status;
  }

  public void setStatus(ActivityStatus status) {
    this.status = status;
  }

  public ActivityTicketAuditStatus getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(ActivityTicketAuditStatus auditStatus) {
    this.auditStatus = auditStatus;
  }

  public Date getActivityStartTime() {
    return activityStartTime;
  }

  public void setActivityStartTime(Date activityStartTime) {
    this.activityStartTime = activityStartTime;
  }

  public Date getActivityEndTime() {
    return activityEndTime;
  }

  public void setActivityEndTime(Date activityEndTime) {
    this.activityEndTime = activityEndTime;
  }

  public String getFeedback() {
    return feedback;
  }

  public void setFeedback(String feedback) {
    this.feedback = feedback;
  }

  public String getAuditReason() {
    return auditReason;
  }

  public void setAuditReason(String auditReason) {
    this.auditReason = auditReason;
  }

  public String getAuditor() {
    return auditor;
  }

  public void setAuditor(String auditor) {
    this.auditor = auditor;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
