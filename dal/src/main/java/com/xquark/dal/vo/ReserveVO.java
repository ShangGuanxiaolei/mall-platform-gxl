package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionReserve;

import java.util.List;
import java.util.Map;

/**
 * @author: yyc
 * @date: 19-4-16 下午2:13
 */
public class ReserveVO extends PromotionReserve {

  /** 每个预售活动的类目 */
  private Map<String, List<ReserveProductVO>> reserveCategoryVOS;

  public Map<String, List<ReserveProductVO>> getReserveCategoryVOS() {
    return reserveCategoryVOS;
  }

  public void setReserveCategoryVOS(Map<String, List<ReserveProductVO>> reserveCategoryVOS) {
    this.reserveCategoryVOS = reserveCategoryVOS;
  }
}
