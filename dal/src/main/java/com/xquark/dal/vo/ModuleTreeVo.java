package com.xquark.dal.vo;

import com.xquark.dal.model.Module;

import java.util.List;
import java.util.Map;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
public class ModuleTreeVo {

  private String id;
  private String text;
  private String icon;
  private Map state;
  private List<ModuleTreeVo> children;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Map getState() {
    return state;
  }

  public void setState(Map state) {
    this.state = state;
  }

  public List<ModuleTreeVo> getChildren() {
    return children;
  }

  public void setChildren(List<ModuleTreeVo> children) {
    this.children = children;
  }

  /**
   * 复制module属性构建TreeVo对象
   */
  public static ModuleTreeVo buildTreeVo(Module module) {
    ModuleTreeVo moduleTreeVo = new ModuleTreeVo();
    moduleTreeVo.setId(module.getId());
    moduleTreeVo.setIcon(module.getIconName());
    moduleTreeVo.setText(module.getName());
    return moduleTreeVo;
  }
}
