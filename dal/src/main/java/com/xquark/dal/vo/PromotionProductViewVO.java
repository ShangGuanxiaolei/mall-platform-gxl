package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;

import java.util.List;

/**
 * 针对一个活动对多个商品的活动限时VO
 * @author wangxinhua
 * @date 2019-04-24
 * @since 1.0
 */
public class PromotionProductViewVO<T extends Promotion, P extends PromotionProductBasicVO> {

    private T promotion;

    private List<P> products;

    public PromotionProductViewVO() {
    }

    public PromotionProductViewVO(T promotion, List<P> products) {
        this.promotion = promotion;
        this.products = products;
    }

    public T getPromotion() {
        return promotion;
    }

    public void setPromotion(T promotion) {
        this.promotion = promotion;
    }

    public List<P> getProducts() {
        return products;
    }

    public void setProducts(List<P> products) {
        this.products = products;
    }
}
