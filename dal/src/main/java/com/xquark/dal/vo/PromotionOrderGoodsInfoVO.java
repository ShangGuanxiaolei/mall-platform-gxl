package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PromotionOrderGoodsInfoVO extends BaseEntityImpl implements Archivable {

    private String name;
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;
    private BigDecimal marketPrice;
    private Double disCountPrice;
    private String code;
    private String pieceGroupTranCode;//活动详情编码
    private String pCode;//活动编码
    private List<PromotionPgMemberInfoVO> memberInfo;
    private Date groupFinishTime;

    public List<PromotionPgMemberInfoVO> getMemberInfo() {
        return memberInfo;
    }

    public void setMemberInfo(List<PromotionPgMemberInfoVO> memberInfo) {
        this.memberInfo = memberInfo;
    }

    public String getPieceGroupTranCode() {
        return pieceGroupTranCode;
    }

    public void setPieceGroupTranCode(String pieceGroupTranCode) {
        this.pieceGroupTranCode = pieceGroupTranCode;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getDisCountPrice() {
        return disCountPrice;
    }

    public void setDisCountPrice(Double disCountPrice) {
        this.disCountPrice = disCountPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Double getSkuDiscount() {
        return skuDiscount;
    }

    public void setSkuDiscount(Double skuDiscount) {
        this.skuDiscount = skuDiscount;
    }

    private Double skuDiscount;



    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }

    public Date getGroupFinishTime() {
        return groupFinishTime;
    }

    public void setGroupFinishTime(Date groupFinishTime) {
        this.groupFinishTime = groupFinishTime;
    }
}
