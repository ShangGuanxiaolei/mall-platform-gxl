package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.status.BonusType;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by chh on 17/6/7.
 */
public class BonusVO extends BonusDetail {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String year;

  private String month;

  private String userName;

  private String userPhone;

  private BonusType type;

  // 直营店董事名称与订单数量信息
  private List<Map> firstData;

  // 一级直营店董事名称与订单数量信息
  private List<Map> secondData;

  // 二级直营店董事名称与订单数量信息
  private List<Map> thirdData;

  // 年度分红级别与各级别达标数等信息
  private List<Map> yearData;

  public List<Map> getYearData() {
    return yearData;
  }

  public void setYearData(List<Map> yearData) {
    this.yearData = yearData;
  }

  public List<Map> getFirstData() {
    return firstData;
  }

  public void setFirstData(List<Map> firstData) {
    this.firstData = firstData;
  }

  public List<Map> getSecondData() {
    return secondData;
  }

  public void setSecondData(List<Map> secondData) {
    this.secondData = secondData;
  }

  public List<Map> getThirdData() {
    return thirdData;
  }

  public void setThirdData(List<Map> thirdData) {
    this.thirdData = thirdData;
  }

  public BonusType getType() {
    return type;
  }

  public void setType(BonusType type) {
    this.type = type;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }
}
