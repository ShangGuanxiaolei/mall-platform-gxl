package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionFullCut;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/23. DESC:
 */
public class PromotionFullCutProductInfoVO extends PromotionFullCut {

    private String productName;

    private BigDecimal productPrice;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * 获取折后价
     */
    public BigDecimal getAfterPrice() {
        if (productPrice == null || getDiscount() == null) {
            return BigDecimal.ZERO;
        }
        return productPrice.multiply(getRealDiscount())
                .divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN);
    }

    /**
     * 获取减去的折扣价
     */
    public BigDecimal getCutDownPrice() {
        return productPrice.subtract(getAfterPrice());
    }
}
