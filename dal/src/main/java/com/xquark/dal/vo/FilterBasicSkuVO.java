package com.xquark.dal.vo;

import com.xquark.dal.model.Sku;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * Created by wangxinhua on 18-3-6. DESC:
 */
public class FilterBasicSkuVO extends FilterBasicVO {

  public FilterBasicSkuVO(ProductBasicVO basicVO) {
    BeanUtils.copyProperties(basicVO, this);
  }

  private List<Sku> skuList;

  public List<Sku> getSkuList() {
    return skuList;
  }

  public void setSkuList(List<Sku> skuList) {
    this.skuList = skuList;
  }
}
