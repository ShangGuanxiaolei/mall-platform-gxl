package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.status.BargainDetailStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-8-8. DESC:
 */
public class PromotionBargainDetailVO extends PromotionBargainDetail {

  private String id;

  private String promotionId;

  private String title;

  private Long helperCount;

  private BigDecimal productPrice;

  private String productId;

  private String productName;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private BargainDetailStatus status;

  private String detailId;

  private String targetUrl;

  private String shareUrl;

  // 是否已砍至最低价
  private boolean isPriceEnd;

  // 是否已达到最大砍价次数
  private boolean isTimesEnd;

  private BigDecimal discount;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validFrom;
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validTo;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidFrom() {
    return validFrom;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidTo() {
    return validTo;
  }

  public String getValidFromStr() {
    if (this.getValidFrom() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidFrom(), "yyyy-MM-dd HH:mm");
  }

  public String getValidToStr() {
    if (this.getValidTo() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidTo(), "yyyy-MM-dd HH:mm");
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public String getShareUrl() {
    return shareUrl;
  }

  public void setShareUrl(String shareUrl) {
    this.shareUrl = shareUrl;
  }

  /**
   * 最大折扣
   */
  private BigDecimal maxDiscount;

  /**
   * 最大砍价次数
   */
  private Integer maxTimes;

  public BigDecimal getMaxDiscount() {
    return maxDiscount;
  }

  public void setMaxDiscount(BigDecimal maxDiscount) {
    this.maxDiscount = maxDiscount;
  }

  public Integer getMaxTimes() {
    return maxTimes;
  }

  public void setMaxTimes(Integer maxTimes) {
    this.maxTimes = maxTimes;
  }

  public boolean getIsPriceEnd() {
    return isPriceEnd;
  }

  public void setIsPriceEnd(boolean priceEnd) {
    isPriceEnd = priceEnd;
  }

  public boolean getIsTimesEnd() {
    return isTimesEnd;
  }

  public void setIsTimesEnd(boolean timesEnd) {
    isTimesEnd = timesEnd;
  }

  public String getTargetUrl() {
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getHelperCount() {
    return helperCount;
  }

  public void setHelperCount(Long helperCount) {
    this.helperCount = helperCount;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public BargainDetailStatus getStatus() {
    return status;
  }

  public void setStatus(BargainDetailStatus status) {
    this.status = status;
  }

  public String getDetailId() {
    return detailId;
  }

  public void setDetailId(String detailId) {
    this.detailId = detailId;
  }
}
