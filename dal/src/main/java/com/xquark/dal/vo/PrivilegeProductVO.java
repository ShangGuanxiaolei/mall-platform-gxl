package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PrivilegeProduct;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 18/01/16.
 */
public class PrivilegeProductVO extends PrivilegeProduct {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String logoUrl;

  private Long productAmount;

  private String siteHost = "http://nffclub.com";

  private String targetUrl;

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }

  public String getTargetUrl() {
    String targetUrl = siteHost + "/p/" + this.getProductId();
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public Long getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(Long productAmount) {
    this.productAmount = productAmount;
  }

  public boolean getIsSellOut() {
    return this.productAmount == null || this.productAmount == 0L;
  }
}
