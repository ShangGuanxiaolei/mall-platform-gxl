package com.xquark.dal.vo;

import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.model.TeamShopCommissionDe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by chh on 17/07/05.
 */
public class TeamShopCommissionVO extends TeamShopCommission {

  protected Logger log = LoggerFactory.getLogger(getClass());

  // 战队数
  private int teamNum;

  // 考核内容
  private String content;

  private List<TeamShopCommissionDe> weeks;

  private List<TeamShopCommissionDe> months;

  public List<TeamShopCommissionDe> getWeeks() {
    return weeks;
  }

  public void setWeeks(List<TeamShopCommissionDe> weeks) {
    this.weeks = weeks;
  }

  public List<TeamShopCommissionDe> getMonths() {
    return months;
  }

  public void setMonths(List<TeamShopCommissionDe> months) {
    this.months = months;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getTeamNum() {
    return teamNum;
  }

  public void setTeamNum(int teamNum) {
    this.teamNum = teamNum;
  }
}
