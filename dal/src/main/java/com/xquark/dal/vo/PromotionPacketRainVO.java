package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionList;
import com.xquark.dal.model.PromotionPacketRain;

/**
 * @author wangxinhua
 * @date 2019-05-01
 * @since 1.0
 */
public class PromotionPacketRainVO extends PromotionPacketRain {

    private PromotionList detail;

    public PromotionList getDetail() {
        return detail;
    }

    public void setDetail(PromotionList detail) {
        this.detail = detail;
    }
}
