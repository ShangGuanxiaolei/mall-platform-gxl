package com.xquark.dal.vo;

/**
 * @author gxl
 */
public class GrandSaleKvVO {
    /**
     * 主KV标题
     */
    private String title;

    /**
     * 主KV显示图片
     */
    private String image;

    /**
     * 主KV点击跳转目标
     */
    private String target;

    /**
     * 主KV点击跳转目标类型
     */
    private String targetType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}