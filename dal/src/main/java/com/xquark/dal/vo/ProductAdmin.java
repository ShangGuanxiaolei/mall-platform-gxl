package com.xquark.dal.vo;

import com.xquark.dal.model.Product;

/**
 * bos系统用到的vo
 *
 * @author huxaya
 */
public class ProductAdmin extends Product {

  private static final long serialVersionUID = 1L;

  private String shopName;

  private String phone;

  private String img;

  private String imgUrl;

  private Boolean archive;

  private String ativityId;

  public String getAtivityId() {
    return ativityId;
  }

  public void setAtivityId(String ativityId) {
    this.ativityId = ativityId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }
}
