package com.xquark.dal.vo;

import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.MerchantPermission;

import java.util.List;

/**
 * Created by quguangming on 16/5/23.
 */
public class MerchantVo extends Merchant {

  private List<MerchantRoleVO> merchantRoleVos;

  private List<MerchantPermission> merchantPermissions;


  public List<MerchantRoleVO> getMerchantRoleVos() {
    return merchantRoleVos;
  }

  public void setMerchantRoleVos(List<MerchantRoleVO> merchantRoleVos) {
    this.merchantRoleVos = merchantRoleVos;
  }

  public List<MerchantPermission> getMerchantPermissions() {
    return merchantPermissions;
  }

  public void setMerchantPermissions(List<MerchantPermission> merchantPermissions) {
    this.merchantPermissions = merchantPermissions;
  }
}
