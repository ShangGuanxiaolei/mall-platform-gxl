package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionPgPrice;

import java.math.BigDecimal;

/**
 * @author wangxinhua
 * @date 2019-04-16
 * @since 1.0
 */
public class PromotionBargainProductVO extends BasePromotionProductVO {

  private BigDecimal promotionPrice;

  public void setPromotionPrice(BigDecimal promotionPrice) {
    this.promotionPrice = promotionPrice;
  }

  @Override
  public Long getAmount() {
    return null;
  }

  @Override
  public BigDecimal getPromotionPrice() {
      return promotionPrice;
  }

  @Override
  public BigDecimal getPromotionConversionPrice() {
      return getPromotion().getConversionPrice();
  }

  @Override
  public BigDecimal getPromotionPoint() {
      return getPromotion().getPoint();
  }

  @Override
  public PromotionPgPrice getPricing() {
      return null;
  }

}
