package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.Product;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.List;

/**
 * Created by wangxinhua on 17-6-29. DESC:
 */
public class ProductCardsVO extends Product {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;  // 主图code

  @Override
  public String getImg() {
    return img;
  }

  @Override
  public void setImg(String img) {
    this.img = img;
  }

  private List<MemberCard> memberCards;

  public List<MemberCard> getMemberCards() {
    return memberCards;
  }

  public void setMemberCards(List<MemberCard> memberCards) {
    this.memberCards = memberCards;
  }
}

