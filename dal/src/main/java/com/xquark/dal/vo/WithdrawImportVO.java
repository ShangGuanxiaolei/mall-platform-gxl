package com.xquark.dal.vo;

import com.google.common.base.Splitter;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by wangxinhua. Date: 2018/8/20 Time: 下午5:57
 */
public class WithdrawImportVO {

  private String tinCode;

  private String remark;

  private String errorMsg;

  public String getTinCode() {
    return tinCode;
  }

  public void setTinCode(String tinCode) {
    this.tinCode = tinCode;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getErrorMsg() {
    return errorMsg;
  }

  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  public Long getCpId() {
    String customerNumber = getRemarkInfo()[0];
    if (StringUtils.isBlank(customerNumber)) {
      return null;
    }
    Long cpId = null;
    try {
      cpId = Long.valueOf(StringUtils.stripStart(customerNumber, "0"));
    } catch (Exception e) {
      // do nothing
      e.printStackTrace();
    }
    return cpId;
  }

  public Integer getMonth() {
    String month = getRemarkInfo()[1];
    return Integer.parseInt(month);
  }

  public Integer getSource() {
    return Integer.parseInt(getRemarkInfo()[2]);
  }

  private String[] getRemarkInfo() {
    String trimed = StringUtils.trim(remark);
    return StringUtils.split(trimed, ";");
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WithdrawImportVO that = (WithdrawImportVO) o;
    return Objects.equals(remark, that.remark);
  }

  @Override
  public int hashCode() {
    return Objects.hash(remark);
  }
}
