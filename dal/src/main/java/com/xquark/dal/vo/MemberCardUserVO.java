package com.xquark.dal.vo;

import com.xquark.dal.model.MemberCard;

import java.util.List;

/**
 * Created by wangxinhua on 17-9-4. DESC:
 */
public class MemberCardUserVO extends MemberCard {

  private List<UserOrderDataVO> users;

  public List<UserOrderDataVO> getUsers() {
    return users;
  }

  public void setUsers(List<UserOrderDataVO> users) {
    this.users = users;
  }
}
