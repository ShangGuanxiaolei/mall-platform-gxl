package com.xquark.dal.vo;

import com.xquark.dal.model.DynamicPricing;

import java.math.BigDecimal;

/**
 * @author wangxinhua
 * @date 2019-05-08
 * @since 1.0
 */
public abstract class BaseDynamicPrice implements DynamicPricing {

    private BigDecimal netWorth;//净值

    private BigDecimal memberPrice;

    private BigDecimal proxyPrice;

    private BigDecimal changePrice;

    private BigDecimal reducedPrice;

    private BigDecimal promoAmt;

    private BigDecimal serverAmt;

    public abstract BigDecimal getPrice();

    public abstract BigDecimal getReduction();

    public abstract BigDecimal getPoint();

    /**
     * 获取兑换价
     */
    public BigDecimal getConversionPrice() {
        final BigDecimal price = getPrice();
        final BigDecimal point = getPoint();
        if (price == null || point == null) {
            return BigDecimal.ZERO;
        }
        return price
                .subtract(
                        point.divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_DOWN));
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getProxyPrice() {
        return proxyPrice;
    }

    public void setProxyPrice(BigDecimal proxyPrice) {
        this.proxyPrice = proxyPrice;
    }

    public BigDecimal getChangePrice() {
        return changePrice;
    }

    public void setChangePrice(BigDecimal changePrice) {
        this.changePrice = changePrice;
    }

    public BigDecimal getReducedPrice() {
        return reducedPrice;
    }

    public void setReducedPrice(BigDecimal reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    @Override
    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    @Override
    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    @Override
    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }
}
