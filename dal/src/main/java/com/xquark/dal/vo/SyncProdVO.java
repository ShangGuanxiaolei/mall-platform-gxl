package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.Date;

public class SyncProdVO {

  private String shopId;
  private String productId;
  private String title;
  //@JsonSerialize(using = JsonResourceUrlSerializerSync.class)
  private String image;      // 图片url  包括商品主图,详情图, ;号分隔
  private String url;        // 商品url
  private Integer sellNum;    // 商品销量
  private BigDecimal price;  // 最低价格
  private Date createTime;    //
  private Boolean archive;      // 0:存在  1:已删除
  private Integer status;         // 0:在售  1 or else:下架
  private Boolean isDelay;       // 是否延迟发货
  private Integer delayDays;   // 延迟发货天数

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getSellNum() {
    return sellNum;
  }

  public void setSellNum(Integer sellNum) {
    this.sellNum = sellNum;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Boolean getIsDelay() {
    return isDelay;
  }

  public void setIsDelay(Boolean isDelay) {
    this.isDelay = isDelay;
  }

  public Integer getDelayDays() {
    return delayDays;
  }

  public void setDelayDays(Integer delayDays) {
    this.delayDays = delayDays;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }
}