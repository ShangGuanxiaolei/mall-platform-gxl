package com.xquark.dal.vo;

/**
 * Created by wangxinhua on 17-6-30. DESC:
 */
public class MemberCountsVO {

  public MemberCountsVO(Long total, Long newRegister) {
    this.total = total;
    this.newRegister = newRegister;
  }

  private final Long total;
  private final Long newRegister;

  public Long getTotal() {
    return total;
  }

  public Long getNewRegister() {
    return newRegister;
  }

}
