package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PromotionBargainSkuVO extends PromotionBargainSku {

  private String name;

  private String img;

  private BigDecimal deductionDPoint;

  /**
   * sku对应活动已发起数量
   */
  private int launchNum;

  public BigDecimal getConversionPrice() {
    return this.getPriceStart()
        .subtract(this.getDeductionDPoint().divide(new BigDecimal(10), 2, RoundingMode.HALF_EVEN));
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public int getLaunchNum() {
    return launchNum;
  }

  public void setLaunchNum(int launchNum) {
    this.launchNum = launchNum;
  }
}
