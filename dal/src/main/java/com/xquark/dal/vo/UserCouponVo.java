package com.xquark.dal.vo;

import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.UserCouponType;
import com.xquark.utils.RandomStringUtil;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * Created by  on 15-11-6.
 */
public class UserCouponVo extends UserCoupon implements CouponView {

  private String couponName;          //优惠券名称

  private BigDecimal discount;        //折扣金额

  private Date validFrom;             //优惠券有效期开始日

  private Date validTo;               //优惠券有效期截止日

  private Integer applyLimit;         // 下单可使用张数

  private BigDecimal applyAbove;      // 满减

  private String strValidFrom;        //裁剪过的优惠券有效期开始日

  private String strValidTo;          //裁剪过的优惠券有效期截止日

  private int intDiscount;            //整形化的折扣金额

  private int intApplyAbove;          //整形化的满减

  private String shopImg;             //店铺图片

  private String shopName;            //店铺名称

  private String productName;         //特定商品

  private String productImg;          //特定商品图片

  private String productId;

  private String userName;

  private String categoryId;

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  // 商品名截取字符串
  private String productNameLimit;

  public String getProductNameLimit() {
    String value = getProductName();
    if (StringUtils.isNotEmpty(value) && value.length() > 12) {
      value = value.substring(0, 12) + "...";
    }
    return value;
  }

  public void setProductNameLimit(String productNameLimit) {
    this.productNameLimit = productNameLimit;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  private UserCouponType type;

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public UserCouponType getType() {
    return type;
  }

  public void setType(UserCouponType type) {
    this.type = type;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }

  public String getShopImg() {
    return shopImg;
  }

  public void setShopImg(String shopImg) {
    this.shopImg = shopImg;
  }

  public int getIntApplyAbove() {
    return intApplyAbove;
  }

  public void setIntApplyAbove(int intApplyAbove) {
    this.intApplyAbove = intApplyAbove;
  }

  public int getIntDiscount() {
    return intDiscount;
  }

  public void setIntDiscount(int intDiscount) {
    this.intDiscount = intDiscount;
  }

  public String getStrValidFrom() {
    return strValidFrom;
  }

  public void setStrValidFrom(String strValidFrom) {
    this.strValidFrom = strValidFrom;
  }

  public String getStrValidTo() {
    if (this.getValidTo() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getValidTo(), "yyyy-MM-dd");
  }

  public void setStrValidTo(String strValidTo) {
    this.strValidTo = strValidTo;
  }

  public BigDecimal getApplyAbove() {
    return applyAbove;
  }

  public void setApplyAbove(BigDecimal applyAbove) {
    this.applyAbove = applyAbove;
  }

  public Integer getApplyLimit() {
    return applyLimit;
  }

  public void setApplyLimit(Integer applyLimit) {
    this.applyLimit = applyLimit;
  }

  public String getCouponName() {
    return couponName;
  }

  public void setCouponName(String couponName) {
    this.couponName = couponName;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  @Override
  public PromotionType getPromotionType() {
    return PromotionType.COUPON;
  }

  @Override
  public String getCouponType() {
    return "优惠券优惠";
  }

  @Override
  public String getUuId() {
    return RandomStringUtil.create();
  }

  @Override
  public Boolean getIsFreeDelivery() {
    return Boolean.FALSE;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  /**
   * 根据价格比较优惠
   */
  @Override
  public int compareTo(CouponView o) {
    if (o == null) {
      return 1;
    }
    BigDecimal thisDiscount = this.getDiscount();
    if (thisDiscount == null) {
      return -1;
    }
    BigDecimal oDiscount = o.getDiscount();
    if (oDiscount == null) {
      return 1;
    }
    return thisDiscount.compareTo(oDiscount);
  }

  public String getApplyAboveStr() {
    BigDecimal applyAbove = getApplyAbove();
    if (applyAbove == null) {
      applyAbove = BigDecimal.ZERO;
    }
    BigDecimal formatted = applyAbove.setScale(2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
    return String.format("满%s使用", formatted.toPlainString());
  }
}
