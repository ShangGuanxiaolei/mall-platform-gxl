package com.xquark.dal.vo;


public class FragmentChip {

  private Boolean isImg;
  private String content;

  public Boolean getIsImg() {
    return isImg;
  }

  public void setIsImg(Boolean isImg) {
    this.isImg = isImg;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

}
