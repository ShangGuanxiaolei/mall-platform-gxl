package com.xquark.dal.vo;

/**
 * @auther liuwei
 * @date 2018-06-30 19:50
 */
public class SupplierSkuExportVO {

  private String skuCode;
  private String barCode;
  private String productName;
  private Integer amount;

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

}
