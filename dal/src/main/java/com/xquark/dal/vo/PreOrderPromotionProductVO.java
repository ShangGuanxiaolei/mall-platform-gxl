package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.PromotionPreOrder;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-22. DESC:
 */
public class PreOrderPromotionProductVO extends PromotionPreOrder implements PromotionProductVO {

  private ProductBasicVO product;

  private Promotion promotion;

  private static final long serialVersionUID = 2138017152029846190L;

  @Override
  public ProductBasicVO getProduct() {
    return product;
  }

  public void setProduct(ProductBasicVO product) {
    this.product = product;
  }

  @Override
  public Promotion getPromotion() {
    return promotion;
  }

  @Override
  public Boolean getPromotionClosed() {
    return promotion.getClosed();
  }

  @Override
  public String getTitle() {
      return promotion.getTitle();
  }

  @Override
  public Date getValidFrom() {
    return promotion.getValidFrom();
  }

  @Override
  public Date getValidTo() {
    return promotion.getValidTo();
  }

  @Override
  public PromotionType getPromotionType() {
      return promotion.getPromotionType();
  }

  @Override
  public BigDecimal getPromotionPrice() {
    return getPreOrderPrice();
  }

  @Override
  public BigDecimal getPromotionConversionPrice() {
    return BigDecimal.ZERO;
  }

  @Override
  public BigDecimal getPromotionPoint() {
    return BigDecimal.ZERO;
  }

  public void setPromotion(Promotion promotion) {
    this.promotion = promotion;
  }

  @Override
  public PromotionPgPrice getPricing() {
    // TODO
    return null;
  }
}
