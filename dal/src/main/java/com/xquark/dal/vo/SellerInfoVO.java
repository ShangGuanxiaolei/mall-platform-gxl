package com.xquark.dal.vo;

public class SellerInfoVO extends UserInfo {

  private String userShopName;

  private String servicePhone;

  public String getUserShopName() {
    return userShopName;
  }

  public void setUserShopName(String userShopName) {
    this.userShopName = userShopName;
  }

  public String getServicePhone() {
    return servicePhone;
  }

  public void setServicePhone(String servicePhone) {
    this.servicePhone = servicePhone;
  }

}
