package com.xquark.dal.vo;

import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.AgentType;

import java.math.BigDecimal;

/**
 * Created by chh on 16/12/19.
 */
public class UserAgentCommissionVO extends UserAgent {

  // 代理佣金id
  private String commissionId;

  // 订单号
  private String orderNo;
  // 订单价格
  private BigDecimal price;
  // 订单数量
  private int amount;
  // 佣金比例
  private BigDecimal rate;
  // 佣金金额
  private BigDecimal fee;
  // 是否发放佣金
  private Boolean offered;
  // 下单经销商
  private String buyerName;
  // 经销商电话
  private String buyerPhone;

  // 下单经销商级别
  private AgentType buyerType;

  private String buyerTypeStr;

  public AgentType getBuyerType() {
    return buyerType;
  }

  public void setBuyerType(AgentType buyerType) {
    this.buyerType = buyerType;
  }

  public String getBuyerTypeStr() {
    if (this.getBuyerType() == null) {
      return "";
    }
    String str = "";
    switch (this.getBuyerType()) {
      case DIRECTOR:
        str = "董事";
        break;
      case GENERAL:
        str = "总代";
        break;
      case FIRST:
        str = "一星";
        break;
      case SECOND:
        str = "二星";
        break;
      default:
        str = this.getBuyerType().toString();
        break;
    }
    return str;
  }

  public void setBuyerTypeStr(String buyerTypeStr) {
    this.buyerTypeStr = buyerTypeStr;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public Boolean getOffered() {
    return offered;
  }

  public String getOfferedStr() {
    return this.offered ? "已发放" : "未发放";
  }

  public void setOffered(Boolean offered) {
    this.offered = offered;
  }

  public String getCommissionId() {
    return commissionId;
  }

  public void setCommissionId(String commissionId) {
    this.commissionId = commissionId;
  }
}
