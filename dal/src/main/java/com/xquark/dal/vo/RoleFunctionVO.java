package com.xquark.dal.vo;

import java.io.Serializable;

/**
 * 作者: wangxh 创建日期: 17-4-19 简介:
 */
public class RoleFunctionVO implements Serializable {

  private static final long serialVersionUID = -2279552481484338421L;

  private String htmlId;
  private String name;
  private String avaRoles;
  private String visRoles;

  private Boolean isShow;
  private Boolean isEnable;

  public String getHtmlId() {
    return htmlId;
  }

  public void setHtmlId(String htmlId) {
    this.htmlId = htmlId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAvaRoles() {
    return avaRoles;
  }

  public void setAvaRoles(String avaRoles) {
    this.avaRoles = avaRoles;
  }

  public String getVisRoles() {
    return visRoles;
  }

  public void setVisRoles(String visRoles) {
    this.visRoles = visRoles;
  }

  public Boolean getShow() {
    return isShow;
  }

  public void setShow(Boolean show) {
    isShow = show;
  }

  public Boolean getEnable() {
    return isEnable;
  }

  public void setEnable(Boolean enable) {
    isEnable = enable;
  }
}
