package com.xquark.dal.vo;

import com.xquark.dal.model.Payment;

public class PaymentVO extends Payment {

  private static final long serialVersionUID = 1L;

  private String code; //payment code

  private String name; //payment name

  private String payAgreementId;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPayAgreementId() {
    return payAgreementId;
  }

  public void setPayAgreementId(String payAgreementId) {
    this.payAgreementId = payAgreementId;
  }

}
