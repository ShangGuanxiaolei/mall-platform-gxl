package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PromotionReserveProduct;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * @author: yyc
 * @date: 19-4-16 下午3:20
 */
public class ReserveProductVO extends PromotionReserveProduct {

  /** 商品名称 */
  private String productName;
  /** 商品主图 */
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;
  /** sku价格 */
  private BigDecimal price;
  /** sku主图 */
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String skuImg;

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public String getSkuImg() {
    return skuImg;
  }

  public void setSkuImg(String skuImg) {
    this.skuImg = skuImg;
  }
}
