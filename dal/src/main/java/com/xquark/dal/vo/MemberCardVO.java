package com.xquark.dal.vo;

import com.xquark.dal.model.MemberCard;

/**
 * Created by wangxinhua on 17-6-29. DESC:
 */
public class MemberCardVO extends MemberCard {

  private boolean checked;
  private Long memberCount;

  public boolean isChecked() {
    return checked;
  }

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public Long getMemberCount() {
    return memberCount;
  }

  public void setMemberCount(Long memberCount) {
    this.memberCount = memberCount;
  }

}
