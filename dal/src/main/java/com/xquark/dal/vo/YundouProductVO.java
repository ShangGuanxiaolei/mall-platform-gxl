package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.YundouProduct;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by chh on 17/07/09.
 */
public class YundouProductVO extends YundouProduct {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private Integer setting;

  public String getTargetUrl() {
    String siteHost = "http://nffclub.com";
    return siteHost + "/p/yundou/" + this.getId();
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public Integer getSetting() {
    return setting;
  }

  public void setSetting(Integer setting) {
    this.setting = setting;
  }

}

