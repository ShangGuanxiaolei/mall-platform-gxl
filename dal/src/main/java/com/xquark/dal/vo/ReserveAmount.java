package com.xquark.dal.vo;

import java.io.Serializable;

public class ReserveAmount implements Serializable {

	private String skuId;

	private int amountLimit;

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public int getAmountLimit() {
		return amountLimit;
	}

	public void setAmountLimit(int amountLimit) {
		this.amountLimit = amountLimit;
	}
}
