package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.List;

public class AreaPostagePair {

  private BigDecimal postage;
  private int first;
  private int second;
  private BigDecimal firstFee;
  private BigDecimal secondFee;
  private List<AreaPair> areas;

  public int getFirst() {
    return first;
  }

  public void setFirst(int first) {
    this.first = first;
  }

  public int getSecond() {
    return second;
  }

  public void setSecond(int second) {
    this.second = second;
  }

  public BigDecimal getFirstFee() {
    return firstFee;
  }

  public void setFirstFee(BigDecimal firstFee) {
    this.firstFee = firstFee;
  }

  public BigDecimal getSecondFee() {
    return secondFee;
  }

  public void setSecondFee(BigDecimal secondFee) {
    this.secondFee = secondFee;
  }


  public List<AreaPair> getAreas() {
    return areas;
  }

  public void setAreas(List<AreaPair> areas) {
    this.areas = areas;
  }

  public BigDecimal getPostage() {
    return postage;
  }

  public void setPostage(BigDecimal postage) {
    this.postage = postage;
  }
}
