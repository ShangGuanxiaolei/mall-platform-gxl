package com.xquark.dal.vo;

/**
 * @author wangxinhua.
 * @date 2018/10/7
 */
public class SkuCombineVO extends SkuBasicVO {

  /**
   * 套装数量
   */
  private Integer number;

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

}
