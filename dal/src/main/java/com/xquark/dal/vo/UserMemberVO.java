package com.xquark.dal.vo;

import com.xquark.dal.model.User;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.type.MemberUpgradeType;
import java.util.Collection;
import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.security.core.GrantedAuthority;

/**
 * Created by wangxinhua on 17-6-26. DESC: 会员VO类
 */
public class UserMemberVO extends User {

  private String id;
  private String name;//目前等于绑定银行卡时候的姓名
  private Long roles;
  private String avatar;
  private String email;
  private String loginname;
  private String phone;
  private String password;
  private Boolean archive;
  private String shopId;
  private String idCardNum;
  private Long employeeId;
  private String employeeName;
  // 第三方用户提供方
  private String partner;
  // 第三方用户id
  private String extUserId;
  private int withdrawType;
  private long functionSet;

  /**
   * 当前买家进入的店铺ID
   */
  private String currentSellerShopId;

  //会员数据
  private String memberCode;           //会员编号
  private String memberLevel;          //会员等级;
  private Integer memberPoints;        //会员经验值
  private Integer consumptionPoints;   //消费积分
  private String weixinCode;           //微信code
  private String weiboCode;            //微博code
  private String cid;                  //cid
  private String cid1;                 //cid1
  private String cid2;                 //cid2
  private String cid3;                 //cid3
  private Double balance;              //余额

  private String code;
  private String unionId;
  private String wechatName;
  private Long yundou;

  private String createdAtStr;

  /* 会员管理相关 */
  private String level;
  private MemberUpgradeType upgradeType;

  public void setCreatedAtStr(String createdAtStr) {
    this.createdAtStr = createdAtStr;
  }

  public String getCreatedAtStr() {
    if (this.getCreatedAt() == null) {
      return "";
    }
    return DateFormatUtils.format(this.getCreatedAt(), "yyyy-MM-dd HH:mm:ss");
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public MemberUpgradeType getUpgradeType() {
    return upgradeType;
  }

  public void setUpgradeType(MemberUpgradeType upgradeType) {
    this.upgradeType = upgradeType;
  }

  @Override
  public void setId(String id) {
    super.setId(id);
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    super.setCreatedAt(createdAt);
  }

  @Override
  public Date getCreatedAt() {
    return super.getCreatedAt();
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    super.setUpdatedAt(updatedAt);
  }

  @Override
  public Date getUpdatedAt() {
    return super.getUpdatedAt();
  }

  @Override
  public String getWechatName() {
    return super.getWechatName();
  }

  @Override
  public void setWechatName(String wechatName) {
    super.setWechatName(wechatName);
  }

  @Override
  public String getUnionId() {
    return super.getUnionId();
  }

  @Override
  public void setUnionId(String unionId) {
    super.setUnionId(unionId);
  }

  @Override
  public String getCode() {
    return super.getCode();
  }

  @Override
  public void setCode(String code) {
    super.setCode(code);
  }

  @Override
  public Double getBalance() {
    return super.getBalance();
  }

  @Override
  public void setBalance(Double balance) {
    super.setBalance(balance);
  }

  @Override
  public Long getYundou() {
    return super.getYundou();
  }

  @Override
  public void setYundou(Long yundou) {
    super.setYundou(yundou);
  }

  @Override
  public boolean hasRole(String role) {
    return super.hasRole(role);
  }

  @Override
  public void updateCid(String anonymousId) {
    super.updateCid(anonymousId);
  }

  @Override
  public String getCid1() {
    return super.getCid1();
  }

  @Override
  public void setCid1(String cid1) {
    super.setCid1(cid1);
  }

  @Override
  public String getCid2() {
    return super.getCid2();
  }

  @Override
  public void setCid2(String cid2) {
    super.setCid2(cid2);
  }

  @Override
  public String getCid3() {
    return super.getCid3();
  }

  @Override
  public void setCid3(String cid3) {
    super.setCid3(cid3);
  }

  @Override
  public String getCid() {
    return super.getCid();
  }

  @Override
  public void setCid(String cid) {
    super.setCid(cid);
  }

  @Override
  public String getMemberCode() {
    return super.getMemberCode();
  }

  @Override
  public void setMemberCode(String memberCode) {
    super.setMemberCode(memberCode);
  }

  @Override
  public String getMemberLevel() {
    return super.getMemberLevel();
  }

  @Override
  public void setMemberLevel(String memberLevel) {
    super.setMemberLevel(memberLevel);
  }

  @Override
  public Integer getConsumptionPoints() {
    return super.getConsumptionPoints();
  }

  @Override
  public void setConsumptionPoints(Integer consumptionPoints) {
    super.setConsumptionPoints(consumptionPoints);
  }

  @Override
  public Integer getMemberPoints() {
    return super.getMemberPoints();
  }

  @Override
  public void setMemberPoints(Integer memberPoints) {
    super.setMemberPoints(memberPoints);
  }

  @Override
  public String getWeiboCode() {
    return super.getWeiboCode();
  }

  @Override
  public void setWeiboCode(String weiboCode) {
    super.setWeiboCode(weiboCode);
  }

  @Override
  public String getWeixinCode() {
    return super.getWeixinCode();
  }

  @Override
  public void setWeixinCode(String weixinCode) {
    super.setWeixinCode(weixinCode);
  }

  @Override
  public UserStatus getUserStatus() {
    return super.getUserStatus();
  }

  @Override
  public void setUserStatus(UserStatus userStatus) {
    super.setUserStatus(userStatus);
  }

  @Override
  public String getName() {
    return super.getName();
  }

  @Override
  public void setName(String name) {
    super.setName(name);
  }

  @Override
  public Long getRoles() {
    return super.getRoles();
  }

  @Override
  public void setRoles(Long roles) {
    super.setRoles(roles);
  }

  @Override
  public boolean hasRole(long role) {
    return super.hasRole(role);
  }

  @Override
  public String getAvatar() {
    return super.getAvatar();
  }

  @Override
  public void setAvatar(String avatar) {
    super.setAvatar(avatar);
  }

  @Override
  public String getLoginname() {
    return super.getLoginname();
  }

  @Override
  public void setLoginname(String loginname) {
    super.setLoginname(loginname);
  }

  @Override
  public String getPhone() {
    return super.getPhone();
  }

  @Override
  public void setPhone(String phone) {
    super.setPhone(phone);
  }

  @Override
  public String getEmail() {
    return super.getEmail();
  }

  @Override
  public void setEmail(String email) {
    super.setEmail(email);
  }

  @Override
  public String getPassword() {
    return super.getPassword();
  }

  @Override
  public void setPassword(String password) {
    super.setPassword(password);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return super.getAuthorities();
  }

  @Override
  public String getUsername() {
    return super.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return super.isAccountNonExpired();
  }

  @Override
  public boolean isAccountNonLocked() {
    return super.isAccountNonLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return super.isCredentialsNonExpired();
  }

  @Override
  public boolean isEnabled() {
    return super.isEnabled();
  }

  @Override
  public Boolean getArchive() {
    return super.getArchive();
  }

  @Override
  public void setArchive(Boolean archive) {
    super.setArchive(archive);
  }

  @Override
  public String getShopId() {
    return super.getShopId();
  }

  @Override
  public void setShopId(String shopId) {
    super.setShopId(shopId);
  }

  @Override
  public String getIdCardNum() {
    return super.getIdCardNum();
  }

  @Override
  public void setIdCardNum(String idCardNum) {
    super.setIdCardNum(idCardNum);
  }

  @Override
  public String getExtUserId() {
    return super.getExtUserId();
  }

  @Override
  public void setExtUserId(String extUserId) {
    super.setExtUserId(extUserId);
  }

  @Override
  public String getPartner() {
    return super.getPartner();
  }

  @Override
  public void setPartner(String partner) {
    super.setPartner(partner);
  }

  @Override
  public String getId() {
    return super.getId();
  }

  @Override
  public boolean isAnonymous() {
    return super.isAnonymous();
  }

  @Override
  public int getWithdrawType() {
    return super.getWithdrawType();
  }

  @Override
  public void setWithdrawType(int withdrawType) {
    super.setWithdrawType(withdrawType);
  }

  @Override
  public Long getFunctionSet() {
    return super.getFunctionSet();
  }

  @Override
  public void setFunctionSet(Long functionSet) {
    super.setFunctionSet(functionSet);
  }

  @Override
  public Boolean getFunctionSts(int bit) {
    return super.getFunctionSts(bit);
  }

  @Override
  public String getCurrentSellerShopId() {
    return super.getCurrentSellerShopId();
  }

  @Override
  public void setCurrentSellerShopId(String currentSellerShopId) {
    super.setCurrentSellerShopId(currentSellerShopId);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public String toString() {
    return super.toString();
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
  }

  public Long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Long employeeId) {
    this.employeeId = employeeId;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }
}
