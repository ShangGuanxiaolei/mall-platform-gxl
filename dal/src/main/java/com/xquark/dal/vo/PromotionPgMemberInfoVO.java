package com.xquark.dal.vo;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;

/**
 * @author LiHaoYang
 * @ClassName PromotionPgMemberInfo
 * @date 2018/9/10 0010
 */
public class PromotionPgMemberInfoVO extends BaseEntityImpl implements Archivable {

    private String headimgurl;
    private String nickname;
    private String memberId;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getIsGroupHead() {
        return isGroupHead;
    }

    public void setIsGroupHead(int isGroupHead) {
        this.isGroupHead = isGroupHead;
    }

    private int isGroupHead;


    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }
}
