package com.xquark.dal.vo;

/**
 * User: kong Date: 2018/7/7. Time: 22:18 物流信息参数
 */
public class LogisticsStockParam {

  //产品id
  private Long productid;

  //产品数量
  private Integer productNum = 0;

  //地区id
  private Integer regionId;

  //陆运条件
  private Integer sfairline = 1;

  //空运条件
  private Integer sfshipping = 10000;

  //礼包编码
  private String bomSn = "";

  private String name;

  public Long getProductid() {
    return productid;
  }

  public void setProductid(Long productid) {
    this.productid = productid;
  }

  public Integer getProductNum() {
    return productNum;
  }

  public void setProductNum(Integer productNum) {
    this.productNum = productNum;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  public Integer getSfairline() {
    return sfairline;
  }

  public void setSfairline(Integer sfairline) {
    this.sfairline = sfairline;
  }

  public Integer getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(Integer sfshipping) {
    this.sfshipping = sfshipping;
  }

  public String getBomSn() {
    return bomSn;
  }

  public void setBomSn(String bomSn) {
    this.bomSn = bomSn;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "LogisticsStockParam{" +
        "productid=" + productid +
        ", productNum=" + productNum +
        ", regionId=" + regionId +
        ", sfairline=" + sfairline +
        ", sfshipping=" + sfshipping +
        ", bomSn='" + bomSn + '\'' +
        '}';
  }
}
