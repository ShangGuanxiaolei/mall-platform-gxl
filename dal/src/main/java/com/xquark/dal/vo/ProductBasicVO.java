package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-11-30. DESC:
 */
public class ProductBasicVO extends BaseDynamicPrice implements FmtQiNiuImgVO {

  private String id;

  private String userId;

  private String shopId;

  private String encode;

  private String u8Encode;

  private Integer priority;

  private ProductStatus status;

  private String model;

  private Long amount;

  private Long secureAmount;

  private String name;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  private BigDecimal price;

  private BigDecimal originalPrice;

  private BigDecimal marketPrice;

  private Long sales;

  private BigDecimal discount;

  private Date updatedAt;
  private Date createdAt;
  private Date onsaleAt;

  private BigDecimal reduction;
  private BigDecimal point;

  private List<? extends FilterBasicVO> filters;
  private int selfOperated;//自营标签

  public int getSelfOperated() {
      return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
      this.selfOperated = selfOperated;
  }

  public List<? extends FilterBasicVO> getFilters() {
    return filters;
  }

  public void setFilters(List<? extends FilterBasicVO> filters) {
    this.filters = filters;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public Long getSales() {
    return sales;
  }

  public void setSales(Long sales) {
    this.sales = sales;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public String getEncode() {
    return encode;
  }

  public void setEncode(String encode) {
    this.encode = encode;
  }

  public String getU8Encode() {
    return u8Encode;
  }

  public void setU8Encode(String u8Encode) {
    this.u8Encode = u8Encode;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String getTinyImgUrl() {
    return img;
  }

  @Override
  public String getMiddleImgUrl() {
    return img;
  }

  public Long getSecureAmount() {
    return secureAmount;
  }

  public void setSecureAmount(Long secureAmount) {
    this.secureAmount = secureAmount;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getOnsaleAt() {
    return onsaleAt;
  }

  public void setOnsaleAt(Date onsaleAt) {
    this.onsaleAt = onsaleAt;
  }

  @Override
  public BigDecimal getReduction() {
    return reduction;
  }

  @Override
  public void setReduction(BigDecimal reduction) {
    this.reduction = reduction;
  }

  @Override
  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }
}
