package com.xquark.dal.vo;

public class LogisticInfoVO {

  private String orderNumber;

  private String statusERP;

  private String logisticsCompany;

  private String logisticsNo;

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public String getStatusERP() {
    return statusERP;
  }

  public void setStatusERP(String statusERP) {
    this.statusERP = statusERP;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getLogisticsNo() {
    return logisticsNo;
  }

  public void setLogisticsNo(String logisticsNo) {
    this.logisticsNo = logisticsNo;
  }


}
