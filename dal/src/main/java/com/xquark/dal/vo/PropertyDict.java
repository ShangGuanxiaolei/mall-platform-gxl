package com.xquark.dal.vo;

public class PropertyDict {

  String type;
  String key;
  String value;
  String describe;

  public PropertyDict() {

  }

  public PropertyDict(String type, String key, String value, String describe) {
    super();
    this.type = type;
    this.key = key;
    this.value = value;
    this.describe = describe;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getDescribe() {
    return describe;
  }

  public void setDescribe(String describe) {
    this.describe = describe;
  }
}
