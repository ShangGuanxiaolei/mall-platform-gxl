package com.xquark.dal.vo;

import java.util.Date;

public class SyncShopVO {

  private String shopId;  // 店铺唯一id 必填
  private String name;  // 店铺名称
  private String nick;    // 店铺拥有者昵称
  private String userId;  // 店铺拥有者id(快店用户id)
  private String phone; // 快店注册手机号
  private String url;    // 店铺url
  //@JsonSerialize(using = JsonResourceUrlSerializerSync.class)
  private Boolean archive;
  private String image;  // 店铺logo图片绝对路径(大图)
  private String description;  //  店铺描述
  private Date createTime;    // 店铺创建时间
  private Long pageSize;    // 页数

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNick() {
    return nick;
  }

  public void setNick(String nick) {
    this.nick = nick;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Long getPageSize() {
    return pageSize;
  }

  public void setPageSize(Long pageSize) {
    this.pageSize = pageSize;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

}
