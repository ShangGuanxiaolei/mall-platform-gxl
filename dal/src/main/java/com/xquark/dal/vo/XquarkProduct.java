package com.xquark.dal.vo;

import java.math.BigDecimal;
import java.util.Date;

public class XquarkProduct {
    private Long id;

    private String code;

    private String encode;

    private String u8Encode;

    private String model;

    private String type;

    private Integer level;

    private Integer priority;

    private String filterSpec;

    private Long userId;

    private Long shopId;

    private String name;

    private String img;

    private Integer imgWidth;

    private Integer imgHeight;

    private String descImg;

    private String status;

    private BigDecimal marketPrice;

    private BigDecimal price;

    private BigDecimal originalPrice;

    private Long amount;

    private Long sales;

    private Boolean archive;

    private Byte recommend;

    private Date recommendAt;

    private BigDecimal discount;

    private Boolean isCommission;

    private BigDecimal commissionRate;

    private Date forsaleAt;

    private Date onsaleAt;

    private Date instockAt;

    private Date createdAt;

    private Date updatedAt;

    private Long fakeSales;

    private Byte isdelay;

    private Integer delaydays;

    private Boolean updateLock;

    private String synchronousflag;

    private Long thirdItemId;

    private Long partnerProductId;

    private String source;

    private Byte inActivity;

    private Boolean isCrossBorder;

    private Boolean enableDesc;

    private Boolean isDistribution;

    private Long sourceProductId;

    private Long sourceShopId;

    private Long feature0;

    private Long feature1;

    private BigDecimal oneyuanPostage;

    private Boolean oneyuanPurchase;

    private BigDecimal specialrate;

    private Boolean isGroupon;

    private String refundAddress;

    private String refundTel;

    private String refundName;

    private String logisticsType;

    private BigDecimal uniformValue;

    private Long templateValue;

    private Byte gift;

    private Integer weight;

    private String attributes;

    private Boolean yundouScale;

    private BigDecimal minYundouPrice;

    private String availableStatus;

    private String reviewStatus;

    private Double height;

    private Double width;

    private Double length;

    private String kind;

    private Long point;

    private BigDecimal deductionDPoint;

    private Boolean supportRefund;

    private Integer numInPackage;

    private String barCode;

    private Long wareHouseId;

    private Long packageId;

    private String outerId;

    private Integer num;

    private String whseCode;

    private String skuId;

    private String skuOuterId;

    private Long skuPrice;

    private Integer skuQuantity;

    private String skuName;

    private String skuProperty;

    private String skuPictureUrl;

    private BigDecimal netWorth;

    private Long supplierId;

    private Integer sfairline;

    private Integer sfshipping;

    private Integer merchantNumber;

    private BigDecimal promoAmt;

    private BigDecimal serverAmt;

    private String deliveryRegion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getEncode() {
        return encode;
    }

    public void setEncode(String encode) {
        this.encode = encode == null ? null : encode.trim();
    }

    public String getU8Encode() {
        return u8Encode;
    }

    public void setU8Encode(String u8Encode) {
        this.u8Encode = u8Encode == null ? null : u8Encode.trim();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getFilterSpec() {
        return filterSpec;
    }

    public void setFilterSpec(String filterSpec) {
        this.filterSpec = filterSpec == null ? null : filterSpec.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    public Integer getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(Integer imgWidth) {
        this.imgWidth = imgWidth;
    }

    public Integer getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(Integer imgHeight) {
        this.imgHeight = imgHeight;
    }

    public String getDescImg() {
        return descImg;
    }

    public void setDescImg(String descImg) {
        this.descImg = descImg == null ? null : descImg.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Byte getRecommend() {
        return recommend;
    }

    public void setRecommend(Byte recommend) {
        this.recommend = recommend;
    }

    public Date getRecommendAt() {
        return recommendAt;
    }

    public void setRecommendAt(Date recommendAt) {
        this.recommendAt = recommendAt;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Boolean getIsCommission() {
        return isCommission;
    }

    public void setIsCommission(Boolean isCommission) {
        this.isCommission = isCommission;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Date getForsaleAt() {
        return forsaleAt;
    }

    public void setForsaleAt(Date forsaleAt) {
        this.forsaleAt = forsaleAt;
    }

    public Date getOnsaleAt() {
        return onsaleAt;
    }

    public void setOnsaleAt(Date onsaleAt) {
        this.onsaleAt = onsaleAt;
    }

    public Date getInstockAt() {
        return instockAt;
    }

    public void setInstockAt(Date instockAt) {
        this.instockAt = instockAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getFakeSales() {
        return fakeSales;
    }

    public void setFakeSales(Long fakeSales) {
        this.fakeSales = fakeSales;
    }

    public Byte getIsdelay() {
        return isdelay;
    }

    public void setIsdelay(Byte isdelay) {
        this.isdelay = isdelay;
    }

    public Integer getDelaydays() {
        return delaydays;
    }

    public void setDelaydays(Integer delaydays) {
        this.delaydays = delaydays;
    }

    public Boolean getUpdateLock() {
        return updateLock;
    }

    public void setUpdateLock(Boolean updateLock) {
        this.updateLock = updateLock;
    }

    public String getSynchronousflag() {
        return synchronousflag;
    }

    public void setSynchronousflag(String synchronousflag) {
        this.synchronousflag = synchronousflag == null ? null : synchronousflag.trim();
    }

    public Long getThirdItemId() {
        return thirdItemId;
    }

    public void setThirdItemId(Long thirdItemId) {
        this.thirdItemId = thirdItemId;
    }

    public Long getPartnerProductId() {
        return partnerProductId;
    }

    public void setPartnerProductId(Long partnerProductId) {
        this.partnerProductId = partnerProductId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public Byte getInActivity() {
        return inActivity;
    }

    public void setInActivity(Byte inActivity) {
        this.inActivity = inActivity;
    }

    public Boolean getIsCrossBorder() {
        return isCrossBorder;
    }

    public void setIsCrossBorder(Boolean isCrossBorder) {
        this.isCrossBorder = isCrossBorder;
    }

    public Boolean getEnableDesc() {
        return enableDesc;
    }

    public void setEnableDesc(Boolean enableDesc) {
        this.enableDesc = enableDesc;
    }

    public Boolean getIsDistribution() {
        return isDistribution;
    }

    public void setIsDistribution(Boolean isDistribution) {
        this.isDistribution = isDistribution;
    }

    public Long getSourceProductId() {
        return sourceProductId;
    }

    public void setSourceProductId(Long sourceProductId) {
        this.sourceProductId = sourceProductId;
    }

    public Long getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Long sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Long getFeature0() {
        return feature0;
    }

    public void setFeature0(Long feature0) {
        this.feature0 = feature0;
    }

    public Long getFeature1() {
        return feature1;
    }

    public void setFeature1(Long feature1) {
        this.feature1 = feature1;
    }

    public BigDecimal getOneyuanPostage() {
        return oneyuanPostage;
    }

    public void setOneyuanPostage(BigDecimal oneyuanPostage) {
        this.oneyuanPostage = oneyuanPostage;
    }

    public Boolean getOneyuanPurchase() {
        return oneyuanPurchase;
    }

    public void setOneyuanPurchase(Boolean oneyuanPurchase) {
        this.oneyuanPurchase = oneyuanPurchase;
    }

    public BigDecimal getSpecialrate() {
        return specialrate;
    }

    public void setSpecialrate(BigDecimal specialrate) {
        this.specialrate = specialrate;
    }

    public Boolean getIsGroupon() {
        return isGroupon;
    }

    public void setIsGroupon(Boolean isGroupon) {
        this.isGroupon = isGroupon;
    }

    public String getRefundAddress() {
        return refundAddress;
    }

    public void setRefundAddress(String refundAddress) {
        this.refundAddress = refundAddress == null ? null : refundAddress.trim();
    }

    public String getRefundTel() {
        return refundTel;
    }

    public void setRefundTel(String refundTel) {
        this.refundTel = refundTel == null ? null : refundTel.trim();
    }

    public String getRefundName() {
        return refundName;
    }

    public void setRefundName(String refundName) {
        this.refundName = refundName == null ? null : refundName.trim();
    }

    public String getLogisticsType() {
        return logisticsType;
    }

    public void setLogisticsType(String logisticsType) {
        this.logisticsType = logisticsType == null ? null : logisticsType.trim();
    }

    public BigDecimal getUniformValue() {
        return uniformValue;
    }

    public void setUniformValue(BigDecimal uniformValue) {
        this.uniformValue = uniformValue;
    }

    public Long getTemplateValue() {
        return templateValue;
    }

    public void setTemplateValue(Long templateValue) {
        this.templateValue = templateValue;
    }

    public Byte getGift() {
        return gift;
    }

    public void setGift(Byte gift) {
        this.gift = gift;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes == null ? null : attributes.trim();
    }

    public Boolean getYundouScale() {
        return yundouScale;
    }

    public void setYundouScale(Boolean yundouScale) {
        this.yundouScale = yundouScale;
    }

    public BigDecimal getMinYundouPrice() {
        return minYundouPrice;
    }

    public void setMinYundouPrice(BigDecimal minYundouPrice) {
        this.minYundouPrice = minYundouPrice;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus == null ? null : availableStatus.trim();
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus == null ? null : reviewStatus.trim();
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind == null ? null : kind.trim();
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public Boolean getSupportRefund() {
        return supportRefund;
    }

    public void setSupportRefund(Boolean supportRefund) {
        this.supportRefund = supportRefund;
    }

    public Integer getNumInPackage() {
        return numInPackage;
    }

    public void setNumInPackage(Integer numInPackage) {
        this.numInPackage = numInPackage;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? null : barCode.trim();
    }

    public Long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(Long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId == null ? null : outerId.trim();
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getWhseCode() {
        return whseCode;
    }

    public void setWhseCode(String whseCode) {
        this.whseCode = whseCode == null ? null : whseCode.trim();
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId == null ? null : skuId.trim();
    }

    public String getSkuOuterId() {
        return skuOuterId;
    }

    public void setSkuOuterId(String skuOuterId) {
        this.skuOuterId = skuOuterId == null ? null : skuOuterId.trim();
    }

    public Long getSkuPrice() {
        return skuPrice;
    }

    public void setSkuPrice(Long skuPrice) {
        this.skuPrice = skuPrice;
    }

    public Integer getSkuQuantity() {
        return skuQuantity;
    }

    public void setSkuQuantity(Integer skuQuantity) {
        this.skuQuantity = skuQuantity;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName == null ? null : skuName.trim();
    }

    public String getSkuProperty() {
        return skuProperty;
    }

    public void setSkuProperty(String skuProperty) {
        this.skuProperty = skuProperty == null ? null : skuProperty.trim();
    }

    public String getSkuPictureUrl() {
        return skuPictureUrl;
    }

    public void setSkuPictureUrl(String skuPictureUrl) {
        this.skuPictureUrl = skuPictureUrl == null ? null : skuPictureUrl.trim();
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getSfairline() {
        return sfairline;
    }

    public void setSfairline(Integer sfairline) {
        this.sfairline = sfairline;
    }

    public Integer getSfshipping() {
        return sfshipping;
    }

    public void setSfshipping(Integer sfshipping) {
        this.sfshipping = sfshipping;
    }

    public Integer getMerchantNumber() {
        return merchantNumber;
    }

    public void setMerchantNumber(Integer merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public String getDeliveryRegion() {
        return deliveryRegion;
    }

    public void setDeliveryRegion(String deliveryRegion) {
        this.deliveryRegion = deliveryRegion == null ? null : deliveryRegion.trim();
    }
}