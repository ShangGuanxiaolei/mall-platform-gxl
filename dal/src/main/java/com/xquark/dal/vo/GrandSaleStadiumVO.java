package com.xquark.dal.vo;

import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleStadiumVO {
    /**
     * 分会场标题
     */
    private String title;

    /**
     * 分会场前端排序
     */
    private Integer sort;

    /**
     * 分会场按钮图片地址
     */
    private String iconUrl;

    /**
     * 分会场活动开始时间
     */
    private Date beginTime;

    /**
     * 分会场活动是否开始
     */
    private boolean isOpen;

    /**
     * 分会场类型
     */
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}