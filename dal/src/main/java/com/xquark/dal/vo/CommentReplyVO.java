package com.xquark.dal.vo;

import java.util.Date;

/**
 * Created by jitre on 11/9/17. 用于在web端展示的评论数据,作为CommentVO的关联数据返回给客户端
 *
 * @see com.xquark.dal.vo.CommentVO
 */
public class CommentReplyVO {

  private String replyId;
  private String fromUserId;
  private String toUserId;
  private String fromUserLoginname;
  private String toUserLoginname;
  private String content;
  private Date createdAt;
  private Boolean isBlocked;

  public String getToUserLoginname() {
    return toUserLoginname;
  }

  public void setToUserLoginname(String toUserLoginname) {
    this.toUserLoginname = toUserLoginname;
  }

  public Boolean getBlocked() {
    return isBlocked;
  }

  public void setBlocked(Boolean blocked) {
    isBlocked = blocked;
  }

  public String getReplyId() {
    return replyId;
  }

  public void setReplyId(String replyId) {
    this.replyId = replyId;
  }

  public String getFromUserId() {
    return fromUserId;
  }

  public void setFromUserId(String fromUserId) {
    this.fromUserId = fromUserId;
  }

  public String getToUserId() {
    return toUserId;
  }

  public void setToUserId(String toUserId) {
    this.toUserId = toUserId;
  }

  public String getFromUserLoginname() {
    return fromUserLoginname;
  }

  public void setFromUserLoginname(String fromUserLoginname) {
    this.fromUserLoginname = fromUserLoginname;
  }

  public String getToUserloginname() {
    return toUserLoginname;
  }

  public void setToUserloginname(String toUserloginname) {
    this.toUserLoginname = toUserloginname;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }
}
