package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFlashSale;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.type.PromotionType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Optional;

/** Created by wangxinhua on 17-11-23. DESC: */
public class FlashSalePromotionProductVO extends PromotionFlashSale implements PromotionProductVO {

  private static final long serialVersionUID = 4788160701515287419L;

  private ProductBasicVO product;

  private PromotionPgPrice pricing;

  private Promotion promotion;

  private Long limitAmount;

  private int selfOperated;

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  @Override
  public ProductBasicVO getProduct() {
    return product;
  }

  public void setProduct(ProductBasicVO product) {
    this.product = product;
    if(this.product != null){
      product.setSelfOperated(this.selfOperated);
    }
  }

  public PromotionPgPrice getPricing() {
    return pricing;
  }

  public void setPricing(PromotionPgPrice pricing) {
    this.pricing = pricing;
  }

  @Override
  public Promotion getPromotion() {
    return promotion;
  }

  public void setPromotion(Promotion promotion) {
    this.promotion = promotion;
    if(this.promotion != null){
      promotion.setSelfOperated(this.getSelfOperated());
    }
  }

  @Override
  public Boolean getPromotionClosed() {
    return promotion.getClosed();
  }

  @Override
  public String getTitle() {
    return promotion.getTitle();
  }

  @Override
  public Date getValidFrom() {
    return promotion.getValidFrom();
  }

  @Override
  public Date getValidTo() {
    return promotion.getValidTo();
  }

  @Override
  public PromotionType getPromotionType() {
      return promotion.getPromotionType();
  }

  @Override
  public BigDecimal getPromotionPrice() {
    if (promotion.getPromotionType().equals(PromotionType.FLASHSALE)) {
      BigDecimal price = product.getPrice();
      BigDecimal discount = getDiscount();
      if (price == null || discount == null) {
        return BigDecimal.ZERO;
      }
      // 表示不再使用原来的秒杀价格配置
      if (discount.signum() == 0) {
        return Optional.ofNullable(pricing).map(PromotionPgPrice::getPromotionPrice)
                .orElse(BigDecimal.ZERO);
      }
      return price.multiply(discount).divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN);
    } else if (promotion.getPromotionType().equals(PromotionType.SALEZONE)) {
      return promotion.getPrice();
    }
    return BigDecimal.ZERO;
  }

  /**
   * 获取活动德分
   */
  public BigDecimal getPromotionPoint() {
    return Optional.ofNullable(pricing).map(PromotionPgPrice::getPoint)
            .orElse(BigDecimal.ZERO);
  }

  public BigDecimal getPromotionConversionPrice() {
    BigDecimal changePrice = pricing.getChangePrice();
    if (changePrice.signum() != 0) {
      return changePrice;
    }
    return getPromotionPrice().subtract(getPromotionPoint().divide(BigDecimal.TEN, 2, RoundingMode.HALF_EVEN));
  }

  public Long getLimitAmount() {
    return limitAmount;
  }

  public void setLimitAmount(Long limitAmount) {
    this.limitAmount = limitAmount;
  }
}
