package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;

/**
 * Created by chh on 16/12/06.
 */
public class HomeItemVO extends HomeItem {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }
}
