package com.xquark.dal.vo;


import com.xquark.dal.BaseEntityImpl;

import java.math.BigDecimal;

public class TwitterChildrenCmVO extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String phone;

  private BigDecimal balance;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }
}
