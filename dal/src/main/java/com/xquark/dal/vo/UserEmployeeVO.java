package com.xquark.dal.vo;

import com.xquark.dal.model.User;

/**
 * Created by wangxinhua on 2018/4/11. DESC:
 */
public class UserEmployeeVO extends User {

  private String employeeName;

  private Long employeeId;

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }

  public Long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Long employeeId) {
    this.employeeId = employeeId;
  }

  public boolean getIsEmployee() {
    return employeeId != null;
  }
}
