package com.xquark.dal.vo;

/**
 * @author gxl
 */
public class GrandSalePromotionVO {
    /**
     * 促销活动id
     */
    private Integer id;

    /**
     * 促销活动名称
     */
    private String name;

    /**
     * 首页图片地址
     */
    private String homePageBanner;

    /**
     * 详情图片地址
     */
    private String detailsPageBanner;

    /**
     * 支付成功页图片地址
     */
    private String successPageBanner;

    /**
     * 首页活动飘窗Icon图片地址
     */
    private String homePageGiftIconBanner;

    /**
     * 跳转路径
     */
    private String targetUrl;

    /**
     * 活动状态：0->未开始,1->进行中,2->已结束
     */
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHomePageBanner() {
        return homePageBanner;
    }

    public void setHomePageBanner(String homePageBanner) {
        this.homePageBanner = homePageBanner;
    }

    public String getDetailsPageBanner() {
        return detailsPageBanner;
    }

    public void setDetailsPageBanner(String detailsPageBanner) {
        this.detailsPageBanner = detailsPageBanner;
    }

    public String getSuccessPageBanner() {
        return successPageBanner;
    }

    public void setSuccessPageBanner(String successPageBanner) {
        this.successPageBanner = successPageBanner;
    }

    public String getHomePageGiftIconBanner() {
        return homePageGiftIconBanner;
    }

    public void setHomePageGiftIconBanner(String homePageGiftIconBanner) {
        this.homePageGiftIconBanner = homePageGiftIconBanner;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}