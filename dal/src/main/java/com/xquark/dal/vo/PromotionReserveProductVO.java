package com.xquark.dal.vo;

import com.xquark.dal.model.ChangePriceItem;
import com.xquark.dal.model.DynamicPricing;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPgPrice;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

/**
 * @author: yyc
 * @date: 19-4-17 下午2:00
 */
public class PromotionReserveProductVO extends BasePromotionProductVO implements ChangePriceItem {

    private BigDecimal promotionPrice;

    private BigDecimal promotionConversionPrice;

    private BigDecimal promotionPoint;

    private PromotionPgPrice pricing;

    /**
     * 当前预购人数
     */
    private Integer preReserveCount;

    /**
     * 预购限制
     */
    private Integer reserveLimit;

    /**
     * 活动状态(１.即将开始,2.进行中,3.已结束,4.已终止)
     */
    private Integer status;

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Long getAmount() {
        return null;
    }

    @Override
    public Boolean getPromotionClosed() {
        Date now = new Date();
        return getValidFrom().before(now) && getValidTo().after(now) && status != null && status > 2;
    }

    @Override
    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    @Override
    public BigDecimal getPromotionConversionPrice() {
        return promotionConversionPrice;
    }

    @Override
    public BigDecimal getPromotionPoint() {
        return promotionPoint;
    }

    @Override
    public PromotionPgPrice getPricing() {
        return pricing;
    }

    public void setPromotionConversionPrice(BigDecimal promotionConversionPrice) {
        this.promotionConversionPrice = promotionConversionPrice;
    }

    public void setPromotionPoint(BigDecimal promotionPoint) {
        this.promotionPoint = promotionPoint;
    }

    @Override
    public BigDecimal getPrice() {
        return getPromotionPrice();
    }

    @Override
    public BigDecimal getPoint() {
        return getPromotionPoint();
    }

    @Override
    public BigDecimal getServerAmt() {
        return getVal(DynamicPricing::getServerAmt).orElse(BigDecimal.ZERO);
    }

    @Override
    public BigDecimal getReduction() {
        return getVal(DynamicPricing::getReduction).orElse(BigDecimal.ZERO);
    }

    public void setPricing(PromotionPgPrice pricing) {
        this.pricing = pricing;
    }

    public Integer getPreReserveCount() {
        return preReserveCount;
    }

    public void setPreReserveCount(Integer preReserveCount) {
        this.preReserveCount = preReserveCount;
    }

    public Integer getReserveLimit() {
        return reserveLimit;
    }

    public void setReserveLimit(Integer reserveLimit) {
        this.reserveLimit = reserveLimit;
    }

    public boolean getIsQuotaUsedOut() {
        if (preReserveCount == null || reserveLimit == null) {
            return true;
        }
        return preReserveCount >= reserveLimit;
    }

    public String getPromotionId() {
        return Optional.ofNullable(getPromotion())
                .map(Promotion::getId)
                .orElse("");
    }

    /**
     * 优先从价格体系中取，否则从商品中取
     */
    private <T> Optional<T> getVal(Function<? super DynamicPricing, ? extends T> getFunc) {
        DynamicPricing target = pricing == null ? getProduct() : pricing;
        if (target != null) {
            return Optional.ofNullable(getFunc.apply(target));
        }
        return Optional.empty();
    }
}
