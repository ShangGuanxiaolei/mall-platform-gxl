package com.xquark.dal.vo;

import com.xquark.dal.model.HealthTestAnswer;
import com.xquark.dal.model.HealthTestQuestion;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-4. DESC:
 */
public class HealthTestQuestionVO extends HealthTestQuestion {

  private String groupName;

  private List<HealthTestAnswer> answers;

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public List<HealthTestAnswer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<HealthTestAnswer> answers) {
    this.answers = answers;
  }

}

