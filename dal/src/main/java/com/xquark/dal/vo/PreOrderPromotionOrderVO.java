package com.xquark.dal.vo;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.status.OrderStatus;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-12-5. DESC:
 */
@ApiModel(value = "订单相关的预购活动VO")
public class PreOrderPromotionOrderVO extends PreOrderPromotionUserVO {

  @ApiModelProperty(value = "订单id")
  private String orderId;

  @ApiModelProperty(value = "订单号")
  private String orderNo;

  @ApiModelProperty(value = "用户名")
  private String userName;

  @ApiModelProperty(value = "用户手机号")
  private String userPhone;

  @ApiModelProperty(value = "订单总金额")
  private BigDecimal orderFee;

  @ApiModelProperty(value = "订单折扣价格")
  private BigDecimal orderDiscountFee;

  @ApiModelProperty(value = "订单状态")
  private OrderStatus orderStatus;

  @ApiModelProperty(value = "申请时间")
  private Date applyTime;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public BigDecimal getOrderFee() {
    return orderFee;
  }

  public void setOrderFee(BigDecimal orderFee) {
    this.orderFee = orderFee;
  }

  public BigDecimal getOrderDiscountFee() {
    return orderDiscountFee;
  }

  public void setOrderDiscountFee(BigDecimal orderDiscountFee) {
    this.orderDiscountFee = orderDiscountFee;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }
}
