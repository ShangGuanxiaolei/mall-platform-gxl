package com.xquark.dal.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wangxinhua on 17-7-5. DESC: 菜单跟对应的题目以及题目的选项
 */
public class HealthTestModuleQuestionVO implements Serializable {

  private String moduleId;
  private String moduleName;
  private String staticName;

  private List<HealthTestQuestionVO> questions;

  public String getModuleId() {
    return moduleId;
  }

  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }

  public String getModuleName() {
    return moduleName;
  }

  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  public List<HealthTestQuestionVO> getQuestions() {
    return questions;
  }

  public void setQuestions(List<HealthTestQuestionVO> questions) {
    this.questions = questions;
  }

  public String getStaticName() {
    return staticName;
  }

  public void setStaticName(String staticName) {
    this.staticName = staticName;
  }
}
