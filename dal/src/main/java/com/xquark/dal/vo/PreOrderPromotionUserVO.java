package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.type.PreOrderApplyStatus;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@ApiModel(value = "预购商品详情")
public class PreOrderPromotionUserVO {

  @ApiModelProperty(name = "id", value = "活动商品id")
  private String id;

  @ApiModelProperty(name = "promotionId", value = "活动id")
  private String promotionId;

  @ApiModelProperty(name = "productId", value = "商品id")
  private String productId;

  @ApiModelProperty(name = "promotionName", value = "活动名", notes = "对应设计中圣诞节敬请期待")
  private String promotionName;

  @ApiModelProperty(name = "productName", value = "商品名")
  private String productName;

  @ApiModelProperty(name = "productImg", value = "商品图片")
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  @ApiModelProperty(name = "status", value = "活动状态", notes = "当前浏览用户对应该活动的状态", example = "PREORDER, AUDIT_PASS, AUDIT_REJECT, SUCCESS")
  private PreOrderApplyStatus status;

  @ApiModelProperty(name = "validFrom", value = "活动开始时间")
  private Date validFrom;

  @ApiModelProperty(name = "validTo", value = "活动结束时间")
  private Date validTo;

  @ApiModelProperty(name = "amount", value = "当前剩余库存")
  private Integer amount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public PreOrderApplyStatus getStatus() {
    return status;
  }

  public void setStatus(PreOrderApplyStatus status) {
    this.status = status;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }
}
