package com.xquark.dal.vo;

public class PromotionBargainPlaceVO {

  private String img;

  private String name;

  private String skuId;

  private String bargainId;

  private String originalPrice;

  private String reservePrice;

  private Integer stock;

  private Integer launchNum;

  /** 被助力的次数 */
  private Integer helpNum;

  private String orderStatus;

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(String originalPrice) {
    this.originalPrice = originalPrice;
  }

  public String getReservePrice() {
    return reservePrice;
  }

  public void setReservePrice(String reservePrice) {
    this.reservePrice = reservePrice;
  }

  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  public Integer getLaunchNum() {
    return launchNum;
  }

  public void setLaunchNum(Integer launchNum) {
    this.launchNum = launchNum;
  }

  public Integer getHelpNum() {
    return helpNum;
  }

  public void setHelpNum(Integer helpNum) {
    this.helpNum = helpNum;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }
}
