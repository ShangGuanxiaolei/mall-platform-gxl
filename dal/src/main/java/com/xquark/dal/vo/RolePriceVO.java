package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

/**
 * Created by chh on 16-12-13.
 */
public class RolePriceVO extends RolePrice {

  private String roleCode;
  private String roleName;
  private String roleBelong;
  private String productName;
  private String productPrice;
  private String productImg;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImgUrl;

  public String getRoleCode() {
    return roleCode;
  }

  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleBelong() {
    return roleBelong;
  }

  public void setRoleBelong(String roleBelong) {
    this.roleBelong = roleBelong;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(String productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getProductImgUrl() {
    return productImgUrl;
  }

  public void setProductImgUrl(String productImgUrl) {
    this.productImgUrl = productImgUrl;
  }
}
