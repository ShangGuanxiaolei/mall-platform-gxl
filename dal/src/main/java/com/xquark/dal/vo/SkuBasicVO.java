package com.xquark.dal.vo;

import com.xquark.dal.model.Sku;
import com.xquark.dal.status.ProductStatus;
import org.apache.commons.lang3.StringUtils;

/**
 * @author wangxinhua.
 * @date 2018/9/29
 */
public class SkuBasicVO extends Sku {

  private String productName;

  private ProductStatus status;

  private Boolean select;

  /**
   * 拼接商品名跟sku名称
   */
  public String getFullName() {
    StringBuilder builder = new StringBuilder();
    if (StringUtils.isNotBlank(productName)) {
      builder.append(productName);
    }
    if (StringUtils.isNotBlank(getSpec())) {
      builder.append(getSpec());
    }
    if (StringUtils.isNotBlank(getSpec1())) {
      builder.append(getSpec1());
    }
    return builder.toString();
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public Boolean getSelect() {
    return select;
  }

  public void setSelect(Boolean select) {
    this.select = select;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }

  public String getStatusStr() {
    if (status == null) {
      return "";
    }
    return status.getNamecn();
  }
}
