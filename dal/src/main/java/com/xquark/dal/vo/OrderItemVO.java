package com.xquark.dal.vo;

import com.xquark.dal.model.OrderItem;

import java.math.BigDecimal;

/**
 * Created by liangfan on 16/10/11.
 */
public class OrderItemVO extends OrderItem {

  private BigDecimal commission;

  private String productUrl;

  private String b2bProductUrl;

  public String getB2bProductUrl() {
    return b2bProductUrl;
  }

  public void setB2bProductUrl(String b2bProductUrl) {
    this.b2bProductUrl = b2bProductUrl;
  }

  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public BigDecimal getCommission() {
    return commission;
  }

  public void setCommission(BigDecimal commission) {
    this.commission = commission;
  }

}
