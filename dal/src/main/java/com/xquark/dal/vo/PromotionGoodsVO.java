package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.math.BigDecimal;
import java.util.Date;

public class PromotionGoodsVO extends BaseEntityImpl implements Archivable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "商品名称")
    private String name; // 商品名称
    private BigDecimal disCountPrice;
    private int productId;
    private String skuCode;
    private int goodsIsQua;
    private String orgGroupQua;
    private String hdsType;
    private String vivilifeType;
    private Boolean  qualityGoods; //是否有赠品
    private int selfOperated; //是否自营:0非自营，1自营

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }
    private int pieceGroupNum;//成团人数
    private Double skuDiscount;
    private Date effectFrom;
    private Date effectTo;
    private String pCode;
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;  // 主图code
    private String fullImg;  // 主图code
    @ApiModelProperty(value = "商品市场价")
    private BigDecimal marketPrice; // 市场价
    /** 是否新人团 */
    private boolean isNewPg;

    public Boolean getQualityGoods() {
        return qualityGoods;
    }

    public void setQualityGoods(Boolean qualityGoods) {
        this.qualityGoods = qualityGoods;
    }

    public String getHdsType() {
        return hdsType;
    }

    public void setHdsType(String hdsType) {
        this.hdsType = hdsType;
    }

    public String getVivilifeType() {
        return vivilifeType;
    }

    public void setVivilifeType(String vivilifeType) {
        this.vivilifeType = vivilifeType;
    }

    public int getGoodsIsQua() {
        return goodsIsQua;
    }

    public void setGoodsIsQua(int goodsIsQua) {
        this.goodsIsQua = goodsIsQua;
    }

    public String getOrgGroupQua() {
        return orgGroupQua;
    }

    public void setOrgGroupQua(String orgGroupQua) {
        this.orgGroupQua = orgGroupQua;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullImg() {
        return fullImg;
    }

    public void setFullImg(String fullImg) {
        this.fullImg = fullImg;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getDisCountPrice() {
        return disCountPrice;
    }

    public void setDisCountPrice(BigDecimal disCountPrice) {
        this.disCountPrice = disCountPrice;
    }

    public int getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(int pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public Double getSkuDiscount() {
        return skuDiscount;
    }

    public void setSkuDiscount(Double skuDiscount) {
        this.skuDiscount = skuDiscount;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public boolean getIsNewPg() {
        return isNewPg;
    }

    public void setIsNewPg(boolean newPg) {
        isNewPg = newPg;
    }

    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }
}
