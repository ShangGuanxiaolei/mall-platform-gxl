package com.xquark.dal.vo;

import com.xquark.dal.model.CombinationItem;

/**
 * User: byy Date: 18-6-1. Time: 上午10:28 商品VO类
 */
public class CombinationItemVO extends CombinationItem {
  //商品名称
  private String productName;
  //商品主图
  private String img;

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  @Override
  public String toString() {
    return super.toString()+"CombinationItemVo{" +
        "productName='" + productName + '\'' +
        ", img='" + img + '\'' +
        '}';
  }


}
