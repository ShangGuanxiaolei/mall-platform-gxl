package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.model.PromotionProduct;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by chh on 16-10-26.
 */
public class PromotionDiamondVO extends PromotionDiamond implements PromotionProduct {

  private BigDecimal amount;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validFrom;
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  private Date validTo;

  private Boolean promotionArchive;

  public Boolean getPromotionArchive() {
    return promotionArchive;
  }

  public void setPromotionArchive(Boolean promotionArchive) {
    this.promotionArchive = promotionArchive;
  }

  @Override
  public BigDecimal getAmount() {
    return this.amount;
  }

  @Override
  public boolean getIsSellOut() {
    return getAmount() == null || getAmount().compareTo(BigDecimal.ZERO) <= 0;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidFrom() {
    return validFrom;
  }

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  public Date getValidTo() {
    return validTo;
  }

}
