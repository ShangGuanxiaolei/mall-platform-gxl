package com.xquark.dal.vo;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntityImpl;
import com.xquark.dal.model.PromotionPgPrice;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author LiHaoYang
 * @ClassName PromotionGoodsDetailVO
 * @date 2018/9/7 0007
 */
public class PromotionGoodsDetailVO extends BaseEntityImpl implements Archivable {

    private String pCode;
    private BigDecimal openMemberPrice;
    private int isWithPoint;
    private BigDecimal marketPrice;
    private BigDecimal disCountPrice;
    private Date effectFrom;
    private Date effectTo;
    private String pDetailCode;
    private Integer jumpQueueTime;
    private int skuLimit;
    private boolean isQua;
    private int num;
    private int selfOperated; //是否自营:0非自营，1自营

    private PromotionPgPrice pgPrice;

    public int getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(int selfOperated) {
        this.selfOperated = selfOperated;
    }

    /** 活动价 */
    private BigDecimal promotionPrice = BigDecimal.ZERO;

    public String getEffectFromInfo() {
        return effectFromInfo;
    }

    public void setEffectFromInfo(String effectFromInfo) {
        this.effectFromInfo = effectFromInfo;
    }

    public String getEffectToInfo() {
        return effectToInfo;
    }

    public void setEffectToInfo(String effectToInfo) {
        this.effectToInfo = effectToInfo;
    }

    private String effectFromInfo="活动尚未开始，请耐心等待";
    private String effectToInfo="您来晚了，活动已结束";

    public BigDecimal getOpenMemberPrice() {
        return openMemberPrice;
    }

    public void setOpenMemberPrice(BigDecimal openMemberPrice) {
        this.openMemberPrice = openMemberPrice;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public boolean isQua() {
        return isQua;
    }

    public void setQua(boolean qua) {
        isQua = qua;
    }

    public int getSkuLimit() {
        return skuLimit;
    }

    public void setSkuLimit(int skuLimit) {
        this.skuLimit = skuLimit;
    }

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }


    public BigDecimal getDisCountPrice() {
        return disCountPrice;
    }

    public void setDisCountPrice(BigDecimal disCountPrice) {
        this.disCountPrice = disCountPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    private int pieceGroupNum;
    private double skuDiscount;


    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public int getIsWithPoint() {
        return isWithPoint;
    }

    public void setIsWithPoint(int isWithPoint) {
        this.isWithPoint = isWithPoint;
    }

    public int getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(int pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public double getSkuDiscount() {
        return skuDiscount;
    }

    public void setSkuDiscount(double skuDiscount) {
        this.skuDiscount = skuDiscount;
    }

    @Override
    public Boolean getArchive() {
        return null;
    }

    @Override
    public void setArchive(Boolean archive) {

    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getJumpQueueTime() {
        return jumpQueueTime;
    }

    public void setJumpQueueTime(Integer jumpQueueTime) {
        this.jumpQueueTime = jumpQueueTime;
    }

    public PromotionPgPrice getPgPrice() {
        return pgPrice;
    }

    public void setPgPrice(PromotionPgPrice pgPrice) {
        this.pgPrice = pgPrice;
    }
}
