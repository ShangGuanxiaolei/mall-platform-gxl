package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.ProductCollection;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/08/01.
 */
public class ProductCollectionVO extends ProductCollection implements FmtQiNiuImgVO {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String userName;

  private String userAvatar;

  private String userPhone;

  private String productName;

  private String productModel;

  private BigDecimal point;//积分

  private BigDecimal deductionDPoint;//德分

  private BigDecimal netWorth;//净值

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private BigDecimal productPrice;

  private String siteHost = "http://nffclub.com";

  private String targetUrl;

  private BigDecimal serverAmt;
  private BigDecimal promoAmt;

  private BigDecimal memberPrice = BigDecimal.ZERO;//会员价
  private BigDecimal proxyPrice = BigDecimal.ZERO;//代理价
  private BigDecimal changePrice = BigDecimal.ZERO;//兑换价现金部分

  private boolean hadGift;
  private String giftType;

  private boolean isSoldOut;
  private String status; //是否已经下架
  //是否新人专区商品
  private boolean freshmanProduct;

  public boolean isFreshmanProduct() {
    return freshmanProduct;
  }

  public void setFreshmanProduct(boolean freshmanProduct) {
    this.freshmanProduct = freshmanProduct;
  }
  private int selfOperated; //是否自营:0非自营，1自营

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public boolean getIsSoldOut() {
    return isSoldOut;
  }

  public void setIsSoldOut(boolean soldOut) {
    isSoldOut = soldOut;
  }

  public boolean isHadGift() {
    return hadGift;
  }

  public void setHadGift(boolean hadGift) {
    this.hadGift = hadGift;
  }

  public String getGiftType() {
    return giftType;
  }

  public void setGiftType(String giftType) {
    this.giftType = giftType;
  }

  public String getTargetUrl() {
    String targetUrl = siteHost + "/p/" + this.getProductId();
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserAvatar() {
    return userAvatar;
  }

  public void setUserAvatar(String userAvatar) {
    this.userAvatar = userAvatar;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductModel() {
    return productModel;
  }

  public void setProductModel(String productModel) {
    this.productModel = productModel;
  }

  @Override
  public String getTinyImgUrl() {
    return productImg;
  }

  @Override
  public String getMiddleImgUrl() {
    return productImg;
  }


  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public BigDecimal getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(BigDecimal netWorth) {
    this.netWorth = netWorth;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }

  public BigDecimal getMemberPrice() {
    return memberPrice;
  }

  public void setMemberPrice(BigDecimal memberPrice) {
    this.memberPrice = memberPrice;
  }

  public BigDecimal getProxyPrice() {
    return proxyPrice;
  }

  public void setProxyPrice(BigDecimal proxyPrice) {
    this.proxyPrice = proxyPrice;
  }

  public BigDecimal getChangePrice() {
    return changePrice;
  }

  public void setChangePrice(BigDecimal changePrice) {
    this.changePrice = changePrice;
  }
}
