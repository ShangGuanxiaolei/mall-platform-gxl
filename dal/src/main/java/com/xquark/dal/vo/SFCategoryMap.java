package com.xquark.dal.vo;

/**
 * @auther liuwei
 * @date 2018-07-04 1:15
 */
public class SFCategoryMap {
  private String sfCategoryCode;
  private String sfCategoryName;

  public String getSfCategoryCode() {
    return sfCategoryCode;
  }

  public void setSfCategoryCode(String sfCategoryCode) {
    this.sfCategoryCode = sfCategoryCode;
  }

  public String getSfCategoryName() {
    return sfCategoryName;
  }

  public void setSfCategoryName(String sfCategoryName) {
    this.sfCategoryName = sfCategoryName;
  }
}
