package com.xquark.dal.vo;

import com.xquark.dal.model.Shop;

public class ShopProp extends Shop {

  private static final long serialVersionUID = 1L;

  private Boolean autoUpdated;

  private String thirdShopUrl;

  public Boolean getAutoUpdated() {
    return autoUpdated;
  }

  public void setAutoUpdated(Boolean autoUpdated) {
    this.autoUpdated = autoUpdated;
  }

  public String getThirdShopUrl() {
    return thirdShopUrl;
  }

  public void setThirdShopUrl(String thirdShopUrl) {
    this.thirdShopUrl = thirdShopUrl;
  }
}
