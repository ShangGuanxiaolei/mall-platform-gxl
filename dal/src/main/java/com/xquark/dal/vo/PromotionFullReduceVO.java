package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullReduceDiscount;

import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;

/**
 * Created by wangxinhua on 17-10-9. DESC:
 */
public abstract class PromotionFullReduceVO<T extends Number> extends Promotion {

  /**
   * 折扣价
   */
  private BigDecimal discountFee;

  public BigDecimal getDiscountFee() {
    return discountFee;
  }

  public void setDiscountFee(BigDecimal discountFee) {
    this.discountFee = discountFee;
  }

  public abstract List<? extends PromotionFullReduceDiscount<T>> getDiscounts();

  /**
   * 根据条件匹配满足的最大折扣
   *
   * @param t 匹配条件数值
   * @return {@link PromotionFullReduceDiscount<T>} 最大折扣 || null
   */
  public abstract PromotionFullReduceDiscount<T> getSatisfiedDiscount(T t);

}
