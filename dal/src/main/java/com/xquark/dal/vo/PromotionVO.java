package com.xquark.dal.vo;

import com.xquark.dal.model.Promotion;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动vo，一个活动对应多个对应商品信息 Created by chh on 17-07-11.
 */
public class PromotionVO extends Promotion {

  // 团购商品信息
  ArrayList<PromotionGrouponProductVO> groupOn = new ArrayList<PromotionGrouponProductVO>();

  // 品牌专题信息
  ArrayList<PromotionDiamondProductVO> diamond = new ArrayList<PromotionDiamondProductVO>();

  //大会售卖商品信息
  List<PromotionPGoodsVO> pGoodsVOS = new ArrayList<>();

  public List<PromotionPGoodsVO> getpGoodsVOS() {
    return pGoodsVOS;
  }

  public void setpGoodsVOS(List<PromotionPGoodsVO> pGoodsVOS) {
    this.pGoodsVOS = pGoodsVOS;
  }

  public ArrayList<PromotionDiamondProductVO> getDiamond() {
    return diamond;
  }

  public void setDiamond(ArrayList<PromotionDiamondProductVO> diamond) {
    this.diamond = diamond;
  }

  public ArrayList<PromotionGrouponProductVO> getGroupOn() {
    return groupOn;
  }

  public void setGroupOn(ArrayList<PromotionGrouponProductVO> groupOn) {
    this.groupOn = groupOn;
  }
}
