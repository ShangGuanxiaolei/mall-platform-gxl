package com.xquark.dal.vo;

import com.xquark.dal.model.UserTeam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chh on 17/07/13.
 */
public class UserTeamVO extends UserTeam {

  protected Logger log = LoggerFactory.getLogger(getClass());

  private String userName;

  private String userPhone;

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

}
