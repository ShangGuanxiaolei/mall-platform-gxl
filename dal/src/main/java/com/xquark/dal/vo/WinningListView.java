package com.xquark.dal.vo;

import com.xquark.dal.model.WinningList;
import com.xquark.dal.status.WinningRank;
import com.xquark.dal.status.WinningStatus;

public class WinningListView {

	private final WinningList winningList;

	private final WinningProductVO product;

	private final WinningRank winningRank;

	private final WinningStatus state;


	public WinningListView(WinningList winningList, WinningProductVO product, int winningRank, int state) {
		this.winningList = winningList;
		this.product = product;
		this.winningRank = WinningRank.getEnum(winningRank);
		this.state = WinningStatus.getEnum(state);
	}

	public WinningList getWinningList() {
		return winningList;
	}

	public WinningProductVO getProduct() {
		return product;
	}

	public WinningRank getWinningRank() {
		return winningRank;
	}

	public WinningStatus getState() {
		return state;
	}
}
