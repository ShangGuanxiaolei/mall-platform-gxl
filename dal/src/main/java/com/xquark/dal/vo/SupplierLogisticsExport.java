package com.xquark.dal.vo;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @auther liuwei
 * @date 2018-06-30 19:50
 */
public class SupplierLogisticsExport {

  private String orderNo;//订单号
  @NotEmpty
  private String logisticsOrderNo;//物流单号
  @NotEmpty
  private String logisticsCompany;//物流公司
  private Date createdAt;//下单时间
  private String buyerPhone;//收件人电话
  private String buyerName;//收件人姓名

  //新增多个单号
  private String logisticsOrderNo2;//物流单号
  private String logisticsOrderNo3;//物流单号
  private String logisticsOrderNo4;//物流单号
  private String logisticsOrderNo5;//物流单号


  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }

  public String getCreatedAt() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(createdAt);
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getLogisticsOrderNo2() {
    return logisticsOrderNo2;
  }

  public void setLogisticsOrderNo2(String logisticsOrderNo2) {
    this.logisticsOrderNo2 = logisticsOrderNo2;
  }

  public String getLogisticsOrderNo3() {
    return logisticsOrderNo3;
  }

  public void setLogisticsOrderNo3(String logisticsOrderNo3) {
    this.logisticsOrderNo3 = logisticsOrderNo3;
  }

  public String getLogisticsOrderNo4() {
    return logisticsOrderNo4;
  }

  public void setLogisticsOrderNo4(String logisticsOrderNo4) {
    this.logisticsOrderNo4 = logisticsOrderNo4;
  }

  public String getLogisticsOrderNo5() {
    return logisticsOrderNo5;
  }

  public void setLogisticsOrderNo5(String logisticsOrderNo5) {
    this.logisticsOrderNo5 = logisticsOrderNo5;
  }
}
