package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.List;
import java.util.Map;

/**
 * 作者: wangxh 创建日期: 17-3-16 简介:
 */
public class OrgTreeVO {

  private String id;
  private String text;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String icon;

  private List<OrgTreeVO> children;

  private Map state;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Object getChildren() {
    return children;
  }

  public void setChildren(List<OrgTreeVO> children) {
    this.children = children;
  }

  public Map getState() {
    return state;
  }

  public void setState(Map state) {
    this.state = state;
  }
}
