package com.xquark.dal.vo;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2018/10/7
 * Time: 14:38
 * Description: Vip资格套装
 */
public class VipSuitVo {
    private String img;//商品图片

    private String code;//商品id

    private String productName;//商品名称

    private Integer status;//状态

    private String supplierName;//供应商

    private Double privce;//售价

    private Integer skuNum;//商品库存

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Double getPrivce() {
        return privce;
    }

    public void setPrivce(Double privce) {
        this.privce = privce;
    }

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }
}