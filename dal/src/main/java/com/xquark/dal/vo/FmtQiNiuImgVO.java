package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonImgMediaSerialize;
import com.xquark.dal.util.json.JsonImgTinySerialize;

/**
 * Created by wangxinhua on 18-3-29. DESC: 格式化七牛图片VO接口
 */
public interface FmtQiNiuImgVO {

  @JsonSerialize(using = JsonImgTinySerialize.class)
  String getTinyImgUrl();

  @JsonSerialize(using = JsonImgMediaSerialize.class)
  String getMiddleImgUrl();

}
