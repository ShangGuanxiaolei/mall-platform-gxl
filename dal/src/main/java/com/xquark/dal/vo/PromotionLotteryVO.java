package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionList;
import com.xquark.dal.model.PromotionLottery;

import java.math.BigDecimal;

/**
 * @author wangxinhua
 * @date 2019-04-28
 * @since 1.0
 */
public class PromotionLotteryVO extends PromotionLottery {

    private PromotionList detail;

    private BigDecimal totalPoint;

    public PromotionList getDetail() {
        return detail;
    }

    public void setDetail(PromotionList detail) {
        this.detail = detail;
    }

    public BigDecimal getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(BigDecimal totalPoint) {
        this.totalPoint = totalPoint;
    }
}
