package com.xquark.dal.vo;

import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import java.util.Date;
import java.util.List;

/**
 * Created by wangxinhua on 17-5-7. DESC:
 */
public class YundouRuleVO extends YundouRule {

  private static final long serialVersionUID = 1088442502751009198L;

  private String roleId;
  private String roleName;
  private String roleCode;
  private Long amount;

  private List<YundouRuleDetail> details;

  @Override
  public String getId() {
    return super.getId();
  }

  @Override
  public void setId(String id) {
    super.setId(id);
  }

  @Override
  public String getOperationId() {
    return super.getOperationId();
  }

  @Override
  public void setOperationId(String operationId) {
    super.setOperationId(operationId);
  }

  public String getRoleId() {
    return roleId;
  }

  public void setRoleId(String roleId) {
    this.roleId = roleId;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  @Override
  public Integer getCode() {
    return super.getCode();
  }

  @Override
  public void setCode(Integer code) {
    super.setCode(code);
  }

  public String getRoleCode() {
    return roleCode;
  }

  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  @Override
  public String getName() {
    return super.getName();
  }

  @Override
  public void setName(String name) {
    super.setName(name);
  }

  @Override
  public String getDescription() {
    return super.getDescription();
  }

  @Override
  public void setDescription(String description) {
    super.setDescription(description);
  }

  @Override
  public Date getCreatedAt() {
    return super.getCreatedAt();
  }

  @Override
  public void setCreatedAt(Date createdAt) {
    super.setCreatedAt(createdAt);
  }

  @Override
  public Date getUpdatedAt() {
    return super.getUpdatedAt();
  }

  public List<YundouRuleDetail> getDetails() {
    return details;
  }

  public void setDetails(List<YundouRuleDetail> details) {
    this.details = details;
  }

  @Override
  public void setUpdatedAt(Date updatedAt) {
    super.setUpdatedAt(updatedAt);
  }
}
