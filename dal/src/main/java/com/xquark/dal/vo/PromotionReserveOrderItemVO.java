package com.xquark.dal.vo;

import com.xquark.dal.model.PromotionReserveOrderItem;

import java.util.Date;

/**
 * @author: yyc
 * @date: 19-4-22 上午11:46
 */
public class PromotionReserveOrderItemVO extends PromotionReserveOrderItem {

  /** 产品名 */
  private String productName;

  /** 商品主图 */
  private String productImg;

  /** 规格名 */
  private String skuStr;

  /** 预约开始 */
  private Date validFrom;

  /** 预约结束 */
  private Date validTo;

  /** 支付开始 */
  private Date payFrom;

  /** 支付结束 */
  private Date payTo;

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }

  public Boolean getCanPay() {
    Date now = new Date();
    return now.after(payFrom) && now.before(payTo);
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Date getPayFrom() {
    return payFrom;
  }

  public void setPayFrom(Date payFrom) {
    this.payFrom = payFrom;
  }

  public Date getPayTo() {
    return payTo;
  }

  public void setPayTo(Date payTo) {
    this.payTo = payTo;
  }
}
