package com.xquark.dal.vo;

/**
 * 作者: wangxh 创建日期: 17-4-5 简介: URL与角色对应关系
 */
public class UrlRoleVO {

  private String url;
  private String role;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }
}
