package com.xquark.dal.vo;

import java.math.BigDecimal;

public class SyncActivityVO {

  private String actId;
  private String actName;
  private String productId;
  private String shopId;
  private BigDecimal markPrice;
  private BigDecimal actPrice;

  public String getActId() {
    return actId;
  }

  public void setActId(String actId) {
    this.actId = actId;
  }

  public String getActName() {
    return actName;
  }

  public void setActName(String actName) {
    this.actName = actName;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public BigDecimal getMarkPrice() {
    return markPrice;
  }

  public void setMarkPrice(BigDecimal markPrice) {
    this.markPrice = markPrice;
  }

  public BigDecimal getActPrice() {
    return actPrice;
  }

  public void setActPrice(BigDecimal actPrice) {
    this.actPrice = actPrice;
  }

}
