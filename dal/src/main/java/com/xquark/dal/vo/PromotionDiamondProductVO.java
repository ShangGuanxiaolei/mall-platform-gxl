package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import java.math.BigDecimal;

/**
 * Created by chh on 16-10-26.
 */
public class PromotionDiamondProductVO extends PromotionDiamond {

  private BigDecimal productPrice;
  private String productName;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;
  private String productId;

  private Long productAmount;

  private String siteHost = "http://nffclub.com";

  public String getTargetUrl() {
    return siteHost + "/p/promotion/" + this.getProductId() + "?promotionId=" + super
        .getPromotionId();
  }

  @Override
  public String getProductId() {
    return productId;
  }

  @Override
  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(BigDecimal productPrice) {
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public Long getProductAmount() {
    return productAmount;
  }

  public void setProductAmount(Long productAmount) {
    this.productAmount = productAmount;
  }

  public boolean getIsSellOut() {
    return this.productAmount == null || this.productAmount == 0L;
  }
}
