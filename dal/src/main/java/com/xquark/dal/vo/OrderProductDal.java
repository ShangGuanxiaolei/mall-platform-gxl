package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

/**
 * User: kong Date: 18-7-24. Time: 上午11:15 顺丰订单产品信息
 */
public class OrderProductDal {

  //产品id
  private Long productId;

  //图片地址
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }
}
