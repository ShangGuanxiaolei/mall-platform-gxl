package com.xquark.dal.vo;

import com.google.common.base.Optional;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullCut;
import com.xquark.dal.type.PromotionDiscountType;
import java.util.List;

/**
 * Created by wangxinhua on 2018/4/16. DESC:
 */
public class PromotionFullCutProductVO extends Promotion {

  private PromotionDiscountType discountType;

  private List<? extends PromotionFullCut> products;

  public PromotionDiscountType getDiscountType() {
    return Optional.fromNullable(discountType).or(PromotionDiscountType.PERCENT);
  }

  public void setDiscountType(PromotionDiscountType discountType) {
    this.discountType = discountType;
  }

  public List<? extends PromotionFullCut> getProducts() {
    return products;
  }

  public void setProducts(List<? extends PromotionFullCut> products) {
    this.products = products;
  }
}
