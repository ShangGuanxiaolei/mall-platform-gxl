package com.xquark.dal.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.Shop;

public class ShopVO extends Shop {

  private static final long serialVersionUID = 1L;

  private String phone;

  private String shopUrl;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String bannerUrl;


  public ShopVO() {

  }

  public ShopVO(Shop shop) {
    BeanUtils.copyProperties(shop, this);
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getShopUrl() {
    return shopUrl;
  }

  public void setShopUrl(String shopUrl) {
    this.shopUrl = shopUrl;
  }

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getBannerUrl() {
    return bannerUrl;
  }

  public void setBannerUrl(String bannerUrl) {
    this.bannerUrl = bannerUrl;
  }

}
