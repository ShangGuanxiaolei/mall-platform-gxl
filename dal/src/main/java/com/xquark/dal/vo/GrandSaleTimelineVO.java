package com.xquark.dal.vo;

/**
 * @author gxl
 */
public class GrandSaleTimelineVO {

    /**
     * 活动节点名称
     */
    private String name;

    /**
     * 活动时间节点是否点亮
     */
    private boolean timeNodeLight;

    /**
     * 活动节点持续时间
     */
    private String duration;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTimeNodeLight() {
        return timeNodeLight;
    }

    public void setTimeNodeLight(boolean timeNodeLight) {
        this.timeNodeLight = timeNodeLight;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}