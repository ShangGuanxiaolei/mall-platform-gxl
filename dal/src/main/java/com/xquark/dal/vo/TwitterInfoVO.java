package com.xquark.dal.vo;

import com.xquark.dal.status.TwitterStatus;

import java.math.BigDecimal;


public class TwitterInfoVO {

  String id;
  //用户名
  String name;
  //角色，
  String role;
  //状态，
  String status;
  //分成比例，
  BigDecimal rate;
  //当月佣金
  BigDecimal income;
  //头像
  String avatar;
  //是否叶子节点
  private boolean isLeaf;
  //下级人员个数
  private long count;

  public boolean getIsLeaf() {
    return isLeaf;
  }

  public void setIsLeaf(boolean isleaf) {
    this.isLeaf = isleaf;
  }

  public long getCount() {
    return count;
  }

  public void setCount(long count) {
    this.count = count;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  public BigDecimal getIncome() {
    return income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }
}
