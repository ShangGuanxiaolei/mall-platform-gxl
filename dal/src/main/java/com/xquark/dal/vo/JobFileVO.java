package com.xquark.dal.vo;

import com.xquark.dal.model.JobSchedulerLog;

/**
 * @author wangxinhua on 2018/7/30. DESC:
 */
public class JobFileVO extends JobSchedulerLog {

  private String fileName;

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
}
