package com.xquark.dal;

/**
 * 是否可以被选择接口
 *
 * @author wangxinhua.
 * @date 2018/9/28
 */
public interface Selectable {

  /**
   * 是否被选择
   */
  boolean getSelect();

}
