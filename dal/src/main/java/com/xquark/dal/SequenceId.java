package com.xquark.dal;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午1:49
 */
public class SequenceId {

  public SequenceId() {
  }

  public SequenceId(char stub) {
    this.stub = stub;
  }

  private Long id;

  private char stub;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public char getStub() {
    return stub;
  }

  public void setStub(char stub) {
    this.stub = stub;
  }
}
