package com.xquark.dal.consts;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public class SCamURLConstants {

  /**
   * 使用环境
   */
  private static final String ENV_URL = URLPrefix.OFFICIAL.URL;

  private enum URLPrefix {
    /**
     * 开发环境
     */
    DEV("http://dev.99baby.cn/scrmapp/scrmapp"),

    /**
     * 正式环境
     */
    OFFICIAL("http://wx.qinyuan.cn/wx/scrmapp");

    final String URL;

    URLPrefix(String url) {
      URL = url;
    }
  }

  /**
   * 同步用户地址
   */
  public static final String SYNC_USER = ENV_URL + "/consumer/user/syncUser";

  /**
   * 同步地址信息
   */
  public static final String SYNC_ADDRESS = ENV_URL + "/consumer/wechatuser/syncAddress";

  /**
   * 删除用户地址
   */
  public static final String SYNC_ADDRESS_DEL = ENV_URL + "/consumer/wechatuser/syncAddressDel";

  /**
   * 设置默认地址
   */
  public static final String SYNC_ADDRESS_SET_DEFAULT =
      ENV_URL + "/consumer/wechatuser/syncSetDefault";

}
