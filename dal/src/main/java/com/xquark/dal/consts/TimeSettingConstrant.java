package com.xquark.dal.consts;

/**
 * @author wangxinhua.
 * @date 2018/9/18
 */
public class TimeSettingConstrant {

  public static final String HDS_KEY = "withdraw_hds";
  public static final String HV_KEY = "withdraw_hv";
  public static final String VIVI_KEY = "withdraw_vivi";


}
