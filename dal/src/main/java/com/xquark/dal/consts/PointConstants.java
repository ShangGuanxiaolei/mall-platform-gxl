package com.xquark.dal.consts;

import java.math.BigDecimal;

/**
 * created by
 *
 * @author wangxinhua at 18-6-12 下午4:31 积分常量
 */
public class PointConstants {

  public static final String DEDUCT_POINT_CODE = "1002";
  public static final String DEDUCT_COMM_CODE = "2002";

  /**
   * 全局德分比例
   */
  public static final BigDecimal POINT_SCALA = BigDecimal.valueOf(10);

  /**
   * 全局积分比例
   */
  public static final BigDecimal COMMISSION_SCALA = BigDecimal.ONE;

}
