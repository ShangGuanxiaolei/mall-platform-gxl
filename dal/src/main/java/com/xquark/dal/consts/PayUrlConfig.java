package com.xquark.dal.consts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 2018/4/9. DESC:
 */
@Component
public class PayUrlConfig {

  // FIXME 将该配置写到配置文件中
  @Value("${site.web.host.usingHttps}")
  private Integer usingHttps;

  @Value("${site.webapi.host.name}")
  private String siteHost;

  public String hostName() {
    return siteHost;
  }

}
