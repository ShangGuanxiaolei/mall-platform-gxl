package com.xquark.dal.consts;

/**
 * Created by wangxinhua on 17-8-24. DESC:
 */
public class WechatConstants {

  public static final String MINIPROGRAM_TYPE_NAME = "healthsource";

  public enum MiniProgramType {

    WYX {
      @Override
      public String getAppId() {
        return WECHAT_MINI_PROGRAM_APPID_WYX;
      }

      @Override
      public String getSecret() {
        return WECHAT_MINI_PROGRAM_SECRET_WYX;
      }
    }, NSC {
      @Override
      public String getAppId() {
        return WECHAT_MINI_PROGRAM_APPID_NSC;
      }

      @Override
      public String getSecret() {
        return WECHAT_MINI_PROGRAM_SECRET_NSC;
      }
    }, DXGJ {
      @Override
      public String getAppId() {
        return WECHAT_MINI_PROGRAM_SECRET_DXGJ;
      }

      @Override
      public String getSecret() {
        return WECHAT_MINI_PROGRAM_APPID_DXGJ;
      }
    }, QY {
      @Override
      public String getAppId() {
        return WECHAT_MINI_PROGRAM_APPID_QY;
      }

      @Override
      public String getSecret() {
        return WECHAT_MINI_PROGRAM_SECRET_QY;
      }
    }, HDS {
      @Override
      public String getAppId() {
        return WECHAT_MINI_PROGRAM_APPID_HDS;
      }

      @Override
      public String getSecret() {
        return WECHAT_MINI_PROGRAM_SECRET_HDS;
      }
    };

    public abstract String getAppId();

    public abstract String getSecret();
  }

  /**
   * 小程序登录sessionId key值
   */
  public static final String WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY = "thirdSessionId";

  private static final String WECHAT_MINI_PROGRAM_APPID_NSC = "wx8725790de161c0b7";
  private static final String WECHAT_MINI_PROGRAM_APPID_WYX = "wx8901ed4eb5a05384";
  private static final String WECHAT_MINI_PROGRAM_APPID_DXGJ = "wx4e53980c0ce56f56";
  private static final String WECHAT_MINI_PROGRAM_APPID_QY = "wxf7a5fc06623b1295";
  private static final String WECHAT_MINI_PROGRAM_APPID_HDS = "wxa4a1fc9ae9458aa9";

  public static final String WECHAT_MINI_PROGRAM_SECRET_NSC = "477c0a62bd73aec2c896c4bd8baf97eb";
  public static final String WECHAT_MINI_PROGRAM_SECRET_WYX = "727e916c46a9537231352067db5c07d0";
  public static final String WECHAT_MINI_PROGRAM_SECRET_DXGJ = "207101e4741ad6fc286d14f14af5b373";
  public static final String WECHAT_MINI_PROGRAM_SECRET_QY = "3f3bfc330c0830c52322691e60d12afd";
  public static final String WECHAT_MINI_PROGRAM_SECRET_HDS = "1ed9cf9ac2c9635618e0a142ecd0d49f";
}
