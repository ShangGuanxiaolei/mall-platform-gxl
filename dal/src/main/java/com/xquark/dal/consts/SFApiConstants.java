package com.xquark.dal.consts;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午11:56
 */
public class SFApiConstants {

  /**
   * 拉取商品地址
   */
  public final static String PULL_PRODUCTS = "/open-api/open/product/getProductList";

  /**
   * 拉取商品详情
   */
  public final static String PULL_PD_DETAILS = "/open-api/open/product/queryProductInfoDetails";

  /**
   * 刷新token
   */
  public final static String RENEW_TOKEN = "/open-api/oauth/token";

  /**
   * 获取区域Region
   */
  public final static String PULL_REGION = "/open-api/open/product/queryRegionList";

  /**
   * 拉取库存的URl
   */
  public final static String PULL_STOCKINFO = "/open-api/open/product/stockInfo";

  /**
   * 创建订单
   */
  public final static String CREATE_ORDER = "/open-api/open/order/createOrder";

  /**
   * 获取物流信息
   */
  public final static String PULL_LOGISTICS = "/open-api/open/order/queryOrderLogInfo";

  /**
   * 获取订单信息
   */
  public final static String SELECT_ORDER = "/open-api/open/order/getOrderList";

  /**
   * 获取消息
   */
  public final static String GET_NOTICE = "/open-api/open/other/getNotice";

  /**
   * 商品库存信息
   */
  public final static String STOCK_INFO = "/open-api/open/product/stockInfo";

  /**
   * \查询商品运费
   */
  public final static String SHIPPING_FEE = "/open-api/open/shipping/getShipingFee";

  /**
   * 查询子订单信息
   */
  public final static String CHILD_ORDER = "/open-api/open/order/getChildOrderInfo";
}
