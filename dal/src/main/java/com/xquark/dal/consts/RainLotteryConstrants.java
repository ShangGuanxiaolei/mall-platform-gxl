package com.xquark.dal.consts;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author wangxinhua
 * @date 2019-04-28
 * @since 1.0
 */
public class RainLotteryConstrants {

    public static int PACKET_RAIN = 0;
    public static int LOTTERY = 1;


    public static final String FLEXIBLE_PACKET_TIME_RULE = "FLEXIBLE_PACKET_TIME_RULE";

    /**
     * 如果奖品小于10,非活动时间就没有奖品
     */
    public final static Integer MIN_LIMIT = 5;

    /**
     * 每场抽奖的总德分
     */
    public final static String REDIS_ATOMIC_NUMBER = "REDIS::ATOMIC::NUMBER";
    /**
     * 当天非白人上线
     */
    public final static String IDENTIRY_LIST_RS = "IDENTITY_LIST::RS";
    /**
     * 当天白人上线
     */
    public final static String IDENTIRY_LIST_NOT_RS = "IDENTITY_LIST::NOT_RS";
    /**
     * 抽奖德分概率
     */
    public final static Function<String, String> LOTTERY_PROBABILITY = id -> "LOTTERY_PROBABILITY::" + id;
    /**
     * 奖品列表1-一等奖 2-二等奖 3-三等奖
     * exp:1,2,3,1,2,3
     */
    public final static Function<String, String> WINNING_LIST = id -> "WINNING_LIST::" + id;

    public final static Function<String, String> WINNING_LIST_ITEM = id -> "WINNING_LIST_ITEM::" + id;
    /**
     * 活动期间 奖品列表
     */
    public final static Function<String, String> ACTIVITY_WINNING_LIST = id -> "ACTIVITY_WINNING_LIST::" + id;
    /**
     * 抽奖次数
     */
    public final static Function<String, String> LOTTERY_TIMES = id -> "LOTTERY::TIMES::" + id;
    /**
     * 普通场次
     */
    public final static String BASE_ORDINARY_SESSION_RANK = "PACKET::RAIN::ORDINARY::SESSION::";
    /**
     * 活动场次
     */
    public final static String BASE_ACTIVITY_SESSION_RANK = "PACKET::RAIN::ACTIVITY::SESSION::";

    public final static String BASE_ACTIVITY_SESSION_RANK_PART = "PACKET::RAIN::ACTIVITY::SESSION::%s::PART::";

    public final static String IS_ACTIVITY_PART = "PACKET::RAIN::ACTIVITY::PART::";

    public final static String CURR_ACTIVE_LOTTERY_KEY = "LOTTERY:ACTIVE:CURRENT";

    public final static BiFunction<String, Long, String> LOTTERY_TOKEN_KEY =
            (id, cpId) -> String.format("LOTTERY_TOKEN::%s::%d", id, cpId);


    public final static BiFunction<String, Long, String> USER_GENERATE_LIMIT_KEY_FUNC =
            (rainTimeId, cpId) -> String.format("USER_GENERATE_LIMIT::%s::%d", rainTimeId, cpId);

    public static String getActivitySessionRankKey(int prize) {
        return BASE_ACTIVITY_SESSION_RANK + prize;
    }

    public static String getActivitySessionRankPartKey(int prize) {
        return String.format(BASE_ACTIVITY_SESSION_RANK_PART, prize);
    }

    /**
     * 普通场次奖品数量key
     */
    public static String getOrdinarySessionRankKey(int prize) {
        return BASE_ORDINARY_SESSION_RANK + prize;
    }

}
