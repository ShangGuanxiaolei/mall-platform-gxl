package com.xquark.dal.consts;

/**
 * Created by wangxinhua on 18-2-1. DESC: 签到相关常量
 */
public class SignConstants {

  public static final int MAX_SIGN_DAYS = 7;

  public static final int DEFAULT_STEP = 1;

}
