package com.xquark.dal.consts;

import com.google.common.base.Function;

/**
 * Created by wangxinhua on 18-2-26. DESC:
 */
public class URLConstants {

  public static Function<String, String> CRM_PRODUCT_URL = new Function<String, String>() {
    @Override
    public String apply(String code) {
      return String.format("http://shop.qinyuan.cn/v2/openapi/crm/product/%s/detail", code);
    }
  };

}
