package com.xquark.dal.consts;

/**
 * created by
 *
 * @author wangxinhua at 18-6-7 下午1:04
 */
public class AuditConstrant {

  public static final int INSERT = 1;

  public static final int UPDATE = 2;

  public static final int DELETE = 3;

}
