package com.xquark.dal.consts;

/**
 * 对应后台配置推荐代码
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/12
 */
public class RecommendCode {

  public final static String PIECE_CNXH_VIP = "CNXH_VIP";
  public final static String PIECE_CNXH = "CNXH";

}
