package com.xquark.dal.consts;

import com.xquark.dal.mybatis.IdTypeHandler;

/**
 * created by
 *
 * @author wangxinhua at 18-7-4 下午1:45
 */
public class PromotionConstants {

  public static String FLASH_SALE_LOCKER = "flash_sale_locker";

  public static String DISTRIBUTED_LOCKER = "distributed_locker";

  /**
   * 限时抢购库存
   */
  public static String FLASH_SALE_AMOUNT_KEY = "fsak";

  public static String FLASH_SALE_TOTAL_AMOUNT_KEY = "fstak";

  /**
   * 单人限购商品数量
   */
  public static String FLASH_SALE_LIMIT_AMOUNT_KEY = "fslak";

  /**
   * 商品已售罄发送邮件
   */
  public static String PRODUCT_SOLD_OUT_SEND_MAIL_KEY = "productSoldOutFlag";

  /**
   * 用户已购买key
   */
  public static String getUserAmountKey(String userId, String promotionId, String skuId) {
    return userId + ":" + promotionId + ":" + skuId;
  }


  public static String getFlashSaleAmountKey(String promotionId, String skuId) {
    return promotionId + ":sku" + skuId + ":amount";
  }

  public static String getFlashSaleAmountKey(String promotionId, Long skuId) {
    return getFlashSaleAmountKey(promotionId, IdTypeHandler.encode(skuId));
  }


  public static String getFlashSaleTotalAmountKey(String promotionId, String skuId) {
    return promotionId + ":sku" + skuId + ":totalAmount";
  }

  public static String getFlashSaleTotalAmountKey(String promotionId, Long skuId) {
      return getFlashSaleTotalAmountKey(promotionId, IdTypeHandler.encode(skuId));
  }
  public static String getFlashSaleLimitAmountKey(String promotionId, String productId) {
    return promotionId + ":product" + productId + ":limitAmount";
  }


  public static String getCommonAmountKey(String promotionId, String skuCode) {
    return promotionId + "_" + skuCode + ":amount";
  }

  /**
   * 拼团剩余人数
   */
  public static String getRestTranMemberKey(String tranCode) {
    return tranCode + ":restMember";
  }

  public static String getPromotionPriceNum(String cpid, String detailCode) {
    final String promotionPriceNumTemplate = "%s:cpid:%s:detailCode";
    return String.format(promotionPriceNumTemplate, cpid, detailCode);
  }


  /**
   * 控制每个团最多的可免单人数
   */
  public static String getFreePayMemberKey(String tranCode) {
    return tranCode + ":freePayMember";
  }

  /**
   * 总拼团人数
   */
  public static String getTotalTranMemberKey(String tranCode) {
    return tranCode + ":totalMember";
  }

  /**
   * 订单占用详情
   */
  public static String getTranOrderDetailKey(String tranCode, String order) {
    return tranCode + ":" + order;
  }

  /**
   * 设置红包雨对应的用户key
   *
   * @param cpId 用户对应的cpId
   * @return key
   */
  public static String setIsJoinPacketRain(Long cpId) {
    return cpId + ":cpId";
  }

  /**
   * 获取红包雨对应的用户key
   *
   * @param cpId 用户对应的cpId
   * @return key
   */
  public static String getIsJoinPacketRain(Long cpId) {
    return cpId + ":cpId";
  }

  /**
   * 商品已售罄的redis标记的key
   *
   * @param productId
   * @return
   */
  public static String getProductSoldOutSendMailKey(String productId) {
    String join = String.join(":", PRODUCT_SOLD_OUT_SEND_MAIL_KEY, productId);
    return join;
  }
}
