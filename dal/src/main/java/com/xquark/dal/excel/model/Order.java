package com.xquark.dal.excel.model;

import java.util.Date;

import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.utils.excel.ExcelResources;

public class Order {

  private String orderNo;
  private UserPartnerType partnerType;
  private OrderType type;
  private PaymentMode payType;
  private OrderStatus status;
  private PayStatus paidStatus;
  private Date createdAt;

  @ExcelResources(title = "交易订单号", order = 1)
  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  @ExcelResources(title = "订单类型", order = 2)
  public UserPartnerType getPartnerType() {
    return partnerType;
  }

  public void setPartnerType(UserPartnerType partnerType) {
    this.partnerType = partnerType;
  }

  @ExcelResources(title = "交易类型", order = 3)
  public OrderType getType() {
    return type;
  }

  public void setType(OrderType type) {
    this.type = type;
  }

  @ExcelResources(title = "支付类型", order = 4)
  public PaymentMode getPayType() {
    return payType;
  }

  public void setPayType(PaymentMode payType) {
    this.payType = payType;
  }

  @ExcelResources(title = "订单状态", order = 5)
  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  @ExcelResources(title = "付款状态", order = 6)
  public PayStatus getPaidStatus() {
    return paidStatus;
  }

  public void setPaidStatus(PayStatus paidStatus) {
    this.paidStatus = paidStatus;
  }


  @ExcelResources(title = "创建时间", order = 7)
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

}
