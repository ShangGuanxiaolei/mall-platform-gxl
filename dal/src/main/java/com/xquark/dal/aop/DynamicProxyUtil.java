package com.xquark.dal.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author wangxinhua
 * @since 1.0
 */
public class DynamicProxyUtil {

    @SuppressWarnings("unchecked")
    public static <T> T create(final InvocationHandler handler, final Class<T> type) {
        return (T) Proxy.newProxyInstance(DynamicProxyUtil.class.getClassLoader(),
                new Class[]{type}, handler);
    }

}
