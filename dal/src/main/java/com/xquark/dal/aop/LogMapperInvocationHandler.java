package com.xquark.dal.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class LogMapperInvocationHandler implements InvocationHandler {

    private final Object target;

    private static Logger LOGGER = LoggerFactory.getLogger((LogMapperInvocationHandler.class));

    public LogMapperInvocationHandler(Object target) {
        this.target = target;
    }

    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        LOGGER.info("正在调用方法: {}::{}", method.getDeclaringClass().getSimpleName(), method.getName());
        return method.invoke(target, args);
    }
}