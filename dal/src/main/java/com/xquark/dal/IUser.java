package com.xquark.dal;

import com.xquark.dal.status.UserStatus;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

/**
 * Created by quguangming on 16/6/6.
 */
public interface IUser extends Archivable, UserDetails, BaseEntity {

  String getUsername();

  String getName();

  String getShopId();

  String getEmail();

  String getPhone();

  Date getCreatedAt();

  Date getUpdatedAt();

  String getPartner();

  String getAvatar();

  boolean hasRole(String role);

  UserStatus getUserStatus();

  boolean isAnonymous();

}
