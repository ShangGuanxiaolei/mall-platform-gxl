package com.xquark.thirds.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/META-INF/applicationContext-thirds.xml")
public class ThirdConfig {

}