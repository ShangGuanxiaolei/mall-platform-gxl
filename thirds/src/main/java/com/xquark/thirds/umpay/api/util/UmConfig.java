package com.xquark.thirds.umpay.api.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UmConfig {

  protected Logger log = LoggerFactory.getLogger(getClass());

  //	String pro_url_pix = "plat.url|http://pay.soopay.net";
//	
////	@Value("${payment.mer.prikey.path.umpay}")
//	String pirKeyFilePath = "plat.cert.path|~/XQuark/data/payCert/umpay/cert_2d59.crt";
////	String pirKeyFilePath = "plat.cert.path|D:\\projects\\umcert\\cert_2d59.crt";
////	@Value("${payment.plat.cert.path.umpay}")
////	String pubKeyFilePath = "9335.mer.prikey.path|~/XQuark/data/payCert/umpay/9335_KuaiKuaiKaiDian.key.p8";
////	String pubKeyFilePath = "9335.mer.prikey.path|D:\\projects\\umcert\\9335_KuaiKuaiKaiDian.key.p8";
////	String pubKeyFilePath = "9854.mer.prikey.path|D:\\projects\\umcert\\9854_xiangquwang.key.p8";
//	String pubKeyFilePath = "9854.mer.prikey.path|~/XQuark/data/payCert/umpay/9854_xiangquwang.key.p8";
//	String pName = "plat.pay.product.name|spay";
//	String encryptParams = "Encrypt.Paramters|card_id,valid_date,cvv2,pass_wd,identity_code,card_holder,recv_account,recv_user_name,identity_holder";
//	
  private static java.util.Map<String, String> configMap = null; //全局

  public static void init(Map<String, String> config) {
    if (configMap == null) {
      configMap = new java.util.HashMap<String, String>();
      configMap.put("plat.url", "http://pay.soopay.net");
      configMap.put("plat.pay.product.name", "spay");
      configMap.put("Encrypt.Paramters",
          "card_id,valid_date,cvv2,pass_wd,identity_code,card_holder,recv_account,recv_user_name,identity_holder");
      configMap.putAll(config);
      LoggerFactory.getLogger(UmConfig.class).info("U付配置信息初始化成功" + config);
    }
  }

  /**
   * 留给动态添加
   */
  public static void reLoad(Map<String, String> config) {
    configMap = null;
    init(config);
  }

  public Map<String, String> loadConfig() {
    if (configMap != null) {
      return configMap;
    }
    return null;
  }

//	
//	public Map<String, String> loadConfig(){
//		if(configMap != null)
//			return configMap;
//		else{
//			try{
//				configMap = new java.util.HashMap<String, String>();
//				String[] arrays = pro_url_pix.split("\\|");
//				configMap.put(arrays[0], arrays[1]);
//				
//				arrays = pirKeyFilePath.split("\\|");
//				configMap.put(arrays[0], arrays[1]);
//				
//				arrays = pubKeyFilePath.split("\\|");
//				configMap.put(arrays[0], arrays[1]);
//				
//				arrays = pName.split("\\|");
//				configMap.put(arrays[0], arrays[1]);
//				
//				arrays = encryptParams.split("\\|");
//				configMap.put(arrays[0], arrays[1]);
//				
//			}catch(Exception e){
//				throw new RuntimeException("umpay配置文件错误");
//			}
//		}
//		
//		return configMap;
//	}
}
