
package com.xquark.thirds.umpay.api.util;

import com.xquark.thirds.umpay.api.log.ILogger;
import com.xquark.thirds.umpay.api.log.LogManager;

/**
 * ***********************************************************************
 * <br>description : 字符串工具类
 *
 * @author umpay
 * @version 1.0 ***********************************************************************
 * @date 2014-8-1 上午09:34:23
 */
public class StringUtil {

  private static final ILogger log = LogManager.getLogger();

  /**
   * 将对象数据转换为String，并去除首尾空格
   */
  public static String trim(Object obj) {
    if (null == obj) {
      return "";
    } else {
      return obj.toString().trim();
    }
  }

  /**
   * 判断字符串是否为空
   */
  public static boolean isEmpty(String str) {
    return null == str || "".equals(str.trim());
  }

  /**
   * 判断字符串是否不为空
   */
  public static boolean isNotEmpty(String str) {
    return !isEmpty(str);
  }
}
