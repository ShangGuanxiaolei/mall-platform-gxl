package com.xquark.thirds.umpay.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.xquark.thirds.util.SpringContextUtil;

/**
 * ***********************************************************************
 * <br>description : 配置文件解析类
 *
 * @author umpay
 * @version 1.0 ***********************************************************************
 * @date 2014-8-1 上午09:31:01
 */
public class ProFileUtil {

  private final static String pro_url_pix = "plat.url";

  public static byte[] getFileByte(String pro) throws IOException {
    //TODO 从容器中获取
//		UmConfig config = (UmConfig)SpringContextUtil.getBean("umConfig");
    UmConfig config = new UmConfig();// (UmConfig)SpringContextUtil.getBean("umConfig");
    Map<String, String> configMap = config.loadConfig();

    byte[] b = null;
    InputStream in = null;
    try {
      String filepath = configMap.get(pro);

      if (null == filepath) {
        throw new RuntimeException("没有找到配置信息" + pro);
      }
      in = new FileInputStream(new File(filepath));
      if (null == in) {
        throw new RuntimeException("文件不存在" + filepath);
      }
      b = new byte[20480];
      in.read(b);
    } finally {
      if (null != in) {
        in.close();
      }
    }
    return b;
  }

  /**
   * <br>description : 获取配置文件key值
   *
   * @version 1.0
   * @date 2014-8-1上午09:31:41
   */
  public static String getPro(String pro) {
    UmConfig config = new UmConfig();// (UmConfig)SpringContextUtil.getBean("umConfig");
    Map<String, String> configMap = config.loadConfig();

    InputStream in = null;
    try {
      return StringUtil.trim(configMap.get(pro));
    } catch (Exception ex) {
      RuntimeException rex = new RuntimeException(ex.getMessage());
      rex.setStackTrace(ex.getStackTrace());
      throw rex;
    } finally {
      if (null != in) {
        try {
          in.close();
        } catch (Exception ex) {
          RuntimeException rex = new RuntimeException(ex.getMessage());
          rex.setStackTrace(ex.getStackTrace());
          throw rex;
        }
      }

    }
  }

  /**
   * 获取平台请求地址
   */
  public static String getUrlPix() {
    return getPro(pro_url_pix);
  }

//	
//	public static byte[] getFileByte(String pro)throws IOException{
//		byte[] b = null;
//		InputStream in = null;
//		try{
//			Properties prop = new Properties();
//			in = ProFileUtil.class.getClassLoader().getResourceAsStream(fileName); 
//			if(null == in)throw new RuntimeException("没有找到配置文件"+fileName);
//			prop.load(in);
//			in.close();
//			String filepath = prop.getProperty(pro);
//			if(null == filepath)throw new RuntimeException("没有找到配置信息"+pro);
//			in = new FileInputStream(new File(filepath));
//			if(null == in)throw new RuntimeException("文件不存在"+filepath);
//			b = new byte[20480];
//			in.read(b);
//		}finally{
//			if(null!=in)in.close();
//		}
//		return b;
//	}
//	
//	/**
//	 * 
//	 * <br>description : 获取配置文件key值
//	 * @param pro
//	 * @return
//	 * @version     1.0
//	 * @date        2014-8-1上午09:31:41
//	 */
//	public static String getPro(String pro){
//		InputStream in = null;
//		try{
//			Properties prop = new Properties();
//			in = ProFileUtil.class.getClassLoader().getResourceAsStream(fileName); 
//			if(null == in)throw new RuntimeException("没有找到配置文件"+fileName);
//			prop.load(in);
//			in.close();
//			return StringUtil.trim(prop.getProperty(pro));
//		}catch(Exception ex){
//			RuntimeException rex = new RuntimeException(ex.getMessage());
//			rex.setStackTrace(ex.getStackTrace());
//			throw rex;
//		}finally{
//			if(null!=in){
//				try{
//					in.close();
//				}catch(Exception ex){
//					RuntimeException rex = new RuntimeException(ex.getMessage());
//					rex.setStackTrace(ex.getStackTrace());
//					throw rex;
//				}
//			}
//			
//		}
//	}
//	
//	/**
//	 * 获取平台请求地址
//	 * @return
//	 */
//	public static String getUrlPix(){
//		return getPro(pro_url_pix);
//	}

}
