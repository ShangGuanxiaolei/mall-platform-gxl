package com.xquark.thirds.baidu.yun.core.log;

public interface YunLogHandler {

  public void onHandle(YunLogEvent event);

}
