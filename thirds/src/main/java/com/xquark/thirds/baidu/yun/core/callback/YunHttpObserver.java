package com.xquark.thirds.baidu.yun.core.callback;

import com.xquark.thirds.baidu.yun.core.event.YunHttpEvent;

public interface YunHttpObserver {

  public void onHandle(YunHttpEvent event);

}
