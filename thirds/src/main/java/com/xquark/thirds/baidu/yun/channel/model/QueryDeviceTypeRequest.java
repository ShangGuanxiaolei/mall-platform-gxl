package com.xquark.thirds.baidu.yun.channel.model;

import com.xquark.thirds.baidu.yun.core.annotation.HttpPathKeyName;
import com.xquark.thirds.baidu.yun.core.annotation.R;

public class QueryDeviceTypeRequest extends ChannelRequest {

  @HttpPathKeyName(param = R.OPTIONAL)
  private Long channelId = null;

  public Long getChannelId() {
    return channelId;
  }

  public void setChannelId(Long channelId) {
    this.channelId = channelId;
  }

}
