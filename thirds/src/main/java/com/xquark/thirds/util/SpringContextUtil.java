package com.xquark.thirds.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtil implements ApplicationContextAware {

  private static ApplicationContext context;// 声明一个静态变量保存

  @Override
  public void setApplicationContext(ApplicationContext contex) throws BeansException {
    context = contex;
  }

  public static ApplicationContext getContext() {
    return context;
  }

  /**
   * 直接获取bean
   */
  public static Object getBean(String beanName) {
    return context.getBean(beanName);
  }
}


