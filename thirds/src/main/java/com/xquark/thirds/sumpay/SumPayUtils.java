package com.xquark.thirds.sumpay;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import org.springframework.util.StringUtils;

public class SumPayUtils {

  private static final String SIGN_ALGORITHMS = "SHA1WithRSA";
  //偶尔科技的私钥
  private static String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANZQH7PM7WUqM2zF20UEcnk+rh3m09MUQ51ATO7KkRK84z5ucqb9wrbTwHW8Q22gkhkBDqk/WvR36AOkIGo+VvFggFJ2CZIO8mtlGnYw/kNRrac3TlGAUYiV3oh0mLT9sb9CK8MDRgHSt1VlHCU7r0jongW/+GZpYDd4RYmyGqDrAgMBAAECfz0cm+2pgghXAFMEGIKVwg7Q0NW1/LE0FhPBQi7VlIjDVAl5dTgpJFz+BOONy6x3HTIEGyon5sfPaVmgc53YKPIl3eor94PMVKD8qk/2d5g+LgPUbb8YCMD4v/MUXE/ILoj4sD1Dezn4LIC6aOWz5XirprNyPmk8KGVh5Ejk/ikCQQD0Y0RL9cpEemoRcWGkvtRCKbeaVD0/8uclzWV29DTiX+OeN87GpOFkMNiaVCJOcEmFYMGQezPzm0cFE55tAj+HAkEA4H8HA+rMHyzdJQo467S2NuG3eFpAAgP8mGISDhbayO6N89OnQR2nnjeuO06V0eo5BB/oqmwpxG6P3uXvhsCEfQJARB0uueUlnPRpf7cUOfCeBFrQO4ljEOHInvaiYwcpfjavoDd5wE/QKjabCFiOv0H4m94d1QEht7H9l2Kic0t/HQJBAJP2xW3shIsLq9os6aVZHzv++0kE2uk5LXRWNyY4JA2JReBuIO/HFuIuOtT2D8MiSGASk+w3jDPe106HaJ79JpUCQQDwV7IW1WJ+0giHSSlhF4DinJHsSmJ0qRBGFpxgnFAf/hI3wgGuetT2jl/yNzKH+eqv5lKIKZ5EN1jYNh+caf/J";
  //偶尔科技的公钥
//	private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWUB+zzO1lKjNsxdtFBHJ5Pq4d5tPTFEOdQEzuypESvOM+bnKm/cK208B1vENtoJIZAQ6pP1r0d+gDpCBqPlbxYIBSdgmSDvJrZRp2MP5DUa2nN05RgFGIld6IdJi0/bG/QivDA0YB0rdVZRwlO69I6J4Fv/hmaWA3eEWJshqg6wIDAQAB";
  //统统付的公钥
  private static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRwq1zs1VVr0mu8TneGkN+hbm8vlRi87lVJgO0jttkeFNVo0mjxVWy8t7DEz8VbbPhp/Nirvab+cR+go0ax+FOe2vGXWJ31KNh5hgtWlh8gBdnPE73O5OyqVxBCvoN6Sotn2eDE//OIFwxe98S3Ss538+FKOEbZ5+G90VntITY/wIDAQAB";

  public String buildRqeuest(SortedMap<String, String> paraMap, String SUM_GATEWAY_NEW) {

    String actionPath = SUM_GATEWAY_NEW;
    String strMethod = "POST";
    String strButtonName = "提交";
    String sign_type = "RSA";
    // 创建系统级参数
    buildSystemPara(paraMap);

    sign(paraMap, sign_type);
    List<String> keys = new ArrayList<String>(paraMap.keySet());

    StringBuffer sbHtml = new StringBuffer();

    sbHtml.append("<form id=\"sumpaysubmit\" name=\"sumpaysubmit\" action=\"" + actionPath
        + "\" method=\"" + strMethod + "\">");

    for (int i = 0; i < keys.size(); i++) {
      String name = (String) keys.get(i);
      String value = (String) paraMap.get(name);

      sbHtml.append("<input type=\"hidden\" name=\"" + name + "\" value=\"" + value + "\"/>");
    }

    // submit按钮控件请不要含有name属性
    sbHtml.append(
        "<input type=\"submit\" value=\"" + strButtonName + "\" style=\"display:none;\"></form>");
    sbHtml.append("<script>document.forms['sumpaysubmit'].submit();</script>");

    return sbHtml.toString();
  }

  /**
   * RSA验签名检查
   *
   * @param content 待签名数据
   * @param sign 签名值
   * @param input_charset 编码格式
   * @return 布尔值
   */
  public static boolean verify(SortedMap<String, String> paraMap, String sign, String input_charset)
      throws Exception {
    boolean bverify = false;
    String preSignStr = preSignString(paraMap);

    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    byte[] encodedKey = Base64.decode(publicKey);
    PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

    Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

    signature.initVerify(pubKey);
    signature.update(preSignStr.getBytes());

    bverify = signature.verify(Base64.decode(sign));
    return bverify;
  }

  private static void sign(SortedMap<String, String> paraMap, String sign_type) {
    String preSignStr = preSignString(paraMap);
    String sign = signContent(preSignStr, "UTF-8");
    //RSA签名
    paraMap.put("sign", sign);
    paraMap.put("sign_type", sign_type);
  }

  private static String preSignString(SortedMap<String, String> paraMap) {
    StringBuffer sb = new StringBuffer();
    Set<Entry<String, String>> es = paraMap.entrySet();
    Iterator<Entry<String, String>> it = es.iterator();
    while (it.hasNext()) {
      Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
      String k = entry.getKey();
      String v = entry.getValue();
      if (null != v && !"".equals(v) && !"sign".equals(k) && !"sign_type".equals(k)) {
        if (sb.length() > 0) {
          sb.append("&");
        }
        sb.append(k + "=" + v);
      }
    }
    return sb.toString();
  }

  private void buildSystemPara(SortedMap<String, String> paraMap) {
    paraMap.put("v", "1.0");
    paraMap.put("service", "sumpay.wap.trade.order.apply");
    paraMap.put("format", "JSON");

    paraMap.put("timestamp", "");//YYYYMMDDHHMMSS
    paraMap.put("terminalType", "wap");
    paraMap.put("terminalInfo", "");//终端详情（包括ip等信息）   JSON格式; 例：{ip:’127.0.0.1’}
    paraMap.put("signType", "RSA");
    paraMap.put("sign", "");

  }

  private static String signContent(String content, String inputCharset) {
    String sign = "";
    if (StringUtils.isEmpty(inputCharset)) {
      inputCharset = "UTF-8";
    }
//		System.out.println("待签名字符串：" + content);
    try {
      PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
      KeyFactory keyf = KeyFactory.getInstance("RSA");
      PrivateKey priKey = keyf.generatePrivate(priPKCS8);

      Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

      signature.initSign(priKey);
      signature.update(content.getBytes(inputCharset));

      byte[] signed = signature.sign();
      sign = Base64.encode(signed).toString();
//			System.out.println("生成签名字符串：" + sign);
      return sign;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sign;
  }
}
