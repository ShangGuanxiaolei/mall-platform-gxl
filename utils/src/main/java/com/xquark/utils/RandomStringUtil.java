package com.xquark.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

public class RandomStringUtil {

  private static volatile RandomStringUtil GLOBAL_INSTANCE;

  /**
   * Generate a random string.
   */
  public String nextString() {
    for (int idx = 0; idx < buf.length; ++idx) {
      buf[idx] = symbols[random.nextInt(symbols.length)];
    }
    return new String(buf);
  }

  /**
   * Generate a random string with static method
   */
  public static String create() {
    if (GLOBAL_INSTANCE == null) {
      GLOBAL_INSTANCE = new RandomStringUtil();
    }
    return GLOBAL_INSTANCE.nextString();
  }

  private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  private static final String lower = upper.toLowerCase(Locale.ROOT);

  private static final String digits = "0123456789";

  private static final String alphanum = upper + lower + digits;

  private final Random random;

  private final char[] symbols;

  private final char[] buf;

  public RandomStringUtil(int length, Random random, String symbols) {
    if (length < 1) {
      throw new IllegalArgumentException();
    }
    if (symbols.length() < 2) {
      throw new IllegalArgumentException();
    }
    assert random != null;
    this.random = random;
    this.symbols = symbols.toCharArray();
    this.buf = new char[length];
  }

  /**
   * Create an alphanumeric string generator.
   */
  public RandomStringUtil(int length, Random random) {
    this(length, random, alphanum);
  }

  /**
   * Create an alphanumeric strings from a secure generator.
   */
  public RandomStringUtil(int length) {
    this(length, new SecureRandom());
  }

  /**
   * Create session identifiers.
   */
  public RandomStringUtil() {
    this(32);
  }

  public static void main(String[] args) {
    System.out.println(RandomStringUtil.create());
  }
}