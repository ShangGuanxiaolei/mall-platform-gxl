/**
 * Ouer.com Inc. Copyright (c) 2014-2015 All Rights Reserved.
 */
package com.xquark.utils.cache;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 线程本地缓存，使用请谨慎，确保清理掉
 * 用来防止同一线程内各业务处理重复调用外部服务
 * @author
 * @version $Id: ThreadLocalCache.java, v 0.1 2015年7月21日 下午5:23:44  Exp $
 */
public class ThreadLocalCache {

  private static final Logger logger = LoggerFactory
      .getLogger(ThreadLocalCache.class);
  private static final ThreadLocal<Map<String, CacheObject>> threadLocal = new ThreadLocal<Map<String, CacheObject>>() {

    @Override
    protected Map<String, CacheObject> initialValue() {
      return new HashMap<String, CacheObject>();
    }

  };

  /**
   * 放入对象
   *
   * @param key
   * @param value
   */
  public static void putIfAbsent(String key, Object value) {
    if (!threadLocal.get().containsKey(key)) {
      threadLocal.get().put(key, new CacheObject(value));
    }
  }

  /**
   * 获取对象
   *
   * @param key
   * @return
   */
  public static Object get(String key) {
    CacheObject obj = threadLocal.get().get(key);
    if (obj != null) {
      obj.counter++;
      return obj.value;
    }
    return null;
  }

  /**
   * 清理所有数据
   */
  public static void clear() {
    if (logger.isDebugEnabled()) {
      // DEBUG 状态下，可以观察每个key的复用情况
      for (Map.Entry<String, CacheObject> entry : threadLocal.get().entrySet()) {
        logger.debug("CacheEntry access:key=" + entry.getKey() + entry.getValue().counter);
      }
    }
    threadLocal.get().clear();
  }

  private static class CacheObject {

    private Object value;
    private int counter = 0;

    CacheObject(Object v) {
      this.value = v;
    }
  }
}
