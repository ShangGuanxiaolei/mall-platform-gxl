package com.xquark.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: chenxi
 */

public final class IpUtils {

  private static final String ipRexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
  private static final Pattern ipPattern = Pattern.compile(ipRexp);

  public static boolean isIP(String host) {
    if (host.length() < 7 || host.length() > 15 || "".equals(host)) {
      return false;
    }

    final Matcher mat = ipPattern.matcher(host);
    return mat.find();
  }
}
