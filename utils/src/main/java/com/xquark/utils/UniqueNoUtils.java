package com.xquark.utils;

import java.net.Inet4Address;
import java.util.Calendar;
import java.util.UUID;

public class UniqueNoUtils {

  public enum UniqueNoType {
    ES, // 主订单号
    ESO, // 订单号
    TDN, // 交易号
    P,//支付请求号
    NPO, // 0元订单支付号
    CID, //匿名用户唯一号
    WITHDRAW, //提现申请号
    R, //退款单号
    PRO, //预约单号
    IM
  }

  private static Calendar cal = Calendar.getInstance();
  private static int seq = 0;
  private static final int ROTATION = 999;
  private static int ipMix = 0;


  // 进程安全
  public static synchronized String next(UniqueNoType type, Integer seq) {
    seq = seq % ROTATION;
    if (ipMix == 0) {
      try {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        String[] ipAddresses = ipAddress.split("\\.");
        ipMix = Integer.parseInt(ipAddresses[3]);
      } catch (Exception e) {
        ipMix = 1;
      }
    }
    cal.setTimeInMillis(System.currentTimeMillis());
    return type + String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$03d%3$03d", cal, ipMix, seq)
            .substring(2);
  }

   /*
    * @Author chp
    * @Description //使用redis生成
    * @Date
    * @Param
    * @return
    **/
  public static synchronized String next2(UniqueNoType type,Long seq) {

    if (ipMix == 0) {
      try {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        String[] ipAddresses = ipAddress.split("\\.");
        ipMix = Integer.parseInt(ipAddresses[3]);
      } catch (Exception e) {
        ipMix = 1;
      }
    }
    cal.setTimeInMillis(System.currentTimeMillis());
    return type + String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$03d%3$03d", cal, ipMix, seq)
            .substring(2);
  }




  // 进程安全
  public static synchronized String next(Integer seq) {
    seq = seq % ROTATION;
    if (ipMix == 0) {
      try {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        String[] ipAddresses = ipAddress.split("\\.");
        ipMix = Integer.parseInt(ipAddresses[3]);
      } catch (Exception e) {
        ipMix = 1;
      }
    }
    cal.setTimeInMillis(System.currentTimeMillis());
    return String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$03d%3$03d", cal, ipMix, seq);
  }

  /**
   * 订单号生成规则：时间戳（精确到秒）+最后一段的IP地址+序列号
   *
   * @return 唯一订单号
   */
  public static synchronized String next(UniqueNoType type) {
    if (seq > ROTATION) {
      seq = 0;
    }
    if (ipMix == 0) {
      try {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        String[] ipAddresses = ipAddress.split("\\.");
        ipMix = Integer.parseInt(ipAddresses[3]);
      } catch (Exception e) {
        ipMix = 1;
      }
    }
    cal.setTimeInMillis(System.currentTimeMillis());
    return type + String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$03d%3$03d", cal, ipMix, seq++)
        .substring(2);
  }
  /**
   * 订单号生成规则：生成为
   *
   * @return
   */
  public static synchronized String nextIm(UniqueNoType type) {
    String s = UUID.randomUUID().toString().replaceAll("-", "").substring(20);
    Integer i = (int) ((Math.random() * 9 + 1) * 100000);
    return type +i.toString()+s;
  }
  public static synchronized String nextErp(UniqueNoType type) {
    String s = UUID.randomUUID().toString().replaceAll("-", "").substring(20);
    Integer i = (int) ((Math.random() * 9 + 1) * 100000);
    return  type+"E"+i.toString()+s;
  }


  public static synchronized String next() {
    if (seq > ROTATION) {
      seq = 0;
    }
    if (ipMix == 0) {
      try {
        String ipAddress = Inet4Address.getLocalHost().getHostAddress();
        String[] ipAddresses = ipAddress.split("\\.");
        ipMix = Integer.parseInt(ipAddresses[3]);
      } catch (Exception e) {
        ipMix = 1;
      }
    }
    cal.setTimeInMillis(System.currentTimeMillis());
    return String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$03d%3$03d", cal, ipMix, seq++);
  }

  public static void main(String[] args) {
    String next = next(UniqueNoType.P, 101649);
    System.out.println(next);
  }
}
