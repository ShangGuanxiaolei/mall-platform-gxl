package com.xquark.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by liangfan on 16/11/2.
 */
public class ImgUtils {


  /**
   * 根据地址获得数据的字节流
   *
   * @param imgUrl 网络连接地址
   */
  public static BufferedImage getImageFromNetByUrl(String imgUrl) {
    BufferedImage img = null;
    try {
      URL url = new URL(imgUrl);
      img = ImageIO.read(url);
    } catch (IOException e) {
    }
    return img;
  }

  /**
   * 改变图片的大小到宽为size，然后高随着宽等比例变化
   *
   * @param is 上传的图片的输入流
   * @param os 改变了图片的大小后，把图片的流输出到目标OutputStream
   * @param size 新图片的宽
   * @param format 新图片的格式
   */
  public static void resizeImage(InputStream is, OutputStream os, int size, String format)
      throws IOException {
    BufferedImage prevImage = ImageIO.read(is);
    double width = prevImage.getWidth();
    double height = prevImage.getHeight();
    double percent = size / width;
    int newWidth = (int) (width * percent);
    int newHeight = (int) (height * percent);
    BufferedImage image = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);
    Graphics graphics = image.createGraphics();
    graphics.drawImage(prevImage, 0, 0, newWidth, newHeight, null);
    ImageIO.write(image, format, os);
    os.flush();
    is.close();
    os.close();
  }

  /**
   * 改变图片的大小到宽为size，然后高随着宽等比例变化
   *
   * @param prevImage 上传的图片的输入流
   * @param size 新图片的宽
   */
  public static BufferedImage resizeImage(BufferedImage prevImage, int size) {
    double width = prevImage.getWidth();
    double height = prevImage.getHeight();
    double percent = size / width;
    int newWidth = (int) (width * percent);
    int newHeight = (int) (height * percent);
    BufferedImage image = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);
    Graphics graphics = image.createGraphics();
    graphics.drawImage(prevImage, 0, 0, newWidth, newHeight, null);
    return image;
  }

  /**
   * @param prevImage 上传的图片的输入流
   * @param newWidth 新图片的宽
   * @param newHeight 新图片的高
   */
  public static BufferedImage resizeImage(BufferedImage prevImage, int newWidth, int newHeight) {
    BufferedImage image = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);
    Graphics graphics = image.createGraphics();
    graphics.drawImage(prevImage, 0, 0, newWidth, newHeight, null);
    return image;
  }

  public static BufferedImage drawTranslucentStringPic(int width, int height, Integer fontHeight,
      String drawStr) {
    try {
      BufferedImage buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D gd = buffImg.createGraphics();
      //设置透明  start
      buffImg = gd.getDeviceConfiguration()
          .createCompatibleImage(width, height, Transparency.TRANSLUCENT);
      gd = buffImg.createGraphics();
      //设置透明  end
      gd.setFont(new Font("微软雅黑", Font.PLAIN, fontHeight)); //设置字体
      gd.setColor(Color.white); //设置颜色
      gd.drawRect(0, 0, width - 1, height - 1); //画边框
      gd.drawString(drawStr, width / 2 - fontHeight * drawStr.length() / 2,
          fontHeight); //输出文字（中文横向居中）
      return buffImg;
    } catch (Exception e) {
      return null;
    }
  }

  public static BufferedImage drawRect(BufferedImage buffImg) {
    try {
      Graphics2D gd = buffImg.createGraphics();
      gd.setColor(Color.white); //设置颜色
      gd.drawRect(0, 0, buffImg.getWidth() - 2, buffImg.getHeight() - 2); //画边框
      return buffImg;
    } catch (Exception e) {
      return null;
    }
  }

  public static void createShopQrcode() {
    int width = 1080;
    int height = 960;
    String shopText = "店铺:					";
    String shopName = "	梁凡";
    String qrcodeHelp = "扫描或者微信长按识别二维码";
    String nameRow = shopText + shopName;
    String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
    String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/186X186.png";
    int bannerW = 1080;
    int bannerH = 384;
    int imgW = 184;
    int imgH = 184;
    File file = new File("/Users/liangfan/Downloads/test007.jpg");
    BufferedImage imageBanner = getImageFromNetByUrl(bannerUrl);
    BufferedImage imageImg = getImageFromNetByUrl(imgUrl);
    BufferedImage imageBannerResize = resizeImage(imageBanner, bannerW, bannerH);
    BufferedImage imageImgResize = resizeImage(imageImg, imgW, imgH);
    BufferedImage imageImgRect = drawRect(imageImgResize);
    BufferedImage ImageQrcode = null;
    HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
    // 指定纠错等级
    hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
    hints.put(EncodeHintType.MARGIN, 1);

    BitMatrix byteMatrix;
    try {
      byteMatrix = new MultiFormatWriter()
          .encode("http://www.jd.com", BarcodeFormat.QR_CODE, 240, 240, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (Exception e) {
      e.printStackTrace();
    }

    Font font = new Font("宋体", Font.PLAIN, 40);
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setBackground(Color.WHITE);
    g2.clearRect(0, 0, width, height);
    g2.setPaint(Color.black);
    g2.setFont(font);

    g2.drawString(nameRow, width / 16, height / 16);
    g2.drawImage(imageBannerResize, 0, height / 8, null);
    g2.drawImage(imageImgRect, width / 16, height / 4, null);
    g2.drawImage(ImageQrcode, width / 16, height / 4 + imageBannerResize.getHeight(), null);
    g2.drawString(qrcodeHelp, width / 3,
        height / 4 + imageBannerResize.getHeight() + ImageQrcode.getHeight() / 2);
    try {
      ImageIO.write(bi, "jpg", file);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {

    int width = 480;
    int height = 960;
    String productName = "陶锅陶锅陶锅陶锅陶锅陶锅陶锅陶锅";
    String price = "$1000.00";
    String priceTitle = "扫描二维码查看详情";
    String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/FsJaVjv_a0z0YpHpQf9ghZZCw-Es";

    int imgW = 480;
    int imgH = 640;
    File file = new File("/Users/liangfan/Downloads/testP007.jpg");
    BufferedImage imageImg = getImageFromNetByUrl(imgUrl);
    BufferedImage imageImgResize = resizeImage(imageImg, imgW, imgH);
    BufferedImage ImageQrcode = null;
    HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
    // 指定纠错等级
    hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
    hints.put(EncodeHintType.MARGIN, 1);

    BitMatrix byteMatrix;
    try {
      byteMatrix = new MultiFormatWriter()
          .encode("http://www.jd.com", BarcodeFormat.QR_CODE, 180, 180, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (Exception e) {
      e.printStackTrace();
    }

    Font font = new Font("宋体", Font.PLAIN, 20);
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setBackground(Color.white);
    g2.clearRect(0, 0, width, height);
    g2.setPaint(Color.black);
    g2.setFont(font);

    g2.drawString(productName, width / 16, height / 16);
    g2.drawImage(imageImgResize, 0, height / 8, null);
    g2.drawImage(ImageQrcode, width / 16, height / 8 + imageImgResize.getHeight(), null);
    g2.drawString(price, width / 2,
        height / 8 + imageImgResize.getHeight() + ImageQrcode.getHeight() / 2);
    g2.drawString(priceTitle, width / 2,
        height / 8 + imageImgResize.getHeight() + ImageQrcode.getHeight() / 2 + 40);
    try {
      ImageIO.write(bi, "jpg", file);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
