package com.xquark.utils.functional;

import com.google.common.base.Function;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 获取对象属性的function
 * @author wangxinhua.
 * @date 2018/9/28
 */
public class BeanToPropertyFunction<F, T> implements Function<F, T> {

  private final static Logger LOGGER = LoggerFactory.getLogger(BeanToPropertyFunction.class);

  private final String fieldName;

  public BeanToPropertyFunction(String fieldName) {
    this.fieldName = fieldName;
  }

  @Override
  public T apply(F f) {
    try {
      @SuppressWarnings("unchecked")
      T ret = (T) FieldUtils.readField(f, fieldName, true);
      return ret;
    } catch (IllegalAccessException e) {
      LOGGER.error("field {} not found or type not match in target {}", fieldName, f);
    }
    LOGGER.warn("field {} not find in target {}", fieldName, f);
    return null;
  }

}
