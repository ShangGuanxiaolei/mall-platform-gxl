package com.xquark.utils.functional;

import com.google.common.base.Function;
import java.math.BigDecimal;

/**
 * @author wangxinhua on 2018/7/28. DESC:
 */
public class StringToDecimal implements Function<String,BigDecimal> {

  @Override
  public BigDecimal apply(String s) {
    return new BigDecimal(s);
  }

}
