package com.xquark.utils.functional;

import com.google.common.base.Function;

/**
 * @author wangxinhua on 2018/7/28. DESC:
 */
public class StringToInteger implements Function<String, Integer> {

  @Override
  public Integer apply(String s) {
    return Integer.valueOf(s);
  }

}
