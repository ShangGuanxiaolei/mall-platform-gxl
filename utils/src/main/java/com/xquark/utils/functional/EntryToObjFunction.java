package com.xquark.utils.functional;

import com.google.common.base.Function;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 */
public class EntryToObjFunction<K, V, T extends MapLike<K, V>> implements Function<Entry<K, V>, T> {

  private final static Logger LOGGER = LoggerFactory.getLogger(EntryToObjFunction.class);

  private final Class<T> clazz;

  public EntryToObjFunction(Class<T> clazz) {
    this.clazz = clazz;
  }

  @Override
  public T apply(Entry<K, V> kvEntry) {
    T ret;
    try {
      ret = clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      String msg = String.format("类型 %s 无法初始化", clazz.getSimpleName());
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
    ret.setKey(kvEntry.getKey());
    ret.setVal(kvEntry.getValue());
    return ret;
  }

}
