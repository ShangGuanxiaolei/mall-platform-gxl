package com.xquark.utils.functional;

/**
 * created by
 *
 * @author wangxinhua at 18-6-30 下午1:17
 */
public interface Consumer<T> {

  void accept(T t);

}
