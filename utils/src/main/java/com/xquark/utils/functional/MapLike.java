package com.xquark.utils.functional;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 */
public interface MapLike<K, V> {

  void setKey(K k);

  void setVal(V v);

}
