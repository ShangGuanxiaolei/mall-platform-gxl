package com.xquark.utils.functional;

import com.google.common.base.Predicate;
import java.math.BigDecimal;

/**
 * @author wangxinhua.
 * @date 2018/11/14
 * 判断一个bigDecimal 是否大于0
 */
public class DecimalOverZeroPredicate implements Predicate<BigDecimal> {

  @Override
  public boolean apply(BigDecimal bigDecimal) {
    if (bigDecimal == null) {
      return false;
    }
    return bigDecimal.signum() > 0;
  }
}
