package com.xquark.utils.functional;

/**
 * Created by wangxinhua on 17-11-19. DESC: 函数接口
 * using guava's {@link com.google.common.base.Function} instead
 */
@Deprecated
public interface Function<T, V> {

  V apply(T t);

}
