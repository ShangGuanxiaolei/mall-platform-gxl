package com.xquark.utils.resourcebundle;

/**
 * @auther chenxi
 */

import java.io.InputStream;

public class PropertiesResouceBundleFactory extends AbstractResourceBundleFactory {

  public PropertiesResouceBundleFactory() {
    super();
  }

  public PropertiesResouceBundleFactory(ClassLoader classLoader) {
    super(classLoader);
  }

  public PropertiesResouceBundleFactory(ResourceBundleLoader loader) {
    super(loader);
  }

  @Override
  protected BossResourceBundle parse(InputStream stream, String systemId)
      throws ResourceBundleCreateException {
    BossResourceBundle resourceBundle = new PropertiesResourceBundle(stream, systemId);
    if (ResourceBundleFactory.isAutoReload()) {
      String bundleName = systemId.substring(0, systemId.length()
          - ResourceBundleConstant.RB_RESOURCE_EXT_PROPERTIES.length());
      resourceBundle = new AutoReloadResourceBundleProxy(
          resourceBundle, this, bundleName, ResourceBundleFactory
          .getCheckModifyInterval());
    }
    return resourceBundle;
  }

  @Override
  protected String getFilename(String bundleName) {
    return super.getFilename(bundleName)
        + ResourceBundleConstant.RB_RESOURCE_EXT_PROPERTIES;
  }

}
