package com.xquark.utils.resourcebundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import com.xquark.utils.MessageUtil;

/**
 * @author chenxi
 */

public class MessageBuilder {

  protected final List<Object> params = new ArrayList<Object>(5);
  protected final ResourceBundle bundle;
  protected final String key;

  public MessageBuilder(String bundleName, String key) {
    this(ResourceBundleFactory.getBundle(bundleName), key);
  }

  public MessageBuilder(ResourceBundle bundle, String key) {
    this.bundle = bundle;
    this.key = key;
  }

  public MessageBuilder append(Object param) {
    params.add(param);
    return this;
  }

  public MessageBuilder append(boolean param) {
    params.add(Boolean.valueOf(param));
    return this;
  }

  public MessageBuilder append(char param) {
    params.add(Character.valueOf(param));
    return this;
  }

  public MessageBuilder append(double param) {
    params.add(Double.valueOf(param));
    return this;
  }

  public MessageBuilder append(float param) {
    params.add(Float.valueOf(param));
    return this;
  }

  public MessageBuilder append(int param) {
    params.add(Integer.valueOf(param));
    return this;
  }

  public MessageBuilder append(long param) {
    params.add(Long.valueOf(param));
    return this;
  }

  public MessageBuilder append(Object[] params) {
    if (params != null) {
      this.params.addAll(Arrays.asList(params));
    }

    return this;
  }

  @Override
  public String toString() {
    return getMessage();
  }

  protected String getMessage() {
    return MessageUtil.getMessage(bundle, key, params.toArray());
  }
}
