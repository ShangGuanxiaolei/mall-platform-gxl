package com.xquark.utils.resourcebundle;

import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @auther chenxi
 */

public class AutoReloadResourceBundleProxy extends BossResourceBundle {

  protected static final Logger logger = LoggerFactory
      .getLogger(AutoReloadResourceBundleProxy.class);

  private volatile BossResourceBundle resourceBundle;
  private final ResourceBundleFactory resourceBundleFactory;
  private final String lookupBundleName;

  private volatile long lastModified;

  private volatile long lastCheckModifyTimestamp;

  private long checkModifyInterval = 5 * 1000;

  public AutoReloadResourceBundleProxy(BossResourceBundle resourceBundle,
      ResourceBundleFactory resourceBundleFactory,
      String lookupBundleName, long checkModifiInterval) {
    this.resourceBundle = resourceBundle;
    setBaseName(resourceBundle.getBaseName());
    this.setLocale(resourceBundle.getLocale());

    this.resourceBundleFactory = resourceBundleFactory;
    this.lookupBundleName = lookupBundleName;
    lastModified = resourceBundleFactory.lastModified(lookupBundleName);
    lastCheckModifyTimestamp = System.currentTimeMillis();
    checkModifyInterval = checkModifiInterval;
  }

  protected boolean isModified() {
    final long tmp = System.currentTimeMillis() - lastCheckModifyTimestamp;
    if (tmp < checkModifyInterval && tmp >= 0) {
      return false;
    }
    lastCheckModifyTimestamp = System.currentTimeMillis();
    if (logger.isDebugEnabled()) {
      logger.debug("check resource last modify time:" + lookupBundleName);
    }
    final long nowLastModified = resourceBundleFactory
        .lastModified(lookupBundleName);
    if (lastModified != nowLastModified) {
      lastModified = nowLastModified;
      return true;
    }
    return false;
  }

  @Override
  public Enumeration<String> getKeys() {
    if (isModified()) {
      try {
        resourceBundle = resourceBundleFactory
            .createBundle(lookupBundleName);
      } catch (final ResourceBundleCreateException e) {
        logger.warn(
            "error resourceBundleFactory.createBundle(lookupBundleName), lookupBundleName:"
                + lookupBundleName, e);
      }
    }

    return resourceBundle.getKeys();
  }

  @Override
  protected Object handleGetObject(String key) {
    if (isModified()) {
      try {
        resourceBundle = resourceBundleFactory
            .createBundle(lookupBundleName);
      } catch (final ResourceBundleCreateException e) {
        logger.warn(
            "error resourceBundleFactory.createBundle(lookupBundleName), lookupBundleName:"
                + lookupBundleName, e);
      }
    }
    return resourceBundle.getObject(key);
  }
}
