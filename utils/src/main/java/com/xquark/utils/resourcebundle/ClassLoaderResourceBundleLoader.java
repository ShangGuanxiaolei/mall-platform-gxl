package com.xquark.utils.resourcebundle;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;

/**
 * @auther chenxi
 */

public class ClassLoaderResourceBundleLoader implements ResourceBundleLoader {

  private ClassLoader classLoader;

  public ClassLoaderResourceBundleLoader() {
    this(null);
  }

  public ClassLoaderResourceBundleLoader(ClassLoader classLoader) {
    if (classLoader == null) {
      this.classLoader = Thread.currentThread().getContextClassLoader();

      if (this.classLoader == null) {
        this.classLoader = ClassLoader.getSystemClassLoader();
      }
    } else {
      this.classLoader = classLoader;
    }
  }

  @Override
  public InputStream openStream(final String bundleFilename)
      throws ResourceBundleCreateException {
    try {
      return (InputStream) AccessController
          .doPrivileged(new PrivilegedExceptionAction<Object>() {
            @Override
            public Object run()
                throws ResourceBundleCreateException {
              URL url = classLoader.getResource(bundleFilename);

              if (url == null) {
                return null;
              }

              try {
                return url.openStream();
              } catch (IOException e) {
                throw new ResourceBundleCreateException(
                    ResourceBundleConstant.RB_FAILED_OPENING_STREAM,
                    new Object[]{bundleFilename}, e);
              }
            }
          });
    } catch (PrivilegedActionException e) {
      throw (ResourceBundleCreateException) e.getException();
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (!ClassLoaderResourceBundleLoader.class.isInstance(obj)) {
      return false;
    }

    return ((ClassLoaderResourceBundleLoader) obj).classLoader == classLoader;
  }

  @Override
  public int hashCode() {
    return (classLoader == null) ? 0 : classLoader.hashCode();
  }
}
