package com.xquark.utils.resourcebundle;

import java.text.MessageFormat;

import com.xquark.utils.exception.ChainedException;

/**
 * @auther chenxi
 */

public class ResourceBundleCreateException extends ChainedException {

  /**
   *
   */
  private static final long serialVersionUID = 3258132457613177654L;

  public ResourceBundleCreateException(String messageId, Throwable cause) {
    super(messageId, cause);
  }

  public ResourceBundleCreateException(String messageId, Object[] params,
      Throwable cause) {
    super(MessageFormat.format(messageId, (params == null) ? new Object[0]
        : params), cause);
  }
}
