package com.xquark.utils.resourcebundle;

import java.lang.ref.SoftReference;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xquark.utils.collection.SoftHashMap;
import com.xquark.utils.i18n.LocaleUtil;

/**
 * @auther chenxi
 */

public abstract class ResourceBundleFactory implements ResourceBundleConstant {

  protected static final Logger logger = LoggerFactory
      .getLogger(ResourceBundleFactory.class);

  private static boolean autoReload = false;

  private static long checkModifyInterval = 10 * 1000;

  public static final BossResourceBundle getBundle(String baseName) {
    return getBundle(baseName, null, (ResourceBundleFactory) null);
  }

  public static final BossResourceBundle getBundle(String baseName,
      Locale locale) {
    return getBundle(baseName, locale, (ResourceBundleFactory) null);
  }

  public static final BossResourceBundle getBundle(String baseName,
      Locale locale, ClassLoader classLoader) {
//		return getBundle(baseName, locale, new XMLResourceBundleFactory(
//				classLoader));
    return getBundle(baseName, locale, new PropertiesResouceBundleFactory(
        classLoader));
  }

  public static final BossResourceBundle getBundle(String baseName,
      Locale locale, ResourceBundleLoader loader) {
//		return getBundle(baseName, locale, new XMLResourceBundleFactory(loader));
    return getBundle(baseName, locale, new PropertiesResouceBundleFactory(loader));
  }

  public static final BossResourceBundle getBundle(String baseName,
      Locale locale, ResourceBundleFactory factory) {
    if (locale == null) {
      locale = LocaleUtil.getContext().getLocale();
    }

    if (factory == null) {
//			factory = new XMLResourceBundleFactory();
      factory = new PropertiesResouceBundleFactory();
    }
    return Helper.getBundleImpl(baseName, locale, factory);
  }

  public static final void clearCache() {
    Helper.cache.clear();
  }

  public static void setCheckModifyInterval(long checkModifyInterval) {
    ResourceBundleFactory.checkModifyInterval = checkModifyInterval;
  }

  public static long getCheckModifyInterval() {
    return ResourceBundleFactory.checkModifyInterval;
  }

  public static final boolean isAutoReload() {
    return ResourceBundleFactory.autoReload;
  }

  public static final void setAutoReload(boolean autoReload) {
    ResourceBundleFactory.autoReload = autoReload;
  }

  public abstract BossResourceBundle createBundle(String bundleName)
      throws ResourceBundleCreateException;

  public long lastModified(String bundleName) {
    return 0;
  }

  @Override
  public abstract boolean equals(Object obj);

  @Override
  public abstract int hashCode();

  private static final class Helper {

    private static final Cache cache = new Cache();

    private static BossResourceBundle getBundleImpl(String baseName,
        Locale locale, ResourceBundleFactory factory) {
      if (baseName == null) {
        throw new NullPointerException(
            ResourceBundleConstant.RB_BASE_NAME_IS_NULL);
      }

      final Object NOT_FOUND = factory;
      String bundleName = baseName;
      String localeSuffix = locale.toString();

      if (localeSuffix.length() > 0) {
        bundleName += ("_" + localeSuffix);
      } else if (locale.getVariant().length() > 0) {
        bundleName += ("___" + locale.getVariant());
      }

      Locale defaultLocale = LocaleUtil.getContext().getLocale();

      Object lookup = cache.get(factory, bundleName, defaultLocale);

      if (NOT_FOUND.equals(lookup)) {
        throwResourceBundleException(true, baseName, locale, null);
      } else if (lookup != null) {
        return (BossResourceBundle) lookup;
      }

      Object parent = NOT_FOUND;

      try {
        Object root = findBundle(factory, baseName, defaultLocale,
            baseName, null, NOT_FOUND);

        if (root == null) {
          root = NOT_FOUND;
          cache.put(factory, baseName, defaultLocale, root);
        }

        final List<String> names = calculateBundleNames(baseName,
            locale);
        List<Object> bundlesFound = new ArrayList<Object>(MAX_BUNDLES_SEARCHED);

        boolean foundInMainBranch = (!NOT_FOUND.equals(root) && (names
            .size() == 0));

        if (!foundInMainBranch) {
          parent = root;

          for (int i = 0; i < names.size(); i++) {
            bundleName = names.get(i);
            lookup = findBundle(factory, bundleName, defaultLocale,
                baseName, parent, NOT_FOUND);
            bundlesFound.add(lookup);

            if (lookup != null) {
              parent = lookup;
              foundInMainBranch = true;
            }
          }
        }

        parent = root;

        if (!foundInMainBranch) {
          final List<?> fallbackNames = calculateBundleNames(
              baseName, defaultLocale);

          for (int i = 0; i < fallbackNames.size(); i++) {
            bundleName = (String) fallbackNames.get(i);

            if (names.contains(bundleName)) {
              break;
            }

            lookup = findBundle(factory, bundleName, defaultLocale,
                baseName, parent, NOT_FOUND);

            if (lookup != null) {
              parent = lookup;
            } else {
              cache.put(factory, bundleName, defaultLocale,
                  parent);
            }
          }
        }

        for (int i = 0; i < names.size(); i++) {
          final String name = names.get(i);
          final Object bundleFound = bundlesFound.get(i);

          if (bundleFound == null) {
            cache.put(factory, name, defaultLocale, parent);
          } else {
            parent = bundleFound;
          }
        }
      } catch (Exception e) {
        cache.cleanUpConstructionList();
        throwResourceBundleException(false, baseName, locale, e);
      } catch (Error e) {
        cache.cleanUpConstructionList();
        throw e;
      }

      if (NOT_FOUND.equals(parent)) {
        throwResourceBundleException(true, baseName, locale, null);
      }

      return (BossResourceBundle) parent;
    }

    private static Object findBundle(ResourceBundleFactory factory,
        String bundleName, Locale defaultLocale, String baseName,
        Object parent, final Object NOT_FOUND)
        throws ResourceBundleCreateException {
      Object result = cache.getWait(factory, bundleName, defaultLocale);

      if (result != null) {
        return result;
      }

      result = factory.createBundle(bundleName);

      if (result != null) {
        Object otherBundle = cache.get(factory, bundleName,
            defaultLocale);

        if (otherBundle != null) {
          result = otherBundle;
        } else {
          final BossResourceBundle bundle = (BossResourceBundle) result;

          if ((!NOT_FOUND.equals(parent))
              && (bundle.getParent() == null)) {
            bundle.setParent((BossResourceBundle) parent);
          }

          bundle.setBaseName(baseName);
          bundle.setLocale(baseName, bundleName);
          cache.put(factory, bundleName, defaultLocale, result);
        }
      }

      return result;
    }

    private static List<String> calculateBundleNames(String baseName,
        Locale locale) {
      final List<String> result = new ArrayList<String>(MAX_BUNDLES_SEARCHED);
      final String language = locale.getLanguage();
      final int languageLength = language.length();
      final String country = locale.getCountry();
      final int countryLength = country.length();
      final String variant = locale.getVariant();
      final int variantLength = variant.length();

      if ((languageLength + countryLength + variantLength) == 0) {
        return result;
      }

      final StringBuilder builder = new StringBuilder(baseName);

      builder.append('_');
      builder.append(language);

      if (languageLength > 0) {
        result.add(builder.toString());
      }

      if ((countryLength + variantLength) == 0) {
        return result;
      }

      builder.append('_');
      builder.append(country);

      if (countryLength > 0) {
        result.add(builder.toString());
      }

      if (variantLength == 0) {
        return result;
      }

      builder.append('_');
      builder.append(variant);
      result.add(builder.toString());

      return result;
    }

    private static void throwResourceBundleException(boolean missing,
        String baseName, Locale locale, Throwable cause) {
      String bundleName = baseName + "_" + locale;

      if (missing) {
        throw new ResourceBundleException(
            ResourceBundleConstant.RB_MISSING_RESOURCE_BUNDLE,
            new Object[]{baseName, locale}, cause, bundleName,
            "");
      } else {
        throw new ResourceBundleException(
            ResourceBundleConstant.RB_FAILED_LOADING_RESOURCE_BUNDLE,
            new Object[]{baseName, locale}, cause, bundleName,
            "");
      }
    }
  }

  private static final class Cache extends SoftHashMap<CacheKey, Object> {

    private static final CacheKey cacheKey = new CacheKey();

    private final Map<CacheKey, Thread> underConstruction = new HashMap<CacheKey, Thread>(
        ResourceBundleConstant.MAX_BUNDLES_SEARCHED,
        ResourceBundleConstant.CACHE_LOAD_FACTOR);

    public Cache() {
      super();
    }

    public synchronized Object get(ResourceBundleFactory factory,
        String bundleName, Locale defaultLocale) {
      cacheKey.set(factory, bundleName, defaultLocale);

      Object result = get(cacheKey);

      cacheKey.clear();
      return result;
    }

    @SuppressWarnings("unused")
    public synchronized Object remove(ResourceBundleFactory factory,
        String bundleName, Locale defaultLocale) {
      cacheKey.set(factory, bundleName, defaultLocale);

      Object result = remove(cacheKey);

      cacheKey.clear();
      return result;
    }

    @SuppressWarnings("unused")
    public synchronized List<Entry<?, ?>> removeAllValue(Object value) {
      List<Entry<?, ?>> result = new LinkedList<Entry<?, ?>>();
      if (value == null) {
        for (Iterator<?> iterator = entrySet().iterator(); iterator
            .hasNext(); ) {
          Map.Entry<?, ?> entry = (Map.Entry<?, ?>) iterator.next();
          if (null == entry.getValue()) {
            result.add(entry);
            iterator.remove();
          }
        }
      } else {
        for (Iterator<?> iterator = entrySet().iterator(); iterator
            .hasNext(); ) {
          Map.Entry<?, ?> entry = (Map.Entry<?, ?>) iterator.next();
          if (value.equals(entry.getValue())) {
            result.add(entry);
            iterator.remove();
          }
        }
      }
      return result;
    }

    public synchronized Object getWait(ResourceBundleFactory factory,
        String bundleName, Locale defaultLocale) {
      Object result;

      cacheKey.set(factory, bundleName, defaultLocale);
      result = get(cacheKey);

      if (result != null) {
        cacheKey.clear();
        return result;
      }

      Thread builder = underConstruction.get(cacheKey);
      boolean beingBuilt = ((builder != null) && (builder != Thread
          .currentThread()));

      if (beingBuilt) {
        while (beingBuilt) {
          cacheKey.clear();

          try {
            wait();
          } catch (InterruptedException e) {
          }

          cacheKey.set(factory, bundleName, defaultLocale);
          beingBuilt = underConstruction.containsKey(cacheKey);
        }

        result = get(cacheKey);

        if (result != null) {
          cacheKey.clear();
          return result;
        }
      }

      underConstruction.put((CacheKey) cacheKey.clone(), Thread
          .currentThread());

      cacheKey.clear();

      return null;
    }

    public synchronized void put(ResourceBundleFactory factory,
        String bundleName, Locale defaultLocale, Object bundle) {
      cacheKey.set(factory, bundleName, defaultLocale);

      put((CacheKey) cacheKey.clone(), bundle);

      underConstruction.remove(cacheKey);

      cacheKey.clear();

      notifyAll();
    }

    public synchronized void cleanUpConstructionList() {
      final Collection<Thread> entries = underConstruction.values();
      final Thread thisThread = Thread.currentThread();

      while (entries.remove(thisThread)) {
      }

      notifyAll();
    }
  }

  private static final class CacheKey implements Cloneable {

    private SoftReference<ResourceBundleFactory> factoryRef;
    private String bundleName;
    private Locale defaultLocale;
    private int hashCode;

    public void set(ResourceBundleFactory factory, String bundleName,
        Locale defaultLocale) {
      this.bundleName = bundleName;
      this.hashCode = bundleName.hashCode();
      this.defaultLocale = defaultLocale;

      if (defaultLocale != null) {
        hashCode ^= defaultLocale.hashCode();
      }

      if (factory == null) {
        this.factoryRef = null;
      } else {
        factoryRef = new SoftReference<ResourceBundleFactory>(factory);
        hashCode ^= factory.hashCode();
      }
    }

    public void clear() {
      set(null, "", null);
    }

    @Override
    public boolean equals(Object other) {
      if (this == other) {
        return true;
      }
      try {
        final CacheKey otherKey = (CacheKey) other;
        if (hashCode != otherKey.hashCode) {
          return false;
        }
        if (!eq(bundleName, otherKey.bundleName)) {
          return false;
        }
        if (!eq(defaultLocale, otherKey.defaultLocale)) {
          return false;
        }
        if (factoryRef == null) {
          return otherKey.factoryRef == null;
        } else {
          return (otherKey.factoryRef != null)
              && eq(factoryRef.get(), otherKey.factoryRef.get());
        }
      } catch (NullPointerException e) {
        return false;
      } catch (ClassCastException e) {
        return false;
      }
    }

    private boolean eq(Object o1, Object o2) {
      return (o1 == null) ? (o2 == null) : o1.equals(o2);
    }

    @Override
    public int hashCode() {
      return hashCode;
    }

    @Override
    public Object clone() {
      try {
        return super.clone();
      } catch (CloneNotSupportedException e) {
        throw new InternalError(MessageFormat.format(
            ResourceBundleConstant.RB_CLONE_NOT_SUPPORTED,
            new Object[]{CacheKey.class.getName()}));
      }
    }

    @Override
    public String toString() {
      return new StringBuffer("CacheKey[factory=").append(
          (factoryRef == null) ? "null" : factoryRef.get()).append(
          ", bundleName=").append(bundleName).append(
          ", defaultLocale=").append(defaultLocale).append("]")
          .toString();
    }
  }

}
