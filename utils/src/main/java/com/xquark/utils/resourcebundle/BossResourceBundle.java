package com.xquark.utils.resourcebundle;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.xquark.utils.MessageUtil;

/**
 * @author chenxi
 */

public abstract class BossResourceBundle extends ResourceBundle {

  private String baseName;

  private Locale locale;

  public String getBaseName() {
    return baseName;
  }

  @Override
  public Locale getLocale() {
    return locale;
  }

  public MessageBuilder getMessageBuilder(String key) {
    return new MessageBuilder(this, key);
  }

  public final String getMessage(String key, Object[] params) {
    return MessageUtil.getMessage(this, key, params);
  }

  public final Map<?, ?> getMap(String key) {
    return (Map<?, ?>) getObject(key);
  }

  public final List<?> getList(String key) {
    return (List<?>) getObject(key);
  }

  protected final void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  protected final void setLocale(Locale locale) {
    this.locale = locale;
  }

  protected final void setLocale(String baseName, String bundleName) {
    if (baseName.length() == bundleName.length()) {
      locale = new Locale("", "");
    } else if (baseName.length() < bundleName.length()) {
      int pos = baseName.length();
      String temp = bundleName.substring(pos + 1);

      pos = temp.indexOf('_');

      if (pos == -1) {
        locale = new Locale(temp, "", "");
        return;
      }

      String language = temp.substring(0, pos);

      temp = temp.substring(pos + 1);
      pos = temp.indexOf('_');

      if (pos == -1) {
        locale = new Locale(language, temp, "");
        return;
      }

      String country = temp.substring(0, pos);

      temp = temp.substring(pos + 1);

      locale = new Locale(language, country, temp);
    } else {
      throw new IllegalArgumentException(
          MessageFormat
              .format(
                  ResourceBundleConstant.RB_BASE_NAME_LONGER_THAN_BUNDLE_NAME,
                  new Object[]{baseName, bundleName}));
    }
  }

  protected final void setParent(BossResourceBundle parent) {
    this.parent = parent;
  }

  protected final java.util.ResourceBundle getParent() {
    return parent;
  }
}
