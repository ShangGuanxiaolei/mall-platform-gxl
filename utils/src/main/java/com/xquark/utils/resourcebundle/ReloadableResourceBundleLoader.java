package com.xquark.utils.resourcebundle;

/**
 * @auther chenxi
 */

public interface ReloadableResourceBundleLoader extends ResourceBundleLoader {

  long lastModified(String bundleFileName);
}
