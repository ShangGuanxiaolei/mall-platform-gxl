package com.xquark.utils.resourcebundle;

import java.io.InputStream;

/**
 * @auther chenxi
 */

public interface ResourceBundleLoader {

  InputStream openStream(String bundleFilename)
      throws ResourceBundleCreateException;

  @Override
  boolean equals(Object obj);

  @Override
  int hashCode();
}
