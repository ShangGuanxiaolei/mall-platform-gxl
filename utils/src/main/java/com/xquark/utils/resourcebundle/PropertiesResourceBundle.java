package com.xquark.utils.resourcebundle;

/**
 * @auther chenxi
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import sun.util.ResourceBundleEnumeration;

public class PropertiesResourceBundle extends BossResourceBundle {

  protected static final String CODE_SUFFIX = StringUtils.substring(
      ResourceBundleConstant.XPATH_RESOURCE_MESSAGE_CODE,
      ResourceBundleConstant.XPATH_RESOURCE_MESSAGE_SPACE.length());

  protected Map<String, Object> values = new HashMap<String, Object>();

  public PropertiesResourceBundle(InputStream stream, String systemId)
      throws ResourceBundleCreateException {
    init(stream, systemId);
  }

  private void init(InputStream stream, String systemId) throws ResourceBundleCreateException {
    Properties props = new Properties();
    try {
      props.load(stream);
      Iterator<Object> it = props.keySet().iterator();
      String key;
      String value;
      while (it.hasNext()) {
        key = (String) it.next();
        value = props.getProperty(key);
        if (values.containsKey(key)) {
          throw new ResourceBundleCreateException(
              ResourceBundleConstant.RB_DUPLICATED_RESOURCE_KEY,
              new Object[]{key}, null);
        }
        values.put(key, value);
      }
    } catch (IOException e) {
      throw new ResourceBundleCreateException("Failed to load properties file from " + systemId, e);
    } finally {
      if (stream != null) {
        try {
          stream.close();
        } catch (IOException e) {
          throw new ResourceBundleCreateException("Failed to load properties file from " + systemId,
              e);
        }
      }
    }
  }

  @Override
  protected Object handleGetObject(String key) {
    return values.get(key);
  }

  @Override
  public Enumeration<String> getKeys() {
    java.util.ResourceBundle parent = getParent();

    return new ResourceBundleEnumeration(values.keySet(),
        (parent != null) ? parent.getKeys() : null);
  }

}
