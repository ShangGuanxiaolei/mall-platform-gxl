package com.xquark.utils.resourcebundle;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author chenxi
 */

public abstract class AbstractResourceBundleFactory extends
    ResourceBundleFactory {

  private final ResourceBundleLoader loader;

  public AbstractResourceBundleFactory() {
    this(new ClassLoaderResourceBundleLoader());
  }

  public AbstractResourceBundleFactory(ClassLoader classLoader) {
    this(new ClassLoaderResourceBundleLoader(classLoader));
  }

  public AbstractResourceBundleFactory(ResourceBundleLoader loader) {
    this.loader = loader;
  }

  public ResourceBundleLoader getLoader() {
    return loader;
  }

  @Override
  public BossResourceBundle createBundle(String bundleName)
      throws ResourceBundleCreateException {
    InputStream stream = null;
    String filename = getFilename(bundleName);

    if (loader != null) {
      stream = loader.openStream(filename);
    }

    if (stream == null) {
      return null;
    }

    try {
      return parse(new BufferedInputStream(stream), filename);
    } finally {
      try {
        stream.close();
      } catch (IOException e) {
      }
    }
  }

  protected String getFilename(String bundleName) {
    return bundleName.replace('.', '/');
  }

  protected abstract BossResourceBundle parse(InputStream stream,
      String systemId) throws ResourceBundleCreateException;

  @Override
  public long lastModified(String bundleName) {
    if (ReloadableResourceBundleLoader.class.isInstance(loader)) {
      String filename = getFilename(bundleName);
      return ((ReloadableResourceBundleLoader) loader)
          .lastModified(filename);
    }
    return super.lastModified(bundleName);
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    try {
      AbstractResourceBundleFactory otherFactory = (AbstractResourceBundleFactory) other;
      return (loader == null) ? (otherFactory.loader == null) : loader
          .equals(otherFactory.loader);
    } catch (NullPointerException npe) {
      return false;
    } catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return (loader == null) ? 0 : loader.hashCode();
  }
}
