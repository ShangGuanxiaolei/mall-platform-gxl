package com.xquark.utils.resourcebundle;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.MissingResourceException;

import com.xquark.utils.exception.ChainedThrowable;
import com.xquark.utils.exception.ChainedThrowableDelegate;

/**
 * @auther chenxi
 */

public class ResourceBundleException extends MissingResourceException implements
    ChainedThrowable {

  /**
   *
   */
  private static final long serialVersionUID = 3258408434930825010L;

  private final ChainedThrowable delegate = new ChainedThrowableDelegate(this);
  private final Throwable cause;

  public ResourceBundleException(String messageId, Object[] params,
      Throwable cause, String bundleName, Object key) {
    super(MessageFormat.format(messageId, (params == null) ? new Object[0]
        : params), bundleName, String.valueOf(key));
    this.cause = cause;
  }

  public String getBundleName() {
    return super.getClassName();
  }

  @Override
  public Throwable getCause() {
    return cause;
  }

  @Override
  public void printStackTrace() {
    delegate.printStackTrace();
  }

  @Override
  public void printStackTrace(PrintStream stream) {
    delegate.printStackTrace(stream);
  }

  @Override
  public void printStackTrace(PrintWriter writer) {
    delegate.printStackTrace(writer);
  }

  @Override
  public void printCurrentStackTrace(PrintWriter writer) {
    super.printStackTrace(writer);
  }
}
