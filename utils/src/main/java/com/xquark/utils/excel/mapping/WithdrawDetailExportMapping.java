package com.xquark.utils.excel.mapping;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * @author luqing
 * @since 2019-05-31
 */
public class WithdrawDetailExportMapping extends BaseExportMapping{

  private final static Map<String,String> DETAIL = ImmutableMap.<String,String>builder()
      .put("序号", "getGenerator.next")
      .put("CPID","getCpId")
      .put("税前金额", "getAmount")
      .put("转入行行名", "getBankName")
      .put("转入行行号", "getBankAccount")
      .put("提现时间", "getWithdrawDateFormat")
      .build();

  @Override
  protected Map<String, String> provideMapping() {
    return DETAIL;
  }
}
