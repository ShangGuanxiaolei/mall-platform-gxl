package com.xquark.utils.excel.mapping;

import com.google.common.collect.ImmutableMap;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author luqing
 * @since 2019-05-31
 */
public class LaborExpensesExportMapping extends BaseExportMapping{

  private final static Map<String,String> LABOR = ImmutableMap.<String,String>builder()
          .put("序号", "getGenerator.next")
          .put("发放月份","getMonth")
          .put("ID","getCpId")
          .put("姓名","getNickName")
          .put("身份证号","getCardId")
          .put("手机号码","getPhone")
          .put("税前金额1","getFirstBefore")
          .put("个税1","getFirstTax")
          .put("实发金额1","getFirstAfter")
          .put("税前金额2","getSecondBefore")
          .put("个税2","getSecondTax")
          .put("实发金额2","getSecondAfter")
          .put("本月税前合计金额","getTotalBefore")
          .put("本月合计个税","getTotalTax")
          .put("本月合计实发金额","getTotalAfter")
          .build();

  @Override
  protected Map<String, String> provideMapping() {
    return LABOR;
  }
}
