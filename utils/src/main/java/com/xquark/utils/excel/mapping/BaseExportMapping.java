package com.xquark.utils.excel.mapping;

import com.google.common.base.Preconditions;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.MapUtils;

/**
 * Created by wangxinhua. Date: 2018/8/20 Time: 下午3:17
 */
public abstract class BaseExportMapping {

  private boolean checked;

  public String[] title() {
    Set<String> keys = getMappingWithCheck().keySet();
    return keys.toArray(new String[0]);
  }

  public String[] value() {
    Collection<String> vals = getMappingWithCheck().values();
    return vals.toArray(new String[0]);
  }

  protected abstract Map<String, String> provideMapping();

  private Map<String, String> getMappingWithCheck() {
    Map<String, String> map = provideMapping();
    if (!checked) {
      Preconditions.checkArgument(MapUtils.isNotEmpty(map), "exceld导出配置为空");
      checked = true;
    }
    return map;
  }

}
