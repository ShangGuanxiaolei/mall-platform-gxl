package com.xquark.utils.http;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public class SCRMResponse<T> extends ServerResponse<T> {

  private String returnMsg;

  private Integer returnCode;

  public String getReturnMsg() {
    return returnMsg;
  }

  public void setReturnMsg(String returnMsg) {
    this.returnMsg = returnMsg;
  }

  public Integer getReturnCode() {
    return returnCode;
  }

  public void setReturnCode(Integer returnCode) {
    this.returnCode = returnCode;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "SCRMResponse{" +
        "returnMsg='" + returnMsg + '\'' +
        ", returnCode=" + returnCode +
        ", data=" + data +
        '}';
  }
}
