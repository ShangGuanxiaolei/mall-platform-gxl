package com.xquark.utils.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import java.util.Map;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午11:26
 */
public class HttpHelper {

  /**
   * 将JSON字符串转为map
   * @param json 字符串
   * @param keyType map 键类型class
   * @param valueType map 值类型class
   * @param <K> map键类型
   * @param <V> map值类型
   * @return 转换后的map
   */
  public static <K, V> Map<K, V> parseToMap(String json,
      Class<K> keyType,
      Class<V> valueType) {
    return JSON.parseObject(json,
        new TypeReference<Map<K, V>>() {
        });
  }

}
