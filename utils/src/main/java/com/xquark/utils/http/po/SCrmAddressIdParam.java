package com.xquark.utils.http.po;

/**
 * Created by wangxinhua on 18-3-6. DESC:
 */
public class SCrmAddressIdParam extends SCrmParam {

  private final Integer aId;

  public SCrmAddressIdParam(Integer aId) {
    this.aId = aId;
  }

  public Integer getaId() {
    return aId;
  }

}
