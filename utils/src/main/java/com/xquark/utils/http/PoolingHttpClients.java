package com.xquark.utils.http;

import com.alibaba.fastjson.JSON;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.springframework.core.io.ClassPathResource;

/**
 * @author Wayne.Wang<5 waynewang @ gmail.com>
 * @since 8:05:04 PM Jul 28, 2014
 */
public class PoolingHttpClients {

  private static final Log log = LogFactory.getLog(PoolingHttpClients.class);

  private static final PoolingClientConnectionManager cm;

  private static final DefaultHttpClient httpClient;

  private static final ResponseHandler<HttpInvokeResult> responseHandler =
      new DefaultResponseHandler();

  static {
    cm = new PoolingClientConnectionManager();
    cm.setMaxTotal(HttpClientConfig.HTTP_MAX_TOTAL_CONN);
    cm.setDefaultMaxPerRoute(HttpClientConfig.HTTP_MAX_CONN_PER_ROUTE);
    httpClient = new DefaultHttpClient(cm);
    // 增加 keep alive 策略
    httpClient.setKeepAliveStrategy(
        (response, context) -> HttpClientConfig.HTTP_KEEPIDLE_DURATION);

    final HttpParams httpParams = httpClient.getParams();
    HttpProtocolParams.setContentCharset(httpParams, HttpClientConfig.HTTP_CHARSET);
    HttpProtocolParams.setHttpElementCharset(httpParams, HttpClientConfig.HTTP_CHARSET);
    HttpConnectionParams.setSoTimeout(httpParams, HttpClientConfig.HTTP_SO_TIMEOUT);
    HttpConnectionParams.setConnectionTimeout(httpParams, HttpClientConfig.HTTP_CONN_TIMEOUT);
    HttpConnectionParams.setSoKeepalive(httpParams, HttpClientConfig.HTTP_KEEPALIVE);
    HttpConnectionParams.setStaleCheckingEnabled(httpParams, HttpClientConfig.HTTP_STALE_CHECK);
    HttpConnectionParams.setTcpNoDelay(httpParams, true);
  }

  public static HttpInvokeResult get(final String url) {
    return get(url, 0);
  }

  public static HttpInvokeResult get(final String url, final long timeout) {
    return get(url, timeout, null);
  }

  public static HttpInvokeResult get(
      final String url, final long timeout, final List<Header> headers) {
    if (log.isDebugEnabled()) {
      log.debug("get url:" + url);
    }
    final HttpGet httpGet = new HttpGet(url);
    addHeaders(headers, httpGet);
    return invoke(httpGet, timeout);
  }

  private static void addHeaders(
      final List<? extends Header> headers, final HttpRequestBase request) {
    if (headers != null) {
      for (final Header header : headers) {
        if (header == null) {
          continue;
        }
        request.addHeader(header);
      }
    }
  }

  public static HttpInvokeResult post(final String url, final Object params) {
    String json = JSON.toJSONString(params);
    return post(url, HttpHelper.parseToMap(json, String.class, Object.class));
  }

  public static HttpInvokeResult post(final String url, final Map<String, ?> params) {

    return post(url, params, 0);
  }

  // 给拉区域调用的方法,方法从上到下层级调用
  public static HttpInvokeResult postJSON(final String url) {
    return postJSON(url, null);
  }

  public static HttpInvokeResult postJSON(final String url, final Object params) {
    return postJSON(url, params, 0);
  }

  public static HttpInvokeResult postJSON(final String url, Object params, final long timeout) {
    if (params == null) {
      params = new Object();
    }
    String json = JSON.toJSONString(params);
    return postJSON(url, json, timeout);
  }

  public static HttpInvokeResult postJSON(final String url, final String json, final long timeout) {
    return postJSON(url, new StringEntity(json, Charset.forName("UTF-8")), timeout);
  }

  public static HttpInvokeResult postJSON(
      final String url, final StringEntity body, final long timeout) {
    final HttpPost httpPost = new HttpPost(url);
    httpPost.setEntity(body);
    addHeaders(
        Collections.singletonList(new BasicHeader("Content-type", "application/json")), httpPost);
    return invoke(httpPost, timeout);
  }

  private static HttpResponse postJSONResponse(final String url, final StringEntity body) {
    final HttpPost httpPost = new HttpPost(url);
    httpPost.setEntity(body);
    addHeaders(
        Collections.singletonList(new BasicHeader("Content-type", "application/json")), httpPost);
    return invokeWithResponse(httpPost, 0);
  }

  private static HttpResponse postJSONResponse(final String url, final String json) {
    return postJSONResponse(url, new StringEntity(json, Charset.forName("UTF-8")));
  }

  public static HttpResponse postJsonResponse(final String url, Object params) {
    if (params == null) {
      params = new Object();
    }
    String json = JSON.toJSONString(params);
    return postJSONResponse(url, json);
  }

  public static HttpInvokeResult post(
      final String url, final Map<String, ?> params, final long timeout) {
    final HttpPost httpPost = new HttpPost(url);

    final List<NameValuePair> nvps = new ArrayList<>();
    if (params != null) {
      for (final Map.Entry<String, ?> entry : params.entrySet()) {
        nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
      }
    }

    httpPost.setEntity(new UrlEncodedFormEntity(nvps, Charset.forName("UTF-8")));

    return invoke(httpPost, timeout);
  }

  public static HttpInvokeResult post(String url, byte[] body) {
    return post(url, body, 0);
  }

  public static HttpInvokeResult post(String url, byte[] body, long timeout) {
    return post(url, body, 0, body.length, timeout);
  }

  public static HttpInvokeResult post(String url, byte[] body, int off, int len) {

    return post(url, body, off, len, 0);
  }

  public static HttpInvokeResult post(String url, byte[] body, int off, int len, long timeout) {
    final HttpPost httpPost = new HttpPost(url);

    httpPost.setEntity(new ByteArrayEntity(body, off, len, ContentType.APPLICATION_JSON));

    return invoke(httpPost, timeout);
  }

  public static HttpResponse invokeWithResponse(final HttpRequestBase request, final long timeout) {
    final String url = request.getURI().toString();
    if (log.isDebugEnabled()) {
      log.debug("invoke url:" + url);
    }

    HttpResponse result;

    if (timeout > 0) {
      request.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, (int) timeout);
      HttpConnectionParams.setConnectionTimeout(request.getParams(), (int) timeout);
    }

    try {
      result = httpClient.execute(request);
      int status = result.getStatusLine().getStatusCode();
      if (status != HttpStatus.SC_OK) {
        request.abort();
        log.error("请求失败,statusCode=" + status + ",url=" + url);
      }
      return result;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * 发起eps支付请求
   */
  public static String postEpsSSL(String url, Object content) {
    return postSSLEntity("cer/epscerts", url, content);
  }

  /**
   * 发送ssl请求
   *
   * @param keyStorePath 证书路径
   * @param url 地址
   * @param content 参数对象
   * @return 返回值
   */
  private static String postSSLEntity(String keyStorePath, String url, Object content) {
    String body;

    File storePathFile;
    try {
      storePathFile = new ClassPathResource(
          keyStorePath, PoolingHttpClients.class.getClassLoader()).getFile();
    } catch (IOException e) {
      log.error("证书文件" + keyStorePath + "在classPath下不存在");
      return null;
    }
    SSLContext sslcontext = customSSLContext(
        storePathFile.getPath(),
        null);

    // 设置协议http和https对应的处理socket链接工厂的对象
    Registry<ConnectionSocketFactory> socketFactoryRegistry =
        RegistryBuilder.<ConnectionSocketFactory>create()
            .register("http", PlainConnectionSocketFactory.INSTANCE)
            .register("https", new SSLConnectionSocketFactory(sslcontext))
            .build();
    PoolingHttpClientConnectionManager connManager =
        new PoolingHttpClientConnectionManager(socketFactoryRegistry);
    HttpClients.custom().setConnectionManager(connManager);

    // 创建自定义的httpclient对象
    CloseableHttpClient client = HttpClients.custom().setConnectionManager(connManager).build();

    // 创建post方式请求对象
    HttpPost httpPost = new HttpPost(url);

    // 设置参数到请求对象中
    httpPost.setEntity(new StringEntity(content.toString(), StandardCharsets.UTF_8));

    System.out.println("请求地址：" + url);
    System.out.println("请求参数：" + content.toString());

    // 设置header信息
    // 指定报文头【Content-type】、【User-Agent】
    httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
    httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

    // 执行请求操作，并拿到结果（同步阻塞）
    CloseableHttpResponse response = null;
    try {
      response = client.execute(httpPost);
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        // 按指定编码转换结果实体为String类型
        body = EntityUtils.toString(entity, StandardCharsets.UTF_8);
        EntityUtils.consume(entity);
        response.close();
        return body;
      }
    } catch (IOException e) {
      log.error("地址 " + url + " 请求失败", e);
    }
    // 获取结果实体
    // 释放链接
    return null;
  }

  static HttpInvokeResult invoke(final HttpRequestBase request, final long timeout) {
    final String url = request.getURI().toString();
    if (log.isDebugEnabled()) {
      log.debug("invoke url:" + url);
    }

    HttpInvokeResult result;

    if (timeout > 0) {
      request.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, (int) timeout);
      HttpConnectionParams.setConnectionTimeout(request.getParams(), (int) timeout);
    }

    try {
      result = httpClient.execute(request, responseHandler);
      if (result.getException() != null) {
        request.abort();
        log.error(
            "请求失败,statusCode="
                + result.getStatusCode()
                + ",url="
                + url
                + ","
                + result.getException().getMessage());
      }
      result.setUrl(request.getURI().toString());
      request.releaseConnection();
      return result;
    } catch (final Throwable e) {
      request.abort();
      log.error("请求失败,url=" + url + "," + e.getMessage());
      result = new HttpInvokeResult();
      result.setUrl(url);
      result.setException(e);
      result.setReason(e.getMessage());
      return result;
    } finally {
      request.reset();
    }
  }

  /**
   * 设置信任自签名证书
   */
  public static SSLContext customSSLContext(String keyStorePath, String keyStorepass) {
    SSLContext sc = null;
    FileInputStream instream = null;
    KeyStore trustStore;
    try {
      trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
      instream = new FileInputStream(new File(keyStorePath));
      trustStore.load(instream, keyStorepass == null ? null : keyStorepass.toCharArray());
      // 相信自己的CA和所有自签名的证书
      sc = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
    } catch (KeyStoreException
        | NoSuchAlgorithmException
        | CertificateException
        | IOException
        | KeyManagementException e) {
      e.printStackTrace();
    } finally {
      try {
        if (instream != null) {
          instream.close();
        }
      } catch (IOException e) {
        log.error("生成eps证书失败", e);
      }
    }
    return sc;
  }
}
