package com.xquark.utils.http;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public abstract class ServerResponse<T> {

  protected T data;

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
