package com.xquark.utils.http.po;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by wangxinhua on 18-2-28. DESC:
 */
public class SCrmParam extends XquarkParam {

  private static final String KEY = "ZIWOW";

  private final long timestamp;

  /**
   * scrm系统的单词拼错了--
   */
  private final String signture;

  SCrmParam() {
    this.timestamp = System.currentTimeMillis();
    this.signture = DigestUtils.md5Hex(KEY.concat(getTimestamp()));
  }

  public String getTimestamp() {
    return String.valueOf(timestamp);
  }

  public static String getKEY() {
    return KEY;
  }

  public String getSignture() {
    return signture;
  }

}
