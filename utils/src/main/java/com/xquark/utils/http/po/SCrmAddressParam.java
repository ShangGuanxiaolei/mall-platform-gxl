package com.xquark.utils.http.po;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public class SCrmAddressParam extends SCrmParam {

  private String id;

  private String unionId;

  private Integer aId;

  private String contacts;

  private String contactsMobile;

  private String provinceId;

  private String provinceName;

  private String cityId;

  private String cityName;

  private String areaId;

  private String areaName;

  private String streetName;

  private Integer isDefault;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public Integer getaId() {
    return aId;
  }

  public void setaId(Integer aId) {
    this.aId = aId;
  }

  public String getContacts() {
    return contacts;
  }

  public void setContacts(String contacts) {
    this.contacts = contacts;
  }

  public String getContactsMobile() {
    return contactsMobile;
  }

  public void setContactsMobile(String contactsMobile) {
    this.contactsMobile = contactsMobile;
  }

  public String getProvinceId() {
    return provinceId;
  }

  public void setProvinceId(String provinceId) {
    this.provinceId = provinceId;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public void setProvinceName(String provinceName) {
    this.provinceName = provinceName;
  }

  public String getCityId() {
    return cityId;
  }

  public void setCityId(String cityId) {
    this.cityId = cityId;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getAreaId() {
    return areaId;
  }

  public void setAreaId(String areaId) {
    this.areaId = areaId;
  }

  public String getAreaName() {
    return areaName;
  }

  public void setAreaName(String areaName) {
    this.areaName = areaName;
  }

  public String getStreetName() {
    return streetName;
  }

  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }

  public Integer getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(boolean isDefault) {
    if (isDefault) {
      this.isDefault = 1;
    } else {
      this.isDefault = 2;
    }
  }
}
