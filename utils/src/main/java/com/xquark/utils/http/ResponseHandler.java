package com.xquark.utils.http;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public interface ResponseHandler<T> {

  void handleRes(ServerResponse<T> res);

}
