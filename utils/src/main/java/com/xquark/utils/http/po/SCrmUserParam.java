package com.xquark.utils.http.po;

/**
 * Created by wangxinhua on 18-2-28. DESC: 同步用户参数
 */
public class SCrmUserParam extends SCrmParam {

  private final String mobile;

  private final String unionId;

  public SCrmUserParam(String mobile, String unionId) {
    this.mobile = mobile;
    this.unionId = unionId;
  }

  public String getMobile() {
    return mobile;
  }

  public String getUnionId() {
    return unionId;
  }
}
