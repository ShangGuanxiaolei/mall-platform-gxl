package com.xquark.utils;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.xquark.utils.excel.SheetContent;
import com.xquark.utils.excel.SheetHeader;
import com.xquark.utils.excel.mapping.BaseExportMapping;
import com.google.common.base.Function;
import com.xquark.utils.resultcode.ColumnMapping;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.Region;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

/**
 * Java操作Excel封装
 *
 * @author charon
 */
public class ExcelUtils {

  private static final Logger log = LoggerFactory.getLogger(ExcelUtils.class);

  /**
   * 导出Excle文档
   *
   * @param objList : Excel数据源
   * @param objClass : Excel数据源中的数据类型
   * @param sheetTitle : 新建Sheet的名称 ex: title = "员工表";
   * @param secondTitle : Sheet各列的标题（第一行各列的名称） ex: strTitle = "员工代码,员工姓名,性别,出生日期,籍贯,所属机构,联系电话,电子邮件,助记码";
   * @param strBody : Sheet各列的取值方法名（各列的值在objClass中get方法名称） ex: strBody =
   * "getCode,getName,getSex,getBirthday,getHomeplace.getName,getOrg.getShortName,getContactTel,getEmail,getZjm"
   * ;
   * @param outputPath : Excel文档保存路径
   */
  public static void exportExcelByObject(List objList, Class objClass,
      String sheetTitle, String firstTitle, String[] secondTitle,
      String[] strBody, String outputPath) {
    // 初始化工作簿
    HSSFWorkbook workbook = initWorkbook(objList, objClass, sheetTitle,
        firstTitle, secondTitle, strBody);
    // 保存Excel文件
    saveExcelFile(workbook, outputPath);
  }

  /**
   * 导出excel，支持多个sheet
   *
   * @param sheetWrappers 参数封装对象
   * @param outputPath 保存路径
   */
  public static void exportExcelByObject(List<? extends SheetContent<?>> sheetWrappers,
      String outputPath) {
    HSSFWorkbook workbook = initWorkbook(sheetWrappers);
    saveExcelFile(workbook, outputPath);
  }

  public static void exportExcelByObjectWithoutTitle(List objList, Class objClass,
      String sheetTitle, String[] secondTitle,
      String[] strBody, String outputPath) {
    // 初始化工作簿
    HSSFWorkbook workbook = initWorkbookWithoutTitle(objList, objClass,
        sheetTitle, secondTitle, strBody, false);
    // 保存Excel文件
    saveExcelFile(workbook, outputPath);
  }

  private static HSSFWorkbook initWorkbookWithoutTitle(List objList, Class objClass,
      String sheetTitle, String[] secondTitle,
      String[] strBody, boolean withIndex) {
    // 创建工作簿（Excel文件）
    HSSFWorkbook workbook = new HSSFWorkbook();
    // 创建Excel工作簿的第一个Sheet页
    HSSFSheet sheet = workbook.createSheet(sheetTitle);
    // 创建Sheet页的文件头（第一行）
    createSecondTitle(workbook, sheet, secondTitle, 0);
    // 创建Sheet页的文件体（后续行）
    createBody(workbook, objList, objClass, sheet, strBody, 1);
    return workbook;
  }

  private static HSSFWorkbook initWorkbookWithoutTitle(
      List<? extends SheetContent<?>> sheetWrappers) {
    Preconditions.checkArgument(!CollectionUtils.isEmpty(sheetWrappers));
    // 创建工作簿（Excel文件）
    HSSFWorkbook workbook = new HSSFWorkbook();
    for (SheetContent<?> sheetWrapper : sheetWrappers) {
      String sheetTitle = sheetWrapper.getTitle();
      String[] secondTitle = sheetWrapper.getDataTitle();
      // 创建Excel工作簿的第一个Sheet页
      HSSFSheet sheet = workbook.createSheet(sheetTitle);
      // 创建Sheet页的文件头（第一行）
      createSecondTitle(workbook, sheet, secondTitle, 0);
      // 创建Sheet页的文件体（后续行）
      createBody(workbook, sheetWrapper.getObjList(), sheetWrapper.getObjType(), sheet,
          sheetWrapper.getDataBody(), 1);
    }
    return workbook;
  }

  /**
   * 初始化工作簿
   *
   * @param objList : Excel数据源
   * @param objClass : Excel数据源中的数据类型
   * @param sheetTitle : 新建Sheet的名称
   * @param secondTitle : Sheet各列的标题（第一行各列的名称）
   * @param strBody : Sheet各列的取值方法名（各列的值在objClass中get方法名称）
   * @deprecated use {@link ExcelUtils#initWorkbook(List)} instead
   */
  @Deprecated
  private static HSSFWorkbook initWorkbook(List objList, Class objClass,
      String sheetTitle, String firstTitle, String[] secondTitle,
      String[] strBody) {
    // 创建工作簿（Excel文件）
    HSSFWorkbook workbook = new HSSFWorkbook();

    // 创建Excel工作簿的第一个Sheet页
    HSSFSheet sheet = workbook.createSheet(sheetTitle);

    // 创建Sheet页的文件头（第一行）
    createFirstTitle(workbook, sheet, firstTitle, 0);

    // 创建Sheet页的文件头（第一行）
    createSecondTitle(workbook, sheet, secondTitle, 1);

    // 创建Sheet页的文件体（后续行）
    createBody(workbook, objList, objClass, sheet, strBody, 2);

    sheet.addMergedRegion(new Region(0, (short) 0, 0,
        (short) (strBody.length + 1)));

    return workbook;
  }

  /**
   * 创建包含多个sheet的excel文件
   */
  private static HSSFWorkbook initWorkbook(List<? extends SheetContent<?>> sheetContents) {
    Preconditions.checkArgument(!CollectionUtils.isEmpty(sheetContents));
    HSSFWorkbook workbook = new HSSFWorkbook();
    for (SheetContent<?> sheetContent : sheetContents) {

      String sheetTitle = sheetContent.getTitle();
      HSSFSheet sheet = workbook.createSheet(sheetTitle);

      List<SheetHeader> headers = sheetContent.getHeaders();
      // 在构造函数中约束了非空
      assert headers != null;
      int headerSize = headers.size();
      for (int i = 0; i < headerSize; i++) {
        SheetHeader header = headers.get(i);
        createTitle(workbook, sheet, header, i);
      }
      // 创建Sheet页的文件体（后续行）
      createBody(workbook, sheetContent.getObjList(), sheetContent.getObjType(), sheet,
          sheetContent.getDataBody(), headerSize);
    }
    return workbook;
  }

  private static HSSFCellStyle createFirstTitleStyle(HSSFWorkbook workbook) {
    HSSFCellStyle cellStyle = workbook.createCellStyle();
    // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND); //填充单元格
    // cellStyle.setFillForegroundColor(HSSFColor.AUTOMATIC.index); //填暗红色
    HSSFFont font = workbook.createFont();
    font.setFontHeightInPoints((short) 24); // 字体大小
    font.setFontName("楷体");
    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 粗体
    font.setColor(HSSFColor.BLACK.index); // 颜色
    cellStyle.setFont(font);
    return cellStyle;
  }

  private static HSSFCellStyle createTitleStyle(HSSFWorkbook workbook, SheetHeader header) {
    HSSFCellStyle cellStyle = workbook.createCellStyle();
    HSSFFont font = workbook.createFont();
    font.setFontHeightInPoints(header.getFontSize()); // 字体大小
    // font.setFontName("楷体");
    font.setBoldweight(header.getBoldWeight()); // 粗体
    font.setColor(header.getFontColor()); // 颜色
    cellStyle.setFont(font);
    return cellStyle;
  }

  private static HSSFCellStyle createSecondTitleStyle(HSSFWorkbook workbook) {
    HSSFCellStyle cellStyle = workbook.createCellStyle();
    // cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND); //填充单元格
    // cellStyle.setFillForegroundColor(HSSFColor.AUTOMATIC.index); //填暗红色
    HSSFFont font = workbook.createFont();
    font.setFontHeightInPoints((short) 12); // 字体大小
    // font.setFontName("楷体");
    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 粗体
    font.setColor(HSSFColor.BLACK.index); // 颜色
    cellStyle.setFont(font);
    return cellStyle;
  }

  /**
   * 创建Excel当前sheet页的头信息
   *
   * @param sheet : Excel工作簿的一个sheet
   * @param strTitle : sheet头信息列表(sheet第一行各列值)
   * @param rowNum 行号
   */
  private static void createFirstTitle(HSSFWorkbook workbook,
      HSSFSheet sheet, String strTitle, int rowNum) {
    HSSFRow row = sheet.createRow(rowNum); // 创建该页的一行
    row.setHeight((short) 640);

    HSSFCell cell1 = row.createCell((short) 0); // 创建该行的一列
    cell1.setCellType(HSSFCell.CELL_TYPE_STRING);
    HSSFRichTextString value = new HSSFRichTextString(strTitle);
    cell1.setCellValue(value);
    cell1.setCellStyle(createFirstTitleStyle(workbook));
  }

  /**
   * 创建Excel当前sheet页的头信息
   *
   * @param sheet : Excel工作簿的一个sheet
   * @param strTitle : sheet头信息列表(sheet第一行各列值)
   * @param rowNum 行号
   */
  private static void createSecondTitle(HSSFWorkbook workbook,
      HSSFSheet sheet, String[] strTitle, int rowNum, HSSFCellStyle style) {
    HSSFRow row = sheet.createRow(rowNum); // 创建该页的一行
    HSSFCell cell;
    style = Optional.fromNullable(style).or(createSecondTitleStyle(workbook));
    for (short i = 0; i < strTitle.length; i++) {
      cell = row.createCell(i); // 创建该行的一列
      cell.setCellType(HSSFCell.CELL_TYPE_STRING);
      HSSFRichTextString value = new HSSFRichTextString(strTitle[i]);
      cell.setCellValue(value);
      cell.setCellStyle(style);
      sheet.setColumnWidth(i, (short) 5120);
    }
  }

  private static void createSecondTitle(HSSFWorkbook workbook,
      HSSFSheet sheet, String[] strTitle, int rowNum) {
    createSecondTitle(workbook, sheet, strTitle, rowNum, createSecondTitleStyle(workbook));
  }

  /**
   * 根据配置创建标题
   */
  private static void createTitle(HSSFWorkbook workbook, HSSFSheet sheet, SheetHeader header,
      int rowNum) {
    String[] titles = header.getContents();
    HSSFCellStyle titleStyle = createTitleStyle(workbook, header);
    createSecondTitle(workbook, sheet, titles, rowNum, titleStyle);
  }

  /**
   * 创建Excel当前sheet页的体信息
   *
   * @param objList : Excel数据源
   * @param objClass : Excel数据源中的数据类型
   * @param sheet : Excel工作簿的sheet页
   */
  private static void createBody(HSSFWorkbook workbook, List objList, Class objClass,
      HSSFSheet sheet, String[] targetMethod, int startRowNum) {
    Method[] ms = objClass.getMethods();
    //cellStyle create超过4000次后会报错 所以放到循环外
    HSSFCellStyle cellStyle = workbook.createCellStyle();
    cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
    // 循环objList对象列表（生成sheet的行）
    for (int objIndex = 0; objIndex < objList.size(); objIndex++) {
      Object obj = objList.get(objIndex);
      HSSFRow row = sheet.createRow(objIndex + startRowNum);
      // 循环strBody目标方法数组（生成sheet的列）
      for (short strIndex = 0; strIndex < targetMethod.length; strIndex++) {
        String targetMethodName = targetMethod[strIndex];
        // 循环ms方法数组，找到目标方法（strBody中指定的方法）并调用
        for (int i = 0; i < ms.length; i++) {
          Method srcMethod = ms[i];
          int len = targetMethodName.indexOf(".") < 0 ? targetMethodName
              .length() : targetMethodName.indexOf(".");
          if (srcMethod.getName().equals(
              targetMethodName.substring(0, len))) {
            HSSFCell cell = row.createCell(strIndex);
            cell.setCellType(HSSFCell.CELL_TYPE_STRING);
            try {
              // 如果方法返回一个引用类型的值
              Object valueObj = null;
              if (targetMethodName.contains(".")) {
                valueObj = referenceInvoke(targetMethodName, obj);
              } else {
                valueObj = srcMethod.invoke(obj);
              }
              HSSFRichTextString value = null;
              if (valueObj == null) {
                value = new HSSFRichTextString("");
              } else {
                if (valueObj instanceof BigDecimal) {
                  cell.setCellStyle(cellStyle);
                }
                value = new HSSFRichTextString((valueObj.toString()));
              }
              cell.setCellValue(value);
            } catch (Exception e) {
              log.debug("反射获取对象值异常:", e);
            }
          }
        }
      }
    }

  }

  /**
   * 方法返回的是一个对象的引用（如：getHomeplace.getName类型的方法序列） 按方法序列逐层调用直到最后放回基本类型的值
   *
   * @param targetMethod : obj对象所包含的方法列
   * @param obj : 待处理的对象
   */
  // getHomeplace.getName emp(obj)
  private static Object referenceInvoke(String targetMethod, Object obj) {
    // 截取方法序列的第一个方法(即截取属于obj对象的方法：getHomeplace())
    String refMethod = targetMethod.substring(0, targetMethod.indexOf("."));
    // 获得后续方法序列(getName())
    targetMethod = targetMethod.substring(targetMethod.indexOf(".") + 1);
    try {
      // 获得第一个方法的执行结果(即obj方法执行的结果：obj.getHomeplace())
      obj = obj.getClass().getMethod(refMethod).invoke(obj);
    } catch (Exception e) {
      log.debug("反射获取对象值异常:", e);
    }

    // 如果方法序列没到最后一节
    if (targetMethod.contains(".")) {
      return referenceInvoke(targetMethod, obj);
      // 如果方法序列到达最后一节
    } else {
      try {
        // 通过obj对象获得该方法链的最后一个方法并调用
        Method tarMethod = obj.getClass().getMethod(targetMethod);
        if (tarMethod == null) {
          return null;
        }
        return tarMethod.invoke(obj);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

  }

  /**
   * 保存Excel文件
   *
   * @param workbook : Excel工作簿
   * @param outputPath : Excel文件保存路径
   */
  private static void saveExcelFile(HSSFWorkbook workbook, String outputPath) {
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(outputPath);
      workbook.write(fos);

      fos.flush();
      fos.close();
    } catch (IOException e) {
      log.debug("文件操作异常:", e);
    } finally {
      if (fos != null) {
        try {
          fos.close();
        } catch (IOException e) {
          log.debug("关闭流文件异常:", e);
        }
      }
    }
  }

  /**
   * 将excel导入后返回的列名映射为对象的属性名，并返回对象集合数据
   *
   * @param arr excel表格导入后的数组
   * @param columnMappingMap 映射关系
   * @param clazz 对象class类型
   * @param <T> class泛型
   * @return 数据集合
   */
  private static <T> List<T> transformObjFromList(List<List<String>> arr,
      Map<String, ColumnMapping> columnMappingMap, Class<T> clazz) {
    if (CollectionUtils.isEmpty(arr)) {
      return Collections.emptyList();
    }
    List<String> columns = arr.get(0);
    // 取剩余数据反射
    List<T> ret = new ArrayList<>();
    for (int i = 1; i < arr.size(); i++) {
      List<String> rows = arr.get(i);
      T target;
      try {
        target = clazz.newInstance();
      } catch (InstantiationException | IllegalAccessException e) {
        log.error("类型 {} 无法实例化, 检查是否有默认构造函数", clazz);
        throw new RuntimeException("获取反射信息错误", e);
      }
      for (int j = 0; j < rows.size(); j++) {
        // 属性名称
        ColumnMapping columnMapping = columnMappingMap.get(columns.get(j));
        if (columnMapping == null) {
          throw new IllegalArgumentException("请检查表格列名格式是否正确");
        }
        String fieldName = columnMapping.getName();
        if (StringUtils.isEmpty(fieldName)) {
          throw new IllegalArgumentException("列名映射关系错误");
        }
        // 属性值
        String fieldValue = rows.get(j);
        Field field = ReflectionUtils.findField(clazz, fieldName);
        if (field == null) {
          throw new IllegalArgumentException("字段" + fieldName + "不正确");
        }
        //  TODO 校验实际类型与配置类型是否一致
        @SuppressWarnings("unused")
        Class<?> declaringClass = field.getDeclaringClass();
        // rawType
        @SuppressWarnings("unchecked")
        Function<String, ?> transformer = columnMapping.getTransformer();
        // 实际的属性值
        Object val;
        // 反射设置值
        try {
          val = transformer.apply(fieldValue);
          FieldUtils.writeField(field, target, val, true);
        } catch (Exception e) {
          String msg = String.format("第 %s 行 %s 列, 属性名: %s 导入失败", i, j, columns.get(j));
          log.error(msg, e);
          throw new RuntimeException(msg);
        }
      }
      ret.add(target);
    }
    return ret;
  }

  public static <T> List<T> excelImport(InputStream fis,
      Map<String, ColumnMapping> columnMappingMap, Class<T> clazz)
      throws IOException, InvalidFormatException {
    List<List<String>> importRet = excelImportWithArr(fis);
    return transformObjFromList(importRet, columnMappingMap, clazz);
  }

  /**
   * 导入Excel文件 使用arrayList代替数组, 过滤掉数组可能出现的空行
   */
  private static List<List<String>> excelImportWithArr(InputStream fis)
      throws IOException, InvalidFormatException {
    Workbook wb = createCommonWorkbook(fis);
    Sheet sheet = wb.getSheetAt(0);
    int rowNum = sheet.getLastRowNum() + 1;
    int cellNum = sheet.getRow(0).getLastCellNum();
    List<List<String>> data = new ArrayList<>();
    for (int j = 0; j < rowNum; j++) {
      Row row = sheet.getRow(j);
      if (row != null && !checkIfRowIsEmpty(row)) {
        List<String> cellList = new ArrayList<>(cellNum);
        for (int i = 0; i < cellNum; i++) {
          Cell cell = row.getCell(i);
          if (cell != null && StringUtils.isNoneBlank(cell.toString())) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cellList.add(cell.toString());
          } else {
            // handle format exception
            cellList.add("");
          }
        }
        data.add(cellList);
      }
    }
    return data;
  }

  /**
   * 导入Excel文件 建议使用返回list的导入方法, 该方法可能会产生空白行
   */
  @Deprecated
  public static String[][] excelImport(InputStream fis) throws IOException, InvalidFormatException {
    Workbook wb = createCommonWorkbook(fis);
    Sheet sheet = wb.getSheetAt(0);
    int rowNum = sheet.getLastRowNum() + 1;
    int cellNum = sheet.getRow(0).getLastCellNum();
    String[][] data = new String[rowNum][cellNum];
    for (int j = 0; j < rowNum; j++) {
      Row row = sheet.getRow(j);
      if (row != null) {
        for (int i = 0; i < cellNum; i++) {
          Cell cell = row.getCell(i);
          if (cell != null && StringUtils.isNoneBlank(cell.toString())) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            data[j][i] = cell.toString();
          } else {
            data[j][i] = "";
          }
        }
      }
    }
    return data;
  }

  private static boolean checkIfRowIsEmpty(Row row) {
    if (row == null) {
      return true;
    }
    if (row.getLastCellNum() <= 0) {
      return true;
    }
    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
      Cell cell = row.getCell(cellNum);
      if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK && StringUtils
          .isNotBlank(cell.toString())) {
        return false;
      }
    }
    return true;
  }

  /**
   * 判断excel版本
   */
  public static Workbook createCommonWorkbook(InputStream inp)
      throws IOException, InvalidFormatException {
    // 首先判断流是否支持mark和reset方法，最后两个if分支中的方法才能支持
    if (!inp.markSupported()) {
      // 还原流信息
      inp = new PushbackInputStream(inp, 8);
    }
    // EXCEL2007使用的是OOM文件格式
    if (POIXMLDocument.hasOOXMLHeader(inp)) {
      //可以直接传流参数，但是推荐使用OPCPackage容器打开
      return new XSSFWorkbook(OPCPackage.open(inp));
    }
    // EXCEL2003使用的是微软的文件系统
    if (POIFSFileSystem.hasPOIFSHeader(inp)) {
      return new HSSFWorkbook(inp);
    }
    throw new IOException("不能解析的excel版本");
  }
}
