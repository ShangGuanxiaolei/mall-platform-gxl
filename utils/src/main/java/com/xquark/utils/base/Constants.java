package com.xquark.utils.base;

/**
 * @Author: tanggb
 * @Time: Create in 9:11 2018/9/10
 * @Description:
 * @Modified by:
 */
public interface Constants {
    /***
     * push通道
     */
    final public static String PUSH_CHANNEL = "push";

    /***
     * 短信通道
     */
    final public static String SMS_CHANNEL = "sms";

    /**
     * 响应请求成功
     */
    final public static String HTTP_RES_CODE_200_VALUE = "success";

    /**
     * 系统错误
     */
    final public static String HTTP_RES_CODE_500_VALUE = "fial";

    /**
     * 响应请求成功code
     */
    final public static Integer HTTP_RES_CODE_200 = 200;

    /**
     * 系统错误
     */
    final public static Integer HTTP_RES_CODE_500 = 500;

    /**
     *
     */

}
