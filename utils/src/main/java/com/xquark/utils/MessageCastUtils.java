package com.xquark.utils;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author luqing
 * @since 2019-05-02
 */
public class MessageCastUtils {

  private final static Logger LOGGER = LoggerFactory.getLogger(MessageCastUtils.class);

  public static Map<String,String> castParam(String needParam){
    if (StringUtils.isEmpty(needParam)){
      return new HashMap<>(0);
    }

    String[] split = needParam.trim().split(",");
    Map<String,String> map = new HashMap<>(split.length);
    for (String ps : split){
      String[] split1 = ps.trim().split(":");
      if (split1.length != 2){
        continue;
      }
      map.put(split1[0],split1[1]);
    }
    return map;
  }

  public static List<Long> castCpIds(String cpIds){
    if (StringUtils.isEmpty(cpIds)){
      return new ArrayList<>(0);
    }

    String[] split = cpIds.split(",");
    List<Long> result = new ArrayList<>(split.length);

    for (String s : split){
      try {
        result.add(Long.valueOf(s));
      } catch (NumberFormatException e) {
        LOGGER.debug("cpId:{} 不合法,跳过推送",s);
      }
    }

    return result;
  }

  public static String castParamMap(Map<String,String> map){

    if (Objects.isNull(map)){
      return "";
    }

    StringBuilder needParam = new StringBuilder();
    for (Map.Entry<String,String> entry : map.entrySet()){
      needParam.append(entry.getKey())
          .append(":")
          .append(entry.getValue())
          .append(",");
    }

    if (!StringUtils.isEmpty(needParam)){
      needParam.deleteCharAt(needParam.lastIndexOf(","));
    }

    return String.valueOf(needParam);
  }

  public static String castCpIdList(List<Long> cpIdList){
    if (Objects.isNull(cpIdList)){
      return "";
    }

    StringBuilder cpIds = new StringBuilder();

    for (Long l : cpIdList){
      cpIds.append(l).append(",");
    }

    if (!StringUtils.isEmpty(cpIds)){
      cpIds.deleteCharAt(cpIds.lastIndexOf(","));
    }
    return String.valueOf(cpIds);
  }

  public static String replaceContent(String content,Map<String,String> params){

    if (MapUtils.isEmpty(params)){
      return content;
    }

    for (Map.Entry<String,String> entry : params.entrySet()){
      content = content.replace(entry.getKey(),entry.getValue());
    }

    return content;

  }


}
