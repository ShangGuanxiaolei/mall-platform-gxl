package com.xquark.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by uzstudio on 15/10/9.
 */
public class StringUtil {

  public static boolean isNull(String value) {
    return value == null || value.length() == 0;
  }

  public static boolean isNotNull(String value) {
    return value != null && value.length() > 0;
  }

  public static String toHexString(byte[] datas) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < datas.length; i++) {
      String hex = Integer.toHexString(datas[i] & 0xFF);
      if (hex.length() <= 1) {
        sb.append('0');
      }
      sb.append(hex);
    }
    return sb.toString();
  }

  public static String toJson(List list) {
    StringBuilder sb = new StringBuilder();
    sb.append('[');
    for (int i = 0; i < list.size(); i++) {
      if (i != 0) {
        sb.append(',');
      }
      Object o = list.get(i);
      if (o instanceof String) {
        String os = (String) o;
        // " => \", however regex expression need double, so it become
        // complex
        os = os.replaceAll("\"", "\\\\\"");
        sb.append('"').append(os).append('"');
        ;
      } else {
        sb.append(list.get(i));
      }
    }
    sb.append(']');
    return sb.toString();
  }

  public static String toJson(Set<String> set) {
    StringBuilder sb = new StringBuilder();
    sb.append('[');
    if (!set.isEmpty()) {
      for (String key : set) {
        sb.append("\"").append(key).append("\",");
      }
      sb.deleteCharAt(sb.length() - 1);
    }
    sb.append(']');
    return sb.toString();
  }

  /**
   * 按指定长度和编码拆分中英混合字符串
   *
   * @param text 被拆分字符串
   * @param length 指定长度，即子字符串的最大长度
   * @param encoding 字符编码
   * @return 拆分后的子字符串列表
   */
  public static ArrayList splitStringByEncoding(String text, int length, String encoding)
      throws UnsupportedEncodingException {
    ArrayList texts = new ArrayList();
    int pos = 0;
    int startInd = 0;
    for (int i = 0; text != null && i < text.length(); ) {
      byte[] b = String.valueOf(text.charAt(i)).getBytes(encoding);
      if (b.length > length) {
        i++;
        startInd = i;
        continue;
      }
      pos += b.length;
      if (pos >= length) {
        int endInd;
        if (pos == length) {
          endInd = ++i;
        } else {
          endInd = i;
        }
        texts.add(text.substring(startInd, endInd));
        pos = 0;
        startInd = i;
      } else {
        i++;
      }
    }
    if (startInd < text.length()) {
      texts.add(text.substring(startInd));
    }
    return texts;
  }

  /**
   * 指定长度按UTF-8编码拆分中英混合字符串，即一个非ASCII码长度为3
   *
   * @param text 被拆分字符串
   * @param length 指定长度，即子字符串的最大长度
   */
  public static ArrayList splitString(String text, int len) {
    try {
      return splitStringByEncoding(text, len, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
