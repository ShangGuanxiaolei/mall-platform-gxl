package com.xquark.utils.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * @author: chenxi
 */

public class ChainedThrowableDelegate implements ChainedThrowable {

  /**
   *
   */
  private static final long serialVersionUID = -5042649821935429254L;

  protected static final Throwable NO_CAUSE = new Throwable();

  private static final String[] CAUSE_METHOD_NAMES = {"getNested",
      "getNestedException", "getNextException", "getTargetException",
      "getException", "getSourceException", "getCausedByException",
      "getRootCause", "getCause"};

  private static final String[] CAUSE_FIELD_NAMES = {"detail"};

  protected Throwable delegatedThrowable;

  public ChainedThrowableDelegate(Throwable throwable) {
    this.delegatedThrowable = throwable;
  }

  public Throwable getCause() {
    Throwable cause = getCauseByWellKnownTypes(delegatedThrowable);

    for (Class<?> throwableClass = delegatedThrowable.getClass(); (cause == null)
        && Throwable.class.isAssignableFrom(throwableClass); throwableClass = throwableClass
        .getSuperclass()) {
      for (int i = 0; (cause == null) && (i < CAUSE_METHOD_NAMES.length); i++) {
        cause = getCauseByMethodName(delegatedThrowable,
            throwableClass, CAUSE_METHOD_NAMES[i]);
      }

      for (int i = 0; (cause == null) && (i < CAUSE_FIELD_NAMES.length); i++) {
        cause = getCauseByFieldName(delegatedThrowable, throwableClass,
            CAUSE_FIELD_NAMES[i]);
      }
    }

    if (cause == delegatedThrowable) {
      cause = null;
    }

    if (cause == NO_CAUSE) {
      return null;
    }

    return cause;
  }

  protected Throwable getCauseByWellKnownTypes(Throwable throwable) {
    Throwable cause = null;
    boolean isWellKnownType = false;

    if (ChainedThrowable.class.isInstance(throwable)) {
      isWellKnownType = true;
      cause = ((ChainedThrowable) throwable).getCause();
    } else if (SQLException.class.isInstance(throwable)) {
      isWellKnownType = true;
      cause = ((SQLException) throwable).getNextException();
    } else if (InvocationTargetException.class.isInstance(throwable)) {
      isWellKnownType = true;
      cause = ((InvocationTargetException) throwable)
          .getTargetException();
    } else if (RemoteException.class.isInstance(throwable)) {
      isWellKnownType = true;
      cause = ((RemoteException) throwable).detail;
    }

    if (isWellKnownType && (cause == null)) {
      return NO_CAUSE;
    }

    return cause;
  }

  protected Throwable getCauseByMethodName(Throwable throwable,
      Class<?> throwableClass, String methodName) {
    Method method = null;

    try {
      method = throwableClass.getMethod(methodName, new Class[0]);
    } catch (NoSuchMethodException ignored) {
    }

    if ((method != null)
        && Throwable.class.isAssignableFrom(method.getReturnType())) {
      Throwable cause = null;

      try {
        cause = (Throwable) method.invoke(throwable, new Object[0]);
      } catch (IllegalAccessException ignored) {
      } catch (IllegalArgumentException ignored) {
      } catch (InvocationTargetException ignored) {
      }

      if (cause == null) {
        return NO_CAUSE;
      }

      return cause;
    }

    return null;
  }

  protected Throwable getCauseByFieldName(Throwable throwable,
      Class<?> throwableClass, String fieldName) {
    Field field = null;

    try {
      field = throwableClass.getField(fieldName);
    } catch (NoSuchFieldException ignored) {
    }

    if ((field != null)
        && Throwable.class.isAssignableFrom(field.getType())) {
      Throwable cause = null;

      try {
        cause = (Throwable) field.get(throwable);
      } catch (IllegalAccessException ignored) {
      } catch (IllegalArgumentException ignored) {
      }

      if (cause == null) {
        return NO_CAUSE;
      }

      return cause;
    }

    return null;
  }

  public void printStackTrace() {
    ExceptionHelper.printStackTrace(this);
  }

  public void printStackTrace(PrintStream stream) {
    ExceptionHelper.printStackTrace(this, stream);
  }

  public void printStackTrace(PrintWriter writer) {
    ExceptionHelper.printStackTrace(this, writer);
  }

  public void printCurrentStackTrace(PrintWriter writer) {
    if (ChainedThrowable.class.isInstance(delegatedThrowable)) {
      ((ChainedThrowable) delegatedThrowable)
          .printCurrentStackTrace(writer);
    } else {
      delegatedThrowable.printStackTrace(writer);
    }
  }
}
