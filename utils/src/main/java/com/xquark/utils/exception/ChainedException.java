package com.xquark.utils.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @author: chenxi
 */

public class ChainedException extends Exception implements ChainedThrowable {

  /**
   *
   */
  private static final long serialVersionUID = -5093514996306392130L;
  private final ChainedThrowable delegate = new ChainedThrowableDelegate(this);
  private Throwable cause;

  public ChainedException() {
    super();
  }

  public ChainedException(String message) {
    super(message);
  }

  public ChainedException(Throwable cause) {
    super((cause == null) ? null : cause.getMessage());
    this.cause = cause;
  }

  public ChainedException(String message, Throwable cause) {
    super(message);
    this.cause = cause;
  }

  public Throwable getCause() {
    return cause;
  }

  public void printStackTrace() {
    delegate.printStackTrace();
  }

  public void printStackTrace(PrintStream stream) {
    delegate.printStackTrace(stream);
  }

  public void printStackTrace(PrintWriter writer) {
    delegate.printStackTrace(writer);
  }

  public void printCurrentStackTrace(PrintWriter writer) {
    super.printStackTrace(writer);
  }
}
