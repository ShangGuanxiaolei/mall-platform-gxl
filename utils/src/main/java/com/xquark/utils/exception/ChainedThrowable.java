package com.xquark.utils.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * @author: chenxi
 */

public interface ChainedThrowable extends Serializable {

  Throwable getCause();

  void printStackTrace();

  void printStackTrace(PrintStream stream);

  void printStackTrace(PrintWriter writer);

  void printCurrentStackTrace(PrintWriter writer);
}