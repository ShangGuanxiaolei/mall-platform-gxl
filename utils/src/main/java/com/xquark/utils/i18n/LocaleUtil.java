package com.xquark.utils.i18n;

import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

/**
 * @author: chenxi
 */

public class LocaleUtil {

  private static final Set<String> AVAILABLE_LANGUAGES = new HashSet<String>();
  private static final Set<String> AVAILABLE_COUNTRIES = new HashSet<String>();
  private static final LocaleInfo systemLocaleInfo;
  private static LocaleInfo defaultLocalInfo;
  private static final ThreadLocal<LocaleInfo> contextLocaleInfoHolder = new ThreadLocal<LocaleInfo>();

  static {
    Locale[] availableLocales = Locale.getAvailableLocales();

    for (int i = 0; i < availableLocales.length; i++) {
      Locale locale = availableLocales[i];

      AVAILABLE_LANGUAGES.add(locale.getLanguage());
      AVAILABLE_COUNTRIES.add(locale.getCountry());
    }

    systemLocaleInfo = new LocaleInfo();

    defaultLocalInfo = systemLocaleInfo;
  }

  private LocaleUtil() {

  }

  public static Locale parseLocale(String localeName) {
    if (localeName != null) {
      String[] localeParts = StringUtils.split(localeName, "_");
      int len = localeParts.length;

      if (len > 0) {
        String language = localeParts[0];
        String country = "";
        String variant = "";

        if (len > 1) {
          country = localeParts[1];
        }

        if (len > 2) {
          variant = localeParts[2];
        }

        return new Locale(language, country, variant);
      }
    }

    return null;
  }

  public static LocaleInfo parseLocaleInfo(String localeName) {
    Locale locale = null;
    String charset = null;

    if (StringUtils.isNotEmpty(localeName)) {
      int index = localeName.indexOf(":");
      String localePart = localeName;
      String charsetPart = null;

      if (index >= 0) {
        localePart = localeName.substring(0, index);
        charsetPart = localeName.substring(index + 1);
      }

      locale = parseLocale(localePart);

      charset = StringUtils.trimToNull(charsetPart);
    }

    return new LocaleInfo(locale, charset);
  }

  public static boolean isLocaleSupported(Locale locale) {
    return (locale != null)
        && AVAILABLE_LANGUAGES.contains(locale.getLanguage())
        && AVAILABLE_COUNTRIES.contains(locale.getCountry());
  }

  public static boolean isCharsetSupported(String charset) {
    return Charset.isSupported(charset);
  }

  public static String getCanonicalCharset(String charset) {
    return Charset.forName(charset).name();
  }

  public static String getCanonicalCharset(String charset,
      String defaultCharset) {
    String result = null;

    try {
      result = getCanonicalCharset(charset);
    } catch (IllegalArgumentException e) {
      if (defaultCharset != null) {
        try {
          result = getCanonicalCharset(defaultCharset);
        } catch (IllegalArgumentException ee) {
        }
      }
    }

    return result;
  }

  public static List<String> calculateBundleNames(String baseName,
      Locale locale) {
    return calculateBundleNames(baseName, locale, false);
  }

  public static List<String> calculateBundleNames(String baseName,
      Locale locale, boolean noext) {
    baseName = (baseName == null) ? "" : baseName;

    if (locale == null) {
      locale = new Locale("");
    }

    String ext = "";
    int extLength = 0;

    if (!noext) {
      int extIndex = baseName.lastIndexOf(".");

      if (extIndex != -1) {
        ext = baseName.substring(extIndex, baseName.length());
        extLength = ext.length();
        baseName = baseName.substring(0, extIndex);

        if (extLength == 1) {
          ext = "";
          extLength = 0;
        }
      }
    }

    List<String> result = new ArrayList<String>(4);
    String language = locale.getLanguage();
    int languageLength = language.length();
    String country = locale.getCountry();
    int countryLength = country.length();
    String variant = locale.getVariant();
    int variantLength = variant.length();

    StringBuilder builder = new StringBuilder(baseName);

    builder.append(ext);
    result.add(builder.toString());
    builder.setLength(builder.length() - extLength);

    if ((languageLength + countryLength + variantLength) == 0) {
      return result;
    }

    if (builder.length() > 0) {
      builder.append('_');
    }

    builder.append(language);

    if (languageLength > 0) {
      builder.append(ext);
      result.add(builder.toString());
      builder.setLength(builder.length() - extLength);
    }

    if ((countryLength + variantLength) == 0) {
      return result;
    }

    builder.append('_').append(country);

    if (countryLength > 0) {
      builder.append(ext);
      result.add(builder.toString());
      builder.setLength(builder.length() - extLength);
    }

    if (variantLength == 0) {
      return result;
    }

    builder.append('_').append(variant);

    builder.append(ext);
    result.add(builder.toString());
    builder.setLength(builder.length() - extLength);

    return result;
  }

  public static LocaleInfo getSystem() {
    return systemLocaleInfo;
  }

  public static LocaleInfo getDefault() {
    return (defaultLocalInfo == null) ? systemLocaleInfo : defaultLocalInfo;
  }

  public static LocaleInfo setDefault(Locale locale) {
    LocaleInfo old = getDefault();

    defaultLocalInfo = new LocaleInfo(locale, null, systemLocaleInfo);

    return old;
  }

  public static LocaleInfo setDefault(Locale locale, String charset) {
    LocaleInfo old = getDefault();

    defaultLocalInfo = new LocaleInfo(locale, charset, systemLocaleInfo);

    return old;
  }

  public static LocaleInfo setDefault(LocaleInfo localeInfo) {
    if (localeInfo == null) {
      return setDefault(null, null);
    } else {
      LocaleInfo old = getDefault();

      defaultLocalInfo = localeInfo;

      return old;
    }
  }

  public static void resetDefault() {
    defaultLocalInfo = systemLocaleInfo;
  }

  public static LocaleInfo getContext() {
    LocaleInfo contextLocaleInfo = contextLocaleInfoHolder
        .get();

    return (contextLocaleInfo == null) ? getDefault() : contextLocaleInfo;
  }

  public static LocaleInfo setContext(Locale locale) {
    LocaleInfo old = getContext();

    contextLocaleInfoHolder.set(new LocaleInfo(locale, null,
        defaultLocalInfo));

    return old;
  }

  public static LocaleInfo setContext(Locale locale, String charset) {
    LocaleInfo old = getContext();

    contextLocaleInfoHolder.set(new LocaleInfo(locale, charset,
        defaultLocalInfo));

    return old;
  }

  public static LocaleInfo setContext(LocaleInfo localeInfo) {
    if (localeInfo == null) {
      return setContext(null, null);
    } else {
      LocaleInfo old = getContext();

      contextLocaleInfoHolder.set(localeInfo);

      return old;
    }
  }

  public static void resetContext() {
    contextLocaleInfoHolder.set(null);
  }

  /**
   * Holds all per-Locale data. FIXME
   */
  static class LocaleData {

    LocaleData(Locale locale) {
      this.locale = locale;
    }

    final Locale locale;
    DateFormatSymbolsEx dateFormatSymbols;
    NumberFormat numberFormat;
  }

  protected static Map<String, LocaleData> locales = new HashMap<String, LocaleData>();

  /**
   * Lookups for locale data and creates new if it doesn't exist.
   */
  protected static LocaleData lookupLocaleData(String code) {
    LocaleData localeData = locales.get(code);
    if (localeData == null) {
      String[] data = decodeLocaleCode(code);
      localeData = new LocaleData(new Locale(data[0], data[1], data[2]));
      locales.put(code, localeData);
    }
    return localeData;
  }

  protected static LocaleData lookupLocaleData(Locale locale) {
    return lookupLocaleData(resolveLocaleCode(locale));
  }

  /**
   * Returns Locale from cache.
   */
  public static Locale getLocale(String language, String country,
      String variant) {
    LocaleData localeData = lookupLocaleData(resolveLocaleCode(language,
        country, variant));
    return localeData.locale;
  }

  /**
   * Returns Locale from cache.
   */
  public static Locale getLocale(String language, String country) {
    return getLocale(language, country, null);
  }

  /**
   * Returns Locale from cache where Locale may be specified also using language code. Converts a
   * locale string like "en", "en_US" or "en_US_win" to <b>new</b> Java locale object.
   */
  public static Locale getLocale(String languageCode) {
    LocaleData localeData = lookupLocaleData(languageCode);
    return localeData.locale;
  }

  /**
   * Transforms locale data to locale code. <code>null</code> values are allowed.
   */
  public static String resolveLocaleCode(String lang, String country,
      String variant) {
    StringBuilder code = new StringBuilder(lang);
    if (StringUtils.isNotEmpty(country)) {
      code.append('_').append(country);
      if (StringUtils.isNotEmpty(variant)) {
        code.append('_').append(variant);
      }
    }
    return code.toString();
  }

  /**
   * Resolves locale code from locale.
   */
  public static String resolveLocaleCode(Locale locale) {
    if (locale == null) {
      locale = Locale.getDefault();
    }
    return resolveLocaleCode(locale.getLanguage(), locale.getCountry(),
        locale.getVariant());
  }

  /**
   * Decodes locale code in string array that can be used for
   * <code>Locale</code> constructor.
   */
  public static String[] decodeLocaleCode(String localeCode) {
    if (localeCode == null) {
      return null;
    }
    String[] data = StringUtils.split(localeCode, '_');
    String result[] = new String[3];
    result[0] = data[0];
    result[1] = result[2] = "";
    if (data.length >= 2) {
      result[1] = data[1];
      if (data.length >= 3) {
        result[2] = data[2];
      }
    }
    return result;
  }

  /**
   * Returns <code>DateFormatSymbols</code> instance for specified locale.
   */
  public static DateFormatSymbolsEx getDateFormatSymbols(Locale locale) {
    LocaleData localeData = lookupLocaleData(locale);
    DateFormatSymbolsEx dfs = localeData.dateFormatSymbols;
    if (dfs == null) {
      dfs = new DateFormatSymbolsEx(locale);
      localeData.dateFormatSymbols = dfs;
    }
    return dfs;
  }

  /**
   * Returns <code>NumberFormat</code> instance for specified locale.
   */
  public static NumberFormat getNumberFormat(Locale locale) {
    LocaleData localeData = lookupLocaleData(locale);
    NumberFormat nf = localeData.numberFormat;
    if (nf == null) {
      nf = NumberFormat.getInstance(locale);
      localeData.numberFormat = nf;
    }
    return nf;
  }

}
