package com.xquark.utils.i18n;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

/**
 * @author: chenxi
 */

public final class LocaleInfo implements Cloneable, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 3257847675461251635L;

  private static final CharsetMap CHARSET_MAP = new CharsetMap();
  private Locale locale;
  private String charset;

  LocaleInfo() {
    this.locale = Locale.getDefault();
    this.charset = LocaleUtil.getCanonicalCharset(new OutputStreamWriter(
        new ByteArrayOutputStream()).getEncoding(), "ISO-8859-1");
  }

  public LocaleInfo(Locale locale) {
    this(locale, null);
  }

  public LocaleInfo(Locale locale, String charset) {
    this(locale, charset, LocaleUtil.getDefault());
  }

  LocaleInfo(Locale locale, String charset, LocaleInfo defaultLocaleInfo) {
    if (locale == null) {
      locale = defaultLocaleInfo.getLocale();

      if (StringUtils.isEmpty(charset)) {
        charset = defaultLocaleInfo.getCharset();
      }
    }

    if (StringUtils.isEmpty(charset)) {
      charset = CHARSET_MAP.getCharSet(locale);
    }

    this.locale = locale;
    this.charset = LocaleUtil.getCanonicalCharset(charset,
        defaultLocaleInfo.getCharset());
  }

  public Locale getLocale() {
    return locale;
  }

  public String getCharset() {
    return charset;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null || !(o instanceof LocaleInfo)) {
      return false;
    }

    if (o == this) {
      return true;
    }

    LocaleInfo otherLocaleInfo = (LocaleInfo) o;

    return locale.equals(otherLocaleInfo.locale)
        && charset.equals(otherLocaleInfo.charset);
  }

  @Override
  public int hashCode() {
    return locale.hashCode() ^ charset.hashCode();
  }

  @Override
  public Object clone() {
    try {
      return super.clone();
    } catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }

  @Override
  public String toString() {
    return locale + ":" + charset;
  }
}
