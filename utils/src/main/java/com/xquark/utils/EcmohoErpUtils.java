package com.xquark.utils;

import org.apache.commons.codec.binary.Base64;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

public class EcmohoErpUtils {


  public static Integer StringToTimestamp(String time) {

    int times = 0;
    try {
      times = (int) ((Timestamp.valueOf(time).getTime()) / 1000);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (times == 0) {
      System.out.println("String转10位时间戳失败");
    }
    return times;

  }

  public static String byte2String(byte[] buff) {
    StringBuffer sbuf = new StringBuffer();
    for (int i = 0; i < buff.length; i++) {
      int tmp = buff[i] & 0XFF;
      String str = Integer.toHexString(tmp);
      if (str.length() == 1) {
        sbuf.append("0" + str);
      } else {
        sbuf.append(str);
      }

    }
    return sbuf.toString();
  }

  public static String getSignMd5(String nick, String methodName,
      String date, String name, String orderid, String formats)
      throws NoSuchAlgorithmException {
    byte[] byteeNick = Base64.encodeBase64(nick.getBytes());
    byte[] byteMethod = Base64.encodeBase64(methodName.getBytes());
    byte[] byteDate = Base64.encodeBase64(date.toString().getBytes());
    byte[] byteName = Base64.encodeBase64(name.getBytes());
    byte[] byteOrder = Base64.encodeBase64(orderid.getBytes());
    byte[] byteFormats = Base64.encodeBase64(formats.getBytes());
    String signBefore = new String(byteeNick) + new String(byteMethod)
        + new String(byteDate) + new String(byteName)
        + new String(byteOrder) + new String(byteFormats);
    MessageDigest md5 = MessageDigest.getInstance("MD5");
    byte[] byteSigh = md5.digest(signBefore.getBytes());
    String signAfter = byte2String(byteSigh);
    return signAfter;
  }

  public static boolean getErpPostResult(String resultXml) {

    SAXBuilder builder = new SAXBuilder();
    StringReader stringReader = new StringReader(resultXml);

    InputSource inputSource = new InputSource(stringReader);
    Document document = null;
    try {
      document = builder.build(inputSource);

      Element root = document.getRootElement();
      String isSuccess = root.getChildText("isSuccess");
      System.out.println("isSuccess" + isSuccess);
      if (isSuccess != null) {
        if (isSuccess.equalsIgnoreCase("true")) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }

    } catch (JDOMException e) {
      e.printStackTrace();
      return false;
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }

  }

  public static void main(String[] args) {
    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<Response>\n" +
        "  <isSuccess>true</isSuccess>\n" +
        "  <errorCode></errorCode>\n" +
        "  <errorNumber></errorNumber>\n" +
        "  <errorMsg></errorMsg>\n" +
        "</Response>\n";
    boolean isSuccess = getErpPostResult(xml);
    System.out.println("getSuccess" + isSuccess);

  }
}
