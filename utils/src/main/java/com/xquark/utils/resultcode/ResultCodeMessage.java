package com.xquark.utils.resultcode;

/**
 * @auther chenxi
 */

public interface ResultCodeMessage {

  String getName();

  ResultCode getResultCode();

  String getMessage();

  int getCode();
}
