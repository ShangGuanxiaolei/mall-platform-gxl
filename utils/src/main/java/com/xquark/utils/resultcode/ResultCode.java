package com.xquark.utils.resultcode;


/**
 * @auther chenxi
 */

public interface ResultCode {

  String getName();

  ResultCodeMessage getMessage();

  int getCode();

  @Override
  String toString();
}
