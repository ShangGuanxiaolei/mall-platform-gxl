package com.xquark.utils.resultcode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xquark.utils.MessageUtil;
import com.xquark.utils.i18n.LocaleUtil;
import com.xquark.utils.resourcebundle.ResourceBundleConstant;
import com.xquark.utils.resourcebundle.ResourceBundleFactory;

/**
 * @auther chenxi
 */

public class ResultCodeUtil implements Serializable, ResourceBundleConstant {

  private static final long serialVersionUID = -1283084401300246727L;
  private final ResultCode resultCode;
  private transient Logger log;
  private transient Map<Locale, ResourceBundle> resourceBundles;

  public ResultCodeUtil(ResultCode resultCode) {
    this.resultCode = resultCode;

    if (!(resultCode instanceof Enum)) {
      throw new IllegalArgumentException(
          "ResultCode should be java.lang.Enum: "
              + getClass().getName());
    }
  }

  public String getName() {
    return Enum.class.cast(resultCode).name();
  }

  public int getCode() {
    return getMessage().getCode();
  }

  public ResultCodeMessage getMessage() {
    final ResourceBundle resourceBundle = getResourceBundle();

    return new ResultCodeMessage() {
      @Override
      public String getName() {
        return resultCode.getName();
      }

      @Override
      public ResultCode getResultCode() {
        return resultCode;
      }

      @Override
      public String getMessage() {
        return MessageUtil.getMessage(resourceBundle,
            resultCode.getName(), null);
      }

      @Override
      public int getCode() {
        String code = MessageUtil
            .getMessage(resourceBundle, resultCode.getName()
                + XPATH_RESOURCE_CODE_SUFFIX, null);
        return (code == null) ? 0 : Integer.valueOf(code);
      }

      @Override
      public String toString() {
        return getMessage();
      }
    };
  }

  public ResultCodeMessage getMessage(final Object... args) {
    final ResourceBundle resourceBundle = getResourceBundle();

    return new ResultCodeMessage() {
      @Override
      public String getName() {
        return resultCode.getName();
      }

      @Override
      public ResultCode getResultCode() {
        return resultCode;
      }

      @Override
      public String getMessage() {
        return MessageUtil.getMessage(resourceBundle,
            resultCode.getName(), args);
      }

      @Override
      public int getCode() {
        String code = MessageUtil
            .getMessage(resourceBundle, resultCode.getName()
                + XPATH_RESOURCE_CODE_SUFFIX, args);
        return (code == null) ? 0 : Integer.valueOf(code);
      }

      @Override
      public String toString() {
        return getMessage();
      }
    };
  }

  protected final Logger getLogger() {
    if (log == null) {
      log = LoggerFactory.getLogger(resultCode.getClass());
    }

    return log;
  }

  protected final synchronized ResourceBundle getResourceBundle() {
    if (resourceBundles == null) {
      resourceBundles = new HashMap<Locale, ResourceBundle>();
    }

    Locale contextLocale = LocaleUtil.getContext().getLocale();
    ResourceBundle resourceBundle = resourceBundles.get(contextLocale);

    if (resourceBundle == null) {
      Class<?> resultCodeClass = resultCode.getClass();

      do {
        String resourceBundleName = resultCodeClass.getName();

        getLogger()
            .debug("Trying to load resource bundle: "
                + resourceBundleName);

        try {
          resourceBundle = ResourceBundleFactory
              .getBundle(resourceBundleName);
        } catch (MissingResourceException e) {
          getLogger().debug(
              "Resource bundle not found: " + resourceBundleName);
          resultCodeClass = resultCodeClass.getSuperclass();
        }
      } while (resourceBundle == null && resultCodeClass != null);

      resourceBundles.put(contextLocale, resourceBundle);
    }

    return resourceBundle;
  }

}
