package com.xquark.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class UserAgentUtils {

  public static void main(String[] args) {
//		String str = "User-Agent: Mozilla/5.0 (Linux; U; Android 4.2.2; zh-cn; H30-T00 Build/HuaweiH30-T00) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 (xiangqu; Client/Android4.2.2 V/1986|1.9.86 channel/huaweiyysc)";
    //新ios
//		String str = "( Xiangqu; Client)[sdafs]ff[{  \"version\" : \"3.2.40\",  \"channel\" : \"appStore\",  \"deviceInfo\" : \"iphone 6 \",  \"userId\" : \"471977\",  \"platform\" : \"iOS\"}]";
    String str = "User-Agent: Mozilla/5.0 (Linux; Android 5.1.1; Find 5 Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/39.0.0.0 Mobile Safari/537.36 (xiangqu; Client/Android5.1.1 V/1990|1.9.90 channel/test UID/137987)";
    int index_ = str.lastIndexOf("(");
    int _index = str.lastIndexOf(")");
    String originUA = "";
    if (str.endsWith(")") && index_ >= 0 && _index >= 0) {
      originUA = str.substring(0, index_);
      String ua = str.substring(index_ + 1, _index);
      System.out.println(ua);
//			userAgent = JSON.parseObject(ua, UserAgent.class);
    } else {
//			originUA = uaTemp;
    }
//		userAgent.setOriginUA(originUA);
  }

  /**
   * 检测是否是app访问
   *
   * @param request 浏览器标识
   * @return true:app接入，false:其他接入
   * @Title: check
   * @Date : 2014-7-7 下午01:29:07
   */
  public static boolean isFromApp(HttpServletRequest request) {
    boolean ret = false;
    String userAgent = request.getHeader("User-Agent");
    if (StringUtils.isNotEmpty(userAgent)) {
      return StringUtils.contains(userAgent.toLowerCase(), "healthsource-b2b-app");
    }
    return ret;
  }


  public static String parseUA(String originUA, String startWith, String endWith) {
    String parseStr = null;
    if (StringUtils.isNotBlank(originUA)) {
      int index_ = originUA.lastIndexOf(startWith);
      int _index = originUA.lastIndexOf(endWith);
      if (originUA.endsWith(endWith) && index_ >= 0 && _index >= 0) {
        parseStr = originUA.substring(index_ + 1, _index);
      }
    }

    return parseStr;
  }

  public static UserAgent getUserAgent(HttpServletRequest request) {
    String userAgent = request.getHeader("User-Agent").trim();

    UserAgent ua = null;
    Logger log = LoggerFactory.getLogger(UserAgentUtils.class);
    String parseUA = parseUA(userAgent, "[", "]");
    if (StringUtils.isNotBlank(parseUA)) {
      try {
        ua = JSON.parseObject(userAgent, UserAgent.class);
      } catch (Exception e) {
        log.warn("userAgent error ", userAgent);
      }
    }

//		if(ua == null){
//			parseUA = parseUA(userAgent, "(", ")");
//			if(StringUtils.isNotBlank(parseUA)){
//				parseUA.ind
//			}
//		}

    return ObjectUtils.defaultIfNull(ua, new UserAgent());
  }
}
