package com.xquark.sms.mandao;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//该Demo主要解决Java在Linux等环境乱码的问题
public class MandaoSmsHelper {

  private static Logger log = LoggerFactory.getLogger(MandaoSmsHelper.class);

  /**
   * 漫道发送短信：传多个手机号就是群发，一个手机号就是单条提交
   */
  public static String sendSms(String serviceURL, String newtokenUrl, String id, String secretkey,
      String mobiles,
      String content, String taskNo, String signNo, Integer chanelTypeId)
      throws UnsupportedEncodingException {
    log.error("+++++++++++++++++++++ MandaoSmsHelper sendSms  ++++++++++++++++++++++++++++++");
    MandaoSmsClient client = new MandaoSmsClient(serviceURL, newtokenUrl, id, secretkey);
    //String content_utf8 = URLEncoder.encode(content, "utf-8");
    String result_mt = client.mdSmsSend(mobiles, content, taskNo, signNo, chanelTypeId);
		
		/*if (result_mt.startsWith("-") || result_mt.equals(""))// 发送短信，如果是以负号开头就是发送失败。
		{
			log.error("send sms fail, return value：" + result_mt + " please check webservice return value table!");
			return "";
		}*/

    // 发送成功：信息编号，如：-8485643440204283743或1485643440204283743，可根据返回值位数判断提交是否成功，
		/*if(result_mt.length() <= 10 || result_mt.length() >= 25){
			log.error("send sms fail, return value：" + result_mt + " please check webservice return value table!");
			return "";
		}*/
    // 欧电云输出返回标识，如果返回值success为true为提交成功。
    log.info("send sms success, return value：" + result_mt + ", mobiles: " + mobiles + ", content: "
        + content);
    return result_mt;
  }

  /**
   * 漫道发送短信：传多个手机号就是群发，一个手机号就是单条提交
   *
   * @param sn 序列号
   * @param pwd 密码
   * @param mobiles 手机号
   * @param content URL_UT8编码内容
   * @return true or false
   */
  public static String sendSms(String serviceURL, String newtokenUrl, String id, String secretkey,
      String mobiles,
      String content, String ext) throws UnsupportedEncodingException {
    return sendSms(serviceURL, newtokenUrl, id, secretkey, mobiles, content, ext, "", 0);
  }

  public static void main(String[] args) throws UnsupportedEncodingException {
    // 输入软件序列号和密码
//		String serviceURL = "http://sdk2.entinfo.cn:8060/webservice.asmx";
    String serviceURL = "http://odianyun.com/open-api/omc/sendTask.do";
    String newtokenUrl = "http://odianyun.com/open-api/newToken.do";
    String id = "910a5f620150908180814";
    String secretkey = "913f7ad32b3120150908180814";
    String mobiles = "18868875152";
    String content = "487561为手机注册验证码，请于1小时内完成验证";
    String taskNo = "MC9120150908191728";
    String signNo = "20150908144171";
    int chanelTypeId = 1;
    String tr = sendSms(serviceURL, newtokenUrl, id, secretkey, mobiles, content, taskNo, signNo,
        chanelTypeId);
    System.out.println(tr);
  }
}
