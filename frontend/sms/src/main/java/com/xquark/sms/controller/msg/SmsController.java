package com.xquark.sms.controller.msg;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.CollagePushMessageMapper;
import com.xquark.dal.mapper.CollageSmsMessageMapper;
import com.xquark.dal.model.CollagePushMessage;
import com.xquark.dal.model.CollageSmsMessage;
import com.xquark.dal.status.SmsMessageStatus;
import com.xquark.dal.status.SmsSendStatus;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.dal.vo.UserInfo;
import com.xquark.service.bill.impl.bw.FieldConstants;
import com.xquark.service.msg.SmsSendRecordService;
import com.xquark.service.msgcore.MessageReciver;
import com.xquark.service.msgcore.ReciverMessage;
import com.xquark.service.push.PushService;
import com.xquark.sms.ResponseObject;
import com.xquark.sms.mandao.WhiteListHelper;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Controller
public class SmsController {

  private Logger log = LoggerFactory.getLogger(getClass());

  //	@Value("${mandao.sms.notify.on-off}")
  //	private boolean smsNotifyOn;

  @Value("${ody.sms.id}")
  private String odySmsId;

  @Value("${ody.sms.secretkey}")
  private String odySmsSecretKey;

  @Value("${ody.sms.newtokenurl}")
  private String newtokenUrl;

  @Value("${mandao.sms.mainserviceurl}")
  private String serviceURL;

  @Value("${mandao.sms.whitelist}")
  private String whitelist;

  @Autowired
  private PushService pushService;

  @Autowired
  private SpringTemplateEngine templateEngine;

  @Autowired
  private SmsSendRecordService smsSendRecordService;

  @Autowired
  @Qualifier("eMaySmsSendHandler")
  private SmsHandler smsSendHandler;

  @Autowired
  private MessageReciver messageReciver;

//  @Autowired
//  private ReciverMessage reciverMessage;

  @Autowired
  private CollageSmsMessageMapper collageSmsMessageMapper;

  @Autowired
  private CollagePushMessageMapper collagePushMessageMapper;

  @Autowired
  private EMaySmsSendHandler eMaySmsSendHandler;

  @Autowired
  private RongLianSmsSendHandler rongLianSmsSendHandler;
  @ResponseBody
  @RequestMapping(value = "monitor")
  public ResponseObject<Boolean> smsServiceMonitor() {
    return new ResponseObject<Boolean>(true);
  }

  //	@ResponseBody
  //	@RequestMapping(value = "/smsTest")
  //	public ResponseObject<Boolean> smsTest(HttpServletRequest request,
  //			HttpServletResponse response) {
  //		String mobile = "18667181802";
  //		String content = "17767070359";
  //		// String mobile = (String) request.getParameter("mobile");
  //		// String content = (String) request.getParameter("content");
  //
  //		boolean result = pushService.sendSms(mobile, content);
  //		return new ResponseObject<Boolean>(result);
  //	}

  @RequestMapping("/mandaoSms/notify")
  public void mandaoSmsNotify(String args, HttpServletResponse response) {
    try {
      //			if(!smsNotifyOn){
      //				response.getWriter().print("0");
      //				return;
      //			}
      if (StringUtils.isNotBlank(args)) {
        String[] batchStrs = args.split(";");
        for (int i = 0; i < batchStrs.length; i++) {
          String batchStr = batchStrs[i];
          if (StringUtils.isNotBlank(batchStr)) {
            String[] msg = batchStr.split(",");
            String mobile = msg[2];
            String thirdBatchId = msg[3];
            SmsSendStatus status;
            if (msg[4].equals("0") || msg[4].equals("DELIVRD")) {
              status = SmsSendStatus.SUCCESS;
            } else {
              status = SmsSendStatus.FAIL;
            }
            String thirdResult = batchStr;
            smsSendRecordService.updateSendResult(mobile, thirdBatchId,
                status.toString(), thirdResult);
          }
        }
      } else {
        log.error("接受通知参数异常");
      }
      response.getWriter().print("0");
    } catch (Exception e) {
      log.error("漫道通知返回失败");
    }
  }

  /**
   * 以Json格式传入数据
   */
  @ResponseBody
  @RequestMapping(value = "/engine/send")
  public ResponseObject<Boolean> smsEngineService(HttpServletRequest request,
      HttpServletResponse response) {
    log.error("+++++++++++++++++++++++++ /engine/send ++++++++++++++++++++++++++++++++++");
    String appId = request.getParameter("appid");
    String mobile = request.getParameter("mobile");
    String template = request.getParameter("content");
    String data = request.getParameter("data");
    String ipAddress = WhiteListHelper.getIpAddr(request);
    String clientIP = request.getParameter("clientIP");
    log.error("+++++++++++++++++++++++++ ipAddress = " + ipAddress
        + " ++++++++++++++++++++++++++++++++++");
    log.error(
        "+++++++++++++++++++++++++ clientIP = " + clientIP + " ++++++++++++++++++++++++++++++++++");
    String content = "";

    IWebContext model1 = new WebContext(request, response, request.getServletContext());
    JSONObject json = JSONObject.parseObject(data);
    if (StringUtils.isNotBlank(data)) {
      Iterator<String> it = json.keySet().iterator();
      while (it.hasNext()) {
        String key = it.next();
        if (key.equals("order")) {
          JSONObject orderJson = json.getJSONObject(key);
          OrderVO order = JSON.toJavaObject(orderJson, OrderVO.class);
          model1.getRequestAttributes().put(key, order);
        } else if (key.equals("buyer")) {
          JSONObject buyerJson = json.getJSONObject(key);
          UserInfo buyer = JSON.toJavaObject(buyerJson, UserInfo.class);
          model1.getRequestAttributes().put(key, buyer);
        } else if (key.equals("seller")) {
          JSONObject sellerJson = json.getJSONObject(key);
          SellerInfoVO seller = JSON.toJavaObject(sellerJson, SellerInfoVO.class);
          model1.getRequestAttributes().put(key, seller);
        } else {
          String str = json.getString(key);
          model1.getRequestAttributes().put(key, str);
        }
      }
      content = templateEngine.process(template, model1).trim();
    } else {
      content = template;
    }

    log.error(
        "+++++++++++++++++++++++++ ready smsSendHandler.doSend ++++++++++++++++++++++++++++++++++");
    return new ResponseObject<Boolean>(smsSendHandler.doSend(appId, mobile, content, ipAddress,
        clientIP));
  }

  // "http://10.8.100.109:8080/sms/send?mobile=18667181802&content=test1&appid=1";
  @ResponseBody
  @RequestMapping(value = "/send")
  public ResponseObject<Boolean> smsService(HttpServletRequest request,
      HttpServletResponse response) {
    String mobile = (String) request.getParameter("mobile");
    String content = (String) request.getParameter("content");
    String appId = (String) request.getParameter("appid");
    String ipAddress = WhiteListHelper.getIpAddr(request);
    String clientIP = (String) request.getParameter("clientIP");
    log.info("send content: " + content + " to mobile: " + mobile + " appid: " + appId
        + ", sms request ip:" + ipAddress);
    return new ResponseObject<Boolean>(smsSendHandler.doSend(appId, mobile, content, ipAddress,
        clientIP));
  }

  @ResponseBody
  @RequestMapping(value = "/send2")
  public ResponseObject<Boolean> smsMessage() {
    CollagePushMessage collagePushMessage = new CollagePushMessage();
    collagePushMessage.setTitle("测试");
    collagePushMessage.setContent("push测试push测试push测试push测试push测试push测试push测试");
    collagePushMessage.setBelognTo("1022038");
    collagePushMessage.setStatus(0);
    collagePushMessage.setTypeMsg("拼团成功");
    CollageSmsMessage collageSmsMessage = new CollageSmsMessage();
    collageSmsMessage.setMobile("13662691715");
    collageSmsMessage.setContent("短信测试");
      ReciverMessage reciverMessage = new ReciverMessage();
      reciverMessage.setCollageSmsMessage(collageSmsMessage);
      reciverMessage.setCollagePushMessage(collagePushMessage);
//      reciverMessage.setType(FieldConstants.SMS_CHANNEL);
//      reciverMessage.setType(FieldConstants.PUSH_CHANNEL);
    boolean b = messageReciver.reciverMsg(reciverMessage);
    return new ResponseObject<Boolean>(b);
  }

  @ResponseBody
  @RequestMapping(value = "/send3")
  public ResponseObject<Boolean> push(int id) {
//    CollagePushMessage collagePushMessage = new CollagePushMessage();
//    collagePushMessage.setTitle("测试");
//    collagePushMessage.setContent("手机测试");
//    collagePushMessageMapper.insert(collagePushMessage);
//    JSONObject json = new JSONObject();
//    json.put("type", FieldConstants.SMS_CHANNEL);
//    json.put("collagePushMessage",collagePushMessage);
//
//    boolean b = messageReciver.reciverMsg(json);
    CollageSmsMessage smsMsg = collageSmsMessageMapper.findById(id);
    boolean b = eMaySmsSendHandler.doSend("", smsMsg.getMobile(), smsMsg.getContent(), "", "");
//    System.out.println(smsMsg);
//    return new ResponseObject<CollageSmsMessage>(smsMsg);

    return new ResponseObject<Boolean>(b);
  }



}
