package com.xquark.sms.controller.msg;

import cn.emay.channel.httpclient.SDKHttpClient;
import com.google.common.base.Optional;
import java.net.URLEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 18-3-8. DESC:
 */
@Component("eMaySmsSendHandler")
public class EMaySmsSendHandler extends SmsHandler {

  private final static String CD_KEY = "8SDK-EMY-6699-SDZQP";

  private final static String PASS_WORD = "908068";

  private final static String ADD_SERIAL = "";

  private final static String SEQ_ID = "1495855723953";

  private final static String SMS_PRIORITY = "1";

  private static final String BASE_URL = "http://hprpt2.eucp.b2m.cn:8080/sdkproxy/sendsms.action";

  private static final String BASE_PARAM = "cdkey=" + CD_KEY + "&password=" + PASS_WORD
      + "&addserial=" + ADD_SERIAL + "&seqid=" + SEQ_ID + "&smspriority=" + SMS_PRIORITY;


  @Override
  public boolean doSend(String appId, String mobile, String content, String ipAddress,
      String clientIP) {
    String result = null;
    try {
      content = URLEncoder.encode("【汉薇商城】" + content, "UTF-8");
      String param = BASE_PARAM + "&phone=" + mobile + "&message=" + content;
      logger.info("+++++++++++++++++++++ ready sendSms  ++++++++++++++++++++++");
      result = SDKHttpClient.sendSMS(BASE_URL, param);
    } catch (Exception e) {
      logger.error("fail send sms", e);
    } finally {
      insertSmsSendRecord(mobile, content, CD_KEY,
          Optional.fromNullable(result).or("error"));
    }
    if (result != null && result.equals("0")) {
      logger.info("result send success, result: " + result);
      return true;
    }
    logger.info("result send failed, result: " + result);
    return false;
  }

  public boolean pieceDoSend(String appId, String mobile, String content, String ipAddress,
                        String clientIP) {
    String result = null;
    try {
      content = URLEncoder.encode("【汉薇商城】" + content, "UTF-8");
      String param = BASE_PARAM + "&phone=" + mobile + "&message=" + content;
      logger.info("+++++++++++++++++++++ ready sendSms  ++++++++++++++++++++++");
      result = SDKHttpClient.sendSMS(BASE_URL, param);
    } catch (Exception e) {
      logger.error("fail send sms", e);
    }
    if (result != null && result.equals("0")) {
      logger.info("result send success, result: " + result);
      return true;
    }
    logger.info("result send failed, result: " + result);
    return false;
  }



  public static void main(String[] args) {
    new EMaySmsSendHandler().doSend("", "18696153096", "测试", "", "");
  }

}
