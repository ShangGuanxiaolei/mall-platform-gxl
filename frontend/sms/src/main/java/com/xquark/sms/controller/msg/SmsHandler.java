package com.xquark.sms.controller.msg;

import com.xquark.dal.model.SmsSendRecord;
import com.xquark.dal.status.SmsSendStatus;
import com.xquark.service.msg.SmsSendRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by wangxinhua on 18-3-8. DESC:
 */
public abstract class SmsHandler {

  Logger logger = LoggerFactory.getLogger(this.getClass());

  private SmsSendRecordService smsSendRecordService;

  @Autowired
  public void setSmsSendRecordService(SmsSendRecordService smsSendRecordService) {
    this.smsSendRecordService = smsSendRecordService;
  }

  public abstract boolean doSend(String appId, String mobile, String content, String ipAddress,
      String clientIP);

  /**
   * 记录日志
   */
  void insertSmsSendRecord(String mobile, String content, String appid,
      String mandaoBatchId) {
    SmsSendRecord record = new SmsSendRecord();
    record.setAppId(appid);
    record.setMobile(mobile);
    record.setContent(content);
    record.setThirdBatchId(mandaoBatchId);
    record.setStatus("".equals(mandaoBatchId) ? SmsSendStatus.FAIL : SmsSendStatus.PENDING);
    smsSendRecordService.insert(record);
  }

}
