package com.xquark.sms.aop;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class SmsSendRateAop {

  Logger logger = LoggerFactory.getLogger(SmsSendRateAop.class);

  private static String SMSLIMIT_PHONE_PRE = "smsLimitPhone_%s";
  private static String SMSLIMIT_IP_PRE = "smsLimitIp_%s";

  @Value("${sms.limit.phone.num}")
  private long phoneLimitNum = 10;

  @Value("${sms.limit.ip.num}")
  private long ipLimitNum = 999999999;

  @Value("${sms.limit.timeout}")
  private int limitTimeout = 300;

  @Pointcut(value = "execution (* com.xquark.sms.controller.msg.SmsHandler.doSend(..)) && args(appId,mobile,content,ipAddress,clientIP)", argNames = "appId,mobile,content,ipAddress,clientIP")
  public void pointcut_doSend(String appId, String mobile, String content, String ipAddress,
      String clientIP) {
  }

  // 短信流量控制
  @Around(value = "pointcut_doSend(appId,mobile,content,ipAddress,clientIP)", argNames = "point,appId,mobile,content,ipAddress,clientIP")
  public Object sendSmsLimitAround(ProceedingJoinPoint point, String appId, String mobile,
      String content, String ipAddress, String clientIP) throws Throwable {
    if (StringUtils.isEmpty(mobile)) {
      return false;
    }

    String key = String.format(SMSLIMIT_PHONE_PRE, mobile);

    try {
//      long result = memcachedClient.incr(key, 1, 1, memcachedClient.getOpTimeout(), limitTimeout);
      long result = 0;
      if (result > phoneLimitNum) {
        logger.error("SmsSendLimitPhone {},{},{},{},{}", appId, mobile, content, ipAddress,
            clientIP);
        // TODO: need better flow control,disable it and control in web page for now
        // return false;
      }
      if (StringUtils.isNotEmpty(clientIP)) {
        key = String.format(SMSLIMIT_IP_PRE, clientIP);
//        result = memcachedClient.incr(key, 1, 1, memcachedClient.getOpTimeout(), limitTimeout);
        result = 1;
        if (result > ipLimitNum) {
          logger.error("SmsSendLimitIp {},{},{},{},{}", appId, mobile, content,
              ipAddress, clientIP);
          return false;
        }
      }
    } catch (Exception ex) {
      logger.error("Error to add to Memcached", ex);
    }

    return point.proceed();
  }
}
