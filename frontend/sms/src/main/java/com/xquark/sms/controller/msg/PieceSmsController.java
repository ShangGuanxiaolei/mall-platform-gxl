package com.xquark.sms.controller.msg;

import com.xquark.dal.mapper.CollageSmsMessageMapper;
import com.xquark.dal.model.CollageSmsMessage;
import com.xquark.dal.status.SmsMessageStatus;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.sms.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 拼团发送短信
 *
 * @author tanggb
 * Create in 9:47 2018/9/20
 */
@RestController
public class PieceSmsController {
    @Autowired
    private CollageSmsMessageMapper collageSmsMessageMapper;

    @Autowired
    private EMaySmsSendHandler eMaySmsSendHandler;

    @ResponseBody
    @RequestMapping(value="/pieceSmsSend", method = RequestMethod.GET)
    public ResponseObject<Boolean> pieceSmsSend(long id){
        CollageSmsMessage smsMsg = collageSmsMessageMapper.findById(id);
        if(smsMsg == null){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "短信信息不存在");
        }
        Boolean aBoolean = send(id, smsMsg.getMobile(), smsMsg.getContent());
        return new ResponseObject<Boolean>(aBoolean);
    }

    /**
     * 发送短信
     */
    private Boolean send(long id,String mobile,String content) {
        int count = 0;
        CollageSmsMessage collageSmsMessage = new CollageSmsMessage();
        collageSmsMessage.setIdRaw(id);
        while (true){
            //1.发送短信
            boolean b = eMaySmsSendHandler.pieceDoSend("", mobile, content, "", "");

            //验证是否发送失败5次
            if(count >= 5){
                collageSmsMessage.setPushStatus(SmsMessageStatus.FAIL);
                collageSmsMessage.setRecount(count);
                collageSmsMessageMapper.reUpdate(collageSmsMessage);
                return false;
            }
            //发送成功直接保存状态
            if(b){
                collageSmsMessage.setPushStatus(SmsMessageStatus.SUCCESS);
                collageSmsMessageMapper.reUpdate(collageSmsMessage);
                return true;
            }else {
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count ++;
            }
        }
    }
}
