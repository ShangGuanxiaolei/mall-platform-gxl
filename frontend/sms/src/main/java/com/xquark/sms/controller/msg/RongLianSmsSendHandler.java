package com.xquark.sms.controller.msg;

import com.xquark.dal.util.SpringContextUtil;
import com.xquark.listener.SmsServerSwitchListener;
import com.xquark.sms.mandao.MandaoSmsHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RongLianSmsSendHandler extends SmsHandler {

  @Value("${ody.sms.id}")
  private String odySmsId;
  @Value("${ody.sms.secretkey}")
  private String odySmsSecretKey;
  @Value("${mandao.sms.whitelist}")
  private String whitelist;
  @Value("${mandao.sms.mainserviceurl}")
  private String serviceURL;
  @Value("${ody.sms.newtokenurl}")
  private String newtokenUrl;

  @Value("${ody.sms.task.no}")
  private String odyTaskNo;
  @Value("${ody.sms.signatrue.no}")
  private String odySignNo;
  @Value("${ody.sms.chaneltype.id}")
  private int odyChanelTypeId;

  /**
   * 发送短信 aop流控,调整参数需要更改aop设置com.xquark.sms.aop.SmsSendRateAop
   */
  @Override
  public boolean doSend(String appId, String mobile, String content, String ipAddress,
      String clientIP) {
    String mandaoBatchId = "";
    logger.error("+++++++++++++++++++++ doSend ++++++++++++++++++++++++++++++");
    if (StringUtils.isEmpty(ipAddress)) {
      logger.error("ipAddress is null, so send fail!");
      return false;
    }
    //if (!WhiteListHelper.isLegalRequest(ipAddress, whitelist)) {
    //	logger.error("ipAddress " + ipAddress + " is not in white list [" + whitelist + "], so send fail!");
    //	return false;
    //}

    if (StringUtils.isEmpty(appId)) {
      logger.error("appid:" + appId + ", so send fail!");
      return false;
    }

    String smsSign = getSignByAppId(appId);
    if (StringUtils.isEmpty(smsSign)) {
      return false;
    }
//		content += smsSign;
    try {
      logger
          .error("+++++++++++++++++++++ smsSign =  " + smsSign + " ++++++++++++++++++++++++++++++");
      Object bean = SpringContextUtil.getBean("smsServerSwitchListener");
      if (bean instanceof SmsServerSwitchListener) {
        SmsServerSwitchListener listener = (SmsServerSwitchListener) bean;
        serviceURL = listener.getServiceURL();
      }
			/*String ext = "";
			if ("2".equals(appId)) {
				ext = "1";
			}
			if ("3".equals(appId)) {
				ext = "2";
			}
			if ("4".equals(appId)) {
				ext = "3";
			}
			if ("5".equals(appId)) {
				ext = "4";
			}
			mandaoBatchId = MandaoSmsHelper.sendSms(serviceURL, mandaoSmsSn, mandaoSmsPwd, mobile,
					content, ext);
			mandaoBatchId = "111";*/
      logger.error(
          "+++++++++++++++++++++ ready MandaoSmsHelper.sendSms  ++++++++++++++++++++++++++++++");
      mandaoBatchId = MandaoSmsHelper
          .sendSms(serviceURL, newtokenUrl, odySmsId, odySmsSecretKey, mobile,
              content, odyTaskNo, odySignNo, odyChanelTypeId);
    } catch (Exception e) {
      logger.error("fail send sms", e);
      mandaoBatchId = "";
    }
    if (mandaoBatchId.equals("") || !"ok".equals(mandaoBatchId)) {
      return false;
    }
    content += smsSign;
    insertSmsSendRecord(mobile, content, appId, mandaoBatchId);

    return true;
  }

  private String getSignByAppId(String appid) {
    String sign = null;
    Integer appCode = new Integer(0);
    try {
      appCode = Integer.valueOf(appid);
      switch (appCode) {
        case 1:
          sign = "【XX】";
          break;
        case 2:
          sign = "【2】";
          break;
        case 3:
          sign = "【3】";
          break;
        case 4:
          sign = "【4】";
          break;
        case 5:
          sign = "【5】";
          break;
        default:
          break;
      }
    } catch (Exception e) {
      logger.error("appid param error, maybe not in 1 2 3");
    }
    return sign;
  }
}
