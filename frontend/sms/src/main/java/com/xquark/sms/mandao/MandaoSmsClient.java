package com.xquark.sms.mandao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Set;

public class MandaoSmsClient {

  private Logger log = LoggerFactory.getLogger(MandaoSmsClient.class);

  private String serviceURL = "http://odianyun.com/open-api/omc/sendTask.do";
  private String newtokenUrl = "http://odianyun.com/open-api/newToken.do";

  private boolean flag = false;
  private String id = "";
  private String secretKey = "";
  private String pwd = "";
  private String newToken = ""; // 新token
  private String originalNO = ""; // 消息原始发送记录编号(随机)

  public MandaoSmsClient(String serviceURL, String newtokenUrl, String id, String secretKey)
      throws UnsupportedEncodingException {
    this.serviceURL = serviceURL;
    this.newtokenUrl = newtokenUrl;
    this.id = id;
    this.secretKey = secretKey;
    this.pwd = this.getMD5(id + secretKey);
  }

  public String getMD5(String sourceStr) throws UnsupportedEncodingException {
    String resultStr = "";
    try {
      byte[] temp = sourceStr.getBytes();
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      md5.update(temp);
      byte[] b = md5.digest();
      for (int i = 0; i < b.length; i++) {
        char[] digit = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] ob = new char[2];
        ob[0] = digit[(b[i] >>> 4) & 0X0F];
        ob[1] = digit[b[i] & 0X0F];
        resultStr += new String(ob);
      }
      return resultStr;
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return null;
    }
  }

  public String mdSmsSend(String mobile, String content, String taskNo,
      String signNo, Integer chanelTypeId) {

    log.error("+++++++++++++++++++++ true mdSmsSend  ++++++++++++++++++++++++++++++");

    String mobile_callbak = mobile;
    String content_callbak = content;
    String taskNo_callbak = taskNo;
    String signNo_callbak = signNo;
    Integer chanelTypeId_callbak = chanelTypeId;

    HashMap<String, Object> result = null;

    //初始化SDK
    CCPRestSmsSDK restAPI = new CCPRestSmsSDK();

    //******************************注释*********************************************
    //*初始化服务器地址和端口                                                       *
    //*沙盒环境（用于应用开发调试）：restAPI.init("sandboxapp.cloopen.com", "8883");*
    //*生产环境（用户应用上线使用）：restAPI.init("app.cloopen.com", "8883");       *
    //*******************************************************************************
    restAPI.init("app.cloopen.com", "8883");

    //******************************注释*********************************************
    //*初始化主帐号和主帐号令牌,对应官网开发者主账号下的ACCOUNT SID和AUTH TOKEN     *
    //*ACOUNT SID和AUTH TOKEN在登陆官网后，在“应用-管理控制台”中查看开发者主账号获取*
    //*参数顺序：第一个参数是ACOUNT SID，第二个参数是AUTH TOKEN。                   *
    //*******************************************************************************
    restAPI.setAccount("8a216da858bd4e200158bec50a7201f0", "f1108d2eb6644795b92df0c14728854c");

    //******************************注释*********************************************
    //*初始化应用ID                                                                 *
    //*测试开发可使用“测试Demo”的APP ID，正式上线需要使用自己创建的应用的App ID     *
    //*应用ID的获取：登陆官网，在“应用-应用列表”，点击应用名称，看应用详情获取APP ID*
    //*******************************************************************************
    restAPI.setAppId("8aaf07085e2d97fd015e3666bb2d0326");

    //******************************注释****************************************************************
    //*调用发送模板短信的接口发送短信                                                                  *
    //*参数顺序说明：                                                                                  *
    //*第一个参数:是要发送的手机号码，可以用逗号分隔，一次最多支持100个手机号                          *
    //*第二个参数:是模板ID，在平台上创建的短信模板的ID值；测试的时候可以使用系统的默认模板，id为1。    *
    //*系统默认模板的内容为“【云通讯】您使用的是云通讯短信模板，您的验证码是{1}，请于{2}分钟内正确输入”*
    //*第三个参数是要替换的内容数组。																														       *
    //**************************************************************************************************

    //**************************************举例说明***********************************************************************
    //*假设您用测试Demo的APP ID，则需使用默认模板ID 1，发送手机号是13800000000，传入参数为6532和5，则调用方式为           *
    //*result = restAPI.sendTemplateSMS("13800000000","1" ,new String[]{"6532","5"});																		  *
    //*则13800000000手机号收到的短信内容是：【云通讯】您使用的是云通讯短信模板，您的验证码是6532，请于5分钟内正确输入     *
    //*********************************************************************************************************************
    result = restAPI
        .sendTemplateSMS(mobile, "201843", new String[]{content.substring(0, 6), "30分钟"});
    log.error("SDKTestGetSubAccounts result=" + result);
    System.out.println("SDKTestGetSubAccounts result=" + result);
    if ("000000".equals(result.get("statusCode"))) {
      //正常返回输出data包体信息（map）
      HashMap<String, Object> data = (HashMap<String, Object>) result.get("data");
      Set<String> keySet = data.keySet();
      for (String key : keySet) {
        Object object = data.get(key);
        System.out.println(key + " = " + object);
      }
      return "ok";
    } else {
      //异常返回输出错误码和错误信息
      return "错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg");
    }

    /**String result = "";
     McSendTaskApiDto sendTaskApiDto = new McSendTaskApiDto();
     sendTaskApiDto.setTaskNo(taskNo);//任务编号
     sendTaskApiDto.setChanelTypeId(chanelTypeId);//任务类型
     sendTaskApiDto.setSignatureNo(signNo);//签名
     sendTaskApiDto.setOriginalID(new String[]{originalNO = originalNO()});
     sendTaskApiDto.setMobiles(new String[]{mobile});

     Map<String, String> contentMap = new HashMap<String, String>();
     contentMap.put("content", content);
     sendTaskApiDto.setContentVar(contentMap);

     try {
     log.info("消息原始发送记录编号：originalNO=" + originalNO);
     // 验证ntoken是否获取过，没有则保存到缓存，如果存在则在缓存中取值
     if (StringUtils.isBlank(newToken)){
     newToken = sendGetRequest(newtokenUrl + "?appId=" + id
     + "&appSecret=" + secretKey);
     }
     HttpPost httpPost = new HttpPost(serviceURL);
     DefaultHttpClient httpclient = new DefaultHttpClient();
     List<NameValuePair> params = new ArrayList<NameValuePair>();
     params.add(new BasicNameValuePair("token", newToken));
     params.add(new BasicNameValuePair("sendTaskApiDto", JSONObject.toJSONString(sendTaskApiDto)+""));
     httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
     HttpResponse response = httpclient.execute(httpPost);
     HttpEntity entity = response.getEntity();
     result = EntityUtils.toString(entity);

     JSONObject reslutJson = (JSONObject) JSON.parse(result);
     log.info("欧电云短信服务返回结果：result=" + reslutJson.toJSONString());
     if (reslutJson.get("success").equals(true)){
     result = reslutJson.get("success") + "";
     } else {
     // 当token失效后则重新获取，并重新发送短信请求
     if ("5".equals(reslutJson.get("errorCode"))  && !flag){
     flag = true; // 设置获取token次数开关
     newToken = sendGetRequest(newtokenUrl + "?appId=" + id
     + "&appSecret=" + secretKey);
     mdSmsSend(mobile_callbak, content_callbak, taskNo_callbak, signNo_callbak, chanelTypeId_callbak);
     } else {
     result = reslutJson.get("errorCode") + "";
     }
     }

     return result;
     }catch(Exception e){
     log.error("call oudianyun service failed!", e);
     return result;
     }**/

  }

  /**
   * Get请求
   */
  public String sendGetRequest(String url) {
    try {
      URL getUrl = new URL(url);
      URLConnection connection = getUrl.openConnection();
      HttpURLConnection httpconn = (HttpURLConnection) connection;
      connection.connect();

      InputStreamReader isr = new InputStreamReader(
          httpconn.getInputStream());
      BufferedReader in = new BufferedReader(isr);
      String inputLine;
      while (null != (inputLine = in.readLine())) {
        JSONObject reslutJson = (JSONObject) JSON.parse(inputLine);
        log.info("获取欧电云token响应结果：result=" + reslutJson.toJSONString());
        if (reslutJson.get("success").equals(true)) {
          String data = reslutJson.get("data") + "";
          if (StringUtils.isNotBlank(data)) {
            JSONObject dataJson = (JSONObject) JSON.parse(data);
            return dataJson.get("appToken") + "";
          }
        }
      }
      in.close();
      httpconn.disconnect();
    } catch (Exception e) {
      log.error("获取欧电云token失败!url=" + url, e);
    }
    return "";

  }

  /**
   * 获取随机数
   */
  private String originalNO() {
    try {
      return this.getMD5(UniqueNoUtils.next(UniqueNoType.CID));
    } catch (UnsupportedEncodingException e) {
      log.error("随机数生成失败", e);
      return System.currentTimeMillis() + "";
    }
  }

}
