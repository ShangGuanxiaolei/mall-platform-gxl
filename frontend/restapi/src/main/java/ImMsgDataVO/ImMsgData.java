package ImMsgDataVO;

public class ImMsgData {

  private String fromId;
  private Integer msgLen;
  private String msgData;
  private Integer msgType;
  private Long iMsgTime;
  private String sMsgTime;

  public String getFromId() {
    return fromId;
  }

  public void setFromId(String fromId) {
    this.fromId = fromId;
  }

  public Integer getMsgLen() {
    return msgLen;
  }

  public void setMsgLen(Integer msgLen) {
    this.msgLen = msgLen;
  }

  public String getMsgData() {
    return msgData;
  }

  public void setMsgData(String msgData) {
    this.msgData = msgData;
  }

  public Integer getMsgType() {
    return msgType;
  }

  public void setMsgType(Integer msgType) {
    this.msgType = msgType;
  }

  public Long getiMsgTime() {
    return iMsgTime;
  }

  public void setiMsgTime(Long iMsgTime) {
    this.iMsgTime = iMsgTime;
  }

  public String getsMsgTime() {
    return sMsgTime;
  }

  public void setsMsgTime(String sMsgTime) {
    this.sMsgTime = sMsgTime;
  }

}
