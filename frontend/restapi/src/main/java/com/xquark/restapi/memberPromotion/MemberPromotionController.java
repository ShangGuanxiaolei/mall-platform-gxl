package com.xquark.restapi.memberPromotion;

import com.xquark.dal.model.MemberPromotion;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.memberPromotion.MemberPromotionService;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 18-3-14. DESC:
 */
@RestController
@RequestMapping("/memberPromotion")
@SuppressWarnings("unused")
public class MemberPromotionController {

  private final MemberPromotionService memberPromotionService;

  @Autowired
  public MemberPromotionController(MemberPromotionService memberPromotionService) {
    this.memberPromotionService = memberPromotionService;
  }

  /**
   * 新增 / 修改会员活动
   */
  @RequestMapping("/save")
  public ResponseObject<Boolean> save(MemberPromotionForm form) {
    MemberPromotion mp = Transformer.fromBean(form, MemberPromotion.class);
    String id = mp.getId();
    Boolean result;
    if (StringUtils.isBlank(id)) {
      result = memberPromotionService.save(mp);
    } else {
      result = memberPromotionService.update(mp);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 查询
   */
  @RequestMapping("/view")
  public ResponseObject<MemberPromotion> view(String id) {
    MemberPromotion mp = memberPromotionService
        .load(id);
    if (mp == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "查询的会员活动不存在");
    }
    return new ResponseObject<>(mp);
  }

  /**
   * 删除会员活动
   */
  @RequestMapping("/delete")
  public ResponseObject<Boolean> delete(String id) {
    Boolean result = memberPromotionService.delete(id);
    return new ResponseObject<>(result);
  }

  /**
   * 会员活动列表
   */
  @RequestMapping("/list")
  public ResponseObject<Map<String, ?>> list(Pageable pageable) {
    Map<String, ?> result = memberPromotionService.list(pageable);
    return new ResponseObject<Map<String, ?>>(result);
  }

}
