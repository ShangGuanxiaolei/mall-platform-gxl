package com.xquark.restapi.product;

import com.xquark.dal.model.Promotion;

/**
 * Created by wangxinhua on 17-11-27. DESC:
 */
public class ProductPromotionVOEX {

  private final Promotion promotion;

  private final ProductVOEx product;

  public ProductPromotionVOEX(Promotion promotion, ProductVOEx product) {
    this.promotion = promotion;
    this.product = product;
  }

  public Promotion getPromotion() {
    return promotion;
  }

  public ProductVOEx getProduct() {
    return product;
  }
}
