//package com.xquark.restapi.order;
//
//import com.alibaba.fastjson.JSONObject;
//import com.google.common.base.Function;
//import com.google.common.base.Optional;
//import com.mangofactory.swagger.annotations.ApiIgnore;
//import com.wordnik.swagger.annotations.Api;
//import com.wordnik.swagger.annotations.ApiOperation;
//import com.xquark.biz.controller.ControllerHelper;
//import com.xquark.biz.res.ResourceFacade;
//import com.xquark.biz.url.UrlHelper;
//import com.xquark.dal.mapper.*;
//import com.xquark.dal.model.*;
//import com.xquark.dal.model.mypiece.PromotionRewardsVo;
//import com.xquark.dal.mybatis.IdTypeHandler;
//import com.xquark.dal.status.OrderRefundStatus;
//import com.xquark.dal.status.OrderStatus;
//import com.xquark.dal.type.*;
//import com.xquark.dal.validation.group.bill.ElectronicGroup;
//import com.xquark.dal.validation.group.bill.NormalGroup;
//import com.xquark.dal.vo.*;
//import com.xquark.dal.voex.OrderVOEx;
//import com.xquark.interceptor.Token;
//import com.xquark.openapi.product.form.OrderSumbitFormApi;
//import com.xquark.openapi.product.form.OrderSumbitFormApi.BillFormType;
//import com.xquark.openapi.vo.OrderVOApi;
//import com.xquark.restapi.BaseController;
//import com.xquark.restapi.ResponseObject;
//import com.xquark.restapi.utils.ValidationUtils;
//import com.xquark.restapi.vo.Json;
//import com.xquark.service.Comment.CommentService;
//import com.xquark.service.address.AddressService;
//import com.xquark.service.address.AddressVO;
//import com.xquark.service.agent.UserAgentService;
//import com.xquark.service.antifake.AntifakeService;
//import com.xquark.service.apiVisitorLog.ApiVisitorLogService;
//import com.xquark.service.auditRule.AuditRuleService;
//import com.xquark.service.bill.BillService;
//import com.xquark.service.cart.CartService;
//import com.xquark.service.cart.vo.CartItemVO;
//import com.xquark.service.cashier.CashierService;
//import com.xquark.service.error.BizException;
//import com.xquark.service.error.GlobalErrorCode;
//import com.xquark.service.groupon.ActivityGrouponService;
//import com.xquark.service.logistics.LogisticsGoodsService;
//import com.xquark.service.mypiece.PromotionRewardsService;
//import com.xquark.service.order.*;
//import com.xquark.service.orderHeader.OrderHeaderService;
//import com.xquark.service.outpay.OutPayAgreementService;
//import com.xquark.service.pintuan.RecordPromotionPgDetail;
//import com.xquark.service.pricing.PromotionService;
//import com.xquark.service.pricing.base.PromotionActivityService;
//import com.xquark.service.pricing.vo.CartPromotionResultVO;
//import com.xquark.service.pricing.vo.PricingResultVO;
//import com.xquark.service.pricing.vo.UserSelectedProVO;
//import com.xquark.service.privilege.PrivilegeCodeService;
//import com.xquark.service.product.ProductService;
//import com.xquark.service.product.vo.ProductVO;
//import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
//import com.xquark.service.shop.ShopService;
//import com.xquark.service.shopTree.ShopTreeService;
//import com.xquark.service.systemRegion.SystemRegionService;
//import com.xquark.service.tinyurl.TinyUrlService;
//import com.xquark.service.union.UnionService;
//import com.xquark.service.user.RolePriceService;
//import com.xquark.service.user.UserService;
//import com.xquark.service.vo.LogisticsStock;
//import com.xquark.service.vo.LogisticsStockParam;
//import com.xquark.service.vo.SubOrder;
//import com.xquark.service.zone.ZoneService;
//import org.apache.commons.collections.CollectionUtils;
//import org.apache.commons.lang3.ObjectUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.lang3.math.NumberUtils;
//import org.apache.commons.lang3.time.DateUtils;
//import org.hibernate.validator.constraints.NotBlank;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.communityDetailController;
//import org.springframework.validation.Errors;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.ConstraintViolation;
//import javax.validation.Valid;
//import javax.validation.Validation;
//import javax.validation.Validator;
//import javax.validation.constraints.NotNull;
//import java.math.BigDecimal;
//import java.text.ParseException;
//import java.util.*;
//import java.util.Map.Entry;
//
//@communityDetailController
//@Api(value = "order2", description = "订单管理")
//public class PieceGroupOrderController extends OrderController {
//
//
//  @Autowired
//  private PromotionService promotionService;
//
//  @Autowired
//  private CartService cartService;
//
//  @Autowired
//  private ShopService shopService;
//
//  @Autowired
//  private FlashSalePromotionProductService flashSalePromotionProductService;
//
//  @Value("${tech.serviceFee.standard}")
//  private String serviceFeethreshold;
//
//  @Value("${order.delaysign.date}")
//  private int defDelayDate;
//
//
//}