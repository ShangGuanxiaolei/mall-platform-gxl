package com.xquark.restapi.waidan.vo;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/5/29
 * Time: 17:46
 * Description: erp物流导出模板
 */
public class ErpWuTempalte {
     private String erpOrderNo;
     private String orderNo;
     private String orderCode;
     private String wayNum;

    public String getErpOrderNo() {
        return erpOrderNo;
    }

    public void setErpOrderNo(String erpOrderNo) {
        this.erpOrderNo = erpOrderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getWayNum() {
        return wayNum;
    }

    public void setWayNum(String wayNum) {
        this.wayNum = wayNum;
    }
}
