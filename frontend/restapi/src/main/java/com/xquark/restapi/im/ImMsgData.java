package com.xquark.restapi.im;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel
public class ImMsgData {

  @ApiModelProperty("mesage length")
  private Integer msgLen;    // msg length

  @ApiModelProperty("message content")
  private String msgData;    // msg content

  @ApiModelProperty("message type, default 1, indecate text msg, will support voice and other kinds of text in the future")
  private Integer msgType;  // default 1, indecate text msg

  @ApiModelProperty("message send time, long type")
  private Long iMsgTime;    // msg send time

  @ApiModelProperty("message send time, date format: yyyy-MM-dd HH:mm:ss")
  private String sMsgTime;  // msg send time

  public Integer getMsgLen() {
    return msgLen;
  }

  public void setMsgLen(Integer msgLen) {
    this.msgLen = msgLen;
  }

  public String getMsgData() {
    return msgData;
  }

  public void setMsgData(String msgData) {
    this.msgData = msgData;
  }

  public Integer getMsgType() {
    return msgType;
  }

  public void setMsgType(Integer msgType) {
    this.msgType = msgType;
  }

  public Long getiMsgTime() {
    return iMsgTime;
  }

  public void setiMsgTime(Long iMsgTime) {
    this.iMsgTime = iMsgTime;
  }

  public String getsMsgTime() {
    return sMsgTime;
  }

  public void setsMsgTime(String sMsgTime) {
    this.sMsgTime = sMsgTime;
  }
}
