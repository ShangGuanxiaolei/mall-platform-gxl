package com.xquark.restapi.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by wangxinhua on 17-11-29. DESC:
 */
public class UserInfoVO {

  @ApiModelProperty(value = "id")
  private String id;

  @ApiModelProperty(value = "昵称")
  private String name;//目前等于绑定银行卡时候的姓名

  @ApiModelProperty(value = "头像")
  private String avatar;

  private String email;

  private String loginname;

  @ApiModelProperty(value = "手机号")
  private String phone;

  private String shopId;

  @ApiModelProperty(value = "身份证号")
  private String idCardNum;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getEmail() {
    return StringUtils.defaultString(email, "");
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getLoginname() {
    return loginname;
  }

  public void setLoginname(String loginname) {
    this.loginname = loginname;
  }

  public String getPhone() {
    return StringUtils.defaultString(phone, "");
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getIdCardNum() {
    if (idCardNum == null) {
      return "";
    }
    return idCardNum.replaceAll("(\\d{6})\\d{8}(\\w{4})", "$1*****$2");
  }

  public void setIdCardNum(String idCardNum) {
    this.idCardNum = idCardNum;
  }

}
