package com.xquark.restapi.user;

import com.xquark.dal.model.User;
import com.xquark.dal.status.UserType;
import com.xquark.restapi.BaseVO;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserVO extends BaseVO {

  private static final long serialVersionUID = 1L;

  private Long cpId;

  private String phone;

  private String name;

  private String avatar;

  private String zoneStr;

  public String getZoneStr() {
    return zoneStr;
  }

  public void setZoneStr(String zoneStr) {
    this.zoneStr = zoneStr;
  }

  /**
   * 是否为首席体验官
   */
  private int expOfficer;

  private String identityName;

  public String getIdentityType() {
    return identityType;
  }

  public void setIdentityType(String identityType) {
    this.identityType = identityType;
  }

  private String identityType;

  public int getExpOfficer() {
    return expOfficer;
  }

  public void setExpOfficer(int expOfficer) {
    this.expOfficer = expOfficer;
  }

  public String getIdentityName() {
    return identityName;
  }

  public void setIdentityName(String identityName) {
    this.identityName = identityName;
  }

  /**
   * 为空则表示未创建店铺
   */
  private String shopUrl;

  private boolean hasPwd;

  private String idCardNum;

  private String shopId;

  private UserType type;

  // 该用户团队成员数
  private long teamNum;

  // 该用户团队贡献数
  private BigDecimal amount;

  private Date createdAt;

  private String createdAtStr;

  private Boolean hasProfile = false;

  private String customerNumber;

  private boolean isPrueWhite;

  private boolean hasIdentity;

  private boolean isLighten;

  private boolean hasBankInfo;

  private boolean isShopOwner;

  private String bankUserName;
  private String bankTincode;
  private String bankCardNumber;
  private String bankName;

  private boolean isMember;
  private boolean isProxy;

  private boolean isAchievement;

  private int registerPointFlag;//是否已显示注册德分提示，1已显示，0未显示

  private String uplineCpId; // 用户上级
  private String uplineNickName; // 用户上级昵称
  private String uplineType; // 用户上级类型

  public String getUplineCpId() {
    return uplineCpId;
  }

  public void setUplineCpId(String uplineCpId) {
    this.uplineCpId = uplineCpId;
  }

  public String getUplineType() {
        return uplineType;
    }

    public void setUplineType(String uplineType) {
        this.uplineType = uplineType;
    }


  public String getUplineNickName() {
    return uplineNickName;
  }

  public void setUplineNickName(String uplineNickName) {
    this.uplineNickName = uplineNickName;
  }

  public int getRegisterPointFlag() {
    return registerPointFlag;
  }

  public void setRegisterPointFlag(int registerPointFlag) {
    this.registerPointFlag = registerPointFlag;
  }

  private Long collections;

  public Long getCollections() {
    return collections;
  }

  public void setCollections(Long collections) {
    this.collections = collections;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getCreatedAtStr() {
    Date date = this.getCreatedAt();
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
          "yyyy-MM-dd HH:mm:ss");
      return simpleDateFormat.format(date);
    }
  }

  public void setCreatedAtStr(String createdAtStr) {
    this.createdAtStr = createdAtStr;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public Boolean getHasProfile() {
    return hasProfile;
  }

  public void setHasProfile(Boolean hasProfile) {
    this.hasProfile = hasProfile;
  }

  public long getTeamNum() {
    return teamNum;
  }

  public void setTeamNum(long teamNum) {
    this.teamNum = teamNum;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public UserVO(User u, String shopUrl) {
    super(u);
    cpId = u.getCpId();
    expOfficer=u.getExpOfficer();
    zoneStr=u.getZoneStr();
    collections=u.getCollections();
    name = u.getName();
    phone = u.getPhone();
    avatar = u.getAvatar();
    hasPwd = StringUtils.hasLength(u.getPassword());
    shopId = u.getShopId();
    idCardNum = u.getIdCardNum();
    createdAt = u.getCreatedAt();
    this.shopUrl = shopUrl;
    this.registerPointFlag=u.getRegisterPointFlag();
  }

  public UserType getType() {
    return type;
  }

  public void setType(UserType type) {
    this.type = type;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getShopUrl() {
    return shopUrl;
  }

  public void setShopUrl(String shopUrl) {
    this.shopUrl = shopUrl;
  }

  public boolean isHasPwd() {
    return hasPwd;
  }

  public void setHasPwd(boolean hasPwd) {
    this.hasPwd = hasPwd;
  }

  public String getIdCardNum() {
    return idCardNum;
  }

  public void setIdCardNum(String idCardNum) {
    this.idCardNum = idCardNum;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public boolean getIsPrueWhite() {
    return isPrueWhite;
  }

  public void setIsPrueWhite(boolean prueWhite) {
    isPrueWhite = prueWhite;
  }

  public boolean isHasIdentity() {
    return hasIdentity;
  }

  public void setHasIdentity(boolean hasIdentity) {
    this.hasIdentity = hasIdentity;
  }

  public boolean getIsLighten() {
    return isLighten;
  }

  public void setIsLighten(boolean lighten) {
    isLighten = lighten;
  }

  public boolean isHasBankInfo() {
    return hasBankInfo;
  }

  public void setHasBankInfo(boolean hasBankInfo) {
    this.hasBankInfo = hasBankInfo;
  }

  public boolean getIsShopOwner() {
    return isShopOwner;
  }

  public void setIsShopOwner(boolean shopOwner) {
    isShopOwner = shopOwner;
  }

  public String getBankUserName() {
    return bankUserName;
  }

  public void setBankUserName(String bankUserName) {
    this.bankUserName = bankUserName;
  }

  public String getBankTincode() {
    return bankTincode;
  }

  public void setBankTincode(String bankTincode) {
    this.bankTincode = bankTincode;
  }

  public String getBankCardNumber() {
    return bankCardNumber;
  }

  public void setBankCardNumber(String bankCardNumber) {
    this.bankCardNumber = bankCardNumber;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public boolean getIsMember() {
    return isMember;
  }

  public void setIsMember(boolean member) {
    isMember = member;
  }

  public boolean getIsProxy() {
    return isProxy;
  }

  public void setIsProxy(boolean proxy) {
    isProxy = proxy;
  }

  public boolean getIsAchievement() {
    return isAchievement;
  }

  public void setIsAchievement(boolean achievement) {
    isAchievement = achievement;
  }
}
