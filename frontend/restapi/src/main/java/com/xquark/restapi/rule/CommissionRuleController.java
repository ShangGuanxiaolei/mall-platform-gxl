package com.xquark.restapi.rule;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.CommissionRule;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.commission.CommissionRuleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 分佣规则controller Created by chh on 16-12-21.
 */
@Controller
@ApiIgnore
public class CommissionRuleController extends BaseController {

  @Autowired
  private CommissionRuleService commissionRuleService;

  /**
   * 获取分佣规则列表
   */
  @ResponseBody
  @RequestMapping("/rule/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<CommissionRule> roles = null;
    roles = commissionRuleService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", commissionRuleService.selectCnt(keyword));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存分佣规则
   */
  @ResponseBody
  @RequestMapping("/rule/save")
  public ResponseObject<Boolean> savePromotion(CommissionRule role) {
    int result = 0;
    if (StringUtils.isNotEmpty(role.getId())) {
      result = commissionRuleService.modify(role);
    } else {
      role.setArchive(false);
      result = commissionRuleService.insert(role);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/rule/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = commissionRuleService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的分佣规则记录<br>
   */
  @ResponseBody
  @RequestMapping("/rule/{id}")
  public ResponseObject<CommissionRule> view(@PathVariable String id, HttpServletRequest req) {
    CommissionRule role = commissionRuleService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }


}
