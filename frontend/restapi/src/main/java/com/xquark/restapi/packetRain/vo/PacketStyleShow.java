package com.xquark.restapi.packetRain.vo;

import com.xquark.dal.vo.PromotionLotteryVO;

import java.util.List;

/**
 * @author Jack Zhu
 * @date 2019/03/18
 */
public class PacketStyleShow {

    private final PromotionLotteryVO promotion;

    private final List<StyleSort> styleSorts;

    public PacketStyleShow(PromotionLotteryVO promotion, List<StyleSort> styleSorts) {
        this.promotion = promotion;
        this.styleSorts = styleSorts;
    }

    public PromotionLotteryVO getPromotion() {
        return promotion;
    }

    public List<StyleSort> getStyleSorts() {
        return styleSorts;
    }
}
