package com.xquark.restapi.im;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class ImController extends BaseController {

  @Autowired
  private ProductService productService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ShopService shopService;

  @Value("${site.web.host.name}")
  private String site;

  @Value("${im.http.host.url}")
  private String host;

  @Value("${qiniu.extfmt}")
  String qiniuExtFmt;

  private String cutqiniuExtFmtStr(String imgUrl) {
    if (imgUrl == null || imgUrl.isEmpty()) {
      return null;
    }
    int idx = imgUrl.indexOf(qiniuExtFmt);
    if (0 < idx && idx < imgUrl.length()) {
      return imgUrl.substring(0, idx);
    }
    return imgUrl;
  }

  private ImProductVO productToVo(Product p) {
    if (p == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "product not exits...");
    }
    ImProductVO vo = new ImProductVO();
    vo.setDefPrice(p.getMarketPrice());
    vo.setPrice(p.getPrice());
    vo.setProductId(p.getId());
    vo.setProductimgUrl(resourceFacade.resolveUrl(p.getImg()));
    vo.setSellerId(p.getUserId());
    vo.setProductName(p.getName());
    vo.setDescription(p.getDescription());
    vo.setSales(p.getSales());
    vo.setAmount(p.getAmount());
    vo.setProductUrl(site + "/" + p.getId());
    Shop shop = shopService.load(p.getShopId());
    if (shop != null) {
      vo.setSellerName(shop.getName());
    }
    return vo;
  }

  private ImOrderVO orderToVo(Order o) {
    if (o == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "order not exits...");
    }
    ImOrderVO vo = new ImOrderVO();
    vo.setOrderId(o.getId());
    vo.setPrice(o.getTotalFee());
    vo.setSellerId(o.getSellerId());
    vo.setOrderSatus(o.getStatus());
    vo.setOrderNo(o.getOrderNo());
    vo.setShopId(o.getShopId());
    List<OrderItem> items = orderItemMapper.selectByOrderId(o.getId());
    List<OrderItemEx> list = new ArrayList<>(items.size());
    for (OrderItem item : items) {
      OrderItemEx ex = new OrderItemEx();
      BeanUtils.copyProperties(item, ex);
      //ex.setProductImgUrl(resourceFacade.resolveUrl2Orig(item.getProductImg()));
      ex.setProductImgUrl(cutqiniuExtFmtStr(resourceFacade.resolveUrl(item.getProductImg())));
      list.add(ex);
    }
    vo.setOrderItems(list);
    Shop shop = shopService.load(o.getShopId());
    if (shop != null) {
      vo.setSellerName(shop.getName());
    }

    return vo;
  }

  @ResponseBody
  @RequestMapping("/im/product/detail")
  public ResponseObject<ImProductVO> productDetail(String partner, String productId) {
    if (StringUtils.isEmpty(partner) || StringUtils.isEmpty(productId)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }
    return new ResponseObject<>(productToVo(productService.load(productId)));
  }

  @ResponseBody
  @RequestMapping("/im/order/detail")
  public ResponseObject<ImOrderVO> orderDetail(String partner, String orderId) {
    if (StringUtils.isEmpty(partner) || StringUtils.isEmpty(orderId)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "parameter error...");
    }
    return new ResponseObject<>(orderToVo(orderService.load(orderId)));
  }

  /**
   * im: message send api
   *
   * @param fId message send userId  (your id)
   * @param tId message sendto userId  (the other's)
   * @param ver version default 1001
   * @param content message content
   */
  @ResponseBody
  @RequestMapping("/im/sendMsg")
  public ResponseObject<Boolean> imSendMsg(@RequestParam("tId") String tId, String ver,
      @RequestParam("content") String content) {
    // generate msg send param
    User u = getCurrentUser();
    ImSendMsg ism = new ImSendMsg();
    ism.setFrom_user_id(u.getId());
    ism.setTo_user_id(tId);
    ism.setHold(0);
    ism.setMsg_seq(0);
    ism.setContent(content);
    ism.setVer(StringUtils.isEmpty(ver) ? 1001 : Integer.parseInt(ver));
    ism.setType("0");
    ism.setSessionId(u.getId());  // TODO

    HttpInvokeResult result = null;
    try {
      result = PoolingHttpClients.post(host + "/query/sendMsg", JSON.toJSONBytes(ism));
      log.debug(JSON.toJSONString(ism) + result.toString());
    } catch (Exception e) {
      log.debug(JSON.toJSONString(ism) + result.toString());
    }

    if (!result.isOK()) {
      log.error("Error to pushMsg:" + result, result.getException());
      return new ResponseObject<>(false);
    }
    return new ResponseObject<>(true);
  }

  /**
   * im message recv api (this api need spec userId both, client need to distinct by your own id)
   *
   * @param tId message sendto userId
   * @param ver version default 1001
   * @return msgvo
   */
  @ResponseBody
  @RequestMapping("/im/recvMsg")
  public ResponseObject<ImMsgDataVO> imRecvMsg(String tId, String ver) {
    // generate msg recv param
    User u = getCurrentUser();
    ImRecvMsg irm = new ImRecvMsg();
    irm.setFrom_user_id(u.getId());
    if (!StringUtils.isEmpty(tId)) {
      irm.setTo_user_id(tId);
    } else {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msg recv error...to_user_id not found");
    }
    irm.setHold(0);
    irm.setVer(StringUtils.isEmpty(ver) ? 1001 : Integer.parseInt(ver));
    irm.setType("2");  // "0": history msg   "1": unread msg(hold connection)  "2": unread msg(api)
    irm.setSessionId(u.getId());  // TODO

    HttpInvokeResult result = null;
    try {
      result = PoolingHttpClients.post(host + "/query/recvMsg", JSON.toJSONBytes(irm));
      log.debug(JSON.toJSONString(irm) + result.toString());
    } catch (Exception e) {
      log.debug(JSON.toJSONString(irm) + result.toString());
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msg recv error...");
    }

    ImMsgDataVO ret = new ImMsgDataVO();
    if (!result.isOK()) {
      log.error("Error to recvMsg:" + result, result.getException());
      return new ResponseObject<>(ret);
    } else {
      if (extractMsgRsp(result.getContent(), ret, u.getId())) {
        return new ResponseObject<>(ret);
      } else {
        return new ResponseObject<>(ret);
      }
    }
  }

  /**
   * im message recv api (all unread msg)
   *
   * @param ver version default 1001
   * @return msgvo
   */
  @ResponseBody
  @RequestMapping("/im/recvMsgAll")
  public ResponseObject<List<ImMsgDataVO>> imRecvMsgAll(String ver) {
    // generate msg recv param
    User u = getCurrentUser();
    ImRecvMsg irm = new ImRecvMsg();
    irm.setFrom_user_id(u.getId());
    //irm.setTo_user_id("");
    irm.setHold(0);
    irm.setVer(StringUtils.isEmpty(ver) ? 1001 : Integer.parseInt(ver));
    irm.setType("2");  // "0": history msg   "1": unread msg(hold connection)  "2": unread msg(api)
    irm.setSessionId(u.getId());  // TODO

    HttpInvokeResult result = null;
    try {
      result = PoolingHttpClients.post(host + "/query/recvMsg", JSON.toJSONBytes(irm));
      log.debug(JSON.toJSONString(irm) + result.toString());
    } catch (Exception e) {
      log.debug(JSON.toJSONString(irm) + result.toString());
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msg recv error...");
    }

    List<ImMsgDataVO> ret = new ArrayList<>();
    if (!result.isOK()) {
      log.error("Error to recvMsg:" + result, result.getException());
      return new ResponseObject<>(ret);
    } else {
      if (extractMsgRspEx(result.getContent(), ret, u.getId())) {
        return new ResponseObject<>(ret);
      } else {
        return new ResponseObject<>(ret);
      }
    }
  }

  /**
   * im message recv api (all unread msg cnt)
   *
   * @param ver version default 1001
   * @return msgvo
   */
  @ResponseBody
  @RequestMapping("/im/recvUnReadMsgCnt")
  public ResponseObject<ImMsgDataCntVO> imRecvUnreadMsgCnt(String ver) {
    // generate msg recv param
    User u = getCurrentUser();
    ImRecvMsg irm = new ImRecvMsg();
    irm.setFrom_user_id(u.getId());
    irm.setHold(0);
    irm.setVer(StringUtils.isEmpty(ver) ? 1001 : Integer.parseInt(ver));
    irm.setSessionId(u.getId());  // TODO

    HttpInvokeResult result = null;
    try {
      result = PoolingHttpClients.post(host + "/query/recvMsgCnt", JSON.toJSONBytes(irm));
      log.debug(JSON.toJSONString(irm) + result.toString());
    } catch (Exception e) {
      log.debug(JSON.toJSONString(irm) + result.toString());
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msg recv error...");
    }

    ImMsgDataCntVO ret = new ImMsgDataCntVO();
    if (!result.isOK()) {
      log.error("Error to recvMsgCnt:" + result, result.getException());
      return new ResponseObject<>(ret);
    } else {
      if (extractMsgCntRspEx(result.getContent(), ret, u.getId())) {
        return new ResponseObject<>(ret);
      } else {
        return new ResponseObject<>(ret);
      }
    }
  }


  private Boolean extractMsgRsp(String content, ImMsgDataVO vo, String userId) {
    JSONObject object = (JSONObject) JSON.parse(content);
    if (object == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msg recv json parser error...");
    }
    Integer ret = object.getInteger("result");
    if (ret != 200) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msg recv logic error....");
    }
    Object msgObj = object.get("msgList");
    if (!(msgObj instanceof JSONArray)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msg recv result data format error.....");
    }

    JSONArray arr = (JSONArray) msgObj;
    List<ImMsgData> msgList = new ArrayList<>();
    for (int i = 0; i < arr.size(); i++) {
      Object o = arr.getJSONObject(i);
      JSONObject jo = (JSONObject) o;
      vo.setAvatar(jo.getString("avatar"));
      vo.setName(jo.getString("name"));
      vo.setNickName(jo.getString("nickName"));
      String fromId = jo.getString("from_id");
      if (!fromId.equals(userId)) {
        vo.setFromId(fromId);
      }
      //e.setiMsgTime(iMsgTime);
      ImMsgData data = new ImMsgData();
      data.setMsgData(jo.getString("msgData"));
      data.setMsgLen(data.getMsgData().length());
      data.setMsgType(1);
      data.setsMsgTime(jo.getString("createTime"));
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date date = null;
      try {
        date = df.parse(data.getsMsgTime());
        if (date != null) {
          data.setiMsgTime(date.getTime());
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }

      msgList.add(data);
      //System.out.println(jo.toString());
    }
    vo.setMsgList(msgList);
    return true;
  }

  private Boolean extractMsgRspEx(String content, List<ImMsgDataVO> ret, String userId) {
    JSONObject object = (JSONObject) JSON.parse(content);
    if (object == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msgEx recv json parser error...");
    }
    Integer result = object.getInteger("result");
    if (result != 200) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msgEx recv logic error....");
    }
    Object msgObj = object.get("msgList");
    if (!(msgObj instanceof JSONArray)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msgEx recv result data format error.....");
    }

    JSONArray arr = (JSONArray) msgObj;
    Map<String, List<ImMsgData>> msgMap = new HashMap<>();
    for (int i = 0; i < arr.size(); i++) {
      Object o = arr.getJSONObject(i);
      JSONObject jo = (JSONObject) o;
      ImMsgDataVO vo = new ImMsgDataVO();
      vo.setAvatar(jo.getString("avatar"));
      vo.setName(jo.getString("name"));
      vo.setNickName(jo.getString("nickName"));
      String fromId = jo.getString("from_id");
      if (!fromId.equals(userId)) {
        vo.setFromId(fromId);
      }
      //vo.setFromId(fromId);
      //e.setiMsgTime(iMsgTime);
      ImMsgData data = new ImMsgData();
      data.setMsgData(jo.getString("msgData"));
      data.setMsgLen(data.getMsgData().length());
      data.setMsgType(1);
      data.setsMsgTime(jo.getString("createTime"));
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date date = null;
      try {
        date = df.parse(data.getsMsgTime());
        if (date != null) {
          data.setiMsgTime(date.getTime());
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }

      List<ImMsgData> d = msgMap.get(fromId);
      if (d == null) {
        d = new ArrayList<>();
        d.add(data);
        msgMap.put(fromId, d);
        ret.add(vo);
      } else {
        d.add(data);
      }
    }

    for (ImMsgDataVO vo : ret) {
      vo.setMsgList(msgMap.get(vo.getFromId()));
    }
    return true;
  }

  private Boolean extractMsgCntRspEx(String content, ImMsgDataCntVO ret, String userId) {
    JSONObject object = (JSONObject) JSON.parse(content);
    if (object == null) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msgEx recv cnt json parser error...");
    }
    Integer result = object.getInteger("result");
    if (result != 200) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "msgEx recv cnt logic error....");
    }
    Integer unreadCnt = object.getInteger("unreadTotal");
    if (unreadCnt == null || unreadCnt == 0) {
      ret.setTotalCnt(0);
      return false;
    }

    Object msgObj = object.get("unreadCntList");
    if (!(msgObj instanceof JSONArray)) {
      throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,
          "msgEx recv cnt result data format error.....");
    }

    JSONArray arr = (JSONArray) msgObj;
    List<ImMsgDataCnt> msgCntList = new ArrayList<>();
    for (int i = 0; i < arr.size(); i++) {
      Object o = arr.getJSONObject(i);
      JSONObject jo = (JSONObject) o;
      ImMsgDataCnt e = new ImMsgDataCnt();
      e.setUserId(jo.getString("user_id"));
      e.setMsgCount(jo.getInteger("count"));
      msgCntList.add(e);
    }
    ret.setTotalCnt(unreadCnt);
    ret.setMsgCntList(msgCntList);

    return true;
  }


}
