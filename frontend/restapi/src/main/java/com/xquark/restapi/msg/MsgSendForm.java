package com.xquark.restapi.msg;

import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 17-12-9. DESC:
 */
public class MsgSendForm {

  @NotBlank(message = "请选择要发送的消息")
  private String msgId;

  @NotNull(message = "请选择要发送的用户")
  private List<String> userIds;

  public String getMsgId() {
    return msgId;
  }

  public void setMsgId(String msgId) {
    this.msgId = msgId;
  }

  public List<String> getUserIds() {
    return userIds;
  }

  public void setUserIds(List<String> userIds) {
    this.userIds = userIds;
  }
}
