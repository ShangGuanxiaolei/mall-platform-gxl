package com.xquark.restapi.fullPieces;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullPieces;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fullReduce.fullPieces.PromotionFullPiecesService;
import com.xquark.service.pricing.PromotionService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
//@RestController
//@RequestMapping("/promotionFullPieces")
public class PromotionFullPiecesController extends BaseController {

  private PromotionService promotionService;

  private PromotionFullPiecesService promotionFullPiecesService;

  @Autowired
  public void setPromotionService(PromotionService promotionService) {
    this.promotionService = promotionService;
  }

  @Autowired
  public void setPromotionFullPiecesService(PromotionFullPiecesService promotionFullPiecesService) {
    this.promotionFullPiecesService = promotionFullPiecesService;
  }

  @RequestMapping(value = "/savePromotion", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<String> save(@RequestBody PromotionFullPiecesVO promotion) {
    String result = promotionFullPiecesService.save(promotion);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/preSave", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Promotion> preSave(@RequestBody Promotion promotion) {
    Promotion result = promotionFullPiecesService.preSave(promotion);
    if (result != null) {
      return new ResponseObject<>(result);
    }
    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "保存失败");
  }

  @RequestMapping("/listTable")
  public ResponseObject<Map<String, Object>> listTable(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable, String keyWord) {
    List<PromotionFullPiecesVO> list;
    Long total;
    try {
      list = promotionFullPiecesService
          .listPromotionVO(order, Sort.Direction.fromString(direction), pageable, keyWord);
      total = promotionFullPiecesService.countPromotion(keyWord);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String error = "满件活动加载失败";
      log.error(error, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, error);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/deleteProduct")
  public ResponseObject<Boolean> deleteProduct(@RequestParam("promotionId") String promotionId,
      @RequestParam("productId") String productId) {
    Boolean result = promotionFullPiecesService.deleteProduct(promotionId, productId);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/addProduct")
  public ResponseObject<Boolean> addProduct(PromotionFullPieces promotionFullPieces) {
    Boolean result = promotionFullPiecesService.addProduct(promotionFullPieces);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/listProduct")
  public ResponseObject<Map<String, Object>> listProductTable(
      @RequestParam("id") String id, Boolean gift,
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<Product> list;
    Long total;
    if (StringUtils.isBlank(id)) {
      id = null;
    }
    try {
      list = promotionFullPiecesService
          .listProductByPromotionId(id, gift, order, Sort.Direction.fromString(direction),
              pageable);
      total = promotionFullPiecesService.countProductByPromotionId(id, gift);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "商品加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/close")
  public ResponseObject<Boolean> close(@RequestParam("id") String id) {
    boolean result = promotionFullPiecesService.close(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/orderDetail")
  public ResponseObject<Map<String, Object>> listOrderDetail(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      Pageable pageable) {
    List<PromotionReduceOrderVO> details;
    Long total;
    try {
      details = promotionFullPiecesService
          .listOrderDetail(order, Sort.Direction.fromString(direction), pageable);
      total = promotionFullPiecesService.countOrderDetail();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "加载明细数据出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", details);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/deleteDiscount")
  public ResponseObject<Boolean> deleteDiscount(@RequestParam String promotionId,
      @RequestParam String id) {
    long discountCount = promotionFullPiecesService.countDiscount(promotionId);
    if (discountCount <= 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "折扣数不能小于一个");
    }
    boolean result = promotionFullPiecesService.deleteDiscount(promotionId, id);
    return new ResponseObject<>(result);
  }
}
