package com.xquark.restapi.im;

public class ImSendMsg {

  private String sessionId;    // not used now
  private Integer ver;      // default 1001
  private String to_user_id;    // send msg to
  private String from_user_id;  // send msg by
  private String type = "1";    // 1:indecate text msg
  private Integer hold = 0;    // for http connect, default 0: break when response came
  private Integer msg_seq = 0;  // msg u send one session, need not to care now
  private String content;      // msg content

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getTo_user_id() {
    return to_user_id;
  }

  public void setTo_user_id(String to_user_id) {
    this.to_user_id = to_user_id;
  }

  public String getFrom_user_id() {
    return from_user_id;
  }

  public void setFrom_user_id(String from_user_id) {
    this.from_user_id = from_user_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getHold() {
    return hold;
  }

  public void setHold(Integer hold) {
    this.hold = hold;
  }

  public Integer getMsg_seq() {
    return msg_seq;
  }

  public void setMsg_seq(Integer msg_seq) {
    this.msg_seq = msg_seq;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getVer() {
    return ver;
  }

  public void setVer(Integer ver) {
    this.ver = ver;
  }

}
