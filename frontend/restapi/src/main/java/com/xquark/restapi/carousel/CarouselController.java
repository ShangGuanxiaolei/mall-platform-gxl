package com.xquark.restapi.carousel;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.CarouselImage;
import com.xquark.dal.model.Product;
import com.xquark.dal.status.CarouselType;
import com.xquark.dal.type.TargetType;
import com.xquark.dal.vo.GrandSalePromotionVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.carousel.CarouselImageService;
import com.xquark.service.carousel.CarouselService;
import com.xquark.service.grandSale.GrandSalePromotionService;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.service.product.ProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 轮播图controller Created by chh on 17-07-10.
 */
@Controller
@ApiIgnore
public class CarouselController extends BaseController {

  @Autowired
  private CarouselService carouselService;

  @Autowired
  private CarouselImageService carouselImageService;

  @Autowired
  private ProductService productService;

  @Autowired
  private GrandSaleStadiumService grandSaleStadiumService;

  @Autowired
  private GrandSalePromotionService grandSalePromotionService;

  /**
   * 轮播图列表
   */
  @ResponseBody
  @RequestMapping("/carousel/list")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<Carousel> result = carouselService.list(pageable, params);
    Long total = carouselService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 删除轮播图
   */
  @ResponseBody
  @RequestMapping("/carousel/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = carouselService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查询是否已经有该类型已经设置过轮播图
   */
  @ResponseBody
  @RequestMapping("/carousel/check")
  public ResponseObject<Boolean> check(@RequestParam("type") String type) {
    String shopId = getCurrentIUser().getShopId();
    long count = carouselService.selectByType(shopId, type);
    if (count > 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存轮播图
   */
  @ResponseBody
  @RequestMapping("/carousel/save")
  public ResponseObject<Boolean> save(Carousel commission) {
    String shopId = getCurrentIUser().getShopId();
    commission.setShopId(shopId);
    commission.setArchive(false);
    int result = carouselService.insert(commission);
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 轮播图明细列表
   */
  @ResponseBody
  @RequestMapping("/carousel/listImage")
  public ResponseObject<Map<String, Object>> listImage(
      @RequestParam("carouselId") String carouselId, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    params.put("carouselId", carouselId);
    List<CarouselImage> result = carouselImageService.list(pageable, params);
    Long total = carouselImageService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 删除轮播图明细
   */
  @ResponseBody
  @RequestMapping("/carousel/deleteImage/{id}")
  public ResponseObject<Boolean> deleteImage(@PathVariable String id) {
    int result = carouselImageService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取轮播图明细
   */
  @ResponseBody
  @RequestMapping("/carousel/getImage/{id}")
  public ResponseObject<CarouselImage> getImage(@PathVariable String id) {
    CarouselImage result = carouselImageService.load(id);
    TargetType targetType = result.getTargetType();
    String target = result.getTarget();
    if (targetType == TargetType.PRODUCT) {
      Product product = productService.load(target);
      if (product != null) {
        result.setTargetName(product.getName());
      }
    } else if (targetType == TargetType.ACTIVITY) {
      //设置会场标题
      if (target != null) {
        String[] split = target.split("=");
        String type = split[split.length - 1];
        String title = grandSaleStadiumService.selectTitleByType(type);
        if (title == null) {
          GrandSalePromotionVO grandSalePromotionVO = grandSalePromotionService.selectGrandSaleForAdmin();
          if (grandSalePromotionVO != null) {
            result.setTargetName(grandSalePromotionVO.getName());
          }
        } else {
          result.setTargetName(title);
        }
      }
    }
    return new ResponseObject<>(result);
  }

  /**
   * 保存轮播图明细
   */
  @ResponseBody
  @RequestMapping("/carousel/saveImage")
  public ResponseObject<Boolean> saveImage(CarouselImage commission) {
    int result;
    if (StringUtils.isNotEmpty(commission.getId())) {
      result = carouselImageService.update(commission);
    } else {
      commission.setArchive(false);
      // 优惠券入口只能保存一张图片
      String carouseId = commission.getCarouselId();
      Carousel carousel = carouselService.load(carouseId);
      if (carousel.getType() == CarouselType.HOME_COUPON) {
        long count = carouselImageService.countByCarouselId(carouseId);
        if (count > 0) {
          return new ResponseObject<>(false);
        }
      }
      result = carouselImageService.insert(commission);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

}
