package com.xquark.restapi.dna;

import com.xquark.dal.mapper.CustomerProfileMapper;
import com.xquark.dal.vo.WxVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.wxservice.JsConfigVo;
import com.xquark.service.wxservice.WechatShareConfigService;
import com.xquark.service.wxservice.WxLoginService;
import com.xquark.service.wxservice.config.WeChatConfig;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chp
 */
@Controller
@RequestMapping("/wx")
public class WxLoginController extends  BaseController {
     private static final Logger logger= LoggerFactory.getLogger(WxLoginController.class);
    Logger log= LoggerFactory.getLogger(this.getClass());
     @Autowired
     private WxLoginService wxLoginService;
     @Autowired
     private WechatShareConfigService wechatShareConfigService;
     @Autowired
     private CustomerProfileMapper customerProfileMapper;


      @RequestMapping("/dna")
      @ResponseBody
      public Object tess(String phone){
          Map<String,Object> map=new LinkedHashMap<>();
          map.put("msg","msg");
          return map;
      }


  /**
   * @Author chp
   * @Description //自动获取code登录
   * @Date
   * @Param
   * @return
   **/

     @RequestMapping(value = "/wxLogin")
     public void wxLogin(HttpServletRequest request, HttpServletResponse resp)  throws  Exception{
         StringBuffer requestURL  = request.getRequestURL();
         String backUrl = requestURL.toString().replace("wxLogin", "callBack");
//         String backUrl="https://uathwapi.handeson.com/mp/v2/wx/callBack";
         /**
          *这儿一定要注意！！首尾不能有多的空格（因为直接复制往往会多出空格），其次就是参数的顺序不能变动
          **/
         String appId= WeChatConfig.appId;
         String url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId+
                 "&redirect_uri=" + URLEncoder.encode(backUrl,"UTF-8")+
                 "&response_type=code" +
                 "&scope=snsapi_userinfo" +
                 "&state=STATE#wechat_redirect";
         resp.sendRedirect(url);
     }




    @RequestMapping(value = "/wxLoginByCode",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseObject<WechatUserBean> getUserInfo(@RequestBody Map<String,Object> map)
    {
        WechatUserBean userInfo = wxLoginService.getUserInfo(map.get("code")+"");
        return new ResponseObject<>(userInfo);
    }





    @RequestMapping(value = "/getCus",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public  ResponseObject<List<WxVO>>  getCustomerId(@RequestBody Map<String,Object> map)//通过unionId获取多账号
    {
        List<WxVO> customer  = wxLoginService.getCustomerId(map);
        if(customer!=null){
            return new ResponseObject<>(customer);
        }
        return new ResponseObject<>();
    }

    @RequestMapping(value = "/getJsConfig",method = RequestMethod.GET)
    @ResponseBody
    public  ResponseObject<JsConfigVo>   getJsTicket(HttpServletRequest request){
        String referer = request.getHeader("referer");
        String url=request.getParameter("url");
        if (url==null||url.equals("")){throw  new BizException(GlobalErrorCode.SCRM_ADDRESS_ILLEGAL);}
         if (referer==null||referer.equals("")){
             referer=url;
         }
        JsConfigVo jsConfig=null;
         try {
             jsConfig = wechatShareConfigService.getJsConfig(referer);
         }
          catch (Exception e){
             throw new BizException(GlobalErrorCode.SIGN_ERROR,"用户鉴权失败");
        }
        return new ResponseObject<>(jsConfig);
    }



    /*
  * @Author chp
  * @Description   微信回调返回用户信息
  * @Date
  * @Param
  * @return
  **/
    @RequestMapping("/callBack")
    @ResponseBody
    public ResponseObject<WechatUserBean> callBack(HttpServletRequest req) throws Exception{
        String code=req.getParameter("code");
        log.info("用户code:"+code);
        WechatUserBean userInfo = wxLoginService.getUserInfo(code);
        return new ResponseObject<>(userInfo);}


}
