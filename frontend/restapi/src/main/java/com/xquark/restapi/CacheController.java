package com.xquark.restapi;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.cache.ButtonCache;
import com.xquark.cache.ModuleCache;
import com.xquark.cache.UrlRoleCache;
import com.xquark.dal.IUser;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者: wangxh 创建日期: 17-4-17 简介:
 */
@RestController
@ApiIgnore
public class CacheController {

  @Autowired
  private ModuleCache moduleCache;

  @Autowired
  private UrlRoleCache urlRoleCache;

  @Autowired
  private ButtonCache buttonCache;

  @RequestMapping("/cache/module")
  public ResponseObject<Boolean> refreshModule() {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    IUser iUser = null;
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        iUser = (IUser) principal;
      }
    }

    try {
      if (iUser != null) {
        moduleCache.refreshByUser(iUser);
      } else {
        return new ResponseObject<>(false);
      }
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "刷新菜单缓存异常\n" + e);
    }
    return new ResponseObject<>(true);
  }

  @RequestMapping("/cache/role")
  public ResponseObject<Boolean> refreshRole() {
    try {
      urlRoleCache.refresh();
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "刷新角色缓存异常\n" + e);
    }
    return new ResponseObject<>(true);
  }

  @RequestMapping("/cache/function")
  public ResponseObject<Boolean> refreshFunction() {
    try {
      buttonCache.refresh();
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "刷新按钮缓存异常\n" + e);
    }
    return new ResponseObject<>(true);
  }
}
