package com.xquark.restapi.logisticsPlatform;

/**
 * @author wangxinhua
 * @date 2019-04-24
 * @since 1.0
 */
public class LogisticsResult {

    private final String skuOuterID;

    private final Integer result;

    public LogisticsResult(String skuOuterID, Integer result) {
        this.skuOuterID = skuOuterID;
        this.result = result;
    }

    public Integer getResult() {
        return result;
    }

    public String getSkuOuterID() {
        return skuOuterID;
    }
}
