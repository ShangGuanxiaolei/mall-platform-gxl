package com.xquark.restapi.view;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.ViewComponent;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.view.ViewComponentService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 页面渲染组件controller Created by chh on 16-10-18.
 */
@Controller
@ApiIgnore
public class ViewComponentController extends BaseController {

  @Autowired
  private ViewComponentService viewComponentService;

  /**
   * 获取页面渲染组件列表
   */
  @ResponseBody
  @RequestMapping("/viewComponent/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<ViewComponent> viewComponents = null;
    viewComponents = viewComponentService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", viewComponentService.selectCnt(keyword));
    aRetMap.put("list", viewComponents);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存页面渲染组件
   */
  @ResponseBody
  @RequestMapping("/viewComponent/save")
  public ResponseObject<Boolean> savePromotion(ViewComponent viewComponent) {
    int result = 0;
    if (StringUtils.isNotEmpty(viewComponent.getId())) {
      result = viewComponentService.modifyViewComponent(viewComponent);
    } else {
      viewComponent.setArchive(false);
      result = viewComponentService.insert(viewComponent);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/viewComponent/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = viewComponentService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的发现记录<br>
   */
  @ResponseBody
  @RequestMapping("/viewComponent/{id}")
  public ResponseObject<ViewComponent> view(@PathVariable String id, HttpServletRequest req) {
    ViewComponent viewComponent = viewComponentService.selectByPrimaryKey(id);
    return new ResponseObject<>(viewComponent);
  }


}
