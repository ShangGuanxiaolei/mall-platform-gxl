package com.xquark.restapi.user;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/3/13
 * Time: 10:07
 * Description: 用户绑定手机号
 */
public class UserFormDna extends  UserForm {
    @NotBlank(message = "用户cpid不能为空")
    private String cpid;

    public String getCpid() {
        return cpid;
    }

    public void setCpid(String cpid) {
        this.cpid = cpid;
    }
}
