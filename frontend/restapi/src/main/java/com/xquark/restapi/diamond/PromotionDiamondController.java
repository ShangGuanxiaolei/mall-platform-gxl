package com.xquark.restapi.diamond;

import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionDiamond;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionDiamondProductVO;
import com.xquark.dal.vo.PromotionDiamondVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.diamond.PromotionDiamondService;
import com.xquark.service.pricing.PromotionService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 钻石专享活动controller Created by chh on 16-10-26.
 */
@Controller
public class PromotionDiamondController extends BaseController {

  @Autowired
  private PromotionDiamondService promotionDiamondService;

  @Autowired
  private PromotionService promotionService;

  /**
   * 获取钻石专享活动列表
   */
  @ResponseBody
  @RequestMapping("/promotionDiamond/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable) {
    String shopId = this.getCurrentIUser().getShopId();
    List<Promotion> diamonds = null;
    diamonds = promotionDiamondService.listPromotion(pageable, "");
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", promotionDiamondService.selectPromotionCnt(""));
    aRetMap.put("list", diamonds);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 根据搜索关键字获取钻石专享活动列表
   */
  @ResponseBody
  @RequestMapping("/promotionDiamond/list/{keyword}")
  public ResponseObject<Map<String, Object>> listkeyword(Pageable pageable,
      @PathVariable String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<Promotion> diamonds = null;
    diamonds = promotionDiamondService.listPromotion(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", promotionDiamondService.selectPromotionCnt(keyword));
    aRetMap.put("list", diamonds);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存钻石专享活动
   */
  @ResponseBody
  @RequestMapping("/promotionDiamond/savePromotion")
  public ResponseObject<Boolean> savePromotion(Promotion promotion) {
    int result = 0;
    if (StringUtils.isNotEmpty(promotion.getId())) {
      result = promotionService.update(promotion);
    } else {
      promotion.setArchive(false);
      promotion.setType(PromotionType.DIAMOND);
      result = promotionService.insert(promotion);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/promotionDiamond/close/{id}")
  public ResponseObject<Boolean> close(@PathVariable String id) {
    int result = promotionDiamondService.close(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取钻石专享活动关联商品列表
   */
  @ResponseBody
  @RequestMapping("/promotionDiamond/product/list/{id}")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable,
      @PathVariable String id) {
    String shopId = this.getCurrentIUser().getShopId();
    List<PromotionDiamondProductVO> products = null;
    products = promotionDiamondService.listPromotionProduct(pageable, id);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", promotionDiamondService.selectPromotionProductCnt(id));
    aRetMap.put("list", products);
    return new ResponseObject<>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/promotionDiamond/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = promotionDiamondService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 保存钻石专享活动商品信息
   */
  @ResponseBody
  @RequestMapping("/promotionDiamond/save")
  public ResponseObject<Boolean> save(PromotionDiamond promotionDiamond) {
    int result = 0;
    String shopId = this.getCurrentIUser().getShopId();

    // 同一个商品不能加入同一个钻石专享活动中
    long count = promotionDiamondService
        .selectPromotionByProductId(promotionDiamond.getPromotionId(), promotionDiamond.getId(),
            promotionDiamond.getProductId());
    if (count > 0) {
      return new ResponseObject<>(false);
    }

    // 查找某个商品在其他有效钻石专享活动中的信息，用于判断这个商品有没有存在同一时段中不同活动中
    List<PromotionDiamondVO> existPromotionProduct = promotionDiamondService.
        listExistPromotionProduct(promotionDiamond.getPromotionId(),
            promotionDiamond.getProductId());
    boolean flag = false;
    Promotion promotion = promotionService.load(promotionDiamond.getPromotionId());
    for (PromotionDiamondVO PromotionDiamondVO : existPromotionProduct) {
      Date validFrom = PromotionDiamondVO.getValidFrom();
      Date validTo = PromotionDiamondVO.getValidTo();
      if (isOverlap(validFrom, validTo, promotion.getValidFrom(), promotion.getValidTo())) {
        flag = true;
        break;
      }
    }
    if (flag) {
      return new ResponseObject<>(false);
    }

    if (StringUtils.isNotEmpty(promotionDiamond.getId())) {
      result = promotionDiamondService.modifyPromotionDiamond(promotionDiamond);
    } else {
      promotionDiamond.setArchive(false);
      promotionDiamond.setShopid(shopId);
      result = promotionDiamondService.insert(promotionDiamond);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 判断两个时间段是否有重叠
   */
  private static boolean isOverlap(Date leftStartDate, Date leftEndDate, Date rightStartDate,
      Date rightEndDate) {
    return
        ((leftStartDate.getTime() >= rightStartDate.getTime())
            && leftStartDate.getTime() < rightEndDate.getTime())
            ||
            ((leftStartDate.getTime() > rightStartDate.getTime())
                && leftStartDate.getTime() <= rightEndDate.getTime())
            ||
            ((rightStartDate.getTime() >= leftStartDate.getTime())
                && rightStartDate.getTime() < leftEndDate.getTime())
            ||
            ((rightStartDate.getTime() > leftStartDate.getTime())
                && rightStartDate.getTime() <= leftEndDate.getTime());

  }


}
