package com.xquark.restapi.WinningList;

import com.xquark.dal.model.WinningDetail;
import com.xquark.dal.vo.WinningListView;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.winningList.WinningService;
import com.xquark.service.winningList.vo.WinningUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

@Controller
public class WinningController extends BaseController {

  @Autowired private WinningService winningService;

  /**
   * 查询状态列表
   *
   * @return 奖品列表
   */
  @ResponseBody
  @RequestMapping("/winning/getwinninglist")
  public ResponseObject<List<WinningListView>> getWinning() {
    Long cpId = User().getCpId();
    if (cpId != null) {
      return ResponseObject.from(winningService.getWinningList(cpId));
    }
    return new ResponseObject<>(Collections.emptyList());
  }



  /** 领取状态的改变 */
  @ResponseBody
  @RequestMapping("/winning/save")
  public ResponseObject<Boolean> changeStatus(@RequestParam String winningId,
      @RequestParam String addressId) {
     return ResponseObject.from(winningService.createWinningOrder(winningId,addressId));
  }

  /** 查看领取详情 */
  @ResponseBody
  @RequestMapping("/winning/detail")
  public ResponseObject<WinningDetail> getReceiveDetail(@RequestParam String winningId) {
    WinningDetail winningDetail = winningService.getAddressDetail(winningId, User().getCpId());
    return new ResponseObject<>(winningDetail);
  }

  /**
   * 获取中奖名单
   * @return 中奖人列表
   */
  @ResponseBody
  @RequestMapping(value = "/winning/getWinningUserList")
  public ResponseObject<List<WinningUser>> getWinningUserList(){
    return new ResponseObject<>(winningService.getWinningUserList());
  }

}
