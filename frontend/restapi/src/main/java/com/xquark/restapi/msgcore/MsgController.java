package com.xquark.restapi.msgcore;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.mapper.MsgListMapper;
import com.xquark.dal.model.CollagePushMessage;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.msgcore.MsgListService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author zzl
 * @date
 *
 */
@Controller("MsgController")
public class MsgController extends BaseController {


  @Autowired
  private MsgListService msgListService;

  private MsgListMapper msgListMapper;


  /**
   * push消息列表查询
   * @param TypeMsg 消息类型
   * @param pageNum 每30条一页，查询第几页。之前查询的条数前端缓存起来，当页数为n时，一共显示n*30条信息
   * @return List<CollagePushMessage>
   */
  @RequestMapping(value = "/msg/list",method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<PageHelper> selectMsgList(String Msgtype, @RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum, @RequestParam(value = "pageSize",defaultValue = "30")Integer pageSize){
    User user = (User) getCurrentIUser();
    PageHelper<CollagePushMessage> selectMsgList=msgListService.selectMsgList(user.getCpId(),Msgtype,pageNum,pageSize);
    return new ResponseObject<>(selectMsgList);
  }


  /**
   * push未读消息列表查询
   * @param belognTo
   * @param pageNum
   * @return List<CollagePushMessage>
   */
  @RequestMapping(value = "/msg/unread/list",method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<List<CollagePushMessage>> selectMsgUnReadList(String belognTo, @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize",defaultValue = "30")Integer pageSize){
    return new ResponseObject<>(msgListService.selectMsgUnReadList(belognTo,pageNum,pageSize));
  }

  /**
   * push消息状态修改
   * @param id
   *
   */
  @RequestMapping(value = "/msg/update",method = RequestMethod.GET)
  public Boolean updateMsgStatus(String id){
    Boolean aBoolean = msgListService.updateMsgStatus(id);
    return aBoolean;
  }

  /**
   * 查询当前用户有几条未读消息
   * @return
   */
  @ResponseBody
  @RequestMapping("/msg/selectCount")
  public ResponseObject<Map<String, Object>> selectCount(){
    User user = (User) getCurrentIUser();
    int count=msgListService.selectMsgByCpId(user.getCpId());
    Map<String,Object> msgCount= ImmutableMap.<String, Object>of("msgCount", count);
    return new ResponseObject<>(msgCount);
  }

  /**
   * 根据类型查询数量和第一条数据
   * @return
   */
  @ResponseBody
  @RequestMapping("/msg/selectTypeInfo")
  public ResponseObject<Map<String, Object>> selectTypeInfo(){
    User user = (User) getCurrentIUser();
    Map<String ,Object> map=new HashMap<>();
    Map<String,Object> ServiceList=msgListService.typeList(user.getCpId(),"SERVICE");
    map.put("ServiceList", ServiceList);

    Map<String,Object> SystemList=msgListService.typeList(user.getCpId(),"SYSTEM");
    map.put("SystemList", SystemList);
    return new ResponseObject<>(map);
  }


  /**
   * 删除消息
   * @param integerList
   * @return
   */
  @ResponseBody
  @RequestMapping("/msg/delete")
  public ResponseObject<Map<String ,Object>> deleteMsg(List<Integer> integerList){
    Map<String,Object> map=new HashMap<>();
    boolean delete=msgListService.deleteMsg(integerList);
    map.put("Delete",delete);
    return  new ResponseObject<>(map);
  }



}

