package com.xquark.restapi.pintuan;

import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.status.ExecStatus;
import com.xquark.dal.status.PieceStatus;
import com.xquark.dal.vo.GroupShareVO;
import com.xquark.dal.vo.PromotionGroupDetailVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.PromotionGroupDetailService;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.pintuan.RecordPromotionPgTranInfoService;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.promotion.PromotionBaseInfoService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LiHaoYang
 * @ClassName PromotionGroupDetailController
 * @date 2018/9/15 0015
 */
@Controller
public class PromotionGroupDetailController {

  private final static Logger LOGGER = LoggerFactory.getLogger(PromotionGroupDetailController.class);

  @Autowired
  private PromotionGroupDetailService promotionGroupDetailService;
  @Autowired
  private PromotionPgMemberInfoService promotionPgMemberInfoService;
  @Autowired
  private RecordPromotionPgTranInfoService promotionPgTranInfoService;
  @Autowired
  private PromotionPgService promotionPgService;
  @Autowired
  private PgPromotionServiceAdapter promotionServiceAdapter;
  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  /**
   * 我的拼团详情
   */
  @ResponseBody
  @RequestMapping("/promotionGroupDetail/selectGroup")
  public ResponseObject<Map<String, Object>> selectGroup(String pieceGroupTranCode) {
    List<PromotionGroupDetailVO> promotionGroupDetailVO = promotionGroupDetailService
        .selectGroupDetail(pieceGroupTranCode);
    Map<String, Object> map = new HashMap<>();
    if (CollectionUtils.isEmpty(promotionGroupDetailVO)) {
      LOGGER.error("拼团 {} promotionGroupDetailVO数据不存在", pieceGroupTranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不存在");
    }
    PGTranInfoVo tranInfoVo = promotionPgTranInfoService.loadPGTranInfo(pieceGroupTranCode);
    if (tranInfoVo == null) {
      LOGGER.error("拼团 {} tranInfoVO数据不存在", pieceGroupTranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不存在");
    }
    GroupShareVO shareVO = promotionPgMemberInfoService.loadShareVO(pieceGroupTranCode);
    int count = 0, beginCount = 0;
    if (shareVO != null) {
      count = shareVO.getTotalMember();
      beginCount = shareVO.getCurrMember();
    }

    PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(tranInfoVo.getpCode());

    // 是否是插队拼团
    boolean canCutline = StringUtils.isNotBlank(pieceGroupTranCode) && "5".equals(baseInfo.getpStatus()) &&
            promotionServiceAdapter.isInCutLineTime(tranInfoVo.getpCode(), tranInfoVo::getGroupFinishTime) || ExecStatus.NORMAL.equals(PieceStatus.fromCode(tranInfoVo.getPieceStatus()).getRet());

    //拼团信息存在，查看是否是新人团
    Boolean isNewPg = promotionPgService.PromotionNewPg(tranInfoVo.getpCode());
    map.put("groupDetail", promotionGroupDetailVO);
    map.put("count", count);
    map.put("beginCount", beginCount);
    map.put("groupStatus", canCutline ? ExecStatus.NORMAL : PieceStatus.fromCode(tranInfoVo.getPieceStatus()).getRet());
    map.put("groupBuyShare", shareVO);
    map.put("isNewPg",isNewPg);
    return new ResponseObject<>(map);
  }

}
