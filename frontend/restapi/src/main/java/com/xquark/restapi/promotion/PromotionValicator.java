package com.xquark.restapi.promotion;

import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.order.CartNextForm;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionOrderDetailService;
import com.xquark.service.promotion.impl.PromotionSkusSerciceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class PromotionValicator {

    @Autowired
    private PromotionSkusSerciceImpl promotionSkusService;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    PromotionBaseInfoService promotionBaseInfoService;

    @Autowired
    private PromotionOrderDetailService promotionOrderDetailService;


    public ResponseObject<Map<String, Object>> validateMeeting(User user, CartNextForm form) {
        //杭州大会活动有效期判断
        List<String> promotionSkuIds = promotionSkusService.getPromotionSkuIds(form.getSkuId());

        List<String> promotionSkuCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(form.getSkuId())) {
            for (String skuId : form.getSkuId()) {
                Sku sku = skuMapper.selectByPrimaryKey(skuId);
                List ret = promotionSkusService.getPromotionSkus(sku.getSkuCode());
                if (CollectionUtils.isNotEmpty(ret)) {
                    promotionSkuCodes.add(sku.getSkuCode());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(promotionSkuCodes)) {
            PromotionBaseInfo pb = promotionBaseInfoService.selectByPtype("meeting");
            if (pb.getEffectFrom().after(new Date())) {
                ResponseObject result = new ResponseObject<>();
                result.setMoreInfo("活动未开始。");
                return result;
            }
            if (pb.getEffectTo().before(new Date())) {
                ResponseObject result = new ResponseObject<>();
                result.setMoreInfo("活动已结束。");
                return result;
            }
        }
        //杭州大会活动有效期判断
        //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
        if (CollectionUtils.isNotEmpty(promotionSkuIds)) {
            boolean hasEnoughStock = promotionSkusService.hasEnoughStock(promotionSkuIds);
            if (!hasEnoughStock) {
                ResponseObject result = new ResponseObject<>();
                result.setMoreInfo("活动商品已售罄。");
                return result;
            }
        }
        //杭州大会活动商品库存判断，2018-10-07 by Chenpeng

//        String inviteCode = form.getInviteCode();
//        if (StringUtils.isNotEmpty(inviteCode)) {
//            int isValidate = promotionInviteCodeService.validate(inviteCode,null);
//            if (isValidate < 0) {
//                ResponseObject result = new ResponseObject<>(GlobalErrorCode.MEETING_INVITE);
//                result.setMoreInfo("邀请码无效或失效。");
//
//                return result;
//            }
//            promotionInviteCodeService.updateRelation(inviteCode, user.getCpId().toString(), null);
//        }

        //杭州大会总价值上线判断，2018-10-07 by Chenpeng
        boolean isOverUpValue;
        if (form.getQty() > 0) {
            isOverUpValue = isOverUpValue(form.getSkuId().get(0), user, form.getQty());
        } else {
            isOverUpValue = isOverUpValue(promotionSkuIds, user);
        }
        if (isOverUpValue) {
            ResponseObject result = new ResponseObject<>();
            result.setMoreInfo("购买总额已超过活动上限。");
            return result;
        }
        //杭州大会总价值上线判断，2018-10-07 by Chenpeng
        return null;
    }

    private boolean isOverUpValue(List<String> promotionSkuIds, User user) {
        return promotionOrderDetailService.isOverUpValue(promotionSkuIds, user,1000,"");
    }

    private boolean isOverUpValue(String promotionSkuId, User user, int qty) {
        Sku sku = skuMapper.selectByPrimaryKey(promotionSkuId);
        List<PromotionBaseInfo> lstSkus = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
        if (CollectionUtils.isEmpty(lstSkus)) {
            return false;
        }
        return promotionOrderDetailService.isOverUpValue(promotionSkuId, user, qty,1000);
    }
}
