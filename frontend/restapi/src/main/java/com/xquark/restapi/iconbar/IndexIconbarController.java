package com.xquark.restapi.iconbar;

import com.xquark.dal.mapper.PromotionWhitelistMapper;
import com.xquark.dal.model.CustomerCareerLevel;
import com.xquark.dal.model.Iconbar;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.Iconbar.IndexIconbarService;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.promotion.PromotionWhiteService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 新客逻辑,首页新图标展示层
 */
@RestController
@RequestMapping("/iconbar")
public class IndexIconbarController extends BaseController {

    private static final Log log = LogFactory.getLog(IndexIconbarController.class);

    private static final String IDENTITY_RC = "RC";
    private static final String IDENTITY_DS = "DS";

    private static final String WHITELIST_KEY = "freshman";
    private static final String OPT_SHOP_KEY = "optShop";//优选商家

    @Autowired
    private IndexIconbarService indexIconbarService;

    @Autowired
    private CareerLevelService careerLevelService;

    @Autowired
    PromotionService promotionService;

    @RequestMapping("/index/getNewIconList")
    public ResponseObject<Map<String, Object>> getNewIconList() {
        Map<String, Object> resultMap = new HashMap<>();
        User user = (User) getCurrentIUser();
        Integer identity; //0=白人(升级权益)  1=有身份(邀请有礼)  2=SR or Other(不显示)
        CustomerCareerLevel level = careerLevelService.load(user.getCpId());
        if (null == level) {
            log.info("==================当前用户查不到对应的身份数据：cpId=" + user.getCpId());
            return new ResponseObject<>("找不到用户[" + user.getCpId() + "]的身份数据", GlobalErrorCode.NOT_FOUND);
        }
        log.info(
                "=================upgrading: 获取当前用户身份：hdsType=" + level.getHdsType() + ", vvType=" + level
                        .getViviLifeType());

        // 白人
        if ((StringUtils.isEmpty(level.getHdsType()) || IDENTITY_RC.equals(level.getHdsType())) &&
                (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
                        .equals(level.getViviLifeType()))) {
            identity = 0;
        /*    upgrade.setType("upgrade");
            upgrade.setBanner(
                    "http://images.handeson.com/Fi30oA2b-0s1qODEGhhH24Mibkdn?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");*/
        } else
            if (IDENTITY_DS.equals(level.getHdsType()) && !level.getIsLightening() &&
                (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
                        .equals(level.getViviLifeType()))) {
            // SR身份的人看不到banner，do nothing
            identity = 2;
//            upgrade = null;
        }
        else {
            // 有身份的人
            identity = 1;
         /*   upgrade.setType("invite");
            upgrade.setBanner(
                    "http://images.handeson.com/FtPIulD9V_hkAhokrefLk6K3kKQj?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");*/
        }

        // 判断当前用户是否在新人专区的白名单中
        boolean isOpen = this.promotionService.checkIsOpen(user.getCpId(), WHITELIST_KEY);
        boolean isWhite = this.promotionService.checkIsWhite(user.getCpId(), WHITELIST_KEY);

        List<Iconbar> indexIconbarList = indexIconbarService.getIndexIconbarList(identity);
        if (null != indexIconbarList && indexIconbarList.size() > 0) {
            for (Iconbar iconbar : indexIconbarList) {
                if (iconbar.getType() == 2) {
                    if (identity == 1) {
                        iconbar.setUpgrade("invite");
                    } else if (identity == 0) {
                        iconbar.setUpgrade("upgrade");
                    }
                }
                if (iconbar.getType() == 4) { // 汉薇自营
                    iconbar.setSearchKey("汉德森");
                }
            }
            // 判断是否在新人专区白名单中，过滤掉新人专区
            if (isOpen && !isWhite) {
                indexIconbarList = indexIconbarList.stream().filter(
                        icon -> !Objects.equals(0, icon.getType())
                ).collect(Collectors.toList());
            }

            //优选商家白名单
            boolean optShopIsOpen = this.promotionService.checkIsOpen(user.getCpId(), OPT_SHOP_KEY);
            boolean optShopIsWhite = this.promotionService.checkIsWhite(user.getCpId(), OPT_SHOP_KEY);
            if (optShopIsOpen && !optShopIsWhite) {
                indexIconbarList = indexIconbarList.stream().filter(
                        icon -> !Objects.equals(1, icon.getType())
                ).collect(Collectors.toList());
            }

            resultMap.put("iconbarList", indexIconbarList);
        }
        return new ResponseObject<>(resultMap);
    }
}
