package com.xquark.restapi.point.vo;

import com.xquark.dal.model.UserPointDTO;

/**
 * @author Jack Zhu
 * @since 2018/12/13
 */
public class UserPointVO {
    private String name;
    private Long createAt;
    private Integer amount;
    private String avatar;
    private String bizType;
    private String leaveWord;

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public static UserPointVO convert(UserPointDTO userPointDTO){
        final UserPointVO userPointVO = new UserPointVO();
        userPointVO.setName(userPointDTO.getName());
        userPointVO.setCreateAt(userPointDTO.getCreateAt().getTime());
        userPointVO.setAmount(userPointDTO.getAmount());
        userPointVO.setAvatar(userPointDTO.getAvatar());
        userPointVO.setBizType(userPointDTO.getBizType());
        userPointVO.setLeaveWord(userPointDTO.getLeaveWord());
        return userPointVO;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Long createAt) {
        this.createAt = createAt;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "UserPointVO{" +
                "name='" + name + '\'' +
                ", createAt=" + createAt +
                ", amount=" + amount +
                '}';
    }
}
