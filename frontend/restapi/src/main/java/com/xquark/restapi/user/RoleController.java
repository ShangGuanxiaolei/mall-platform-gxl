package com.xquark.restapi.user;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.Role;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.user.RoleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户对应角色controller Created by chh on 16-12-13.
 */
@Controller
@ApiIgnore
public class RoleController extends BaseController {

  @Autowired
  private RoleService roleService;

  /**
   * 获取用户对应角色列表
   */
  @ResponseBody
  @RequestMapping("/role/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<Role> roles = null;
    roles = roleService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", roleService.selectCnt(keyword));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存用户对应角色
   */
  @ResponseBody
  @RequestMapping("/role/save")
  public ResponseObject<Boolean> savePromotion(Role role) {
    int result = 0;
    if (StringUtils.isNotEmpty(role.getId())) {
      result = roleService.modify(role);
    } else {
      role.setArchive(false);
      result = roleService.insert(role);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/role/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = roleService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的用户对应角色记录<br>
   */
  @ResponseBody
  @RequestMapping("/role/{id}")
  public ResponseObject<Role> view(@PathVariable String id, HttpServletRequest req) {
    Role role = roleService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }


}
