package com.xquark.restapi.yundou;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.IUser;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.User;
import com.xquark.dal.model.YundouOperationDetail;
import com.xquark.dal.model.YundouProduct;
import com.xquark.dal.model.YundouSetting;
import com.xquark.dal.type.YundouCategoryType;
import com.xquark.dal.vo.YundouOperationDetailVO;
import com.xquark.dal.vo.YundouProductVO;
import com.xquark.dal.vo.YundouRuleDetailVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.yundou.YundouOperationDetailService;
import com.xquark.service.yundou.YundouProductService;
import com.xquark.service.yundou.YundouSettingService;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/yundou")
@ApiIgnore
public class YundouController extends BaseController {

  @Autowired
  private YundouSettingService yundouSettingService;

  @Autowired
  private YundouOperationDetailService yundouOperationDetailService;

  @Autowired
  private YundouProductService yundouProductService;

  /**
   * 保存积分设置
   */
  @ResponseBody
  @RequestMapping("/saveSetting")
  public ResponseObject<Boolean> saveSetting(YundouSetting setting) {
    int result = 0;
    if (StringUtils.isNotEmpty(setting.getId())) {
      result = yundouSettingService.update(setting);
    } else {
      String shopId = getCurrentIUser().getShopId();
      setting.setShopId(shopId);
      setting.setArchive(false);
      result = yundouSettingService.insert(setting);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 积分记录列表信息
   */
  @ResponseBody
  @RequestMapping("/recordList")
  public ResponseObject<Map<String, Object>> listTwitterMember(
      @RequestParam(value = "direction", required = false) String direction,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(direction)) {
      params.put("direction", direction);
    }

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }

    params.put("startDate", startDate);
    params.put("endDate", endDate);

    String shopId = getCurrentIUser().getShopId();
    List<YundouOperationDetailVO> result = yundouOperationDetailService.list(pageable, params);
    Long total = yundouOperationDetailService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取概况中的数据
    summary = yundouOperationDetailService.getSummary(shopId);

    return new ResponseObject<>(summary);
  }

  /**
   * 删除积分商品设置
   */
  @ResponseBody
  @RequestMapping("/deleteProduct/{id}")
  public ResponseObject<Boolean> deleteProduct(@PathVariable String id) {
    int result = yundouProductService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取积分商品设置
   */
  @ResponseBody
  @RequestMapping("/getProduct/{id}")
  public ResponseObject<YundouProductVO> getProduct(@PathVariable String id) {
    YundouProductVO result = yundouProductService.load(id);
    return new ResponseObject<>(result);
  }

  /**
   * 积分商品列表
   */
  @ResponseBody
  @RequestMapping("/productList")
  public ResponseObject<Map<String, Object>> productList(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      String keyWord,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    IUser user = getCurrentIUser();
    if (user instanceof Merchant) {
      String shopId = user.getShopId();
      params.put("shopId", shopId);
    }
    if (StringUtils.isNotBlank(order)) {
      params.put("order", order);
      params.put("direction", direction);
    }
    if (StringUtils.isNotBlank(keyWord)) {
      params.put("keyWord", keyWord);
    }
    List<YundouProductVO> result = yundouProductService.list(pageable, params);
    Long total = yundouProductService.selectCntWithProducts(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 查询是否已经有该商品已经设置过积分设置
   */
  @ResponseBody
  @RequestMapping("/checkProduct")
  public ResponseObject<Boolean> checkProduct(@RequestParam("productId") String productId) {
    String shopId = getCurrentIUser().getShopId();
    long count = yundouProductService.countByProductId(shopId, productId);
    if (count > 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存积分商品设置
   */
  @ResponseBody
  @RequestMapping("/saveProduct")
  public ResponseObject<Boolean> saveProduct(HttpServletRequest request) {
    boolean flag = false;
    String shopId = getCurrentIUser().getShopId();
    String id = request.getParameter("id");
    String productId = request.getParameter("productId");

    String category = request.getParameter("category");
    String level = request.getParameter("level");
    String canDeduction = request.getParameter("canDeduction");
    String isForce = request.getParameter("isForce");

    YundouCategoryType categoryType = YundouCategoryType.valueOf(category);

    // 如果id不为空，则说明是修改，只用更新佣金明细记录的信息即可
    if (StringUtils.isNotEmpty(id)) {
      YundouProduct de = new YundouProduct();
      de.setId(id);
      de.setCanDeduction(new BigDecimal(canDeduction).intValue());
      de.setCategory(categoryType);
      de.setLevel(new BigDecimal(level).intValue());
      de.setIsForce(Boolean.valueOf(isForce));
      yundouProductService.update(de);
    }
    // 如果id为空，则说明是新增，则先要新增主表信息，再分别插入各种类型佣金子表信息
    else {
      YundouProduct de = new YundouProduct();
      de.setId(id);
      de.setCanDeduction(new BigDecimal(canDeduction).intValue());
      de.setCategory(categoryType);
      de.setLevel(new BigDecimal(level).intValue());
      de.setIsForce(Boolean.valueOf(isForce));
      de.setShopId(shopId);
      de.setArchive(false);
      de.setProductId(productId);
      yundouProductService.insert(de);
    }
    flag = true;
    if (flag) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @RequestMapping("/homeList")
  @ResponseBody
  public ResponseObject<List<Map<String, Object>>> yundouHomeList() {
    List<Map<String, Object>> result;
    try {
      result = yundouProductService.listCategory();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "加载首页数据失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/listProduct")
  public ResponseObject<Map<String, Object>> listProduct(String order, String direction,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotBlank(order)) {
      params.put("order", order);
    } else {
      params.put("order", "created_at");
    }
    if (StringUtils.isNotBlank(direction)) {
      params.put("direction", direction);
    }

    List<YundouProductVO> result = yundouProductService.list(pageable, params);
    Long total = yundouProductService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  @RequestMapping("/detail")
  @ResponseBody
  public ResponseObject<Map<String, Object>> listDetail(Pageable pageable) {
    IUser user = getCurrentIUser();
    if (!(user instanceof User)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户类型不正确");
    }
    String userId = user.getId();
    List<YundouOperationDetail> details = yundouOperationDetailService
        .selectByUser(userId, "created_at", pageable, Sort.Direction.ASC.name());
    Map<String, String> params = new HashMap<>();
    params.put("userId", userId);
    Long total = yundouOperationDetailService.selectCnt(params);

    Map<String, Object> result = new HashMap<>();
    result.put("list", details);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/record")
  @ResponseBody
  public ResponseObject<Map<String, Object>> userRecord(
      @RequestParam(value = "order", required = false, defaultValue = "created_at") String order,
      @RequestParam(value = "direction", required = false, defaultValue = "DESC") String direction,
      Pageable pageable) {
    Map<String, Object> result = new HashMap<>();
    List<YundouRuleDetailVO> records;
    IUser user = getCurrentIUser();
    if (!(user instanceof User)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户类型不正确");
    }
    String userId = user.getId();
    User dbUser = userService.load(userId);
    Long yundou = dbUser.getYundou();
    try {
      records = yundouOperationDetailService
          .listRuleDetailByUser(userId, order, Sort.Direction.fromString(direction), pageable);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取积分明细出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    result.put("list", records);
    result.put("yundou", yundou);
    return new ResponseObject<>(result);
  }

}