package com.xquark.restapi.rule;

import com.xquark.dal.model.FreshmanRule;
import com.xquark.dal.type.RulesType;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.rules.FreshmanRuleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 新人规则展示层:
 *  1. 新人升级规则
 *  2. 德分规则
 */
@RestController
@RequestMapping("/rules")
public class FreshmanRuleController {

    private static final Log log = LogFactory.getLog(FreshmanRuleController.class);

    @Autowired
    private FreshmanRuleService freshmanRuleService;

    @RequestMapping(value = "/freshman/{ruleId}", method = RequestMethod.GET)
    public ResponseObject<FreshmanRule> getFreshUpgradeRule(@PathVariable Integer ruleId) {
        FreshmanRule freshmanRule = new FreshmanRule();
        if (ruleId == 1) {
            freshmanRule = freshmanRuleService.getFreshmanRule(RulesType.RULE_FRESHMAN.toString());
        } else if (ruleId == 2) {
            freshmanRule = freshmanRuleService.getFreshmanRule(RulesType.RULE_POINT.toString());
        }
        return new ResponseObject<>(freshmanRule);
    }
}
