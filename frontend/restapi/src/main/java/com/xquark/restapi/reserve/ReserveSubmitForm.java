package com.xquark.restapi.reserve;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * @author: yyc
 * @date: 19-4-18 下午5:23
 */
public class ReserveSubmitForm {

  /** 商品skuId */
  @NotBlank private String skuId;

  /** 商品id */
  @NotBlank private String productId;

  /** 购买数量 */
  @NotNull private Integer qty;

  /** 活动id */
  @NotBlank private String promotionId;

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Integer getQty() {
    return qty;
  }

  public void setQty(Integer qty) {
    this.qty = qty;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }
}
