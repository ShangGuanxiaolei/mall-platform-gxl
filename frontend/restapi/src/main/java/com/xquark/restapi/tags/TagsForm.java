package com.xquark.restapi.tags;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 17-11-16. DESC:
 */
@ApiModel(value = "标签表单")
public class TagsForm {

  /**
   * id
   */
  @ApiModelProperty(value = "标签id")
  private String id;


  /**
   * 标签名称
   */
  @ApiModelProperty(value = "标签名称", required = true)
  @NotBlank(message = "标签名称不能为空")
  private String name;

  /**
   * 标签描述
   */
  @ApiModelProperty(value = "标签描述", notes = "保留字段，暂时不使用")
  private String description = "";

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
