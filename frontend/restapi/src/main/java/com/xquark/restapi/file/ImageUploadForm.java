package com.xquark.restapi.file;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.wordnik.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import com.xquark.dal.type.FileBelong;

public class ImageUploadForm {

  /**
   * 文件属于产品、店铺
   */
  @NotNull(message = "{valid.notBlank.message}")
  @ApiModelProperty(value = "文件属于产品、店铺(统一传入PRODUCT即可)", required = true)
  private FileBelong belong;

  @NotNull(message = "{valid.fileBelong.message}")
  @Valid
  @ApiModelProperty(value = "图片文件数据，MultipartFile类型", required = true)
  private List<MultipartFile> file;


  public FileBelong getBelong() {
    return belong;
  }

  public void setBelong(FileBelong belong) {
    this.belong = belong;
  }

  public List<MultipartFile> getFile() {
    return file;
  }

  public void setFile(List<MultipartFile> file) {
    this.file = file;
  }
}
