package com.xquark.restapi.product;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionTitle;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.*;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.cache.annotation.DoGuavaCache;
import com.xquark.service.cache.constant.CacheKeyConstant;
import com.xquark.service.cache.constant.ExpireAt;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.promotion.PromotionWhiteService;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@Api(value = "限时抢购接口")
@RestController
@RequestMapping("/flashSale")
public class FlashSalePromotionController extends BaseController {

  //白名单魔法值
  private static final Long MAGIC_NUMBER = 9999999L;

  private FlashSalePromotionProductService flashSalePromotionService;

  @Autowired private PromotionWhiteService promotionWhiteService;

  @Autowired private CustomerProfileService customerProfileService;

  @Autowired
  public FlashSalePromotionController(
      FlashSalePromotionProductService flashSalePromotionService) {
    this.flashSalePromotionService = flashSalePromotionService;
  }

  @RequestMapping(value = "/lockerSwitch")
  public ResponseObject<Boolean> lockerSwitch(@RequestParam("switch") String switcher) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    if (StringUtils.equals(switcher, "on")) {
      redisUtils.set(PromotionConstants.FLASH_SALE_LOCKER, 1);
    } else if (StringUtils.equals(switcher, "off")) {
      redisUtils.del(PromotionConstants.FLASH_SALE_LOCKER);
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  @RequestMapping(value = "/lockerStatus")
  public ResponseObject<String> lockerStatus() {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    Integer locker = redisUtils.get(PromotionConstants.FLASH_SALE_LOCKER);
    String ret = "";
    if (locker != null) {
      ret = locker.toString();
    }
    return new ResponseObject<>(ret);
  }

  /**
   * 专区活动标题配置
   * @param saleZoneTitle 专区标题名称
   * @return 操作结果
   */
  @RequestMapping(value = "/saveSaleZoneTitle")
  public ResponseObject<Boolean> saveSaleZoneTitle(String saleZoneTitle) {
    if(StringUtils.isBlank(saleZoneTitle))
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "标题不能为空");
    boolean result = flashSalePromotionService
            .savePromotionTitle(PromotionType.SALEZONE, saleZoneTitle);
    return new ResponseObject<>(result);
  }

  /**
   * 查询活动标题
   * @param promotionType
   * @return
   */
  @RequestMapping(value = "/findSaleZoneTitle")
  public ResponseObject<PromotionTitle> findSaleZoneTitle(PromotionType promotionType) {
    if(promotionType == null)
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "活动类型不能为空");
    PromotionTitle result = flashSalePromotionService
            .selectPromotionTitleByType(promotionType);
    return new ResponseObject<>(result);
  }

  /**
   * 根据批次号查询活动
   * @param batchNo 批次号
   * @return 活动
   */
  @RequestMapping(value = "/findDateByBatchNo", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> findDateByBatchNo(String batchNo){
    Promotion promotion = flashSalePromotionService.findDateByBatchNo(batchNo);
    Map<String, Object> result = new HashMap<>();
    result.put("promotion", promotion);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  @ApiOperation(value = "获取抢购商品列表", notes = "若首页获取则isAvailable为true过滤掉无效商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> listProducts(Integer grandSaleId) {
    return listFlashSale(customerProfileService.getCurrLevelType(), grandSaleId, PromotionType.FLASHSALE);
  }

  /**
   * 首页获取秒杀商品, 按一个活动对应多个商品分组
   */
  @DoGuavaCache(key = CacheKeyConstant.FLASHSALE_GROUP_KEY, expireAt = ExpireAt.SIXTH)
  @RequestMapping(value = "/listGroup", method = RequestMethod.GET)
  public ResponseObject<List<PromotionProductViewVO<Promotion, PromotionProductBasicVO>>> listFlashSaleMulti(Integer grandSaleId) {
    return new ResponseObject<>(listFlashSaleMultiPd(grandSaleId).asJava());
  }

  /**
   * @deprecated 已废弃，后续使用秒杀模块
   */
  @Deprecated
  @RequestMapping(value = "/salezone/list", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listSaleZone() {
      return new ResponseObject<>(Collections.emptyMap());
  }

  @ApiOperation(value = "后台查询所有用户的抢购活动申请", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
  @RequestMapping(value = "/listApply", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listApplyOrderVO(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<FlashSalePromotionOrderVO> list;
    Long count;
    try {
      list = flashSalePromotionService
          .listOrderVO(order, Direction.fromString(direction), pageable);
      count = flashSalePromotionService.countOrderVO();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取抢购申请列表失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/view/{id}")
  @ApiOperation(value = "查看活动商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<FlashSalePromotionProductVO> view(@PathVariable("id") String id) {
    FlashSalePromotionProductVO vo = (FlashSalePromotionProductVO) flashSalePromotionService
        .loadPromotionProductVO(id);
    return new ResponseObject<>(vo);
  }

  @RequestMapping(value = "/viewProduct/{id}")
  @ApiOperation(value = "查看活动商品详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<FlashSalePromotionUserVO> viewProduct(
      @ApiParam(name = "id", value = "通过list接口得到的id") @PathVariable("id") String id) {
    FlashSalePromotionUserVO userVO = flashSalePromotionService
        .loadUserVO(id, getCurrentIUser().getId());
    return new ResponseObject<>(userVO);
  }

  @RequestMapping(value = "/close")
  @ApiOperation(value = "关闭活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> close(@RequestParam("id") String id) {
    boolean result = flashSalePromotionService.close(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/checkInPromotion")
  @ApiOperation(value = "检查商品是否已经参与了活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> inPromotion(String productId) {
    boolean result = flashSalePromotionService.inPromotion(productId);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/audit")
  @ApiOperation(value = "更新申请状态", notes = "必须为后台用户才可以操作", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> updateStatus(@RequestParam("applyId") String applyId,
      @RequestParam("status") String status) {
    IUser user = getCurrentIUser();
    if (!(user instanceof Merchant)) {
      throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "该操作只有管理员可以执行");
    }
    boolean result = flashSalePromotionService.updateStatus(applyId, status);
    return new ResponseObject<>(result);
  }

  /**
   * 秒杀调整后针对活动与商品一对多的情况处理
   * @param grandSaleId 会场id
   *
   */
  @SuppressWarnings("unchecked")
  private Seq<PromotionProductViewVO<Promotion, PromotionProductBasicVO>> listFlashSaleMultiPd(Integer grandSaleId) {
    List<FlashSalePromotionProductVO> list
            = flashSalePromotionService.listProductVOs(customerProfileService.getCurrLevelType(),
            grandSaleId, PromotionType.FLASHSALE);
    return Stream.ofAll(list).groupBy(FlashSalePromotionProductVO::getPromotion)
            .map(t -> {
              final Promotion promotion = t._1;
              final Stream<FlashSalePromotionProductVO> pdVOStream = t._2;
              final List<PromotionProductBasicVO> pds = pdVOStream
                      .map(p -> new PromotionProductBasicVO(p.getPromotionPrice(), p.getAmount(),
                              p.getSales(), p.getPromotionPoint(), p.getPromotionConversionPrice(), p.getProduct()))
                      .asJava();
              return new PromotionProductViewVO(promotion, pds);
            });
  }

  private ResponseObject<Map<String, Object>> listFlashSale(CareerLevelType levelType, Integer grandSaleId, PromotionType promotionType) {
    List<FlashSalePromotionProductVO> list;
    try {
      list = flashSalePromotionService.listProductVOs(levelType, grandSaleId, promotionType);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取秒杀商品列表失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    return new ResponseObject<>(result);
  }

}
