package com.xquark.restapi.product;

import com.xquark.dal.status.FilterSpec;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.LogisticsType;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.dal.vo.SlaveInfoVO;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

public class ProductForm {

  private String id;

  @Size(min = 0, max = 60, message = "{valid.product.name.message}")
  private String name;

  // 滤芯等级
  private Integer level;

  private String encode;

  private String detailH5;//商品h5详情

  private Double height = 0.0;
  private Double width = 0.0;
  private Double length = 0.0;

  private String model;

  private Boolean enableDesc;

  // 商品类型，默认普通商品
  private ProductType type = ProductType.NORMAL;

  // 滤芯规则，默认单品
  private FilterSpec filterSpec = FilterSpec.SINGLE;

  // @Size(min = 0, mafx = 20)
  @NotNull(message = "{valid.notBlank.message}")
  private List<String> imgs;

  private String tagList;

  @NotNull(message = "{valid.notBlank.message}")
  private Integer recommend;

  /*
   * @NotBlank(message = "{valid.notBlank.message}")
   *
   * @Size(min = 0, max = 200) private String descImg;
   */
  @NotNull(message = "{valid.notBlank.message}")
  private ProductStatus status;

  private String description;

  private Long forsaleDate;

  @Valid
  private List<SkuForm> skus;

  @Valid
  private List<SkuMappingForm> skuMappings;

  @Valid
  private List<TagForm> tags;

  @Valid
  private List<SlaveInfoVO> slaves;

  private Date combineValidFrom;

  private Date combineValidTo;

  /**
   * 积分抵扣 可以不填，默认使用全局设置
   */
  private Integer yundouScale;

  /**
   * 最低所需RMB消费
   */
  private BigDecimal minYundouPrice;

  // 商品类别id字段 （可以不填）
  private String category;


  private Boolean supportRefund = true;

  private Integer numInPackage = 0;


  private String wareHouseId;

  private  String deliveryRegion;

  public String getDeliveryRegion() {
    return deliveryRegion;
  }

  public void setDeliveryRegion(String deliveryRegion) {
    this.deliveryRegion = deliveryRegion;
  }

  @NotBlank
  private String supplierId;

  //商品品牌
  private String brand;

  //商品审核状态
  private ProductReviewStatus reviewStatus;

  // 是否延迟发货 （可以不填，默认为0）
  private Integer delayed = 0;

  // 延迟发货时间（天） （delayed = 0的时候才有效）
  private Integer delayAt = 0;

  // 是否跨境(1：是，0：否)
  private Integer isCrossBorder;

  private String skuCode; // 第三方商品唯一编码

  private String skuCodeResources; // 第三方商品编码来源

  private Boolean notSendToErp;

  private Boolean oneyuanPurchase;//是否一元购商品
  private BigDecimal oneyuanPostage; //一元购商品邮费

  private BigDecimal specialRate; //特殊邮费

  private BigDecimal commissionRate; // 分佣比例

  private BigDecimal originalPrice; //成本价

  private String refundAddress;

  private String refundTel;

  private String refundName;

  private LogisticsType logisticsType;

  private BigDecimal point; //商品返利积分

  private BigDecimal deductionDPoint; //可用德分抵扣值

    private BigDecimal netWorth; //净值

  private BigDecimal uniformValue;

  private String templateValue;

  private Boolean gift;

  private BigDecimal serverAmt; // 服务费

  private BigDecimal promoAmt; // 推广费

  private String kind;

  private Integer weight;

  private String attributes; // 商品对应的规格属性id，如颜色，尺码等

  private int selfOperated; //是否自营:0非自营，1自营

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  public String getAttributes() {
    return attributes;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public Boolean getGift() {
    return gift;
  }

  public void setGift(Boolean gift) {
    this.gift = gift;
  }

  public ProductReviewStatus getReviewStatus() {
    return reviewStatus;
  }

  public void setReviewStatus(ProductReviewStatus reviewStatus) {
    this.reviewStatus = reviewStatus;
  }

  public LogisticsType getLogisticsType() {
    return logisticsType;
  }

  public void setLogisticsType(LogisticsType logisticsType) {
    this.logisticsType = logisticsType;
  }

  public BigDecimal getUniformValue() {
    return uniformValue;
  }

  public void setUniformValue(BigDecimal uniformValue) {
    this.uniformValue = uniformValue;
  }

  public String getTemplateValue() {
    return templateValue;
  }

  public void setTemplateValue(String templateValue) {
    this.templateValue = templateValue;
  }

  public String getRefundAddress() {
    return refundAddress;
  }

  public void setRefundAddress(String refundAddress) {
    this.refundAddress = refundAddress;
  }

  public String getRefundTel() {
    return refundTel;
  }

  public void setRefundTel(String refundTel) {
    this.refundTel = refundTel;
  }

  public String getRefundName() {
    return refundName;
  }

  public void setRefundName(String refundName) {
    this.refundName = refundName;
  }

  public BigDecimal getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(BigDecimal originalPrice) {
    this.originalPrice = originalPrice;
  }

  public BigDecimal getCommissionRate() {
    return commissionRate;
  }

  public String getKind() {
    return kind;
  }

  public void setKind(String kind) {
    this.kind = kind;
  }

  public void setCommissionRate(BigDecimal commissionRate) {
    this.commissionRate = commissionRate;
  }

  public BigDecimal getSpecialRate() {
    return specialRate;
  }

  public void setSpecialRate(BigDecimal specialRate) {
    this.specialRate = specialRate;
  }

  public Boolean getOneyuanPurchase() {
    return oneyuanPurchase;
  }

  public void setOneyuanPurchase(Boolean oneyuanPurchase) {
    this.oneyuanPurchase = oneyuanPurchase;
  }

  public BigDecimal getOneyuanPostage() {
    return oneyuanPostage;
  }

  public void setOneyuanPostage(BigDecimal oneyuanPostage) {
    this.oneyuanPostage = oneyuanPostage;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getSkuCodeResources() {
      if (StringUtils.isBlank(skuCodeResources)) {
          return SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("CLIENT").orElseThrow(RuntimeException::new).getName();
      }
    return skuCodeResources;
  }

  public void setSkuCodeResources(String skuCodeResources) {
    this.skuCodeResources = skuCodeResources;
  }

  public Integer getIsCrossBorder() {
    return isCrossBorder;
  }

  public void setIsCrossBorder(Integer isCrossBorder) {
    this.isCrossBorder = isCrossBorder;
  }

  public ProductStatus getStatus() {
    return status;
  }

  public void setStatus(ProductStatus status) {
    this.status = status;
  }


  public List<SkuMappingForm> getSkuMappings() {
    return skuMappings;
  }

  public void setSkuMappings(List<SkuMappingForm> skuMappings) {
    this.skuMappings = skuMappings;
  }

  public List<SkuForm> getSkus() {
    return skus;
  }

  public void setSkus(List<SkuForm> skus) {
    this.skus = skus;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<TagForm> getTags() {
    return tags;
  }

  public void setTags(List<TagForm> tags) {
    this.tags = tags;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Integer getRecommend() {
    return recommend;
  }

  public void setRecommend(Integer recommend) {
    this.recommend = recommend;
  }

  public List<String> getImgs() {
    return imgs;
  }

  public void setImgs(List<String> imgs) {
    this.imgs = imgs;
  }

  /*
   * public String getDescImg() { return descImg; }
   *
   * public void setDescImg(String descImg) { this.descImg = descImg; }
   */

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getForsaleDate() {
    return forsaleDate;
  }

  public void setForsaleDate(Long forsaleDate) {
    this.forsaleDate = forsaleDate;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Integer getDelayed() {
    return delayed;
  }

  public void setDelayed(Integer delayed) {
    this.delayed = delayed;
  }

  public Integer getDelayAt() {
    return delayAt;
  }

  public void setDelayAt(Integer delayAt) {
    this.delayAt = delayAt;
  }

  public Boolean getEnableDesc() {
    return enableDesc;
  }

  public void setEnableDesc(Boolean enableDesc) {
    this.enableDesc = enableDesc;
  }

  public Boolean getNotSendToErp() {
    return notSendToErp;
  }

  public void setNotSendToErp(Boolean notSendToErp) {
    this.notSendToErp = notSendToErp;
  }

  public Integer getYundouScale() {
    return yundouScale;
  }

  public void setYundouScale(Integer yundouScale) {
    this.yundouScale = yundouScale;
  }

  public BigDecimal getMinYundouPrice() {
    return minYundouPrice;
  }

  public void setMinYundouPrice(BigDecimal minYundouPrice) {
    this.minYundouPrice = minYundouPrice;
  }

  public String getEncode() {
    return encode;
  }

  public String getDetailH5() {
    return detailH5;
  }

  public void setDetailH5(String detailH5) {
    this.detailH5 = detailH5;
  }

  public void setEncode(String encode) {
    this.encode = encode;
  }

  public List<String> getTagList() {
    if (StringUtils.isBlank(tagList)) {
      return Collections.emptyList();
    }
    return Arrays.asList(tagList.split(", "));
  }

  public void setTagList(String tagList) {
    this.tagList = tagList;
  }

  public ProductType getType() {
    return type;
  }

  public void setType(ProductType type) {
    this.type = type;
  }

  public FilterSpec getFilterSpec() {
    return filterSpec;
  }

  public void setFilterSpec(FilterSpec filterSpec) {
    this.filterSpec = filterSpec;
  }

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(Double width) {
    this.width = width;
  }

  public Double getLength() {
    return length;
  }

  public void setLength(Double length) {
    this.length = length;
  }


    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public Boolean getSupportRefund() {
    return supportRefund;
  }

  public void setSupportRefund(Boolean supportRefund) {
    this.supportRefund = supportRefund;
  }

  public Integer getNumInPackage() {
    return numInPackage;
  }

  public void setNumInPackage(Integer numInPackage) {
    this.numInPackage = numInPackage;
  }

  public String getWareHouseId() {
    return wareHouseId;
  }

  public void setWareHouseId(String wareHouseId) {
    this.wareHouseId = wareHouseId;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public String getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(String supplierId) {
    this.supplierId = supplierId;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }

  public Date getCombineValidFrom() {
    return combineValidFrom;
  }

  public void setCombineValidFrom(Date combineValidFrom) {
    this.combineValidFrom = combineValidFrom;
  }

  public Date getCombineValidTo() {
    return combineValidTo;
  }

  public void setCombineValidTo(Date combineValidTo) {
    this.combineValidTo = combineValidTo;
  }

  public List<SlaveInfoVO> getSlaves() {
    return slaves;
  }

  public void setSlaves(List<SlaveInfoVO> slaves) {
    this.slaves = slaves;
  }
  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
