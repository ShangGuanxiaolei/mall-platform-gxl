package com.xquark.restapi.withdraw;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.SubAccount;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.WithdrawAdmin;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.bank.WithdrawApplyService;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class WithdrawController extends BaseController {

  @Autowired
  private WithdrawApplyService withdrawApplyService;

  @Autowired
  private AccountApiService accountApiService;

  /**
   * 我的提现，可提现金额
   */
  @ResponseBody
  @RequestMapping("/withdraw/myWithdraw")
  public ResponseObject<Map<String, Object>> myWithdraw() {
    Map<String, Object> params = new HashMap<>();
    String userId = getCurrentIUser().getId();
    SubAccount avalidSubAccount = accountApiService
        .findSubAccountByUserId(userId, AccountType.AVAILABLE);
    // 可提现金额等于可用账户余额
    BigDecimal withdraw = avalidSubAccount.getBalance().setScale(2);
    params.put("withdrawAmount", withdraw);
    return new ResponseObject<>(params);
  }

  /**
   * 提现记录明细
   */
  @ResponseBody
  @RequestMapping("/withdraw/listByApp")
  public ResponseObject<List<WithdrawAdmin>> listByApp(Pageable pageable,
      @RequestParam(value = "status", required = false) String status) {
    Map<String, Object> params = new HashMap<>();
    String userId = getCurrentIUser().getId();
    params.put("userId", userId);
    if (StringUtils.isNotEmpty(status)) {
      params.put("status", status);
    }
    List<WithdrawAdmin> result = withdrawApplyService.listWithdrawApply(params, pageable);
    return new ResponseObject<>(result);
  }

}