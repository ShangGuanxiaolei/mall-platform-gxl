package com.xquark.restapi.user;

import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.freshman.vo.QueryCustomerRelationshipResponseVo;
import com.xquark.service.freshman.vo.RelationshipInfoVo;
import com.xquark.service.user.UserScanService;
import com.xquark.service.user.vo.UserScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/21
 * Time:21:08
 * Des:用户扫码
 */
@Controller
@RequestMapping("user/scan")
public class UserScanController extends BaseController {

    @Autowired
    private UserScanService userScanService;
    @Autowired
    private FreshManService freshManService;

    /**
     * 个人或商家码
     * @param type
     * @return
     */
    @RequestMapping("code/{type}")
    @ResponseBody
    public ResponseObject<UserScan> selfScanCode(@PathVariable("type") String type, Long cpId,
                                                 Long fromCpId,String spId){
        ResponseObject<UserScan> ro = new ResponseObject<UserScan>();
        User user = (User)this.getCurrentIUser();
        if(cpId == null){
            cpId = user.getCpId();
        }
        UserScan  userScan = userScanService.userScan(cpId, fromCpId, type,spId);
        ro.setData(userScan);
        return ro;
    }
    /**
     * 查询结算中心关系
     * @param cpId
     * @return
     */
    @RequestMapping(value = "relation/{cpId}")
    @ResponseBody
    public ResponseObject<RelationshipInfoVo> getCenterRelationShipInfo(@PathVariable("cpId") Long cpId){
        ResponseObject<RelationshipInfoVo> ro = new ResponseObject<RelationshipInfoVo>();
        RelationshipInfoVo vo = new  RelationshipInfoVo();
        QueryCustomerRelationshipResponseVo response = this.freshManService.queryCustomerRelationship(cpId);
        if(response != null ){
            vo.setCpId(cpId);
            vo.setUpLine(response.getUpline());
            vo.setRemark("数据来源于结算中心");
            int level = response.getResCode();
            vo.setLevel(level);
            vo.setLevelName(getRelationshipLevelName(level));
        }
        ro.setData(vo);
        return ro;
    }

    private String getRelationshipLevelName(int level) {
        switch (level){
            case 10:
                return "cpId不存在";
            case 11:
                return "弱关系";
            case 12:
                return "次强关系";
            case 13:
                return "强关系";
        }
        return "关系未知";
    }
}