package com.xquark.restapi.order;


import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * 订单发货Form表单逻辑
 *
 * @author odin
 */
public class OrderShippedForm extends OrderForm {

  /**
   * 物流公司
   */
  // @NotNull(message = "{logisticsCompany.notBlank.message}")
  @ApiModelProperty(value = "物流公司")
  private String logisticsCompany;

  /**
   * 物流订单号
   */
  // @NotBlank(message = "{logisticsOrderNo.notBlank.message}")
  @ApiModelProperty(value = "物流单号")
  private String logisticsOrderNo;

  /**
   * 防伪物流码
   */
  private String logisticsAntifake;

  public String getLogisticsAntifake() {
    return logisticsAntifake;
  }

  public void setLogisticsAntifake(String logisticsAntifake) {
    this.logisticsAntifake = logisticsAntifake;
  }

  public String getLogisticsOrderNo() {
    return logisticsOrderNo;
  }

  public void setLogisticsOrderNo(String logisticsOrderNo) {
    this.logisticsOrderNo = logisticsOrderNo;
  }

  public String getLogisticsCompany() {
    return logisticsCompany;
  }

  public void setLogisticsCompany(String logisticsCompany) {
    this.logisticsCompany = logisticsCompany;
  }
}
