package com.xquark.restapi.product;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.vo.ProductCollectionVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.product.ProductCollectionService;
import com.xquark.service.product.VipProductService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionSkuGiftService;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@Api(value = "productCollection", description = "商品收藏")
public class ProductCollectionController extends BaseController {

  @Autowired
  private ProductCollectionService productCollectionService;
  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;
  @Autowired
  private SkuMapper skuMapper;
  @Autowired
  private PromotionSkusService promotionSkusService;
  @Autowired
  private PromotionSkuGiftService promotionSkuGiftService;

  @Autowired
  private FreshManService freshManService;

  @Autowired
  private VipProductService vipProductService;

  /**
   * 用户收藏商品列表
   */
  @ResponseBody
  @RequestMapping("/productCollection/list")
  @ApiIgnore
  public ResponseObject<Map<String, Object>> list(Pageable pageable) {
    Map<String, Object> params = new HashMap<String, Object>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<ProductCollectionVO> result = productCollectionService.list(pageable, params);
    Long total = productCollectionService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<String, Object>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<Map<String, Object>>(aRetMap);

  }

  /**
   * 删除用户收藏商品
   */
  @ResponseBody
  @RequestMapping(value = "/productCollection/delete", method = RequestMethod.POST)
  @ApiOperation(value = "用户取消收藏某个商品", notes = "用户取消收藏某个商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> delete(@RequestParam("productId") String productId) {
    String userId = getCurrentIUser().getId();
    int result = productCollectionService.deleteByProductId(userId, productId);
    return new ResponseObject<Boolean>(result > 0);
  }

  /**
   * 查询用户是否已经有该商品已经收藏过
   */
  @ResponseBody
  @RequestMapping(value = "/productCollection/check", method = RequestMethod.POST)
  @ApiOperation(value = "查询用户是否已经有该商品已经收藏过", notes = "查询用户是否已经有该商品已经收藏过", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> check(@RequestParam("productId") String productId) {
    String userId = getCurrentIUser().getId();
    ProductCollection collection = productCollectionService.selectByProductId(userId, productId);
    if (collection != null) {
      return new ResponseObject<Boolean>(true);
    } else {
      return new ResponseObject<Boolean>(false);
    }
  }

  /**
   * 保存用户收藏商品
   */
  @ResponseBody
  @RequestMapping(value = "/productCollection/save", method = RequestMethod.POST)
  @ApiOperation(value = "用户收藏某个商品", notes = "用户收藏某个商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> save(@RequestParam("productId") String productId) {
    String userId = getCurrentIUser().getId();

      // FIXME 针对分享的VIP订单, VIP商品不能收藏
      List<VipProduct> vipProducts = vipProductService.listVipProducts();
      List<String> vipProductIds = null;
      if (org.apache.commons.collections.CollectionUtils.isNotEmpty(vipProducts)) {
          vipProductIds = new ArrayList<>();
          for (VipProduct vipProduct : vipProducts) {
              vipProductIds.add(vipProduct.getProductId());
          }
      }
      if (org.apache.commons.collections.CollectionUtils.isNotEmpty(vipProductIds)
              && StringUtil.isNotEmpty(productId)) {
        if (vipProductIds.contains(productId)) {
          ResponseObject res = new ResponseObject<>();
          res.setMoreInfo("VIP商品不能收藏呦...");
          return res;
        }
      }

    int result = 0;
    ProductCollection collectionExists =
        productCollectionService.selectByProductId(userId, productId);
    if (collectionExists != null) {
      // 商品已经收藏过
      return new ResponseObject<>(Boolean.TRUE);
    }

    List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectExsitSalesPromotionByProductId(productId);
    //是否是促销商品
    //促销折扣商品限购数量校验,购物车该商品的数量
    if (CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)) {
      if (promotion4ProductSalesVOS.get(0).getEffectTo().before(new Date())) {
        ResponseObject res = new ResponseObject<>();
        res.setMoreInfo("商品活动已结束");
        return res;
      }
      if (promotion4ProductSalesVOS.get(0).getEffectFrom().after(new Date())) {
        ResponseObject res = new ResponseObject<>();
        res.setMoreInfo("商品活动未开始");
        return res;
      }
    }

    ProductCollection collection = new ProductCollection();
    collection.setUserId(userId);
    collection.setProductId(productId);
    collection.setArchive(false);
    result = productCollectionService.insert(collection);

    if (result == 1) {
      return new ResponseObject<Boolean>(true);
    } else {
      return new ResponseObject<Boolean>(false);
    }
  }

  /**
   * 客户端用户我的收藏商品列表
   */
  @ResponseBody
  @RequestMapping(value = "/productCollection/listByApp", method = RequestMethod.POST)
  @ApiOperation(value = "我的商品收藏列表", notes = "我的商品收藏列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<ProductCollectionVO>> listByApp(Pageable pageable) {
    String userId = getCurrentIUser().getId();
    List<ProductCollectionVO> result = productCollectionService.listByApp(pageable, userId);
    List<ProductCollectionVO> newResult =this.convretCartItems(result);
    return new ResponseObject<List<ProductCollectionVO>>(newResult);
  }

  /**
   * 为订单商品增加赠品信息
   * @param collectItems
   */
  private List<ProductCollectionVO> convretCartItems(List<ProductCollectionVO> collectItems) {
    if (org.apache.commons.collections.CollectionUtils.isEmpty(collectItems)) {
      return collectItems;
    }

    //为提交订单接口准备数据：参加大会售卖活动的skuId
    List<String> promotionSkuIds = new ArrayList<>();
    for (ProductCollectionVO collectItem : collectItems) {
      String productId = collectItem.getProductId();

      //是否是新人专区商品
      FreshManProductVo f = this.freshManService.selectFreshmanProductById(productId);
      if(f != null && StringUtils.isNotBlank(f.getProductId()))
        collectItem.setFreshmanProduct(true);
      else {
        collectItem.setFreshmanProduct(false);
      }

      //Sku sku = skuMapper.selectByPrimaryKey(productId);
      List<Sku> skus = skuMapper.selectByProductId(productId);
      if(CollectionUtils.isEmpty(skus)) {
        continue;
      }
      Sku sku = skus.get(0);
      //根据商品sku获取活动信息
      List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
      if (CollectionUtils.isEmpty(promotionBaseInfos)) {
        continue;
      }
      String pCode = promotionBaseInfos.get(0).getpCode();

      //检查这个商品有无活动，没有则认为是没有赠品
      List<PromotionSkuVo> promotionSkuVos = promotionSkusService.getPromotionSkus(sku.getSkuCode());
      if (CollectionUtils.isEmpty(promotionSkuVos)) {
        continue;
      }
      //TODO 加活动类型判断
      //if(杭州大会活动){
      promotionSkuIds.add(sku.getId());
      PromotionSkuVo promotionSkuVo = promotionSkuVos.get(0);//TODO 变为get(i)

      //检查这个活动商品有没有赠品，
      List<PromotionSkuGift> giftList = promotionSkuGiftService.getPromotionSkuGiftList(pCode, promotionSkuVo.getSkuCode());
      if (CollectionUtils.isEmpty(giftList)) {
        continue;
      }
      //step1. 设置有无赠品
      collectItem.setHadGift(true);
      //买赠信息 （目前为买gift赠1）
      int gift = promotionSkuVo.getGift();

      String str = "";
      switch (gift) {
        case 0:
          str = "";
          break;
        case 1:
          str = "一";
          break;
        case 2:
          str = "二";
          break;
        case 3:
          str = "三";
          break;
        case 4:
          str = "四";
          break;
        case 5:
          str = "五";
          break;
        case 6:
          str = "六";
          break;
        case 7:
          str = "七";
          break;
        case 8:
          str = "八";
          break;
        case 9:
          str = "九";
          break;
        case 10:
          str = "十";
          break;

      }
      //设置赠品数量
      collectItem.setGiftType("买" + str + "赠一");
    }
    return collectItems;
    }

}
