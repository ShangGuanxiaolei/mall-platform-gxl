package com.xquark.restapi.logisticsPlatform;

import com.xquark.dal.IUser;
import com.xquark.dal.mapper.SupplierMapper;
import com.xquark.dal.model.Supplier;
import com.xquark.openapi.scrm.PolyControllerApi;
import com.xquark.restapi.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** author: liuwei Date: 18-9-18. Time: 下午5:30 */
@RestController
public class LogisticsPlatformController extends BaseController {

  private static final Logger LOGGER = LoggerFactory.getLogger(PolyControllerApi.class);

  @Autowired SupplierMapper supplierMapper;

  @RequestMapping(
      value = "/logistics/auth",
      consumes = "application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  public String LogisticsSign(
      @RequestParam(required = false) boolean debug,
      HttpServletRequest request,
      HttpServletResponse response)
      throws Exception {
    LOGGER.info("========== 开始验证签名 ===========");

    String method = getParameter("method", request);

    IUser user = getCurrentIUser();
    if (user == null) {
      return "{\"code\": \"400002\", \"message\": \"System Error\",\"subcode\": "
          + "\"GSE.SYSTEM_ERROR\",\"submessage\": \"请先使用供应商帐号登录\"}";
    }
    Supplier supplier = supplierMapper.getSupplierByName(user.getName());
    request.setAttribute("supplier", supplier);

    if (method.equalsIgnoreCase("Differ.JH.Business.GetOrder")) {
      LOGGER.info("========== 转发到订单同步 ==========");
      request.getRequestDispatcher("/logistics/order").forward(request, response);
      return null;
    } else if (method.equalsIgnoreCase("Differ.JH.Business.Send")) {
      LOGGER.info("========== 转发到订单发送 ==========");
      request.getRequestDispatcher("/logistics/ordersend").forward(request, response);
      return null;
    } else if (method.equalsIgnoreCase("Differ.JH.Business.DownloadProduct")) {
      LOGGER.info("========== 转发到商品下载 ==========");
      request.getRequestDispatcher("/logistics/download/product").forward(request, response);
      return null;
    } else if (method.equalsIgnoreCase("Differ.JH.Business.SyncStock")) {
      LOGGER.info("========== 转发到商品库存同步 ==========");
      request.getRequestDispatcher("/logistics/sync/stock").forward(request, response);
      return null;
    } else if (method.equalsIgnoreCase("Differ.JH.Business.SyncStockBatch")) {
      LOGGER.info("========== 转发到批量商品库存同步 ========");
      request.getRequestDispatcher("/logistics/sync/stock/batch").forward(request, response);
      return null;
    }
    return "{\"code\": \"10000\", \"message\": \"SUCCESS\",\"subcode\": "
        + "\"SUCCESS\",\"submessage\": \"签名验证成功\"}";
  }

  private static String getParameter(String name, HttpServletRequest request) {
    return StringUtils.defaultString(request.getParameter(name));
  }
}
