package com.xquark.restapi.product;

import java.util.Date;
import java.util.List;

/**
 * 组合套装
 * @author wangxinhua.
 * @date 2018/9/28
 */
public class CombineForm {

  private String masterId;

  private List<String> slaveIds;

  private Date validFrom;

  private Date validTo;

  public String getMasterId() {
    return masterId;
  }

  public void setMasterId(String masterId) {
    this.masterId = masterId;
  }

  public List<String> getSlaveIds() {
    return slaveIds;
  }

  public void setSlaveIds(List<String> slaveIds) {
    this.slaveIds = slaveIds;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

}
