package com.xquark.restapi.product;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFlashSale;
import com.xquark.helper.Transformer;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by wangxinhua on 17-11-30. DESC:
 */
public class FlashSaleForm {

  private String id;

  private String promotionId;

  @NotBlank(message = "活动标题不能为空")
  @ApiModelProperty(value = "title", required = true)
  private String title;

  @NotBlank(message = "活动图片不能为空")
  @ApiModelProperty(value = "img", required = true)
  private String img;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "validFrom", required = true)
  @NotNull(message = "活动开始日期不能为空")
  private Date validFrom;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "validTo", required = true)
  @NotNull(message = "活动结束日期不能为空")
  private Date validTo;

  @NotBlank(message = "商品id不能为空")
  @ApiModelProperty(value = "productId", required = true)
  private String productId;

  @NotNull(message = "抢购价格不能为空")
  @ApiModelProperty(value = "discount", required = true)
  private BigDecimal discount;

  @NotNull(message = "预订库存不能为空")
  @ApiModelProperty(value = "amount", required = true)
  private Long amount;

  private Long limitAmount;

  private Boolean isPointUsed;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public Long getLimitAmount() {
    return limitAmount;
  }

  public void setLimitAmount(Long limitAmount) {
    this.limitAmount = limitAmount;
  }

  public PromotionFlashSale getPromotinProduct() {
    return Transformer.fromBean(this, PromotionFlashSale.class);
  }

  public Boolean getIsPointUsed() {
    return isPointUsed;
  }

  public void setIsPointUsed(Boolean pointUsed) {
    isPointUsed = pointUsed;
  }
}
