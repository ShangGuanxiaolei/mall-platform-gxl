package com.xquark.restapi.product;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductDesc;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.product.ProductDescService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class ProductDescController extends BaseController {

  @Autowired
  private ProductDescService productDescService;
  @Autowired
  private ProductService productService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private UrlHelper urlHelper;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private ProdSyncMapper prodSyncMapper;

  @Autowired
  private ActivityService activityService;
  @Autowired
  private FragmentService fragmentService;
  @Autowired
  private FragmentImageService fragmentImageService;
  @Value("${xiangqu.cart.host.url}")
  String xiangquCartHost;
  @Value("${site.web.host.name}")
  String webHostName;


  /**
   * 编辑商品的富文本信息
   */
  @ResponseBody
  @RequestMapping("/product/saveProductDesc")
  public ResponseObject<Boolean> createProductDesc(@ModelAttribute ProductDesc productDesc) {
    int rc = 0;
    if (StringUtils.isBlank(productDesc.getId())) {
      rc = productDescService.insert(productDesc);
    } else {
      rc = productDescService.update(productDesc);
    }

    return new ResponseObject<>(rc > 0);
  }

  /**
   * 修改product enable_desc字段  开启/关闭富文本
   */
  @ResponseBody
  @RequestMapping("/product/changeEnableDesc")
  public ResponseObject<Product> changeEnableDesc(@RequestParam String productId,
      @RequestParam Boolean enableDesc) {
    Product p = new Product();
    p.setId(productId);
    p.setEnableDesc(enableDesc);
    int rc = productService.updateEnableDesc(p);
    Product pd = null;
    if (rc > 0) {
      pd = productService.load(productId);
      return new ResponseObject<>(pd);
    }

    return new ResponseObject<>(pd);
  }

}
