package com.xquark.restapi.im;

import java.math.BigDecimal;
import java.util.List;

import com.xquark.dal.status.OrderStatus;

public class ImOrderVO {

  private String sellerId;
  private String orderId;
  private String orderNo;
  private String shopId;
  private Integer totalNum;
  private OrderStatus orderSatus;
  private BigDecimal price;
  private List<OrderItemEx> orderItems;
  private String sellerName;

  public String getSellerId() {
    return sellerId;
  }

  public void setSellerId(String sellerId) {
    this.sellerId = sellerId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Integer getTotalNum() {
    return totalNum;
  }

  public void setTotalNum(Integer totalNum) {
    this.totalNum = totalNum;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public OrderStatus getOrderSatus() {
    return orderSatus;
  }

  public void setOrderSatus(OrderStatus orderSatus) {
    this.orderSatus = orderSatus;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public List<OrderItemEx> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(List<OrderItemEx> orderItems) {
    this.orderItems = orderItems;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

}
