package com.xquark.restapi.activity;

import com.xquark.restapi.ResponseObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author jim
 * @description ExceptionTestController
 * 提供给测试人员，上线后可以删除
 * @date 2019-01-28、15:09
 *
 */
@RestController
@RequestMapping("/exceptionCtrl")
@Deprecated
public class ExceptionTestController {

    private static final Log logger = LogFactory.getLog(ActivityExclusiveController.class);

    @ResponseBody
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseObject<Integer> exceptionTest(@RequestParam("param") String param) throws Throwable{
        logger.info("*********exceptionController,param:"+param);
        switch (param) {
            case "exception1" : throw new Exception("throw Exception1");
            case "exception2" : throw new Exception("throw Exception2");
        }
        return new ResponseObject<>();
    }
}
