package com.xquark.restapi.logistics;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.domain.AlipayItemAuditRule;
import com.vdlm.common.lang.CollectionUtil;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.AddressMapper;
import com.xquark.dal.mapper.CustomerAddressMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.SfAppConfigMapper;
import com.xquark.dal.mapper.SystemRegionMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.CustomerAddress;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.model.Sku;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.vo.OrderItemsDal;
import com.xquark.dal.vo.SFOrderLogDal;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.SfRegionService;
import com.xquark.service.order.impl.AsyncSubmitOrder;
import com.xquark.service.order.vo.OrderAddressVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.vo.*;
import com.xquark.service.wms.WmsExpressService;
import com.xquark.utils.StringUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;

import java.util.*;

import com.xquark.utils.unionpay.HttpClient;
import org.apache.commons.lang.StringUtils;
import org.neo4j.cypher.internal.compiler.v2_1.ast.Add;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: kong Date: 2018/6/29. Time: 18:02 顺丰
 */
@RestController
@RequestMapping("/logistics")
public class LogisticsGoodsController {

  @Autowired
  private SfAppConfigMapper sfConfigMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private AddressMapper addressMapper;

  @Autowired
  private LogisticsGoodsService logisticsGoodsService;

  @Autowired
  private WmsExpressService wmsExpressService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private OrderService orderService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private ProductService productService;

  @Autowired
  private SfRegionService sfRegionService;

  @Autowired
  private SystemRegionMapper systemRegionMapper;
    /**
     * 功能描述: 物流查询老接口
     * */
    @RequestMapping("/queryOrderLogInfo")
    public ResponseObject<List<SFOrderLog>> queryOrderLogInfo(String orderNo, String dest) {
        ResponseObject<List<SFOrderLog>> responseObject = new ResponseObject<>();
        List<SFOrderLog> sfOrderLogs = null;
        if (orderNo == null || dest == null) {
            return responseObject;
        }
        try {
            if (("SF_REFUNDABLE").equals(dest) || "SF_NOT_REFUNDABLE".equals(dest)) {
                SFOrderCheck sfOrderCheck = logisticsGoodsService.getOrderDetail(orderNo);
                String billSn = sfOrderCheck.getOrderItems().get(0).getShippingSn();
                sfOrderLogs = logisticsGoodsService.getSFOrderLog(orderNo, billSn);
                responseObject.setData(sfOrderLogs);
                return responseObject;
            } else if("CLIENT_REFUNDABLE".equals(dest)|| "CLIENT_NOT_REFUNDABLE".equals(dest)) {
                List<SFOrderLogDal> sfOrderLogDals = wmsExpressService.getRouteByOrderNo(orderNo);
                sfOrderLogs = new ArrayList<>();
                for (SFOrderLogDal s : sfOrderLogDals) {
                    SFOrderLog sfOrderLog = new SFOrderLog();
                    sfOrderLog.setTime(s.getTime().getTime() / 1000 + "");
                    sfOrderLog.setCommont(s.getCommont());
                    sfOrderLogs.add(sfOrderLog);
                }
                responseObject.setData(sfOrderLogs);
                return responseObject;
            }else{
                //查询第三方物流信息
                Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
                String billNo = (String)waybillMap.get("logistics_order_no");
                //运单号可能为多个，由逗号分隔
                String type = (String)waybillMap.get("logistics_company_type");
                String logisticsCompany =(String)waybillMap.get("logistics_company");
                String phone = (String)waybillMap.get("phone");
                //取收件人手机号后四位
                String end4Phone ="";

                if(StringUtil.isNotNull(phone)){
                    end4Phone=phone.substring(phone.length()-4);
                }
                List<SFOrderLog> thirdLogistics = new ArrayList<>();
                if(StringUtil.isNotNull(logisticsCompany)&&logisticsCompany.contains("顺丰")){
                    //如果是顺丰 运单号+ : + 收件人手机号后四位
                    thirdLogistics = logisticsGoodsService.getThirdLogistics(billNo+":"+end4Phone,type);

                }else{
                    thirdLogistics = logisticsGoodsService.getThirdLogistics(billNo,type);

                }

                if(CollectionUtil.isNotEmpty(thirdLogistics)){
                    responseObject.setData(thirdLogistics);
                    return responseObject;
                }
            }
        } catch (Exception e) {
            log.error("路由查询错误", e);
        }
        responseObject.setData(sfOrderLogs);
        return responseObject;
    }


  /**
   * 功能描述: 物流查询新接口 支持多运单物流信息
   * @author Luxiaoling
   * @date 2019/2/25 11:48
   * @param  orderNo, dest
   * @return  com.xquark.restapi.ResponseObject<java.util.List<com.xquark.service.vo.MultiWaybillInfo>>
   */
  @RequestMapping("/queryLogisticsInfo")
  public ResponseObject<List<MultiWaybillInfo>> queryLogisticsInfo(String orderNo, String dest) {
    ResponseObject<List<MultiWaybillInfo>> responseObject = new ResponseObject<>();
    List<SFOrderLog> sfOrderLogs = null;
    List<MultiWaybillInfo> multiWaybillInfoList =null;
    if (orderNo == null || dest == null) {
      return responseObject;
    }
    try {
      if (("SF_REFUNDABLE").equals(dest) || "SF_NOT_REFUNDABLE".equals(dest)) {
        SFOrderCheck sfOrderCheck = logisticsGoodsService.getOrderDetail(orderNo);
        String billSn = sfOrderCheck.getOrderItems().get(0).getShippingSn();
        sfOrderLogs = logisticsGoodsService.getSFOrderLog(orderNo, billSn);
        MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
        multiWaybill.setWayBillNo(billSn);
        multiWaybill.setWayBillInfo(sfOrderLogs);
        multiWaybillInfoList= new ArrayList<>();
        multiWaybillInfoList.add(multiWaybill);
        responseObject.setData(multiWaybillInfoList);
        return responseObject;
      } else if("CLIENT_REFUNDABLE".equals(dest)|| "CLIENT_NOT_REFUNDABLE".equals(dest)) {
        List<SFOrderLogDal> sfOrderLogDals = wmsExpressService.getRouteByOrderNo(orderNo);
        sfOrderLogs = new ArrayList<>();
        for (SFOrderLogDal s : sfOrderLogDals) {
          SFOrderLog sfOrderLog = new SFOrderLog();
          sfOrderLog.setTime(s.getTime().getTime() / 1000 + "");
          sfOrderLog.setCommont(s.getCommont());
          sfOrderLogs.add(sfOrderLog);
        }
        Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
        String billNo = (String)waybillMap.get("logistics_order_no");
        MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
        multiWaybill.setWayBillNo(billNo);
        multiWaybill.setWayBillInfo(sfOrderLogs);
        multiWaybillInfoList= new ArrayList<>();
        multiWaybillInfoList.add(multiWaybill);
        responseObject.setData(multiWaybillInfoList);
        return responseObject;
      }else{
        //查询第三方物流信息
        Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
        String billNo = (String)waybillMap.get("logistics_order_no");
        //运单号可能为多个，由逗号分隔
        List<String> billNoList = Arrays.asList(billNo.split(","));
        String type = (String)waybillMap.get("logistics_company_type");
        String logisticsCompany =(String)waybillMap.get("logistics_company");
        String phone = (String)waybillMap.get("phone");
        //取收件人手机号后四位
          String end4Phone ="";

          if(StringUtil.isNotNull(phone)){
              end4Phone=phone.substring(phone.length()-4);
          }

        multiWaybillInfoList= new ArrayList<>();
        for (String wayBillNo : billNoList) {
          //分别调用阿里云市场物流接口查询物流信息
            List<SFOrderLog> thirdLogistics = new ArrayList<>();
            if(StringUtil.isNotNull(logisticsCompany)&&logisticsCompany.contains("顺丰")){
                //如果是顺丰 运单号+ : + 收件人手机号后四位
                thirdLogistics = logisticsGoodsService.getThirdLogistics(wayBillNo+":"+end4Phone,type);

            }else{
                thirdLogistics = logisticsGoodsService.getThirdLogistics(wayBillNo,type);

            }
          MultiWaybillInfo multiWaybill = new MultiWaybillInfo();
          multiWaybill.setWayBillNo(wayBillNo);
          multiWaybill.setWayBillInfo(thirdLogistics);
          multiWaybillInfoList.add(multiWaybill);
        }

        responseObject.setData(multiWaybillInfoList);
        return responseObject;
      }
    } catch (Exception e) {
      log.error("路由查询错误", e);
    }
    responseObject.setData(multiWaybillInfoList);
    return responseObject;
  }


  @RequestMapping("/getOrderList")
  public ResponseObject<JSONObject> find(String orderNo) {
    ResponseObject<JSONObject> responseObject = new ResponseObject<>();
    String err = "";
    try {
      SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.SELECT_ORDER, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      err = "查询不到该订单";
      Order order = orderMapper.getLogsticsByOrderNo(orderNo);
      JSONObject jsonObject = new JSONObject();
      responseObject.setMoreInfo("获取成功");
      jsonObject.put("page", 1);
      jsonObject.put("pageSize", 9);
      jsonObject.put("queryParam", order.getPartnerOrderNo());
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String content = res.getContent();
      err = "解析返回数据出错";
      JSONObject jsonValue = JSONObject.parseObject(content).getJSONObject("data");
      JSONArray jsonArray = jsonValue.getJSONArray("orderItems");
      String shippingSn = jsonArray.getJSONObject(0).getString("shippingSn");
      Order orderLoc = new Order();
      orderLoc.setOrderNo(orderNo);
      orderLoc.setLogisticsOrderNo(shippingSn);
      err = "更新物流单号出错";
      orderMapper.updateOrderByOrderNo(orderLoc);
      responseObject.setData(jsonValue);
    } catch (Exception e) {
      e.printStackTrace();
      responseObject.setMoreInfo(err);
    }
    return responseObject;
  }

  @RequestMapping("/getNotice")
  public ResponseObject<JSONObject> get() {
    ResponseObject<JSONObject> responseObject = new ResponseObject<>();
    String err = "";
    try {
//      List<Address> addresses =addressMapper.getAddressById(696+"");
      SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.GET_NOTICE, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      err = "发送请求发生错误";
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, null);
      String content = res.getContent();
      err = "解析返回数据发生错误";
      List<String> orderSns = parseContent(content);
//      JSONObject jsonObject = JSONObject.parseObject(content);
//      responseObject.setData(jsonObject);
      if (orderSns.size() == 0) {
        responseObject.setMoreInfo("沒有数据返回！");
        return responseObject;
      }
      orderSns = orderMapper.getOrderNoByParentNo(orderSns);
      err = "获取订单信息发生错误";
      for (String s : orderSns) {
        find(s);
      }
    } catch (Exception e) {
      e.printStackTrace();
      responseObject.setMoreInfo(err);
    }
    return responseObject;
  }

  public List<String> parseContent(String url) {
    JSONObject jsonObject = JSONObject.parseObject(url);
    JSONArray jsonArray = jsonObject.getJSONArray("data");
    List<String> orderSns = new ArrayList<>();
    for (int i = 0; i < jsonArray.size(); i++) {
      JSONArray orderSn = null;
      try {
        orderSn = jsonArray.getJSONObject(i).getJSONObject("msg_body").getJSONArray("order_sn");
        if (jsonArray.getJSONObject(i).getInteger("msg_type") == 2) {
          for (int j = 0; j < orderSn.size(); j++) {
            orderSns.add(orderSn.getString(j));
          }
        }
      } catch (Exception e) {
        JSONObject orderObj = jsonArray.getJSONObject(i).getJSONObject("msg_body");
        if (jsonArray.getJSONObject(i).getInteger("msg_type") == 2) {
          String orderTo = jsonArray.getJSONObject(i).getJSONObject("msg_body")
              .getString("order_sn");
          orderSns.add(orderTo);
        }
      }
    }
    return orderSns;
  }

  @RequestMapping("/getStock")
  @ResponseBody
  public ResponseObject<JSONObject> getSFProduct() {
    ResponseObject<JSONObject> responseObject = new ResponseObject<JSONObject>();
    List<LogisticsStockParam> params = new ArrayList<>();
    LogisticsStockParam param = new LogisticsStockParam();
    param.setBomSn("");
    param.setProductid(282629L);
    param.setProductNum(1);
    param.setRegionId(866);
    param.setSfairline(0);
    param.setSfshipping(10);
    params.add(param);
    List<LogisticsStock> logisticsStock = logisticsGoodsService.getStock(params);
    return responseObject;
  }

  @RequestMapping("/getShipping")
  @ResponseBody
  public ResponseObject<JSONObject> getShipping() {
    ResponseObject<JSONObject> responseObject = new ResponseObject<JSONObject>();
    ShippingGoodsAllParam shippingGoodsAllParam = new ShippingGoodsAllParam();
    shippingGoodsAllParam.setUserrank(10);
    List<ShippingGoodsParam> shippingGoodsParams = new ArrayList<>();
    ShippingGoodsParam shippingGoodsParam = new ShippingGoodsParam();
    shippingGoodsParam.setSellPrice(2600.0);
    shippingGoodsParam.setProductSn(9300201442L + "");
    shippingGoodsParam.setProductIndex(1500020598L + "");
    shippingGoodsParam.setWeight(50000.0);
    shippingGoodsParam.setMerchantNumber(16147 + "");
    shippingGoodsParam.setRegionId(2);
    shippingGoodsParam.setCityId(50);
    shippingGoodsParam.setSfshipping(100 + "");
    shippingGoodsParam.setProductId(201442l);
    shippingGoodsParams.add(shippingGoodsParam);
    shippingGoodsAllParam.setShippingGoodsParams(shippingGoodsParams);
    ShippingGoodsAll shippingGoodsAlls = logisticsGoodsService
        .getShippingFee(shippingGoodsAllParam);
    return responseObject;
  }

  @RequestMapping("/findSubOrder")
  @ResponseBody
  public ResponseObject<List<SubOrder>> findSubOrder(String orderNo) {
    ResponseObject<List<SubOrder>> responseObject = new ResponseObject<>();
    List<SubOrder> subOrders = null;
    try {
      subOrders = logisticsGoodsService.findSubOrder(orderNo);
    } catch (Exception e) {
      log.error("查询子订单失败", e);
    }
    responseObject.setData(subOrders);
    return responseObject;
  }

  @RequestMapping("/getOrderDetail")
  @ResponseBody
  public ResponseObject<List<OrderItems>> orderDtail(String orderNo, String dest) {
    ResponseObject<List<OrderItems>> responseObject = new ResponseObject<>();
    List<OrderItems> orderItems = new ArrayList<>();
    if (orderNo == null || dest == null) {
      return responseObject;
    }
    try {
      if (dest.equals("SF_REFUNDABLE") || dest.equals("SF_NOT_REFUNDABLE")) {
        List<SubOrder> subOrders = logisticsGoodsService.findSubOrder(orderNo);
        if (subOrders.size() == 0) {
          SubOrder subOrder = new SubOrder();
          subOrder.setOrderSn(orderNo);
          subOrders.add(subOrder);
        }
        for (SubOrder s : subOrders) {
          SFOrderCheck sfOrderCheck = logisticsGoodsService.getOrderDetail(s.getOrderSn());
          orderItems.add(sfOrderCheck.getOrderItems().get(0));
        }
      }else if("CLIENT_REFUNDABLE".equals(dest)|| "CLIENT_NOT_REFUNDABLE".equals(dest)) {
        List<OrderItemsDal> orderItemsDals = productMapper.getLocOrderItems(orderNo);
        OrderItems orderItems1 = new OrderItems();
        List<OrderProduct> orderProducts = new ArrayList<>();
        for (OrderItemsDal o : orderItemsDals) {
          OrderProduct orderProduct = new OrderProduct();
          orderItems1.setShippingTypeName(o.getShippingTypeName());
          orderItems1.setOrderSn(o.getOrderSn());
          orderItems1.setShippingStatusStr(o.getShippingStatusStr());
          orderItems1.setShippingSn(o.getShippingSn());
          orderProduct.setProductImg(o.getImg());
          orderProducts.add(orderProduct);
        }
        checkStatus(orderItems1);
        orderItems.add(orderItems1);
        orderItems1.setSfOrderProducts(orderProducts);
      }else {
        //第三方物流头信息
        Map waybillMap = logisticsGoodsService.getbillNoByOrderNo(orderNo);
        String billNo = (String)waybillMap.get("logistics_order_no");
        //运单号可能为多个，由逗号分隔
        if(StringUtil.isNotNull(billNo)){
          List<String> billNoList = Arrays.asList(billNo.split(","));
          List<OrderItemsDal> orderItemsDals = productMapper.getLocOrderItems(orderNo);
          //拆分的物流头信息，只有运单号不一致,其他的物流公司，图片，订单号，运单状态都是一致的
          for (String wayBillNo : billNoList) {
            List<OrderProduct> orderProducts = new ArrayList<>();
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setProductImg(orderItemsDals.get(0).getImg());
            orderProducts.add(orderProduct);
            OrderItems orderItem = new OrderItems();
            orderItem.setSfOrderProducts(orderProducts);
            orderItem.setOrderSn(orderNo);
            orderItem.setShippingSn(wayBillNo);
            orderItem.setShippingStatusStr(orderItemsDals.get(0).getShippingStatusStr());
            orderItem.setShippingTypeName(orderItemsDals.get(0).getShippingTypeName());
            checkStatus(orderItem);
            orderItems.add(orderItem);
          }
        }else{
          //没有运单号表示没有发货 按照原有的逻辑查询
          List<OrderItemsDal> orderItemsDals = productMapper.getLocOrderItems(orderNo);
          OrderItems orderItems1 = new OrderItems();
          List<OrderProduct> orderProducts = new ArrayList<>();
          for (OrderItemsDal o : orderItemsDals) {
            OrderProduct orderProduct = new OrderProduct();
            orderItems1.setShippingTypeName(o.getShippingTypeName());
            orderItems1.setOrderSn(o.getOrderSn());
            orderItems1.setShippingStatusStr(o.getShippingStatusStr());
            orderItems1.setShippingSn(o.getShippingSn());
            orderProduct.setProductImg(o.getImg());
            orderProducts.add(orderProduct);
          }
          checkStatus(orderItems1);
          orderItems.add(orderItems1);
          orderItems1.setSfOrderProducts(orderProducts);

        }

      }
    } catch (Exception e) {
      log.error("顺丰订单详情查询失败", e);
    }
    responseObject.setData(orderItems);
    return responseObject;
  }

  public void checkStatus(OrderItems orderItems) {
    if (orderItems.getShippingStatusStr().equals("SHIPPED")) {
      orderItems.setShippingStatusStr("已发货");
    } else if (orderItems.getShippingStatusStr().equals("SUBMITTED")) {
      orderItems.setShippingStatusStr("已提交");
    } else if (orderItems.getShippingStatusStr().equals("DELIVERY")) {
      orderItems.setShippingStatusStr("正在发货");
    } else if (orderItems.getShippingStatusStr().equals("SUCCESS")) {
      orderItems.setShippingStatusStr("已签收");
    } else {
      orderItems.setShippingStatusStr("待确认");
    }
  }

  //  @RequestMapping("/updateSFLoc")
  public void getLoc() {
    StringBuffer info = new StringBuffer();
    List<String> orderNos = orderMapper.getOrderNoByLoc();
    for (String s : orderNos) {
      getOrderLoc(s);
    }
    info.append("顺丰更新物流信息一共获取数据" + orderNos.size() + "条");
    log.info(info.toString());
  }

  private void getOrderLoc(String orderNo) {
    try {
      SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.SELECT_ORDER, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      Order order = orderMapper.getLogsticsByOrderNo(orderNo);
      if (order == null || order.getPartnerOrderNo() == null) {
        return;
      }
      List<SubOrder> subOrders = logisticsGoodsService.findSubOrder(order.getPartnerOrderNo());
      if (subOrders != null && subOrders.size() != 0) {
        order.setPartnerOrderNo(subOrders.get(0).getOrderSn());
      }
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("page", 1);
      jsonObject.put("pageSize", 9);
      jsonObject.put("queryParam", order.getPartnerOrderNo());
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String content = res.getContent();
      JSONObject jsonValue = JSONObject.parseObject(content).getJSONObject("data");
      JSONArray jsonArray = jsonValue.getJSONArray("orderItems");
      if (jsonArray.size() == 0) {
        return;
      }
      String shippingSn = jsonArray.getJSONObject(0).getString("shippingSn");
      Long shippingTime = jsonArray.getJSONObject(0).getLong("shippingTime") * 1000L;
      if (StringUtils.isNotBlank(shippingSn)) {
        Order orderLoc = new Order();
        orderLoc.setOrderNo(orderNo);
        orderLoc.setLogisticsOrderNo(shippingSn);
        orderLoc.setStatus(OrderStatus.SHIPPED);
        orderLoc.setShippedAt(new Date(shippingTime));
        orderMapper.updateOrderByOrderNo(orderLoc);
      }
    } catch (Exception e) {
      log.error("获取顺丰物流单号错误", e);
    }
  }

  //  @RequestMapping("orderPush")
  public void pushOrder() {
    AsyncSubmitOrder asyncSubmitOrder = new AsyncSubmitOrder();
    List<Order> splitedOrders = orderService.queryOrderCanPush();

    for (Order order : splitedOrders) {
      List<OrderItem> orderItemList = orderService.listOrderItems(order.getId());
      OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(order.getId());
      if (orderItemList != null && !orderItemList.isEmpty()) {
        for (OrderItem orderItem : orderItemList) {
          Product product = productService.load(orderItem.getProductId());

          orderItem.setProduct(product);

          Sku sku = productService.loadSku(orderItem.getSkuId());
          orderItem.setSku(sku);

          order.setSource(ProductSource.valueOf(sku.getSkuCodeResources()));
        }

        Map<Order, List<OrderItem>> orderListMap = new HashMap<Order, List<OrderItem>>();
        orderListMap.put(order, orderItemList);

        asyncSubmitOrder.asyncSubmit(orderListMap, orderAddressVO, sfConfigMapper, profile, host,
            sfRegionService,
            systemRegionMapper, orderService, logisticsGoodsService);
      }
    }
  }
}
