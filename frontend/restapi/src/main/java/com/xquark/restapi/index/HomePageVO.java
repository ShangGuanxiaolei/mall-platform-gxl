package com.xquark.restapi.index;

import java.util.List;

import com.xquark.restapi.category.CategoryVO;
import com.xquark.restapi.category.ProductListSortTypeVO;
import com.xquark.service.poster.vo.PosterVO;
import com.xquark.service.product.vo.ProductVO;

public class HomePageVO {

  private List<PosterVO> posters;
  private List<CategoryVO> categories;
  private List<ProductListSortTypeVO> sortTypes;
  private List<ProductVO> products;

  public List<PosterVO> getPosters() {
    return posters;
  }

  public void setPosters(List<PosterVO> posters) {
    this.posters = posters;
  }

  public List<CategoryVO> getCategories() {
    return categories;
  }

  public void setCategories(List<CategoryVO> categories) {
    this.categories = categories;
  }

  public List<ProductVO> getProducts() {
    return products;
  }

  public void setProducts(List<ProductVO> products) {
    this.products = products;
  }

  public List<ProductListSortTypeVO> getSortTypes() {
    return sortTypes;
  }

  public void setSortTypes(List<ProductListSortTypeVO> sortTypes) {
    this.sortTypes = sortTypes;
  }
}
