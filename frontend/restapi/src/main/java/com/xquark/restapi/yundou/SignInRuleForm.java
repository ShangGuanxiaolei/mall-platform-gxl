package com.xquark.restapi.yundou;

import com.xquark.dal.status.LongOperation;
import javax.validation.constraints.NotNull;

/**
 * Created by wangxinhua on 17-11-25. DESC:
 */
public class SignInRuleForm {

  private String id;

  /**
   * 签到积分初始值
   */
  @NotNull(message = "签到初始值不能为空")
  private Long init;

  /**
   * 签到计算方式
   */
  @NotNull(message = "签到计算方式不能为空")
  private LongOperation operation;

  /**
   * 签到运算数
   */
  @NotNull(message = "签到运算数不能为空")
  private Integer operationNumber;

  /**
   * 签到获得的最大积分
   */
  @NotNull(message = "签到最大积分不能为空")
  private Long max;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getInit() {
    return init;
  }

  public void setInit(Long init) {
    this.init = init;
  }

  public LongOperation getOperation() {
    return operation;
  }

  public void setOperation(LongOperation operation) {
    this.operation = operation;
  }

  public Integer getOperationNumber() {
    return operationNumber;
  }

  public void setOperationNumber(Integer operationNumber) {
    this.operationNumber = operationNumber;
  }

  public Long getMax() {
    return max;
  }

  public void setMax(Long max) {
    this.max = max;
  }
}
