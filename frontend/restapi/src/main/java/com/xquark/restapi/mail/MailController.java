package com.xquark.restapi.mail;

import com.xquark.restapi.ResponseObject;
import com.xquark.service.mail.JavaMailWithAttachment;
import com.xquark.service.mail.MailService;
import com.xquark.service.mail.SendMail;
import com.xquark.service.outpay.impl.alipay.HttpRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: kong Date: 2018/6/30. Time: 18:09
 */
@RestController
@RequestMapping("/mail")
public class MailController {

  @RequestMapping("/sendMail")
  public ResponseObject<String> send(@Email String email) {
    ResponseObject<String> responseObject = new ResponseObject<>();
    responseObject.setMoreInfo("发送成功");
    try {
      File file = new File("page.txt");
      if (file.exists()) {
        file.delete();
      }
      InputStream inputStream = MailController.class.getClassLoader()
          .getResourceAsStream("page.md");
      inputstreamtofile(inputStream, file);
      SendMail cn = new SendMail();
      // 设置发件人地址、收件人地址和邮件标题
      cn.setAddress(email, "page");
      // 设置要发送附件的位置和标题
//      cn.setAffix("book.txt", "submit.txt");
      // 设置smtp服务器以及邮箱的帐号和密码
      cn.send(file);
    } catch (Exception e) {
      e.printStackTrace();
      responseObject.setMoreInfo("发送失败");
    }
    return responseObject;
  }

  public static void inputstreamtofile(InputStream ins, File file) {
    try {
      OutputStream os = new FileOutputStream(file);
      int bytesRead = 0;
      byte[] buffer = new byte[8192];
      while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
        os.write(buffer, 0, bytesRead);
      }
      os.close();
      ins.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
