package com.xquark.restapi.recommend;

import com.xquark.dal.model.RecommendProductVO;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.recommend.RecommendService;
import com.xquark.utils.DynamicPricingUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 推荐商品接口
 */
@RestController
public class RecommendController extends BaseController {

  @Autowired private RecommendService recommendService;

  /**
   * 推荐商品列表
   *
   * @param code 推荐商品编码
   * @return 推荐商品集合
   */
  @RequestMapping(value = "/recommend/{code}", method = RequestMethod.GET)
  public ResponseObject<List<RecommendProductVO>> getRecommendProduct(
      @PathVariable String code) {
    List<RecommendProductVO> recommendProductList =
        recommendService.getRecommendProductList(code);
    // 重新计算金额
    recommendProductList.forEach(DynamicPricingUtil.rePricing((User) getCurrentIUser()));
    return new ResponseObject<>(recommendProductList);
  }
}
