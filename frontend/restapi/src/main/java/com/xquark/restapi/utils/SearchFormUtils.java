package com.xquark.restapi.utils;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * User: huangjie
 * Date: 2018/6/5.
 * Time: 下午5:13
 */
public class SearchFormUtils {

    /**
     * 校验搜索表单
     *
     * @param form
     */
    public static void checkForm(SearchForm form) {
        if (StringUtils.isBlank(form.getKeyWord())) {
            form.setKeyWord("");
        }
        if (StringUtils.isBlank(form.getOrder())) {
            form.setOrder("updated_at");
        }
        if (StringUtils.isBlank(form.getDirection()) || !form.getDirection().equals("asc") || !form.getDirection().equals("desc")) {
            form.setDirection("asc");
        }
    }

    /***
     * 生成params
     * @param form
     * @param page
     * @param pageable
     * @return
     */
    public static Map<String, Object> generateMapParams(SearchForm form, Pageable page,
        boolean pageable) {
        Map<String, Object> params = ImmutableMap.of(
                "keyWord", form.getKeyWord(),
                "order", form.getOrder(),
                "direction", form.getDirection(),
            "page", page,
            "pageable", pageable
        );
        return params;
    }


    /**
     * 校验并生成params
     * @param form
     * @param page
     * @param pageable
     * @return
     */
    public static Map<String, Object> checkAndGenerateMapParams(SearchForm form, Pageable page,
        boolean pageable) {
        checkForm(form);
        return generateMapParams(form, page, pageable);
    }

}
