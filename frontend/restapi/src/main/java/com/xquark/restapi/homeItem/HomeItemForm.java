package com.xquark.restapi.homeItem;

import com.xquark.dal.model.HomeItem;

/**
 * Created by root on 16-12-06.
 */
public class HomeItemForm extends HomeItem {

  private String imgs;

  public String getImgs() {
    return imgs;
  }

  public void setImgs(String imgs) {
    this.imgs = imgs;
  }
}
