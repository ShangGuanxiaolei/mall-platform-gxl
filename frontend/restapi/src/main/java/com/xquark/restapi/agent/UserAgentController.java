package com.xquark.restapi.agent;

import com.alibaba.fastjson.JSON;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.cache.FounderAreaCache;
import com.xquark.cache.TokenCache;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.UserAgentMapper;
import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Message;
import com.xquark.dal.model.Role;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.TweetImage;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.model.UserAgentForm;
import com.xquark.dal.model.UserAgentWhiteList;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.status.RoleBelong;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.CategoryTreeVO;
import com.xquark.dal.vo.UserAgentCommissionVO;
import com.xquark.dal.vo.UserAgentInfoVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.interceptor.Token;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.homeItem.HomeItemForm;
import com.xquark.service.address.AddressService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.agent.UserAgentWhiteListService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.ImgUtils;
import com.xquark.utils.ResourceResolver;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

/**
 * 代理用户controller Created by chh on 16-12-6.
 */
@Controller
@ApiIgnore
public class UserAgentController extends BaseController {

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private UserService userService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private TinyUrlService tinyUrlService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ExcelService excelService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private UserAgentMapper userAgentMapper;

  @Autowired
  private AuditRuleService auditRuleService;

  @Autowired
  private Qiniu qiniu;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private UserAgentWhiteListService whiteListService;

  @Autowired
  private TokenCache tokenCache;

  @Autowired
  private FounderAreaCache founderAreaCache;

  @Autowired
  private ZoneMapper zoneMapper;

  /**
   * 获取代理用户页面列表
   */
  @ResponseBody
  @RequestMapping("/userAgent/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, HttpServletRequest request) {
    String userId = this.getCurrentIUser().getId();
    String shopId = this.getCurrentIUser().getShopId();
    List<UserAgentVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String status = request.getParameter("status");
    String type = request.getParameter("type");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String phone = request.getParameter("phone");
    String name = request.getParameter("name");
    String parentName = request.getParameter("parentName");
    String notUserId = request.getParameter("notUserId");

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }

    params.put("type", type);
    params.put("status", status);
    params.put("startDate", startDate);
    params.put("endDate", endDate);
    params.put("notUserId", notUserId);

    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(parentName)) {
      params.put("parentName", "%" + parentName + "%");
    }

    viewPages = userAgentService.list(pageable, params);
    for (UserAgentVO vo : viewPages) {
      UserAgentVO parentVo = userAgentService.selectByUserId(vo.getParentUserId());
      if (parentVo == null) {
        parentVo = new UserAgentVO();
        parentVo.setName("总部");
      }
      vo.setParentVo(parentVo);

      // 查询该用户的未结算佣金和已结算佣金
      BigDecimal withdrawFee = commissionService.getWithdrawAll(vo.getUserId());//获取累计收入
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
      Date nowDate = new Date();
      Calendar cal = Calendar.getInstance();
      cal.setTime(nowDate);
      cal.add(Calendar.MONTH, -1);
      Date lastMonth = cal.getTime();
      String lastMonthStr = sdf.format(lastMonth);
      BigDecimal unWithdrawFee = commissionService
          .getWithdrawOfMonth(vo.getUserId(), lastMonthStr);//获取上个月的收益
      vo.setWithdrawFee(withdrawFee);
      vo.setUnWithdrawFee(unWithdrawFee);

      // 当前登陆用户的角色，如果为客户管理员角色，则需要将手机号，名称等字段进行隐藏显示
      Merchant merchant = getCurrentMerchant();
      if (merchant != null && "CUSTOMER".equals(merchant.getRoles())) {
        // 姓名
        vo.setName(vo.getName().substring(0, 1) + createAsterisk(vo.getName().length() - 1));

        // 手机号
        Pattern p =Pattern.compile("(\\w{5})(\\w+)");
        Matcher m = p.matcher(vo.getPhone());
        String n = m.replaceAll("$1******");
        vo.setPhone(n);

        // 身份证号
        p =Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
        m = p.matcher(vo.getIdcard());
        n = m.replaceAll("$1********$3");
        vo.setIdcard(n);

        // 上级名称
        vo.getParentVo().setName(vo.getParentVo().getName().substring(0, 1) + createAsterisk(
            vo.getParentVo().getName().length() - 1));
      }

    }

    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", userAgentService.selectCnt(params));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  //生成很多个*号
  public String createAsterisk(int length) {
    StringBuilder stringBuffer = new StringBuilder();
    for (int i = 0; i < length; i++) {
      stringBuffer.append("*");
    }
    return stringBuffer.toString();
  }

  private Merchant getCurrentMerchant() {
    Merchant merchant = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof Merchant) {
        merchant = (Merchant) principal;
      }
    }
    return merchant;
  }

  /**
   * 获取所有代理的佣金信息
   */
  @ResponseBody
  @RequestMapping("/userAgent/listCommission")
  public ResponseObject<Map<String, Object>> listCommission(Pageable pageable,
      HttpServletRequest request) {
    String shopId = this.getCurrentIUser().getShopId();
    List<UserAgentCommissionVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String buyerPhone = request.getParameter("buyerPhone");
    String buyerName = request.getParameter("buyerName");
    String sellerPhone = request.getParameter("sellerPhone");
    String sellerName = request.getParameter("sellerName");
    String userId = request.getParameter("userId");
    String orderNo = request.getParameter("orderNo");

    params.put("startDate", startDate);
    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (Exception e) {
      }
    }
    params.put("buyerPhone", buyerPhone);
    params.put("buyerName", buyerName);
    params.put("sellerPhone", sellerPhone);
    params.put("sellerName", sellerName);

    if (StringUtils.isNoneEmpty(userId)) {
      params.put("userId", userId);
    }
    if (StringUtils.isNoneEmpty(orderNo)) {
      params.put("orderNo", "%" + orderNo + "%");
    }

    viewPages = userAgentService.listUserCommission(pageable, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", userAgentService.selectCntUserCommission(params));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 根据手机号或者订单号获取某个代理的佣金信息
   */
  @ResponseBody
  @RequestMapping("/userAgent/searchCommission")
  public ResponseObject<Map<String, Object>> searchCommission(Pageable pageable,
      HttpServletRequest request) {
    String shopId = this.getCurrentIUser().getShopId();
    List<UserAgentCommissionVO> viewPages = null;
    String userId = request.getParameter("userId");
    String keys = request.getParameter("keys");
    if (StringUtils.isNotEmpty(keys)) {
      keys = "%" + keys + "%";
    }
    viewPages = userAgentService.searchUserCommission(pageable, userId, keys);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }


  @ResponseBody
  @RequestMapping("userAgent/updateOffered")
  public ResponseObject<Boolean> changeOffered(@RequestParam("ids") String ids,
      @RequestParam("offered") Boolean offered) {
    Boolean result = commissionService.updateOffered(ids, offered);
    return new ResponseObject<>(result);
  }

  /**
   * 保存代理用户
   *
   * @param updatePhone 是否更新为已经存在的手机号
   */
  @ResponseBody
  @RequestMapping("/userAgent/save")
  public ResponseObject<Boolean> save(UserAgentForm userAgentForm,
      @RequestParam(required = false) String updatePhone) {
    int result = 0;
    UserAgent userAgent = new UserAgent();
    BeanUtils.copyProperties(userAgentForm, userAgent);

    String addressDetail = userAgentForm.getAddressDetail();
    String district = userAgentForm.getDistrict();

    // 先更新用户地址信息
    String addressId = userAgentForm.getAddress();
    if (StringUtils.isNotEmpty(addressId)) {
      Address address = new Address();
      address.setId(addressId);
      address.setZoneId(district);
      address.setStreet(addressDetail);
      addressService.updateByPrimaryKeySelective(address);
    }

    // 如果是更新为已经存在的手机号，则先删除掉手机号之前用户的相关信息
    if ("1".equals(updatePhone)) {
      String phone = userAgent.getPhone();
      User user = userService.loadAnonymousByPhone(phone);
      userAgentMapper.deleteShopTreeByPhone(user.getShopId());
      userAgentMapper.deleteShopByPhone(phone);
      userAgentService.deleteInfoByPhone(phone);
    }

    Role role = roleService
        .selectByCodeAndBelong(userAgent.getType().toString(), RoleBelong.B2B.toString());
    if (role != null) {
      userAgent.setRole(role.getId());
    }

    if (StringUtils.isNotEmpty(userAgent.getId())) {
      result = userAgentService.modify(userAgent);

      // 更新代理用户的同时，也要同步更新用户表和shop表相应的name和phone字段
      UserAgentVO vo = userAgentService.selectByPrimaryKey(userAgent.getId());
      String userId = vo.getUserId();
      User old = userService.load(userId);
      User userUpdate = new User();
      userUpdate.setId(userId);
      userUpdate.setName(userAgent.getName());
      userUpdate.setPhone(userAgent.getPhone());
      // 如果原帐号手机号与loginname相同，则修改手机号后同时也要修改掉loginname，否则原手机号无法再注册，会提示重复
      if (old.getPhone().equals(old.getLoginname())) {
        userUpdate.setLoginname(userAgent.getPhone());
      }
      userService.updateByBosUserInfo(userUpdate);

      Shop existShop = shopMapper.selectByUserId(userId);
      if (existShop != null) {
        Shop shop = new Shop();
        shop.setName(userAgentForm.getName());
        shop.setId(existShop.getId());
        shopMapper.updateByPrimaryKeySelective(shop);
      }


    } else {
      userAgent.setArchive(false);
      result = userAgentService.insert(userAgent);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 代理修改保存时检查手机号是否已存在
   */
  @ResponseBody
  @RequestMapping("/userAgent/checkPhone")
  public ResponseObject<Boolean> checkPhone(UserAgentForm userAgentForm) {
    int result = 0;
    UserAgent userAgent = new UserAgent();
    BeanUtils.copyProperties(userAgentForm, userAgent);
    String phone = userAgentForm.getPhone();
    if (StringUtils.isNotEmpty(phone)) {
      if (StringUtils.isNotEmpty(userAgent.getId())) {
        userAgent = userAgentService.selectByPrimaryKey(userAgent.getId());
        User user = userService.loadAnonymousByPhone(phone);
        if (user != null && !user.getId().equals(userAgent.getUserId())) {
          return new ResponseObject<>(true);
        } else {
          return new ResponseObject<>(false);
        }
      } else {
        User user = userService.loadAnonymousByPhone(phone);
        if (user != null) {
          return new ResponseObject<>(true);
        } else {
          return new ResponseObject<>(false);
        }
      }
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 申请代理用户
   */
  @ResponseBody
  @RequestMapping("/userAgent/apply")
  @Token(remove = true)
  public ResponseObject<String> apply(UserAgentForm userAgentForm, HttpServletRequest request) {
    String result = "0";
    String userId = this.getCurrentIUser().getId();
    String parentPhone = userAgentForm.getParentPhone();
    String phone = userAgentForm.getPhone();
    String weixin = userAgentForm.getWeixin();
    boolean isFromApp = userAgentForm.getIsFromApp();

    // 如果是微信分享出来的链接进行申请，则下面两个参数会有值，否则就是在app中进行的申请
    String parentId = userAgentForm.getParentId();
    AgentType agentType = userAgentForm.getAgentType();
    if (agentType != null) {
      userAgentForm.setType(agentType);
    }

    User parent = null;
    if (StringUtils.isNotEmpty(parentPhone)) {
      // 根据手机号查找上级userid
      parent = userService.loadAnonymousByPhone(parentPhone);
      // 查询该手机号的上级代理是否存在
      UserAgentVO parentAgent = userAgentService.selectByPhone(parentPhone);
      if (parentAgent == null || parent == null) {
        result = "1";
      } else if (parentAgent.getStatus() != AgentStatus.ACTIVE) {
        result = "10";
      } else {
        // 判断上级代理角色类型与申请的代理角色是否匹配
        AgentType parentType = parentAgent.getType();
        if ((parentType == AgentType.SECOND && (userAgentForm.getType() == AgentType.GENERAL
            || userAgentForm.getType() == AgentType.FIRST)) ||
            (parentType == AgentType.FIRST && (userAgentForm.getType() == AgentType.GENERAL)) ||
            (parentType == AgentType.SPECIAL && userAgentForm.getType() != AgentType.SPECIAL)) {
          result = "6";
        }

      }
    }

    // 查询是否有相同申请手机号的代理
    UserAgentVO agent = userAgentService.selectByPhone(phone);
    if (agent != null) {
      result = "2";
    }

    // 查询是否有相同微信号的代理
    agent = userAgentService.selectByWeixin(weixin);
    if (agent != null) {
      result = "3";
    }

    if ("0".equals(result)) {
      result = userAgentService.addAgent(userId, userAgentForm, parentId, parent, isFromApp);
    }

    // 如果提交失败了后，需要将token重新放入cache中，否则客户端没法再提交请求
    if (!"ok".equals(result)) {
      tokenCache.put(request.getParameter("token"));
    }

    return new ResponseObject<>(result);
  }

  /**
   * 后台手工录入代理
   */
  @ResponseBody
  @RequestMapping("/userAgent/apply4Admin")
  public ResponseObject<String> apply4Admin(UserAgentForm userAgentForm,
      HttpServletRequest request) {
    String result = "0";
    String parentPhone = userAgentForm.getParentPhone();
    String phone = userAgentForm.getPhone();
    String weixin = userAgentForm.getWeixin();

    // 汇购网用户id
    String extUid = request.getParameter("extUid");

    // 如果是微信分享出来的链接进行申请，则下面两个参数会有值，否则就是在app中进行的申请
    String parentId = userAgentForm.getParentId();
    AgentType agentType = userAgentForm.getAgentType();
    if (agentType != null) {
      userAgentForm.setType(agentType);
    }

    User parent = null;
    if (StringUtils.isNotEmpty(parentPhone)) {
      // 根据手机号查找上级userid
      parent = userService.loadAnonymousByPhone(parentPhone);
      // 查询该手机号的上级代理是否存在
      UserAgentVO parentAgent = userAgentService.selectByPhone(parentPhone);
      if (parentAgent == null) {
        result = "1";
      } else if (parentAgent.getStatus() != AgentStatus.ACTIVE) {
        result = "10";
      } else {
        // 判断上级代理角色类型与申请的代理角色是否匹配
        AgentType parentType = parentAgent.getType();
        if ((parentType == AgentType.SECOND && (userAgentForm.getType() == AgentType.GENERAL
            || userAgentForm.getType() == AgentType.FIRST)) ||
            (parentType == AgentType.FIRST && (userAgentForm.getType() == AgentType.GENERAL)) ||
            (parentType == AgentType.SPECIAL && userAgentForm.getType() != AgentType.SPECIAL)) {
          result = "6";
        }

      }
    }

    // 查询是否有相同申请手机号的代理
    UserAgentVO agent = userAgentService.selectByPhoneActive(phone);
    if (agent != null) {
      result = "2";
    }

    // 查询是否有相同申请手机号的待审核或已冻结代理
    UserAgentVO applyAgent = userAgentService.selectByPhoneApplying(phone);
    if (applyAgent != null) {
      result = "9";
    }

    // 查询是否有相同微信号的代理
    agent = userAgentService.selectByWeixin(weixin);
    if (agent != null) {
      result = "3";
    }

    // 汇购网用户id是否已经存在代理用户
    if (StringUtils.isNotEmpty(extUid)) {
      User user = userService.loadExtUserByUid(extUid);
      if (user != null) {
        result = "7";
      }
    }

    if ("0".equals(result)) {

      result = userAgentService.addAgent4Admin(userAgentForm, parentId, parent, extUid);
    }

    return new ResponseObject<>(result);
  }

  private void pushMessage(Long msgId, String userId, String url, Integer type, String relatId,
      String data) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setData(data);
    MessageNotifyEvent event = new MessageNotifyEvent(message, userId, url, type, relatId);
    applicationContext.publishEvent(event);
  }

  @ResponseBody
  @RequestMapping("/userAgent/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = userAgentService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 逻辑删除代理
   */
  @ResponseBody
  @RequestMapping("/userAgent/deleteLogically/{id}")
  public ResponseObject<Boolean> deleteLogically(@PathVariable String id) {
    int result = userAgentService.deleteLogically(id);

    // 如果联合创始人进行了冻结，解冻，删除等操作，需要刷新缓存
    UserAgent agentVo = userAgentService.selectByPrimaryKey(id);
    if (agentVo.getType() == AgentType.FOUNDER) {
      founderAreaCache.refresh();
    }

    return new ResponseObject<>(result > 0);
  }


  /**
   * 查看某个具体的代理用户记录<br>
   */
  @ResponseBody
  @RequestMapping("/userAgent/{id}")
  public ResponseObject<UserAgentVO> view(@PathVariable String id, HttpServletRequest req) {
    UserAgentVO userAgentVO = userAgentService.selectByPrimaryKey(id);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

    // 显示*号的身份证号
    String idcard = userAgentVO.getIdcard();
    Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
    Matcher m = p.matcher(idcard);
    String hiddenIdcard = m.replaceAll("$1********$3");
    if (StringUtils.isEmpty(hiddenIdcard)) {
      hiddenIdcard = "********";
    }

    String certNum = userAgentVO.getCertNum();
    String zip = "";
    String addressId = userAgentVO.getAddress();
    if (StringUtils.isNotEmpty(addressId)) {
      Address address = addressService.load(addressId);
      if (address != null) {
        Zone zone = zoneMapper.selectByPrimaryKey(address.getZoneId());
        if (zone != null) {
          zip = zone.getZipCode();
        }
        if (StringUtils.isNotEmpty(zip) && zip.length() > 3) {
          zip = zip.substring(0, 3);
        }
      }
    }
    userAgentVO.setCertNumView(certNum + (zip == null ? "" : zip));

    userAgentVO.setHiddenIdcard(hiddenIdcard);

    return new ResponseObject<>(userAgentVO);
  }


  /**
   * 根据userid查看某个具体的代理用户记录<br>
   */
  @ResponseBody
  @RequestMapping("/userAgent/userId/{id}")
  public ResponseObject<UserAgentVO> viewUserId(@PathVariable String id, HttpServletRequest req) {
    UserAgentVO userAgentVO = userAgentService.selectByUserId(id);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

    String certNum = userAgentVO.getCertNum();
    String zip = "";
    String addressId = userAgentVO.getAddress();
    if (StringUtils.isNotEmpty(addressId)) {
      Address address = addressService.load(addressId);
      if (address != null) {
        Zone zone = zoneMapper.selectByPrimaryKey(address.getZoneId());
        if (zone != null) {
          zip = zone.getZipCode();
        }
        if (StringUtils.isNotEmpty(zip) && zip.length() > 3) {
          zip = zip.substring(0, 3);
        }
      }
    }
    userAgentVO.setCertNumView(certNum + (zip == null ? "" : zip));

    // 显示*号的身份证号
    String idcard = userAgentVO.getIdcard();
    Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
    Matcher m = p.matcher(idcard);
    String hiddenIdcard = m.replaceAll("$1********$3");
    userAgentVO.setHiddenIdcard(hiddenIdcard);

    return new ResponseObject<>(userAgentVO);
  }

  /**
   * 查询某个代理下级各个代理类型人数汇总
   */
  @ResponseBody
  @RequestMapping("/userAgent/getTotalByParentId")
  public ResponseObject<List<Map>> getTotalByParentId(
      @RequestParam(required = false) String parentId, HttpServletRequest req) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<Map> maps = userAgentService.getTotalByParentId(parentId);

    return new ResponseObject<>(maps);
  }


  /**
   * 分页查询某个代理下级某种代理类型所有的明细人员信息
   */
  @ResponseBody
  @RequestMapping("/userAgent/listByParentAndType")
  public ResponseObject<List<UserAgentVO>> listByParentAndType(Pageable pageable,
      @RequestParam(required = false) String parentId, @RequestParam String type,
      HttpServletRequest req) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<UserAgentVO> userAgentVOs = userAgentService.listByParentAndType(pageable, parentId, type);
    UserAgentVO parentAgent = userAgentService.selectByUserId(parentId);
    AgentType agentType = parentAgent.getType();
    // 查询代理明细是否能显示下级及当月佣金
    for (UserAgentVO vo : userAgentVOs) {
      long count = userAgentService.selectCntByParentId(vo.getUserId());
      vo.setIsLeaf(count > 0 ? false : true);
      vo.setCommissionFee(commissionService.getFeeByMonth(vo.getUserId()));
      // 获取用户详细地址信息
      String addressId = vo.getAddress();
      Address address = addressService.load(addressId);
      String details = "";
      if (address != null) {
        List<Zone> zoneList = zoneService.listParents(address.getZoneId());
        for (Zone zone : zoneList) {
          details += zone.getName();
        }
        details += address.getStreet();
      }
      vo.setAddressStreet(details);
    }

    return new ResponseObject<>(userAgentVOs);
  }

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  @ResponseBody
  @RequestMapping("/userAgent/listApplyingByParent")
  public ResponseObject<List<UserAgentVO>> listApplyingByParent(Pageable pageable,
      @RequestParam(required = false) String parentId, HttpServletRequest request) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<UserAgentVO> userAgentVOs = userAgentService.listApplyingByParent(pageable, parentId);

    for (UserAgentVO vo : userAgentVOs) {
      // 获取用户详细地址信息
      String addressId = vo.getAddress();
      Address address = addressService.load(addressId);
      String details = "";
      if (address != null) {
        List<Zone> zoneList = zoneService.listParents(address.getZoneId());
        for (Zone zone : zoneList) {
          details += zone.getName();
        }
        details += address.getStreet();
      }
      vo.setAddressStreet(details);

      // 默认代理审核由平台审核
      vo.setAuditType(AuditType.PLATFORM);
      AgentType type = vo.getType();
      String parentUserId = vo.getParentUserId();
      UserAgentVO parentAgentVO = userAgentService.selectByUserId(parentUserId);
      // 根据代理审核规则配置的规则，判断该代理是由平台还是上级进行审核
      if (parentAgentVO != null) {
        AuditRule auditRule = auditRuleService.selectByAgentType(type, parentAgentVO.getType());
        if (auditRule != null) {
          vo.setAuditType(auditRule.getAuditType());
        }
      }

    }

    return new ResponseObject<>(userAgentVOs);
  }

  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  @ResponseBody
  @RequestMapping("/userAgent/countApplyingByParent")
  public ResponseObject<Long> countApplyingByParent(Pageable pageable,
      @RequestParam(required = false) String parentId, HttpServletRequest request) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    long count = userAgentService.countApplyingByParent(parentId);
    return new ResponseObject<>(count);
  }

  /**
   * 代理申请列表查询
   */
  @ResponseBody
  @RequestMapping("/userAgent/apply/list")
  public ResponseObject<Map<String, Object>> listTwitterApply(Pageable pageable,
      @RequestParam(required = false) String parentId, HttpServletRequest request) {
    // 如果没有传入parentId，则默认查找所有待审核信息
    Map<String, Object> params = new HashMap<>();
    String status = request.getParameter("status");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String phone = request.getParameter("phone");
    String name = request.getParameter("name");
    String parentName = request.getParameter("parentName");
    String type = request.getParameter("type");

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }

    params.put("status", status);
    params.put("startDate", startDate);
    params.put("endDate", endDate);
    params.put("type", type);
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(parentName)) {
      params.put("parentName", "%" + parentName + "%");
    }

    List<UserAgentVO> userAgentVOs = userAgentService.listUserByParent(pageable, parentId, params);

    for (UserAgentVO vo : userAgentVOs) {
      UserAgentVO parentVo = userAgentService.selectByUserId(vo.getParentUserId());
      if (parentVo == null) {
        parentVo = new UserAgentVO();
        parentVo.setName("总部");
      }
      vo.setParentVo(parentVo);

      // 获取用户详细地址信息
      String addressId = vo.getAddress();
      Address address = addressService.load(addressId);
      String details = "";
      if (address != null) {
        List<Zone> zoneList = zoneService.listParents(address.getZoneId());
        for (Zone zone : zoneList) {
          details += zone.getName();
        }
        details += address.getStreet();
      }
      vo.setAddressStreet(details);
    }

    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("applyTotal", userAgentService.countUserByParent(parentId, params));
    aRetMap.put("list", userAgentVOs);

    return new ResponseObject<>(aRetMap);
  }

  /**
   * 审核代理申请人
   */
  @ResponseBody
  @RequestMapping("/userAgent/audit")
  public ResponseObject<Boolean> audit(@RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.ACTIVE);
    int result = userAgentService.modify(agent);

    UserAgentVO vo = userAgentService.selectByPrimaryKey(id);
    String userId = vo.getUserId();

    // 推送消息给代理人，提醒申请通过
    try {
      // json格式的数据字符串
      String data = getXcxData(vo, PushMsgType.MSG_AGENT_AUDIT.getValue(), true);
      pushMessage(PushMsgId.Agent_Audit.getId(), userId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_AUDIT.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给代理人，提醒申请通过 error " + e.toString());
    }

    return new ResponseObject<>(result > 0);
  }

  /**
   * 审核通过后发送自定义消息给小程序的申请代理人
   */
  private String getXcxData(UserAgentVO userAgent, long type, boolean isAudit) {
    String data = "";
    HashMap map = new HashMap();
    String parentId = userAgent.getParentUserId();
    User parent = userService.load(parentId);
    Shop shop = shopService.findByUser(userAgent.getUserId());
    Shop parentShop = shopService.findByUser(parentId);
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    // 发送相关json数据给小程序
    map.put("fromAccount", parent.getPhone());
    map.put("fromAccountName", parent.getName());
    map.put("fromAccountAvatar", resourceFacade.resolveUrl(parentShop.getImg()));
    map.put("toAccount", userAgent.getPhone());
    map.put("toAccountName", userAgent.getName());
    map.put("toAccountAvatar", resourceFacade.resolveUrl(shop.getImg()));
    map.put("id", userAgent.getId());
    map.put("cDate", (new Date()).getTime());
    map.put("type", type);
    map.put("isAudit", isAudit);
    data = JSON.toJSONString(map);
    return data;
  }


  /**
   * 解冻账户
   */
  @ResponseBody
  @RequestMapping("/userAgent/unFrozen")
  public ResponseObject<Boolean> unFrozen(@RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.ACTIVE);
    int result = userAgentService.modify(agent);

    // 如果联合创始人进行了冻结，解冻，删除等操作，需要刷新缓存
    UserAgent agentVo = userAgentService.selectByPrimaryKey(id);
    if (agentVo.getType() == AgentType.FOUNDER) {
      founderAreaCache.refresh();
    }

    return new ResponseObject<>(result > 0);
  }

  /**
   * 解冻账户
   */
  @ResponseBody
  @RequestMapping("/userAgent/frozen")
  public ResponseObject<Boolean> frozen(@RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.FROZEN);
    int result = userAgentService.modify(agent);

    // 如果联合创始人进行了冻结，解冻，删除等操作，需要刷新缓存
    UserAgent agentVo = userAgentService.selectByPrimaryKey(id);
    if (agentVo.getType() == AgentType.FOUNDER) {
      founderAreaCache.refresh();
    }

    return new ResponseObject<>(result > 0);
  }

  /**
   * 拒绝代理申请人
   */
  @ResponseBody
  @RequestMapping("/userAgent/reject")
  public ResponseObject<Boolean> reject(@RequestParam String id) {

    UserAgentVO vo = userAgentService.selectByPrimaryKey(id);
    String userId = vo.getUserId();
    // 推送消息给代理人，提醒申请拒绝
    try {
      // json格式的数据字符串
      String data = getXcxData(vo, PushMsgType.MSG_AGENT_AUDIT.getValue(), false);
      pushMessage(PushMsgId.Agent_Audit.getId(), userId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_AUDIT.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给代理人，提醒申请拒绝 error " + e.toString());
    }
    int result = userAgentService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取分享经销商链接
   */
  @ResponseBody
  @RequestMapping("/userAgent/getShareUrl")
  public ResponseObject<Map> getShareUrl(HttpServletRequest req) {
    HashMap map = new HashMap();
    String parentId = this.getCurrentIUser().getId();
    //map.put("general", getUrl(req, "GENERAL", parentId));
    //map.put("first", getUrl(req, "FIRST", parentId));
    //map.put("second", getUrl(req, "SECOND", parentId));
    map.put("url", getUrl(req, parentId));
    return new ResponseObject<Map>(map);
  }

  /**
   * 保存代理用户的图片,把imageForm转化成model
   */
  private void initImage(HomeItemForm form) {
    List<TweetImage> imgs = new ArrayList<>();
    TweetImage pimg = null;
    int idx = 0;   // 从0开始
    if (null != form.getImgs() && !"".equals(form.getImgs())) {
      String[] fimgs = form.getImgs().split(",");
      for (String img : fimgs) {
        form.setImg(img);
      }
    }
  }

  private String getUrl(HttpServletRequest req, String parentId) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/userAgent/apply" + "?parentId="
            + parentId);
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 改变useragent类型，提升为董事等
   */
  @ResponseBody
  @RequestMapping("/userAgent/changeType")
  public ResponseObject<Boolean> changeType(@RequestParam String id, @RequestParam String type) {
    Role role = roleService.selectByCodeAndBelong(type, RoleBelong.B2B.toString());
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setRole(role.getId());
    agent.setType(AgentType.valueOf(type));
    int result = userAgentService.modify(agent);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 改变上级
   */
  @ResponseBody
  @RequestMapping("/userAgent/changeParent")
  public ResponseObject<Boolean> changeParent(@RequestParam String id, @RequestParam String userId,
      @RequestParam String parentUserId) {
    // 先改变useragent里的上下级关系
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setParentUserId(parentUserId);
    int result = userAgentService.modify(agent);

    // 然后同时变更shoptree里的上下级关系
    Shop shop = shopMapper.selectByUserId(userId);
    Shop parentShop = shopMapper.selectByUserId(parentUserId);
    String rootShopId = shopTreeService.selectRootShopByShopId(parentShop.getId()).getRootShopId();
    shopTreeService.changeParent(rootShopId, parentShop.getId(), shop.getId());

    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/userAgent/getDirectChildren")
  public ResponseObject<HashMap> getDirectChildren(@RequestParam(required = false) String userId,
      @RequestParam(required = false) String keys, Pageable pageable, HttpServletRequest request) {
    HashMap map = new HashMap();
    //用户名，角色，状态，分成比例，当月佣金
    List<UserAgentInfoVO> listTwitter = new ArrayList<>();
    String shopId = "";
    if (userId == null || userId.equals("")) {
      shopId = this.getCurrentIUser().getShopId();
    } else {
      shopId = userService.load(userId).getShopId();
    }
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

    // keys不为空代表是搜索树状图，使用不同的查询语句
    if (StringUtils.isEmpty(keys)) {
      List<ShopTree> listTree = shopTreeService
          .listDirectChildrenAgent(rootShopId, shopId, pageable);
      Long total = shopTreeService.countChildrenAgent(rootShopId, shopId);
      for (ShopTree shopTree : listTree) {
        String desShopId = shopTree.getDescendantShopId();
        // 查找这个下级用户的相关信息
        UserAgentInfoVO userAgentInfoVO = this
            .getUserAgentInfoVO(desShopId, shopTree.getRootShopId());
        listTwitter.add(userAgentInfoVO);
      }
      map.put("total", total);
      map.put("list", listTwitter);
      // 查找传过来用户的相关信息
      UserAgentInfoVO userAgentInfoVO = this.getUserAgentInfoVO(shopId, rootShopId);
      map.put("parent", userAgentInfoVO);
    } else {
      List<ShopTree> listTree = shopTreeService.listByPhoneOrName(rootShopId, keys);
      List list = new ArrayList();
      map.put("total", 0);
      map.put("parent", null);
      if (listTree != null && listTree.size() > 0) {
        HashMap treeMap = this.treeMenuList(listTree);
        List treeList = (List) treeMap.get("list");
        map.put("id", (String) treeMap.get("id"));
        list.add(treeList.get(treeList.size() - 1));
      }
      map.put("list", list);
    }

    return new ResponseObject(map);
  }


  /**
   * 根据手机号或名称查询代理是否存在
   */
  @ResponseBody
  @RequestMapping("/userAgent/searchByPhoneOrName")
  public ResponseObject<Boolean> searchByPhoneOrName(@RequestParam String keys) {
    String shopId = this.getCurrentIUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<ShopTree> listTree = shopTreeService.listByPhoneOrName(rootShopId, keys);
    return new ResponseObject<>(listTree.size() > 0);
  }

  /**
   * 得到某个代理的本身与上级的树结构
   */
  private HashMap treeMenuList(List<ShopTree> allList) {
    HashMap result = new HashMap();
    String id = "";
    List list = new ArrayList();
    for (int i = 0, n = allList.size(); i < n; i++) {
      ShopTree object = allList.get(i);
      String ancestorShopId = object.getAncestorShopId();
      // 查找这个用户的相关信息
      UserAgentInfoVO userAgentInfoVO = this
          .getUserAgentInfoVO(ancestorShopId, object.getRootShopId());
      CategoryTreeVO treeVO = new CategoryTreeVO();
      treeVO.setId(userAgentInfoVO.getId());
      String avatar = userAgentInfoVO.getAvatar();
      if (avatar != null && avatar.startsWith("qn|")) {
        ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
            .getBean("resourceFacade");
        avatar = resourceFacade.resolveUrl(avatar);
      }
      String role = userAgentInfoVO.getRole().toLowerCase();
      String text = "<div class='custom'>" +
          "<img src='" + avatar + "' alt='' class='user-icon'>" +
          "<em class='" + role + "'>" + userAgentInfoVO.getName() + "</em>" +
          "&nbsp;&nbsp;<em style='color: red'>" + userAgentInfoVO.getCount() + "</em>" +
          "</div>";

      treeVO.setText(text);
      Map map = new HashMap();
      if (i != 0) {
        List c_node = new ArrayList();
        c_node.add(list.get(i - 1));
        map.put("opened", true);
        map.put("selected", false);
        treeVO.setChildren(c_node);
      } else {
        map.put("opened", false);
        map.put("selected", true);
        treeVO.setChildren(userAgentInfoVO.getCount() > 0 ? true : false);
        id = userAgentInfoVO.getId();
      }
      treeVO.setState(map);
      list.add(treeVO);
    }
    result.put("id", id);
    result.put("list", list);
    return result;
  }


  /**
   * 查找用户的相关twitter树结构信息
   */
  private UserAgentInfoVO getUserAgentInfoVO(String shopId, String rootShopId) {
    UserAgentInfoVO userAgentInfoVO = new UserAgentInfoVO();

    Shop shop = shopService.load(shopId);
    String desUserId = shopService.load(shopId).getOwnerId();
    User desUser = userService.load(desUserId);
    // 这个下级是否还有下级，以及还有多少个下级人员
    long count = shopTreeService.countChildrenAgent(rootShopId, shopId);
    userAgentInfoVO.setCount(count);
    if (count > 0) {
      userAgentInfoVO.setIsLeaf(false);
    } else {
      userAgentInfoVO.setIsLeaf(true);
    }
    userAgentInfoVO.setId(desUserId);
    userAgentInfoVO.setName(desUser.getName());
    // 树状图代理头像显示对应店铺的头像
    if (StringUtils.isNotEmpty(shop.getImg())) {
      userAgentInfoVO.setAvatar(shop.getImg());
    } else {
      userAgentInfoVO.setAvatar(desUser.getAvatar());
    }
    UserAgentVO userAgentVO = userAgentService.selectByUserId(desUserId);
    if (userAgentVO != null) {
      userAgentInfoVO.setRole(userAgentVO.getType().toString());
      userAgentInfoVO.setStatus(userAgentVO.getStatus().toString());
      if (userAgentInfoVO.getRate() == null) {
        userAgentInfoVO.setRate(new BigDecimal("10"));
      }
      if (userAgentInfoVO.getIncome() == null) {
        userAgentInfoVO.setIncome(new BigDecimal(0.00));
      }
    }
    return userAgentInfoVO;
  }

  /**
   * 根据手机号或微信号查询是否存在
   */
  @ResponseBody
  @RequestMapping("/userAgent/selectByPhoneOrWeixin")
  public ResponseObject<UserAgentVO> selectByPhoneOrWeixin(@RequestParam String keys) {
    UserAgentVO vo = userAgentService.selectByPhoneOrWeixin(keys);
    if (vo != null) {
      vo.setAgentTypeName(getAgentTypeName(vo.getType()));
    }
    return new ResponseObject<>(vo);
  }

  /**
   * 根据手机号查询是否存在
   */
  @ResponseBody
  @RequestMapping("/userAgent/selectByPhone")
  public ResponseObject<UserAgentVO> selectByPhone(@RequestParam String keys) {
    UserAgentVO vo = userAgentService.selectByPhone(keys);
    if (vo != null) {
      vo.setAgentTypeName(getAgentTypeName(vo.getType()));
    }
    return new ResponseObject<>(vo);
  }

  private String getAgentTypeName(AgentType type) {
    String agentTypeName = "";
    if (type == AgentType.FOUNDER) {
      agentTypeName = "联合创始人";
    } else if (type == AgentType.DIRECTOR) {
      agentTypeName = "董事";
    } else if (type == AgentType.GENERAL) {
      agentTypeName = "总顾问";
    } else if (type == AgentType.FIRST) {
      agentTypeName = "一星顾问";
    } else if (type == AgentType.SECOND) {
      agentTypeName = "二星顾问";
    } else if (type == AgentType.SPECIAL) {
      agentTypeName = "特约";
    }
    return agentTypeName;
  }

  /**
   * 生成经销商授权证书图片
   */
  @RequestMapping(value = "/userAgent/certImg", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] generateCertImg(HttpServletRequest req) {
    // 授权证书在qiliu上的地址

    String certImgUrl = "http://images.handeson.com/FrabqDMWuYF2Cn5nArhO3TNSg4BE";
    // 生成经销商授权证书图片
    int width = 708;
    int height = 1001;
    // 获取当前用户的经销商信息
    String userId = this.getCurrentIUser().getId();
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
//		String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/186X186.png";
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      // 首先从qiliu上获取经销商原图
      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(certImgUrl);
      BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);

      Font font = new Font("宋体", Font.BOLD, 22);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);

      // 显示*号的身份证号
      String idcard = userAgentVO.getIdcard();
      Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
      Matcher m = p.matcher(idcard);
      String hiddenIdcard = m.replaceAll("$1********$3");
      g2.drawImage(imageImgRect, 0, 0, null);
      // 将代理用户的信息写到图片固定的位置上

      g2.drawString(userAgentVO.getName(), 150, 326);
      g2.drawString(userAgentVO.getWeixin(), 150, 359);
      g2.drawString(hiddenIdcard, 150, 390);

      g2.setFont(new Font("宋体", Font.BOLD, 25));
      g2.drawString(userAgentVO.getAgentTypeName(), 380, 419);

      g2.setFont(new Font("宋体", Font.BOLD, 18));
      String certNum = userAgentVO.getCertNum();
      String zip = "";
      String addressId = userAgentVO.getAddress();
      if (StringUtils.isNotEmpty(addressId)) {
        Address address = addressService.load(addressId);
        if (address != null) {
          Zone zone = zoneMapper.selectByPrimaryKey(address.getZoneId());
          if (zone != null) {
            zip = zone.getZipCode();
          }
          if (StringUtils.isNotEmpty(zip) && zip.length() > 3) {
            zip = zip.substring(0, 3);
          }
        }
      }
      // 证书的编码需要带上地区编码,精确到省的编码
      g2.drawString(certNum + (zip == null ? "" : zip), 172, 697);

      g2.drawString(userAgentVO.getStartDate(), 172, 728);
      g2.drawString(userAgentVO.getEndDate(), 330, 728);

      g2.setFont(new Font("宋体", Font.BOLD, 19));
      //g2.drawString(userAgentVO.getAgentTypeName(), 458, 437);

      String name = userAgentVO.getName();
      if (name.length() == 2) {
        g2.drawString(name, 290, 447);
      } else if (name.length() == 3) {
        g2.drawString(name, 282, 447);
      } else if (name.length() == 4) {
        g2.drawString(name, 270, 447);
      } else {
        g2.drawString(name, 262, 447);
      }
      g2.drawString("19911111111", 210, 665);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。

  }

  private String getEndDate(Date start) {
    // 授权截止时间为申请日期的当前最后一天
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年");
    return formatter.format(start) + "12月31日";
  }

  /**
   * 生成代理申请分享二维码
   */
  @RequestMapping(value = "/userAgent/qrcode", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] generateQrcode(HttpServletRequest req) {

    String parentId = this.getCurrentIUser().getId();
    String shareUrl = getUrl(req, parentId);
    int width = 800;
    int height = 800;
    BufferedImage ImageQrcode = null;
    try {
      HashMap<EncodeHintType, Object> hints = new HashMap<>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);

      BitMatrix byteMatrix;
      byteMatrix = new MultiFormatWriter()
          .encode(shareUrl, BarcodeFormat.QR_CODE, width, height, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (WriterException e) {
      log.error("generateQrcode error", e);
      throw new RuntimeException(e);
    }

    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {

      Font font = new Font("宋体", Font.BOLD, 20);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);
      g2.drawImage(ImageQrcode, 0, 0, null);
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。

  }

  public static void main(String[] args) {
    // 授权证书在qiliu上的地址
    String certImg = "qn|xaya|FvVEOIpMrCNs0doK4q5jBMA01BLI";
    // 生成经销商授权证书图片
    int width = 708;
    int height = 1001;

//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
    String imgUrl = "http://images.handeson.com/FrabqDMWuYF2Cn5nArhO3TNSg4BE";
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {

      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      Font font = new Font("宋体", Font.BOLD, 20);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);

      // 显示*号的身份证号
      String idcard = "420112198507251518";
      Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
      Matcher m = p.matcher(idcard);
      String hiddenIdcard = m.replaceAll("$1********$3");
      g2.drawImage(imageImgRect, 0, 0, null);
      // 将代理用户的信息写到图片固定的位置上

      g2.drawString("曹洪辉", 150, 326);
      g2.drawString("fb_chh", 150, 359);
      g2.drawString(hiddenIdcard, 150, 390);

      g2.setFont(new Font("宋体", Font.BOLD, 23));
      g2.drawString("董事", 380, 419);

      g2.setFont(new Font("宋体", Font.BOLD, 18));
      g2.drawString("19911111111", 210, 665);
      g2.drawString("QPTQPY123221" + "", 172, 697);
      g2.drawString("2017年02月05日", 172, 728);
      g2.drawString("2017年02月05日", 330, 728);

      g2.setFont(new Font("宋体", Font.BOLD, 19));
      //g2.drawString(userAgentVO.getAgentTypeName(), 458, 437);

      String name = "曹洪曹曹洪";
      if (name.length() == 2) {
        g2.drawString(name, 290, 447);
      } else if (name.length() == 3) {
        g2.drawString(name, 282, 447);
      } else if (name.length() == 4) {
        g2.drawString(name, 270, 447);
      } else {
        g2.drawString(name, 262, 447);
      }

      File file = new File("/home/caohonghui/1.png");

      ImageIO.write(bi, "png", file);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }


  /**
   * 代理佣金列表导出
   */
  @ResponseBody
  @RequestMapping("/userAgent/exportCommission")
  public void exportCommission(HttpServletRequest request, HttpServletResponse resp) {

    List<UserAgentCommissionVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String buyerPhone = request.getParameter("buyerPhone");
    String buyerName = request.getParameter("buyerName");
    String sellerPhone = request.getParameter("sellerPhone");
    String sellerName = request.getParameter("sellerName");

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (Exception e) {
      }
    }
    params.put("startDate", startDate);
    params.put("buyerPhone", buyerPhone);
    params.put("buyerName", buyerName);
    params.put("sellerPhone", sellerPhone);
    params.put("sellerName", sellerName);

    viewPages = userAgentService.listUserCommission(null, params);

    String sheetStr = "代理佣金";
    String filePrefix = "commissionList";
    String[] secondTitle = new String[]{
        "订单号",
        "订单价格",
        "订单数量",
        "佣金比例",
        "下单经销商",
        "经销商电话",
        "下单经级别",
        "收利人",
        "收利人电话",
        "收利人级别",
        "返利金额",
        "返利时间",
        "佣金发放状态"};

    String[] strBody = new String[]{
        "getOrderNo",
        "getPrice",
        "getAmount",
        "getRate",
        "getBuyerName",
        "getBuyerPhone",
        "getBuyerTypeStr",
        "getName",
        "getPhone",
        "getTypeStr",
        "getFee",
        "getCreatedAtStr",
        "getOfferedStr"};

    excelService.export(filePrefix, viewPages, UserAgentCommissionVO.class, sheetStr,
        transParams2Title(params), secondTitle, strBody, resp, true);
  }

  /**
   * 代理会员列表导出
   */
  @ResponseBody
  @RequestMapping("/userAgent/exportMember")
  public void exportMember(HttpServletRequest request, HttpServletResponse resp) {

    List<UserAgentVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();

    String status = request.getParameter("selectStatus");
    String type = request.getParameter("level");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String phone = request.getParameter("phone");
    String name = request.getParameter("agentName");
    String parentName = request.getParameter("parentName");

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (Exception e) {
      }
    }

    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(parentName)) {
      params.put("parentName", "%" + parentName + "%");
    }

    params.put("startDate", startDate);
    params.put("status", status);
    params.put("type", type);

    viewPages = userAgentService.list(null, params);
    for (UserAgentVO vo : viewPages) {
      UserAgentVO parentVo = userAgentService.selectByUserId(vo.getParentUserId());
      if (parentVo == null) {
        parentVo = new UserAgentVO();
        parentVo.setName("总部");
      }
      vo.setParentName(parentVo.getName());

      // 查询该用户的未结算佣金和已结算佣金
      BigDecimal withdrawFee = commissionService.getWithdrawAll(vo.getUserId());//获取累计收入
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
      Date nowDate = new Date();
      Calendar cal = Calendar.getInstance();
      cal.setTime(nowDate);
      cal.add(Calendar.MONTH, -1);
      Date lastMonth = cal.getTime();
      String lastMonthStr = sdf.format(lastMonth);
      BigDecimal unWithdrawFee = commissionService
          .getWithdrawOfMonth(vo.getUserId(), lastMonthStr);//获取上个月的收益
      vo.setWithdrawFee(withdrawFee);
      vo.setUnWithdrawFee(unWithdrawFee);
    }

    String sheetStr = "代理会员";
    String filePrefix = "memberList";
    String[] secondTitle = new String[]{
        "代理姓名",
        "代理信息",
        "代理级别",
        "身份证号",
        "授权编码",
        "上级名称",
        "未结算佣金",
        "已结算佣金",
        "加入时间",
        "状态"};

    String[] strBody = new String[]{
        "getName",
        "getPhone",
        "getTypeStr",
        "getIdcard",
        "getCertNum",
        "getParentName",
        "getUnWithdrawFee",
        "getWithdrawFee",
        "getCreatedAtStr",
        "getStatusStr"};

    excelService.export(filePrefix, viewPages, UserAgentVO.class, sheetStr,
        transParams2TitleMember(params), secondTitle, strBody, resp, true);
  }

  private String transParams2TitleMember(Map<String, Object> params) {

    LinkedHashMap keyCn = new LinkedHashMap();
    keyCn.put("startDate", "加入开始时间");
    keyCn.put("endDate", "加入结束时间");
    keyCn.put("status", "状态");
    keyCn.put("type", "代理级别");
    keyCn.put("phone", "手机号");
    keyCn.put("name", "代理姓名");
    keyCn.put("parentName", "上级名称");

    UserAgent agent = new UserAgent();

    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          // 结束时间需要特别处理，查询条件里加了一天，这里展示需要减去一天
          if ("endDate".equals(key)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date endDate = (Date) value;
            Date date = DateUtils.addDays(endDate, -1);
            value = sdf.format(date);
          }

          // 枚举类型的条件，值需要返回中文
          if ("status".equals(key)) {
            agent.setStatus(AgentStatus.valueOf(value.toString()));
            value = agent.getStatusStr();
          }
          if ("type".equals(key)) {
            agent.setType(AgentType.valueOf(value.toString()));
            value = agent.getTypeStr();
          }

          result += "{" + keyCn.get(key) + "=" + value.toString().replaceAll("%", "") + "}";
          i++;
        }
      }
    }
    return "代理会员查询条件：" + result;

  }

  private String transParams2Title(Map<String, Object> params) {
    LinkedHashMap keyCn = new LinkedHashMap();
    keyCn.put("startDate", "返利开始时间");
    keyCn.put("endDate", "返利结束时间");
    keyCn.put("buyerPhone", "下单人手机");
    keyCn.put("buyerName", "下单经销商");
    keyCn.put("sellerPhone", "收利人手机");
    keyCn.put("sellerName", "收利人");

    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          // 结束时间需要特别处理，查询条件里加了一天，这里展示需要减去一天
          if ("endDate".equals(key)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date endDate = (Date) value;
            Date date = DateUtils.addDays(endDate, -1);
            value = sdf.format(date);
          }
          result += "{" + keyCn.get(key) + "=" + value.toString().replaceAll("%", "") + "}";
          i++;
        }
      }
    }
    return "返利查询条件：" + result;

  }

  /**
   * 代理概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/userAgent/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取代理概况中的今日数据,本周订单,代理,交易额,佣金数
    summary = userAgentService.getSummary(shopId);

    return new ResponseObject<>(summary);
  }

  /**
   * 代得到总店userid
   */
  @ResponseBody
  @RequestMapping("/userAgent/getRootUserId")
  public ResponseObject<String> getRootUserId() {
    String userId = "";
    String shopId = getCurrentIUser().getShopId();
    Shop shop = shopService.load(shopId);
    userId = shop.getOwnerId();
    return new ResponseObject<>(userId);
  }

  /**
   * 获取代理的合同图片
   */
  @ResponseBody
  @RequestMapping("/userAgent/getContractImg")
  public ResponseObject<ArrayList> getContractImg() {
    ArrayList list = new ArrayList();
    String userId = getCurrentIUser().getId();
    UserType type = userService.getUserType(userId);
    // 已经申请通过的代理才能查看合同信息
    if (type == null || type != UserType.B2B) {
      return new ResponseObject<>(list);
    }
    String imgarr = userAgentService.getContractImg(userId);
    // 如果当前代理没有合同图片，则即时合成合同图片
    if (StringUtils.isEmpty(imgarr)) {
      genContractImg(userId);
      imgarr = userAgentService.getContractImg(userId);
    }
    String[] imgs = imgarr.split(",");
    for (String img : imgs) {
      list.add(img.trim());
    }
    return new ResponseObject<>(list);
  }

  /**
   * 生成代理的合同图片
   */
  @ResponseBody
  @RequestMapping("/userAgent/genContractImg")
  public void genContractImgApi() {
    String userId = getCurrentIUser().getId();
    genContractImg(userId);
  }

  /**
   * 合成代理图片
   */
  private void genContractImg(String userId) {
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {
      return;
    }
    // 合同共有7页，对应qiniu服务器上的7张图片
    String contract_001 = "http://images.handeson.com/Fo4l_XRfLYVJdNY8rmb3hkf3Ggpk";
    String contract_002 = "http://images.handeson.com/FkSBrlbXabdrTWvt1b-c37et6bEs";
    String contract_003 = "http://images.handeson.com/Fihd9rJxGiF0Bynz8iQSrP_5qV9w";
    String contract_004 = "http://images.handeson.com/Fht-YgMJQsgu4P4sQgPGTvoug9pi";
    String contract_005 = "http://images.handeson.com/FpcCbvBS71UsqplIn2yNFZ9BV1W6";
    String contract_006 = "http://images.handeson.com/Fuc7TJ4VbgHVDitV8Wz2OFm-Ydas";
    String contract_007 = "http://images.handeson.com/FjY1DkdfkwZ25SoWvan_juEamPJA";
    ArrayList contractImgs = new ArrayList();
    // 第一张与第七张图片需要将用户信息合成一张新的图片
    int width = 1240;
    int height = 1753;

    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    ByteArrayOutputStream os7 = new ByteArrayOutputStream();//新建流。
    try {

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      // 第一张图片合成
      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(contract_001);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      Font font = new Font("宋体", Font.BOLD, 27);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = Color.BLACK;
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);
      g2.drawString("HT" + IdTypeHandler.decode(userAgentVO.getId()), 800, 150);
      g2.drawString(userAgentVO.getName(), 270, 688);
      g2.drawString(userAgentVO.getIdcard(), 330, 732);
      g2.drawString(userAgentVO.getWeixin(), 300, 773);
      g2.drawString(userAgentVO.getPhone(), 300, 814);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
      InputStream is1 = new ByteArrayInputStream(os.toByteArray());
      java.util.List<InputStream> ins = new ArrayList<>();
      ins.add(is1);

      // 第七张图片合成
      imageImg = ImgUtils.getImageFromNetByUrl(contract_007);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);
      // 甲方信息
      g2.drawString("武汉巴巴贝尔信息科技有限公司", 260, 858);
      g2.drawString("武汉市硚口区同心健康产业园A座1303", 260, 900);
      g2.drawString("15827268585", 260, 942);
      g2.drawString("朱玲", 300, 982);
      g2.drawString(sdf.format(userAgentVO.getCreatedAt()), 260, 1025);

      // 乙方信息
      g2.drawString(userAgentVO.getName(), 815, 858);
      g2.drawString(userAgentVO.getIdcard(), 863, 900);
      g2.drawString(userAgentVO.getPhone(), 815, 942);
      g2.drawString(userAgentVO.getWeixin(), 835, 982);
      g2.drawString(sdf.format(userAgentVO.getCreatedAt()), 815, 1025);

      ImageIO.write(bi, "png", os7);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
      InputStream is7 = new ByteArrayInputStream(os7.toByteArray());
      ins.add(is7);

      // 将合成的图片上传到qiniu服务器
      final java.util.List<UpLoadFileVO> vos = qiniu.uploadImgStream(ins, FileBelong.PRODUCT);
      String contract_001_new = qiniu.genQiniuFileUrl(vos.get(0).getKey());
      String contract_007_new = qiniu.genQiniuFileUrl(vos.get(1).getKey());
      contractImgs.add(contract_001_new);
      contractImgs.add(contract_002);
      contractImgs.add(contract_003);
      contractImgs.add(contract_004);
      contractImgs.add(contract_005);
      contractImgs.add(contract_006);
      contractImgs.add(contract_007_new);

      //contractImgs.add(imgUrl);
      // 更新代理的合同图片字段
      UserAgent updateUserAgent = new UserAgent();
      updateUserAgent.setId(userAgentVO.getId());
      updateUserAgent.setContractImg(StringUtils.strip(contractImgs.toString(), "[]"));
      userAgentService.modify(updateUserAgent);

    } catch (Exception e) {
      log.error("生成代理合同图片报错", e);
    }
  }

  /**
   * 更新用户手势图片
   */
  @ResponseBody
  @RequestMapping("/userAgent/updateLifeImg")
  public ResponseObject<Boolean> updateLifeImg(@RequestParam(required = false) String url) {
    String userId = getCurrentIUser().getId();
    int result = userAgentService.updateLifeImg(userId, url);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 新用户身份证照片
   */
  @ResponseBody
  @RequestMapping("/userAgent/updateIdcardImg")
  public ResponseObject<Boolean> updateIdcardImg(@RequestParam(required = false) String url) {
    String userId = getCurrentIUser().getId();
    int result = userAgentService.updateIdcardImg(userId, url);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/userAgent/updateMultiPartImg")
  public Map<String, Object> updateMultiPartImg(@RequestParam("imgFile") MultipartFile file,
      @RequestParam("id") String id,
      @RequestParam("type") String type) {
    Map<String, Object> map = new HashMap<>();
    String imgUrl = "";
    if (!file.isEmpty()) {
      InputStream fileInputStream;
      try {
        fileInputStream = file.getInputStream();
        UpLoadFileVO vo = resourceFacade.uploadFileStream(fileInputStream, FileBelong.PRODUCT);
        String img = vo.getId();
        imgUrl = resourceFacade.resolveUrl(img);
        UserAgent agent = userAgentService.selectByPrimaryKey(id);
        int result = 0;
        if (StringUtils.equals("idCard", type)) {
          result = userAgentService.updateIdcardImg(agent.getUserId(), imgUrl);
        } else if (StringUtils.equals("lifeCard", type)) {
          result = userAgentService.updateLifeImg(agent.getUserId(), imgUrl);
        } else {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "图片类型不正确");
        }
        if (result == 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改图片失败");
        }
      } catch (Exception e) {
        log.error("文件上传失败", e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, e);
      }
    }
    map.put("img", imgUrl);
    map.put("error", "上传成功");
    return map;
  }

  @ResponseBody
  @RequestMapping("/userAgent/addWhiteList")
  public ResponseObject<Boolean> addWhiteList(@RequestParam("phone") String phone) {
    UserAgentWhiteList exists = whiteListService.loadByPhone(phone);
    if (exists != null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该代理已经在白名单中");
    }
    exists = new UserAgentWhiteList();
    exists.setPhone(phone);
    exists.setEnable(true);
    Boolean result = whiteListService.save(exists);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/userAgent/updateWhiteList")
  public ResponseObject<Boolean> updateWhiteList(@RequestParam("id") String id,
      String phone,
      Boolean enable) {
    UserAgentWhiteList exists = whiteListService.load(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在");
    }
    if (enable != null) {
      exists.setEnable(enable);
    }
    if (!StringUtils.isBlank(phone)) {
      exists.setPhone(phone);
    }
    Boolean result = whiteListService.update(exists);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/userAgent/removeWhiteList/{id}")
  public ResponseObject<Boolean> removeWhiteList(@PathVariable("id") String id) {
    UserAgentWhiteList exists = whiteListService.load(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要删除的用户不存在");
    }
    Boolean result = whiteListService.delete(id);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping(value = "/userAgent/whitelist/{id}")
  public ResponseObject<UserAgentWhiteList> view(@PathVariable("id") String id) {
    UserAgentWhiteList whiteList = whiteListService.load(id);
    return new ResponseObject<>(whiteList);
  }

  @ResponseBody
  @RequestMapping("userAgent/viewWhiteList")
  public ResponseObject<Map<String, Object>> viewWhiteList(String order, String direction,
      Pageable pageable, String keyword) {
    if (StringUtils.isBlank(direction)) {
      direction = "ASC";
    }
    Map<String, Object> result = new HashMap<>();
    List<UserAgentWhiteList> whiteLists = whiteListService.list(order, pageable,
        Sort.Direction.fromString(direction), keyword);
    int count = whiteListService.count();
    result.put("list", whiteLists);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("userAgent/inWhiteList")
  public ResponseObject<Boolean> inWhiteList(@RequestParam("phone") String phone) {
    Boolean result = whiteListService.searchInListWithPhone(phone);
    return new ResponseObject<>(result);
  }

  /**
   * 根据wxbotId查询某个代理
   */
  @ResponseBody
  @RequestMapping("/userAgent/selectByWxbotId")
  public ResponseObject<UserAgentVO> selectByWxbotId(@RequestParam String keys) {
    UserAgentVO vo = userAgentService.selectByWxbotId(keys);
    if (vo != null) {
      vo.setAgentTypeName(getAgentTypeName(vo.getType()));
    }
    return new ResponseObject<>(vo);
  }

  /**
   * 根据手机号或身份证号查询某个代理
   */
  @ResponseBody
  @RequestMapping("/userAgent/selectByPhoneOrIdcard")
  public ResponseObject<UserAgentVO> selectByPhoneOrIdcard(@RequestParam String keys) {
    UserAgentVO vo = userAgentService.selectByPhoneOrIdcard(keys);
    if (vo != null) {
      vo.setAgentTypeName(getAgentTypeName(vo.getType()));
    }
    return new ResponseObject<>(vo);
  }


  /**
   * 更新wxbotId字段
   */
  @ResponseBody
  @RequestMapping("/userAgent/updateWxbotId")
  public ResponseObject<Boolean> updateWxbotId(@RequestParam(required = false) String id,
      @RequestParam(required = false) String wxbotId) {
    int result = userAgentService.updateWxbotId(id, wxbotId);
    return new ResponseObject<>(result > 0);
  }


  /**
   * 获取所有联合创始人代理列表
   */
  @ResponseBody
  @RequestMapping("/userAgent/listFounder")
  public ResponseObject<Map<String, Object>> listFounder(Pageable pageable,
      HttpServletRequest request) {
    String userId = this.getCurrentIUser().getId();
    String shopId = this.getCurrentIUser().getShopId();
    List<UserAgentVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String phone = request.getParameter("phone");
    String name = request.getParameter("name");

    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }

    viewPages = userAgentService.listFounder(pageable, params);
    for (UserAgentVO vo : viewPages) {
      UserAgentVO parentVo = userAgentService.selectByUserId(vo.getParentUserId());
      if (parentVo == null) {
        parentVo = new UserAgentVO();
        parentVo.setName("总部");
      }
      vo.setParentVo(parentVo);
      // 将地区中文显示
      String area = vo.getArea();
      StringBuilder sb = new StringBuilder();
      if (StringUtils.isNotEmpty(area)) {
        Zone areaVo = zoneService.load(area);
        for (String zone : areaVo.getPath().split(">")) {
          if (StringUtils.isNotEmpty(zone)) {
            Zone zoneVo = zoneService.load(zone);
            sb.append(zoneVo.getName());
          }
        }
        sb.append(areaVo.getName());
      }
      vo.setAreaName(sb.toString());
    }

    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", userAgentService.selectCnt(params));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 更新area字段
   */
  @ResponseBody
  @RequestMapping("/userAgent/updateArea")
  public ResponseObject<Boolean> updateArea(@RequestParam(required = false) String id,
      @RequestParam(required = false) String area) {
    int result = userAgentService.updateArea(id, area);
    // 有联合创始人修改了封地区域时，需要刷新缓存
    founderAreaCache.refresh();
    return new ResponseObject<>(result > 0);
  }

}
