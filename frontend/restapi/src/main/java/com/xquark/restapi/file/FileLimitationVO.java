package com.xquark.restapi.file;

import com.google.common.base.Preconditions;
import com.xquark.dal.type.FileUploadType;

/**
 * Created by wangxinhua on 17-12-20. DESC:
 */
public class FileLimitationVO {

    private final FileUploadType type;

    private FileLimitationVO(FileUploadType type) {
        this.type = type;
    }

    public static FileLimitationVO of(FileUploadType type) {
        Preconditions.checkNotNull(type);
        return new FileLimitationVO(type);
    }

    public String getStr() {
        return type.getStr();
    }

    public int getSize() {
        return type.getLimitSize();
    }

    public String[] getPostfix() {
        return type.getPostfix();
    }
}
