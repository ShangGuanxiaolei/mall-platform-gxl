package com.xquark.restapi.msg;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.Message;
import com.xquark.dal.type.MessageType;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.msg.vo.UserMessageVO;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class MessageController {

  @Autowired
  private MessageService messageService;

  @ResponseBody
  @RequestMapping("/messages")
  public ResponseObject<List<UserMessageVO>> inbox() {
    List<UserMessageVO> list = messageService.loadInboxMessages();
    return new ResponseObject<>(list);
  }

  @ResponseBody
  @RequestMapping("/message/{id}")
  public ResponseObject<UserMessageVO> view(@PathVariable String id) {
    UserMessageVO message = messageService.loadUserMessage(id);
    return new ResponseObject<>(message);
  }

  @ResponseBody
  @RequestMapping("/message/view")
  public ResponseObject<Message> viewMsg(@RequestParam("id") String id) {
    Message message = messageService.loadMessage(id);
    return new ResponseObject<>(message);
  }

  @ResponseBody
  @RequestMapping("/message/delete")
  public ResponseObject<Boolean> delete(@RequestParam("id") String id) {
    boolean result = messageService.delete(id);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/message/send")
  public ResponseObject<UserMessageVO> send(@RequestParam(value = "userId") String userId) {
    String title = "测试标题";
    String content = "测试内容";
    UserMessageVO message = messageService.sendSystemMessage(userId, title, content);
    return new ResponseObject<>(message);
  }

  @ResponseBody
  @RequestMapping("/message/listByAdmin")
  public ResponseObject<Map<String, Object>> listByAdmin(MessageType type, Pageable pageable) {
    Map<String, MessageType> params = Collections.singletonMap("type", type);
    List<Message> list = messageService.listMessageByAdmin(params, pageable);
    long total = messageService.countMessageByAdmin(params);
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/message/sendNotification")
  public ResponseObject<Boolean> sendNtification(MsgSendForm form) {
    String msgId = form.getMsgId();
    List<String> userIds = form.getUserIds();
    try {
      messageService.sendNotification(userIds, msgId);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "发送失败", e);
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  @ResponseBody
  @RequestMapping("/message/save")
  public ResponseObject<Boolean> saveNotification(@Validated MessageForm messageForm,
      Errors errors) {
    ControllerHelper.checkException(errors);
    String id = messageForm.getId();
    Message toSave = Transformer.fromBean(messageForm, Message.class);
    boolean result;
    if (StringUtils.isBlank(id)) {
      result = messageService.saveNotification(toSave);
    } else {
      result = messageService.updateNotification(toSave);
    }
    return new ResponseObject<>(result);
  }

}
