package com.xquark.restapi.user;

import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.url.UrlHelper;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.CustomerCareerLevelMapper;
import com.xquark.dal.mapper.ProductCollectionMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.IdentityVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionGrouponUserVO;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.bank.CustomerBankService;
import com.xquark.service.cart.CartService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.member.MemberService;
import com.xquark.service.memberCard.MemberCardService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.msg.vo.UserMessageVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.signin.SignInService;
import com.xquark.service.team.TeamService;
import com.xquark.service.team.UserTeamService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.TwitterLevelService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.twitter.impl.TwitterShopComServiceImpl;
import com.xquark.service.user.UserService;
import com.xquark.service.user.vo.WhiteListInfo;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.UserCardService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author chh 2017-07-19
 */
@RestController
@Api(value = "user", description = "我的")
public class UserController extends BaseController {
  private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private UserService userService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private UserPartnerService userPartnerService;

  @Autowired
  private UserTeamService userTeamService;

  @Autowired
  private TeamService teamService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ShopAccessLogService shopAccessLogService;

  @Autowired
  private MemberService memberService;

  @Autowired
  private TinyUrlService tinyUrlService;

    @Autowired
    private CartService cartService;

  @Autowired
  private PromotionGrouponService promotionGrouponService;

  @Autowired
  private SignInService signInService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private TwitterShopComServiceImpl twitterShopComServiceImpl;

  @Autowired
  private TwitterLevelService twitterLevelService;

  @Autowired
  private UserCardService userCardService;

  @Autowired
  private MemberCardService memberCardService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ProductCollectionMapper productCollectionMapper;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private CareerLevelService careerLevelService;

    @Autowired
    UrlHelper urlHelper;

    @Autowired
    private CustomerBankService customerBankService;

    @Autowired
    private CustomerCareerLevelMapper customerCareerLevelMapper;

  @Value("${site.web.host.name}")
  String siteHost;

  /**
   * 获取用户信息，返回用户，店铺，推客，合伙人，战队等信息
   */
  @ResponseBody
  @RequestMapping(value = "/user/getInfo", method = RequestMethod.POST)
  @ApiOperation(value = "获取用户信息", notes = "获取用户信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject getInfo(HttpServletRequest req) {

      try {
          updateSigninedData(req);  //更新登录用户的数据
          UserType userType = userService
                  .getUserType(userService.getCurrentUser().getId());

          User user = userService.load(userService.getCurrentUser().getId());
          Long cpId = user.getCpId();
          if (StringUtils.isNotEmpty(user.getZoneId())) {
              List<Zone> zoneList = zoneService.listParents(user.getZoneId());
              String details = "";
              for (Zone zone : zoneList) {
                  details += zone.getName();
              }
              user.setZoneStr(details);
          }
          user.setCollections(productCollectionMapper.selectCntByUserId(cpId.toString()));

          String unionId = user.getUnionId();
          RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
          if (org.springframework.util.StringUtils.isEmpty(unionId) && cpId != null) {
              unionId = redisUtils.get(String.valueOf(cpId));
          }
          String customerNumber = user.getCustomerNumber();
          UserVO userVO = new UserVO(
                  user,
                  urlHelper.genShopUrl(userService.getCurrentUser()
                          .getShopId()));
//      HealthTestProfile profile = profileService.loadByUser(user.getId());
//      if (profile != null) {
//        userVO.setHasProfile(true);
//      }

          String IdentityName=userService.selectIdType(cpId.toString());
          String userViViType=userService.selectViViType(user.getCpId().toString());
          userVO.setIdentityName("顾客");
          userVO.setIdentityType("RC");
          if("DS".equals(IdentityName) || "DS".equals(userViViType)){
              userVO.setIdentityName("会员");
              userVO.setIdentityType("DS");
          }
          if("SP".equals(IdentityName) || "SP".equals(userViViType)){
              userVO.setIdentityName("代理");
              userVO.setIdentityType("SP");
          }

          userVO.setType(userType);
          // 是否有身份
          userVO.setHasIdentity(customerProfileService.hasIdentity(cpId));
          // 是否点亮
          userVO.setIsLighten(customerProfileService.isLighten(cpId));
          userVO.setCustomerNumber(customerNumber);
          if (org.springframework.util.StringUtils.isEmpty(user.getAvatar())) {
              // dts同步时不会同步头像信息
              String customerAvatar = customerProfileService.loadAvatarByCpIdAndUnionId(cpId, unionId);
              userVO.setAvatar(customerAvatar);
          }

          CustomerProfile customerProfile = customerProfileService.queryCustomerProfileByCpId(cpId);
          if(customerProfile != null){
              userVO.setBankUserName(customerProfile.getNameCN());
              userVO.setBankTincode(customerProfile.getTinCode());
          }

          List<CustomerBank> customerBankList = customerBankService.queryCustomerBankListByCpId(cpId);
          userVO.setHasBankInfo(false);
          if(customerBankList != null && !customerBankList.isEmpty()){
              CustomerBank customerBank = customerBankList.get(0);
              userVO.setHasBankInfo(true);
              int len = customerBank.getBankAccount().length();
              userVO.setBankCardNumber(customerBank.getBankAccount().substring(len - 4));

              String bankUserName = userVO.getBankUserName();
              String bankTincode = userVO.getBankTincode();
              if((bankUserName == null || bankUserName.trim().length() == 0)
                      || (bankTincode == null || bankTincode.trim().length() == 0)){

                  userVO.setBankUserName(customerBank.getAccountName());
                  userVO.setBankTincode(customerBank.getTincode());
              }
          }

          //是否是店主身份
          CustomerCareerLevel customerCareerLevel = customerCareerLevelMapper.selectByPrimaryKey(cpId);
          userVO.setIsShopOwner(false);
          userVO.setIsMember(false);
          userVO.setIsProxy(false);
          if(customerCareerLevel != null) {
              String sp = customerCareerLevel.getViviLifeType();
              if("SP".equals(sp)) {
                  userVO.setIsShopOwner(true);
              }

              //是全国代理或店主，则显示代理价
              if(customerCareerLevel.getIsLightening() ||
                      (!customerCareerLevel.getIsLightening() && "SP".equals(sp))){
                  userVO.setIsProxy(true);
              }
              if(userVO.isHasIdentity()){ //是vip或超级会员，则显示会员价
                  userVO.setIsMember(true);
              }
          }

          /**
           * 是否有我的业绩
           */
          boolean isShopkeeper = careerLevelService.isShopkeeper(cpId);
          boolean isVip = careerLevelService.isVip(cpId);
          if (isVip || isShopkeeper) {
              userVO.setIsAchievement(true);
          } else {
              userVO.setIsAchievement(false);
          }

          redisUtils.set("login_return_" + cpId, JSONObject.toJSONString(new IdentityVO(
                  userVO.getIsProxy(), userVO.getIsMember())));

          // 上级信息
          Long uplineCpId = customerProfile.getUplineCpId();
          if(!Objects.isNull(uplineCpId)){
              // 加三个0
              userVO.setUplineCpId("000" + uplineCpId);
              User u = userService.selectUserIdByCpId(uplineCpId.toString());
              if(u != null && StringUtils.isNotBlank(u.getNickName())) userVO.setUplineNickName(u.getNickName());
              else {
                  if(u != null) userVO.setUplineNickName(u.getName());
              }

              // 类型
              if(uplineCpId.equals(2000002L) || u == null){
                  userVO.setUplineCpId("公司");
                  userVO.setUplineType("company");
              }else {
                  userVO.setUplineType("self");
              }
          }

          return new ResponseObject<>(userVO);
      } catch (BizException e) {
          // 未登录状态
          log.error("登录失败", e);
          return new ResponseObject<>("登录失败: " + e.getMessage(),
                  GlobalErrorCode.UNAUTHORIZED);
      }
  }

  @ResponseBody
  @RequestMapping(value = "/user/update", method = RequestMethod.POST)
  @ApiOperation(value = "更新用户信息", notes = "更新用户信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> update(@ModelAttribute User user, HttpServletRequest req) {
    // 更新用户表
    String userId = this.getCurrentIUser().getId();
    user.setId(userId);
    boolean flag = userService.updateByBosUserInfo(user);
    return new ResponseObject<>(flag);
  }


  /**
   * 查询手机号对应的用户是否已经存在
   */
  @RequestMapping("/user/checkPhone")
  @ApiIgnore
  public ResponseObject<Boolean> checkPhone(@RequestParam("phone") String phone) {
    User user = userService.loadAnonymousByPhone(phone);
    return new ResponseObject<>(user != null);
  }

  /**
   * 我的小店会员查询
   */
  @RequestMapping("/user/myMembers")
  @ApiIgnore
  public ResponseObject<List<UserVO>> myMembers() {
    String userId = getCurrentIUser().getId();
    User user = userService.load(userId);
    List<UserVO> userVOs = null;

    return new ResponseObject<>(userVOs);
  }

  /**
   * 我的店铺订单查询
   */
  @RequestMapping("/user/order/seller")
  @ApiIgnore
  public ResponseObject<List<OrderVO>> sellerOrder(
      @RequestParam(value = "status", required = false) String status, HttpServletRequest req,
      Pageable pager) {
    String userId = getCurrentIUser().getId();
    User user = userService.load(userId);
    List<OrderVO> orders = null;
    Map<String, Object> params = new HashMap<>();
    if (status != null) {
      String sellerShopId = user.getShopId();
      String spId = shopTreeService.selectRootShopByShopId(sellerShopId).getRootShopId();

      String[] statusArr = null;
      if ("topay".equals(status)) {
        statusArr = new String[]{OrderStatus.SUBMITTED.name()};
      }

      if ("tosend".equals(status)) {
        statusArr = new String[]{OrderStatus.PAID.name()};
      }

      if ("send".equals(status)) {
        statusArr = new String[]{OrderStatus.SHIPPED.name()};
      }

      if ("finish".equals(status)) {
        statusArr = new String[]{OrderStatus.CANCELLED.name(), OrderStatus.SUCCESS.name(),
            OrderStatus.CLOSED.name()};
      }
      if ("refund".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
      }
      orders = memberService
          .getSellerOrderListByOrderStatus(statusArr, sellerShopId, spId, pager, params);
    }

    return new ResponseObject<>(orders);
  }

  /**
   * 我的店铺售后订单查询
   */
  @RequestMapping("/user/order/seller/refund")
  @ApiIgnore
  public ResponseObject<List<OrderVO>> sellerOrderRefund(@RequestParam("status") String status,
      HttpServletRequest req, Pageable pager) {
    String userId = getCurrentIUser().getId();
    User user = userService.load(userId);
    List<OrderVO> orders = null;
    Map<String, Object> params = new HashMap<>();
    if (status != null) {
      String sellerShopId = user.getShopId();
      String spId = shopTreeService.selectRootShopByShopId(sellerShopId).getRootShopId();

      String[] statusArr = null;

      if ("all".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name(), OrderStatus.CLOSED.name()};
      }

      if ("toclose".equals(status)) {
        statusArr = new String[]{OrderStatus.CLOSED.name()};
      }

      if ("toreturn".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        params.put("buyerRequire", "2");
      }

      if ("torefund".equals(status)) {
        params.put("buyerRequire", "1");
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
      }

      orders = memberService
          .getSellerOrderListByOrderStatus(statusArr, sellerShopId, spId, pager, params);
    }

    return new ResponseObject<>(orders);
  }

  @ResponseBody
  @RequestMapping(value = "/user/order/promotion")
  @ApiOperation(value = "活动订单查询", notes = "我的活动订单查询", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, ?>> promotionOrder(@ApiParam(value = "活动类型", required = true)
  @RequestParam("pType") PromotionType type, Pageable pageable) {
    Map<String, Object> result = new HashMap<>();
    List<OrderVO> list;
    Long count;
    IUser currUser = getCurrentIUser();
    String userId = currUser.getId();
    String sellerShopId = currUser.getShopId();
    String spId = shopTreeService.selectRootShopByShopId(sellerShopId).getRootShopId();
    try {
      list = orderService.listByPromotionTypeAndUserId(type, userId, spId, pageable);
      count = orderService.countByPromotionTypeAndUserId(type, userId, spId);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "订单查询失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    result.put("list", list);
    result.put("total", count);
    return new ResponseObject<Map<String, ?>>(result);
  }

  /**
   * 我的订单查询
   */
  @ResponseBody
  @RequestMapping(value = "/user/order/buyer", method = RequestMethod.POST)
  @ApiOperation(value = "我的订单查询", notes = "我的订单查询", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> buyerOrder(
      @ApiParam(value = "订单状态（topay代表待付款，tosend代表待发货，send代表待收货，finish代表已完成，refund代表退款中）", required = false) @RequestParam(value = "status", required = false) String status,
      HttpServletRequest req, Pageable pager) {
    HashMap result = new HashMap();
    String userId = getCurrentIUser().getId();
    User user = userService.load(userId);
    List<OrderVO> orders = null;
    Map<String, Object> params = new HashMap<>();
    //if (status != null) {
    String sellerShopId = user.getShopId();
    String spId = shopTreeService.selectRootShopByShopId(sellerShopId).getRootShopId();

    String[] statusArr = null;
    if ("topay".equals(status)) {
      statusArr = new String[]{OrderStatus.SUBMITTED.name()};
    }

    if ("tosend".equals(status)) {
      statusArr = new String[]{OrderStatus.PAID.name(), OrderStatus.PAIDNOSTOCK.name()};
    }

    if ("send".equals(status)) {
      statusArr = new String[]{OrderStatus.SHIPPED.name()};
    }

    if ("finish".equals(status)) {
      statusArr = new String[]{OrderStatus.CANCELLED.name(), OrderStatus.SUCCESS.name(),
          OrderStatus.CLOSED.name()};
    }
    if ("refund".equals(status)) {
      statusArr = new String[]{OrderStatus.REFUNDING.name()};
    }

    if ("tocomment".equals(status)) {
      statusArr = new String[]{OrderStatus.SUCCESS.name()};
      params.put("needComment", true);
    }
    orders = memberService.getOrderListByOrderStatus(statusArr, userId, spId, pager, params);
    long count = memberService.countOrderListByOrderStatus(statusArr, userId, spId, params);
    result.put("list", orders);
    result.put("count", count);
    //}

    return new ResponseObject<Map>(result);
  }

  /**
   * 我的退款/售后订单查询
   */
  @RequestMapping("/user/order/buyer/refund")
  @ApiIgnore
  public ResponseObject<List<OrderVO>> buyerOrderRefund(@RequestParam("status") String status,
      HttpServletRequest req, Pageable pager) {
    String userId = getCurrentIUser().getId();
    User user = userService.load(userId);
    List<OrderVO> orders = null;
    Map<String, Object> params = new HashMap<>();
    if (status != null) {
      String sellerShopId = user.getShopId();
      String spId = shopTreeService.selectRootShopByShopId(sellerShopId).getRootShopId();

      String[] statusArr = null;

      if ("all".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name(), OrderStatus.CLOSED.name()};
      }

      if ("toclose".equals(status)) {
        statusArr = new String[]{OrderStatus.CLOSED.name()};
      }

      if ("toreturn".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        params.put("buyerRequire", "2");
      }

      if ("torefund".equals(status)) {
        params.put("buyerRequire", "1");
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
      }

      orders = memberService
          .getOrderListByOrderStatus(statusArr, userId, spId, pager, params);
    }

    return new ResponseObject<>(orders);
  }

  /**
   * 我的推广
   */
  @RequestMapping("/user/myPromotion")
  @ApiIgnore
  public ResponseObject<Map> myPromotion(HttpServletRequest req) {
    String userId = getCurrentIUser().getId();
    Map map = new HashMap();
    User user = userService.load(userId);
    String shopId = user.getShopId();
    String shareUrl = "";
    Team team = teamService.selectByUserId(userId);
    if (team != null) {
      shareUrl = getTeamShareUrl(req);
    } else {
      shareUrl = getShareUrl(req);
    }

    map.put("shareTitle", "招募合伙人,带你赚钱带你飞");
    map.put("shareUrl", shareUrl);
    map.put("avatar", user.getAvatar());
    map.put("qrcodeUrl", siteHost + "/twitterCenter/promotion/qrcode/" + shopId);

    return new ResponseObject<>(map);
  }

  private String getShareUrl(HttpServletRequest req) {
    String key = tinyUrlService
        .insert(req.getScheme() + "://" + req.getServerName() + "/shop/" + getCurrentIUser()
            .getShopId() + "?currentSellerShopId=" + getCurrentIUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 战队中的推客分享的地址链接
   */
  private String getTeamShareUrl(HttpServletRequest req) {
    String key = tinyUrlService
        .insert(req.getScheme() + "://" + req.getServerName() + "/twitter/teamPromotion/"
            + getCurrentIUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 我的拼团查询
   */
  @RequestMapping("/user/myGroupon")
  @ApiIgnore
  public ResponseObject<List<PromotionGrouponUserVO>> myGroupon(
      @RequestParam(value = "status", required = false) String status, HttpServletRequest req,
      Pageable pager) {
    String userId = getCurrentIUser().getId();
    List<PromotionGrouponUserVO> orders = null;
    orders = promotionGrouponService.listUserPromotion(userId, status, pager);
    // 我的拼团点击明细跳转地址
    for (PromotionGrouponUserVO userVO : orders) {
      String targetUrl =
          siteHost + "/p/promotion/" + userVO.getProductId() + "?promotionId=" + userVO
              .getPromotionId() + "&activityGrouponId=" + userVO
              .getActivityGrouponId() + "&from=view";
      String shareUrl =
          siteHost + "/p/promotion/share/" + userVO.getProductId() + "?promotionId="
              + userVO.getPromotionId() + "&activityId=" + userVO
              .getActivityGrouponId();
      userVO.setTargetUrl(targetUrl);
      userVO.setShareUrl(shareUrl);
    }
    return new ResponseObject<>(orders);
  }

  /**
   * 我的消息
   */
  @ResponseBody
  @RequestMapping(value = "/user/myMessage", method = RequestMethod.POST)
  @ApiOperation(value = "我的消息", notes = "我的消息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UserMessageVO>> myMessage(HttpServletRequest req, Pageable pager) {
    List<UserMessageVO> messageVOs = messageService.loadAllMessages();
    return new ResponseObject<>(messageVOs);
  }

  /**
   * 设置所有消息为已读状态
   */
  @ResponseBody
  @RequestMapping(value = "/user/readAllMessage", method = RequestMethod.POST)
  @ApiOperation(value = "设置所有消息为已读状态", notes = "设置所有消息为已读状态", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> readAllMessage() {
    String userId = getCurrentIUser().getId();
    messageService.updateUserAllRead(userId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(value = "/user/list", method = RequestMethod.POST)
  public ResponseObject<Map<String, Object>> listUsers(Pageable pageable, boolean needPage) {
    String rootShopId = getCurrentIUser().getShopId();
    List<User> users = userService
        .listUsersByRootShopId(rootShopId, null, null, needPage ? pageable : null);
    Long total = userService.countUsersByRootShopId(rootShopId, null, null);
    Map<String, Object> result = new HashMap<>();
    List<UserInfoVO> userInfoVOS = Transformer.fromIterable(users, UserInfoVO.class);
    result.put("list", userInfoVOS);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping(value = "/user/iEmployee/register")
  public ResponseObject<Boolean> isInsideEmployee(@RequestParam("id") Long id,
      @RequestParam("name") String name) {
    boolean isInside = userService.isInsideEmployee(id, name);
    if (!isInside) {
      throw new BizException(GlobalErrorCode.NOT_INSIDE_EMPLOYEE_ERROR, "您不是内部员工, 无法注册");
    }
    String userId = getCurrentIUser().getId();
    boolean isRegistered = userService.isRegistedEmployee(userId);
    if (isRegistered) {
      throw new BizException(GlobalErrorCode.INSIDE_EMPLOYEE_REGISTERED, "您已经注册为内部员工, 请直接登陆");
    }
    boolean hasEmployeeInUse = userService.hasEmployeeId(id);
    if (hasEmployeeInUse) {
      throw new BizException(GlobalErrorCode.INSIDE_EMPLOYEE_REGISTERED, "该内部员工号已被使用");
    }
    boolean result = userService.saveUserEmployee(new InsideUserEmployee(userId, id));
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/user/iEmployee/remove")
  public ResponseObject<Boolean> removeEmployee(@RequestParam("employeeId") Long employeeId) {
    boolean hasEmployeeInUse = userService.hasEmployeeId(employeeId);
    if (!hasEmployeeInUse) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该员工未注册");
    }
    boolean result = userService.removeEmployee(employeeId);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/user/iEmployee/update")
  public ResponseObject<Boolean> updateEmployeeId(@RequestParam String userId,
      @RequestParam Long id) {
    boolean result = userService.updateEmployeeId(userId, id);
    return new ResponseObject<>(result);
  }

    @RequestMapping(value = "/user/isWhite/check")
    public ResponseObject<WhiteListInfo> getWhiteInfo(@RequestParam(value = "cpId",required = false) Long cpId) {

        ResponseObject<WhiteListInfo> ro = new ResponseObject<WhiteListInfo>();
        if(cpId == null){
          User user = (User)this.getCurrentIUser();
          cpId = user.getCpId();
        }
        WhiteListInfo result = this.userService.checkIsWhite(cpId);
        ro.setData(result);
        return ro;
    }

    /**
     * 更新登录后用户的相关数据
     */
    private void updateSigninedData(HttpServletRequest req) {

        final String CLIENT_ID = "vd_cid";
        //获取当前用户的cid
        String cid = null;
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(CLIENT_ID) && cookie.getValue() != null) {   //未输入的匿名用户
                    cid = cookie.getValue();
                    break;
                }
            }
        }
        User anonymousUser = null;
        //还原确认用户身份前的用户信息
        if (cid != null) {
            anonymousUser = userService.loadByLoginname(cid);
        }
        //更新当前用户数据
        User currentUser = (User) userService.getCurrentUser();
        //用户登录状态
        if (currentUser != null) {
            currentUser.setUserStatus(UserStatus.LOGINED);
        }

        if (currentUser != null && cid != null) {
            currentUser.updateCid(cid);
            userService.updateUserInfo(currentUser);
        }

        if (anonymousUser != null) {
            cartService.updateCartUserId(anonymousUser.getId());
        }

        if (currentUser != null) {
            Authentication auth = new UsernamePasswordAuthenticationToken(currentUser, null,
                    currentUser.getAuthorities());
            SecurityContextHolder.clearContext();
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
    }
}
