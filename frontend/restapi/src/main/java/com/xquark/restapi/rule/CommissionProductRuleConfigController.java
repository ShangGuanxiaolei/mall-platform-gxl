package com.xquark.restapi.rule;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.CommissionProductRuleConfig;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.CommissionProductRuleConfigVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.commission.CommissionProductRuleConfigService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 商品佣金设置controller Created by chh on 16-12-21.
 */
@Controller
@ApiIgnore
public class CommissionProductRuleConfigController extends BaseController {

  @Autowired
  private CommissionProductRuleConfigService commissionProductRuleConfigService;

  /**
   * 获取商品佣金设置列表
   */
  @ResponseBody
  @RequestMapping("/ruleProduct/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<CommissionProductRuleConfigVO> roles = null;
    roles = commissionProductRuleConfigService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", commissionProductRuleConfigService.selectCnt(keyword));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存商品佣金设置
   */
  @ResponseBody
  @RequestMapping("/ruleProduct/save")
  public ResponseObject<Boolean> savePromotion(CommissionProductRuleConfig role) {
    int result = 0;
    if (StringUtils.isNotEmpty(role.getId())) {
      result = commissionProductRuleConfigService.modify(role);
    } else {
      role.setArchive(false);
      result = commissionProductRuleConfigService.insert(role);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/ruleProduct/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = commissionProductRuleConfigService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的商品佣金设置记录<br>
   */
  @ResponseBody
  @RequestMapping("/ruleProduct/{id}")
  public ResponseObject<CommissionProductRuleConfigVO> view(@PathVariable String id,
      HttpServletRequest req) {
    CommissionProductRuleConfigVO role = commissionProductRuleConfigService.selectByPrimaryKey(id);
    String rule = role.getCommissionRuleId();
    role.setCommissionRuleId("" + IdTypeHandler.decode(rule));
    return new ResponseObject<>(role);
  }


}
