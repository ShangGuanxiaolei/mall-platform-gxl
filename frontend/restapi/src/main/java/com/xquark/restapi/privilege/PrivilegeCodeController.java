package com.xquark.restapi.privilege;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.vo.PrivilegeCodeVO;
import com.xquark.dal.vo.PrivilegeProductVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.privilege.PrivilegeCodeService;
import com.xquark.service.privilege.PrivilegeProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 特权码相关controller Created by chh on 18-01-16.
 */
@Controller
@ApiIgnore
public class PrivilegeCodeController extends BaseController {

  @Autowired
  private PrivilegeCodeService privilegeCodeService;

  @Autowired
  private PrivilegeProductService privilegeProductService;


  /**
   * 前端页面查找用户所有的特权码
   */
  @ResponseBody
  @RequestMapping("/privilegeCode/my")
  public ResponseObject<List<PrivilegeCodeVO>> my() {
    String userId = getCurrentIUser().getId();
    List<PrivilegeCodeVO> promotions = privilegeCodeService.selectByUserId(userId);
    return new ResponseObject<>(promotions);
  }

  /**
   * 前端页面用户兑换特权码
   */
  @ResponseBody
  @RequestMapping("/privilegeCode/use")
  public Json use(@RequestParam String code) {
    String userId = getCurrentIUser().getId();
    Json json = new Json();
    PrivilegeCode privilegeCode = privilegeCodeService.selectByCode(code);
    if (privilegeCode == null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("该特权码不存在，请确认。");
      return json;
    } else {
      if (privilegeCode.getValidTo().compareTo(new Date()) < 0) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("该特权码已过期，不能参与兑换。");
        return json;
      }
    }

    boolean codeExist = privilegeCodeService.codeExist(code);
    if (codeExist) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("该特权码已领用，请不要重复领取。");
      return json;
    }

    boolean acquire = privilegeCodeService.acquireCode(code, userId);
    if (acquire) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("兑换成功。");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("兑换失败，请联系系统管理员。");
    }
    return json;
  }

  /**
   * 判断该用户是否有还没过期，且有购买数量的特权码
   */
  @ResponseBody
  @RequestMapping("/privilegeCode/check")
  public ResponseObject<Boolean> check() {
    String userId = getCurrentIUser().getId();
    boolean flag = false;
    List<PrivilegeCodeVO> promotions = privilegeCodeService.selectByUserId(userId);
    for (PrivilegeCodeVO vo : promotions) {
      if (vo.getValidTo().compareTo(new Date()) > 0 && vo.getRemainQty() > 0) {
        flag = true;
        break;
      }
    }
    return new ResponseObject<>(flag);
  }

  /**
   * 获取特权码商品
   */
  @ResponseBody
  @RequestMapping(value = "/privilegeCode/getProduct")
  public ResponseObject<HashMap> getProduct(Pageable pageable) {
    HashMap map = new HashMap();
    List<PrivilegeProductVO> result = privilegeProductService.getHomeForApp(pageable, "");
    long count = privilegeProductService.getCount();
    map.put("count", count);
    map.put("list", result);
    return new ResponseObject<>(map);
  }


}
