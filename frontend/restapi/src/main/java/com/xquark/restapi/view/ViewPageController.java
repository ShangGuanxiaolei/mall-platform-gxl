package com.xquark.restapi.view;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.ViewPage;
import com.xquark.dal.model.ViewPageComponent;
import com.xquark.dal.vo.ViewPageComponentVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.view.ViewPageComponentService;
import com.xquark.service.view.ViewPageService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 自定义页面controller Created by chh on 16-10-18.
 */
@Controller
@ApiIgnore
public class ViewPageController extends BaseController {

  @Autowired
  private ViewPageService viewPageService;

  @Autowired
  private ViewPageComponentService viewPageComponentService;

  /**
   * 获取自定义页面列表
   */
  @ResponseBody
  @RequestMapping("/viewPage/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<ViewPage> viewPages = null;
    viewPages = viewPageService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", viewPageService.selectCnt(keyword));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存自定义页面
   */
  @ResponseBody
  @RequestMapping("/viewPage/save")
  public ResponseObject<Boolean> save(ViewPage viewPage) {
    int result = 0;
    if (StringUtils.isNotEmpty(viewPage.getId())) {
      result = viewPageService.modifyViewPage(viewPage);
    } else {
      viewPage.setArchive(false);
      result = viewPageService.insert(viewPage);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/viewPage/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = viewPageService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取自定义页面关联页面组件列表
   */
  @ResponseBody
  @RequestMapping("/viewPage/component/list/{id}")
  public ResponseObject<Map<String, Object>> componentList(Pageable pageable,
      @PathVariable String id) {
    String shopId = this.getCurrentIUser().getShopId();
    List<ViewPageComponentVO> products = null;
    products = viewPageService.listComponent(pageable, id);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", viewPageService.selectComponentCnt(id));
    aRetMap.put("list", products);
    return new ResponseObject<>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/viewPage/deletePageComponent/{id}")
  public ResponseObject<Boolean> deletePageComponent(@PathVariable String id) {
    int result = viewPageComponentService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/viewPage/saveComponent")
  public ResponseObject<Boolean> saveComponent(ViewPageComponent viewPageComponent) {
    int result = 0;
    if (StringUtils.isNotEmpty(viewPageComponent.getId())) {
      result = viewPageComponentService.modifyViewPageComponent(viewPageComponent);
    } else {
      viewPageComponent.setArchive(false);
      result = viewPageComponentService.insert(viewPageComponent);
    }
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的页面关联组件记录
   */
  @ResponseBody
  @RequestMapping("/viewPageComponent/{id}")
  public ResponseObject<ViewPageComponent> saveComponent(@PathVariable String id) {
    ViewPageComponent viewComponent = viewPageComponentService.selectByPrimaryKey(id);
    return new ResponseObject<>(viewComponent);
  }


}
