package com.xquark.restapi.logisticsPlatform;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.LogisticsOrderSendMapper;
import com.xquark.dal.model.Supplier;
import com.xquark.restapi.order.OrderSendController;
import com.xquark.service.yundou.OrderSendResult;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * author: liuwei Date: 18-9-19. Time: 上午11:00
 */
@RestController
public class LogisticsOrderSendController {

  @Autowired
  private LogisticsOrderSendMapper orderSendMapper;

  private final static Logger LOGGER = LoggerFactory.getLogger(OrderSendController.class);

  /**
   * 增加
   *
   * @param request
   * @return
   */
  @RequestMapping(value = "/logistics/ordersend",method = RequestMethod.POST,
      consumes = "application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  public String add(HttpServletRequest request) {
    LOGGER.info("========== 开始处理订单发货请求 ===========");
    OrderSendResult osr = new OrderSendResult();
    String bizcontent = request.getParameter("bizcontent");
    Supplier supplier = (Supplier) request.getAttribute("supplier");
    JSONObject json = JSONObject.parseObject(bizcontent);
    String orderNo = json.getString("PlatOrderNo");
    String logisticName = json.getString("LogisticName");
    String logisticNo = json.getString("LogisticNo");
    //以下数据暂时用不到
    Integer.parseInt(json.getString("IsSplit"));
    json.getString("LogisticType");
    json.getString("SenderName");
    json.getString("SenderTel");
    json.getString("SenderAddress");
    try {
      //解析数据以备多包裹数据的传输，发货信息以备后期使用//TODO
      if(json.getString("SendType") != null){
        json.getString("SendType");
      }
      if(json.getString("SenderName") != null){
        json.getString("SenderName");
      }
      if(json.getString("SenderTel") != null){
        json.getString("SenderTel");
      }
      if(json.getString("SenderAddress") != null){
        json.getString("SenderAddress");
      }
      if(json.getString("IsHwgFlag") != null){
        Integer.parseInt(json.getString("IsHwgFlag"));
      }
      if(json.getString("SubPlatOrderNo").contains("|")){
        json.getString("SubPlatOrderNo");
      }
      LOGGER.info("========== 调用service层 ===========");
      Integer rows = orderSendMapper.updateLogisticsInfo(orderNo,logisticName,
          logisticNo,supplier.getCode());
      if(rows > 0){
        osr.setCode("10000");
        osr.setMessage("SUCCESS");
        osr.setSubcode(null);
        osr.setSubmessage(null);
      }else {
        throw new NullPointerException();
      }
    }catch (NullPointerException e){
      osr.setCode("40400");
      osr.setMessage("Order was nonexistent ");
      osr.setSubcode("NullPointerException");
      osr.setSubmessage("订单不存在");
      LOGGER.error("订单不存在",e);
    }catch (RuntimeException e) {
      osr.setCode("40000");
      osr.setMessage("System Error");
      osr.setSubcode("GSE.SYSTEM_ERROR");
      osr.setSubmessage("[20887]服务异常请稍后再试");
      LOGGER.error("未知错误",e);
    }
    return JSONObject.toJSONString(osr);
  }

}
