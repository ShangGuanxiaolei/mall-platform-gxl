package com.xquark.restapi.dnaof;

import java.io.Serializable;

/**
 * 商品主体信息
 * @author M
 *
 */
public class Question implements Serializable {
	
	private static final long serialVersionUID = 3801099071750542384L;
	
	private String sampleCode;  //样本编号
//第一项：基本个人信息
	private String NAME;  //姓名

	private String DOB;  //出生日期  1
	
	private String SEX;  //性别  2
	
	private String ETHM;  //种族  4
	
	private String ETHD;  //民族  5
	
//第二项：基本身体状况
	private String HEIGHT;  //身高  7
	
	private String BODYWEIGHT;  //体重  8
	
	private String WAISTE;  //腰围  9
	
	private String BODYTYPE;  //身材   10

	private String OBESITY_AGE;  //您在16岁之前肥胖吗  11
	
	private String DONE_BODYFAT;  //您知道自己目前的体脂百分比吗  12
	
	private String TEST_BODYFAT;  //体脂百分数  13
	
	private String TEST_BODYFAT_METHOD;  //请选择用于计算您的人体脂肪百分比的方法。 14
//第三项：运动和目标	
	private String ACTIVITY;  //您每周都会运动健身吗？  15

	private String GOAL;  //您的主要健身目标是什么？  16
	
	private String ACT_OC;  //您的职业工作类型是什么？  17
	
	private String ACT_OCH;  //每天的平均工作时间和每周工作的的天数？每天几小时  18
	
	private String ACT_OCD;  //每天的平均工作时间和每周工作的的天数？每周几天  19
	
	private String ACT_TR;  //您通勤的主要交通工具是什么？  20
	
	private String ACT_TRM;  //您通勤的主要交通工具是什么？每天平均几个小时  21
	
	private String ACT_TRD;  //您通勤的主要交通工具是什么？每周平均几天   22 
	
	private String ACT_HY;  //您是否做家务？  23
	
	private String ACT_HYM;  //每天平均几个小时 24 
	
	private String ACT_HYD;  //每周平均几天  25
	
//第四项：生活方式（1）	
	private String DIET;  //您之前有过节食吗？  26
	
	private String DIETCURRENT;  //您是否遵守任何饮食限制？  29
//第五项：生活方式（2）	
	private String ALLERGY_HAS;  //您是否对下列食物有任何过敏或不耐受症？  30
	
	private String ALLERGY_EGG;  //蛋类  31
	
	private String ALLERGY_PEANUT;  //花生  32
	
	private String ALLERGY_FISH;  //鱼类  33
	
	private String ALLERGY_WHEAT;  //小麦  34
	
	private String ALLERGY_MILK;  //牛奶  35
	
	private String ALLERGY_TREENUT;  //坚果  36
	
	private String ALLERGY_SHELLFISH;  //贝类  37
	
	private String ALLERGY_SOY;  //大豆  38
	
	private String ALLERGY_LACTOSE;  //乳糖  39
	
	private String ALLERGY_YEAST;  //酵母  40  
	
	private String ALLERGY_GLUTEN;  //面筋  41
	
	private String CAFFEINE_USE;  //您是否食（饮）用含咖啡因的产品？  42
	
	private String ALCOHOL_USE;  //您是否饮酒？  43
	
	private String TOBACCO_USE;  //您是否吸烟？  44
	
	private String TOBACCO_QUIT;  //您戒烟大约多久了？  45
	
	private String NICOTINE_USE;  //您目前使用尼古丁产品吗？  56
	
	private String NICOTINE_USE_P;  //尼古丁贴片  57
	
	private String NICOTINE_USE_I;  //尼古丁吸入器  58
	
	private String NICOTINE_USE_G;  //尼古丁口香糖  59
	
	private String NICOTINE_USE_V;  //电子烟  60
	
	private String NICOTINE_USE_O;  //其它  61
	
//第六项：健康	
	private String SLEEP;  //您每晚睡8小时以上吗？63
	
	private String SLEEPWAKE;  //您通常在几点钟之间起床？  64
	
	private String SLEEPBED;  //您通常在几点钟之间睡觉？  65
	
	private String MORNINGPERSON;  //您的生物钟类型？  66
	
	private String STEROIDS_USE;  //您曾经服用过类固醇吗？  67
	
	private String DONE_TESTOSTERONE;  //您知道您的血睾酮水平？  68
	
	private String TEST_TESTOSTERONE;  //您知道您的血睾酮水平？纳克/升  69
	
	private String DONE_BLOOD_GLUCOSE;  //您知道您的空腹血糖水平吗？	70
	
	private String TEST_BLOOD_GLUCOSE;  //您知道您的空腹血糖水平吗？毫摩尔/升	71
	
	private String DONE_BLOOD_TRI;  //您知道您的空腹血液甘油三酯水平？  72
	
	private String TEST_BLOOD_TRI;  //您知道您的空腹血液甘油三酯水平？ 毫克/分升  73
	
	private String HYPERTENSION;  //高血压  75
	
	private String DIABETES;  //糖尿病  76
	
	private String CHOLESTEROL;  //高胆固醇  77
	
	private String COELIAC_DISEASE;  //乳糜泻 （消化道功能障碍） 78

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getDOB() {
		return DOB;
	}

	public void setDOB(String dOB) {
		DOB = dOB;
	}

	public String getSEX() {
		return SEX;
	}

	public void setSEX(String sEX) {
		SEX = sEX;
	}

	public String getETHM() {
		return ETHM;
	}

	public void setETHM(String eTHM) {
		ETHM = eTHM;
	}

	public String getETHD() {
		return ETHD;
	}

	public void setETHD(String eTHD) {
		ETHD = eTHD;
	}

	public String getHEIGHT() {
		return HEIGHT;
	}

	public void setHEIGHT(String hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public String getBODYWEIGHT() {
		return BODYWEIGHT;
	}

	public void setBODYWEIGHT(String bODYWEIGHT) {
		BODYWEIGHT = bODYWEIGHT;
	}

	public String getWAISTE() {
		return WAISTE;
	}

	public void setWAISTE(String wAISTE) {
		WAISTE = wAISTE;
	}

	public String getBODYTYPE() {
		return BODYTYPE;
	}

	public void setBODYTYPE(String bODYTYPE) {
		BODYTYPE = bODYTYPE;
	}

	public String getOBESITY_AGE() {
		return OBESITY_AGE;
	}

	public void setOBESITY_AGE(String oBESITY_AGE) {
		OBESITY_AGE = oBESITY_AGE;
	}

	public String getDONE_BODYFAT() {
		return DONE_BODYFAT;
	}

	public void setDONE_BODYFAT(String dONE_BODYFAT) {
		DONE_BODYFAT = dONE_BODYFAT;
	}

	public String getTEST_BODYFAT() {
		return TEST_BODYFAT;
	}

	public void setTEST_BODYFAT(String tEST_BODYFAT) {
		TEST_BODYFAT = tEST_BODYFAT;
	}

	public String getTEST_BODYFAT_METHOD() {
		return TEST_BODYFAT_METHOD;
	}

	public void setTEST_BODYFAT_METHOD(String tEST_BODYFAT_METHOD) {
		TEST_BODYFAT_METHOD = tEST_BODYFAT_METHOD;
	}

	public String getACTIVITY() {
		return ACTIVITY;
	}

	public void setACTIVITY(String aCTIVITY) {
		ACTIVITY = aCTIVITY;
	}

	public String getGOAL() {
		return GOAL;
	}

	public void setGOAL(String gOAL) {
		GOAL = gOAL;
	}

	public String getACT_OC() {
		return ACT_OC;
	}

	public void setACT_OC(String aCT_OC) {
		ACT_OC = aCT_OC;
	}

	public String getACT_OCH() {
		return ACT_OCH;
	}

	public void setACT_OCH(String aCT_OCH) {
		ACT_OCH = aCT_OCH;
	}

	public String getACT_OCD() {
		return ACT_OCD;
	}

	public void setACT_OCD(String aCT_OCD) {
		ACT_OCD = aCT_OCD;
	}

	public String getACT_TR() {
		return ACT_TR;
	}

	public void setACT_TR(String aCT_TR) {
		ACT_TR = aCT_TR;
	}

	public String getACT_TRD() {
		return ACT_TRD;
	}

	public void setACT_TRD(String aCT_TRD) {
		ACT_TRD = aCT_TRD;
	}

	public String getACT_TRM() {
		return ACT_TRM;
	}

	public void setACT_TRM(String aCT_TRM) {
		ACT_TRM = aCT_TRM;
	}

	public String getACT_HY() {
		return ACT_HY;
	}

	public void setACT_HY(String aCT_HY) {
		ACT_HY = aCT_HY;
	}

	public String getACT_HYD() {
		return ACT_HYD;
	}

	public void setACT_HYD(String aCT_HYD) {
		ACT_HYD = aCT_HYD;
	}

	public String getACT_HYM() {
		return ACT_HYM;
	}

	public void setACT_HYM(String aCT_HYM) {
		ACT_HYM = aCT_HYM;
	}

	public String getDIET() {
		return DIET;
	}

	public void setDIET(String dIET) {
		DIET = dIET;
	}

	public String getDIETCURRENT() {
		return DIETCURRENT;
	}

	public void setDIETCURRENT(String dIETCURRENT) {
		DIETCURRENT = dIETCURRENT;
	}

	public String getALLERGY_HAS() {
		return ALLERGY_HAS;
	}

	public void setALLERGY_HAS(String aLLERGY_HAS) {
		ALLERGY_HAS = aLLERGY_HAS;
	}

	public String getALLERGY_EGG() {
		return ALLERGY_EGG;
	}

	public void setALLERGY_EGG(String aLLERGY_EGG) {
		ALLERGY_EGG = aLLERGY_EGG;
	}

	public String getALLERGY_PEANUT() {
		return ALLERGY_PEANUT;
	}

	public void setALLERGY_PEANUT(String aLLERGY_PEANUT) {
		ALLERGY_PEANUT = aLLERGY_PEANUT;
	}

	public String getALLERGY_FISH() {
		return ALLERGY_FISH;
	}

	public void setALLERGY_FISH(String aLLERGY_FISH) {
		ALLERGY_FISH = aLLERGY_FISH;
	}

	public String getALLERGY_WHEAT() {
		return ALLERGY_WHEAT;
	}

	public void setALLERGY_WHEAT(String aLLERGY_WHEAT) {
		ALLERGY_WHEAT = aLLERGY_WHEAT;
	}

	public String getALLERGY_MILK() {
		return ALLERGY_MILK;
	}

	public void setALLERGY_MILK(String aLLERGY_MILK) {
		ALLERGY_MILK = aLLERGY_MILK;
	}

	public String getALLERGY_TREENUT() {
		return ALLERGY_TREENUT;
	}

	public void setALLERGY_TREENUT(String aLLERGY_TREENUT) {
		ALLERGY_TREENUT = aLLERGY_TREENUT;
	}

	public String getALLERGY_SHELLFISH() {
		return ALLERGY_SHELLFISH;
	}

	public void setALLERGY_SHELLFISH(String aLLERGY_SHELLFISH) {
		ALLERGY_SHELLFISH = aLLERGY_SHELLFISH;
	}

	public String getALLERGY_SOY() {
		return ALLERGY_SOY;
	}

	public void setALLERGY_SOY(String aLLERGY_SOY) {
		ALLERGY_SOY = aLLERGY_SOY;
	}

	public String getALLERGY_LACTOSE() {
		return ALLERGY_LACTOSE;
	}

	public void setALLERGY_LACTOSE(String aLLERGY_LACTOSE) {
		ALLERGY_LACTOSE = aLLERGY_LACTOSE;
	}

	public String getALLERGY_YEAST() {
		return ALLERGY_YEAST;
	}

	public void setALLERGY_YEAST(String aLLERGY_YEAST) {
		ALLERGY_YEAST = aLLERGY_YEAST;
	}

	public String getALLERGY_GLUTEN() {
		return ALLERGY_GLUTEN;
	}

	public void setALLERGY_GLUTEN(String aLLERGY_GLUTEN) {
		ALLERGY_GLUTEN = aLLERGY_GLUTEN;
	}

	public String getCAFFEINE_USE() {
		return CAFFEINE_USE;
	}

	public void setCAFFEINE_USE(String cAFFEINE_USE) {
		CAFFEINE_USE = cAFFEINE_USE;
	}

	public String getALCOHOL_USE() {
		return ALCOHOL_USE;
	}

	public void setALCOHOL_USE(String aLCOHOL_USE) {
		ALCOHOL_USE = aLCOHOL_USE;
	}

	public String getTOBACCO_USE() {
		return TOBACCO_USE;
	}

	public void setTOBACCO_USE(String tOBACCO_USE) {
		TOBACCO_USE = tOBACCO_USE;
	}

	public String getTOBACCO_QUIT() {
		return TOBACCO_QUIT;
	}

	public void setTOBACCO_QUIT(String tOBACCO_QUIT) {
		TOBACCO_QUIT = tOBACCO_QUIT;
	}

	public String getNICOTINE_USE() {
		return NICOTINE_USE;
	}

	public void setNICOTINE_USE(String nICOTINE_USE) {
		NICOTINE_USE = nICOTINE_USE;
	}

	public String getNICOTINE_USE_P() {
		return NICOTINE_USE_P;
	}

	public void setNICOTINE_USE_P(String nICOTINE_USE_P) {
		NICOTINE_USE_P = nICOTINE_USE_P;
	}

	public String getNICOTINE_USE_I() {
		return NICOTINE_USE_I;
	}

	public void setNICOTINE_USE_I(String nICOTINE_USE_I) {
		NICOTINE_USE_I = nICOTINE_USE_I;
	}

	public String getNICOTINE_USE_G() {
		return NICOTINE_USE_G;
	}

	public void setNICOTINE_USE_G(String nICOTINE_USE_G) {
		NICOTINE_USE_G = nICOTINE_USE_G;
	}

	public String getNICOTINE_USE_V() {
		return NICOTINE_USE_V;
	}

	public void setNICOTINE_USE_V(String nICOTINE_USE_V) {
		NICOTINE_USE_V = nICOTINE_USE_V;
	}

	public String getNICOTINE_USE_O() {
		return NICOTINE_USE_O;
	}

	public void setNICOTINE_USE_O(String nICOTINE_USE_O) {
		NICOTINE_USE_O = nICOTINE_USE_O;
	}

	public String getSLEEP() {
		return SLEEP;
	}

	public void setSLEEP(String sLEEP) {
		SLEEP = sLEEP;
	}

	public String getSLEEPWAKE() {
		return SLEEPWAKE;
	}

	public void setSLEEPWAKE(String sLEEPWAKE) {
		SLEEPWAKE = sLEEPWAKE;
	}

	public String getSLEEPBED() {
		return SLEEPBED;
	}

	public void setSLEEPBED(String sLEEPBED) {
		SLEEPBED = sLEEPBED;
	}

	public String getMORNINGPERSON() {
		return MORNINGPERSON;
	}

	public void setMORNINGPERSON(String mORNINGPERSON) {
		MORNINGPERSON = mORNINGPERSON;
	}

	public String getSTEROIDS_USE() {
		return STEROIDS_USE;
	}

	public void setSTEROIDS_USE(String sTEROIDS_USE) {
		STEROIDS_USE = sTEROIDS_USE;
	}

	public String getDONE_TESTOSTERONE() {
		return DONE_TESTOSTERONE;
	}

	public void setDONE_TESTOSTERONE(String dONE_TESTOSTERONE) {
		DONE_TESTOSTERONE = dONE_TESTOSTERONE;
	}

	public String getTEST_TESTOSTERONE() {
		return TEST_TESTOSTERONE;
	}

	public void setTEST_TESTOSTERONE(String tEST_TESTOSTERONE) {
		TEST_TESTOSTERONE = tEST_TESTOSTERONE;
	}

	public String getDONE_BLOOD_GLUCOSE() {
		return DONE_BLOOD_GLUCOSE;
	}

	public void setDONE_BLOOD_GLUCOSE(String dONE_BLOOD_GLUCOSE) {
		DONE_BLOOD_GLUCOSE = dONE_BLOOD_GLUCOSE;
	}

	public String getTEST_BLOOD_GLUCOSE() {
		return TEST_BLOOD_GLUCOSE;
	}

	public void setTEST_BLOOD_GLUCOSE(String tEST_BLOOD_GLUCOSE) {
		TEST_BLOOD_GLUCOSE = tEST_BLOOD_GLUCOSE;
	}

	public String getDONE_BLOOD_TRI() {
		return DONE_BLOOD_TRI;
	}

	public void setDONE_BLOOD_TRI(String dONE_BLOOD_TRI) {
		DONE_BLOOD_TRI = dONE_BLOOD_TRI;
	}

	public String getTEST_BLOOD_TRI() {
		return TEST_BLOOD_TRI;
	}

	public void setTEST_BLOOD_TRI(String tEST_BLOOD_TRI) {
		TEST_BLOOD_TRI = tEST_BLOOD_TRI;
	}

	public String getHYPERTENSION() {
		return HYPERTENSION;
	}

	public void setHYPERTENSION(String hYPERTENSION) {
		HYPERTENSION = hYPERTENSION;
	}

	public String getDIABETES() {
		return DIABETES;
	}

	public void setDIABETES(String dIABETES) {
		DIABETES = dIABETES;
	}

	public String getCHOLESTEROL() {
		return CHOLESTEROL;
	}

	public void setCHOLESTEROL(String cHOLESTEROL) {
		CHOLESTEROL = cHOLESTEROL;
	}

	public String getCOELIAC_DISEASE() {
		return COELIAC_DISEASE;
	}

	public void setCOELIAC_DISEASE(String cOELIAC_DISEASE) {
		COELIAC_DISEASE = cOELIAC_DISEASE;
	}

}
