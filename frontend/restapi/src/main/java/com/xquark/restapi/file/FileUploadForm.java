package com.xquark.restapi.file;

import com.xquark.dal.type.FileUploadType;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by wangxinhua on 17-12-20. DESC:
 */
public class FileUploadForm {

    @NotNull(message = "文件类型不能为空")
    private FileUploadType type;

    @NotNull(message = "上传文件不能为空")
    private List<MultipartFile> file;

    public FileUploadType getType() {
        return type;
    }

    public void setType(FileUploadType type) {
        this.type = type;
    }

    public List<MultipartFile> getFile() {
        return file;
    }

    public void setFile(List<MultipartFile> file) {
        this.file = file;
    }
}
