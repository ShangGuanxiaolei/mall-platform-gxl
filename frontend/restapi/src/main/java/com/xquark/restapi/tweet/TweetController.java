package com.xquark.restapi.tweet;

import com.xquark.dal.mapper.TweetImageMapper;
import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.TweetImage;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.view.MarketingTweetService;
import com.xquark.service.view.vo.MarketingTweetVO;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

/**
 * 发现页面controller Created by chh on 16-10-19.
 */
//@communityDetailController
public class TweetController extends BaseController {

  @Autowired
  private MarketingTweetService tweetService;

  @Autowired
  private TweetImageMapper tweetImageMapper;

  /**
   * 获取发现页面列表
   */
  @ResponseBody
  @RequestMapping("/tweet/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<MarketingTweet> viewPages = null;
    viewPages = tweetService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", tweetService.selectCnt(keyword));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存发现
   */
  @ResponseBody
  @RequestMapping("/tweet/save")
  public ResponseObject<Boolean> save(TweetForm tweetForm) {
    int result = 0;
    // 处理图片信息
    List<TweetImage> imgs = initImage(tweetForm);
    MarketingTweet marketingTweet = new MarketingTweet();
    BeanUtils.copyProperties(tweetForm, marketingTweet);
    if (StringUtils.isNotEmpty(tweetForm.getId())) {
      result = tweetService.modifyMarketingTweet(marketingTweet, imgs);
    } else {
      tweetForm.setArchive(false);
      result = tweetService.insert(marketingTweet, imgs);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/tweet/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = tweetService.delete(id);
    return new ResponseObject<>(result > 0);
  }


  /**
   * 查看某个具体的发现记录<br>
   */
  @ResponseBody
  @RequestMapping("/tweet/{id}")
  public ResponseObject<MarketingTweetVO> view(@PathVariable String id, HttpServletRequest req) {
    MarketingTweetVO marketingTweetVO = tweetService.load(id);
    if (marketingTweetVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    return new ResponseObject<>(marketingTweetVO);
  }

  /**
   * 保存发现记录时的图片,把imageForm转化成model
   */
  private List<TweetImage> initImage(TweetForm form) {
    List<TweetImage> imgs = new ArrayList<>();
    TweetImage pimg = null;
    int idx = 0;   // 从0开始
    if (null != form.getImgs() && !"".equals(form.getImgs())) {
      String[] fimgs = form.getImgs().split(",");
      for (String img : fimgs) {
        pimg = new TweetImage();
        pimg.setImgOrder(idx++);
        pimg.setImg(img);
        imgs.add(pimg);
      }
    }
    return imgs;
  }

  /**
   * app获取发现页面列表
   */
  @ResponseBody
  @RequestMapping("/tweet/listApp")
  public ResponseObject<Map<String, Object>> listApp(Pageable pageable) {
    String shopId = this.getCurrentIUser().getShopId();
    List<MarketingTweetAppVO> vos = new ArrayList<>();
    List<MarketingTweet> viewPages = null;
    viewPages = tweetService.listApp(pageable);
    for (MarketingTweet marketingTweet : viewPages) {
      MarketingTweetAppVO appVO = new MarketingTweetAppVO(marketingTweet);
      // 图片
      final List<TweetImage> imgs = tweetImageMapper.selectByTweetId(appVO.getId());
      for (final TweetImage i : imgs) {
        i.setImgUrl(i.getImg());
      }
      appVO.setImgs(imgs);

      // 计算标题发布时间距离现在多长时间
      Date createdAt = appVO.getCreatedAt();
      String dayDesc = dateDiff(createdAt, new Date());
      appVO.setDayDesc(dayDesc);
      vos.add(appVO);
    }
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("list", vos);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 计算两个时间相差多少天多少小时
   */
  public String dateDiff(Date startTime, Date endTime) {
    String dayDesc = "";
    //按照传入的格式生成一个simpledateformate对象
    long nd = 1000 * 24 * 60 * 60;//一天的毫秒数
    long nh = 1000 * 60 * 60;//一小时的毫秒数
    long nm = 1000 * 60;//一分钟的毫秒数
    long ns = 1000;//一秒钟的毫秒数long diff;try {
    //获得两个时间的毫秒时间差异
    long diff = endTime.getTime() - startTime.getTime();
    long day = diff / nd;//计算差多少天
    long hour = diff % nd / nh;//计算差多少小时
    long min = diff % nd % nh / nm;//计算差多少分钟
    if (day > 0) {
      dayDesc = day + "天" + hour + "小时" + min + "分钟前";
    } else {
      if (hour > 0) {
        dayDesc = hour + "小时" + min + "分钟前";
      } else {
        dayDesc = min + "分钟前";
      }
    }
    return dayDesc;
  }

}
