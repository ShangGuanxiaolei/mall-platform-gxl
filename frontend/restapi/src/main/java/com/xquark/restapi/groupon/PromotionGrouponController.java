package com.xquark.restapi.groupon;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionGrouponProductVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.order.OrderService;
import com.xquark.service.pricing.PromotionService;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 团购活动controller Created by chh on 16-10-12.
 */
@Controller
@ApiIgnore
public class PromotionGrouponController extends BaseController {

  @Autowired
  private PromotionGrouponService promotionGrouponService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @ResponseBody
  @RequestMapping("/promotionGroupon/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = promotionGrouponService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/promotionGroupon/close/{id}")
  public ResponseObject<Boolean> close(@PathVariable String id) {
    int result = promotionGrouponService.close(id);
    List<String> orderIds = activityGrouponService.needRefundOrdersByPromotionGrouponId(id);
    // 结束拼团活动后，需要自动将该活动对应的未成团订单退款
    for (String orderId : orderIds) {
      orderService.refundSystem(IdTypeHandler.encode(new Long(orderId)));
    }

    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取团购活动关联商品列表
   */
  @ResponseBody
  @RequestMapping("/promotionGroupon/list")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable,
      @RequestParam String keywords) {
    String shopId = this.getCurrentIUser().getShopId();
    List<Promotion> diamonds = null;
    diamonds = promotionGrouponService.listPromotion(pageable, "");
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", promotionGrouponService.selectPromotionCnt(""));
    aRetMap.put("list", diamonds);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 获取团购活动活动关联商品列表
   */
  @ResponseBody
  @RequestMapping("/promotionGroupon/product/list/{id}")
  public ResponseObject<Map<String, Object>> listById(Pageable pageable, @PathVariable String id) {
    String shopId = this.getCurrentIUser().getShopId();
    List<PromotionGrouponProductVO> products = null;
    products = promotionGrouponService.listPromotionProduct(pageable, id);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", promotionGrouponService.selectPromotionProductCnt(id));
    aRetMap.put("list", products);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存团购活动
   */
  @ResponseBody
  @RequestMapping("/promotionGroupon/savePromotion")
  public ResponseObject<Boolean> savePromotion(Promotion promotion) {
    int result = 0;
    if (StringUtils.isNotEmpty(promotion.getId())) {
      result = promotionService.update(promotion);
    } else {
      promotion.setArchive(false);
      promotion.setType(PromotionType.GROUPON);
      result = promotionService.insert(promotion);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存团购活动商品信息
   */
  @ResponseBody
  @RequestMapping("/promotionGroupon/save")
  public ResponseObject<Boolean> save(PromotionGrouponVO promotionGroupon) {
    try {
      if (StringUtils.isNotEmpty(promotionGroupon.getId())) {
        promotionGrouponService.modifyPromotionGroupon(promotionGroupon);
      } else {
        promotionGroupon.setSales(new BigDecimal("0"));
        promotionGroupon.setArchive(false);
        promotionGrouponService.insert(promotionGroupon);
      }
      return new ResponseObject<>(true);
    } catch (Exception e) {
      log.error("save product groupon error.", e);
      return new ResponseObject(e);
    }
  }


}
