package com.xquark.restapi.supplier.form;

import com.xquark.dal.model.Supplier;
import com.xquark.dal.model.SupplierWareHouse;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午8:11
 */
public class SupplierForm extends Supplier {

  /**
   * 供应商下面管理的仓库
   */
  private List<SupplierWareHouse> wareHouseList;

  public List<SupplierWareHouse> getWareHouseList() {
    return wareHouseList;
  }

  public void setWareHouseList(List<SupplierWareHouse> wareHouseList) {
    this.wareHouseList = wareHouseList;
  }

}
