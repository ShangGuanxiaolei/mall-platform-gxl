package com.xquark.restapi.customer;

import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.OrderVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.cuservice.ConsultOrderService;
import com.xquark.service.cuservice.CusService;
import com.xquark.service.cuservice.vo.Constant;
import com.xquark.service.cuservice.vo.ConsultOrderResult;
import com.xquark.service.cuservice.vo.OfflineButton;
import com.xquark.service.user.UserService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/1/4
 * Time: 13:17
 * Description: 客户服务
 */
@RestController
@RequestMapping("/cus")
public class CusController extends BaseController {
    private final Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private CusService  cusService;
    @Autowired
    private ConsultOrderService consultOrderService;
    @Autowired
    private UserService userService;


//    /**
//     *  物流查询
//     */
//    @RequestMapping(value = "/queryLogistics", method = RequestMethod.POST  ,produces="application/json;charset=UTF-8")
//    public ResponseObject<Map<String, Object>> queryLogistics(String orderNo) {
//        System.out.println("aaaaa");
//        Map<String, Object> stringObjectMap = cusService.queryLogistics(orderNo);
//        return new ResponseObject<>(stringObjectMap);
//    }
//    @RequestMapping(value = "/queryAdress",method = RequestMethod.POST ,produces="application/json;charset=UTF-8")
//    public ResponseObject<Map<String, Object>>  changeAdress(String orderNo){
//        Map<String, Object> stringObjectMap = cusService.queryAdress(orderNo);
//        return new ResponseObject<>(stringObjectMap);
//    }

    @ResponseBody
    @RequestMapping(value = "/order/list/{type}", method = RequestMethod.GET)
    @ApiOperation(value = "客服订单查询", notes = "分页传递page(0代表第几页),size(3代表一页显示多少条数据)，pageable(true)\n"
            +
            "type传递订单状态(MY_ORDER代表我的订单，LOGISTI_CQUERY代表物流查询，CONSULT_APPLY代表申请售后，MODIFY_ADRESS代表修改地址)", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<OrderVO>> listOrder(@PathVariable String type, Pageable pageable) {

        ResponseObject ro = new ResponseObject();
        if (StringUtils.isBlank(type)) {
            ro.setMoreInfo("查询分类type不能为空！");
            return ro;
        }
        Map<String, Object> tagMap = new HashMap<>();
        User user = (User)getCurrentIUser();
        Long cpId = null;
        if(Objects.isNull(user)){
            cpId = user.getCpId();
            tagMap.put("cpId", cpId);
        }
        //以上是参数校验
        List<ConsultOrderResult> orderList = new ArrayList<ConsultOrderResult>();
        log.info("客服查询订单入参" + type + tagMap);
        //我的订单
        if (Constant.MY_ORDER.name().equalsIgnoreCase(type)) {
            orderList = this.consultOrderService.selectMyOrder(pageable);
            //若字段过多，可以新建一个实体类格式化数据再返回
            ro.setData(orderList);
            log.info("在线客服查询我的订单返回" + ro + tagMap);
            return ro;
        }
        //物流查询
        if (Constant.LOGISTI_CQUERY.name().equalsIgnoreCase(type)) {
            orderList = this.consultOrderService.selectLogisticsList(null, pageable,type);
            ro.setData(orderList);
            log.info("在线客服物流查询订单返回" + ro + tagMap);
            return ro;
        }
        //申请售后
        if (Constant.CONSULT_APPLY.name().equalsIgnoreCase(type)) {
            orderList = this.consultOrderService.selectConsultApplyList(null, pageable,type);
            ro.setData(orderList);
            log.info("在线客服查询申请售后订单返回" + ro + tagMap);
            return ro;
        }
        //修改地址
        if (Constant.MODIFY_ADRESS.name().equalsIgnoreCase(type)) {
            //30分钟内
            orderList = this.consultOrderService.selectModifyAdressList(null, pageable,type);
            ro.setData(orderList);
            log.info("在线客服查询修改地址订单返回" + ro + tagMap);
            return ro;
        }

        return ro;
    }

    /**
     * 售后申请中通过订单单号查询订单信息
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryConsultApply/{orderNo}", method = RequestMethod.GET)
    public ResponseObject<ConsultOrderResult> queryConsultApplyByNo(@PathVariable String orderNo){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(orderNo)){
            ro.setMoreInfo("orderNo不能为空！");
            return ro;
        }
        ConsultOrderResult consultOrderResult= this.consultOrderService.selectConsultApplyByNo(orderNo);
        ro.setData(consultOrderResult);
        return ro;
    }


    /**
     * 我的订单中通过订单单号查询订单信息
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryMyOrder/{orderNo}", method = RequestMethod.GET)
    public ResponseObject<ConsultOrderResult> queryMyOrderByNo(@PathVariable String orderNo){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(orderNo)){
            ro.setMoreInfo("orderNo不能为空！");
            return ro;
        }
        ConsultOrderResult consultOrderResult= this.consultOrderService.selectMyOrderByNo(orderNo);
        ro.setData(consultOrderResult);
        return ro;
    }

    /**
     * 修改地址中通过订单单号查询订单信息
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryModifyAdress/{orderNo}", method = RequestMethod.GET)
    public ResponseObject<ConsultOrderResult> queryModifyAdressByNo(@PathVariable String orderNo){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(orderNo)){
            ro.setMoreInfo("orderNo不能为空！");
            return ro;
        }
        ConsultOrderResult consultOrderResult= this.consultOrderService.selectModifyAdressByNo(orderNo);
        if (consultOrderResult == null){
            ro.setMoreInfo("支付成功30分钟内才可修改地址！");
        }
        ro.setData(consultOrderResult);
        return ro;
    }

    /**
     * 查询物流中通过订单单号查询订单信息
     * @param orderNo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryLogistics/{orderNo}", method = RequestMethod.GET)
    public ResponseObject<ConsultOrderResult> queryLogisticsByNo(@PathVariable String orderNo){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(orderNo)){
            ro.setMoreInfo("orderNo不能为空！");
            return ro;
        }
        ConsultOrderResult consultOrderResult= this.consultOrderService.selectLogisticsByNo(orderNo);
        ro.setData(consultOrderResult);
        return ro;
    }

    /**
     * 查询在线客服button列表
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/queryButtonList", method = RequestMethod.GET)
    public ResponseObject<OfflineButton> queryButtonList(){
        ResponseObject ro = new ResponseObject();
        List<OfflineButton> obList = this.consultOrderService.selectOfflineButtonList();
        ro.setData(obList);
        return ro;
    }
}
