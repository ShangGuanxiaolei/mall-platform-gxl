package com.xquark.restapi.product;

import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-2-12. DESC:
 */
public class FilterBindForm {

  @NotBlank
  private String productId;

  @NotNull
  private List<String> filterIds;

  // 判断是绑定操作还是解绑操作
  private boolean unBind;

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public List<String> getFilterIds() {
    return filterIds;
  }

  public void setFilterIds(List<String> filterIds) {
    this.filterIds = filterIds;
  }

  public boolean getIsUnBind() {
    return unBind;
  }

  public void setUnBind(boolean unBind) {
    this.unBind = unBind;
  }

}
