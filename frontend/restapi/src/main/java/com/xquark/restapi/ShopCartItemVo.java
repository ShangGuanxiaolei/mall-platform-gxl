package com.xquark.restapi;

import java.util.List;

import com.xquark.dal.vo.ShopVO;
import com.xquark.service.cart.vo.CartItemVO;

public class ShopCartItemVo {

  public ShopCartItemVo(ShopVO shop, List<CartItemVO> cartItems) {
    this.shop = shop;
    this.cartItems = cartItems;
  }

  private ShopVO shop;

  private List<CartItemVO> cartItems;

  public ShopVO getShop() {
    return shop;
  }

  public void setShop(ShopVO shop) {
    this.shop = shop;
  }

  public List<CartItemVO> getCartItems() {
    return cartItems;
  }

  public void setCartItems(List<CartItemVO> cartItems) {
    this.cartItems = cartItems;
  }


}
