package com.xquark.restapi.waidan.vo;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/5/28
 * Time: 15:41
 * Description: 非erp模板
 */
public class NotErpTemplate {

       private String   orderNo;//订单编号
       private String   receiver;//收件人
       private String   phone;//手机号
       private String   street;//收货地址
       private String   leaveWord;//留言
       private String   name;//商品名字
       private String   skuName;//sku名称
       private String   skuCode;//sku编码
       private String   count;//数量

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
