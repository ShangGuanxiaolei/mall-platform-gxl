package com.xquark.restapi.index;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.model.AccessReport;
import com.xquark.dal.model.Activity;
import com.xquark.dal.model.Poster;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.User;
import com.xquark.dal.type.ProductListSortType;
import com.xquark.dal.vo.ActivityEX;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.category.CategoryVO;
import com.xquark.restapi.category.ProductListSortTypeVO;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.category.TermService;
import com.xquark.service.poster.PosterService;
import com.xquark.service.poster.vo.PosterVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopAccessService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class IndexController extends BaseController {

  @Autowired
  private ShopAccessService shopAccessService;

  @Autowired
  private ProductService productService;

  @Autowired
  private TermService termService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ActivityService activityService;

  @Autowired
  private PosterService posterService;

  @Autowired
  private UrlHelper urlHelper;

  @Value("${shop.access.default.img}")
  String accessDefaultImg;

  @Value("${qiniu.resource.host}")
  String qiniuResourceHost;

  @Value("${site.web.host.name}")
  String siteHost;

  /**
   * 首页客户访问统计
   */
  @ResponseBody
  @RequestMapping("/index/visitors")
  public ResponseObject<AccessReportVO> visitors() {
    String shopId = shopAccessService.getCurrentUser().getShopId();
    Calendar cal = Calendar.getInstance();
    Calendar date = Calendar.getInstance();
    date.clear();
    date.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    date.add(Calendar.DAY_OF_MONTH, -1);
    AccessReport accessReport = shopAccessService.findReportByShopIdAndDate(shopId, date.getTime());
    if (accessReport == null) {
      accessReport = new AccessReport();
      accessReport.setUv(0);
      accessReport.setImg(accessDefaultImg);
    }
    AccessReportVO vo = new AccessReportVO();
    BeanUtils.copyProperties(accessReport, vo);

    if (StringUtils.isNotBlank(accessReport.getImg())) {
      vo.setImgUrl(accessReport.getImg());
    }
    vo.setDate(cal.getTime());
    return new ResponseObject<>(vo);
  }

  /**
   * 帮助，快速入门
   */
  @ResponseBody
  @RequestMapping("/index/start")
  public ResponseObject<Map<String, Object>> start() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 5, 15);
    Map<String, Object> start = new HashMap<>();
    start.put("imgUrl", qiniuResourceHost + "/market_img.png");
    start.put("iconUrl", qiniuResourceHost + "/rocket.png");
    start.put("iconTitle", "邀请");
    start.put("redirectUrl", siteHost + "/about/help.html");
    start.put("title", "给朋友的一封信");
    start.put("date", cal.getTime());
    start.put("description", "");
    return new ResponseObject<>(start);
  }


  /**
   * 最近商品发布手册 默认从昨天开始取三天, 每天的商品数不超过12个
   */
  @ResponseBody
  @RequestMapping("/index/products/recently")
  public ResponseObject<List<Map<String, Object>>> recently() throws ParseException {
    String shopId = shopAccessService.getCurrentUser().getShopId();
    Calendar cal = Calendar.getInstance();
    Calendar date = Calendar.getInstance();
    date.clear();
    date.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    List<Map<String, Object>> recentlys = productService
        .listProductByRecently(shopId, date.getTime(), 3);
    for (Map<String, Object> recently : recentlys) {
      @SuppressWarnings("unchecked")
      List<Product> products = (List<Product>) recently.get("products");
      recently.put("products", generateImgUrl(products));
    }
    return new ResponseObject<>(recentlys);
  }

  @ResponseBody
  @RequestMapping("/index/home-activity2")
  public ResponseObject<HomePageVO> homeActivity2() {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.info("user does not login");
    }

    HomePageVO result = new HomePageVO();
    String tag = "首页海报";
    List<PosterVO> posters = new ArrayList<>();
    List<Poster> posterList = posterService.listByTag(tag);
    for (Poster po : posterList) {
      String url = po.getUrl();
      if (StringUtils.isEmpty(url)) {
        url = "";
      } else {
        if (url.indexOf("http") != 0) {
          url = siteHost + url;
        }
        if (user != null) {
          url += "?union_id=" + user.getId();
        }
      }
      posters.add(new PosterVO(po.getImgCode(), url, po.getImgCode()));
    }
    result.setPosters(posters);

    List<ProductListSortTypeVO> types = new ArrayList<>();
    for (ProductListSortType t : ProductListSortType.values()) {
      ProductListSortTypeVO type = new ProductListSortTypeVO();
      type.setTitle("按" + t.toString() + "排序");
      type.setValue(t.name());
      types.add(type);
    }
    result.setSortTypes(types);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/index/home-activity")
  public ResponseObject<HomePageVO> homeActivity() {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.info("user does not login");
    }

    HomePageVO result = new HomePageVO();

    String tag = "首页海报";
    List<PosterVO> posters = new ArrayList<>();
    List<Poster> posterList = posterService.listByTag(tag);
    for (Poster po : posterList) {
      String url = po.getUrl();
      if (StringUtils.isEmpty(url)) {
        url = "";
      } else {
        if (url.indexOf("http") != 0) {
          url = siteHost + url;
        }
        if (user != null) {
          url += "?union_id=" + user.getId();
        }
      }
      posters.add(new PosterVO(po.getImgCode(), url, po.getImgCode()));
    }
    result.setPosters(posters);

    List<ActivityEX> activities = activityService.listAll();
    List<CategoryVO> categories = new ArrayList<>(activities.size());
    for (Activity activity : activities) {
      CategoryVO category = new CategoryVO();
      category.setId(activity.getId());
      category.setName(activity.getName());
      categories.add(category);
    }
    result.setCategories(categories);

    List<ProductListSortTypeVO> types = new ArrayList<>();
    for (ProductListSortType t : ProductListSortType.values()) {
      ProductListSortTypeVO type = new ProductListSortTypeVO();
      type.setTitle("按" + t.toString() + "排序");
      type.setValue(t.name());
      types.add(type);
    }
    result.setSortTypes(types);

    String actId = activities.get(0).getId();

    Pageable pageable = new PageRequest(0, 200);
    List<Product> prods = productService.listProductsByActId(actId, pageable);
    List<ProductVO> products = new ArrayList<>();
    for (Product p : prods) {
      ProductVO prodVO = new ProductVO(p);
      prodVO.setImgUrl(p.getImg());
      if (user != null) {
        prodVO.setUrl(siteHost + "/p/" + p.getId() + "?union_id=" + user.getId());
      } else {
        prodVO.setUrl(siteHost + "/p/" + p.getId());
      }
      prodVO.setSales(p.getFakeSales()); // 使用fake数据
      prodVO.setCommission(prodVO.getPrice().multiply(prodVO.getCommissionRate()));
      products.add(prodVO);
    }
    result.setProducts(products);

    return new ResponseObject<>(result);
  }

  private List<ProductVOEx> generateImgUrl(List<Product> products) {
    List<ProductVOEx> exs = new ArrayList<>();
    ProductVOEx ex = null;
    for (Product product : products) {
      ex = new ProductVOEx(new ProductVO(product), urlHelper.genProductUrl(product.getId()),
          product.getImg(), null);
      exs.add(ex);
    }
    return exs;
  }

}
