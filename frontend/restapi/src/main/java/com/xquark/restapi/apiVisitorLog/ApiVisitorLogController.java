package com.xquark.restapi.apiVisitorLog;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.ApiVisitorLog;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.apiVisitorLog.ApiVisitorLogService;
import com.xquark.service.order.OrderService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class ApiVisitorLogController extends BaseController {

  @Autowired
  private ApiVisitorLogService apiVisitorLogService;

  @Autowired
  private OrderService orderService;

  @ResponseBody
  @RequestMapping("/apiVisitorLog/newOrderCount")
  public ResponseObject<Integer> newOrderCount() {
    int newOrderCount = 0;
    String userId = getCurrentUser().getId();
    String url = "/order/list/{status}";
    ApiVisitorLog apiVisitorLog = apiVisitorLogService.findByUserAndUrl(userId, url);
    if (apiVisitorLog != null) {
      Date lastVisitTime = apiVisitorLog.getUpdatedAt();
      newOrderCount = orderService.countNoVisitOrderBySellerId(userId, lastVisitTime);
    }
    return new ResponseObject<>(newOrderCount);
  }

}
