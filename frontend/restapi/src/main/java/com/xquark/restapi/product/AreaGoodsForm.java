package com.xquark.restapi.product;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.PromotionFlashSale;
import com.xquark.dal.type.PromotionType;
import com.xquark.helper.Transformer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-30. DESC:
 */
public class AreaGoodsForm {

  private String area_id;

  private String area_promotionId;

  @NotBlank(message = "活动标题不能为空")
  @ApiModelProperty(value = "area_title", required = true)
  private String area_title;

  @NotBlank(message = "活动图片不能为空")
  @ApiModelProperty(value = "area_img", required = true)
  private String area_img;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "area_validFrom", required = true)
  @NotNull(message = "活动开始日期不能为空")
  private Date area_validFrom;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "area_validTo", required = true)
  @NotNull(message = "活动结束日期不能为空")
  private Date area_validTo;

  @NotBlank(message = "商品id不能为空")
  @ApiModelProperty(value = "area_productId", required = true)
  private String area_productId;

  @ApiModelProperty(value = "price", required = true)
  private BigDecimal price;

  @ApiModelProperty(value = "promotion_price", required = true)
  private BigDecimal promotion_price;

  @ApiModelProperty(value = "deduction_point", required = true)
  private BigDecimal deduction_point;

  @NotNull(message = "预订库存不能为空")
  @ApiModelProperty(value = "amount", required = true)
  private Long area_amount;

  //限购件数
  private Long area_limitAmount;

  //是否支持德分
  private Boolean isPointUsedForArea;

  //是否计算收益
  private Boolean isCalculateEarnings;

  /**
   * 是否显示倒计时
   */
  private Boolean isShowTime;

  /**
   * 活动批次
   */
  private Integer batchNo;

  /**
   * 邮费
   */
  private BigDecimal logisticsFee;

  //活动类型
  private static final PromotionType promotionType = PromotionType.SALEZONE;

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }

  public Integer getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(Integer batchNo) {
    this.batchNo = batchNo;
  }

  public String getArea_id() {
    return area_id;
  }

  public void setArea_id(String area_id) {
    this.area_id = area_id;
  }

  public String getArea_promotionId() {
    return area_promotionId;
  }

  public void setArea_promotionId(String area_promotionId) {
    this.area_promotionId = area_promotionId;
  }

  public String getArea_title() {
    return area_title;
  }

  public void setArea_title(String area_title) {
    this.area_title = area_title;
  }

  public String getArea_img() {
    return area_img;
  }

  public void setArea_img(String area_img) {
    this.area_img = area_img;
  }

  public Date getArea_validFrom() {
    return area_validFrom;
  }

  public void setArea_validFrom(Date area_validFrom) {
    this.area_validFrom = area_validFrom;
  }

  public Date getArea_validTo() {
    return area_validTo;
  }

  public void setArea_validTo(Date area_validTo) {
    this.area_validTo = area_validTo;
  }

  public String getArea_productId() {
    return area_productId;
  }

  public void setArea_productId(String area_productId) {
    this.area_productId = area_productId;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getPromotion_price() {
    return promotion_price;
  }

  public void setPromotion_price(BigDecimal promotion_price) {
    this.promotion_price = promotion_price;
  }

  public BigDecimal getDeduction_point() {
    return deduction_point;
  }

  public void setDeduction_point(BigDecimal deduction_point) {
    this.deduction_point = deduction_point;
  }

  public Long getArea_amount() {
    return area_amount;
  }

  public void setArea_amount(Long area_amount) {
    this.area_amount = area_amount;
  }

  public Long getArea_limitAmount() {
    return area_limitAmount;
  }

  public void setArea_limitAmount(Long area_limitAmount) {
    this.area_limitAmount = area_limitAmount;
  }

  public Boolean getIsPointUsedForArea() {
    return isPointUsedForArea;
  }

  public void setIsPointUsedForArea(Boolean pointUsedForArea) {
    isPointUsedForArea = pointUsedForArea;
  }

  public Boolean getIsCalculateEarnings() {
    return isCalculateEarnings;
  }

  public void setIsCalculateEarnings(Boolean calculateEarnings) {
    isCalculateEarnings = calculateEarnings;
  }

  public Boolean getIsShowTime() {
    return isShowTime;
  }

  public void setIsShowTime(Boolean showTime) {
    isShowTime = showTime;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  private String productId;

  private Long amount;

  private String promotionId;

  private String id;

  public PromotionFlashSale getPromotionProduct() {
    return Transformer.fromBean(this, PromotionFlashSale.class);
  }

}
