package com.xquark.restapi.partner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PartnerProductCommission;
import com.xquark.dal.model.PartnerProductCommissionDe;
import com.xquark.dal.model.PartnerShopCommission;
import com.xquark.dal.model.PartnerType;
import com.xquark.dal.model.PartnerTypeRelation;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.model.UserSigninLog;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.PartnerStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PartnerCmVO;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.PartnerProductCommissionVO;
import com.xquark.dal.vo.UserPartnerApplyVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.user.UserVO;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.common.SignService;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerProductComDeService;
import com.xquark.service.partner.PartnerProductComService;
import com.xquark.service.partner.PartnerShopComService;
import com.xquark.service.partner.PartnerTypeRelationService;
import com.xquark.service.partner.PartnerTypeService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.product.ProductBlackListService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.UserService;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.userAgent.impl.UserSigninLogFactory;
import com.xquark.service.zone.ZoneService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class PartnerController extends BaseController {

  @Autowired
  private UserSigninLogService userSigninLogService;

  @Autowired
  private SignService signService;

  @Autowired
  private UserService userService;

  @Autowired
  private RememberMeServices rememberMeServices;

  @Autowired
  private CouponService couponService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ExcelService excelService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserPartnerService userPartnerService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private PartnerProductComService partnerProductComService;

  @Autowired
  private PartnerProductComDeService partnerProductComDeService;

  @Autowired
  private PartnerTypeService partnerTypeService;

  @Autowired
  private PartnerShopComService partnerShopComService;

  @Autowired
  private PartnerTypeRelationService partnerTypeRelationService;

  @Autowired
  private ProductBlackListService productBlackListService;

  @Autowired
  UrlHelper urlHelper;


  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @RequestMapping(value = "/partner/redirect", params = {"backUrl"})
  public String redirect(HttpServletRequest request, HttpServletResponse response,
      @RequestParam("backUrl") String backurl) {
    // 合作方(partner code)
    final String partner = request.getParameter("partner");
    // 合作方用户唯一id(userId in partner)
    final String externalUserId = request.getParameter("user_id");
    // 合作方用户昵称(user name)
    final String externalUserName = request.getParameter("user_name");
    // 用户头像(avatar)
    final String avatar = request.getParameter("avatar");
    // 用户手机号(mobile)
    final String mobile = request.getParameter("mobile");

    // 签名内容
    final String sign = request.getParameter("sign");

    final String deviceId = StringUtils.defaultString(request.getParameter("did"));

    final String queryString = request.getQueryString();
    boolean signCheck = signService.signCheck(partner, sign, queryString);

    if (!signCheck) {
      return "partner/redirect_error";
    } else {
      User user = null;
      // 尝试手机号登录
      if (StringUtils.isNotEmpty(mobile)) {
        user = userService.loadByLoginname(mobile);
      }
      // 用户不存在，再尝试匿名登录
      if (user == null) {
        final String loginname = externalUserId + "@" + partner;
        user = userService.loadByLoginname(loginname);
      }

      if (user == null) {
        user = userService.registerExtUser(partner, externalUserId, externalUserName, avatar);
//                if("xiangqu".equalsIgnoreCase(partner)){
//                	// 想去新用户送5元优惠券
//                	couponService.grantCoupon("XQ.FIRST", user.getId(), deviceId);
//                }
      } else if (user.getUsername().equals(externalUserName)) {
        // TODO update user externalUserName
      }

      couponService.autoGrantCoupon(partner, user.getId(), deviceId);

//            //TODO 跟首单5元一样， 发放的代码需要重新考虑
//            if("xiangqu".equalsIgnoreCase(partner)){
//            	couponService.grantCoupon("XQ.61", user.getId(), deviceId);
//            	couponService.grantCoupon("XQ.XQ.HUAWEI1507", user.getId(), deviceId);
//            }

      // FIXME why?? comment it
//            request.getSession().invalidate();
      request.getSession().getId();

      final Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
          user.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(auth);
      rememberMeServices.loginSuccess(request, response, auth);

      //记录用户登录环境Log
      final UserSigninLog log = UserSigninLogFactory.createUserSigninLog(request, user);
      userSigninLogService.insert(log);

      return "redirect:" + backurl;  //return backurl; 错误
    }
  }

  @RequestMapping("/partner/redirect")
  public String redirect(HttpServletRequest request, HttpServletResponse response) {
    // 合作方 code
    final String partner = request.getParameter("partner");
    // 合作方用户唯一id
    final String externalUserId = request.getParameter("user_id");
    // 合作方用户昵称
    final String externalUserName = request.getParameter("user_name");
    // 用户头像
    final String avatar = request.getParameter("avatar");
    // 用户手机号
    final String mobile = request.getParameter("mobile");
    // 联盟推广id
    final String unionId = request.getParameter("union_id");
    // 跳转的目标页面
    String targetUrl = request.getParameter("target_url");
    // 签名方式  MD5
    //final String signType = request.getParameter("sign_type");
    // 签名内容
    final String sign = request.getParameter("sign");

    final String deviceId = StringUtils.defaultString(request.getParameter("did"));

    final String queryString = request.getQueryString();
    final boolean signCheck = signService.signCheck(partner, sign, queryString);
    if (!signCheck) {
      return "partner/redirect_error";
    } else {
      User user = null;
      // 尝试手机号登录
      if (StringUtils.isNotEmpty(mobile)) {
        user = userService.loadByLoginname(mobile);
      }
      // 用户不存在，再尝试匿名登录
      if (user == null) {
        final String loginname = externalUserId + "@" + partner;
        user = userService.loadByLoginname(loginname);
      }

      if (user == null) {
        user = userService.registerExtUser(partner, externalUserId, externalUserName, avatar);

//                if("xiangqu".equalsIgnoreCase(partner)){
//                	// 想去新用户送5元优惠券
//                	couponService.grantCoupon("XQ.FIRST", user.getId(), deviceId);
//                }
      } else if (user.getUsername().equals(externalUserName)) {
        // TODO update user externalUserName
      }

      couponService.autoGrantCoupon(partner, user.getId(), deviceId);

//            //TODO 跟首单5元一样， 发放的代码需要重新考虑
//            if("xiangqu".equalsIgnoreCase(partner)){
//            	couponService.grantCoupon("XQ.61", user.getId(), deviceId);
//            }

      // FIXME why?? comment it
//            request.getSession().invalidate();
      request.getSession().getId();

      final Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
          user.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(auth);
      rememberMeServices.loginSuccess(request, response, auth);

      //记录用户登录环境Log
      final UserSigninLog log = UserSigninLogFactory.createUserSigninLog(request, user);
      userSigninLogService.insert(log);

      if (StringUtils.isNotEmpty(unionId)) {
        targetUrl = targetUrl.contains("?") ? targetUrl + "&unionId=" + unionId
            : targetUrl + "?unionId=" + unionId;
      }

      //logger.debug("User[" + user.getId() + "], ext_user_id=" + externalUserId + " from " + partner + " is authed success" );
      logger.debug("User[" + user.getId() + "], ext_user_id=" + externalUserId + " from " + partner
          + " is authed success" +
          " with extuserName:" + externalUserName + " and avatar:" + avatar);

      return "redirect:" + targetUrl;
    }
  }

  /**
   * 卖家获取订单信息
   */
  @ResponseBody
  @RequestMapping("/partner/order/list")
  public ResponseObject<Map<String, Object>> listTwitterOrder(@RequestParam("status") String status,
      @RequestParam("orderNo") String orderNo,
      @RequestParam("sellerPhone") String sellerPhone,
      @RequestParam("partnerPhone") String partnerPhone,
      @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    params.put("rootShopId", getCurrentIUser().getShopId());
    if (!orderNo.equals("")) {
      params.put("orderNo", orderNo);
    }
    if (!sellerPhone.equals("")) {
      params.put("sellerPhone", sellerPhone);
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (!partnerPhone.equals("")) {
      params.put("partnerPhone", partnerPhone);
    }

    if (!status.equals("") && !status.equals("ALL")) {
      List<String> statusList = new ArrayList<>();
      statusList.add(status);
      params.put("status", statusList);
    }

    List<OrderVO> result = orderService.listByPartner(params, pageable);
    for (OrderVO order : result) {
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      if (order.getOrderAddress() != null) {
        List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
        String addressDetails = "";
        for (Zone zone : zoneList) {
          addressDetails += zone.getName();
        }
        addressDetails += order.getOrderAddress().getStreet();
        order.setAddressDetails(addressDetails);
      }
      String sellerName =
          userService.load(order.getSellerId()) != null ? userService.load(order.getSellerId())
              .getLoginname() : "";
      String buyerName =
          userService.load(order.getBuyerId()) != null ? userService.load(order.getBuyerId())
              .getName() : "";
      order.setSellerName(sellerName);
      order.setBuyerName(buyerName);
      List<Commission> list = commissionService
          .listByOrderIdAndType(order.getId(), CommissionType.PARTNER);
      BigDecimal sumCommission = new BigDecimal(0);
      for (Commission commission : list) {
        sumCommission = sumCommission.add(commission.getFee());
      }
      order.setCommissionFee(sumCommission);

    }
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("orderTotal", orderService.countByPartner(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }


  /**
   * 禁用shop
   */
  @ResponseBody
  @RequestMapping("/partnerMember/disableShop/{id}")
  public ResponseObject<Boolean> disableShop(@PathVariable String id) {
    //TODO liangfan
    String shopId = getCurrentIUser().getShopId();
    UserPartner userPartner = userPartnerService
        .selectActiveUserPartnersByUserIdAndShopId(id, shopId);
    int result = userPartnerService.delete(userPartner.getId());
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }


  }

  /**
   * 获取团队人员列表
   */
  @ResponseBody
  @RequestMapping(value = "/partnerCenter/teamMember")
  public ResponseObject<List<UserVO>> twitterTeam(
      @RequestParam(value = "status", required = false) String status, Pageable pageable) {
//        if ("all".equals(status)){
//
//        } else if ("twitter".equals(status)) {
//
//        } else if ("leader".equals(status)) {
//
//        }
    //partnerTeamCount
    String currentShopId = getCurrentIUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(currentShopId).getRootShopId();
    List<UserVO> result = new ArrayList<>(pageable.getPageSize());
    List<ShopTree> shopTreeList = shopTreeService.listChildren(rootShopId, currentShopId);
    for (ShopTree shopTree : shopTreeList) {
      String shopId = shopTree.getDescendantShopId();
      String userId = shopService.load(shopId).getOwnerId();
      result.add(new UserVO((User) userService.load(userId), urlHelper.genShopUrl(shopId)));
    }
    if (StringUtils.isNotBlank(rootShopId) && StringUtils.isNotBlank(currentShopId)) {
      Long myChildrenCount = shopTreeService.countChildren(rootShopId, currentShopId);
      //model.addAttribute("partnerTeamCount", myChildrenCount);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 合伙人结算记录列表
   */
  @ResponseBody
  @RequestMapping("/partnerRecord/list")
  public ResponseObject<Map<String, Object>> listPartnerRecord(
      @RequestParam(value = "status", required = false) String status,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "orderNo", required = false) String orderNo,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", name);
    }
    if (StringUtils.isNotEmpty(orderNo)) {
      params.put("orderNo", orderNo);
    }

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }
    if (StringUtils.isNotEmpty(startDate)) {
      params.put("startDate", startDate);
    }
    if (StringUtils.isNotEmpty(endDate)) {
      params.put("endDate", endDate);
    }
    if (StringUtils.isNotEmpty(status)) {
      params.put("status", status);
    }

    String shopId = getCurrentIUser().getShopId();
    List<PartnerOrderCmVO> result = userPartnerService
        .listPartnerCommissionByShopId(shopId, params, pageable);
    Long total = userPartnerService.countPartnerCommissionByShopId(shopId, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }


  /**
   * 合伙人成员列表
   */
  @ResponseBody
  @RequestMapping("/partnerMember/list")
  public ResponseObject<Map<String, Object>> listTwitterMember(
      @RequestParam("status") String status, @RequestParam("startDate") String startDate,
      @RequestParam("endDate") String endDate,
      @RequestParam("phone") String phone, @RequestParam("shopName") String shopName,
      @RequestParam("partner_status") String partner_status, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (!phone.equals("")) {
      params.put("phone", phone);
    }
    if (!shopName.equals("")) {
      params.put("shopName", shopName);
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (!endDate.equals("")) {
      params.put("endDate", endDate);
    }

    // 申请状态
    if (!partner_status.equals("") && !partner_status.equals("ALL")) {
      params.put("partner_status", partner_status);
    }

    if (!status.equals("") && !status.equals("ALL")) {
      List<String> statusList = new ArrayList<>();
      statusList.add(status);
      params.put("status", statusList);
    }

    String shopId = getCurrentIUser().getShopId();
    List<PartnerCmVO> result = userPartnerService
        .selectUserPartnersCmByShopId(shopId, params, pageable);
    for (PartnerCmVO vo : result) {
      BigDecimal sumCommission = new BigDecimal(0);
      if (vo.getFee() == null) {
        vo.setFee(sumCommission);
      }
      String userId = vo.getUserId();
      // 获取该合伙人的所有类型名称
      StringBuilder typeSb = new StringBuilder();
      List<PartnerTypeRelation> relations = partnerTypeRelationService
          .selectByUserId(shopId, userId);
      for (PartnerTypeRelation relation : relations) {
        String typeStr = relation.getTypeStr();
        String typeId = relation.getTypeId();
        if ("".equals(typeSb.toString())) {
          typeSb.append(typeStr);
        } else {
          typeSb.append(" | " + typeStr);
        }
        if (StringUtils.isNotEmpty(typeId)) {
          PartnerType partnerType = partnerTypeService.selectByPrimaryKey(typeId);
          if (partnerType != null) {
            typeSb.append(" (" + partnerType.getName() + ")");
          }
        }
      }
      vo.setTypeStr(typeSb.toString());

      // 判断合伙人是否有黑名单商品
      Map<String, Object> blackparams = new HashMap<>();
      blackparams.put("userId", userId);
      long blackTotal = productBlackListService.selectCnt(params);
      if (blackTotal > 0) {
        vo.setBlackList(true);
      } else {
        vo.setBlackList(false);
      }

    }
    Long total = userPartnerService.countUserPartnersCmByShopId(shopId, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }


  /**
   * 合伙人获得下级
   */
  @ResponseBody
  @RequestMapping("/partnerMember/children")
  public ResponseObject<Map<String, Object>> listPartnerChildren(
      @RequestParam("userId") String userId, Pageable pageable) {
    Map<String, Object> aRetMap = new HashMap<>();
    if (StringUtils.isNotBlank(userId)) {
      String shopId = shopService.findByUser(userId).getId();
      String rootShopId = getCurrentIUser().getShopId();
      List<ShopTree> result = shopTreeService
          .listChildren4Admin(rootShopId, shopId, null, pageable);
      List<Shop> shopList = new ArrayList<>();
      List<OrderVO> shopList1 = new ArrayList<>();
      Long total = shopTreeService.countChildren(rootShopId, shopId);
      for (ShopTree shopTree : result) {
        String userShopId = shopTree.getDescendantShopId();
        shopList.add(shopService.load(userShopId));
      }
      aRetMap.put("total", total);
      aRetMap.put("list", shopList);
    }
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 查询该推客是否已经是合伙人
   */
  @ResponseBody
  @RequestMapping("/partnerMember/existShop/{id}")
  public ResponseObject<Boolean> existShop(@PathVariable String id) {
    String shopId = getCurrentIUser().getShopId();
    UserPartner userPartner = userPartnerService
        .selectActiveUserPartnersByUserIdAndShopId(id, shopId);
    if (userPartner != null) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }


  }

  /**
   * 合伙人概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/partner/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取合伙人概况中的今日数据,本周订单,合伙人,交易额,佣金数
    summary = userPartnerService.getSummary(shopId);

    return new ResponseObject<>(summary);
  }

  /**
   * 合伙人申请列表查询
   */
  @ResponseBody
  @RequestMapping("/partner/apply/list")
  public ResponseObject<Map<String, Object>> listPartnerApply(Pageable pageable,
      HttpServletRequest request) {
    Map<String, Object> params = new HashMap<>();
    String partner_status = request.getParameter("partner_status");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String phone = request.getParameter("phone");
    String shopName = request.getParameter("shopName");
    String name = request.getParameter("userName");

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }

    params.put("partner_status", partner_status);
    params.put("startDate", startDate);
    params.put("endDate", endDate);
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(shopName)) {
      params.put("shopName", "%" + shopName + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    List<UserPartnerApplyVO> result = userPartnerService.listPartnerApply(params, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("applyTotal", userPartnerService.countPartnerApply(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 合伙人申请审核
   */
  @ResponseBody
  @RequestMapping("/partner/apply/{id}")
  public ResponseObject<UserPartner> partnerApply(@PathVariable String id) {
    UserPartner userPartner = new UserPartner();
    userPartner.setId(id);
    userPartner.setUpdatedAt(new Date());
    userPartner.setStatus(PartnerStatus.ACTIVE);
    userPartnerService.update(userPartner);
    return new ResponseObject<>(userPartner);
  }

  /**
   * 合伙人商品佣金列表
   */
  @ResponseBody
  @RequestMapping("/partner/productList")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<PartnerProductCommissionVO> result = partnerProductComService.list(pageable, params);
    Long total = partnerProductComService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 查询是否已经有该商品已经设置过分佣设置
   */
  @ResponseBody
  @RequestMapping("/partner/checkProductCommission")
  public ResponseObject<Boolean> checkProductCommission(
      @RequestParam("productId") String productId) {
    String shopId = getCurrentIUser().getShopId();
    long count = partnerProductComService.selectByProductId(shopId, productId);
    if (count > 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }


  /**
   * 保存合伙人商品佣金设置
   */
  @ResponseBody
  @RequestMapping("/partner/saveProductCommission")
  public ResponseObject<Boolean> saveProductCommission(HttpServletRequest request) {
    boolean flag = false;
    String shopId = getCurrentIUser().getShopId();
    String id = request.getParameter("id");
    String productId = request.getParameter("productId");
    String team_id = request.getParameter("teamId");
    String team_rate = request.getParameter("teamRate");
    String shareholder_id = request.getParameter("shareholderId");
    String shareholder_rate = request.getParameter("shareholderRate");
    String platform_id = request.getParameter("platformId");
    String platform_rate = request.getParameter("platformRate");

    // 如果id不为空，则说明是修改，只用更新佣金明细记录的信息即可
    if (StringUtils.isNotEmpty(id)) {
      PartnerProductCommissionDe de = new PartnerProductCommissionDe();
      de.setId(team_id);
      de.setRate(new BigDecimal(team_rate));
      partnerProductComDeService.updateByPrimaryKeySelective(de);

      de = new PartnerProductCommissionDe();
      de.setId(shareholder_id);
      de.setRate(new BigDecimal(shareholder_rate));
      partnerProductComDeService.updateByPrimaryKeySelective(de);

      de = new PartnerProductCommissionDe();
      de.setId(platform_id);
      de.setRate(new BigDecimal(platform_rate));
      partnerProductComDeService.updateByPrimaryKeySelective(de);
    }
    // 如果id为空，则说明是新增，则先要新增主表信息，再分别插入各种类型佣金子表信息
    else {
      PartnerProductCommission commission = new PartnerProductCommission();
      commission.setShopId(shopId);
      commission.setArchive(false);
      commission.setProductId(productId);
      partnerProductComService.insert(commission);

      PartnerProductCommissionDe de = new PartnerProductCommissionDe();
      de.setParentId(commission.getId());
      de.setType(CommissionType.TEAM);
      de.setRate(new BigDecimal(team_rate));
      partnerProductComDeService.insert(de);

      de = new PartnerProductCommissionDe();
      de.setParentId(commission.getId());
      de.setType(CommissionType.SHAREHOLDER);
      de.setRate(new BigDecimal(shareholder_rate));
      partnerProductComDeService.insert(de);

      de = new PartnerProductCommissionDe();
      de.setParentId(commission.getId());
      de.setType(CommissionType.PLATFORM);
      de.setRate(new BigDecimal(platform_rate));
      partnerProductComDeService.insert(de);
    }
    flag = true;
    if (flag) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 删除合伙人商品佣金
   */
  @ResponseBody
  @RequestMapping("/partner/deleteProductCommission/{id}")
  public ResponseObject<Boolean> deleteProductCommission(@PathVariable String id) {
    int result = partnerProductComService.deleteByPrimaryKey(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取合伙人商品佣金
   */
  @ResponseBody
  @RequestMapping("/partner/getProductCommission/{id}")
  public ResponseObject<PartnerProductCommissionVO> getProductCommission(@PathVariable String id) {
    PartnerProductCommissionVO result = partnerProductComService.selectByPrimaryKey(id);
    return new ResponseObject<>(result);
  }

  /**
   * 合伙人类型列表
   */
  @ResponseBody
  @RequestMapping("/partner/typeList")
  public ResponseObject<Map<String, Object>> typeList(@RequestParam("type") String type,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    params.put("type", type);
    List<PartnerType> result = partnerTypeService.list(pageable, params);
    Long total = partnerTypeService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }


  /**
   * 保存合伙人类型
   */
  @ResponseBody
  @RequestMapping("/partner/saveType")
  public ResponseObject<Boolean> saveType(PartnerType partnerType) {
    int result = 0;
    String shopId = getCurrentIUser().getShopId();
    if (StringUtils.isNotEmpty(partnerType.getId())) {
      result = partnerTypeService.updateByPrimaryKeySelective(partnerType);
    } else {
      partnerType.setShopId(shopId);
      partnerType.setArchive(false);
      result = partnerTypeService.insert(partnerType);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 删除合伙人类型
   */
  @ResponseBody
  @RequestMapping("/partner/deleteType/{id}")
  public ResponseObject<Boolean> deleteType(@PathVariable String id) {
    int result = partnerTypeService.deleteByPrimaryKey(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取合伙人类型
   */
  @ResponseBody
  @RequestMapping("/partner/getType/{id}")
  public ResponseObject<PartnerType> getType(@PathVariable String id) {
    PartnerType result = partnerTypeService.selectByPrimaryKey(id);
    return new ResponseObject<>(result);
  }

  /**
   * 合伙人设置中默认的分红设置
   */
  @ResponseBody
  @RequestMapping("/partner/getDefaultCommission")
  public ResponseObject<Map> getDefaultCommission() {
    HashMap map = new HashMap();
    String rootShopId = getCurrentIUser().getShopId();
    //获得shopId
    String shopId = getCurrentIUser().getShopId();
    PartnerShopCommission platform = null;    // 平台合伙人佣金
    PartnerShopCommission team = null;          //团队合伙人佣金
    PartnerShopCommission shareholder = null;  //股东合伙人佣金
    //加载合伙人默认配置的佣金信息
    List<PartnerShopCommission> partnerShopCommissions = partnerShopComService
        .selectByShopId(shopId);
    for (PartnerShopCommission partnerShopCommission : partnerShopCommissions) {
      if (partnerShopCommission.getType() == CommissionType.PLATFORM) {
        platform = partnerShopCommission;
      }
      if (partnerShopCommission.getType() == CommissionType.TEAM) {
        team = partnerShopCommission;
      }
      if (partnerShopCommission.getType() == CommissionType.SHAREHOLDER) {
        shareholder = partnerShopCommission;
      }
    }
    map.put("platform", platform);
    map.put("team", team);
    map.put("shareholder", shareholder);
    return new ResponseObject<Map>(map);
  }

  /**
   * 获取某个合伙人设置的合伙人类型
   */
  @ResponseBody
  @RequestMapping("/partner/getTypeSet")
  public ResponseObject<Map> getTypeSet(@RequestParam("userId") String userId) {
    HashMap map = new HashMap();
    //获得shopId
    String shopId = getCurrentIUser().getShopId();
    PartnerTypeRelation platform = null;    // 平台合伙人类型
    PartnerTypeRelation team = null;          //团队合伙人类型
    PartnerTypeRelation shareholder = null;  //股东合伙人类型
    //加载合伙人默认配置的佣金信息
    List<PartnerTypeRelation> typeRelations = partnerTypeRelationService
        .selectByUserId(shopId, userId);
    for (PartnerTypeRelation typeRelation : typeRelations) {
      if (typeRelation.getType() == CommissionType.PLATFORM) {
        platform = typeRelation;
      }
      if (typeRelation.getType() == CommissionType.TEAM) {
        team = typeRelation;
      }
      if (typeRelation.getType() == CommissionType.SHAREHOLDER) {
        shareholder = typeRelation;
      }
    }
    map.put("platformType", platform);
    map.put("teamType", team);
    map.put("shareholderType", shareholder);
    return new ResponseObject<Map>(map);
  }


  /**
   * 保存合伙人类型设置
   */
  @ResponseBody
  @RequestMapping("/partner/saveTypeSet")
  public ResponseObject<Boolean> saveTypeSet(@RequestParam("userId") String userId,
      @RequestParam("types") String types) {
    JSONArray typesjson = JSON.parseArray(types);
    //获得shopId
    String shopId = getCurrentIUser().getShopId();
    // 保存前先删掉用户之前设置的合伙人类型
    partnerTypeRelationService.deleteByUserId(shopId, userId);
    for (Object aTypesjson : typesjson) {
      JSONObject map = (JSONObject) aTypesjson;
      String typeId = (String) map.get("typeId");
      String type = (String) map.get("type");
      PartnerTypeRelation partnerTypeRelation = new PartnerTypeRelation();
      partnerTypeRelation.setShopId(shopId);
      partnerTypeRelation.setArchive(false);
      partnerTypeRelation.setType(CommissionType.valueOf(type));
      // 如果typeid为空，则修改为74,对应的long型为0
      if (StringUtils.isEmpty(typeId)) {
        typeId = "74";
      }
      partnerTypeRelation.setTypeId(typeId);
      partnerTypeRelation.setUserId(userId);
      partnerTypeRelationService.insert(partnerTypeRelation);
    }
    return new ResponseObject<>(true);
  }

}
