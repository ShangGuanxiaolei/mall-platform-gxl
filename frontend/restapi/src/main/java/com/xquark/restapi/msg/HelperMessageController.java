package com.xquark.restapi.msg;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.HelperMessage;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.msg.HelperMessageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//@communityDetailController
//@ApiIgnore
public class HelperMessageController {

  //	@Autowired
  private HelperMessageService helperService;

  //	@Autowired
  private ResourceFacade resourceFacade;

  @Value("${site.web.host.name}")
  private String siteWebHostName;

  @ResponseBody
  @RequestMapping("/message/helper/list")
  public ResponseObject<List<HelperMessage>> list(Pageable pageble) {
    List<HelperMessage> messages = helperService.loadHelperList(pageble);
    for (HelperMessage message : messages) {
      message.setIconKey(message.getIcon());
      message
          .setIcon(resourceFacade.resolveUrl(message.getIcon() + "|" + ResourceFacade.IMAGE_S025));
      //TODO 须改成ResourceFacade
      if (!message.getUrl().startsWith(ResourceFacade.EXT_RES_PREFIX)) {
        message.setUrl(siteWebHostName + message.getUrl());
      }
    }
    return new ResponseObject<>(messages);
  }

}
