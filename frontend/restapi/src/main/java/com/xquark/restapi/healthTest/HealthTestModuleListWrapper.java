package com.xquark.restapi.healthTest;

import com.xquark.dal.vo.HealthTestModuleQuestionVO;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-11. DESC:
 */
public class HealthTestModuleListWrapper {

  private List<HealthTestModuleQuestionVO> info;

  public List<HealthTestModuleQuestionVO> getInfo() {
    return info;
  }

  public void setInfo(List<HealthTestModuleQuestionVO> info) {
    this.info = info;
  }
}
