package com.xquark.restapi.healthTest;

import com.xquark.dal.IUser;
import com.xquark.dal.model.HealthTestAnswer;
import com.xquark.dal.model.HealthTestAnswerGroup;
import com.xquark.dal.model.HealthTestAnswerGroupExample;
import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestModuleExample;
import com.xquark.dal.model.HealthTestPhysique;
import com.xquark.dal.model.HealthTestProfile;
import com.xquark.dal.model.HealthTestQuestion;
import com.xquark.dal.model.HealthTestQuestionExample;
import com.xquark.dal.model.HealthTestResult;
import com.xquark.dal.model.HealthTestResultProduct;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.HealthTestComputeType;
import com.xquark.dal.status.StaticModule;
import com.xquark.dal.type.GenderType;
import com.xquark.dal.vo.HealthModuleQuestionsSave;
import com.xquark.dal.vo.HealthTestModuleQuestionVO;
import com.xquark.dal.vo.HealthTestModuleVO;
import com.xquark.dal.vo.HealthTestProfileVO;
import com.xquark.dal.vo.HealthTestQuestionVO;
import com.xquark.dal.vo.HealthTestResultVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.HealthTestAnswerService;
import com.xquark.service.healthTest.HealthTestDescription;
import com.xquark.service.healthTest.HealthTestModuleService;
import com.xquark.service.healthTest.HealthTestPhysiqueService;
import com.xquark.service.healthTest.HealthTestProfileService;
import com.xquark.service.healthTest.HealthTestQuestionService;
import com.xquark.service.healthTest.HealthTestResultService;
import com.xquark.service.healthTest.impl.HealthTestModuleServiceImpl;
import com.xquark.utils.ExcelUtils;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by wangxinhua on 17-7-4. DESC: 健康自测接口
 */
//@RestController
//@RequestMapping("/healthTest")
public class HealthTestController extends BaseController {

  private HealthTestModuleService moduleService;

  private HealthTestQuestionService questionService;

  private HealthTestAnswerService answerService;

  private HealthTestPhysiqueService physiqueService;

  private HealthTestProfileService profileService;

  private HealthTestResultService resultService;

  @RequestMapping(value = "/results", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> result(
      @RequestBody HealthTestModuleListWrapper listWrapper) {
    Map<String, Object> resultMap = new HashMap<>();
    Set<HealthTestDescription> descriptions = new LinkedHashSet<>();
    HealthTestModule basicTest = moduleService.loadByStaticName(StaticModule.PROFILE.name());
    List<HealthTestModuleQuestionVO> infoList = listWrapper.getInfo();
    if (CollectionUtils.isEmpty(infoList)) {
      return new ResponseObject<>();
    }
    for (HealthTestModuleQuestionVO questionVO : listWrapper.getInfo()) {
      // 基础测试跳过
      if (basicTest != null && basicTest.getId().equals(questionVO.getModuleId())) {
        continue;
      }
      HealthTestDescription description = questionService.getResultDescription(questionVO);
      if (description != null) {
        HealthTestModule parent = moduleService.loadParent(questionVO.getModuleId());
        if (parent != null) {
          HealthTestComputeType computeType = HealthTestComputeType.valueOf(parent.getType());
          // TODO wangxinhua 基础测试 计算类型COUNT 九辩体质测试类型SUM 如有更多类型不能根据这两种类型判断
          if (description.getDescription() == null && computeType == HealthTestComputeType.COUNT) {
            description.setDescription("您状态不错，继续保持哦∽健康的体质，才能创造美好生活呀☺");
          }
          descriptions.add(description);
        }
      }
    }
    resultMap.put("list", descriptions);
    User user = (User) getCurrentIUser();
    HealthTestProfile profile = profileService.loadByUser(user.getId());
    if (profile != null) {
      resultMap.put("profile", new HealthTestProfileVO(profile));
    }
    return new ResponseObject<>(resultMap);
  }

  // -------- module --------------
  @RequestMapping("/module/view")
  public ResponseObject<HealthTestModule> view(String id) {
    HealthTestModule module = moduleService.load(id);
    return new ResponseObject<>(module);
  }

  @RequestMapping("/module/list")
  public ResponseObject<Map<String, Object>> list(
      String parentId,
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      Pageable pageable) {

    HealthTestModuleExample moduleExample = HealthTestModuleServiceImpl.createExample(parentId,
        order, Sort.Direction.fromString(direction), pageable);
    List<HealthTestModule> moduleVOS;
    Long total;
    try {
      moduleVOS = moduleService.list(moduleExample);
      total = moduleService.count(moduleExample);
    } catch (Exception e) {
      String msg = "加载数据失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", moduleVOS);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/questions")
  public ResponseObject<Map<String, Object>> listWithQuestions(
      @RequestParam("moduleIds") String[] moduleIds) {

    Set<String> ids = new HashSet<>();
    for (String id : moduleIds) {
      List<HealthTestModule> children = moduleService.list(id);
      if (children != null && children.size() > 0) {
        for (HealthTestModule module : children) {
          ids.add(module.getId());
        }
      } else {
        ids.add(id);
      }
    }
    List<HealthTestModuleQuestionVO> questionVOList = new ArrayList<>();
    IUser user = getCurrentIUser();
    String userId = user.getId();
    try {
      HealthTestProfile profile = profileService.loadByUser(userId);
      Integer requiredSex = null;
      if (profile != null) {
        GenderType genderType = GenderType.valueOf(profile.getGender());
        requiredSex = genderType.getCode();
      }
      HealthTestModule basicTest = moduleService.loadByStaticName(StaticModule.PROFILE.name());
      // 没做过基础测试则强制加入基础测试并置顶
      if (basicTest != null && profile == null && !ids.contains(basicTest.getId())) {
        // TODO wangxinhua 没有做过测试则强制将做基础测试并放到第一位
        // TODO wangxinhua 暂时先手动加
        HealthTestModuleQuestionVO basicTestVO = new HealthTestModuleQuestionVO();
        basicTestVO.setModuleId(basicTest.getId());
        basicTestVO.setModuleName(basicTest.getName());
        basicTestVO.setStaticName(basicTest.getStaticName());
        basicTestVO.setQuestions(new ArrayList<HealthTestQuestionVO>());
        questionVOList.add(basicTestVO);
      }
      String[] idsString = ids.toArray(new String[ids.size()]);
      questionVOList.addAll(questionService.listWithModule(idsString, requiredSex));
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "数据加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", questionVOList);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/jsTree")
  public List<Map<String, Object>> jsTree(
      @RequestParam(value = "parentId", required = false) String parentId) {
    List<HealthTestModuleVO> moduleVOS =
        StringUtils.isBlank(parentId) ? moduleService.treeMenuList() :
            moduleService.treeMenuList(parentId);
    List<Map<String, Object>> result = new ArrayList<>();
    Map<String, Object> resultMap = new HashMap<>();
    resultMap.put("id", "0");
    resultMap.put("text", "健康自测菜单");
    resultMap.put("children", moduleVOS);
    result.add(resultMap);
    return result;
  }

  @RequestMapping("/module/listTree")
  public ResponseObject<List<HealthTestModuleVO>> listTree(
      @RequestParam(value = "parentId", required = false) String parentId) {
    List<HealthTestModuleVO> moduleVOS;
    try {
      IUser user = getCurrentIUser();
      String userId = user.getId();
      HealthTestProfile profile = profileService.loadByUser(userId);
      HealthTestModuleExample example = new HealthTestModuleExample();
      if (profile != null) {
        GenderType genderType = GenderType.valueOf(profile.getGender());
        int genderCode = genderType.getCode();
        List<Integer> genderList = new ArrayList<>();
        genderList.add(GenderType.N.getCode());
        genderList.add(genderCode);
        example.or()
            .andStaticNameNotEqualTo(StaticModule.PROFILE.name())
            .andRequiredSexIn(genderList);
        example.or()
            .andStaticNameIsNull()
            .andRequiredSexIn(genderList);
      }
      example.setOrderByClause("sort_no");
      moduleVOS = StringUtils.isBlank(parentId) ? moduleService.treeMenuList(example) :
          moduleService.treeMenuList(parentId, example);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "数据加载出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(moduleVOS);
  }

  @RequestMapping("/module/listSub")
  public ResponseObject<Map<String, Object>> listSub(@RequestParam("parentId") String parentId) {
    if (!"0".equals(parentId)) {
      HealthTestModule parent = moduleService.load(parentId);
      if (parent == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "父菜单不存在");
      }
    }
    List<HealthTestModule> subs;
    Integer count;
    try {
      subs = moduleService.list(parentId);
      count = moduleService.loadSubCounts(parentId);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "查找子菜单失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", subs);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/save")
  public ResponseObject<HealthTestModule> save(HealthTestModule module) {
    if (module == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不正确");
    }
    // 页面的根节点
    if (StringUtils.isBlank(module.getParentId()) || "0".equals(module.getParentId())) {
      module.setParentId(null);
    }
    if (StringUtils.isBlank(module.getId())) {
      module.setId(null);
    }
    String id = module.getId();
    String parentId = module.getParentId();
    try {
      if (StringUtils.isBlank(id)) {
        HealthTestModule exists = moduleService.loadChild(parentId, module.getName());
        if (exists != null) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该菜单已存在");
        }
        int level = moduleService.countLevel(parentId);
        if (level >= HealthTestModule.MAX_LEVEL) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "菜单已达到最大层级，不能增加新的子菜单");
        }
        List<HealthTestQuestion> questions = questionService.list(parentId);
        if (questions.size() > 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该菜单已配置问题，请先清空问题后建立新菜单");
        }
        moduleService.save(module);
        String unEncodedId = module.getId();
        String encodedId = IdTypeHandler.encode(Long.valueOf(unEncodedId));
        module.setId(encodedId);
      } else {
        moduleService.update(module);
      }
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "保存菜单失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(module);
  }

  @RequestMapping("/module/delete")
  public ResponseObject<Boolean> delete(
      @RequestParam("id") String moduleId,
      SecurityContextHolderAwareRequestWrapper requestWrapper) {
    HealthTestModule module = moduleService.load(moduleId);
    if (module == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要删除的菜单不存在");
    }
    String staticName = module.getStaticName();
    boolean isRoot = requestWrapper.isUserInRole("ROLE_ROOT");
    if (StringUtils.isNotBlank(staticName) && !isRoot) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除该菜单会影响功能正常使用，请联系管理员");
    }
    boolean result;
    try {
      result = moduleService.delete(moduleId);
    } catch (Exception e) {
      String msg = "删除菜单失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/subCounts")
  public ResponseObject<Integer> subCounts(@RequestParam("id") String id) {
    if ("0".equals(id)) {
      id = null;
    }
    Integer counts = moduleService.loadSubCounts(id);
    return new ResponseObject<>(counts);
  }

  @RequestMapping("/module/results")
  public ResponseObject<Map<String, Object>> results(
      @RequestParam("moduleId") String moduleId,
      String order,
      @RequestParam(defaultValue = "ASC") String direction,
      Pageable pageable) {
    List<HealthTestResultVO> results;
    Long count;
    try {
      results = resultService
          .listResultVO(moduleId, order, Sort.Direction.fromString(direction), pageable);
      count = resultService.countResults(moduleId);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "数据加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", results);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/saveResult")
  public ResponseObject<Boolean> saveResult(HealthTestResult healthTestResult) {
    if (healthTestResult == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "表单不能为空");
    }
    boolean result;
    String id = healthTestResult.getId();
    try {
      if (StringUtils.isBlank(id)) {
        healthTestResult.setId(null);
        result = resultService.saveResult(healthTestResult);
      } else {
        result = resultService.updateResult(healthTestResult);
      }
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "测试结果保存失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(result);
  }

  // -------- module end--------------

  // -------- question --------------

  @RequestMapping("/question/save")
  public ResponseObject<Boolean> save(HealthTestQuestion question) {
    if (question == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不能为空");
    }
    boolean result;
    try {
      if (StringUtils.isBlank(question.getId())) {
        result = questionService.save(question);
      } else {
        result = questionService.update(question);
      }
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "保存失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/question/delete")
  public ResponseObject<Boolean> delete(@RequestParam("id") String id) {
    boolean result = questionService.delete(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/question/listTable")
  public ResponseObject<Map<String, Object>> listTable(
      String moduleId, @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    if ("0".equals(moduleId)) {
      moduleId = null;
    }
    HealthTestQuestionExample questionExample = new HealthTestQuestionExample();
    if (StringUtils.isNotBlank(moduleId)) {
      // 有字节点则取出所有字节点的题目总和
      List<HealthTestModule> children = moduleService.list(moduleId);
      if (children != null && children.size() > 0) {
        List<String> ids = new ArrayList<>();
        for (HealthTestModule module : children) {
          ids.add(module.getId());
        }
        if (ids.size() > 0) {
          questionExample.or().andModuleIdIn(ids);
        }
      } else {
        questionExample.or()
            .andModuleIdEqualTo(moduleId);
      }
    }
    if (StringUtils.isNotBlank(order)) {
      questionExample.setOrderByClause("`" + order + "` " + direction);
    }
    if (pageable != null) {
      questionExample.setOffset(pageable.getOffset());
      questionExample.setLimit(pageable.getPageSize());
    }
    List<HealthTestQuestion> questionList;
    Long total;
    try {
      questionList = questionService.list(questionExample);
      total = questionService.count(questionExample);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "数据加载出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", questionList);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/question/list")
  public ResponseObject<Map<String, Object>> listQuestion(
      String moduleId, @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction) {
    if ("0".equals(moduleId)) {
      moduleId = null;
    }
    HealthTestQuestionExample questionExample = new HealthTestQuestionExample();
    if (StringUtils.isNotBlank(moduleId)) {
      HealthTestModule module = moduleService.load(moduleId);
      if (module == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "菜单不存在");
      }
      // 有字节点则取出所有字节点的题目总和
      List<HealthTestModule> children = moduleService.list(module.getId());
      if (children != null && children.size() > 0) {
        List<String> ids = new ArrayList<>();
        for (HealthTestModule child : children) {
          ids.add(child.getId());
        }
        questionExample.or().andModuleIdIn(ids);
      } else {
        questionExample.or()
            .andModuleIdEqualTo(moduleId);
      }
    }
    if (StringUtils.isNotBlank(order)) {
      questionExample.setOrderByClause("`" + order + "` " + direction);
    }
    List<HealthTestQuestionVO> questionVOList;
    long total;
    try {
      questionVOList = questionService.listVO(questionExample);
      total = questionService.count(questionExample);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "测试题加载出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", questionVOList);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/question/import")
  public ResponseObject<Map<String, Integer>> importFIle(
      @RequestParam("excelFile") MultipartFile file) throws IOException {
    Map<String, Integer> resultMap = new HashMap<>();
    if (!file.isEmpty()) {
      try (InputStream inputStream = file.getInputStream()) {
        String[][] excelData = ExcelUtils.excelImport(inputStream);
        if (excelData == null || excelData.length == 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "文件不能为空");
        }
        // 从第二行开始循环, 第一行为excel标题行

        Map<String, HealthModuleQuestionsSave> modulesToSave = new HashMap<>();
        String currentQuestionName = null;
        try {
          for (String[] row : Arrays.copyOfRange(excelData, 1, excelData.length)) {
            // 菜单
            String moduleName = row[0];

            if (StringUtils.isBlank(moduleName)) {
              continue;
            }
            String type = row[5];

            // 问题
            String questionName = row[2];
            currentQuestionName = questionName;
            log.debug("当前导入问题: " + currentQuestionName);
            String questionType = row[3];
            String groupId = IdTypeHandler.encode(Long.valueOf(row[4]));
            Integer requiredSex = GenderType.valueOf(row[6]).getCode();

            HealthTestQuestion question = new HealthTestQuestion(questionName, groupId,
                questionType);
            HealthTestModule module = new HealthTestModule();
            module.setName(moduleName);
            module.setType(type);
            module.setRequiredSex(requiredSex);
            module.setRequired(false);

            // 所属菜单的名称
            String belong = row[1];
            // TODO wangxinhua 查询增加缓存不用每次导入都查
            HealthTestModule parentModule = moduleService.loadByName(belong);
            if (parentModule == null)
            // TODO wangxinhua 暂时如果对不上父节点直接跳过
            {
              continue;
            }
            module.setParentId(parentModule.getId());
            HealthModuleQuestionsSave healthModuleQuestionsSave = modulesToSave.get(moduleName);
            if (healthModuleQuestionsSave == null) {
              healthModuleQuestionsSave = new HealthModuleQuestionsSave(module);
              modulesToSave.put(moduleName, healthModuleQuestionsSave);
            }
            healthModuleQuestionsSave.getQuestionList().add(question);
          }
        } catch (Exception e) {
          String msg = "文件格式有误, 问题" + currentQuestionName + "格式不正确c";
          log.error(msg, e);
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
        }
        boolean saveResult = moduleService.save(modulesToSave.values());
        if (saveResult) {
          resultMap.put("errorCode", 200);
        } else {
          resultMap.put("errorCode", 500);
        }
      } catch (InvalidFormatException e) {
        String msg = "文件格式不正确";
        log.error(msg, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
      } catch (BizException e) {
        throw e;
      } catch (Exception e) {
        String msg = "文件导入失败";
        log.error(msg, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
      }
    }
    return new ResponseObject<>(resultMap);
  }

  // -------- question end --------------

  // -------- answer --------------
  @RequestMapping("/answer/list")
  public ResponseObject<Map<String, Object>> listAnswer(
      String questionId, @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {

    if (StringUtils.isBlank(questionId)) {
      questionId = null;
    }
    List<HealthTestAnswer> list;
    Long total;
    try {
      list = answerService.list(questionId, order, Sort.Direction.fromString(direction), pageable);
      total = answerService.countByQuestionId(questionId);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "答案加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/answer/listByGroup")
  public ResponseObject<Map<String, Object>> listByGroup(
      @RequestParam("groupId") String groupId,
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      Pageable pageable) {
    if (StringUtils.isBlank(groupId)) {
      groupId = null;
    }
    List<HealthTestAnswer> answerList = new ArrayList<>();
    Long total = 0L;
    if (groupId != null) {
      try {
        answerList = answerService
            .listByGroup(groupId, order, Sort.Direction.fromString(direction), pageable);
        total = answerService.countByGroupId(groupId);
      } catch (BizException e) {
        throw e;
      } catch (Exception e) {
        String msg = "加载答案出错";
        log.error(msg, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
      }
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", answerList);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/answer/listGroup")
  public ResponseObject<Map<String, Object>> listGroup() {
    List<HealthTestAnswerGroup> groupList;
    Long groupTotal;
    try {
      HealthTestAnswerGroupExample example = new HealthTestAnswerGroupExample();
      example.or()
          .andShareEqualTo(true);
      groupList = answerService.listGroup(example);
      groupTotal = answerService.countGroup(example);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取答案模板出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", groupList);
    result.put("total", groupTotal);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/answer/save")
  public ResponseObject<String> saveAnswer(@RequestParam("content") String content,
      String groupId) {
    String[] contents = StringUtils.split(content, ",");
    if (contents == null || contents.length == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "答案不能为空");
    }
    HealthTestAnswerGroup group;
    try {
      if (StringUtils.isBlank(groupId)) {
        group = new HealthTestAnswerGroup();
        group.setShare(false);
        answerService.saveGroup(group);
        groupId = IdTypeHandler.encode(Long.valueOf(group.getId()));
      }
      for (String c : contents) {
        HealthTestAnswer answer = new HealthTestAnswer();
        answer.setContent(c);
        answer.setGroupId(groupId);
        Integer alias = answerService.countAlias(groupId);
        answer.setAlias(++alias);
        answerService.save(answer);
      }
    } catch (Exception e) {
      String msg = "答案保存失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(groupId);
  }

  @RequestMapping("/answer/delete")
  public ResponseObject<Boolean> deleteAnswer(@RequestParam("id") String id) {
    boolean result = answerService.delete(id);
    return new ResponseObject<>(result);
  }

  // -------- answer end --------------

  // -------- physique --------------

  @RequestMapping("/physique/save")
  public ResponseObject<Boolean> save(HealthTestPhysique physique) {
    HealthTestPhysique exists = physiqueService.loadByName(physique.getName());
    if (exists != null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "体质名称已存在");
    }
    boolean saveResult;
    try {
      saveResult = physiqueService.save(physique);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "体质保存失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(saveResult);
  }

  @RequestMapping("/physique/list")
  public ResponseObject<Map<String, Object>> listPhysique(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<HealthTestPhysique> physiqueList;
    Long count;
    try {
      physiqueList = physiqueService.list(order, Sort.Direction.fromString(direction), pageable);
      count = physiqueService.count();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "数据加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", physiqueList);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  // -------- physique end --------------

  // -------- profile --------------

  @RequestMapping("/profile/hasProfile")
  public ResponseObject<Boolean> hasProfile() {
    IUser user = getCurrentIUser();
    String userId = user.getId();
    HealthTestProfile profile = profileService.loadByUser(userId);
    return new ResponseObject<>(profile != null);
  }

  @RequestMapping(value = "/profile/upload")
  public ResponseObject<HealthTestProfileVO> uploadProfile(HealthTestProfile profile) {
    Integer weight = profile.getWeight();
    Integer height = profile.getHeight();
    // 不允许通过接口修改BMI
    double bmi;
    if (weight == null || weight == 0 || height == null || height == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "信息填写不正确");
    }
    IUser user = getCurrentIUser();
    String userId = user.getId();
    profile.setUserId(userId);
    if (profile.getBmi() != null) {
      profile.setBmi(null);
    }
    try {
      double meterHeight = height / 100.0;
      double bmiValue = weight / (meterHeight * meterHeight);
      BigDecimal bigDecimal = new BigDecimal(bmiValue);
      bmi = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
      profile.setBmi(bmi);
      HealthTestProfile profileExists = profileService.loadByUser(userId);
      if (profileExists == null) {
        profileService.save(profile);
      } else {
        profile.setId(profileExists.getId());
        profileService.update(profile);
      }
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "信息有误");
    }
    return new ResponseObject<>(new HealthTestProfileVO(profile));
  }

  @RequestMapping("/profile/load")
  public ResponseObject<HealthTestProfile> loadProfile() {
    IUser user = getCurrentIUser();
    String userId = user.getId();
    HealthTestProfile profile = profileService.loadByUser(userId);
    return new ResponseObject<>(profile);
  }

  @RequestMapping("/profile/update")
  public ResponseObject<Boolean> updateProfile(HealthTestProfile profile) {
    IUser user = getCurrentIUser();
    String userId = user.getId();
    HealthTestProfile profileExists = profileService.load(userId);
    if (profileExists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户信息不存在");
    }
    profile.setId(profileExists.getId());
    boolean result;
    try {
      result = profileService.update(profile);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "更新用户信息出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<>(result);
  }

  // -------- profile end --------------
  // -------- result --------------

  @RequestMapping("/result/delete")
  public ResponseObject<Boolean> deleteResult(@RequestParam("id") String id) {
    boolean result = resultService.delete(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/result/addProduct")
  public ResponseObject<Boolean> addBenefitProduct(HealthTestResultProduct resultProduct) {
    boolean result = resultService.bindProduct(resultProduct);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/result/removeProduct")
  public ResponseObject<Boolean> removeBenefitProduct(HealthTestResultProduct resultProduct) {
    boolean result = resultService.unbindProduct(resultProduct);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/result/listProduct")
  public ResponseObject<Map<String, Object>> listProducts(String resultId,
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<Product> products;
    Long count;
    if (StringUtils.isBlank(resultId)) {
      resultId = null;
    }
    try {
      products = resultService
          .listProductsByResultId(resultId, order, Sort.Direction.fromString(direction), pageable);
      count = resultService.countProductsByResultId(resultId);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "商品加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", products);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  // -------- result end --------------

  @Autowired
  public void setModuleService(HealthTestModuleService moduleService) {
    this.moduleService = moduleService;
  }

  @Autowired
  public void setQuestionService(HealthTestQuestionService questionService) {
    this.questionService = questionService;
  }

  @Autowired
  public void setAnswerService(HealthTestAnswerService answerService) {
    this.answerService = answerService;
  }

  @Autowired
  public void setPhysiqueService(HealthTestPhysiqueService physiqueService) {
    this.physiqueService = physiqueService;
  }

  @Autowired
  public void setProfileService(HealthTestProfileService profileService) {
    this.profileService = profileService;
  }

  @Autowired
  public void setResultService(HealthTestResultService resultService) {
    this.resultService = resultService;
  }
}
