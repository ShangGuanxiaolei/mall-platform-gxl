package com.xquark.restapi.reserve;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author: yyc
 * @date: 19-4-18 下午3:16
 */
public class ReserveProductForm {

  /** 优惠活动id */
  @NotBlank private String promotionId;

  /** 类目名称 */
  @NotBlank private String categoryName;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }
}
