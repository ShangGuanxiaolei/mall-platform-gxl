package com.xquark.restapi.yundou;

import static com.xquark.dal.consts.SignConstants.DEFAULT_STEP;
import static com.xquark.dal.consts.SignConstants.MAX_SIGN_DAYS;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.SignIn;
import com.xquark.dal.model.SignInRule;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.dal.vo.SignInVO;
import com.xquark.helper.Transformer;
import com.xquark.helper.Generator;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.promotion.PromotionProductServiceFactory;
import com.xquark.service.signin.DayGenerator;
import com.xquark.service.signin.SignInService;
import com.xquark.service.yundou.OldPointDeductionResult;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.YundouAmountService;
import com.xquark.service.yundou.YundouDeductionResultVO;
import com.xquark.service.yundou.YundouOperationService;
import com.xquark.service.yundou.impl.PointDeductionService;
import com.xquark.utils.DateUtils;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-5-11. DESC:
 */
@RestController
@RequestMapping("/yundou")
@Api(value = "用户积分, 签到接口")
public class YundouAmountController extends BaseController {

  private static final Logger logger = LoggerFactory.getLogger(YundouAmountController.class);

  private YundouOperationService operationService;

  private SignInService signInService;

  private YundouAmountService yundouAmountService;

  private PointDeductionService pointDeductionService;

  private ProductService productService;

  @Autowired
  public void setOperationService(YundouOperationService operationService) {
    this.operationService = operationService;
  }

  @Autowired
  public void setSignInService(SignInService signInService) {
    this.signInService = signInService;
  }

  @Autowired
  public void setYundouAmountService(YundouAmountService yundouAmountService) {
    this.yundouAmountService = yundouAmountService;
  }

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Autowired
  public void setPointDeductionService(
      PointDeductionService pointDeductionService) {
    this.pointDeductionService = pointDeductionService;
  }

  @ApiOperation(value = "根据商品id与skuId以及当前用户计算购买时所需积分以及抵扣额")
  @RequestMapping("/calDeduction")
  public ResponseObject<YundouDeductionResultVO> calaulateDeduction(
      @RequestParam(value = "productId", required = false) String productId,
      @RequestParam("skuId") String skuId,
      @RequestParam("amount") int amount,
      @RequestParam(value = "promotionProductId", required = false) String promotionProductId,
      @RequestParam(value = "promotionFrom", required = false) PromotionType type) {

    PromotionProductVO promotionProduct = null;
    Product product;
    Sku sku;
    // 如果活动商品存在，则价格按活动商品计算
    if (StringUtils.isNotBlank(promotionProductId) && type != null) {
      promotionProduct = PromotionProductServiceFactory
          .loadPromotionProduct(type, promotionProductId);
      if (promotionProduct != null) {
        if (StringUtils.isBlank(productId)) {
          productId = promotionProduct.getProduct().getId();
        }
      }
    }

    product = productService.load(productId);
    sku = productService.loadSku(productId, skuId);
    if (product == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "该商品不存在");
    }
    if (sku == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "该SKU不存在");
    }

    // 根据活动设置sku价格
    if (promotionProduct != null) {
      sku.setPrice(promotionProduct.getPromotionPrice());
      sku.setMarketPrice(promotionProduct.getPromotionPrice());
    }

    IUser user = getCurrentIUser();
    if (!(user instanceof User)) {
      throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "用户状态不正确");
    }
    OldPointDeductionResult result = pointDeductionService
        .calculateDeduction(product, sku, amount,
            userService.load(user.getId()));
    return new ResponseObject<>(
        new YundouDeductionResultVO(result, amount));
  }

  /**
   * 云豆增减接口
   *
   * @param operationCode 操作代码
   * @param id 业务id
   */
  @RequestMapping("/amount/modify")
  @ApiOperation(value = "修改用户积分", notes = "根据传入的操作代码以及业务id修改用户及分数", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<List<Result>> modifyYundou(@RequestParam("code") Integer operationCode,
      @RequestParam("id") String id) {
    YundouOperation operation = operationService.loadByCode(operationCode);
    if (operation == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "操作代码不合法");
    }
    if (!operation.getEnable()) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该操作已被禁用");
    }
    List<Result> results;
    try {
      results = yundouAmountService.modifyAmount(operation, id);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      logger.error("修改云豆数量错误", e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改云豆数量错误");
    }
    return new ResponseObject<>(results);
  }

  @RequestMapping("/signIn/rule/list")
  @ApiOperation(value = "获取签到规则接口", notes = "签到规则全局仅有一条记录", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Map<String, Object>> listRule() {
    SignInRule rule = signInService.loadSignInRule();
    // 数据库中未配置时使用默认配置
    assert rule != null;
    List<SignInRule> rules = Collections.singletonList(rule);
    Map<String, Object> result = new HashMap<>();
    result.put("list", rules);
    result.put("total", rules.size());
    return new ResponseObject<>(result);
  }

  @RequestMapping("/signIn/rule/save")
  @ApiOperation(value = "保存签到规则", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Boolean> saveRule(@Validated SignInRuleForm form, Errors errors) {
    ControllerHelper.checkException(errors);
    SignInRule rule = Transformer.fromBean(form, SignInRule.class);
    boolean result = signInService.saveOrUpdateRule(rule);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/signIn")
  @ApiOperation(value = "用户签到接口", notes = "用户登录状调用时为当前用户签到", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<SignInVO> signIn() {
    IUser user = getCurrentIUser();
    if (!(user instanceof User)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户类型不正确");
    }
    String userId = user.getId();
    SignIn newSignIn = new SignIn(userId);
    SignIn lastSigIn = signInService.loadLastModifiedByUser(userId);

    if (lastSigIn == null) {
      // 没有签到过, 签到数+1
      newSignIn.setSignCount(1L);
      newSignIn.setSignSum(1L);
    } else {
      // 总签到天数
      Long signInSum = lastSigIn.getSignSum();
      // 连续签到天数
      Long signInCount = lastSigIn.getSignCount();

      // 最后签到日期
      Date lastSignInDate = lastSigIn.getCreatedAt();
      if (DateUtils.isToday(lastSignInDate)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您今天已经签到过，不能重复签到哦");
      }

      Date yesterday = this.getYesterday();
      if (DateUtils.isBeforeDay(lastSignInDate, yesterday)) {
        // 最后一次签到在昨天之前, 连续签到归0
        signInCount = 0L;
      }
      // user-getInfo处同步修改
      // 七天一次循环
      if (signInCount == 7L) {
        signInCount = 0L;
      }
      newSignIn.setSignCount(++signInCount);
      newSignIn.setSignSum(++signInSum);
    }
    SignInVO signInVO = signInService.signIn(newSignIn);
    return new ResponseObject<>(signInVO);
  }

  @RequestMapping("/signIn/rule/show")
  @ApiOperation(value = "根据id查看签到规则", notes = "id非必传，默认返回当前默认规则")
  public ResponseObject<SignInRule> showRule(String id) {
    SignInRule rule;
    if (StringUtils.isBlank(id)) {
      rule = signInService.loadSignInRule();
    } else {
      rule = signInService.loadSignInRule(id);
    }
    return new ResponseObject<>(rule);
  }

  @RequestMapping("/signIn/rule/delete")
  @ApiOperation(value = "根据id删除签到规则")
  public ResponseObject<Boolean> deleteRule(@RequestParam("id") String id) {
    boolean result = signInService.deleteSignInRule(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/signIn/view")
  @ApiOperation(value = "获取签到进度接口", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<SignInResultWrapper> signinPageView() {

    final IUser user = getCurrentIUser();
    String userId = user.getId();

    Generator<Long> signInGenerator = signInService.getGenerator();
    List<Long> signInArray = Arrays.asList(signInGenerator.limit(MAX_SIGN_DAYS));

    // 获取最后一个签到周期的第一天
    // 用户已经签到过且当前天数没有在周期数之后，则签到的第一天为该天
    // 若有日期在最后一次签到之前的都为已签到
    SignIn lastCycleBegan = signInService.loadLastCycleBegin(userId);
    // 最后一次签到
    SignIn lastSignIn = signInService.loadLastModifiedByUser(userId);
    final Date lastSignInDate = lastSignIn == null ? null : lastSignIn.getCreatedAt();
    final Date lastCycleBeginDate =
        lastCycleBegan == null ? null : lastCycleBegan.getCreatedAt();

    // 当前签到的第一天
    Date today = new Date();
    boolean needRestart = needRestart(lastCycleBeginDate, lastSignInDate, today);
    Date dateBegin = needRestart ? today : lastCycleBeginDate;

    final Generator<Date> dateGenerator = new DayGenerator(dateBegin);
    // 通过生成器生成日期 - 积分键值对列表
    List<SignInDateEntry> dateEntries = Transformer.fromIterable(signInArray,
        new Function<Long, SignInDateEntry>() {
          @Override
          public SignInDateEntry apply(Long aLong) {
            Date next = dateGenerator.next();
            // 在最后一次签到日期或最后一次签到前都为已经签到过
            boolean isSignIn =
                lastSignInDate != null && next.compareTo(lastSignInDate) <= 0;
            return new SignInDateEntry(next, aLong, isSignIn);
          }
        });
    SignInDateEntry todayEntry = CollectionUtils.find(dateEntries,
        new Predicate<SignInDateEntry>() {
          @Override
          public boolean evaluate(SignInDateEntry object) {
            return object.getIsToday();
          }
        });
    long todayPoints = todayEntry.getSignInPoints();

    // 获取全局配置
    SignInRule globalRule = signInService.loadSignInRule();
    int step = Optional.fromNullable(globalRule
        .getOperationNumber()).or(DEFAULT_STEP);
    int init = globalRule.getInit().intValue();
    // 明日签到加积分数
    long tomorrowPoints = todayPoints == MAX_SIGN_DAYS ? init : (todayPoints + step);
    long signCount = lastSignIn != null ? lastSignIn.getSignCount() : 0;

    return new ResponseObject<>(
        new SignInResultWrapper(dateEntries, todayPoints, tomorrowPoints, signCount));
  }

  private Date getYesterday() {
    final Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, -1);
    return cal.getTime();
  }

  /**
   * 判断签单日期是否需要重新计数 需重新计算的条件: 1. 已经断签 2. 签到日期已经为下个周期的第一天 3. 没有签到过
   *
   * @param lastCycleBegin 用户最后一个签到周期的第一天
   * @param lastSignIn 用户最后一次签到
   * @param today 今天
   * @return true or false
   */
  private boolean needRestart(Date lastCycleBegin, Date lastSignIn, Date today) {
    // 签到日期大于最后一次签到的后一天表示断签
    boolean isBreak = lastSignIn != null && DateUtils
        .isAfterDay(today, DateUtils.getNextDayStart(lastSignIn));
    // 已经超过循环
    boolean isCycleEnd =
        lastCycleBegin != null && DateUtils.getDifferenceDays(today, lastCycleBegin) >= 6;
    return lastCycleBegin == null || isBreak || isCycleEnd;
  }
}
