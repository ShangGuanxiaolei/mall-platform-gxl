package com.xquark.restapi.order;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.aop.anno.NotNull;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.ServMessageMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.status.OrderRefundWarehouseCheckStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ServMessageStatus;
import com.xquark.dal.type.OrderRefundActionType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.validation.group.order.ApplyForChangeGroup;
import com.xquark.dal.validation.group.order.ApplyForRefundGroup;
import com.xquark.dal.validation.group.order.ApplyForReissueGroup;
import com.xquark.dal.validation.group.order.ProceedRefundGroup;
import com.xquark.dal.vo.OrderRefundVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.activityPtOutTime.PtlSelectOutTimeService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.vo.OrderChangeRequestForm;
import com.xquark.service.order.vo.OrderRefundRequestForm;
import com.xquark.service.order.vo.OrderReissueRequestForm;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.product.ProductService;
import com.xquark.service.push.PushService;
import com.xquark.service.user.UserService;
import com.xquark.utils.MD5Util;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@ApiIgnore
@Validated
public class OrderRefundController extends BaseController {

  @Autowired
  private OrderRefundService orderRefundService;

  private OrderHeaderService orderHeaderService;

  private UserService userService;
  @Autowired
  private FreshManService freshManService;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setOrderHeaderService(OrderHeaderService orderHeaderService) {
    this.orderHeaderService = orderHeaderService;
  }

  @Value("${sms.ali.template.refund}")
  private String ALI_SMS_REFUND_TEMPLATE;

  private PushService pushService;

  @Autowired
  public void setPushService(PushService pushService) {
    this.pushService = pushService;
  }

  @Autowired
  private OrderService orderService;

  @Autowired
  private PtlSelectOutTimeService ptSelectOutTimeService;

  @Autowired
  private ServMessageMapper servMessageMapper;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ProductService productService;

  private static final String REFUND_REASON = "未收到货";

  /**
   * 我的退货请求列表
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/list")
  public ResponseObject<List<OrderRefundVO>> list(Pageable pageable) {
    List<OrderRefundVO> vos = orderRefundService.list(getCurrentIUser().getId(), pageable);
    return new ResponseObject<>(vos);
  }

  /**
   * 检查退货申请是否是最新
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/checkRequestData")
  public ResponseObject<Boolean> checkRequestData(String requestData, String id) {
    return new ResponseObject<>(validation(requestData, id));
  }


  /***
   * 取消退款
   * @param orderId
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/cancel")
  public ResponseObject<Boolean> cancelRefund(@RequestParam String orderId) {
    cancelAndRejectRefund(orderId);
    List<OrderRefund> list = orderRefundService.listByOrderId(orderId);
    Order order = orderService.load(orderId);
    if (list != null && list.size() > 0) {
      OrderRefund refund = list.get(0);
      orderRefundService.executeBySystem(OrderRefundActionType.CANCEL, refund, Collections.
              <String, Object>emptyMap());
      //更新中台订单表的状态
      if(order != null && order.getStatus() == OrderStatus.REFUNDING){
        orderHeaderService.updateStatusByOrderNo(Integer.toString(7), order.getOrderNo());
        //new日期对象
        Date date = new Date(System.currentTimeMillis());
        //转换提日期输出格式
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        String dateStr = dateFormat.format(date).replace("-", "");
        //更新订单月
        orderHeaderService.updateOtherMonthByOrderNo(Integer.parseInt(dateStr), order.getOrderNo());
      }

    } else {
      return new ResponseObject<>(false);
    }

    return new ResponseObject<>(true);
  }

  /**
   * 同意退货退款
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/confirm")
  public ResponseObject<OrderRefundVO> confirm(OrderRefundConfirmForm form) {
    log.info("begin /order/refund/confirm");

    if (!validation(form.getRequestData(), form.getId())) {
      log.debug("申请退货退款的申请[ " + form.getId() + " ]已经被修改");
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT_2, "买家已经修改了退款申请，请返回查看");
    }

    // 获取订单退货信息
    OrderRefundVO refundInfo = orderRefundService.loadVO(form.getId());
    if (OrderRefundStatus.SUBMITTED != refundInfo.getStatus()) {
      log.debug(
              "申请退货退款的申请[ " + form.getId() + " ]还未提交 refund status=[" + refundInfo.getStatus() + "]");
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "退款申请买家未提交");
    }

    Map<String, Object> params = null;
    OrderRefundActionType action;
    if ("1".equals(form.getAgree())) {
      // 同意订单申请
      if (refundInfo.getBuyerRequire() == 2) {
        // 买家要求是 退货并退款
        if (StringUtils.isBlank(form.getReturnAddress()) || StringUtils
                .isBlank(form.getReturnName())
                || StringUtils.isBlank(form.getReturnPhone())) {
          log.debug("申请退货退款的申请[ " + form.getId() + " ]的收货人信息不完整");
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "收货人姓名、联系方式、退货地址不能为空");
        }
      }

      refundInfo.setReturnAddress(form.getReturnAddress());
      refundInfo.setReturnName(form.getReturnName());
      refundInfo.setReturnPhone(form.getReturnPhone());
      refundInfo.setReturnMemo(form.getReturnMemo());
      action = OrderRefundActionType.CONFIRM;
    } else {
      // 拒绝申请
      if (StringUtils.isBlank(form.getRefuseReason()) || StringUtils.isBlank(form.getRefuseDetail())
              || form.getRefuseEvidences() == null) {
        log.debug("申请退货退款的申请[ " + form.getId() + " ]的拒绝申请的理由不完整");
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拒绝原因、拒绝说明、凭证都不能为空");
      }

      refundInfo.setStatus(OrderRefundStatus.REJECT_REFUND);
      refundInfo.setRefuseReason(form.getRefuseReason());
      refundInfo.setRefuseDetail(form.getRefuseDetail());
      refundInfo.setRefuseEvidences(form.getRefuseEvidences());
      action = OrderRefundActionType.REJECT;

      params = new HashMap<>();
      params.put("attachs", form.getRefuseEvidences());
    }

    orderRefundService.execute(action, refundInfo, params);

    refundInfo = orderRefundService.loadVO(refundInfo.getId());
    refundInfo.setOpDetails(orderRefundService.initOpDetail(refundInfo, "3"));
    return new ResponseObject<>(refundInfo);
  }

  /**
   * 被拒绝的退货申请标记成功
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/confirmSign")
  public ResponseObject<Boolean> confirmSign(OrderRefundConfirmForm form) {
    // 获取申请
    OrderRefundVO refundVO = orderRefundService.loadVO(form.getId());

    // 申请未被拒绝
    if (refundVO.getStatus() != OrderRefundStatus.RETURN_ING) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "退款状态有误");
    }

    OrderRefundActionType action;
    if (form.getAgree().equals("1")) {
      action = OrderRefundActionType.SIGN;
    } else {
      action = OrderRefundActionType.REJECT_SIGN;
    }

    orderRefundService.execute(action, refundVO, null);
    return new ResponseObject<>(true);
  }

  /**
   * 退款申请
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/apply", method = RequestMethod.POST)
  public ResponseObject<Boolean> applyForRefund(
          @RequestBody @Validated(ApplyForRefundGroup.class) OrderRefundRequestForm form) {
    //检查订单是否已经提交过申请
    if (orderRefundService.listByOrderId(form.getOrderId()).size() > 0) {
      GlobalErrorCode code = GlobalErrorCode.REPEAT_APPLY_REFUND;
      return new ResponseObject<>(false, code.getError(), code);
    }
    orderRefundService.applyForRefund(form);
    return new ResponseObject<>(true, GlobalErrorCode.SUCESS);
  }

  /**
   * 换货申请
   * @param form
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/change/apply", method = RequestMethod.POST)
  public ResponseObject<Boolean> applyForChange(
          @RequestBody @Validated(ApplyForChangeGroup.class) OrderChangeRequestForm form) {
    GlobalErrorCode code;
    //检查订单是否已经提交过申请
    if (orderRefundService.listByOrderId(form.getOrderId()).size() > 0) {
      code = GlobalErrorCode.REPEAT_APPLY_CHANGE;
      return new ResponseObject<>(false, code.getError(), code);
    }
    if(orderRefundService.applyForChange(form)){
      return new ResponseObject<>(true, "换货申请成功", GlobalErrorCode.SUCESS);
    }else{
      code = GlobalErrorCode.APPLY_CHANGE_FAIL;
      return new ResponseObject<>(false, code.getError(), code);
    }
  }

  /**
   * 换货申请(后台)
   * @param orderId
   * @param changeReason
   * @param orderItems
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/change/adminApply", method = RequestMethod.POST)
  public ResponseObject<Boolean> applyChangeForAdmin(String orderId, String changeReason, String orderItems) {
    //检查订单是否已经提交过申请
    if (orderRefundService.listByOrderId(orderId).size() > 0) {
      GlobalErrorCode code = GlobalErrorCode.REPEAT_APPLY_CHANGE;
      return new ResponseObject<>(false, code.getError(), code);
    }
    orderRefundService.applyChangeForAdmin(orderId, changeReason, orderItems);
    return new ResponseObject<>(true, GlobalErrorCode.SUCESS);
  }

  /**
   * 补货申请
   *
   * @param form
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/reissue/apply", method = RequestMethod.POST)
  public ResponseObject<Boolean> applyForReissue(
          @RequestBody @Validated(ApplyForReissueGroup.class) OrderReissueRequestForm form) {
    //检查订单是否已经提交过申请
    if (orderRefundService.listByOrderId(form.getOrderId()).size() > 0) {
      GlobalErrorCode code = GlobalErrorCode.REPEAT_APPLY_REISSUE;
      return new ResponseObject<>(false, code.getError(), code);
    }
    if(orderRefundService.applyReissue(form)){
      return new ResponseObject<>(true, GlobalErrorCode.SUCESS);
    }else{
      return new ResponseObject<>(false, "申请补货失败", GlobalErrorCode.INTERNAL_ERROR);
    }
  }

  /**
   * 换货下单
   * @param orderId
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/change/confirm")
  public ResponseObject<OrderRefundVO> confirmChange(String orderId) {
    //调用服务层换货
    orderService.confirmChange(orderId);
    //下单成功，删除原订单号对应的换货商品记录
    // TODO 拒绝补, 换货订单走该逻辑
//    cancelAndRejectRefund(orderId);
    //发送消息
    try {
      ServMessage(ServMessageStatus.REPLACEOUT.getWord(orderService.load(orderId).getOrderNo()),orderService.load(orderId).getOrderNo());
    }catch (Exception e){
      e.printStackTrace();
      log.error("订单{}退款发送失败",orderService.load(orderId).getOrderNo());
    }
    return new ResponseObject<>();
  }

  /**
   * 换货下单(后台申请售后)
   * @param orderId
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/change/submit")
  public ResponseObject<OrderRefundVO> submitChange(String orderId, String productIdAndAmount) {
    //调用服务层换货
    orderService.submitChange(orderId, productIdAndAmount);
    //下单成功，删除原订单号对应的换货商品记录
    // TODO 拒绝补, 换货订单走该逻辑
//    cancelAndRejectRefund(orderId);

    return new ResponseObject<>();
  }

  /**
   * 退货订单详情
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/detail")
  public ResponseObject<OrderRefundVOEx> detail(@RequestParam("id") String id) {
    OrderRefundVO refundVO = orderRefundService.loadVO(id);
    OrderRefundVOEx voEx = new OrderRefundVOEx(refundVO);
    voEx.setRequestData(MD5Util.MD5Encode(refundVO.toString(), "UTF-8"));
    voEx.setOpDetails(orderRefundService.initOpDetail(refundVO, "3"));
    return new ResponseObject<>(voEx);
  }

  /**
   * 更新退款的仓库审核状态,换货业务也需要调用此接口，在确认货物入仓后，客服需要帮助用户在后台下换货订单
   */
  @RequestMapping(value = "/order/refund/warehouse/status/{id} ", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> updateRefundWmsCheckStatus(
          @RequestParam("status") OrderRefundStatus status, @PathVariable("id") String id,
          @NotNull @RequestParam OrderRefundWarehouseCheckStatus checkStatus,
          @NotBlank @RequestParam String remark) {
    Boolean ret = orderRefundService
            .updateStatusByOrderId(id, status, ImmutableMap.<String, Object>of(
                    "wmsCheckStatus", checkStatus,
                    "wmsCheckRemark", remark
            ));
    orderHeaderService.updateStatusByOrderNo(Integer.toString(10), orderService.load(id).getOrderNo());
    return new ResponseObject<>(ret);
  }

  /**
   * 更新退款申请的审核状态
   */
  @RequestMapping(value = "/order/refund/status/{id} ", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> updateRefundApplyStatus(
          @RequestParam("status") OrderRefundStatus status, @PathVariable("id") String id) {
    Boolean ret = true;
    Optional<Order> order = Optional.fromNullable(orderService.load(id));
    if (!order.isPresent()) {
      return new ResponseObject<>("订单不存在", GlobalErrorCode.NOT_FOUND);
    }

    List<OrderRefund> list = orderRefundService.listByOrderId(id);
    OrderRefund refund;
    if (list != null && list.size() > 0) {
      refund = list.get(0);
    }else{
      return new ResponseObject<>("订单不存在", GlobalErrorCode.NOT_FOUND);
    }

    //拒绝退货
    if(status.equals(OrderRefundStatus.REJECT_REFUND)) {
      orderRefundService.executeBySystem(OrderRefundActionType.CANCEL, refund, Collections.
              <String, Object>emptyMap());
      //修改中台订单状态为退货取消
      ret = orderHeaderService.updateStatusByOrderNo(Integer.toString(7), order.get().getOrderNo()) > 0;

      //拒绝退货需要判断发放新人得分
      freshManService.freshManGrantPoint(order.get());

      //发送消息
      try {
        ServMessage(ServMessageStatus.REFUNDAPPLICATIONFAIL.getWord(order.get().getOrderNo()), order.get().getOrderNo());
      }catch (Exception e){
        e.printStackTrace();
        log.error("订单{}退货消息发送失败",order.get().getOrderNo());
      }
      return new ResponseObject<>(ret);
    }

    //拒绝换货、补货
    if (status.equals(OrderRefundStatus.REJECT_CHANGE) || status.equals(OrderRefundStatus.REJECT_REISSUE)) {
      orderRefundService.executeBySystem(OrderRefundActionType.CANCEL, refund, Collections.
              <String, Object>emptyMap());
      cancelAndRejectRefund(id);//取消或拒绝申请处理
      //更新中台订单状态为退货取消
      ret = orderHeaderService.updateStatusByOrderNo(Integer.toString(7), order.get().getOrderNo()) > 0;

      if(status.equals(OrderRefundStatus.REJECT_CHANGE)) {
        //发送换货拒绝消息
        try {
          ServMessage(ServMessageStatus.REPLACEAPPLICATIONFAIL.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        } catch (Exception e) {
          e.printStackTrace();
          log.error("订单{}换货消息发送失败", order.get().getOrderNo());
        }
      }else if(status.equals(OrderRefundStatus.REJECT_REISSUE)){
        //发送补货拒绝消息
        try {
          ServMessage(ServMessageStatus.REISSUEFAIL.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        } catch (Exception e) {
          e.printStackTrace();
          log.error("订单{}补货消息发送失败", order.get().getOrderNo());
        }
      }
      return new ResponseObject<>(ret);
    }

    //同意用户的申请
    ret = orderRefundService.updateStatusByOrderId(id, status, null);//根据传入的 status 修改售后订单状态

    if(status.equals(OrderRefundStatus.WMS_CHECKED)){//后台传入的状态为wms已审核，表示换货不需要执行退货入仓
      ret = orderRefundService.updateStatusByOrderId(id, status, ImmutableMap.<String, Object>of(
              "wmsCheckStatus", "CHECKED_PASS",
              "wmsCheckRemark", "换货请求，客服审核无须退货入仓"
      ));
      //发送换货通过审核消息(不需要入仓)

      try {
        ServMessage(ServMessageStatus.REPLACEAPPLICATIONPASSFAIL.getWord(order.get().getOrderNo()), order.get().getOrderNo());
      }catch (Exception e){
        e.printStackTrace();
        log.error("订单{}换货审核消息发送失败",order.get().getOrderNo());
      }
      return new ResponseObject<>(ret);//直接返回不需要发送短信通知用户
    }

    if (status.equals(OrderRefundStatus.ACCEPT_REFUND)) {//同意退货修改中台订单状态
      orderHeaderService.updateStatusByOrderNo(Integer.toString(8), order.get().getOrderNo());

      //同意退款 处理新人礼单扣除发给上级的德分和收益
      freshManService.frenshManDeductUplinePointAndCommission(order.get());

      //发送消息
      //未收到货
      if(REFUND_REASON.equals(refund.getRefundReason())){
        try {
          ServMessage(ServMessageStatus.REFUNDAPPLICATIONPASSFAIL.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        } catch (Exception e) {
          e.printStackTrace();
          log.error("订单{}同意退款消息发送失败", order.get().getOrderNo());
        }
      }else {
        try {
          ServMessage(ServMessageStatus.REFUNDAPPLICATIONPASS.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        } catch (Exception e) {
          e.printStackTrace();
          log.error("订单{}同意退款消息发送失败", order.get().getOrderNo());
        }
      }
    }

    //如果是"未收到货"的理由，不发送退货短信通知
    if (REFUND_REASON.equals(refund.getRefundReason())) {
      orderRefundService.updateStatusByOrderId(id, status, ImmutableMap.<String, Object>of(
              "wmsCheckStatus", "CHECKED_PASS",
              "wmsCheckRemark", "买家未收到货"
      ));
      //未收到货，修改中台订单状态为外仓确认
      orderHeaderService.updateStatusByOrderNo(Integer.toString(10), orderService.load(id).getOrderNo());
      refund.setStatus(OrderRefundStatus.WMS_CHECKED);
      orderRefundService.update(refund);
      log.info("用户id：" + order.get().getBuyerId() + " 选择的退货理由为未收到货，不发送短信。");
      //发送换货通过审核消息
      if (!status.equals(OrderRefundStatus.ACCEPT_REFUND) && status.equals(OrderRefundStatus.ACCEPT_CHANGE)){

        try {
          ServMessage(ServMessageStatus.REPLACEAPPLICATIONPASS.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        }catch (Exception e){
          e.printStackTrace();
          log.error("订单{}换货审核消息发送失败",order.get().getOrderNo());
        }
      }
      //发送补货通过审核消息
      if (!status.equals(OrderRefundStatus.ACCEPT_REFUND) && status.equals(OrderRefundStatus.ACCEPT_REISSUE)){
        //发送消息
        try {
          ServMessage(ServMessageStatus.REISSUEPASS.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        }catch (Exception e){
          e.printStackTrace();
          log.error("订单{}补货审核消息发送失败",order.get().getOrderNo());
        }
      }
    } else {
      pushService.sendSmsEngine(userService.load(order.get().getBuyerId()).getPhone(), "",
              UserPartnerType.ALISMS, "", ALI_SMS_REFUND_TEMPLATE);
      log.info("给用户(用户id)：" + order.get().getBuyerId() + " 发送退货短信=================");
      //发送换货通过审核消息
      if (!status.equals(OrderRefundStatus.ACCEPT_REFUND) && status.equals(OrderRefundStatus.ACCEPT_CHANGE)){

        try {
          ServMessage(ServMessageStatus.REPLACEAPPLICATIONPASS.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        }catch (Exception e){
          e.printStackTrace();
          log.error("订单{}换货审核消息发送失败",order.get().getOrderNo());
        }
      }
      //发送补货通过审核消息
      if (!status.equals(OrderRefundStatus.ACCEPT_REFUND) && status.equals(OrderRefundStatus.ACCEPT_REISSUE)){
        //发送消息
        try {
          ServMessage(ServMessageStatus.REISSUEPASS.getWord(order.get().getOrderNo()), order.get().getOrderNo());
        }catch (Exception e){
          e.printStackTrace();
          log.error("订单{}补货审核消息发送失败",order.get().getOrderNo());
        }
      }
    }


    return new ResponseObject<>(ret);
  }

  /**
   * 补货确认修改订单状态
   *
   * @param orderNo
   * @param orderId
   * @return
   */
  @RequestMapping(value = "/update/order/status")
  @ResponseBody
  public ResponseObject<Boolean> updateOrderStatus(@RequestParam String orderNo, @RequestParam String orderId) {
    Boolean ret;
    Optional<OrderVO> order = Optional.fromNullable(orderService.loadByOrderNo(orderNo));
    if (!order.isPresent()) {
      return new ResponseObject<>("订单不存在", GlobalErrorCode.NOT_FOUND);
    }
/*
    OrderRefund orderRefund = orderRefundService.loadOrderRefund(orderId);
    List<OrderItem> orderItemList = orderItemMapper.selectByOrderIdAndPT(orderId);//先获取订单商品信息
    for (OrderItem orderItem :
            orderItemList) {//对订单商品进行遍历
      OrderItem item = orderItemMapper.selectByPrimaryKey(orderItem.getId());
      productService.updateSkuStock(item.getSkuId(), item.getAmount());//补货出仓扣减库存
      productService.updateAmountForRefund(item.getProductId(), item.getAmount());
    }
    orderRefundService.updateStatusByOrderId(orderId, OrderRefundStatus.SUCCESS, null);//修改售后订单为success
    if(orderRefund != null && orderRefund.getOrderStatus() != null){
      ret = orderService.updateStatusByOrderNo(orderRefund.getOrderStatus(), orderNo);
    }else{
      ret = orderService.updateStatusByOrderNo(OrderStatus.SUCCESS, orderNo);
    }*/

    //新增补货下单逻辑
    ret = orderService.confirmReissue(orderId);

    return new ResponseObject<>(ret);
  }

  /**
   * 买家提交退款物流信息
   */
  @ResponseBody
  @RequestMapping(value = "/order/refund/proof", method = RequestMethod.POST)
  @ApiOperation(value = "买家提交退款申请", notes = "买家提交退款申请", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> proof(@RequestBody @Validated(ProceedRefundGroup.class)
                                               OrderRefundRequestForm form) {

    GlobalErrorCode code = GlobalErrorCode.SUCESS;

    List<OrderRefund> refunds = orderRefundService.listByOrderId(form.getOrderId());
    if (CollectionUtils.isEmpty(refunds)) {
      code = GlobalErrorCode.NEED_APPLY_REFUND;
      return new ResponseObject<>(false, code.getError(), code);
    }
    Optional<? extends OrderRefund> refundVO = Optional.fromNullable(refunds.get(0));
    if (!refundVO.isPresent()) {
      code = GlobalErrorCode.NEED_APPLY_REFUND;
      return new ResponseObject<>(false, code.getError(), code);
    }else{
      if (refundVO.get().getStatus().equals(OrderRefundStatus.SUBMITTED)) {
        code = GlobalErrorCode.REPEAT_APPLY_REFUND_PROOF;
        return new ResponseObject<>(false, code.getError(), code);
      }
      if (refundVO.get().getStatus().equals(OrderRefundStatus.ACCEPT_REFUND) || refundVO.get().getStatus().equals(OrderRefundStatus.ACCEPT_CHANGE)
              || refundVO.get().getStatus().equals(OrderRefundStatus.ACCEPT_REISSUE)) {
        //提交凭证
        refundVO.get().setStatus(OrderRefundStatus.SUBMITTED);
        refundVO.get().setLogisticsCompany(form.getLogisticsCompany());
        refundVO.get().setLogisticsNo(form.getLogisticsNo());
        orderRefundService.update(refundVO.get());
        orderHeaderService.updateStatusByOrderNo(Integer.toString(9), orderService.load(form.getOrderId()).getOrderNo());
      } else {
        code = GlobalErrorCode.NEED_PASS_REFUND_CHECK;
        return new ResponseObject<>(false, code.getError(), code);
      }
    }

    return new ResponseObject<>(true, code.getError(), code);
  }

  /**
   * 获取某个订单的退款申请信息
   */
  @ResponseBody
  @RequestMapping(value = "/order/refundInfo", method = RequestMethod.POST)
  @ApiOperation(value = "获取某个订单的退款申请信息", notes = "获取某个订单的退款申请信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<OrderRefund> refundInfo(@RequestParam String orderId) {
    OrderRefund refund = null;
    List<OrderRefund> list = orderRefundService.listByOrderId(orderId);
    if (list != null) {
      refund = list.get(0);
    }
    return new ResponseObject<>(refund);
  }

  /**
   * 录入物流信息接口（后端仓库使用）
   * @param orderId
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/order/wms/update", method = RequestMethod.POST)
  public ResponseObject<Boolean> updateWmsMessage(@RequestParam String orderId, @RequestParam String logiCompany,
      @RequestParam String logiNumber) {
    return new ResponseObject<>(orderRefundService.updateWmsCheckStatus(orderId, logiCompany, logiNumber));
  }

  @ResponseBody
  @RequestMapping(value = "/order/refund/task", method = RequestMethod.POST)
  public void refundTask(){
    ptSelectOutTimeService.PtOutTimeService();
    log.info("=================调用定时任务接口，处理过期拼团活动订单=====================");
  }

  //============私有方法===================

  /**
   * 验证退货信息是否与数据库中信息一致，是否是最新的退款申请
   */
  private boolean validation(String requestData, String id) {
    // 获取申请退货订单信息
    OrderRefund orderRefund = orderRefundService.load(id);
    String validationData = MD5Util.MD5Encode(orderRefund.toString(), "UTF-8");
    return validationData.equals(requestData);
  }

  /**
   * 取消和拒绝申请售后处理
   * @param orderId
   */
  private void cancelAndRejectRefund(String orderId){
    List<OrderItem> orderItems = orderItemMapper.selectByOrderIdAndPT(orderId);
    if(orderItems != null){
      for (OrderItem orderItem : orderItems) {
        if(PromotionType.APPLY_CHANGE_GOODS.equals(orderItem.getPromotionType())
                || PromotionType.APPLY_REISSUE_GOODS.equals(orderItem.getPromotionType())){
          //如果为申请换货、补货的商品，在拒绝时要删除对应记录
          orderItemMapper.deleteByPrimaryKey(orderItem.getId());
        }
      }
    }
  }

  /**
   * 发送消息
   * @param status
   * @param orderNo
   */
  private void ServMessage(String status,String orderNo){

    String cpid=servMessageMapper.selectCpidByOrderNo(orderNo);
    Integer push=servMessageMapper.message(status,ServMessageStatus.TYPEMSG.getWord(),ServMessageStatus.STATUS.getWord(),cpid,ServMessageStatus.TYPESYSTEM.getWord());

  }

}
