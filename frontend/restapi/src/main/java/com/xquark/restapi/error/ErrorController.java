package com.xquark.restapi.error;


import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.GlobalErrorCode;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * servlet过滤器异常处理 有错误时统一返回json错误信息给调用端 Created by chh on 17-1-22.
 */
@Controller
@ApiIgnore
public class ErrorController extends BaseController {

  private static final Logger LOG = LoggerFactory.getLogger(ErrorController.class);

  /**
   * 异常处理
   */
  @ResponseBody
  @RequestMapping("/customError")
  public ResponseObject<?> error(HttpServletRequest request, Exception e) {
    log.error("cause:", e);
    GlobalErrorCode ec = (GlobalErrorCode) request.getAttribute("errorType");
    if (ec != null) {
      return new ResponseObject<Object>(ec.getError(), ec);
    } else {
      return new ResponseObject<Object>(e.toString(), GlobalErrorCode.INTERNAL_ERROR);
    }
  }

}
