package com.xquark.restapi.trade;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.OutPay;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.trade.TradeService;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class TradeController extends BaseController {

  @Autowired
  private OutPayService outPayService;

  @Autowired
  private TradeService tradeService;

  /**
   * 交易收支明细
   */
  @ResponseBody
  @RequestMapping("/trade/recordList")
  public ResponseObject<Map<String, Object>> listTwitterOrder(
      @RequestParam(value = "outpayType", required = false) String outpayType,
      @RequestParam(value = "orderNo", required = false) String orderNo,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "userName", required = false) String userName,
      @RequestParam(value = "userPhone", required = false) String userPhone, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(outpayType)) {
      params.put("outpayType", outpayType);
    }

    if (StringUtils.isNotEmpty(orderNo)) {
      params.put("orderNo", "%" + orderNo + "%");
    }
    if (StringUtils.isNotEmpty(userName)) {
      params.put("userName", "%" + userName + "%");
    }
    if (StringUtils.isNotEmpty(userPhone)) {
      params.put("userPhone", "%" + userPhone + "%");
    }

    if (StringUtils.isNotEmpty(startDate)) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    List<OutPay> result = outPayService.selectRecords(params, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", outPayService.countRecords(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 交易概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/trade/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取总收入,总支出,本周交易额
    summary = tradeService.getSummary();

    return new ResponseObject<>(summary);
  }


}