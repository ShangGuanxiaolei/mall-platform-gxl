package com.xquark.restapi.groupon;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.mapper.PromotionGrouponMapper;
import com.xquark.dal.model.ActivityGroupon;
import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.groupon.ActivityGrouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 团购活动controller Created by chh on 16-10-9.
 */
@Controller
@ApiIgnore
public class ActivityGrouponController extends BaseController {

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private PromotionGrouponMapper promotionGrouponMapper;

  /**
   * 团购商品在用户点击开团分享时，创建一个对应的团购活动信息
   */
  @ResponseBody
  @RequestMapping("/activityGroupon/create")
  public ResponseObject<Boolean> create(@RequestParam("productId") String productId) {
    ActivityGroupon activityGroupon = new ActivityGroupon();
    PromotionGrouponVO promotionGrouponVO = promotionGrouponMapper.selectByProductId(productId);
    activityGroupon.setGrouponId(promotionGrouponVO.getId());
    activityGroupon.setUserId(getCurrentIUser().getId());
    activityGroupon.setStatus(ActivityGrouponStatus.CREATED);
    activityGroupon.setArchive(false);
    int result = activityGrouponService.insert(activityGroupon);
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

}
