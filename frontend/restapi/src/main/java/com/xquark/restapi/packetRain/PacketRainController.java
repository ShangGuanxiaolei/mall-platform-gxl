package com.xquark.restapi.packetRain;

import com.google.common.collect.ImmutableMap;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.PointServiceApi;
import com.xquark.dal.mapper.PromotionListMapper;
import com.xquark.dal.model.PacketRain;
import com.xquark.dal.model.PromotionList;
import com.xquark.dal.model.PromotionLotteryProbability;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.PromotionLotteryVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.packetRain.vo.PacketStyleShow;
import com.xquark.restapi.packetRain.vo.StyleSort;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.packetRain.PacketRainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 红包雨控制层
 */
@RestController
@RequestMapping("/packet/rain")
public class PacketRainController extends BaseController {

  @Autowired
  private PacketRainService packetRainService;

  @Autowired
  private PromotionListMapper promotionListMapper;

  private PointServiceApi pointServiceApi;

  @Autowired
  public void setPointServiceApi(PointContextInitialize initialize) {
    this.pointServiceApi = initialize.getPointServiceApi();
  }

  @RequestMapping(value = "/lottery/show", method = RequestMethod.GET)
  public ResponseObject<PacketStyleShow> showSort() {
    final PromotionLotteryVO currPromotion = packetRainService.getCurrentPromotionLotteryVO()
            .orElseThrow(BizException.ofSupplier(GlobalErrorCode.NOT_FOUND, "没有进行中的活动哟"));
    final Long cpId = getCurrentUser().getCpId();
    final PromotionList list = promotionListMapper.selectByPrimaryKey(currPromotion.getPromotionListId());
    if (list == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动信息不存在");
    }
    final BigDecimal totalPoint = pointServiceApi.sumByTrancd(cpId, Trancd.LOTTERY_EARN, list.getTriggerStartTime(),
            list.getTriggerEndTime());
    currPromotion.setTotalPoint(totalPoint);
    final List<StyleSort> styleSorts = packetRainService.getPromotionLotteryProbability(currPromotion.getId())
            .stream()
            .sorted(Comparator.comparing(PromotionLotteryProbability::getSortOrder))
            .map(plp -> new StyleSort(plp.getSortOrder(), plp.getLotteryType(),
                    plp.getPrize(), plp.getImage(), plp.getName()))
            .collect(Collectors.toList());
    return new ResponseObject<>(new PacketStyleShow(currPromotion, styleSorts));
  }

  /**
   * 查询红包雨活动信息
   *
   * @return 红包雨实体
   */
  @RequestMapping(value = "/findPacketRain", method = RequestMethod.GET)
  public ResponseObject<Map<String, ?>> findPacketRain() {
    User user = User();
    if (user == null || user.getCpId() == null)
      throw new BizException(GlobalErrorCode.USER_NOT_EXIST);
    final PacketRain packetRain = packetRainService.findPacketRain(user.getCpId());
    BigDecimal totalPoint = BigDecimal.ZERO;
    if (packetRain.getState() != 2) {
      totalPoint = pointServiceApi.sumByTrancd(user.getCpId(), Trancd.PACKET_RAIN,
              packetRain.getTriggerStartTime(), packetRain.getTriggerEndTime());
    }
    final Map<String, ?> ret = ImmutableMap.of(
            "currentUser", user,
            "totalPoint", totalPoint,
            "packetRain", packetRain
    );
    return new ResponseObject<>(ret);
  }

  /**
   * 将用户信息存入redis
   * @return null
   */
  @RequestMapping(value = "/savePacketRain", method = RequestMethod.GET)
  public ResponseObject savePacketRain(){
    User user = User();
    if (user == null || user.getCpId() == null)
      throw new BizException(GlobalErrorCode.USER_NOT_EXIST);
    packetRainService.savePacketRain(user.getCpId());
    return new ResponseObject();
  }

  /**
   * 首页banner图展示
   *
   * @return map
   */
  @RequestMapping(value = "/index", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> index(HttpServletRequest request) {
    User user = User();
    if (user == null || user.getCpId() == null)
      throw new BizException(GlobalErrorCode.USER_NOT_EXIST);
    return new ResponseObject<>(packetRainService.whiteCheck(user.getCpId(), request));
  }

}
