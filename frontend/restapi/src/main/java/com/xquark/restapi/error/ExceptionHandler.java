package com.xquark.restapi.error;

import com.google.common.collect.ImmutableSet;
import com.xquark.monitor.WarnEntity;
import com.xquark.monitor.WarnEnv;
import com.xquark.monitor.WarnSystemName;
import com.xquark.monitor.drive.MonitorWarn;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * General error handler for the application.
 */
@ControllerAdvice
class ExceptionHandler {

  Logger log = LoggerFactory.getLogger(getClass());

  @Value("${profiles.active}")
  private String currentEnv;

  private final static Set<String> WARN_ENV_SET = ImmutableSet.builder()
          .add(WarnEnv.prod)
          .add(WarnEnv.stage)
          .add(WarnEnv.uat)
          .add(WarnEnv.sit)
          .add(WarnEnv.uat2)
          .build()
          .stream()
          .map(Object::toString)
          .collect(Collectors.toSet());



  /**
   * Handle exceptions thrown by handlers.
   */
  @ResponseBody
  @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
  public ResponseObject<?> exception(HttpServletRequest req, Exception e) {
    /*Exception 预警*/
    if(WARN_ENV_SET.contains(currentEnv)) {
      // 强制转型安全, contains表示当前环境符合枚举类型
      WarnEntity warnEntity = new WarnEntity(WarnEnv.valueOf(currentEnv),
              WarnSystemName.REST_API, e, req);

      MonitorWarn monitorWarn = MonitorWarn.getInstance();
      monitorWarn.sendMonitorMsg(warnEntity);
    }
    /*Exception 预警 end*/
    log.error(req.getRequestURI() + " general error", e);

    BizException be = getBizException(e);
    GlobalErrorCode ec;
    String moreInfo;
    if (e instanceof MissingServletRequestParameterException) {
      ec = GlobalErrorCode.INVALID_ARGUMENT;
      RequestContext requestContext = new RequestContext(req);
      Object[] params = new Object[1];
      params[0] = ((MissingServletRequestParameterException) e).getParameterName();
      moreInfo = requestContext.getMessage("valid.notBlank.param", params);
    } else if (e instanceof SQLException) {
      ec = GlobalErrorCode.INTERNAL_ERROR;
      moreInfo = e.getClass().getName();
    } else if (be == null) {
      ec = GlobalErrorCode.UNKNOWN;
      moreInfo = e.getClass().getName();
    } else {
      ec = be.getErrorCode();
      moreInfo = be.getMessage();
    }
    return new ResponseObject<>(moreInfo, ec);
  }

  /**
   * 异常处理器:处理当请求的参数不符合规范时产生的异常(表单)
   *
   * @param e 非检查异常
   * @return 返回HTTP状态码400
   */
  @org.springframework.web.bind.annotation.ExceptionHandler(value = MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ResponseObject<String> violationException(MethodArgumentNotValidException e) {
    // 取出所有的校验错误并整理为一个字符串
    BindingResult bindingResult = e.getBindingResult();
    List<ObjectError> allErrors = bindingResult.getAllErrors();
    StringBuilder violationResult = new StringBuilder();
    for (ObjectError error : allErrors) {
      violationResult.append(error.getDefaultMessage()).append(" ");
    }
    // 记录日志
    log.info("ConstraintViolationException:{}", violationResult.toString());
    return new ResponseObject<>(violationResult.toString());
  }


  private BizException getBizException(Throwable e1) {
    Throwable e2 = e1;
    do {
      if (e2 instanceof BizException) {
        return (BizException) e2;
      }
      e1 = e2;
      e2 = e1.getCause();
    } while (e2 != null && e2 != e1);

    return null;
  }
}