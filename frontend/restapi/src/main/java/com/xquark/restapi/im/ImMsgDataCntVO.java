package com.xquark.restapi.im;

import java.util.List;

public class ImMsgDataCntVO {

  private Integer totalCnt = 0;
  private List<ImMsgDataCnt> msgCntList;

  public Integer getTotalCnt() {
    return totalCnt;
  }

  public void setTotalCnt(Integer totalCnt) {
    this.totalCnt = totalCnt;
  }

  public List<ImMsgDataCnt> getMsgCntList() {
    return msgCntList;
  }

  public void setMsgCntList(List<ImMsgDataCnt> msgCntList) {
    this.msgCntList = msgCntList;
  }

}
