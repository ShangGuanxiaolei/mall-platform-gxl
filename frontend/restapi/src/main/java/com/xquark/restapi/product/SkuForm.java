package com.xquark.restapi.product;

import java.math.BigDecimal;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

public class SkuForm {

  private String id;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec1;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec2;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec3;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec4;

  @Size(min = 0, max = 100, message = "{valid.sku.spec.message}")
  private String spec5;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.price.message}")
  private BigDecimal price;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.price.message}")
  private BigDecimal marketPrice;

  @NotNull(message = "{valid.notBlank.message}")
  @Range(min = 0, max = 99999999, message = "{valid.sku.amount.message}")
  private Integer amount;


  @NotNull(message = "{valid.notBlank.message}")
  @Range(min = 0, max = 99999999, message = "{valid.sku.amount.message}")
  private Integer secureAmount;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.point.message}")
  private BigDecimal point;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.point.message}")
  private BigDecimal serverAmt;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.point.message}")
  private BigDecimal promoAmt;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.deductionDPoint.message}")
  private BigDecimal deductionDPoint;

  @NotNull(message = "{valid.tag.notBlank.message}")
  @Digits(integer = 10, fraction = 2, message = "{valid.sku.deductionDPoint.message}")
  private BigDecimal netWorth;

  @NotBlank
  private String barCode;

  private String skuCode; // 第三方商品唯一编码
  private String skuCodeResources; // 第三方商品编码来源

  private String attributes; // sku对应的规格属性值id，如白色，S码等

//	@NotNull(message = "{valid.notBlank.message}")
	private String skuImg;


	private Double height = 0.0;
	private Double width = 0.0;
	private Double length = 0.0;

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getNumInPackage() {
		return numInPackage;
	}

	public void setNumInPackage(Integer numInPackage) {
		this.numInPackage = numInPackage;
	}

	private Integer weight;

	private Integer numInPackage = 0;




	public String getSkuImg() {
		return skuImg;
	}

	public void setSkuImg(String skuImg) {
		this.skuImg = skuImg;
	}



	public String getAttributes() {
    return attributes;
  }

  public void setAttributes(String attributes) {
    this.attributes = attributes;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getSkuCodeResources() {
    return skuCodeResources;
  }

  public void setSkuCodeResources(String skuCodeResources) {
    this.skuCodeResources = skuCodeResources;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public String getSpec1() {
    return spec1;
  }

  public void setSpec1(String spec1) {
    this.spec1 = spec1;
  }

  public String getSpec2() {
    return spec2;
  }

  public void setSpec2(String spec2) {
    this.spec2 = spec2;
  }

  public String getSpec3() {
    return spec3;
  }

  public void setSpec3(String spec3) {
    this.spec3 = spec3;
  }

  public String getSpec4() {
    return spec4;
  }

  public void setSpec4(String spec4) {
    this.spec4 = spec4;
  }

  public String getSpec5() {
    return spec5;
  }

  public void setSpec5(String spec5) {
    this.spec5 = spec5;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public Integer getSecureAmount() {
    return secureAmount;
  }

  public void setSecureAmount(Integer secureAmount) {
    this.secureAmount = secureAmount;
  }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }
}
