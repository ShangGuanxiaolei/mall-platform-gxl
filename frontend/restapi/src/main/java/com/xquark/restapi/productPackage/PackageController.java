package com.xquark.restapi.productPackage;

import com.xquark.dal.model.Package;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.productPackage.PackageService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: kong Date: 18-6-13. Time: 下午6:10 产品包装箱控制器
 */
@RestController
@RequestMapping("/package")
public class PackageController {

  @Autowired
  private PackageService packageService;


  /**
   * 通过id查找
   */
  @RequestMapping(value = "/{id}")
  public ResponseObject<Package> getByPrimaryKey(@PathVariable("id") @NotBlank String id) {
    Package productPackage = packageService.getByPrimaryKey(id);
    return new ResponseObject<>(productPackage);
  }

  /**
   * 编辑
   */
  @RequestMapping(value = "/edit")
  public ResponseObject<Boolean> update(@Valid Package productPackage) {
    Boolean isSuccess;
    if (StringUtils.isNotBlank(productPackage.getId())) {
      isSuccess = packageService.update(productPackage);
    } else {
      isSuccess = packageService.insert(productPackage);
    }
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 通过id删除
   */
  @RequestMapping(value = "/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable("id") @NotBlank String id) {
    Boolean isSuccess = packageService.delete(id);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 批量删除
   */
  @RequestMapping(value = "/batchDelete")
  public ResponseObject<Boolean> batchDelete(@RequestParam("ids") String id) {
    String[] ids = id.split(",");
    Boolean isSuccess = packageService.batchDelete(Arrays.asList(ids));
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 计算数据的数量
   */
  @RequestMapping(value = "/count")
  public ResponseObject<Long> count(SearchForm searchForm, Pageable page) {
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    Long num = packageService.count(params);
    return new ResponseObject<>(num);
  }

  /**
   * 分页查询
   */
  @RequestMapping(value = "/list")
  public ResponseObject<Map<String, Object>> list(SearchForm searchForm, Pageable page) {
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    List<Package> packages = packageService.list(page, params);
    Long num = packageService.count(params);
    Map<String, Object> listAndNum = new HashMap<String, Object>();
    listAndNum.put("list", packages);
    listAndNum.put("total", num);
    return new ResponseObject<>(listAndNum);
  }
}
