package com.xquark.restapi.reserve;

import com.xquark.dal.model.GrandSaleModule;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.ReserveVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.grandSale.GrandSaleModuleService;
import com.xquark.service.reserve.PromotionReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.Objects;

/**
 * 预售
 *
 * @author: yyc
 * @date: 19-4-18 下午1:58
 */
@RestController
@RequestMapping("/reserve")
public class PromotionReserveController extends BaseController {

    private final PromotionReserveService promotionReserveService;

    private final GrandSaleModuleService grandSaleModuleService;

    @Autowired
    public PromotionReserveController(PromotionReserveService promotionReserveService, GrandSaleModuleService grandSaleModuleService) {
        this.promotionReserveService = promotionReserveService;
        this.grandSaleModuleService = grandSaleModuleService;
    }

    /**
     * 根据会场活动id和类目名查询商品列表
     *
     * @param grandSaleId 会场活动id
     * @return ResponseObject
     */
    @RequestMapping("/activity/{id}")
    public ResponseObject<ReserveVO> getReserveProductList(@PathVariable("id") Integer grandSaleId) {
        // 找到所有预约活动中有效的第一个活动
        final ReserveVO vo = grandSaleModuleService
                .selectByGrandSaleIdAndPromotionType(grandSaleId, PromotionType.RESERVE)
                .stream()
                .map(GrandSaleModule::getPromotionId)
                .map(promotionReserveService::loadActivityById)
                .filter(Objects::nonNull)
                .filter(ReserveVO::isValid)
                .min(Comparator.comparing(ReserveVO::getValidFrom)
                        .thenComparing(ReserveVO::getCreatedAt))
                .orElse(null);
        return new ResponseObject<>(vo);
    }

    /**
     * 预约提交
     *
     * @param form ReserveSubmitForm
     * @return ResponseObject
     */
    @RequestMapping("/submit")
    public ResponseObject<Boolean> submitReserveOrder(@Valid @RequestBody ReserveSubmitForm form) {
        boolean ret =
                promotionReserveService.saveReserveOrder(
                        form.getPromotionId(), form.getProductId(), form.getSkuId(), form.getQty());
        return new ResponseObject<>(ret);
    }

    /**
     * 校验活动
     *
     * @param preOrderNo String
     * @return ResponseObject
     */
    @RequestMapping("/beforePromotionPay")
    public ResponseObject<Boolean> beforePromotionPay(@RequestParam("preOrderNo") String preOrderNo) {
//        // 查询是否已经生成了订单
//        final OrderStatus orderStatus =
//                promotionReserveService.selectOrderStatus(preOrderNo);
//        if (orderStatus != null) {
//            throw new BizException(GlobalErrorCode.RESERVE_PRE_ORDER_NO_USED);
//        }
        promotionReserveService.checkPromotion(preOrderNo);
        return new ResponseObject<>(Boolean.TRUE);
    }

    @RequestMapping("/cancel")
    public ResponseObject<Boolean> cancel() {
        boolean b = promotionReserveService.endReserveStatus();
        return new ResponseObject<>(b);
    }
}
