package com.xquark.restapi.helper;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.Helper;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.helper.HelperService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-12-7. DESC: 客服、帮助相关接口
 */
@RestController
@RequestMapping("/helper")
@Api(value = "帮助接口")
public class HelperController {

  private HelperService helperService;

  @Autowired
  public HelperController(HelperService helperService) {
    this.helperService = helperService;
  }

  @RequestMapping("/list")
  @ApiOperation(value = "分页查询帮助信息", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> list(Pageable pageable) {
    List<Helper> helpers = helperService.listHelperWithOutBLob(pageable);
    Long count = helperService.countHelper();
    Map<String, Object> result = new HashMap<>();
    result.put("list", helpers);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/view")
  @ApiOperation(value = "根据id获取帮助信息", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Helper> view(@RequestParam("id") String id) {
    Helper helper = helperService.load(id);
    if (helper == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "该帮助不存在");
    }
    return new ResponseObject<>(helper);
  }

  @RequestMapping("/save")
  @ApiOperation(value = "保存", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> save(@Validated HelperForm form, Errors errors) {
    ControllerHelper.checkException(errors);
    String id = form.getId();
    boolean result;
    Helper helper = Transformer.fromBean(form, Helper.class);
    if (StringUtils.isBlank(id)) {
      result = helperService.save(helper);
    } else {
      result = helperService.update(helper);
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/delete")
  public ResponseObject<Boolean> delete(@RequestParam("id") String id) {
    boolean result = helperService.delete(id);
    return new ResponseObject<>(result);
  }

}
