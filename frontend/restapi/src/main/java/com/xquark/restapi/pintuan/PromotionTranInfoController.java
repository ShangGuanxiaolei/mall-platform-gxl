package com.xquark.restapi.pintuan;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.vo.PromotionTranInfoVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.order.OrderService;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.pintuan.PromotionTranInfoService;
import com.xquark.service.pintuan.RecordPromotionPgTranInfoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @author LiHaoYang
 * @ClassName PromotionTranInfoController
 * @date 2018/9/13 0013
 */
@Controller
public class PromotionTranInfoController extends BaseController {

  private final static Logger LOGGER = LoggerFactory.getLogger(PromotionTranInfoController.class);

  @Autowired
  private PromotionTranInfoService promotionTranInfoService;

  @Autowired
  private RecordPromotionPgTranInfoService recordPromotionPgTranInfoService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private PromotionPgService promotionPgService;

  /**
   * 查询当前用户为团长的拼团信息
   */
  @RequestMapping("/promotionTranInfo/selectMyGroupInfo")
  @ResponseBody
  public ResponseObject<Map<String, Object>> selectMyGroupInfo(
      @RequestParam(value = "page", defaultValue = "1") Integer page,
      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
    User user = (User) getCurrentIUser();
    List<? extends PromotionTranInfoVO> tranInfoList = promotionTranInfoService
        .selectMyGroupInfo(user.getCpId(), page, pageSize);
    Map<String, Object> ret = ImmutableMap.of("tranInfoList", tranInfoList);
    return new ResponseObject<>(ret);
  }


  /**
   * 查询当前用户不为团长的拼团信息
   */
  @RequestMapping("/promotionTranInfo/selectMyNoGroupInfo")
  @ResponseBody
  public ResponseObject<Map<String, Object>> selectMyNoGroupInfo(
      @RequestParam(value = "page", defaultValue = "1") Integer page,
      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
    User user = (User) getCurrentIUser();
    List<? extends PromotionTranInfoVO> tranInfoList = promotionTranInfoService
        .selectMyNoGroupInfo(user.getCpId(), page, pageSize);
    Map<String, Object> ret = ImmutableMap.of("tranInfoList", tranInfoList);
    return new ResponseObject<>(ret);
  }

  /**
   * 校验拼团状态是否为已支付
   */
  @RequestMapping("/promotionTranInfo/checkPayStatus")
  @ResponseBody
  public ResponseObject<Boolean> checkTranPaidStatus(@RequestParam("orderNo") String orderNo,
      @RequestParam("tranCode") String tranCode) {
    Order order = orderService.loadByOrderNo(orderNo);
    if (order == null) {
      LOGGER.error("找不到对应订单号 {}", orderNo);
      return new ResponseObject<>(false);
    }
    PGTranInfoVo tranInfo = recordPromotionPgTranInfoService.loadPGTranInfo(tranCode);
    if (tranInfo == null) {
      LOGGER.error("找不到对应tranCode: {}", tranCode);
      return new ResponseObject<>(false);
    }
    boolean isPaid = order.getPaidStatus() == PayStatus.SUCCESS;
    // FIXME 魔法值
    boolean isTranPaid = StringUtils.equals(tranInfo.getPieceStatus(), "1") || StringUtils
        .equals(tranInfo.getPieceStatus(), "2");
    return new ResponseObject<>(isPaid && isTranPaid);
  }
}
