package com.xquark.restapi.im;

import java.math.BigDecimal;

public class ImProductVO {

  private String productimgUrl;
  private String productName;
  private BigDecimal price;
  private BigDecimal defPrice;
  private String sellerId;
  private String productId;
  private String description;
  private Integer sales;
  private Integer amount;
  private String productUrl;
  private String sellerName;

  public String getProductimgUrl() {
    return productimgUrl;
  }

  public void setProductimgUrl(String productimgUrl) {
    this.productimgUrl = productimgUrl;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getDefPrice() {
    return defPrice;
  }

  public void setDefPrice(BigDecimal defPrice) {
    this.defPrice = defPrice;
  }

  public String getSellerId() {
    return sellerId;
  }

  public void setSellerId(String sellerId) {
    this.sellerId = sellerId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getSales() {
    return sales;
  }

  public void setSales(Integer sales) {
    this.sales = sales;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

}
