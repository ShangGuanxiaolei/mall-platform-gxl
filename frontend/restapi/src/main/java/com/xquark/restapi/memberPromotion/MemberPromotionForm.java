package com.xquark.restapi.memberPromotion;

import com.xquark.dal.type.MemberPromotionStatus;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-3-14. DESC:
 */
public class MemberPromotionForm {

  private String id;

  /**
   * 活动标题
   */
  @NotBlank(message = "标题不能为空")
  private String title;

  /**
   * 介绍图
   */
  @NotBlank(message = "请上传主图")
  private String banner;

  /**
   * 状态, 默认激活
   */
  private MemberPromotionStatus status = MemberPromotionStatus.ACTIVE;

  /**
   * 跳转链接
   */
  private String url;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBanner() {
    return banner;
  }

  public void setBanner(String banner) {
    this.banner = banner;
  }

  public MemberPromotionStatus getStatus() {
    return status;
  }

  public void setStatus(MemberPromotionStatus status) {
    this.status = status;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
