package com.xquark.restapi.rights;

import com.alibaba.fastjson.JSONObject;
import com.xquark.biz.url.UrlHelper;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.RecommendCode;
import com.xquark.dal.mapper.PromotionVipsuitMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.FragmentAndDescController;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.product.VipProductService;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.recommend.RecommendService;
import com.xquark.service.sku.SkuAttributeService;
import com.xquark.utils.DynamicPricingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rights")
public class RightsController extends FragmentAndDescController {

  private static final Log logger = LogFactory.getLog(RightsController.class);

  @Autowired
  private CareerLevelService careerLevelService;
  @Autowired
  private VipProductService vipProductService;
  @Autowired
  private SkuAttributeService skuAttributeService;
  @Autowired
  private PromotionCouponService couponService;
  @Autowired
  private UrlHelper urlHelper;
  @Autowired
  private FlashSalePromotionProductService flashSaleService;
  @Autowired
  private CommentService commentService;
  @Autowired
  private PromotionVipsuitMapper promotionVipsuitMapper;
  @Autowired
  private CustomerProfileService customerProfileService;
  @Autowired
  private RecommendService recommendService;

  @Autowired
  private FirstOrderService firstOrderService;

  private static final String IDENTITY_RC = "RC";
  private static final String IDENTITY_DS = "DS";


  @ResponseBody
  @RequestMapping(value = "/upgrading", method = RequestMethod.GET)
  public ResponseObject<RightUpgrade> getUserRights() {
    User user = (User) getCurrentIUser();

    CustomerCareerLevel level = careerLevelService.load(user.getCpId());
    if (null == level) {
      logger.info("==================当前用户查不到对应的身份数据：cpId=" + user.getCpId());
      return new ResponseObject<>("找不到用户[" + user.getCpId() + "]的身份数据", GlobalErrorCode.NOT_FOUND);
    }
    logger.info(
        "=================upgrading: 获取当前用户身份：hdsType=" + level.getHdsType() + ", vvType=" + level
            .getViviLifeType());

    RightUpgrade upgrade = new RightUpgrade();
    // 白人
    if ((StringUtils.isEmpty(level.getHdsType()) || IDENTITY_RC.equals(level.getHdsType())) &&
        (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
            .equals(level.getViviLifeType()))) {
      upgrade.setType("upgrade");
      upgrade.setBanner(
          "http://images.handeson.com/Fi30oA2b-0s1qODEGhhH24Mibkdn?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");
    } else if (IDENTITY_DS.equals(level.getHdsType()) && !level.getIsLightening() &&
        (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
            .equals(level.getViviLifeType()))) {
      // SR身份的人看不到banner，do nothing
      upgrade = null;
    } else {
      // 有身份的人
      upgrade.setType("invite");
      upgrade.setBanner(
          "https://images.handeson.com/FqwTOf57rN42KZeBoUHnX58EKGmG?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");
    }
    return new ResponseObject<>(upgrade);
  }

  /**
   * 前端使用，获取VIP商品详情
   */
  @ResponseBody
  @RequestMapping(value = "/product/vipsuits/{id}", method = RequestMethod.GET)
  //@DoGuavaCache(key = CacheKeyConstant.PRODUCT_VIP)
  public ResponseObject getVipSuitById(@PathVariable("id") String productId,
      HttpServletRequest req) {
    Boolean aBoolean = vipProductService.findVipProductByProductId(productId);
    if (aBoolean) {
      ProductVO product = productService.load(productId);
      if (product == null) {
        RequestContext requestContext = new RequestContext(req);
        throw new BizException(GlobalErrorCode.NOT_FOUND,
            requestContext.getMessage("product.not.found"));
      }

      //test
      convertUrl(product); // 生成商品的图片URL
      // 约定获取商品的图文信息的时候，从productService中获取，实现：先获取富文本信息，再获取片段信息
      setFragmentAndDescInfo(product);

      // 获取商品的多sku规格属性
      List<SkuAttributeVO> skuAttributeVOs = skuAttributeService
          .findAttributesByProductId(productId);
      product.setSkuAttributes(skuAttributeVOs);

      List<BasePromotionCouponVO> availableCoupons = couponService
          .listBasePromotionCouponByProduct(product.getId());
      product.setCouponList(availableCoupons);

      // 计算积分兑换比例
      // 只在前端调用时计算
      User user = (User) getCurrentIUser();
      Long cpId = userService.selectCpIdByUserId(user.getId());

      ProductVOEx result = new ProductVOEx(product, urlHelper.genProductUrl(
          product.getId() + "?upline=" + cpId),
          product.getImg(), null);

      PromotionProductVO promotionProduct = flashSaleService
          .loadPromotionProductByPId(product.getId());
      if (promotionProduct != null) {
        result.setCanUsePromotion(promotionProduct);
        result.setCanUsePromotionType(PromotionType.FLASHSALE);
      }

      // 放入评价统计信息
      Map<String, Long> commentTotalMap = commentService.commentTotalMap(product.getId());
      result.setCommentTotalMap(commentTotalMap);

      //用第一个sku的数据覆盖商品的规格数据
      if (CollectionUtils.isNotEmpty(result.getSkus())) {
        Sku sku = result.getSkus().get(0);
        result.setDeductionDPoint(sku.getDeductionDPoint());
        result.setNetWorth(sku.getNetWorth());
        result.setPoint(sku.getPoint());
        result.setPrice(sku.getPrice());
        result.setAmount(sku.getAmount());
        result.setServerAmt(sku.getServerAmt());
        result.setPromoAmt(sku.getPromoAmt());

        if (promotionProduct != null && promotionProduct.getPromotionType()
            .equals(PromotionType.FLASHSALE) && (user instanceof User)) {
          Promotion promotion = promotionProduct.getPromotion();
          sku.setPrice(result.getCanUsePromotion().getPromotionPrice());
          BigDecimal discount = promotion.getDiscount().divide(BigDecimal.TEN);
          result.setDeductionDPoint(
              sku.getDeductionDPoint().multiply(discount).setScale(2, BigDecimal.ROUND_HALF_EVEN));
        }

        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + cpId);
        if (json != null) {

          JSONObject jsonObject = JSONObject.parseObject(json);
          Boolean isProxy = jsonObject.getBoolean("isProxy");
          Boolean isMember = jsonObject.getBoolean("isMember");

          BigDecimal conversionPrice = result.getConversionPrice();

          BigDecimal subtractValue = result.getPoint().add(sku.getServerAmt());
          BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2, 1);
          result.setProxyPrice(proxyPrice);

          result.setMemberPrice(sku.getPrice().subtract(result.getPoint()).setScale(2, 1));
          if (isProxy) {
            result.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2, 1));
            result.setSavedPrice(subtractValue);
            result.setEarnedPrice(subtractValue);
          } else if (isMember) {
            result.setChangePrice(conversionPrice.subtract(result.getPoint()).setScale(2, 1));
            result.setSavedPrice(result.getPoint());
            result.setEarnedPrice(result.getPoint());
          } else {
            result.setChangePrice(conversionPrice);
            result.setSavedPrice(result.getPoint());
            result.setEarnedPrice(result.getPoint());
          }

          if (promotionProduct != null && promotionProduct.getPromotionType()
              .equals(PromotionType.FLASHSALE) && (user instanceof User)) {
            result.setChangePrice(
                sku.getPrice().subtract(result.getDeductionDPoint().divide(BigDecimal.TEN))
                    .setScale(2, 1));
          }

        }
      }
      //fix 升级权益新人浮标
      Boolean firstOrder = firstOrderService.firstOrder(user.getCpId());
      result.setFirstOrder(firstOrder);
      int isSr=0;
      CustomerCareerLevel level = careerLevelService.load(user.getCpId());
        if (null == level) {
            log.info("==================当前用户查不到对应的身份数据：cpId=" + user.getCpId());
            return new ResponseObject<>("找不到用户[" + user.getCpId() + "]的身份数据", GlobalErrorCode.NOT_FOUND);
        }
        if (IDENTITY_DS.equals(level.getHdsType()) && !level.getIsLightening() &&
                (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
                        .equals(level.getViviLifeType()))) {
            isSr=1;
        }

      result.setIsSR(isSr);
      return new ResponseObject<>(result);
    } else {
      ResponseObject responseObject=new ResponseObject();
      responseObject.setMoreInfo("该vip商品已下架");
      return responseObject;
    }
  }

  private void convertUrl(ProductVO vo) {
    if (null != vo.getImgs() && vo.getImgs().size() > 0) {
      for (ProductImageVO img : vo.getImgs()) {
        img.setImgUrl(img.getImg());
      }
    }
  }

  /**
   * 后端使用，查询vip商品列表
   */
  @ResponseBody
  @RequestMapping(value = "/product/vipsuits/list", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> getVipSuitList(Pageable page) {
    Map<String, Object> aRetMap = new HashMap<>();
    List<VipProductBlackList> vipProducts = vipProductService.listVipsuits(page);
    Long total = promotionVipsuitMapper.countVipsuits();
    aRetMap.put("vipsuits", vipProducts);
    aRetMap.put("total", total);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 后端使用，更改VIP商品状态
   */
  @ResponseBody
  @RequestMapping(value = "/product/updateVipsuits", method = RequestMethod.POST)
  public ResponseObject<Boolean> updateVipSuit(
      @RequestParam("productid") Integer productid, @RequestParam("status") Integer status) {
    if (status == null && productid == null) {
      logger.info("传入参数不可为空");
    } else {
      vipProductService.updateVipStatus(productid, status);
    }
    return new ResponseObject<>();
  }

  /**
   * 后端使用，添加VIP套装商品
   */
  @ResponseBody
  @RequestMapping(value = "/product/addVipsuits", method = RequestMethod.POST)
  public ResponseObject<Integer> insertPromotionVipsuit(
      @RequestParam("productCodeStr") String productCodeStr) {
    if (StringUtils.isNotEmpty(productCodeStr) && StringUtils.isNotBlank(productCodeStr)) {
      String[] productCodes = productCodeStr.split(",");
      vipProductService.insertPromotionVipsuit(productCodes);
    } else {
      logger.info("VIP套装商品添加失败");
    }
    return new ResponseObject<>();
  }

  /**
   * 前端使用，展示的VIP套装商品列表
   */
  @ResponseBody
  @RequestMapping("/product/vipsuits")
  public ResponseObject<Map<String, Object>> getVipProducts(String productId) {

    Map<String, Object> aRetMap = new HashMap<>(3);
    User user = (User) getCurrentIUser();
    Long cpId = user.getCpId();
    // 商品id为空则返回升级权益相关的原逻辑
    List<VipProduct> vipProducts;
    boolean hasIdentity = customerProfileService.hasIdentity(cpId);
    // 没商品id - 升级权益 或
    // 有商品id 但是没身份 也走升级权益
    if (StringUtils.isBlank(productId) || (StringUtils.isNotBlank(productId) && !hasIdentity)) {
      vipProducts = vipProductService.listVipProducts();
      setEarnedPrice(vipProducts,cpId);
    } else {
      // 有商品有身份
      // 走推荐商品
      List<RecommendProductVO> recommend = recommendService.getRecommendProductList(RecommendCode.PIECE_CNXH_VIP);
      // 后台配置了推荐商品则取后台配置;
      // 否则走原商品推荐
      vipProducts = CollectionUtils.isEmpty(recommend)
          ? vipProductService.listVIPRecommends(productId)
          : Transformer.fromIterable(recommend, VipProduct.class);
      setEarnedPrice(vipProducts,cpId);
    }
    for (VipProduct vipProduct : vipProducts) {
        DynamicPricingUtil.rePricing(vipProduct, user);
    }
    if (null != vipProducts && !vipProducts.isEmpty()) {
      aRetMap.put("skus", vipProducts);
    } else {
      vipProducts = new ArrayList<>();
      aRetMap.put("skus", vipProducts);
    }

    CustomerCareerLevel level = careerLevelService.load(user.getCpId());
      if (null == level) {
          log.info("==================当前用户查不到对应的身份数据：cpId=" + user.getCpId());
          return new ResponseObject<>("找不到用户[" + user.getCpId() + "]的身份数据", GlobalErrorCode.NOT_FOUND);
      }
      if (IDENTITY_DS.equals(level.getHdsType()) && !level.getIsLightening() &&
              (StringUtils.isEmpty(level.getViviLifeType()) || IDENTITY_RC
                      .equals(level.getViviLifeType()))) {
          aRetMap.put("isSR",1);
      }
        else{
        aRetMap.put("isSR",0);
     }
    aRetMap.put("hasIdentity", hasIdentity);
    return new ResponseObject<>(aRetMap);
  }

  private void setEarnedPrice(List<VipProduct> vipProducts,Long cpId){
    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    String json = redisUtils.get("login_return_" + cpId);
    if (json != null) {

      JSONObject jsonObject = JSONObject.parseObject(json);
      Boolean isProxy = jsonObject.getBoolean("isProxy");
      Boolean isMember = jsonObject.getBoolean("isMember");
      if(isProxy) {
        for (int i = 0; i < vipProducts.size(); i++) {
          BigDecimal point = vipProducts.get(i).getPoint();
          vipProducts.get(i).setEarnedPrice(vipProducts.get(i).getServerAmt().add(point));
        }
      }else if(isMember){
        for (int i = 0; i < vipProducts.size(); i++) {
          vipProducts.get(i).setEarnedPrice(vipProducts.get(i).getPoint());
        }
      }
    }
  }



  public class RightUpgrade {

    private String type;
    private String banner;

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getBanner() {
      return banner;
    }

    public void setBanner(String banner) {
      this.banner = banner;
    }
  }
}
