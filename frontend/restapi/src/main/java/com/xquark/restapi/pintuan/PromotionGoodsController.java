package com.xquark.restapi.pintuan;

import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.vo.PromotionGoodsVO;
import com.xquark.dal.vo.PromotionPGoodsVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.order.OrderService;
import com.xquark.service.pintuan.PromotionGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LiHaoYang
 * @date 2018/9/6 0006
 */
@Controller("promotionGoodsController")
public class PromotionGoodsController extends BaseController {

  @Autowired
  private PromotionGoodsService promotionGoodsService;

  @Autowired
  private OrderService orderService;

  /**
   * 查询出已开始的活动商品
   */
  @ResponseBody
  @RequestMapping("/promotionGoods/selectBeginningPromotionGoods")
  public PageHelper<PromotionPGoodsVO> selectBeginningPromotionGoods(
          @RequestParam(value = "page", defaultValue = "0") Integer page,
          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
    User user = (User) getCurrentIUser();
    return promotionGoodsService.selectProduct(page, pageSize, user.getCpId());
  }

  /**
   * 根据pCode查询出已开始的活动商品列表
   */
  @ResponseBody
  @RequestMapping(value = "/promotionGoods/selectBeginningPromotionGoodsByPcode", method = RequestMethod.POST)
  public PageHelper<PromotionPGoodsVO> selectBeginningPromotionGoodsByPcode(
          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
          @RequestParam("pCode") String pCode) {
    User user = (User) getCurrentIUser();
    return promotionGoodsService.selectPromotionGoodsByPcode(pageNum, pageSize, user.getCpId(), pCode);
  }

  /**
   * 查询出已开始的活动商品(拼团使用)
   */
  @ResponseBody
  @RequestMapping("/promotionGoods/selectBeginningPromotionGoods2")
  public PageHelper<PromotionGoodsVO> selectBeginningPromotionGoods(
          Integer grandSaleId,
          String cpid,
          @RequestParam(value = "page", defaultValue = "1") Integer page,
          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
    User user = (User) getCurrentIUser();
    return promotionGoodsService.selectBeginningPromotionGoods(grandSaleId, user.getCpId().toString(), page, pageSize);
  }

  /**
   * 查询出未开始的活动商品(拼团使用)
   */
  @RequestMapping("/promotionGoods/selectNotBeginningPromotionGoods")
  @ResponseBody
  public PageHelper<PromotionGoodsVO> selectNotBeginningPromotionGoods(
          Integer grandSaleId,
          String cpid,
          @RequestParam(value = "page", defaultValue = "1") Integer page,
          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
    User user = (User) getCurrentIUser();
    return promotionGoodsService.selectNotBeginningPromotionGoods(grandSaleId, user.getCpId().toString(), page, pageSize);
  }

  /**
   * 封装预热团+正在团列表
   * @param grandSaleId
   * @param page
   * @param pageSize
   * @return
   */
  @ResponseBody
  public @RequestMapping("/promotionGoods/all")
  ResponseObject<Map<String, Object>> extractGetBeginningPromotionGoods(
          Integer grandSaleId,
          @RequestParam(value = "page", defaultValue = "1") Integer page,
          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

    User user = (User) getCurrentIUser();
    Map<String, Object> result = new HashMap<>();

    //正在拼的团
    PageHelper<PromotionGoodsVO> startingPg =
            promotionGoodsService.selectBeginningPromotionGoods(grandSaleId, user.getCpId().toString(), page, pageSize);
    result.put("startPg", startingPg);

    //获取预热的团
    PageHelper<PromotionGoodsVO> willStartPg =
            promotionGoodsService.selectNotBeginningPromotionGoods(grandSaleId, user.getCpId().toString(), page, pageSize);
    result.put("willStartPg", willStartPg);

    return new ResponseObject<>(result);

  }

  @RequestMapping(value = "/promotionGoods/paid/")
  public void payCallback2() {
    log.info("begin on order autoCancel");
    long begin = System.currentTimeMillis();
    int result = orderService.autoCancel();
    long end = System.currentTimeMillis();
    log.info("end on order autoCancel");
    String info = "买家未付款超过15分钟自动取消订单成功：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

}
