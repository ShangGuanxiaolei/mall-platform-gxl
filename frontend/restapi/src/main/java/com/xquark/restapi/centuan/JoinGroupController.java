package com.xquark.restapi.centuan;


import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.dal.model.mypiece.PieceConfirmOrder;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.restapi.BaseController;
import com.xquark.service.cantuan.Impl.PromotionPaGroupServiceImpl;
import com.xquark.service.cantuan.PromotionPaGroupService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.mypiece.StockCheckService;
import com.xquark.service.pintuan.CommitOrderService;
import com.xquark.service.pintuan.impl.CommitOrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @Author: tanggb
 * @Time: Create in 14:41 2018/9/8
 * @Description: 进入参团页面进行活动判断，拼团时间判断
 * @Modified by:
 */
@Controller
@RequestMapping("/join")
public class JoinGroupController extends BaseController {
    @Autowired
    private StockCheckService stockCheckService;
    @Autowired
    PromotionPaGroupServiceImpl promotionPaGroupServiceImpl;

    @Deprecated
    @RequestMapping("/isActivity")
    public boolean isActivity(@RequestBody ConfirmAndCheckVo confirmAndCheckVo,User user){

        if(user == null){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在");
        }

        PromotionPaGroupService promotionPaGroupService = new PromotionPaGroupServiceImpl();
        promotionPaGroupService.isActivity(confirmAndCheckVo,user);

        if(promotionPaGroupService ==null){
            return false;
        }

        return true;
    }



    @Deprecated
    @RequestMapping("/confirmOrder")
    public Map<String,Object> confirmOrder(@RequestBody ConfirmAndCheckVo confirmAndCheckVo, User user){

        if(user == null){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在");
        }

        //判定活动与拼团有效期
        PromotionPaGroupService promotionPaGroupService = new PromotionPaGroupServiceImpl();
        promotionPaGroupService.isActivity(confirmAndCheckVo,user);

        //库存检查
        StockCheckBean stockCheckBean = confirmAndCheckVo.getStockCheckBean();
        stockCheckService.checkStock(stockCheckBean);

        //提交确认定单
        PieceConfirmOrder pco = confirmAndCheckVo.getPieceConfirmOrder();
        CommitOrderService commitOrderService = new CommitOrderServiceImpl();
        Map<String,Object> map = commitOrderService.confirmPiece(pco,user);

        //获取拼团应用锁

        return  map;
    }
}

