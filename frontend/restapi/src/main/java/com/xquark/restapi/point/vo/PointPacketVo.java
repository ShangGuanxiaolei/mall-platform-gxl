package com.xquark.restapi.point.vo;

import com.google.common.collect.Lists;
import com.xquark.service.pointgift.dto.PointPacketDTO;
import com.xquark.service.pointgift.dto.PointSendType;
import com.xquark.service.pointgift.dto.UserPointDetailDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class PointPacketVo {
    private List<PointPacketDetailVO> pointPacketVos;
    private Integer availableAmount;
    private Boolean newPerson;


    public List<PointPacketDetailVO> getPointPacketVos() {
        return pointPacketVos;
    }


    public static PointPacketVo valueOf(PointPacketDTO pointPacketDTO, Boolean isNew) {
        final PointPacketVo packetVo = new PointPacketVo();
        final ArrayList<PointPacketDetailVO> packetVoArrayList = Lists.newArrayList();
        packetVo.setAvailableAmount(pointPacketDTO.getAvailableAmount().intValue());
        packetVo.setPointPacketVos(packetVoArrayList);
        for (UserPointDetailDTO packetDetailDTO : pointPacketDTO.getPacketDetailDTOS()) {
            final PointPacketDetailVO vo = new PointPacketDetailVO();
            vo.setCreateAt(packetDetailDTO.getCreateTime());
            vo.setPointGiftNo(packetDetailDTO.getPacketNo());
            vo.setPointPacketType(packetDetailDTO.getPointType().getMessage());
            vo.setAmount(PointSendType.computeType(packetDetailDTO.getAmount(), packetDetailDTO.getPointType()));
            vo.setBizType(packetDetailDTO.getPointType().name());
            vo.setPacketType(packetDetailDTO.getPacketType());
            packetVoArrayList.add(vo);
        }
        packetVo.setNewPerson(isNew);
        return packetVo;
    }

    public Boolean getNewPerson() {
        return newPerson;
    }

    public void setNewPerson(Boolean newPerson) {
        this.newPerson = newPerson;
    }

    public void setPointPacketVos(List<PointPacketDetailVO> pointPacketVos) {
        this.pointPacketVos = pointPacketVos;
    }

    public Integer getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(Integer availableAmount) {
        this.availableAmount = availableAmount;
    }

    @Override
    public String toString() {
        return "PointPacketVo{" +
                "pointPacketVos=" + pointPacketVos +
                ", availableAmount='" + availableAmount + '\'' +
                '}';
    }
}
