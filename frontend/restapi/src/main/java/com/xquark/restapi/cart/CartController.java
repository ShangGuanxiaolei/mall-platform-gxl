package com.xquark.restapi.cart;

import com.google.common.collect.ImmutableSet;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.FlashSalePromotionProductVO;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemGroupVO;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cart.vo.CartPromotionInfo;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.pintuan.PromotionPgPriceService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.VipProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.*;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.promotion.impl.PromotionSkusSerciceImpl;
import com.xquark.service.shop.ShopService;
import com.xquark.utils.DynamicPricingUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;

import static com.xquark.dal.type.PromotionType.*;

import static com.xquark.dal.type.PromotionType.*;

@Controller
@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
@Api(value = "cart", description = "购物车管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartController extends BaseController {

  @Autowired private CartService cartService;
  @Autowired private CouponService couponService;

  @Autowired private ShopAccessMapper shopAccessMapper;

  @Autowired private ShopService shopService;

  @Autowired private PromotionService promotionService;

  @Autowired private AddressService addressService;

  @Autowired private FlashSalePromotionProductService flashSalePromotionProductService;

  @Autowired private CartItemMapper cartItemMapper;

  @Autowired private PromotionOrderDetailService promotionOrderDetailService;

  @Autowired private SkuMapper skuMapper;

  @Autowired private PromotionSkusSerciceImpl promotionSkusService;

  @Autowired private PromotionSkuGiftService promotionSkuGiftService;

  @Autowired private PromotionInviteCodeService promotionInviteCodeService;

  @Autowired private PromotionBaseInfoService promotionBaseInfoService;
  @Autowired private PromotionConfigService promotionConfigService;
  @Autowired private PromotionPgPriceService pgPriceService;

  @Autowired
  private ProductService productService;
  @Autowired
  private FreshManProductService freshManProductService;
  @Autowired
  private FirstOrderService firstOrderService;
  @Autowired
  private FirstOrderMapper firstOrderMapper;
  @Autowired
  private PromotionSkusMapper promotionSkusMapper;
  @Autowired
  private FreshManService freshManService;

  @Autowired
  private VipProductService vipProductService;

  /**
   * 购物车
   */
  @RequestMapping(value = "/cart", method = RequestMethod.POST)
  @ResponseBody
  @ApiIgnore
  public ResponseObject<List<CartItemGroupVO>> cart() {
    List<CartItemVO> cartItems = cartService.checkout();
    Map<Shop, List<CartItemVO>> cartItemMap = new LinkedHashMap<>();
    for (CartItemVO item : cartItems) {
      List<CartItemVO> list = cartItemMap.get(item.getShop());
      if (list == null) {
        list = new ArrayList<>();
        cartItemMap.put(item.getShop(), list);
      }
      // TODO will do this in a proper way
      // 设置购物车中的图片大小为140宽度
      item.getProduct().setImgUrl(item.getProduct().getImg() + "|{w=140}");
      list.add(item);
    }

    List<CartItemGroupVO> result = new ArrayList<>();
    for (Entry<Shop, List<CartItemVO>> entry : cartItemMap.entrySet()) {
      CartItemGroupVO vo = new CartItemGroupVO();
      vo.setShop(entry.getKey());
      vo.setCartItems(entry.getValue());
      result.add(vo);
    }
    return new ResponseObject<>(result);
  }

  /** 供web和app购物车请求数据使用 */
  @RequestMapping(value = "/cart/getCart", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<Map> getCart(String shopId,
      HttpServletRequest req) {
    Integer cartStatus = 0;
    HashMap map = new HashMap();
    if (StringUtils.isEmpty(shopId)) {
      shopId = shopService.loadRootShop().getId();
    }
    if (shopId == null) {
      List<String> recentShopRawIds = shopAccessMapper.selectRecentVisitedShop(5);

      Map<String, Shop> recentShops = new HashMap<>();
      for (String recentShopRawId : recentShopRawIds) {
        String recentShopId = IdTypeHandler.encode(Long.parseLong(recentShopRawId));
        Shop recentShop = shopService.load(recentShopId);
        recentShops.put(recentShopId, recentShop);
      }
      map.put("recentShops", recentShops);
      cartStatus = 2;
    } else {
      Shop shop = shopService.load(shopId);
      map.put("shop", shop);
    }

    // 去掉参数中重复的skuId
    List<String> skuIds = new ArrayList<>();

    Integer amount = null;

    List<String> errors = new ArrayList<>();
    List<CartItemVO> cartItems;
    String nextUrl;

    if (skuIds != null && skuIds.size() > 0) {
      // 过滤相同的sku
      Set<String> skuIdSet = new LinkedHashSet<>(skuIds);
      if (skuIds.size() == 1 && amount != null) {
        String sid = (String) skuIds.toArray()[0];
        cartService.saveOrUpdateCartItemAmount(sid, amount);
      }
      try {
        cartItems = cartService.checkout(skuIdSet);
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems(skuIdSet);
      }
      nextUrl = "/cart/next?skuId=" + skuIdSet;
    } else if (StringUtils.isNotEmpty(shopId)) {
      try {
        /* ----
        * update by liubei 2015-11-16
        * disable cart list shop filter for new cart function
        * also disable amount and status check to avoid cart item implicit drop
            cartItems = cartService.checkout(shopId);
        ---- */
        cartItems = cartService.listCartItems(shopId);
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems(shopId);
      }
      nextUrl = "/cart/next?shopId=" + shopId;
    } else {
      try {
        cartItems = cartService.checkout();
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems();
      }
      nextUrl = "/cart/next";
    }

    // 可在购物车中实现修改购买规格
    Map<String, Integer> skuMap = new HashMap<>();
    Map<Shop, List<CartItemVO>> cartItemMap = new LinkedHashMap<>();

    User user = (User) getCurrentIUser();
    for (CartItemVO item : cartItems) {
      String productId = item.getProductId();
//      //判断是否是新人专区商品
//      List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(productId);
//      if(CollectionUtils.isNotEmpty(freshManProductVos)){
//        //是否是新人专区商品
//        item.setFreshmanProduct(true);
//      }

      //折扣促销商品 特殊处理
      List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectSalesPromotionByProductId(productId);
      if(CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)){
        item.setSaleProduct(true);
        item.setCornerTab(promotion4ProductSalesVOS.get(0).getCornerTab());
      }

      skuMap.put(item.getSkuId(), item.getAmount());

      Shop srcShop = item.getShop();
      List<CartItemVO> list = cartItemMap.get(srcShop);
      if (list == null) {
        list = new ArrayList<>();
        cartItemMap.put(srcShop, list);
      }

      Sku sku = item.getSku();
      if (sku != null) {
        ProductVO productVO = item.getProduct();
        // FIXME 临时处理, 把cartItem中的多规格值赋予商品, 在购物车中显示
        // 客户端调整后再去掉
        productVO.setName(item.getTitle());

        CartPromotionInfo info = new CartPromotionInfo();
        flashSalePromotionProductService.loadActivePromotionId(productId)
                .ifPresent(promotionId -> {
                  // 如果在活动中
                  info.setInFlashSale(true);
                  PromotionPgPrice pgPrice = pgPriceService.loadByPromotionIdAndSkuId(promotionId,
                          sku, FLASHSALE);
                  if (pgPrice != null) {
                      // 覆盖sku的价格体系
                      sku.setPrice(pgPrice.getPromotionPrice());
                      BeanUtils.copyProperties(pgPrice, sku);
                      sku.setPoint(pgPrice.getReduction());
                      sku.setDeductionDPoint(pgPrice.getPoint());
                  }
                });
        item.setPromotionInfo(info);

        // 重新计算sku价格
        DynamicPricingUtil.rePricing(item, user);
      }


      //是否售罄逻辑
      boolean commonProduct = this.productService.isCommonProduct(productId);
      if(commonProduct){//普通商品
        boolean b = this.productService.checkCommonProductSoldOut(productId);
        ProductVO productVO = item.getProduct();
        productVO.setIsSoldOut(b);
      }else {
        List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectByProductId(productId);
        boolean b = this.productService.checkGroupSoldOut(productId,promotionBaseInfos.get(0).getpCode());
        ProductVO productVO = item.getProduct();
        productVO.setIsSoldOut(b);
      }
      //新人标识
      ProductVO product = item.getProduct();
      if(!Objects.isNull(product)){
        FreshManProductVo f = this.freshManService.selectFreshmanProductById(productId);
        if(f != null && StringUtils.isNotBlank(f.getProductId())){
          item.setFreshmanProduct(true);
          product.setFreshmanProduct(true);
        }
      }
      list.add(item);
    }

    if (cartStatus == 2) {
      cartItemMap.clear();
    } else if (cartItemMap.size() > 0) {
      cartStatus = 1;
    }

    // 计算购物车费用
    // PricingResultVO prices = pricingService.calculate(skuMap, null, null);
    CartPromotionResultVO cartPromotionRet =
        promotionService.calculate4CartWithOutPromotions(cartItemMap, getCurrentUser(), null);

    map.put("prices", cartPromotionRet.getTotalPricingResult());
    map.put("shopPromotions", cartPromotionRet.getShopPromotions());
    map.put("shopCoupons", cartPromotionRet.getShopCoupons());
    map.put("cartItemCoupons", cartPromotionRet.getCartItemCoupons());
    map.put("cartItemPromotions", cartPromotionRet.getCartItemPromotions());
    map.put("shopPricingResult", cartPromotionRet.getShopPricingResult());
    map.put("cartStatus", cartStatus);

    // FIXME:to delete
    this.convretCartItems(cartItems);
    map.put("cartItems", cartItems);
    map.put("cartItemMap", cartItemMap);
    map.put("nextUrl", nextUrl);
    map.put("skuIds", skuIds);
    map.put("shopId", shopId);
    map.put("errors", errors);

    return new ResponseObject<>(map);
  }

  /** 商品加入购物车 */
  @RequestMapping(value = "/cart/add", method = RequestMethod.POST)
  @ResponseBody
  @ApiOperation(
      value = "商品加入购物车",
      notes = "商品加入购物车",
      httpMethod = "POST",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject add(
      @RequestParam("skuId") String skuId,
      @RequestParam("amount") Integer amount,
      @RequestParam("productId") String prodctId) {

    AtomicReference<PromotionType> promotionType = new AtomicReference<>(NORMAL);
    User user = (User) getCurrentIUser();

    // FIXME 针对分享的VIP订单, VIP商品不支持加入购物车
    List<VipProduct> vipProducts = vipProductService.listVipProducts();
    List<String> vipProductIds = null;
    if (CollectionUtils.isNotEmpty(vipProducts)) {
      vipProductIds = new ArrayList<>();
      for (VipProduct vipProduct : vipProducts) {
        vipProductIds.add(vipProduct.getProductId());
      }
    }
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    if (!PromotionType.VIP_RIGHT.equals(promotionType)
            && CollectionUtils.isNotEmpty(vipProductIds)
            && StringUtils.isNotBlank(sku.getProductId())) {
      if (vipProductIds.contains(sku.getProductId()))
        throw new BizException(GlobalErrorCode.ERROR_PARAM, "请至首页升级权益购买");
    }

    // 杭州大会售卖活动检查活动商品库存， 2018-10-07
    List<String> skuIds = new ArrayList<>();
    skuIds.add(skuId);
    List<String> promotionSkuIds = this.getPromotionSkuIds(skuIds);
    if (CollectionUtils.isNotEmpty(promotionSkuIds)) {
      boolean hasEnoughPromotionStock = this.hasEnoughStock(skuIds);
      if (!hasEnoughPromotionStock) {
        ResponseObject result = new ResponseObject<Integer>();
        result.setMoreInfo("活动商品已售罄。");
        return result;
      }

      promotionType.set(PromotionType.MEETING);
      // 杭州大会售卖活动检查活动商品库存， 2018-10-07
      // 杭州大会售卖活动检查是否超过活动金额， 2018-10-07
      //      boolean isOverUpValue = this.isOverUpValue(skuId, user, amount);
      //      if (isOverUpValue) {
      //        ResponseObject result = new ResponseObject<Integer>();
      //        result.setMoreInfo("购买总额已超过活动上限。");
      //        return result;
      //      }
      // 杭州大会售卖活动检查是否超过活动金额， 2018-10-07
    }
    //检查该商品是否为新人专区商品
    long productId = IdTypeHandler.decode(prodctId);
    Integer freshmanProduct = firstOrderMapper.countFreshmanProduct(productId);
    if(freshmanProduct > 0){
      boolean checkStock = firstOrderService.checkStock(productId);
      if (!checkStock) {
        ResponseObject result = new ResponseObject<Integer>();
        result.setMoreInfo("活动商品已售罄。");
        return result;
      }
      promotionType.set(PromotionType.FRESHMAN);
    }

    //查看是否是促销商品
    List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectExsitSalesPromotionByProductId(prodctId);
    //购物车已有件数
    Integer cartCount = 0;
    List<CartItemVO> cartItemVOS = cartService.listCartItems();
    for (CartItemVO cartItemVO : cartItemVOS) {
      if(skuId.equals(cartItemVO.getSkuId())){
        cartCount=cartItemVO.getAmount();
        sku = productService.loadSku(skuId);
        if (sku == null) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品规格[id=" + skuId + "]不存在， 不能购买");
        }
        // 检查商品状态和库存
        ProductVO product = productService.load(sku.getProductId());
        cartService.checkStadiumProductBuyLimit(cartCount + amount, sku, product);
      }
    }

    //促销折扣商品限购数量校验,购物车该商品的数量
    if(CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)){
      promotionType.set(PromotionType.PRODUCT_SALES);

      if (promotion4ProductSalesVOS.get(0).getEffectTo().before(new Date())) {
        ResponseObject result = new ResponseObject<Integer>();
        result.setMoreInfo("商品活动已结束");
        return result;
      }
      if (promotion4ProductSalesVOS.get(0).getEffectFrom().after(new Date())) {
        ResponseObject result = new ResponseObject<Integer>();
        result.setMoreInfo("商品活动未开始");
        return result;
      }
      Integer buyCountlimit   = promotionSkusMapper.getBuyCountLimitBySkuId(skuId);
      if(null!=buyCountlimit){
        Integer buyCount  = promotionSkusMapper.getBuyCountBuySkuId(user.getId(), skuId);
        if(amount >(buyCountlimit-buyCount-cartCount)){
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "促销限购" +buyCountlimit + "件哦");

        }
      }
    }

    String rootShopId = getCurrentUser().getCurrentSellerShopId();

    // 如果是秒杀或者一元购商品则修改类型
    loadFlashSalePromotion(skuId).ifPresent(pm -> promotionType.set(pm.getPromotionType()));

    // 目前仅支持购物车里面只有一个活动商品, 根据规则调整互斥逻辑
    if (!isActivity(promotionType.get(), cartService.getCartPromotionType())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "只允许添加一个活动商品。");
    }

    flashSalePromotionProductService.checkPromotionStart(prodctId);

    cartService.addToCart(user.getId(), skuId, amount, rootShopId, prodctId);

    //判断是否为新人专区商品
//    long productId = IdTypeHandler.decode(prodctId);
//    FreshmanProduct freshmanProduct = firstOrderService.freshmanProduct(productId);
//    if(freshmanProduct.getCountProduct() > 0){
//      //剩余限购个数 = 后台管理配置-已下单购买数
//      long userId = IdTypeHandler.decode(user.getId());
//      Integer productNumber = firstOrderService.countProductNumber(userId);
//      if(productNumber < 1){
//        throw  new BizException(GlobalErrorCode.INTERNAL_ERROR, "您新人专区商品限额已用完");
//      }
//      //购物车内新人专区商品数
//      Integer countCart = firstOrderService.countCart(userId);
//      //剩餘限購數
//      Integer surplus = productNumber - countCart;
//      if(surplus < 0 ){
//        throw  new BizException(GlobalErrorCode.INTERNAL_ERROR, "您新人专区商品限额已用完");
//      }
//      //如果加入购物车商品大于限购数，则报错
//      if(amount > surplus){
//        String str = numberConversionLetter(surplus);
//        throw  new BizException(GlobalErrorCode.INTERNAL_ERROR, "您只允许添加"+ str +"个新人专区商品。");
//      }else {
//        ResponseObject responseObject = new ResponseObject<Integer>();
//        Map map = new HashMap();
//        map.put("surplusCount",surplus);
//        responseObject.setData(map);
//        return responseObject;
//      }
//    }

    List<CartItemVO> cartItems = cartService.checkout();
    Integer count = 0;
    for (CartItemVO cartItemVO : cartItems) {
      if (cartItemVO.getAmount() > 0) {
        count++;
      }
    }

    return new ResponseObject<>(count);
  }

  /** 购物车商品数量 */
  @RequestMapping(value = "/cart/count", method = RequestMethod.POST)
  @ResponseBody
  @ApiOperation(
      value = "购物车商品数量",
      notes = "购物车商品数量",
      httpMethod = "POST",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Integer> count(
      @RequestHeader(value = "User-DeviceId", required = false) String deviceId) {
    // TODO 性能待改进
    Integer count;
    count = cartService.count();

    // FIXME 6.1活动发放优惠券，临时方法，用3天。没其他地方调用
    User user = getCurrentUser();
    couponService.autoGrantCoupon(user.getPartner(), user.getId(), deviceId);
    //        if("xiangqu".equalsIgnoreCase(user.getPartner())){
    //            couponService.grantCoupon("XQ.61", user.getId(), deviceId);
    //        }

    /*
      List<CartItemVO> cartItems = cartService.checkout();
      for (CartItemVO cartItemVO : cartItems) {
          if (cartItemVO.getAmount() > 0) {
              count++;
          }
      }
    */
    return new ResponseObject<>(count);
  }

  /** 购物车商品数量更新 */
  @ResponseBody
  @RequestMapping(value = "/cart/update", method = RequestMethod.POST)
  @ApiOperation(
      value = "购物车商品数量更新",
      notes = "购物车商品数量更新",
      httpMethod = "POST",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> update(
      @RequestParam("id") String id,
      @RequestParam("amount") Integer amount,
      @RequestParam(value = "newSkuId", required = false) String newSkuId) {

      //购物车新人专区商品数量限制

    //根据skuid判断
//    List<FreshManProductVo> freshManProductVos = freshManProductService.selectBySkuId(id);
//    //如果当前操作商品是新人专区商品
//    if(CollectionUtils.isNotEmpty(freshManProductVos)){
//      //查询当前登入人的购物车内容
//      int cartFreshmanProductAmount = 0 ;
//      List<CartItemVO> cartItemVOS = cartService.listCartItems();
//      if(CollectionUtils.isNotEmpty(cartItemVOS)) {
//        for (CartItemVO cartItemVO : cartItemVOS) {
//          if (id.equals(cartItemVO.getSkuId())){
//            continue;
//          }
//          List<FreshManProductVo> cartFreshManProductVos = freshManProductService.selectBySkuId(cartItemVO.getSkuId());
//          if(CollectionUtils.isNotEmpty(cartFreshManProductVos)){
//            cartFreshmanProductAmount=cartFreshmanProductAmount+cartItemVO.getAmount();
//          }
//        }
//      }
//      //查询当前登入人 还可以购买的新人专区商品数量
//      User user = (User) getCurrentUser();
//      Integer canBuyCount = firstOrderService.countProductNumber(user.getCpId());
//      Integer findfreshmanValue = firstOrderMapper.findfreshmanValue();
//
//      //校验购物车下单的 新人专区商品数量不能超过限购
//      if(canBuyCount<(cartFreshmanProductAmount+amount)){
//        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "新人福利每人限购" +findfreshmanValue+ "件哦");
//      }
//
//    }
    //查看是否是促销商品
    List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectSalesPromotionBySkuId(id);
    if(CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)){
      //促销折扣商品限购数量校验,购物车该商品的数量
      if(CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)){
        Integer buyCountlimit   = promotionSkusMapper.getBuyCountLimitBySkuId(id);
        if(null!=buyCountlimit){
          User user = (User) getCurrentUser();

          Integer buyCount  = promotionSkusMapper.getBuyCountBuySkuId(user.getId(), id);

          if(amount >(buyCountlimit-buyCount)){
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "促销限购" +buyCountlimit + "件哦");
          }
        }
      }
    }

    CartItem ci = cartService.saveOrUpdateCartItemAmount(id, amount);
    // 更新skuid
    if (StringUtils.isNotEmpty(newSkuId)) {
      CartItem updateCi = new CartItem();
      updateCi.setId(ci.getId());
      updateCi.setSkuId(newSkuId);
      cartItemMapper.updateByPrimaryKeySelective(updateCi);
    }
    return new ResponseObject<>(ci != null);
  }

  private void checkSaleProductLimit(){

  }


  @ResponseBody
  @RequestMapping(value = "/cart/delete", method = RequestMethod.POST)
  @ApiOperation(
      value = "删除购物车内某个条目",
      notes = "删除购物车内某个条目",
      httpMethod = "POST",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> delete(@RequestParam("id") String id) {
    return new ResponseObject<>(cartService.remove(id));
  }

  @ResponseBody
  @RequestMapping(value = "/cart/batchDelete", method = RequestMethod.POST)
  public ResponseObject<List<String>> batchDelete(@RequestParam("ids") String[] ids) {
    if (ids == null || ids.length == 0) {
      return new ResponseObject<>(Collections.<String>emptyList());
    }
    List<String> deleted = new ArrayList<>();
    for (String id : ids) {
      try {
        cartService.remove(id);
        deleted.add(id);
      } catch (Exception e) {
        log.error("购物车id {} 删除失败");
      }
    }
    return new ResponseObject<>(deleted);
  }

  @ResponseBody
  @RequestMapping(value = "/cart/clear", method = RequestMethod.POST)
  @ApiOperation(
      value = "清空购物车",
      notes = "清空购物车",
      httpMethod = "POST",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> clear() {
    cartService.clear();
    return new ResponseObject<>(Boolean.TRUE);
  }

  /** 获取用户在某个商铺下的购物车内商品总数 */
  @RequestMapping(value = "/cart/countByShopId", method = RequestMethod.POST)
  @ResponseBody
  @ApiIgnore
  public ResponseObject<Integer> countByShopId(@RequestParam("shopId") String shopId) {
    Integer count = 0;
    String userId = this.getCurrentIUser().getId();
    count = cartService.countByShopId(userId, shopId);
    return new ResponseObject<>(count);
  }

  /** 包含活动等优惠信息的价格计算 */
  @RequestMapping(value = "/cart/pricingPromotions", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<CartPromotionResultVO> pricingPromotions(
      @Valid @ModelAttribute CartPricingForm form) {
    List<CartItemVO> cartItems = new ArrayList<>();
    Integer qty = form.getQty();
    Set<String> skuIds = new LinkedHashSet<>(form.getSkuIds());

    User user = getCurrentUser();
    String realShopId = user.getCurrentSellerShopId();
    if (CollectionUtils.isNotEmpty(skuIds)) {
      // 过滤相同的sku
      Set<String> skuIdSet = new LinkedHashSet<>(skuIds);
      try {
        cartItems = cartService.checkout(skuIdSet, realShopId, form.getPromotionFrom());
      } catch (BizException e) {
        cartItems = cartService.listCartItems(skuIdSet);
      }
    }

    // 可在购物车中实现修改购买规格
    Map<Shop, List<CartItemVO>> cartItemMap = buildCartItemMap(cartItems, qty);

    AddressVO addressVO = null;
    if (StringUtils.isNotEmpty(form.getAddressId())) {
      addressVO = addressService.loadUserAddress(form.getAddressId());
    }

    Shop rootShop = shopService.loadRootShop();
    List<String> usingCoupons = form.getUsingCoupons();
    UserSelectedProVO userSelectedProVO = UserSelectedProVO.build(rootShop, usingCoupons);
    // TODO wangxinhua 后期需要加入活动商品
    CartPromotionResultVO cartPromotionRet =
        promotionService.calculate4Cart(
            cartItemMap,
            getCurrentUser(),
            userSelectedProVO,
            addressVO,
            false,
            false,
            form.getPromotionInfo(),
            PricingMode.CONFIRM);

    return new ResponseObject<>(cartPromotionRet);
  }

  /** 该接口计算价格时不包含优惠信息 */
  @RequestMapping(value = "/cart/pricing", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<CartPromotionResultVO> pricing(
          @Valid @ModelAttribute CartPricingForm form, HttpServletRequest req, Model model) {
    List<CartItemVO> cartItems = new ArrayList<>();
    Integer qty = form.getQty();
    Set<String> skuIds = new LinkedHashSet<>(form.getSkuIds());

    String realShopId = shopService.loadRootShop().getId();
    if (CollectionUtils.isNotEmpty(skuIds)) {
      // 过滤相同的sku
      Set<String> skuIdSet = new LinkedHashSet<>(skuIds);
      try {
        cartItems = cartService.checkout(skuIdSet, realShopId, null);
      } catch (BizException e) {
        cartItems = cartService.listCartItems(skuIdSet);
      }
    }

    // 可在购物车中实现修改购买规格
    Map<Shop, List<CartItemVO>> cartItemMap = buildCartItemMap(cartItems, qty);

    AddressVO addressVO = null;
    if (StringUtils.isNotEmpty(form.getAddressId())) {
      addressVO = addressService.loadUserAddress(form.getAddressId());
    }

    // 计算购物车费用
    CartPromotionResultVO cartPromotionRet =
        promotionService.calculate4Cart(
            cartItemMap, getCurrentUser(), null, addressVO,
                false, false, null, PricingMode.CART);
    return new ResponseObject<>(cartPromotionRet);
  }

  /**
   * 通过购物车商品构建map
   *
   * @param cartItems 购物车商品
   * @param directBuyQty 直接购买数量, 该值只在直接购买时会传入
   * @return 构建好的map
   */
  private Map<Shop, List<CartItemVO>> buildCartItemMap(
          List<CartItemVO> cartItems, Integer directBuyQty) {
    if (CollectionUtils.isEmpty(cartItems)) {
      return Collections.emptyMap();
    }
    Map<Shop, List<CartItemVO>> cartItemMap = new LinkedHashMap<>();
    for (CartItemVO item : cartItems) {
      if (directBuyQty != null) {
        item.setAmount(directBuyQty);
      }

      Shop srcShop = item.getShop();
      List<CartItemVO> list = cartItemMap.get(srcShop);
      if (list == null) {
        list = new ArrayList<>();
        cartItemMap.put(srcShop, list);
      }
      list.add(item);
    }
    return cartItemMap;
  }

  /**
   * 检查参加活动的用户购买总金额是否超过
   *
   * @param skuIds
   * @param user
   * @return
   */
  private boolean isOverUpValue(List<String> skuIds, User user) {
    List<String> promotionSkuIds = this.getPromotionSkuIds(skuIds);
    // 判断有无超过上限
    boolean isOver = promotionOrderDetailService.isOverUpValue(promotionSkuIds, user, 1000, "");
    return isOver;
  }

  private boolean isOverUpValue(String skuId, User user, int qty) {
    List<String> skuIds = new ArrayList<String>();
    skuIds.add(skuId);
    List<String> promotionSkuIds = this.getPromotionSkuIds(skuIds);
    // no promotion sku == true
    if (CollectionUtils.isEmpty(promotionSkuIds)) {
      return false;
    }
    // 查询活动金额上限
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    PromotionConfig top_value =
        promotionConfigService.selectBySkuCodeAndConfigName(sku.getSkuCode(), "top_value");

    // 判断有无超过上限
    boolean isOver =
        promotionOrderDetailService.isOverUpValue(
            skuId, user, qty, Integer.valueOf(top_value.getConfigValue()));
    return isOver;
  }

  /**
   * 指定活动商品有无库存
   *
   * @param skuIds
   * @return
   */
  public boolean hasEnoughStock(List<String> skuIds) {
    List<String> promotionSkuIds = this.getPromotionSkuIds(skuIds);
    // 没有活动商品，则不做活动库存检查
    boolean isEnough = true;
    if (CollectionUtils.isEmpty(skuIds)) {
      return isEnough;
    }
    isEnough = promotionSkusService.hasEnoughStock(promotionSkuIds);
    return isEnough;
  }

  /**
   * 过滤活动商品
   *
   * @param skuIds
   * @return
   */
  private List<String> getPromotionSkuIds(List<String> skuIds) {
    List<String> promotionSkuIds = new ArrayList<String>();
    for (String skuId : skuIds) {
      Sku sku = skuMapper.selectByPrimaryKey(skuId);
      SkuExtend skuEx = new SkuExtend();
      BeanUtils.copyProperties(sku, skuEx);

      // 检查这个商品有无活动，没有则认为是没有赠品
      List<PromotionSkuVo> promotionSkuVos =
          promotionSkusService.getPromotionSkus(skuEx.getSkuCode());
      if (CollectionUtils.isEmpty(promotionSkuVos)) {
        continue;
      }
      promotionSkuIds.add(sku.getId());
    }
    return promotionSkuIds;
  }

  private Optional<PromotionProductVO> loadFlashSalePromotion(String skuId) {
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    FlashSalePromotionProductVO flashSalePromotionProductVO =
        (FlashSalePromotionProductVO)
            flashSalePromotionProductService.loadPromotionProductByProductId(sku.getProductId());
    return Optional.ofNullable(flashSalePromotionProductVO);
  }

  /**
   * @param promotionTypeLocal 将要加入的商品
   * @param promotionTypeCart 已经加入的商品
   * @return
   */
  private boolean isActivity(PromotionType promotionTypeLocal, PromotionType promotionTypeCart) {
    boolean isActivity = true;
    switch (promotionTypeLocal) {
      case NORMAL:
        break;
      case MEETING:
        if (SALEZONE.equals(promotionTypeCart) || FLASHSALE.equals(promotionTypeCart) || FRESHMAN.equals(promotionTypeCart)|| PRODUCT_SALES.equals(promotionTypeCart)) {
          isActivity = false;
        }
        break;
      case SALEZONE:
        if (!promotionTypeCart.equals(PromotionType.NORMAL)) {
          isActivity = false;
        }
        break;
      case FLASHSALE:
        // 秒杀跟秒杀不互斥
        if (!ImmutableSet.of(NORMAL, FLASHSALE,PRODUCT_SALES).contains(promotionTypeCart)) {
          isActivity = false;
        }
        break;
      case FRESHMAN:
        if (SALEZONE.equals(promotionTypeCart) || FLASHSALE.equals(promotionTypeCart) || MEETING.equals(promotionTypeCart) || PRODUCT_SALES.equals(promotionTypeCart)) {
          isActivity = false;
        }
        break;
      case PRODUCT_SALES:
        if (SALEZONE.equals(promotionTypeCart) || MEETING.equals(promotionTypeCart) || FRESHMAN.equals(promotionTypeCart)) {
            isActivity = false;
        }
        break;
      default:
        break;
    }
    return isActivity;
  }

  /**
   * 为订单商品增加赠品信息
   *
   * @param cartItems
   */
  private void convretCartItems(List<CartItemVO> cartItems) {
    if (CollectionUtils.isEmpty(cartItems)) {
      return;
    }
    int meetingPromotionCount = 0;
    // 为提交订单接口准备数据：参加大会售卖活动的skuId
    List<String> promotionSkuIds = new ArrayList<>();
    for (CartItemVO cartItem : cartItems) {
      Sku sku = cartItem.getSku();
      SkuExtend skuEx = new SkuExtend();
      BeanUtils.copyProperties(sku, skuEx);

      // 根据商品sku获取活动信息
      List<PromotionBaseInfo> promotionBaseInfos =
          promotionBaseInfoService.selectBySkuCode(skuEx.getSkuCode());
      if (CollectionUtils.isEmpty(promotionBaseInfos)) {
        continue;
      }
      String pCode = promotionBaseInfos.get(0).getpCode();

      // 检查这个商品有无活动，没有则认为是没有赠品
      List<PromotionSkuVo> promotionSkuVos =
              promotionSkusService.getPromotionSkus(skuEx.getSkuCode());
      if (CollectionUtils.isEmpty(promotionSkuVos)) {
        continue;
      }
      // TODO 加活动类型判断
      // if(杭州大会活动){
      meetingPromotionCount++;
      promotionSkuIds.add(sku.getId());

      PromotionSkuVo promotionSkuVo = promotionSkuVos.get(0); // TODO 变为get(i)
      // 检查这个活动商品有没有赠品，
      List<PromotionSkuGift> giftList =
          promotionSkuGiftService.getPromotionSkuGiftList(pCode, promotionSkuVo.getSkuCode());
      if (CollectionUtils.isEmpty(giftList)) {
        continue;
      }
      // step1. 设置有无赠品
      skuEx.setHadGift(true);

      // 买赠信息 （目前为买gift赠1）
      int gift = promotionSkuVo.getGift();
      int giftCount = 0;
      // 计算赠品数量
      if (gift > 0) {
        // 舍掉小数取整
        giftCount = (int) Math.floor(cartItem.getAmount() / gift);
      }
      String str = this.numberConversionLetter(gift);
      // 设置赠品数量
      skuEx.setGiftType("买" + str + "赠一");
      skuEx.setGiftCount(giftCount);
      cartItem.setSku(skuEx);
      // }
    }
  }

  private String numberConversionLetter(int num){
    String str = "";
    switch (num) {
      case 0:
        str = "";
        break;
      case 1:
        str = "一";
        break;
      case 2:
        str = "二";
        break;
      case 3:
        str = "三";
        break;
      case 4:
        str = "四";
        break;
      case 5:
        str = "五";
        break;
      case 6:
        str = "六";
        break;
      case 7:
        str = "七";
        break;
      case 8:
        str = "八";
        break;
      case 9:
        str = "九";
        break;
      case 10:
        str = "十";
        break;
    }
    return str;
  }
}
