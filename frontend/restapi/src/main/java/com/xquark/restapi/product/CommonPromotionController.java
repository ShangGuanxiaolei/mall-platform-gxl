package com.xquark.restapi.product;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.PromotionBaseInfoMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.dal.model.PromotionNameListVO;
import com.xquark.dal.vo.CommonPromotionProductVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.service.promotion.PromotionTempStockService;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@Api(value = "限时抢购接口")
@RestController
@RequestMapping("/commonSale")
public class CommonPromotionController extends BaseController {

  private FlashSalePromotionProductService flashSalePromotionService;
  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private PromotionConfigService promotionConfigService;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PromotionSkusService promotionSkusService;

  @Autowired
  private PromotionBaseInfoMapper promotionBaseInfoMapper;
  @Autowired
  private PromotionTempStockService promotionTempStockService;

  @Autowired
  public CommonPromotionController(
      FlashSalePromotionProductService flashSalePromotionService) {
    this.flashSalePromotionService = flashSalePromotionService;
  }

  @RequestMapping(value = "/up2Redis", method = RequestMethod.POST)
  @ApiOperation(value = "上传库存至Redis", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> savePreOrder(@Validated CommonActivitySaleForm form,
      @ApiIgnore Errors errors) {

//    ControllerHelper.checkException(errors);
    //PromotionConfig pc = promotionConfigService.selectByConfigType("meeting_pid");
    String promotionId = "meeting";
    if(null!=form.getPromotionId()){

        promotionId = form.getPromotionId();
    }

    // 将值放到redis中
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(promotionId, form.getSkuCode());
    redisUtils.set(activitySkuAmountKey, form.getAmount().intValue());
    return new ResponseObject<>(true);
  }

  @RequestMapping(value = "/listRedisSkus", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listForPc(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "desc") String direction, Pageable pageable,
  @RequestParam(defaultValue = "pCode") String pCode) {

      List<String> skuCodes= new ArrayList<>();

    List<CommonPromotionProductVO> list = new ArrayList<>();
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String promotionId = "meeting";
    List<PromotionSkuVo> promotionSkusByPcode = promotionSkusService.getPromotionSkusByPcode(pCode);
    for (PromotionSkuVo promotionSkuVo : promotionSkusByPcode) {
      skuCodes.add(promotionSkuVo.getSkuCode());
    }
    promotionId=pCode;
    for (String skuCode : skuCodes) {
      String redisKey = PromotionConstants.getCommonAmountKey(promotionId, skuCode);
      redisUtils.get(redisKey);
      CommonPromotionProductVO vo = new CommonPromotionProductVO();
      vo.setSkuCode(skuCode);
      vo.setpCode(pCode);
      if (null != redisUtils.get(redisKey)) {
        vo.setAmount((long) redisUtils.get(redisKey));
        list.add(vo);
      }

    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", 0);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/ttestUp2Redis", method = RequestMethod.POST)
  public ResponseObject<Boolean> savePre(@RequestParam("skuCode")String skuCode,@RequestParam("amount")Integer amount) {


    String promotionId = "meeting";
    if(null!=skuCode){
      List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoMapper.selectEffectiveBySkuCode(skuCode);

      if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
        promotionId = promotionBaseInfos.get(0).getpCode();
      }
    }

    // 将值放到redis中
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(promotionId, skuCode);
    redisUtils.set(activitySkuAmountKey, amount.intValue());
    return new ResponseObject<>(true);
  }

    /**
     * 功能描述:手动同步redis大会活动到db
     * @author Luxiaoling
     * @param
     * @return  com.xquark.restapi.ResponseObject<java.lang.Boolean>
     */
  @RequestMapping(value = "/setRedisPersistence", method = RequestMethod.POST)
  public ResponseObject<Boolean> redisToPersistence() {
    ResponseObject ro = new ResponseObject(true,GlobalErrorCode.SUCESS);

    PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
    String lock_type = "";
    if (null == lock_type_config) {
      lock_type = "DB";
    }
    lock_type = lock_type_config.getConfigValue();

    if ("DB".equals(lock_type)) {

      return new ResponseObject(false,"活动库存配置为DB时无需同步",GlobalErrorCode.INTERNAL_ERROR);
    }

    try {
    this.promotionTempStockService.synchroMeetingStock2DB();
    ro.setData(true);
    }catch (Exception e){
      log.error("库存redis持久化手动生成异常："+e.getMessage());
      ro = new ResponseObject(false,"服务器异常",GlobalErrorCode.INTERNAL_ERROR);
    }
    return ro;
  }

    /**
     * 功能描述: 大会活动redis库存查询 下拉列表获取有效活动
     * @author Luxiaoling
     * @date 2019/2/18 15:58
     * @param
     * @return
     */
    @RequestMapping(value = "/getEffectivePromotionName", method = RequestMethod.POST)
    public ResponseObject<Map<String, Object>> getEffectivePromotionName() {

        Map<String, Object> result = new HashMap<>();

        List<PromotionNameListVO> promotionNameList = promotionBaseInfoService.selectPromotionName();
        result.put("promotionNameList",promotionNameList);
        ResponseObject<Map<String, Object>> ro = new ResponseObject(result,GlobalErrorCode.SUCESS);
        return ro;
    }

}
