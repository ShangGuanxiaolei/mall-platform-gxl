package com.xquark.restapi.point.vo;

/**
 * @author Jack Zhu
 * @since 2018/12/17
 */
public class PointGiftNoVO {
    private String pointGiftNo;

    public PointGiftNoVO(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }
}
