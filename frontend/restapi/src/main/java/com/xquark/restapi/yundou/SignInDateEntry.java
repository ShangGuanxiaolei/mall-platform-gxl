package com.xquark.restapi.yundou;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.utils.DateUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wangxinhua on 17-11-24. DESC:
 */
@ApiModel
public class SignInDateEntry implements Serializable {

  private static final long serialVersionUID = -5835659805438209132L;

  private static SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("MM.dd");

  @ApiModelProperty(value = "日期")
  private final Date date;

  @ApiModelProperty(value = "签到后能得到的积分")
  private final Long signInPoints;

  @ApiModelProperty(value = "该天是否已经签到过")
  private final boolean isSignIn;

  SignInDateEntry(Date date, Long signInPoints, boolean isSignIn) {
    this.date = date;
    this.signInPoints = signInPoints;
    this.isSignIn = isSignIn;
  }

  public Date getDate() {
    return date;
  }

  public Long getSignInPoints() {
    return signInPoints;
  }

  public String getDateStr() {
    if (getIsToday()) {
      return "今日";
    }
    if (DateUtils.isSameDay(date, DateUtils.getNextDayStart(new Date()))) {
      return "明日";
    }
    return DATE_TIME_FORMAT.format(date);
  }

  public boolean getIsToday() {
    if (getDate() == null) {
      return false;
    }
    return DateUtils.isToday(date);
  }

  public boolean isSignIn() {
    return isSignIn;
  }

}
