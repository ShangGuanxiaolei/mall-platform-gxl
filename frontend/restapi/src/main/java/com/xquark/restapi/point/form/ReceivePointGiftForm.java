package com.xquark.restapi.point.form;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;

public class ReceivePointGiftForm {

    /**
     * 抢红包积分用户Id
     */
    private String receiveUserId;

    /**
     * 发红包积分用户Id
     */
    private String grantUserId;

    /**
     * 积分红包Id
     */
    private String pointGiftId;

    /**
     * 赠送积分红包
     */
    private BigDecimal giftPoint;

    /**
     * 赠言
     */
    private String leaveWord;

    /**
     * 红包编号
     */
    private  String pointGiftNo;

    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public BigDecimal getGiftPoint() {
        return giftPoint;
    }

    public void setGiftPoint(BigDecimal giftPoint) {
        this.giftPoint = giftPoint;
    }

    public String getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getGrantUserId() {
        return grantUserId;
    }

    public void setGrantUserId(String grantUserId) {
        this.grantUserId = grantUserId;
    }

    public String getPointGiftId() {
        return pointGiftId;
    }

    public void setPointGiftId(String pointGiftId) {
        this.pointGiftId = pointGiftId;
    }

    public static void main(String[] args) {

        final ReceivePointGiftForm receivePointGiftForm = new ReceivePointGiftForm();
        receivePointGiftForm.setPointGiftNo("");
        receivePointGiftForm.setLeaveWord("");
        receivePointGiftForm.setReceiveUserId("");
        receivePointGiftForm.setPointGiftId("ss");
        receivePointGiftForm.setGrantUserId("ss");
        receivePointGiftForm.setGiftPoint(BigDecimal.TEN);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            final String s = objectMapper.writeValueAsString(receivePointGiftForm);
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
