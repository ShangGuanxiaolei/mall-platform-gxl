package com.xquark.restapi.im;

import java.math.BigDecimal;
import java.util.Date;

import com.xquark.dal.BaseEntityImpl;

public class OrderItemEx extends BaseEntityImpl {

  private static final long serialVersionUID = 1L;

  private String orderId;

  private String productId;

  private String skuId;

  private String productName;

  private String skuStr;

  private String productImg;

  private String productImgUrl;

  private BigDecimal price;

  private BigDecimal marketPrice;

  private Integer amount;

  private Boolean special;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(BigDecimal marketPrice) {
    this.marketPrice = marketPrice;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public String getProductImgUrl() {
    return productImgUrl;
  }

  public void setProductImgUrl(String productImgUrl) {
    this.productImgUrl = productImgUrl;
  }

  public String getTitle() {
    return this.productName;
  }

  public Boolean getSpecial() {
    return special;
  }

  public void setSpecial(Boolean special) {
    this.special = special;
  }

}