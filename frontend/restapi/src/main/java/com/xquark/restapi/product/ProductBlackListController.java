package com.xquark.restapi.product;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.ProductBlackList;
import com.xquark.dal.vo.ProductBlackListVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.product.ProductBlackListService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class ProductBlackListController extends FragmentAndDescController {

  @Autowired
  private ProductBlackListService productBlackListService;


  /**
   * 查询某个合伙人用户所有的黑名单商品列表信息
   */
  @ResponseBody
  @RequestMapping("/productBlack/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, HttpServletRequest request,
      @RequestParam String userId) {
    Map<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    List<ProductBlackListVO> result = productBlackListService.list(pageable, params);

    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", productBlackListService.selectCnt(params));
    aRetMap.put("list", result);

    return new ResponseObject<>(aRetMap);
  }

  /**
   * 客户端用户获取自己设置的黑名单商品列表
   */
  @ResponseBody
  @RequestMapping("/productBlack/listByApp")
  public ResponseObject<List<ProductBlackListVO>> listByApp(Pageable pageable,
      HttpServletRequest request) {
    String userId = getCurrentIUser().getId();
    Map<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    List<ProductBlackListVO> result = productBlackListService.list(pageable, params);
    return new ResponseObject<>(result);
  }


  /**
   * 新增黑名单商品
   */
  @ResponseBody
  @RequestMapping("/productBlack/save")
  public ResponseObject<Boolean> save(@RequestParam String productId) {
    String userId = getCurrentIUser().getId();
    ProductBlackList blackList = new ProductBlackList();
    blackList.setProductId(productId);
    blackList.setArchive(false);
    blackList.setUserId(userId);
    int result = productBlackListService.insert(blackList);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 删除黑名单商品
   */
  @ResponseBody
  @RequestMapping("/productBlack/delete")
  public ResponseObject<Boolean> delete(@RequestParam String productId) {
    String userId = getCurrentIUser().getId();
    int result = productBlackListService.deleteByProductId(userId, productId);
    return new ResponseObject<>(result > 0);
  }

}
