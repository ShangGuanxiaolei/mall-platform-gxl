package com.xquark.restapi.app;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.Version;
import com.xquark.dal.status.AppUpdateType;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.app.VersionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

/**
 * app更新controller Created by chh on 17-4-1.
 */
@Controller
@ApiIgnore
public class VersionController extends BaseController {

  @Autowired
  private VersionService versionService;

  @Autowired
  private UserService userService;

  /**
   * 获取app更新配置页面列表
   */
  @ResponseBody
  @RequestMapping("/version/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword,
      @RequestParam String appType) {
    String shopId = this.getCurrentIUser().getShopId();
    List<Version> viewPages = null;
    viewPages = versionService.list(pageable, appType);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", versionService.selectCnt(appType));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存app更新配置
   */
  @ResponseBody
  @RequestMapping("/version/save")
  public ResponseObject<Boolean> save(Version versionForm) {
    int result = 0;
    Version version = new Version();
    BeanUtils.copyProperties(versionForm, version);
    if (StringUtils.isNotEmpty(version.getId())) {
      result = versionService.modify(version);
    } else {
      version.setArchive(false);
      result = versionService.insert(version);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/version/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = versionService.delete(id);
    return new ResponseObject<>(result > 0);
  }


  /**
   * 查看某个具体的app更新配置记录<br>
   */
  @ResponseBody
  @RequestMapping("/version/{id}")
  public ResponseObject<Version> view(@PathVariable String id, HttpServletRequest req) {
    Version version = versionService.selectByPrimaryKey(id);
    if (version == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    return new ResponseObject<>(version);
  }

  /**
   * 根据传入的app类型与版本号，查询app更新类型
   */
  @ResponseBody
  @RequestMapping("/version/selectByVersion")
  public ResponseObject<Version> selectByVersion(@RequestParam String version,
      @RequestParam String appType, HttpServletRequest req) {
    List<Version> versionVOs = versionService.selectByVersion(appType, version);
    // 默认不更新
    Version result = new Version();
    result.setType(AppUpdateType.NO);
    // 是否有强制更新的版本
    boolean isForce = false;
    for (int i = 0, n = versionVOs.size(); i < n; i++) {
      Version versionVO = versionVOs.get(i);
      // 将最大的版本号和更新类型返回给app使用
      if (i == 0) {
        result.setVersion(versionVO.getVersion());
        result.setType(versionVO.getType());
        result.setDescription(versionVO.getDescription());
        result.setUrl(versionVO.getUrl());
      }
      if (versionVO.getType() == AppUpdateType.FORCE) {
        isForce = true;
      }
    }
    if (isForce) {
      result.setType(AppUpdateType.FORCE);
    }
    return new ResponseObject<>(result);
  }


}
