package com.xquark.restapi.tweet;

import com.xquark.dal.model.MarketingTweet;

/**
 * Created by root on 16-10-19.
 */
public class TweetForm extends MarketingTweet {

  private String imgs;

  public String getImgs() {
    return imgs;
  }

  public void setImgs(String imgs) {
    this.imgs = imgs;
  }
}
