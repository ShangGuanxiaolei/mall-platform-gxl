package com.xquark.restapi.promotion;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.PromotionBargainDetail;
import com.xquark.dal.model.PromotionBargainHistory;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.PromotionBargainDetailUserVO;
import com.xquark.dal.vo.PromotionBargainPlaceVO;
import com.xquark.dal.vo.PromotionBargainSkuVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.bargain.PromotionBargainDetailService;
import com.xquark.service.bargain.PromotionBargainService;
import com.xquark.service.error.GlobalErrorCode;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 砍价接口
 *
 * @author wangxinhua
 * @date 2019-04-06
 * @since 1.0
 */
@RestController
@RequestMapping("/bargain")
public class PromotionBargainController extends BaseController {

  private final PromotionBargainService bargainService;

  private final PromotionBargainDetailService bargainDetailService;

  @Autowired
  public PromotionBargainController(
      PromotionBargainService bargainService, PromotionBargainDetailService bargainDetailService) {
    this.bargainService = bargainService;
    this.bargainDetailService = bargainDetailService;
  }

  /**
   * 发起砍价
   *
   * @param bargainId 活动id
   * @param skuId skuId
   * @return 发起砍价信息
   */
  @RequestMapping("/start")
  public ResponseObject<PromotionBargainDetail> start(
      @RequestParam("bargainId") final String bargainId,
      @RequestParam("skuId") final String skuId) {
    final Either<String, PromotionBargainDetail> ret = bargainService.start(bargainId, skuId);
    return ResponseObject.from(ret, GlobalErrorCode.BARGAIN_BIZ_ERR);
  }

  /**
   * 帮忙砍价
   *
   * @param detailId 发起砍价的id
   * @return 帮砍结果
   */
  @RequestMapping("/help")
  public ResponseObject<PromotionBargainHistory> help(@RequestParam String detailId) {
    final Either<String, PromotionBargainHistory> help = bargainService.help(detailId);
    return ResponseObject.from(help, GlobalErrorCode.BARGAIN_BIZ_ERR);
  }

  /**
   * 首页列出砍价活动
   *
   * @param order 排序字段
   * @param dir 排序顺序
   * @param pageable 分页参数
   * @return 砍价活动结果
   */
  @RequestMapping("/index")
  public ResponseObject<Map<String, Object>> list(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String dir,
      Pageable pageable) {
    final List<PromotionBargainSkuVO> promotions =
        bargainService.listPromotion(order, Sort.Direction.fromString(dir), pageable);
    final Long total = bargainService.countPromotion();
    final Map<String, Object> ret = ImmutableMap.of("total", total, "list", promotions);
    return ResponseObject.success(ret);
  }

  /**
   * 查询所有我发起砍价
   *
   * @param order 排序字段
   * @param dir 排序顺序
   * @param pageable 分页参数
   * @return 我发起的砍价活动结果
   */
  @RequestMapping("/selectMyBargainInfo")
  public ResponseObject<Map<String, Object>> listMyBargain(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String dir,
      Pageable pageable) {
    User user = getCurrentUser();
    String userId = user.getId();
    final List<PromotionBargainPlaceVO> detailUserVOS =
        bargainDetailService.selectByUser(userId, order, Sort.Direction.fromString(dir), pageable);
    final Long total = bargainDetailService.countMyPromotion();
    final Map<String, Object> ret = ImmutableMap.of("total", total, "list", detailUserVOS);
    return ResponseObject.success(ret);
  }

  /** 查询商品信息,显示可砍至的最底价和砍价活动剩余时间 */
  // todo

  /** 查询我的砍价进度，单sku */
  @RequestMapping("/viewSchedule")
  public PromotionBargainDetailUserVO listMyBargainSchedule(
      @RequestParam("bargainDetailId") String bargainDetailId) {
    return bargainDetailService.selectMyBargainScheduleByUserId(bargainDetailId);
  }

  /** 查询砍价活动会场,并关联显示显示我是否发起订单 */
  @RequestMapping("/promotions")
  public List<PromotionBargainPlaceVO> listPromotionPlace(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String dir,
      Pageable pageable) {
    return bargainService.listPromotionPlace(order, Sort.Direction.fromString(dir), pageable);
  }
}
