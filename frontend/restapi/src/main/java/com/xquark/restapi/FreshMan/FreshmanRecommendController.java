package com.xquark.restapi.FreshMan;

import com.alibaba.fastjson.JSONObject;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.ProductDistributorMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.DomainTypeHandler;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.dal.vo.UserInfoVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.freshman.FreshmanRecommendService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.RolePriceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 * 推荐文章，甄选文章
 *
 * @author tanggb
 * @date 2019/03/08 09:58
 * @version 1.0
 */
@Controller
public class FreshmanRecommendController extends BaseController {

    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private ShopService shopService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDistributorMapper productDistributorMapper;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private UserAgentService userAgentService;
    @Autowired
    private RolePriceService rolePriceService;
    @Autowired
    private FreshmanRecommendService freshmanRecommendService;

    final static Logger log = LoggerFactory.getLogger(FreshmanRecommendController.class);

    /**
     * 商品推荐
     * 规则：成为vip分别有两个档,200档，500档，
     * 根据查询用户当前状态，推荐商品达到下一个档次，当用户消费满500时，则成为vip
     * 当用户成为vip后，则给用户推荐各个品类中销售top 1 商品
     *
     * @author tanggb
     */
    @ResponseBody
    @RequestMapping(value = "/freshman/recommend", method = RequestMethod.GET)
    public ResponseObject<Map> getRecommendProducts(HttpServletRequest req) {
        IUser iUser = getCurrentIUser();
        UserInfoVO userInfoVO = userService.getUserInfo(iUser.getId());
        User user = userInfoVO.getUser();
        Map resultMap = new HashMap(4);
        //判断该用户是否为vip
        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        log.info("============hasIdentity==========",hasIdentity);
        //判断vip_type是否有效即大于0
        Integer vipType = freshmanRecommendService.findVipType(user.getCpId());
        log.info("==========vipType==========",vipType);
        if(vipType == null){
            vipType = 0;
        }
        //是，则给用户推荐各个品类中销售top 1 商品
        if(hasIdentity || vipType > 0) {
            this.VipProduct(user,req,resultMap);
        }else {
            //获取用户消费的累计金额
            BigDecimal countAmount = freshmanRecommendService.countUserSpending(user.getCpId());
            log.info("如果消费金额小于200，则在拼团商品池中取200-累计消费金额相近的10个商品:",countAmount.compareTo(new BigDecimal(200)));
            String rootShopId = shopService.loadRootShop().getId();
            //如果消费金额小于200，则在拼团商品池中取200-累计消费金额相近的10个商品
            //普推逻辑查出6个普推商品，然后查出强推全部
            if(countAmount.compareTo(new BigDecimal(0)) >= 0 && countAmount.compareTo(new BigDecimal(200)) < 0){
                 //判断强推是否存在，存在则走强推逻辑
                List<Product> freshmanProduct=null;
                 if(freshmanRecommendService.countByStrongPush()>0){
//                      走强推逻辑商品
                     freshmanProduct = freshmanRecommendService.findFreshmanProductStrong(new BigDecimal(200).subtract(countAmount));
                 }
                 else {
                       //走普推逻辑商品
                     //通过200-countAmount的金额查询出大于的最近10个商品
                     freshmanProduct = freshmanRecommendService.findFreshmanProduct(new BigDecimal(200).subtract(countAmount));
                 }
                log.info("通过200-countAmount的金额查询出大于的最近10个商品:",freshmanProduct);
                this.returnMap(resultMap,freshmanProduct, rootShopId, user, req);
                log.info("======200 ===resultMap:",resultMap);
                return new ResponseObject<>(resultMap);
            }
            //如果消费金额小于500,则在拼团商品池中取500-累计消费金额相近的10个商品
            //强推逻辑查出6个普推商品，然后查出强推全部
            if(countAmount.compareTo(new BigDecimal(200)) >= 0 && countAmount.compareTo(new BigDecimal(500)) < 0){
                //通过500-countAmount的金额，查询大于此价的最近10个商品
                List<Product> freshmanProduct=null;
                if(freshmanRecommendService.countByStrongPush()>0){
//                      走强推逻辑商品
                    freshmanProduct = freshmanRecommendService.findFreshmanProductStrong(new BigDecimal(500).subtract(countAmount));
                }
                else {
                    //走普推逻辑商品
                    //通过200-countAmount的金额查询出大于的最近10个商品
                    freshmanProduct = freshmanRecommendService.findFreshmanProduct(new BigDecimal(500).subtract(countAmount));
                }

                log.info("通过500-countAmount的金额查询出大于的最近10个商品:",freshmanProduct);
                this.returnMap(resultMap,freshmanProduct, rootShopId, user, req);
                log.info("======500 ===resultMap:",resultMap);
                return new ResponseObject<>(resultMap);
            }

            if(countAmount.compareTo(new BigDecimal(500)) >= 0){
                this.VipProduct(user,req,resultMap);
            }
        }
        log.info("======vip ===resultMap:",resultMap);
        return new ResponseObject<>(resultMap);
    }

    /**
     * 用户成为vip后台，给推荐的商品列表
     * * @param user
     * @param req
     * @param resultMap
     */
    private void VipProduct(User user,HttpServletRequest req,Map resultMap){
        Product product = null;
        List<Product> productList = new ArrayList<>();
        String rootShopId = shopService.loadRootShop().getId();

        List<Category> categoryList = categoryService.getAllCategoryIdList();
        for (Category category : categoryList) {
            product = productService.productsBySale(category.getId(), rootShopId);
            if (product == null){
                continue;
            }
            productList.add(product);
        }

        List<ProductVOEx> productVOExes = generateImgUrls(productList, rootShopId, req);

        levelPrice(user,productVOExes);

        //去重过滤
        Set<ProductVOEx> productVOExSet = new LinkedHashSet<>(productVOExes);

        resultMap.put("list", productVOExSet);
        resultMap.put("count", productVOExSet.size());
    }

    private List<ProductVOEx> generateImgUrls(List<Product> products, String shopId,
                                              HttpServletRequest req) {
        List<ProductVOEx> exs = new ArrayList<>();
        ProductVOEx ex = null;
        Activity activity = null;
        if (products == null) {
            return exs;
        }
        for (Product product : products) {

            ex = new ProductVOEx(new ProductVO(product), getProductTargetUrl(req, product.getId()),
                    product.getImg(), null);

            // 设置进货类型的商品详情页
            //ex.setB2bProductUrl(getB2bProductShareUrl(req,product.getCode()));

            //fixme
            if (product.getCommissionRate() != null) {
                ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            //商品卖家端是否上架
            ProductDistributor productDistributor = productDistributorMapper
                    .selectByProductIdAndShopId(product.getId(), shopId);

            //有记录并且状态为上架
            if (productDistributor != null && ProductStatus.ONSALE
                    .equals(productDistributor.getStatus())) {
                ex.setOnSaleForSeller(true);
            } else {
                ex.setOnSaleForSeller(false);
            }
            //多少卖家在卖
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            ex.setCountSeller(count);

            // 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
            // 利润为商品原价和经销商价格之差
            String productId = product.getId();
            String userId = this.getCurrentIUser().getId();
            //判断当前用户是否在代理角色中
            UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
            if (userAgentVO != null) {
                String role = userAgentVO.getRole();
                RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
                if (rolePriceVO != null) {
                    BigDecimal oriPrice = ex.getPrice();
                    BigDecimal rolePrice = rolePriceVO.getPrice();
                    ex.setPrice(rolePrice);
                    ex.setCommission(oriPrice.subtract(rolePrice));
                }
            }

            //ex.setProductUrl(siteHost + "/p/" + ex.getId() + "?union_id=" + getCurrentUser().getId());
            exs.add(ex);
        }
        return exs;
    }

    private String getProductTargetUrl(HttpServletRequest req, String code) {
        return req.getScheme() + "://" + req.getServerName() + "/p/" + code;
    }
    //200档，500档
    private void returnMap(Map resultMap,List<Product> freshmanProduct, String rootShopId,User user,
                           HttpServletRequest req){
        List<ProductVOEx> productVOExes = this.generateImgUrls(freshmanProduct, rootShopId, req);
        levelPrice(user,productVOExes);
        //去重过滤
        Set<ProductVOEx> productVOExSet = new LinkedHashSet<>(productVOExes);

        resultMap.put("list", productVOExSet);
        resultMap.put("count", productVOExSet.size());
    }

    private void levelPrice(User user,List<ProductVOEx> productVOExes){
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + user.getCpId());
        if(json != null) {
            JSONObject jsonObject = JSONObject.parseObject(json);
            Boolean isProxy = jsonObject.getBoolean("isProxy");
            Boolean isMember = jsonObject.getBoolean("isMember");

            for (ProductVOEx p : productVOExes) {
                String pid;
                if (StringUtils.isNotBlank(pid = p.getId())) {
                    List<Sku> skus = skuMapper.selectByProductId(pid);
                    if (CollectionUtils.isNotEmpty(skus)) {
                        Sku sku = skus.get(0);
                        p.setDeductionDPoint(sku.getDeductionDPoint());
                        p.setPoint(sku.getPoint());
                        p.setPrice(sku.getPrice());

                        BigDecimal subtractValue = p.getPoint().add(sku.getServerAmt());
                        BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2, 1);
                        p.setProxyPrice(proxyPrice);

                        BigDecimal conversionPrice = p.getConversionPrice();
                        p.setMemberPrice(sku.getPrice().subtract(p.getPoint()).setScale(2, 1));

                        if (isProxy) {
                            p.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2, 1));
                        } else if (isMember) {
                            p.setChangePrice(conversionPrice.subtract(p.getPoint()).setScale(2, 1));
                        } else {
                            p.setChangePrice(conversionPrice);
                        }
                        p.setAmount(sku.getAmount());
                    }
                }
            }
        }
    }
}
