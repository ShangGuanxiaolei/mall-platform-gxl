package com.xquark.restapi.user;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.RolePrice;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.user.RolePriceService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户角色对应商品价格controller Created by chh on 16-12-13.
 */
@Controller
@ApiIgnore
public class RolePriceController extends BaseController {

  @Autowired
  private RolePriceService rolePriceService;

  /**
   * 获取用户角色对应商品价格列表
   */
  @ResponseBody
  @RequestMapping("/rolePrice/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<RolePriceVO> roles = null;
    roles = rolePriceService.list(pageable, keyword);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", rolePriceService.selectCnt(keyword));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存用户角色对应商品价格
   */
  @ResponseBody
  @RequestMapping("/rolePrice/save")
  public ResponseObject<Boolean> savePromotion(RolePrice role) {
    int result = 0;
    if (StringUtils.isNotEmpty(role.getId())) {
      result = rolePriceService.modify(role);
    } else {
      role.setArchive(false);
      result = rolePriceService.insert(role);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/rolePrice/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = rolePriceService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的用户角色对应商品价格记录<br>
   */
  @ResponseBody
  @RequestMapping("/rolePrice/{id}")
  public ResponseObject<RolePriceVO> view(@PathVariable String id, HttpServletRequest req) {
    RolePriceVO role = rolePriceService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }


}
