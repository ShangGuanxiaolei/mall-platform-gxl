package com.xquark.restapi.logisticsPlatform;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.GoodInfo;
import com.xquark.dal.model.Supplier;
import com.xquark.openapi.scrm.PolyControllerApi;
import com.xquark.service.logisticsPlatform.LogisticsProductAndStockService;
import com.xquark.service.pricing.vo.ResponsePolySysnStock;
import com.xquark.service.pricing.vo.ResponseResult;
import com.xquark.service.pricing.vo.ThirdSupplierResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * author: liuwei Date: 18-9-19. Time: 上午11:03
 */
@RestController
public class LogisticsProductAndStockController {

    private final static Logger LOGGER = LoggerFactory.getLogger(PolyControllerApi.class);

    @Autowired
    private LogisticsProductAndStockService productAndStockService;

    @RequestMapping(value = "/logistics/download/product",
            consumes = "application/x-www-form-urlencoded;charset=utf-8",
            produces = "application/x-www-form-urlencoded;charset=utf-8")
    public String downloadProduct(HttpServletRequest request) {
        LOGGER.info("========== 开始处理商品请求 ===========");
        ResponseResult<List<GoodInfo>> rr = new ResponseResult<List<GoodInfo>>();
        String bizcontent = request.getParameter("bizcontent");
        Supplier supplier = (Supplier) request.getAttribute("supplier");
        JSONObject json = JSONObject.parseObject(bizcontent);
        try {
            //调用service的方法返回商品数据
            LOGGER.info("========== 开始获取商品相关信息 ===========");
            List<GoodInfo> goodslist = productAndStockService.getProductList(
                    Integer.parseInt(json.getString("PageIndex")),
                    Integer.parseInt(json.getString("PageSize")), supplier.getId());
            Integer totalcount = productAndStockService.getCountTotal(supplier.getId());
            rr.setCode("10000");
            rr.setMessage("SUCCESS");
            rr.setTotalcount(totalcount);
            rr.setGoodslist(goodslist);
        } catch (RuntimeException e) {
            rr.setCode("40000");
            rr.setMessage("参数不符合要求");
            LOGGER.error("========== 请求参数错误 ===========", e);
        }
        return JSONObject.toJSONString(rr);
    }

    @RequestMapping(value = "/logistics/sync/stock/batch",
            consumes = "application/x-www-form-urlencoded;charset=utf-8",
            produces = "application/x-www-form-urlencoded;charset=utf-8")
    public String syncStockBatch(HttpServletRequest request) {
        final String bizcontent = request.getParameter("bizcontent");
        final Supplier supplier = (Supplier) request.getAttribute("supplier");
        final JSONObject json = JSONObject.parseObject(bizcontent);
        final JSONArray list = json.getJSONArray("list");
        if (list.isEmpty()) {
            return ThirdSupplierResult.error("请传入list参数, 同步库存列表", "400001").toJson();
        }
        List<LogisticsResult> collect = list.toJavaList(LogisticsItem.class).stream()
                .map(syncStockInner(supplier))
                .collect(Collectors.toList());
        return ThirdSupplierResult.success(collect).toJson();
    }

    @RequestMapping(value = "/logistics/sync/stock",
            consumes = "application/x-www-form-urlencoded;charset=utf-8",
            produces = "application/x-www-form-urlencoded;charset=utf-8")
    public String syncStock(HttpServletRequest request) {
        ResponsePolySysnStock<Integer> rp = new ResponsePolySysnStock<>();
        String bizcontent = request.getParameter("bizcontent");
        Supplier supplier = (Supplier) request.getAttribute("supplier");
        JSONObject json = JSONObject.parseObject(bizcontent);
        Integer quantity = Optional.ofNullable(json.getString("quantity"))
                .map(Integer::valueOf).orElse(-1);
        String barCode = json.getString("skuOuterID");

        try {
            LOGGER.info("========== 开始同步商品库存 ===========");
            int stock = syncStockInner(supplier, barCode, quantity).getResult();
            rp.setCode("10000");
            rp.setMessage("SUCCESS");
            rp.setQuantity(stock);
            LOGGER.info("========== 同步库存结束 ===========");
        } catch (RuntimeException e) {
            rp.setCode("40000");
            rp.setMessage("服务器异常，请稍后再试");
            rp.setQuantity(null);
            LOGGER.error("运行时未知异常", e);
        }
        return JSONObject.toJSONString(rp);
    }

    private Function<LogisticsItem, LogisticsResult> syncStockInner(Supplier supplier) {
        return item -> syncStockInner(supplier, item.getSkuOuterID(), item.getQuantity());
    }

    private LogisticsResult syncStockInner(Supplier supplier, String barCode, Integer quantity) {
        if (StringUtils.isNotBlank(barCode) && quantity != -1) {
            Integer rows = productAndStockService.saveProductSkuQuantity(quantity, barCode,
                    supplier.getCode());
            if (rows > 0) {
                return new LogisticsResult(barCode, productAndStockService.getProductQuantity(barCode, supplier.getCode()));
            }
        }
        return new LogisticsResult(barCode, -1);
    }

}
