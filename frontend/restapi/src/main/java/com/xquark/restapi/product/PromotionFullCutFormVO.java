package com.xquark.restapi.product;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.xquark.dal.type.PromotionDiscountType;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.util.json.JsonStringDateDeserializer;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 2018/4/16. DESC:
 */
public class PromotionFullCutFormVO {

  private String id;

  @NotBlank(message = "请填写标题")
  private String title;

  /**
   * 默认不填描述
   */
  private String details = "";

  /**
   * 优惠活动生效范围 默认范围为自选
   */
  private PromotionScope scope = PromotionScope.PRODUCT;

  private PromotionUserScope userScope;

  @NotNull(message = "优惠类型不能为空")
  private PromotionDiscountType discountType;

  /**
   * 全局优惠金额 不传则是指定商品优惠
   */
  private BigDecimal discount;

  private Boolean isFreeDelivery = false;

  /**
   * 活动开始时间
   */
  @JsonDeserialize(using = JsonStringDateDeserializer.class)
  private Date validFrom;

  /**
   * 活动结束时间
   */
  @JsonDeserialize(using = JsonStringDateDeserializer.class)
  private Date validTo;

  private List<PromotionFullCutProductFormVO> productDetails;

  /**
   * 活动类型
   */
  @NotNull(message = "活动类型不能为空")
  private PromotionType type;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public PromotionScope getScope() {
    return scope;
  }

  public void setScope(PromotionScope scope) {
    this.scope = scope;
  }

  public PromotionUserScope getUserScope() {
    return userScope;
  }

  public void setUserScope(PromotionUserScope userScope) {
    this.userScope = userScope;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public PromotionType getType() {
    return type;
  }

  public void setType(PromotionType type) {
    this.type = type;
  }

  public PromotionDiscountType getDiscountType() {
    return discountType;
  }

  public void setDiscountType(PromotionDiscountType discountType) {
    this.discountType = discountType;
  }

  public List<PromotionFullCutProductFormVO> getProductDetails() {
    return productDetails;
  }

  public void setProductDetails(List<PromotionFullCutProductFormVO> productDetails) {
    this.productDetails = productDetails;
  }

  public Boolean getIsFreeDelivery() {
    return isFreeDelivery;
  }

  public void setIsFreeDelivery(Boolean freeDelivery) {
    isFreeDelivery = freeDelivery;
  }
}
