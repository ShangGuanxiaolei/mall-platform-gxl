package com.xquark.restapi.antifake;

import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.vo.AntifakeVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.antifake.AntifakeService;
import com.xquark.utils.StringUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 防伪查询
 *
 * @author chh 2017-02-06
 */
@Controller
@ApiIgnore
public class AntifakeController extends BaseController {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private AntifakeService antifakeService;

  /**
   * 获取物流码追溯订单信息
   */
  @ResponseBody
  @RequestMapping("/antifake/list")
  public ResponseObject<Map<String, Object>> listCommission(Pageable pageable,
      HttpServletRequest request) {
    String shopId = this.getCurrentIUser().getShopId();
    List<AntifakeVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String orderNo = request.getParameter("orderNo");
    String code = request.getParameter("code");
    if (StringUtils.isNotEmpty(orderNo)) {
      params.put("orderNo", "%" + orderNo + "%");
    }
    if (StringUtils.isNotEmpty(code)) {
      List codes = antifakeService.getCodes(code);
      params.put("codes", codes);
    }
    Map<String, Object> aRetMap = new HashMap<>();
    // 如果没有过滤条件，默认不查询数据
    if (params.size() == 0) {
      viewPages = new ArrayList<>();
      aRetMap.put("total", 0);
      aRetMap.put("list", viewPages);
    } else {
      viewPages = antifakeService.list(pageable, params);
      aRetMap.put("total", antifakeService.selectCnt(params));
      aRetMap.put("list", viewPages);
    }
    return new ResponseObject<>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/antifake/query")
  public ResponseObject<String> query(HttpServletRequest req, HttpServletResponse resp) {
    String result = "";
    // 追踪码
    String serial = req.getParameter("serial");
    // 防伪码
    String code = req.getParameter("code");

    // 调用第三方接口进行防伪查询
    String url = "http://fwei.now315.com/f.php?a=cu2";
    try {
      Map<String, String> mapParam = new HashMap<>();
      mapParam.put("code", code);
      if (StringUtil.isNotNull(serial)) {
        mapParam.put("serial", serial);
      }
      String response = getData(url, mapParam);
      log.info("防伪查询接口返回 " + response);
      JSONObject jsonObject = JSONObject.parseObject(response);
      result = jsonObject.getString("errmsg");
    } catch (Exception e) {
      log.error("防伪查询出错!", e);
      e.printStackTrace();
    }

    return new ResponseObject<>(result);
  }

  public static String getData(String url, Map<String, String> paramMap)
      throws UnsupportedEncodingException, IOException {
    String charset = "UTF-8";
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    HttpURLConnection connect = (HttpURLConnection) (new URL(url).openConnection());
    connect.setRequestMethod("POST");
    connect.setDoOutput(true);
    connect.setConnectTimeout(1000 * 10);
    connect.setReadTimeout(1000 * 80);
    // 设置文件字符集:
    connect.setRequestProperty("Charset", "UTF-8");
    connect.setRequestProperty("ContentType", "application/json"); //采用通用的URL百分号编码的数据MIME类型来传参和设置请求头
    connect.setDoInput(true);
    // 连接
    connect.connect();

    // 获取URLConnection对象对应的输出流
    PrintWriter out = new PrintWriter(connect.getOutputStream());

    // 设置请求属性
    String param = "";
    if (paramMap != null && paramMap.size() > 0) {
      for (String key : paramMap.keySet()) {
        String value = paramMap.get(key);
        param += key + "=" + value + "&";
      }
      param = param.substring(0, param.length() - 1);
    }

    // 发送请求参数
    out.print(param);
    // flush输出流的缓冲
    out.flush();

    // 接收数据
    int responseCode = connect.getResponseCode();
    if (responseCode == HttpURLConnection.HTTP_OK) {
      InputStream in = connect.getInputStream();
      byte[] data = new byte[1024];
      int len = 0;
      while ((len = in.read(data, 0, data.length)) != -1) {
        outStream.write(data, 0, len);
      }
      in.close();
    }
    // 关闭连接
    connect.disconnect();
    String response = outStream.toString();
    return response;
  }

  /**
   * 测试主方法
   */
  public static void main(String[] args) {
    Map<String, String> mapParam = new HashMap<>();
    mapParam.put("code", "3066610011365181");
    //mapParam.put("serial", null);
    String url = "http://fwei.now315.com/f.php?a=cu2";
    try {
      String result = getData(url, mapParam);

      JSONObject jsonObject = JSONObject.parseObject(result);
      String errmsg = jsonObject.getString("errmsg");

      System.out.println("errmsg=" + errmsg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
