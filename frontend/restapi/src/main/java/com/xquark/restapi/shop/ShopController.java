package com.xquark.restapi.shop;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Optional;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductDistributorMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.PromotionCouponMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.CategoryActivity;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.PartnerSetting;
import com.xquark.dal.model.ProdSync;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductDistributor;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.Tag;
import com.xquark.dal.model.ThirdCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.status.SyncAuditStatus;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.dal.vo.ShopPostAge;
import com.xquark.dal.vo.TwitterInfoVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.DeviceTypes;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.category.CategoryForm;
import com.xquark.restapi.category.CategoryVO;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.category.TermTaxonomyService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.BannerVO;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shop.ShopStyleVO;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.smstpl.SmsTplService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.UserCardService;
import com.xquark.utils.ImgUtils;
import com.xquark.utils.ResourceResolver;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
@ApiIgnore
public class ShopController extends BaseController {

  @Autowired
  private PromotionCouponMapper promotionCouponMapper;
  @Autowired
  private SmsTplService smsTplService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private ProductService productService;
  @Autowired
  private UrlHelper urlHelper;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private ZoneService zoneService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private TermTaxonomyService termTaxonomyService;
  @Autowired
  private OrderService orderService;
  @Autowired
  private ProdSyncMapper prodSyncMapper;
  @Autowired
  private ShopPostAgeService shopPostAgeService;
  @Autowired
  private PromotionCouponService promotionCouponService;
  @Autowired
  private ShopTreeService shopTreeService;
  @Autowired
  private ProductDistributorMapper productDistributorMapper;
  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private TinyUrlService tinyUrlService;
  @Autowired
  private UserService userService;
  @Autowired
  private UserTwitterService userTwitterService;
  @Autowired
  private UserPartnerService userPartnerService;
  @Autowired
  private FamilyCardSettingService familyCardSettingService;
  @Autowired
  private UserCardService userCardService;
  @Autowired
  private PartnerSettingService partnerSettingService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private ShopMapper shopMapper;


  @Value("${shop.banners}")
  private String bannerKeys;

  @Value("${spider.url}")
  private String spiderurl;

  @Value("${site.web.host.name}")
  private String siteHost;

  /**
   * 获取店铺/shop/{id}
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/{id}")
  public ResponseObject<ShopVO> view(@PathVariable String id, HttpServletRequest req) {
    Shop shop = shopService.load(id);
    if (shop == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("shop.not.found"));
    }

    return new ResponseObject<>(shopToVo(shop, req));
  }

  /**
   * 获取我的店铺
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/mine")
  public ResponseObject<ShopVO> mine(HttpServletRequest req) {
    return new ResponseObject<>(shopToVo(shopService.mine(), req));
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/promotionCouponlist")
  public ResponseObject<List<PromotionCouponVo>> promotionCouponlist(
      @RequestParam(required = false) String shopid) {
    List<PromotionCouponVo> list = promotionCouponService.loadShopPromotionCoupon(shopid, null);
    return new ResponseObject<>(list);
  }

  /**
   * 更新邮费设置
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updatePostageSet")
  public ResponseObject<Boolean> updatePostAgeSet(@RequestBody ShopPostAge shopPostAge) {
    Boolean ret = false;
    IUser user = getCurrentIUser();
    shopPostAge.setShopId(user.getShopId());
    if (shopPostAge.getShopId() != null) {
      ret = shopPostAgeService.setPostAgeByShop(shopPostAge);
    }
    return new ResponseObject<>(ret);
  }

  /**
   * 获取邮费设置
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/getPostageSet")
  public ResponseObject<ShopPostAge> getPostAge(@RequestParam(required = false) String id) {
    IUser user = getCurrentIUser();
    if (user != null) {
      if (StringUtils.isNotEmpty(id)) {
        return new ResponseObject<>(shopPostAgeService.getPostAgeById(id));
      } else {
        return new ResponseObject<>(
            shopPostAgeService.getPostAgeByDefault(user.getShopId()));
      }
    } else {
      return new ResponseObject<>();
    }
  }

  private PostAgeZoneVO containsZone(String zoneTag, List<PostAgeZoneVO> list) {
    if (zoneTag != null && list != null && list.size() != 0) {
      for (PostAgeZoneVO avo : list) {
        if (zoneTag.equals(avo.getZoneTag())) {
          return avo;
        }
      }
    }
    return null;
  }

  /**
   * 获取设置邮费的地区
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/getPostageZones")
  public ResponseObject<List<PostAgeZoneVO>> getPostAgeZones() {
    List<PostAgeZoneVO> ret = new ArrayList<>();

    List<Zone> zoneList = zoneService.listPostageZones();
    for (Zone zone : zoneList) {
      PostAgeZoneVO avo = containsZone(zone.getZoneTag(), ret);
      if (avo != null) {
        avo.getZones().add(zone);
      } else {
        avo = new PostAgeZoneVO();
        List<Zone> zones = new ArrayList<>();
        zones.add(zone);
        avo.setZoneTag(zone.getZoneTag());
        avo.setZones(zones);
        ret.add(avo);
      }
    }

    List<Zone> cityList = zoneService.listPostageCityZones();
    for (Zone zone : cityList) {
      PostAgeZoneVO avo = containsZone(zone.getZoneTag(), ret);
      if (avo != null) {
        avo.getZones().add(zone);
      } else {
        avo = new PostAgeZoneVO();
        List<Zone> zones = new ArrayList<>();
        zones.add(zone);
        avo.setZoneTag(zone.getZoneTag());
        avo.setZones(zones);
        ret.add(avo);
      }
    }

    return new ResponseObject<>(ret);
  }

  /**
   * 开通担保交易
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/openDanbao")
  public ResponseObject<Boolean> openDanbao() {
    return new ResponseObject<>(shopService.openDanbao());
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/save")
  public ResponseObject<ShopVO> save(@Valid @ModelAttribute ShopForm form, Errors errors,
      HttpServletRequest req) {
    ControllerHelper.checkException(errors);
    Shop shop = shopService.findByUser(getCurrentUser().getId());
    if (null == shop) {
      shop = new Shop();
    }
    BeanUtils.copyProperties(form, shop);
    shop = shopService.create(shop);
    if (shop == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          requestContext.getMessage("valid.shop.exist.message"));
    }
    User user = (User) shopService.getCurrentUser();
    user.setShopId(shop.getId());
    return new ResponseObject<>(shopToVo(shop, req));
  }

  private Boolean updateThirdCommission(ThirdCommission atc, HttpServletRequest req) {
    Boolean ret = false;
    if (atc == null || atc.getThirdId() == null) {
      return ret;
    }

    Shop shop = shopService.load(this.getCurrentUser().getShopId());
    if (atc.getCommissionRate() != null) {
      if (atc.getCommissionRate().compareTo(BigDecimal.valueOf(0.05)) < 0 ||
          atc.getCommissionRate().compareTo(BigDecimal.valueOf(1)) > 0) {
        RequestContext requestContext = new RequestContext(req);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("invalid.third.commisionrate"));
      }

      Map<String, Object> params = new HashMap<>();
      params.put("shopId", shop.getId());
      params.put("unionId", atc.getThirdId());
      List<ProdSync> aList = prodSyncMapper.findByParmas(params, null);
      ProdSync aps = new ProdSync();
      if (aList != null && aList.size() != 0) {
        if (aList.size() == 1) {
          aps.setId(aList.get(0).getId());
          aps.setAuditSts(SyncAuditStatus.AUDITTING.toString());
          aps.setCommissionRate(atc.getCommissionRate());
          ret = prodSyncMapper.update(aps) == 1 ? true : false;
        } else {
          ret = false;
        }
      } else {
        aps.setShopId(shop.getId());
        aps.setName(shop.getName());
        aps.setSynced(false);
        aps.setAuditSts(SyncAuditStatus.AUDITTING.toString());
        aps.setCommissionRate(atc.getCommissionRate());
        User aUser = userService.load(atc.getThirdId());
        if (aUser != null) {
          aps.setUnionId(aUser.getId());
        } else {
          aps.setUnionId(IdTypeHandler.encode(16618945L));
        }
        ret = prodSyncMapper.insert(aps) == 1 ? true : false;
      }

      shop.setDanbao(true);
      shopService.update(shop);
    }
    return ret;
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/update")
  public ResponseObject<Boolean> update(@Valid @ModelAttribute ShopForm form, Errors errors,
      HttpServletRequest req) {
    ControllerHelper.checkException(errors);

    // 先更新用户表
    String userId = this.getCurrentIUser().getId();
    User userUpdate = new User();
    userUpdate.setId(userId);
    if (StringUtils.isNotEmpty(form.getName())) {
      userUpdate.setName(form.getName());
    }
    if (StringUtils.isNotEmpty(form.getImg())) {
      userUpdate.setAvatar(form.getImg());
    }
    userService.updateByBosUserInfo(userUpdate);

    // 如果该用户有shop，则同时也更新掉shop表相关信息
    User user = userService.load(userId);
    if (StringUtils.isNotEmpty(user.getShopId())) {
      Shop shop = new Shop();
      shop.setId(user.getShopId());
      if (StringUtils.isNotEmpty(form.getName())) {
        shop.setName(form.getName());
      }
      if (StringUtils.isNotEmpty(form.getImg())) {
        shop.setImg(form.getImg());
      }
      shopMapper.updateByPrimaryKeySelective(shop);
    }

    return new ResponseObject<>(true);
  }

  /**
   * 更换店招
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateBanner/{id}")
  public ResponseObject<Boolean> updateBanner(@PathVariable String id, String banner) {
    Shop shop = new Shop();
    shop.setId(id);
    shop.setBanner(banner);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  /**
   * 获取某个店铺的标签
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/tag/{shopId}")
  public ResponseObject<List<Tag>> tag(@PathVariable String shopId, String tag) {
    return new ResponseObject<>(shopService.findTagsByShopId(shopId, tag));
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/tags")
  public ResponseObject<List<Tag>> tags() {
    return new ResponseObject<>(shopService.listUserTags());
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/tag/save")
  public ResponseObject<Tag> saveTag(String tag) {
    return new ResponseObject<>(shopService.saveUserTag(tag));
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/tag/update")
  public ResponseObject<Tag> updateTag(String id, String tag) {
    return new ResponseObject<>(shopService.updateUserTag(id, tag));
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/tag/remove")
  public ResponseObject<Boolean> removeTag(String tag) {
    shopService.removeUserTag(tag);
    return new ResponseObject<>(true);
  }

  /**
   * 列出所有商铺
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/list")
  public ResponseObject<List<Shop>> list(Pageable pageble) {
    return new ResponseObject<>(shopService.listAll(pageble));
  }

  private ThirdCommission createThirdCommission(User aUser, String shopId) {
    if (aUser == null || shopId == null) {
      return null;
    }

    ThirdCommission atc = new ThirdCommission();
    atc.setThirdPartner(aUser.getName());
    atc.setAuditSts(SyncAuditStatus.UNAUDIT.toString());

    List<ProdSync> psList = prodSyncMapper.selectByShopId(shopId, null);
    if (psList != null && psList.size() != 0) {
      for (ProdSync aps : psList) {
        if (aps.getUnionId().equals(aUser.getId())) {
          atc.setCommissionRate(aps.getCommissionRate());
          atc.setAuditSts(aps.getAuditSts());
          atc.setFailedReason(aps.getAuditNote());
        }
      }
    }
    atc.setThirdId(aUser.getId());
    return atc;
  }

  private ShopVO shopToVo(Shop shop, HttpServletRequest req) {
    if (shop != null) {
      shop = shopService.load(shop.getId());
      ShopVO vo = new ShopVO(shop);
      vo.setShopUrl(getShareUrl(req));
      if (StringUtils.isNotBlank(vo.getImg())) {
        vo.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(vo.getBanner())) {
        vo.setBannerUrl(shop.getBanner());
      }

      vo.setCountDraft(productService.countProductsByStatus(vo.getId(), ProductStatus.DRAFT));// 草稿
      vo.setCountForsale(
          productService.countProductsByStatus(vo.getId(), ProductStatus.FORSALE));// 发布计划数
      vo.setCountOnsale(
          productService.countProductsByStatus(vo.getId(), ProductStatus.ONSALE));// 在售商品数
      vo.setCountOutofstock(productService.countProductsByOutofStock(vo.getId()));// 缺货
      vo.setLocalName(getLocalArea(shop.getProvinceId(), shop.getCityId()));  //加载所在区域
      vo.setPostFreeRegions(parserPostFreeStr(shop.getFreeZone())); // 免邮区域

      vo.setCountOfOrderClose(orderService.countSellerOrdersByStatus(OrderStatus.CLOSED));
      vo.setCountOfOrderPaid(orderService.countSellerOrdersByStatus(OrderStatus.PAID));
      vo.setCountOfOrderShipped(orderService.countSellerOrdersByStatus(OrderStatus.SHIPPED));
      vo.setCountOfOrderSubmitted(orderService.countSellerOrdersByStatus(OrderStatus.SUBMITTED));
      vo.setCountOfOrderSuccess(orderService.countSellerOrdersByStatus(OrderStatus.SUCCESS));

      List<ThirdCommission> aList = new ArrayList<>();
      List<User> feeAcctList = userService.getFeeSplitAcct();
      for (User aUser : feeAcctList) {  // 多个分润账号
        if (aUser.getLoginname()
            .equalsIgnoreCase(UserPartnerType.XIANGQU.toString()))  // 暂时只开放"想去"
        {
          aList.add(createThirdCommission(aUser, vo.getId()));
        }
      }
      vo.setThirdCommissions(aList);
      ShopPostAge spa = shopPostAgeService.getPostAgeByShop(shop.getId());
      if (spa != null) {
        if (spa.getPostageStatus() != null) {
          vo.setPostageStatus(
              spa.getPostageStatus());  // 用记亲数据替换老数据, 老数据其实也被新流程更新过了, 为的是将来删除老字段不再维护
        }
        if (spa.getPostage() != null) {
          vo.setPostage(spa.getPostage());
        }
      }
      return vo;
    } else {
      return null;
    }
  }

  // 如果有多个名邮地区以坚线分隔 '|', 一期只会有一个免邮地区, 但可能会包括 浙江沪 长三角等多地区地名
  private List<String> parserPostFreeStr(String postStr) {
    if (null == postStr) {
      return null;
    }
    String[] regions = postStr.split("\\|");
    return Arrays.asList(regions);
  }

  private String getLocalArea(Long provId, Long cityId) {
    String localName = new String();
    Zone zone = null;

    if (provId != null && provId.intValue() > 0) {
      zone = zoneService.load(provId.toString());
      if (zone != null) {
        localName += zone.getName();
      }
    }

    if (cityId != null && cityId.intValue() > 0) {
      zone = zoneService.load(cityId.toString());
      if (zone != null) {
        localName += zone.getName();
      }
    }

    return localName;
  }

  /**
   * for PC move product function
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/movestart")
  public ResponseObject<Map<String, Object>> movestart() {

    Map<String, Object> result = new HashMap<>();
    //1.  判断卖家是否已搬过家 (出错都当做初次搬家,多验证一次)
    String shopUrl = shopService.checkRepeatMoving();
    if (StringUtils.isNotEmpty(shopUrl)) {
      //2. 已搬家进入跳过验证,提醒该卖家为二次搬家并直接根据shopId进行二次搬家
      try {
        //model.addAttribute("shopUrl", URLEncoder.encode(shopUrl, "utf-8"));
        result.put("shopUrl", URLEncoder.encode(shopUrl, "utf-8"));
      } catch (Exception e) {
        log.warn("URLEncoder.encode err");
      }
    } else {
      //3. 没有搬过家按原有流程开始进行搬家
      final long rnd = this.shopService.getRnd();
      result.put("rnd", rnd);
    }

    return new ResponseObject<>(result);
  }


  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step1")
  public String step1(Model model) {
    String moveFirst = "shop/step1";
    String moveAgain = "shop/step5";

    //1.  判断卖家是否已搬过家 (出错都当做初次搬家,多验证一次)
    String shopUrl = shopService.checkRepeatMoving();
    if (StringUtils.isNotEmpty(shopUrl)) {

      //2. 已搬家进入跳过验证,提醒该卖家为二次搬家并直接根据shopId进行二次搬家
      try {
        model.addAttribute("shopUrl", URLEncoder.encode(shopUrl, "utf-8"));
      } catch (Exception e) {
        log.warn("URLEncoder.encode err");
      }
      return moveAgain;
    } else {
      //3. 没有搬过家按原有流程开始进行搬家
      final long rnd = this.shopService.getRnd();
      model.addAttribute("rnd", rnd);
      return moveFirst;
    }
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step2")
  public String step2(long rnd, Model model) {
    model.addAttribute("rnd", rnd);
    return "shop/step2";
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step2check")
  public ResponseObject<Map<String, Object>> step2check(long rnd, String itemId,
      HttpServletRequest req) {
    final int deviceType = DeviceTypes.getDeviceType(req);
    final Map<String, Object> result = this.shopService
        .moveThirdShopProducts(this.getCurrentUser(), rnd, itemId,
            deviceType, 1);

    return new ResponseObject<>(result);
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step3")
  public String step3(long rnd, String itemId, String shopName, int shopType, Model model) {
    model.addAttribute("rnd", rnd);
    model.addAttribute("itemId", itemId);
    model.addAttribute("shopName", shopName);
    model.addAttribute("shopType", shopType);

    return "shop/step3";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step4")
  public String step4() {
    return "shop/step4";
  }

  // option 0: 只新增商品, 1:新增并更新已有商品
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step5")
  @ResponseBody
  public ResponseObject<Map<String, Object>> step5(String shopUrl,
      @RequestParam(value = "option", required = true, defaultValue = "1") int option,
      HttpServletRequest req) {
    final int deviceType = DeviceTypes.getDeviceType(req);
    if (shopUrl == null || shopUrl.isEmpty()) {
      return new ResponseObject<>();
    }
    try {
      String aUrl = URLDecoder.decode(shopUrl, "utf-8");
      if (aUrl != null) {
        String[] urls = aUrl.split("\\|");
        Map<String, Object> result = null;
        for (String url : urls) {
          Thread.sleep(1000);
          result = shopService
              .moveThirdShopProducts(getCurrentUser(), url, getCurrentUser().getShopId(),
                  deviceType, option);
          if (result.get("statusCode").equals("200")) {
            continue;
          }
          return new ResponseObject<>(result);
        }
        return new ResponseObject<>(result);
      }
      return new ResponseObject<>();
    } catch (UnsupportedEncodingException | InterruptedException e) {
      e.printStackTrace();
      return new ResponseObject<>();
    }
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/setPostage")
  public ResponseObject<Boolean> updatePostByPrimaryKey(@Valid @ModelAttribute ShopForm form) {
    Shop shop = shopService.load(this.getCurrentUser().getShopId());
    if (shop == null) {
      return new ResponseObject<>(false);
    }

    if (form.getPostageStatus() != null) {
      shop.setPostageStatus(form.getPostageStatus());

      if (form.getPostageStatus()) {
        if (form.getFreeZone() != null) {
          shop.setFreeZone(form.getFreeZone());
        }
        if (form.getPostage() != null) {
          shop.setPostage(form.getPostage());
        }
      }
    }

    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateImg")
  public ResponseObject<Boolean> updateImgByPrimaryKey(String id, String img) {
    Shop shop = shopService.load(id);
    shop.setImg(img);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateName")
  public ResponseObject<Boolean> updateNameByPrimaryKey(String id, String name) {
    Shop shop = shopService.load(id);
    shop.setName(name);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateWechat")
  public ResponseObject<Boolean> updateWechatByPrimaryKey(String id, String wechat) {
    Shop shop = shopService.load(id);
    shop.setWechat(wechat);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateDesc")
  public ResponseObject<Boolean> updateDescByPrimaryKey(String id, String description) {
    Shop shop = shopService.load(id);
    shop.setDescription(description);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateBulletin")
  public ResponseObject<Boolean> updateBulletinByPrimaryKey(String id, String bulletin) {
    Shop shop = shopService.load(id);
    shop.setBulletin(bulletin);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updateLocal")
  public ResponseObject<Boolean> updateLocalByPrimaryKey(String id, String provinceId,
      String cityId) {
    Shop shop = shopService.load(id);
    Long l_provinceId = 0L;
    Long l_cityId = 0L;
    if (StringUtils.isNotEmpty(provinceId)) {
      l_provinceId = Long.valueOf(provinceId);
    }
    if (StringUtils.isNotEmpty(cityId)) {
      l_cityId = Long.valueOf(cityId);
    }
    shop.setProvinceId(l_provinceId);
    shop.setCityId(l_cityId);

    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updatePostageStatus")
  public ResponseObject<Boolean> updateLocalByPrimaryKey(String id, Boolean postageStatus) {
    Shop shop = shopService.load(id);
    shop.setPostageStatus(postageStatus);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/updatePostage")
  public ResponseObject<Boolean> updateLocalByPrimaryKey(String id, String freeZone,
      BigDecimal postage) {
    Shop shop = shopService.load(id);
    shop.setFreeZone(freeZone);
    shop.setPostage(postage);
    return new ResponseObject<>(shopService.update(shop) == 1);
  }

  /**
   * http://localhost:8888/v2/shop/category/products?categoryId=2 获取某个店铺下的某个分类的商品
   */
  @ResponseBody
  @RequestMapping(value = "/shop/category/products")
  public ResponseObject<Map<String, Object>> productAll2(String categoryId, Pageable page) {
    List<Product> products = categoryService.listProductsInCategory(categoryId, page);
    Map<String, Object> map = new HashMap<>();
    map.put("product", products);
    map.put("page", page);
    return new ResponseObject<>(map);
  }

  /**
   * v2/shop/category/list 获取某个店铺的所有分类列表
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/list")
  public ResponseObject<List<CategoryVO>> categoryList(
      @RequestParam(required = false) String shopId,
      @RequestParam(required = false) Taxonomy taxonomy) {
    taxonomy = Optional.fromNullable(taxonomy).or(Taxonomy.GOODS);
    List<CategoryVO> list = new ArrayList<>();
    List<Category> categories;
    if (shopId == null || "".equals(shopId)) {
      categories = categoryService.listUserRootGoods(taxonomy);
    } else {
      categories = categoryService.listRootCategoriesByShop(shopId, taxonomy);
    }

    // 将分类按照idx和父子节点进行排序
    List<Category> sortCategories = new ArrayList<>();
    sortList(categories, null, sortCategories);

    for (Category cat : sortCategories) {
      CategoryVO vo = new CategoryVO();
      BeanUtils.copyProperties(cat, vo);
      vo.setCategoryImgUrl(cat.getImg());
      //String name = vo.getName();
      vo.setDisplayName(vo.getName());
      long productCount = categoryService.countUserGoodsProducts(cat.getId());
      vo.setProductCount(productCount);
      long sellerCount = categoryService.countUserCategorySellers(shopId, cat.getId());
      vo.setSellerCount(sellerCount);
      //分类下所有商品列表
      //http://51shop.mobi/catalog/allProducts?categoryId=6625&shopId=cabvysu7
      vo.setCategoryH5Url(
          siteHost + "/catalog/allProducts?categoryId=" + cat.getId() + "&shopId=" + shopId);
      // 手动的改成树形结构，在二级option标签的内容前面加入空格，形成树形的样子。
      String treepath = vo.getTreePath() == null ? "" : vo.getTreePath();
      String[] level = treepath.split(">");
      for (String aLevel : level) {
        if (!"".equals(aLevel)) {
          vo.setName("&nbsp;&nbsp;&nbsp;" + vo.getName());
        }
      }
      list.add(vo);
    }
    return new ResponseObject<>(list);
  }

  /**
   * v2/shop/category/list 获取某个店铺的所有分类列表
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/listOnsale")
  public ResponseObject<List<CategoryVO>> categoryListOnSale(
      @RequestParam(required = false) String shopId) {
    List<CategoryVO> list = new ArrayList<>();
    List<Category> categories;
    if (shopId == null || "".equals(shopId)) {
      shopId = getCurrentUser().getShopId();
    }
    categories = categoryService.listCategoriesOnsaleByShop(shopId);
    for (Category cat : categories) {
      CategoryVO vo = new CategoryVO();
      BeanUtils.copyProperties(cat, vo);
      vo.setCategoryImgUrl(cat.getImg());
      //String name = vo.getName();
      vo.setDisplayName(vo.getName());
      long productCount = categoryService.countUserGoodsProducts(cat.getId());
      vo.setProductCount(productCount);
      long sellerCount = categoryService.countUserCategorySellers(shopId, cat.getId());
      vo.setSellerCount(sellerCount);
      if (vo.getStatus().equals(CategoryStatus.ONSALE)) {
        vo.setOnSaleForSeller(true);
      } else {
        vo.setOnSaleForSeller(false);
      }
      //分类下所有商品列表
      //http://51shop.mobi/shop/brand/cabvysu7/6689?currentSellerShopId=cabvysu7
      vo.setCategoryH5Url(
          siteHost + "/shop/brand/" + shopId + "/" + cat.getId() + "?currentSellerShopId="
              + shopId);
      list.add(vo);
    }
    return new ResponseObject<>(list);
  }


  private void sortList(List<Category> list, String id, List<Category> resultList) {
    for (Category node : list) {
      if ((node.getParentId() + "").equals("" + id)) {
        resultList.add(node);
        sortList(list, node.getId(), resultList);
      }
    }
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/categoryactivity/list")
  public ResponseObject<List<CategoryActivity>> categoryList() {
    List<CategoryActivity> list = categoryService.listCategoryActivity();
//		if (list.size()==0) {
//			List<CategoryActivity> plist = new ArrayList<CategoryActivity>();
//			List<Category> categories=categoryService.listUserRootGoods();
//			for (Category cat : categories) {
//				CategoryActivity vo = new CategoryActivity();
//				vo.setId(cat.getId());
//				vo.setActiviyId("0");
//				vo.setName(cat.getName());
//				plist.add(vo);
//			}
//			return new ResponseObject<List<CategoryActivity>>(plist);
//		}
    return new ResponseObject<>(list);
  }


  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/view")
  public ResponseObject<Category> categoryView(@RequestParam String id) {
    Category category = categoryService.load(id);
    if (category != null) {
      return new ResponseObject<>(category);
    } else {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "分类不存在");
    }
  }

  /**
   * 保存商品的分类信息 v2/shop/category/save?name=xbe
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/save")
  public ResponseObject<Category> categorySave(@RequestParam String name) {
    Category goods = categoryService.saveUserGoods(name);
    return new ResponseObject<>(goods);
  }

  /**
   * 批量修改分类信息
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/batchUpdate")
  public ResponseObject<Boolean> batchUpdate(@Valid @ModelAttribute CategoryForm form) {
    if (form == null || form.getCategorys() == null || form.getCategorys().isEmpty()) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请输入您要修改的分类信息");
    }

    categoryService.batchUpdateUserGoodsName(form.getCategorys());
    return new ResponseObject<>(true);
  }

  /**
   * http://localhost:8888/v2/shop/category/onsale?id=6060&shopId=xbw123578 小b端分类上架
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/onsale")
  public ResponseObject<Boolean> categoryOnsale(@RequestParam String id,
      @RequestParam(required = false) String shopId) {
    return new ResponseObject<>(categoryService.addCategory(id, shopId));
  }

  /**
   * http://localhost:8888/v2/shop/category/instock?id=6060&shopId=xbw123578 小b端分类下架
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/instock")
  public ResponseObject<Boolean> categoryUpdate(@RequestParam String id) {
    return new ResponseObject<>(categoryService.updateForInstock(id));
  }

  /**
   * http://localhost:8888/v2/shop/category/update?id=6060&name=xbw123578 修改分类
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/update")
  public ResponseObject<CategoryVO> categoryUpdate(@RequestParam String id,
      @RequestParam String name) {
    Category cat = categoryService.updateUserGoodsName(id, Taxonomy.GOODS, name);
    CategoryVO vo = new CategoryVO();
    BeanUtils.copyProperties(cat, vo);
    long productCount = categoryService.countUserGoodsProducts(cat.getId());
    vo.setProductCount(productCount);
    return new ResponseObject<>(vo);
  }

  /**
   * 删除商品分类信息（该商品分类下已经有了商品，则无法删除） v2/shop/category/remove
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/remove")
  public ResponseObject<Boolean> categoryRemove(@RequestParam String id) {
    categoryService.removeUesrGoods(id);
    return new ResponseObject<>(true);
  }

  /**
   * 批量删除商品分类信息 http://localhost:8888/v2/shop/category/batchRemove?ids=1&ids=2
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/batchRemove")
  public ResponseObject<Boolean> categoryRemove(@RequestParam String... ids) {
    categoryService.removeUesrGoodsCategory(ids);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/move-before")
  public ResponseObject<Boolean> categoryMoveBefore(@RequestParam String srcId,
      @RequestParam String desId) {
    categoryService.moveBefore(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/move-after")
  public ResponseObject<Boolean> categoryMoveAfter(@RequestParam String srcId,
      @RequestParam String desId) {
    categoryService.moveAfter(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/update-idx")
  public ResponseObject<Boolean> categoryUpdateIdx(
      @Valid @ModelAttribute CategoryIdxUpdateForm form) {
    if (form.getIdxs() == null || form.getIdxs().size() == 0) {
      categoryService.updateIdx(form.getCategoryIds());
    } else {
      for (int i = 0; i < form.getCategoryIds().size(); i++) {
        String cateId = form.getCategoryIds().get(i);
        Integer idx = form.getIdxs().get(i);
        categoryService.updateIdx(cateId, idx);
      }
    }
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/{id}/styles")
  public ResponseObject<ShopStyleVO> shopStyles(@PathVariable String id) {
    ShopStyleVO ss = shopService.loadShopStyle(id);
    return new ResponseObject<>(ss);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/styles")
  public ResponseObject<ShopStyleVO> myShopStyles() {
    ShopStyleVO ss = shopService.loadShopStyle();
    return new ResponseObject<>(ss);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/styles/update")
  public ResponseObject<ShopStyleVO> updateShopStyles(@ModelAttribute ShopStyleVO form) {
    if (!StringUtils.isEmpty(form.getListView())) {
      form.setListView(form.getListView().toLowerCase());
    }
    shopService.updateShopStyles(form);
    ShopStyleVO ss = shopService.loadShopStyle();
    return new ResponseObject<>(ss);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/styles/banners")
  public ResponseObject<List<BannerVO>> styleBanners() {
    String[] bannerKeyArray = StringUtils.split(bannerKeys, ",");
    List<BannerVO> r = new ArrayList<>(bannerKeyArray.length);
    for (String bannerKey : bannerKeyArray) {
      BannerVO b = new BannerVO();
      b.setImgKey(bannerKey);
      b.setImgUrl(bannerKey);
      r.add(b);
    }
    return new ResponseObject<>(r);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/statistics")
  public ResponseObject<Map<String, Object>> statistics() {
    return new ResponseObject<>(shopService.loadStatistics());
  }

  /**
   * b2b数据统计
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/statistics/2b")
  public ResponseObject<Map<String, Object>> statisticsB2B() {
    return new ResponseObject<>(shopService.loadB2BStatistics());
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/set_fragment")
  public ResponseObject<Boolean> saveFragment(boolean enable) {
    shopService.saveFragment(enable);
    return new ResponseObject<>(true);
  }

  /**
   * 保存第三方佣金设置(想去)
   */
  @ResponseBody
  @RequestMapping("/shop/saveThirdCommission")
  public ResponseObject<Boolean> saveThirdCommission(ThirdCommission form) {
    shopService.updateThirdCommission(form);
    return new ResponseObject<>(true);
  }

  /**
   * 保存本店铺的佣金设置--保存店铺
   */
  @ResponseBody
  @RequestMapping("/shop/saveShopCommission")
  public ResponseObject<Shop> saveShopCommission(String commisionRate) {
    if (StringUtils.isBlank(commisionRate)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请输入合法佣金");
    }

    BigDecimal rate = new BigDecimal(commisionRate);
    if (rate.compareTo(BigDecimal.ZERO) < 0 || rate.compareTo(new BigDecimal(1)) > 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请输入合法佣金");
    }

    Shop shop = new Shop();
    shop.setId(getCurrentUser().getShopId());
    shop.setCommisionRate(rate);
    shopService.update(shop);
    return new ResponseObject<>(shopService.load(getCurrentUser().getShopId()));
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/product/list")
  public ResponseObject<Map<String, Object>> list(String order,
      String direction, String category, String isGroupon, Pageable pageable,
      HttpServletRequest request) {
    Map<String, Object> aRetMap = new HashMap<>();
    List<Product> products = new ArrayList<>();
    List<ProductDistributor> productDistributors = null;
    String shopId = request.getParameter("shopId");
    if (shopId == null || "".equals(shopId)) {
      shopId = this.getCurrentIUser().getShopId();
    }
    if (category == null || category.equals("")) {
      productDistributors = productDistributorMapper.listByShopId(shopId, category, pageable);
      for (ProductDistributor productDistributor : productDistributors) {
        String productId = productDistributor.getProductId();
        Product product = productService.load(productId);
        products.add(product);
      }
    } else {
      ShopTree tree = shopTreeService.selectRootShopByShopId(shopId);
      products = productMapper.listProductsBySales(tree.getRootShopId(),
          categoryService.load(category).getSourceCategoryId(), isGroupon, pageable,
          Sort.Direction.DESC, "", null, "", "",
          null, null,0);
    }

    List<ProductVOEx> exs = new ArrayList<>();
    ProductVOEx ex = null;
    for (Product product : products) {
      ex = new ProductVOEx(new ProductVO(product), getProductShareUrl(request, product.getCode()),
          product.getImg(), null);
      if (product.getCommissionRate() != null) {
        ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
      }
      long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
      ex.setCountSeller(count);
      exs.add(ex);
    }
    aRetMap.put("list", exs);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * app:我的店铺单品排序更新
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/product/list/sort")
  public ResponseObject<Boolean> updataShopProductSort(String sortIds) {
    if (StringUtils.isBlank(sortIds)) {
      return new ResponseObject<>("店铺上架单品为0", GlobalErrorCode.UNKNOWN);
    }
    String shopId = getCurrentIUser().getShopId();
    List<String> productIds = JSON.parseArray(sortIds, String.class);//传递过来的为json类型的id集合
    int result = shopService.sortShopProducts(productIds, shopId);
    if (result != 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }


  /**
   * app:我的店铺品牌排序更新
   */
  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/category/listOnsale/sort")
  public ResponseObject<Boolean> updataShopCategorySort(String sortIds) {
    if (StringUtils.isBlank(sortIds)) {
      return new ResponseObject<>("店铺上架品牌为0", GlobalErrorCode.UNKNOWN);
    }
    List<String> categoryIds = JSON.parseArray(sortIds, String.class);//传递过来的为json类型的id集合
    int result = categoryService.sortShopCategory(categoryIds);
    if (result != 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX
      + "/qrcode", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] generateQrcode(HttpServletRequest req) {

    //生成一张制定规格的白色图片
    //写入店铺：店铺名
    //将banner覆盖在白色图片的指定位置
    //将img覆盖到banner的指定位置
    //将二维码覆盖到指定位置
    int width = 1080;
    int height = 960;
    String shopText = "店铺:					";
    String shopId = getCurrentUser().getShopId();
    Shop shop = shopService.load(shopId);
    String shopName = shop.getName();
    String qrcodeHelp = "扫描或者微信长按识别二维码";
    String nameRow = shopText + shopName;
//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
//		String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/186X186.png";
    String img = shop.getImg();
    String banner = shop.getBanner();
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String imgUrl = resourceFacade.resolveUrl(img);
    String bannerUrl = resourceFacade.resolveUrl(banner);
    int bannerW = 1080;
    int bannerH = 384;
    int imgW = 184;
    int imgH = 184;
    BufferedImage imageBanner = ImgUtils.getImageFromNetByUrl(bannerUrl);
    BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
    BufferedImage imageBannerResize = ImgUtils.resizeImage(imageBanner, bannerW, bannerH);
    BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, imgW, imgH);
    BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);
    BufferedImage ImageQrcode = null;
    try {
      HashMap<EncodeHintType, Object> hints = new HashMap<>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);

      BitMatrix byteMatrix;
      byteMatrix = new MultiFormatWriter()
          .encode(getShareUrl(req), BarcodeFormat.QR_CODE, 240, 240, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (WriterException e) {
      log.error("generateQrcode error", e);
      throw new RuntimeException(e);
    }

    Font font = new Font("宋体", Font.PLAIN, 40);
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setBackground(Color.WHITE);
    g2.clearRect(0, 0, width, height);
    g2.setPaint(Color.black);
    g2.setFont(font);

    g2.drawString(nameRow, width / 16, height / 16);
    g2.drawImage(imageBannerResize, 0, height / 8, null);
    g2.drawImage(imageImgRect, width / 16, height / 4, null);
    g2.drawImage(ImageQrcode, width / 16, height / 4 + imageBannerResize.getHeight(), null);
    g2.drawString(qrcodeHelp, width / 3,
        height / 4 + imageBannerResize.getHeight() + ImageQrcode.getHeight() / 2);
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。


  }

  private String getShareUrl(HttpServletRequest req) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/shop/" + getCurrentUser().getShopId()
            + "?currentSellerShopId=" + getCurrentUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }


  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/user/userInfo")
  public ResponseObject<Map> getUserInfo(@RequestParam(required = false) String userId) {
    // 用户信息
    if (userId == null || userId.equals("")) {
      userId = this.getCurrentIUser().getId();
    }
    User user = userService.loadUserEmployeeVO(userId);
    if (user == null) {
      throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "用户不存在");
    }
    // 店铺信息
    Shop shop = shopService.load(user.getShopId());
    ShopVO shopVO = new ShopVO(shop);
    if (StringUtils.isNotBlank(shopVO.getImg())) {
      shopVO.setImgUrl(shop.getImg());
    }
    if (StringUtils.isNotBlank(shopVO.getBanner())) {
      shopVO.setBannerUrl(shop.getBanner());
    }
    HashMap map = new HashMap();
    map.put("user", user);
    map.put("shop", shopVO);

    UserAgentVO userAgentVO = userAgentService.selectByUserId(user.getId());
    map.put("userAgent", userAgentVO);

    // 是否合伙人
    String rootShopId = shopTreeService.selectRootShopByShopId(shop.getId()).getRootShopId();
    UserPartner userPartner = userPartnerService
        .selectUserPartnersByUserIdAndShopId(userId, rootShopId);
    if (userPartner != null) {
      map.put("isPartner", true);
    } else {
      map.put("isPartner", false);
    }
    return new ResponseObject<Map>(map);
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/next/twitterList")
  public ResponseObject<List<TwitterInfoVO>> nextTwitterInfo(
      @RequestParam(required = false) String userId, Pageable pageable,
      HttpServletRequest request) {
    //用户名，角色，状态，分成比例，当月佣金
    List<TwitterInfoVO> listTwitter = new ArrayList<>();
    User user = new User();
    if (userId == null || userId.equals("")) {
      userId = this.getCurrentIUser().getId();
    }
    String shopId = userService.load(userId).getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<ShopTree> listTree = shopTreeService.listDirectChildrenPage(rootShopId, shopId, pageable);
    Long total = shopTreeService.countDirectChildren(rootShopId, shopId);
    for (ShopTree shopTree : listTree) {
      TwitterInfoVO twitterInfoVO = new TwitterInfoVO();
      String desShopId = shopTree.getDescendantShopId();
      String desUserId = shopService.load(desShopId).getOwnerId();
      User desUser = userService.load(desUserId);

      // 这个下级是否还有下级，以及还有多少个下级人员
      long count = shopTreeService.countDirectChildren(rootShopId, desShopId);
      twitterInfoVO.setCount(count);
      if (count > 0) {
        twitterInfoVO.setIsLeaf(false);
      } else {
        twitterInfoVO.setIsLeaf(true);
      }

      twitterInfoVO.setId(desUserId);
      twitterInfoVO.setName(desUser.getName());
      if (desUser.getAvatar() != null) {
        twitterInfoVO.setAvatar(desUser.getAvatar());
      }
      UserTwitter userTwitter = userTwitterService.selectByUserIdAndShopId(desUserId, rootShopId);
      if (userTwitter != null) {
        twitterInfoVO.setRole("推客");
        twitterInfoVO.setStatus(userTwitter.getStatus().toString());
        //查找比例
        UserCard userCard = userCardService.selectUserCardsByUserIdAndShopId(userId, rootShopId);
        //新增推客默认都是大B下面的银卡用户。默认一个大B只有一个银卡配置，推客的金银卡通过job每个月动态变更，
        if (userCard != null) {
          String cardId = userCard.getCardId();
          FamilyCardSetting setting = familyCardSettingService.load(cardId);
          if (setting != null) {
            twitterInfoVO.setRate(new BigDecimal(setting.getPersonalCmRate()));
          }

        } else {
          //卖家找不到对应卡信息 默认为大b下的银卡用户
          FamilyCardSetting silverCardSetting = familyCardSettingService
              .loadSilverCardSetting(rootShopId);
          twitterInfoVO.setRate(new BigDecimal(silverCardSetting.getPersonalCmRate()));
        }
        twitterInfoVO.setIncome(commissionService.getFeeByMonth(desUserId));
      }
      UserPartner userPartner = userPartnerService
          .selectUserPartnersByUserIdAndShopId(desUserId, rootShopId);
      if (userPartner != null) {
        twitterInfoVO.setRole("合伙人");
        twitterInfoVO.setStatus(userPartner.getStatus().toString());
        //查找比例
        PartnerSetting partnerSetting = partnerSettingService.loadByShopId(rootShopId);
        if (partnerSetting != null) {
          twitterInfoVO.setRate(new BigDecimal(partnerSetting.getCmRate()));
        }

        twitterInfoVO.setIncome(commissionService.getFeeByMonth(desUserId));
      }
      listTwitter.add(twitterInfoVO);

    }
    return new ResponseObject(listTwitter);
  }

  private String getProductShareUrl(HttpServletRequest req, String code) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/p/" + code + "?currentSellerShopId="
            + getCurrentUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }


}
