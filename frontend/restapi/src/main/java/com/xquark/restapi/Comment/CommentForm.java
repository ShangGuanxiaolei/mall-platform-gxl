package com.xquark.restapi.Comment;

import com.xquark.dal.type.CommentType;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 2018/4/19. DESC:
 */
public class CommentForm {

  /**
   * 被评论对象的id
   */
  @NotBlank(message = "评价id不能为空")
  private String objId;

  private String content;

  /**
   * 评论对象的类型
   */
  @NotNull(message = "评价类型不能为空")
  private CommentType type;

  @NotNull(message = "评价星级不能为空")
  private Integer star;

  public String getObjId() {
    return objId;
  }

  public void setObjId(String objId) {
    this.objId = objId;
  }

  public String getContent() {
    return StringUtils.defaultIfBlank(content, "好评!");
  }

  public void setContent(String content) {
    this.content = content;
  }

  public CommentType getType() {
    return type;
  }

  public void setType(CommentType type) {
    this.type = type;
  }

  public Integer getStar() {
    return star;
  }

  public void setStar(Integer star) {
    this.star = star;
  }
}
