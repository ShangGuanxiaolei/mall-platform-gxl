package com.xquark.restapi.helper;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 17-12-7. DESC:
 */
@ApiModel(value = "帮助表单")
public class HelperForm {

  @ApiModelProperty(value = "id")
  private String id;

  @ApiModelProperty(value = "标题", required = true)
  @NotBlank(message = "请输入标题")
  private String title;

  @NotBlank(message = "请输入简介")
  @ApiModelProperty(value = "简介", required = true)
  private String intro;

  @NotBlank(message = "请上传图片")
  @ApiModelProperty(value = "图标", required = true)
  private String icon;

  @ApiModelProperty(value = "排序号", required = true)
  @NotNull(message = "请填写排序号")
  private Integer sort;

  @ApiModelProperty(value = "内容", required = true)
  @NotBlank(message = "请输入内容")
  private String content;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIntro() {
    return intro;
  }

  public void setIntro(String intro) {
    this.intro = intro;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
