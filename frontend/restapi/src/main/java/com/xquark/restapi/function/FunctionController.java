package com.xquark.restapi.function;

import com.xquark.cache.ButtonCache;
import com.xquark.dal.model.Function;
import com.xquark.dal.vo.FunctionVO;
import com.xquark.dal.vo.MerchantRoleVO;
import com.xquark.function.FunctionService;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantRoleService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by wangxinhua on 17-4-22. DESC:
 */
//@RestController
//@ApiIgnore
public class FunctionController {

  @Autowired
  private FunctionService functionService;

  @Autowired
  private MerchantRoleService merchantRoleService;

  @Autowired
  private ButtonCache buttonCache;

  /**
   * 保存按钮
   *
   * @param function 按钮对象
   * @return 是否成功
   */
  @RequestMapping(value = "/function/save", method = RequestMethod.POST)
  public ResponseObject<Boolean> save(Function function) {

    if (StringUtils.isBlank(function.getName()) || StringUtils.isBlank(function.getHtmlId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "按钮名或ID不能为空");
    }
    function.setArchive(false);
    return new ResponseObject<>(functionService.save(function));

  }

  /**
   * 按钮表格对应的数据
   *
   * @param moduleId 菜单id
   * @param order 排序
   * @param pageable 分页
   * @param direction 方向
   * @return Map
   */
  @RequestMapping(value = "/function/listTable", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listTable(String moduleId, String order,
      Pageable pageable, String direction) {

    if (StringUtils.isBlank(direction)) {
      direction = "ASC";
    }

    List<FunctionVO> functionList;
    Integer total;
    Map<String, Object> result = new HashMap<>();
    if (StringUtils.isBlank(moduleId) || "0".equals(moduleId)) {
      functionList = functionService
          .loadFunctionVOList(order, pageable, Direction.fromString(direction));
      total = functionService.count();
    } else {
      functionList = functionService
          .loadFunctionVOList(moduleId, order, pageable, Direction.fromString(direction));
      total = functionService.count(moduleId);
      result.put("moduleId", moduleId);
    }

    result.put("total", total);
    result.put("list", functionList);
    return new ResponseObject<>(result);

  }

  /**
   * 列出按钮对应的角色，并设置是否选中
   *
   * @param id 按钮id
   * @param settings enable 或 show，没有指定则不勾选
   * @param pageable 分页
   * @param direction 数据排序方向
   * @return Map
   */
  @RequestMapping(value = "/function/listRoleTable", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listRoles(@RequestParam("functionId") String id,
      @RequestParam("settings") String settings, Pageable pageable, String direction) {

    if (StringUtils.isBlank(direction)) {
      direction = "ASC";
    }

    List<MerchantRoleVO> roles = merchantRoleService.list(pageable, null);
    List<String> permitRoles;

    Map<String, Object> resultMap = new HashMap<>();
    if ("show".equals(settings)) {
      permitRoles = functionService.loadVisibleRoles(id);
    } else if ("enable".equals(settings)) {
      permitRoles = functionService.loadEnableRoles(id);
    } else {
      // 没有正确设置settings参数则不设置isSelect
      permitRoles = new ArrayList<>();
    }

    if (roles.size() > 0 && permitRoles.size() > 0) {
      for (MerchantRoleVO roleVO : roles) {
        for (String role : permitRoles) {
          if (roleVO.getRoleName().equals(role)) {
            roleVO.setIsSelect(true);
            break;
          }
        }
      }
    }
    // 取非ROOT和非BASIC角色
    resultMap.put("total", merchantRoleService.selectNormalCnt());
    resultMap.put("list", roles);
    return new ResponseObject<>(resultMap);
  }

  /**
   * 修改按钮的角色
   *
   * @param functionId 按钮id
   * @param settings 修改可见还是可用
   * @param idsToSave 新绑定的角色id
   * @param idsToRemove 要移除的角色id
   * @return 是否成功
   */
  @RequestMapping(value = "/function/modifyRole", method = RequestMethod.POST)
  public ResponseObject<Boolean> modifyRole(@RequestParam String functionId,
      @RequestParam("settings") String settings, String idsToSave, String idsToRemove) {

    Boolean result;
    String[] rolesToSave = null;
    String[] rolesToRemove = null;

    if (!StringUtils.isBlank(idsToSave)) {
      rolesToSave = idsToSave.split(",");
    }
    if (!StringUtils.isBlank(idsToRemove)) {
      rolesToRemove = idsToRemove.split(",");
    }
    if ("show".equals(settings)) {
      result = functionService.modifyVisibleRoles(functionId, rolesToSave, rolesToRemove);
    } else if ("enable".equals(settings)) {
      result = functionService.modifyEnableRoles(functionId, rolesToSave, rolesToRemove);
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "setting必须为show或enable");
    }

    //TODO wangxh 暂时刷新所有按钮缓存
    buttonCache.refresh();

    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/function/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable("id") String id) {
    Boolean result = functionService.delete(id);
    return new ResponseObject<>(result);
  }

}
