package com.xquark.restapi.grandSale;

import com.xquark.dal.model.GrandSaleStadium;
import com.xquark.dal.model.Product;
import com.xquark.dal.vo.GrandSaleKvVO;
import com.xquark.dal.vo.GrandSalePromotionVO;
import com.xquark.dal.vo.GrandSaleStadiumVO;
import com.xquark.dal.vo.GrandSaleTimelineVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.grandSale.GrandSaleKvService;
import com.xquark.service.grandSale.GrandSaleModuleService;
import com.xquark.service.grandSale.GrandSalePromotionService;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.service.grandSale.GrandSaleTimelineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 520大型促销主会场 Controller 层
 */
@RestController
@RequestMapping("/grandSale")
public class GrandSalePromotionController extends BaseController {

  @Autowired
  private GrandSaleKvService grandSaleKvService;

  @Autowired
  private GrandSalePromotionService grandSalePromotionService;

  @Autowired
  private GrandSaleModuleService grandSaleModuleService;

  @Autowired
  private GrandSaleTimelineService grandSaleTimelineService;

  @Autowired
  private GrandSaleStadiumService grandSaleStadiumService;

  /**
   * 获取主KV对象
   * @param grandSaleId 大型促销活动表id
   * @return 主KV对象
   */
  @Deprecated
  @RequestMapping(value = "/getGrandSaleKv", method = RequestMethod.GET)
  public ResponseObject<List<GrandSaleKvVO>> getGrandSaleKv(@RequestParam Integer grandSaleId) {
    List<GrandSaleKvVO> grandSaleKvVOList = grandSaleKvService.selectByGrandSaleId(grandSaleId);
    if (grandSaleKvVOList == null || grandSaleKvVOList.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleKvVOList);
  }

  /**
   * 获取大型活动促销活动模块
   * @param grandSaleId 大型促销活动表id
   * @return 大型活动促销活动模块
   */
  @Deprecated
  @RequestMapping(value = "/getGrandSaleModule", method = RequestMethod.GET)
  public ResponseObject<Map> getGrandSaleModule(@RequestParam Integer grandSaleId) {
    Map map = grandSaleModuleService.selectByGrandSaleId(grandSaleId);
    if (map == null || map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }

  /**
   * 客户端会场入口
   * @return 大型促销活动对象
   */
  @RequestMapping(value = "/getGrandSalePromotion", method = RequestMethod.GET)
  public ResponseObject<GrandSalePromotionVO> getGrandSalePromotion() {
    GrandSalePromotionVO grandSalePromotionVO = grandSalePromotionService.selectGrandSale(getCurrentUser());
    if (grandSalePromotionVO == null) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSalePromotionVO);
  }

  /**
   * 获取大型活动促销时间轴
   * @param grandSaleId 大型促销活动表id
   * @return 大型活动促销时间轴
   */
  @RequestMapping(value = "/getGrandSaleTimeline", method = RequestMethod.GET)
  public ResponseObject<List<GrandSaleTimelineVO>> getGrandSaleTimeline(@RequestParam Integer grandSaleId) {
    List<GrandSaleTimelineVO> grandSaleTimelineVOList = grandSaleTimelineService.selectByGrandSaleId(grandSaleId);
    if (grandSaleTimelineVOList == null || grandSaleTimelineVOList.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleTimelineVOList);
  }

  /**
   * 获取主会场KV图及活动模块信息
   * @param grandSaleId 大型促销活动表id
   * @return 主会场信息
   */
  @RequestMapping(value = "/getGrandSale", method = RequestMethod.GET)
  public ResponseObject<Map<Object, Object>> getGrandSale(@RequestParam Integer grandSaleId) {
    Map<Object, Object> map = grandSaleModuleService.selectByGrandSaleId(grandSaleId);
    List<GrandSaleKvVO> grandSaleKvVOList = grandSaleKvService.selectByGrandSaleId(grandSaleId);
    map.put("grandSaleKvList", grandSaleKvVOList);
    return new ResponseObject<>(map);
  }

  /**
   * 获取大型促销活动分会场入口按钮数据
   * @param grandSaleId 大型促销活动表id
   * @return 大型活动促销分会场
   */
  @RequestMapping(value = "/getGrandSaleStadium", method = RequestMethod.GET)
  public ResponseObject<List<GrandSaleStadiumVO>> getGrandSaleStadium(@RequestParam Integer grandSaleId) {
    List<GrandSaleStadiumVO> grandSaleStadiumVOList = grandSaleStadiumService.selectByGrandSaleId(grandSaleId);
    if (grandSaleStadiumVOList == null || grandSaleStadiumVOList.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleStadiumVOList);
  }

  /**
   * 获取所有的会场信息，供后台配置轮播图使用
   * @return 会场信息
   */
  @RequestMapping(value = "/getAllStadium", method = RequestMethod.GET)
  public ResponseObject<List<GrandSaleStadium>> getAllStadium() {
    List<GrandSaleStadium> grandSaleStadiumList = grandSaleStadiumService.selectAll();
    if (grandSaleStadiumList == null || grandSaleStadiumList.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleStadiumList);
  }

  /**
   * 分会场获取分类和banner
   * @return map
   */
  @RequestMapping(value = "/getStadiumBannerAndCategory", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> getStadiumBannerAndCategory(@RequestParam String stadiumType) {
    Map<String, Object> map = grandSaleStadiumService.selectBannerAndCategory(stadiumType);
    if (map == null || map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }

  /**
   * 获取分会场商品列表
   * @param stadiumType 分会场类型
   * @return 分会场商品列表
   */
  @RequestMapping(value = "/getStadiumProduct", method = RequestMethod.POST)
  public ResponseObject<Map<String, Object>> getStadiumProduct(@RequestParam String stadiumType,
                                                               @RequestParam String categoryId,
                                                               @RequestParam String grandSaleId,
                                                               Pageable pageable) {
    List<Product> products = grandSaleStadiumService.selectStadiumProduct(stadiumType, categoryId, pageable,grandSaleId);
    if (products == null || products.isEmpty()) {
      return new ResponseObject<>();
    }
    Map<String, Object> map = new HashMap<>();
    map.put("list", products);
    map.put("count", grandSaleStadiumService.countProductsByCategoryIds(stadiumType, categoryId));
    return new ResponseObject<>(map);
  }

  /**
   * 获取分享接口数据
   * @param stadiumType 分会场类型
   * @return 分享接口数据
   */
  @RequestMapping(value = "/shareGranSale", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> shareGranSale(@RequestParam String stadiumType) {
    Map<String, Object> map = grandSaleStadiumService.selectShareMessage(stadiumType);
    if (map == null || map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }

}
