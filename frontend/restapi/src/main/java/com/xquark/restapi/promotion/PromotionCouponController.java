package com.xquark.restapi.promotion;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shopTree.ShopTreeService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 优惠券restapicontroller Created by chh on 17-08-02.
 */
@Controller
@ApiIgnore
public class PromotionCouponController extends BaseController {

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private ShopTreeService shopTreeService;

  /**
   * 查询当前可用的优惠券，供web端调用
   */
  @ResponseBody
  @RequestMapping("/promotionCoupon/listForApp")
  public ResponseObject<List<PromotionCouponVo>> listForApp(Pageable pageable) {
    String shopId = ((User) getCurrentIUser()).getCurrentSellerShopId();
    // 只能查询到当前店铺的优惠券
    List<PromotionCouponVo> promotions = promotionCouponService.listForApp(pageable, shopId);
    return new ResponseObject<>(promotions);
  }

  /**
   * 查询我的优惠券，供web端调用
   */
  @ResponseBody
  @RequestMapping("/promotionCoupon/myCoupon")
  public ResponseObject<List<UserCouponVo>> myCoupon(
      @RequestParam(required = false) String status) {
    String userId = getCurrentIUser().getId();
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotBlank(userId)) {
      params.put("uId", userId);
    }
    String[] statusArr = null;
    if (StringUtils.isNoneBlank(status)) {
      if (StringUtils.equals(status, CouponStatus.USED.name())) {
        // 如果前台需要查询已使用的优惠券, 则同时需要查出当前被锁定的优惠券
        statusArr = new String[]{status, CouponStatus.LOCKED.name()};
      } else {
        statusArr = new String[]{status};
      }
    }
    List<UserCouponVo> couponVos = userCouponService
        .selectMyCouponListPage(params, null, statusArr);
    return new ResponseObject<>(couponVos);
  }

  @ResponseBody
  @RequestMapping(value = "/promotionCoupon/acquire")
  public ResponseObject<Boolean> acquireShopCoupon(@RequestParam(required = false) String phone,
      @RequestParam("promotionCouponId") String promotionCouponId,
      @RequestParam(value = "shopId", required = false) String shopId, HttpServletRequest request,
      HttpServletResponse response) {
    if (StringUtils.isEmpty(shopId)) {
      ShopTree rootShopTree = shopTreeService.getRootShop();
      shopId = rootShopTree.getDescendantShopId();
    }

    //获取优惠券总数判断是否可以领取
    Long remainder = promotionCouponService.selectRemainder(promotionCouponId);
    if (remainder < 1) {
      throw new BizException(GlobalErrorCode.COUPON_OUT_OF_STOCK, "你来晚了, 优惠券已被领完");
    }
    // 用户判断获取用户ID
    User user = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        user = (User) principal;
      }
    }

    if (user != null) {

      // 判断优惠券是否可使用
      PromotionCouponVo promotionCouponVo = promotionCouponService.load(promotionCouponId);
      if (promotionCouponVo.getStatus() != CouponStatus.VALID
          || promotionCouponVo.getValidTo().compareTo(new Date()) < 0) {
        throw new BizException(GlobalErrorCode.COUPON_OUT_OF_TIME, "领取失败，该张券已经过期");
      }

      // 判断是否达到领取上限
      int acquireLimit = promotionCouponVo.getAcquireLimit();
      Long acquireCount = userCouponService
          .countAcquirePromotionCoupons(user.getId(), promotionCouponId);
      if (acquireCount >= acquireLimit) {
        throw new BizException(GlobalErrorCode.COUPON_ACQUIRED, "您已领取此优惠券请勿重复领取");
      }
      //添加记录到用户优惠券中
      UserCoupon userCoupon = new UserCoupon();
      userCoupon.setCouponId(promotionCouponId);
      userCoupon.setPhone(user.getPhone());
      userCoupon.setStatus(CouponStatus.VALID);
      userCoupon.setUserId(user.getId());
      String ua = request.getHeader("User-Agent");
      String ip = getIpAddr(request);
      userCoupon.setDeviceIp(ip);
      if (ua != null && ua.length() >= 512) {
        ua = ua.substring(0, 511);
      }
      userCoupon.setDeviceAgent(ua);
      userCoupon.setShopId(shopId);

      int result = userCouponService.acquireCoupon(userCoupon);
      if (result <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠券领取失败");
      }
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "优惠券领取失败");
    }

    return new ResponseObject<>(Boolean.TRUE);
  }

  private String getIpAddr(HttpServletRequest request) {
    String ip = request.getHeader("x-forwarded-for");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return ip;
  }

}
