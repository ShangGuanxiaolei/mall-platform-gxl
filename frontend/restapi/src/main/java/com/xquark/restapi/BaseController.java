package com.xquark.restapi;

import com.xquark.dal.IUser;
import com.xquark.dal.model.User;
import com.xquark.service.user.impl.UserServiceImpl;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Optional;

/**
 * rest-api工程的基类Controller
 *
 * @author odin
 */
@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
public class BaseController {

  protected Logger log = LoggerFactory.getLogger(getClass());

  protected final Integer DEFAULT_PAGE_SIZE = 20;
  @Autowired
  protected UserServiceImpl userService;

  /**
   * 请使用getCurrentIUser
   */
  public User getCurrentUser() {
    return userService.getCurrentUser();
  }

  public User User() {
    return ((User) getCurrentIUser());
  }

  /**
   * 判断当前用户是否为后端用户
   */
  protected Optional<Boolean> isFrontUser() {
    final IUser user = userService.getCurrentIUser();
    return Try.of(() -> (user instanceof User))
            .toJavaOptional();
  }

  public IUser getCurrentIUser() {
    return userService.getCurrentUser();
  }

  /**
   * 直接接受request 注：request中提供的getQueryString方法只对Get方法才能生效， 在我们不知道方法的情况下最好重写getQueryString
   */
  protected String getQueryString(HttpServletRequest request) {
    StringBuilder sbuf = new StringBuilder("");
    String name = null;
    for (Enumeration<String> names = request.getParameterNames(); names.hasMoreElements(); ) {
      name = names.nextElement();
      if (sbuf.toString().length() > 0) {
        sbuf.append("&");
      }
      sbuf.append(name);
      sbuf.append("=");
      sbuf.append(request.getParameter(name));
//	        System.out.println("name:" + name + "  values=" + values);
    }

    return sbuf.toString();
  }

  protected User loadExtUser(String domain, String extUid) {
    User user = userService.loadExtUser(domain, extUid);
    return user;
  }
}
