package com.xquark.restapi;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午8:51
 */
public class AddressSequenceVO {

  private final Long addressId;

  public AddressSequenceVO(Long addressId) {
    this.addressId = addressId;
  }

  public Long getAddressId() {
    return addressId;
  }

}
