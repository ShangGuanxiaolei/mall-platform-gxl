package com.xquark.restapi.user;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class RegisterForm {

  private static final String NOT_BLANK_MESSAGE = "{valid.notBlank.message}";
  private static final String MOBILE_MESSAGE = "{valid.mobile.message}";

  @NotBlank(message = NOT_BLANK_MESSAGE)
  @Pattern(regexp = "(13\\d|14[57]|15[^4,\\D]|17[678]|18\\d)\\d{8}|170[059]\\d{7}", message = MOBILE_MESSAGE)
  private String mobile;

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

}
