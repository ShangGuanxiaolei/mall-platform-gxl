package com.xquark.restapi.tags;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Tags;
import com.xquark.dal.type.TagsCategory;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.tags.TagsService;
import com.xquark.service.tags.impl.ProductTagsService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-11-16. DESC:
 */
@RestController
@RequestMapping("/tags")
@Api(value = "标签接口")
public class TagsController {

  private ProductTagsService productTagsService;

  private ProductService productService;

  private TagsService tagsService;

  @Autowired
  public void setProductTagsService(ProductTagsService productTagsService) {
    this.productTagsService = productTagsService;
  }

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  @Autowired
  public void setTagsService(TagsService tagsService) {
    this.tagsService = tagsService;
  }

  /**
   * 仅保存标签
   *
   * @param tag 标签对象
   * @return true or false
   */
  @RequestMapping(value = "/save", method = RequestMethod.POST)
  @ApiOperation(value = "保存标签", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> save(
      @Validated @ApiParam(value = "需要保存的标签对象", required = true) TagsForm tag,
      @ApiIgnore Errors errors) {
    ControllerHelper.checkException(errors);
    Tags tags = Transformer.fromBean(tag, Tags.class);
    boolean result = tagsService.save(tags);
    return new ResponseObject<>(result);
  }

  /**
   * 为某个商品打上标签
   *
   * @param productId 商品id
   * @param tag 标签表单
   * @return true or false
   */
  @RequestMapping(value = "/tagProduct/{productId}", method = RequestMethod.POST)
  @ApiOperation(value = "为商品打标签", notes = "若标签不存在则同时创建标签", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Boolean> tagProduct(@PathVariable("productId") String productId,
      @Validated @ApiParam(value = "标签对象", required = true) final TagsForm tag,
      @ApiIgnore Errors errors) {
    ControllerHelper.checkException(errors);
    if (!productService.exists(productId)) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "商品不存在");
    }
    Tags tagToSave = Transformer
        .fromBean(tag, Tags.class);
    boolean result = productTagsService.saveWithRelation(productId, tagToSave);
    return new ResponseObject<>(result);
  }

  /**
   * 根据标签名删除标签
   *
   * @param name 标签名
   * @return true or false
   */
  @RequestMapping(value = "/delete", method = RequestMethod.POST)
  @ApiOperation(value = "删除标签", notes = "根据标签名删除标签", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Boolean> delete(@RequestParam("name") String name) {
    boolean result = tagsService.deleteByName(name);
    return new ResponseObject<>(result);
  }

  /**
   * 删除商品的标签
   *
   * @param productId 商品id
   * @param name 标签名
   * @return true or false
   */
  @RequestMapping(value = "/unTag/{productId}", method = RequestMethod.POST)
  @ApiOperation(value = "删除商品标签", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Boolean> delete(@PathVariable("productId") String productId,
      @RequestParam("name") String name) {
    boolean result = productTagsService.unTag(productId, name);
    return new ResponseObject<>(result);
  }

  /**
   * 根据标签id更新标签
   *
   * @param tag tag对象
   * @return true or false
   */
  @RequestMapping(value = "/update", method = RequestMethod.POST)
  @ApiOperation(value = "更新标签", notes = "根据标签id更新标签", produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<Boolean> update(
      @Validated @ApiParam(value = "需要更新的标签对象", required = true) TagsForm tag,
      @ApiIgnore Errors errors) {
    ControllerHelper.checkException(errors);
    Tags tagsToUpdate = new Tags();
    BeanUtils.copyProperties(tag, tagsToUpdate);
    boolean result = tagsService.update(tagsToUpdate);
    return new ResponseObject<>(result);
  }

  /**
   * 根据标签名查询关联商品 db实现
   *
   * @param pageable 分页对象，用于指定前几条商品
   * @param names 标签名 多个标签名做并集处理
   * @return {@link List<Product>} 商品集合
   */
  @RequestMapping(value = "/listProducts", method = RequestMethod.POST)
  @ApiOperation(value = "根据标签列出改标签关联的商品", notes = "支持传入多个name，商品做并集处理, 按商品销量排序",
      produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "POST")
  public ResponseObject<List<Product>> listProductsByTags(Pageable pageable,
      @RequestParam("names") String... names) {
    // TODO wangxinhua 用redis的set求并集提高搜索性能
    List<Product> list = productTagsService.listObjByTagNames(pageable, names);
    return new ResponseObject<>(list);
  }

  @RequestMapping(value = "/list")
  @ApiOperation(value = "查询所有标签", notes = "分页查询多个标签，按标签点击量排序",
      produces = MediaType.APPLICATION_JSON_VALUE, httpMethod = "GET")
  public ResponseObject<Map<String, Object>> list(Pageable pageable,
      @RequestParam(value = "category", required = false) TagsCategory tagsCategory) {
    Map<String, Object> result = tagsService.list(pageable, tagsCategory);
    return new ResponseObject<>(result);
  }

  /**
   * 根据商品id查询该商品对应的标签
   *
   * @param productId 商品id
   * @return {@link List<Tags>} 标签集合
   */
  @RequestMapping(value = "/list/{productId}", method = RequestMethod.POST)
  @ApiOperation(value = "根据商品查询该商品对应的标签", produces = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "POST")
  public ResponseObject<List<Tags>> listTagsByProductId(
      @PathVariable("productId") String productId, Pageable pageable) {
    List<Tags> tags = productTagsService.listTags(productId, pageable);
    return new ResponseObject<>(tags);
  }

}
