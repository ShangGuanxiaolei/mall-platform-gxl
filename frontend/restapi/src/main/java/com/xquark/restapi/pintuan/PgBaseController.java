package com.xquark.restapi.pintuan;

import com.google.common.base.Optional;
import com.xquark.dal.mapper.RecordPromotionPgTranInfoMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.type.BillType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.openapi.product.form.OrderSumbitFormApi;
import com.xquark.openapi.vo.OrderVOApi;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.ValidationUtils;
import com.xquark.service.address.AddressService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.bill.BillService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.mypiece.PieceMainOrderService;
import com.xquark.service.order.OrderUserDeviceService;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * @Author: tanggb
 * @Time: Create in 20:08 2018/9/11
 * @Description:
 * @Modified by:
 */
class PgBaseController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(PgBaseController.class);
    @Autowired
    private ZoneService zoneService;

    @Autowired
    private UserService userService;

    @Autowired
    private ShopService shopService;

    @Autowired
    private AuditRuleService auditRuleService;

    @Autowired
    private BillService billService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private PieceMainOrderService pieceMainOrderService;

    @Autowired
    private OrderUserDeviceService orderUserDeviceService;

    @Autowired
    private RecordPromotionPgTranInfoMapper recordPromotionPgTranInfoMapper;

    @Autowired
    private ProductService productService;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Value("${tech.serviceFee.standard}")
    private String serviceFeethreshold;

    @Value("${order.delaysign.date}")
    private int defDelayDate;

    /**
     * @Author: tanggb
     * @Description: 提交订单
     * @Param: form
     * @param request
     * @Return: ResponseObject<OrderVOApi>
     * @time:  2018/9/12 9:33
     * @Modified:
     */
    public ResponseObject<OrderVOApi> submitOrder( OrderSumbitFormApi form, HttpServletRequest request){

        MainOrder mainOrder = null;
        Address address = null;
        //1.校验发票信息
        Optional<OrderSumbitFormApi.BillFormType> billFormType = Optional.fromNullable(form.getBillFormType());
        Optional<? extends BaseBill> bill = checkInvoice(form, billFormType);

        //2.汇购网特殊要求，订单提交不会传递addressid过来，而会传consignee，phone,zoneId,street信息，zoneid对应的是汇购网的zone,
        //通过判断这些信息是否有存在的address，如果有则用存在的address，否则自动创建一个address

        //判断是否有地址，有地址则加载地址信息
        if (StringUtils.isNotEmpty(form.getAddressId())) {
            address = addressService.load(form.getAddressId());
        }
        //没有地址，则自动创建一个addresss
        if (StringUtils.isNotEmpty(form.getZoneId())) {
            Zone zone = zoneService.load(form.getZoneId());
            address = new Address();
            address.setConsignee(form.getConsignee());
            address.setZoneId(zone.getId());
            address.setStreet(form.getStreet());
            address.setPhone(form.getPhone());
            address.setCommon(false);
            address.setIsDefault(false);
            address = addressService.saveUserAddress(address, true);
            form.setAddressId(address.getId());
        }

        //3.查询总店商铺
        //3.1 防止多次提交
        mainOrder = unMultipleSubmit(form,address);
        //创建订单用户信息
        OrderUserDevice orderUserDevice = buildOrderUserDevice(request);
        orderUserDevice.setOrderId(mainOrder.getId());
        orderUserDevice.setUserId(mainOrder.getBuyerId());
        orderUserDeviceService.save(orderUserDevice);

        OrderVOApi orderVOApi = new OrderVOApi();

        MainOrderVO mainOrderVO = pieceMainOrderService.loadVO(mainOrder.getId());

        if (mainOrderVO == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "订单生成失败");
        }

        //4.存储发票信息
        if (bill.isPresent()) {
            bill.get().setOrderId(mainOrderVO.getId());
            if (billFormType.isPresent()) {
                switch (billFormType.get()) {
                    case PERSONAL:
                        billService.makePersonalBill((PersonalBill) bill.get());
                        break;
                    case COMPANY:
                        billService.makeCompanyBill((CompanyBill) bill.get());
                        break;
                    case ADDED:
                        billService.makeAddedTaxBill((AddedTaxBill) bill.get());
                        break;
                }
            }
        }

        orderVOApi.setMainOrderVO(mainOrderVO);
        BigDecimal goodsFee = BigDecimal.ZERO;
        BigDecimal totalFee = BigDecimal.ZERO;
        BigDecimal logisticsFee = BigDecimal.ZERO;
        for (OrderVO orderVO : mainOrderVO.getOrders()) {
            goodsFee = goodsFee.add(orderVO.getGoodsFee());
            totalFee = totalFee.add(orderVO.getTotalFee());
            logisticsFee = logisticsFee.add(orderVO.getLogisticsFee());
        }

        //5.订单买家信息，当自提方式无法带出订单用户信息时，取订单买家信息
        if (mainOrderVO.getOrders() != null && mainOrderVO.getOrders().size() > 0) {
            String buyerId = mainOrderVO.getOrders().get(0).getBuyerId();

            // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
            AuditRule auditRule = auditRuleService
                    .selectByOrderId(mainOrderVO.getOrders().get(0).getId());
            if (auditRule != null) {
                mainOrderVO.getOrders().get(0).setAuditType(auditRule.getAuditType());
            }
            User buyer = userService.load(buyerId);
            orderVOApi.setBuyer(buyer);
        }
        orderVOApi.setGoodsFee(goodsFee);
        orderVOApi.setTotalFee(totalFee);
        orderVOApi.setLogisticsFee(logisticsFee);

        return new ResponseObject<>(orderVOApi);
    }

    /**
     * @Author: tanggb
     * @Description:
     * @Param: mainOrder
     * @param form1
     * @Return:
     * @time:  2018/9/11 23:23
     * @Modified:
     */
    private MainOrder unMultipleSubmit(OrderSumbitFormApi form1,Address address){
        PieceOrderSumbitFormApi form = (PieceOrderSumbitFormApi) form1;
        //查询总店商铺
        Shop sh = shopService.loadRootShop();
        //判断重复提交
        if (StringUtils.isNotBlank(form.getOrderId())) {
            MainOrder mainOrder = pieceMainOrderService.load(form.getOrderId());
            return mainOrder;
        }

        // 3.1.1 下单计算优惠券逻辑
        Shop rootShop = shopService.loadRootShop();
        List<String> usingCoupons = form.getUsingCoupons();
        //3.1.2 活动类型
        PromotionType promotionType = form.getPromotionFrom();
        UserSelectedProVO userSelectedProVO = UserSelectedProVO.build(rootShop, usingCoupons);
        //3.1.3 判断优惠是否存在
        boolean coupons = (promotionType != null && promotionType != PromotionType.COUPON)&&CollectionUtils.isNotEmpty(usingCoupons);
        if (coupons) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "优惠券与活动不能同时选择");
        }
        OrderAddress oa = getOrderAddress(form);

        //3.1.4 直接购买
//        if (form.isDirectBuy()) {
            //3.1.4.1代销商品立即购买时保存productLiteralId FIXME dongsongjie 将参数放入form中传递
        if (StringUtils.isNotEmpty(form.getProLiteralId())) {
            //3.1.4.1.1根据商品编码查询商品
            Product product = productService.findProductById(form.getProLiteralId());
            if (product != null) {
                Shop shop = shopService.load(product.getShopId());
                if (shop != null) {
                }
            }
        }
        // 拼团相关
        String pTranCode = form.getPgTranInfoVo().getPieceGroupTranCode();
        //拼團折扣率
        BigDecimal pgDiscount = recordPromotionPgTranInfoMapper.selectPgDiscount(form.getPgTranInfoVo().getpCode(), form.getPgTranInfoVo().getpDetailCode());
        if (null == pgDiscount) {
            pgDiscount = BigDecimal.ZERO;
        }

        //3.1.4.1.2 直接下单主订单方法
        Integer isGroupHead = form.getPgMemberInfoVo().getIsGroupHead();
        MainOrder mainOrder = pieceMainOrderService
                .submitBySkuId2(form.getSkuId(), oa, form.getRemark(), userSelectedProVO,
                        address,
                        true, form.getQty(), sh, null, form.isUseDeduction(),
                        promotionType, form.getIsPrivilege(), form.isNeedInvoice(),
                        form.getSelectPoint(), form.getSelectCommission(), form.getUpLine(), pTranCode, pgDiscount.intValue(), isGroupHead);
        return mainOrder;
//        }   // 3.1.4.2 购物车下单
//        else if (form.isCartBuy()) {
//            //3.1.4.2.1 购物车下单主订单方法
//            MainOrder mainOrder = pieceMainOrderByCartService
//                    .submitBySkuIds2(form.getSkuIds(), oa, form.getShopRemarks(),
//                            userSelectedProVO, promotionType, address, true, sh, "",
//                            form.isUseDeduction(), form.isNeedInvoice(),
//                            form.getSelectPoint(), form.getSelectCommission(), form.getUpLine(), pCode);
//            return mainOrder;
//        } else {
//            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单提交参数不正确");
//        }

    }

    /**
     * 检查是否开票
     * @param form
     * @param billFormType
     * @return
     */
    private Optional<? extends BaseBill> checkInvoice(OrderSumbitFormApi form,Optional<OrderSumbitFormApi.BillFormType> billFormType){
        Optional<? extends BaseBill> bill = Optional.absent();

        //1.1 是否开发票
        if (null != form.isNeedInvoice() && form.isNeedInvoice()) {
            if (billFormType.isPresent()) {
                switch (billFormType.get()) {
                    case ADDED:
                        bill = Optional.fromNullable(form.getAddedTaxBill());
                        break;
                    case PERSONAL:
                        bill = Optional.fromNullable(form.getPersonalBill());
                        break;
                    case COMPANY:
                        bill = Optional.fromNullable(form.getCompanyBill());
                        break;
                }
                if (!bill.isPresent()) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "传递的发票表单类型不正确");
                } else {
                    Class<?> group = bill.get().getType().equals(BillType.ELECTRONIC) ?
                            ElectronicGroup.class : NormalGroup.class;
                    Set<ConstraintViolation<BaseBill>> ret = validator.validate(bill.get(), group);
                    if (CollectionUtils.isNotEmpty(ret)) {
                        String info = ValidationUtils.extractMessage(ret);
                        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, info);
                    }
                }
            } else {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "未传递发票表单的类型");
            }
        }
        return bill;
    }

    /**
     * 获得当前订单的收货地址和联系方式
     */
    private OrderAddress getOrderAddress(OrderSumbitFormApi form) {
        OrderAddress oa = new OrderAddress();
        //是否自提
        if ("1".equals(form.getIspickup())) {
            oa = null;
        } else {
            if (StringUtils.isNotEmpty(form.getAddressId())) {
                Address address = addressService.load(form.getAddressId());
                if (address == null) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "地址不存在");
                }
                BeanUtils.copyProperties(address, oa);
            } else {
                form.setStreet(form.getStreet().trim());
                BeanUtils.copyProperties(form, oa);
            }
        }
        return oa;
    }

    private OrderUserDevice buildOrderUserDevice(HttpServletRequest request) {
        OrderUserDevice device = new OrderUserDevice();
        device.setUserAgent(request.getHeader("User-Agent"));
        device.setClientIp(request.getRemoteAddr());
        device.setServerIp(request.getServerName());
        return device;
    }
}
