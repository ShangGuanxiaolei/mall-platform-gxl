package com.xquark.restapi.pintuan;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.dal.vo.OrderVO;
import com.xquark.interceptor.Token;
import com.xquark.openapi.product.form.OrderSumbitFormApi;
import com.xquark.openapi.vo.OrderVOApi;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.GroupService;
import com.xquark.service.pintuan.PromotionGroupDetailService;
import com.xquark.service.pintuan.TransPGService;
import com.xquark.service.pintuan.impl.RecordPromotionPgTranInfoServiceImpl;
import com.xquark.service.pricing.base.PieceStateException;
import com.xquark.thirds.umpay.api.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**拼团订单提交
 * @author  gx
 * @date 2018-09-05
 */
@Controller
@Api(value = "order", description = "拼团订单管理")
public class PieceOrderController extends PgBaseController {
    private static Logger logger = LoggerFactory.getLogger(PieceOrderController.class);

    @Value("${tech.serviceFee.standard}")
    private String serviceFeethreshold;

    @Value("${order.delaysign.date}")
    private int defDelayDate;

    @Autowired
    private TransPGService transPGService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private RecordPromotionPgTranInfoServiceImpl recordPromotionPgTranInfoService;

    @Autowired
    private PieceGroupPayCallBackService pieceGroupPayCallBackService;

    @Autowired
    private PromotionGroupDetailService promotionGroupDetailService;

    /**
     * 拼团订单提交
     */
    @Deprecated
    @ResponseBody
    @RequestMapping(value = "/pieceOrder/submitPieceOrder", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "拼团订单提交", notes = "订单提交后返回详情相关的vo", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @Token(save = true)
    public ResponseObject<OrderVOApi> submitPieceOrder(@RequestBody @Validated PieceOrderSumbitFormApi form, HttpServletRequest request){

        log.info("拼团订单提交");
        String pTranCode = form.getPgTranInfoVo().getPieceGroupTranCode();
        User user = (User) getCurrentIUser();
        //重复参团校验
        boolean hadJoined = pieceGroupPayCallBackService.hadJoined(pTranCode, user.getCpId().toString());
        if (hadJoined) {
            log.info("重复参团校验");
            return new ResponseObject(GlobalErrorCode.DUPLICATED_PIECE);
        }

        //参团应用锁=活动编码+拼团编码
        String lockKey = form.getPgTranInfoVo().getpCode() + form.getPgTranInfoVo().getPieceGroupTranCode();

        //如果是参团，则需要竞争并发锁
        if (form.getPgMemberInfoVo().getIsGroupHead() == null || form.getPgMemberInfoVo().getIsGroupHead() != 1) {
//            String pCode = form.getPgTranInfoVo().getpCode();
            //User usr = (User) getCurrentIUser();
            log.info("查询拼团信息，pTranCode=" + pTranCode);
            Map retTran = recordPromotionPgTranInfoService.selectAllByTranCode(pTranCode);
            PGTranInfoVo pgTranInfoVo = new PGTranInfoVo();
            pgTranInfoVo.setShareLink((String) retTran.get("share_Link"));
            pgTranInfoVo.setpCode((String) retTran.get("p_Code"));
            pgTranInfoVo.setGroupHeadId((String) retTran.get("group_Head_Id"));
            pgTranInfoVo.setPieceGroupTranCode(pTranCode);
            pgTranInfoVo.setGroupSkuCode((String) retTran.get("group_Sku_Code"));
            pgTranInfoVo.setGroupOpenMemberId((String) retTran.get("group_Open_Member_Id"));
            pgTranInfoVo.setPieceStatus((String) retTran.get("piece_Status"));
            pgTranInfoVo.setpDetailCode((String) retTran.get("p_Detail_Code"));
            //pg_member_info 表参团没数据
//            PGMemberInfoVo pgMemberInfoVo = new PGMemberInfoVo();
            //团员
//            pgMemberInfoVo.setIsGroupHead(0);
            // TODO 参团是不是白人有区别否
            //参团未支付，0.
//            pgMemberInfoVo.setStatus(0);
            //拼团成员详情编码1
//            pgMemberInfoVo.setPieceGroupDetailCode(form.getPgMemberInfoVo().getPieceGroupDetailCode());
//            form.setPgMemberInfoVo(pgMemberInfoVo);
            form.setPgMemberInfoVo(form.getPgMemberInfoVo());
            form.setPgTranInfoVo(pgTranInfoVo);
//            //参团资格的判断
//            boolean isNew = promotionGoodsService.IsNew(form.getPgMemberInfoVo().getMemberId());
//            if (isNew){
//                System.out.println("...");
//            }else{
//                ResponseObject<OrderVOApi> result = new ResponseObject<>();
//                result.setMoreInfo("不是白人无法参团");
//                return result;
//            }
            //实例化RedisUtil:拿RedisUtil实例
//            RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class,1000, 2000);
//            try {
                //拿锁
//                boolean isOk = redisUtils.lock(lockKey);
//                int times =3;
                //库存锁定失败则重试三次
//                if (!isOk) {
//                    while (times > 0) {
//                        isOk = redisUtils.lock(lockKey);
//                        if (isOk) {
//                            break;
//                        } else {
//                            times--;
//                        }
//                    }
//                }
//                //库存锁定失败则退出
//                if (!isOk) {
//                    return null;
//                }

//                boolean isFull = pieceGroupPayCallBackService.isGroupFullEx(pTranCode);
//
//                if (isFull) {
//                    return null;
//                }
                //拿锁成功，则下单

                log.info("开始下单，pTranCode=" + pTranCode);
                return doOrder(form, request);
//            } catch (InterruptedException e) {
//                logger.warn("get stock lock failed,", e);
//                return null;
//            } finally {
//                redisUtils.unlock(lockKey);
//            }
        } else {
//            //开团资格的判断
//            PromotionGoodsVO promotionGoodsVO = promotionGoodsService.selectUserQua(Integer.parseInt(form.getPgMemberInfoVo().getMemberId()));
//            String orgGroupQua = promotionGoodsVO.getOrgGroupQua();
//            if(!(orgGroupQua.contains("SO")||orgGroupQua.contains("SR")||orgGroupQua.contains("SO"))){
//                ResponseObject<OrderVOApi> orderVOApiResponseObject = new ResponseObject<>();
//                orderVOApiResponseObject.setMoreInfo("无开团资格");
//                return orderVOApiResponseObject;
//            }
            //如果是开团就直接下单
            return doOrder(form, request);
        }
    }

    /**
     * 订单执行
     * @param form1
     * @param request
     * @return
     */
    @Deprecated
    private ResponseObject<OrderVOApi> doOrder(OrderSumbitFormApi form1, HttpServletRequest request) {
        PieceOrderSumbitFormApi form = (PieceOrderSumbitFormApi) form1;
        // 老代码转换值到拼团
        form.getPgTranInfoVo().setGroupSkuCode(form.getSkuId());
        //ConfirmAndCheckVo confirmAndCheckVo,User user
        StockCheckBean stockCheckBean = new StockCheckBean();
        stockCheckBean.setP_code(form.getPgTranInfoVo().getpCode());
        //添加库存校验数量
        Integer pieceGroupNum =    promotionGroupDetailService.findPieceGroupNum(form.getPgTranInfoVo().getpCode());
        Integer pieceSkuLimit = promotionGroupDetailService.findPieceSkuLimit(form.getPgTranInfoVo().getpCode());
        stockCheckBean.setP_sku_num(pieceGroupNum*pieceSkuLimit);
        User usr = (User) getCurrentIUser();
        if(StringUtil.isNotEmpty(form.getTranCode())){
            stockCheckBean.setTranCode(form.getTranCode());//设置tranCode
        }

        //活动合法性校验
        Map<String,String> result = null;
        try {
            result = groupService.orderingCheck(stockCheckBean);
        } catch (PieceStateException e) {
            log.info("合法性校验失败，request=null");
            return new ResponseObject<>(GlobalErrorCode.DUPLICATED_PIECE);
        }
        if (null == request) {

            log.info("合法性校验失败，request=null");
            return new ResponseObject<>(GlobalErrorCode.DUPLICATED_PIECE);
        }
        ResponseObject<OrderVOApi> orderApi = new ResponseObject<>();
        //检查合法，则提交订单
        if (null == result.get("is_success")
                || "1".equals((String) result.get("is_success"))) {
            //提交订单
            log.info("提交订单");
            orderApi = super.submitOrder(form,request);

            //==================================================================
            //保存拼团信息
            //保存拼团成员详情
            form.getPgMemberInfoVo().setMemberId(usr.getCpId().toString());
            form.getPgTranInfoVo().setGroupHeadId(usr.getCpId().toString());
            form.getPgTranInfoVo().setGroupOpenMemberId(usr.getCpId().toString());
            form.getPgTranInfoVo().setGroupSkuCode(form.getSkuId());

            int isGroupMaster = 0;
            if (form.getPgMemberInfoVo().getIsGroupHead() != null && form.getPgMemberInfoVo().getIsGroupHead() == 1) {
                isGroupMaster =1;
            }
            log.info("保存拼团成员详情");

            String mainOrderNo = orderApi.getData().getMainOrderVO().getOrderNo();
            for (OrderVO orderVO : orderApi.getData().getMainOrderVO().getOrders()) {
                for (OrderItem orderItem : orderVO.getOrderItems()) {
                    transPGService.transPGService(form.getPgMemberInfoVo(),isGroupMaster,
                            form.getPgTranInfoVo(), mainOrderNo, orderVO.getOrderNo(), orderItem.getId(), usr);
                }
            }

        }
//      else{
//            // todo 合法性校验未通过，门户端友好返回,
//        String errorVOMsg = (String)result.get("msg") == null ? (String)result.get("msg") : (String)result.get("errormsg");
//        orderApi.setMoreInfo(errorVOMsg);
//        logger.info("拼团活动[pCode:"+form.getPgTranInfoVo().getpCode()+"],"+errorVOMsg);
//        }
        return orderApi;
    }

}
