package com.xquark.restapi.im;

import java.util.List;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel
public class ImMsgDataVO {

  private String fromId;    // userId who send msg to u

  @ApiModelProperty("the msg sender name")
  private String name;

  @ApiModelProperty("the msg sender nickname")
  private String nickName;

  @ApiModelProperty("user avatar")
  private String avatar;

  @ApiModelProperty("msg content data list")
  private List<ImMsgData> msgList;  // msgData

  public List<ImMsgData> getMsgList() {
    return msgList;
  }

  public void setMsgList(List<ImMsgData> msgList) {
    this.msgList = msgList;
  }

  public String getFromId() {
    return fromId;
  }

  public void setFromId(String fromId) {
    this.fromId = fromId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

}
