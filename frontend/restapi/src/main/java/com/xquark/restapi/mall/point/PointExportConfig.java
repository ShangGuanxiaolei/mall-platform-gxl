package com.xquark.restapi.mall.point;

import static com.xquark.utils.resultcode.ColumnMapping.STRING_TO_DECIMAL;
import static com.xquark.utils.resultcode.ColumnMapping.STRING_TO_INTEGER;
import static com.xquark.utils.resultcode.ColumnMapping.STRING_TO_STRING;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.type.PlatformType;
import com.xquark.service.excel.item.CommissionWithdrawItem;
import com.xquark.utils.excel.SheetHeader;
import com.xquark.utils.excel.mapping.*;
import com.xquark.utils.resultcode.ColumnMapping;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午3:48
 */
public class PointExportConfig {

  static final String ZH_KEY = "ZH";

  static final String NON_ZH_KEY = "NON_ZH";

  /**
   * 积分导出mapping
   */
  @SuppressWarnings("unchecked")
  static final Map<String, ColumnMapping> POINT_COLUMN_MAPPING
      = ImmutableMap.of(
      "customerNumber", new ColumnMapping<>("customerNumber", STRING_TO_STRING),
      "commission", new ColumnMapping("commission", STRING_TO_DECIMAL),
      "comm_source", new ColumnMapping("commSource", STRING_TO_INTEGER),
      "point", new ColumnMapping("point", STRING_TO_DECIMAL),
      "point_source", new ColumnMapping("pointSource", STRING_TO_INTEGER)
  );

  /**
   * 提现导出mapping
   */
  @SuppressWarnings("unchecked")
  static final Map<String, ColumnMapping> WITHDRAW_COLUMN_MAPPING
      = ImmutableMap.of(
      "证件号码", new ColumnMapping("tinCode", STRING_TO_STRING),
      "备注", new ColumnMapping("remark", STRING_TO_STRING),
      "错误标识", new ColumnMapping("errorMsg", STRING_TO_STRING)
  );

  /**
   * 根据银行分组的方法 中行单独分一组, 其他银行分一组
   */
  static final Function<CommissionWithdrawItem, String> COMMISSION_BANK_GROUP_FUNC
      = new Function<CommissionWithdrawItem, String>() {
    @Override
    public String apply(CommissionWithdrawItem commissionWithdrawVO) {
      if (StringUtils.equals(commissionWithdrawVO.getBankName(), "中国银行")) {
        return ZH_KEY;
      }
      return NON_ZH_KEY;
    }
  };

  /**
   * 提现导出中行mapping
   */
  static final BaseExportMapping ZH_MAPPING = new CommissionZHExportMapping();

  /**
   * 提现导出非中行mapping
   */
  static final BaseExportMapping NON_ZH_MAPPING = new CommissionNonZHExportMapping();

  /**
   * 提现明细Mapper
   */
  static final BaseExportMapping DETAIL = new WithdrawDetailExportMapping();

  static final BaseExportMapping LABOR = new LaborExpensesExportMapping();

  /**
   * 中行头 TODO 数据静态化
   */
  static List<SheetHeader> getZhSheetHeader(PlatformType platform) {
    return ImmutableList.of(
        // 第一行
        SheetHeader.Builder
            .start(new String[]{"业务类型：", "C1-人民币/外币行内代付", "转出账号：", platform.getWithdrawAccount(),
                "币种：",
                "CNY-人民币", "业务摘要：", "工资"})
            .fontSize((short) 11)
            .fontName("Calibri")
            .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
            .build(),
//       第二行空行
        SheetHeader.Builder.start(new String[0]).build(),
        SheetHeader.Builder
            .start(ZH_MAPPING.title())
            .fontSize((short) 11)
            .fontName("Calibri")
            .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
            .build());
  }

  /**
   * 非中行头
   */
  static List<SheetHeader> getNonZhSheetHeader(PlatformType platform) {
    return ImmutableList.of(
        SheetHeader.Builder
            .start(
                new String[]{"业务类型：", "C2-人民币跨行代付(小额定期贷记)", "转出账号：", platform.getWithdrawAccount(),
                    "币种：",
                    "CNY-人民币", "业务摘要：", "工资"})
            .fontName("Calibri")
            .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
            .fontSize((short) 11).build(),
        SheetHeader.Builder.start(new String[0]).build(),
        SheetHeader.Builder
            .start(NON_ZH_MAPPING.title())
            .fontName("Calibri")
            .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
            .fontSize((short) 11)
            .build()
    );
  }

  /**
   * 提现详情头
   * @return 提现详情头
   */
  static List<SheetHeader> getDetailSheetHeader() {
    return ImmutableList.of(
            SheetHeader.Builder
                    .start(DETAIL.title())
                    .fontName("Calibri")
                    .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
                    .fontSize((short) 11)
                    .build()
    );
  }


  static List<SheetHeader> getLaborSheetHeader() {
    return ImmutableList.of(
            SheetHeader.Builder
                    .start(LABOR.title())
                    .fontName("Calibri")
                    .boldWeight(HSSFFont.BOLDWEIGHT_NORMAL)
                    .fontSize((short) 11)
                    .build()
    );
  }


}
