package com.xquark.restapi.wechat;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 17-10-26. DESC:
 */
public class MiniProgramAuthTokenForm {

  @NotBlank(message = "encryptedData不能为空")
  private String encryptedData;

  @NotBlank(message = "iv不能为空")
  private String iv;

  @NotBlank(message = "code不能为空")
  private String code;

  private String fromCpId;

  private String channel;

  private String scanCodeType; //小程序扫码跳转码类型

  private String sceneKey; //获取微信二维码短链key的值

  private String spreaderId;//赋能专区生成的推荐人唯一号，扫描商家推广码时会携带；推广人编号-1代表推广人是商家自身

  public String getSceneKey() {
    return sceneKey;
  }

  public void setSceneKey(String sceneKey) {
    this.sceneKey = sceneKey;
  }

  public String getScanCodeType() {
    return scanCodeType;
  }

  public void setScanCodeType(String scanCodeType) {
    this.scanCodeType = scanCodeType;
  }

  public String getEncryptedData() {
    return encryptedData;
  }

  public String getIv() {
    return iv;
  }

  public String getCode() {
    return code;
  }

  public void setEncryptedData(String encryptedData) {
    this.encryptedData = encryptedData;
  }

  public void setIv(String iv) {
    this.iv = iv;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getFromCpId() {
    return fromCpId;
  }

  public void setFromCpId(String fromCpId) {
    this.fromCpId = fromCpId;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getSpreaderId() {
    return spreaderId;
  }

  public void setSpreaderId(String spreaderId) {
    this.spreaderId = spreaderId;
  }

  @Override
  public String toString() {
    return "MiniProgramAuthTokenForm{" +
            "encryptedData='" + encryptedData + '\'' +
            ", iv='" + iv + '\'' +
            ", code='" + code + '\'' +
            ", fromCpId='" + fromCpId + '\'' +
            ", channel='" + channel + '\'' +
            ", scanCodeType='" + scanCodeType + '\'' +
            ", sceneKey='" + sceneKey + '\'' +
            ", spreaderId='" + spreaderId + '\'' +
            '}';
  }
}
