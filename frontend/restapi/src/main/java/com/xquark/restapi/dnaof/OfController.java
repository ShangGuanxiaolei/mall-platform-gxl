package com.xquark.restapi.dnaof;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.vo.Bind;
import com.xquark.dal.vo.OfReport;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.dnaOf.OfService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.utils.EncryptUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/2/15
 * Time: 13:48
 * Description:欧飞接口调用
 */

@Controller
@RequestMapping("/dna")
public class OfController {
    private static final Log log = LogFactory.getLog(OfController.class);

    @Autowired
    private OfService ofService;

    @Autowired
    private ProductMapper productMapper;

    @RequestMapping(value = "/bind",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //样本绑定接口
    public ResponseObject<Object> bind(@Param("bind") Bind bind, @Param("cpid")Integer cpid) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        //CPID加密给欧飞接口
        try {
            EncryptUtil encryptUtil = new EncryptUtil();
            String ofcpid = encryptUtil.encrypt(cpid.toString());
            map.put("cpid",ofcpid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("token", token);
        map.put("name", bind.getName());
        map.put("sex", bind.getSex());
        map.put("birth", bind.getBirth());
        map.put("phone", bind.getPhone());
        map.put("birthAddress", bind.getBirthAddress());
        map.put("sampleCode", bind.getSampleCode());
        String s = HttpRequest.doPost(OFConfig.BIND_URL,map);
        JSONObject jsonObject = JSON.parseObject(s);
        String success = jsonObject.getString("success");
        String message = jsonObject.getString("message");
        System.out.println(message);

        if(message.equals("绑定成功")){
            OfReport ofReport = new OfReport();
            ofReport.setCpid(cpid);
            ofReport.setSampleCode(bind.getSampleCode());
            ofReport.setName(bind.getName());
            ofReport.setSex(bind.getSex());
            ofReport.setBirth(bind.getBirth());
            ofReport.setAddress(bind.getAddress());
            ofReport.setPhone(bind.getPhone());
            ofReport.setBirthAddress(bind.getBirthAddress());
            ofReport.setCreatedAt(new Date());
            Integer integer = ofService.insertOfReport(ofReport);
        }

        log.info("欧飞绑定链接:"+OFConfig.BIND_URL+"欧飞用户绑定信息:"+s);
        return new ResponseObject<>(message);
    }



    @RequestMapping(value = "/getReportList",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //获取报告列表接口
    public ResponseObject<Object> getReportList(@Param("cpid")Integer cpid) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        try {
            EncryptUtil encryptUtil = new EncryptUtil();
            String ofcpid = encryptUtil.encrypt(cpid.toString());
            map.put("cpid",ofcpid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.put("token", token);
        String s = HttpRequest.doPost(OFConfig.REPORTLIST_URL, map);
        System.out.println(s);
        JSONObject jsonObject = JSON.parseObject(s);
//        String data = jsonObject.getString("data");

        return new ResponseObject<>(jsonObject);
    }



    @RequestMapping(value = "/getSample", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //获取样本信息
    public ResponseObject<Object> getSample(@Param("sampleCode") String sampleCode,@Param("cpid") String cpid) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("sampleCode", sampleCode);
        try {
            EncryptUtil encryptUtil = new EncryptUtil();
            String ofcpid = encryptUtil.encrypt(cpid.toString());
            map.put("cpid",ofcpid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String s = HttpRequest.doPost(OFConfig.SAMPLE_URL, map);
        System.out.println(s);
        JSONObject jsonObject = JSON.parseObject(s);
        String data = jsonObject.getString("data");
        System.out.println(data);
        JSONObject jo = JSON.parseObject(data);
        String address = jo.getString("address");
        System.out.println(address);
        return new ResponseObject<>(jsonObject);
    }



    @RequestMapping(value = "/getPreviewReport", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //获取预览报告链接
    public ResponseObject<Object> getPreviewReport(@Param("phone") String phone,@Param("sampleCode") String sampleCode) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("phone", phone);
        map.put("token", token);
        map.put("sampleCode",sampleCode );
        String s = HttpRequest.doPost(OFConfig.LJ_URL, map);
        JSONObject jsonObject = JSON.parseObject(s);
        String data = jsonObject.getString("data");
        JSONObject jo = JSON.parseObject(data);
        String reportUrl = jo.getString("reportUrl");
        StringBuffer sb = new StringBuffer(reportUrl);
        StringBuffer Url = sb.append("&customerCode="+OFConfig.CODE);
        return new ResponseObject<>(Url);
    }



    @RequestMapping(value = "/question/save", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //填写调查问卷地址
    public ResponseObject<Object> questionSave(@Param("question") HandesonQuestion question) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("token",token);
        map.put("data",JSON.toJSONString(question));
        String s = HttpRequest.doPost(OFConfig.WJ_URL, map);
        JSONObject jsonObject = JSON.parseObject(s);
        return new ResponseObject<>(jsonObject );
    }



    @RequestMapping(value = "/getQuestion", produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    //查询调查问卷
    public ResponseObject<Object> getQuestion(@Param("sampleCode") String sampleCode) {
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("sampleCode", sampleCode);
        String s1 = HttpRequest.doPost(OFConfig.CXWJ_URL, map);
        return new ResponseObject<>(s1);
    }


    //获取欧飞问卷调查题目及选项
    @RequestMapping(value = "/query/questionnaire",produces = "application/json; charset=utf-8")
    @ResponseBody
    public ResponseObject<List<Map<String,String>>> queryQuestionnaire(){
        List<Map<String, String>> maps = ofService.queryQuestionnaire();
        return new ResponseObject<>(maps);
    }
    //欧飞校验样本绑定号接口
    @RequestMapping(value = "/checkSample")
    @ResponseBody
    public ResponseObject<Object> checkSample(@Param("sampleCode") String sampleCode){
        String token = getToken();
        HashMap<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("sampleCode", sampleCode);
        String s = HttpRequest.doPost(OFConfig.JYYB_URL, map);
        JSONObject jsonObject = JSON.parseObject(s);
        return new ResponseObject<>(jsonObject);

    }

    //校验当前用户的受检人是否绑定过
    @RequestMapping(value = "/check/subjectPeople", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Map<String, Object>> checkSubjectPeopleByName(@RequestBody Map map) {
        Integer cpid = (Integer) map.get("cpid");
        String name = (String) map.get("name");
        String sampleCode = (String) map.get("sampleCode");
        Map<String, Object> resultMap = new HashMap<>();
        if (cpid != null && name != null && sampleCode != null && !name.equals("") && !sampleCode.equals("")) {
            boolean flag = false;
            Boolean isSampleCode = ofService.checkSampleCode(sampleCode); //校验样本码
            Integer count = ofService.checkSubjectPeopleByName(cpid, name); //校验受检人是否有绑定
            if (count >= 1) {
                flag = true;
            }
            OfReport bindedOfrepor = ofService.getBindedOfreportByName(cpid, name);
            resultMap.put("isSampleCode", isSampleCode);
            resultMap.put("isBinded", flag);
            resultMap.put("subjectPeople", bindedOfrepor);
        } else {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "cpid 或 name 或 sampleCode 不可为空");
        }
        return new ResponseObject<>(resultMap);
    }

    // 首页app获取基因产品code
    @RequestMapping(value = "/getDNAProductId", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Map<String, Object>> getDNAProductId() {
        Map<String, Object> resultMap = new HashMap<>();
        List<String> dnaProductCode = productMapper.getDNAProductCode();
        if (null != dnaProductCode && dnaProductCode.size() > 0) {
            resultMap.put("productCode", dnaProductCode.get(0));
            return new ResponseObject<>(resultMap);
        }
        return new ResponseObject<>(resultMap);
    }




    //获取新token方法
    public  String getToken(){
        HashMap<String, String> map1 = new HashMap<>();
        map1.put("code", OFConfig.CODE);
        String s = HttpRequest.doPost(OFConfig.TOKEN_URL, map1);
        JSONObject jsonObject = JSON.parseObject(s);
        String token = (String)jsonObject.get("token");
        return token;
    }
}