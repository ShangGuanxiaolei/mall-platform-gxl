package com.xquark.restapi.dnaof;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/2/15
 * Time: 14:08
 * Description:
 */
public class OFConfig {
    //汉德森欧飞code
    public static final String CODE = "d0e883c2-5025-4bb8-b9dd-bb521481ab69";



    /*uat*/
//    //获取token地址
//    public static final String TOKEN_URL = "http://132.232.149.20:8083/security/getToken";
//
//    //样本绑定地址
//    public static final String BIND_URL = "http://132.232.149.20:8083/sample/bind";
//
//    //获取报告列表地址
//    public static final String REPORTLIST_URL = "http://132.232.149.20:8083/report/getReportList";
//
//    //获取样本信息地址
//    public static final String SAMPLE_URL = "http://132.232.149.20:8083/report/getSample";
//
//    //获得预览报告链接地址
//    public static final String LJ_URL = "http://132.232.149.20:8083/report/previewReport";
//
//    //填写调查问卷地址
//    public static final String WJ_URL = "http://132.232.149.20:8083/question/save";
//
//    //查询问卷调查地址
//    public static final String CXWJ_URL = "http://132.232.149.20:8083/question/getQuestion";
//
//    //绑定前校验样本编号
//    public static final String JYYB_URL = "http://132.232.149.20:8083/report/checkSample";
    /*uat end*/

       /*prod*/
    //获取token地址
    public static final String TOKEN_URL = "http://api.omicsfit.com/security/getToken";

    //样本绑定地址
    public static final String BIND_URL = "http://api.omicsfit.com/sample/bind";

    //获取报告列表地址
    public static final String REPORTLIST_URL = "http://api.omicsfit.com/report/getReportList";

    //获取样本信息地址
    public static final String SAMPLE_URL = "http://api.omicsfit.com/report/getSample";

    //获得预览报告链接地址
    public static final String LJ_URL = "http://api.omicsfit.com/report/previewReport";

    //填写调查问卷地址
    public static final String WJ_URL = "http://api.omicsfit.com/question/save";

    //查询问卷调查地址
    public static final String CXWJ_URL = "http://api.omicsfit.com/question/getQuestion";

    //绑定前校验样本编号
    public static final String JYYB_URL = "http://api.omicsfit.com/report/checkSample";
    /*prod end*/
}