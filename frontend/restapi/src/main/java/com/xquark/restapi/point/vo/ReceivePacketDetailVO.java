package com.xquark.restapi.point.vo;

import com.google.common.collect.Lists;
import com.xquark.dal.model.UserDTO;
import com.xquark.dal.model.UserPointDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class ReceivePacketDetailVO {
    private static final String BULLING_TITLE = "领取详情";
    private static final String UN_BULLING_TITLE = "红包来源";
    private List<UserPointVO> userPointVOS;
    private UserDTO belongUser;
    /**
     * 入账
     */
    private Boolean increment;
    private String title;
    private Integer totalAmount;
    private Integer remainingPoint;


    public ReceivePacketDetailVO() {
    }


    public static ReceivePacketDetailVO valueOf(UserDTO userDTO,boolean billingType, List<UserPointDTO> userPointDTOS, BigDecimal totalAmount) {
        int remainingPoint = totalAmount.intValue();
        List<UserPointVO> userPointVOS = Lists.newArrayList();
        for (UserPointDTO userPointDTO : userPointDTOS) {
            remainingPoint = remainingPoint - userPointDTO.getAmount();
            userPointVOS.add(UserPointVO.convert(userPointDTO));
        }
        final ReceivePacketDetailVO receivePacketDetailVO = new ReceivePacketDetailVO();
        receivePacketDetailVO.setRemainingPoint(billingType?remainingPoint:null);
        receivePacketDetailVO.setUserPointVOS(userPointVOS);
        receivePacketDetailVO.setTotalAmount(totalAmount.intValue());
        receivePacketDetailVO.setTitle(billingType? BULLING_TITLE : UN_BULLING_TITLE);
        receivePacketDetailVO.setIncrement(!billingType);
        receivePacketDetailVO.setBelongUser(userDTO);
        return receivePacketDetailVO;
    }


    public UserDTO getBelongUser() {
        return belongUser;
    }

    public void setBelongUser(UserDTO belongUser) {
        this.belongUser = belongUser;
    }

    public Boolean getIncrement() {
        return increment;
    }

    public void setIncrement(Boolean increment) {
        this.increment = increment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<UserPointVO> getUserPointVOS() {
        return userPointVOS;
    }

    public void setUserPointVOS(List<UserPointVO> userPointVOS) {
        this.userPointVOS = userPointVOS;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getRemainingPoint() {
        return remainingPoint;
    }

    public void setRemainingPoint(Integer remainingPoint) {
        this.remainingPoint = remainingPoint;
    }

    public static String getBullingTitle() {
        return BULLING_TITLE;
    }

    public static String getUnBullingTitle() {
        return UN_BULLING_TITLE;
    }



    @Override
    public String toString() {
        return "ReceivePacketDetailVO{" +
                "userPointVOS=" + userPointVOS +
                ", totalAmount=" + totalAmount +
                ", remainingPoint=" + remainingPoint +
                '}';
    }
}
