package com.xquark.restapi.product;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.url.UrlHelper;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.status.FilterSpec;
import com.xquark.dal.status.ProductReviewStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.*;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.*;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.brand.BrandService;
import com.xquark.service.cache.annotation.DoGuavaCache;
import com.xquark.service.cache.constant.CacheKeyConstant;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.diamond.PromotionDiamondService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.grandSale.GrandSaleStadiumService;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.pintuan.PromotionPgPriceService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.privilege.PrivilegeProductService;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductTopService;
import com.xquark.service.product.SFProductService;
import com.xquark.service.product.VipProductService;
import com.xquark.service.product.dto.PushProductMsg;
import com.xquark.service.product.dto.PushShipmentNum;
import com.xquark.service.product.dto.PushSplitOrder;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductSearchVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.product.vo.SfStockVO;
import com.xquark.service.productSales.ProductSalesService;
import com.xquark.service.productSales.vo.CheckSaleCustomer;
import com.xquark.service.promotion.*;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.sf.SFMessageService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.sku.SkuAttributeService;
import com.xquark.service.supplier.SupplierService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.UserService;
import com.xquark.utils.HtmlUtils;
import com.xquark.utils.ImgUtils;
import com.xquark.utils.ResourceResolver;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.function.Predicate;

@Controller
@Api(value = "product", description = "商品管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController extends FragmentAndDescController {

    @Autowired
    private UserService userService;
    @Autowired
    private UrlHelper urlHelper;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProdSyncMapper prodSyncMapper;
    @Autowired
    private SkuMappingMapper skuMappingMapper;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductImageMapper productImageMapper;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivityProductMapper activityProductMapper;
    @Autowired
    private FragmentService fragmentService;
    @Autowired
    private ProductFragmentService productFragmentService;
    @Autowired
    private FragmentImageService fragmentImageService;
    @Autowired
    private PromotionGrouponService promotionGrouponService;
    @Autowired
    private PromotionService promotionService;
    @Autowired
    private ShopTreeService shopTreeService;
    @Autowired
    private PromotionConfigService promotionConfigService;

    @Autowired
    private PromotionDiamondService promotionDiamondService;

    @Autowired
    private ProductDistributorMapper productDistributorMapper;
    @Autowired
    private TinyUrlService tinyUrlService;

    @Autowired
    private ProductTopService productTopService;

    @Autowired
    private SkuAttributeService skuAttributeService;

    @Autowired
    private PrivilegeProductService privilegeProductService;

    @Autowired
    private PromotionCouponService couponService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SFProductService sfProductService;

    @Autowired
    private FlashSalePromotionProductService flashSaleService;

    @Autowired
    private ProductCombineService productCombineService;

    @Autowired
    private VipProductService vipProductService;

    @Value("${site.web.host.name}")
    private String siteHost;

    private BrandService brandService;

    private SupplierService supplierService;

    @Autowired
    private PromotionSkusService promotionSkusService;

    @Autowired
    private PromotionSkuGiftService promotionSkuGiftService;

    @Autowired
    private FirstOrderService firstOrderService;

    @Autowired
    private ProductSalesService productSalesService;

    @Autowired
    private PromotionPgPriceService promotionPgPriceService;

    @Autowired
    public void setSupplierService(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @Autowired
    public void setBrandService(BrandService brandService) {
        this.brandService = brandService;
    }

    @Autowired
    private GrandSaleStadiumService grandSaleStadiumService;

    @Autowired
    private FreshManService freshManService;

    @Autowired
    private SFMessageService sfMessageService;

    @Value("${xiangqu.cart.host.url}")
    String xiangquCartHost;

    @Value("${shop.product.limit}")
    private String productLimit;

    @Value("${product.img.limit}")
    private String imgLimit;

    /**
     * 查看某个具体的商品<br>
     */
    @ResponseBody
    @RequestMapping("/product/{id}")
    public ResponseObject<ProductVOEx> view(
            @PathVariable String id,
            HttpServletRequest req,
            @RequestParam(value = "promotionProductId", required = false) String promotionProductId,
            @RequestParam(value = "promotionFrom", required = false) PromotionType promotionType,
            @RequestParam(value = "pCode", required = false) String pCode) {
        IUser iUser = getCurrentIUser();
        UserInfoVO userInfoVO = userService.getUserInfo(iUser.getId());
        User user = userInfoVO.getUser();

        ProductVO product = productService.load(id);
        if (product == null) {
            RequestContext requestContext = new RequestContext(req);
            throw new BizException(
                    GlobalErrorCode.NOT_FOUND, requestContext.getMessage("product.not.found"));
        }

        // 校验该商品是否是基因商品
        List<Long> ofProducts = brandService.loadOFBrandProduct();
        if (CollectionUtils.isNotEmpty(ofProducts)) {
            long productId = IdTypeHandler.decode(id);
            if (ofProducts.contains(productId)) {
                product.setDNAProduct(true);
            }
        }

        //test
        convertUrl(product); // 生成商品的图片URL
        // 约定获取商品的图文信息的时候，从productService中获取，实现：先获取富文本信息，再获取片段信息
        setFragmentAndDescInfo(product);

        // 获取商品的多sku规格属性
        List<SkuAttributeVO> skuAttributeVOs = skuAttributeService.findAttributesByProductId(id);
        product.setSkuAttributes(skuAttributeVOs);

        List<BasePromotionCouponVO> availableCoupons =
                couponService.listBasePromotionCouponByProduct(product.getId());
        product.setCouponList(availableCoupons);

        // 计算积分兑换比例
        // 只在前端调用时计算
        //    IUser user = getCurrentIUser();
        Long cpId = userService.selectCpIdByUserId(user.getId());

        ProductVOEx result =
                new ProductVOEx(
                        product,
                        urlHelper.genProductUrl(product.getId() + "?upline=" + cpId),
                        product.getImg(),
                        null);

        // 只有前端用户才使用活动价覆盖
        final PromotionProductVO promotionProduct = isFrontUser()
                .filter(Predicate.isEqual(true))
                .flatMap($ -> SpringPromotionProductLoader
                        .load(promotionType, pCode, id))
                .orElse(null);

        BigDecimal minPrice = result.getSkus()
                .stream()
                .map(Sku::getPrice)
                .min(BigDecimal::compareTo)
                .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INVALID_ARGUMENT, "商品信息不正确"));
        if (promotionProduct != null) {
            result.setCanUsePromotion(promotionProduct);
            result.setCanUsePromotionType(promotionProduct.getPromotionType());
            Promotion promotion = promotionProduct.getPromotion();
            if (promotion != null) {
                final PromotionType type = promotion.getType();
                result.setIsShowTime(promotion.getIsShowTime());
                // 更新展示价格为活动价
                result.modifySkus(s -> {
                    BigDecimal price;
                    PromotionPgPrice pgPrice = promotionPgPriceService
                            .loadByPromotionIdAndSkuId(promotion.getId(), s, promotion.getType());
                    if (pgPrice != null) {
                        price = pgPrice.getPromotionPrice();
                        BeanUtils.copyProperties(pgPrice, s);
                        s.setDeductionDPoint(pgPrice.getPoint());
                        s.setPoint(pgPrice.getReduction());
                    } else {
                        price = java.util.Optional.ofNullable(promotion.getPrice())
                                .orElse(promotion.getRealDiscount()
                                        .multiply(result.getPrice())
                                        .divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN));
                        // 特殊处理预购逻辑, 后台配置的规格不全则把库存置为0，表现为前端无法购买
                        if (PromotionType.RESERVE == type) {
                            s.setAmount(0);
                        }
                    }
                    s.setPrice(price);
                });
            } else {
                result.setIsShowTime(true);
            }
        }

        // 用第一个sku的数据覆盖商品的规格数据
        Sku minPriceSku = result.getSkus().stream()
                .min(Comparator.comparing(Sku::getPrice))
                .orElse(null);
        if (minPriceSku != null) {

            //判断是否为新人专区商品
            long productId = IdTypeHandler.decode(product.getId());
            FreshmanProduct freshmanProduct = firstOrderService.freshmanProduct(productId);
            if (freshmanProduct != null) {

                result.setFreshmanProduct(true);
                //新人多规格取划线价
                List<Sku> skus = this.freshManService.selectFreshManSku(product.getId());
                if (skus != null || !skus.isEmpty()) {
                    result.setOriginalPrice(skus.get(0).getOriginalPrice());
                    result.setPrice(skus.get(0).getPrice());
                    minPriceSku.setDeductionDPoint(skus.get(0).getDeductionDPoint());
                    minPriceSku.setNetWorth(skus.get(0).getNetWorth());
                    minPriceSku.setPoint(skus.get(0).getPoint());
                    minPriceSku.setPrice(skus.get(0).getPrice());
                    minPriceSku.setAmount(skus.get(0).getAmount());
                    minPriceSku.setServerAmt(skus.get(0).getServerAmt());
                    minPriceSku.setPromoAmt(skus.get(0).getPromoAmt());
                }
            }

            result.setDeductionDPoint(minPriceSku.getDeductionDPoint());
            result.setNetWorth(minPriceSku.getNetWorth());
            result.setPoint(minPriceSku.getPoint());
            result.setPrice(minPrice);
            result.setAmount(minPriceSku.getAmount());
            result.setServerAmt(minPriceSku.getServerAmt());
            result.setPromoAmt(minPriceSku.getPromoAmt());

            // 优先调整会员价等
            RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
            String json = redisUtils.get("login_return_" + cpId);
            if (json != null) {

                JSONObject jsonObject = JSONObject.parseObject(json);
                Boolean isProxy = jsonObject.getBoolean("isProxy");
                Boolean isMember = jsonObject.getBoolean("isMember");

                BigDecimal conversionPrice = minPriceSku.getConversionPrice();
                BigDecimal reduction = minPriceSku.getReduction();
                BigDecimal price = minPriceSku.getPrice();
                BigDecimal serverAmt = minPriceSku.getServerAmt();

                BigDecimal subtractValue = reduction.add(serverAmt);
                BigDecimal proxyPrice = price.subtract(subtractValue).setScale(2,
                        RoundingMode.HALF_EVEN);
                result.setProxyPrice(proxyPrice);

                result.setMemberPrice(price.subtract(reduction).setScale(2,
                        RoundingMode.HALF_EVEN));

                BigDecimal changePrice;
                if (isProxy) {
                    changePrice = conversionPrice.subtract(subtractValue).setScale(2,
                            RoundingMode.HALF_EVEN);

                    result.setSavedPrice(subtractValue);
                    result.setEarnedPrice(subtractValue);
                } else if (isMember) {
                    changePrice = conversionPrice.subtract(reduction).setScale(2,
                            RoundingMode.HALF_EVEN);

                    result.setSavedPrice(reduction);
                    result.setEarnedPrice(reduction);
                } else {
                    changePrice = conversionPrice;

                    result.setSavedPrice(reduction);
                    result.setEarnedPrice(reduction);
                }
                result.setChangePrice(changePrice);
            }

            //判断此商品是否为分会场商品
            String type = minPriceSku.getPromotionType();
            if (type != null) {
                StadiumType stadiumType = null;
                try {
                    stadiumType = StadiumType.valueOf(type);
                } catch (Exception e) {
                    log.error("分会场类型转换失败,type:{}", type, e);
                }
                StadiumType[] stadiumTypes = StadiumType.values();
                boolean contains = ArrayUtils.contains(stadiumTypes, stadiumType);

                GrandSaleStadium grandSaleStadium = grandSaleStadiumService.selectOneByType(type);
                if (grandSaleStadium == null) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场商品信息不存在");
                }
                result.setEffectFrom(grandSaleStadium.getBeginTime());
                result.setEffectTo(grandSaleStadium.getEndTime());
                result.setIsStadiumProduct(contains);
//                if(contains==true){
//                    result.setCanUsePromotionType(PromotionType.BRANCH);
//                }

            }
        }

        //---------------------计算商品的多sku排序逻辑
        if (result.getSkus().size() > 1) {
            result.setSkus(sortSkuList(result));
        }
        // 放入评价统计信息
        Map<String, Long> commentTotalMap = commentService.commentTotalMap(product.getId());
        result.setCommentTotalMap(commentTotalMap);

        //如果传入pCode证明是活动商品
        if (null != pCode && !"".equals(pCode)) {
            PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);
            //是大会活动商品
            //if(baseInfo.getpType().contains("meeting")){
            if (null != baseInfo && baseInfo.getpType().contains("meeting")) {
                //杭州1013大会售卖活动，详情页增加活动信息， by Chenpeng
                this.convertPromotionInfo(id, result, pCode);
            }

            //拼团增加已售罄功能有pCode by Yarm
            if (null != baseInfo && baseInfo.getpType().contains("piece")) {
                this.groupConvertPromotionInfo(id, result, pCode);
            }

        } else {

            boolean isCommonProduct = true; // 标记是否是普通商品
            // 如果没有传入pCode，则根据传入的productid去判断有没有参加活动
            List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectByProductId(id);
            if (CollectionUtils.isNotEmpty(promotionBaseInfos)
                    && promotionBaseInfos.get(0).getpType().contains("meeting")) {
                // 杭州1013大会售卖活动，详情页增加活动信息， by Chenpeng
                this.convertPromotionInfo(id, result, promotionBaseInfos.get(0).getpCode());
                isCommonProduct = false;
            }

            // 拼团增加已售罄功能没有pCode by Yarm
            if (CollectionUtils.isNotEmpty(promotionBaseInfos)
                    && promotionBaseInfos.get(0).getpType().contains("piece")) {
                this.groupConvertPromotionInfo(id, result, promotionBaseInfos.get(0).getpCode());
                isCommonProduct = false;
            }

            // 普通商品已售罄校验逻辑 by Yarm
            if (isCommonProduct) {
                this.commonProductConvertPromotionInfo(id, result);
            }
        }

        //折扣促销活动商品 特殊处理
        List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectSalesPromotionByProductId(id);
        if (CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)) {
//          result.setCustomerType(promotion4ProductSalesVOS.get(0).getCustomerType());
            this.convertProductSalesPromtion(result, promotion4ProductSalesVOS.get(0), iUser, minPriceSku);
        }

        // 验证是否为白人，首次下单，再确定是否弹出新人专区 by tanggb
        Boolean firstOrder = firstOrderService.firstOrder(user.getCpId());
        result.setFirstOrder(firstOrder);

        return new ResponseObject<>(result);
    }

    //折扣促销 特殊处理
    private void convertProductSalesPromtion(ProductVOEx result, Promotion4ProductSalesVO productSalesVO, IUser user, Sku minisku) {
        User userInfo = (User) user;
        result.setCanUsePromotionType(PromotionType.PRODUCT_SALES);
        result.setCarriageFree(productSalesVO.getCarriageFree());
        if (productSalesVO.getTimeShow()) {
            Long cutDown = productSalesVO.getEffectTo().getTime() - new Date().getTime();
            result.setSaleCutDown(cutDown);
            result.setEffectFrom(productSalesVO.getEffectFrom());
            result.setEffectTo(productSalesVO.getEffectTo());

        }
        if (null != minisku) {
            result.setOriginalPrice(minisku.getOriginalPrice());
        }
        CheckSaleCustomer checkCustomerType = productSalesService.checkCustomerType(productSalesVO.getCustomerType(), userInfo);
        result.setCanBuySaleProduct(checkCustomerType.canBuyCustomer);
        result.setCanBuySaleProductToast(checkCustomerType.getToast());
        List<Sku> skus = result.getSkus();
        for (Sku sku : skus) {
            PromotionSkuVo salesSkus = promotionSkusService.getProductSalesSkus(sku.getSkuCode(), productSalesVO.getpCode());
            if (null != salesSkus && null != salesSkus.getBuyLimit()) {
                sku.setProductSalesLimit(salesSkus.getBuyLimit());
            }
        }
    }

    //普通商品数据转化 by Yarm
    private void commonProductConvertPromotionInfo(String id, ProductVOEx result) {
        boolean isSoldOut = this.productService.checkCommonProductSoldOut(id);
        result.setIsSoldOut(isSoldOut);
    }

    //拼团数据转化 by Yarm
    private void groupConvertPromotionInfo(String productId, ProductVOEx result, String pCode) {
        boolean isSoldOut = this.productService.checkGroupSoldOut(productId, pCode);
        result.setIsSoldOut(isSoldOut);
    }

    @ResponseBody
    @RequestMapping("/product/promotion/{id}")
    @ApiOperation(value = "查看某个具体的活动商品", notes = "查看某个具体的活动商品信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<ProductPromotionVOEX> viewPromotionProduct(@PathVariable String id,
                                                                     HttpServletRequest req) {
        return null;
    }

    /***
     * 空sku，用于占位保证商品详情返回的skulist是填满的
     */
    private final static Sku EMPTY_SKU = new Sku();

    static {
        EMPTY_SKU.setAmount(-1001);
        EMPTY_SKU.setId("mhaa");
        EMPTY_SKU.setPrice(BigDecimal.ZERO);
        EMPTY_SKU.setNetWorth(BigDecimal.ZERO);
        EMPTY_SKU.setPoint(BigDecimal.ZERO);
        EMPTY_SKU.setServerAmt(BigDecimal.ZERO);
        EMPTY_SKU.setDeductionDPoint(BigDecimal.ZERO);
    }

    /***
     * sku数组排序
     * @param product
     * @return skuList
     */
    private List<Sku> sortSkuList(ProductVOEx product) {
        Map<String, Integer> attributeItemIndexMap = new HashMap<>(); //存储属性值在当前规格下面的索引
        Map<Integer, Integer> attributeLayerMap = new HashMap<>();//存储每一个规格层次的属性值的总数
        int totalSkuCount = 1;

        List<SkuAttributeVO> skuAttributes = product.getSkuAttributes();
        int itemIndex = 0;
        int layer = 0;
        for (SkuAttributeVO skuAttribute : skuAttributes) {
            List<SkuAttributeItem> items = skuAttribute.getItems();
            if (CollectionUtils.isNotEmpty(items)) {
                ++layer;
                attributeLayerMap.put(layer - 1, items.size());
                for (SkuAttributeItem item : items) {
                    attributeItemIndexMap.put(item.getName(), itemIndex);
                    ++itemIndex;
                }
                itemIndex = 0;
            }
        }
        //计算总sku的数量
        Collection<Integer> itemCount = attributeLayerMap.values();
        for (Integer count : itemCount) {
            totalSkuCount *= count;
        }
        //构建一个对应数量的sku的数组
        List<Sku> skuList = new ArrayList<>(totalSkuCount);
        for (int i = 0; i < totalSkuCount; i++) {
            skuList.add(i, EMPTY_SKU);
        }

        //将所有的sku按顺序存储到skuList中
        List<Sku> rawSkuList = product.getSkus();
        for (Sku sku : rawSkuList) {
            String specs = sku.getSpec();
            if (StringUtils.isNotBlank(specs) && specs.split(",").length == layer) {
                //计算每个sku在返回数组中的index
                int index = 0;
                String[] items = specs.split(",");
                for (int i = 0; i < layer; i++) {
                    String item = items[i];
                    int itemOffset = attributeItemIndexMap.get(item);
                    int temp = 1;
                    for (int j = i + 1; j < layer; j++) {
                        Integer count = attributeLayerMap.get(j);
                        temp *= count;
                    }
                    index += itemOffset * temp;
                }
                skuList.set(index, sku);
            }
        }
        return skuList;
    }

    @ResponseBody
    @RequestMapping("/product/view")
    @ApiIgnore
    public ResponseObject<ProductVOEx> view2(String id, HttpServletRequest req) {
        return this.view(id, req, "", null, "");
    }

    /**
     * 删除某个具体的删除
     */
    @ResponseBody
    @RequestMapping("/product/delete/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> delete(@PathVariable String id) {
        int result = productService.delete(id);
        return new ResponseObject<>(result > 0);
    }

    /**
     * 商品下架
     */
    @ResponseBody
    @RequestMapping("/product/instock/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> instock(@PathVariable String id) {
        int result = 0;
        try {
            result = productService.instock(id);
            /**
             * 商品下架后，设置分销池商品下架
             */
            List<ActivityProduct> aplist = activityProductMapper.selectByproductId(id);
            if (null != aplist && aplist.size() > 0) {
                for (ActivityProduct activityProduct : aplist) {
                    // 设置分销池商品下架
                    if (!activityProduct.getArchive()) {
                        activityProduct.setArchive(true);
                        activityProductMapper.update(activityProduct);
                    }
                }
            }
        } catch (Exception e) {
        }
        return new ResponseObject<>(result > 0);
    }

    /**
     * 商品上架
     */
    @ResponseBody
    @RequestMapping("/product/onsale/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> onsale(@PathVariable String id) {
        int result = productService.onsale(id);
        return new ResponseObject<>(result > 0);
    }

    /**
     * 批量删除商品
     */
    @ResponseBody
    @RequestMapping("/product/batchDelete/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> batchDelete(@PathVariable String id) {
        String[] ids = id.split(",");
        productService.batchDelete(ids);
        return new ResponseObject<>(true);
    }

    /**
     * 大b商品小b上架
     */
    @ResponseBody
    @RequestMapping("/product/activity/onsale/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> activityProductOnsale(@PathVariable String id) {
        User user = getCurrentUser();
        int result = 0;
        String shopId = user.getShopId();
        Product product = productService.load(id);
        ProductDistributor productDistributor = productDistributorMapper
                .selectByProductIdAndShopId(product.getId(), shopId);
        if (productDistributor != null) {
            result = productDistributorMapper.updateForOnsale(productDistributor.getId());
        } else {
            productDistributor = new ProductDistributor();
            productDistributor.setShopId(shopId);
            productDistributor.setProductId(product.getId());
            productDistributor.setStatus(ProductStatus.ONSALE);
            result = productDistributorMapper.insert(productDistributor);
        }
        return new ResponseObject<>(result > 0);
    }

    /**
     * 小b商品下架
     */
    @ResponseBody
    @RequestMapping("/product/activity/instock/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> activityProductInstock(@PathVariable String id) {
        User user = getCurrentUser();
        String shopId = user.getShopId();
        Product product = productService.load(id);
        ProductDistributor productDistributor = productDistributorMapper
                .selectByProductIdAndShopId(product.getId(), shopId);
        int result = 0;
        result = productDistributorMapper.updateForInstock(productDistributor.getId());
        return new ResponseObject<>(result > 0);
    }

    /**
     * 设置商品定时发布
     */
    @ResponseBody
    @RequestMapping("/product/forsale/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> forsale(@PathVariable String id, String forsaleDate) {
        Date forsaleAt = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            forsaleAt = df.parse(forsaleDate);
        } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
        }

        int result = productService.forsale(id, forsaleAt);
        return new ResponseObject<>(result > 0);
    }

    /**
     * 商品佣金设置
     */
    /*@ResponseBody
    @RequestMapping("/product/setcommission")
	public ResponseObject<Boolean> setCommission(String id, String commission) {
		return null;
	}*/

    /**
     * 商品保存，新增和修改共用一个api，接口
     */
    @ResponseBody
    @RequestMapping("/product/save")
    @ApiIgnore
    public ResponseObject<ProductVOEx> save(@Valid @ModelAttribute ProductForm form, Errors errors,
                                            HttpServletRequest req) {
        ControllerHelper.checkException(errors);
        RequestContext requestContext = new RequestContext(req);

        if (StringUtils.isBlank(userService.getCurrentUser().getShopId())) {
            throw new BizException(GlobalErrorCode.UNKNOWN,
                    requestContext.getMessage("shop.not.found"));
        }

        if (form.getSkus() == null || form.getSkus().size() == 0) {
            throw new BizException(GlobalErrorCode.UNKNOWN,
                    requestContext.getMessage("valid.sku.notBlank.message"));
        }

        if (form.getDelayed() > 0 && form.getDelayAt() == 0) {
            throw new BizException(GlobalErrorCode.UNKNOWN,
                    requestContext.getMessage("valid.isDelay.delayAt.notBlank.message"));
        }

        ProductVOEx ret = save(form, requestContext);
        // 保存或更新组合信息
        List<SlaveInfoVO> slaves = form.getSlaves();
        if (CollectionUtils.isNotEmpty(slaves) && CollectionUtils.isNotEmpty(ret.getSkus())) {
            productCombineService.saveOrUpdateCombine(ret.getSkus().get(0).getId(), ret.getId(),
                    slaves, form.getCombineValidFrom(), form.getCombineValidTo());
        }
        log.info("商品保存成功{}",form);
        return new ResponseObject<>(ret);
    }

    /**
     * 将原save方法抽取到公共方法 FIXME 改方法应该拆分到service处理
     *
     * @param form           表单对象
     * @param requestContext 请求上下文
     */
    private ProductVOEx save(ProductForm form,
                             RequestContext requestContext) {
        Product product = new Product();
        BeanUtils.copyProperties(form, product);
        product.setRecommend(form.getRecommend() == 1);

        if (form.getStatus() == ProductStatus.FORSALE) {
            try {
                product.setForsaleAt(new Date(form.getForsaleDate()));
            } catch (Exception e) {
                throw new BizException(GlobalErrorCode.UNKNOWN,
                        requestContext.getMessage("valid.product.forsale.notBlank.message"));
            }
        }
        Shop shop = shopService.load(getCurrentIUser().getShopId());
        product.setShopId(shop.getId());
        if (StringUtils.isBlank(product.getName())) {
            product.setName(StringUtils.abbreviate(product.getDescription(), 25));
        }

        product.setUserId(userService.getCurrentUser().getId());

        List<Sku> skus = initSku(form);
        // 新的sku模式中，不需要skuMapping
        List<SkuMapping> skuMapping = new ArrayList<>(); //initSkuMapping(form);
        List<ProductImage> imgs = initImage(form);

        List<Tag> tags = initTag(form);
        List<String> tagStrings = form.getTagList();
        List<Tags> tagsToSave = Transformer.fromIterable(tagStrings, new Function<String, Tags>() {
            @Override
            public Tags apply(String s) {
                return new Tags(s);
            }
        });
        product.setImg(imgs.get(0).getImg());

        if (product.getEnableDesc() == null) {
            product.setEnableDesc(false);
        }

        int rc = 0;
        if (null != form.getSkus() && form.getSkus().size() > 0) {
            String sId = form.getSupplierId();
            Supplier supplier = supplierService.getByPrimaryKey(sId);
            Optional<SkuType> type = SkuCodeResourcesType
                    .getSkuCodeResourceBySupplierCode(supplier.getCode());
            for (Sku sku : skus) {
                //sku.setSkuCode(form.getSkuCode());
                sku.setSkuCodeResources(type.orElse(SkuCodeResourcesType.getSkuCodeResourceBySupplierCode("CLIENT").orElseThrow(RuntimeException::new)).getName());
            }
        }
        //汉薇自营标签
        product.setSelfOperated(form.getSelfOperated());

        if (StringUtils.isBlank(form.getId())) {
            product.setSource(ProductSource.CLIENT.name());
            rc = productService.create(product, skus, tags, tagsToSave, imgs, skuMapping);
        } else {
            rc = productService.update(product, skus, tags, imgs, skuMapping);
        }
        //设置商品的品牌关联
        String brands;
        if (StringUtils.isNotBlank(brands = form.getBrand())) {
            List<String> brandIds = Arrays.asList(brands.split(","));
            brandService.updateProductBrand(product.getId(), brandIds);
        }

        if (rc == 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
        }

        // 设置商品类目
        // 商品分类为空也需要进入方法，因为有可能之前有分类，这次取消了分类，需要进入这个方法去删除之前设置的商品分类关系
        //if(StringUtils.isNotBlank(form.getCategory())){
        if (form.getCategory() != null) {
            categoryService.addProductCategory(product.getId(), form.getCategory());
        }

        // 更新分销商品信息
        String productCode = product.getId();
        List<Product> distProductList = productService.findProductsBySourceProductID(productCode);

        log.info("Current user id:" + userService.getCurrentUser().getId());
        int retCode = 0;
        if (null != distProductList && distProductList.size() > 0) {
            for (Product distProduct : distProductList) {
                log.info("distProduct id:" + productCode + " result:" + JSON
                        .toJSONString(distProduct));

                String backupId = distProduct.getId();
                BeanUtils.copyProperties(form, distProduct);
                distProduct.setId(backupId);

                Product refProductFullInfo = productService.load(distProduct.getId());
                distProduct.setUserId(refProductFullInfo.getUserId());
                distProduct.setShopId(refProductFullInfo.getShopId());

                distProduct.setName(product.getName());

                distProduct.setImg(imgs.get(0).getImg());

                if (distProduct.getEnableDesc() == null) {
                    distProduct.setEnableDesc(false);
                }

                // 设置商品features
                if (form.getNotSendToErp() != null) {
                    distProduct.setFeature(ProductFeaturesKeyType.NOT_SEND_TO_ERP,
                            form.getNotSendToErp());
                }

                List<Sku> distSkus = initSku(form);
                List<SkuMapping> distSkuMapping = initSkuMapping(form);
                retCode = productService
                        .updateDistributionProduct(distProduct, distSkus, tags, imgs,
                                distSkuMapping);
                if (retCode == 0) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
                }

                // 设置商品类目
                if (StringUtils.isNotBlank(form.getCategory())) {
                    categoryService.addProductCategory(distProduct.getId(), form.getCategory());
                }
            }
        }

        ProductVO vo = productService.load(product.getId());
        convertUrl(vo); // 生成商品的图片URL
        formatSynchronousVal(vo);
        return new ProductVOEx(vo, urlHelper.genProductUrl(vo.getId()), vo.getImg(), null);
    }

    /**
     * 生成商品的图片URL
     */
    private void convertUrl(ProductVO vo) {
        if (null != vo.getImgs() && vo.getImgs().size() > 0) {
            for (ProductImageVO img : vo.getImgs()) {
                img.setImgUrl(img.getImg());
            }
        }
        //TODO
        //vo.setCategory(categoryService.loadCategoryByProductId(vo.getId()));
    }

    /**
     * 保存商品时的标签,把tagform转化成model
     */
    private List<Tag> initTag(ProductForm form) {
        List<Tag> tags = new ArrayList<>();
        Tag tag = null;
        if (null != form.getTags() && form.getTags().size() > 0) {
            for (TagForm tagForm : form.getTags()) {
                tag = new Tag();
                BeanUtils.copyProperties(tagForm, tag);
                tags.add(tag);
            }
        }
        return tags;
    }

    /**
     * 保存商品时的sku,把skuform转化成model
     */
    private List<Sku> initSku(ProductForm form) {
        List<Sku> skus = new ArrayList<>();
        Sku sku = null;
        if (null != form.getSkus() && form.getSkus().size() > 0) {
            for (SkuForm skuForm : form.getSkus()) {
                sku = new Sku();
                BeanUtils.copyProperties(skuForm, sku);
                skus.add(sku);
            }
        }
        return skus;
    }

    private List<SkuMapping> initSkuMapping(ProductForm form) {
        List<SkuMapping> skuMappings = new ArrayList<>();

        if (null != form.getSkuMappings() && form.getSkuMappings().size() > 0) {

            SkuMapping skuMapping = null;
            for (SkuMappingForm skuMappingForm : form.getSkuMappings()) {
                skuMapping = new SkuMapping();
                BeanUtils.copyProperties(skuMappingForm, skuMapping);
                skuMappings.add(skuMapping);
            }
        }
        return skuMappings;
    }

    /**
     * 保存商品时的图片,把imageForm转化成model
     */
    private List<ProductImage> initImage(ProductForm form) {
        List<ProductImage> imgs = new ArrayList<>();
        ProductImage pimg = null;
        int idx = 0;   // 从0开始
        if (null != form.getImgs() && form.getImgs().size() > 0) {
            for (String img : form.getImgs()) {
                pimg = new ProductImage();
                pimg.setImgOrder(idx++);
                pimg.setImg(img);
                //快店发布、编辑商品，图片默认为组图
                pimg.setType(1);
                imgs.add(pimg);
            }
        }
        return imgs;
    }

    /**
     * 店长推荐列表zzd
     */
    @ResponseBody
    @RequestMapping("/product/list/recommend")
    @ApiIgnore
    public ResponseObject<List<ForsaleListVO>> listForRecommend(Pageable pageable) {
        String shopId = this.getCurrentUser().getShopId();
        return new ResponseObject<>(forsale2Vo(
                generateImgUrl(productService.listProductsByRecommend(shopId, pageable))));
    }

    /**
     * 计划发布列表
     */
    @ResponseBody
    @RequestMapping("/product/list/forsale")
    @ApiIgnore
    public ResponseObject<List<ForsaleListVO>> listForsale(Pageable pageable) {
        String shopId = this.getCurrentUser().getShopId();
        return new ResponseObject<>(
                forsale2Vo(generateImgUrl(productService.listProductsByForSale(shopId, pageable))));
    }


    /**
     * 计划发布列表by zzd pc端返回map,ap端没有返回总的记录数，故需再次新增此方法
     */
    @ResponseBody
    @RequestMapping("/product/list/forsalebyPC")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> listForsaleByPC(Pageable pageable) {
        String shopId = this.getCurrentUser().getShopId();
        Map<String, Object> map = new HashMap<>();
        List<ForsaleListVO> lsForSale = forsale2Vo(
                generateImgUrl(productService.listProductsByForSale(shopId, pageable)));
        map.put("list", lsForSale);
        map.put("categoryTotal",
                productService.countProductsByStatus(shopId, ProductStatus.FORSALE));
        //return new ResponseObject<List<ForsaleListVO>>(forsale2Vo(generateImgUrl(productService.listProductsByForSale(shopId, pageable))));
        return new ResponseObject<>(map);
    }

    @SuppressWarnings("deprecation")
    private List<ForsaleListVO> forsale2Vo(List<ProductVOEx> products) {
        Map<String, List<Product>> mapProducts = new LinkedHashMap<>();
        List<Product> productsTmp = null;
        String forsaleAt = null;
        for (Product product : products) {
            forsaleAt = DateFormatUtils.format(product.getForsaleAt(), "yyyy-MM-dd HH");
            // 15 minutes interval per list
            int minutes = product.getForsaleAt().getMinutes();
            if (0 <= minutes && minutes < 15) {
                forsaleAt += ":00";
            } else if (15 <= minutes && minutes < 30) {
                forsaleAt += ":15";
            } else if (30 <= minutes && minutes < 45) {
                forsaleAt += ":30";
            } else { // 45 <= minutes && minutes < 60
                forsaleAt += ":45";
            }
            productsTmp = mapProducts.get(forsaleAt);
            if (productsTmp == null) {
                productsTmp = new ArrayList<>();
            }

            productsTmp.add(product);
            mapProducts.put(forsaleAt, productsTmp);
        }
        String serverTime = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        List<ForsaleListVO> vos = new ArrayList<>();
        try {
            for (String date : mapProducts.keySet()) {
                List<Product> ps = mapProducts.get(date);
                ForsaleListVO vo = new ForsaleListVO();
                vo.setDate(DateUtils.parseDate(date, "yyyy-MM-dd HH:mm"));
                vo.setServerTime(serverTime);
                vo.setList(ps);
                vos.add(vo);
            }
        } catch (ParseException e) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
        }

        return vos;
    }

    /**
     * 修改商品展示的优先级
     */
    @ResponseBody
    @RequestMapping("/product/updatePriority")
    public ResponseObject<Boolean> updatePriority(@RequestParam String id,
                                                  @RequestParam Integer priority) {
        boolean isExists = productService.exists(id);
        if (!isExists) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
        }
        boolean result = productService.updatePriority(id, priority);
        return new ResponseObject<>(result);
    }

    @ResponseBody
    @RequestMapping("/product/listGift")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> listGift(
            @RequestParam(defaultValue = "created_at") String order,
            @RequestParam(defaultValue = "DESC") String direction,
            String category, Pageable pageable) {
        List<Product> products;
        Long total;
        try {
            products = productService
                    .listGiftProduct(category, order, Direction.fromString(direction), pageable);
            total = productService.countGiftProduct(category);
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            String msg = "赠品加载失败";
            log.error(msg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("list", generateImgUrl(products));
        result.put("categoryTotal", total);
        return new ResponseObject<>(result);
    }

    @ResponseBody
    @RequestMapping("/filter/list")
    public ResponseObject<Map<String, ?>> listFilterBySpec(
            @RequestParam(defaultValue = "created_at") String order,
            @RequestParam(defaultValue = "DESC") String direction,
            @RequestParam FilterSpec spec,
            @RequestParam String category,
            @RequestParam String status,
            Pageable pageable) {
        // 过滤条件
        Map<String, ?> params = ImmutableMap.of(
                "spec", spec,
                "category", category,
                "status", status
        );
        List<Product> filters = productService.listFilterBySpec(order,
                Direction.DESC, pageable, params);
        List<? extends ProductVO> genProducts = generateImgUrl(filters);
        Long total = productService.countFilterBySpec(params);
        Map<String, ?> result = ImmutableMap.of("list", genProducts, "categoryTotal", total);
        return new ResponseObject<Map<String, ?>>(result);
    }

    /**
     * 按各种排序，获取商品列表
     */
    @ResponseBody
    @RequestMapping("/product/list")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> list(String order,
                                                    String productId, String direction, String category, String isGroupon, Pageable pageable,
                                                    ProductType type, @RequestParam(required = false) ProductStatus status,
                                                    @RequestParam(required = false) ProductReviewStatus reviewStatus, String brand,
                                                    String supplier, String keyword,
                                                    HttpServletRequest request, String decodeId) {

        // 默认类型为普通商品
        type = Optional.ofNullable(type).orElse(ProductType.NORMAL);
        Map<String, Object> params = new HashMap<>();
        params.put("type", type);
        params.put("status", status);
        params.put("reviewStatus", reviewStatus);
        params.put("brand", brand);
        params.put("supplier", supplier);
        params.put("keyword", keyword);
        params.put("decodeId", decodeId);

        String shopId = request.getParameter("shopId");
        if (shopId == null || "".equals(shopId)) {
            shopId = this.getCurrentIUser().getShopId();
        }
        //改为所有小b指向总店模式
        String rootShopId = null;
        if (StringUtils.isNotBlank(shopId))
            rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

        order = StringUtils.defaultIfBlank(order, "onsale"); // ios传参是onsale ,pc/安卓 是onsaleAt
        direction = StringUtils.defaultIfBlank(direction, "desc");
        List<Product> products = null;

        if ("sales".equals(order)) { //按销量
            products = productService.listProductsBySales(rootShopId, category, isGroupon, pageable,
                    Direction.fromString(direction), "", null, "", "",
                    status, reviewStatus);
        } else if ("amount".equals(order)) { //按库存
            products = productService
                    .listProductsByAmount(rootShopId, category, isGroupon, pageable,
                            Direction.fromString(direction), type, status, reviewStatus);
        } else if ("soldout".equals(order)) { //按下架时间
            products = productService
                    .listProductsBySoldout(rootShopId, category, isGroupon, pageable,
                            Direction.fromString(direction), type, status, reviewStatus);
        } else if ("statusDraft".equals(order)) { //按草稿
            products = productService
                    .listProductsByStatusDraft(rootShopId, category, isGroupon, pageable, type, status,
                            reviewStatus);
        } else if ("outofstock".equals(order)) { //按缺货
            products = productService
                    .listProductsByOutOfStock(rootShopId, category, isGroupon, pageable, type, status,
                            reviewStatus);
        } else if ("postage".equalsIgnoreCase(order)) {
            products = productService
                    .listProductsByPostAge(rootShopId, category, isGroupon, pageable,
                            Direction.fromString(direction), type, status, reviewStatus);
        } else if ("delay".equalsIgnoreCase(order)) { //按计划发布
            products = productService
                    .listProductsByDelay(rootShopId, category, isGroupon, pageable, type, status,
                            reviewStatus);
        } else if ("price".equals(order)) {
            /** by zzd 根据市场价格进行排序**/
            products = productService.listProductsByPrice(rootShopId, category, isGroupon, pageable,
                    Direction.fromString(direction), "", null, "", "", null, type);
        } else if ("onsaleAt".equals(order) || "onsale".equals(order)) {
            if (ProductStatus.SOLDOUT.equals(status)) {
                //普通商品售罄
                products = productService
                        .listProductsBySoldOutNotSf(rootShopId, category, isGroupon, pageable,
                                Direction.fromString(direction), type, ProductStatus.ONSALE, reviewStatus);
            } else {
                // 在售的上架商a:action Find
                products = productService
                        .listProductsByOnsaleAt(rootShopId, category, isGroupon, pageable, params);
            }
        }

        if (CollectionUtils.isNotEmpty(products)) {
            for (Product product : products) {
                setSku(product);
            }
        }

        Map<String, Object> aRetMap = new HashMap<>();
        Long total = productService
                .getLastTotalCnt(rootShopId, category, isGroupon, order, params);
        if (Objects.isNull(total)) {
            total = 0L;
        }
        aRetMap.put("categoryTotal", total);
        aRetMap.put("total", total);

        //已售罄
        if (ProductStatus.SOLDOUT.equals(status)) {
            List<Product> productList = productService
                    .listProductsBySoldOutNotSf(rootShopId, category, isGroupon, null,
                            Direction.fromString(direction), type, ProductStatus.ONSALE, reviewStatus);
            if (productList != null && !productList.isEmpty()) {
                aRetMap.put("categoryTotal", productList.size());
                aRetMap.put("total", productList.size());
            }
        }

        List<? extends ProductVO> generatedProduct;

        if ("postage".equalsIgnoreCase(order)) {
            generatedProduct = generateImgUrlEx(products);
        } else {
            generatedProduct = generateImgUrl(products);
        }

        // 若查询滤芯则添加用户选择信息
        List<String> checkedIdList;
        if (type == ProductType.FILTER && StringUtils.isNoneBlank(productId)) {
            // 此处的商品为滤芯
            for (ProductVO product : generatedProduct) {
                if (productService.isFilterBounded(productId, product.getId())) {
                    product.setSelect(true);
                }
            }
            checkedIdList = productService.boundedFilterIds(productId);
            aRetMap.put("checkedList", checkedIdList);
        }
        aRetMap.put("list", generatedProduct);
      aRetMap.put("shopId", shopId);

        return new ResponseObject<>(aRetMap);
    }


    /**
     * 为商品赋予基础规格属性
     */
    private Sku setSku(Product product) {
        if (Objects.isNull(product))
            return null;
        String pid;
        if (StringUtils.isNotBlank(pid = product.getId())) {
            Sku sku = skuMapper.selectMinPriceSkuByProductId(pid);
            if (Objects.isNull(sku))
                return null;
            product.setDeductionDPoint(sku.getDeductionDPoint());
            product.setPoint(sku.getPoint());
            product.setPrice(sku.getPrice());
            Integer totalAmount = skuMapper.countTotalAmount(pid);
            product.setAmount(totalAmount);
            product.setServerAmt(sku.getServerAmt());
            return sku;
        }

        return null;
    }

    /**
     * 获取商品关联滤芯
     *
     * @param productId 商品id
     * @param pageable  分页对象
     * @return 表格数据
     */
    @RequestMapping("product/listFilterTable")
    @ResponseBody
    public ResponseObject<Map<String, Object>> listFilter(String productId, Pageable pageable) {
        List<FilterBasicVO> list = productService.listFilterByProductId(productId, null, pageable);
        Long total = productService.countFilterByProductId(productId, null);
        Map<String, Object> result = new HashMap<>();
        Collection<String> checkedIds = Collections2.transform(list,
                new com.google.common.base.Function<FilterBasicVO, String>() {
                    @Override
                    public String apply(FilterBasicVO input) {
                        return input.getId();
                    }
                });
        result.put("list", list);
        result.put("total", total);
        result.put("checkedList", checkedIds);
        return new ResponseObject<>(result);
    }

    /**
     * 从SkuCode获取商品列表
     */
    @ResponseBody
    @RequestMapping("/product/search/skucode")
    @ApiIgnore
    public ResponseObject<List<ProductVOEx>> searchBySkuCode(
            @RequestParam("skucode") String skucode) {
        // 签名做jian
        String shopId = this.getCurrentUser().getShopId();
        List<Product> list = productService.findProductsBySkuCode(shopId, skucode);

        List<ProductVOEx> voList = new ArrayList<>();
        for (Product product : list) {
            formatSynchronousVal(product);
            ProductVO vo = new ProductVO(product);
            setFragmentAndDescInfo(vo);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());
            voList.add(voEX);
        }
        return new ResponseObject<>(voList);
    }

    /**
     * 商品查询
     */
    @ResponseBody
    @RequestMapping("/product/search/{shopId}/{key}")
    @ApiIgnore
    public ResponseObject<List<ProductVOEx>> search(@PathVariable String shopId,
                                                    @PathVariable String key) {
        List<Product> list = productService.search(shopId, key, ProductType.NORMAL);
        List<ProductVOEx> voList = new ArrayList<>();
        for (Product product : list) {
            formatSynchronousVal(product);
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());
            voList.add(voEX);
        }
        return new ResponseObject<>(voList);
    }

    /**
     * 商品查询 modify by zzd 新增 用于pc端,进行分页
     */
    @ResponseBody
    @RequestMapping("/product/searchbyPc/{shopId}/{key}")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> search(@PathVariable String shopId,
            @PathVariable String key, ProductType type,
            @RequestParam(required = false) String productId,
            @RequestParam(required = false) String categoryId, Pageable page) {
        Map<String, Object> map = new HashMap<>();
        List<Product> list = productService.searchWithNoStatus(shopId, key, page, type, productId, categoryId);
        List<ProductVOEx> voList = new ArrayList<>();
        for (Product product : list) {
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());
            voList.add(voEX);

        }

        map.put("list", voList);
        Long count = productService.CountTotalByNameWithNoStatus(shopId, key, productId, categoryId);
        map.put("categoryTotal", count);
        return new ResponseObject<>(map);
    }

    /**
     * 商品查询 modify by zzd 新增 用于pc端,进行分页
     */
    @ResponseBody
    @RequestMapping("/product/searchbyPcAll/{shopId}/{key}")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> searchAll(@PathVariable String shopId,
                                                         @PathVariable String key, Pageable page) {
        key = StringUtils.trim(key);
        Map<String, Object> map = new HashMap<>();
        List<Product> list = productService.searchAll(shopId, key, page);
        List<ProductVOEx> voList = new ArrayList<>();
        for (Product product : list) {
            ProductVO vo = new ProductVO(product);
            vo.setImgUrl(product.getImg());
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            setSku(voEX);
            voList.add(voEX);
        }

        map.put("list", voList);
        Long count = productService.CountTotalByName(shopId, key);
        map.put("categoryTotal", count);
        return new ResponseObject<>(map);
    }


    /**
     * 保存热搜关键字
     */
    @ResponseBody
    @RequestMapping(value = "/product/createHotSearchKey", method = RequestMethod.POST)
    @ApiOperation(value = "创建热搜关键字", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> createHotSearchKey(@RequestParam("key") String key,
                                                      @RequestParam("productIds") String productIds) {
        if (null == key || key.length() < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "热门词条不能为空");
        }
        if (null == productIds || productIds.length() < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请选择商品");
        }
        boolean result = productService.newHotSearchKey(key, productIds);
        // 将值放到redis中
//    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
//    String promotionId = promotion.getId();
//    String flashSaleAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId);
//    String flashSaleLimitKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId);
//    String flashSaleTotalAmountKey = PromotionConstants.getFlashSaleTotalAmountKey(promotionId);
//    redisUtils.set(flashSaleLimitKey, form.getLimitAmount().intValue());
//    redisUtils.set(flashSaleAmountKey, form.getAmount().intValue());
//    // 总值
//    redisUtils.set(flashSaleTotalAmountKey, form.getAmount().intValue());

        return new ResponseObject<>(result);
    }

    /**
     * 获取指定热搜关键字信息
     */
    @ResponseBody
    @RequestMapping(value = "/product/hotSearchKeys/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "获取指定热搜关键字", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @DoGuavaCache(key = CacheKeyConstant.HOT_SEARCH_KEYS)
    public ResponseObject<HotSearchKey> getHotSearchKeyById(@PathVariable("id") String id,
                                                            HttpServletRequest req) {
        HotSearchKey hsk = productService.getHotSearchKeyById(id);
        // 添加至Redis
        return new ResponseObject<>(hsk);
    }

    /**
     * 获取指定热搜关键字信息
     */
    @ResponseBody
    @RequestMapping(value = "/product/hotSearchKeys/remove", method = RequestMethod.POST)
    @ApiOperation(value = "获取指定热搜关键字", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> removeHotSearchKeyById(@RequestParam("ids") String ids,
                                                          HttpServletRequest req) {
        if (null == ids || ids.length() < 1) {
            return new ResponseObject<>(GlobalErrorCode.INVALID_ARGUMENT);
        }
        Boolean result = productService.removeHotSearchKeyByIds(Arrays.asList(ids.split(",")));
        // 添加至Redis
        return new ResponseObject<>(result);
    }

    /**
     * 更新热搜关键字
     */
    @ResponseBody
    @RequestMapping(value = "/product/updateHotSearchKey", method = RequestMethod.GET)
    @ApiOperation(value = "更新热搜关键字配置", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> updateHotSearchKey(@RequestParam("id") String id,
                                                      @RequestParam(value = "key", required = false) String key,
                                                      @RequestParam(value = "productIds", required = false) String productIds) {
        if (null == id || id.length() < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请选择商品!");
        }
        if ((null == key || key.length() < 1) && (null == productIds || productIds.length() < 1)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "热门词条和商品不能同时为空");
        }
        boolean result = productService.updateHotSearchKey(id, key, productIds);
        // 添加至Redis
        return new ResponseObject<>(result);
    }

    /**
     * 列出可用热搜关键字
     */
    @ResponseBody
    @RequestMapping(value = "/product/listHotSearchKeys", method = RequestMethod.GET)
    @ApiOperation(value = "列出热搜关键字(前端使用)", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<HotSearchKey>> listHotSearchKey(Pageable page) {
        List<HotSearchKey> result = productService.listValidHotSearchKeys(null, page);
        return new ResponseObject<>(result);
    }

    @ResponseBody
    @RequestMapping(value = "/product/listHotSearchKeys2", method = RequestMethod.GET)
    @ApiOperation(value = "列出热搜关键字(前端使用)", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Map<String, Object>> countHotSearchKey(@RequestParam("key") String key,
                                                                 Pageable page) {
        Integer totalCount = productService.countTotalByKey(key);

        Map<String, Object> map = new HashMap<>();
        map.put("total", totalCount);
        if (totalCount > 0) {
            map.put("pageNum", page.getPageNumber());
            map.put("list", productService.listValidHotSearchKeys(key, page));
        }

        return new ResponseObject<>(map);
    }

    /**
     * 更新热搜关键字启用状态
     */
    @ResponseBody
    @RequestMapping(value = "/product/updateKeyState", method = RequestMethod.GET)
    @ApiOperation(value = "更新热搜关键字显示状态", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> updateKeyState(@RequestParam(value = "id") String id,
                                                  @RequestParam(value = "state") Integer state) {
        if (null == id || id.length() < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "id不能为空");
        }
        if (null == state || state > 1 || state < 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "state参数不合法");
        }
        boolean result = productService.updateHotKeyState(id, state);
        return new ResponseObject<>(result);
    }

    /**
     * 更新热搜关键字
     */
    @ResponseBody
    @RequestMapping(value = "/product/updateDisplayState", method = RequestMethod.GET)
    @ApiOperation(value = "更新热搜关键字显示状态", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> saveHotSearchKey(@RequestParam(value = "id") String id,
                                                    @RequestParam(value = "display") Integer display) {
        if (null == id || id.length() < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "id不能为空");
        }
        if (null == display || display > 1 || display < 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "display参数不合法");
        }
        boolean result = productService.setHotSearchKeyDisplayState(id, display);
        return new ResponseObject<>(result);
    }

    /**
     * 更新热搜展示数量配置
     */
    @ResponseBody
    @RequestMapping(value = "/product/updateDisplayLimit", method = RequestMethod.GET)
    @ApiOperation(value = "更新热搜关键字显示状态", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> updateDisplayLimit(@RequestParam(value = "limit") String limit) {
        if (null == limit || limit.length() < 1 || Integer.parseInt(limit) < 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不合法");
        }
        boolean result = productService.updateHotSearchKeyDisplayLimit(limit);
        return new ResponseObject<>(result);
    }

    /**
     * 获取热搜词
     */
    @ResponseBody
    @RequestMapping(value = "/product/hotSearchKeys", method = RequestMethod.GET)
    @ApiOperation(value = "获取热搜词字符列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<String>> hotSearchKeys() {
        Integer displayNum = productService.findHotSearchKeyDisplayLimit();
        List<String> hotSearchedKeys = productService.listHotSearchedKeys(displayNum);
        return new ResponseObject<>(hotSearchedKeys);
    }


    /**
     * app选品商品查询
     */
    @ResponseBody
    @RequestMapping("/product/searchbyApp")
    @ApiOperation(value = "根据关键字查询商品", notes =
            "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
                    "排序传递order(default代表上架时间，price代表价格),direction(asc,desc)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Map<String, Object>> searchByApp(
            @RequestParam(value = "key", required = false) String key, Pageable page,
            HttpServletRequest req,
            @RequestParam(value = "order", required = false) String order,
            @RequestParam(value = "direction", required = false) String direction,
            @RequestParam(value = "scope", required = false) ProductType scope
            , ProductSearchVO productSearchVO) {
        if (Objects.isNull(productSearchVO)) {
            productSearchVO = new ProductSearchVO();
        }
        List<Product> list = new ArrayList<>();
        List<Product> hotSearchedProducts = new ArrayList<>();

        order = StringUtils.defaultIfBlank(order, "default").toLowerCase();
        direction = StringUtils.defaultIfBlank(direction, "desc").toLowerCase();

        Long totalCount = 0L;

        // 判断该关键词是否配置了热搜词
        Boolean ifHotsearchKeyExists = productService.checkHotsearchKeyExists(key);
        if (ifHotsearchKeyExists) {
            // 先判断是否为热搜词商品搜索
            hotSearchedProducts = productService.filterHotSearchedProducts(key, order, direction, page, productSearchVO);
            totalCount = productService.countHotSearchedProducts(key);
        } else {
            list = getNormalSearchProducts(key, page, order, direction, scope, productSearchVO);
        }
        // 合并搜索结果
        if (hotSearchedProducts != null)
            list.addAll(hotSearchedProducts);

        if (list == null || list.isEmpty())
            totalCount = 1L;
        // TODO: 对活动商品进行过滤（搜索的所有商品结果中不包含活动商品）
/*    List<Long> activityIds = vipProductService.listExclusiveActivity();
    // 保存加密后的活动商品ID集合
    List activityEncodeIds = new ArrayList<>();
    // 将查询出的活动商品ID进行加密
    if (activityIds.size() > 0) {
      for (Long activityId : activityIds) {
        String encodeId = IdTypeHandler.encode(activityId);
        activityEncodeIds.add(encodeId);
      }
    }
    // 移除搜索结果中的活动套装商品
    for (int i = 0; i < list.size(); i++) {
      Product product = list.get(i);
      if (activityEncodeIds.contains(product.getId())) {
        list.remove(product);
        i--;
      }
    }*/

        String shopId = getCurrentIUser().getShopId();
        List<ProductVOEx> voList = new ArrayList<>();

        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + getCurrentUser().getCpId());

        for (Product product : list) {
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());

            if (product.getCommissionRate() != null) {
                voEX.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            //商品卖家端是否上架

            //Product productSource = productMapper.selectProductBySourceProductIdAndShopId(product.getSourceProductId(),shopId);
            ProductDistributor productDistributor = productDistributorMapper
                    .selectByProductIdAndShopId(product.getId(), shopId);
            if (productDistributor != null) {
                voEX.setOnSaleForSeller(true);
            } else {
                voEX.setOnSaleForSeller(false);
            }
            //多少卖家在卖
            //long count = productDistributorMapper.countProductsSellerById(product.getSourceProductId());
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            voEX.setCountSeller(count);

            voEX.setProductUrl(
                    siteHost + "/p/" + voEX.getId() + "?union_id=" + getCurrentUser().getId());

            voList.add(voEX);

            Sku sku = setSku(voEX);

            if (json != null && sku != null) {
                JSONObject jsonObject = JSONObject.parseObject(json);
                Boolean isProxy = jsonObject.getBoolean("isProxy");
                Boolean isMember = jsonObject.getBoolean("isMember");

                BigDecimal conversionPrice = voEX.getConversionPrice();

                BigDecimal subtractValue = voEX.getPoint().add(sku.getServerAmt());
                BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2, 1);
                voEX.setProxyPrice(proxyPrice);

                voEX.setMemberPrice(sku.getPrice().subtract(voEX.getPoint()).setScale(2, 1));

                if (isProxy) {
                    voEX.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2, 1));

                    voEX.setSavedPrice(subtractValue);
                    voEX.setEarnedPrice(subtractValue);
                } else if (isMember) {
                    voEX.setChangePrice(conversionPrice.subtract(voEX.getPoint()).setScale(2, 1));

                    voEX.setSavedPrice(voEX.getPoint());
                    voEX.setEarnedPrice(voEX.getPoint());
                } else {
                    voEX.setChangePrice(conversionPrice);

                    voEX.setSavedPrice(voEX.getPoint());
                    voEX.setEarnedPrice(voEX.getPoint());
                }

            }

        }
        Map<String, Object> map = new HashMap<>();
        map.put("list", voList);
        if (totalCount < 1) {
            totalCount = productService.CountTotalByName(shopId, key);
        }
        map.put("categoryTotal", totalCount);
        return new ResponseObject<>(map);
    }

    // 新icon汉薇自营商品
    @ResponseBody
    @RequestMapping("/product/hw/selfSupport")
    @ApiOperation(value = "首页icon汉薇自营商品", notes =
            "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
                    "排序传递order(default代表上架时间，price代表价格),direction(asc,desc)", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Map<String, Object>> selfSupport(
            @RequestParam(value = "order", required = false) String order,
            @RequestParam(value = "direction", required = false) String direction,
            @RequestParam(value = "scope", required = false) ProductType scope, Pageable page) {
        List<Product> list;
        order = StringUtils.defaultIfBlank(order, "default").toLowerCase();
        direction = StringUtils.defaultIfBlank(direction, "desc").toLowerCase();

        Long totalCount = 0L;

        list = getNormalSelfSupportProducts(page, order, direction, scope);

        String shopId = getCurrentIUser().getShopId();
        List<ProductVOEx> voList = new ArrayList<>();

        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String json = redisUtils.get("login_return_" + getCurrentUser().getCpId());

        for (Product product : list) {
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());

            if (product.getCommissionRate() != null) {
                voEX.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            ProductDistributor productDistributor = productDistributorMapper
                    .selectByProductIdAndShopId(product.getId(), shopId);
            if (productDistributor != null) {
                voEX.setOnSaleForSeller(true);
            } else {
                voEX.setOnSaleForSeller(false);
            }
            //多少卖家在卖
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            voEX.setCountSeller(count);

            voEX.setProductUrl(siteHost + "/p/" + voEX.getId() + "?union_id=" + getCurrentUser().getId());

            //自营标识
            voEX.setSelfOperated(product.getSelfOperated());

            voList.add(voEX);

            Sku sku = setSku(voEX);

            if (json != null && sku != null) {
                JSONObject jsonObject = JSONObject.parseObject(json);
                Boolean isProxy = jsonObject.getBoolean("isProxy");
                Boolean isMember = jsonObject.getBoolean("isMember");

                BigDecimal conversionPrice = voEX.getConversionPrice();

                BigDecimal subtractValue = voEX.getPoint().add(sku.getServerAmt());
                BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2, 1);
                voEX.setProxyPrice(proxyPrice);

                voEX.setMemberPrice(sku.getPrice().subtract(voEX.getPoint()).setScale(2, 1));

                if (isProxy) {
                    voEX.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2, 1));

                    voEX.setSavedPrice(subtractValue);
                    voEX.setEarnedPrice(subtractValue);
                } else if (isMember) {
                    voEX.setChangePrice(conversionPrice.subtract(voEX.getPoint()).setScale(2, 1));

                    voEX.setSavedPrice(voEX.getPoint());
                    voEX.setEarnedPrice(voEX.getPoint());
                } else {
                    voEX.setChangePrice(conversionPrice);

                    voEX.setSavedPrice(voEX.getPoint());
                    voEX.setEarnedPrice(voEX.getPoint());
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("list", voList);
        if (totalCount < 1) {
            totalCount = productService.CountTotalBySelfSupport(shopId);
        }
        map.put("categoryTotal", totalCount);
        return new ResponseObject<>(map);
    }


    private List<Product> getNormalSearchProducts(String key, Pageable page, String order,
                                                  String direction, ProductType scope, ProductSearchVO pVO) {
        List<Product> list = new ArrayList<>();
        String rootShopId = shopTreeService.getRootShop().getAncestorShopId();

        // 如果滤芯的商品型号全匹配则查询所有匹配的滤芯
        // 否则按正常逻辑搜索
        boolean fullMatch = false;

        if (scope == ProductType.FILTER) {
            // 匹配用户输入的时候是否为商品型号
            if (productService.selectIsModelExists(key)) {
                // 输入商品型号则匹配所有滤芯返回
                list = productService.listFilterByProductModel(key, page);
                fullMatch = true;
            }
        }

        if (!fullMatch) {
            if (StringUtils.isEmpty(order) || order.equals("default")) {
                list = productService.search(rootShopId, key, page, scope);
            } else if (order.equals("price")) {
                list = productService.searchByPrice(rootShopId, key, page, direction,
                        scope);
            } else {
                list = productService.listProductsBySearchOrdered(rootShopId, key, page,
                        direction, scope, order);
            }
        }
        //1.过滤汉薇自营
        if (!Objects.isNull(pVO.getSelfOperatedKey()) && pVO.getSelfOperatedKey() == 1) {
            list = productService.filterSelfOperatedByPVo(list);
        }
        //2过滤是否有货
        if (!Objects.isNull(pVO.getStockKey()) && pVO.getStockKey() == 1) {
            list = productService.filterIsStockByPVo(list);
        }
        return list;
    }


    /**
     * 汉薇自营商品列表
     *
     * @param page
     * @param order
     * @param direction
     * @param scope
     * @return
     */
    private List<Product> getNormalSelfSupportProducts(Pageable page, String order, String direction, ProductType scope) {
        List<Product> list;
        String rootShopId = shopTreeService.getRootShop().getAncestorShopId();
        if (StringUtils.isEmpty(order) || order.equals("default")) {
            list = productService.selfSupport(rootShopId, page, scope);
        } else if (order.equals("price")) {
            list = productService.selfSupportByPrice(rootShopId, page, direction, scope);
        } else {
            list = productService.selfSupportByOrdered(rootShopId, page, direction, scope, order);
        }
        return list;
    }

    /**
     * 商品管理首页
     */
    @ResponseBody
    @RequestMapping("/product/index")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> productIndex() {
        String shopId = this.getCurrentUser().getShopId();
        Map<String, Object> index = new HashMap<>();
        index.put("draft", productService.countProductsByStatus(shopId, ProductStatus.DRAFT)); // 草稿
        index.put("forsale",
                productService.countProductsByStatus(shopId, ProductStatus.FORSALE)); // 发布计划
        index.put("outofstock", productService.countProductsByOutofStock(shopId)); // 缺货
        index.put("onsale",
                productService.countProductsByStatus(shopId, ProductStatus.ONSALE)); // 上架
        List<Product> products = productService
                .listProductsByOnsaleAt(shopId, "", "", new PageRequest(0, 10));
        index.put("products", generateImgUrl(products));

        return new ResponseObject<>(index);
    }

    @ResponseBody
    @RequestMapping("/product/all")
    @ApiIgnore
    public ResponseObject<List<ProductAdmin>> productAll(Pageable pager) {
        Map<String, Object> params = new HashMap<>();
        List<ProductAdmin> products = productService.listProductsByAdmin(params, pager);
        return new ResponseObject<>(products);
    }

    @ResponseBody
    @RequestMapping("/product/jiankangmao/all")
    @ApiIgnore
    public ResponseObject<List<ProductAdmin>> productAll4jiankangmao(Pageable pager) {
        Map<String, Object> params = new HashMap<>();
        params.put("shopId", "70wuyvgk");
        List<ProductAdmin> products = productService.listProductsByAdmin(params, pager);
        return new ResponseObject<>(products);
    }

    /**
     * 查看某个具体的商品<br>
     */
    @ResponseBody
    @RequestMapping("/product/jiankangmao/{id}")
    @ApiIgnore
    public ResponseObject<ProductVOEx> viewJiankangmao(@PathVariable String id,
                                                       HttpServletRequest req) {
        ProductVO product = productService.load(id);
        RequestContext requestContext = new RequestContext(req);
        if (product == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND,
                    requestContext.getMessage("product.not.found"));
        }
        //当前商品不是jiankangmao的商品
        if (!"70wuyvgk".equals(product.getShopId())) {
            throw new BizException(GlobalErrorCode.UNAUTHORIZED,
                    requestContext.getMessage("product.not.found"));
        }
        convertUrl(product); // 生成商品的图片URL
        // 约定获取商品的图文信息的时候，从productService中获取，实现：先获取富文本信息，再获取片段信息
        setFragmentAndDescInfo(product);
        return new ResponseObject<>(
                new ProductVOEx(product, urlHelper.genProductUrl(product.getId()), product.getImg(),
                        null));
    }

    /**
     * TODO 该接口不需要用户登录
     */
    @ResponseBody
    @RequestMapping("/product/skus")
    @ApiOperation(value = "获取某个商品的所有sku信息", notes = "获取某个商品的所有sku信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<SkuWithMappingVO> skusWithMapping(String productId, String partner,
                                                            HttpServletRequest req) {
        ProductVO product = productService.load(productId);
        if (product == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product.not.found");
        }

        SkuWithMappingVO vo = new SkuWithMappingVO();

        List<Sku> skus = genPartnerSkus(productId, req);

        vo.setSkus(skus);
        vo.setSkuMappings(product.getSkuMappings());

        return new ResponseObject<>(vo);
    }

    /**
     * TODO 该接口不需要用户登录
     */
    @ResponseBody
    @RequestMapping("/product/{id}/skus")
    @ApiIgnore
    public ResponseObject<List<Sku>> skus(@PathVariable String id, HttpServletRequest req) {
        return new ResponseObject<>(genPartnerSkus(id, req));
    }

    private List<Sku> genPartnerSkus(String productId, HttpServletRequest req) {
        Product product = productService.findProductById(productId);
        RequestContext requestContext = new RequestContext(req);
        if (product == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                    requestContext.getMessage("product.not.found"));
        }

        List<Sku> products = productService.listSkus(productId);
        List<ProdSync> aList = prodSyncMapper.selectByPassedShopId(product.getShopId());
        User aUser = userService.loadByLoginname("xiangqu");
        List<Sku> result = new ArrayList<>();
        for (Sku sku : products) {
            String skuUrl =
                    xiangquCartHost + "/cart/checkout?skuId=" + sku.getId() + "&partner=xiangqu";
            if (product != null) {
                if (aList != null && aList.size() > 0) {
                    skuUrl += "&union_id=";
                    skuUrl += aList.get(0).getUnionId();
                } else {
                    if (aUser != null) {
                        skuUrl += "&union_id=";
                        skuUrl += aUser.getId();
                    }
                }
            }
            try {
                sku.setSkuUrl(URLEncoder.encode(skuUrl, "utf-8"));
            } catch (Exception e) {
                log.warn("URLEncoder.encode err for skuUrl");
            }
            if (sku.getAmount() > 0) {
                result.add(sku);
            }
        }

        return result;
    }

    private List<ProductVOEx> generateImgUrlEx(List<Product> products) {
        List<ProductVOEx> exs = new ArrayList<>();
        ProductVOEx ex = null;
        if (products == null) {
            return exs;
        }
        for (Product product : products) {
            if (Objects.isNull(product))
                continue;
            ex = new ProductVOEx(new ProductVO(product), urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            //fixme
            if (product.getCommissionRate() != null) {
                ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            ex.setCountSeller(productMapper.countProductsSellerById(product.getId()));
            //ex.setCategory(categoryService.loadCategoryByProductId(product.getId()));
            exs.add(ex);
        }
        return exs;
    }


    private List<ProductVOEx> generateImgUrl(List<Product> products) {
        List<ProductVOEx> exs = new ArrayList<>();
        ProductVOEx ex = null;
        Activity activity = null;
        if (products == null) {
            return exs;
        }
        for (Product product : products) {
            if (Objects.isNull(product))
                continue;
            //TODO 单独提取  150122 by.aton
            //商品参加的活动
            activity = activityService.obtainProductCurrentActivities(product.getId());

            formatSynchronousVal(product);

            ex = new ProductVOEx(new ProductVO(product), urlHelper.genProductUrl(product.getId()),
                    product.getImg(), activity == null ? null : activity.getName());
            //ex.setCategory(categoryService.loadCategoryByProductId(product.getId()));
            //fixme
            if (product.getCommissionRate() != null) {
                ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            ex.setCountSeller(count);
            exs.add(ex);
        }
        //TODO
        return exs;
    }

    // 格式化商品来源渠道
    private void formatSynchronousVal(Product product) {
        String synchronousVal = "";
        String synchronous = product.getSynchronousFlag();
        if (synchronous != null && synchronous.length() > 1) {
            if (synchronous.substring(9, 10).equals("1")) {
                synchronousVal += "XIANGQU" + ",";
            }
        }
        product.setSynchronousFlag(synchronousVal);
    }

    /**
     * 获取延迟发货商品列表
     */
    @ResponseBody
    @RequestMapping("/product/list/delay")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> productListDelay(Pageable pageable) {
        String shopId = this.getCurrentUser().getShopId();
        List<Product> products = null;
        long total = productService.countDelayProduct(shopId);
        if (total > 0) {
            products = productService.listDelayProduct(shopId, pageable);
        } else {
            products = new ArrayList<>();
        }
        Map<String, Object> aRetMap = new HashMap<>();
        aRetMap.put("categoryTotal", total);
        aRetMap.put("list", generateImgUrl(products));
        return new ResponseObject<>(aRetMap);
    }

    @ResponseBody
    @RequestMapping("/product/batch-onsale")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> batchOnsale(@Valid @ModelAttribute BatchIdsForm form,
                                                           Errors errors) {
        ControllerHelper.checkException(errors);
        List<String> successList = new ArrayList<>();
        List<Map<String, String>> failList = new ArrayList<>();
        for (int i = 0; i < form.getIds().size(); i++) {
            Map<String, String> failMap = new LinkedHashMap<>();
            failMap.put("id", form.getIds().get(i));
            try {
                int result = productService.onsale(form.getIds().get(i));
                if (result > 0) {
                    successList.add(form.getIds().get(i));
                    continue;
                } else {
                    failMap.put("message", "update fail");
                    failList.add(failMap);
                }
            } catch (Exception e) {
                failMap.put("message", e.getMessage());
                failList.add(failMap);
            }
        }
        Map<String, Object> json = new HashMap<>();
        json.put("isAllSuccess", successList.size() == form.getIds().size());
        json.put("successList", successList);
        json.put("failList", failList);
        return new ResponseObject<>(json);
    }

    @ResponseBody
    @RequestMapping("/product/batch-instock")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> batchInstockWithLock(
            @Valid @ModelAttribute BatchIdsForm form, Errors errors) {
        ControllerHelper.checkException(errors);
        List<String> successList = new ArrayList<>();
        List<Map<String, String>> failList = new ArrayList<>();
        for (int i = 0; i < form.getIds().size(); i++) {
            Map<String, String> failMap = new LinkedHashMap<>();
            failMap.put("id", form.getIds().get(i));
            try {
                int result = productService.instock(form.getIds().get(i));
                /**
                 * 商品下架后，设置分销池商品下架
                 */
                List<ActivityProduct> aplist = activityProductMapper
                        .selectByproductId(form.getIds().get(i));
                if (null != aplist && aplist.size() > 0) {
                    for (ActivityProduct activityProduct : aplist) {
                        // 设置分销池商品下架
                        if (!activityProduct.getArchive()) {
                            activityProduct.setArchive(true);
                            activityProductMapper.update(activityProduct);
                        }
                    }
                }
                if (result > 0) {
                    successList.add(form.getIds().get(i));
                    continue;
                } else {
                    failMap.put("message", "update fail");
                    failList.add(failMap);
                }
            } catch (Exception e) {
                failMap.put("message", e.getMessage());
                failList.add(failMap);
            }
        }
        Map<String, Object> json = new HashMap<>();
        json.put("isAllSuccess", successList.size() == form.getIds().size());
        json.put("successList", successList);
        json.put("failList", failList);
        return new ResponseObject<>(json);
    }

    /**
     * 审核同步商品来源
     *
     * @param sourceVal  资源平台
     * @param examineRet 审核结果 1:通过 0：不通过
     */
    @ResponseBody
    @RequestMapping("/product/synchronous")
    @ApiIgnore
    public ResponseObject<Boolean> synchronous(String[] ids, String sourceVal, Integer examineRet) {
        // need to be refine
        int result = 0;
        List<String> lsId = new ArrayList<>(ids.length);
        for (String id : ids) {
            lsId.add(id);
        }
        if (examineRet != 0 && examineRet != 1) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "state.error");
        }
        List<String> sKeys = productService.obtainDbSynchronous(lsId);
        if (sKeys.size() > 0) {
            String dbVal = null;
            List<String> sourceCodes = new ArrayList<>(ids.length);
            for (int i = 0; i < sKeys.size(); i++) {
                if (sKeys.get(i) == null) {
                    dbVal = HtmlUtils.padLeft("0", 8);
                } else if (sKeys.get(i).length() < 2) {
                    dbVal = HtmlUtils.padLeft(sKeys.get(i), 8);
                } else {
                    dbVal = sKeys.get(i);
                }
                int t = 0;
                if (sourceVal != null) {
                    switch (sourceVal) {
                        case "xiangqu":
                            if (examineRet > 0) {
                                t = examineRet | Integer.valueOf(dbVal.substring(9, 10));
                            } else {
                                t = examineRet & Integer.valueOf(dbVal.substring(9, 10));
                            }
                            sourceCodes.add(dbVal.substring(0, 9) + String.valueOf(t));
                            break;
                        case "mogujie":
                            if (examineRet > 0) {
                                t = examineRet | Integer.valueOf(dbVal.substring(8, 9));
                            } else {
                                t = examineRet & Integer.valueOf(dbVal.substring(8, 9));
                            }
                            sourceCodes.add(dbVal.substring(0, 8) + String.valueOf(t) + dbVal
                                    .substring(9, 10));
                            break;
                        default:
                            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "platform.error");
                    }
                } else {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "platform.error");
                }
                result = productService.synchronous(lsId.get(i), sourceCodes.get(i));
            }
        } else {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "product.not.found");
        }
        return new ResponseObject<>(result > 0);
    }

    /**
     * 获取所有在售商品（ap端不存在）
     */
    public ResponseObject<List<Product>> listProductsAll(Pageable pageable) {
        String shopId = this.getCurrentUser().getShopId();
        return new ResponseObject<>(productService.listProducts(shopId));
    }

    /**
     * 商品保存，新增和修改共用一个 api，接口
     */
    @ResponseBody
    @RequestMapping("/product/addDistributionProduct")
    @ApiIgnore
    public ResponseObject<ProductVOEx> addDistributionProduct(
            @RequestParam("productId") String productId, @RequestParam("shopId") String shopId,
            @RequestParam(required = false) String cateid) {
        //ControllerHelper. checkException(errors);
        //RequestContext requestContext = new RequestContext(req);
        ProductForm form = new ProductForm();
        Product productSrc = productService.findProductById(productId);
        Product product = new Product();
        BeanUtils.copyProperties(productSrc, form);
        form.setId("");

        product.setSourceProductId(productSrc.getId());
        product.setSourceShopId(productSrc.getShopId());
        product.setIsDistribution(true);
        //product.setId(productSrc.getId());
        product.setRecommend(null != form.getRecommend() && form.getRecommend() == 1);

        if (form.getStatus() == ProductStatus.FORSALE) {
            try {
                product.setForsaleAt(new Date(form.getForsaleDate()));
            } catch (Exception e) {
                //throw new BizException(GlobalErrorCode.UNKNOWN, requestContext.getMessage("valid.product.forsale.notBlank.message" ));
            }
        }
        product.setName(productSrc.getName());

        // 商品的名称取描述的前25个字符
        if (StringUtils.isBlank(product.getName())) {
            product.setName(StringUtils.abbreviate(productSrc.getDescription(), 25));
        }

        product.setUserId(userService.getCurrentUser().getId());
        product.setStatus(productSrc.getStatus());
        Shop shop = shopService.mine();
        product.setShopId(shop.getId());

        //List<Sku> skus = initSku( form);
        //List<SkuMapping> skuMapping = initSkuMapping(form );
        //List<ProductImage> imgs = initImage( form);
        List<Sku> skus = skuMapper.selectByProductId(productId);
        List<SkuMapping> skuMapping = skuMappingMapper.selectByProductId(productId);
        List<ProductImage> imgs = productImageMapper.selectByProductId(productId);

        List<Tag> tags = initTag(form);
        product.setImg(null != imgs && imgs.size() > 0 ? imgs.get(0).getImg() : null);

        if (productSrc.getEnableDesc() == null) {
            product.setEnableDesc(false);
        }
        product.setAmount(productSrc.getAmount());
        product.setArchive(productSrc.getArchive());
        product.setCreatedAt(productSrc.getCreatedAt());
        product.setDelayed(productSrc.getDelayed());
        product.setDelayAt(productSrc.getDelayAt());

        int rc = 0;

        if (StringUtils.isBlank(form.getId())) {
            product.setSource(ProductSource.CLIENT);
            rc = productService.create(product, skus, tags, imgs, skuMapping);
            List<ProductFragment> listProductFragment = productFragmentService
                    .selectByProductId(productId);
            if (listProductFragment != null) {
                for (int i = 0; i < listProductFragment.size(); i++) {
                    ProductFragment productFragmentCopy = new ProductFragment();
                    ProductFragment productFragment = listProductFragment.get(i);
                    BeanUtils.copyProperties(productFragment, productFragmentCopy);
                    productFragmentCopy.setId("");
                    productFragmentCopy.setProductId(product.getId());
                    productFragmentService.insert(productFragmentCopy);
                    Fragment fragmentCopy = new Fragment();
                    FragmentVO fragmentVO = fragmentService
                            .selectById(productFragment.getFragmentId());
                    BeanUtils.copyProperties(fragmentVO, fragmentCopy);
                    fragmentCopy.setId("");
                    fragmentCopy.setShopId(shop.getId());
                    fragmentService.insert(fragmentCopy);
                    List<FragmentImageVO> listFragmentImageVO = fragmentVO.getImgs();
                    if (listFragmentImageVO != null) {
                        for (int j = 0; j < listFragmentImageVO.size(); j++) {
                            FragmentImageVO fragmentImageVO = listFragmentImageVO.get(i);
                            FragmentImage fragmentImageCopy = new FragmentImage();
                            BeanUtils.copyProperties(fragmentImageVO, fragmentImageCopy);
                            fragmentImageCopy.setId("");
                            fragmentImageService.insert(fragmentImageCopy);
                        }
                    }

                }
            }
        } else {
            rc = productService.update(product, skus, tags, imgs, skuMapping);
        }

        if (rc == 0) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
        }
        // 设置商品类目
        if (StringUtils.isNotBlank(form.getCategory())) {
            categoryService.addProductCategory(product.getId(), form.getCategory());
        } else {
            if (cateid != null) {
                categoryService.addProductCategory(product.getId(), cateid);
            }
        }
        ProductVO vo = productService.load(product.getId());
        convertUrl(vo); // 生成商品的图片URL
        formatSynchronousVal(vo);
        return new ResponseObject<>(
                new ProductVOEx(vo, urlHelper.genProductUrl(vo.getId()), vo.getImg(), null));
    }

    @ResponseBody
    @RequestMapping(value = "product/validActivityIsOnsale")
    @ApiIgnore
    public Boolean validActivityIsOnsale(@Valid @ModelAttribute BatchIdsForm form, Errors errors) {
        boolean flag = false;
        log.info(super.getCurrentUser().getPhone() + "分销池商品productId in:" + form.getIds().get(0));
        List<ActivityProduct> aplist = activityProductMapper
                .selectByproductId(form.getIds().get(0));
        if (null != aplist && aplist.size() > 0) {
            for (ActivityProduct activityProduct : aplist) {
                // 如果该商品在分销池存在上架状态，则再次提示是否下架
                if (!activityProduct.getArchive()) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * 修改sku编码
     */
    @ResponseBody
    @RequestMapping("/product/{skuId}/{skuCode}")
    @ApiIgnore
    public Boolean updateSkuCode(@PathVariable String skuId, @PathVariable String skuCode) {
        boolean flag = false;
        int resUpd = 0;
        try {
            Sku sku = new Sku();
            sku.setId(skuId);
            sku.setSkuCode(skuCode);
            resUpd = skuMapper.updateByPrimaryKeySelective(sku);
        } catch (Exception e) {
            log.error("规格编码修改失败, skuId=" + skuId, e);
        }
        if (resUpd > 0) {
            flag = true;
        }
        return flag;
    }

    /**
     * 验证规格编码是否重复
     */
    @ResponseBody
    @RequestMapping("/product/validSkuCode")
    @ApiIgnore
    public Boolean validSkuCode(@RequestParam("skuCode") String skuCode,
                                @RequestParam("skuResources") String skuResources) {
        try {
            List<Sku> skus = skuMapper.findBySkuCode4SkuResources(skuCode, skuResources);
            if (null != skus && skus.size() > 0) {
                for (Sku sku : skus) {
                    if (StringUtils.isBlank(sku.getSkuCode())) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception e) {
            log.error("sku查询失败.. skuCode=" + skuCode, e);
        }
        return false;

    }

    @ResponseBody
    @RequestMapping(value = "product/validActivity")
    @ApiIgnore
    public Boolean validActivity(@Valid @ModelAttribute BatchIdsForm form, Errors errors) {
        boolean flag = false;
        log.info(super.getCurrentUser().getPhone() + "商品productId in:" + form.getIds().get(0));
        Product pro = productService.findProductById(form.getIds().get(0));
        // 是否为分销商品
        if (null != pro && !"0".equals(pro.getSourceProductId())) {
            List<ActivityProduct> aplist = activityProductMapper
                    .selectByproductId(pro.getSourceProductId());
            if (null != aplist && aplist.size() > 0) {
                for (ActivityProduct activityProduct : aplist) {
                    // 该商品在分销池为下架
                    if (activityProduct.getArchive()) {
                        flag = true;
                        break;
                    }
                }
            } else {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 选择商品为团购商品
     */
    @ResponseBody
    @RequestMapping(value = "product/groupon/save")
    @ApiIgnore
    public ResponseObject<Boolean> grouponSave(PromotionGrouponVO promotionGrouponVO) {
        Product product = productMapper.selectByPrimaryKey(promotionGrouponVO.getProductId());
        try {
            // 首先往活动主表中插入一条记录，然后往团购活动明细表中插入具体活动与商品关联关系数据
            Promotion promotion = new Promotion();
            promotion.setTitle("【团购】" + product.getName());
            promotion.setValidFrom(promotionGrouponVO.getValidFrom());
            promotion.setValidTo(promotionGrouponVO.getValidTo());
            promotion.setType(PromotionType.GROUPON);
            promotion.setArchive(false);
            promotionService.insert(promotion);

            // 团购活动明细表
            PromotionGroupon promotionGroupon = new PromotionGroupon();
            promotionGroupon.setProductId(product.getId());
            promotionGroupon.setDiscount(promotionGrouponVO.getDiscount());
            promotionGroupon.setShopid(product.getShopId());
            promotionGroupon.setNumbers(promotionGrouponVO.getNumbers());
            promotionGroupon.setAmount(promotionGrouponVO.getAmount());
            promotionGroupon.setSales(new BigDecimal("0"));
            promotionGroupon.setPromotionId(promotion.getId());
            promotionGroupon.setArchive(false);
            promotionGrouponService.insert(promotionGroupon);

            return new ResponseObject<>(true);
        } catch (Exception e) {
            log.error("save product groupon error.", e);
            return new ResponseObject(e);
        }
    }

    /**
     * 修改团购商品的团购相关信息
     */
    @ResponseBody
    @RequestMapping(value = "product/groupon/edit")
    @ApiIgnore
    public ResponseObject<Boolean> grouponEdit(PromotionGrouponVO promotionGroupon) {
        PromotionGroupon sourcePromotionGroupon = promotionGrouponService
                .selectByPrimaryKey(promotionGroupon.getId());
        try {
            if (sourcePromotionGroupon != null) {
                sourcePromotionGroupon.setDiscount(promotionGroupon.getDiscount());
                sourcePromotionGroupon.setNumbers(promotionGroupon.getNumbers());
                promotionGrouponService.modifyPromotionGroupon(sourcePromotionGroupon);
                promotionGrouponService.modifyPromotion(promotionGroupon);
            }
            return new ResponseObject<>(true);
        } catch (Exception e) {
            log.error("save product groupon error.", e);
            return new ResponseObject(e);
        }
    }

    /**
     * web客户端点击品牌团分类后查询分类和商品信息
     */
    @ResponseBody
    @RequestMapping("/product/list/forbrand")
    @ApiIgnore
    public ResponseObject<List<ForbrandListVO>> listForbrand(Pageable pageable,
                                                             @RequestParam(value = "shopId", required = false) String shopId,
                                                             @RequestParam(value = "categoryId", required = false) String categoryId) {
        List<ForbrandListVO> vos = new ArrayList<>();
        String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
        // 先分页查询品牌团下的分类
        List<Category> categorys = categoryService
                .listCategoriesByShopAndStatusPage(pageable, rootShopId, categoryId,
                        CategoryStatus.ONSALE);
        // 查询各个分类下面的两条商品数据
        for (Category category : categorys) {
            ForbrandListVO vo = new ForbrandListVO();
            vo.setCategoryId(category.getId());
            vo.setImg(category.getImg());
            List<Product> products = productMapper
                    .list2ProductsByCategoryId(category.getId(), rootShopId);
            List<ProductVOEx> imgproducts = generateImgUrl(products);
            vo.setList(imgproducts);
            vos.add(vo);
        }
        return new ResponseObject<>(vos);
    }

    /**
     * app端访问钻石专享,点击分类后展示此分类下所有专享商品页面
     */
    @ResponseBody
    @RequestMapping("/product/list/fordiamond")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> listFordiamond(Pageable pageable,
                                                              @RequestParam(value = "categoryId", required = false) String categoryId) {
        Map<String, Object> aRetMap = new HashMap<>();
        String shopId = getCurrentIUser().getShopId();
        String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
        List<PromotionDiamondProductVO> products = promotionDiamondService
                .listDiamondApp(rootShopId, categoryId, 0);
        aRetMap.put("list", products);
        return new ResponseObject<>(aRetMap);
    }

    @RequestMapping(value = "product/qrcode/{productId}", method = RequestMethod.GET, produces = "image/png")
    @ApiIgnore
    public @ResponseBody
    byte[] generateQrcode(@PathVariable("productId") String productId, HttpServletRequest req) {
        Product product = productService.load(productId);
        ArrayList names = new ArrayList();
        String productName = product.getName();
        if (productName.length() > 19) {
            names.add(productName.substring(0, 19));
            names.add(productName.substring(19, productName.length()));
        } else {
            names.add(productName);
        }
        String price = "¥" + product.getPrice().toPlainString();
        String priceTitle = "扫描二维码查看详情";
        String img = product.getImg();
        ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
                .getBean("resourceFacade");
        String imgUrl = resourceFacade.resolveUrl(img);
        int width = 840;
        int height = 1401;
        int imgW = 840;
        int imgH = 840;
        BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
        BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, imgW, imgH);
        BufferedImage ImageQrcode = null;
        HashMap<EncodeHintType, Object> hints = new HashMap<>();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
        hints.put(EncodeHintType.MARGIN, 1);

        BitMatrix byteMatrix;
        try {
            byteMatrix = new MultiFormatWriter()
                    .encode(getShareUrl(req, product.getCode()), BarcodeFormat.QR_CODE, 300, 300,
                            hints);
            ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Font font = new Font("宋体", Font.PLAIN, 40);
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) bi.getGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
                RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2.setBackground(Color.white);
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.black);
        g2.setFont(font);

        for (int i = 0; i < names.size(); i++) {
            g2.drawString((String) names.get(i), width / 16, (40 * i) + height / 16);
        }
        g2.drawImage(imageImgResize, 0, height / 8, null);
        g2.drawImage(ImageQrcode, width / 16, height / 8 + imageImgResize.getHeight(), null);
        g2.drawString(price, width / 16 + ImageQrcode.getWidth() + 20,
                height / 8 + imageImgResize.getHeight() + ImageQrcode.getHeight() / 2);
        g2.drawString(priceTitle, width / 16 + ImageQrcode.getWidth() + 20,
                height / 8 + imageImgResize.getHeight() + ImageQrcode.getHeight() / 2 + 60);

        ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
        try {
            ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
        } catch (IOException e) {
            log.error("generate png error", e);
            throw new RuntimeException(e);
        }

        return os.toByteArray();//从流中获取数据数组。

    }

    private String getShareUrl(HttpServletRequest req, String code) {
        String key = tinyUrlService
                .insert(req.getScheme() + "://" + req.getServerName() + "/p/" + code
                        + "?currentSellerShopId=" + getCurrentUser().getShopId());
        return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
    }

    /**
     * 获取商品的所有图片
     */
    @ResponseBody
    @RequestMapping("/product/listImgs")
    @ApiIgnore
    public ResponseObject<Map> listImgs(
            @RequestParam(value = "productId", required = true) String productId) {
        HashMap map = new HashMap();
        Product product = productService.load(productId);
        List<ProductImageVO> vos = new ArrayList<>();
        List<ProductImage> list = productImageMapper.selectByProductId(productId);
        for (ProductImage img : list) {
            ProductImageVO vo = new ProductImageVO();
            BeanUtils.copyProperties(img, vo);
            vo.setImgUrl(img.getImg());
            vos.add(vo);
        }
        map.put("product", product);
        map.put("imgs", vos);
        return new ResponseObject<Map>(map);
    }

    /**
     * 爆款商品列表
     */
    @ResponseBody
    @RequestMapping("/product/topList")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> productList(Pageable pageable) {
        Map<String, Object> params = new HashMap<>();
        String shopId = getCurrentIUser().getShopId();
        params.put("shopId", shopId);
        List<ProductTopVO> result = productTopService.list(pageable, params);
        Long total = productTopService.selectCnt(params);
        Map<String, Object> aRetMap = new HashMap<>();
        aRetMap.put("total", total);
        aRetMap.put("list", result);
        return new ResponseObject<>(aRetMap);

    }

    /**
     * 删除爆款商品
     */
    @ResponseBody
    @RequestMapping("/product/deleteTop/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> deleteProductCommission(@PathVariable String id) {
        int result = productTopService.deleteForArchive(id);
        return new ResponseObject<>(result > 0);
    }


    /**
     * 删除改变商品的审核状态
     */
    @ResponseBody
    @RequestMapping("/product/review/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> updateReviewStatus(@PathVariable("id") String id,
                                                      @RequestParam ProductReviewStatus status) {
        boolean result = productService.updateReviewStatus(id, status);
        return new ResponseObject<>(result);
    }


    /**
     * 查询是否已经有该商品已经设置过爆款商品
     */
    @ResponseBody
    @RequestMapping("/product/checkTop")
    @ApiIgnore
    public ResponseObject<Boolean> checkProductCommission(
            @RequestParam("productId") String productId, @RequestParam("id") String id) {
        String shopId = getCurrentIUser().getShopId();
        long count = productTopService.selectByProductId(shopId, productId, id);
        if (count > 0) {
            return new ResponseObject<>(true);
        } else {
            return new ResponseObject<>(false);
        }
    }

    /**
     * 保存爆款商品
     */
    @ResponseBody
    @RequestMapping("/product/saveTop")
    @ApiIgnore
    public ResponseObject<Boolean> saveTop(@RequestParam("id") String id,
                                           @RequestParam("productId") String productId, @RequestParam("logo") String logo,
                                           @RequestParam(value = "idx", required = false) String idx) {
        String shopId = getCurrentIUser().getShopId();
        int result = 0;
        if (StringUtils.isNotEmpty(id)) {
            ProductTop commission = new ProductTop();
            commission.setId(id);
            commission.setProductId(productId);
            commission.setLogo(logo);
            result = productTopService.update(commission);
        } else {
            ProductTop commission = new ProductTop();
            commission.setShopId(shopId);
            commission.setProductId(productId);
            commission.setArchive(false);
            commission.setLogo(logo);
            commission.setIdx(new BigDecimal("0").intValue());
            result = productTopService.insert(commission);
        }
        if (result == 1) {
            return new ResponseObject<>(true);
        } else {
            return new ResponseObject<>(false);
        }
    }


    /**
     * 特权码商品列表
     */
    @ResponseBody
    @RequestMapping("/product/privilegeList")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> privilegeList(Pageable pageable) {
        Map<String, Object> params = new HashMap<>();
        String shopId = getCurrentIUser().getShopId();
        params.put("shopId", shopId);
        List<PrivilegeProductVO> result = privilegeProductService.list(pageable, params);
        Long total = productTopService.selectCnt(params);
        Map<String, Object> aRetMap = new HashMap<>();
        aRetMap.put("total", total);
        aRetMap.put("list", result);
        return new ResponseObject<>(aRetMap);

    }

    /**
     * 删除特权码商品
     */
    @ResponseBody
    @RequestMapping("/product/deletePrivilege/{id}")
    @ApiIgnore
    public ResponseObject<Boolean> deletePrivilege(@PathVariable String id) {
        int result = privilegeProductService.deleteForArchive(id);
        return new ResponseObject<>(result > 0);
    }

    /**
     * 查询是否已经有该商品已经设置过特权码商品
     */
    @ResponseBody
    @RequestMapping("/product/checkPrivilege")
    @ApiIgnore
    public ResponseObject<Boolean> checkPrivilege(
            @RequestParam("productId") String productId, @RequestParam("id") String id) {
        String shopId = getCurrentIUser().getShopId();
        long count = privilegeProductService.selectByProductId(shopId, productId, id);
        if (count > 0) {
            return new ResponseObject<>(true);
        } else {
            return new ResponseObject<>(false);
        }
    }

    /**
     * 保存特权码商品
     */
    @ResponseBody
    @RequestMapping("/product/savePrivilege")
    @ApiIgnore
    public ResponseObject<Boolean> savePrivilege(@RequestParam("id") String id,
                                                 @RequestParam("productId") String productId, @RequestParam("logo") String logo,
                                                 @RequestParam(value = "idx", required = false) String idx) {
        String shopId = getCurrentIUser().getShopId();
        int result = 0;
        if (StringUtils.isNotEmpty(id)) {
            PrivilegeProduct commission = new PrivilegeProduct();
            commission.setId(id);
            commission.setProductId(productId);
            commission.setLogo(logo);
            result = privilegeProductService.update(commission);
        } else {
            PrivilegeProduct commission = new PrivilegeProduct();
            commission.setShopId(shopId);
            commission.setProductId(productId);
            commission.setArchive(false);
            commission.setLogo(logo);
            commission.setIdx(new BigDecimal("0").intValue());
            result = privilegeProductService.insert(commission);
        }
        if (result == 1) {
            return new ResponseObject<>(true);
        } else {
            return new ResponseObject<>(false);
        }
    }

    /**
     * 单个解除绑定
     *
     * @param productId 商品id
     * @param filterId  滤芯id
     * @return 解绑是否成功
     */
    @RequestMapping(value = "/product/filter/unbind")
    @ResponseBody
    public ResponseObject<Boolean> unBindFilter(@RequestParam String productId,
                                                @RequestParam String filterId) {
        boolean result = productService
                .unBindFilterIfBounded(new ProductFilter(productId, filterId.trim()));
        return new ResponseObject<>(result);
    }

    /**
     * 单个绑定
     *
     * @param productId 商品id
     * @param filterId  滤芯id
     * @return 解绑是否成功
     */
    @RequestMapping(value = "/product/filter/bind")
    @ResponseBody
    public ResponseObject<Boolean> bindFilter(@RequestParam String productId,
                                              @RequestParam String filterId) {
        boolean result = productService
                .bindFilterIfNotBounded(new ProductFilter(productId, filterId.trim()));
        return new ResponseObject<>(result);
    }

    /**
     * 滤芯绑定
     *
     * @param form 绑定表单对象
     * @return true or false
     */
    @RequestMapping(value = "/product/filter/bind", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseObject<Boolean> bindFilterAll(@RequestBody @Validated FilterBindForm form,
                                                 Errors errors) {
        ControllerHelper.checkException(errors);
        String pId = form.getProductId();
        boolean isUnBind = form.getIsUnBind();
        List<String> fIds = form.getFilterIds();
        boolean ret;
        if (isUnBind) {
            ret = productService.unBindFilterIfBounded(pId, fIds);
        } else {
            ret = productService.bindFilterIfNotBounded(pId, fIds);
        }
        return new ResponseObject<>(ret);
    }

    @RequestMapping(value = "/product/filter/listSelect")
    public ResponseObject<Map<String, ?>> listFilterSelect(String pId) {
        return null;
    }

    @RequestMapping(value = "/product/sf/pull")
    @ResponseBody
    public ResponseObject<Boolean> pullSFProduct(Integer limit) {
        sfProductService.pullProducts(limit);
        return new ResponseObject<>(Boolean.TRUE);
    }

    @ResponseBody
    @RequestMapping(value = "/product/sf/pullDetail")
    public ResponseObject<Boolean> pullDetails(String encode) {
        sfProductService.pullProductDetails(encode);
        return new ResponseObject<>(Boolean.TRUE);
    }

    @RequestMapping(value = "/product/sf/pullByEncode")
    @ResponseBody
    public ResponseObject<Boolean> pullByEncode(String encode) {
        sfProductService.pullProductByEncode(encode);
        return new ResponseObject<>(Boolean.TRUE);
    }

    /**
     * 顺丰推送消息请求接口 -- 订单拆单接口
     * @param pushSplitOrder
     * @return
     */
    @RequestMapping(value = "/product/sf/pushSplitOrder", method = RequestMethod.POST)
    @ResponseBody
    public String splitOrder(@RequestBody PushSplitOrder pushSplitOrder) {
        log.info("splitOrder回调接口被调用：{}",JSON.toJSONString(pushSplitOrder));
        String result = sfMessageService.splitOrder(pushSplitOrder);
        log.info("splitOrder回调接口被调用保存的结果：{}",result);
        return result;
    }

    /**
     * 顺丰推送消息请求接口 --订单发货、签收接口
     * @param pushShipmentNum
     * @return
     */
    @RequestMapping(value = "/product/sf/pushShipmentNumber", method = RequestMethod.POST)
    @ResponseBody
    public String shipmentNumber(@RequestBody PushShipmentNum pushShipmentNum) {
        log.info("shipmentNumber回调接口被调用：{}",JSON.toJSONString(pushShipmentNum));
        String result = sfMessageService.shipmentNumber(pushShipmentNum);
        log.info("shipmentNumber回调接口被调用保存的结果：{}",result);
        return result;
    }

    /**
     * 顺丰推送消息请求接口 --商品修改及上下架接口
     * @param pushProductMsg
     * @return
     */
    @RequestMapping(value = "/product/sf/pushUpdateProductMsg", method = RequestMethod.POST)
    @ResponseBody
    public String updateProductMsg(@RequestBody PushProductMsg pushProductMsg) {
        log.info("updateProductMsg回调接口被调用：{}",JSON.toJSONString(pushProductMsg));
        String result = sfMessageService.updateProductMsg(pushProductMsg);
        log.info("updateProductMsg回调接口被调用保存的结果：{}",result);
        return result;
    }

    @RequestMapping(value = "/product/sku/exit/{code} ", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Boolean> ifSkuCodeExit(@PathVariable("code") @NotBlank String code) {
        int count = skuMapper.countBySkuCode(code);
        Boolean ret = count > 0;
        return new ResponseObject<>(ret);
    }

    @RequestMapping("/product/productForSale")
    @ResponseBody
    public ResponseObject<Void> productForSale(String id, Date time) {
        ResponseObject<Void> responseObject = new ResponseObject<>();
        Integer num = 0;
        if (id != null && time != null) {
            num = productService.productForSale(id, time);
        }
        if (num > 0) {
            responseObject.setMoreInfo("上架成功");
        } else {
            responseObject.setMoreInfo("上架失败");
        }
        return responseObject;
    }

    @RequestMapping("/product/hotProduct")
    @ResponseBody
    public ResponseObject<Map> hotProduct(Pageable page, String category, String brand,
                                          String supplier, String keyword, String productId, String idsToS) {
        Map map = new HashMap();
        List<String> ids = new ArrayList<>();
        if (StringUtils.isNotBlank(idsToS)) {
            String[] sId = idsToS.split(",");
            for (int i = 0; i < sId.length - 1; i++) {
                ids.add(sId[i]);
            }
            if (ids.isEmpty()) {
                map.put("categoryTotal", 0);
                map.put("list", new ArrayList<>());
                return new ResponseObject(map);
            }
        }
        List<HotProduct> hotProducts = productMapper
                .getHotProduct(page, category, brand, supplier, keyword, productId, ids);
        Integer total = productMapper.countHotProduct();
        for (HotProduct h : hotProducts) {
            h.setId(IdTypeHandler.encode(Long.parseLong(h.getId())));
        }
        map.put("categoryTotal", total);
        map.put("list", hotProducts);
        return new ResponseObject(map);
    }

    @RequestMapping("/product/getHotLimit")
    @ResponseBody
    public ResponseObject<Integer> getLimit() {
        Integer limit = productMapper.getHotSearchKeyDisplayLimit();
        return new ResponseObject<>(limit);
    }

    @RequestMapping("/product/getNumOfHot")
    @ResponseBody
    public ResponseObject<Integer> getNum() {
        Integer number = productMapper.getNumOfHot();
        return new ResponseObject<>(number);
    }

    /**
     * 校验顺丰商品是否售罄
     *
     * @param id
     * @param province
     * @return
     */
    @RequestMapping("/product/getSfSoldOut/{id}")
    @ResponseBody
    public ResponseObject<SfStockVO> getSfSoldOut(@PathVariable("id") String id, String province
            , String city, String region) {
        ResponseObject<SfStockVO> ro = new ResponseObject<SfStockVO>();

        if (StringUtils.isBlank(province)) {
            ro.setMoreInfo("字段province不能为空！");
            ro.setData(null);
            return ro;
        }

        List<SfStockVO> list = this.productService.checkSfStock(id, province, city, region);
        if (list == null || list.isEmpty()) {
            ro.setData(null);
            return ro;
        }

        ro.setData(list.get(0));
        return ro;
    }

    /**
     * 转换商品活动信息
     */
    private void convertPromotionInfo(String productId, ProductVOEx result, String pCode) {
        if (StringUtils.isEmpty(productId)) {
            return;
        }
        //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
        boolean isSoldOut = false;
        String skuCode = promotionSkusService.querySkuCodeByProductid(productId, pCode);
        PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);
        List<String> skuids = new ArrayList<>();
        if (StringUtils.isNotEmpty(skuCode)) {
            Sku sku = skuMapper.selectBySkuCode(skuCode);
            if (sku != null) {
                skuids.add(sku.getId());
            }
        }
        List<String> promotionSkuIds = null;
        if (CollectionUtils.isNotEmpty(skuids)) {
            promotionSkuIds = getPromotionSkuIds(skuids);
        }
        if (CollectionUtils.isNotEmpty(promotionSkuIds)) {
            boolean hasEnoughStock = hasEnoughStock(promotionSkuIds);
            if (!hasEnoughStock) {
                isSoldOut = true;
            }
        }
        if (baseInfo.getEffectTo().before(new Date())) {
            isSoldOut = true;
        }
        //设置是否需要邀请码
        //设置是否是大会商品
        boolean isCode = false;
        boolean isMeetingProduct = false;

        if (null != baseInfo) {
            isMeetingProduct = true;
            String configType = baseInfo.getpType();
            String configName = "isCode";
            Map map = new HashMap();
            map.put("configType", configType);
            map.put("configName", configName);
            PromotionConfig promotionConfig = promotionConfigService.selectByConfig(map);
            if (promotionConfig != null) {
                if ("on".equals(promotionConfig.getConfigValue())) {
                    isCode = true;
                }
            }
            result.setIsCode(isCode);
            result.setIsMeetingProduct(isMeetingProduct);

        }
        //杭州大会设置有无库存
        result.setIsSoldOut(isSoldOut);
        //商品详情页 已售罄改为 “即将到货 ”,活动可配
        result.setSoldOutTitle("即将到货");
        if (isSoldOut) {
            String configType = baseInfo.getpType();
            String configName = "SOLD_OUT_TITLE";
            Map map = new HashMap();
            map.put("configType", configType);
            map.put("configName", configName);
            PromotionConfig promotionConfig = promotionConfigService.selectByConfig(map);
            if (promotionConfig != null) {
                if (promotionConfig.getConfigValue() != null) {
                    result.setSoldOutTitle(promotionConfig.getConfigValue());
                }
            }
        }

        //String skuCode = promotionSkusService.querySkuCodeByProductid(productId,pCode);
        //为提交订单接口准备数据：参加大会售卖活动的skuId
//    Sku sku = skuMapper.selectByPrimaryKey(productId);
        //检查这个商品有无活动，没有则认为是没有赠品
        //List<PromotionSkuVo> promotionSkuVos = promotionSkusService.getPromotionSkus(skuCode);

//    List<PromotionBaseInfo> plst = promotionBaseInfoService.selectBySkuCode(skuCode);
//    if (CollectionUtils.isEmpty(plst)) {
//      return;
//    }
//    PromotionBaseInfo promotionBaseInfo = plst.get(0);
        if (null != baseInfo) {
            Date effectTo = baseInfo.getEffectTo();
            Date effectFrom = baseInfo.getEffectFrom();
            if (null != effectTo) {
                result.setEffectTo(effectTo);
            }
            if (null != effectFrom) {
                result.setEffectFrom(effectFrom);
            }
        }

        //检查这个活动商品有没有赠品，
        List<PromotionSkuGift> giftList = promotionSkuGiftService
                .getPromotionSkuGiftList(pCode, skuCode);
        if (CollectionUtils.isEmpty(giftList)) {
            // FIXME 临时处理抢购前端显示问题
            result.setGiftType("新春特惠");
            return;
        }
        //step1. 设置有无赠品
        result.setHadGift(true);

        String giftType = "";
        //设置买赠信息
        PromotionSkuVo promotionSkuVo = promotionSkusService
                .selectProSkuBySkuCodeAndPcode(skuCode, pCode);
        if (null == promotionSkuVo) {
            return;
        }
        //设置购买数量才赠送
        result.setGiftCount(promotionSkuVo.getGift());
        if (promotionSkuVo.getGift() == 1) {
            giftType = "活动专享 买一赠一";
        } else if (promotionSkuVo.getGift() == 2) {
            giftType = "活动专享 买二赠一";
        } else if (promotionSkuVo.getGift() == 3) {
            giftType = "活动专享 买三赠一";
        }
        result.setGiftType(giftType);
    }

    /**
     * 过滤活动商品
     */
    private List<String> getPromotionSkuIds(List<String> skuIds) {
        List<String> promotionSkuIds = new ArrayList<String>();
        for (String skuId : skuIds) {
            Sku sku = skuMapper.selectByPrimaryKey(skuId);
            SkuExtend skuEx = new SkuExtend();
            BeanUtils.copyProperties(sku, skuEx);

            //检查这个商品有无活动，没有则认为是没有赠品
            List<PromotionSkuVo> promotionSkuVos = promotionSkusService
                    .getPromotionSkus(skuEx.getSkuCode());
            if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionSkuVos)) {
                continue;
            }
            promotionSkuIds.add(sku.getId());
        }
        return promotionSkuIds;
    }


    /**
     * 检查是否有足够的活动库存
     */
    public boolean hasEnoughStock(List<String> promotionSkuIds) {

        boolean isEnough = promotionSkusService.hasEnoughStock(promotionSkuIds);
        return isEnough;
    }

    @Autowired
    private PromotionBaseInfoService promotionBaseInfoService;
}
