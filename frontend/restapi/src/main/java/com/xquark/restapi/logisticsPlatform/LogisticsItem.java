package com.xquark.restapi.logisticsPlatform;

/**
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
public class LogisticsItem {

    private Integer quantity;
    private String skuOuterID;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSkuOuterID() {
        return skuOuterID;
    }

    public void setSkuOuterID(String skuOuterID) {
        this.skuOuterID = skuOuterID;
    }
}
