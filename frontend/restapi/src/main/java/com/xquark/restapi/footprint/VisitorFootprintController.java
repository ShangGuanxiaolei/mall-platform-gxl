package com.xquark.restapi.footprint;

import com.xquark.dal.model.*;
import com.xquark.dal.page.PageHelper;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.footprint.FootprintService;
import com.xquark.service.platform.CareerLevelService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *@ClassName VisitorFootprintController
 *@Description 遊客足跡相关接口控制中心
 *@Date 2018/11/14
 *@Version 1.0
 **/
@RestController
@RequestMapping("/footprint")
public class VisitorFootprintController extends BaseController {

    private static final Log logger = LogFactory.getLog(VisitorFootprintController.class);

    @Autowired
    private FootprintService footprintService;
    @Autowired
    private CareerLevelService careerLevelService;

    private static final String IDENTITY_SP = "SP";


    /*
     * 功能描述:根据提供的日期显示该用户下所有的顾客足迹信息
     * @author Kwonghom
     * @date 2018/11/14
     * @param []
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/showCustomerListByDate", method = RequestMethod.POST)
    public ResponseObject<Map<String, List<VisitorInfo>>> showCustomerListByDate(
            @RequestParam(value ="cpId",required = false) String cpId,
            @RequestParam("date") String date,
            @RequestParam("isToday") Boolean isToday) {
        User user = (User) getCurrentIUser();
        Map<String, List<VisitorInfo>> customerList = new LinkedHashMap<>();
        customerList = footprintService.customerList(String.valueOf(user.getCpId()), date, isToday);
//        customerList = footprintService.customerList("1022462", date, isToday);
        return new ResponseObject<>(customerList);
    }

    @ResponseBody
    @RequestMapping(value = "/showVisitorsByDate", method = RequestMethod.POST)
    public ResponseObject<Map<String, List<VisitorInfo>>> showVisitorsByDate(
            @RequestParam("date") String date,
            @RequestParam("isToday") Boolean isToday) {
        User user = (User) getCurrentIUser();
        Map<String, List<VisitorInfo>> visitorMap =
                footprintService.visitorList(user.getCpId().toString(), date, isToday);
//                footprintService.visitorList("1000100", date, isToday);
        return new ResponseObject<>(visitorMap);
    }

    @ResponseBody
    @RequestMapping(value = "/visitorDateMarked", method = RequestMethod.POST)
    public ResponseObject<List<String>> visitorDateMarked(@RequestParam("month")String monthStr){
        User currentIUser = (User)getCurrentIUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Date month;
        try {
            month = sdf.parse(monthStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return new ResponseObject("时间格式转换异常", GlobalErrorCode.UNKNOWN);
        }
        List<String> marketDates = footprintService.visitorDateMarked(month, currentIUser);
        return new ResponseObject(marketDates);
    }

    /**
     * 功能描述:展示顾客详情(常用收件人，总收益)
     * @author lxl
     * @date 2018/11/14
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getUserDetail", method = RequestMethod.POST)
    public ResponseObject<VisitorInfo> getUserDetail(@RequestParam("cpId")String cpId) {
        User currentIUser = (User)getCurrentIUser();
        CustomerCareerLevel level = careerLevelService.load(currentIUser.getCpId());
        if (null == level) {
            logger.info("==================当前用户查不到对应的身份数据：cpId=" + currentIUser.getCpId());
            return new ResponseObject<>("找不到用户[" + currentIUser.getCpId() + "]的身份数据", GlobalErrorCode.NOT_FOUND);
        }
        logger.info(
                "=================upgrading: 获取当前用户身份：hdsType=" + level.getHdsType() + ", vvType=" + level
                        .getViviLifeType());
        //是否查服务费
        Boolean hasServerCharge= false;
        //  TODO
        if (IDENTITY_SP.equals(level.getHdsType())
                || IDENTITY_SP.equals(level.getViviLifeType())) {
            hasServerCharge= true;

        }

        //根据cpid查询顾客详情
        VisitorInfo visitorInfo = footprintService.queryConsumerDetail(cpId,hasServerCharge,currentIUser);

        return new ResponseObject(visitorInfo);
    }

    /**
     * 根据传入的月份 给日期控件标记小红点
     * @date 2018/11/14
     * @author lxl
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/dateMarkedByMonth", method = RequestMethod.POST)
    public ResponseObject<List<String>> dateMarkedByMonth(@RequestParam("month")String monthStr){
        User currentIUser = (User)getCurrentIUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Date month = new Date();
        try {
            month = sdf.parse(monthStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return new ResponseObject("时间格式转换异常", GlobalErrorCode.UNKNOWN);
        }
        List<String> marketDates = footprintService.dateMarkedByMonth(month, currentIUser);
        return new ResponseObject(marketDates);
    }

    /**
     *根据用户id查询用户购买的订单list
     */
    @ResponseBody
    @RequestMapping("/footOrder")
    public PageHelper<FootOrder> selectOrderInfoByCpIdAndOrderNo(@RequestParam("cpId") String cpId,@RequestParam(value="paidAt", required = false) String paidAt, @RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
        User user= (User) getCurrentIUser();
        PageHelper<FootOrder> list=footprintService.selectOrderInfoByCpIdAndOrderNo(Long.valueOf(cpId),user.getCpId(),paidAt,page,pageSize);
        if (null==list && "".equals(list)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "当前用户没有订单哦~");
        }
        for (int i = 0; i <list.getResult().size() ; i++) {
            List<FootOrderItem> footOrders=footprintService.selectOrderProductInfoByCpIdAndOrderNo(Long.valueOf(cpId),list.getResult().get(i).getOrderNo());
            int num=0;
            BigDecimal price = new BigDecimal(0);
            BigDecimal totalPrice=new BigDecimal(0);
            for (int j = 0; j < footOrders.size(); j++) {
                num+=footOrders.get(j).getAmount();
                price=price.add(footOrders.get(j).getPrice());
                totalPrice=totalPrice.add(footOrders.get(j).getTotalPrice());
            }
            list.getResult().get(i).setTotalPrice(totalPrice);
            list.getResult().get(i).setFootOrderItems(footOrders);
            list.getResult().get(i).setTotalNum(num);
        }

        return list;
    }


}
