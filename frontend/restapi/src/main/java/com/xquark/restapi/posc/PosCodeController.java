package com.xquark.restapi.posc;

import com.xquark.restapi.ResponseObject;
import com.xquark.service.posc.PosCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangxinhua.
 * @date 2018/9/28
 */
@RestController
@RequestMapping("/openapi/pos")
public class PosCodeController {

  private final PosCodeService posCodeService;

  @Autowired
  public PosCodeController(PosCodeService posCodeService) {
    this.posCodeService = posCodeService;
  }

  @RequestMapping("/isexist")
  public ResponseObject<Boolean> isSnCodeExists(@RequestParam String snCode) {
    Boolean ret = posCodeService.isSnCodeExists(snCode);
    return new ResponseObject<>(ret);
  }

}
