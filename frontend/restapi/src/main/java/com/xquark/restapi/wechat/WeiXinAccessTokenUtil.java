package com.xquark.restapi.wechat;

/** Created by root on 17-9-23. */

import com.alibaba.fastjson.JSONObject;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 获取微信access_token工具类 <一句话功能简述>
 *
 * @author xlp
 * @version [V1.00, 2016年10月22日]
 * @see [相关类/方法]
 * @since V1.00
 */
public class WeiXinAccessTokenUtil {
  private static final Logger DEBUG_LOGGER = Logger.getLogger(WeiXinAccessTokenUtil.class);

  private static final String WX_CODE_UN_LIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit";

  private static final String WX_CODE_QR_CODE = "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode";


  /**
   * 调用微信无限获取二维码接口
   */
  public static String getQRCodeUnLimit(String accessToken, Map<String, Object> params) {
    HttpResponse res =
        PoolingHttpClients.postJsonResponse(WX_CODE_UN_LIMIT + "?access_token=" + accessToken, params);
    try {
      String qrCodeFromResponse = getQRCodeFromResponse(res);
      EntityUtils.consume(res.getEntity());
      return qrCodeFromResponse;
    } catch (IOException e) {
      DEBUG_LOGGER.error("从响应中获取qrCode错误", e);
    }
    return null;
  }


  /**
   * 调用微信无限获取二维码接口
   */
  public static String getWxQRCode(String accessToken, Map<String, Object> params) {
    HttpResponse res =
            PoolingHttpClients.postJsonResponse(WX_CODE_QR_CODE + "?access_token=" + accessToken, params);
    try {
      String qrCodeFromResponse = getQRCodeFromResponse(res);
      EntityUtils.consume(res.getEntity());
      return qrCodeFromResponse;
    } catch (IOException e) {
      DEBUG_LOGGER.error("从响应中获取qrCode错误", e);
    }
    return null;
  }


  /**
   * 获取微信access_token <功能详细描述>
   *
   * @param appid
   * @param secret
   * @return
   * @see [类、类#方法、类#成员]
   */
  public static GetAccessTokenRsp getWeiXinAccessToken(String appid, String secret) {
    if (StringUtils.isEmpty(appid) || StringUtils.isEmpty(secret)) {
      DEBUG_LOGGER.error("appid or secret is null");
      return null;
    }
    GetAccessTokenRsp getAccessTokenRsp = new GetAccessTokenRsp();
    try {
      String url =
          "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
              + appid
              + "&secret="
              + secret;
      HttpClient httpClient = new HttpClient();
      GetMethod getMethod = new GetMethod(url);
      int execute = httpClient.executeMethod(getMethod);
      System.out.println("execute:" + execute);
      String getResponse = getMethod.getResponseBodyAsString();
      JSONObject result = JSONObject.parseObject(getResponse);
      getAccessTokenRsp.setAccessToken(result.getString("access_token"));
    } catch (IOException e) {
      DEBUG_LOGGER.error("getAccessToken failed,desc:::" + e);
      e.printStackTrace();
    }
    System.out.println(getAccessTokenRsp);
    return getAccessTokenRsp;
  }

  /**
   * 二进制流转Base64字符串
   *
   * @param data 二进制流
   * @return data
   * @throws IOException 异常
   */
  public static String getImageString(byte[] data) throws IOException {
    BASE64Encoder encoder = new BASE64Encoder();
    return data != null ? encoder.encode(data) : "";
  }

  /**
   * 获取对应销售人员的小程序二维码 <功能详细描述>
   *
   * @param path
   * @param token
   * @return
   * @see [类、类#方法、类#成员]
   */
  public static String getQrcode(Qiniu qiniu, String path, String token) {
    String qrcode = "";
    try {
      String url = "https://api.weixin.qq.com/wxa/getwxacode?access_token=" + token;
      //            String url =
      // "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=" + token;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("path", path); // 你二维码中跳向的地址
      map.put("width", "200"); // 图片大小
      JSONObject json = JSONObject.parseObject(JSONObject.toJSONString(map));
      DefaultHttpClient httpClient = new DefaultHttpClient();
      HttpPost httpPost = new HttpPost(url);
      httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");

      StringEntity se = new StringEntity(json.toString());
      se.setContentType("application/json");
      se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "UTF-8"));
      httpPost.setEntity(se);
      // httpClient.execute(httpPost);
      HttpResponse response = httpClient.execute(httpPost);
      qrcode = getQRCodeFromResponse(response);
      httpPost.abort();
    } catch (IOException e) {
      DEBUG_LOGGER.error("getAccessToken failed,desc:::" + e);
      e.printStackTrace();
    }
    return qrcode;
  }

  private static String getQRCodeFromResponse(HttpResponse response) throws IOException {
    String qrCode = null;
    if (response == null) {
      return null;
    }
    HttpEntity resEntity = response.getEntity();
    if (resEntity != null) {
      InputStream is;
      is = resEntity.getContent();
      byte[] temp = StreamUtils.copyToByteArray(is);
      qrCode = getImageString(temp);
      qrCode = "data:image/png;base64," + qrCode.replaceAll("\\n", "");
    }
    return qrCode;
  }
}
