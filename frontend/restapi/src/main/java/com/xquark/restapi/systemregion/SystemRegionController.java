package com.xquark.restapi.systemregion;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.model.SystemRegion;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.systemRegion.SystemRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * User: a9175
 * Date: 2018/6/20.
 * Time: 11:32
 */
@Controller
@Api(value = "systemRegion", description = "地区管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class SystemRegionController extends BaseController {
    @Autowired
    private SystemRegionService SystemRegionService;

    @ResponseBody
    @RequestMapping(value = "/systemRegion/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "获取某个id对应的地区信息", notes = "获取某个id对应的地区信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<SystemRegion> view(@PathVariable String id) {
        return new ResponseObject<>(SystemRegionService.load(id));
    }

    @ResponseBody
    @RequestMapping("/systemRegion/roots")
    @ApiIgnore
    public ResponseObject<List<SystemRegion>> roots() {
        return new ResponseObject<>(SystemRegionService.listRoots());
    }

    @ResponseBody
    @RequestMapping(value = "/systemRegion/{id}/parent", method = RequestMethod.POST)
    @ApiOperation(value = "获取某个id对应的地区的直接上级地区", notes = "获取某个id对应的地区的直接上级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<SystemRegion> parent(@PathVariable String id) {
        return new ResponseObject<>(SystemRegionService.findParent(id));
    }

    /**
     * 获取zoneId的所有父级
     */
    @ResponseBody
    @RequestMapping(value = "/systemRegion/{id}/parents", method = RequestMethod.POST)
    @ApiOperation(value = "获取某个id对应的地区的所有上级地区", notes = "获取某个id对应的地区的所有上级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<SystemRegion>> parents(@PathVariable String id) {
        return new ResponseObject<>(SystemRegionService.listParents(id));
    }

    @ResponseBody
    @RequestMapping(value = "/systemRegion/{id}/children", method = RequestMethod.POST)
    @ApiOperation(value = "获取某个id对应的地区的所有下级地区", notes = "获取某个id对应的地区的所有下级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<SystemRegion>> children(@PathVariable String id) {
        return new ResponseObject<>(SystemRegionService.listChildren(id));
    }

    @ResponseBody
    @RequestMapping("/systemRegion/{id}/update-path")
    @ApiIgnore
    public ResponseObject<Boolean> updatePath(@PathVariable String id) {   //没有path 此功能未实现
        SystemRegionService.updateZonePath(id);
        return new ResponseObject<>(true);
    }
}
