package com.xquark.restapi.Combination;

import com.xquark.dal.model.Combination;
import com.xquark.dal.type.CombinationStatus;
import com.xquark.dal.vo.CombinationVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.combination.CombinationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: byy Date: 18-6-1. Time: 下午8:15 组合商品控制器
 */
@RestController
public class CombinationController {

  @Autowired
  private CombinationService combinationService;

  /**
   * 添加
   */
  @RequestMapping(value="/combination/insert")
  public ResponseObject<Boolean> insert(@Valid Combination combination){
    boolean isSuccess =combinationService.insert(combination);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 通过id查找
   */
  @RequestMapping(value="/combination/getByKey")
  public ResponseObject<Combination> getByPrimaryKey(String id){
    Combination combination=combinationService.getByPrimaryKey(id);
    return new ResponseObject<>(combination);
  }

  /**
   * 更新
   */
  @RequestMapping(value="/combination/update")
  public ResponseObject<Boolean> update(@Valid Combination combination){
    Boolean isSuccess=combinationService.update(combination);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 通过id删除
   */
  @RequestMapping(value="/combination/delete")
  public ResponseObject<Boolean> update(String id){
    Boolean isSuccess=combinationService.delete(id);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 批量删除
   */
  @RequestMapping(value="/combination/batchDelete")
  public ResponseObject<Boolean> batchDelete(List<String> ids){
    Boolean isSuccess=combinationService.batchDelete(ids);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 计算数据的数量
   */
  @RequestMapping(value="/combination/count")
  public ResponseObject<Long> count(SearchForm searchForm, Pageable page){
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    Long num=combinationService.count(params);
    return new ResponseObject<>(num);
  }

  /**
   * 分页查询
   */
  @RequestMapping(value="/combination/list")
  public ResponseObject<Map<String,Object>> list(SearchForm searchForm, Pageable page){
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    List<Combination> combinations=combinationService.list(page,params);
    Long num=combinationService.count(params);
    Map<String,Object> listAndNum=new HashMap<String, Object>();
    listAndNum.put("combinations",combinations);
    listAndNum.put("num",num);
    return new ResponseObject<>(listAndNum);
  }

  /**
   * 根据id，关键字，分页查找商品信息
   * @param id
   * @return
   */
  @RequestMapping(value="/combination/getByCombinationId")
  public ResponseObject<Map<String,Object>> getByCombinationId(@RequestParam("id") String id, SearchForm searchForm, Pageable page){
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    CombinationVO combinationVOS=combinationService.getVOByPrimaryId(id,page,params);
    Long num=combinationService.count(params);
    Map<String,Object> listAndNum=new HashMap<String, Object>();
    listAndNum.put("combinationVOS",combinationVOS);
    listAndNum.put("num",num);
    return new ResponseObject<>(listAndNum);
  }

  /**
   * 根据组合id改变组合状态
   * @param status
   * @param id
   * @return
   */
  @RequestMapping(value="/combination/updateStatus")
  public ResponseObject<Boolean> updateStatus(CombinationStatus status,String id){
    Boolean isSuccess=combinationService.updateStatus(status,id);
    return new ResponseObject<>(isSuccess);
  }


}
