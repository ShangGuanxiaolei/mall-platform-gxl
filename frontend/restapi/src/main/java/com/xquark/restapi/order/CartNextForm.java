package com.xquark.restapi.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.type.PromotionType;

import java.util.List;
import java.util.Map;

/**
 * @author tonghu
 */
@ApiModel("确认订单表单")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartNextForm extends PromotionInfoForm {

  @ApiModelProperty("多个skuId")
  private List<String> skuId;

  private Map<String, String> shopCoupons;

  // get from address return
  private Map<String, String> shopRemarksCache;

  @ApiModelProperty("当前shop")
  private String shopId;

  private String addressId;

  private boolean fromCart;

  @ApiModelProperty("直接下单的购买数量")
  private int qty;//直接下单购买数量

  @ApiModelProperty("直接下单购买商品的id")
  private String productId; // 直接下单购买代销商品的id

  @ApiModelProperty(value = "活动类型", notes = "从活动下单时传入", allowableValues = "PREORDER, FLASHSALE")
  private PromotionType promotionFrom;

  @ApiModelProperty(value = "活动商品id", notes = "从活动商品详情页下单时传入")
  private String promotionProductId;

  private String promotionId;

  @ApiModelProperty("是否使用积分抵扣现金")
  private boolean useDeduction; // 是否使用积分抵扣

  private String yundouProductId;

  // 是否来自特权码商品
  private String isPrivilege;
  private String pDetailCode;
  private String pCode;
  private String tranCode;

  public String getTranCode() {
    return tranCode;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }

  public String getIsPrivilege() {
    return isPrivilege;
  }

  public void setIsPrivilege(String isPrivilege) {
    this.isPrivilege = isPrivilege;
  }

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public List<String> getSkuId() {
    return skuId;
  }

  public void setSkuId(List<String> skuId) {
    this.skuId = skuId;
  }

  public String getShopId() {
    return shopId;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Map<String, String> getShopRemarksCache() {
    return shopRemarksCache;
  }

  public void setShopRemarksCache(Map<String, String> shopRemarksCache) {
    this.shopRemarksCache = shopRemarksCache;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionProductId() {
    return promotionProductId;
  }

  public void setPromotionProductId(String promotionProductId) {
    this.promotionProductId = promotionProductId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getYundouProductId() {
    return yundouProductId;
  }

  public void setYundouProductId(String yundouProductId) {
    this.yundouProductId = yundouProductId;
  }

  public boolean getFromCart() {
    return fromCart;
  }

  public void setFromCart(boolean fromCart) {
    this.fromCart = fromCart;
  }

  public boolean getUseDeduction() {
    return useDeduction;
  }

  public void setUseDeduction(boolean useDeduction) {
    this.useDeduction = useDeduction;
  }

  public String getAddressId() {
    return addressId;
  }

  public void setAddressId(String addressId) {
    this.addressId = addressId;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

}
