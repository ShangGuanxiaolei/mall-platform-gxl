package com.xquark.restapi.FreshMan;

import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.freshman.pojo.FreshManPageInfo;
import com.xquark.service.freshman.pojo.FreshManProduct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/3/5
 * Time:14:14
 * Des:新人页面
 */
@Controller
@RequestMapping("freshman")
public class FreshManController extends BaseController {

    @Autowired
    private FreshManService freshManService;

    /**
     * 获取新人页面数据
     * @param cpId
     * @return
     */
    @RequestMapping(value = "welfare/pageInfo", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<FreshManPageInfo> getFreshManPageInfo(Long cpId){
        ResponseObject<FreshManPageInfo> responseObject = new ResponseObject<FreshManPageInfo>();
        if(cpId == null){
            User user = (User) getCurrentIUser();
            cpId = user.getCpId();
        }
        FreshManPageInfo pageInfo = this.freshManService.selectFreshManPageInfo(cpId);
        responseObject.setData(pageInfo);
        return responseObject;
    }

    /**
     * 获取新人页面获取单个tag商品列表
     * @param tabId
     * @return
     */
    @RequestMapping(value = "welfare/goods/list/{tabId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<List<FreshManProduct>> getTagGoodsList(@PathVariable("tabId") Integer tabId){
        ResponseObject<List<FreshManProduct>> ro = new ResponseObject<List<FreshManProduct>>();
        if(tabId == null){
            ro.setMoreInfo("tabId不能为空!");
            return ro;
        }
        User user = (User) getCurrentIUser();
        Long cpId = user.getCpId();
        List<FreshManProduct> list = this.freshManService.selectFreshManProductListByTabId(tabId,cpId);
        if(list != null || list.isEmpty()){
            ro.setData(list);
        }
        return ro;
    }

    /**
     * 用户已知道获取到德分
     * @param reqMap
     * @return
     */
    @RequestMapping(value = "welfare/point/confirm", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Boolean> pointConfirm(@RequestBody Map<String, String> reqMap){
        ResponseObject<Boolean> ro = new ResponseObject<Boolean>();
        Long cpId = null;
        String paramCpId = reqMap.get("cpId");
        if(reqMap != null && StringUtils.isNotBlank(paramCpId)){
            cpId = Long.parseLong(paramCpId);
        }
        if(cpId == null){
            User user = (User) getCurrentIUser();
            cpId = user.getCpId();
        }
        boolean b = this.freshManService.updateFreshManPointStataus(cpId);
        ro.setData(b);
        return ro;
    }
}