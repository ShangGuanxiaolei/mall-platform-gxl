package com.xquark.restapi.notify;


import com.xquark.dal.model.MessageNotify;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.notify.MessageNotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MessageNotifyController  {

    @Autowired
    private MessageNotifyService messageNotifyService;

    @ResponseBody
    @RequestMapping("/notify/show")
    public ResponseObject<MessageNotify> NotifyMessage() {
        return new ResponseObject<>(messageNotifyService.selectMessageNotifyByTime());
    }


}
