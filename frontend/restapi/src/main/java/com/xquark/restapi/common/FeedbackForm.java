package com.xquark.restapi.common;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotBlank;

@ApiModel("反馈表单")
public class FeedbackForm {

  private static final String NOT_BLANK_MESSAGE = "{valid.notBlank.message}";

  @ApiModelProperty(value = "反馈用户名")
  private String name;

  @ApiModelProperty(value = "联系方式")
  @NotBlank(message = NOT_BLANK_MESSAGE)
  @Pattern(regexp = "(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$",
      message = "请输入正确的手机号码")
  private String contact;

  @NotBlank(message = "请填写反馈内容")
  private String content;

  @NotNull(message = NOT_BLANK_MESSAGE)
  @ApiModelProperty(value = "选中的标签id，多个")
  private List<String> tagIds;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public List<String> getTagIds() {
    return tagIds;
  }

  public void setTagIds(List<String> tagIds) {
    this.tagIds = tagIds;
  }
}
