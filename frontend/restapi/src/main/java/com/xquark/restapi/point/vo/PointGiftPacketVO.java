package com.xquark.restapi.point.vo;

import com.google.common.collect.Lists;
import com.xquark.dal.model.UserDTO;
import com.xquark.dal.model.UserPointDTO;

import java.util.List;

/**
 * @author Jack Zhu
 * @since 2018/12/19
 */
public class PointGiftPacketVO {
    private List<UserPointVO> userPointVOS;
    private UserDTO belongUser;
    private Integer size;
    private static final  int LIMIT_MEMBER = 8;
    public PointGiftPacketVO() {
    }

    public PointGiftPacketVO(List<UserPointDTO> userPointDTOS, UserDTO belongUser) {
        this.userPointVOS  = Lists.newArrayList();
        int i = 0;
        for (UserPointDTO userPointDTO : userPointDTOS) {
            if (i == LIMIT_MEMBER) {
                break;
            }
            i++;
            userPointVOS.add(UserPointVO.convert(userPointDTO));
        }
        this.size = userPointDTOS.size();
        this.belongUser = belongUser;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<UserPointVO> getUserPointVOS() {
        return userPointVOS;
    }

    public void setUserPointVOS(List<UserPointVO> userPointVOS) {
        this.userPointVOS = userPointVOS;
    }

    public UserDTO getBelongUser() {
        return belongUser;
    }

    public void setBelongUser(UserDTO belongUser) {
        this.belongUser = belongUser;
    }

    @Override
    public String toString() {
        return "PointGiftPacketVO{" +
                "userPointVOS=" + userPointVOS +
                ", belongUser=" + belongUser +
                ", size=" + size +
                '}';
    }
}
