package com.xquark.restapi.wechat;

/**
 * Created by wangxinhua on 17-12-15. DESC:
 */
public class ShareConfigVO {

  private final String appId;
  private final String timestamp;
  private final String nonceStr;
  private final String signature;

  public ShareConfigVO(String appId, String timestamp, String nonceStr, String signature) {
    this.appId = appId;
    this.timestamp = timestamp;
    this.nonceStr = nonceStr;
    this.signature = signature;
  }

  public String getAppId() {
    return appId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getNonceStr() {
    return nonceStr;
  }

  public String getSignature() {
    return signature;
  }

  public String getWxJsapiCall() {
    return "wxJsapiCall";
  }
}
