package com.xquark.restapi.cart;

import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.type.PromotionType;
import com.xquark.restapi.order.PromotionInfoForm;
import java.util.List;
import java.util.Map;
import org.hibernate.validator.constraints.NotEmpty;

public class CartPricingForm extends PromotionInfoForm {

  // 商品skuId数组
  @NotEmpty
  @ApiModelProperty(value = "选中的sku信息，多个则传入多次（如skuIds=xxx&skuIds=yyyy）")
  private List<String> skuIds;

  @ApiModelProperty(value = "不需要传入")
  @Deprecated
  // 使用usingCoupons参数
  private Map<String, String> shopCoupons;

  /**
   * 使用的优惠券
   */
  private List<String> usingCoupons;

  private PromotionType promotionFrom;

  // 收件地址区域
  @ApiModelProperty(value = "不需要传入")
  private String zoneId;
  // 使用的优惠券
  @ApiModelProperty(value = "不需要传入")
  private String couponId;

  @ApiModelProperty(value = "不需要传入")
  private Integer qty;

  @ApiModelProperty(value = "不需要传入")
  private String addressId;

  @ApiModelProperty(value = "传入true")
  private boolean fromCart;

  public String getAddressId() {
    return addressId;
  }

  public void setAddressId(String addressId) {
    this.addressId = addressId;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public List<String> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<String> skuIds) {
    this.skuIds = skuIds;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Integer getQty() {
    return qty;
  }

  public void setQty(Integer qty) {
    this.qty = qty;
  }

  public boolean getFromCart() {
    return fromCart;
  }

  public void setFromCart(boolean fromCart) {
    this.fromCart = fromCart;
  }

  public List<String> getUsingCoupons() {
    return usingCoupons;
  }

  public void setUsingCoupons(List<String> usingCoupons) {
    this.usingCoupons = usingCoupons;
  }

  public PromotionType getPromotionFrom() {
    return promotionFrom;
  }

  public void setPromotionFrom(PromotionType promotionFrom) {
    this.promotionFrom = promotionFrom;
  }
}
