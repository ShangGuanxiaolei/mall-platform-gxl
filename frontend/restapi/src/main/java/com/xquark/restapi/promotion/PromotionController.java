package com.xquark.restapi.promotion;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.*;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.InviteCodeStatus;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.dal.vo.ProductSelectVO;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.dal.vo.PromotionVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fullReduce.fullCut.PromotionFullCutService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.ProductTopService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionInviteCodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 活动controller Created by chh on 17-08-01.
 */
@Controller
@ApiIgnore
public class PromotionController extends BaseController {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private ProductTopService productTopService;

    @Autowired
    private PromotionFullCutService fullCutService;

    @Autowired
    private ProductService productService;
    @Autowired
    private PromotionInviteCodeService promotionInviteCodeService;
    @Autowired
    private PromotionConfigService promotionConfigService;
    @Autowired
    private PromotionBaseInfoService promotionBaseInfoService;

    /**
     * 获取限时抢购活动列表
     */
    @ResponseBody
    @RequestMapping("/promotion/listFlashsale")
    public ResponseObject<List<PromotionVO>> listFlashsale(Pageable pageable) {
        List<PromotionVO> promotions = promotionService
                .getPromotionForApp(pageable, PromotionType.FLASHSALE.toString());
        return new ResponseObject<>(promotions);
    }

    /**
     * 获取品牌/专题活动列表
     */
    @ResponseBody
    @RequestMapping("/promotion/listTweet")
    public ResponseObject<List<PromotionVO>> listTweet(Pageable pageable) {
        List<PromotionVO> promotions = promotionService
                .getPromotionForApp(pageable, PromotionType.DIAMOND.toString());
        return new ResponseObject<>(promotions);
    }

    /**
     * 获取砍价商品活动列表
     */
    @ResponseBody
    @RequestMapping("/promotion/listBargain")
    public ResponseObject<List<PromotionVO>> listBargain(Pageable pageable) {
        List<PromotionVO> promotions = promotionService
                .getPromotionForApp(pageable, PromotionType.BARGAIN.toString());
        return new ResponseObject<>(promotions);
    }

    /**
     * 获取团购活动列表
     */
    @ResponseBody
    @RequestMapping("/promotion/lisGroupon")
    public ResponseObject<List<PromotionVO>> listGroupon(Pageable pageable) {
        List<PromotionVO> promotions = promotionService
                .getPromotionForApp(pageable, PromotionType.GROUPON.toString());
        return new ResponseObject<>(promotions);
    }

    /**
     * 获取爆款推荐商品列表
     */
    @ResponseBody
    @RequestMapping("/promotion/listProductTop")
    public ResponseObject<List<ProductTopVO>> listProductTop(Pageable pageable,
                                                             @RequestParam(required = false) String productId) {
        String sellerShopId = getCurrentUser().getCurrentSellerShopId();
        List<ProductTopVO> vos = productTopService.getHomeForApp(pageable, sellerShopId);
        LinkedList result = new LinkedList();
        for (ProductTopVO product : vos) {
            if (product.getProductId().equals(productId)) {
                result.addFirst(product);
            } else {
                result.add(product);
            }
        }
        return new ResponseObject<List<ProductTopVO>>(result);
    }

    @ResponseBody
    @RequestMapping("/promotion/listProductSelective")
    public ResponseObject<Map<String, Object>> listProductWithSelect(
            String promotionId,
            ProductType type,
            @RequestParam final PromotionType promotionType,
            @RequestParam final PromotionUserScope scope,
            String categoryId, ProductStatus status,
            String keyword, Pageable pageable) {
        if (!(promotionType == PromotionType.FULLCUT || promotionType == PromotionType.INSIDE_BUY)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型暂不支持");
        }
        Function<PdPromotionInner, Boolean> isSelectFunction = new Function<PdPromotionInner, Boolean>() {
            @Override
            public Boolean apply(PdPromotionInner inner) {
                return fullCutService.inPromotion(inner.getProductId(), inner.getPromotionId(), promotionType, scope);
            }
        };

        Map<String, Object> params = new HashMap<>();
        params.put("type", Optional.fromNullable(type).or(ProductType.NORMAL));
        if (StringUtils.isNoneBlank(categoryId)) {
            params.put("categoryId", categoryId);
        }
        if (status != null) {
            params.put("status", status);
        }
        if (StringUtils.isNoneBlank(keyword)) {
            params.put("keyword", keyword.trim());
        }

        List<ProductSelectVO> list = productService.listProductWithSelectInfo(promotionId,
                params, isSelectFunction, pageable);
        Long total = productService.countProductWithParams(params);
        Map<String, Object> result = ImmutableMap.of("list", list, "total", total);
        return new ResponseObject<>(result);
    }


    /**
     * 查看是否已有某种类型的活动
     *
     * @param type 活动类型
     * @return 是否有该类型活动
     */
    @ResponseBody
    @RequestMapping("/promotion/hasActivate")
    public ResponseObject<Boolean> hasActivate(@RequestParam PromotionType type) {
        Boolean hasActivate = promotionService.hasActivate(type);
        return new ResponseObject<>(hasActivate);
    }

    /**
     * 将productId排在list的第一个
     */
    private List sortProduct(List products, String productId) {
        LinkedList result = new LinkedList();
        for (Object product1 : products) {
            PromotionProduct product = (PromotionProduct) product1;
            if (product.getProductId().equals(productId)) {
                result.addFirst(product);
            } else {
                result.add(product);
            }
        }
        return result;
    }


    /**
     * 售卖活动以及橱窗产品查询 供app查询
     */
    @ResponseBody
    @RequestMapping(value="/promotion/getMeetingSaleProList", method = RequestMethod.POST)
    public PageHelper<PromotionVO> getMeetingSaleProList(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize",defaultValue = "100")Integer pageSize) {
        User user = (User)getCurrentIUser();
        PageHelper<PromotionVO> promotions = promotionService
                .getMeetingSaleProList(user.getCpId(),pageNum,pageSize, PromotionType.MEETING.toString());
        return promotions;
    }

    /**
     * 大会活动校验邀请码
     */
    @ResponseBody
    @RequestMapping(value="/promotion/validateInviteCode", method = RequestMethod.POST)
    public ResponseObject validateInviteCode(@RequestParam(value = "pCode") String pCode, @RequestParam(value = "inviteCode")String inviteCode) {
        User currentIUser = (User)getCurrentIUser();
        int validate = promotionInviteCodeService.validate(inviteCode, pCode);

        PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);
        Map map =new HashMap();
        map.put("configType",baseInfo.getpType());
        List<PromotionConfig> promotionConfigs = promotionConfigService.selectListByConfigType(map);
        String codeInvalidRemind ="";
        for (PromotionConfig promotionConfig : promotionConfigs) {
            if("codeInvalidRemind".equals(promotionConfig.getConfigName())
                    &&null!=promotionConfig.getConfigValue()
                    &&!"".equals(promotionConfig.getConfigValue()) ){
                codeInvalidRemind=promotionConfig.getConfigValue();
            }
        }

        if(validate<0){
            //邀请码校验不通过
            return new ResponseObject<>(false,codeInvalidRemind,GlobalErrorCode.MEETING_INVITE);

        }else if(validate == InviteCodeStatus.NEVER.getCode()){
            //邀请码有效且未绑定
            //则绑定邀请码
            int i = promotionInviteCodeService.updateRelation(inviteCode,currentIUser.getCpId().toString(),pCode);
            if(i>0){
                //绑定成功，校验通过
                return new ResponseObject<>(true,"邀请码绑定成功",GlobalErrorCode.SUCESS);

            }else{
                //绑定失败
                return new ResponseObject<>(false,"邀请码绑定失败",GlobalErrorCode.MEETING_INVITE);

            }
        }
        //校验通过
        return new ResponseObject<>(true,"邀请码已绑定",GlobalErrorCode.SUCESS);
    }

}
