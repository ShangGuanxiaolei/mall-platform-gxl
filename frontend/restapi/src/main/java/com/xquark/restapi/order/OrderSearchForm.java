package com.xquark.restapi.order;

import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.type.PaymentMode;

/**
 * 订单查询Form表单逻辑
 */
public class OrderSearchForm {

  private String orderNo;

  private PaymentMode paymentMode;

  private OrderStatus status;

  private String completeStartDate;

  private String completeEndDate;

  private String logisticsNo;

  private OrderSortType orderType;

  private String buyerName;

  private String sellerName;

  private OrderRefundStatus refundStatus;

  private AgentType agentType;

  private String buyerRequire;

  private String supplier;

  private String consignee; //收件人

  public String getSupplier() {
      return supplier;
  }

  public void setSupplier(String supplier) {
      this.supplier = supplier;
  }

  public String getBuyerRequire() {
    return buyerRequire;
  }

  public void setBuyerRequire(String buyerRequire) {
    this.buyerRequire = buyerRequire;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public AgentType getAgentType() {
    return agentType;
  }

  public void setAgentType(AgentType agentType) {
    this.agentType = agentType;
  }

  private ActivityGrouponStatus grouponStatus;

  public ActivityGrouponStatus getGrouponStatus() {
    return grouponStatus;
  }

  public void setGrouponStatus(ActivityGrouponStatus grouponStatus) {
    this.grouponStatus = grouponStatus;
  }

  public OrderSortType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderSortType orderType) {
    this.orderType = orderType;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public PaymentMode getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(PaymentMode paymentMode) {
    this.paymentMode = paymentMode;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public String getCompleteStartDate() {
    return completeStartDate;
  }

  public void setCompleteStartDate(String completeStartDate) {
    this.completeStartDate = completeStartDate;
  }

  public String getCompleteEndDate() {
    return completeEndDate;
  }

  public void setCompleteEndDate(String completeEndDate) {
    this.completeEndDate = completeEndDate;
  }

  public OrderRefundStatus getRefundStatus() {
    return refundStatus;
  }

  public void setRefundStatus(OrderRefundStatus refundStatus) {
    this.refundStatus = refundStatus;
  }

  public String getLogisticsNo() {
    return logisticsNo;
  }

  public void setLogisticsNo(String logisticsNo) {
    this.logisticsNo = logisticsNo;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }
}
