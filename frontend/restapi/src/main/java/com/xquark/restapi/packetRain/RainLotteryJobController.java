package com.xquark.restapi.packetRain;

import com.xquark.restapi.ResponseObject;
import com.xquark.service.lottery.LotteryJobService;
import com.xquark.service.packetRain.PacketRainJobService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通过接口
 *
 * @author wangxinhua
 * @date 2019-04-28
 * @since 1.0
 */
@RestController
@RequestMapping("/openapi/job")
public class RainLotteryJobController {

    private final LotteryJobService lotteryJobService;

    private final PacketRainJobService rainJobService;

    @Autowired
    public RainLotteryJobController(LotteryJobService lotteryJobService, PacketRainJobService rainJobService) {
        this.lotteryJobService = lotteryJobService;
        this.rainJobService = rainJobService;
    }

    @RequestMapping("/lotteryInit")
    public ResponseObject<Boolean> lotteryInit() {
        final Either<String, Boolean> ret = lotteryJobService.init();
        return ResponseObject.from(ret);
    }

    @RequestMapping("/lotteryPrizeInit")
    public ResponseObject<Boolean> initLotteryPrize() {
        final Either<String, Boolean> ret = lotteryJobService.initPrices(true);
        return ResponseObject.from(ret);
    }

    @RequestMapping("/lotteryPrizePush")
    public ResponseObject<Boolean> pushLotteryPrize() {
        final Either<String, Boolean> ret = lotteryJobService.pushPrices(true);
        return ResponseObject.from(ret);
    }

    @RequestMapping("/rainInit")
    public ResponseObject<Boolean> rainInit() {
        final Either<String, Boolean> ret = rainJobService.init(false);
        return ResponseObject.from(ret);
    }

    @RequestMapping("/rainClear")
    public ResponseObject<Boolean> clearUserRainInfo(@RequestParam Long cpId) {
        final Either<String, Boolean> ret = rainJobService.clearUserRain(cpId);
        return ResponseObject.from(ret);
    }

}
