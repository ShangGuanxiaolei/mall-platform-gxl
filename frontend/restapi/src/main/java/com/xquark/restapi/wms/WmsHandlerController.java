package com.xquark.restapi.wms;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.WmsProductMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.WmsBatch;
import com.xquark.dal.model.WmsExpress;
import com.xquark.dal.model.WmsInventory;
import com.xquark.dal.model.WmsOrder;
import com.xquark.dal.model.WmsProduct;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.LogisticsStockParam;
import com.xquark.dal.vo.OrderItemsDal;
import com.xquark.dal.vo.SFOrderLogDal;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.logistics.impl.LogisticsGoodsServiceImpl;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.product.ProductService;
import com.xquark.service.productPackage.PackageService;
import com.xquark.service.sku.SkuAttributeService;
import com.xquark.service.vo.LogisticsStock;
import com.xquark.service.vo.LogisticsStockOut;
import com.xquark.service.wms.WmsBatchService;
import com.xquark.service.wms.WmsExpressService;
import com.xquark.service.wms.WmsInventoryService;
import com.xquark.service.wms.WmsOrderNtsProductService;
import com.xquark.service.wms.WmsOrderService;
import com.xquark.service.wms.WmsProductService;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: kong Date: 2018/6/20. Time: 10:49
 */
@RestController
@RequestMapping("/wmsupdate")
public class WmsHandlerController {

  private static final Logger log = LoggerFactory.getLogger(WmsHandlerController.class);

  /**
   * WmsOrder调试
   */
  @Autowired
  private WmsOrderService wmsOrderService;

  @RequestMapping("/ordergive")
  public ResponseObject<String> test() {
    List<WmsOrder> wmsOrders = orderService.getWmsOrder();
    Set<String> orderNos = wmsOrderService.loadWmsOrderSet();
    log.info("开始同步wms订单数据");
    for (WmsOrder o : wmsOrders) {
      wmsOrderService.handleWmsOrder(o, orderNos);
    }
    log.info("wms订单数据同步完成");
    return new ResponseObject<>("done");
  }

  @RequestMapping("/orderget")
  public ResponseObject<String> test01() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    List<WmsOrder> list = wmsOrderService.getByWmsEdiStatus(1);
    changeWmsEdiStatusAndUpdate(list);
    return responseObject;
  }

  public void changeWmsEdiStatusAndUpdate(List<WmsOrder> list) {
    for (WmsOrder w : list) {
      try {
        if (w.getStatus() != 9) {
          continue;
        }
        w.setWmsEdiStatus(2);
        wmsOrderService.update(w);
        /*发货状态未定义*/
        OrderStatus status = OrderStatus.SHIPPED;
        orderService.updateStatusByOrderNo(status, w.getOrderNo());
        w.setWmsEdiStatus(3);
        wmsOrderService.update(w);
      } catch (Exception e) {
        log.error("获取Wms订单表编号出错:{},trace:{}", w.getOrderNo(), e);
      }
    }
  }

  /**
   * WmsBatch调试
   */
  @Autowired
  private WmsBatchService wmsBatchService;

  @RequestMapping("/batchget")
  public ResponseObject<String> testbatch() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    List<WmsBatch> list = wmsBatchService.getByEdiStatus(1);
    changeEdiStatusAndUpdate(list);
    return responseObject;
  }

  public void changeEdiStatusAndUpdate(List<WmsBatch> list) {
    for (WmsBatch w : list) {
      w.setEdiStatus(2);
      wmsBatchService.update(w);
      /*批次表未创建*/
      w.setEdiStatus(3);
      wmsBatchService.update(w);
    }
  }

  /**
   * WmsInventory调试
   */

  @Autowired
  private WmsInventoryService wmsInventoryService;

  @Autowired
  private ProductService productService;

  @Autowired
  private SkuMapper skuMapper;

  @RequestMapping("/inventoryget")
  public ResponseObject<String> testInventory() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    Date start = new Date();
    List<WmsInventory> list = wmsInventoryService.getByEdiStatus(1, start);
    wmsInventoryService.changeEdiStatusAndUpdate(list, start);
    return responseObject;
  }

  @Autowired
  private ProductMapper productMapper;

  /**
   * WmsExpress调试
   */

  @Autowired
  private WmsExpressService wmsExpressService;

  @Autowired
  private OrderService orderService;

  @RequestMapping("/expressget")
  public ResponseObject<String> testExpress() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    List<WmsExpress> list = wmsExpressService.getByEdiStatusAndStatus(1, 0);
    changeEdiStatusAndUpdateExpress(list);
    return responseObject;
  }

  public void changeEdiStatusAndUpdateExpress(List<WmsExpress> list) {
    for (WmsExpress w : list) {
      try {
        w.setEdiStatus(2);
        Boolean buttum = wmsExpressService.update(w);
        orderService.updateByOrderNo(w);
        w.setEdiStatus(3);
        wmsExpressService.update(w);
      } catch (Exception e) {
        log.error("快递表数据快递单号:{},trace:{}", w.getOrderNo(), e);
      }
    }
  }

  /**
   * WmsProduct调试
   */

  @Autowired
  private WmsProductService wmsProductService;

  @RequestMapping("/productgive")
  public ResponseObject<String> testproduct() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    List<WmsProduct> list = productService.getWmsProduct();
    AddOrUpdate(list);
    return responseObject;
  }

  public void AddOrUpdate(List<WmsProduct> list) {
    Set<String> codes = wmsProductService.getCodeSet();
    for (WmsProduct w : list) {
//      if(w.getCode()==null){
//        continue;
//      }
      try {
        if (codes.contains(w.getCode())) {
          /*更新数据待优化*/
          wmsProductService.update(w);
        } else {
          wmsProductService.insert(w);
        }
        w.setEdiStatus(1);
        wmsProductService.update(w);
      } catch (Exception e) {
        log.error("Wms产品表sku编号:{},trace:{}", w.getCode(), e);
      }
    }
  }

  /**
   * Excel测试
   */
  @Autowired
  private PackageService packageService;

  @Autowired
  private SkuAttributeService skuAttributeService;

  @RequestMapping("/product")
  public ResponseObject<String> testmyword() {
    ResponseObject<String> responseObject = new ResponseObject<>();
    responseObject.setData("success");
    File file = new File("/home/byy/Desktop/一件代发商品售价导入模板.xlsx");
    if (!file.exists()) {
      responseObject.setMoreInfo("文件不存在");
      return responseObject;
    }
    try {
      skuAttributeService.importExcel(file);//"/root/test.xlsx"

    } catch (Exception e) {
      responseObject.setData(e.getMessage());
    }
    return responseObject;
  }

  /**
   * 状态改变测试
   */
  @Autowired
  private OrderHeaderService orderHeaderService;

  @Autowired
  private MainOrderService mainOrderService;

  @RequestMapping("/testHeader")
  public ResponseObject<String> testHeader() {
    ResponseObject<String> responseObject = new ResponseObject<>();
//    int num=orderHeaderService.updateStatusByOrderNo("9","01H10079713test");
//    responseObject.setData(num+"");
    Map<String, String> params = new HashMap<>();
    params.put("out_trade_no", "ES180629220617005008");
    params.put("trade_no", "ESO180629220617005009");
//    //1.跟新xquark_order为已支付
//    orderService.updateStatusByOrderNo(OrderStatus.PAID,params.get("out_trade_no"));
//    //2.order_header为已支付
//    orderHeaderService.updateStatusByOrderNo("1",params.get("out_trade_no"));
//    //3.跟新xquark_order字段pay_no
//    orderService.updatePayNoByOrderNo(params.get("out_trade_no"),params.get("trade_no"));
//    List<String> list=mainOrderService.getOrderNoByMain("ES180629220617005008");
//    mainOrderService.updateStatusByOrderNo("ES180629220617005008",MainOrderStatus.PAID);
//    String data="{\"code\": 0,\"msg\": \"success\","
//        + "    \"data\": {\n"
//        + "        \"orderId\": 14518050,"
//        + "         \"orderSn\": 123456789"
//        + "    }}";
//    JSONObject jsonObject=JSON.parseObject(data);
//    JSONObject jsonArray=jsonObject.getJSONObject("data");
//
//    Order orderNext=new Order();
//    orderNext.setOrderNo("ESO180629153556003005");
//    orderNext.setLogisticsCompany("顺丰");
//    orderNext.setLogisticsOrderNo(jsonArray.getString("orderSn"));
//    orderNext.setPartnerOrderNo(jsonArray.getString("orderId"));
//    orderService.updateOrderByOrderNo(orderNext);
//    //<point.core.version>2.1.57-o</point.core.version>
    List<String> orderNos = mainOrderService.getOrderNoByMain(params.get("out_trade_no"));
    mainOrderService.updateStatusByOrderNo(params.get("out_trade_no"), MainOrderStatus.PAID);
    for (String orderNo : orderNos) {
      //1.跟新xquark_order为已支付
      orderService.updateStatusByOrderNo(OrderStatus.PAID, orderNo);
      //2.order_header为已支付
      orderHeaderService.updateStatusByOrderNo("1", orderNo);
      //3.跟新xquark_order字段pay_no
      orderService.updatePayNoByOrderNo(orderNo, params.get("trade_no"));
    }
    return responseObject;
  }

  @RequestMapping("/testOrder")
  public ResponseObject<String> testOrder() {
    ResponseObject<String> responseObject = new ResponseObject<>();
//    orderService.updateStatusByOrderNo(OrderStatus.SHIPPED,"ESO180629162054005007");
    Order orderNext = new Order();
    orderNext.setOrderNo("ESO180629182125003001");
    orderNext.setLogisticsCompany("顺丰");
    orderNext.setLogisticsOrderNo("12312131");
    orderNext.setPartnerOrderNo("019238781394");
    orderNext.setStatus(OrderStatus.SHIPPED);
    orderService.updateOrderByOrderNo(orderNext);
    return responseObject;//comiss_paid积分消费   paid_point得分
    //AsyncSubmitOrder
  }

  /**
   * WmsNts测试
   */
  @Autowired
  private WmsOrderNtsProductService wmsOrderNtsProductService;

//  @Autowired
//  private SfRegionService sfRegionService;
//  @RequestMapping("/testWmsNts")
//  @ResponseBody
//  public String getData(Integer a) {
//    Integer length = wmsOrderNtsProductService.getWmsOrderNtsProduct();
//    int Province = sfRegionService.getsfid(a);
//    return Province + "";
//  }

  //ESO180702172245244007

  @RequestMapping("/testInstock")
  public void testInstock() {
//    Integer num = productService.productInstock();
//    int a = 1;
  }

  @Autowired
  private LogisticsGoodsServiceImpl goodsService;

  @Autowired
  private WmsProductMapper wmsProductMapper;

  @Autowired
  private OrderMapper orderMapper;

  @RequestMapping("/findSFInStock")
  public ResponseObject<List<LogisticsStockOut>> findInstock(String orderNo)
      throws Exception {
    ResponseObject<List<LogisticsStockOut>> responseObject = new ResponseObject<>();
    List<String> orderNos = orderMapper.getOrderNoByLoc();
    List<LogisticsStockOut> logisticsStockOuts = new ArrayList<>();
    for (String orderNo1 : orderNos) {
      List<LogisticsStockParam> logisticsStockParams = wmsProductMapper.getSFInStock(orderNo1);
      List<com.xquark.service.vo.LogisticsStockParam> logisticsStockParams1 = new ArrayList<>();
      List<LogisticsStockOut> logisticsStockOuts1 = new ArrayList<>();
      for (LogisticsStockParam p : logisticsStockParams) {
        com.xquark.service.vo.LogisticsStockParam logisticsStockParam = new com.xquark.service.vo.LogisticsStockParam();
        logisticsStockParam.setBomSn(p.getBomSn());
        logisticsStockParam.setProductid(p.getProductid());
        logisticsStockParam.setProductNum(p.getProductNum());
        logisticsStockParam.setRegionId(p.getRegionId());
        logisticsStockParam.setSfairline(p.getSfairline());
        logisticsStockParam.setSfshipping(p.getSfshipping());
        logisticsStockParams1.add(logisticsStockParam);
        LogisticsStockOut logisticsStockOut = new LogisticsStockOut();
        logisticsStockOut.setOrderNo(orderNo1);
        logisticsStockOut.setProductId(p.getProductid());
        logisticsStockOut.setSfZoneId(p.getRegionId());
        logisticsStockOut.setName(p.getName());
        logisticsStockOuts1.add(logisticsStockOut);
      }
      List<LogisticsStock> logisticsStocks = goodsService.getStock(logisticsStockParams1);
      for (LogisticsStock l : logisticsStocks) {
        for (LogisticsStockOut o : logisticsStockOuts1) {
          if (o.getProductId().longValue() == l.getProductId().longValue()) {
            o.setSfStock(l.getMaxSaleNum());
            if (o.getSfStock().intValue() != 0) {
              logisticsStockOuts1.remove(o);
            }
            break;
          }
        }
      }
      logisticsStockOuts.addAll(logisticsStockOuts1);
    }
    XSSFWorkbook workbook =
        new XSSFWorkbook();
    //在Excel工作簿中创建一个工作表
    XSSFSheet sheet =
        workbook.createSheet("Demo");
    //createRow创建行,参数是行号:0 1 2 3...
//    XSSFRow row = sheet.createRow(0);
    //在行中创建数据格子, 参数是序号: 0 1 2 3...
//    XSSFCell cell = row.createCell(0);
    //向格子填写数据
    for (int i = 0; i < logisticsStockOuts.size() + 1; i++) {
      XSSFRow row = sheet.createRow(i);
      for (int j = 0; j < 5; j++) {
        XSSFCell cell = row.createCell(j);
        if (i == 0) {
          switch (j) {
            case 0:
              cell.setCellValue("订单编号");
              break;
            case 1:
              cell.setCellValue("订单区域Id");
              break;
            case 2:
              cell.setCellValue("最大可销售库存");
              break;
            case 3:
              cell.setCellValue("产品Id");
              break;
            case 4:
              cell.setCellValue("产品名称");
          }
          continue;
        }
        LogisticsStockOut logisticsStock = logisticsStockOuts.get(i - 1);
        switch (j) {
          case 0:
            cell.setCellValue(logisticsStock.getOrderNo());
            break;
          case 1:
            cell.setCellValue(logisticsStock.getSfZoneId());
            break;
          case 2:
            cell.setCellValue(logisticsStock.getSfStock());
            break;
          case 3:
            cell.setCellValue(logisticsStock.getProductId());
            break;
          case 4:
            cell.setCellValue(logisticsStock.getName());
        }
      }
    }
    FileOutputStream fileOutputStream = new FileOutputStream(
        new File("/home/byy/Desktop/test.xlsx"));
    workbook.write(fileOutputStream);
    responseObject.setData(logisticsStockOuts);
    return responseObject;
  }

  @RequestMapping("/wmsExpressRoute")
  public ResponseObject<List<SFOrderLogDal>> wmsExpress(String orderNo) {
    ResponseObject<List<SFOrderLogDal>> responseObject = new ResponseObject<>();
    List<SFOrderLogDal> sfOrderLogDals = wmsExpressService.getRouteByOrderNo(orderNo);
    responseObject.setData(sfOrderLogDals);
    return responseObject;
  }

  @RequestMapping("/wmsproduct")
  public ResponseObject<List<OrderItemsDal>> wmsProduct(String orderNo) {
    ResponseObject<List<OrderItemsDal>> responseObject = new ResponseObject<>();
    List<OrderItemsDal> orderItemsDals = productMapper.getLocOrderItems(orderNo);
    responseObject.setData(orderItemsDals);
    return responseObject;
  }

  @RequestMapping("/hotSearch")
  public ResponseObject<List> hotSearch(Integer pageFront, Integer page) {
    List list = new ArrayList();
    for (int i = pageFront * page; i < pageFront * page + 9; i++) {
      Map map = new HashMap();
      map.put("value", "结果" + i);
      map.put("time", new Date().toString());
      list.add(map);
    }
    return new ResponseObject<>(list);
  }

  @RequestMapping("/hotSearch/total")
  public ResponseObject<Integer> total() {
    return new ResponseObject<>(109);
  }
}
