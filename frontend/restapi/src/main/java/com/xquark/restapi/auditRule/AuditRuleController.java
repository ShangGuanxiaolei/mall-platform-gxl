package com.xquark.restapi.auditRule;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.AuditRule;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.auditRule.AuditRuleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 订单审核规则controller Created by chh on 17-1-20.
 */
@Controller
@ApiIgnore
public class AuditRuleController extends BaseController {

  @Autowired
  private AuditRuleService auditRuleService;

  /**
   * 获取用户对应角色列表
   */
  @ResponseBody
  @RequestMapping("/auditRule/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword,
      @RequestParam String type) {
    String shopId = this.getCurrentIUser().getShopId();
    List<AuditRule> roles = null;
    roles = auditRuleService.list(pageable, keyword, type);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", auditRuleService.selectCnt(keyword, type));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存用户对应角色
   */
  @ResponseBody
  @RequestMapping("/auditRule/save")
  public Json savePromotion(AuditRule role) {
    Json json = new Json();
    int result = 0;
    AuditRule existRule = auditRuleService
        .selectByBuyerAndSeller(role.getBuyerType(), role.getSellerType(), role.getId(),
            role.getType());
    if (existRule != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("审核类型已经存在，请重新选择！");
      return json;
    }

    if (StringUtils.isNotEmpty(role.getId())) {
      result = auditRuleService.modify(role);
    } else {
      role.setArchive(false);
      result = auditRuleService.insert(role);
    }
    if (result == 1) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("保存成功！");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("保存失败！");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping("/auditRule/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = auditRuleService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的用户对应角色记录<br>
   */
  @ResponseBody
  @RequestMapping("/auditRule/{id}")
  public ResponseObject<AuditRule> view(@PathVariable String id, HttpServletRequest req) {
    AuditRule role = auditRuleService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }


}
