package com.xquark.restapi.point.vo;

import java.math.BigDecimal;

public class PointPacketReceiveVO {

    /**
     * pointGift : 领取积分
     * statusNo :  领取状态码
     * statusInfo : 领取信息
     */

    private BigDecimal pointGift;
    private String statusNo;
    private String statusInfo;

    public PointPacketReceiveVO(){}
    public PointPacketReceiveVO(BigDecimal pointGift, String statusNo, String statusInfo){
        this.pointGift = pointGift;
        this.statusNo  = statusNo;
        this.statusInfo = statusInfo;
    }

    public BigDecimal getPointGift() {
        return pointGift;
    }

    public void setPointGift(BigDecimal pointGift) {
        this.pointGift = pointGift;
    }

    public String getStatusNo() {
        return statusNo;
    }

    public void setStatusNo(String statusNo) {
        this.statusNo = statusNo;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }
}
