package com.xquark.restapi.yundou;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.YundouRule;
import com.xquark.dal.model.YundouRuleDetail;
import com.xquark.dal.vo.YundouRuleVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.yundou.YundouRuleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-5-5. DESC:
 */
@RestController
@ApiIgnore
public class YundouRuleController {

  @Autowired
  private YundouRuleService ruleService;

  @RequestMapping("/yundou/rules/save")
  public ResponseObject<Boolean> save(YundouRuleVO rule) {

    if (StringUtils.isBlank(rule.getOperationId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "操作id不能为空");
    }
    if (StringUtils.isBlank(rule.getName())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规则名不能为空");
    }
    if (rule.getCode() == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规则代码不能为空");
    }

    if (rule.getAmount() == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "云豆数量不能为空");
    }

    String id = rule.getId();
    Boolean result;
    if (StringUtils.isBlank(id)) {
      // 保存
      YundouRule exists = ruleService.loadByName(rule.getName());
      if (exists != null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规则已存在");
      }
      result = ruleService.save(rule);
    } else {
      // 更新
      YundouRule exists = ruleService.load(id);
      if (exists == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的规则不存在");
      }
      rule.setId(exists.getId());
      result = ruleService.updateById(rule);
    }
    return new ResponseObject<>(result);
  }


  @RequestMapping("/yundou/rules/updateDetail")
  public ResponseObject<Boolean> updateDetail(@RequestBody List<YundouRuleDetail> details) {
    Boolean result = true;
    for (YundouRuleDetail detail : details) {
      YundouRuleDetail exits = ruleService
          .loadDetailAmount(detail.getRuleId(), detail.getRoleId());
      if (exits == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "云豆规则明细不存在");
      }
      exits.setAmount(detail.getAmount());
      if (!ruleService.updateDetail(exits)) {
        result = false;
      }
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/yundou/rules/remove/{id}")
  public ResponseObject<Boolean> remove(@PathVariable("id") String id) {
    YundouRule exists = ruleService.load(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要删除的规则不存在");
    }
    String ruleId = exists.getId();
    // 删除明细表数据
    ruleService.deleteDetail(ruleId);
    return new ResponseObject<>(ruleService.deleteById(id));
  }

  @RequestMapping("/yundou/rules/show/{id}")
  public ResponseObject<YundouRuleVO> showVO(@PathVariable("id") String id) {
    YundouRuleVO result = ruleService.loadRuleVO(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/yundou/rules/list")
  public ResponseObject<Map<String, Object>> listRules(String operationId,
      String order,
      Pageable pageable,
      String direction) {

    Map<String, Object> result = new HashMap<>();
    List<YundouRule> ruleList;
    Integer count;

    if (StringUtils.isBlank(direction)) {
      direction = "ASC";
    }

    if (StringUtils.isBlank(operationId)) {
      ruleList = ruleService.list(order, pageable, Sort.Direction.fromString(direction));
      count = ruleService.count();
    } else {
      ruleList = ruleService.listByOperationId(operationId, order, pageable,
          Sort.Direction.fromString(direction));
      count = ruleService.countByOperation(operationId);
    }

    result.put("total", count);
    result.put("list", ruleList);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/yundou/rules/listDetail")
  public ResponseObject<Map<String, Object>> listDetail(String operationId,
      String order,
      Pageable pageable,
      String direction) {

    Map<String, Object> result = new HashMap<>();
    List<YundouRuleVO> ruleList;
    Integer count;

    if (StringUtils.isBlank(direction)) {
      direction = "ASC";
    }

    if (StringUtils.isBlank(operationId)) {
      ruleList = ruleService
          .listRuleDetail(null, order, pageable, Sort.Direction.fromString(direction));
      count = ruleService.count();
    } else {
      ruleList = ruleService.listRuleDetail(operationId, order, pageable,
          Sort.Direction.fromString(direction));
      count = ruleService.countByOperation(operationId);
    }

    result.put("total", count);
    result.put("list", ruleList);
    return new ResponseObject<>(result);
  }

}
