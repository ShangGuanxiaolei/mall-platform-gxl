package com.xquark.restapi.pintuan;


import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.model.PromotionTempStock;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.vo.BasePromotionCouponVO;
import com.xquark.dal.vo.PromotionGoodsDetailVO;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.FragmentAndDescController;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.cantuan.PieceCacheService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.pintuan.PromotionGoodsDetailService;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import com.xquark.service.pintuan.vo.PgSkuDetailVo;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.pricing.impl.pricing.impl.PiecePrice;
import com.xquark.service.product.ProductDescService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.sku.SkuAttributeService;
import io.vavr.collection.Stream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Supplier;

/**
 * @author LiHaoYang
 * @ClassName PromotionGoodsDetailController
 * @date 2018/9/8 0008
 */
@Controller
public class PromotionGoodsDetailController
    extends FragmentAndDescController {

  private final static Logger LOGGER = LoggerFactory.getLogger(PromotionPgMemberInfoController.class);
  @Autowired
  private PromotionGoodsDetailService promotionGoodsDetailService;
  @Autowired
  protected ProductService productService;
  @Autowired
  protected ProductDescService productDescService;
  @Autowired
  private SkuAttributeService skuAttributeService;
  @Autowired
  private CommentService commentService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  protected ShopService shopService;
  @Autowired
  private UrlHelper urlHelper;
  @Autowired
  private PromotionCouponService couponService;
  @Autowired
  private PieceCacheService pieceCacheService;
  @Autowired
  private PgPromotionServiceAdapter pgPromotionServiceAdapter;
  @Autowired
  private FlashSalePromotionProductService flashSaleService;
  @Autowired
  private PromotionPgMemberInfoService promotionPgMemberInfoService;
  @Autowired
  private FirstOrderService firstOrderService;

  @ResponseBody
  @RequestMapping(value = {"/promotionPgDetail/{id}/{pCode}"})
  @ApiOperation(value = "查看某个具体的活动商品", notes = "查看某个具体的活动商品信息", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> pgDetail(
      @PathVariable(value = "id") String productCode,
      @PathVariable(value = "pCode") String pCode,
      @RequestParam(value = "tranCode", required = false) String tranCode, HttpServletRequest req) {
    //1. 取拼团活动详细信息
    User user = (User) getCurrentIUser();
    Map<String, Object> objectMap = new HashMap<>();
    PromotionGoodsDetailVO goodsDetailVO = promotionGoodsDetailService
        .selectPromotionTimeAndMaxDiscount(productCode, pCode, user.getCpId());

    List<PromotionGoodsDetailVO> detailVOList = promotionGoodsDetailService
        .selectPeopleAndDiscountPrice(pCode, tranCode);

    for (PromotionGoodsDetailVO aDetailVOList : detailVOList) {
      final String detailCode = aDetailVOList.getpDetailCode();
      if (detailCode == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "detailCode不存在");
      }

      final PiecePrice piecePrice = getPiecePrice(detailCode, user);
      if (piecePrice != null) {
        final Integer priceLimit = piecePrice.getLimit();
        final BigDecimal memberPrice = piecePrice.getMemberPrice();
        aDetailVOList.setOpenMemberPrice(memberPrice);
        if (priceLimit != null && !pieceCacheService.isCanObtainOpenMemberPrice(String.valueOf(user.getCpId()), detailCode, priceLimit)) {
          aDetailVOList.setOpenMemberPrice(null);
        }
      }
    }
    List<Sku> promotionSku = this.selectSku(productCode);
    //2. 取到sku详情
    PgSkuDetailVo skuDetailVo = this.getSkuDetal(productCode, req, () -> {
      // 获取库存数
      // skuCode -> 库存详情
      return Stream.ofAll(pgPromotionServiceAdapter
            .listTempStock(pCode))
            .groupBy(PromotionTempStock::getSkuCode)
            .mapValues(Stream::last)
            .toJavaMap();
    });
    // 取活动价最低的价格体系, 覆盖商品的信息
    Stream.ofAll(detailVOList)
            .map(PromotionGoodsDetailVO::getPgPrice)
            .minBy(Comparator.comparing(PromotionPgPrice::getPromotionPrice))
            .forEach(p -> {
              BeanUtils.copyProperties(p, skuDetailVo);
              skuDetailVo.setDeductionDPoint(p.getPoint());
            });

    if (StringUtils.isNotEmpty(tranCode)) {

      PGTranInfoVo tranInfoVo = Optional
          .ofNullable(pgPromotionServiceAdapter.loadPGTranInfo(tranCode))
          .orElseThrow(BizException.ofSupplier(GlobalErrorCode.INVALID_ARGUMENT, "参团失败, 拼团不存在"));
      //查询成团有效时间
      int regimentTime = promotionPgMemberInfoService.selectRegimentTime(pCode);
      if (regimentTime == 0) {
        LOGGER.error("拼团 {} 信息不正确", tranCode);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
      }
      //查询开团时间
      Date groupOpenTime = promotionPgMemberInfoService.selectGroupOpenTime(tranCode);
      if (null == groupOpenTime) {
        LOGGER.error("拼团 {} 信息不正确", tranCode);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
      }

      //结束时间
      long date = regimentTime * 1000 * (60 * 60);
      Timestamp timestamp = new Timestamp(date + groupOpenTime.getTime());
      long time = timestamp.getTime();

      int jumpQueueTime = Optional.ofNullable(goodsDetailVO.getJumpQueueTime())
          .orElse(0);
      // 已成团, 且允许插队, 需要计算插队时间
      if (tranInfoVo.getGroupFinishTime() != null && jumpQueueTime > 0) {
        time = new Timestamp(
            jumpQueueTime * 1000 * 3600 + tranInfoVo.getGroupFinishTime().getTime())
            .getTime();
      }

      objectMap.put("endTime", time);

      //小程序拼团价格金额应该和拼团金额同步,不应该再取最低折扣
      if (!CollectionUtils.isEmpty(detailVOList)) {
        goodsDetailVO.setDisCountPrice(detailVOList.get(0).getDisCountPrice());
      }
    }

    //验证是否为白人，首次下单，再确定是否弹出新人专区 by tanggb
    Boolean firstOrder = firstOrderService.firstOrder(user.getCpId());
    skuDetailVo.setFirstOrder(firstOrder);

    objectMap.put("detailVOList", detailVOList);
    objectMap.put("promotionSku", promotionSku);
    objectMap.put("goodsDetailVO", goodsDetailVO);
    objectMap.put("skuDetail", skuDetailVo);
    return new ResponseObject<>(objectMap);
  }

  @ResponseBody
  private List<Sku> selectSku(String code) {
    return promotionGoodsDetailService.selectSku(code);
  }

  /**
   * 生成商品的图片URL
   */
  private void convertUrl(ProductVO vo) {
    if (null != vo.getImgs() && vo.getImgs().size() > 0) {
      for (ProductImageVO img : vo.getImgs()) {
        img.setImgUrl(img.getImg());
      }
    }
    vo.setCategory(categoryService.loadCategoryByProductId(vo.getId()));
  }

  private PgSkuDetailVo getSkuDetal(String id, HttpServletRequest req, Supplier<Map<String, PromotionTempStock>> pgStock) {

    ProductVO product = productService.load(id);
    if (product == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }

    //test
    convertUrl(product); // 生成商品的图片URL
    // 约定获取商品的图文信息的时候，从productService中获取，实现：先获取富文本信息，再获取片段信息
    setFragmentAndDescInfo(product);

    // 获取商品的多sku规格属性
    List<SkuAttributeVO> skuAttributeVOs = skuAttributeService.findAttributesByProductId(id);
    product.setSkuAttributes(skuAttributeVOs);

    List<BasePromotionCouponVO> availableCoupons = couponService
        .listBasePromotionCouponByProduct(product.getId());
    product.setCouponList(availableCoupons);

    // 计算积分兑换比例
    // 只在前端调用时计算
    IUser user = getCurrentIUser();
    Long cpId = userService.selectCpIdByUserId(user.getId());

    PgSkuDetailVo result = new PgSkuDetailVo(product, urlHelper.genProductUrl(
        product.getId() + "?upline=" + cpId),
        product.getImg(), null);

    //---------------------计算商品的多sku排序逻辑
    if(result.getSkus().size()>1){
      result.setSkus(skuAttributeService.sortSkuList(result));
    }
    // 放入评价统计信息
    Map<String, Long> commentTotalMap = commentService.commentTotalMap(product.getId());
    result.setCommentTotalMap(commentTotalMap);

    //用第一个sku的数据覆盖商品的规格数据
    if (CollectionUtils.isNotEmpty(result.getSkus())) {
      Sku sku = result.getSkus().get(0);
      result.setDeductionDPoint(sku.getDeductionDPoint());
      result.setNetWorth(sku.getNetWorth());
      result.setPoint(sku.getPoint());
      result.setPrice(sku.getPrice());
      result.setAmount(sku.getAmount());
    }

    final Map<String, PromotionTempStock> tempStockMap = pgStock.get();
    // 设置规格的库存为拼团库存
    if (MapUtils.isNotEmpty(tempStockMap)) {
      result.getSkus().forEach(s -> s.setAmount(tempStockMap.get(s.getSkuCode()).getUsableSkuNum()));
    }

    return result;
  }

  public IUser getCurrentIUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        IUser user = (IUser) principal;
        if (!user.isAnonymous()) {
          return user;
        }
      }

      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  private PiecePrice getPiecePrice(String detailCode, User user) {
    return pgPromotionServiceAdapter.getPiecePrice(detailCode, user.getCpId());
  }

}
