package com.xquark.restapi.utils;


import java.util.Set;
import javax.validation.ConstraintViolation;

/**
 * User: huangjie Date: 2018/6/11. Time: 下午3:41 校验工具类
 */
public class ValidationUtils {


  /**
   * 从校验返回的错误集合中获取对应的错误信息
   */
  public static <T> String extractMessage(Set<ConstraintViolation<T>> messageSet) {
    StringBuilder messageBuilder = new StringBuilder();
    for (ConstraintViolation<T> violation : messageSet) {
      messageBuilder.append(" ");
      messageBuilder.append(violation.getPropertyPath().toString() + violation.getMessage());
    }
    return messageBuilder.toString();
  }

}
