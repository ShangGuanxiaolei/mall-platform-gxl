package com.xquark.restapi.supplier;

import com.google.common.base.Optional;
import com.xquark.dal.model.Supplier;
import com.xquark.dal.model.SupplierWareHouse;
import com.xquark.dal.type.ErpServiceDestinationFactory;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.base.BaseController;
import com.xquark.restapi.supplier.form.SupplierForm;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.order.util.ProxyContext;
import com.xquark.service.order.util.impl.InitMethodStrategy;
import com.xquark.service.supplier.SupplierService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: jitre Date: 18-6-14. Time: 下午3:55
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController extends BaseController<Supplier, SupplierService> {

  private SupplierService service;

  @Autowired
  public void setService(SupplierService service) {
    this.service = service;
  }

  /**
   * 保存与修改
   */
  @RequestMapping(value = "/edit", params = {"type=override"})
  public ResponseObject<Boolean> insert(@Valid @RequestBody SupplierForm obj) {
    Boolean isSuccess;

    Optional<List<SupplierWareHouse>> list = Optional.fromNullable(obj.getWareHouseList());
    if (StringUtils.isNotBlank(obj.getId())) {
      isSuccess = service.update(obj, list.or(Collections.<SupplierWareHouse>emptyList()));
    } else {
      if (SkuCodeResourcesType.getSkuCodeResourceBySupplierCode(obj.getCode()).isPresent()) {
        return new ResponseObject<>(false);
      }
      isSuccess = service.insert(obj, list.or(Collections.<SupplierWareHouse>emptyList()));
    }
    if (isSuccess) {
      Supplier supplier = new Supplier();
      supplier.setIfMappingToEnum(true);
      supplier.setCode(obj.getCode());
      supplier.setArchive(true);
      supplier.setName(obj.getName());
      new InitMethodStrategy(new ProxyContext(null, null, null,
              supplier, null))
              .execute();
    }
    return new ResponseObject<>(isSuccess);
  }


  /**
   * 分页查询
   */
  @RequestMapping(value = "/list", params = {"type=override"})
  public ResponseObject<Map<String, Object>> list(SearchForm searchForm, Pageable page,
      boolean pageable) {
    Map<String, Object> params = SearchFormUtils
        .checkAndGenerateMapParams(searchForm, page, pageable);
    List<? extends Supplier> list = getService().list(params);
    //判断供应商是否在系统的skuCodeResourceType中存在映射
    for (Supplier s : list) {
      java.util.Optional<SkuType> type = SkuCodeResourcesType
              .getSkuCodeResourceBySupplierCode(s.getCode());
      boolean isMapped = false;
      if (type.isPresent()) {
        isMapped = true;
      }
      s.setIfMappingToEnum(isMapped);
    }
    Long num = getService().count(params);
    Map<String, Object> listAndNum = new HashMap<>();
    listAndNum.put("list", list);
    listAndNum.put("total", num);
    return new ResponseObject<>(listAndNum);
  }

  @Override
  protected SupplierService getService() {
    return service;
  }

}
