package com.xquark.restapi.freshmanLottery;

import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.PointServiceApi;
import com.xquark.dal.model.FreshmanLottery;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.lottery.FreshmanLotteryService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 新人首单抽奖结果展示层
 */
@RestController
@RequestMapping("/freshman")
public class FreshmanLotteryController extends BaseController {

    private static final Log log = LogFactory.getLog(FreshmanLotteryController.class);

    @Autowired
    private FreshmanLotteryService freshmanLotteryService;

    private PointServiceApi pointServiceApi;

    /**
     * 新人首单抽奖, 后台计算抽奖, 返回抽奖结果
     * @return
     */
    @RequestMapping("/lottery")
    public ResponseObject<Map<String, Object>> freshmanFirstDraw() {
        User user = (User) getCurrentIUser();
        Integer lotteryCount = freshmanLotteryService.queryFreshmanLotteryCountByCpid(user.getCpId());
        if (lotteryCount >= 1) {
            throw new BizException(GlobalErrorCode.FRESHMAN_LOTTERY_LIMIT, "抽奖次数已用完,不能在进行抽奖!");
        }
        Map<String, Object> resultMap = new HashMap<>();
        Object[][] lotteryPool = initLotteryPool();
        Object[] award = award(lotteryPool);
        Integer resultLog = freshmanLotteryService.insertFreshmanLotteryLog(user.getCpId().intValue(), (Integer) award[1]);
        if (resultLog > 0) {
            resultMap.put("lotteryId", award[0]);
            resultMap.put("point", award[1]);
            Integer lotteryPoint = (Integer) award[1]; //
            pointServiceApi.grant(user.getCpId(), "抽奖", Trancd.LOTTERY_F, PlatformType.E, new BigDecimal(lotteryPoint));
        }
        return new ResponseObject<>(resultMap);
    }

    @Autowired
    public void setPointService(PointContextInitialize initialize) {
        this.pointServiceApi = initialize.getPointServiceApi();
    }

    // 初始化抽奖池
    private Object[][] initLotteryPool() {
        Object[][] prizeArr = new Object[0][];
        int coount = 0;
        List<FreshmanLottery> freshmanLotteryList = freshmanLotteryService.getAllFreshmanLotteryIsOpen();
        if (null != freshmanLotteryList && freshmanLotteryList.size() > 0 ) {
            prizeArr = new Object[freshmanLotteryList.size()][];
            for (FreshmanLottery freshmanLottery : freshmanLotteryList) {
                Object[] prizeRules = new Object[3];
                prizeRules[0] = freshmanLottery.getId();
                prizeRules[1] = freshmanLottery.getPoint();
                prizeRules[2] = freshmanLottery.getProbability();
                prizeArr[coount++] = prizeRules;
            }
        }
        return prizeArr;
    }

    //抽奖后返回奖品等级及奖品描述
    public static Object[] award(Object[][] prizeArr){
        //概率数组
        float obj[] = new float[prizeArr.length];
        int count = 0;
        for (float probability : obj) {
            obj[count] = Float.parseFloat(prizeArr[count][2].toString());
            count ++;
        }
        Integer prizeId = getRand(obj); //根据概率获取奖项号码
        Integer point = (Integer) prizeArr[prizeId][1];//德分
        int lotteryId = prizeId + 1;
        return new Object[]{lotteryId, point};
    }


    //根据概率获取奖项
    public static Integer getRand(float obj[]){
        Integer result = null;
        try {
            float sum = 0.0f;//概率数组的总概率精度
            float min = 0.0f;//
            for(int i=0;i<obj.length;i++){
                BigDecimal beforSum = new BigDecimal(Float.toString(sum));
                BigDecimal objValue = new BigDecimal(Float.toString(obj[i]));
                sum = beforSum.add(objValue).floatValue();
            }
            for(int i=0;i<obj.length;i++){//概率数组循环
                BigDecimal db = new BigDecimal(Math.random() * (sum - min) + min);
                BigDecimal b = new BigDecimal(Float.toString(obj[i]));
                if(compareMethod(db,b) == -1){//中奖
                    result = i;
                    break;
                }else{
                    sum -=obj[i];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int compareMethod(BigDecimal a,BigDecimal b){
        return a.compareTo(b);
    }

}

