package com.xquark.restapi.utils;

/**
 * User: byy Date: 18-6-4. Time: 下午8:56 组合表单
 */
public class SearchForm {

  private String keyWord;

  private String order;

  private String direction;


  public String getKeyWord() {
    return keyWord;
  }

  public void setKeyWord(String keyWord) {
    this.keyWord = keyWord;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }


  @Override
  public String toString() {
    return "SearchForm{" +
        "keyWord='" + keyWord + '\'' +
        ", order='" + order + '\'' +
        ", direction='" + direction + '\'' +
        '}';
  }
}
