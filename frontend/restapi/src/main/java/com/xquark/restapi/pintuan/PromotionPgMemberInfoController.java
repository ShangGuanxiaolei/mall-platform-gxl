package com.xquark.restapi.pintuan;

import com.xquark.dal.mapper.CustomerCareerLevelMapper;
import com.xquark.dal.model.CustomerCareerLevel;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.User;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.promotion.PromotionBaseInfoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LiHaoYang
 * @ClassName PromotionPgMemberController
 * @date 2018/9/10 0010
 */
@Controller
public class PromotionPgMemberInfoController extends BaseController {

  private final static Logger LOGGER = LoggerFactory.getLogger(PromotionPgMemberInfoController.class);

  @Autowired
  private PromotionPgMemberInfoService promotionPgMemberInfoService;

  @Autowired
  private PromotionPgService promotionPgService;

  @Autowired
  private CustomerCareerLevelMapper customerCareerLevelMapper;

  @Autowired
  private CustomerProfileService profileService;

  @Autowired
  private PgPromotionServiceAdapter promotionServiceAdapter;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;


  /**
   * 开团信息
   */
  @RequestMapping("/promotionPgMember/selectMemberInfoAndTime")
  @ResponseBody
  public ResponseObject<Map<String, Object>> selectMemberInfoAndTime(String bizNo, String orderNo) {

    PromotionOrderGoodsInfoVO pov = promotionPgMemberInfoService.loadByOrderNO(orderNo);
    if (pov == null) {
      pov = promotionPgMemberInfoService.loadByBizNo(bizNo);
      // 如果bizNo能取到数据, 则把bizNo对应的订单号拿出来
      orderNo = promotionPgMemberInfoService.loadMainOrderNoByBizNo(bizNo);
    }
    if (pov == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不存在");
    }
    //根据bizNo获取tranCode
    String tranCode = pov.getPieceGroupTranCode();
    ////根据bizNo获取pCode
    String pCode = pov.getpCode();

    //拼团总人数
    int count = promotionPgMemberInfoService.selectPgMemberCount(tranCode);

    //是否是新人团
    boolean isNewPg = promotionPgService.PromotionNewPg(pCode);
    if (count == 0) {
      LOGGER.error("拼团 {} 信息不正确", tranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
    }
    User currUser = (User) getCurrentIUser();
    //当前拼团已有人数
    int beginCount = promotionPgMemberInfoService.selectSuccessPromotionMemberCountAndOrder(
            tranCode, orderNo);
    //拼团成员信息
    //List<PromotionPgMemberInfoVO> memberInfo= promotionPgMemberInfoService.selectMemberInfo(tranCode);
    //查询成团有效时间
    int regimentTime = promotionPgMemberInfoService.selectRegimentTime(pCode);
    if (regimentTime == 0) {
      LOGGER.error("拼团 {} 信息不正确", tranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
    }
    //查询开团时间
    Date groupOpenTime = promotionPgMemberInfoService.selectGroupOpenTime(tranCode);

    //结束时间
    long date = regimentTime * 1000 * (60 * 60);
    Timestamp timestamp = new Timestamp(date + groupOpenTime.getTime());
    long time = timestamp.getTime();

    //查询当前拼团的商品信息
    PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoService
            .selectGoodsInfoWithOrder(tranCode, currUser.getCpId(), orderNo);

    //当前拼团的状态
    String tranStatus = promotionPgMemberInfoService.selectStatus(tranCode);
    String cpId = String.valueOf(currUser.getCpId());
    boolean isJoin = this.isJoin(cpId, tranCode);
    boolean isPureWhite = profileService.isFirstOrder(cpId);
    Map<String, Object> map = new HashMap<>();
    map.put("isJoin", isJoin);
    map.put("regimentTime", time);
    map.put("groupOpenTime", groupOpenTime);
    map.put("MemberCount", count);
    map.put("tranStatus", tranStatus);
    map.put("OrderGoods", promotionOrderGoodsInfoVO);
    map.put("beginCount", beginCount);
    map.put("remainCount", count - beginCount);
    map.put("isNewPg", isNewPg);
    map.put("isPureWhite", isPureWhite);
    //拼团人员的状态：true满；false不满
    //拼团时间校验
//        Boolean vIsPieceOverTime;
//        PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
//        promotionBaseInfo.setpCode(pCode);
//        StatusAndTimesVO checkTime = checkStatusAndTimesService.check(promotionBaseInfo);
//        Timestamp timestamp = new Timestamp((new Date()).getTime());
//        if(timestamp.after(checkTime.getEffectTo())){
//            vIsPieceOverTime = true;
//        }
//        vIsPieceOverTime = false;
    map.put("vIsMemberFull", beginCount - count >= 0);
    return new ResponseObject<>(map);
  }

  /**
   * 参团信息
   */
  @RequestMapping("/promotionPgMember/selectJoinInfo")
  @ResponseBody
  public ResponseObject<Map<String, Object>> selectJoinInfo(String tranCode) {
    Map<String, Object> map = new HashMap<>();
    User user = (User) getCurrentIUser();
    String cpId = String.valueOf(user.getCpId());

    //查询商品信息
    PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoService
            .selectGoodsInfo(tranCode);
    String pCode = promotionOrderGoodsInfoVO.getpCode();
    //判断用户是否在拼团中
    boolean isJoin = this.isJoin(cpId, tranCode);
    //查询当前拼团的拼团状态
    String tranStatus = promotionPgMemberInfoService.selectStatus(tranCode);
    //查询成团有效时间
    int regimentTime = promotionPgMemberInfoService.selectRegimentTime(pCode);
    //查询开团时间
    Date groupOpenTime = promotionPgMemberInfoService.selectGroupOpenTime(tranCode);

    //结束时间
    long date = regimentTime * 1000 * (60 * 60);
    Timestamp timestamp = new Timestamp(date + groupOpenTime.getTime());
    long time = timestamp.getTime();

    //当前拼团已有人数
    int beginCount = promotionPgMemberInfoService.selectSuccessPromotionMemberCount(tranCode);

    //拼团总人数
    int count = promotionPgMemberInfoService.selectPgMemberCount(tranCode);

    CustomerCareerLevel customerCareerLevel = customerCareerLevelMapper.selectByPrimaryKey(getCurrentUser().getCpId());
    //当前用户的身份，如果是纯白人，参团成功则可以拼团
    boolean isCanPg = false;
    boolean isNewPg = promotionPgService.PromotionNewPg(pCode);

    if (customerCareerLevel != null) {
      CareerLevelType careerLevelType = CareerLevelType.getFinalLevelType(customerCareerLevel.getHdsType(), customerCareerLevel.getViviLifeType());
      boolean hasWeek = profileService.getUplineWeekIgnoreIdentity(user.getCpId()) != null;
      if (careerLevelType == CareerLevelType.RC && !hasWeek) {
        careerLevelType = CareerLevelType.PW;//纯白人
        isCanPg = true;
      }

      if (!isJoin) {//未参团才做参团身份校验
        String joinGroup = promotionPgService.selectCanJoinGroup(pCode);
        if (joinGroup == null) {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团身份不正确");
        }

        if (!joinGroup.contains(careerLevelType.name())) {//用户身份不在参团身份里面
          if (joinGroup.equals(CareerLevelType.PW.name())) {//新人团
            throw new BizException(GlobalErrorCode.PIECE_IDENTITY_NOT_MATCH, "新人团只对新人开放呦！");
          }

          if (joinGroup.contains(CareerLevelType.DS.name()) || joinGroup.contains(CareerLevelType.SP.name())) {//会员团
            throw new BizException(GlobalErrorCode.PIECE_IDENTITY_NOT_MATCH, "此次活动仅面向会员参加请继续关注汉薇|我要成为会员");
          }

          if (joinGroup.contains(CareerLevelType.RC.name())) {//白人可参团
            if (profileService.hasIdentity(Long.valueOf(cpId))) {
              throw new BizException(GlobalErrorCode.PIECE_IDENTITY_NOT_MATCH, "此次活动仅面向非会员参加请继续关注汉薇");
            }
            throw new BizException(GlobalErrorCode.PIECE_IDENTITY_NOT_MATCH, "此次活动仅面向会员参加请继续关注汉薇|我要成为会员");
          }
        }
      }
    }

    PromotionBaseInfo baseInfo = promotionBaseInfoService.selectByPCode(pCode);

    boolean isPureWhite = profileService.isFirstOrder(cpId);
    map.put("orderInfo", promotionOrderGoodsInfoVO);
    map.put("isJoin", isJoin);
    map.put("regimentTime", time);
    map.put("groupOpenTime", groupOpenTime);
    map.put("tranStatus", tranStatus);
    map.put("count", count);
    map.put("beginCount", beginCount);
    map.put("isCanPg", isCanPg);
    map.put("isNewPg", isNewPg);
    map.put("isPureWhite", isPureWhite);
    // 是否在插队拼团有效期内, 只有未参过团才会使用该标记
    map.put("inCutLine", !isJoin && StringUtils.isNotBlank(tranCode)
        && promotionServiceAdapter.isInCutLineTime(pCode,
        promotionOrderGoodsInfoVO::getGroupFinishTime));
    map.put("canShareCutLine", StringUtils.isNotBlank(tranCode)
            && "5".equals(baseInfo.getpStatus()) && promotionServiceAdapter.isInCutLineTime(pCode,
            promotionOrderGoodsInfoVO::getGroupFinishTime));

    return new ResponseObject<>(map);

  }

  //判断用户是否在拼团中
  private boolean isJoin(String cpId, String tranCode) {
    PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoService.selectGoodsInfo(tranCode);
    List<PromotionPgMemberInfoVO> memberInfo = promotionOrderGoodsInfoVO.getMemberInfo();
    for (PromotionPgMemberInfoVO aMemberInfo : memberInfo) {
      if (cpId.contains(aMemberInfo.getMemberId())) {
        return true;
      }
    }
    return false;
  }

}
