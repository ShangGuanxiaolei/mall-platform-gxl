package com.xquark.restapi.product;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

import java.util.List;

public class ForbrandListVO {

  private String categoryId;
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;
  List<ProductVOEx> list;

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public List<ProductVOEx> getList() {
    return list;
  }

  public void setList(List<ProductVOEx> list) {
    this.list = list;
  }
}
