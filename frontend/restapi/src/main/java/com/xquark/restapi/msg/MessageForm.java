package com.xquark.restapi.msg;

/**
 * Created by wangxinhua on 17-11-28. DESC:
 */
public class MessageForm {

  private String id;

  private String title;

  private String content;

//    @NotNull(message = "发送范围不能为空")
//    private SendScope scope;

  /**
   * 立即发送
   */
//    private boolean sendNow;

//    private List<String> userIds;
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


}
