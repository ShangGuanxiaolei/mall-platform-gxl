package com.xquark.restapi.fullCut;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionFullCut;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionFullCutProductInfoVO;
import com.xquark.dal.vo.PromotionFullCutProductVO;
import com.xquark.dal.vo.PromotionFullCutVO;
import com.xquark.dal.vo.PromotionReduceOrderVO;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.PromotionFullCutFormVO;
import com.xquark.restapi.product.PromotionFullCutProductFormVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fullReduce.fullCut.PromotionFullCutService;
import com.xquark.service.pricing.PromotionService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-9-21. DESC:
 */
@RestController
@RequestMapping("/promotionFullCut")
public class PromotionFullCutController extends BaseController {

  private PromotionService promotionService;

  private PromotionFullCutService promotionFullCutService;

  @Autowired
  public void setPromotionService(PromotionService promotionService) {
    this.promotionService = promotionService;
  }

  @Autowired
  public void setPromotionFullCutService(PromotionFullCutService promotionFullCutService) {
    this.promotionFullCutService = promotionFullCutService;
  }

  @Deprecated
  // 沁园商城项目使用下面的保存方法
//    @RequestMapping(value = "/savePromotion", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<String> save(@RequestBody PromotionFullCutVO promotion) {
    String result = promotionFullCutService.save(promotion);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/savePromotion", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> savePromotion(@RequestBody PromotionFullCutFormVO promotion) {
    PromotionFullCutProductVO toSave = Transformer.fromBean(promotion,
        PromotionFullCutProductVO.class);

    List<PromotionFullCut> fullCuts;
    final String promotionId = promotion.getId();
    PromotionType type = promotion.getType();
    PromotionScope scope = promotion.getScope();
    if (scope == PromotionScope.PRODUCT) {
      List<PromotionFullCutProductFormVO> productForms = promotion.getProductDetails();
      if (CollectionUtils.isEmpty(productForms)) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请指定自定义折扣");
      }
      fullCuts = Transformer.fromIterable(productForms,
          new Function<PromotionFullCutProductFormVO, PromotionFullCut>() {
            @Override
            public PromotionFullCut apply(
                PromotionFullCutProductFormVO form) {
              return new PromotionFullCut(promotionId, form.getProductId(),
                  form.getDiscount());
            }
          });
    } else {
      boolean hasActivated = promotionService.hasActivate(type);
      if (hasActivated) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            "您已经配置该类型活动，请先结束其他活动再设置全场活动");
      }
      fullCuts = Collections.emptyList();
    }
    toSave.setProducts(fullCuts);
    boolean result = promotionFullCutService.save(toSave);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/preSave", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Promotion> preSave(@RequestBody Promotion promotion) {
    Promotion result = promotionFullCutService.preSave(promotion);
    if (result != null) {
      return new ResponseObject<>(result);
    }
    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "保存失败");
  }

  @RequestMapping("/listPromotion")
  public ResponseObject<Map<String, Object>> listTableNew(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      String keyword,
      Pageable pageable) {
    List<Promotion> promotions = promotionFullCutService.listPromotion(order,
        Direction.fromString(direction), pageable, keyword);
    Long total = promotionFullCutService.countPromotion(keyword);
    Map<String, Object> result = ImmutableMap.of("list", promotions,
        "total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/listTable")
  public ResponseObject<Map<String, Object>> listTable(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable,
      String keyWord) {
    List<PromotionFullCutVO> list;
    Long total;
    try {
      list = promotionFullCutService
          .listPromotionVO(order, Sort.Direction.fromString(direction), pageable,
              keyWord);
      total = promotionFullCutService.countPromotion(keyWord);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String error = "满减活动加载失败";
      log.error(error, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, error);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/deleteProduct")
  public ResponseObject<Boolean> deleteProduct(@RequestParam("promotionId") String promotionId,
      @RequestParam("productId") String productId) {
    Boolean result = promotionFullCutService.deleteProduct(promotionId, productId);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/addProduct")
  public ResponseObject<Boolean> addProduct(PromotionFullCut promotionFullCut) {
    Boolean result = promotionFullCutService.addProduct(promotionFullCut);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/listProduct")
  public ResponseObject<Map<String, Object>> listProductTable(
      @RequestParam("id") String id, Pageable pageable) {
    List<PromotionFullCutProductInfoVO> list;
    Long total;
    if (StringUtils.isBlank(id)) {
      id = null;
    }
    try {
      list = promotionFullCutService.listVOWithProductInfo(id, pageable);
      total = promotionFullCutService.countVOWithProductInfo(id);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "折扣商品加载失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }

    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/close")
  public ResponseObject<Boolean> close(@RequestParam("id") String id) {
    boolean result = promotionFullCutService.close(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/orderDetail")
  public ResponseObject<Map<String, Object>> listOrderDetail(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction,
      Pageable pageable) {
    List<PromotionReduceOrderVO> details;
    Long total;
    try {
      details = promotionFullCutService
          .listOrderDetail(order, Sort.Direction.fromString(direction), pageable);
      total = promotionFullCutService.countOrderDetail();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "加载明细数据出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", details);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/deleteDiscount")
  public ResponseObject<Boolean> deleteDiscount(@RequestParam String promotionId,
      @RequestParam String id) {
    long discountCount = promotionFullCutService.countDiscount(promotionId);
    if (discountCount <= 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "折扣数不能小于一个");
    }
    boolean result = promotionFullCutService.deleteDiscount(promotionId, id);
    return new ResponseObject<>(result);
  }

  /**
   * 获取商品详情
   */
  @RequestMapping("/view/{id}")
  public ResponseObject<PromotionFullCutProductVO> loadInfo(@PathVariable String id) {
    PromotionFullCutProductVO result = promotionFullCutService.loadProductVO(id);
    return new ResponseObject<>(result);
  }

}
