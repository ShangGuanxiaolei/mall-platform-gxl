package com.xquark.restapi.product;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.type.PromotionApplyStatus;
import com.xquark.dal.vo.PreOrderPromotionOrderVO;
import com.xquark.dal.vo.PreOrderPromotionProductVO;
import com.xquark.dal.vo.PreOrderPromotionUserVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.promotion.impl.PreOrderPromotionProductService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-11-22. DESC:
 */
@RestController
@RequestMapping("/preOrder")
@Api(value = "预购活动、商品接口")
public class PreOrderPromotionController extends BaseController {

  private final PreOrderPromotionProductService preOrderPromotionService;

  @Autowired
  public PreOrderPromotionController(
      PreOrderPromotionProductService preOrderPromotionService) {
    this.preOrderPromotionService = preOrderPromotionService;
  }

  @RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
  @ApiOperation(value = "保存预购商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> savePreOrder(@Validated PreOrderForm form,
      @ApiIgnore Errors errors) {
    ControllerHelper.checkException(errors);
    boolean result = preOrderPromotionService
        .savePromotionProduct(form.getPromotion(), form.getPromotinProduct());
    return new ResponseObject<>(result);
  }

  /**
   * 获取预购商品列表
   *
   * @param isAvailable 是否值查询当前时间有效的商品
   * @param order 排序
   * @param direction 方向
   * @param pageable 分页
   */
  @RequestMapping(value = "/list", method = RequestMethod.GET)
  @ApiOperation(value = "获取预购商品列表", notes = "若首页获取则isAvailable为true过滤掉无效商品", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> listProducts(
      @ApiParam(value = "isAvailable", defaultValue = "false") boolean isAvailable,
      @ApiParam(value = "order", defaultValue = "sales", allowableValues = "created_at, sales") String order,
      @ApiParam(value = "direction", defaultValue = "DESC") @RequestParam(defaultValue = "DESC") String direction,
      Pageable pageable) {
    List<PreOrderPromotionProductVO> list;
    Long total;
    try {
      list = preOrderPromotionService
          .listAllPromotionProductVOs(order, Direction.fromString(direction), pageable,
              isAvailable);
      total = preOrderPromotionService.countAllProducts(isAvailable);
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取预购商品列表失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", total);
    return new ResponseObject<>(result);
  }

  @ApiOperation(value = "后台查询所有用户的预购活动申请", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
  @RequestMapping(value = "/listApply", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listApplyOrderVO(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<PreOrderPromotionOrderVO> list;
    Long count;
    try {
      list = preOrderPromotionService.listOrderVO(order, Direction.fromString(direction), pageable);
      count = preOrderPromotionService.countOrderVO();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      String msg = "获取预购申请列表失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<>();
    result.put("list", list);
    result.put("total", count);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/view/{id}")
  @ApiOperation(value = "查看活动商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<PreOrderPromotionProductVO> view(@PathVariable("id") String id) {
    PreOrderPromotionProductVO vo = (PreOrderPromotionProductVO) preOrderPromotionService
        .loadPromotionProductVO(id);
    return new ResponseObject<>(vo);
  }

  @RequestMapping(value = "/viewProduct/{id}")
  @ApiOperation(value = "查看活动商品详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<PreOrderPromotionUserVO> viewProduct(
      @ApiParam(name = "id", value = "通过list接口得到的id") @PathVariable("id") String id) {
    PreOrderPromotionUserVO userVO = preOrderPromotionService
        .loadUserVO(id, getCurrentIUser().getId());
    return new ResponseObject<>(userVO);
  }

  @RequestMapping(value = "/close")
  @ApiOperation(value = "关闭活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> close(String id) {
    boolean result = preOrderPromotionService.close(id);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/checkInPromotion")
  @ApiOperation(value = "检查商品是否已经参与了活动", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> inPromotion(String productId) {
    boolean result = preOrderPromotionService.inPromotion(productId);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/audit")
  @ApiOperation(value = "更新申请状态", notes = "必须为后台用户才可以操作", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> updateStatus(@RequestParam("applyId") String applyId,
      @RequestParam("status") String status) {
    IUser user = getCurrentIUser();
    if (!(user instanceof Merchant)) {
      throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "该操作只有管理员可以执行");
    }
    boolean result = preOrderPromotionService
        .auditApply(applyId, PromotionApplyStatus.valueOf(status));
    return new ResponseObject<>(result);
  }

}
