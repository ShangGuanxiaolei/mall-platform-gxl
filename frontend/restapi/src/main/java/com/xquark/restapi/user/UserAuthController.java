package com.xquark.restapi.user;

import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.authentication.AppAuthenticationException;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.msg.PushMessageService;
import com.xquark.biz.url.UrlHelper;
import com.xquark.biz.verify.VerificationFacade;
import com.xquark.biz.verify.impl.VerificationFacadeImpl.SmsType;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.IUser;
import com.xquark.dal.consts.WechatConstants;
import com.xquark.dal.mapper.CustomerCareerLevelMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.dal.type.ClientModuleType;
import com.xquark.dal.vo.IdentityVO;
import com.xquark.dal.vo.UserIdentityVO;
import com.xquark.dal.vo.UserInfoVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.bank.CustomerBankService;
import com.xquark.service.cart.CartService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.healthTest.HealthTestProfileService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.UserService;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.userAgent.impl.UserSigninLogFactory;
import com.xquark.service.wxservice.config.WeChatConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jamesp
 */
@RestController
@ApiIgnore
public class UserAuthController extends BaseController {

  private static final String SMS_VERIFIED = "sms_verified";

  @Autowired
  private UserSigninLogService userSigninLogService;

  @Autowired
  private VerificationFacade veriFacade;

  @Autowired
  private MessageService messageService;

  @Autowired
  private PushMessageService pushMessageService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private CartService cartService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private HealthTestProfileService profileService;

  @Autowired
  private CustomerBankService customerBankService;

  @Autowired
  private CustomerCareerLevelMapper customerCareerLevelMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private PointGiftService pointGiftService;

  @Value("${site.web.host.name}")
  private String domainName;

  @Autowired
  UrlHelper urlHelper;

  @Autowired
  private RememberMeServices rememberMeServices;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private CareerLevelService careerLevelService;

  /**
   * 检查用户手机号是否注册 http://localhost:8888/v2/registered?mobile=15968118387
   * 或 检查当前用户用户是否绑定手机号
   */
  @RequestMapping("/registered")
  public ResponseObject<Boolean> isRegistered(String mobile) {
    if (StringUtils.isEmpty(mobile)) {
      try {
        IUser currUser = getCurrentIUser();
        User user = userService.load(currUser.getId());
        return new ResponseObject<>(!StringUtils.isEmpty(user.getPhone()));
      } catch (Exception e) {
        log.error("用户未登录, 无法获取手机号");
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "未指定手机号, 清先登录或传递手机号参数");
      }
    }
    return new ResponseObject<>(userService.isRegistered(mobile));
  }

  /**
   * 注册发送验证码 http://localhost:8888/v2/send-sms-code?mobile=15968118380
   *
   * @param form with mobile
   * @return 是否已发送sms
   */
  @RequestMapping("/send-sms-code")
  public ResponseObject<Boolean> sendSmsCode(
          @Valid @ModelAttribute UserForm form, Errors errors, HttpServletRequest request) {
    ControllerHelper.checkException(errors);
    String thirdSessionId = request
            .getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
    veriFacade.generateCodeForMiniProgram(form.getMobile(), thirdSessionId);
    return new ResponseObject<>(Boolean.TRUE);
  }
  /**
   * @Author chp
   * @Description 不需要权限的验证码
   * @Date
   * @Param
   * @return
   **/
  @RequestMapping("/sendSmsCode")
  public ResponseObject<Boolean> sendSmsCodeH5(
          @Valid @ModelAttribute UserForm form, Errors errors, HttpServletRequest request) {
    ControllerHelper.checkException(errors);
    String thirdSessionId = request
            .getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
    boolean b = veriFacade.generateCodeForMiniProgram(form.getMobile(), thirdSessionId);
    ResponseObject<Boolean> res = new ResponseObject<>();
     res.setData(b);
     res.setMoreInfo(WeChatConfig.smsCode);
    return res;
  }

  /**
   * 密码修改，校验验证码
   */
  @RequestMapping("/pwdModify/verify")
  public ResponseObject<Boolean> pwdModifyVerify(@Valid @ModelAttribute UserForm form,
                                                 Errors errors, HttpServletRequest req, HttpServletResponse resp) {
    return checkSms(form, errors, req, resp, SmsType.MODIFY_PWD);
  }
   /**
    * @Author chp
    * @Description //dna验证码校验
    * @Date
    * @Param
    * @return
    **/
  @RequestMapping("/register/verifyDna")
  public ResponseObject<Boolean> registerVerifyDna(@Valid @ModelAttribute UserForm form,
                                                Errors errors, HttpServletRequest req, HttpServletResponse resp) {
    return checkSms(form, errors, req, resp, SmsType.SIGN);
  }


  /**
   * 注册  step1:校验验证码 http://localhost:8888/v2/register/verify?mobile=15968118380&smsCode=814813
   *
   * @param form: mobile + smscode
   */
  @RequestMapping("/register/verify")
  public ResponseObject<Boolean> registerVerify(@Valid @ModelAttribute UserForm form,
                                                Errors errors, HttpServletRequest req, HttpServletResponse resp) {
    return checkSms(form, errors, req, resp, SmsType.SIGN);
  }

//  private ResponseObject<Boolean> checkSmsDNA(UserFormDna form, Errors errors, HttpServletRequest req,
//                                           HttpServletResponse resp, SmsType smsType,HttpSession session) {
//    req.getSession().setAttribute(SMS_VERIFIED, Boolean.FALSE);
//    if (errors.hasErrors() || !StringUtils.hasText(form.getSmsCode())) {
////			return new ResponseObject<Boolean>(false);
//      throw new BizException(GlobalErrorCode.ERROR_PARAM, "请输入正确的验证码");
//      //RequestContext requestContext = new RequestContext(req);
//      //ControllerHelper.checkException(errors,
//      //requestContext.getMessage("register.smscode.not.null")); // error
//    }
//    if (errors.hasErrors() || !StringUtils.hasText(form.getCpid())) {
////			return new ResponseObject<Boolean>(false);
//      throw new BizException(GlobalErrorCode.ERROR_PARAM, "cpid不能为空");
//      //RequestContext requestContext = new RequestContext(req);
//      //ControllerHelper.checkException(errors,
//      //requestContext.getMessage("register.smscode.not.null")); // error
//    }
//    Boolean valid = veriFacade.verifyCodeDNA(form.getCpid(),form.getMobile(), form.getSmsCode(), smsType,session);
//
//    if (!valid) {
//      RequestContext requestContext = new RequestContext(req);
//      log.error("register verify error, invalid smscode, mobile:" + form.getMobile()
//              + " smsCode:" + form.getSmsCode());
//      //throw new BizException(GlobalErrorCode.UNKNOWN,
//      //requestContext.getMessage("register.smscode.not.valid"));
////			return new ResponseObject<Boolean>(false);
//      throw new BizException(GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR, "请输入正确的验证码");
//    }
//
//    req.getSession().setAttribute(SMS_VERIFIED, Boolean.TRUE);
//    return new ResponseObject<>(true);
//
//  }




  private ResponseObject<Boolean> checkSms(UserForm form, Errors errors, HttpServletRequest req,
                                           HttpServletResponse resp, SmsType smsType) {
    req.getSession().setAttribute(SMS_VERIFIED, Boolean.FALSE);
    if (errors.hasErrors() || !StringUtils.hasText(form.getSmsCode())) {
//			return new ResponseObject<Boolean>(false);
      throw new BizException(GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR, "请输入正确的验证码");
      //RequestContext requestContext = new RequestContext(req);
      //ControllerHelper.checkException(errors,
      //requestContext.getMessage("register.smscode.not.null")); // error
    }

    Boolean valid = veriFacade.verifyCode(form.getMobile(), form.getSmsCode(), smsType);

    if (!valid) {
      RequestContext requestContext = new RequestContext(req);
      log.error("register verify error, invalid smscode, mobile:" + form.getMobile()
              + " smsCode:" + form.getSmsCode());
      //throw new BizException(GlobalErrorCode.UNKNOWN,
      //requestContext.getMessage("register.smscode.not.valid"));
//			return new ResponseObject<Boolean>(false);
      throw new BizException(GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR, "请输入正确的验证码");
    }

    req.getSession().setAttribute(SMS_VERIFIED, Boolean.TRUE);
    return new ResponseObject<>(true);

  }

  /**
   * 注册 step2:接收密码创建用户
   *
   * @param form: mobile + pwd
   */
  @RequestMapping("/register/create")
  public ResponseObject<UserVO> registerCreate(@Valid @ModelAttribute UserForm form,
                                               Errors errors, HttpServletRequest req, HttpServletResponse resp) {

    if (!veriFacade.verifyCode(form.getMobile(), form.getSmsCode(), SmsType.SIGN)) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.UNKNOWN,
              requestContext.getMessage("register.smscode.not.valid"));
    }

    if (form.getPwd() == null || form.getPwd().length() < 6
            || form.getPwd().length() > 50) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              requestContext.getMessage("register.password.invalid"));
    }

    User newUser = userService.register(form.getMobile(), form.getPwd());

    // 创建用户的同时创建shop，设置用户的shopid
    Shop shop = new Shop();
    shop.setName(form.getMobile());
    shop.setOwnerId(newUser.getId());
    shopService.create(shop);

    newUser.setShopId(shop.getId());

    String rootShopId = shopTreeService.getRootShop().getRootShopId();
    newUser.setCurrentSellerShopId(rootShopId);

    // 自动登录, 只做了rememberMe
    Authentication auth = new UsernamePasswordAuthenticationToken(newUser,
            null, newUser.getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(auth);
    rememberMeServices.loginSuccess(req, resp, auth);

    //记录用户登录环境Log
    UserSigninLog log = UserSigninLogFactory.createUserSigninLog(req, newUser);
    userSigninLogService.insert(log);

    Message message = messageService.loadMessage(IdTypeHandler.encode(100L));
		/*if (message.getId() != null){
			PushMessage pushMessage = new PushMessage();
			//TODO 发送消息时,确认发送到的app名字
			pushMessage.setApp("hotshop");
			pushMessage.setTitle(message.getTitle());
			pushMessage.setDesc(message.getContent());
			pushMessage.setMsgType(PushMsgType.MSG_NORMAL.getValue());
			String userAgent = req.getHeader("User-Agent");
			if (userAgent.indexOf("iPhone") > 0 || userAgent.indexOf("iPad") > 0){
				pushMessage.setDetailUrl("/message/" + message.getId());
				pushMessage.setDeviceType(PushMessageDeviceType.IOS);
				pushMessage.setType(PushMessageType.NOTIFICATION);
			} else if (userAgent.indexOf("Android") > 0){
				pushMessage.setDetailUrl(domainName + "/message/" + message.getId());
				pushMessage.setType(PushMessageType.MESSAGE);
				pushMessage.setDeviceType(PushMessageDeviceType.ANDROID);
			}
			if (form.getBaiduChannelId() != null && form.getBaiduUserId() != null){
				pushMessage.setBaiduChannelId(form.getBaiduChannelId());
				pushMessage.setBaiduUserId(form.getBaiduUserId());
				pushMessageService.send(pushMessage);
			} else {
				pushMessage.setBaiduTagName(newUser.getId());
				pushMessageService.sendDelay(pushMessage);
			}
		}*/
    UserVO userVO = new UserVO(newUser, urlHelper.genShopUrl(newUser.getShopId()));
    HealthTestProfile profile = profileService.loadByUser(newUser.getId());
    if (profile != null) {
      userVO.setHasProfile(true);
    }
    return new ResponseObject<>(new UserVO(newUser,
            urlHelper.genShopUrl(newUser.getShopId())));
  }

  /**
   * 手机号必须是短信验证过的才能注册。
   *
   * @param form mobile和smsCode
   * @return 该用户
   */
  @RequestMapping("/register")
  public ResponseObject<UserVO> register(
          @Valid @ModelAttribute UserForm form, Errors errors,
          HttpServletRequest req, HttpServletResponse resp) {
    if (errors.hasErrors() || !StringUtils.hasText(form.getSmsCode())) {
      RequestContext requestContext = new RequestContext(req);
      ControllerHelper.checkException(errors,
              requestContext.getMessage("register.smscode.not.null")); // error
    }
    if (!Boolean.TRUE.equals(req.getSession().getAttribute(SMS_VERIFIED)) && !veriFacade
            .verifyCode(form.getMobile(), form.getSmsCode(), SmsType.SIGN)) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.UNKNOWN,
              requestContext.getMessage("register.smscode.not.valid"));
    }
    if (form.getPwd() == null || form.getPwd().length() < 6 || form.getPwd().length() > 50) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              requestContext.getMessage("register.password.invalid"));
    }

    User newUser = userService.register(form.getMobile(), form.getPwd());
    // 自动登录, 只做了rememberMe
    Authentication auth = new UsernamePasswordAuthenticationToken(newUser,
            null, newUser.getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(auth);
    rememberMeServices.loginSuccess(req, resp, auth);

    //记录用户登录环境Log
    UserSigninLog log = UserSigninLogFactory.createUserSigninLog(req, newUser);
    userSigninLogService.insert(log);

    if (StringUtils.isEmpty(newUser.getShopId())) {

      Message message = messageService.loadMessage(IdTypeHandler.encode(100L));
			/*if (message.getId() != null){
				PushMessage pushMessage = new PushMessage();
				//TODO 发送消息时,确认发送到的app名字
				pushMessage.setApp("hotshop");
				pushMessage.setTitle(message.getTitle());
				pushMessage.setDesc(message.getContent());
				pushMessage.setMsgType(PushMsgType.MSG_NORMAL.getValue());
				String userAgent = req.getHeader("User-Agent");
				if (userAgent != null && (userAgent.indexOf("iPhone") > 0 || userAgent.indexOf("iPad") > 0)){
					pushMessage.setDetailUrl("/message/" + message.getId());
					pushMessage.setDeviceType(PushMessageDeviceType.IOS);
					pushMessage.setType(PushMessageType.NOTIFICATION);
				} else if (userAgent != null && userAgent.indexOf("Android") > 0){
					pushMessage.setDetailUrl(domainName + "/message/" + message.getId());
					pushMessage.setType(PushMessageType.MESSAGE);
					pushMessage.setDeviceType(PushMessageDeviceType.ANDROID);
				}
				if (form.getBaiduChannelId() != null && form.getBaiduUserId() != null){
					pushMessage.setBaiduChannelId(form.getBaiduChannelId());
					pushMessage.setBaiduUserId(form.getBaiduUserId());
					pushMessageService.send(pushMessage);
				} else {
					pushMessage.setBaiduTagName(newUser.getId());
					pushMessageService.sendDelay(pushMessage);
				}
			}*/
      Shop shop = new Shop();
      shop.setName(form.getMobile());
      shop.setOwnerId(getCurrentUser().getId());
      shopService.create(shop);
    }

    return new ResponseObject<>(new UserVO(newUser,
            urlHelper.genShopUrl(newUser.getShopId())));
  }

  /**
   * 检查用户是否设置了密码 http://localhost:8888/v2/has-pwd?mobile=15968118380
   */
  @RequestMapping("/has-pwd")
  public ResponseObject<Boolean> hasPwd(@RequestParam("mobile") String mobile,
                                        HttpServletRequest req) {
    boolean result = false;
    try {
      String pwd = userService.loadUserByUsername(mobile).getPassword();
      if (!StringUtils.isEmpty(pwd)) {
        result = true;
      }
    } catch (UsernameNotFoundException e) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
              requestContext.getMessage("valid.user.notExist.message"));
    }
    return new ResponseObject<>(result);
  }

  /**
   * 忘记密码，发送验证码，修改密码 http://localhost:8888/v2/forget-pwd?mobile=15968118380
   */
  @RequestMapping("/forget-pwd")
  public ResponseObject<Boolean> forgetPwd(
          @Valid @ModelAttribute UserForm form, Errors errors,
          HttpServletRequest req) {
    ControllerHelper.checkException(errors);
    try {
      userService.loadUserByUsername(form.getMobile());
    } catch (UsernameNotFoundException ex) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
              requestContext.getMessage("valid.user.notExist.message"));
    }

    veriFacade.generateCode(form.getMobile(), SmsType.MODIFY_PWD);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 忘记密码第二步，验证smsCode，并改密 http://localhost:8888/v2/validate-forget-pwd?mobile=15968118380&pwd=123456&smsCode=158499
   */
  @RequestMapping("/validate-forget-pwd")
  public ResponseObject<UserVO> validateForPwdChange(
          @Valid @ModelAttribute UserForm form, Errors errors,
          HttpServletRequest req, HttpServletResponse resp) {
    RequestContext requestContext = new RequestContext(req);

    if (errors.hasErrors() || StringUtils.isEmpty(form.getPwd())) {
      ControllerHelper.checkException(errors,
              requestContext.getMessage("register.password.invalid"));
    }

    // valdiate code
    Boolean valid = veriFacade.verifyCode(form.getMobile(),
            form.getSmsCode(), SmsType.MODIFY_PWD);
    if (!valid) {
      throw new BizException(GlobalErrorCode.UNKNOWN,
              requestContext.getMessage("register.smscode.not.valid"));
    }
    // set password empty
    if (!userService.emptyUserPassword(form.getMobile(), form.getSmsCode())) {
      log.warn("清空密码失败");
    }

    User newUser = userService.loadAnonymousByPhone(form.getMobile());
    if (newUser == null) {
      throw new BizException(GlobalErrorCode.UNKNOWN, requestContext.getMessage("用户不存在"));
    }

    Authentication auth = new UsernamePasswordAuthenticationToken(newUser,
            form.getPwd(), newUser.getAuthorities());
    SecurityContextHolder.clearContext();
    SecurityContextHolder.getContext().setAuthentication(auth);
    if (!userService.changePwd(null, form.getPwd())) {
      log.error("修改密码失败");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改密码失败");
    }
    rememberMeServices.loginSuccess(req, resp, auth);

    //记录用户登录环境Log
    UserSigninLog log = UserSigninLogFactory.createUserSigninLog(req, newUser);
    userSigninLogService.insert(log);

    UserVO userVO = new UserVO(newUser, urlHelper.genShopUrl(newUser.getShopId()));
    userVO.setHasPwd(true);
    return new ResponseObject<>(userVO);
    // return new ResponseObject<Boolean>(Boolean.TRUE);//
    // veriFacade.generateCode(form.getMobile()));
  }

  /**
   * 未登录用户会跳到本页面；请使用 /signin_check?u=xx&p=yy 登录
   *
   * @return 带http status code, 因为访问任何未授权页面，都会跳到这里。
   */
  @RequestMapping(value = "/signin")
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ResponseObject<?> signinPage(HttpServletRequest req) {
    return new ResponseObject<Object>(
            new RequestContext(req).getMessage("signin.failed"),
            GlobalErrorCode.UNAUTHORIZED);
  }

  @RequestMapping(value = "/signin_fail")
  public ResponseObject<?> signinFailedPage(HttpServletRequest req) {
    String moreInfo = new RequestContext(req).getMessage("signin.failed");

    //处理Spring安全校验传递回来的错误信息
    Object obj = req.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    if (obj != null && obj instanceof AuthenticationException) {
      moreInfo = ((AuthenticationException) obj).getMessage();
    }

    return new ResponseObject<Object>(
            moreInfo,
            GlobalErrorCode.UNAUTHORIZED);
  }

  private String genSigninFailedInfo(AuthenticationException authenticationException) {
    String ret = "";

    // 对于自定义验证错误的处理
    if (authenticationException.getClass().equals(AppAuthenticationException.class)) {
      ret = authenticationException.getMessage();
    } else { //对于Spring security自带的错误类型进行处理, 按目前的设计此处属于意外情况
      ret = GlobalErrorCode.AUTH_UNKNOWN.getErrorCode() + ":" + GlobalErrorCode.AUTH_UNKNOWN
              .getError();
    }

    return ret;
  }

  @RequestMapping("/user/current/career")
  public ResponseObject<UserIdentityVO> careerLevel() {
    User curr = (User) getCurrentIUser();
    Long cpId = curr.getCpId();
    CustomerCareerLevel level = customerCareerLevelMapper.selectByPrimaryKey(cpId);
    if (level == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "用户身份信息不存在");
    }
    CareerLevelType levelType = level.getFinalType();

    boolean isNew = !customerProfileService.getIdentify(curr.getCpId());
    UserIdentityVO ret = new UserIdentityVO(cpId, isNew, levelType);
    boolean isInWhiteList = pointGiftService.isInWhiteList(cpId);
    if (!isInWhiteList) {
      // 不在白名单里面, 禁用德分红包功能
      ret.setBlackModules(ClientModuleType.POINT_PACKET);
    }
    return new ResponseObject<>(ret);
  }

  /**
   * @return current user
   */
  @RequestMapping(value = "/signined")
  public ResponseObject signined(HttpServletRequest req) {
    try {
      updateSigninedData(req);  //更新登录用户的数据
      UserType userType = userService
              .getUserType(userService.getCurrentUser().getId());

      User user = userService.load(userService.getCurrentUser().getId());
      Long cpId = user.getCpId();
      String unionId = user.getUnionId();
      RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
      if (StringUtils.isEmpty(unionId) && cpId != null) {
        unionId = redisUtils.get(String.valueOf(cpId));
      }
      String customerNumber = user.getCustomerNumber();
      UserVO userVO = new UserVO(
              user,
              urlHelper.genShopUrl(userService.getCurrentUser()
                      .getShopId()));
//      HealthTestProfile profile = profileService.loadByUser(user.getId());
//      if (profile != null) {
//        userVO.setHasProfile(true);
//      }

      String IdentityName=userService.selectIdType(cpId.toString());
      String userViViType=userService.selectViViType(user.getCpId().toString());
      userVO.setIdentityName("顾客");

      if("DS".equals(IdentityName) || "DS".equals(userViViType)){
        userVO.setIdentityName("会员");
      }
      if("SP".equals(IdentityName) || "SP".equals(userViViType)){
        userVO.setIdentityName("代理");
      }

      userVO.setType(userType);
      // 是否有身份
      userVO.setHasIdentity(customerProfileService.hasIdentity(cpId));
      // 是否点亮
      userVO.setIsLighten(customerProfileService.isLighten(cpId));
      userVO.setCustomerNumber(customerNumber);
      if (StringUtils.isEmpty(user.getAvatar())) {
        // dts同步时不会同步头像信息
        String customerAvatar = customerProfileService.loadAvatarByCpIdAndUnionId(cpId, unionId);
        userVO.setAvatar(customerAvatar);
      }

      CustomerProfile customerProfile = customerProfileService.queryCustomerProfileByCpId(cpId);
      if(customerProfile != null){
        userVO.setBankUserName(customerProfile.getNameCN());
        userVO.setBankTincode(customerProfile.getTinCode());
      }

      List<CustomerBank> customerBankList = customerBankService.queryCustomerBankListByCpId(cpId);
      userVO.setHasBankInfo(false);
      if(customerBankList != null && !customerBankList.isEmpty()){
        CustomerBank customerBank = customerBankList.get(0);
        userVO.setHasBankInfo(true);
        int len = customerBank.getBankAccount().length();
        userVO.setBankCardNumber(customerBank.getBankAccount().substring(len - 4));

        String bankUserName = userVO.getBankUserName();
        String bankTincode = userVO.getBankTincode();
        if((bankUserName == null || bankUserName.trim().length() == 0)
                || (bankTincode == null || bankTincode.trim().length() == 0)){

          userVO.setBankUserName(customerBank.getAccountName());
          userVO.setBankTincode(customerBank.getTincode());
        }
      }

      //是否是店主身份
      CustomerCareerLevel customerCareerLevel = customerCareerLevelMapper.selectByPrimaryKey(cpId);
      userVO.setIsShopOwner(false);
      userVO.setIsMember(false);
      userVO.setIsProxy(false);
      if(customerCareerLevel != null) {
        String sp = customerCareerLevel.getViviLifeType();
        if("SP".equals(sp)) {
          userVO.setIsShopOwner(true);
        }

        //是全国代理或店主，则显示代理价
        if(customerCareerLevel.getIsLightening() ||
                (!customerCareerLevel.getIsLightening() && "SP".equals(sp))){
          userVO.setIsProxy(true);
        }
        if(userVO.isHasIdentity()){ //是vip或超级会员，则显示会员价
          userVO.setIsMember(true);
        }
      }

      /**
       * 是否有我的业绩
       */
      boolean isShopkeeper = careerLevelService.isShopkeeper(cpId);
      boolean isVip = careerLevelService.isVip(cpId);
      if (isVip || isShopkeeper) {
        userVO.setIsAchievement(true);
      } else {
        userVO.setIsAchievement(false);
      }

      redisUtils.set("login_return_" + cpId, JSONObject.toJSONString(new IdentityVO(
          userVO.getIsProxy(), userVO.getIsMember())));

      return new ResponseObject<>(userVO);
    } catch (BizException e) {
      // 未登录状态
      log.error("登录失败", e);
      return new ResponseObject<>("登录失败: " + e.getMessage(),
          GlobalErrorCode.UNAUTHORIZED);
    }
  }

  /**
   * @return boolean
   */
  @RequestMapping(value = "/logoutSuccess")
  public ResponseObject<Boolean> logoutSuccess(HttpServletRequest req) {
    IUser iUser = getCurrentIUser();
    if (iUser instanceof User) {
      User user = (User) iUser;
      Long cpId = user.getCpId();
      if (cpId != null) {
        String sCpId = String.valueOf(cpId);
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        redisUtils.del(sCpId);
      }
    }
    return new ResponseObject(Boolean.TRUE);
  }

  /**
   * 更新登录后用户的相关数据
   */
  private void updateSigninedData(HttpServletRequest req) {

    final String CLIENT_ID = "vd_cid";
    //获取当前用户的cid
    String cid = null;
    Cookie[] cookies = req.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(CLIENT_ID) && cookie.getValue() != null) {   //未输入的匿名用户
          cid = cookie.getValue();
          break;
        }
      }
    }
    User anonymousUser = null;
    //还原确认用户身份前的用户信息
    if (cid != null) {
      anonymousUser = userService.loadByLoginname(cid);
    }
    //更新当前用户数据
    User currentUser = (User) userService.getCurrentUser();
    //用户登录状态
    if (currentUser != null) {
      currentUser.setUserStatus(UserStatus.LOGINED);
    }

    if (currentUser != null && cid != null) {
      currentUser.updateCid(cid);
      userService.updateUserInfo(currentUser);
    }

    if (anonymousUser != null) {
      cartService.updateCartUserId(anonymousUser.getId());
    }

    if (currentUser != null) {
      Authentication auth = new UsernamePasswordAuthenticationToken(currentUser, null,
              currentUser.getAuthorities());
      SecurityContextHolder.clearContext();
      SecurityContextHolder.getContext().setAuthentication(auth);
    }
  }

  @RequestMapping(value = "/user/change-pwd")
  public ResponseObject<Boolean> changePwd(
          @RequestParam(value = "oldPwd", required = false) String oldPwd,
          @RequestParam("newPwd") String newPwd, HttpServletRequest req,
          HttpServletResponse res) {
    if (org.apache.commons.lang3.StringUtils.isBlank(newPwd)) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
              requestContext.getMessage("register.password.invalid"));
    }
    boolean result = userService.changePwd(oldPwd, newPwd);
    if (result) {
      User user = (User) getCurrentIUser();
      Authentication auth = new UsernamePasswordAuthenticationToken(user,
              newPwd, user.getAuthorities());
      SecurityContextHolder.clearContext();
      SecurityContextHolder.getContext().setAuthentication(auth);
      rememberMeServices.loginSuccess(req, res, auth);
    }

    return new ResponseObject<>(result);
  }

  /**
   * 绑定手机号废弃的版本，为了兼容老版本应用，没有删掉 微信登陆绑定手机号，根据用户loginname更新用户手机号 http://localhost:8888/v2/user/updatephone?id=xxx&mobile=15968118380
   *
   * @deprecated
   */
  @RequestMapping("/user/updatePhone")
  public ResponseObject<Boolean> updatePhone(@Valid @ModelAttribute UserForm form,
                                             @RequestParam(value = "id", required = false) String id,
                                             @RequestParam(value = "mobile", required = false) String mobile) {
    if (!veriFacade.verifyCode(form.getMobile(), form.getSmsCode(), SmsType.MODIFY_PHONE)) {
      return new ResponseObject<>(Boolean.FALSE);
      //RequestContext requestContext = new RequestContext(req);
      //throw new BizException(GlobalErrorCode.UNKNOWN,
      //requestContext.getMessage("register.smscode.not.valid"));
    }
    try {

      // 先判断这个绑定的手机号有没有用户记录，如果有则将微信id更新到此记录的loginname里即可，同时删除之前这个默认创建的微信用户
      // 如果没有用户记录，则将创建的微信用户更新下手机号即可
      User phoneUser = userService.loadAnonymousByPhone(mobile);
      User wechatUser = userService.load(id);
      String userId = "";
      if (phoneUser == null) {
        userService.updateAnonymousPhone(wechatUser.getLoginname(), mobile);
        userId = id;
      } else {
        // 将微信id更新到此记录的loginname
        User user = new User();
        user.setId(phoneUser.getId());
        user.setLoginname(wechatUser.getLoginname());
        userService.updateByBosUserInfo(user);

        // 删除之前这个默认创建的微信用户
        userService.delete(id);
        userId = phoneUser.getId();
      }

      // 绑定手机号成功后，需要重新设置user的认证
      User user = userService.load(userId);
      Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
              user.getAuthorities());
      SecurityContextHolder.clearContext();
      SecurityContextHolder.getContext().setAuthentication(auth);
    } catch (Exception e) {
      log.error(e.toString());
      return new ResponseObject<>(Boolean.FALSE);
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 微信绑定手机号新版本，绑定完成后返回绑定后的用户信息给客户端 微信登陆绑定手机号，根据用户loginname更新用户手机号 http://localhost:8888/v2/user/updatephone?id=xxx&mobile=15968118380
   */
  @RequestMapping("/user/updatePhoneNew")
  public ResponseObject<UserVO> updatePhoneNew(@Validated @ModelAttribute UserForm form,
                                               String addressId,
                                               Errors errors, HttpServletRequest request) {
    ControllerHelper.checkException(errors);
    String mobile = form.getMobile();
    String sessionId = request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);

    // 默认不带sessionId时从app入口进入
    if (StringUtils.isEmpty(sessionId)) {
      if (!veriFacade.verifyCode(mobile, form.getSmsCode(), SmsType.MODIFY_PHONE)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "短信验证码错误");
      }
    } else {
      // 从小程序入口进入
      if (!veriFacade.verifyCodeForMiniProgram(mobile, form.getSmsCode(), sessionId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "短信验证码错误");
      }
    }
    // 先判断这个绑定的手机号有没有用户记录，如果有则将微信id更新到此记录的loginname里即可，同时删除之前这个默认创建的微信用户
    // 如果没有用户记录，则将创建的微信用户更新下手机号即可
//    User phoneUser = userService.loadAnonymousByPhone(mobile);
    List<User> userList = userService.queryUserByPhone(mobile);
    if (userList != null && !userList.isEmpty()) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该手机号已注册");
    }
    User wechatUser = (User) getCurrentIUser();
    if (wechatUser == null) {
      // 正常通过小程序访问的请求在此处应该已经通过getSession登录
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户尚未登录");
    }
    Long cpId = wechatUser.getCpId();

    User userToUpdate = new User();
    userToUpdate.setCpId(cpId);
    userToUpdate.setUnionId(wechatUser.getUnionId());
    userToUpdate.setPhone(mobile);
    if (!StringUtils.isEmpty(addressId)) {
      userToUpdate.setZoneId(addressId);
    }
    userService.updateByCpIdSelective(userToUpdate);
    // 绑定手机号成功后，需要重新设置user的认证
    User user = userService.loadByCpId(cpId);
    Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
            user.getAuthorities());
    SecurityContextHolder.clearContext();
    SecurityContextHolder.getContext().setAuthentication(auth);
    log.error("context error");
    UserVO userVO = new UserVO(
            user, urlHelper.genShopUrl(user.getShopId()));
    return new ResponseObject<>(userVO);
  }

  /**
   * 微信登陆绑定手机号，请求验证码 http://localhost:8888/v2/register/bindPhone?mobile=15968118380
   */
//  @RequestMapping("/register/bindPhoneDna")
//  public ResponseObject<Boolean> bindPhoneDna(
//          @Valid @ModelAttribute UserForm form, Errors errors,
//          HttpServletRequest req) {
//    veriFacade.generateCode(form.getMobile(), SmsType.MODIFY_PHONE);
//    return new ResponseObject<>(Boolean.TRUE);
//  }

//chp 查

  /**
   * 微信登陆绑定手机号，请求验证码 http://localhost:8888/v2/register/bindPhone?mobile=15968118380
   */
  @RequestMapping("/register/bindPhoneDna")
  public ResponseObject<Boolean> bindPhoneDna(
          @Valid @ModelAttribute UserForm form, Errors errors,
          HttpServletRequest req, HttpSession session) {
    boolean b = veriFacade.generateCodeDNA(form.getMobile(), SmsType.MODIFY_PHONE,session);
    ResponseObject<Boolean> res = new ResponseObject<>();
    res.setData(b);
//    res.setMoreInfo(WeChatConfig.smsCode);
    return res;
  }


  /**
   * 微信登陆绑定手机号，校验验证码
   */
  @RequestMapping("/register/bindPhoneDna/verify")
  public ResponseObject<Boolean> verifyBindPhoneDNA(@Valid @ModelAttribute UserFormDna form,
                                                    Errors errors, HttpServletRequest req, HttpServletResponse resp,HttpSession session) {
    return checkSmsDNA(form, errors, req, resp, SmsType.MODIFY_PHONE,session);
  }



  private ResponseObject<Boolean> checkSmsDNA(UserFormDna form, Errors errors, HttpServletRequest req,
                                              HttpServletResponse resp, SmsType smsType, HttpSession session) {
    req.getSession().setAttribute(SMS_VERIFIED, Boolean.FALSE);
    if (errors.hasErrors() || !StringUtils.hasText(form.getSmsCode())) {
//			return new ResponseObject<Boolean>(false);
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "请输入正确的验证码");
      //RequestContext requestContext = new RequestContext(req);
      //ControllerHelper.checkException(errors,
      //requestContext.getMessage("register.smscode.not.null")); // error
    }
    if (errors.hasErrors() || !StringUtils.hasText(form.getCpid())) {
//			return new ResponseObject<Boolean>(false);
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "cpid不能为空");
      //RequestContext requestContext = new RequestContext(req);
      //ControllerHelper.checkException(errors,
      //requestContext.getMessage("register.smscode.not.null")); // error
    }
    Boolean valid = veriFacade.verifyCodeDNA(form.getCpid(),form.getMobile(), form.getSmsCode(), smsType,session);

    if (!valid) {
      RequestContext requestContext = new RequestContext(req);
      log.error("register verify error, invalid smscode, mobile:" + form.getMobile()
              + " smsCode:" + form.getSmsCode());
      //throw new BizException(GlobalErrorCode.UNKNOWN,
      //requestContext.getMessage("register.smscode.not.valid"));
//			return new ResponseObject<Boolean>(false);
      throw new BizException(GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR, "请输入正确的验证码");
    }

    req.getSession().setAttribute(SMS_VERIFIED, Boolean.TRUE);
    return new ResponseObject<>(true);

  }

//chp  结束

  /**
   * 微信登陆绑定手机号，请求验证码 http://localhost:8888/v2/register/bindPhone?mobile=15968118380
   */
  @RequestMapping("/register/bindPhone")
  public ResponseObject<Boolean> bindPhone(
          @Valid @ModelAttribute UserForm form, Errors errors,
          HttpServletRequest req) {
    veriFacade.generateCode(form.getMobile(), SmsType.MODIFY_PHONE);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 微信登陆绑定手机号，校验验证码
   */
  @RequestMapping("/register/bindPhone/verify")
  public ResponseObject<Boolean> verifyBindPhone(@Valid @ModelAttribute UserForm form,
                                                 Errors errors, HttpServletRequest req, HttpServletResponse resp) {
    return checkSms(form, errors, req, resp, SmsType.MODIFY_PHONE);
  }

  /**
   * 返回当前用户的信息，包含用户，推客，合伙人，战队信息
   */
  @RequestMapping(value = "/getUserInfo")
  public ResponseObject<UserInfoVO> getUserInfo(HttpServletRequest req) {
    UserInfoVO info = null;
    try {
      info = userService.getUserInfo(this.getCurrentUser().getId());
    } catch (BizException e) {
      // 未登录状态
      return new ResponseObject(new RequestContext(req).getMessage("signin.failed"),
              GlobalErrorCode.UNAUTHORIZED);
    }
    return new ResponseObject<>(info);
  }

  /**
   * 返回当前用户的用户类型
   */
  @RequestMapping(value = "/getUserType")
  public ResponseObject<UserType> getUserType(HttpServletRequest req) {
    UserType type = null;
    try {
      type = userService.getUserType(this.getCurrentUser().getId());
    } catch (BizException e) {
      // 未登录状态
      return new ResponseObject(new RequestContext(req).getMessage("signin.failed"),
              GlobalErrorCode.UNAUTHORIZED);
    }
    return new ResponseObject<>(type);
  }


  /**
   * app:上传微信二维码名片
   */
  @RequestMapping(value = "/user/updateWxQRcodeCard")
  public ResponseObject<Boolean> updateWxQRcodeCard(String qrcode) {
    boolean isSuccess = userService.updateWxQRcodeCard(this.getCurrentUser().getId(), qrcode);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * app:获取微信二维码名片
   */
  @RequestMapping(value = "/user/getWxQRcodeCard")
  public ResponseObject<Map<String, String>> getWxQRcodeCard() {
    Map<String, String> map = new HashMap<>();
    String qrcodeUrl = userService.getWxQRcodeCard(this.getCurrentUser().getId());
    map.put("qrcodeUrl", qrcodeUrl);
    return new ResponseObject<>(map);
  }

  /**
   * 保存友盟device_token信息到user表中，供后续消息推送使用
   */
  @RequestMapping(value = "/user/bindUmeng")
  public ResponseObject<Boolean> bindUmeng(HttpServletRequest req,
                                           @RequestParam(value = "type") String type,
                                           @RequestParam(value = "deviceToken") String deviceToken) {
    //更新当前用户数据
    User currentUser = (User) userService.getCurrentUser();
    User user = new User();
    user.setId(currentUser.getId());

    if ("android".equals(type)) {
      user.setCid2(deviceToken);
      userService.updateCid2Null(deviceToken);
    } else if ("ios".equals(type)) {
      user.setCid3(deviceToken);
      userService.updateCid3Null(deviceToken);
    }
    userService.updateByBosUserInfo(user);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 用户退出app时，将deviceToken与用户解绑，避免退出后仍然收到推送的消息
   */
  @RequestMapping(value = "/user/unBindUmeng")
  public ResponseObject<Boolean> unBindUmeng(HttpServletRequest req,
                                             @RequestParam(value = "type") String type) {
    //更新当前用户数据
    User currentUser;
    try {
      currentUser = (User) userService.getCurrentUser();
      User user = new User();
      user.setId(currentUser.getId());

      if ("android".equals(type)) {
        user.setCid2("");
      } else if ("ios".equals(type)) {
        user.setCid3("");
      }
      userService.updateByBosUserInfo(user);
    } catch (Exception e) {
      return new ResponseObject<>(Boolean.FALSE);
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

}

