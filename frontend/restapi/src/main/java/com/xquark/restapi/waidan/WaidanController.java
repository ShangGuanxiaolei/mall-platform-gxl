package com.xquark.restapi.waidan;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.dal.vo.AmountVo;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.waidan.chpUtils.ExcelExportHelper;
import com.xquark.restapi.waidan.chpUtils.ExcelReadHelper;
import com.xquark.restapi.waidan.chpUtils.date.DateUtils;
import com.xquark.restapi.waidan.vo.ErpWuTempalte;
import com.xquark.restapi.waidan.vo.NotErpTemplate;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.impl.OrderImportAndExportServiceImpl;
import com.xquark.service.winningList.WinningService;
import com.xquark.service.wms.WmsOrderService;
import com.xquark.utils.UniqueNoUtils;
import io.vavr.control.Either;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/5/23
 * Time: 10:06
 * Description: 外部订单导入导出功能
 */
@Controller
@RequestMapping("/or")
public class WaidanController extends BaseController {
     @Autowired
     private OrderImportAndExportServiceImpl  orderImportAndExportService;
     @Autowired
     private OrderItemMapper   orderItemMapper;
     @Autowired
     private    XquarkOrderThirdErpMapper xquarkOrderThirdErpMapper; //erp 导入信息表
     @Autowired
     private    XquarkOrderThirdNoerpMapper  xquarkOrderThirdNoerpMapper;
     @Autowired
     private XquarkOrderThirdInfoMapper xquarkOrderThirdInfoMapper;
     @Autowired
     private XquarkOrderThirdInfoDetailMapper xquarkOrderThirdInfoDetailMapper ;
     @Autowired
     private XquarkOrderThirdCheckMapper  xquarkOrderThirdCheckMapper;
     @Autowired
     private WinningService winningService;
     Logger log=LoggerFactory.getLogger(this.getClass());
     @Autowired
     private OrderService orderService;
     @Autowired
     private WmsOrderService wmsOrderService;

    /*
     * @Author chp
     * @Description  查询的第三方订单列表,多条件查询
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getOrderList",method = RequestMethod.GET)
    @ResponseBody
    public  ResponseObject<Object>    getOrderList(String   orderStatus, //订单状态
                                                   String   thirdOrderNo,//第三方订单号
                                                   String   hvOrderNo,//hv订单号
                                                   String   source ,//source
                                                   String   startTime,//查询起始时间
                                                   String   endTime  //查询结束时间
            , Pageable pageable){
              Date startTimea=null;
              Date endTimea=null;
             if(StringUtils.isBlank(orderStatus)) {
                 orderStatus=null;
             }
             if(StringUtils.isBlank(thirdOrderNo)){
                 thirdOrderNo=null;
             }
             if(StringUtils.isBlank(hvOrderNo)){
                 hvOrderNo=null;
             }
             if(StringUtils.isBlank(source)){
                 source=null;
             }
             if(StringUtils.isNotBlank(startTime)&&!"".equals(startTime)&&!"null".equals(startTime)){
                 startTimea=DateUtils.string3Date(startTime);
             }
             else {
                 startTimea=null;

             }
             if(StringUtils.isNotBlank(endTime)&&!"".equals(endTime)&&!"null".equals(endTime)){
                 endTimea=DateUtils.string3Date(endTime);
            } else {
                 endTimea=null;
             }
        List<Map<String, Object>> orderList = orderImportAndExportService.getOrderList(
                orderStatus,
                thirdOrderNo,
                hvOrderNo,
                source,
                startTimea,
                endTimea,
                pageable);


        int orderListCount =  orderImportAndExportService.getOrderListCount(
                orderStatus,
                thirdOrderNo,
                hvOrderNo,
                source,
                startTimea,
                endTimea,
                pageable
        );
        Map<String,Object> map=new LinkedHashMap<>();
        map.put("list",orderList);
        map.put("count",orderListCount);
        return  new ResponseObject<>(map);

    }
   /*
    * @Author chp
    * @Description  第三方订单导入 erp和非erp导入
    * @Date
    * @Param
    * @return
    **/
//    @RequestMapping(value = "/OrderImport/erp" ,method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public   ResponseObject<Boolean>   OrderImport(@RequestBody Map map){
//        final String lot = UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.IM);
//         String  name="root";
////         String  lot="lot";
//         Long   cpid=1234L;
//         Date date =  new Date();
//            //1.订单导入多重判断
//         Object list  =  map.get("orderList");
//         if(null!=list&&!"".equals(list)){
//             List<ErpImportOrder> orderlist = JSONArray.parseArray(JSON.toJSONString(map.get("orderList")), ErpImportOrder.class);
//                 //1查询数据库是否有重复记录
//             Optional.of(orderlist).orElseThrow( () -> new BizException(GlobalErrorCode.UNKNOWN, "订单数据不能为空"));
//             //有重复记录只存临时表，并记日志
//             //查出订单号
//             List<String> orderNoList=orderlist.stream().map(ErpImportOrder::getOrderItemNo).collect(Collectors.toList());
//             List<String> thirdItemList  = orderImportAndExportService.getThirdItemList(orderNoList);
//            if (thirdItemList.size()>0){
//
////           过滤出符合条件的订单
//                List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> thirdItemList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
////                记录日志写入
//                try {
//                    //失败时日志记录
//                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
//                    xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
//                    xquarkOrderThirdInfoWithBLOBs.setName(name);
//                    xquarkOrderThirdInfoWithBLOBs.setLot(lot);
//                    xquarkOrderThirdInfoWithBLOBs.setResult("1");
//                    xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
//                    xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
//                    int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
////                    分别记录成功和失败的日志
//                for (ErpImportOrder erpImportOrder  : collect) {
//                    XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                    xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                    xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                    xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                    xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                    xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                    xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                    xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                    xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                    xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                    xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                    xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                    xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                    xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                    xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                    xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                    xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                    xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                    xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                    xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                    xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                    xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                    xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                    xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                    xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                    xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                    xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                    xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                    xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                    xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                    xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                    xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                    xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                    xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                    xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                    xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                    xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                    xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                    xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                    //生成导入批次
//                    xquarkOrderThirdErp.setLot(lot);
//                    xquarkOrderThirdErp.setAchieve("1");
//                    xquarkOrderThirdErp.setCreateTime(date);
//                    int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                    //失败时日志详情记录
//                    XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                    xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                    xquarkOrderThirdInfoDetail.setLot(lot);
//                    xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                    xquarkOrderThirdInfoDetail.setReceiver(name);
//                    xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                    xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                    xquarkOrderThirdInfoDetail.setImportStatus("0");
//                    xquarkOrderThirdInfoDetail.setReason("重复导入");
//                    xquarkOrderThirdInfoDetail.setCreateAt(date);
//                    xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                    int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                    if(1!=insert||1!=insert1||1!=insert2){
//                        throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel数据异常， excel导入失败，请重新导入");
//                    }
//                }
//                    // 重复导入信息的信息插入
//                    List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->!thirdItemList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
//                    for (ErpImportOrder erpImportOrder  : collect2) {
//                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                        xquarkOrderThirdErp.setLot(lot);
//                        xquarkOrderThirdErp.setAchieve("0");
//                        xquarkOrderThirdErp.setCreateTime(new Date());
//                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                        if(1!=insert){
//                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                        }
//                    }
//                }catch (Exception e){
//                    log.info("数据类型错误");
//                  throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"参数类型错误");
//                }
////                订单记录
//                ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
//                booleanResponseObject.setMoreInfo("导入失败订单号已存在");
//                return booleanResponseObject;
//            }
//
//
////              存入记录
//             //2查询数据库sku是否都存在
//             List<String> skuLlist = orderlist.stream().map(ErpImportOrder::getSkuCode).collect(Collectors.toList());
//             List<String> skuListco =  orderImportAndExportService.getSkuList(skuLlist);
//             if(skuListco.size()>0){
//                  //sku不存在 记录导入日志表
//                 List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
//                 //记录日志表和日志详表
//                 try{
//                     XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
//                     xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
//                     xquarkOrderThirdInfoWithBLOBs.setName(name);
//                     xquarkOrderThirdInfoWithBLOBs.setLot(lot);
//                     xquarkOrderThirdInfoWithBLOBs.setResult("1");
//                     xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
//                     xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
//                     int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
//                     for (ErpImportOrder erpImportOrder : collect) {
//                         //sku不存在时记录失败信息
//                         XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                         xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                         xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                         xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                         xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                         xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                         xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                         xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                         xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                         xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                         xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                         xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                         xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                         xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                         xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                         xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                         xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                         xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                         xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                         xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                         xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                         xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                         xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                         xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                         xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                         xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                         xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                         xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                         xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                         xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                         xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                         xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                         xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                         xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                         xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                         xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                         xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                         xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                         xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                         //生成导入批次
//                         xquarkOrderThirdErp.setLot(lot);
//                         xquarkOrderThirdErp.setAchieve("1");
//                         xquarkOrderThirdErp.setCreateTime(date);
//                         int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                         //失败时日志详情记录
//                         XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                         xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                         xquarkOrderThirdInfoDetail.setLot(lot);
//                         xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                         xquarkOrderThirdInfoDetail.setReceiver(name);
//                         xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                         xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                         xquarkOrderThirdInfoDetail.setImportStatus("0");
//                         xquarkOrderThirdInfoDetail.setReason("skuCode不存在");
//                         xquarkOrderThirdInfoDetail.setCreateAt(date);
//                         xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                         int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                         if(1!=insert||1!=insert1||1!=insert2){
//                             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                         }
//
//                     }
//                     List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) -> !skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
////                     未导入记录信息
//                     for (ErpImportOrder erpImportOrder : collect2) {
//                         XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                         xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                         xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                         xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                         xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                         xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                         xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                         xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                         xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                         xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                         xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                         xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                         xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                         xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                         xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                         xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                         xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                         xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                         xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                         xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                         xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                         xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                         xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                         xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                         xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                         xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                         xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                         xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                         xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                         xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                         xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                         xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                         xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                         xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                         xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                         xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                         xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                         xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                         xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                         xquarkOrderThirdErp.setLot(lot);
//                         xquarkOrderThirdErp.setAchieve("0");
//                         xquarkOrderThirdErp.setCreateTime(new Date());
//                         int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                         if(1!=insert){
//                             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                         }
//                     }
//                 }catch (Exception e){
//                     log.info("数据类型错误");
//                     throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"参数类型错误");
//                 }
//                  //记录日志表
//                 ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
//                 booleanResponseObject.setMoreInfo("导入失败skuCode不存在");
//                 return booleanResponseObject;
//             }
//                 //校验所有sku库存是否满足要求，库存是否满足要求
//             List<String>   amountList=new LinkedList<>();
//             for (ErpImportOrder erpImportOrder  : orderlist) {
//                 List<String> amountSku  = orderItemMapper.getAmountSku(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
//                 if(amountSku.size()>0){
//                     amountList.add(amountSku.get(0));
//                 }
//             }
//              if(amountList.size()>0){
//                  //记录日志表
//                  List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> amountList.contains(erp.getSkuCode())).collect(Collectors.toList());
//                  try {
//                      //失败时日志记录
//                      XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
//                      xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
//                      xquarkOrderThirdInfoWithBLOBs.setName(name);
//                      xquarkOrderThirdInfoWithBLOBs.setLot(lot);
//                      xquarkOrderThirdInfoWithBLOBs.setResult("1");
//                      xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
//                      xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
//                      int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
////                    分别记录成功和失败的日志
//                      for (ErpImportOrder erpImportOrder  : collect) {
//                          XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                          xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                          xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                          xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                          xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                          xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                          xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                          xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                          xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                          xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                          xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                          xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                          xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                          xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                          xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                          xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                          xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                          xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                          xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                          xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                          xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                          xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                          xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                          xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                          xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                          xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                          xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                          xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                          xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                          xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                          xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                          xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                          xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                          xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                          xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                          xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                          xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                          xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                          xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                          //生成导入批次
//                          xquarkOrderThirdErp.setLot(lot);
//                          xquarkOrderThirdErp.setAchieve("1");
//                          xquarkOrderThirdErp.setCreateTime(date);
//                          int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                          //失败时日志详情记录
//                          XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                          xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                          xquarkOrderThirdInfoDetail.setLot(lot);
//                          xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                          xquarkOrderThirdInfoDetail.setReceiver(name);
//                          xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                          xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                          xquarkOrderThirdInfoDetail.setImportStatus("0");
//                          xquarkOrderThirdInfoDetail.setReason("sku库存不足");
//                          xquarkOrderThirdInfoDetail.setCreateAt(date);
//                          xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                          int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                          if(1!=insert||1!=insert1||1!=insert2){
//                              throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                          }
//                      }
//                      // 重复导入信息的信息插入
//                      List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->!amountList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
//                      for (ErpImportOrder erpImportOrder  : collect2) {
//                          XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                          xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                          xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                          xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                          xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                          xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                          xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                          xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                          xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                          xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                          xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                          xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                          xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                          xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                          xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                          xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                          xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                          xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                          xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                          xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                          xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                          xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                          xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                          xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                          xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                          xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                          xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                          xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                          xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                          xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                          xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                          xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                          xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                          xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                          xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                          xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                          xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                          xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                          xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                          xquarkOrderThirdErp.setLot(lot);
//                          xquarkOrderThirdErp.setAchieve("0");
//                          xquarkOrderThirdErp.setCreateTime(new Date());
//                          int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                          if(1!=insert){
//                              throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                          }
//                      }
//                  }catch (Exception e){
//                      log.info("数据类型错误");
//                      throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"参数类型错误");
//                  }
//
//                  ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
//                  booleanResponseObject.setMoreInfo("sku库存不足");
//                  return booleanResponseObject;
//              }
//              //库存不足的订单
//              List<ErpImportOrder>  amountLi=new ArrayList<>();
//              //生成订单的情况 生成订单时校验库存是否足
//             try{
//
//                 for (ErpImportOrder erpImportOrder  :  orderlist) {
//                     // 生成订单
//                     // 生成订单前去校验库存
//                     List<String> amountCheck =  orderItemMapper.getAmountCheck(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
//                     if(amountCheck.size()>0){
//                         Either<String, Boolean> winningOrder2  = winningService.createWinningOrder2(cpid, lot, erpImportOrder);
//                         Boolean aBoolean = winningOrder2.get();
//                         if(!aBoolean){
//                             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_CREATE_ERROR,"创建订单失败");
//                         }
//                     }
//                   else {
//                         amountLi.add(erpImportOrder);
//                     }
////
//                 }
//               if(amountLi.size()>0)  {
//                   List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->!amountLi.contains(erp.getSkuCode())).collect(Collectors.toList());
//
//
//                    //部分导入失败记录审核表
//                   XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
//                   xquarkOrderThirdCheck.setLot(lot);
//                   xquarkOrderThirdCheck.setImportTime(date);
//                   xquarkOrderThirdCheck.setSubmitUser(name);
//                   xquarkOrderThirdCheck.setSourceFrom("erp");
//                   xquarkOrderThirdCheck.setCheckTime(date);
//                   xquarkOrderThirdCheck.setCreateAt(date);
//                   xquarkOrderThirdCheck.setUpdateAt(date);
//                   int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
//                   if(1!=insert1){
//                       throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                   }
//                   //部分导入失败记录日志主表
//                   XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
//                   xquarkOrderThirdInfo.setImportTime(date);
//                   xquarkOrderThirdInfo.setName(name);
//                   xquarkOrderThirdInfo.setLot(lot);
//                   xquarkOrderThirdInfo.setResult("2");
//                   xquarkOrderThirdInfo.setCreateAt(date);
//                   xquarkOrderThirdInfo.setUpdateAt(date);
//                   int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
//                   if(1!=insert3){
//                       throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                   }
//
//                   for (ErpImportOrder erpImportOrder : collect2) {
//                       XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                       xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                       xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                       xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                       xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                       xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                       xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                       xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                       xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                       xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                       xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                       xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                       xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                       xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                       xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                       xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                       xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                       xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                       xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                       xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                       xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                       xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                       xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                       xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                       xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                       xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                       xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                       xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                       xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                       xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                       xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                       xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                       xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                       xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                       xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                       xquarkOrderThirdErp.setLot(lot);
//                       xquarkOrderThirdErp.setAchieve("1");
//                       xquarkOrderThirdErp.setCreateTime(new Date());
//                       int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                       if(1!=insert){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                       }
//                       //失败时日志详情记录
//                       XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                       xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdInfoDetail.setLot(lot);
//                       xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdInfoDetail.setReceiver(name);
//                       xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdInfoDetail.setImportStatus("1");
//                       xquarkOrderThirdInfoDetail.setCreateAt(date);
//                       xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                       int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                       if(1!=insert2){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                       }
//
//                   }
//
//
//
//                   for (ErpImportOrder erpImportOrder : amountLi) {
//                       XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                       xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                       xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                       xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                       xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                       xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                       xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                       xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                       xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                       xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                       xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                       xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                       xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                       xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                       xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                       xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                       xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                       xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                       xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                       xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                       xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                       xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                       xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                       xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                       xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                       xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                       xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                       xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                       xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                       xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                       xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                       xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                       xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                       xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                       xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                       xquarkOrderThirdErp.setLot(lot);
//                       xquarkOrderThirdErp.setAchieve("0");
//                       xquarkOrderThirdErp.setCreateTime(new Date());
//                       int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                       if(1!=insert){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                       }
//                       //部分失败时日志详情记录
//                       XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                       xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdInfoDetail.setLot(lot);
//                       xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdInfoDetail.setReceiver(name);
//                       xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdInfoDetail.setImportStatus("0");
//                       xquarkOrderThirdInfoDetail.setReason("sku库存不足");
//                       xquarkOrderThirdInfoDetail.setCreateAt(date);
//                       xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                       int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                       if(1!=insert2){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                       }
//
//                   }
//                   ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
//                   booleanResponseObject.setMoreInfo("部分导入失败");
//                   return booleanResponseObject;
//               }
//               else {
//                 //记录审核表 全部成功
//                //记录日志表 全部成功
//                // 记录日志详表全部成功
//                   //全部成功审核表
//                   XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
//                   xquarkOrderThirdCheck.setLot(lot);
//                   xquarkOrderThirdCheck.setImportTime(date);
//                   xquarkOrderThirdCheck.setSubmitUser(name);
//                   xquarkOrderThirdCheck.setSourceFrom("erp");
//                   xquarkOrderThirdCheck.setCheckTime(date);
//                   xquarkOrderThirdCheck.setCreateAt(date);
//                   xquarkOrderThirdCheck.setUpdateAt(date);
//                   int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
//                   if(1!=insert1){
//                       throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                   }
//                   //全部成功日志主表
//                   XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
//                   xquarkOrderThirdInfo.setImportTime(date);
//                   xquarkOrderThirdInfo.setName(name);
//                   xquarkOrderThirdInfo.setLot(lot);
//                   xquarkOrderThirdInfo.setResult("0");
//                   xquarkOrderThirdInfo.setCreateAt(date);
//                   xquarkOrderThirdInfo.setUpdateAt(date);
//                   int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
//                   if(1!=insert3){
//                       throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
//                   }
//                   for (ErpImportOrder erpImportOrder : orderlist) {
//                       XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
//                       xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
//                       xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
//                       xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
//                       xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
//                       xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
//                       xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
//                       xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
//                       xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
//                       xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
//                       xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
//                       xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
//                       xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
//                       xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
//                       xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
//                       xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
//                       xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
//                       xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
//                       xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
//                       xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
//                       xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
//                       xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
//                       xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
//                       xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
//                       xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
//                       xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
//                       xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
//                       xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
//                       xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
//                       xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
//                       xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
//                       xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
//                       xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
//                       xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
//                       xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
//                       xquarkOrderThirdErp.setLot(lot);
//                       xquarkOrderThirdErp.setAchieve("1");
//                       xquarkOrderThirdErp.setCreateTime(new Date());
//                       int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
//                       if(1!=insert){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请确认数据无误");
//                       }
//                       //成功时日志详情记录
//                       XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
//                       xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
//                       xquarkOrderThirdInfoDetail.setLot(lot);
//                       xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
//                       xquarkOrderThirdInfoDetail.setReceiver(name);
//                       xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
//                       xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
//                       xquarkOrderThirdInfoDetail.setImportStatus("1");
//                       xquarkOrderThirdInfoDetail.setCreateAt(date);
//                       xquarkOrderThirdInfoDetail.setUpdateAt(date);
//                       int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
//                       if(1!=insert2){
//                           throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请确认数据无误");
//                       }
//                   }
//               }
//             }catch (Exception e){
//                     log.info("excel导入数据异常");
//                 throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel,数据异常excel导入失败，确认数据无误");
//
//             }
//
//         }
//
//        return  new ResponseObject<>(true);
//    }

    public ResponseObject<Boolean>   insertRecord( List<ErpImportOrder> collect,List<ErpImportOrder> nosucess,String msg,Date date  ){


         return   new ResponseObject<>(false);
    }








     /*
      * @Author chp
      * @Description  第三方物流导出，可导出指定和全部的物流信息
      * @Date
      * @Param
      * @return
      **/
    @RequestMapping(value = "/LogisticsExport",method = RequestMethod.GET )
    @ResponseBody
    public   ResponseObject<Object>    OrderExport(@RequestBody  Map map){
              if("".equals(map.get("orderList"))){

        }
             else {

              }

        return  null;
    }

     /*
      * @Author chp
      * @Description     日志列表
      * @Date
      * @Param
      * @return
      **/
    @RequestMapping(value ="/getLogList",method = RequestMethod.GET )
    @ResponseBody
    public ResponseObject<Object>  getLogList(
           @RequestParam("result") String   result , //导入结果
                 String startTime,//查询起始时间
                 String   endTime , //查询结束时间
                 Pageable page

    ){
        Date startTimea=null;
        Date endTimea=null;

        if(StringUtils.isNotBlank(startTime)&&!"".equals(startTime)&&!"null".equals(startTime)){
            startTimea=DateUtils.string3Date(startTime);
        }
        else {
            startTimea=null;

        }
        if(StringUtils.isNotBlank(endTime)&&!"".equals(endTime)&&!"null".equals(endTime)){
            endTimea=DateUtils.string3Date(endTime);
        } else {
            endTimea=null;
        }


    // 分为日志查询列表和查询详情

        List<Map<String, Object>> infoList =  orderImportAndExportService.getInfoList(result, startTimea, endTimea, page);
        int infoListCount =  orderImportAndExportService.getInfoListCount(result, startTimea, endTimea, page);
        Map<String,Object> map=new LinkedHashMap<>();
        map.put("list",infoList);
        map.put("count",infoListCount);
        return  new ResponseObject<>(map);
    }



    /*
     * @Author chp
     * @Description     导出日志详情excel
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getInfoList/exportExcel/notErp",method =RequestMethod.GET)
    public void getLogInfoNotERP(@RequestParam("lot") String lot
            ,HttpServletResponse response){
        List<NotErpImportOrderExcel> infoListErpExcel = orderItemMapper.getInfoListNotErpExcel(lot);

        String [] header={
                "订单编号",
                "收件人",
                "联系方式",
                "详细地址",
                "买家留言",
                "商品名称",
                "规格名称",
                "规格编号",
                "商品数量",
                "原因"
        };

        String [] orderlists={
       "orderNo" ,
        //收件人
       "receiver",
        //联系方式
        "phone",
        //收货地址
         "street",
        //买家名称
         "buyerRemark",
        //商品名称
         "goodName",
        //规格名称
         "skuName",
        //规格编码
         "skuCode",
         //商品数量
         "count",
         "reason"
        };

        List<Object>  list=infoListErpExcel.stream().collect(Collectors.toList());
        export2(header,orderlists,list,"日志详情","notErplogInfo",response);
    }




   /*
    * @Author chp
    * @Description     导出日志详情excel
    * @Date
    * @Param
    * @return
    **/
   @RequestMapping(value = "/getInfoList/exportExcel",method =RequestMethod.GET)
    public void getLogInfo(@RequestParam("lot") String lot
   ,HttpServletResponse response){

       List<ErpImportOrderExcel> infoListErpExcel = orderItemMapper.getInfoListErpExcel(lot);

       String [] header={
       "系统订单号",
       "运单号",
       "快递公司",
       "快递模板名称",
       "仓库名称",
       "买家昵称",
       "收件人",
       "联系方式",
       "详细地址",
       "订单编号","插旗颜色","店铺名称","卖家备注","买家留言","快递单备注","订单状态","订单类型",
       "子订单编号","商品名称","商品简称","商家编码","规格名称","规格编码","商品数量","单价",
       "实收金额","优惠金额","运费","下单时间","付款时间","发货时间","打发货单时间","打快递单时间",
       "打发货单操作人","打快递单操作人","省","市","区","原始单号","原因"
       };

       String [] orderlists={
               "erpOrderNo",
               "wayNum",
               "company",
               "orderModel",
               "warehouse",
               "buyName",
               "receiver",
               "phone",
               "street",
               "thirdOrderNo",
               "flagColor",
               "shopName",
               "sellerRemark",
               "buyerRemark",
               "orderRemark",
               "orderStatus",
               "orderType",
               "orderItemNo",
               "goodName",
               "nameSing",
               "sellerNo",
               "skuName",
               "skuCode",
               "count",
               "price",
               "payMoney",
               "discounts",
               "carriage",
               "orderTime",
               "payTime",
               "sendTime",
               "printSend",
               "printNo",
               "printSendMan",
               "printNoMan",
               "province",
               "city",
               "area",
               "firstOrderNo",
               "reason"
       };

        List<Object>  list=infoListErpExcel.stream().collect(Collectors.toList());
        export2(header,orderlists,list,"日志详情","logInfo",response);
    }

  /*
   * @Author chp
   * @Description //下载非erp模板
   * @Date
   * @Param
   * @return
   **/
  @RequestMapping("/temDownLoad")
   public void getNoErpTem(HttpServletResponse response){

//创建HSSFWorkbook对象(excel的文档对象)
      HSSFWorkbook wb = new HSSFWorkbook();
//建立新的sheet对象（excel的表单）
      HSSFSheet sheet=wb.createSheet("非erp导入模板");
//在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
//        HSSFRow row1=sheet.createRow(0);
////创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
//        HSSFCell cell=row1.createCell(0);
////设置单元格内容
//        cell.setCellValue("学员考试成绩一览表");
////合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
//        sheet.addMergedRegion(new CellRangeAddress(0,0,0,3));
//在sheet里创建第二行
      HSSFRow row2=sheet.createRow(0);
//创建单元格并设置单元格内容
      row2.createCell(0).setCellValue("订单编号");
      row2.createCell(1).setCellValue("收件人");
      row2.createCell(2).setCellValue("联系方式");
      row2.createCell(3).setCellValue("详细地址");
      row2.createCell(4).setCellValue("买家留言");
      row2.createCell(5).setCellValue("商品名称");
      row2.createCell(6).setCellValue("规格名称");
      row2.createCell(7).setCellValue("规格编码");
      row2.createCell(8).setCellValue("商品数量");
//在sheet里创建第三行

//.....省略部分代码
      try{

          OutputStream output=response.getOutputStream();
          response.reset();
          response.setHeader("Content-disposition", "attachment; filename=importTem.xls");
          response.setContentType("application/msexcel");
          response.setCharacterEncoding("utf-8");
          response.setContentType("application/msexcel");
          response.setHeader("Access-Control-Allow-Origin", "*");
          response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
          response.setHeader("Access-Control-Allow-Credentials", "true");
          wb.write(output);
          output.close();
      }catch (Exception e){
          log.info("下载模板失败");

      }
//
//      String [] header=new String[]{"订单编号","收件人","联系方式","详细地址","买家留言","商品名称","规格名称","规格编码","商品数量"};
//      List<Map<String,Object>> li=new ArrayList<>();
//      List<Object> list=new LinkedList<>();
//      NotErpTemplate notErpTemplate = new NotErpTemplate();
//      list.add(notErpTemplate);
//      String sheetTitle="非erp模板";
//      String filename="template";
////      Map<String,Object> map =new LinkedHashMap<>();
////      map.put("orderNo","1");
////      map.put("receiver","2");
////      map.put("phone","3");
////      map.put("street","4");
////      map.put("leaveWord","5");
////      map.put("name","6");
////      map.put("skuName","7");
////      map.put("skuCode","8");
////      map.put("count","9");
////      li.add(map);
////      List<NotErpTemplate> notErpTemplates = listMapToBean(li, NotErpTemplate.class);
//      //把user的Name全部拉出来存放到list2中
////      List<Object>  lista = notErpTemplates.stream().collect(Collectors.toList());
//      //      getList(li,NotErpTemplate.class);
////      NotErpTemplate notErpTemplate1  = map2Bean(map, NotErpTemplate.class);
////     NotErpTemplate notErpTemplate = new NotErpTemplate();
//      list.add(notErpTemplate);
//      export(header,list,sheetTitle,filename,response);
   }
    /**
     * @Author chp
     * @Description //查询lot
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getInfoDetails/lot",method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Object>  getInfoDetailsErpLot(@RequestParam("lot") String lot){

        String erp = orderItemMapper.getErp(lot);

        return new ResponseObject<>(erp);
    }





    /**
     * @Author chp
     * @Description //查询导入详情erp
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getInfoDetails/Erp",method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Object>  getInfoDetailsErp(@RequestParam("lot") String lot,Pageable page){
        List<Map<String, Object>> infoDetail = orderImportAndExportService.getInfoListErp(lot,page);
        int infoListErpCount = orderImportAndExportService.getInfoListErpCount(lot);
        Map<String,Object>  map= new LinkedHashMap<>();
        map.put("list",infoDetail);
        map.put("count",infoListErpCount);
        return  new ResponseObject<>(map);
    }


    /**
     * @Author chp
     * @Description //查询详情非erp
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getInfoDetails/notErp",method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Object>  getInfoDetailsNotErp(@RequestParam("lot") String lot,Pageable page){
        List<Map<String, Object>> infoListNotErp = orderItemMapper.getInfoListNotErp(lot, page);
           int    infoListNotErpCount  = orderItemMapper.getInfoListNotErpCount(lot, page);

        Map<String,Object>  map= new LinkedHashMap<>();
        map.put("list",infoListNotErp);
        map.put("count",infoListNotErpCount);
        return  new ResponseObject<>(map);
    }
    /**
     * @Author chp
     * @Description //查询导入详情失效
     * @Date
     * @Param
     * @return
     **/
   @RequestMapping(value = "/getInfoDetails",method = RequestMethod.GET)
   @ResponseBody
   public ResponseObject<Object>  getInfoDetails(String lot){
       List<Map<String, Object>> infoDetail = orderImportAndExportService.getInfoDetail(lot);
       return  new ResponseObject<>(infoDetail);
   }

    /*
     * @Author chp
     * @Description //查询审核列表
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getCheckList",method =RequestMethod.GET )
    @ResponseBody
    public  ResponseObject<Object>  getCheckList(
            String   orderStatus, //订单状态
            String   source ,//来源
            String startTime,//查询起始时间
            String   endTime,  //查询结束时间
            Pageable page
    ){
          Date startTimea=null;
          Date endTimea=null;
          if(StringUtils.isBlank(orderStatus)){
             orderStatus=null;
          }
          if(StringUtils.isBlank(source)){
             source=null;
          }


        if(StringUtils.isNotBlank(startTime)&&!"".equals(startTime)&&!"null".equals(startTime)){
            startTimea=DateUtils.string3Date(startTime);
        }
        else {
            startTimea=null;

        }
        if(StringUtils.isNotBlank(endTime)&&!"".equals(endTime)&&!"null".equals(endTime)){
            endTimea=DateUtils.string3Date(endTime);
        } else {
            endTimea=null;
        }


        List<Map<String, Object>> checkList = orderImportAndExportService.getCheckList(orderStatus, source, startTimea, endTimea, page);
        int checkListCount  = orderImportAndExportService.getCheckListCount(orderStatus, source, startTimea, endTimea, page);
        Map<String,Object> map=new LinkedHashMap<>();
        map.put("list",checkList);
        map.put("count",checkListCount);
        return  new ResponseObject<>(map);
    }
    /*
     * @Author chp
     * @Description //同意审核或拒绝审核
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value ="/submitCheck/{type}",method = RequestMethod.GET)
    @ResponseBody
    @Transactional
    public  ResponseObject<Boolean>  updateStatus ( @RequestParam("lot") String lot , @PathVariable("type") String type){
        if(lot.equals("")||null==lot)  {
            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_CHECK_ERROR,"请检查导入批次");
        }
        String[] lots  = lot.replaceAll("\"", "").split(",");
        if(type.equals("agree")){
            int i = orderImportAndExportService.updateCheck(lots, "1");
            if(0==i){
                throw  new BizException(GlobalErrorCode.NOT_FOUND,"更新失败");
            }
            log.info("审核通过");
            //审核通过后修改订单状态，改为支付状态  修改支付状态
            int i1 = orderImportAndExportService.updateStatus(lots);

            if(0==i1){
             throw  new BizException(GlobalErrorCode.NOT_FOUND,"更新失败");
         }


            //批量扣减sku库存
            List<Map<String,Object>>  skuCount =  orderItemMapper.getSkuCount(lot);
            for (int i2 = 0; i2 < skuCount.size(); i2++) {
                 Long  skuId = (Long)skuCount.get(i2).get("skuId");
                Integer  amount =   (Integer) skuCount.get(i2).get("amount");
//                核对库存是否足够
                int skuAmountCheck  = orderItemMapper.getSkuAmountCheck(skuId);
                if((skuAmountCheck-amount) < 5){
                    throw  new BizException(GlobalErrorCode.ORDER_IMPORT_NO_AMOUNT,"sku库存不足，审核失败skuid="+skuId);
                }
//                 扣减库存
                int i3 = orderItemMapper.updateSkuAmount(skuId, amount);
                if(i3==0){  throw  new BizException(GlobalErrorCode.NOT_FOUND,"库存更新失败");}
                }

        }
        if(type.equals("disAgree")){
            int i = orderImportAndExportService.updateCheck(lots, "2");
//            审核不通过删除订单
            int i1 = orderItemMapper.updateOrderStatue(lots);
            if(0==i||0==i1){
                throw  new BizException(GlobalErrorCode.NOT_FOUND,"更新失败");
            }
            log.info("审核不通过");
        }
        return  new ResponseObject<>(true);

    }



    /*
     * @Author chp
     * @Description //下载物流信息
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/getNotErpWl",method = RequestMethod.GET)
    public void getNotErp(@RequestParam  String orderList,String type,HttpServletResponse response){
        List<Map<String, Object>> erpWl=new ArrayList<>();
        if(type.equals("0")){
            erpWl=orderItemMapper.getNotErpWlAll();

        }
        else {
            if(orderList.equals("")||orderList==null){
                throw  new BizException(GlobalErrorCode.NOT_FOUND,"请选择要导出的订单");
            }
            String[] lots  = orderList.replaceAll("\"", "").split(",");
            erpWl= orderItemMapper.getNotErpWl(lots);
        }

//        查询物流信息


//创建HSSFWorkbook对象(excel的文档对象)
        HSSFWorkbook wb = new HSSFWorkbook();
//建立新的sheet对象（excel的表单）
        HSSFSheet sheet=wb.createSheet("非erp导入模板");
//在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
//        HSSFRow row1=sheet.createRow(0);
////创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
//        HSSFCell cell=row1.createCell(0);
////设置单元格内容
//        cell.setCellValue("学员考试成绩一览表");
////合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
//        sheet.addMergedRegion(new CellRangeAddress(0,0,0,3));
//在sheet里创建第二行
        HSSFRow row2=sheet.createRow(0);
//创建单元格并设置单元格内容
        row2.createCell(0).setCellValue("订单编号");
        row2.createCell(1).setCellValue("汉微订单号");
        row2.createCell(2).setCellValue("收件人");
        row2.createCell(3).setCellValue("联系方式");
        row2.createCell(4).setCellValue("快递公司");
        row2.createCell(5).setCellValue("运单号1");
        row2.createCell(6).setCellValue("运单号2");
        row2.createCell(7).setCellValue("运单号3");
        if(erpWl.size()>0){
            for (int i = 1; i <= erpWl.size(); i++) {
                HSSFRow row=sheet.createRow(i);
                //创建单元格并设置单元格内容
                row.createCell(0).setCellValue(erpWl.get(i-1).get("orderNo").toString());
                row.createCell(1).setCellValue(erpWl.get(i-1).get("hvOrderNo").toString());
                row.createCell(2).setCellValue(erpWl.get(i-1).get("receiver").toString());
                row.createCell(3).setCellValue(erpWl.get(i-1).get("phone").toString());
                row.createCell(4).setCellValue(erpWl.get(i-1).get("company").toString());
                row.createCell(5).setCellValue(erpWl.get(i-1).get("wayNo1").toString());
            }
        }
//在sheet里创建第三行

//.....省略部分代码

        try{
            OutputStream output=response.getOutputStream();
            response.reset();
            response.setHeader("Content-disposition", "attachment; filename=notErpwuliu.xls");
            response.setContentType("application/msexcel");
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/msexcel");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            wb.write(output);
            output.close();
        }catch (Exception e){
            log.info("导出物流信息失败");

        }

    }


    /*
     * @Author chp
     * @Description //  物流erp导出       改
     * @Date
     * @Param
     * @return
     **/
     @RequestMapping(value="/getErpList",method = RequestMethod.GET)
      public  void getErp(@RequestParam  String orderList,String type,HttpServletResponse response){
         List<Map<String, Object>> erpWl=new ArrayList<>();
         if(type.equals("0")){
             erpWl=orderItemMapper.getErpWlAll();

         }

         else {
             if(orderList.equals("")||orderList==null){
                 throw  new BizException(GlobalErrorCode.NOT_FOUND,"请选择要导出的订单");
             }
             String[] lots  = orderList.replaceAll("\"", "").split(",");

             erpWl  = orderImportAndExportService.getErpWl(lots);
         }

         if (null!=erpWl||0==erpWl.size()){
             for (int i = 0; i < erpWl.size(); i++) {
                 String   s= (String) erpWl.get(i).get("orderCode");
                 erpWl.get(i).put("orderCode",getCode(s));
             }
             String [] header=new String[]{"系统订单号","订单编号","物流编码","运单号"};
             String sheetTitle="erp物流信息";
             String filename="erpwuliu";
             List<ErpWuTempalte> erpWuTempaltes  = listMapToBean(erpWl, ErpWuTempalte.class);
             List<Object> list=erpWuTempaltes.stream().collect(Collectors.toList());
            export(header,list,sheetTitle,filename,response);
         }

     }

     /*
      * @Author chp
      * @Description    非erp模板上传
      * @Date
      * @Param
      * @return
      **/
     @RequestMapping(value = "/excelUpload",method = RequestMethod.POST)
     @ResponseBody
     @Transactional(rollbackFor = {BizException.class})
    public ResponseObject<Boolean>   getNotErpExcelList(@RequestParam("file") MultipartFile file,@RequestParam("user") String user)  throws  Exception{
    // 非erp导入无需校验是否重复导入
         //1.校验sku是否存在
         //2.校验库存是否足够
         //3.生成订单过程校验库存是否重复
         File toFile = null;
         if(file.equals("")||file.getSize()<=0){
             file = null;
         }else {
             InputStream ins = null;
             ins = file.getInputStream();
             toFile = new File(file.getOriginalFilename());
             inputStreamToFile(ins, toFile);
             ins.close();
         }

         String [] orderlists={
              "orderNo",
              "receiver",
              "phone",
              "street",
              "buyerRemark",
              "goodName",
              "skuName",
              "skuCode",
              "count"
         };

         List<Object> objects  = ExcelReadHelper.excelRead(toFile, orderlists, NotErpImportOrder.class);
         List<NotErpImportOrder> noterp  = getObjToList(objects, NotErpImportOrder.class);
         if(noterp.size()==0){
             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_ORDER_NULL,"订单数据不能为空");
         }
         List<NotErpImportOrder> orderlist = noterp.stream().filter(s -> s.getSkuCode()!= null && !s.getSkuCode().equals("")).collect(Collectors.toList());
         final String lot = UniqueNoUtils.nextIm(UniqueNoUtils.UniqueNoType.IM);
         String  name=user;
         Long   cpid=3067104L;
         Date date =  new Date();
         //1.订单导入多重判断
//            List<ErpImportOrder> orderlist = JSONArray.parseArray(JSON.toJSONString(map.get("orderList")), ErpImportOrder.class);
         //1查询数据库是否有重复记录
         Optional.of(orderlist).orElseThrow( () -> new BizException(GlobalErrorCode.UNKNOWN, "订单数据不能为空"));
         //sku是否都存在

//              存入记录
         //2查询数据库sku是否都存在

         List<String> skuLlist = orderlist.stream().map(NotErpImportOrder::getSkuCode).filter(x->x!=null).distinct().collect(Collectors.toList());


         List<String> skuListco1 =  orderImportAndExportService.getSkuList(skuLlist);
         if(skuLlist.size()>0&&skuListco1.size()!=skuLlist.size()){
             List<String> skuListco=  skuLlist.stream().filter((String s) -> !skuListco1.contains(s)).collect(Collectors.toList());
             //sku不存在 记录导入日志表
             List<NotErpImportOrder> collect  = orderlist.stream().filter((NotErpImportOrder erp) -> skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
             //记录日志表和日志详表
             try{
                 XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
                 xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
                 xquarkOrderThirdInfoWithBLOBs.setName(name);
                 xquarkOrderThirdInfoWithBLOBs.setLot(lot);
                 xquarkOrderThirdInfoWithBLOBs.setResult("1");
                 xquarkOrderThirdInfoWithBLOBs.setSource("1");
                 xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
                 xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
                 int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
                 for (NotErpImportOrder erpImportOrder : collect) {
                     //sku不存在时记录失败信息
                     XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                     xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                     xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                     xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                     xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                     xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                     xquarkOrderThirdNoerp.setLot(lot);
                     xquarkOrderThirdNoerp.setCreateTime(date);
                     xquarkOrderThirdNoerp.setUpdateTime(date);

                     int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                     //失败时日志详情记录
                     XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                     xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdInfoDetail.setLot(lot);
                     xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdInfoDetail.setImportStatus("0");
                     xquarkOrderThirdInfoDetail.setReason("skuCode不存在");
                     xquarkOrderThirdInfoDetail.setCreateAt(date);
                     xquarkOrderThirdInfoDetail.setUpdateAt(date);
                     int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                     if(1!=insert||1!=insert1||1!=insert2){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }

                 }
                 List<NotErpImportOrder> collect2  = orderlist.stream().filter((NotErpImportOrder erp) -> !skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
//                     未导入记录信息
                 if(collect2.size()>0){
                     for (NotErpImportOrder erpImportOrder : collect2) {
                         XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                         xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                         xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                         xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                         xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                         xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                         xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                         xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                         xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                         xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                         xquarkOrderThirdNoerp.setLot(lot);
                         xquarkOrderThirdNoerp.setCreateTime(date);
                         xquarkOrderThirdNoerp.setUpdateTime(date);
                         int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                         if(1!=insert){
                             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                         }
                     }
                 }
             }catch (Exception e){
//                    改改
//                 throw  new Exception(e.getMessage());
//                    log.info("数据类型错误:"+e.getMessage());
                throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"excel数据有误"+e.getMessage());
             }
             //记录日志表
             ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
             booleanResponseObject.setMoreInfo("导入失败skuCode不存在&lot="+lot);
             return booleanResponseObject;
         }
         //校验所有sku库存是否满足要求，库存是否满足要求
         List<String>   amountList=new LinkedList<>();
         for (NotErpImportOrder erpImportOrder  : orderlist) {
             List<String> amountSku  = orderItemMapper.getAmountSku(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
             if(amountSku.size()>0){
                 amountList.add(amountSku.get(0));
             }
         }
         if(amountList.size()>0){
             //记录日志表
             List<NotErpImportOrder> collect  = orderlist.stream().filter((NotErpImportOrder erp) -> amountList.contains(erp.getSkuCode())).collect(Collectors.toList());
             try {
                 //失败时日志记录
                 XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
                 xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
                 xquarkOrderThirdInfoWithBLOBs.setName(name);
                 xquarkOrderThirdInfoWithBLOBs.setLot(lot);
                 xquarkOrderThirdInfoWithBLOBs.setResult("1");
                 xquarkOrderThirdInfoWithBLOBs.setSource("1");
                 xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
                 xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
                 int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
//                    分别记录成功和失败的日志
                 for (NotErpImportOrder erpImportOrder  : collect) {
                     XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                     xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                     xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                     xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                     xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                     xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                     xquarkOrderThirdNoerp.setLot(lot);
                     xquarkOrderThirdNoerp.setCreateTime(date);
                     xquarkOrderThirdNoerp.setUpdateTime(date);
                     int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                     //失败时日志详情记录
                     XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                     xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdInfoDetail.setLot(lot);
                     xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdInfoDetail.setImportStatus("0");
                     xquarkOrderThirdInfoDetail.setReason("sku库存不足");
                     xquarkOrderThirdInfoDetail.setCreateAt(date);
                     xquarkOrderThirdInfoDetail.setUpdateAt(date);
                     int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                     if(1!=insert||1!=insert1||1!=insert2){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }
                 }
                //库存不足成功信息不记录

             }catch (Exception e){
                 log.info("sku库存不足");
                 throw  new BizException(GlobalErrorCode.ORDER_IMPORT_NO_AMOUNT,"sku库存不足");
             }

             ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
             booleanResponseObject.setMoreInfo("sku库存不足&lot="+lot);
             return booleanResponseObject;
         }
         List<NotErpImportOrder>  amountLi=new ArrayList<>();
         //生成订单的情况 生成订单时校验库存是否足
         try{

             for (NotErpImportOrder erpImportOrder  :  orderlist) {
                 // 生成订单
                 // 生成订单前去校验库存
                 List<String> amountCheck =  orderItemMapper.getAmountCheck(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
                 if(amountCheck.size()>0){
//                      创建订单
                     Either<String, Boolean> winningOrder2  = winningService.createWinningOrder3(cpid, lot, erpImportOrder);

                     Boolean aBoolean = winningOrder2.get();
                     if(!aBoolean){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_CREATE_ERROR,"创建订单失败");
                     }
                 }
                 else {
                     amountLi.add(erpImportOrder);
                 }

             }
             if(amountLi.size()>0)  {
                 List<NotErpImportOrder> collect2  = orderlist.stream().filter((NotErpImportOrder erp) ->amountLi.contains(erp.getSkuCode())).collect(Collectors.toList());
                 //部分导入失败记录审核表
                 XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
                 xquarkOrderThirdCheck.setLot(lot);
                 xquarkOrderThirdCheck.setImportTime(date);
                 xquarkOrderThirdCheck.setSubmitUser(name);
                 xquarkOrderThirdCheck.setSourceFrom("非erp");
                 xquarkOrderThirdCheck.setCheckTime(date);
                 xquarkOrderThirdCheck.setCreateAt(date);
                 xquarkOrderThirdCheck.setCheckState("0");
                 xquarkOrderThirdCheck.setUpdateAt(date);
                 int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
                 if(1!=insert1){
                     throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                 }
                 //部分导入失败记录日志主表
                 XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
                 xquarkOrderThirdInfo.setImportTime(date);
                 xquarkOrderThirdInfo.setName(name);
                 xquarkOrderThirdInfo.setLot(lot);
                 xquarkOrderThirdInfo.setSource("1");
                 xquarkOrderThirdInfo.setResult("2");
                 xquarkOrderThirdInfo.setCreateAt(date);
                 xquarkOrderThirdInfo.setUpdateAt(date);
                 int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
                 if(1!=insert3){
                     throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                 }
                    //导入成功的
                 for (NotErpImportOrder erpImportOrder : collect2) {
                     XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                     xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                     xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                     xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                     xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                     xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                     xquarkOrderThirdNoerp.setLot(lot);
                     xquarkOrderThirdNoerp.setCreateTime(date);
                     xquarkOrderThirdNoerp.setUpdateTime(date);
                     int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                     if (1 != insert) {
                         throw new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR, "excel导入失败，请重新导入");
                     }
                     //失败时日志详情记录
                     XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail = new XquarkOrderThirdInfoDetail();
                     xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdInfoDetail.setLot(lot);
                     xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdInfoDetail.setImportStatus("1");
                     xquarkOrderThirdInfoDetail.setCreateAt(date);
                     xquarkOrderThirdInfoDetail.setUpdateAt(date);
                     int insert2 = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                     if (1 != insert2) {
                         throw new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR, "excel导入失败，请重新导入");
                     }
                 }

                        //导入失败的
                 for (NotErpImportOrder erpImportOrder : amountLi) {
                     XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                     xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                     xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                     xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                     xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                     xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                     xquarkOrderThirdNoerp.setLot(lot);
                     xquarkOrderThirdNoerp.setCreateTime(date);
                     xquarkOrderThirdNoerp.setUpdateTime(date);

                     int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                     if(1!=insert){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }
                     //失败时日志详情记录
                     XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                     xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdInfoDetail.setLot(lot);
                     xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdInfoDetail.setImportStatus("0");
                     xquarkOrderThirdInfoDetail.setReason("sku库存不足");
                     xquarkOrderThirdInfoDetail.setCreateAt(date);
                     xquarkOrderThirdInfoDetail.setUpdateAt(date);
                     int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                     if(1!=insert2){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }

                 }
                 ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
                 booleanResponseObject.setMoreInfo("部分导入失败&lot="+lot);
                 return booleanResponseObject;
             }
             else {
                 //记录审核表 全部成功
                 //记录日志表 全部成功
                 // 记录日志详表全部成功
                 //全部成功审核表
                 XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
                 xquarkOrderThirdCheck.setLot(lot);
                 xquarkOrderThirdCheck.setImportTime(date);
                 xquarkOrderThirdCheck.setSubmitUser(name);
                 xquarkOrderThirdCheck.setSourceFrom("非erp");
                 xquarkOrderThirdCheck.setCheckTime(date);
                 xquarkOrderThirdCheck.setCheckState("0");
                 xquarkOrderThirdCheck.setCreateAt(date);
                 xquarkOrderThirdCheck.setUpdateAt(date);
                 int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
                 if(1!=insert1){
                     throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                 }
                 //全部成功日志主表
                 XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
                 xquarkOrderThirdInfo.setImportTime(date);
                 xquarkOrderThirdInfo.setName(name);
                 xquarkOrderThirdInfo.setLot(lot);
                 xquarkOrderThirdInfo.setResult("0");
                 xquarkOrderThirdInfo.setSource("1");
                 xquarkOrderThirdInfo.setCreateAt(date);
                 xquarkOrderThirdInfo.setUpdateAt(date);
                 int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
                 if(1!=insert3){
                     throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                 }
                 for (NotErpImportOrder erpImportOrder : orderlist) {
                     XquarkOrderThirdNoerp xquarkOrderThirdNoerp  = new XquarkOrderThirdNoerp();
                     xquarkOrderThirdNoerp.setOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdNoerp.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdNoerp.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdNoerp.setStreet(erpImportOrder.getStreet());
                     xquarkOrderThirdNoerp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                     xquarkOrderThirdNoerp.setGoodName(erpImportOrder.getGoodName());
                     xquarkOrderThirdNoerp.setSkuName(erpImportOrder.getSkuName());
                     xquarkOrderThirdNoerp.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdNoerp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                     xquarkOrderThirdNoerp.setLot(lot);
                     xquarkOrderThirdNoerp.setCreateTime(date);
                     xquarkOrderThirdNoerp.setUpdateTime(date);
                     int insert =xquarkOrderThirdNoerpMapper.insertSelective(xquarkOrderThirdNoerp);
                     if(1!=insert){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }
                     //成功时日志详情记录
                     XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                     xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getOrderNo());
                     xquarkOrderThirdInfoDetail.setLot(lot);
                     xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                     xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                     xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                     xquarkOrderThirdInfoDetail.setImportStatus("1");
                     xquarkOrderThirdInfoDetail.setCreateAt(date);
                     xquarkOrderThirdInfoDetail.setUpdateAt(date);
                     int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                     if(1!=insert2){
                         throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                     }
                 }
             }
         }catch (Exception e){
             log.info("excel导入数据异常---------------------------"+e.getMessage());

             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel,数据异常excel导入失败，请重新导入"+e.getMessage());

         }

         ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
         objectResponseObject.setMoreInfo("导入成功&lot="+lot);
         objectResponseObject.setData(true);
         return  objectResponseObject ;
    }










    /*
     * @Author chp
     * @Description //erp excel 上传
     * @Date
     * @Param
     * @return
     **/
    @RequestMapping(value = "/excelUpload/erp",method =RequestMethod.POST)
    @ResponseBody
    @Transactional(rollbackFor = {BizException.class})
    public ResponseObject<Boolean> getExcelList(@RequestParam("file") MultipartFile file,@RequestParam("user") String user) throws Exception{
        File toFile = null;
        if(file.equals("")||file.getSize()<=0){
            file = null;
        }else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }

         String [] orderlists={
                 "erpOrderNo",
                "wayNum",
                "company",
                "orderModel",
                "warehouse",
                "buyName",
                "receiver",
                "phone",
                "street",
                "thirdOrderNo",
                "flagColor",
                "shopName",
                "sellerRemark",
                "buyerRemark",
                "orderRemark",
                "orderStatus",
                "orderType",
                "orderItemNo",
                "goodName",
                "nameSing",
                "sellerNo",
                "skuName",
                "skuCode",
                "count",
                "price",
                "payMoney",
                "discounts",
                "carriage",
                "orderTime",
                "payTime",
                "sendTime",
                "printSend",
                "printNo",
                "printSendMan",
                "printNoMan",
                "province",
                "city",
                "area",
                "firstOrderNo"
         };

             List<Object> objects  = ExcelReadHelper.excelRead(toFile, orderlists, ErpImportOrder.class);
             List<ErpImportOrder> orderlis2  = getObjToList(objects, ErpImportOrder.class);
        if(orderlis2.size()==0){
            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_ORDER_NULL,"订单数据不能为空");
        }
        List<ErpImportOrder> orderlist  = orderlis2.stream().filter(s -> s.getSkuCode() != null && !s.getSkuCode().equals("")).collect(Collectors.toList());

        final String lot = UniqueNoUtils.nextErp(UniqueNoUtils.UniqueNoType.IM);
        String  name=user;
//        String  lot="testlot";
        Long   cpid=3067104L;
        Date date =  new Date();
        //1.订单导入多重判断
//            List<ErpImportOrder> orderlist = JSONArray.parseArray(JSON.toJSONString(map.get("orderList")), ErpImportOrder.class);
            //1查询数据库是否有重复记录
            Optional.of(orderlist).orElseThrow( () -> new BizException(GlobalErrorCode.UNKNOWN, "订单数据不能为空"));
            //有重复记录只存临时表，并记日志
            //查出订单号

            List<String> orderNoList=orderlist.stream().map(ErpImportOrder::getOrderItemNo).collect(Collectors.toList());
            List<String> thirdItemList  = orderItemMapper.getThirdItemList(orderNoList);
            if (thirdItemList.size()>0){
//               过滤出重复订单
                List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> thirdItemList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
//                记录日志写入
                try {
                    //失败时日志记录
                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
                    xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
                    xquarkOrderThirdInfoWithBLOBs.setName(name);
                    xquarkOrderThirdInfoWithBLOBs.setLot(lot);
                    xquarkOrderThirdInfoWithBLOBs.setResult("1");
                    xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
                    xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
                    int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
//                    分别记录成功和失败的日志
                    for (ErpImportOrder erpImportOrder  : collect) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        //生成导入批次
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(date);
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        //失败时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("0");
                        xquarkOrderThirdInfoDetail.setReason("重复导入");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if(1!=insert||1!=insert1||1!=insert2){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel数据异常， excel导入失败，请重新导入");
                        }
                    }
                    //  过滤出非重复订单
                    List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->!thirdItemList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
                    for (ErpImportOrder erpImportOrder  : collect2) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("0");
                        xquarkOrderThirdErp.setCreateTime(new Date());
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        if(0==insert){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                    }
                }catch (Exception e){
                    log.info("数据类型错误");
                    throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"excel导入失败,请检查数据");
                }
//                订单记录
                ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
                booleanResponseObject.setMoreInfo("导入失败订单号已存在&lot="+lot);
                return booleanResponseObject;
            }


//              存入记录
            //2查询数据库sku是否都存在

            List<String> skuLlist = orderlist.stream().map(ErpImportOrder::getSkuCode).filter(x->x!=null).distinct().collect(Collectors.toList());

        List<String> skuListco1 =  orderImportAndExportService.getSkuList(skuLlist);
        if(skuLlist.size()>0&&skuListco1.size()!=skuLlist.size()){
            List<String> skuListco=  skuLlist.stream().filter((String s) -> !skuListco1.contains(s)).collect(Collectors.toList());
                //sku不存在 记录导入日志表
                List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
                //记录日志表和日志详表
                try{
                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
                    xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
                    xquarkOrderThirdInfoWithBLOBs.setName(name);
                    xquarkOrderThirdInfoWithBLOBs.setLot(lot);
                    xquarkOrderThirdInfoWithBLOBs.setResult("1");
                    xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
                    xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
                    int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
                    for (ErpImportOrder erpImportOrder : collect) {
                        //sku不存在时记录失败信息
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        //生成导入批次
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(date);
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        //失败时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("0");
                        xquarkOrderThirdInfoDetail.setReason("skuCode不存在");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if(1!=insert||1!=insert1||1!=insert2){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }

                    }
                    List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) -> !skuListco.contains(erp.getSkuCode())).collect(Collectors.toList());
//                     未导入记录信息
                    if(collect2.size()>0){
                        for (ErpImportOrder erpImportOrder : collect2) {
                            XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                            xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                            xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                            xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                            xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                            xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                            xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                            xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                            xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                            xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                            xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                            xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                            xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                            xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                            xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                            xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                            xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                            xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                            xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                            xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                            xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                            xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                            xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                            xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                            xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                            xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                            xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                            xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                            xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                            xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                            xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                            xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                            xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                            xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                            xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                            xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                            xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                            xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                            xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                            xquarkOrderThirdErp.setLot(lot);
                            xquarkOrderThirdErp.setAchieve("0");
                            xquarkOrderThirdErp.setCreateTime(new Date());
                            int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                            if(1!=insert){
                                throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                            }
                        }
                    }
                }catch (Exception e){
//                  log.info("数据类型错误:"+e.getMessage());
                 throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"excel数据有误"+e.getMessage());
                }
                //记录日志表
                ResponseObject<Boolean>  booleanResponseObject = new ResponseObject<>(false);
                booleanResponseObject.setMoreInfo("导入失败skuCode不存在&lot="+lot);
                return booleanResponseObject;
            }
            //校验所有sku库存是否满足要求，库存是否满足要求
            List<String>   amountList=new LinkedList<>();
            for (ErpImportOrder erpImportOrder  : orderlist) {
                List<String> amountSku  = orderItemMapper.getAmountSku(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
                if(amountSku.size()>0){
                    amountList.add(amountSku.get(0));
                }
            }
            if(amountList.size()>0){
                //记录日志表
                List<ErpImportOrder> collect  = orderlist.stream().filter((ErpImportOrder erp) -> amountList.contains(erp.getSkuCode())).collect(Collectors.toList());
                try {
                    //失败时日志记录
                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfoWithBLOBs = new XquarkOrderThirdInfoWithBLOBs();
                    xquarkOrderThirdInfoWithBLOBs.setImportTime(date);
                    xquarkOrderThirdInfoWithBLOBs.setName(name);
                    xquarkOrderThirdInfoWithBLOBs.setLot(lot);
                    xquarkOrderThirdInfoWithBLOBs.setResult("1");
                    xquarkOrderThirdInfoWithBLOBs.setCreateAt(date);
                    xquarkOrderThirdInfoWithBLOBs.setUpdateAt(date);
                    int insert1 =  xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfoWithBLOBs);
//                    分别记录成功和失败的日志
                    for (ErpImportOrder erpImportOrder  : collect) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        //生成导入批次
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(date);
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        //失败时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("0");
                        xquarkOrderThirdInfoDetail.setReason("sku库存不足");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if(1!=insert||1!=insert1||1!=insert2){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                    }
                    // 重复导入信息的信息插入
                    List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->amountList.contains(erp.getOrderItemNo())).collect(Collectors.toList());
                    for (ErpImportOrder erpImportOrder  : collect2) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("0");
                        xquarkOrderThirdErp.setCreateTime(new Date());
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        if(1!=insert){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                    }
                }catch (Exception e){
                    log.info("数据类型错误");
                    throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"excel数据有误请核对后再导入");
                }

                ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
                booleanResponseObject.setMoreInfo("sku库存不足&lot="+lot);
                return booleanResponseObject;
            }
            List<ErpImportOrder>  amountLi=new ArrayList<>();
            //生成订单的情况 生成订单时校验库存是否足
            try{

                for (ErpImportOrder erpImportOrder  :  orderlist) {
                    // 生成订单
                    // 生成订单前去校验库存
                    List<String> amountCheck =  orderItemMapper.getAmountCheck(erpImportOrder.getSkuCode(), Integer.parseInt(erpImportOrder.getCount()));
                    if(amountCheck.size()>0){
                        Either<String, Boolean> winningOrder2  = winningService.createWinningOrder2(cpid, lot, erpImportOrder);
                        Boolean aBoolean = winningOrder2.get();
                        if(!aBoolean){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_CREATE_ERROR,"创建订单失败");
                        }
                    }
                    else {
                        amountLi.add(erpImportOrder);
                    }

                }
                if(amountLi.size()>0)  {
                    List<ErpImportOrder> collect2  = orderlist.stream().filter((ErpImportOrder erp) ->!amountLi.contains(erp.getSkuCode())).collect(Collectors.toList());
                    //部分导入失败记录审核表
                    XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
                    xquarkOrderThirdCheck.setLot(lot);
                    xquarkOrderThirdCheck.setImportTime(date);
                    xquarkOrderThirdCheck.setSubmitUser(name);
                    xquarkOrderThirdCheck.setSourceFrom("erp");
                    xquarkOrderThirdCheck.setCheckTime(date);
                    xquarkOrderThirdCheck.setCreateAt(date);
                    xquarkOrderThirdCheck.setCheckState("0");
                    xquarkOrderThirdCheck.setUpdateAt(date);
                    int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
                    if(1!=insert1){
                        throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                    }
                    //部分导入失败记录日志主表
                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
                    xquarkOrderThirdInfo.setImportTime(date);
                    xquarkOrderThirdInfo.setName(name);
                    xquarkOrderThirdInfo.setLot(lot);
                    xquarkOrderThirdInfo.setResult("2");
                    xquarkOrderThirdInfo.setCreateAt(date);
                    xquarkOrderThirdInfo.setUpdateAt(date);
                    int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
                    if(1!=insert3){
                        throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                    }

                    for (ErpImportOrder erpImportOrder : collect2) {
                        XquarkOrderThirdErp xquarkOrderThirdErp = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime(DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(new Date());
                        int insert = xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        if (1 != insert) {
                            throw new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR, "excel导入失败，请重新导入");
                        }
                        //失败时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("1");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2 = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if (1 != insert2) {
                            throw new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR, "excel导入失败，请重新导入");
                        }
                    }


                    for (ErpImportOrder erpImportOrder : amountLi) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(new Date());
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        if(1!=insert){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                        //失败时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("0");
                        xquarkOrderThirdInfoDetail.setReason("sku库存不足");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if(1!=insert2){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }

                    }
                    ResponseObject<Boolean> booleanResponseObject = new ResponseObject<>(false);
                    booleanResponseObject.setMoreInfo("部分导入失败&lot"+lot);
                    return booleanResponseObject;
                }
                else {
                    //记录审核表 全部成功
                    //记录日志表 全部成功
                    // 记录日志详表全部成功
                    //全部成功审核表
                    XquarkOrderThirdCheck xquarkOrderThirdCheck   = new XquarkOrderThirdCheck();
                    xquarkOrderThirdCheck.setLot(lot);
                    xquarkOrderThirdCheck.setImportTime(date);
                    xquarkOrderThirdCheck.setSubmitUser(name);
                    xquarkOrderThirdCheck.setSourceFrom("erp");
                    xquarkOrderThirdCheck.setCheckTime(date);
                    xquarkOrderThirdCheck.setCheckState("0");
                    xquarkOrderThirdCheck.setCreateAt(date);
                    xquarkOrderThirdCheck.setUpdateAt(date);
                    int insert1  = xquarkOrderThirdCheckMapper.insert(xquarkOrderThirdCheck);
                    if(1!=insert1){
                        throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                    }
                    //全部成功日志主表
                    XquarkOrderThirdInfoWithBLOBs xquarkOrderThirdInfo  = new XquarkOrderThirdInfoWithBLOBs();
                    xquarkOrderThirdInfo.setImportTime(date);
                    xquarkOrderThirdInfo.setName(name);
                    xquarkOrderThirdInfo.setLot(lot);
                    xquarkOrderThirdInfo.setResult("0");
                    xquarkOrderThirdInfo.setCreateAt(date);
                    xquarkOrderThirdInfo.setUpdateAt(date);
                    int insert3  = xquarkOrderThirdInfoMapper.insert(xquarkOrderThirdInfo);
                    if(1!=insert3){
                        throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                    }
                    for (ErpImportOrder erpImportOrder : orderlist) {
                        XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
                        xquarkOrderThirdErp.setErpOrderNo(erpImportOrder.getErpOrderNo());
                        xquarkOrderThirdErp.setWayNum(erpImportOrder.getWayNum());
                        xquarkOrderThirdErp.setCompany(erpImportOrder.getCompany());
                        xquarkOrderThirdErp.setOrderModel(erpImportOrder.getOrderModel());
                        xquarkOrderThirdErp.setWareHouse(erpImportOrder.getWarehouse());
                        xquarkOrderThirdErp.setBuyName(erpImportOrder.getBuyName());
                        xquarkOrderThirdErp.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdErp.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdErp.setStreet(erpImportOrder.getStreet());
                        xquarkOrderThirdErp.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdErp.setFlagColor(erpImportOrder.getFlagColor());
                        xquarkOrderThirdErp.setShopName(erpImportOrder.getShopName());
                        xquarkOrderThirdErp.setSellerRemark(erpImportOrder.getSellerRemark());
                        xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());
                        xquarkOrderThirdErp.setOrderRemark(erpImportOrder.getOrderRemark());
                        xquarkOrderThirdErp.setOrderStatus(erpImportOrder.getOrderStatus());
                        xquarkOrderThirdErp.setOrderType(erpImportOrder.getOrderType());
                        xquarkOrderThirdErp.setOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdErp.setGoodName(erpImportOrder.getGoodName());
                        xquarkOrderThirdErp.setNameSing(erpImportOrder.getNameSing());
                        xquarkOrderThirdErp.setSkuName(erpImportOrder.getSkuName());
                        xquarkOrderThirdErp.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdErp.setCount(Integer.parseInt(erpImportOrder.getCount()));
                        xquarkOrderThirdErp.setPrice(erpImportOrder.getPrice());
                        xquarkOrderThirdErp.setPayMoney(erpImportOrder.getPayMoney());
                        xquarkOrderThirdErp.setDiscounts(erpImportOrder.getDiscounts());
                        xquarkOrderThirdErp.setCarriage(erpImportOrder.getCarriage());
                        xquarkOrderThirdErp.setOrderTime( DateUtils.string3Date(erpImportOrder.getOrderTime()));
                        xquarkOrderThirdErp.setPayTime(DateUtils.string3Date(erpImportOrder.getPayTime()));
                        xquarkOrderThirdErp.setSendTime(DateUtils.string3Date(erpImportOrder.getSendTime()));
                        xquarkOrderThirdErp.setPrintSend(DateUtils.string3Date(erpImportOrder.getPrintSend()));
                        xquarkOrderThirdErp.setPrintNo(DateUtils.string3Date(erpImportOrder.getPrintNo()));
                        xquarkOrderThirdErp.setPrintSendMan(erpImportOrder.getPrintSendMan());
                        xquarkOrderThirdErp.setPrintNoMan(erpImportOrder.getPrintNoMan());
                        xquarkOrderThirdErp.setProvince(erpImportOrder.getProvince());
                        xquarkOrderThirdErp.setCity(erpImportOrder.getCity());
                        xquarkOrderThirdErp.setArea(erpImportOrder.getArea());
                        xquarkOrderThirdErp.setFirstOrderNo(erpImportOrder.getFirstOrderNo());
                        xquarkOrderThirdErp.setLot(lot);
                        xquarkOrderThirdErp.setAchieve("1");
                        xquarkOrderThirdErp.setCreateTime(new Date());
                        int insert =  xquarkOrderThirdErpMapper.insert(xquarkOrderThirdErp);
                        if(1!=insert){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                        //成功时日志详情记录
                        XquarkOrderThirdInfoDetail xquarkOrderThirdInfoDetail  = new XquarkOrderThirdInfoDetail();
                        xquarkOrderThirdInfoDetail.setThirdOrderNo(erpImportOrder.getThirdOrderNo());
                        xquarkOrderThirdInfoDetail.setLot(lot);
                        xquarkOrderThirdInfoDetail.setPhone(erpImportOrder.getPhone());
                        xquarkOrderThirdInfoDetail.setReceiver(erpImportOrder.getReceiver());
                        xquarkOrderThirdInfoDetail.setThirdOrderItemNo(erpImportOrder.getOrderItemNo());
                        xquarkOrderThirdInfoDetail.setSkuCode(erpImportOrder.getSkuCode());
                        xquarkOrderThirdInfoDetail.setImportStatus("1");
                        xquarkOrderThirdInfoDetail.setCreateAt(date);
                        xquarkOrderThirdInfoDetail.setUpdateAt(date);
                        int insert2  = xquarkOrderThirdInfoDetailMapper.insert(xquarkOrderThirdInfoDetail);
                        if(1!=insert2){
                            throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel导入失败，请重新导入");
                        }
                    }
                }
            }catch (Exception e){
                log.info("excel导入数据异常---------------------------"+e.getMessage());

               throw  new BizException(GlobalErrorCode.ORDER_IMPORT_INSERT_ERROR,"excel,数据异常excel导入失败，请重新导入"+e.getMessage());

            }


//             JSONArray objects1   = new JSONArray(objects);
//             String s = objects1.toJSONString();
//             List<ErpImportOrder> erpImportOrders  = JSONArray.parseArray(s, ErpImportOrder.class);
//             String orderTime  = erpImportOrders.get(0).getOrderTime();
//             String printSend = erpImportOrders.get(0).getPrintSend();
//             Date date =  DateUtils.string3Date(orderTime);
//             Date date2 =  DateUtils.string3Date(printSend);
//             System.out.println("ssss"+objects);
//             System.out.println(date.getTime()+date2.getTime()+"");













//        File fo =new File("D://bbb.xlsx");
//        InputStream as=new FileInputStream(fo);
//        InputStream is = file.getInputStream();
//        Workbook hssfWorkbook = null;
//        hssfWorkbook = new XSSFWorkbook(is);
//        if (file.getName().endsWith("xlsx")){
//            hssfWorkbook = new XSSFWorkbook(is);//Excel 2007
//        }else if (file.getName().endsWith("xls")){
//            hssfWorkbook = new HSSFWorkbook(is);//Excel 2003
//        }
//
//
//
//        System.out.println(hssfWorkbook.toString());
        ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
        objectResponseObject.setMoreInfo("导入成功,lot="+lot);
        objectResponseObject.setData(true);
        return  objectResponseObject ;
    }

   /*
    * @Author chp
    * @Description //用一句话描述作用
    * @Date
    * @Param
    * @return
    **/
    public static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            System.out.println("sss");
            e.printStackTrace();
        }
    }


    /*
   * @Author chp
   * @Description // 根据物流公司名称获取物流编码
   * @Date
   * @Param
   * @return
   **/
    public String getCode(String company){
      if(company.matches(".*京广.*")){
          return  "-1";
      }
        if(company.matches(".*中国邮政.*")){
            return  "1";
        }
        if(company.matches("EMS")){
            return  "2";
        }
        if(company.matches("EMS经济快递")){
            return  "3";
        }
        if(company.matches(".*申通.*")){
            return  "100";
        }
        if(company.matches(".*圆通.*")){
            return  "101";
        }
        if(company.matches(".*韵达.*")){
            return  "102";
        }
        if(company.matches(".*宅急送.*")){
            return  "103";
        }
        if(company.matches(".*百世物流.*")){
            return  "105";
        }
        if(company.matches(".*联邦.*")){
            return  "106";
        }
        if(company.matches(".*德邦物流.*")){
            return  "107";
        }
        if(company.matches(".*中通.*")){
            return  "500";
        }
        if(company.matches(".*百世汇通.*")){
            return  "502";
        }
        if(company.matches(".*天天.*")){
            return  "504";
        }

        if(company.matches(".*顺丰.*")){
            return  "505";
        }
        if(company.matches(".*佳吉.*")){
            return  "1056";
        }
        if(company.matches(".*黑猫.*")){
            return  "1185";
        }
        if(company.matches(".*新邦.*")){
            return  "1186";
        }
        if(company.matches(".*天地.*")){
            return  "1191";
        }
        if(company.matches(".*能达.*")){
            return  "1192";
        }
        if(company.matches(".*龙邦.*")){
            return  "1195";
        }
        if(company.matches(".*快捷.*")){
            return  "1204";
        }
        if(company.matches(".*优速.*")){
            return  "1207";
        }
        if(company.matches(".*增益.*")){
            return  "1208";
        }
        if(company.matches(".*联昊通.*")){
            return  "1214";
        }
        if(company.matches(".*全峰.*")){
            return  "1216";
        }
        if(company.matches(".*城市100.*")){
            return  "1262";
        }
        if(company.matches(".*自用.*")){
            return  "100000";
        }
        if(company.matches(".*国通.*")){
            return  "200143";
        }
        if(company.matches(".*邮政快递包裹.*")){
            return  "200734";
        }
        if(company.matches(".*汇强.*")){
            return  "200982";
        }
        if(company.matches(".*速尔.*")){
            return  "201174";
        }
        if(company.matches(".*信丰.*")){
            return  "202855";
        }
        if(company.matches(".*安能菜鸟.*")){
            return  "1505447848";
        }
        if(company.matches(".*安能物流.*")){
            return  "1409279116817";
        }
        if(company.matches(".*德邦快递.*")){
            return  "5000000110730";
        }
        return "";
    }

    /*
     * @Author chp
     * @Description  excel导出模板
     * @Date
     * @Param
     * @return
     **/
    public void  export2(String [] header,String[] pro, List<Object> list,String sheetTitle,String filename,HttpServletResponse response){
        ExcelExportHelper excelExportHelper =  new ExcelExportHelper();
        try {
            HSSFWorkbook wb  = excelExportHelper.exportExcel(header,pro,list, sheetTitle);
            //输出Excel文件
            OutputStream output=response.getOutputStream();
            response.reset();
            response.setHeader("Content-disposition", "attachment; filename="+filename+".xls");
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/msexcel");
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            response.setHeader("Access-Control-Allow-Credentials", "true");

            wb.write(output);
            output.close();
        }catch (Exception e){
            log.info("下载模板失败");

        }


    }


     /*
      * @Author chp
      * @Description  excel导出模板
      * @Date
      * @Param
      * @return
      **/
     public void  export(String [] header,List<Object> list,String sheetTitle,String filename,HttpServletResponse response){
         ExcelExportHelper excelExportHelper =  new ExcelExportHelper();
         try {
             HSSFWorkbook wb  = excelExportHelper.exportExcel(header,list, sheetTitle);
             //输出Excel文件
             OutputStream output=response.getOutputStream();
             response.reset();
             response.setHeader("Content-disposition", "attachment; filename="+filename+".xls");
             response.setCharacterEncoding("utf-8");
             response.setContentType("application/msexcel");
             response.setHeader("Access-Control-Allow-Origin", "*");
             response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
             response.setHeader("Access-Control-Allow-Credentials", "true");
             wb.write(output);
             output.close();
         }catch (Exception e){
             log.info("下载模板失败");

         }


     }
   /*
    * @Author chp
    * @Description //  将list object转为 list对象
    * @Date
    * @Param
    * @return
    **/
     public  static <T> List<T> getObjToList(List<Object>  list,Class<T> T){

         if(list.size()>0){
             JSONArray objects1   = new JSONArray(list);
             String s = objects1.toJSONString();
             List<T> lis = JSONArray.parseArray(s, T);
            return  lis;
         }
         return  null ;
     }



    /**
     * 将 List<Map>对象转化为List<JavaBean> 此方法已通过测试
     * @author chp
     * @param  -- 要转化的类型
     * @param -- map
     * @return Object对象
     * @version 2016年3月20日 11:03:01
     */
    public static <T > List<T> convertListMap2ListBean(List<Map<String,Object>> listMap, Class T) throws Exception {
        List<T> beanList = new ArrayList<T>();
        for(int i=0, n=listMap.size(); i<n; i++){
            Map<String,Object> map = listMap.get(i);
            T bean = convertMap2Bean(map,T);
            beanList.add(bean);
        }
        return beanList;
    }


    /*
     * @Author chp
     * @Description // list map转list javabean
     * @Date
     * @Param
     * @return
     **/
    public  <T> List<T>  listMapToBean(List<Map<String,Object>> listMap, Class<T> T){
      try {
          if(null!=listMap&&listMap.size()>0){
              List<T> beanList = new ArrayList<T>();
              for(int i=0, n=listMap.size(); i<n; i++){
                  Map<String,Object> map = listMap.get(i);
                  T bean = map2Bean(map,T);
                  beanList.add(bean);
              }
              return beanList;
          }

      }
      catch (Exception E){
           log.info("转换失败");
      }
     return   null;
    }




    /**
     *
     *
     * Map转换层Bean，使用泛型免去了类型转换的麻烦。
     * @param <T>
     * @param map
     * @param class1
     * @return
     */
    public static <T> T map2Bean(Map<String, Object> map, Class<T> class1) {
        T bean = null;
        try {
            bean = class1.newInstance();
            BeanUtils.populate(bean, map);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return bean;
    }



    /**
     * 将 Map对象转化为JavaBean   此方法已经测试通过
     * @author wyply115
     * @param-- type 要转化的类型
     * @param map
     * @return Object对象
     * @version 2016年3月20日 11:03:01
     */
    public static <T> T convertMap2Bean(Map map, Class T) throws Exception {
        if(map==null || map.size()==0){
            return null;
        }
        BeanInfo beanInfo = Introspector.getBeanInfo(T);
        T bean = (T)T.newInstance();
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (int i = 0, n = propertyDescriptors.length; i <n ; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            String upperPropertyName = propertyName.toUpperCase();
            if (map.containsKey(upperPropertyName)) {
                Object value = map.get(upperPropertyName);
                //这个方法不会报参数类型不匹配的错误。
                BeanUtils.copyProperty(bean, propertyName, value);
//用这个方法对int等类型会报参数类型不匹配错误，需要我们手动判断类型进行转换，比较麻烦。
//descriptor.getWriteMethod().invoke(bean, value);
//用这个方法对时间等类型会报参数类型不匹配错误，也需要我们手动判断类型进行转换，比较麻烦。
//BeanUtils.setProperty(bean, propertyName, value);
            }
        }
        return bean;
    }



     @RequestMapping("/testV/{id}")
     public  void  test (@PathVariable("id") String id){

         XquarkOrderThirdErp xquarkOrderThirdErp1  = xquarkOrderThirdErpMapper.selectByPrimaryKey(1L);

         ErpImportOrder erpImportOrder  = new ErpImportOrder();
         XquarkOrderThirdErp xquarkOrderThirdErp  = new XquarkOrderThirdErp();
         xquarkOrderThirdErp.setBuyerRemark(erpImportOrder.getBuyerRemark());

        if(id.equals("1")){
          List<String>  list=   new ArrayList<>();
              list.add("461222913936570623");
            List<String> thirdItemList =  orderItemMapper.getThirdItemList(list);

            System.out.println(thirdItemList);
        }

         if(id.equals("2")){
//            orderItemMapper.   getCpid("")
//             throw  new BizException(GlobalErrorCode.UNKNOWN,"低价错误");
         }
         if(id.equals("3")){
             throw  new BizException(GlobalErrorCode.ORDER_IMPORT_TYPR_ERROR,"数据类型错误");
         }

         if(id.equals("4")){
             log.info("开始同步wms订单数据");
             Date start = new Date();
             List<WmsOrder> wmsOrders = orderService.getWmsOrder2();
             List<WmsOrder> eso =  wmsOrders.stream().filter(x -> x.getOrderNo().equals("ESO190618134817003007")).collect(Collectors.toList());

             Set<String> orderNos = wmsOrderService.loadWmsOrderSet();
             for (WmsOrder o : eso) {
                 try{
                     wmsOrderService.handleWmsOrder(o, orderNos);
                 } catch (Exception e) {
                     log.error("订单{}同步wms数据错误", o.getOrderNo(), e);
                 }
             }
             log.info("wms订单数据同步完成, 耗时: {} s", (new Date().getTime() - start.getTime()) / 1000);
         }





//         List<AmountVo>  list =new ArrayList<>();
//         Map<String,Object> map = new LinkedHashMap<>();
//         AmountVo amountVo =  new AmountVo();
//         amountVo.setSkuCode("ssasa");
//         amountVo.setAmount(11);
//         AmountVo amountVo2 =  new AmountVo();
//         amountVo2.setSkuCode("065589L");
//         amountVo2.setAmount(1);
//         list.add(amountVo);
//         list.add(amountVo2);
//
//         System.out.println("ssasas"+c.size());

     }



}



