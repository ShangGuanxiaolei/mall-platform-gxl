package com.xquark.restapi.pintuan;

import com.xquark.dal.model.AddedTaxBill;
import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.PersonalBill;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.openapi.product.form.OrderSumbitFormApi;
import com.xquark.restapi.order.PromotionInfoForm;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Pattern;
import org.apache.commons.collections.CollectionUtils;

/**
 * 提交订单Form表单逻辑 扩展下单Form，新增支付方式
 *
 * @author odin
 */
public class PieceOrderSumbitFormApi extends OrderSumbitFormApi {

  // =========== 老拼团参数 - 提交接口转移后清除
  private PGMemberInfoVo pgMemberInfoVo;

  private PGTranInfoVo pgTranInfoVo;
  // ======================================


  public PGMemberInfoVo getPgMemberInfoVo() {
    return pgMemberInfoVo;
  }

  public void setPgMemberInfoVo(PGMemberInfoVo pgMemberInfoVo) {
    this.pgMemberInfoVo = pgMemberInfoVo;
  }

  public PGTranInfoVo getPgTranInfoVo() {
    return pgTranInfoVo;
  }

  public void setPgTranInfoVo(PGTranInfoVo pgTranInfoVo) {
    this.pgTranInfoVo = pgTranInfoVo;
  }
}