package com.xquark.restapi;

/**
 * @auther liuwei
 * @date 2018/6/25 22:22
 */
public class ResponsePOrder<T> {
  private String code;
  private String message;
  private Integer numtotalorder;
  private T orders;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getNumtotalorder() {
    return numtotalorder;
  }

  public void setNumtotalorder(Integer numtotalorder) {
    this.numtotalorder = numtotalorder;
  }

  public T getOrders() {
    return orders;
  }

  public void setOrders(T orders) {
    this.orders = orders;
  }

  @Override
  public String toString() {
    return "ResponsePOrder{" +
        "code='" + code + '\'' +
        ", message='" + message + '\'' +
        ", numtotalorder=" + numtotalorder +
        ", orders=" + orders +
        '}';
  }
}
