package com.xquark.restapi.category;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;
import org.springframework.beans.BeanUtils;

import com.xquark.dal.model.Category;

public class CategoryVO extends Category {

  private long productCount;

  private long sellerCount;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String categoryImgUrl;

  private String displayName;

  private String categoryH5Url;

  private Boolean onSaleForSeller;

  public CategoryVO() {
  }

  public CategoryVO(Category cat) {
    BeanUtils.copyProperties(cat, this);
  }

  public Boolean getOnSaleForSeller() {
    return onSaleForSeller;
  }

  public void setOnSaleForSeller(Boolean onSaleForSeller) {
    this.onSaleForSeller = onSaleForSeller;
  }

  public String getCategoryH5Url() {
    return categoryH5Url;
  }

  public void setCategoryH5Url(String categoryH5Url) {
    this.categoryH5Url = categoryH5Url;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getCategoryImgUrl() {
    return categoryImgUrl;
  }

  public void setCategoryImgUrl(String categoryImgUrl) {
    this.categoryImgUrl = categoryImgUrl;
  }

  public long getSellerCount() {
    return sellerCount;
  }

  public void setSellerCount(long sellerCount) {
    this.sellerCount = sellerCount;
  }

  public long getProductCount() {
    return productCount;
  }

  public void setProductCount(long productCount) {
    this.productCount = productCount;
  }

}
