package com.xquark.restapi.product;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 2018/4/16. DESC:
 */
public class PromotionFullCutProductFormVO {

  private String productId;

  private BigDecimal discount;

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }
}
