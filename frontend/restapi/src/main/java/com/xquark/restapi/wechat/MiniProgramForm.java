package com.xquark.restapi.wechat;

/**
 * Created by jqx on 18/8/3.
 */
public class MiniProgramForm {
  private String encryptedData;
  private String iv;
  private String code;
  private String cpToken;
  private Long cpId;
  private String productId;

  private String tranCode;

  private String scanCodeType; //小程序扫码跳转码类型

  private Long fromCpId; //被扫的cpId

  private String spId; //推广人

  public String getSpId() {
    return spId;
  }

  public void setSpId(String spId) {
    this.spId = spId;
  }

  public Long getFromCpId() {
    return fromCpId;
  }

  public void setFromCpId(Long fromCpId) {
    this.fromCpId = fromCpId;
  }

  public String getScanCodeType() {
    return scanCodeType;
  }

  public void setScanCodeType(String scanCodeType) {
    this.scanCodeType = scanCodeType;
  }

  public String getTranCode() {
    return tranCode;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }

  public String getEncryptedData() {
    return encryptedData;
  }

  public void setEncryptedData(String encryptedData) {
    this.encryptedData = encryptedData;
  }

  public String getIv() {
    return iv;
  }

  public void setIv(String iv) {
    this.iv = iv;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getCpToken() {
    return cpToken;
  }

  public void setCpToken(String cpToken) {
    this.cpToken = cpToken;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }
}
