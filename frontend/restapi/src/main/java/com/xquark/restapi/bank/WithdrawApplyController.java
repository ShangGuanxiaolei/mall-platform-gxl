package com.xquark.restapi.bank;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.vo.WithdrawAdmin;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.outpay.impl.TenPaymentImpl;
import com.xquark.service.pay.PayRequestService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.UserService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class WithdrawApplyController extends BaseController {

  @Autowired
  WithdrawApplyService withdrawApplyService;

  @Autowired
  UserService userService;

  @Autowired
  PayRequestService payRequestService;

  @Autowired
  private TenPaymentImpl tenPayment;

  @Autowired
  private ExcelService excelService;


  @Autowired
  private ShopTreeService shopTreeService;


  @ResponseBody
  @RequestMapping("/withdraw/save")
  public ResponseObject<Boolean> save(@Valid @ModelAttribute WithdrawApplyForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    WithdrawApply withdrawApply = new WithdrawApply();
    BeanUtils.copyProperties(form, withdrawApply);
    int type = userService.load(form.getUserId()).getWithdrawType();
    withdrawApply.setType(type);
    if (StringUtils.isBlank(form.getId())) {
      return new ResponseObject<>(withdrawApplyService.insert(withdrawApply) == 1);
    } else {
      return new ResponseObject<>(withdrawApplyService.update(withdrawApply) == 1);
    }
  }

  @ResponseBody
  @RequestMapping("/withdraw/list")
  public ResponseObject<Map<String, Object>> list(Model modal, Pageable pageable,
      WithdrawApplySearchForm withdrawApplySearchForm, String shopId) {
    Map<String, Object> param = new HashMap<>();
    if (StringUtils.isNotEmpty(withdrawApplySearchForm.getSellerName())) {
      param.put("sellerName", "%" + withdrawApplySearchForm.getSellerName() + "%");
    }
    if (StringUtils.isNotEmpty(withdrawApplySearchForm.getSellerPhone())) {
      param.put("phone", "%" + withdrawApplySearchForm.getSellerPhone() + "%");
    }
    if (withdrawApplySearchForm.getStatus() != null) {
      param.put("status", withdrawApplySearchForm.getStatus());
    }
    if (withdrawApplySearchForm.getStartDate() != null) {
      param.put("startDate", withdrawApplySearchForm.getStartDate());
    }
    if (withdrawApplySearchForm.getEndDate() != null) {
      param.put("endDate", withdrawApplySearchForm.getEndDate());
    }

    if (StringUtils.isBlank(shopId)) {//后台使用，不传shopId
      param.put("rootShopId", getCurrentIUser().getShopId());
    } else {//app使用，传入shopId
      String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
      param.put("rootShopId", rootShopId);
    }

    List<WithdrawAdmin> result = withdrawApplyService.listWithdrawApply(param, pageable);
    Map<String, Object> aRetMap = new HashMap<>();

    aRetMap.put("withdrawTotal", withdrawApplyService.countWithdrawApply(param));
    aRetMap.put("list", result);

    return new ResponseObject<>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/withdraw/applylist")
  public ResponseObject<Map<String, Object>> applylist(Model modal, Pageable pageable,
      WithdrawApplySearchForm withdrawApplySearchForm) {
    Map<String, Object> param = new HashMap<>();
    param.put("userId", getCurrentIUser().getId());
    List<WithdrawAdmin> result = withdrawApplyService.listWithdrawApply(param, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("list", result);

    return new ResponseObject<>(aRetMap);
  }

  /**
   * 推客提现打款
   */
  @ResponseBody
  @RequestMapping(value = "/withdraw/withdrawpay")
  public ResponseObject<Boolean> withdrawPay(HttpServletRequest req, HttpServletResponse resp,
      String withdrawApplyIds, String withdrawconfirmMoneys) {
    try {
      String[] ids = withdrawApplyIds.split(",");
      String[] confirmMoneys = withdrawconfirmMoneys.split(",");
      String rootShopId = getCurrentIUser().getShopId();
      for (int i = 0; i < ids.length; i++) {
        tenPayment.withdraw(ids[i], confirmMoneys[i], rootShopId);
      }

    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "批量付款到微信用户，请求失败", e);
    }
    return new ResponseObject(true);
  }

  /**
   * 提现申请导出
   */
  @ResponseBody
  @RequestMapping("/withdraw/exportExcel")
  public void export2Excel(WithdrawApplySearchForm withdrawApplySearchForm,
      HttpServletResponse resp) {
    Map<String, Object> param = new HashMap<>();
    if (StringUtils.isNotEmpty(withdrawApplySearchForm.getSellerName())) {
      param.put("sellerName", "%" + withdrawApplySearchForm.getSellerName() + "%");
    }
    if (StringUtils.isNotEmpty(withdrawApplySearchForm.getSellerPhone())) {
      param.put("phone", "%" + withdrawApplySearchForm.getSellerPhone() + "%");
    }
    if (withdrawApplySearchForm.getStatus() != null) {
      param.put("status", withdrawApplySearchForm.getStatus());
    }
    param.put("startDate", withdrawApplySearchForm.getStartDate());
    param.put("endDate", withdrawApplySearchForm.getEndDate());
    param.put("rootShopId", getCurrentIUser().getShopId());

    List<WithdrawAdmin> result = withdrawApplyService.listWithdrawApply4WxExport(param);

    String sheetStr = "提现申请详情";
    String filePrefix = "withdrawApplyList";
    String[] secondTitle = new String[]{
        "创建时间",
        "推客名称", "联系电话", "申请金额",
        "支付金额", "结算时间", "支付方式",
        "状态"};

    String[] strBody = new String[]{
        "getCreatedAtStr",
        "getAccountName", "getPhone", "getApplyMoney",
        "getConfirmMoney", "getPayAtStr", "getOpeningBank",
        "getStatusStr"};

    param.put("startDate", getDateStr(withdrawApplySearchForm.getStartDate()));
    param.put("endDate", getDateStr(withdrawApplySearchForm.getEndDate()));
    excelService.export(filePrefix, result, WithdrawAdmin.class, sheetStr,
        transParams2Title(param), secondTitle, strBody, resp, true);
  }


  private String transParams2Title(Map<String, Object> params) {
    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          result += "{" + key + "=" + value.toString() + "}";
          i++;
        }
      }
    }
    return result;
  }

  public String getDateStr(Date date) {
    if (date == null) {
      return "";
    } else {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
          "yyyy-MM-dd");
      return simpleDateFormat.format(date);
    }
  }

}
