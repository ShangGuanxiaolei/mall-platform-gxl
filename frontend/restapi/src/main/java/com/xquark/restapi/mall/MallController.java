package com.xquark.restapi.mall;

import static java.lang.Math.random;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.mail.MailController;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.mail.SendMail;
import com.xquark.service.mall.MallService;
import com.xquark.service.product.ProductService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chh on 16-9-13.
 */
@Controller
@ApiIgnore
public class MallController extends BaseController {

  @Autowired
  private MallService mallService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private ProductService productService;

  @Autowired
  //利用spring注入后就可以取所有登录用户，不能加static属性，否则会取不到
  private SessionRegistry sessionRegistry;

  /**
   * 商城概况中今日交易额，今日订单数,总订单，已成功订单等
   */
  @ResponseBody
  @RequestMapping("/mall/getSummary")
  public ResponseObject<Map<String, Object>> getSummary(HttpServletRequest req) {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    summary = mallService.getSummary(shopId);

    Map userInfo = userAgentService.getUserInfo();

    HttpSession session = req.getSession();
    ServletContext application = session.getServletContext();

    // 在application范围由一个HashSet集保存所有的session
    HashSet sessions = (HashSet) application.getAttribute("sessions");
    /**Iterator iterator = sessions.iterator();
     while (iterator.hasNext()){
     HttpSession ses = (HttpSession)iterator.next();
     SessionInformation info = sessionRegistry.getSessionInformation(ses.getId());
     }

     List<Object> slist = sessionRegistry.getAllPrincipals();
     // 允许用户多次登陆，因此这里可能有重复用户数据，需要过滤
     HashMap userMap = new HashMap();
     for(Object object : slist){
     if(object instanceof  User){
     User user = (User)object;
     userMap.put(user.getId(),user.getId());
     }
     }**/

    userInfo.put("onLineUser", sessions != null ? sessions.size() : 0);
    summary.put("userInfo", userInfo);
    return new ResponseObject<>(summary);
  }

  /**
   * 商城概况中在线用户
   */
  @ResponseBody
  @RequestMapping("/mall/getUsers")
  public ResponseObject<ArrayList<Map>> getUsers() {
    ArrayList list = new ArrayList();
    Map<String, String> data = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();

    for (int i = 0; i < 24; i++) {
      data = new HashMap<>();
      data.put("key", "商家安卓用户");
      data.put("value", "" + (100 + (int) (random() * 100)));
      String hour = "";
      if (i < 10) {
        hour = "0" + i;
      } else {
        hour = "" + i;
      }
      data.put("date", "01/06/15 " + hour + ":00");
      list.add(data);
    }

    for (int i = 0; i < 24; i++) {
      data = new HashMap<>();
      data.put("key", "商家苹果用户");
      data.put("value", "" + (100 + (int) (random() * 100)));
      String hour = "";
      if (i < 10) {
        hour = "0" + i;
      } else {
        hour = "" + i;
      }
      data.put("date", "01/06/15 " + hour + ":00");
      list.add(data);
    }

    for (int i = 0; i < 24; i++) {
      data = new HashMap<>();
      data.put("key", "买家安卓用户");
      data.put("value", "" + (100 + (int) (random() * 100)));
      String hour = "";
      if (i < 10) {
        hour = "0" + i;
      } else {
        hour = "" + i;
      }
      data.put("date", "01/06/15 " + hour + ":00");
      list.add(data);
    }

    for (int i = 0; i < 24; i++) {
      data = new HashMap<>();
      data.put("key", "买家苹果用户");
      data.put("value", "" + (100 + (int) (random() * 100)));
      String hour = "";
      if (i < 10) {
        hour = "0" + i;
      } else {
        hour = "" + i;
      }
      data.put("date", "01/06/15 " + hour + ":00");
      list.add(data);
    }

    return new ResponseObject<ArrayList<Map>>(list);
  }

  /**
   * 商城概况中交易额
   */
  @ResponseBody
  @RequestMapping("/mall/getSales")
  public ResponseObject<ArrayList<Map>> getSales() {
    ArrayList datalist = new ArrayList();
    ArrayList list = new ArrayList();
    Map<String, String> data = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();

    for (int i = 0; i < 7; i++) {
      data = new HashMap<>();
      data.put("value", "" + (100 + (int) (random() * 100)));
      String day = (14 + i) + "";
      data.put("date", "2015/06/" + day);
      datalist.add(data);
    }

    HashMap map = new HashMap();
    map.put("name", "交易额");
    map.put("values", datalist);
    list.add(map);

    return new ResponseObject<ArrayList<Map>>(list);
  }

  /**
   * 商城概况中一个月交易额
   */
  @ResponseBody
  @RequestMapping("/mall/getSalesMonth")
  public ResponseObject<ArrayList<Map>> getSalesMonth() {
    ArrayList datalist = new ArrayList();
    ArrayList list = new ArrayList();
    Map<String, String> data = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();

    for (int i = 1; i < 32; i++) {
      data = new HashMap<>();
      data.put("value", "" + (100 + (int) (random() * 100)));
      String day = "";
      if (i < 10) {
        day = "0" + i;
      } else {
        day = "" + i;
      }
      data.put("date", "2015-06-" + day);
      list.add(data);
    }

    return new ResponseObject<ArrayList<Map>>(list);
  }

  @ResponseBody
  @RequestMapping(value = "/mall/import/product")
  public ResponseObject<Void> productImport(@RequestParam("url") String url){
    log.info("===============================开始商品导入=================================");
    try {
      URL netUrl = new URL(url);
      log.info("===============================调用service层处理====================================");
      Integer excelLength = productService.importExcelToInsert(netUrl.openStream());
      return new ResponseObject<>("已导入"+excelLength+"行数据",GlobalErrorCode.SUCESS);
    }catch (RuntimeException e){
      log.error("商品导入异常",e);
      return new ResponseObject<>(e.getMessage(),GlobalErrorCode.FILE_SIZE_TOO_LARGE);
    }catch (Exception e){
      log.error("商品导入异常",e);
    }
    return new ResponseObject<>("导入完成",GlobalErrorCode.SUCESS);
  }


}
