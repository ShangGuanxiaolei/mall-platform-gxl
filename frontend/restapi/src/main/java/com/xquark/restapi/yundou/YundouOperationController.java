package com.xquark.restapi.yundou;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.YundouOperation;
import com.xquark.dal.model.YundouOperationType;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.yundou.YundouAmountService;
import com.xquark.service.yundou.YundouOperationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-5-3. DESC:
 */
@RestController
@ApiIgnore
public class YundouOperationController {

  private static Logger logger = LoggerFactory.getLogger(YundouOperationController.class);

  @Autowired
  private YundouOperationService operationService;

  @Autowired
  private YundouAmountService amountService;

  /**
   * 新增操作
   *
   * @param yundouOperation 操作对象
   * @return 是否成功
   */
  @RequestMapping("/yundou/operation/save")
  public ResponseObject<Boolean> saveOperation(YundouOperation yundouOperation) {

    if (StringUtils.isBlank(yundouOperation.getName())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "操作名称不能为空");
    }

    if (yundouOperation.getCode() == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "操作代码不能为空");
    }

    if (StringUtils.isBlank(yundouOperation.getTypeId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "操作类型不能为空");
    }

    Boolean result;
    String id = yundouOperation.getId();
    if (StringUtils.isBlank(id)) {
      // 创建
      yundouOperation.setId(null);
      yundouOperation.setEnable(true);
      YundouOperation exists = operationService.loadByName(yundouOperation.getName());
      if (exists != null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该名称已存在");
      }
      result = operationService.save(yundouOperation);
    } else {
      // 更新
      YundouOperation exists = operationService.load(id);
      if (exists == null) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的操作不存在");
      }
      result = operationService.updateById(yundouOperation);
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/yundou/operation/remove/{id}")
  public ResponseObject<Boolean> removeOperation(@PathVariable String id) {
    YundouOperation exists = operationService.load(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要删除的操作不存在");
    }
    return new ResponseObject<>(operationService.deleteById(id));
  }

  @RequestMapping("/yundou/operation/show/{id}")
  public ResponseObject<YundouOperation> show(@PathVariable String id) {
    YundouOperation exists = operationService.load(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "查询的操作不存在");
    }
    return new ResponseObject<>(exists);
  }

  /**
   * 列出所有操作类型
   */
  @RequestMapping("/yundou/operation/types")
  public ResponseObject<Map<String, Object>> listTypes() {
    Map<String, Object> result = new HashMap<>();
    List<YundouOperationType> types = operationService.listTypes();
    Integer counts = operationService.countTypes();
    result.put("list", types);
    result.put("total", counts);
    return new ResponseObject<>(result);
  }

  /**
   * 列出所有操作
   *
   * @param order 顺序
   * @param pageable 分页
   * @param direction 方向
   */
  @RequestMapping("/yundou/operation/list")
  public ResponseObject<Map<String, Object>> listOperation(
      @RequestParam(value = "order", required = false, defaultValue = "code") String order,
      Pageable pageable,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction) {
    Map<String, Object> result = new HashMap<>();
    List<YundouOperation> yundouOperations = operationService
        .list(order, pageable, Sort.Direction.fromString(direction));
    Integer count = operationService.count();
    result.put("total", count);
    result.put("list", yundouOperations);
    return new ResponseObject<>(result);
  }

}
