package com.xquark.restapi.bonus;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.vo.BonusVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.bonus.BonusService;
import com.xquark.service.excel.ExcelService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 代理用户分红controller Created by chh on 17-6-7.
 */
@Controller
@ApiIgnore
public class BonusController extends BaseController {

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private BonusService bonusService;

  @Autowired
  private ExcelService excelService;

  /**
   * 获取所有代理的分红信息
   */
  @ResponseBody
  @RequestMapping("/bonus/list")
  public ResponseObject<Map<String, Object>> listCommission(Pageable pageable,
      HttpServletRequest request) {
    String shopId = this.getCurrentIUser().getShopId();
    List<BonusVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String name = request.getParameter("name");
    String phone = request.getParameter("phone");
    String type = request.getParameter("type");
    String year = request.getParameter("year");
    String month = request.getParameter("month");
    String status = request.getParameter("status");

    params.put("type", type);
    params.put("year", year);
    params.put("month", month);
    params.put("status", status);

    if (StringUtils.isNoneEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNoneEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }

    viewPages = bonusService.list(pageable, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", bonusService.selectCnt(params));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 更新分红发放状态
   */
  @ResponseBody
  @RequestMapping("bonus/updateStatus")
  public ResponseObject<Boolean> updateStatus(@RequestParam("ids") String ids,
      @RequestParam("status") String status) {
    Boolean result = bonusService.updateStatus(ids, status);
    return new ResponseObject<>(result);
  }

  /**
   * 代理月度分红列表导出
   */
  @ResponseBody
  @RequestMapping("/bonus/exportMonth")
  public void exportMonth(HttpServletRequest request, HttpServletResponse resp) {

    List<BonusVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String name = request.getParameter("name");
    String phone = request.getParameter("phone");
    String year = request.getParameter("year");
    String month = request.getParameter("month");
    String status = request.getParameter("status");

    params.put("type", "MONTH");
    params.put("year", year);
    params.put("month", month);
    params.put("status", status);

    if (StringUtils.isNoneEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNoneEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }

    viewPages = bonusService.list(null, params);

    String sheetStr = "代理月度分红";
    String filePrefix = "bonusMonthList";
    String[] secondTitle = new String[]{
        "分红年度",
        "分红月度",
        "返利人",
        "返利人手机号",
        "分红等级",
        "分红比例",
        "分红总金额",
        "分红金额",
        "直营店下单数",
        "一级直营下单数",
        "二级直营下单数",
        "发放状态"};

    String[] strBody = new String[]{
        "getYear",
        "getMonth",
        "getUserName",
        "getUserPhone",
        "getLevel",
        "getRate",
        "getAmount",
        "getFee",
        "getFirstNum",
        "getSecondNum",
        "getThirdNum",
        "getStatusStr"};

    excelService.export(filePrefix, viewPages, BonusVO.class, sheetStr,
        transParams2Title(params), secondTitle, strBody, resp, true);
  }

  /**
   * 代理年度分红列表导出
   */
  @ResponseBody
  @RequestMapping("/bonus/exportYear")
  public void exportYear(HttpServletRequest request, HttpServletResponse resp) {

    List<BonusVO> viewPages = null;
    Map<String, Object> params = new HashMap<>();
    String name = request.getParameter("name");
    String phone = request.getParameter("phone");
    String year = request.getParameter("year");
    String status = request.getParameter("status");

    params.put("type", "YEAR");
    params.put("year", year);
    params.put("status", status);

    if (StringUtils.isNoneEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNoneEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }

    viewPages = bonusService.list(null, params);

    String sheetStr = "代理年度分红";
    String filePrefix = "bonusYearList";
    String[] secondTitle = new String[]{
        "分红年度",
        "返利人",
        "返利人手机号",
        "分红等级",
        "分红比例",
        "分红总金额",
        "分红金额",
        "发放状态"};

    String[] strBody = new String[]{
        "getYear",
        "getUserName",
        "getUserPhone",
        "getLevel",
        "getRate",
        "getAmount",
        "getFee",
        "getStatusStr"};

    excelService.export(filePrefix, viewPages, BonusVO.class, sheetStr,
        transParams2Title(params), secondTitle, strBody, resp, true);
  }

  private String transParams2Title(Map<String, Object> params) {
    LinkedHashMap keyCn = new LinkedHashMap();
    keyCn.put("year", "分红年度");
    keyCn.put("month", "分红月度");
    keyCn.put("name", "收利人");
    keyCn.put("phone", "收利人手机");

    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        if ("type".equals(key)) {
          continue;
        }
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          result += "{" + keyCn.get(key) + "=" + value.toString().replaceAll("%", "") + "}";
          i++;
        }
      }
    }
    return "分红查询条件：" + result;

  }

  /**
   * 获取所有代理的分红信息
   */
  @ResponseBody
  @RequestMapping("/bonus/getMyBonus")
  public ResponseObject<ArrayList<Map>> getMyBonus(Pageable pageable, HttpServletRequest request) {
    String userId = this.getCurrentIUser().getId();
    ArrayList<Map> vos = null;
    // 先获取该用户所有的分红记录
    vos = bonusService.getMyBonus(userId);
    return new ResponseObject<>(vos);
  }

}
