package com.xquark.restapi.category;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Activity;
import com.xquark.dal.model.ActivityTicket;
import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.model.PreferentialType;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.status.ActivityTicketAuditStatus;
import com.xquark.dal.type.ActivityChannel;
import com.xquark.dal.type.ActivityStatus;
import com.xquark.dal.type.ActivityType;
import com.xquark.dal.vo.ActivityEX;
import com.xquark.dal.vo.CampaignProductEX;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.activity.ActivityVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class ActivityController extends BaseController {

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ProductService productService;

  @Autowired
  private ActivityService activityService;

  @Autowired
  private ShopService shopService;

  @Value("${site.web.host.name}")
  private String siteHost;


  /**
   * 促销活动(分销商品)
   */
  @RequestMapping("/activity/one")
  @ResponseBody
  public ResponseObject<List<ProductVO>> products(HttpServletRequest req, Pageable pageable,
      @RequestParam(required = false) Integer cateid) {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.info("user does not login");
    }
    // Pageable pageable = new PageRequest(page - 1, DEFAULT_PAGE_SIZE);
    int cid = 0;
    if (cateid == null) {
      cid = 0;
    } else {
      cid = cateid;
    }
    List<Product> prods = productService.listActivityProducts(pageable, cid);
    List<ProductVO> products = new ArrayList<>();
    for (Product p : prods) {
      p.setFakeSales(null == p.getFakeSales() ? 0 : p.getFakeSales());
      ProductVO prodVO = new ProductVO(p);
      prodVO.setImgUrl(p.getImg());
      if (user != null) {
        prodVO.setUrl(siteHost + "/p/" + p.getId() + "?union_id=" + user.getId());
      } else {
        prodVO.setUrl(siteHost + "/p/" + p.getId());
      }
      prodVO.setSales(p.getFakeSales()); // 使用fake数据
      prodVO.setCommission(null == prodVO.getPrice()
          || null == prodVO.getCommissionRate() ? null : prodVO
          .getPrice().multiply(prodVO.getCommissionRate()));
      products.add(prodVO);
    }
    return new ResponseObject<>(products);
  }

  /**
   * 新促销活动(分销商品)
   */
  @RequestMapping("/activity/new")
  @ResponseBody
  public ResponseObject<List<ProductVO>> categoryproducts(HttpServletRequest req, Pageable pageable,
      @RequestParam(required = false) Integer cateid) {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.info("user does not login");
    }
    // Pageable pageable = new PageRequest(page - 1, DEFAULT_PAGE_SIZE);
    int cid = 0;
    if (cateid == null) {
      cid = 0;
    } else {
      cid = cateid;
    }
    List<Product> prods = productService.listActivityProducts(pageable, cid, user.getId());
    List<ProductVO> products = new ArrayList<>();
    for (Product p : prods) {
      p.setFakeSales(null == p.getFakeSales() ? 0 : p.getFakeSales());
      ProductVO prodVO = new ProductVO(p);
      prodVO.setImgUrl(p.getImg());
      if (user != null) {
        prodVO.setUrl(siteHost + "/p/" + p.getId() + "?union_id=" + user.getId());
      } else {
        prodVO.setUrl(siteHost + "/p/" + p.getId());
      }
      prodVO.setSales(p.getFakeSales()); // 使用fake数据
      prodVO.setCommission(null == prodVO.getPrice()
          || null == prodVO.getCommissionRate() ? null : prodVO
          .getPrice().multiply(prodVO.getCommissionRate()));
      products.add(prodVO);
    }
    return new ResponseObject<>(products);
  }

  /**
   * 卖家用户创建活动基本信息保存
   */
  @RequestMapping("/activity/save")
  @ResponseBody
  public ResponseObject<ActivityVO> save(@Valid @ModelAttribute ActivitySaveForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    if (form.getStartTime() - new Date().getTime() < 10 * 60 * 1000) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动开始时间必须至少10分钟之后");
    }
    if (form.getEndTime() - form.getStartTime() < 10 * 60 * 1000) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动持续时间不得小于10分钟");
    }
    if (form.getDiscount() != null && (form.getDiscount() <= 0 || form.getDiscount() > 1)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "折扣必须是大于0小于1的数值");
    }

    Activity activity = transForm2Activity(form);
    if (activity == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动Type类型异常");
    }
    Activity po = activityService.saveActivity(activity);

    if (po.getType() == ActivityType.PRIVATE) {
      Shop shop = shopService.findByUser(activity.getCreatorId());
      ActivityTicket ticket = new ActivityTicket();
      ticket.setActivityId(po.getId());
      ticket.setShopId(shop.getId());
      // ticket的有效时间从活动上继承过来
      ticket.setStartTime(po.getStartTime());
      ticket.setEndTime(po.getEndTime());
      // 审核状态的初始状态，自己的活动，自己的ticket默认是审核通过
      ticket.setAuditStatus(ActivityTicketAuditStatus.APPROVED);
      ticket.setStatus(ActivityStatus.NOT_STARTED);
      ticket.setCreatedAt(new Date());

      //ticket记录优惠信息
      ticket.setPreferentialType(form.getPreferentialType());
      ticket.setDiscount(form.getDiscount());
      ticket.setReason(form.getReason());
      ticket.setReduction(form.getReduction());
      activityService.saveActivityTicket(ticket);

      //全店铺参加活动-加入活动商品(入库 xquark_campaign_product)
      if (form.getPreferentialType().equals(PreferentialType.SHOP_DISCOUNT)) {
        activityService.addProductByShopDiscount(activity, form.getDiscount());
        productService.lockProductByShopId(shop.getId());
      }
    }

    ActivityVO vo = activityService.loadVO(po.getId());
    return new ResponseObject<>(vo);
  }

  /**
   * 卖家用户创建活动基本信息更新
   */
  @RequestMapping("/activity/update")
  @ResponseBody
  public ResponseObject<ActivityVO> update(@Valid @ModelAttribute ActivitySaveForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    if (form.getStartTime() != null) {
      if (form.getStartTime() <= new Date().getTime()) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动开始时间不能早于当前时间");
      }
    }
    if (form.getEndTime() - form.getStartTime() < 10 * 60 * 1000) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动持续时间不得小于10分钟");
    }
    if (form.getDiscount() != null && (form.getDiscount() <= 0 || form.getDiscount() > 1)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "折扣必须是大于0小于1的数值");
    }

    Activity activity = transForm2Activity(form);
    if (activity == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动Type类型异常");
    }

    activity = activityService.update(activity);

    if (activity.getType() == ActivityType.PRIVATE) {
      //Shop shop = shopService.findByUser(activity.getCreatorId());
      ActivityTicket oldTicket = activityService.getDefaultJoinActivityTicket(activity.getId());

      ActivityTicket newTicket = new ActivityTicket();
      newTicket.setId(oldTicket.getId());
      // ticket的有效时间从活动上继承过来
      newTicket.setStartTime(activity.getStartTime());
      newTicket.setEndTime(activity.getEndTime());
      // 审核状态的初始状态，自己的活动，自己的ticket默认是审核通过
      newTicket.setAuditStatus(ActivityTicketAuditStatus.APPROVED);
      newTicket.setStatus(ActivityStatus.NOT_STARTED);
      newTicket.setCreatedAt(new Date());

      //ticket记录优惠信息
      newTicket.setPreferentialType(form.getPreferentialType());
      newTicket.setDiscount(form.getDiscount());
      newTicket.setReason(form.getReason());
      activityService.saveActivityTicket(newTicket);
      // 这里可能造成报名表和活动商品表折扣信息不一致,更新报名表折扣里强制更新一下报名下的活动商品
      activityService.updateDiscountByTicket(newTicket.getId(), newTicket.getDiscount());
    }

    ActivityVO result = activityService.loadVO(activity.getId());
    return new ResponseObject<>(result);
  }

  /**
   * 活动中我的所有可供选择的商品
   */
  @RequestMapping("/activity/products/available")
  @ResponseBody
  public ResponseObject<List<CampaignProductVO>> availableProducts(@RequestParam String id,
      String channel, Pageable pager, String key) {
    User user = getCurrentUser();

    List<CampaignProduct> camProducts = activityService.listCampaignProducts(id);
    Map<String, CampaignProduct> prodMap = new HashMap<>();
    for (CampaignProduct cp : camProducts) {
      prodMap.put(cp.getProductId(), cp);
    }

    List<CampaignProductVO> result = new ArrayList<>();

    List<Product> products = productService
        .listProductsAvailableByChannel(id, user.getShopId(), pager, channel, key);

    // TODO 有问题，如果只是复制
    for (Product p : products) {
      if (p.getMarketPrice() == null) {
        p.setMarketPrice(p.getPrice());
      }

      CampaignProductVO cpv = new CampaignProductVO();
      CampaignProduct cp = prodMap.get(p.getId());
      if (cp != null) {
        BeanUtils.copyProperties(cp, cpv);
      }

      BeanUtils.copyProperties(p, cpv);

      cpv.setImgUrl(p.getImg());
      result.add(cpv);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 活动中我的商品
   */
  @RequestMapping("/activity/products/campaign")
  @ResponseBody
  public ResponseObject<List<CampaignProductVO>> campaignProducts(@RequestParam String id) {
//        List<CampaignProduct> cps = activityService.listCampaignProducts(id);
    User user = getCurrentUser();
    List<CampaignProductEX> cps = activityService.loadCampaignProductEX(id, user.getShopId());

    List<CampaignProductVO> result = new ArrayList<>();
    for (CampaignProductEX cp : cps) {
      CampaignProductVO cpv = new CampaignProductVO();
      BeanUtils.copyProperties(cp, cpv);

      Product p = productService.load(cp.getProductId());
      if (p == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品不存在");
      }
      if (p.getMarketPrice() == null) {
        p.setMarketPrice(p.getPrice());
      }
      BeanUtils.copyProperties(p, cpv);

      cpv.setImgUrl(p.getImg());
      result.add(cpv);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 客户端首页获取的所有的活动数据(包含公共数据和私有数据)
   */
  @RequestMapping("/activity/list")
  @ResponseBody
  public ResponseObject<List<ActivityVO>> list(Pageable pager) {
    User user = getCurrentUser();
    return new ResponseObject<>(
        activityService.listPublicAndPrivate(user.getId(), user.getShopId()));
  }

  /**
   * 平台活动
   * @param id
   * @param req
   * @param pageable
   * @return
   */
    /*@RequestMapping("/activity/pub-list")
    @ResponseBody
    public ResponseObject<List<ActivityVO>> pubList(Pageable pager) {
        return new ResponseObject<List<ActivityVO>>(activityService.listPublic());
    }*/

  /**
   * 我创建的活动
   */
    /*@RequestMapping("/activity/my-list")
    @ResponseBody
    public ResponseObject<List<ActivityVO>> mine(Pageable pager) {
        User user = getCurrentUser();
        return new ResponseObject<List<ActivityVO>>(activityService.listByUser(user.getId()));
    }*/

  /**
   * 活动详情
   */
  @RequestMapping("/activity/detail")
  @ResponseBody
  public ResponseObject<ActivityVO> view(@RequestParam String id) {
    ActivityVO activity = activityService.loadVO(id);
    return new ResponseObject<>(activity);
  }

  /**
   * 关闭活动
   */
    /*@RequestMapping("/activity/close")
    @ResponseBody
    public ResponseObject<Boolean> close(@RequestParam String id) {
        activityService.close(id);
        return new ResponseObject<Boolean>(Boolean.TRUE);
    }*/

  /**
   * 删除活动
   */
  @RequestMapping("/activity/delete")
  @ResponseBody
  public ResponseObject<Boolean> delete(@RequestParam String id) {
    int r = activityService.delete(id);
    if (r > 0) {
      //活动删除成功，处理活动商品数据
      activityService.optActivityProducts(id);
    }
    return new ResponseObject<>(r > 0);
  }

  /**
   * 活动中增加商品
   */
  @RequestMapping("/activity/products/add")
  @ResponseBody
  public ResponseObject<Boolean> addProducts(@ModelAttribute ActivityProductForm form) {
    User user = getCurrentUser();
    Shop shop = shopService.findByUser(user.getId());
    Activity activity = activityService.selectByPrimaryKey(form.getId());
    ActivityEX activityEX = activityService
        .loadActivityEx(form.getId(), shop.getId(), activity.getType());
    if (activityEX == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动不存在");
    }
    // 私有活动其实最好不要限制, 但现在三方活动是通过私有活动来配置的, 先加上限制
    if (ActivityStatus.IN_PROGRESS.equals(activity.getStatus()) && ActivityType.PRIVATE
        .equals(activity.getType())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已经开始，不能添加商品");
    }
    if (ActivityStatus.CLOSED.equals(activity.getStatus())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已经关闭，不能添加商品");
    }
    ActivityTicket ticket = activityService.getDefaultJoinActivityTicket(form.getId());
    if (ticket == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您还没有参加该活动，请从活动页面的报名按钮进入");
    }

    for (CampaignProduct prod : form.getProducts()) {
      if (activityEX.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
        prod.setDiscount(activityEX.getDiscount());
      }
      if (activityEX.getPreferentialType().equals(PreferentialType.PRODUCT_REDUCTION_PRICE)) {
        if (prod.getReduction() == null || prod.getReduction() <= 0)  // TODO or >= orig_price
        {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "部分商品减价不合适, 减价值需大于0并小于商品原价");
        }
      }
      if (activityEX.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
        if (prod.getDiscount() == null || prod.getDiscount() <= 0.0
            || prod.getDiscount() >= 1.0)  // TODO or >= orig_price
        {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "部分商品折扣不合适,折扣值需在1到10之间");
        }
      }
      if (activityEX.getPreferentialType().equals(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT)) {
        if (prod.getDiscount() == null || prod.getDiscount() <= 0.0 || prod.getDiscount() >= 1.0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "部分商品折扣值不合适");
        }
      }
      prod.setTicketId(ticket.getId());
      prod.setActivityId(form.getId());
    }

    // 当换商品时,之前参与的商品还是被锁住, 需要先解锁
    productService.unlockProductByShopIdEx(user.getShopId());
    for (CampaignProduct prod : form.getProducts()) {
      productService.lockProduct(prod.getProductId(), true);
    }

//        activityService.addProducts(form.getProducts());
    activityService.addProductToPrivateAct(ticket, user.getShopId(), form.getProducts());

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 活动中去掉商品
   */
  @RequestMapping("/activity/products/remove")
  @ResponseBody
  public ResponseObject<Boolean> removeProducts(@ModelAttribute ActivityProductRemoveForm form) {
    activityService.removeProducts(form.getId(), form.getProductIds());
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 参加平台促销活动
   * @param id
   * @return
   */
    /*@RequestMapping("/activity/request")
    @ResponseBody
    public ResponseObject<ActivityTicket> request(@RequestParam String id) {
        ActivityTicket ticket = activityService.requestTicket(id);
        return new ResponseObject<ActivityTicket>(ticket);
    }*/

  /**
   * 提交参加促销活动
   */
  @RequestMapping("/activity/join")
  @ResponseBody
  public ResponseObject<Boolean> join(ActivityJoinForm form) {
    User user = getCurrentUser();
    Activity activity = activityService.load(form.getId());
    if (!activity.getType().equals(ActivityType.PUBLIC_FOREVER)
        && activity.getStatus() == ActivityStatus.CLOSED) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已经结束");
    }
    if (activity.getType() == ActivityType.PRIVATE) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "个人活动不支持报名操作");
    }
    Date now = new Date(System.currentTimeMillis());
    if (activity.getApplyStartTime() != null && now.before(activity.getApplyStartTime())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动报名未开始");
    }
    if (!activity.getType().equals(ActivityType.PUBLIC_FOREVER)
        && activity.getApplyEndTime() != null && now.after(activity.getApplyEndTime())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动报名已结束");
    }
    if (form.getPreferentialType() == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动报名参数错误");
    }
    if (2 == form.getPreferentialType()) {  // 如果是折扣类型
      if (form.getDiscount() == null || form.getDiscount() <= .0 || form.getDiscount() >= 1.0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请设置正确的折扣值(0~10之间).");
      }
    }

    ActivityTicket ticket = activityService.getDefaultJoinActivityTicket(form.getId());

    ActivityTicket bean = new ActivityTicket();
    bean.setActivityId(form.getId());
    bean.setAuditStatus(ActivityTicketAuditStatus.SUBMITTED);
    bean.setCreatedAt(new Date());
    bean.setShopId(user.getShopId());
    bean.setStatus(ActivityStatus.NOT_STARTED);
    bean.setReason(form.getReason());
    bean.setPreferentialType(form.getPreferentialType());
    bean.setDiscount(form.getDiscount());
    if (ticket == null) {
      activityService.insertTicket(bean);
    } else {
      bean.setId(ticket.getId());
      activityService.updateTicket(bean);
    }
    return new ResponseObject<>(Boolean.TRUE);
  }


  /**
   * 推出活动
   */
  @RequestMapping("/activity/unjoin")
  @ResponseBody
  public ResponseObject<Boolean> unjoin(@RequestParam String id) {
    activityService.unjoin(id);
    return new ResponseObject<>(Boolean.TRUE);
  }

  private Activity transForm2Activity(ActivitySaveForm form) {
    Activity activity = new Activity();
    BeanUtils.copyProperties(form, activity);
    if (form.getStartTime() != null) {
      activity.setStartTime(new Date(form.getStartTime()));
    }
    if (form.getEndTime() != null) {
      activity.setEndTime(new Date(form.getEndTime()));
    }
    if (form.getApplyStartTime() != null) {
      activity.setApplyStartTime(new Date(form.getApplyStartTime()));
    }
    if (form.getApplyEndTime() != null) {
      activity.setEndTime(new Date(form.getApplyEndTime()));
    }
    if (form.getChannel() == null) {
      activity.setChannel(ActivityChannel.PRIVATE);
    }
    User user = getCurrentUser();
    if (form.getType() == null) {
      activity.setType(ActivityType.PRIVATE);
    }
    activity.setCreatorId(user.getId());
    activity.setStatus(ActivityStatus.NOT_STARTED);
    if (activity.getType() == ActivityType.PRIVATE) {
      if (activityService.existInRange(user.getId(), activity.getStartTime(), activity.getEndTime(),
          activity.getId())) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "多个活动的有效时间不能有重叠");
      }
      Shop shop = shopService.findByUser(user.getId());
      activity.setImg(shop.getImg());
      activity.setBanner(shop.getBanner());
    } else if (activity.getType() == ActivityType.PUBLIC) {
      activity.setImg(form.getImg());
      activity.setBanner(form.getBanner());
    } else {
      activity = null;
    }
    return activity;
  }


}
