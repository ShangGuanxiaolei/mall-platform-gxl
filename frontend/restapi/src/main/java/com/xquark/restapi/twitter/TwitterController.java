package com.xquark.restapi.twitter;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PartnerSetting;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.TwitterLevel;
import com.xquark.dal.model.TwitterLevelRelation;
import com.xquark.dal.model.TwitterProductCommission;
import com.xquark.dal.model.TwitterShopCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.model.UserTeam;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.PartnerStatus;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.vo.CommissionVO;
import com.xquark.dal.vo.OrderItemVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PartnerOrderCmVO;
import com.xquark.dal.vo.TeamVO;
import com.xquark.dal.vo.TwitterChildrenCmDisplayVO;
import com.xquark.dal.vo.TwitterInfoVO;
import com.xquark.dal.vo.TwitterProductCommissionVO;
import com.xquark.dal.vo.UserTwitterApplyVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.user.UserVO;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.apiVisitorLog.ApiVisitorLogService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderItemCommentService;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.team.TeamService;
import com.xquark.service.team.UserTeamService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.TwitterLevelRelationService;
import com.xquark.service.twitter.TwitterLevelService;
import com.xquark.service.twitter.TwitterProductComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.twitter.impl.TwitterShopComServiceImpl;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.service.wechat.WechatService;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.UserCardService;
import com.xquark.utils.EmojiFilter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class TwitterController extends BaseController {

  @Autowired
  private OrderItemCommentService orderItemCommentService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ApiVisitorLogService apiVisitorLogService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @Autowired
  private WechatService wechatService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private TinyUrlService tinyUrlService;

  @Autowired
  private SystemRegionService systemRegionService;

  @Autowired
  UrlHelper urlHelper;

  @Autowired
  private UserCardService userCardService;

  @Autowired
  private FamilyCardSettingService familyCardSettingService;

  @Autowired
  private UserPartnerService userPartnerService;

  @Autowired
  private PartnerSettingService partnerSettingService;

  @Autowired
  private TwitterLevelService twitterLevelService;

  @Autowired
  private TwitterShopComServiceImpl twitterShopComServiceImpl;

  @Autowired
  private TwitterProductComService twitterProductComService;

  @Autowired
  private TwitterLevelRelationService twitterLevelRelationService;

  @Autowired
  private TeamService teamService;

  @Autowired
  private UserTeamService userTeamService;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private ProductService productService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  /**
   * 获取团队人员列表
   */
  @ResponseBody
  @RequestMapping("/twitterCenter/teamMember")
  public ResponseObject<Map<String, Object>> listTwitterMember(Pageable pageable) {
    String currentShopId = getCurrentIUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(currentShopId).getRootShopId();
    Map<String, Object> aRetMap = new HashMap<>();
    List<UserVO> result = new ArrayList<>();
    // 团队成员获取该用户的所有直接下级
    long count = shopTreeService.countChildren(rootShopId, currentShopId);
    if (StringUtils.isNotBlank(rootShopId) && StringUtils.isNotBlank(currentShopId)) {
      List<ShopTree> listShopTree = shopTreeService
          .listChildrenPage(rootShopId, currentShopId, pageable);
      if (listShopTree != null) {
        for (ShopTree shopTree : listShopTree) {
          Shop shop = shopService.load(shopTree.getDescendantShopId());
          String shopId = shop.getId();
          String userId = shop.getOwnerId();
          User user = (User) userService.load(userId);
          if (user == null) {
            continue;
          }
          UserVO userVO = new UserVO(user, urlHelper.genShopUrl(shopId));
          // 该用户团队成员数
          userVO.setTeamNum(shopTreeService.countChildren(rootShopId, shopId));
          // 该用户团队贡献数
          userVO.setAmount(orderService.sumBySellerShopId(shopId));
          result.add(userVO);
        }
      }
      aRetMap.put("total", count);
      aRetMap.put("list", result);
    }
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 我的小店会员查询
   */
  @ResponseBody
  @RequestMapping("/twitterCenter/getMyMembers")
  public ResponseObject<List<User>> getMyMembers(Pageable pageable) {
    String currentShopId = getCurrentIUser().getShopId();
    List<User> result = userTwitterService.getMyMembers(currentShopId, pageable);
    return new ResponseObject<>(result);
  }


  /**
   * 卖家获取订单信息 app:2c收益列表
   */
  @ResponseBody
  @RequestMapping("/twitterCenter/order/list")
  public ResponseObject<List<OrderVO>> listTwitterCenterOrder(@RequestParam("status") String status,
      Pageable pageable) {

    String currentShopId = getCurrentUser().getShopId();
    String userId = shopService.load(currentShopId).getOwnerId();
    Map<String, Object> params = new HashMap<>();
    List<OrderVO> result = new ArrayList<>();
    String commissionStatus = "";
    if ("success".equals(status)) {
      commissionStatus = "SUCCESS";
    } else if ("process".equals(status)) {
      commissionStatus = "NEW";
    } else {
      commissionStatus = null;
    }
    params.put("userPhone", StringUtils.defaultIfBlank(userService.load(userId).getPhone(), null));
    params.put("status", StringUtils.defaultIfBlank(commissionStatus, null));
    List<CommissionVO> listCommissionVO = commissionService
        .listOrderNoByAdmin(params, pageable);//查询每个收益的orderNo
    if (listCommissionVO != null) {
      for (CommissionVO commissionVO : listCommissionVO) {
        BigDecimal sumCommission = new BigDecimal(0);
        OrderVO order = orderService.loadByOrderNo(commissionVO.getOrderNo());
        List<OrderItemVO> orderItemVOs = orderService
            .selectVOByOrderIdAndUserId(order.getId(), userId);
        order.setOrderItemVOs(orderItemVOs);
        for (OrderItemVO orderItemVO : orderItemVOs) {
          sumCommission = sumCommission.add(orderItemVO.getCommission());
        }
        order.setCommissionFee(sumCommission);
        result.add(order);
      }

      if (result != null) {
        for (OrderVO order : result) {
          String imgUrl = "";

          for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);

          }
          order.setImgUrl(imgUrl);

        }
      }
    }
    return new ResponseObject<>(result);
  }

  /**
   * app:2b收益列表
   */
  @ResponseBody
  @RequestMapping("/twitterCenter/order/list/2b")
  public ResponseObject<List<OrderVO>> listTwitterCenterOrderTob(
      @RequestParam("showAll") boolean showAll, Pageable pageable) {

    String currentShopId = getCurrentUser().getShopId();
    String userId = shopService.load(currentShopId).getOwnerId();
    Map<String, Object> params = new HashMap<>();
    List<OrderVO> result = new ArrayList<>();
    params.put("showAll", showAll);
    params.put("userId", userId);
    //params.put("userPhone", StringUtils.defaultIfBlank(userService.load(userId).getPhone(), null));
    List<CommissionVO> listCommissionVO = commissionService
        .listSuccessOrderNo(params, pageable);//查询当月收益的orderNo
    if (listCommissionVO != null) {
      for (CommissionVO commissionVO : listCommissionVO) {
        OrderVO order = orderService.loadByOrderNo(commissionVO.getOrderNo());
        List<OrderItemVO> orderItemVOs = orderService
            .selectVOByOrderIdAndUserId(order.getId(), userId);
        order.setOrderItemVOs(orderItemVOs);
        order.setCommissionFee(commissionVO.getFee());
        order.setCreatedAt(commissionVO.getUpdatedAt());
        result.add(order);
      }

      if (result != null) {
        for (OrderVO order : result) {
          String imgUrl = "";

          for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
          }
          order.setImgUrl(imgUrl);

        }
      }
    }
    return new ResponseObject<>(result);
  }

  /**
   * 卖家获取订单信息
   */
  @ResponseBody
  @RequestMapping("/twitter/order/list")
  public ResponseObject<Map<String, Object>> listTwitterOrder(@RequestParam("status") String status,
      @RequestParam("orderNo") String orderNo,
      @RequestParam("sellerPhone") String sellerPhone, @RequestParam("startDate") String startDate,
      @RequestParam("endDate") String endDate,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    params.put("rootShopId", getCurrentIUser().getShopId());
    if (!orderNo.equals("")) {
      params.put("orderNo", "%" + orderNo + "%");
    }
    if (!sellerPhone.equals("")) {
      params.put("sellerPhone", "%" + sellerPhone + "%");
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (!status.equals("") && !status.equals("ALL")) {
      List<String> statusList = new ArrayList<>();
      statusList.add(status);
      params.put("status", statusList);
    }

    List<OrderVO> result = orderService.listByMerchant(params, pageable);
    for (OrderVO order : result) {
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      if (order.getOrderAddress() != null) {
        List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
        String addressDetails = "";
        for (Zone zone : zoneList) {
          addressDetails += zone.getName();
        }

        addressDetails += order.getOrderAddress().getStreet();
        order.setAddressDetails(addressDetails);
      }
      String sellerName = userService.load(order.getSellerId()).getName();
      String buyerName = userService.load(order.getBuyerId()).getName();
      order.setSellerName(sellerName);
      order.setBuyerName(buyerName);
      List<Commission> list = commissionService.listByOrderId(order.getId());
      BigDecimal sumCommission = new BigDecimal(0);
      for (Commission commission : list) {
        sumCommission = sumCommission.add(commission.getFee());
      }
      order.setCommissionFee(sumCommission);
    }
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("orderTotal", orderService.countByMerchant(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 查看订单<br>
   */
  @ResponseBody
  @RequestMapping("/twitter/order/orderItem")
  public ResponseObject<Map<String, Object>> view(@RequestParam("orderNo") String orderNo,
      Pageable pageable) {
    Map<String, Object> aRetMap = new HashMap<>();
    if (StringUtils.isEmpty(orderNo)) {
      return new ResponseObject<>(aRetMap);
    }
    OrderVO orderVO = orderService.loadByOrderNo(orderNo);

    String addressDetails = "";
    // 收货地址
    if (orderVO.getOrderAddress() != null) {
      String zoneId = orderVO.getOrderAddress().getZoneId();
      if (StringUtils.isNotBlank(zoneId)) {
        addressDetails = systemRegionService
            .buildAddressDetail(zoneId, orderVO.getOrderAddress().getStreet());
      } else {
        addressDetails = orderVO.getOrderAddress().getStreet();
      }
    }

    List<OrderItem> orderItems = orderVO.getOrderItems();
    for (OrderItem orderItem : orderItems) {
      orderItem.setAddressDetails(addressDetails);
      orderItem.setShippedAt(orderVO.getShippedAt());
      orderItem.setSucceedAt(orderVO.getSucceedAt());
      orderItem.setCreatedAt(orderVO.getCreatedAt());

      orderItem.setLogisticsCompany(orderVO.getLogisticsCompany());
      orderItem.setLogisticsOrderNo(orderVO.getLogisticsOrderNo());
      Product product = productService.load(orderItem.getProductId());
      orderItem.setProduct(product);
    }

    aRetMap.put("itemTotal", orderItems.size());
    aRetMap.put("list", orderItems);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 查看换货订单--modify 2018-10-26 00:07 by gxl
   * @param orderId
   * @param pageable
   * @return
   */
  @ResponseBody
  @RequestMapping("/twitter/order/changeOrderItem")
  public ResponseObject<Map<String, Object>> viewChangeGoods(@RequestParam("orderId") String orderId,
      Pageable pageable) {
    Map<String, Object> aRetMap = new HashMap<>();
    if (StringUtils.isEmpty(orderId)) {
      return new ResponseObject<>(aRetMap);
    }
    List<OrderItem> orderItemList = orderItemMapper.selectByOrderIdAndPT(orderId);
    for (OrderItem orderItem : orderItemList) {
      Product product = productService.load(orderItem.getProductId());
      orderItem.setProduct(product);
    }
    aRetMap.put("itemTotal", orderItemList.size());
    aRetMap.put("list", orderItemList);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 换货时，展示换货商品信息
   * @param orderNo
   * @param pageable
   * @return
   *//*
  @ResponseBody
  @RequestMapping("/twitter/order/selectGoods")
  public ResponseObject<Map<String, Object>> viewGoods(@RequestParam("orderNo") String orderNo, Pageable pageable) {
    Map<String, Object> map = new HashMap<>();
    if (StringUtils.isEmpty(orderNo)) {
      return new ResponseObject<>(map);
    }
    OrderVO order = orderService.loadByOrderNo(orderNo);

    List<OrderItem> orderItems = order.getOrderItems();
    OrderItem orderItem = orderItems.get(0);
    Product product = null;
    if (orderItem != null) {
      String productId = orderItem.getProductId();
      product = productService.load(productId);//查询出商品信息
    } else {
      throw new BizException(GlobalErrorCode.REPEAT_SUBMIT, "未获取到商品");
    }
    map.put("itemTotal", orderItems.size());
    map.put("productList", product);
    return new ResponseObject<>(map);
  }*/

  /**
   * 卖家订单分佣
   */
  @ResponseBody
  @RequestMapping("/twitter/order/commission")
  public ResponseObject<Map<String, Object>> listCommission(@RequestParam("orderNo") String orderNo,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    params.put("orderNo", orderNo);
    List<CommissionVO> result = commissionService.listCommissionsByAdmin(params, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("commissionTotal", commissionService.countCommissionsByAdmin(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 推客会员列表信息
   */
  @ResponseBody
  @RequestMapping("/twitterMember/list")
  public ResponseObject<Map<String, Object>> listTwitterMember(
      @RequestParam(value = "status", required = false) String status,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "keys", required = false) String keys,
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (StringUtils.isNotEmpty(status) && !"ALL".equals(status)) {
      params.put("status", status);
    }

    String shopId = getCurrentIUser().getShopId();
    List<TwitterChildrenCmDisplayVO> result = userTwitterService
        .selectChildrenCmByRootId(shopId, params, pageable);
    Long total = userTwitterService.countChildrenCmByRootId(shopId, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 推客等级列表信息
   */
  @ResponseBody
  @RequestMapping("/twitterMember/levelList")
  public ResponseObject<Map<String, Object>> levelList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<TwitterLevel> result = twitterLevelService.list(pageable, params);
    Long total = twitterLevelService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 保存推客等级
   */
  @ResponseBody
  @RequestMapping("/twitterMember/saveLevel")
  public ResponseObject<Boolean> saveLevel(TwitterLevel level) {
    int result = 0;
    if (StringUtils.isNotEmpty(level.getId())) {
      result = twitterLevelService.updateByPrimaryKey(level);
    } else {
      String shopId = getCurrentIUser().getShopId();
      level.setOwnShopId(IdTypeHandler.decode(shopId));
      level.setArchive(false);
      result = twitterLevelService.insert(level);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存推客设置
   */
  @ResponseBody
  @RequestMapping("/twitterMember/saveCommissionSetting")
  public ResponseObject<Boolean> saveCommissionSetting(TwitterShopCommission commission) {
    int result = 0;
    if (StringUtils.isNotEmpty(commission.getId())) {
      result = twitterShopComServiceImpl.updateDefaultByShopId(commission);
    } else {
      String shopId = getCurrentIUser().getShopId();
      commission.setOwnShopId(IdTypeHandler.decode(shopId));
      commission.setArchive(false);
      result = twitterShopComServiceImpl.createDefault(commission);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/twitterMember/deleteLevel/{id}")
  public ResponseObject<Boolean> deleteLevel(@PathVariable String id) {
    int result = twitterLevelService.deleteByPrimaryKey(id);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/twitterMember/getLevel/{id}")
  public ResponseObject<TwitterLevel> getLevel(@PathVariable String id) {
    TwitterLevel result = twitterLevelService.selectByPrimaryKey(id);
    return new ResponseObject<>(result);
  }

  /**
   * 获取推客设置中默认的三级分佣设置
   */
  @ResponseBody
  @RequestMapping("/twitterMember/getDefaultCommission")
  public ResponseObject<TwitterShopCommission> getDefaultCommission() {
    String rootShopId = getCurrentIUser().getShopId();
    TwitterShopCommission result = twitterShopComServiceImpl.selectDefaultByshopId(rootShopId);
    return new ResponseObject<>(result);
  }

  /**
   * 推客商品佣金列表
   */
  @ResponseBody
  @RequestMapping("/twitterMember/productList")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable,
      @RequestParam(value = "productName", required = false) String productName
      , @RequestParam(value = "category", required = false) String category) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    if (StringUtils.isNotEmpty(productName)) {
      params.put("productName", "%" + productName + "%");
    }
    if (StringUtils.isNotEmpty(category)) {
      params.put("category", category);
    }

    params.put("shopId", shopId);
    List<TwitterProductCommission> result = twitterProductComService.list(pageable, params);
    Long total = twitterProductComService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 查询是否已经有该商品已经设置过分佣设置
   */
  @ResponseBody
  @RequestMapping("/twitterMember/checkProductCommission")
  public ResponseObject<Boolean> checkProductCommission(
      @RequestParam("productId") String productId) {
    String shopId = getCurrentIUser().getShopId();
    long count = twitterProductComService.selectByProductId(shopId, productId);
    if (count > 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }


  /**
   * 保存推客商品佣金设置
   */
  @ResponseBody
  @RequestMapping("/twitterMember/saveProductCommission")
  public ResponseObject<Boolean> saveProductCommission(TwitterProductCommission commission) {
    String shopId = getCurrentIUser().getShopId();
    int result = 0;
    if (StringUtils.isNotEmpty(commission.getId())) {
      result = twitterProductComService.update(commission);
    } else {

      commission.setShopId(IdTypeHandler.decode(shopId));
      commission.setArchive(false);
      result = twitterProductComService.insert(commission);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存推客等级
   */
  @ResponseBody
  @RequestMapping("/twitterMember/saveLevelRelation")
  public ResponseObject<Boolean> saveLevelRelation(@RequestParam("userId") String userId,
      @RequestParam("levelId") String levelId, @RequestParam("validAt") String validAt) {
    String shopId = getCurrentIUser().getShopId();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    TwitterLevelRelation relation = new TwitterLevelRelation();
    relation.setArchive(false);
    relation.setUserId(userId);
    relation.setLevelId(levelId);
    relation.setShopId(shopId);
    try {
      relation.setValidAt(sdf.parse(validAt));
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "日期转换出错");
    }

    // 保存推客等级前先删除用户之前所有的推客等级信息
    twitterLevelRelationService.deleteByUserId(shopId, userId);
    int result = twitterLevelRelationService.insert(relation);
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 删除商品佣金
   */
  @ResponseBody
  @RequestMapping("/twitterMember/deleteProductCommission/{id}")
  public ResponseObject<Boolean> deleteProductCommission(@PathVariable String id) {
    int result = twitterProductComService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取商品佣金
   */
  @ResponseBody
  @RequestMapping("/twitterMember/getProductCommission/{id}")
  public ResponseObject<TwitterProductCommissionVO> getProductCommission(@PathVariable String id) {
    TwitterProductCommissionVO result = twitterProductComService.load(id);
    return new ResponseObject<>(result);
  }

  /**
   * 提升为合伙人
   */
  @ResponseBody
  @RequestMapping("/twitterMember/setPartner/{id}")
  public ResponseObject<Boolean> setPartner(@PathVariable String id) {
    int result = 0;
    String rootShopId = getCurrentIUser().getShopId();
    //TODO liangfan
    UserPartner userPartner = new UserPartner();
    userPartner.setOwnerShopId(rootShopId);
    userPartner.setUserId(id);
    // 默认合伙人状态为申请中，待月初由触发器统一修改为已生效 modify by chh 2016-08-11
    // 设置为合伙人后即时生效
    userPartner.setStatus(PartnerStatus.ACTIVE);
    result = userPartnerService.create(userPartner);
    return new ResponseObject<>(result > 0);

  }

  /**
   * 禁用shop
   */
  @ResponseBody
  @RequestMapping("/twitterMember/enableShop/{id}")
  public ResponseObject<Boolean> enableShop(@PathVariable String id) {
    UserTwitter twitter = new UserTwitter();
    twitter.setId(id);
    twitter.setStatus(TwitterStatus.ACTIVE);
    int result = userTwitterService.update(twitter);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 禁用shop
   */
  @ResponseBody
  @RequestMapping("/twitterMember/disableShop/{id}")
  public ResponseObject<Boolean> disableShop(@PathVariable String id) {
    UserTwitter twitter = new UserTwitter();
    twitter.setId(id);
    twitter.setStatus(TwitterStatus.FROZEN);
    int result = userTwitterService.update(twitter);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 推客申请列表查询
   */
  @ResponseBody
  @RequestMapping("/twitter/apply/list")
  public ResponseObject<Map<String, Object>> listTwitterApply(Pageable pageable,
      HttpServletRequest request) {
    Map<String, Object> params = new HashMap<>();
    String status = request.getParameter("status");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String phone = request.getParameter("phone");
    String shopName = request.getParameter("shopName");
    String name = request.getParameter("name");

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }

    params.put("status", status);
    params.put("startDate", startDate);
    params.put("endDate", endDate);
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(shopName)) {
      params.put("shopName", "%" + shopName + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    List<UserTwitterApplyVO> result = userTwitterService
        .selectUserTwitterApplyingByShopId(getCurrentIUser().getShopId(), params, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("applyTotal",
        userTwitterService.countUserTwitterApplyingByShopId(getCurrentIUser().getShopId(), params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 推客审核通过
   */
  @ResponseBody
  @RequestMapping("/twitter/apply/{id}")
  public ResponseObject<Shop> twitterApply(@PathVariable String id) {
    UserTwitter twitterApply = userTwitterService.load(id);
    Shop twitter = shopService.createDistributeShop(twitterApply);
    UserTwitter updateUserTwitter = new UserTwitter();
    updateUserTwitter.setId(id);
    updateUserTwitter.setStatus(TwitterStatus.ACTIVE);
    userTwitterService.update(updateUserTwitter);

    // 如果上级推客在某个战队中，则该推客默认也属于该战队
    String parentUserId = twitterApply.getParentUserId();
    TeamVO team = teamService.selectByUserId(parentUserId);
    if (team != null) {
      UserTeam userTeam = new UserTeam();
      userTeam.setShopId(shopTreeService.selectRootShopByShopId(twitter.getId()).getRootShopId());
      userTeam.setUserId(twitterApply.getUserId());
      userTeam.setArchive(false);
      userTeam.setStatus(ShopStatus.ACTIVE);
      userTeam.setTeamId(team.getId());
      userTeamService.insert(userTeam);
    }

    return new ResponseObject<>(twitter);
  }

  /**
   * 卖家获取订单信息
   */
  @ResponseBody
  @RequestMapping("/twitterMember/children")
  public ResponseObject<Map<String, Object>> listTwitterChildren(
      @RequestParam("userId") String userId, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = shopService.findByUser(userId).getId();
    String rootShopId = getCurrentIUser().getShopId();
    List<ShopTree> result = shopTreeService
        .listDirectChildren4Admin(rootShopId, shopId, null, pageable);
    ShopTree parentShopTree = shopTreeService.selectDirectParent(rootShopId, shopId);
    String parentShopName = shopService.load(parentShopTree.getAncestorShopId()).getName();
    List<Shop> shopList = new ArrayList<>();
    Long total = shopTreeService.countDirectChildren(rootShopId, shopId);
    for (ShopTree shopTree : result) {
      String userShopId = shopTree.getDescendantShopId();
      shopList.add(shopService.load(userShopId));
    }
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", shopList);
    aRetMap.put("parentShopName", parentShopName);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 推客会员
   */
  @ResponseBody
  @RequestMapping("/twitterMember/userslist")
  public ResponseObject<Map<String, Object>> listUserTwitter(@RequestParam("status") String status,
      @RequestParam(required = false) String name, @RequestParam(required = false) String phone,
      Pageable pageable) {

    Map<String, Object> params = new HashMap<>();
//		if(!phone.equals("")) {
//			params.put("phone", phone);
//		}
//		if(!shopName.equals("")) {
//			params.put("shopName", shopName);
//		}
//
//		if(!startDate.equals("")) {
//			params.put("startDate", startDate);
//		}
//
//		if(!endDate.equals("")) {
//			params.put("endDate", endDate);
//		}
//
//		if(!status.equals("") && !status.equals("ALL")){
//			List<String> statusList = new ArrayList<String>();
//			statusList.add(status);
//			params.put("status", statusList);
//		}
    if (name != null && name.equals("")) {
      name = null;
    }

    if (phone != null && phone.equals("")) {
      phone = null;
    }

    String shopId = getCurrentIUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<User> result = userService.listUsersByRootShopId(rootShopId, name, phone, pageable);
    Long total = userService.countUsersByRootShopId(rootShopId, name, phone);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 推客会员
   */
  @ResponseBody
  @RequestMapping("/twitterMember/changeParent")
  public ResponseObject<Boolean> listUserTwitter(@RequestParam("parentUserId") String parentUserId,
      @RequestParam("userId") String userId) {
    boolean result = false;
    if (!userId.equals(parentUserId)) {
      String newParentShopId = shopService.findByUser(parentUserId).getId();
      String shopId = shopService.findByUser(userId).getId();
      String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
      result = shopTreeService.changeParent(rootShopId, newParentShopId, shopId);

    }
    return new ResponseObject<>(result);

  }

  /**
   * twitter概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/twitter/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取推客概况中的今日数据,本周订单,推客,交易额,佣金数
    summary = userTwitterService.getSummary(shopId);

    return new ResponseObject<>(summary);
  }

  @ResponseBody
  @RequestMapping("/twitter/getDirectChildren")
  public ResponseObject<HashMap> getDirectChildren(@RequestParam(required = false) String userId,
      Pageable pageable, HttpServletRequest request) {
    HashMap map = new HashMap();
    //用户名，角色，状态，分成比例，当月佣金
    List<TwitterInfoVO> listTwitter = new ArrayList<>();
    User user = new User();
    if (userId == null || userId.equals("")) {
      userId = this.getCurrentIUser().getId();
    }
    String shopId = userService.load(userId).getShopId();
    if (StringUtils.isEmpty(shopId)) {
      shopId = this.getCurrentIUser().getShopId();
    }
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<ShopTree> listTree = shopTreeService.listDirectChildrenPage(rootShopId, shopId, pageable);
    Long total = shopTreeService.countDirectChildren(rootShopId, shopId);
    for (ShopTree shopTree : listTree) {
      String desShopId = shopTree.getDescendantShopId();
      // 查找这个下级用户的相关信息
      TwitterInfoVO twitterInfoVO = this.getTwitterInfoVO(desShopId, shopTree.getRootShopId());
      listTwitter.add(twitterInfoVO);
    }
    map.put("total", total);
    map.put("list", listTwitter);
    // 查找传过来用户的相关信息
    TwitterInfoVO twitterInfoVO = this.getTwitterInfoVO(shopId, rootShopId);
    map.put("parent", twitterInfoVO);
    return new ResponseObject(map);
  }

  /**
   * 查找用户的相关twitter树结构信息
   */
  private TwitterInfoVO getTwitterInfoVO(String shopId, String rootShopId) {
    TwitterInfoVO twitterInfoVO = new TwitterInfoVO();

    String desUserId = shopService.load(shopId).getOwnerId();
    User desUser = userService.load(desUserId);
    // 这个下级是否还有下级，以及还有多少个下级人员
    long count = shopTreeService.countDirectChildren(rootShopId, shopId);
    twitterInfoVO.setCount(count);
    if (count > 0) {
      twitterInfoVO.setIsLeaf(false);
    } else {
      twitterInfoVO.setIsLeaf(true);
    }

    twitterInfoVO.setId(desUserId);
    twitterInfoVO.setName(desUser.getName());
    if (desUser.getAvatar() != null) {
      twitterInfoVO.setAvatar(desUser.getAvatar());
    }
    UserTwitter userTwitter = userTwitterService.selectByUserIdAndShopId(desUserId, rootShopId);
    if (userTwitter != null) {
      twitterInfoVO.setRole("推客");
      twitterInfoVO.setStatus(userTwitter.getStatus().toString());
      //查找比例
      UserCard userCard = userCardService.selectUserCardsByUserIdAndShopId(desUserId, rootShopId);
      //新增推客默认都是大B下面的银卡用户。默认一个大B只有一个银卡配置，推客的金银卡通过job每个月动态变更，
      if (userCard != null) {
        String cardId = userCard.getCardId();
        FamilyCardSetting setting = familyCardSettingService.load(cardId);
        if (setting != null) {
          twitterInfoVO.setRate(new BigDecimal(setting.getPersonalCmRate()));
        }
      } else {
        //卖家找不到对应卡信息 默认为大b下的银卡用户
        FamilyCardSetting silverCardSetting = familyCardSettingService
            .loadSilverCardSetting(rootShopId);
        twitterInfoVO.setRate(new BigDecimal(silverCardSetting.getPersonalCmRate()));
      }
      twitterInfoVO.setIncome(commissionService.getFeeByMonth(desUserId));
    }
    UserPartner userPartner = userPartnerService
        .selectUserPartnersByUserIdAndShopId(desUserId, rootShopId);
    if (userPartner != null) {
      twitterInfoVO.setRole("合伙人");
      twitterInfoVO.setStatus(userPartner.getStatus().toString());
      //查找比例
      PartnerSetting partnerSetting = partnerSettingService.loadByShopId(rootShopId);
      if (partnerSetting != null) {
        twitterInfoVO.setRate(new BigDecimal(partnerSetting.getCmRate()));
      } else {
        twitterInfoVO.setRate(new BigDecimal("20"));
      }

      twitterInfoVO.setIncome(commissionService.getFeeByMonth(desUserId));
    }
    // 设置默认值
    if (twitterInfoVO.getRole() == null) {
      twitterInfoVO.setRole("推客");
    }
    if (twitterInfoVO.getStatus() == null) {
      twitterInfoVO.setStatus(TwitterStatus.ACTIVE.toString());
    }
    if (twitterInfoVO.getRate() == null) {
      twitterInfoVO.setRate(new BigDecimal("10"));
    }
    if (twitterInfoVO.getIncome() == null) {
      twitterInfoVO.setIncome(new BigDecimal(0.00));
    }
    return twitterInfoVO;
  }


  /**
   * 推客结算记录列表
   */
  @ResponseBody
  @RequestMapping("/twitter/listRecord")
  public ResponseObject<Map<String, Object>> listRecord(
      @RequestParam(value = "status", required = false) String status,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "orderNo", required = false) String orderNo,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", name);
    }
    if (StringUtils.isNotEmpty(orderNo)) {
      params.put("orderNo", orderNo);
    }

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }
    if (StringUtils.isNotEmpty(startDate)) {
      params.put("startDate", startDate);
    }
    if (StringUtils.isNotEmpty(endDate)) {
      params.put("endDate", endDate);
    }
    if (StringUtils.isNotEmpty(status)) {
      params.put("status", status);
    }

    String shopId = getCurrentIUser().getShopId();
    List<PartnerOrderCmVO> result = userTwitterService
        .listCommissionByShopId(shopId, params, pageable);
    Long total = userTwitterService.countCommissionByShopId(shopId, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  @ResponseBody
  @RequestMapping(value = "/twitter/applying")
  public ResponseObject<Boolean> apply(
      @RequestParam(value = "shopId", required = false) String shopId,
      @RequestParam("phone") String phone,
      @RequestParam(value = "realName", required = false) String realName, HttpServletRequest req) {
    //FIXME duplicate phone in the same root shop by liangfan

    String nickname = EmojiFilter.filterEmoji(realName);
    // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
    if (nickname == null || "".equals(nickname)) {
      nickname = "普通用户";
    }

    // 更新用户对应的手机号和名称
    User user = getCurrentUser();
    User updateUser = new User();
    updateUser.setId(user.getId());
    updateUser.setPhone(phone);
    updateUser.setName(nickname);
    userService.updateUserInfo(updateUser);

    // 更新用户店铺对应的名称
    String currentShopId = user.getShopId();
    if (StringUtils.isNotEmpty(currentShopId)) {
      Shop currentShop = shopService.load(currentShopId);
      if (currentShop != null) {
        Shop updateShop = new Shop();
        updateShop.setId(currentShopId);
        updateShop.setName(nickname);
        shopMapper.updateByPrimaryKeySelective(updateShop);
      }
    }

    // 如果没有传递上级shopid过来，则默认上级是总店
    if (StringUtils.isEmpty(shopId)) {
      shopId = shopTreeService.getRootShop().getAncestorShopId();
    }

    // 新增推客申请信息
    UserTwitter userTwitter = new UserTwitter();
    userTwitter.setStatus(TwitterStatus.APPLYING);
    userTwitter.setUserId(user.getId());
    userTwitter.setOwnerShopId(shopTreeService.selectRootShopByShopId(shopId).getRootShopId());
    userTwitter.setParentUserId(shopService.load(shopId).getOwnerId());
    userTwitterService.create(userTwitter);

    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping(value = "/twitter/isFirstTwitterCenter")
  public ResponseObject<Boolean> apply(HttpServletRequest req) {
    String userId = getCurrentIUser().getId();
    boolean flag = userTwitterService.isFirstTwitter(userId);
    return new ResponseObject<>(flag);
  }


}