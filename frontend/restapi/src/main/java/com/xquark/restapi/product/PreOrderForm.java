package com.xquark.restapi.product;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionPreOrder;
import com.xquark.helper.Transformer;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by wangxinhua on 17-11-22. DESC:
 */
@ApiModel("预购表单")
public class PreOrderForm {

  private String id;
  private String promotionId;

  @NotBlank(message = "活动标题不能为空")
  @ApiModelProperty(value = "title", required = true)
  private String title;

  @NotBlank(message = "活动图片不能为空")
  @ApiModelProperty(value = "img", required = true)
  private String img;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "validFrom", required = true)
  @NotNull(message = "活动开始日期不能为空")
  private Date validFrom;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "canPayTime", required = true)
  @NotNull(message = "预购商品可以正式购买的时间")
  private Date canPayTime;

  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
  @ApiModelProperty(value = "validTo", required = true)
  @NotNull(message = "活动结束日期不能为空")
  private Date validTo;

  @NotBlank(message = "商品id不能为空")
  @ApiModelProperty(value = "productId", required = true)
  private String productId;

  @NotNull(message = "预购定金不能为空")
  @ApiModelProperty(value = "preOrderPrice", required = true)
  private BigDecimal preOrderPrice;

  @NotNull(message = "预购价格不能为空")
  @ApiModelProperty(value = "discount", required = true)
  private BigDecimal discount;

  @NotNull(message = "免定金所需积分不能为空")
  @ApiModelProperty(value = "freePoints", required = true)
  private Long freePoints;

  @NotNull(message = "预订库存不能为空")
  @ApiModelProperty(value = "amount", required = true)
  private Integer amount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getPreOrderPrice() {
    return preOrderPrice;
  }

  public void setPreOrderPrice(BigDecimal preOrderPrice) {
    this.preOrderPrice = preOrderPrice;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public Long getFreePoints() {
    return freePoints;
  }

  public void setFreePoints(Long freePoints) {
    this.freePoints = freePoints;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Date getCanPayTime() {
    return canPayTime;
  }

  public void setCanPayTime(Date canPayTime) {
    this.canPayTime = canPayTime;
  }

  public Promotion getPromotion() {
    Promotion promotion = new Promotion();
    promotion.setId(promotionId);
    promotion.setDiscount(discount);
    promotion.setValidFrom(validFrom);
    promotion.setValidTo(validTo);
    promotion.setTitle(title);
    promotion.setImg(img);
    return promotion;
  }

  public PromotionPreOrder getPromotinProduct() {
    return Transformer.fromBean(this, PromotionPreOrder.class);
  }
}
