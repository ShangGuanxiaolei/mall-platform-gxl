package com.xquark.restapi.order;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.service.point.type.FunctionCodeType;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.vdlm.common.lang.CollectionUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.*;
import com.xquark.dal.type.*;
import com.xquark.dal.validation.group.bill.ElectronicGroup;
import com.xquark.dal.validation.group.bill.NormalGroup;
import com.xquark.dal.vo.*;
import com.xquark.dal.voex.DiscountDetailVO;
import com.xquark.dal.voex.OrderVOEx;
import com.xquark.dal.voex.PieceGroupVO;
import com.xquark.interceptor.Token;
import com.xquark.openapi.product.form.OrderSumbitFormApi;
import com.xquark.openapi.product.form.OrderSumbitFormApi.BillFormType;
import com.xquark.openapi.vo.OrderVOApi;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.address.AddressForm;
import com.xquark.restapi.utils.ValidationUtils;
import com.xquark.restapi.vo.Json;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.apiVisitorLog.ApiVisitorLogService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.bill.BillService;
import com.xquark.service.cantuan.PieceCacheService;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.*;
import com.xquark.service.freshman.pojo.FreshManToast;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.lottery.FreshmanLotteryService;
import com.xquark.service.order.*;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.ThirdPaymentQueryRes;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.privilege.PrivilegeCodeService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.VipProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.*;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.promotion.impl.PromotionSkusSerciceImpl;
import com.xquark.service.reserve.PromotionReserveService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.track.FreshmanTrackService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.UserService;
import com.xquark.service.vipRight.VipRightParams;
import com.xquark.service.vipRight.VipRightPromotionInfo;
import com.xquark.service.vipRight.VipRightService;
import com.xquark.service.vo.LogisticsStock;
import com.xquark.service.vo.LogisticsStockParam;
import com.xquark.service.vo.SubOrder;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.DynamicPricingUtil;
import com.xquark.utils.UniqueNoUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.xquark.dal.type.PromotionType.*;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Api(value = "order", description = "订单管理")
public class OrderController extends BaseController {

    private static final String WHITELIST_CONFIRE_ALERT_CONTENT = "该活动尚未开始，请耐心等待";
    @Autowired
    private CustomerProfileService customerProfileService;
    @Autowired
    private PromotionConfigService promotionConfigService;
    @Autowired
    private OrderItemCommentService orderItemCommentService;
    @Autowired
    private OrderService orderService;

    @Autowired
    private ServMessageMapper servMessageMapper;
    @Autowired
    private ZoneService zoneService;
    @Autowired
    private ResourceFacade resourceFacade;
    @Autowired
    private ApiVisitorLogService apiVisitorLogService;

    @Autowired
    private PgPromotionServiceAdapter pgPromotionServiceAdapter;

    @Autowired
    private PromotionOrderDetailService promotionOrderDetailService;

    private OrderHeaderService orderHeaderService;

    @Autowired
    private PieceCacheService pieceCacheService;
    @Autowired
    private PromotionSkusMapper promotionSkusMapper;

    @Autowired
    public void setOrderHeaderService(OrderHeaderService orderHeaderService) {
        this.orderHeaderService = orderHeaderService;
    }


    @Autowired
    private UserService userService;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private OrderRefundService orderRefundService;

    @Autowired
    private UnionService unionService;
    @Autowired
    private CashierService cashierService;

    @Autowired
    private ShopService shopService;

    @Autowired
    private ActivityGrouponService activityGrouponService;

    @Autowired
    private UrlHelper urlHelper;

    @Autowired
    private UserAgentService userAgentService;

    @Autowired
    private AuditRuleService auditRuleService;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private BillService billService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private MainOrderService mainOrderService;

    @Autowired
    private OrderUserDeviceService orderUserDeviceService;

    @Autowired
    private ProductService productService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private CartService cartService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private PrivilegeCodeService privilegeCodeService;

    @Autowired
    private SystemRegionService systemRegionService;

    @Autowired
    private PersonalBillMapper personalBillMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private FlashSalePromotionProductService flashSalePromotionProductService;

    @Autowired
    private SfRegionService sfRegionService;

    @Autowired
    private PromotionBaseInfoService promotionBaseInfoService;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Autowired
    private ThirdPaymentQueryService thirdPaymentQueryService;

    @Value("${tech.serviceFee.standard}")
    private String serviceFeethreshold;

    @Value("${order.delaysign.date}")
    private int defDelayDate;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private SkuMapper skuMapper;

    @Autowired
    private LogisticsGoodsService logisticsGoodsService;

    @Autowired
    private VipRightService vipRightService;

    @Autowired
    private PromotionSkusSerciceImpl promotionSkusService;

    @Autowired
    private PromotionSkuGiftService promotionSkuGiftService;

    @Autowired
    private PromotionInviteCodeService promotionInviteCodeService;

    @Autowired
    private PromotionPgMemberInfoService promotionPgMemberInfoService;

    @Autowired
    private PointContextInitialize pointContextInitialize;

    @Autowired
    private FreshmanLotteryService freshmanLotteryService;

    @Autowired
    private FreshmanTrackService freshmanTrackService;

    /**
     * 买家获取订单信息
     */
    @Autowired
    private PieceGroupPayCallBackService pieceGroupPayCallBackService;

    @Autowired
    private FreshManProductService freshManProductService;
    @Autowired
    private FirstOrderService firstOrderService;

    @Autowired
    private FreshManConfigMapper freshManConfigMapper;

    @Autowired
    private FreshmanRecommendService freshmanRecommendService;
    @Autowired
    private FreshManFlagService freshManFlagService;
    @Autowired
    private FreshManService freshManService;
    @Autowired
    private FirstOrderMapper firstOrderMapper;

    @Autowired
    private PromotionReserveService promotionReserveService;

    @Autowired
    private VipProductService vipProductService;

    /**
     * 买家获取订单信息
     */
    @ResponseBody
    @RequestMapping("/my/orders")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> myOrders(String status, Pageable pageable) {
        Map<String, Object> result = new HashMap<>();
        OrderStatus os = null;
        try {
            if (StringUtils.isNotEmpty(status)) {
                os = OrderStatus.valueOf(status.toUpperCase());
            }
        } catch (Exception e) {
            log.error("status emnuaration error:" + e);
        }
        List<OrderVO> orders = orderService.listByStatus4Buyer(os, pageable);
        for (OrderVO order : orders) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }
            order.setImgUrl(imgUrl);

            if (order.getOrderAddress() != null) {
                getSysteRegion(order);
            }

            //判断是否显示退款按钮
            if (order.getStatus() != OrderStatus.SUBMITTED
                    && order.getStatus() != OrderStatus.SUCCESS
                    && order.getStatus() != OrderStatus.CANCELLED) {

                if (order.getStatus() == OrderStatus.CLOSED) {
                    List<OrderRefund> refunds = orderRefundService.listByOrderId(order.getId());
                    if (refunds.size() > 0) {
                        order.setShowRefundBtn(true);
                    }
                } else {
                    order.setShowRefundBtn(true);
                }
            }
        }
        Long count = orderService.getCountByStatus4Buyer(os);

        result.put("orders", orders);
        result.put("count", count);
        if (status == null) {
            Long submittedCount = orderService.countBuyerOrdersByStatus(OrderStatus.SUBMITTED);
            Long paidCount = orderService.countBuyerOrdersByStatus(OrderStatus.PAID);
            Long shippedCount = orderService.countBuyerOrdersByStatus(OrderStatus.SHIPPED);
            result.put("submittedCount", submittedCount);
            result.put("paidCount", paidCount);
            result.put("shippedCount", shippedCount);
        }
        return new ResponseObject<>(result);
    }

    @ResponseBody
    @RequestMapping(value = "/order/third/status", method = RequestMethod.GET)
    public ResponseObject<ThirdPaymentQueryRes> queryThird(@RequestParam String orderNo) {
        final Order order = orderService.loadByOrderNo(orderNo);
        if (order == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单不存在");
        }
        java.util.Optional<ThirdPaymentQueryRes> query = thirdPaymentQueryService.query(order);
        return new ResponseObject<>(query.orElse(null));
    }

    @ResponseBody
    @RequestMapping("/order/list2")
    public ResponseObject<List<OrderVO>> search2(String status, Pageable pageable) {
        OrderStatus os = null;
        try {
            if (StringUtils.isNotEmpty(status)) {
                os = OrderStatus.valueOf(status.toUpperCase());
            }
        } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单状态不支持");
        }

        List<OrderVO> orderData = getOrderData(os, pageable, orderService::listByStatus4Seller2);
        return new ResponseObject<>(orderData);
    }

    private List<OrderVO> getOrderData(OrderStatus status, Pageable pageable, BiFunction<OrderStatus, Pageable, List<OrderVO>> provider) {
        List<OrderVO> result = provider.apply(status, pageable);
        for (OrderVO order : result) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
                item.setProductName(item.getTitle());
            }
            order.setImgUrl(imgUrl);

            if (order.getOrderAddress() != null) {
                getSysteRegion(order);
            }
        }

        if (result.size() > 0 && OrderStatus.PAID.equals(status)) {
            OrderVO vo = result.get(0);
            vo.setSeq(orderService.selectOrderSeqByShopId(vo.getShopId()));
        }
        return result;
    }


    /**
     * 卖家获取订单信息
     */
    @ResponseBody
    @RequestMapping("/order/list")
    @ApiIgnore
    public ResponseObject<List<OrderVO>> search(String status, Pageable pageable) {
        OrderStatus os = null;
        try {
            if (StringUtils.isNotEmpty(status)) {
                os = OrderStatus.valueOf(status.toUpperCase());
            }
        } catch (Exception e) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单状态不支持");
        }

        List<OrderVO> orderData = getOrderData(os, pageable, orderService::listByStatus4Seller);
        return new ResponseObject<>(orderData);
    }

    /**
     * 卖家获取订单信息
     */
    @ResponseBody
    @RequestMapping("/merchant/order/list")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> search4Merchant(OrderSearchForm orderSearchForm,
                                                               Pageable pageable) {
        Map<String, Object> params = new HashMap<>();
        if (orderSearchForm.getStatus() != null) {
            params.put("status", new OrderStatus[]{orderSearchForm.getStatus()});
        }
        if (orderSearchForm.getPaymentMode() != null) {
            params.put("payType", new PaymentMode[]{orderSearchForm.getPaymentMode()});
        }
        if (StringUtils.isNotEmpty(orderSearchForm.getOrderNo()) &&
                !orderSearchForm.getOrderNo().contains("ESO") &&
                !orderSearchForm.getOrderNo().contains("P")) {
            params.put("tradeNo", orderSearchForm.getOrderNo());
        } else if (StringUtils.isNotEmpty(orderSearchForm.getOrderNo())) {
            params.put("orderNo", "%" + orderSearchForm.getOrderNo() + "%");
        }
        if (orderSearchForm.getOrderType() != null) {
            params.put("orderType", orderSearchForm.getOrderType());
        }
        if (orderSearchForm.getGrouponStatus() != null) {
            params.put("grouponStatus", orderSearchForm.getGrouponStatus());
        }
        if (orderSearchForm.getSupplier() != null) {
            params.put("supplier", orderSearchForm.getSupplier());
        }
        if (orderSearchForm.getAgentType() != null) {
            params.put("agentType", orderSearchForm.getAgentType());
        }
        if (StringUtils.isNotEmpty(orderSearchForm.getBuyerName())) {
            params.put("buyerName", "%" + orderSearchForm.getBuyerName() + "%");
        }
        if (StringUtils.isNotEmpty(orderSearchForm.getSellerName())) {
            params.put("sellerName", "%" + orderSearchForm.getSellerName() + "%");
        }

        if (StringUtils.isNotEmpty(orderSearchForm.getBuyerRequire())) {
            params.put("buyerRequire", orderSearchForm.getBuyerRequire());
        }

        if (null != orderSearchForm.getRefundStatus()) {
            params.put("refundStatus", orderSearchForm.getRefundStatus().toString());
        }

        if (StringUtils.isNotEmpty(orderSearchForm.getLogisticsNo())) {
            params.put("logisticsNo", orderSearchForm.getLogisticsNo());
        }

        if (StringUtils.isNotEmpty(orderSearchForm.getConsignee())) {
            params.put("consignee", "%" + orderSearchForm.getConsignee() + "%");
        }

        params.put("startDate", orderSearchForm.getCompleteStartDate());

        if (StringUtils.isNotBlank(orderSearchForm.getCompleteEndDate())) {
            try {
                Date date = DateUtils.addDays(
                        DateUtils.parseDate(orderSearchForm.getCompleteEndDate(), "yyyy-MM-dd"), 1);
                params.put("endDate", date);
            } catch (ParseException e) {
            }
        }

        params.put("rootShopId", getCurrentIUser().getShopId());

        List<OrderVO> result = orderService.listByMerchant(params, pageable);
        for (OrderVO order : result) {
            String imgUrl = "";
            // 订单的总数量和平均价格
            BigDecimal amount = new BigDecimal("0");
            BigDecimal price = new BigDecimal("0");
            int i = 0;
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
                amount = amount.add(new BigDecimal(item.getAmount()));
                price = price.add(item.getPrice());
                i++;
            }
            if (i != 0)
                order.setPrice(price.divide(new BigDecimal(i), 2, BigDecimal.ROUND_HALF_UP));
            order.setAmount(amount);

            order.setImgUrl(imgUrl);
            // 非自提方式带出地址
            if (order.getIsPickup() != 1) {
                getSysteRegion(order);
            }
            // 自提方式买家根据buyid获取用户信息
            else {
                String buyerId = order.getBuyerId();
                User buyer = userService.load(buyerId);
                order.setBuyerName(buyer.getName());
                order.setBuyerPhone(buyer.getPhone());
            }
            // 团购订单同时要查询团购状态
            if (order.getOrderType() == OrderSortType.GROUPON) {
                ActivityGroupon activityGroupon = activityGrouponService
                        .findActivityGrouponByOrderId(order.getId());
                order.setGrouponStatus(activityGroupon.getStatus());
            }

            // b2b经销商订单需要显示用户级别
            String buyerId = order.getBuyerId();
            UserAgentVO userAgentVO = userAgentService.selectByUserId(buyerId);
            if (userAgentVO != null) {
                order.setAgentType(userAgentVO.getType());
            }

            // b2b经销商订单需要判断哪些订单需要平台审核
            AuditRule auditRule = auditRuleService.selectByOrderId(order.getId());
            if (auditRule != null) {
                order.setAuditType(auditRule.getAuditType());
            }

            // 退款订单、换货订单、补货订单
            if (order.getStatus() == OrderStatus.REFUNDING || order.getStatus() == OrderStatus.CHANGING
                    || order.getStatus() == OrderStatus.REISSUING) {
                OrderRefund refund;
                List<OrderRefund> list = orderRefundService.listByOrderId(order.getId());
                if (list != null && list.size() > 0) {
                    refund = list.get(0);
                    order.setRefundAt(refund.getCreatedAt());
                    order.setRefundFee(refund.getRefundFee());
                    order.setRefundLogisticsCompany(refund.getLogisticsCompany());
                    order.setRefundLogisticsNo(refund.getLogisticsNo());
                    order.setRefundMemo(refund.getRefundMemo());
                    order.setRefundReason(refund.getRefundReason());
                    order.setBuyerRequire("" + refund.getBuyerRequire());
                    order.setRefundImgList(refund.getRefundImgList());
                    order.setRefundStatus(refund.getStatus().toString());
                }
            }
        }

        Map<String, Object> aRetMap = new HashMap<>();
        aRetMap.put("orderTotal", orderService.countByMerchant(params));
        aRetMap.put("list", result);
        return new ResponseObject<>(aRetMap);
    }

    /**
     * 查询订单
     */
    private List<OrderVO> orderListProvider(String userId, String status, Pageable pageable, Function<OrderStatus, List<OrderVO>> statusQuery) {
        List<OrderVO> result;
        OrderStatus os;
        if ("all".equalsIgnoreCase(status)) {
            result = statusQuery.apply(null);
        } else if (StringUtils.equalsIgnoreCase(status, OrderStatus.COMMENT.name())) {
            result = orderService.listUnCommentOrder(pageable, userId);
        } else {
            try {
                os = OrderStatus.valueOf(status.toUpperCase());
            } catch (Exception e) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单类型不正确");
            }
            result = statusQuery.apply(os);
        }
        listOrderHander(result);
        apiVisitorLogService.visit(orderService.getCurrentUser().getId(), "/order/list/{status}");
        this.convretOrderVOs(result);
        return result;
    }

    private void listOrderHander(List<OrderVO> result) {
        for (final OrderVO order : result) {
            // 预约单的订单编号和订单号相同
            if (order.getOrderNo().contains(UniqueNoUtils.UniqueNoType.PRO.name())
                    && order.getOrderNo().equals(order.getPreOrderNo())) {
                order.setOrderNo(null);
                continue;
            }
            String imgUrl = "";
            Integer num = personalBillMapper.haveBill(order.getMainOrderId());
            order.setHaveBill(num > 0);
            if (order.getOrderItems().size() < 1) {
                continue;
            }

            String productId = order.getOrderItems().get(0).getProductId();
            Product product = productMapper.getSupIdById(productId);
            if (product != null) {
                String name = supplierMapper.getNameById(Integer.parseInt(product.getSupplierId()));
                if (StringUtils.equals(name, "顺丰") || StringUtils.equals(name, "汉德森")) {
                    order.setCheckLoc(true);
                } else {
                    order.setCheckLoc(false);
                }
                order.setCheckRef(product.getSupportRefund());
            }
            List<SubOrder> subOrders = logisticsGoodsService.findSubOrder(order.getPartnerOrderNo());
            List<SubOrderDal> subOrderDals = new ArrayList<>();
            for (SubOrder s : subOrders) {
                SubOrderDal subOrderDal = new SubOrderDal();
                subOrderDal.setOrderId(s.getOrderId());
                subOrderDal.setOrderSn(s.getOrderSn());
                subOrderDal.setOrderStatus(s.getOrderStatus());
                subOrderDals.add(subOrderDal);
            }
            order.setSubOrder(subOrderDals);
            if (order.getPartnerOrderNo() == null) {
                order.setPartnerOrderNo(order.getOrderNo());
            }

            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
                item.setProductName(item.getTitle());
                String skuId = item.getSkuId();
                if (StringUtils.isNotBlank(skuId)) {
                    Sku sku = skuMapper.selectByPrimaryKey(skuId);
                    item.setSku(sku);
                }
                Product p = productMapper.getSupIdById(item.getProductId());
                if (p != null)
                    item.setSelfOperated(p.getSelfOperated());
            }

            order.setImgUrl(imgUrl);
            if (order.getOrderAddress() != null) {
                getSysteRegion(order);
            } else {
                order.setAddressDetails("自提");
            }

            //设置售后按钮是否显示
            if (order.getCapableToRefund()) {//订单可退
                if (order.getOrderType() == OrderSortType.EXCHANGE || order.getOrderType() == OrderSortType.REISSUE) {
                    order.setShowRefundBtn(false);//如果为换货订单类型、拼团订单，不显示售后
                } else {
                    //是否在可申请售后时间内判断
                    if (checkOvertime(order)) {
                        order.setShowRefundBtn(true);
                    } else {
                        order.setShowRefundBtn(false);
                    }
                }
            }

            order.setShowCancelRefundBtn(false);
            order.setCheckCancelOrder(false);

            //如果可以进行退款，则要设置可退金额字段
            if (order.getCapableToRefund()) {
                order.setRefundFee(order.getTotalFee());//fixme 应该是setRefundableFee，此处为了兼容客户端
            }

            if (OrderStatus.PAID == order.getStatus()) {
                if (null != order.getPaidAt()) {
                    long t = System.currentTimeMillis() - order.getPaidAt().getTime();
                    if (t <= 1800000) {//支付半小时内可以取消
                        if (order.getOrderType() == OrderSortType.EXCHANGE || order.getOrderType() == OrderSortType.REISSUE) {
                            order.setCheckCancelOrder(false);//换货订单不显示取消订单按钮
                        } else {
                            order.setCheckCancelOrder(true);
                        }
                    }
                }
            }

            // VIP订单和大会订单不可取消
            if (OrderSortType.VIP_RIGHT.equals(order.getOrderType()) ||
                    /**
                     * 修改地址新增功能：大会商品支持取消订单
                     */
//          OrderSortType.MEETING.equals(order.getOrderType()) ||
                    OrderSortType.isPiece(order.getOrderType())) {
                order.setCheckCancelOrder(false);
            } else if (OrderSortType.MEETING.equals(order.getOrderType())) {
                if (OrderStatus.PAID == order.getStatus() || OrderStatus.PAIDNOSTOCK == order.getStatus()) {
                    if (null != order.getPaidAt()) {
                        long t = System.currentTimeMillis() - order.getPaidAt().getTime();
                        if (t <= 1800000) {//支付半小时内可以取消
                            if (order.getOrderType() == OrderSortType.EXCHANGE || order.getOrderType() == OrderSortType.REISSUE) {
                                order.setCheckCancelOrder(false);//换货订单不显示取消订单按钮
                            } else {
                                order.setCheckCancelOrder(true);
                            }
                        }
                    }
                }
            }

            List<OrderRefund> orderRefundList = orderRefundService.listByOrderId(order.getId());
            if (orderRefundList != null && orderRefundList.size() > 0) {
                OrderRefund refund = orderRefundList.get(0);
                order.setRefundStatus(refund.getStatus().toString());
                if (refund.getStatus() == OrderRefundStatus.SUBMITTED
                        || refund.getStatus() == OrderRefundStatus.SUCCESS) {
                    order.setShowCancelRefundBtn(true);
                }
            }

            order.setCheckingAt(order.getSucceedAt());
            boolean isCommented = orderService.isOrderComment(order.getId(), order.getBuyerId());
            order.setCommented(isCommented);
            //物流用dest设值
            order.setDestForLogistics(order.getDest());

            // 设置前端是否显示修改地址
            if (order.getDest() != null) {
                if (order.getDest().equals("SF_REFUNDABLE") ||
                        order.getDest().equals("SF_NOT_REFUNDABLE")) {
                    order.setCheckAddress(false);
                } else {
                    order.setCheckAddress(true);
                }
            }

            /**
             * 设置前端是否显示申请退款
             */
            if (order.getDest() != null) {
                if (order.getDest().contains("NOT_REFUNDABLE")) {
                    log.info("存在包含关系");
                    order.setDest(ErpServiceDestinationFactory.getErp("NOT_REFUNDABLE"));
                } else {
                    log.info("不存在包含关系");
                    order.setDest(ErpServiceDestinationFactory.getErp("REFUNDABLE"));
                }
            }
        }
    }

    @ResponseBody
    @RequestMapping("/order/list/{status}")
    @ApiIgnore
    public ResponseObject<List<OrderVO>> listOrder(@PathVariable String status,
                                                   @RequestParam(required = false) String userId,
                                                   @RequestParam(required = false) String dateStr, Pageable pageable) {
        List<OrderVO> result = orderListProvider(userId, status, pageable,
                s -> orderService.listByStatus(s, pageable, userId, dateStr));
        return new ResponseObject<>(result);
    }

    /**
     * 2019-04-19 适配新的PENDING接口, 老接口暂时不动
     */
    @ResponseBody
    @RequestMapping("/order/list2/{status}")
    public ResponseObject<List<OrderVO>> listOrder2(@PathVariable String status,
                                                    @RequestParam(required = false) String userId,
                                                    @RequestParam(required = false) String dateStr, Pageable pageable) {
        List<OrderVO> result = orderListProvider(userId, status, pageable,
                s -> orderService.listByStatus2(s, pageable, userId, dateStr));
        return new ResponseObject<>(result);
    }

    /**
     * 已关闭订单状态合并搜索接口特殊处理
     */
    @SuppressWarnings({"unchecked", "unused"})
    @ResponseBody
    @RequestMapping("/order/listByClosed/{status}")
    @ApiIgnore
    public ResponseObject<List<OrderVO>> searchOrderByKey(@RequestParam("key") String key,
                                                          @PathVariable String status) {
        if (status == null) {
            return new ResponseObject<>();
        }
        if (key == null || key.isEmpty()) {
            ResponseObject<List<OrderVO>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((List<OrderVO>) Collections.EMPTY_LIST);
            return aEmptyList;
        }

        List<OrderVO> result = orderService.listByStatusKey4Seller(OrderStatus.CLOSED, key);

        for (OrderVO order : result) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }
            order.setImgUrl(imgUrl);

            if (order.getOrderAddress() != null) {
                getSysteRegion(order);
            }
        }
        return new ResponseObject<>(result);
    }


    @ResponseBody
    @RequestMapping("/order/listByKey/{status}")
    @ApiIgnore
    public ResponseObject<List<OrderVO>> listOrderByKey(String key, @PathVariable String status,
                                                        Pageable pageable, @RequestParam(required = false) String userId,
                                                        @RequestParam(required = false) String startTime,
                                                        @RequestParam(required = false) String endTime) {
        //订单号 买家姓名 电话
        if (status == null) {
            return new ResponseObject<>();
        }
        OrderStatus os;
        List<OrderVO> result;

        //todo 时间段查询
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);

        if ("all".equalsIgnoreCase(status)) {
            result = orderService.listByStatusKey4Seller(null, key, pageable, params, userId);
        } else {
            try {
                os = OrderStatus.valueOf(status.toUpperCase());
            } catch (Exception e) {
                return new ResponseObject<>();
            }
            result = orderService.listByStatusKey4Seller(os, key, pageable, params, userId);
        }
        for (OrderVO order : result) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }

            for (OrderItemVO item : order.getOrderItemVOs()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }

            order.setImgUrl(imgUrl);

            if (order.getOrderAddress() != null) {
                getSysteRegion(order);
            }
        }

        return new ResponseObject<>(result);
    }

    private BigDecimal findRefundableFee(OrderVO order, OrderStatus orderStatus) {
        BigDecimal refundableFee;

        refundableFee = order.getTotalFee();
        if (orderStatus != OrderStatus.PAID) {
            refundableFee = refundableFee.subtract(order.getLogisticsFee());
        }
        return refundableFee;
    }

    /**
     * 订单详情
     */
    @ResponseBody
    @RequestMapping("/order/detail")
    @ApiIgnore
    public ResponseObject<OrderVOEx> view2(@RequestParam String id, HttpServletRequest req) {
        return this.view(id, req);
    }

    /**
     * 订单删除
     */
    @ResponseBody
    @RequestMapping(value = "/order/delete", method = POST)
    @ApiOperation(value = "订单删除", notes = "订单删除", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> delete(@RequestParam String orderId) {
        return new ResponseObject<>(orderService.delete(orderId) == 1);
    }

    /**
     * 修改订单地址
     */
    @ResponseBody
    @RequestMapping(value = "/order/update/{orderId}/systemRegion", method = POST)
    @ApiOperation(value = "修改订单地址", notes = "修改订单地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Map<String, Object>> update(@PathVariable String orderId, @ModelAttribute AddressForm form) {
        Address address = new Address();
        BeanUtils.copyProperties(form, address);
        Map<String, Object> resultMap = new HashMap<>();
        boolean b = orderService.updateOrderAddress(orderId, address);
        if (b) {
            resultMap.put("updateStatus", true);
        } else {
            resultMap.put("updateStatus", false);
        }
        return new ResponseObject<>(resultMap);
    }

    /**
     * 卖家更新未付款订单的价格
     */
    @RequestMapping("/order/{id}/update-price")
    @ResponseBody
    @ApiIgnore
    public ResponseObject<OrderVO> updatePrice(@PathVariable String id, BigDecimal price) {
        orderService.updateTotalPrice(id, price);

        OrderVO order = orderService.loadVO(id);
        String imgUrl = "";
        for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
        }
        order.setImgUrl(imgUrl);

        if (order.getOrderAddress() != null) {
            getSysteRegion(order);
        }

        return new ResponseObject<>(order);
    }

    /**
     * 卖家更新未付款订单的价格
     */
    @RequestMapping("/order/update-price")
    @ResponseBody
    @ApiIgnore
    public ResponseObject<OrderVO> updateOrderPrice(String id, String price, String goodsFee,
                                                    String logisticsFee) {
        if (StringUtils.isNoneBlank(goodsFee) || StringUtils.isNoneBlank(logisticsFee)) {
            BigDecimal bGoodsFee = null, bLogisticsFee = null;
            if (StringUtils.isNotBlank(goodsFee)) {
                bGoodsFee = NumberUtils.createBigDecimal(goodsFee)
                        .setScale(2, BigDecimal.ROUND_DOWN);
            }

            if (StringUtils.isNoneBlank(logisticsFee)) {
                bLogisticsFee = NumberUtils.createBigDecimal(logisticsFee)
                        .setScale(2, BigDecimal.ROUND_DOWN);
            }
            if (bGoodsFee.compareTo(BigDecimal.ZERO) == 0) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品价格不能为0");
            }
            //商品价格不能改为0
            orderService.updatePrice(id, bGoodsFee, bLogisticsFee);

        } else { //支持老接口
            BigDecimal bPrice = new BigDecimal(price).setScale(2, BigDecimal.ROUND_DOWN);
            orderService.updateTotalPrice(id, bPrice);
        }

        OrderVO order = orderService.loadVO(id);
        String imgUrl = "";
        for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
        }
        order.setImgUrl(imgUrl);

        if (order.getOrderAddress() != null) {
            getSysteRegion(order);
        }

        return new ResponseObject<>(order);
    }

    private void getSysteRegion(OrderVO order) {
        OrderAddress oa = order.getOrderAddress();
        List<SystemRegion> systemRegionList = systemRegionService.listParents(oa.getZoneId());
        StringBuilder details = new StringBuilder();
        for (SystemRegion systemRegion : systemRegionList) {
            details.append(systemRegion.getName());
        }
        details.append(oa.getStreet());

        oa.setSystemRegions(systemRegionList);

        order.setAddressDetails(details.toString());
    }

    @ResponseBody
    @RequestMapping(value = "/api/order/cancel", method = POST)
    @ApiOperation(value = "订单取消", notes = "订单取消", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> cancelOrder(@RequestParam String orderId) {
        Order order = orderService.load(orderId);

        if (!order.getStatus().equals(OrderStatus.PAID) && !order.getStatus()
                .equals(OrderStatus.SUBMITTED)) {
            if (!OrderSortType.MEETING.equals(order.getOrderType())) {
                throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "当前订单无法取消");
            } else {
                if (!order.getStatus().equals(OrderStatus.PAID) &&
                        !order.getStatus().equals(OrderStatus.PAIDNOSTOCK)) {
                    throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "当前订单无法取消");
                }
            }
        }

        if (OrderStatus.PAID == order.getStatus() || OrderStatus.PAIDNOSTOCK == order.getStatus()) {
            List<OrderItem> orderItemList = orderItemMapper.selectByOrderId(orderId);
            OrderItem orderItem = null;
            if (orderItemList != null && !orderItemList.isEmpty()) {
                orderItem = orderItemList.get(0);
            }
            if (orderItem != null && orderItem.getProductId() != null) {
                Product product = productService.load(orderItem.getProductId());
                if (StringUtils.equals(product.getSource(), ProductSource.SF.name())) {
                    throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "生鲜食品暂不支持取消订单");
                }
            }

            // 半支付小时后不能取消订单
            long t = System.currentTimeMillis() - order.getPaidAt().getTime();
            if (t > 1800000) {
                throw new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR, "当前订单无法取消");
            } else {
                orderService.refund(orderId);
                return new ResponseObject<>(true);
            }
        }
        if (order.getStatus().equals(OrderStatus.SUBMITTED)) {
            //如果订单还未付款，则取消全部订单
            orderService.cancelMainOrder(order.getMainOrderId());
        }
        return new ResponseObject<>(true);
    }

    /**
     * 卖家订单发货
     */
    @ResponseBody
    @RequestMapping("/order/shipped")
    @ApiIgnore
    public ResponseObject<Boolean> shipped(@Valid @ModelAttribute OrderShippedForm form,
                                           Errors errors) {
        ControllerHelper.checkException(errors);
        String logisticComp = "";
        try {
            logisticComp = LogisticsCompany.valueOf(form.getLogisticsCompany()).toString();
        } catch (Exception ex) {
            logisticComp = form.getLogisticsCompany();
        }
        return new ResponseObject<>(
                orderService.ship(form.getOrderId(), logisticComp, form.getLogisticsOrderNo()));
    }

    /**
     * 卖家订单退款
     *
     * @param orderId 订单id
     * @param refundment 退款金额
     * @param refundReason 退款原因
     * @param logisticsFee 物流费用
     * @return
     */
    @ResponseBody
    @RequestMapping("/order/refund")
    @ApiIgnore
    @Transactional
    public ResponseObject<Boolean> refund(@RequestParam String orderId, String refundment, String refundReason, String logisticsFee) {
        List<OrderRefund> orderRefunds = orderRefundService.listByOrderId(orderId);
        if (orderRefunds != null && orderRefunds.size() > 0) {
            for (OrderRefund orderRefund :
                    orderRefunds) {
                if ("company".equals(refundReason)) {//如果为公司原因，统一替换为商品质量问题，适配当前代码及需求
                    orderRefund.setRefundReason("商品质量问题");
                    orderRefundService.update(orderRefund);
                }
            }
        }
        if (refundment != null) {
            BigDecimal refundFee = new BigDecimal(refundment).setScale(2, BigDecimal.ROUND_DOWN);
            orderService.refund(orderId, refundFee);
        } else {
            orderService.refund(orderId);
        }
        //积分充值抵用户退货的邮费
        Order order = orderService.load(orderId);
        BigDecimal logiFee = new BigDecimal(logisticsFee);
        if (!logiFee.equals(BigDecimal.ZERO)) {
            if (new BigDecimal(100).compareTo(logiFee) < 0) {
                throw new BizException(GlobalErrorCode.UNKNOWN, "输入的积分超过最大限制");
            }
            if (new BigDecimal(logisticsFee).signum() == -1) {
                throw new BizException(GlobalErrorCode.UNKNOWN, "输入的积分不能为负数");
            }
            if (order != null && order.getBuyerId() != null) {
                UserInfoVO userInfo = userService.getUserInfo(order.getBuyerId());
                if (userInfo != null && userInfo.getUser() != null) {
                    pointContextInitialize.getCommissionServiceApi().modify(userInfo.getUser().getCpId(), order.getOrderNo(),
                            FunctionCodeType.getLogisticPayBack(), PlatformType.E, logiFee);
                }
            }
        }

        //退货完成，修改order_header表状态
        orderHeaderService
                .updateStatusByOrderNo(Integer.toString(11), orderService.load(orderId).getOrderNo());
        //发送消息
        try {
            ServMessage(ServMessageStatus.REFUNDSUCCESS.getWord(orderService.load(orderId).getOrderNo()), orderService.load(orderId).getOrderNo());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("订单{}退款发送失败", orderService.load(orderId).getOrderNo());
        }
        return new ResponseObject<>(true);
    }

    /**
     * 退款新接口141230版本更新
     */
    @ResponseBody
    @RequestMapping("/order/refundFee")
    @ApiIgnore
    public ResponseObject<Boolean> refundFee(String orderId, String goodsFee, String logisticsFee) {

        BigDecimal bGoodsFee = new BigDecimal(goodsFee).setScale(2, BigDecimal.ROUND_DOWN);
        BigDecimal bLogisticsFee = new BigDecimal(logisticsFee).setScale(2, BigDecimal.ROUND_DOWN);

        orderService.refund(orderId, bGoodsFee, bLogisticsFee);
        return new ResponseObject<>(true);
    }

    /**
     * 订单签收
     */
    @ResponseBody
    @RequestMapping(value = "/order/signed", method = POST)
    @ApiOperation(value = "订单确认收货", notes = "订单确认收货", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> signed(@RequestParam String orderId) {
        orderService.sign(orderId);
        return new ResponseObject<>(true);
    }

    /**
     * 延迟签收 (推迟账务打款时间, 买家发起)
     */
    @ResponseBody
    @RequestMapping("/order/delaySign")
    @ApiIgnore
    public ResponseObject<Boolean> delaySign(@RequestParam String orderId) {
        orderService.delaySign(orderId);
        return new ResponseObject<>(true);
    }

    /**
     * 提醒卖家发货
     */
    @ResponseBody
    @RequestMapping("/order/remindShip")
    @ApiIgnore
    public ResponseObject<Boolean> remindShip(@RequestParam("orderId") String orderId) {
        orderService.remindShip(orderId);
        return new ResponseObject<>(true);
    }

    /**
     * 订单留言
     */
    @ResponseBody
    @RequestMapping("/order/send-message")
    @ApiIgnore
    public ResponseObject<Boolean> sendOrderMessage(@Valid @ModelAttribute OrderMessage form) {
        if (StringUtils.isEmpty(form.getOrderId())) {
            return new ResponseObject<>(false);
        }
        return new ResponseObject<>(orderService.saveMessage(form));
    }

    /**
     * 查看有留言订单
     */
    @SuppressWarnings("unchecked")
    @ResponseBody
    @RequestMapping("/order/list-msgorders")
    @ApiIgnore
    public ResponseObject<List<MsgOrdersVO>> listMessageOrders(Pageable page) {
        List<MsgOrdersVO> messages = orderService.listMsgOrders(page);
        if (messages != null && messages.size() != 0) {
            for (MsgOrdersVO aVo : messages) {
                aVo.setProductImg(aVo.getProductImg());
            }
            return new ResponseObject<>(messages);
        } else {
            ResponseObject<List<MsgOrdersVO>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((List<MsgOrdersVO>) Collections.EMPTY_LIST);
            return aEmptyList;
        }
    }

    private List<OrderMessage> filterBuyerCmt(List<OrderMessage> msgList) {
        if (msgList == null || msgList.size() == 0) {
            return null;
        }

        List<OrderMessage> retList = new ArrayList<>();
        for (OrderMessage om : msgList) {
            if (om.getGroupId().equals("0")) {
                retList.add(om);
            }
        }
        Collections.reverse(retList);
        return retList;
    }

    private List<OrderMessage> filterSellerReps(String groupId, List<OrderMessage> msgList) {
        if (msgList == null || msgList.size() == 0) {
            return null;
        }
        List<OrderMessage> retList = new ArrayList<>();

        for (OrderMessage om : msgList) {
            if (om.getGroupId().equals(groupId)) {
                retList.add(om);
            }
        }
        Collections.reverse(retList);
        return retList;
    }

    /**
     * 查看某条订单留言与回复
     */
    @SuppressWarnings("unchecked")
    @ResponseBody
    @RequestMapping("/order/list-messages")
    @ApiIgnore
    public ResponseObject<Map<String, Object>> listOrderMessages(@RequestParam String orderId) {
        // 检查该订单是否属于当前用户
        final Map<String, Object> result = new HashMap<>();
        Order aOrder = orderService.load(orderId);
        if (orderId == null || aOrder == null || !aOrder.getSellerId()
                .equals(getCurrentUser().getId())) {
            throw new BizException(GlobalErrorCode.UNAUTHORIZED, "参数不合法");
        }
        List<OrderMessage> messages = orderService.viewMessages(orderId);
        if (messages == null) {
            ResponseObject<Map<String, Object>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((Map<String, Object>) Collections.EMPTY_MAP);
            return aEmptyList;
        }

        List<OrderMessageVO> retList = new ArrayList<>();
        List<OrderMessage> bCmtList = filterBuyerCmt(messages);
        if (bCmtList != null && bCmtList.size() != 0) {
            for (OrderMessage om : bCmtList) {
                filterSellerReps(om.getId(), messages);
                OrderMessageVO aVo = new OrderMessageVO();
                aVo.setLeaderMsg(om); // 一条买家留言
                aVo.setRepsMsg(filterSellerReps(om.getId(), messages));    // 一条或多条卖家回复
                retList.add(aVo);
            }
        } // else 垃圾数据 一条订单的留言以买家发起, 不会只有卖家自己的回复

        MsgProdInfoVO aVo = orderService.selectProdInfoByOrderId(orderId);
        if (aVo != null) {
            result.put("productImg", resourceFacade.resolveUrl(aVo.getProdImg()));
            result.put("productName", aVo.getProdName());
            result.put("skuDesc", aVo.getSkuDesc());
            result.put("paidAt", aVo.getPaidAt());
            result.put("paidFee", aVo.getPaidFee());
            result.put("orderId", orderId);
            result.put("avatar", null);
        } else {
            ResponseObject<Map<String, Object>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((Map<String, Object>) Collections.EMPTY_MAP);
            return aEmptyList;
        }
        User aBuyerUser = userService.load(messages.get(0).getBuyerId());
        if (aBuyerUser != null) {
            result.put("buyerNick", aBuyerUser.getName());
        }
        User aSellerUser = userService.load(messages.get(0).getSellerId());
        if (aSellerUser != null) {
            result.put("sellerNick", aSellerUser.getLoginname());
        }
        result.put("msgList", retList);

        orderService.setMsgRead(orderId);

        return new ResponseObject<>(result);
    }

    /**
     * 查看当前用户所有订单留言
     */
    @SuppressWarnings("unchecked")
    @ResponseBody
    @RequestMapping("/order/list-allMsgs")
    @ApiIgnore
    public ResponseObject<List<OrderMsgVO>> listAllMsgs(Pageable page) { // 分页
        // 获取卖家对买家的回复列表
        List<OrderMessage> aList = orderService.viewReplyMsgs(page);
        if (aList == null || aList.size() == 0) {
            ResponseObject<List<OrderMsgVO>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((List<OrderMsgVO>) Collections.EMPTY_LIST);
            return aEmptyList;
        }

        // 根据卖家对买家留言的groupId找到是回复的哪条买家留言
        List<OrderMsgVO> aRetList = new ArrayList<>();
        for (OrderMessage om : aList) {
            OrderMessage myMsg = orderService.selectOrderMsgById(om.getGroupId());
            if (myMsg != null) {
                OrderMsgVO aVo = new OrderMsgVO();
                aVo.setOrderId(myMsg.getOrderId());
                aVo.setBuyerContent(myMsg.getContent());
                aVo.setSellerContent(om.getContent());
                aVo.setMsgTime(myMsg.getCreatedAt().getTime());
                List<OrderItem> itemList = orderItemMapper.selectByOrderId(myMsg.getOrderId());
                if (itemList != null) {
                    aVo.setProductId(itemList.get(0).getProductId());
                    aVo.setTitle(itemList.get(0).getProductName());
                    aVo.setImgUrl(itemList.get(0).getProductImg());
                    aRetList.add(aVo);
                }
            }
        }
        return new ResponseObject<>(aRetList);
    }

    /**
     * 查看某条订单留言
     */
    @SuppressWarnings("unchecked")
    @ResponseBody
    @RequestMapping("/order/list-Msgs")
    @ApiIgnore
    public ResponseObject<List<OrderMessage>> listOrderMsgs(@RequestParam String orderId) {
        List<OrderMessage> omList = orderService.viewMessages(orderId);
        if (omList != null) {
            return new ResponseObject<>(omList);
        } else {
            ResponseObject<List<OrderMessage>> aEmptyList = new ResponseObject<>();
            aEmptyList.setData((List<OrderMessage>) Collections.EMPTY_LIST);
            return aEmptyList;
        }
    }

    /**
     * 获取物流公司列表
     */
    @ResponseBody
    @RequestMapping("/order/listLogisticsInfo")
    @ApiIgnore
    public ResponseObject<List<LogisticsInfoVO>> listLogistics() {
        List<LogisticsInfoVO> aList = new ArrayList<>();
        for (LogisticsCompany aLC : LogisticsCompany.values()) {
            LogisticsInfoVO avo = new LogisticsInfoVO();
            if (!aLC.equals(LogisticsCompany.OTHER)) {
                avo.setCompany(aLC.toString());
                aList.add(avo);
            }
        }
        return new ResponseObject<>(aList);
    }


    /***
     * 更新订单的售后入口
     * @param capableToRefund
     * @param orderId
     * @return
     */
    @RequestMapping(value = "/order/{id}/refund/capability ", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Boolean> updateCapableToRefund(
            @RequestParam @NotNull Boolean capableToRefund,
            @PathVariable("id") @NotBlank String orderId) {
        boolean ret = orderService.updateCapableToRefundById(capableToRefund, orderId, null);
        return new ResponseObject<>(ret);
    }


    /**
     * 订单item评论
     */
    @ResponseBody
    @RequestMapping("/order/comment/add")
    public ResponseObject<Boolean> addComment(OrderItemComment form) {
        String userId = getCurrentIUser().getId();
        String orderItemId = form.getOrderItemId();
        OrderItem orderItem = orderItemMapper.selectByPrimaryKey(orderItemId);
        // 先插入订单item评论表，然后插入对应商品的评价表
        form.setBuyerId(userId);
        form.setSellerId(userId);
        form.setProductScore(0);
        int rc = orderItemCommentService.insert(form);
        // 插入对应商品的评价表
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setArchive(false);
        comment.setContent(form.getContent());
        comment.setObjId(orderItem.getProductId());
        comment.setType(CommentType.PRODUCT);
        commentService.insert(comment);
        return new ResponseObject<>(rc == 1);
    }

    /**
     * b2b进货订单审核操作
     */
    @ResponseBody
    @RequestMapping(value = "/api/order/audit")
    @ApiIgnore
    public ResponseObject<Boolean> audit(HttpServletRequest request) {
        String orderId = request.getParameter("orderId");
        orderService.audit(orderId);
        return new ResponseObject<>(true);
    }

    /**
     * 订单确认收货请求
     */
    @ResponseBody
    @RequestMapping(value = "/api/order/confirmShipped")
    @ApiIgnore
    public ResponseObject<Boolean> confirmShipped(HttpServletRequest request) {
        String orderId = request.getParameter("orderId");
        OrderVO order = orderService.loadVO(orderId);
        if (order == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单["
                    + orderId + "]不存在");
        }

        if (order.getId() != null) {
            orderService.executeBySystem(order.getId(), OrderActionType.SIGN, null);
            log.info("订单  orderNo=[" + order.getOrderNo() + "]" + "以签收");
        }

        return new ResponseObject<>(true);
    }

    /**
     * 卖家审核发货接口（b2b经销商使用）
     */
    @ResponseBody
    @RequestMapping("/order/auditAndShip")
    @ApiIgnore
    public Json auditAndShip(@Valid @ModelAttribute OrderShippedForm form, Errors errors) {
        ControllerHelper.checkException(errors);
        Json json = new Json();
        String logisticComp = form.getLogisticsCompany();

        boolean flag = orderService
                .auditAndship(form.getOrderId(), logisticComp, form.getLogisticsOrderNo());
        if (!flag) {
            json.setRc(Json.RC_FAILURE);
            json.setMsg("审核失败！");
        } else {
            json.setRc(Json.RC_SUCCESS);
            json.setMsg("审核成功！");
        }

        return json;
    }

    /**
     * 订单修改数量（b2b进货模式）
     */
    @ResponseBody
    @RequestMapping("/order/updateAmount")
    @ApiIgnore
    public ResponseObject<Boolean> updateAmount(@RequestParam String orderNo,
                                                @RequestParam String orderAmount) {
        OrderVO orderVO = orderService.loadByOrderNo(orderNo);
        List<OrderItem> orderItems = orderVO.getOrderItems();
        OrderItem orderItem = orderItems.get(0);
        BigDecimal price = orderItem.getPrice();
        BigDecimal goodsFee = price.multiply(new BigDecimal(orderAmount));

        // 修改orderitem的数量
        OrderItem updateOrderItem = new OrderItem();
        updateOrderItem.setId(orderItem.getId());
        updateOrderItem.setAmount(new Integer(orderAmount));
        orderItemMapper.updateByPrimaryKeySelective(updateOrderItem);

        // 修改order的各种金额
        orderService.updatePriceSystem(orderVO.getId(), goodsFee);

        return new ResponseObject<>(true);
    }

    /**
     * b2b类型进货订单，订单完成前都能强制取消
     */
    @ResponseBody
    @RequestMapping("/api/order/forceCancel")
    @ApiIgnore
    public ResponseObject<Boolean> forceCancelOrder(@RequestParam String orderId) {
        // 先强制将订单状态更改为submitted，然后调用cancel流程
        Order order = new Order();
        order.setId(orderId);
        order.setStatus(OrderStatus.SUBMITTED);
        orderService.update(order);

        orderService.cancel(orderId);
        return new ResponseObject<>(true);
    }


    /**
     * 获取未审核和待收货订单总数
     */
    @ResponseBody
    @RequestMapping("/api/order/submittedCount")
    @ApiIgnore
    public ResponseObject<Long> submittedCount() {
        Map<String, Object> params = new HashMap<>();

        String userId = getCurrentUser().getId();
        params.put("userId", userId);

        long count = orderService.countOrder(params);
        return new ResponseObject<>(count);
    }

    /**
     * 获取各种类型订单总数
     */
    @ResponseBody
    @RequestMapping("/api/order/typeCount")
    public ResponseObject<Long> shippedCount(
            @RequestParam(value = "status", required = true) String status) {
        String userId = getCurrentIUser().getId();
        long count = orderService.countTypeOrderCount(userId, status);
        return new ResponseObject<>(count);
    }


    /**
     * 订单换货完成操作
     */
    @ResponseBody
    @RequestMapping(value = "/order/changeOrder", method = POST)
    @ApiIgnore
    public ResponseObject<Boolean> changeOrder(@RequestParam String orderId) {
        OrderRefund refund = null;
        boolean flag = false;
        List<OrderRefund> list = orderRefundService.listByOrderId(orderId);
        if (list != null) {
            refund = list.get(0);
        }
        if (refund == null) {
            return new ResponseObject<>(false);
        }
        if (orderMapper.updateOrderByCancelRefund(orderId, refund.getOrderStatus()) > 0) {
            orderRefundService.successByOrderId(orderId);
            flag = true;
        }
        return new ResponseObject<>(flag);
    }

    /**
     * 确认订单
     */
    @ResponseBody
    @RequestMapping(value = "/order/confirmOrder", method = POST)
    @ApiIgnore
    public ResponseObject<Boolean> confirmOrder(@RequestParam String orderId) {
        int result = orderService.confirmOrder(orderId);
        return new ResponseObject<>(result > 0);
    }

    /**
     * 检查顺风产品库存
     */
    private void checkSfInventory(OrderSumbitFormApi form, List<Sku> skuList,
                                  List<CartItemVO> cartItems) {
        if (skuList == null || skuList.isEmpty()) {
            return;
        }

        int len = skuList.size();
        String[] productIdArray = new String[len];
        for (int i = 0; i < len; i++) {
            productIdArray[i] = skuList.get(i).getProductId();
        }

        AddressVO address = fillAddressDefault(form.getAddressId());
        List<Product> productList2 = productService.selectByIds(productIdArray);
        List<LogisticsStockParam> logisticsStockParamList = new ArrayList<>();
        if (productList2 != null && !productList2.isEmpty()) {
            for (Product product : productList2) {
                if (product.getSource().equals(ProductSource.SF.name())) {
                    LogisticsStockParam logisticsStockParam = new LogisticsStockParam();
                    logisticsStockParam.setProductid(IdTypeHandler.decode(product.getSourceProductId()));
                    Long areaId = sfRegionService.getsfid(Long.parseLong(address.getZoneId()));
                    logisticsStockParam.setRegionId(Integer.parseInt(String.valueOf(areaId)));
                    logisticsStockParam.setSfairline(product.getSfairline());
                    logisticsStockParam.setSfshipping(product.getSfshipping());
                    logisticsStockParamList.add(logisticsStockParam);
                }
            }
        }

        if (productList2 != null && !productList2.isEmpty()) {
            List<LogisticsStock> logisticsStockList = logisticsGoodsService
                    .getStock(logisticsStockParamList);
            if (logisticsStockParamList.size() != logisticsStockList.size()) {
                throw new BizException(GlobalErrorCode.PRODUCT_INVENTORY,
                        "库存查询失败");
            }
            for (Product product : productList2) {
                if (!Objects.equals(product.getSource(), ProductSource.SF.name())) {
                    continue;
                }
                for (LogisticsStock logisticsStock : logisticsStockList) {
                    if (IdTypeHandler.decode(product.getSourceProductId()) == logisticsStock
                            .getProductId()) {

                        int amount = 0;

                        ProductVO productVO = null;
                        for (CartItemVO cartItemVO : cartItems) {
                            productVO = productService.load(cartItemVO.getSku().getProductId());
                            if (productVO != null
                                    && IdTypeHandler.decode(productVO.getSourceProductId()) == logisticsStock
                                    .getProductId()) {
                                amount = cartItemVO.getAmount();
                                break;
                            }
                        }

                        if (amount == 0) {
                            throw new BizException(GlobalErrorCode.PRODUCT_INVENTORY, "商品数量异常");
                        }

                        int maxSaleNum = logisticsStock.getMaxSaleNum();
                        if (maxSaleNum == 0 || maxSaleNum < amount) {
                            throw new BizException(GlobalErrorCode.PRODUCT_INVENTORY,
                                    product.getName() + " 库存不足");
                        } else {
                            if (address != null && address.getZoneId() != null) {
                                List<SystemRegion> systemRegionList = systemRegionService
                                        .listParents(address.getZoneId());
                                if (systemRegionList != null && !systemRegionList.isEmpty()) {
                                    SystemRegion systemRegion = systemRegionList.get(2);
                                    if ("市辖区".equals(systemRegion.getName())) {
                                        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该区域无效，请重新选择");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 订单提交前大会活动限制相关校验
     */
    private String checkMeetingPromotion(OrderSumbitFormApi form) {
        User user = (User) getCurrentIUser();
        //杭州大会活动有效期判断
        List<String> lstSkuId = null;
        if (form.getQty() > 0) {
            lstSkuId = new ArrayList<>();
            lstSkuId.add(form.getSkuId());
        } else {
            lstSkuId = form.getSkuIds();
        }

        List<String> promotionSkuIds = new ArrayList<>();
        List<String> promotionSkuCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(lstSkuId)) {
            for (String skuId : lstSkuId) {
                Sku sku = skuMapper.selectByPrimaryKey(skuId);
                List ret = promotionSkusService.getPromotionSkus(sku.getSkuCode());
                if (CollectionUtils.isNotEmpty(ret)) {
                    promotionSkuIds.add(skuId);
                    promotionSkuCodes.add(sku.getSkuCode());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(promotionSkuCodes)) {
            for (String promotionSkuCode : promotionSkuCodes) {
                //根据商品sku获取活动信息
                List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService
                        .selectEffectiveBySkuCode(promotionSkuCode);
                if (CollectionUtils.isEmpty(promotionBaseInfos)) {
                    continue;
                }
                if (!promotionBaseInfos.get(0).getpType().contains("meeting")) {
                    continue;
                }
                String pCode = promotionBaseInfos.get(0).getpCode();
                //查询活动配置信息
                Map configType = new HashMap();
                configType.put("configType", promotionBaseInfos.get(0).getpType());
                List<PromotionConfig> promotionConfigs = promotionConfigService
                        .selectListByConfigType(configType);
                //活动金额上限默认不设限
                String top_value = "10000000";

                if (null != promotionConfigs && promotionConfigs.size() > 0) {
                    for (PromotionConfig promotionConfig : promotionConfigs) {
                        if ("top_value".equals(promotionConfig.getConfigName())
                                && null != promotionConfig.getConfigValue()
                                && !"".equals(promotionConfig.getConfigValue())) {
                            top_value = promotionConfig.getConfigValue();
                        }

                    }
                }
                //杭州大会活动有效期判断
                PromotionBaseInfo pb = promotionBaseInfos.get(0);
                Date effectFrom = pb.getEffectFrom();
                if (effectFrom.after(new Date())) {

                    return "活动未开始。";
                }
                if (pb.getEffectTo().before(new Date())) {

                    return "活动已结束。";
                }

                //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
                boolean hasEnoughStock = promotionSkusService.hasEnoughStock(promotionSkuIds);
                if (!hasEnoughStock) {

                    return "活动商品已售罄。";
                }

                //杭州大会总价值上线判断，2018-10-07 by Chenpeng
                boolean isOverUpValue;
                if (form.getQty() > 0) {
                    //直接下单
                    isOverUpValue = isOverUpValue(form.getSkuId(), user, form.getQty(),
                            Integer.valueOf(top_value));
                } else {
                    isOverUpValue = isOverUpValue(promotionSkuCodes, user, Integer.valueOf(top_value), pCode);
                }
                if (isOverUpValue) {

                    return "购买总额已超过活动上限。";
                }
                //大会活动增加商品购买次数限制
                Map<String, String> map;
                if (form.getQty() > 0) {
                    map = isOverRestriction(form.getSkuId(), user, form.getQty(), pCode);
                } else {
                    map = isOverRestriction(promotionSkuCodes, user, pCode);
                }

                if ("true".equals(map.get("stock"))) {

                    return "商品库存不足";
                }

                if ("true".equals(map.get("restriction"))) {

                    return "商品购买数量超过活动规定数量";
                }

                PromotionConfig config = promotionConfigService
                        .selectBySkuCodeAndConfigName(promotionSkuCode, "isCode");
                String configValue = config.getConfigValue();

                //根据skuCode判断是否有需要邀请码的商品
                if (configValue != null && configValue.equals("on")) {
                    //判断当前用户是否绑定邀请码
                    String code = promotionInviteCodeService
                            .selectInviteCode(user.getCpId().toString(), pCode);
                    if (null == code || ("").equals(code)) {

                        String inviteCode = form.getInviteCode();
                        if (StringUtils.isNotEmpty(inviteCode)) {
                            inviteCode.toLowerCase();
                            int isValidate = promotionInviteCodeService.validate(inviteCode, pCode);

                            if (isValidate < 0) {

                                return "邀请码无效或失效。";
                            }
                            //绑定邀请码
                            promotionInviteCodeService
                                    .updateRelation(inviteCode, user.getCpId().toString(), pCode);
                        }

                    }
                }
            }
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/order/submit/freed")
    public ResponseObject<Boolean> submitFreed(String tranCode) {
        User user = (User) getCurrentIUser();
        // 订单已经下了则不再解锁了
        if (!customerProfileService.isNew(String.valueOf(user.getCpId()))) {
            return new ResponseObject<>(false);
        }
        pieceCacheService.unLockFreePayMember(tranCode, user.getCpId());
        return new ResponseObject<>(true);
    }

    /**
     * 把订单状态更新为PENDING
     *
     * @param mainOrderId 主订单号
     * @return 更新结果
     */
    @ResponseBody
    @RequestMapping(value = "/order/pending", method = POST)
    public ResponseObject<Boolean> pending(@RequestParam String mainOrderId) {
        final MainOrderVO mainOrderVO = mainOrderService.loadVO(mainOrderId);
        if (mainOrderVO == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "主订单不存在");
        }
        if (!StringUtils.equals(mainOrderVO.getBuyerId(), getCurrentIUser().getId())) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "买家信息不匹配");
        }
        if (mainOrderVO.getStatus() != MainOrderStatus.SUBMITTED) {
            log.warn("订单 {} 状态 {} 不正确, 无法变更PENDING", mainOrderVO.getOrderNo(),
                    mainOrderVO.getStatus());
            return new ResponseObject<>(false);
        }
        return new ResponseObject<>(mainOrderService.pending(mainOrderVO));
    }

    /**
     * 手动调用定时任务处理PENDING
     *
     * @return 错误信息
     */
    @ResponseBody
    @RequestMapping(value = "/order/action/handlerPending", method = POST)
    public ResponseObject<Map<String, String>> handlePending() {
        final List<MainOrder> orders = mainOrderService.listPending(5);
        final Map<String, String> errors = new HashMap<>();
        for (MainOrder o : orders) {
            try {
                cashierService.handlePending(o);
            } catch (Exception e) {
                log.error("订单 {} 处理PENDING状态出错", o.getOrderNo(), e);
                errors.put(o.getOrderNo(), e.getMessage()	);
            }
        }
        return new ResponseObject<>(errors);
    }

    /**
     * 订单提交
     */
    @ResponseBody
    @RequestMapping(value = "/order/submit", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "订单提交", notes = "订单提交后返回详情相关的vo", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    @Token(save = true)
    public ResponseObject<OrderVOApi> submit(@RequestBody @Validated OrderSumbitFormApi form,
                                             HttpServletRequest request) {

        OrderVOApi orderVOApi;
        String tranCode = form.getTranCode();
        User user = (User) getCurrentIUser();
        boolean isCutInLIne = false;
        // 拼团且是参团
        if (promotionCheck(form, tranCode)) {
            String userId = getCurrentIUser().getId();
            boolean hasUnPaidOrder = pgPromotionServiceAdapter.hasUnPaidOrder(tranCode, userId);
            if (hasUnPaidOrder) {
                throw new BizException(GlobalErrorCode.PIECE_ERROR, "您有未付款的拼团订单, 请先完成订单");
            }
            // 校验参团数
            if (!pieceCacheService.check(tranCode)) {
                if (pgPromotionServiceAdapter.isInCutLineTimeByTran(tranCode)) {
                    // 插队拼团，允许放行
                    isCutInLIne = true;
                } else {
                    log.error("团 {} 拼团人数已满", tranCode);
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团人数已满");
                }
            }
        }
        /**
         * 大会活动订单提交前相关校验
         */
        String moreInfo = this.checkMeetingPromotion(form);
        if (null != moreInfo && !"".equals(moreInfo)) {
            ResponseObject result = new ResponseObject<>();
            result.setMoreInfo(moreInfo);
            return result;
        }

        List<Sku> skuList;

        if (form.getQty() > 0) {
            skuList = skuMapper.selectByIds(new String[]{form.getSkuId()});
        } else {
            List<String> skuIdList = form.getSkuIds();
            String[] array = new String[skuIdList.size()];
            String[] skuArray = skuIdList.toArray(array);
            skuList = skuMapper.selectByIds(skuArray);
        }

        //校验白名单
//    if(user != null){
//      this.checkIsWhiteSubmit(skuList,user.getCpId());
//    }
        // 主订单
        MainOrder mainOrder = null;
        Address address = null;

        //校验发票信息
        Optional<? extends BaseBill> bill = Optional.absent();
        Optional<BillFormType> billFormType = Optional.fromNullable(form.getBillFormType());
        if (null != form.isNeedInvoice() && form.isNeedInvoice()) {
            if (billFormType.isPresent()) {
                switch (billFormType.get()) {
                    case ADDED:
                        bill = Optional.fromNullable(form.getAddedTaxBill());
                        break;
                    case PERSONAL:
                        bill = Optional.fromNullable(form.getPersonalBill());
                        break;
                    case COMPANY:
                        bill = Optional.fromNullable(form.getCompanyBill());
                        break;
                }
                if (!bill.isPresent()) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "传递的发票表单类型不正确");
                } else {
                    Class<?> group = bill.get().getType().equals(BillType.ELECTRONIC) ?
                            ElectronicGroup.class : NormalGroup.class;
                    Set<ConstraintViolation<BaseBill>> ret = validator.validate(bill.get(), group);
                    if (CollectionUtils.isNotEmpty(ret)) {
                        String info = ValidationUtils.extractMessage(ret);
                        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, info);
                    }
                }
            } else {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "未传递发票表单的类型");
            }
        }

        // 特权商品提交订单需要检查是否有特权码
        if ("1".equals(form.getIsPrivilege())) {
            boolean flag = false;
            List<PrivilegeCodeVO> promotions = privilegeCodeService.selectByUserId(user.getId());
            for (PrivilegeCodeVO vo : promotions) {
                if (vo.getValidTo().compareTo(new Date()) > 0 && vo.getRemainQty() > 0) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您没有可使用的特权码，无法购买活动商品。");
            }
        }

        // 汇购网特殊要求，订单提交不会传递addressid过来，而会传consignee，phone,zoneId,street信息，zoneid对应的是汇购网的zone
        // 通过判断这些信息是否有存在的address，如果有则用存在的address，否则自动创建一个address
        if (StringUtils.isNotEmpty(form.getAddressId())) {
            address = addressService.load(form.getAddressId());
        } else {
            if (StringUtils.isNotEmpty(form.getZoneId())) {
                Zone zone = zoneService.load(form.getZoneId());
                address = new Address();
                address.setConsignee(form.getConsignee());
                address.setZoneId(zone.getId());
                address.setStreet(form.getStreet());
                address.setPhone(form.getPhone());
                address.setCommon(false);
                address.setIsDefault(false);
                address = addressService.saveUserAddress(address, true);
                form.setAddressId(address.getId());
            }
        }

        Shop sh = shopService.loadRootShop();
        String realShopId = sh.getId();

        Set<String> skuIds = new HashSet<>();
        for (Sku sku : skuList) {
            skuIds.add(sku.getId());
        }

        List<CartItemVO> cartItems;
        PromotionInfo promotionInfo = form.getPromotionInfo();
        if (promotionInfo != null) {
            promotionInfo.setCutInLine(isCutInLIne);
        }
        PromotionType promotionType = promotionInfo == null ? null : promotionInfo.getPromotionType();
        if (form.getQty() > 0) {
            // 直接下单,不走购物车流程
            cartItems = cartService.checkout(skuIds, form.getQty(), realShopId, promotionType);
        } else if (skuIds.size() > 0) {
            // 以sku结算
            cartItems = cartService.checkout(skuIds, realShopId, promotionType);
        } else {
            // 以店铺结算
            cartItems = cartService.checkout(realShopId, promotionType);
        }
        Integer feshManCount = 0;
        for (CartItemVO item : cartItems) {
            //新人专区商品限购数量校验
            List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(item.getProductId());
            if (CollectionUtil.isNotEmpty(freshManProductVos)) {
                feshManCount = feshManCount + item.getAmount();
            }
        }

        for (int i = 0; i <cartItems.size(); i++) {
            if(cartItems.get(i).getProduct().isFreshmanProduct()==true) {
                freshManService.freshmanLimit(feshManCount, user);
            }
        }

        checkSfInventory(form, skuList, cartItems);

        if (StringUtils.isNotBlank(form.getOrderId())) { // 多次提交
            mainOrder = mainOrderService.load(form.getOrderId());
        } else {

            // 下单计算优惠券逻辑
            Shop rootShop = shopService.loadRootShop();
            List<String> usingCoupons = form.getUsingCoupons();

            UserSelectedProVO userSelectedProVO = UserSelectedProVO.build(rootShop, usingCoupons);

            if ((promotionType != null && promotionType != PromotionType.COUPON)
                    && CollectionUtils.isNotEmpty(usingCoupons)) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "优惠券与活动不能同时选择");
            }
            //如果是权益升级订单
            PromotionType promotionFrom = form.getPromotionFrom();
            if (PromotionType.VIP_RIGHT.equals(promotionFrom)) {
                promotionInfo = new VipRightPromotionInfo("", PromotionType.VIP_RIGHT);

            }

            OrderAddress oa = getOrderAddress(form);
            if (form.isDirectBuy()) {//直接下单
                //下单业务处理（直接下单）
                mainOrder = mainOrderService
                        .submitBySkuId(form.getSkuId(), oa, form.getRemark(), userSelectedProVO,
                                address,
                                true, form.getQty(), realShopId, null, form.isUseDeduction(),
                                form.getIsPrivilege(), form.isNeedInvoice(),
                                form.getSelectPoint(), form.getSelectCommission(), form.getUpLine(), promotionInfo);
            } else if (form.isCartBuy()) {   // 购物车下单
                mainOrder = mainOrderService
                        .submitBySkuIds(form.getSkuIds(), oa, form.getShopRemarks(),
                                userSelectedProVO, promotionType, address, true, realShopId, "",
                                form.isUseDeduction(), form.isNeedInvoice(),
                                form.getSelectPoint(), form.getSelectCommission(), form.getUpLine());
            } else {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单提交参数不正确");
            }

        }

        OrderUserDevice orderUserDevice = buildOrderUserDevice(request);
        orderUserDevice.setOrderId(mainOrder.getId());
        orderUserDevice.setUserId(mainOrder.getBuyerId());
        orderUserDeviceService.save(orderUserDevice);

        orderVOApi = new OrderVOApi();

        MainOrderVO mainOrderVO = mainOrderService.loadVO(mainOrder.getId());
        if (mainOrderVO == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "订单生成失败");
        }

        //存储发票信息
        if (bill.isPresent()) {
            bill.get().setOrderId(mainOrderVO.getId());
            if (billFormType.isPresent()) {
                switch (billFormType.get()) {
                    case PERSONAL:
                        billService.makePersonalBill((PersonalBill) bill.get());
                        break;
                    case COMPANY:
                        billService.makeCompanyBill((CompanyBill) bill.get());
                        break;
                    case ADDED:
                        billService.makeAddedTaxBill((AddedTaxBill) bill.get());
                        break;
                }
            }
        }

        orderVOApi.setMainOrderVO(mainOrderVO);
        BigDecimal goodsFee = BigDecimal.ZERO;
        BigDecimal totalFee = BigDecimal.ZERO;
        BigDecimal logisticsFee = BigDecimal.ZERO;
        BigDecimal hadPayFee = BigDecimal.ZERO;
        for (OrderVO orderVO : mainOrderVO.getOrders()) {
            goodsFee = goodsFee.add(orderVO.getGoodsFee());
            totalFee = totalFee.add(orderVO.getTotalFee());
            if (!Objects.equals(orderVO.getOrderType(), OrderSortType.PIECE)
                    ||(Objects.equals(orderVO.getOrderType(), OrderSortType.PIECE)&&isCutInLIne)) {
                //（当前订单金额不包含拼团订单）应付金额 = goodfee - discountfee +（德分+红包分）/10 + 收益
                BigDecimal orderHadPay =
                        orderVO.getGoodsFee().
                                subtract(orderVO.getDiscountFee()).
                                add((orderVO.getPaidPoint().add(orderVO.getPaidPointPacket())).divide(new BigDecimal(10))).
                                add(orderVO.getPaidCommission());
                hadPayFee = hadPayFee.add(orderHadPay);
            }
            logisticsFee = logisticsFee.add(orderVO.getLogisticsFee());
            if (Objects.equals(orderVO.getOrderType(), OrderSortType.PIECE)) {
                orderVO.setOrderType(OrderSortType.PIECE_GROUP);
            }

        }

        // 订单买家信息，当自提方式无法带出订单用户信息时，取订单买家信息
        if (mainOrderVO.getOrders() != null && mainOrderVO.getOrders().size() > 0) {
            String buyerId = mainOrderVO.getOrders().get(0).getBuyerId();

            // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
            AuditRule auditRule = auditRuleService
                    .selectByOrderId(mainOrderVO.getOrders().get(0).getId());
            if (auditRule != null) {
                mainOrderVO.getOrders().get(0).setAuditType(auditRule.getAuditType());
            }

            User buyer = userService.load(buyerId);
            orderVOApi.setBuyer(buyer);
        }
        orderVOApi.setGoodsFee(goodsFee);
        orderVOApi.setTotalFee(totalFee);
        orderVOApi.setLogisticsFee(logisticsFee);

        //判断是否是白人，是白人并且消费满500 更新orderheader表

        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        log.info("当前登入人cpid:{},是否有身份:{}",user.getCpId(),hasIdentity);

        if (!hasIdentity) {
            BigDecimal alreadyPay = freshmanRecommendService.countUserSpending(user.getCpId());
            log.info("当前登入人累计消费金额{}元,当前订单需支付金额{}元",alreadyPay,hadPayFee);

            Map pointConfig = freshManService.getPointConfig("2");
            String configAmountStr = (String) pointConfig.get("amount");
            int less = alreadyPay.compareTo(new BigDecimal(configAmountStr));
            int bigger = (alreadyPay.add(hadPayFee)).compareTo(new BigDecimal(configAmountStr));
            log.info("累计金额与配置金额{}比较结果:{}",configAmountStr,less);
            log.info("累计金额加当前订单金额与配置金额{}比较结果:{}",configAmountStr,bigger);

            //大于等于500
            if (less == -1 && bigger >= 0) {
                for (int i = 0; i < mainOrderVO.getOrders().size(); i++) {
                    //新人升级只更新最后一个子订单
                    if (i == mainOrderVO.getOrders().size() - 1) {
                        Integer joinType = orderHeaderService.selectJoinTypeByOrderNo(mainOrderVO.getOrders().get(i).getOrderNo());
                        log.info("最后一个子订单orderNo:{}的joinType是{}",mainOrderVO.getOrders().get(i).getOrderNo(),joinType);

                        //如果是vip升级的订单 则不更新
                        if (null == joinType || 0 == joinType) {
                            orderHeaderService.updateJoinTypeByOrderNo(3, mainOrderVO.getOrders().get(i).getOrderNo());
                            log.info("新人订单金额满更新orderHeader表 joinType=3，OrderNo=" + mainOrderVO.getOrders().get(i).getOrderNo());
                        } else if (5==joinType){

                            //当前订单是新人礼单 并且也满足满五百升级 joinType=6
                            orderHeaderService.updateJoinTypeByOrderNo(6, mainOrderVO.getOrders().get(i).getOrderNo());
                            log.info("新人礼并且满500更新 orderHeader表 joinType=6，OrderNo=" + mainOrderVO.getOrders().get(i).getOrderNo());

                        }else if (2 != joinType) {
                            orderHeaderService.updateJoinTypeByOrderNo(3, mainOrderVO.getOrders().get(i).getOrderNo());
                            log.info("新人订单金额满更新orderHeader表 joinType=3，OrderNo=" + mainOrderVO.getOrders().get(i).getOrderNo());
                        }
                    }
                }
            }
        }

        return new ResponseObject<>(orderVOApi);
    }

    private boolean promotionCheck(@Validated @RequestBody OrderSumbitFormApi form, String tranCode) {
        return PromotionType.isPiece(form.getPromotionFrom()) && StringUtils.isNotBlank(tranCode);
    }

    private boolean checkIsWhiteSubmit(List<Sku> skus, Long cpId) {
        if (skus != null && !skus.isEmpty()) {
            Set<Boolean> b = new HashSet<>();
            for (Sku s : skus) {
                if (s != null) {
                    boolean isWhite = promotionBaseInfoService.checkIsWhiteBySkuId(s.getSkuCode(), cpId);
                    b.add(isWhite);
                }
            }
            //对多个sku做校验
            if (b != null && 1 == b.size() && b.contains(false)) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, WHITELIST_CONFIRE_ALERT_CONTENT);
            }
        }
        return true;
    }

    /**
     * 订单详情
     */
    @ResponseBody
    @RequestMapping(value = "/order/{id}", method = POST)
    @ApiOperation(value = "订单详情", notes = "订单详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<OrderVOEx> view(@PathVariable String id, HttpServletRequest req) {
        OrderVO order = orderService.loadVO(id);

        if (order == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单[id=" + id + "]不存在，或者没有权限访问");
        }

        // 设置物流公司官网
        for (LogisticsCompany logisticsCompany : LogisticsCompany.values()) {
            if (logisticsCompany.getName().equals(order.getLogisticsCompany())) {
                order.setLogisticsOfficial(logisticsCompany.getUrl());
            }
        }
        //TODO 兼容老版本处理
        if ("顺丰".equals(order.getLogisticsCompany()) || "顺丰快递".equals(order.getLogisticsCompany())
                || "SF_EXPRESS".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.sf-express.com/");
        } else if ("圆通".equals(order.getLogisticsCompany()) || "圆通快递"
                .equals(order.getLogisticsCompany()) || "YTO".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.yto.net.cn/");
        } else if ("申通".equals(order.getLogisticsCompany()) || "STO"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.sto.cn/");
        } else if ("中通".equals(order.getLogisticsCompany()) || "ZTO"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.zto.cn/");
        } else if ("百世汇通".equals(order.getLogisticsCompany()) || "BESTEX"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.htky365.com/");
        } else if ("韵达".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.yundaex.com/");
        } else if ("天天".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.ttkdex.com/");
        } else if ("全峰".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.qfkd.com.cn/");
        } else if ("邮政EMS".equals(order.getLogisticsCompany()) || "中国邮政"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.ems.com.cn/");
        } else {
            order.setLogisticsOfficial("");
        }

        Shop shop = shopService.load(order.getShopId());
        if (shop != null) {
            order.setShopName(shop.getName());
        }
        User currUser = (User) getCurrentIUser();
        String userId = getCurrentUser().getId();
        if (userId.equals(order.getBuyerId())) {
            order.setFromType("in");
        } else {
            order.setFromType("out");
        }
        String imgUrl = "";
        List<OrderItemVO> orderItemVOs = new ArrayList<>();
        BigDecimal orderAmount = BigDecimal.ZERO;
        BigDecimal cutDown = BigDecimal.ZERO;

        IdentityVO identityVO = DynamicPricingUtil.getIdentityVO(currUser.getCpId());
        final boolean isProxy = identityVO != null && identityVO.getIsProxy();
        PromotionType usingPromotionType = null;
        for (final OrderItem item : order.getOrderItems()) {
            // 重新计算orderItem中的价格
            DynamicPricingUtil.rePricing(item, currUser);
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
            orderAmount = orderAmount.add(BigDecimal.valueOf(item.getAmount()));
            Sku sku = item.getSku();
            cutDown = cutDown.add(java.util.Optional.ofNullable(sku).map(input -> {
                BigDecimal val = input.getPoint().multiply(BigDecimal.valueOf(item.getAmount()));
                if (isProxy) {
                    val = val.add(input.getServerAmt().multiply(BigDecimal.valueOf(item.getAmount())));
                }
                return val;
            }).orElse(BigDecimal.ZERO));
            OrderItemVO orderItemVO = new OrderItemVO();
            BeanUtils.copyProperties(item, orderItemVO);
            Commission commission = unionService.loadByOrderItemAndUserId(item.getId(), userId);
            if (commission != null) {
                orderItemVO.setCommission(commission.getFee());
            } else {
                orderItemVO.setCommission(new BigDecimal("0"));
            }

            orderItemVO.setProductUrl(urlHelper.genProductUrl(orderItemVO.getProductId()));

            usingPromotionType = orderItemVO.getPromotionType();
            if (usingPromotionType != null) {
                ProductVO productVO = (ProductVO) orderItemVO.getProduct();
                if (usingPromotionType == PromotionType.INSIDE_BUY) {
                    // 折后价
                    productVO.setInsideBuyPrice(orderItemVO.getDiscountPrice());
                } else if (usingPromotionType == PromotionType.FULLCUT) {
                    productVO.setFullCutPrice(orderItemVO.getDiscountPrice());
                }
            }

            //加自营标签
            Product product = productMapper.getSupIdById(item.getProductId());
            if (product != null) {
                orderItemVO.setSelfOperated(product.getSelfOperated());
                item.setSelfOperated(product.getSelfOperated());
            }

            //是否是新人专区商品
            FreshManProductVo f =
                    this.freshManService.selectFreshmanProductById(item.getProductId());
            if (f != null && StringUtils.isNotBlank(f.getProductId()))
                orderItemVO.setFreshmanProduct(true);
            else {
                orderItemVO.setFreshmanProduct(false);
            }

            orderItemVOs.add(orderItemVO);
        }
        order.setCutDown(cutDown);
        order.setAmount(orderAmount);
        order.setOrderItemVOs(orderItemVOs);
        order.setImgUrl(imgUrl);

        // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
        AuditRule auditRule = auditRuleService.selectByOrderId(order.getId());
        if (auditRule != null) {
            order.setAuditType(auditRule.getAuditType());
        }

        if (order.getOrderAddress() != null) {
            getSysteRegion(order);
        }

        // 分佣
        List<Commission> commissions = unionService.listByOrderId(order.getId());
        BigDecimal cmFee = BigDecimal.ZERO;
        for (Commission cm : commissions) {
            cmFee = cmFee.add(cm.getFee());
        }
        order.setCommissionFee(cmFee);
        if (order.getDiscountFee() == null) {
            order.setDiscountFee(BigDecimal.ZERO);
        }

        List<CouponInfoVO> orderCoupons = cashierService
                .loadCouponInfoByOrderNo(order.getOrderNo());
        order.setOrderCoupons(orderCoupons);

        OrderVOEx orderEx = new OrderVOEx(order, orderService.findOrderFees(order));
        BigDecimal discountFee = ObjectUtils
                .defaultIfNull(orderEx.getDiscountFee(), BigDecimal.ZERO);
        orderEx.setDefDelayDate(defDelayDate);
        orderEx.setRefundableFee(findRefundableFee(orderEx, orderEx.getStatus()));

        orderEx.setShowRefundBtn(orderService.checkAndUpdateCapableToRefund(orderEx));
        orderEx.setShowCancelRefundBtn(false);

        List<OrderRefund> orderRefundList = orderRefundService.listByOrderId(orderEx.getId());
        if (orderRefundList != null && orderRefundList.size() > 0) {
            OrderRefund refund = orderRefundList.get(0);
            orderEx.setRefundStatus(refund.getStatus().toString());
            if (refund.getStatus() == OrderRefundStatus.SUBMITTED
                    || refund.getStatus() == OrderRefundStatus.SUCCESS
                    || refund.getStatus() == OrderRefundStatus.CANCELLED) {
                orderEx.setShowCancelRefundBtn(true);
            }
        }

        orderEx.setCheckingAt(order.getSucceedAt());
        if ((orderEx.getStatus() == OrderStatus.PAID ||
                orderEx.getStatus() == OrderStatus.SHIPPED ||
                (orderEx.getStatus() == OrderStatus.SUCCESS && !orderEx.getIs30DaysOut()) ||
                orderEx.getStatus() == OrderStatus.COMMENT) && (orderRefundList == null
                || orderRefundList.size() == 0)) {
            orderEx.setShowRefundBtn(true);
        }

        boolean isCommented = orderService.isOrderComment(order.getId(), order.getBuyerId());
        order.setCommented(isCommented);

        //TODO buyerName  from user
        orderEx.setBuyerName(userService.loadByAdmin(orderEx.getBuyerId()) == null ? "无"
                : userService.loadByAdmin(orderEx.getBuyerId()).getName());
        orderEx.setBuyerImgUrl(userService.loadByAdmin(orderEx.getBuyerId()) == null ? ""
                : userService.loadByAdmin(orderEx.getBuyerId()).getAvatar());

        BigDecimal logisDiscount = orderEx.getLogisticDiscount();
        BigDecimal commissionLogisticsDiscount = orderEx.getCommissionLogisticsDiscount();
        if (logisDiscount.compareTo(BigDecimal.ZERO) != 0) {
            orderEx.setLogisticsFee(logisDiscount);
        } else {
            if (commissionLogisticsDiscount.compareTo(BigDecimal.ZERO) > 0) {
                orderEx.setLogisticsFee(commissionLogisticsDiscount.add(orderEx.getLogisticsFee()));
            }
        }

        if (OrderSortType.isPiece(order.getOrderType())) {
            String orderNo = order.getOrderNo();
            PromotionOrderDetail detail = pgPromotionServiceAdapter.loadPieceDetailByOrderNo(orderNo);
            orderEx.setOrderType(OrderSortType.PIECE_GROUP);
            if (detail != null) {
                String tranCode = detail.getPiece_group_tran_code();
                PGTranInfoVo tranInfo = pgPromotionServiceAdapter.loadPGTranInfo(tranCode);
                // 拼团人数详情
                List<PromotionGroupDetailVO> promotionGroupDetailVOS = pgPromotionServiceAdapter
                        .listGroupDetailByTranCode(detail.getPiece_group_tran_code());
                List<PromotionPgMemberInfoVO> promotionPgMemberInfoVOs = promotionPgMemberInfoService
                        .selectMemberInfoBycpId(tranCode, getCurrentUser().getCpId());
                int isGroupHead = 0;
                for (PromotionPgMemberInfoVO promotionPgMemberInfoVO :
                        promotionPgMemberInfoVOs) {
                    if (getCurrentUser().getCpId().toString().equals(promotionPgMemberInfoVO.getMemberId())) {
                        isGroupHead = 1;
                    }
                }
                if (tranInfo != null && CollectionUtils.isNotEmpty(promotionGroupDetailVOS)) {
                    int totalMember = pgPromotionServiceAdapter.countPgMember(tranCode);
                    int beginCount = pgPromotionServiceAdapter.countCurrPgMember(tranCode);
                    ExecStatus tranStatus = PieceStatus.fromCode(tranInfo.getPieceStatus()).getRet();
                    orderEx.setPieceGroup(
                            new PieceGroupVO(detail, totalMember, beginCount, tranStatus, promotionGroupDetailVOS,
                                    isGroupHead));
                }
            }
        }

        // 把展示的总金额除掉活动优惠
        //杭州大会增加订单详情赠品信息 2018-10-08
        convretOrderVO(orderEx);
        orderEx.setDiscountDetailStr(buildDetailVOList(orderEx, usingPromotionType));
        //杭州大会增加订单详情赠品信息 2018-10-08
        return new ResponseObject<>(orderEx);
    }

    /**
     * 获得当前订单的收货地址和联系方式
     */
    private OrderAddress getOrderAddress(OrderSumbitFormApi form) {
        OrderAddress oa = new OrderAddress();
        if ("1".equals(form.getIspickup())) {
            oa = null;
        } else {
            if (StringUtils.isNotEmpty(form.getAddressId())) {
                Address address = addressService.load(form.getAddressId());
                if (address == null) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "地址不存在");
                }
                BeanUtils.copyProperties(address, oa);
            } else {
                form.setStreet(form.getStreet().trim());
                BeanUtils.copyProperties(form, oa);
            }
        }
        return oa;
    }

    /**
     * 构建客户端优惠详情列表
     */
    private List<DiscountDetailVO> buildDetailVOList(OrderVOEx order, PromotionType usingPromotionType) {
        Set<DiscountDetailVO> ret = Sets.newTreeSet();
        ret.add(new DiscountDetailVO(CUT_DOWN, order.getCutDown()));
        ret.add(new DiscountDetailVO(LOGISTICS, order.getLogisticsFee()));
        ret.add(new DiscountDetailVO(LOGISTICS_DISCOUNT, order.getLogisticDiscount()));
        if (usingPromotionType != null) {
            ret.add(new DiscountDetailVO(usingPromotionType, order.getPromotionDiscount()));
        }
        BigDecimal paidPointPacket = order.getPaidPointPacket();
        if (paidPointPacket != null && paidPointPacket.signum() > 0) {
            ret.add(new DiscountDetailVO(POINT_PACKET, paidPointPacket.divide(
                    BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN)));
        }
        return Lists.newArrayList(ret);
    }

    private OrderUserDevice buildOrderUserDevice(HttpServletRequest request) {
        OrderUserDevice device = new OrderUserDevice();
        device.setUserAgent(request.getHeader("User-Agent"));
        device.setClientIp(request.getRemoteAddr());
        device.setServerIp(request.getServerName());
        return device;
    }

    /**
     * 订单确认接口，根据传入的sku等信息返回订单商品项目、实付金额、优惠信息等
     */
    @ResponseBody
    @RequestMapping(value = "/order/confirm", method = POST)
    @ApiOperation(value = "订单确认接口", notes = "根据传入的sku等信息返回订单商品项目、实付金额、优惠信息等,其中cartItemMap代表订单的商品集合，shopPricingResult代表实际支付金额数据", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Map<String, Object>> confirm(
            @RequestBody @ModelAttribute CartNextForm form) {
        User user = (User) getCurrentIUser();
        //杭州大会活动有效期判断
        List<String> promotionSkuCodes = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(form.getSkuId())) {
            for (String skuId : form.getSkuId()) {
                Sku sku = skuMapper.selectByPrimaryKey(skuId);
                //判断白名单
//          if(sku != null){
//              this.checkIsWhiteConfirm(sku.getSkuCode(),user.getCpId());
//          }

                List ret = promotionSkusService.getPromotionSkus(sku.getSkuCode());
                if (CollectionUtils.isNotEmpty(ret)) {
                    promotionSkuCodes.add(sku.getSkuCode());
                }
            }
        }

        //大会活动是否需要邀请码
        boolean isCode = false;
        //用户是否已经绑定邀请码
        boolean isBoundingInviteCode = false;
        if (CollectionUtils.isNotEmpty(promotionSkuCodes)) {
            for (String promotionSkuCode : promotionSkuCodes) {
                //根据商品sku获取活动信息
                List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService
                        .selectEffectiveBySkuCode(promotionSkuCode);
                if (CollectionUtils.isEmpty(promotionBaseInfos)) {
                    continue;
                }
                //只校验大会活动的商品
                if (!promotionBaseInfos.get(0).getpType().contains("meeting")) {
                    continue;
                }
                String pCode = promotionBaseInfos.get(0).getpCode();
                //查询活动配置信息
                Map<String, String> configType = new HashMap<>();
                configType.put("configType", promotionBaseInfos.get(0).getpType());
                List<PromotionConfig> promotionConfigs = promotionConfigService
                        .selectListByConfigType(configType);
                //活动金额上限，默认不设限
                String top_value = "10000000";

                if (null != promotionConfigs && promotionConfigs.size() > 0) {
                    for (PromotionConfig promotionConfig : promotionConfigs) {
                        if ("top_value".equals(promotionConfig.getConfigName())
                                && null != promotionConfig.getConfigValue()
                                && !"".equals(promotionConfig.getConfigValue())) {
                            top_value = promotionConfig.getConfigValue();
                        }

                        if ("isCode".equals(promotionConfig.getConfigName())
                                && null != promotionConfig.getConfigValue()
                                && !"".equals(promotionConfig.getConfigValue())) {
                            if ("on".equals(promotionConfig.getConfigValue())) {
                                //true表示需要邀请码
                                isCode = true;
                            }
                        }
                    }
                }
                //杭州大会活动有效期判断
                PromotionBaseInfo pb = promotionBaseInfos.get(0);
                Date effectFrom = pb.getEffectFrom();
                if (effectFrom.after(new Date())) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("活动未开始。");
                    return result;
                }
                if (pb.getEffectTo().before(new Date())) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("活动已结束。");
                    return result;
                }

                //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
                List<String> promotionSkuIds = promotionSkusService.getPromotionSkuIds(form.getSkuId());
                boolean hasEnoughStock = promotionSkusService.hasEnoughStock(promotionSkuIds);
                if (!hasEnoughStock) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("活动商品已售罄。");
                    return result;
                }


                //杭州大会总价值上线判断，2018-10-07 by Chenpeng
                boolean isOverUpValue;
                if (form.getQty() > 0) {
                    //直接下单
                    isOverUpValue = isOverUpValue(form.getSkuId().get(0), user, form.getQty(),
                            Integer.valueOf(top_value));
                } else {
                    isOverUpValue = isOverUpValue(promotionSkuCodes, user, Integer.valueOf(top_value), pCode);
                }
                if (isOverUpValue) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("购买总额已超过活动上限。");
                    return result;
                }
                //大会活动增加商品购买次数限制

                Map<String, String> map;
                if (form.getQty() > 0) {
                    map = isOverRestriction(form.getSkuId().get(0), user, form.getQty(), pCode);
                } else {
                    map = isOverRestriction(promotionSkuCodes, user, pCode);
                }

                if ("true".equals(map.get("stock"))) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("商品库存不足");
                    return result;
                }

                if ("true".equals(map.get("restriction"))) {
                    ResponseObject result = new ResponseObject<>();
                    result.setMoreInfo("商品购买数量超过活动规定数量");
                    return result;
                }


                //查询用户有没有绑定邀请码
                String invateCode = promotionInviteCodeService
                        .selectInviteCode(user.getCpId().toString(), pCode);
                //如果已经绑定邀请码，则不需要输入邀请码
                if (StringUtils.isNotEmpty(invateCode)) {
                    isBoundingInviteCode = true;
                }
            }

        }

        Map<String, Object> result = new HashMap<>();

        boolean hadJoined = pieceGroupPayCallBackService
                .hadJoined(form.getTranCode(), getCurrentUser().getCpId().toString());
        if (hadJoined) {
            log.info("重复参团校验");
            throw new BizException(GlobalErrorCode.DUPLICATED_PIECE, "请勿重复参团");
        }

        String promotionProductId = form.getPromotionProductId();

        PromotionType promotionType = form.getPromotionFrom();
        String promotionId = form.getPromotionId();
        if (promotionType == null && StringUtils.isNoneBlank(promotionId)) {
            Promotion promotion = promotionService.load(promotionId);
            if (promotion != null) {
                promotionType = promotion.getPromotionType();
            }
        }

        List<CartItemVO> cartItems = new ArrayList<>();
        String realShopId = shopService.loadRootShop().getId();
        if (form.getQty() > 0) {
            // 直接下单,不走购物车流程
            Set<String> skuIds = new LinkedHashSet<>(form.getSkuId());
            cartItems = cartService.checkout(skuIds, form.getQty(), realShopId, promotionType);//qty直接下单购买的数量
        } else if (form.getSkuId() != null && form.getSkuId().size() > 0) {
            // 以sku结算
            Set<String> skuIds = new LinkedHashSet<>(form.getSkuId());
            cartItems = cartService.checkout(skuIds, realShopId, promotionType);
        } else if (StringUtils.isNotEmpty(realShopId)) {
            // 以店铺结算
            cartItems = cartService.checkout(realShopId, promotionType);
        }

        if (CollectionUtils.isEmpty(cartItems)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请选择要结算的商品");
        }

        // 购物车中所有商品id
        List<String> productIds = new ArrayList<>();

        // 购物车商品按照店铺分组，注意shop做key，需要实现shop的equals方法
        Map<Shop, List<CartItemVO>> cartItemMap = new HashMap<>();
        for (CartItemVO item : cartItems) {
            List<CartItemVO> list = cartItemMap.get(item.getShop());
            if (list == null) {
                list = new ArrayList<>();
                cartItemMap.put(item.getShop(), list);
            }
            productIds.add(item.getProductId());
            list.add(item);
        }

        // FIXME 针对分享的VIP订单, VIP商品被分享人不支持购买
        List<VipProduct> vipProducts = vipProductService.listVipProducts();
        List<String> vipProductIds = null;
        if (CollectionUtils.isNotEmpty(vipProducts)) {
            vipProductIds = new ArrayList<>();
            for (VipProduct vipProduct : vipProducts) {
                vipProductIds.add(vipProduct.getProductId());
            }
        }
        if (!PromotionType.VIP_RIGHT.equals(promotionType)
                && CollectionUtils.isNotEmpty(vipProductIds)
                && CollectionUtils.isNotEmpty(productIds)) {
            for (String productId : productIds) {
                if (vipProductIds.contains(productId))
                    throw new BizException(GlobalErrorCode.ERROR_PARAM, "请至首页升级权益购买");
            }
        }

        if (CollectionUtils.isNotEmpty(productIds)) {
            for (String pId : productIds) {
                // 校验抢购活动是否开始
                flashSalePromotionProductService.checkPromotionStart(pId);
            }
        }

        AddressVO address = fillAddressDefault(form.getAddressId());
        result.put("address", address);

        CartPromotionResultVO cartPromotionRet;
        UserSelectedProVO userSelectedPro = new UserSelectedProVO();
        if (form.getShopCoupons() != null) {

            Map<Shop, String> selShopCouponMap = new LinkedHashMap<>();
            Map<String, String> shopCouponParam = form.getShopCoupons();
            for (Entry<String, String> entry : shopCouponParam.entrySet()) {
                String shopId = entry.getKey();
                String shopCouponId = entry.getValue();
                Shop eachShop = shopService.load(shopId);
                selShopCouponMap.put(eachShop, shopCouponId);
            }
            userSelectedPro.setSelShopCouponMap(selShopCouponMap);

            Map<Shop, String> selShopPromotionMap = new LinkedHashMap<>();
            userSelectedPro.setSelShopPromotionMap(selShopPromotionMap);

            Map<CartItemVO, String> selCartItemPromotionMap = new LinkedHashMap<>();
            userSelectedPro.setSelCartItemPromotionMap(selCartItemPromotionMap);

            Map<CartItemVO, String> selCartItemCouponMap = new LinkedHashMap<>();
            userSelectedPro.setSelCartItemCouponMap(selCartItemCouponMap);
        }

        cartPromotionRet = promotionService
                .calculate4Cart(cartItemMap, getCurrentUser(), userSelectedPro, address,
                        false, true, form.getPromotionInfo(), PricingMode.CONFIRM);

        PricingResultVO pricingResultVO = cartPromotionRet.getTotalPricingResult();

        // FIXME 特殊处理邮费显示逻辑
        Map<PromotionType, BigDecimal> discountDetail = pricingResultVO.getDiscountDetail();
        discountDetail.put(LOGISTICS, pricingResultVO.getLogistics());
        discountDetail.put(LOGISTICS_DISCOUNT, pricingResultVO.getLogisticsDiscount());

        result.put("prices", pricingResultVO);
        result.put("skuIds", form.getSkuId());
        if (form.getShopRemarksCache() != null) {
            result.put("shopRemarksCache", form.getShopRemarksCache());
        }
        result.put("productId", form.getProductId());
        result.put("qty", form.getQty());

        // FIXME:to delete
        result.put("skuId", form.getSkuId() != null ? form.getSkuId().get(0) : "");
        result.put("shopId", form.getShopId());

        Integer feshManCount = 0;
        int isShowMessage = 0;
        String Message = "";
        //促销商品购买数量
        Integer cuxaioCount = 0;
        for (CartItemVO item : cartItems) {
            Sku sku = item.getSku();
            if (sku != null) {
                // 重新计算sku中的价格
                DynamicPricingUtil.rePricing(item, user);
                ProductVO productVO = item.getProduct();
                // FIXME 临时处理, 把cartItem中的多规格值赋予商品, 在购物车中显示
                // 客户端调整后再去掉
                productVO.setName(item.getTitle());

                //新人专区商品限购数量校验
                List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(item.getProductId());
                if (CollectionUtil.isNotEmpty(freshManProductVos)) {
                    feshManCount = feshManCount + item.getAmount();

                }
                List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectExsitSalesPromotionByProductId(item.getProductId());
                if (CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)) {
                    if (promotion4ProductSalesVOS.get(0).getEffectTo().before(new Date())) {
                        ResponseObject res = new ResponseObject<>();
                        res.setMoreInfo("商品活动已结束");
                        return res;
                    }
                    if (promotion4ProductSalesVOS.get(0).getEffectFrom().after(new Date())) {
                        ResponseObject res = new ResponseObject<>();
                        res.setMoreInfo("商品活动未开始");
                        return res;
                    }
                }

                //查看是否是促销商品
                int aBoolean = promotionSkusMapper.countSaleSku(item.getSku().getSkuCode(), item.getProductId());
                //促销折扣商品限购数量校验,购物车该商品的数量
                if (0 != aBoolean) {
                    Integer buyCountlimit = promotionSkusMapper.getBuyCountlimit(item.getSku().getSkuCode());
                    if (null != buyCountlimit) {
                        IUser currentIUser = getCurrentIUser();
                        Integer buyCount = promotionSkusMapper.getBuyCount(currentIUser.getId(), item.getSku().getSkuCode());
                        Integer goodCount = promotionSkusMapper.getGoodCount(item.getSku().getSkuCode());
                        if (item.getAmount() > (buyCountlimit - buyCount)) {
                            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "促销限购" + buyCountlimit + "件哦");

                        }
                        if (item.getAmount() > goodCount) {
                            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动商品已售罄");
                        }
                    }

                }

                //校验购买数量是够通过

            }


            //新人标记
            FreshManProductVo f =
                    this.freshManService.selectFreshmanProductById(item.getProductId());
            if (!Objects.isNull(f) && StringUtils.isNotBlank(f.getProductId())) {
                item.setFreshmanProduct(true);
                item.getProduct().setFreshmanProduct(true);
            }
        }

        FreshManConfigVo freshManConfigVo = freshManConfigMapper.selectByName("FRESHMAN_NOTIFY_FLAG");
        FreshManConfigVo freshManConfigVoMessage = freshManConfigMapper.selectByName("FRESHMAN_NOTIFY");


        //如果为新人商品，进入限购逻辑判断
        for (int i = 0; i <cartItems.size(); i++) {
            if(cartItems.get(i).getProduct().isFreshmanProduct()==true) {
                long userId = IdTypeHandler.decode(user.getId());
                Integer canBuyCount = firstOrderService.countProductNumber(userId);
                //是否显示限购文案
                if (freshManConfigVo.getValue().equals("1")&& canBuyCount>feshManCount) {
                    String message=freshManConfigVoMessage.getValue();
                    Integer canBuyOrderLimit=firstOrderMapper.selectOrderLimit();
                    Integer findfreshmanValue = firstOrderMapper.findfreshmanValue();
                    String a=message.replace("{0}",canBuyOrderLimit.toString());
                    canBuyCount-=feshManCount;
                    message=a.replace("{1}",canBuyCount.toString());
                    isShowMessage = 1;
                    Message = message;
                }
                freshManService.freshmanLimit(feshManCount, user);
            }
        }
        //新人商品限购



        if (CollectionUtils.isNotEmpty(promotionSkuCodes)) {
            //增加1013杭州大会赠品订单行-2018-10-07 by Chenpeng
            this.convretCartItems(cartItems, result);
        }

        //step2.设置邀请码判断
        //如果有大会活动，则需要邀请码
        if (isCode) {
            //如果没有绑定邀请码，则需要输入邀请码
            if (!isBoundingInviteCode) {
                result.put("needInvitCode", true);
            }
        }
        result.put("cartItems", cartItems);
        result.put("promotionId", promotionProductId);
        result.put("isShowMessage", isShowMessage);
        result.put("Message", Message);
        return new ResponseObject<>(result);
    }

    /**
     * 提交订单判断白名单
     *
     * @param skuId
     * @param cpId
     * @return
     */
    private boolean checkIsWhiteConfirm(String skuId, Long cpId) {
        boolean isWhite = promotionBaseInfoService.checkIsWhiteBySkuId(skuId, cpId);
        if (!isWhite) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, WHITELIST_CONFIRE_ALERT_CONTENT);
        }
        return true;
    }

    //判断是否超过限购数量(购物车)
    private Map<String, String> isOverRestriction(List<String> promotionSkuCodes, User user,
                                                  String pCode) {
        Map<String, String> flag = promotionOrderDetailService
                .isOverRestriction(promotionSkuCodes, user, pCode);
        return flag;
    }

    //判断是否超过限购数量(单个商品)
    private Map<String, String> isOverRestriction(String promotionSkuId, User user, int qty,
                                                  String pCode) {
        Map<String, String> flag = promotionOrderDetailService
                .isOverRestriction(promotionSkuId, user, qty, pCode);
        return flag;
    }


    AddressVO fillAddressDefault(String addressId) {
        AddressVO address = null;
        if (StringUtils.isNotBlank(addressId)) {
            address = addressService.loadUserAddress(addressId);
        }
        if (address == null) {
            address = addressService.loadCurrDefault();
        }
        return address;
    }

    @RequestMapping("/order/items")
    @ResponseBody
    public ResponseObject<List<OrderItemVO>> listOrderItems(@RequestParam String orderId) {
        List<OrderItemVO> result = orderItemMapper.selectVOByOrderId(orderId);
        return new ResponseObject<>(result);
    }

    @RequestMapping("/order/getOrderStatus")
    @ResponseBody
    public ResponseObject<JSONObject> getStatus() {
        ResponseObject<JSONObject> responseObject = new ResponseObject<>();
        try {
            User buyer = getCurrentUser();
            int num1 = orderMapper.getOrderByStatusAndUser(buyer.getId(), OrderStatus.SUBMITTED);
            int num2 = orderMapper.getOrderByStatusAndUser(buyer.getId(), OrderStatus.PAID);
            int num2_1 = orderMapper.getOrderByStatusAndUser(buyer.getId(), OrderStatus.PAIDNOSTOCK);
            int num3 = orderMapper.getOrderByStatusAndUser(buyer.getId(), OrderStatus.SHIPPED);
            int num4 = orderMapper.getOrderByStatusAndUser2(buyer.getId(), OrderStatus.REFUNDING);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("submitted", reserveOrderNumber(num1, OrderStatus.SUBMITTED));
            jsonObject.put("paid", num2 + num2_1);
            jsonObject.put("shipped", num3);
            jsonObject.put("refunding", num4);
            responseObject.setData(jsonObject);
            responseObject.setMoreInfo("成功获取");
        } catch (Exception e) {
            responseObject.setMoreInfo("获取失败");
        }
        return responseObject;
    }

    /**
     * 加上预约单数量
     *
     * @param num         int
     * @param orderStatus OrderStatus
     * @return int
     */
    private int reserveOrderNumber(int num, OrderStatus orderStatus) {
        return num + promotionReserveService.countOrderStatus(orderStatus);
    }

    // 订单支付完成页
    @ResponseBody
    @RequestMapping("/order/finishPayPage")
    public ResponseObject<PayfinishPage> orderFinishPage(@RequestParam("orderId") String mainOrderId) {
        User user = (User) getCurrentIUser();
        Long currentCpId = user.getCpId();

        final MainOrderVO mainOrderVO = mainOrderService.loadVO(mainOrderId);
        if (mainOrderVO == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "主订单不存在");
        }
        // 订单状态为未支付
        if (mainOrderVO.getStatus() != MainOrderStatus.PAID) {
            boolean isPaid;
            try {
                isPaid = thirdPaymentQueryService.query(mainOrderVO)
                        .map(ThirdPaymentQueryRes::isPaid).orElse(false);
            } catch (Exception e) {
                log.error("查询订单状态失败");
                isPaid = false;
            }
            if (!isPaid) {
                // 确认三方支付通道也未支付
                PayfinishPage payfinishPage = new PayfinishPage();
                payfinishPage.setOrderNo(mainOrderId);
                payfinishPage.setPayStatus(0);
                return new ResponseObject<>(payfinishPage);
            }
        }
        // 其余情况表示已经支付成功
        Map<String, Object> resultMap = callbackSuccessOrder(mainOrderVO, currentCpId);
        return new ResponseObject<>((PayfinishPage) resultMap.get("payfinishPage"));
    }

    /**
     * 支付前轮循获取银联支付订单成功的数据，临时方案
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping("/order/getCallBack")
    public ResponseObject<Boolean> getPayCallBackStatus(@RequestParam("orderId") String orderId) {
        ResponseObject<Boolean> r = new ResponseObject<>();
        if (StringUtils.isBlank(orderId)) {
            r.setData(true);
            return r;
        }
        boolean b = this.orderService.getPayCallBackStatus(orderId);
        r.setData(b);
        return r;
    }

    /**
     * 支付前轮循获取银联支付订单成功的数据的轮循时间，临时方案
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/order/getCallBackTime")
    public ResponseObject<Integer> getPayCircleTime() {
        ResponseObject<Integer> r = new ResponseObject<>();
        r.setData(promotionConfigService.getEspCircleTime());
        return r;
    }

    /**
     * 04-20 变更， 改方法不再判断是否已支付，在改方法之前对订单状态做判断
     */
    private Map<String, Object> callbackSuccessOrder(MainOrderVO orderVO, Long currentCpid) {
        String mainOrderId = orderVO.getId();
        Map<String, Object> resultMap = new HashMap<>();
        PayfinishPage payfinishPage = new PayfinishPage();
        List<OrderVO> orders = orderVO.getOrders();
        boolean hasIdentity = customerProfileService.hasIdentity(currentCpid);

        boolean hasVipOrder = false;
        String vipOrderId = null;
        payfinishPage.setPayStatus(1);
        payfinishPage.setOrderNo(orderVO.getId());
        BigDecimal totalFee = new BigDecimal(0);
        if (null != orders && orders.size() > 0) {
            for (OrderVO order : orders) {
                totalFee = totalFee.add(order.getTotalFee() == null ? new BigDecimal(0) : order.getTotalFee());
                if ("VIP_RIGHT".equals(order.getOrderType().name())) {
                    hasVipOrder = true;
                    vipOrderId = order.getId();
                }
            }
        }
        payfinishPage.setPaidFee(totalFee.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString()); //实付金额

        // TODO 新客二期:未抽奖用户可以显示抽奖, 跟身份无关
        FreshmanLotteryInfoVo freshmanLotteryInfoVo = new FreshmanLotteryInfoVo();
        Integer lotteryCount = freshmanLotteryService.queryFreshmanLotteryCountByCpid(currentCpid);
        if (lotteryCount >= 1) { //已抽过
            payfinishPage.setShowLottery(false);
            freshmanLotteryInfoVo.setLotteryCount(0);
        } else {
            payfinishPage.setShowLottery(true);
            freshmanLotteryInfoVo.setLotteryCount(1);
            // 抽奖德分
            List<Integer> pointList = new ArrayList<>();
            List<FreshmanLottery> freshmanLotteryList = freshmanLotteryService.getAllFreshmanLotteryIsOpen();
            if (CollectionUtils.isNotEmpty(freshmanLotteryList)) {
                for (FreshmanLottery freshmanLottery : freshmanLotteryList) {
                    pointList.add(freshmanLottery.getPoint());
                }
            }
            freshmanLotteryInfoVo.setPointList(pointList);
        }
        payfinishPage.setLottery(freshmanLotteryInfoVo);

        if (!hasIdentity) { //白人
            payfinishPage.setWhiteCustom(true);
            payfinishPage.setShowFreshmanTrack(true);
            payfinishPage.setShowUpgradeVIP(true);
            FreshmanWindowInfoVo freshmanWindowInfoVo = new FreshmanWindowInfoVo();
            BigDecimal consumedMoney = freshmanTrackService.queryConsumedMoneyByCpid(currentCpid); //累计消费金额
//            Boolean isFirstOder = firstOrderService.firstOrderOfFinishPayPage(orderVO.getBuyerId(), mainOrderId);
            // TODO 新客二期:首单逻辑暂时不用
            if (consumedMoney.intValue() < 500) {
                handleWindowInfoDown500(currentCpid, freshmanWindowInfoVo, consumedMoney, hasVipOrder, vipOrderId);
            } else {
                handleWindowInfoUp500(currentCpid, freshmanWindowInfoVo, hasVipOrder, vipOrderId);
            }

            Boolean hasPieceOrder = firstOrderService.countMainOrderByPiece(orderVO.getBuyerId(), mainOrderId);
            if (hasPieceOrder) { // 有开团且是老用户
                if (consumedMoney.intValue() < 200) {
                    boolean existFlagData = freshManFlagService.isExistFlagData(currentCpid);
                    if (!existFlagData) {
                        FreshManToast freshManToast = new FreshManToast();
                        freshManToast.setCpId(currentCpid);
                        freshManToast.setType(1);
                        freshManFlagService.updateFlag4Toast(freshManToast);
                    }
                }
            }

            // 是否弹窗
            Integer flag = freshmanTrackService.queryFreshmanFlag(currentCpid, consumedMoney.intValue(), freshmanTrackService.queryIsVIPOder(currentCpid));
            if (flag == 1) {
                payfinishPage.setShowSpringWindow(false);
            } else {
                payfinishPage.setShowSpringWindow(true);
            }
            payfinishPage.setToastInfo(freshmanWindowInfoVo);

            // 新人轨迹
            FreshmanTrackInfoVo freshmanTrackInfoVo;
            freshmanTrackInfoVo = freshmanTrackService.getFreshmanTrackInfo(currentCpid);
            payfinishPage.setFreshmanGuide(freshmanTrackInfoVo);

        }
        resultMap.put("payfinishPage", payfinishPage);
        return resultMap;
    }

    // 支付完成页,处理累计消费超过500的弹窗
    private void handleWindowInfoUp500(Long currentCpid, FreshmanWindowInfoVo freshmanWindowInfoVo, Boolean
            hasVipOrder, String vipOrderId) {
        FreshManToast freshManToast = new FreshManToast();
        freshManToast.setCpId(currentCpid);
        Integer lastVipOrderId = freshmanTrackService.queryIsVIPOder(currentCpid);
        String activateTime = null;
        if (hasVipOrder && IdTypeHandler.decode(vipOrderId) == lastVipOrderId) {
            freshmanWindowInfoVo.setType(4); // VIP弹窗
            Date vipOrderPaidAt = freshmanTrackService.queryVIPOrderPaidAtById((int) IdTypeHandler.decode(vipOrderId)); //VIP套餐升级时间
            if (vipOrderPaidAt != null && vipOrderPaidAt.after(new Date())) {
                freshManToast.setVipType(1);
                activateTime = getDateString(currentCpid, vipOrderPaidAt, freshManToast);
            }
        } else {
            freshmanWindowInfoVo.setType(5); // 满500弹窗
            Date full500PaidAt = freshmanTrackService.queryIdentityActiveTime(currentCpid); //满500身份变更时间
            freshManToast.setVipType(2);
            activateTime = getDateString(currentCpid, full500PaidAt, freshManToast);
        }

        freshmanWindowInfoVo.setEffectTime(activateTime); //满500身份变更时间,满500后下VIP订单身份生效时间以VIP订单为准
    }

    private String getDateString(Long currentCpid, Date changeIdentityTime, FreshManToast freshManToast) {
        String activateTime = null;
        if (null != changeIdentityTime) {
            try {
//                boolean existFlagData = freshManFlagService.isExistFlagData(currentCpid);
                FreshManFlagVo freshManFlagVo = freshManFlagService.selectFlag4Toast(currentCpid);
                if (freshManFlagVo == null) {
                    User user = userService.selectUserIdByCpId(String.valueOf(currentCpid));
                    Date trackActiveTime = freshmanTrackService.queryFreshmanTrackStartTime();
                    if (user != null && trackActiveTime != null && user.getCreatedAt().after(trackActiveTime)) {
                        // 活动之后注册
                        freshManToast.setType(1);
                    } else {
                        freshManToast.setType(2);
                    }
                    if (changeIdentityTime.getTime() > System.currentTimeMillis()) {
                        freshManToast.setRecordTime(changeIdentityTime);
                        activateTime = getIdentityActivateTime(changeIdentityTime);
                    }
                    freshManFlagService.updateFlag4Toast(freshManToast);
                } else {
                    if (changeIdentityTime.getTime() > System.currentTimeMillis() && (freshManFlagVo.getEffectAt() == null || changeIdentityTime.before(freshManFlagVo.getEffectAt()))) {
                        freshManToast.setRecordTime(changeIdentityTime);
                        activateTime = getIdentityActivateTime(changeIdentityTime);
                        freshManFlagService.updateVipTime(freshManToast);
                    }
                }
            } catch (Exception e) {
                log.error("记录VIP生效时间异常");
                e.printStackTrace();
            }
        }
        return activateTime;
    }

    // 支付完成页,处理累计消费不超过500的弹窗
    private void handleWindowInfoDown500(Long currentCpid, FreshmanWindowInfoVo freshmanWindowInfoVo, BigDecimal
            consumedMoney, Boolean hasVipOrder, String vipOrderId) {
        List<FreshmanTrack> freshmanTracks = freshmanTrackService.selectAllFreshmanTrack();
        FreshManToast freshManToast = new FreshManToast();
        Integer lastVipOrderId = freshmanTrackService.queryIsVIPOder(currentCpid);
        freshManToast.setCpId(currentCpid);
        if (hasVipOrder && IdTypeHandler.decode(vipOrderId) == lastVipOrderId) {
            Date vipOrderPaidAt = freshmanTrackService.queryVIPOrderPaidAtById((int) IdTypeHandler.decode(vipOrderId)); //VIP套餐升级时间
            if (null != vipOrderPaidAt && vipOrderPaidAt.after(new Date())) { //VIP弹窗
                freshManToast.setVipType(1);
                String activateTime = getDateString(currentCpid, vipOrderPaidAt, freshManToast);
                freshmanWindowInfoVo.setType(4);
                freshmanWindowInfoVo.setEffectTime(activateTime); //VIP套餐身份变更时间
            }
        } else if (consumedMoney.intValue() >= 200) {
            freshmanWindowInfoVo.setType(3);
            for (int i = freshmanTracks.size() - 1; i >= 0; i--) {
                if ("1".equals(freshmanTracks.get(i).getGrade())) {
                    freshmanWindowInfoVo.setPoint(Integer.parseInt(freshmanTracks.get(i).getPoint())); //发放得分数
                    break;
                }
            }
        } else {
            freshmanWindowInfoVo.setType(2);
            for (int i = freshmanTracks.size() - 1; i >= 0; i--) {
                if ("2".equals(freshmanTracks.get(i).getGrade())) {
                    freshmanWindowInfoVo.setAmount(new BigDecimal(freshmanTracks.get(i).getAmount()).subtract(consumedMoney).setScale(2, BigDecimal.ROUND_HALF_EVEN).stripTrailingZeros()); //再消费金额
                    break;
                }
            }
        }
    }

    // 新人身份生效时间解析
    private String getIdentityActivateTime(Date paidAt) {
        String identityEffectTime = null;
        if (null != paidAt) {
            if (paidAt.getTime() > System.currentTimeMillis()) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                identityEffectTime = sdf.format(paidAt);
            }
        }
        return identityEffectTime;
    }


    public static void main(String[] args) {
        Product product = new Product();
        LogisticsStock logisticsStock = new LogisticsStock();
        product.setSource(ProductSource.SF.name());
        product.setSourceProductId("b8t5");
        logisticsStock.setProductId(89L);
        if (StringUtils.equals(product.getSource(), ProductSource.SF.name()) &&
                IdTypeHandler.decode(product.getSourceProductId()) == logisticsStock.getProductId()) {
        }
    }

    /**
     * 为订单商品增加赠品信息
     */
    private void convretCartItems(List<CartItemVO> cartItems, Map<String, Object> result) {
        if (CollectionUtils.isEmpty(cartItems)) {
            return;
        }
        int meetingPromotionCount = 0;
        //为提交订单接口准备数据：参加大会售卖活动的skuId
        List<String> promotionSkuIds = new ArrayList<>();
        for (CartItemVO cartItem : cartItems) {
            Sku sku = cartItem.getSku();
            SkuExtend skuEx = new SkuExtend();
            BeanUtils.copyProperties(sku, skuEx);

            //根据商品sku获取活动信息
            List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService
                    .selectBySkuCode(skuEx.getSkuCode());
            if (CollectionUtils.isEmpty(promotionBaseInfos)) {
                continue;
            }
            //只校验大会活动的商品
            if (!promotionBaseInfos.get(0).getpType().contains("meeting")) {
                continue;
            }
            String pCode = promotionBaseInfos.get(0).getpCode();
            //检查这个商品有无活动，没有则认为是没有赠品
            List<PromotionSkuVo> promotionSkuVos = promotionSkusService
                    .getPromotionSkus(skuEx.getSkuCode());
            if (CollectionUtils.isEmpty(promotionSkuVos)) {
                continue;
            }
            //TODO 加活动类型判断
            meetingPromotionCount++;
            promotionSkuIds.add(sku.getId());

            PromotionSkuVo promotionSkuVo = promotionSkuVos.get(0);//TODO 变为get(i)
            //检查这个活动商品有没有赠品，
            List<PromotionSkuGift> giftList = promotionSkuGiftService
                    .getPromotionSkuGiftList(pCode, promotionSkuVo.getSkuCode());
            if (CollectionUtils.isEmpty(giftList)) {
                continue;
            }
            //step1. 设置有无赠品
            skuEx.setHadGift(true);
            //买赠信息 （目前为买gift赠1）
            int gift = promotionSkuVo.getGift();
            int giftCount = 0;
            //计算赠品数量
            if (gift > 0) {
                //舍掉小数取整
                giftCount = (int) Math.floor(cartItem.getAmount() / gift);
            }
            //设置赠品数量
            skuEx.setGiftCount(giftCount);
            cartItem.setSku(skuEx);
            //新人商品
            FreshManProductVo f =
                    this.freshManService.selectFreshmanProductById(cartItem.getProductId());
            if (!Objects.isNull(f) && StringUtils.isNotBlank(f.getProductId()))
                cartItem.setFreshmanProduct(true);
            else {
                cartItem.setFreshmanProduct(false);
            }
        }

        //step3.设置活动sku判断
        result.put("promotionSkuIds", promotionSkuIds);
    }

    /**
     * 检查拼团与VIP资格
     */
    @ResponseBody
    @RequestMapping(value = "/order/check")
    public ResponseObject<String> orderCheckValid(
            @RequestBody @ModelAttribute VipRightParams params) {
        if (null != params.getOrderType() &&
                PromotionType.VIP_RIGHT.name().equals(params.getOrderType())) {
            User user = (User) getCurrentIUser();
            params.setCpId(user.getCpId());
            params.setBuyerId(user.getId());
            //判断是否有身份
            boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
            if (hasIdentity) {
                return new ResponseObject<>("", "您已经是会员", GlobalErrorCode.UNKNOWN);
            }

            // 查看当前用户是否已下过VIP资格订单
            // 1. 若订单尚未支付，看是否距下单时间半小时内，若在则不允许再下单
            // 2. 若订单已支付，则提示已升级为VIP，不允许再下单
            // 3. 若是本就是vip，則3.1 or 3.2。
            Integer valid = vipRightService.checkValid(params);

            if (1 == valid) {
                return new ResponseObject<>("", "您已经是会员", GlobalErrorCode.UNKNOWN);
            } else if (2 == valid) {
                return new ResponseObject<>("", "您有订单尚未支付", GlobalErrorCode.UNKNOWN);
            } else if (3 == valid) {
                return new ResponseObject<>("", "您尚未关联手机号，请联系客服4000186266", GlobalErrorCode.UNKNOWN);
            }
        }
        return new ResponseObject<>(GlobalErrorCode.SUCESS);
    }

    /**
     * 检查参加活动的用户购买总金额是否超过
     */
    private boolean isOverUpValue(List<String> promotionSkuIds, User user, int topValue,
                                  String pCode) {
        boolean isOver = promotionOrderDetailService
                .isOverUpValue(promotionSkuIds, user, topValue, pCode);

        return isOver;
    }


    private boolean isOverUpValue(String promotionSkuId, User user, int qty, int topValue) {
        Sku sku = skuMapper.selectByPrimaryKey(promotionSkuId);
        List<PromotionBaseInfo> lstSkus = promotionBaseInfoService.selectBySkuCode(sku.getSkuCode());
        if (CollectionUtils.isEmpty(lstSkus)) {
            return false;
        }
        boolean isOver = promotionOrderDetailService.isOverUpValue(promotionSkuId, user, qty, topValue);

        return isOver;
    }


    /**
     * 为订单商品增加赠品信息
     */
    private void convretOrderVOs(List<OrderVO> orders) {
        if (CollectionUtils.isEmpty(orders)) {
            return;
        }
        for (OrderVO orderVO : orders) {
            List<OrderItem> orderItems = orderVO.getOrderItems();
            converOrderItem(orderItems);
            //用户查询订单时，订单状态统一维护为PAID
            if (orderVO.getStatus().equals(OrderStatus.PAIDNOSTOCK)) {
                orderVO.setStatus(OrderStatus.PAID);
            }
        }
    }


    /**
     * 为订单详情增加赠品信息
     */
    private void convretOrderVO(OrderVOEx orderEx) {
        if (null == orderEx) {
            return;
        }

        List<OrderItem> orderItems = orderEx.getOrderItems();
        converOrderItem(orderItems);
    }

    private void converOrderItem(List<OrderItem> orderItems) {

        for (OrderItem orderItem : orderItems) {
            Sku sku = orderItem.getSku();
            if (null != sku && null != orderItem.getPromotionType() && orderItem.getPromotionType()
                    .equals(PromotionType.MEETING_GIFT)) {
                orderItem.setPrice((new BigDecimal(0.00)).setScale(2, BigDecimal.ROUND_HALF_UP));
                orderItem.setReduction((new BigDecimal(0.00)).setScale(2, BigDecimal.ROUND_HALF_UP));
                sku.setReducedPrice((new BigDecimal(0.00)).setScale(2, BigDecimal.ROUND_HALF_UP));
            }
            //是否是新人专区商品
            FreshManProductVo f = this.freshManService.selectFreshmanProductById(orderItem.getProductId());
            if (f != null && StringUtils.isNotBlank(f.getProductId()))
                orderItem.setFreshmanProduct(true);
            else {
                orderItem.setFreshmanProduct(false);
            }
        }
    }

    /**
     * 检查是否超过售后时间
     */
    private boolean checkOvertime(Order order) {
        //发货后14天内可申请售后
        if (order == null) {
            return false;
        }
        Date shippedAt = order.getShippedAt();
        Date paidAt = order.getPaidAt();
        if (paidAt == null
                || order.getStatus().equals(OrderStatus.CANCELLED)
                || order.getStatus().equals(OrderStatus.CLOSED)) {
            return false;
        }
        if (shippedAt != null) {
            long t = System.currentTimeMillis() - shippedAt.getTime();
            if (t >= (3600000 * 24 * 14)) //14天后售后入口自动关闭
                return false;
        }
        return true;
//        return !(t >= (3600000 * 24 * 14));//14天后售后入口自动关闭
    }

    /**
     * 发送消息
     *
     * @param status
     * @param orderNo
     */
    private void ServMessage(String status, String orderNo) {

        String cpid = servMessageMapper.selectCpidByOrderNo(orderNo);
        Integer push = servMessageMapper.message(status, ServMessageStatus.TYPEMSG.getWord(), ServMessageStatus.STATUS.getWord(), cpid, ServMessageStatus.TYPESYSTEM.getWord());

    }

    @RequestMapping(value = "order/finishpage/toastfeedback", method = {POST, RequestMethod.GET})
    @ResponseBody
    public ResponseObject<Boolean> toastFeedback(@RequestBody(required = false) Map<String, String> repMap,
                                                 @RequestParam(value = "type", required = false) Integer type) {
        ResponseObject<Boolean> ro = new ResponseObject<>();
        ro.setData(false);
        FreshManToast f = new FreshManToast();
        User user = (User) getCurrentIUser();
        f.setCpId(user.getCpId());
        //参数校验
        if (type != null)
            f.setType(type);
        else {
            if (repMap == null) {
                ro.setMoreInfo("参数不能为空");
                return ro;
            }
            String typeStr = repMap.get("type");
            if (StringUtils.isNotBlank(typeStr)) {
                type = Integer.parseInt(typeStr);
                f.setType(type);
            }
        }
        if (f.getType() == null) {
            ro.setMoreInfo("type不能为空");
            return ro;
        }

        //校验接口的type类型是否满足约定的类型
        boolean isType = validateType(f.getType());
        if (!isType)
            return ro;

        //业务处理
        boolean b = this.freshManFlagService.updateFlag4Toast(f);
        ro.setData(b);
        if (b)
            ro.setMoreInfo("success");
        else {
            ro.setMoreInfo("fail");
        }

        log.info(new StringBuffer().append("用户cpid是:")
                .append(f.getCpId())
                .append("的用户更改升级vip表flag的数据返回状态是：")
                .append(b)
                .toString());
        return ro;
    }

    /**
     * 校验type
     *
     * @param type
     * @return
     */
    private boolean validateType(Integer type) {
        Integer[] typeArr = FreshManToast.TYPE_ARR;
        for (Integer i : typeArr) {
            if (i == type)
                return true;
        }
        return false;
    }
}