package com.xquark.restapi.test;


import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.config.SystemConfigService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.ownerAdress.CustomerOwnerAdress;
import com.xquark.service.product.SFProductService;
import com.xquark.service.test.TestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Controller
@ApiIgnore
public class TestController extends BaseController {

  @Autowired
  private TestService testService;

  @Autowired
  private CustomerOwnerAdress customerOwnerAdress;
  @Autowired
  private SFProductService sFProductService;
  @Autowired
  private SystemConfigService systemConfigService;
  @Autowired
  private FreshManService freshManService;
  @ResponseBody
  @RequestMapping("/test/fix/product")
  public ResponseObject<Boolean> fix() {
    testService.fixThirdNull();
    return new ResponseObject<>(true);
  }

  @ResponseBody
  //@RequestMapping("/test/fix/decode")
  public ResponseObject<Boolean> decode() {
    testService.decodeSpiderShopId();
    return new ResponseObject<>(true);
  }

  // 处理重复商品
  @ResponseBody
  //@RequestMapping("/test/fix/duplicate")
  public ResponseObject<Boolean> duplicate() {
    testService.duplicateProduct();
    return new ResponseObject<>(true);
  }

  @ResponseBody
  //@RequestMapping("/test/fix/encode")
  public ResponseObject<Boolean> encode() {
    testService.encodeProductId();
    return new ResponseObject<>(true);
  }

  // 标题 + 图片
  @ResponseBody
  //@RequestMapping("/test/fix/thirdnull1")
  public ResponseObject<Boolean> thirdnull1() {
    testService.thirdnull1();
    return new ResponseObject<>(true);
  }

  // sku + 图片
  @ResponseBody
  //@RequestMapping("/test/fix/thirdnull2")
  public ResponseObject<Boolean> thirdnull2() {
    testService.thirdnull2();
    return new ResponseObject<>(true);
  }


  // 店铺不存在的默认标记为api发布
  @ResponseBody
  //@RequestMapping("/test/fix/thirdnull3")
  public ResponseObject<Boolean> thirdnull3() {
    testService.thirdnull3();
    return new ResponseObject<>(true);
  }

  // spider对应xquark第一张type=1不一致
  @ResponseBody
  //@RequestMapping("/test/fix/img1")
  public ResponseObject<Boolean> fixImgOrder1() {
    testService.fixImgOrder1();
    return new ResponseObject<>(true);
  }

  // 重爬因淘宝规则改动导致同步快店数据有误
  @ResponseBody
  //@RequestMapping("/test/fix/productRule")
  public ResponseObject<Map<String, Object>> fixPorudctBySpider(String id) {
    Map<String, Object> result = new HashMap<>();
    result = testService.fixPorudctBySpider(id);
    return new ResponseObject<>(result);
  }

  // 重爬详情未搬成功
  @ResponseBody
  @RequestMapping("/test/spider/fragment")
  public ResponseObject<Map<String, Object>> reSpiderForFragment(String type) {
    Map<String, Object> result = new HashMap<>();
    if (type == null) {
      result.put("code", "302");
      result.put("ret", "type can not be empty");
    } else {
      result = testService.reSpiderForFragment(type);
    }

    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/test/test/test")
  public String test(String id){
    boolean b = sFProductService.checkProductIdShowStatus(id);
    if(b){
      return "true";
    }
    return "false";
  }

  @ResponseBody
  @RequestMapping("/test/config")
  public Map<String, Object> testConfig(Long cpId){
    this.freshManService.setFreshManCommission(cpId);
  return null;
  }

  public static void main(String[] args) throws Exception {


    Properties prop = new Properties();
    prop.setProperty("mail.host", "smtp.qiye.aliyun.com");
    prop.setProperty("mail.transport.protocol", "smtp");
    prop.setProperty("mail.smtp.auth", "true");
    prop.put("mail.smtp.connectiontimeout", 2000);
    prop.put("mail.smtp.timeout", 8000);
    //使用JavaMail发送邮件的5个步骤
    //1、创建session
    Session session = Session.getInstance(prop);
    //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
    session.setDebug(true);
    //2、通过session得到transport对象
    Transport ts = session.getTransport();
    //3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
    ts.connect("smtp.qiye.aliyun.com", "hdsalarm@handeson.com", "S229rQAy35");
    //4、创建邮件
    Message message = createSimpleMail(session);
    //5、发送邮件
    ts.sendMessage(message, message.getAllRecipients());
    ts.close();
  }

  public static MimeMessage createSimpleMail(Session session)
          throws Exception {
    //创建邮件对象
    MimeMessage message = new MimeMessage(session);
    //指明邮件的发件人
    message.setFrom(new InternetAddress("hdsalarm@handeson.com"));
    //指明邮件的收件人，现在发件人和收件人是一样的，那就是自己给自己发
    message.setRecipient(Message.RecipientType.TO, new InternetAddress("yarm.yang@handeson.com"));
    message.setRecipient(Message.RecipientType.CC, new InternetAddress("yarm.yang@handeson.com"));
    //邮件的标题
    message.setSubject("只包含文本的简单邮件");
    //邮件的文本内容
    message.setContent("你好啊！", "text/html;charset=UTF-8");
    //返回创建好的邮件对象
    return message;
  }
}
