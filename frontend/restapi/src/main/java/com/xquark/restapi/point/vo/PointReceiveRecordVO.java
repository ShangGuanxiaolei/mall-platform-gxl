package com.xquark.restapi.point.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hs
 */
public class PointReceiveRecordVO {


    /**
     * name : feeling
     * createAt : 1544689951000
     * amount : 55
     * avatar : https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83er4aFbuaMS5BLEep0jxDiaiagremEfF0xMN0GKy4pa1qjIb03UZ1Zp016PWW6XibHHoibPMXZN7ibSruag/132
     * bizType : RECEIVE
     */

    private String name;
    private Date createAt;
    private BigDecimal amount;
    private String avatar;
    private String bizType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }
}
