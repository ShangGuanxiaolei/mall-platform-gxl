package com.xquark.restapi.merchantSupplier;

import com.xquark.dal.model.MerchantSupplier;
import com.xquark.restapi.base.BaseController;
import com.xquark.service.merchantSupplier.MerchantSupplierService;
import com.xquark.service.merchantSupplier.MerchantSupplierServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: huangjie Date: 2018/7/2. Time: 下午8:45
 */
@RestController
@RequestMapping("/merchant/supplier")
public class MerchantSupplierController  extends BaseController<MerchantSupplier,MerchantSupplierService> {
  
  
  private MerchantSupplierServiceImpl service;

  @Autowired
  public void setMerchantSupplierServiceImpl(MerchantSupplierServiceImpl service) {
      this.service = service;
  }
  
  @Override
  protected MerchantSupplierServiceImpl getService() {
    return service;
  }
}
