package com.xquark.restapi.wechat;

import com.xquark.service.BaseService;
import com.xquark.service.product.ProductService;

/**
 * Created by wangxinhua on 17-12-15. DESC:
 */
public enum ShareType {

  PRODUCT("商品", "", ProductService.class);

  private final String alias;

  private final String url;

  private final Class<?> service;

  ShareType(String alias, String url, Class<? extends BaseService> service) {
    this.alias = alias;
    this.service = service;
    this.url = url;
  }

  public String getAlias() {
    return alias;
  }

  public Class<?> getService() {
    return service;
  }

  public String getUrl() {
    return url;
  }
}
