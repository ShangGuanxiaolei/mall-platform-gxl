package com.xquark.restapi.index;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xquark.dal.model.AccessReport;
import com.xquark.dal.util.json.JsonResourceUrlSerializer;

public class AccessReportVO extends AccessReport {

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String imgUrl;

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

}
