package com.xquark.restapi.mall.point;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.ToastInfoType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.dal.vo.CommissionWithdrawVO;
import com.hds.xquark.dal.vo.LaborExpensesVO;
import com.hds.xquark.dal.vo.WithdrawDetailVO;
import com.hds.xquark.service.point.PointCommService;
import com.hds.xquark.service.point.PointServiceApi;
import com.xquark.dal.mapper.FileHeaderMapper;
import com.xquark.dal.mapper.JobSchedulerLogMapper;
import com.xquark.dal.mapper.PointBalanceMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.type.JobStatus;
import com.xquark.dal.type.JobType;
import com.xquark.dal.type.PlatformType;
import com.xquark.dal.vo.*;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelGeneratorHelper;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.excel.item.CommissionWithdrawItem;
import com.xquark.service.excel.item.LaborExpensesItem;
import com.xquark.service.excel.item.WithdrawDetailItem;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.utils.ExcelUtils;
import com.xquark.utils.StringUtil;
import com.xquark.utils.excel.SheetContent;
import com.xquark.utils.resultcode.ColumnMapping;
import io.vavr.control.Try;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.xquark.restapi.mall.point.PointExportConfig.*;

/**
 * @author liuwei
 * @date 18-7-26 上午11:33
 */
@RestController
@RequestMapping("/point")
public class PointBalanceController extends BaseController {

    @Autowired
    private ExcelService excelService;

    @Autowired
    private PointBalanceMapper pointBalanceMapper;

    @Autowired
    private FileHeaderMapper fileHeaderMapper;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private JobSchedulerLogMapper jobSchedulerLogMapper;

    private PointCommService pointCommService;

    private PointServiceApi pointService;

    @Autowired
    public void setPointCommService(PointContextInitialize initialize) {
        this.pointCommService = initialize.getPointService();
        this.pointService = initialize.getPointServiceApi();
    }


    @RequestMapping("/add/{code}/{cpId}")
    private void test(@PathVariable(value = "code") String code, @PathVariable(value = "cpId") Long cpId) {
        // 处理线上新人购物满200，但由于银联回调未返回导致未发放2000德分的问题，手动发放2000德分
        User user = (User) getCurrentIUser();
        if (user.getCpId().equals(3037008L) && StringUtil.isNotNull(code) && "freshman".equals(code)) {
            log.info("log point grant for issues, cpid="+user.getCpId());
            this.pointService.grant(cpId
                    , "德分奖励发放"
                    , Trancd.FRESHMAN
                    , com.hds.xquark.dal.type.PlatformType.E
                    , new BigDecimal(2000));
            log.info("log point grant succeed, cpid="+user.getCpId());
        }
    }


    /**
     * 积分用excel导出
     */
    @RequestMapping("/exportExcel/byOrder")
    public ResponseObject<Void> exportSkuExcel(@RequestParam("month") String month,
                                               HttpServletResponse resp) {
        log.info("===================================开始按订单导出========================================");
        try {
            JobSchedulerLog jobSchedulerLog = new JobSchedulerLog();
            jobSchedulerLog.setJobType(201);
            jobSchedulerLog.setJobName("MonthEnd Order Export");
            jobSchedulerLog.setJobStatus(0);
            jobSchedulerLog.setStartDate(new Date());
            Integer jobId = jobSchedulerLogMapper.insert(jobSchedulerLog);
            List<PointExportByOrderVO> orderList = pointBalanceMapper
                    .selectPointExportByOrder(Integer.parseInt(month));
            //excel的工作簿名称和文件前缀
            String sheetStr = "收入报表-按订单";
            String filePrefix = "MonthEndOrders_" + month;
            excelService.export(filePrefix, orderList, PointExportByOrderVO.class, sheetStr,
                    null, PointExportByOrderSpec.KEY_SET,
                    PointExportByOrderSpec.VALUE_SET, resp, false);
            JobSchedulerLog jobSchedulerLog1 = new JobSchedulerLog();
            jobSchedulerLog1.setJobStatus(JobStatus.CREATED.getCode());
            jobSchedulerLog1.setCompletedDate(new Date());
            jobSchedulerLog1.setUpdatedDate(new Date());
            jobSchedulerLogMapper.updateByPrimaryKeySelective(jobSchedulerLog1);
            return new ResponseObject<>(GlobalErrorCode.SUCESS);
        } catch (Exception e) {
            JobSchedulerLog jobSchedulerLog = new JobSchedulerLog();
            jobSchedulerLog.setExceptionMsg(e.getMessage());
            jobSchedulerLog.setUpdatedDate(new Date());
            jobSchedulerLogMapper.updateByPrimaryKeySelective(jobSchedulerLog);
            log.error("按订单导出excel异常", e);
            return new ResponseObject<>("积分暂时不想出现", GlobalErrorCode.POINT_RECORD_NOT_FOUNT);
        }
    }

    /**
     * 德分用excel导出
     */
    @RequestMapping("/exportExcel/byCustomer")
    public ResponseObject<Void> exportLogisticsExcel(@RequestParam("month") String month,
                                                     HttpServletResponse resp) {
        log.info("===================================开始按客户导出========================================");
        try {
            //excel的工作簿名称和文件前缀
            String sheetStr = "收入报表-按客户";
            String filePrefix = "MonthEndCustomers_" + month;
            JobSchedulerLog jobSchedulerLog = new JobSchedulerLog();
            jobSchedulerLog.setJobType(202);
            jobSchedulerLog.setJobName("MonthEnd Customer Export");
            jobSchedulerLog.setJobStatus(0);
            jobSchedulerLog.setStartDate(new Date());
            Integer jobId = jobSchedulerLogMapper.insert(jobSchedulerLog);
            List<PointExportByCustomerVO> customerList = pointBalanceMapper.selectPointExportByCustomer(
                    Integer.parseInt(month));
            excelService.export(filePrefix, customerList, PointExportByCustomerVO.class, sheetStr,
                    null, PointExportByCustomerSpec.KEY_SET,
                    PointExportByCustomerSpec.VALUE_SET, resp, false);
            JobSchedulerLog jobSchedulerLog1 = new JobSchedulerLog();
            jobSchedulerLog1.setJobStatus(JobStatus.CREATED.getCode());
            jobSchedulerLog1.setCompletedDate(new Date());
            jobSchedulerLog1.setUpdatedDate(new Date());
            jobSchedulerLogMapper.updateByPrimaryKeySelective(jobSchedulerLog1);
            return new ResponseObject<>(GlobalErrorCode.SUCESS);
        } catch (Exception e) {
            JobSchedulerLog jobSchedulerLog = new JobSchedulerLog();
            jobSchedulerLog.setExceptionMsg(e.getMessage());
            jobSchedulerLog.setUpdatedDate(new Date());
            jobSchedulerLogMapper.updateByPrimaryKeySelective(jobSchedulerLog);
            log.error("按客户导出excel异常", e);
            return new ResponseObject<>("德分暂时不想出现", GlobalErrorCode.COMMISSION_RECORD_NOT_FOUNT);
        }
    }

    @RequestMapping("/exportExcel/withdraw")
    public ResponseObject<?> exportPointWithdraw(@RequestParam Integer month,
                                                 @RequestParam PlatformType platform, HttpServletResponse response) {
        log.info("============================== 开始导出 {} 月积分体现报表 ===================================",
                month);
        JobSchedulerLog jobSchedulerLog = JobSchedulerLog.empty(JobType.EXPORT_WITHDRAW);
        JobStatus status = JobStatus.ERROR;
        String jobMsg = "";
        List<CommissionWithdrawVO> targetList = pointCommService.listWithdrawVO(month,
                com.hds.xquark.dal.type.PlatformType.valueOf(platform.name()));
        if (CollectionUtils.isEmpty(targetList)) {
            String msg = String.format("月份 %d 暂没有可提现数据", month);
            log.info(msg);
            return new ResponseObject<>(msg, GlobalErrorCode.INVALID_ARGUMENT);
        }
        // 转换为带生成器的list
        List<CommissionWithdrawItem> items = Transformer
                .fromIterable(targetList, CommissionWithdrawItem.class);

        // 将数据拆分为中行与非中行
        ImmutableListMultimap<String, CommissionWithdrawItem> bankGroupedMap = Multimaps
                .index(items, COMMISSION_BANK_GROUP_FUNC);
        List<CommissionWithdrawItem> zhList = bankGroupedMap.get(ZH_KEY);
        List<CommissionWithdrawItem> nonZhList = bankGroupedMap.get(NON_ZH_KEY);

        String fileName = platform.getNameCN() + "提现积分报表" + month.toString();
        List<SheetContent<CommissionWithdrawItem>> sheetContents = ImmutableList.of(
                new SheetContent<>("人民币外币行内代付", zhList, CommissionWithdrawItem.class, ZH_MAPPING.value(),
                        getZhSheetHeader(PlatformType.fromCode(platform.getCode()))),
                new SheetContent<>("人民币跨行代付", nonZhList, CommissionWithdrawItem.class,
                        NON_ZH_MAPPING.value(),
                        getNonZhSheetHeader(PlatformType.fromCode(platform.getCode())))
        );
        try {
            excelService.export(fileName, sheetContents, response, false);
            status = JobStatus.COMPLETED;
        } catch (Exception e) {
            jobMsg = "导出提现报表失败";
            log.error(jobMsg, e);
            return new ResponseObject<>(jobMsg, GlobalErrorCode.INTERNAL_ERROR);
        } finally {
            // 重置生成器
            ExcelGeneratorHelper.reset(ExcelGeneratorHelper.WITHDRAW_ZH_INDEX_KEY);
            ExcelGeneratorHelper.reset(ExcelGeneratorHelper.WITHDRAW_NON_ZH_INDEX_KEY);
            jobSchedulerLog.complete(status, jobMsg);
            jobSchedulerLogMapper.insert(jobSchedulerLog);
        }
        log.info("============================== {} 月积分提现报表导出成功 ===================================",
                month);
        return new ResponseObject<>(GlobalErrorCode.SUCESS);
    }

    @RequestMapping("/exportExcel/withdraw/detail")
    public ResponseObject<?> exportPointWithdrawDetail(@RequestParam Integer month,
                                                       @RequestParam PlatformType platform, HttpServletResponse response) {

        log.info("============================== 开始导出 {} 积分详情报表 ===================================",month);
        JobSchedulerLog jobSchedulerLog = JobSchedulerLog.empty(JobType.EXPORT_WITHDRAW_DETAIL);
        JobStatus status = JobStatus.ERROR;
        String jobMsg = "";
        List<WithdrawDetailVO> targetList = pointCommService.listWithdrawDetailVO(month,
                com.hds.xquark.dal.type.PlatformType.valueOf(platform.name()));

        if (CollectionUtils.isEmpty(targetList)) {
            String msg = String.format("月份 %d 暂没有可提现数据", month);
            log.info(msg);
            return new ResponseObject<>(msg, GlobalErrorCode.INVALID_ARGUMENT);
        }

        String fileName = platform.getNameCN() + "提现积分明细报表" + month.toString();

        try {
            List<WithdrawDetailItem> items = Transformer
                    .fromIterable(targetList, WithdrawDetailItem.class);
            List<SheetContent<WithdrawDetailItem>> sheetContents = Collections.singletonList(
                    new SheetContent<>(
                            "提现积分明细",
                            items,
                            WithdrawDetailItem.class,
                            DETAIL.value(),
                            getDetailSheetHeader()
                    )
            );
            excelService.export(fileName, sheetContents, response, false);
            status = JobStatus.COMPLETED;
        } catch (Exception e) {
            jobMsg = "导出提现报表失败";
            log.error(jobMsg, e);
            return new ResponseObject<>(jobMsg, GlobalErrorCode.INTERNAL_ERROR);
        } finally {
            // 重置生成器
            ExcelGeneratorHelper.reset(ExcelGeneratorHelper.WITHDRAW_DETAIL_KEY);
            jobSchedulerLog.complete(status, jobMsg);
            jobSchedulerLogMapper.insert(jobSchedulerLog);
        }
        log.info("============================== {} 月积分提现明细报表导出成功 ===================================",
                month);
        return new ResponseObject<>(GlobalErrorCode.SUCESS);
    }

    @RequestMapping("/exportExcel/laborExpenses")
    public ResponseObject<?> exportLaborExpenses(@RequestParam String start,
                                                 @RequestParam String end, HttpServletResponse response) {

        log.info("============================== 开始导出 {} - {} 劳务报酬报表 ===================================",start,end);
        JobSchedulerLog jobSchedulerLog = JobSchedulerLog.empty(JobType.EXPORT_LABOR_EXPENSES);
        JobStatus status = JobStatus.ERROR;
        String jobMsg = "";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final Date startDate = Try.of(() -> format.parse(start))
                .getOrElseThrow(() -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, "结束时间格式不正确"));
        final Date endDate = Try.of(() -> format.parse(end))
                .getOrElseThrow(() -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, "结束时间格式不正确"));

        List<LaborExpensesVO> targetList = pointCommService.listLaborExpensesVO(startDate, endDate);

        if (CollectionUtils.isEmpty(targetList)) {
            String msg = String.format(" %s - %s 暂没有劳务报酬数据", start,end);
            log.info(msg);
            return new ResponseObject<>(msg, GlobalErrorCode.INVALID_ARGUMENT);
        }

        try {
            String fileName = start +"-" + end + " 劳务报酬报表" ;
            List<LaborExpensesItem> items = Transformer
                    .fromIterable(targetList, LaborExpensesItem.class);

            List<SheetContent<LaborExpensesItem>> sheetContents = Collections.singletonList(
                    new SheetContent<>(
                            "劳务报酬明细",
                            items,
                            LaborExpensesItem.class,
                            LABOR.value(),
                            getLaborSheetHeader()
                    )
            );

            excelService.export(fileName, sheetContents, response, false);
            status = JobStatus.COMPLETED;
        } catch (Exception e) {
            jobMsg = "导出劳务报酬报表失败";
            log.error(jobMsg, e);
            return new ResponseObject<>(jobMsg, GlobalErrorCode.INTERNAL_ERROR);
        } finally {
            // 重置生成器
            ExcelGeneratorHelper.reset(ExcelGeneratorHelper.LABOR_EXPENSES_KEY);
            jobSchedulerLog.complete(status, jobMsg);
            jobSchedulerLogMapper.insert(jobSchedulerLog);
        }
        log.info("============================== {} - {} 劳务报酬报表导出成功 ===================================", start,end);
        return new ResponseObject<>(GlobalErrorCode.SUCESS);
    }



    @RequestMapping("/balance/jobFileInfo")
    public ResponseObject<JobFileVO> jobInfo(@RequestParam("jobId") Long jobId) {
        JobFileVO jobSchedulerLog = jobSchedulerLogMapper.selectJobFileVO(jobId);
        return new ResponseObject<>(jobSchedulerLog);
    }

    /**
     * 根据jobId触发导入
     */
    @RequestMapping("/balance/import/withdraw")
    public ResponseObject<Boolean> importPointWithdraw(@RequestParam Long jobId,
                                                       @RequestParam final Integer month, @RequestParam final PlatformType platform) {
        final boolean ret = importExcel(jobId, WITHDRAW_COLUMN_MAPPING, WithdrawImportVO.class,
                withdrawImportVOS -> customerProfileService.executeWithdrawFeedback(withdrawImportVOS, month, platform));
        if (!ret) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "没有落账数据，已将 " + month + " 更新为成功落账");
        }
        return new ResponseObject<>(true);
    }

    /**
     * 根据job触发导入
     */
    @RequestMapping("/balance/import")
    public ResponseObject<Boolean> importPointComm(@RequestParam("jobId") Long jobId, @RequestParam("usedType") Integer usedType) {
        Boolean ret = importExcel(jobId, POINT_COLUMN_MAPPING, PointImportVO.class,
                pointImportVOS -> customerProfileService.savePointImport(pointImportVOS, usedType));
        return new ResponseObject<>(ret);
    }

    /**
     * 通用方法处理excel导入
     *
     * @param jobId jobId
     * @param mapping excel映射map
     * @param clazz 导入映射对象
     * @param function 实际导入处理逻辑函数
     * @param <T> 映射的泛型类型
     * @return 处理成功或失败
     */
    private <T> boolean importExcel(Long jobId, Map<String, ColumnMapping> mapping, Class<T> clazz,
                                    Function<List<T>, Integer> function) {
        FileHeader fileHeader = fileHeaderMapper.selectByJobId(jobId);
        if (fileHeader == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "job不存在");
        }
        JobSchedulerLog jobSchedulerLog = jobSchedulerLogMapper.selectByPrimaryKey(jobId);
        if (jobSchedulerLog == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "上传任务未创建");
        }
        JobStatus jobStatus = JobStatus.ERROR;
        String jobMsg = "";
        String filePath = fileHeader.getFilePath();
        int rowCount = 0;
        try (InputStream inputStream = new URL(filePath).openStream()) {
            List<T> excelList = ExcelUtils.excelImport(inputStream,
                    mapping, clazz);
            if (function != null) {
                rowCount = function.apply(excelList);
            }
            jobStatus = JobStatus.COMPLETED;
            fileHeader.setRowCount(rowCount);
        } catch (IOException e) {
            jobMsg = String.format("打开文件 %s 出错", filePath);
            log.error(jobMsg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, jobMsg);
        } catch (InvalidFormatException e) {
            jobMsg = String.format("文件 %s 格式不正确", filePath);
            log.error(jobMsg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, jobMsg);
        } catch (Exception e) {
            jobMsg = String.format("任务id: %d, %s", jobId, e.toString());
            log.error(jobMsg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, jobMsg);
        } finally {
            jobSchedulerLog.complete(jobStatus, jobMsg);
            jobSchedulerLogMapper.updateByPrimaryKeySelective(jobSchedulerLog);
            fileHeaderMapper.updateSelective(fileHeader);
        }
        if (rowCount == -1) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @RequestMapping(value = "/withdraw/info",method = RequestMethod.GET)
    public ResponseObject<String> getNowToastInfo(){
        Calendar ca = Calendar.getInstance();
        return new ResponseObject<>(ToastInfoType.byDayOfMonth(ca.get(Calendar.DAY_OF_MONTH)).info());
    }

}
