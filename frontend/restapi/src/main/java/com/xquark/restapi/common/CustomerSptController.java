package com.xquark.restapi.common;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.CustomerService;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.customerspport.CustomerSupportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class CustomerSptController extends BaseController {

  @Autowired
  private CustomerSupportService customerSupportService;

  @ResponseBody
  @RequestMapping("/customer/service")
  public ResponseObject<CustomerService> getSupport() {
    return new ResponseObject<>(customerSupportService.getSupport());
  }
}
