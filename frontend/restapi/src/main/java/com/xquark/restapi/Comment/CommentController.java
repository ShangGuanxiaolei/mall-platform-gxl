package com.xquark.restapi.Comment;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.Comment;
import com.xquark.dal.model.CommentLike;
import com.xquark.dal.model.CommentReply;
import com.xquark.dal.type.CommentType;
import com.xquark.dal.vo.CommentManagment;
import com.xquark.dal.vo.CommentVO;
import com.xquark.dal.vo.ReplyManagment;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.Comment.CommentService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by jitre on 11/13/17.
 */
@Controller
@Api(value = "comment", description = "评论管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController extends BaseController {

  private final CommentService commentService;

  @Autowired
  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }


  /**
   * 添加评论
   *
   * @return 添加评论的id
   */
  @ApiOperation(value = "添加一个评论", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE, notes = "添加一个评论,返回新增评论的id")
  @RequestMapping(value = "/comment", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<String> addComment(
      @ApiParam(value = "回复对象", required = true) final Comment comment) {
    comment.setUserId(getCurrentIUser().getId());
    commentService.addComment(comment);
    return new ResponseObject<>(comment.getId());
  }

  /**
   * 批量添加评论
   */
  @RequestMapping(value = "/comments", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseObject<Boolean> addComments(@RequestBody List<CommentForm> comments,
      Errors errors) {
    ControllerHelper.checkException(errors);
    final String userId = getCurrentIUser().getId();
    List<Comment> commentsToSave = Transformer.fromIterable(comments,
        new Function<CommentForm, Comment>() {
          @Override
          public Comment apply(CommentForm commentForm) {
            Comment comment = Transformer.fromBean(commentForm, Comment.class);
            comment.setUserId(userId);
            return comment;
          }
        });
    boolean result = commentService.addComments(commentsToSave);
    return new ResponseObject<>(result);
  }

  /**
   * 删除评论
   */
  @ApiOperation(value = "通过id删除一个评论", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id删除一个评论,参数在url路径上传递")
  @RequestMapping(value = "/comment/delete/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> deleteComment(
      @ApiParam(value = "评论的id", required = true) @PathVariable("id") final String id) {
    commentService.deleteCommentById(id);
    return new ResponseObject<>(Boolean.TRUE);
  }


  /**
   * 批量删除评论
   */
  @RequestMapping(value = "/comment/batchDelete/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> batchDeleteComment(@PathVariable("id") final String id) {

    String[] ids = id.split(",");
    commentService.batchDeleteComment(ids);

    return new ResponseObject<>(Boolean.TRUE);
  }


  /**
   * 更新评论
   */
  @ApiOperation(value = "通过id更新一个评论", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id更新一个评论,id通过路径传递")
  @RequestMapping(value = "/comment/update/{id}", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<Boolean> updateComment(
      @ApiParam(value = "评论的id", required = true) @PathVariable("id") final String id,
      @ApiParam(value = "修改后的评论内容", required = true) @RequestParam final String content) {
    Comment comment = new Comment();
    comment.setId(id);
    comment.setContent(content);
    commentService.updateCommentById(comment);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 获取web界面上的评论数据
   *
   * @param objId 评论对象的id
   * @param size 获取数据的条数
   * @param offset 偏移量
   */
  @ApiOperation(value = "获取web界面上的评论数据(包括回复)", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过参数offset和size来控制数据抓取的位置")
  @RequestMapping(value = "/comment/view/phone", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<List<CommentVO>> getCommentsListToPhone(
      @ApiParam(value = "评论对象的id", required = true) @RequestParam final String objId,
      @ApiParam(value = "从哪开始抓取(偏移)", required = true) @RequestParam final int offset,
      @ApiParam(value = "抓取的size", required = true) @RequestParam final int size) {
    List<CommentVO> commentsVO = commentService
        .getCommentsVO(objId, getCurrentIUser().getId(), offset, size);
    return new ResponseObject<>(commentsVO);
  }

  @RequestMapping(value = "/comment/productComments")
  @ResponseBody
  public ResponseObject<Map<String, Object>> productComments(@RequestParam String productId,
      Integer star,
      Pageable pageable) {
    List<CommentVO> comments = commentService
        .listCommentByProductIdAndStar(productId, star, pageable);
    Long total = commentService.countCommentByProductIdAndStar(productId, star);
    Map<String, Object> result = ImmutableMap.of("comments", comments,
        "total", total);
    return new ResponseObject<>(result);
  }

  @RequestMapping(value = "/comment/productTotalMap")
  @ResponseBody
  public ResponseObject<Map<String, Long>> productCommentsMap(@RequestParam String productId) {
    Map<String, Long> result = commentService.commentTotalMap(productId);
    return new ResponseObject<>(result);
  }

  /**
   * 获取后台界面上的评论数据
   *
   * @param type 评论的类型
   * @param blocked 是否被屏蔽
   * @param direction 降序(时间字段）
   * @param pageable 分页
   */
  @RequestMapping(value = "/comment/view/pc", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Map<String, Object>> getCommentsListToPc(final CommentType type,
      final Boolean blocked, final String direction, final Pageable pageable) {

    Boolean descByTime;
    if (direction.equals("asc")) {
      descByTime = Boolean.TRUE;
    } else {
      descByTime = Boolean.FALSE;
    }

    //获取数据
    Map<String, Object> ret = new HashMap();
//    ImmutableMap
    List<CommentManagment> commentList = commentService
        .getCommentList(type, blocked, descByTime, pageable);
    ret.put("list", commentList);

    //获取数据的条数
    Integer count = commentService.getCommentCount(type, blocked, descByTime, pageable);
    ret.put("count", count);

    return new ResponseObject<>(ret);
  }


  /**
   * 获取评论
   *
   * @return 返回空则表示不存在
   */
  @ApiOperation(value = "通过id获取一个评论", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id获取一个评论,参数通过路径传递")
  @RequestMapping(value = "/comment/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Comment> getComment(
      @ApiParam(value = "评论的id", required = true) @PathVariable("id") final String id) {
    Comment comment = commentService.getCommentById(id);

    return new ResponseObject<>(comment);
  }

  /**
   * 屏蔽一个评论
   */
  @RequestMapping(value = "/comment/block/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> blockComment(@PathVariable("id") final String id) {

    commentService.blockComment(id);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 批量屏蔽评论
   */
  @RequestMapping(value = "/comment/batchBlock/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> batchBlockComment(@PathVariable("id") final String id) {

    String[] ids = id.split(",");
    commentService.batchBlockComment(ids);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 撤销屏蔽一个评论
   */
  @RequestMapping(value = "/comment/unblock/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> unblockComment(@PathVariable("id") final String id) {

    commentService.unblockComment(id);

    return new ResponseObject<>(Boolean.TRUE);
  }


  /**
   * 批量撤销屏蔽
   */
  @RequestMapping(value = "/comment/batchUnBlock/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> batchUnBlockComment(@PathVariable("id") final String id) {

    String[] ids = id.split(",");
    commentService.batchUnBlockComment(ids);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 添加回复
   *
   * @return 添加评论的id
   */
  @ApiOperation(value = "对评论添加回复", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE, notes = "添加一个评论")
  @RequestMapping(value = "/comment/reply", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<String> addCommentReply(
      @ApiParam(value = "回复对象", required = true) final CommentReply reply) {
    reply.setId(getCurrentIUser().getId());
    commentService.addCommentReply(reply);
    return new ResponseObject<>(reply.getId());
  }

  /**
   * 删除回复
   */
  @ApiOperation(value = "通过id删除一个回复", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id删除一个回复,参数通过路径传递")
  @RequestMapping(value = "/comment/reply/delete/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> deleteCommentReply(
      @ApiParam(value = "回复的id", required = true) @PathVariable("id") final String id) {
    commentService.deleteCommentReplyById(id);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 更新回复
   */
  @ApiOperation(value = "通过id更新一个回复", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id更新一个回复,id参数通过路径传递")
  @RequestMapping(value = "/comment/reply/update/{id}", method = {RequestMethod.POST})
  @ResponseBody
  public ResponseObject<Boolean> updateCommentReply(
      @ApiParam(value = "回复的id", required = true) @PathVariable("id") final String id,
      @ApiParam(value = "更新的回复内容", required = true) @RequestParam final String content) {
    CommentReply reply = new CommentReply();
    reply.setId(id);
    reply.setContent(content);
    commentService.updateCommentReplyById(reply);

    return new ResponseObject<>(Boolean.TRUE);
  }


  /**
   * 获取回复
   *
   * @return 返回空则表示不存在
   */
  @ApiOperation(value = "通过id获取一个回复", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "通过id获取一个回复,参数通过路径传递")
  @RequestMapping(value = "/comment/reply/{id}", method = {RequestMethod.GET})
  @ResponseBody
  public ResponseObject<CommentReply> getCommentReplyById(
      @ApiParam(value = "回复的id", required = true) @PathVariable("id") final String id) {
    CommentReply reply = commentService.getCommentReplyById(id);

    return new ResponseObject<>();
  }

  /**
   * 获取回复数据，用于在后台进行管理
   *
   * @return 返回空则表示不存在
   */
  @RequestMapping(value = "/comment/{commentID}/reply", method = {RequestMethod.GET})
  @ResponseBody
  public ResponseObject<Map<String, Object>> getCommentReplyListByCommentId(
      @PathVariable("commentID") final String id) {

    //获取数据
    Map<String, Object> ret = new HashMap();
    List<ReplyManagment> replyList = commentService.getReplysList(id);
    ret.put("list", replyList);

    //获取数据的条数
    Integer count = commentService.getReplysCount(id);
    ret.put("count", count);

    return new ResponseObject<>(ret);
  }


  /**
   * 屏蔽一个回复
   */
  @RequestMapping(value = "/comment/reply/block/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> blockReply(@PathVariable("id") final String id) {

    commentService.blockReply(id);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 撤销屏蔽一个回复
   */
  @RequestMapping(value = "/comment/reply/unblock/{id}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Boolean> unblockReply(@PathVariable("id") final String id) {

    commentService.unblockReply(id);

    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 点赞一个评论
   */
  @ApiOperation(value = "点赞一个评论", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE, notes = "点赞一个评论")
  @RequestMapping(value = "/comment/like", method = RequestMethod.POST)
  @ResponseBody
  public ResponseObject<Integer> likeComment(
      @ApiParam(value = "点赞", required = true) final CommentLike like) {
    like.setUserId(getCurrentIUser().getId());
    int ret = commentService.likeComment(like);

    return new ResponseObject<>(ret);
  }

  /**
   * 取消点赞一个评论
   */
  @ApiOperation(value = "取消点赞一个评论", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE, notes = "取消点赞一个评论")
  @RequestMapping(value = "/comment/{commentId}/like", method = RequestMethod.GET)
  @ResponseBody
  public ResponseObject<Integer> dislikeComment(
      @ApiParam(value = "评论的id", required = true) @PathVariable("commentId") final String commentId/*@ApiParam(value="用户的的id",required = true)@RequestParam final String userId*/) {
    CommentLike like = new CommentLike();
    like.setCommentId(commentId);
    like.setUserId(getCurrentIUser().getId());
    int ret = commentService.cancelLikeByCommentIdAndUserId(like);

    return new ResponseObject<>(ret);
  }
}
