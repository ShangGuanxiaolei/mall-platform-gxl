package com.xquark.restapi.fragment;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.mapper.FragmentMapper;
import com.xquark.dal.model.Fragment;
import com.xquark.dal.model.FragmentImage;
import com.xquark.dal.model.ProductFragment;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.FragmentAndDescController;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.product.vo.ProductVO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class FragmentController extends FragmentAndDescController {

  @Autowired
  private ProductFragmentService productFragmentService;

  @Autowired
  private FragmentMapper fragmentMapper;

  @Value("${fragment.limit}")
  private String limit;

  @Value("${product.img.limit}")
  private String limitImgs;

  @ResponseBody
  @RequestMapping("/fragment/list")
  public ResponseObject<List<FragmentVO>> listFragmentByShop() {
    List<FragmentVO> list = getFragmentByShop(getCurrentUser().getShopId());
    return new ResponseObject<>(list);
  }

  @ResponseBody
  @RequestMapping("/fragment/{id}")
  public ResponseObject<FragmentVO> loadFragment(@PathVariable String id) {
    FragmentVO vo = fragmentService.selectById(id);
    if (vo == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, id + "片段信息不存在");
    }

    setFragmentImage(vo);
    return new ResponseObject<>(vo);
  }

  @ResponseBody
  @RequestMapping("/fragment/saveIdx")
  public ResponseObject<Boolean> saveFragmentIdx(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      log.warn("保存片段的时候，ids 不能为空");
      return new ResponseObject<>(false);
    }

    Set<Fragment> forms = new HashSet<>();
    String index = "";
    int i = 0;
    for (String s : ids) {
      i++;
      if (StringUtils.isBlank(s)) {
        index = index + i + ",";
        continue;
      }

      Fragment f = new Fragment();
      f.setId(s);
      f.setIdx(i);
      forms.add(f);
    }

    if (forms.size() != ids.length) {
      log.warn("保存片段的时候，ids 存在无效空值,为空的有" + index);
      return new ResponseObject<>(false);
    }

    fragmentService.update(forms.toArray(new Fragment[forms.size()]));
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/fragment/save")
  public ResponseObject<FragmentVO> saveFragment(@Valid @ModelAttribute FragmentForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    String shopId = getCurrentIUser().getShopId();
    //fragemnt新增中图片的最多数量
    Integer limitNum = Integer.valueOf(limitImgs);
    Integer maxFragment = form.getImgs().size();
    if (limitNum < maxFragment) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "新建片段中商品图片总数量不能大于：" + limitNum);
    }

    Fragment fragment = new Fragment();
    BeanUtils.copyProperties(form, fragment);
    fragment.setShopId(shopId);
    if (StringUtils.isBlank(form.getId())) {
      fragmentService.insert(fragment);
    } else {
      fragmentService.update(fragment);
    }

    FragmentVO result = new FragmentVO();
    BeanUtils.copyProperties(fragment, result);

    fragmentImageService.deleteByFragmentId(fragment.getId());
    List<FragmentImageVO> fragmentImageList = new ArrayList<>();
    List<String> imgs = form.getImgs();
    for (int i = 0; i < imgs.size(); i++) {
      FragmentImage bean = new FragmentImage();
      bean.setImg(imgs.get(i));
      bean.setIdx(i);
      bean.setFragmentId(fragment.getId());
      fragmentImageService.insert(bean);

      FragmentImageVO imgVo = new FragmentImageVO();
      BeanUtils.copyProperties(bean, imgVo);
      String imgUrl = resourceFacade.resolveUrl(imgVo.getImg());
      imgVo.setImgUrl(imgUrl);
      fragmentImageList.add(imgVo);
    }
    result.setImgs(fragmentImageList);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/fragment/saveShowModel")
  public ResponseObject<Boolean> saveShowModel(String id, boolean showModel) {
    Fragment fragment = new Fragment();
    fragment.setId(id);
    fragment.setShowModel(showModel);
    fragmentService.update(fragment);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/fragment/delete")
  public ResponseObject<Boolean> deleteByFragmentId(String id) {
    //productFragmentService.deleteByProductId(id); // wrong id
    fragmentImageService.deleteByFragmentId(id);
    fragmentService.delete(id);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/fragment/move-before")
  public ResponseObject<Boolean> moveBeforeFragment(String srcId, String desId) {
    fragmentService.moveBefore(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/fragment/move-after")
  public ResponseObject<Boolean> moveAfterFragment(String srcId, String desId) {
    fragmentService.moveAfter(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/productFragment/move-before")
  public ResponseObject<Boolean> moveBeforeProductFragment(String srcId, String desId) {
    productFragmentService.moveBefore(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/productFragment/move-after")
  public ResponseObject<Boolean> moveAfterProductFragment(String srcId, String desId) {
    productFragmentService.moveAfter(srcId, desId);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/productFagement/selectById")
  public ResponseObject<List<FragmentVO>> loadFagement(String productId) {
    List<FragmentVO> list = getProductFragmentList(productId);
    return new ResponseObject<>(list);
  }

  @ResponseBody
  @RequestMapping("/productFragment/save")
  public ResponseObject<Boolean> saveProductFragment(
      @Valid @ModelAttribute ProductFragmentForm form, Errors errors) {
    ControllerHelper.checkException(errors);
    String shopId = fragmentService.getCurrentUser().getShopId();
    ProductVO product = productService.load(form.getProductId());
    if (!shopId.equals(product.getShopId())) {
      throw new BizException(GlobalErrorCode.UNAUTHORIZED, "当前用户与店铺不符");
    }
    // 暂时规避客户端参数丢失导致详情被无意删除的bug  2015-07-09
    if (form.getFragmentIds() == null || form.getFragmentIds().size() == 0) {
      return new ResponseObject<>(true);
    }

    Integer limitNum = Integer.valueOf(limit);
    Integer maxFragment = form.getFragmentIds().size();
    if (limitNum < maxFragment) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "商品的段落描述不能大于：" + limitNum);
    }

    productFragmentService.deleteByProductId(form.getProductId());
    if (form.getFragmentIds() != null) {
      for (int i = 0; i < form.getFragmentIds().size(); i++) {
        String fragmentId = form.getFragmentIds().get(i);
        ProductFragment bean = new ProductFragment();
        bean.setProductId(form.getProductId());
        bean.setFragmentId(fragmentId);
        productFragmentService.insert(bean);
      }
    }
    log.info("productFragment保存成功，{}",form);
    return new ResponseObject<>(true);
  }

}
