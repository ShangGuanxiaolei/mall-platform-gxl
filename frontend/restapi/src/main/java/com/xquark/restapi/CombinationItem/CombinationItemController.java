package com.xquark.restapi.CombinationItem;

import com.xquark.dal.model.CombinationItem;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.combinationItem.CombinationItemService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: byy Date: 18-6-1. Time: 下午8:15 组合商品控制器
 */
@RestController
public class CombinationItemController {

  @Autowired
  private CombinationItemService combinationItemService;

  /**
   * 添加
   */
  @RequestMapping(value="/combinationItem/insert")
  public ResponseObject<Boolean> insert(@Valid CombinationItem combinationItem){
    boolean isSuccess =combinationItemService.insert(combinationItem);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 通过id查找
   */
  @RequestMapping(value="/combinationItem/getByKey")
  public ResponseObject<CombinationItem> getByPrimaryKey(String id){
    CombinationItem combinationItem=combinationItemService.getByPrimaryKey(id);
    return new ResponseObject<>(combinationItem);
  }

  /**
   * 更新
   */
  @RequestMapping(value="/combinationItem/update")
  public ResponseObject<Boolean> update(@Valid CombinationItem combinationItem){
    Boolean isSuccess=combinationItemService.update(combinationItem);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 通过id删除
   */
  @RequestMapping(value="/combinationItem/delete")
  public ResponseObject<Boolean> update(String id){
    Boolean isSuccess=combinationItemService.delete(id);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 批量删除
   */
  @RequestMapping(value="/combinationItem/batchDelete")
  public ResponseObject<Boolean> batchDelete(List<String> ids){
    Boolean isSuccess=combinationItemService.batchDelete(ids);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 计算数据的数量
   */
  @RequestMapping(value="/combinationItem/count")
  public ResponseObject<Long> count(SearchForm searchForm, Pageable page){
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    Long num=combinationItemService.count(params);
    return new ResponseObject<>(num);
  }

  /**
   * 分页查询
   */
  @RequestMapping(value="/combinationItem/list")
  public ResponseObject<Map<String,Object>> list(SearchForm searchForm, Pageable page){
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    List<CombinationItem> combinations=combinationItemService.list(page,params);
    Long num=combinationItemService.count(params);
    Map<String,Object> listAndNum=new HashMap<String, Object>();
    listAndNum.put("combinations",combinations);
    listAndNum.put("num",num);
    return new ResponseObject<>(listAndNum);
  }

}
