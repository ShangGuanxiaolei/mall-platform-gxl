package com.xquark.restapi;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午5:32
 */
public class MemberSequenceVO {

  private final Long cpId;

  public MemberSequenceVO(Long cpId) {
    this.cpId = cpId;
  }

  public Long getCpId() {
    return cpId;
  }

  public String getCustomerNumber() {
    if (cpId == null) {
      return "";
    }
    return String.format("%010d", cpId);
  }

}
