package com.xquark.restapi.WareHouse;

import com.xquark.dal.model.WareHouse;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.wareHouse.WareHouseService;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: shihao Date: 18-6-14. Time: 下午3:55
 */
@RestController
@RequestMapping("/warehouse")
public class WareHouseController {

  private WareHouseService wareHouseService;

  @Autowired
  public WareHouseController(WareHouseService wareHouseService) {
    this.wareHouseService = wareHouseService;
  }

  /*´*
   * 添加 仓库名称
   */
  @RequestMapping(value = "/edit")
  public ResponseObject<Boolean> insert(@Valid WareHouse wareHouse) {
    Boolean isSuccess;
    if (StringUtils.isNotBlank(wareHouse.getId())) {
      isSuccess = wareHouseService.update(wareHouse);
    } else {
      isSuccess = wareHouseService.insert(wareHouse);
    }
    return new ResponseObject<>(isSuccess);
  }


  /**
   * 删除仓库
   */
  @RequestMapping(value = "/delete/{id}")
  public ResponseObject<Boolean> delete(@NotBlank @PathVariable("id") String id) {
    Boolean isSuccess = wareHouseService.delete(id);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 批量删除仓库
   */
  @RequestMapping(value = "/batchDelete")
  public ResponseObject<Boolean> batchDelete(@NotBlank @RequestParam("ids") String id) {
    String[] ids = id.split(",");
    Boolean isSuccess = wareHouseService.batchDelete(Arrays.asList(ids));
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 根据id 查询仓库
   */

  @RequestMapping(value = "/{id}")
  public ResponseObject<WareHouse> getByPrimaryKey(@NotBlank @PathVariable("id") String id) {
    WareHouse byPrimaryKey = wareHouseService.getByPrimaryKey(id);
    return new ResponseObject<>(byPrimaryKey);
  }

    /**
     * 计算数据的数量
     */
    @RequestMapping(value = "/count")
    public ResponseObject<Long> count(SearchForm searchForm, Pageable page) {
      Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page,
          true);
        Long num = wareHouseService.count(params);
        return new ResponseObject<>(num);
    }

    /**
     * 分页查询
     */
    @RequestMapping(value = "/list")
    public ResponseObject<Map<String, Object>> list(SearchForm searchForm, Pageable page,
        boolean pageable) {
      Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page,
          pageable);
      List<WareHouse> warehouses = wareHouseService.list(params);
        Long num = wareHouseService.count(params);
        Map<String, Object> listAndNum = new HashMap<String, Object>();
      listAndNum.put("list", warehouses);
      listAndNum.put("total", num);
        return new ResponseObject<>(listAndNum);
    }

}
