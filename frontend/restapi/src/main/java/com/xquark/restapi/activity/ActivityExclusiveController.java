package com.xquark.restapi.activity;

import com.xquark.dal.model.ActivityExclusive;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.product.ActivityProductService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/activity")
public class ActivityExclusiveController extends BaseController {

    private static final Log logger = LogFactory.getLog(ActivityExclusiveController.class);

    @Autowired
    private ActivityProductService activityProductService;

    @ResponseBody
    @RequestMapping(value = "/addExclusiveProduct", method = RequestMethod.POST)
    public ResponseObject<Integer> addExclusiveProduct(@RequestParam("productId") Long productId, @RequestParam("pCode") String pCode) {
        ActivityExclusive activityExclusive = new ActivityExclusive();
        if (productId != null && pCode != null) {
            activityExclusive.setProductId(productId);
            activityExclusive.setpCode(pCode);
            activityExclusive.setStatus(1);
            activityProductService.insertActivityProduct(activityExclusive);
        } else {
            logger.info("传入参数不可为空");
        }
        return new ResponseObject<>();
    }

    @ResponseBody
    @RequestMapping(value = "/updateExclusiveStatus", method = RequestMethod.POST)
    public ResponseObject<Boolean> updateExclusiveStatus(@RequestParam("productId") Integer productId, @RequestParam("status") Integer status) {
        if(status == null && productId == null){
            logger.info("传入参数不可为空");
        }else{
            activityProductService.updateActivityProduct(productId, status);
        }
        return new ResponseObject<>();
    }
}
