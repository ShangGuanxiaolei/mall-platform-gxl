package com.xquark.restapi.team;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Team;
import com.xquark.dal.model.TeamApply;
import com.xquark.dal.model.TeamProductCommission;
import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.model.TeamShopCommissionDe;
import com.xquark.dal.model.UserTeam;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.ShopStatus;
import com.xquark.dal.status.TeamType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.TeamApplyVO;
import com.xquark.dal.vo.TeamProductCommissionVO;
import com.xquark.dal.vo.TeamRecordVO;
import com.xquark.dal.vo.TeamShopCommissionVO;
import com.xquark.dal.vo.TeamVO;
import com.xquark.dal.vo.UserTeamVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.order.OrderService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.team.TeamApplyService;
import com.xquark.service.team.TeamProductComService;
import com.xquark.service.team.TeamService;
import com.xquark.service.team.TeamShopComDeService;
import com.xquark.service.team.TeamShopComService;
import com.xquark.service.team.UserTeamService;
import com.xquark.service.zone.ZoneService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class TeamController extends BaseController {

  @Autowired
  private TeamShopComService teamShopComService;

  @Autowired
  private TeamShopComDeService teamShopComDeService;

  @Autowired
  private TeamProductComService teamProductComService;

  @Autowired
  private TeamService teamService;

  @Autowired
  private UserTeamService userTeamService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private TeamApplyService teamApplyService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ExcelService excelService;

  /**
   * 保存战队管理默认设置
   */
  @ResponseBody
  @RequestMapping("/team/saveSetting")
  public ResponseObject<Boolean> saveLevel(
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "num", required = false) String num,
      @RequestParam(value = "defaultStatus", required = false) String defaultStatus,
      @RequestParam(value = "id", required = false) String id,
      @RequestParam(value = "weekCheckbox[]", required = false, defaultValue = "") String[] weekCheckbox,
      @RequestParam(value = "weekSale[]", required = false, defaultValue = "") String[] weekSale,
      @RequestParam(value = "weekTotalAmount[]", required = false, defaultValue = "") String[] weekTotalAmount,
      @RequestParam(value = "weekLeaderAmount[]", required = false, defaultValue = "") String[] weekLeaderAmount,
      @RequestParam(value = "monthCheckbox[]", required = false, defaultValue = "") String[] monthCheckbox,
      @RequestParam(value = "monthSale[]", required = false, defaultValue = "") String[] monthSale,
      @RequestParam(value = "monthTotalAmount[]", required = false, defaultValue = "") String[] monthTotalAmount,
      @RequestParam(value = "monthLeaderAmount[]", required = false, defaultValue = "") String[] monthLeaderAmount) {
    String shopId = getCurrentIUser().getShopId();
    int result = 0;
    TeamShopCommission teamShopCommission = null;
    if (StringUtils.isEmpty(id)) {
      // 新增默认设置
      teamShopCommission = new TeamShopCommission();
      teamShopCommission.setArchive(false);
      teamShopCommission.setDefaultStatus(Boolean.valueOf(defaultStatus));
      teamShopCommission.setName(name);
      teamShopCommission.setNum(new BigDecimal(num).intValue());
      teamShopCommission.setOwnShopId(shopId);
      result = teamShopComService.insert(teamShopCommission);
    } else {
      teamShopCommission = teamShopComService.selectByPrimaryKey(id);
      teamShopCommission.setName(name);
      teamShopCommission.setNum(new BigDecimal(num).intValue());
      teamShopComService.updateByPrimaryKey(teamShopCommission);
      // 先删除战队自然周,自然月设置
      teamShopComDeService.deleteByParentId(id);
    }

    // 自然周设置
    for (int i = 0, n = weekCheckbox.length; i < n; i++) {
      TeamShopCommissionDe de = new TeamShopCommissionDe();
      de.setArchive(false);
      de.setDefaultStatus(Boolean.valueOf(weekCheckbox[i]));
      de.setLeaderAmount(new BigDecimal(weekLeaderAmount[i]));
      de.setParentId(teamShopCommission.getId());
      de.setSales(new BigDecimal(weekSale[i]));
      de.setTotalAmount(new BigDecimal(weekTotalAmount[i]));
      de.setType(TeamType.WEEK);
      teamShopComDeService.insert(de);
    }

    // 自然月设置
    for (int i = 0, n = monthCheckbox.length; i < n; i++) {
      TeamShopCommissionDe de = new TeamShopCommissionDe();
      de.setArchive(false);
      de.setDefaultStatus(Boolean.valueOf(monthCheckbox[i]));
      de.setLeaderAmount(new BigDecimal(monthLeaderAmount[i]));
      de.setParentId(teamShopCommission.getId());
      de.setSales(new BigDecimal(monthSale[i]));
      de.setTotalAmount(new BigDecimal(monthTotalAmount[i]));
      de.setType(TeamType.MONTH);
      teamShopComDeService.insert(de);
    }

    return new ResponseObject<>(true);
  }

  /**
   * 战队商品黑名单列表
   */
  @ResponseBody
  @RequestMapping("/team/productList")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<TeamProductCommissionVO> result = teamProductComService.list(pageable, params);
    Long total = teamProductComService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 删除商品黑名单
   */
  @ResponseBody
  @RequestMapping("/team/deleteProductCommission/{id}")
  public ResponseObject<Boolean> deleteProductCommission(@PathVariable String id) {
    int result = teamProductComService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查询是否已经有该商品已经设置过黑名单
   */
  @ResponseBody
  @RequestMapping("/team/checkProductCommission")
  public ResponseObject<Boolean> checkProductCommission(
      @RequestParam("productId") String productId) {
    String shopId = getCurrentIUser().getShopId();
    long count = teamProductComService.selectByProductId(shopId, productId);
    if (count > 0) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 保存推客商品黑名单
   */
  @ResponseBody
  @RequestMapping("/team/saveProductCommission")
  public ResponseObject<Boolean> saveProductCommission(
      @RequestParam("productId") String productId) {
    String shopId = getCurrentIUser().getShopId();
    int result = 0;
    TeamProductCommission commission = new TeamProductCommission();
    commission.setShopId(shopId);
    commission.setProductId(productId);
    commission.setArchive(false);
    result = teamProductComService.insert(commission);

    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 战队成员列表
   */
  @ResponseBody
  @RequestMapping("/team/memberList")
  public ResponseObject<Map<String, Object>> memberList(
      @RequestParam(value = "status", required = false) String status,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "userPhone", required = false) String userPhone,
      @RequestParam(value = "userName", required = false) String userName,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "groupId", required = false) String groupId, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();

    if (StringUtils.isNotEmpty(userName)) {
      params.put("userName", "%" + userName + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(userPhone)) {
      params.put("userPhone", "%" + userPhone + "%");
    }
    if (StringUtils.isNotEmpty(status)) {
      params.put("status", status);
    }
    if (StringUtils.isNotEmpty(groupId)) {
      params.put("groupId", groupId);
    }
    if (StringUtils.isNotEmpty(startDate)) {
      params.put("startDate", startDate);
    }
    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    params.put("shopId", shopId);
    List<TeamVO> result = teamService.list(pageable, params);
    Long total = teamService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 战队分组列表
   */
  @ResponseBody
  @RequestMapping("/team/groupList")
  public ResponseObject<Map<String, Object>> groupList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<TeamShopCommissionVO> result = teamShopComService.listNonDefaultByshopId(pageable, params);
    // 找出每个战队的明细信息，拼接出考核内容信息
    for (TeamShopCommissionVO vo : result) {
      List<TeamShopCommissionDe> des = teamShopComDeService.selectByParentId(vo.getId());
      StringBuilder sb = new StringBuilder();
      for (TeamShopCommissionDe de : des) {
        if (de.getType() == TeamType.WEEK) {
          sb.append("按自然周 :");
        } else if (de.getType() == TeamType.MONTH) {
          sb.append("按自然月 :");
        }
        sb.append("销售满" + de.getSales() + "元， 战队总提成" + de.getTotalAmount() + "元， 其中队长提" + de
            .getLeaderAmount() + "元。</br>");
      }
      vo.setContent(sb.toString());
    }

    Long total = teamShopComService.selectCntNonDefault(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  @ResponseBody
  @RequestMapping("/team/deleteGroup/{id}")
  public ResponseObject<Boolean> deleteLevel(@PathVariable String id) {
    int result = teamShopComService.deleteByPrimaryKey(id);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/team/getGroup/{id}")
  public ResponseObject<TeamShopCommission> getGroup(@PathVariable String id) {
    TeamShopCommissionVO result = teamShopComService.selectByPrimaryKey(id);
    String shopId = getCurrentIUser().getShopId();
    Map map = teamShopComService.getWeeksAndMonths(result, shopId);
    result.setWeeks((List<TeamShopCommissionDe>) map.get("weeks"));
    result.setMonths((List<TeamShopCommissionDe>) map.get("months"));
    return new ResponseObject<TeamShopCommission>(result);
  }

  @ResponseBody
  @RequestMapping("/team/disableShop/{id}")
  public ResponseObject<Boolean> disableShop(@PathVariable String id) {
    int result = teamService.updateStatus(id, ShopStatus.FROZEN.toString());
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/team/enableShop/{id}")
  public ResponseObject<Boolean> enableShop(@PathVariable String id) {
    int result = teamService.updateStatus(id, ShopStatus.ACTIVE.toString());
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/team/getInfo/{id}")
  public ResponseObject<TeamVO> getInfo(@PathVariable String id) {
    TeamVO result = teamService.load(id);
    return new ResponseObject<>(result);
  }

  /**
   * 保存战队设置
   */
  @ResponseBody
  @RequestMapping("/team/saveInfo")
  public ResponseObject<Boolean> saveInfo(@RequestParam(value = "id", required = false) String id,
      @RequestParam(value = "logo", required = false) String logo,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "groupId", required = false) String groupId,
      @RequestParam(value = "userId", required = false) String userId) {
    String shopId = getCurrentIUser().getShopId();
    int result = 0;
    Team team = new Team();
    team.setName(name);
    team.setGroupId(groupId);
    team.setLogo(logo);
    if (StringUtils.isNotEmpty(id)) {
      team.setId(id);
      result = teamService.update(team);
    } else {
      team.setArchive(false);
      team.setStatus(ShopStatus.ACTIVE);
      team.setShopId(shopId);
      team.setUserId(userId);
      result = teamService.insert(team);
      // 新增战队时，同时需要将队长插入到战队成员中
      UserTeam userTeam = new UserTeam();
      userTeam.setShopId(shopId);
      userTeam.setUserId(userId);
      userTeam.setArchive(false);
      userTeam.setStatus(ShopStatus.ACTIVE);
      userTeam.setTeamId(team.getId());
      userTeamService.insert(userTeam);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }

  }

  /**
   * 战队结算记录列表
   */
  @ResponseBody
  @RequestMapping("/team/listRecord")
  public ResponseObject<Map<String, Object>> listRecord(
      @RequestParam(value = "status", required = false) String status,
      @RequestParam(value = "startDate", required = false) String startDate,
      @RequestParam(value = "endDate", required = false) String endDate,
      @RequestParam(value = "teamName", required = false) String teamName,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(teamName)) {
      params.put("teamName", "%" + teamName + "%");
    }

    // 截止日期加一天，否则无法查询到截止日期那天的数据
    if (StringUtils.isNotEmpty(endDate)) {
      endDate = com.xquark.utils.DateUtils.addOne(endDate);
    }
    if (StringUtils.isNotEmpty(startDate)) {
      params.put("startDate", startDate);
    }
    if (StringUtils.isNotEmpty(endDate)) {
      params.put("endDate", endDate);
    }
    if (StringUtils.isNotEmpty(status)) {
      params.put("status", status);
    }

    String shopId = getCurrentIUser().getShopId();
    List<TeamRecordVO> result = teamService.listCommissionByShopId(shopId, params, pageable);
    Long total = teamService.countCommissionByShopId(shopId, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 获取订单信息
   */
  @ResponseBody
  @RequestMapping("/team/order/list")
  public ResponseObject<Map<String, Object>> listTwitterOrder(@RequestParam("status") String status,
      @RequestParam("orderNo") String orderNo,
      @RequestParam("sellerPhone") String sellerPhone, @RequestParam("startDate") String startDate,
      @RequestParam("endDate") String endDate,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    params.put("rootShopId", getCurrentIUser().getShopId());
    if (!orderNo.equals("")) {
      params.put("orderNo", orderNo);
    }
    if (!sellerPhone.equals("")) {
      params.put("sellerPhone", sellerPhone);
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (!status.equals("") && !status.equals("ALL")) {
      List<String> statusList = new ArrayList<>();
      statusList.add(status);
      params.put("status", statusList);
    }

    List<OrderVO> result = orderService.listByTeam(params, pageable);
    for (OrderVO order : result) {
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      if (order.getOrderAddress() != null) {
        List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
        String addressDetails = "";
        for (Zone zone : zoneList) {
          addressDetails += zone.getName();
        }
        addressDetails += order.getOrderAddress().getStreet();
        order.setAddressDetails(addressDetails);
      }
      String sellerName = userService.load(order.getSellerId()).getName();
      String buyerName = userService.load(order.getBuyerId()).getName();
      order.setSellerName(sellerName);
      order.setBuyerName(buyerName);
      /**List<Commission> list = commissionService.listByOrderIdAndType(order.getId(), CommissionType.PARTNER);
       BigDecimal sumCommission = new BigDecimal(0);
       for (Commission commission : list) {
       sumCommission = sumCommission.add(commission.getFee()) ;
       }
       order.setCommissionFee(sumCommission);**/

    }
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("orderTotal", orderService.countByTeam(params));
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  @ResponseBody
  @RequestMapping("/team/order/list/exportExcel")
  public void export2Excel(@RequestParam("status") String status,
      @RequestParam("orderNo") String orderNo,
      @RequestParam("sellerPhone") String sellerPhone, @RequestParam("startDate") String startDate,
      @RequestParam("endDate") String endDate, HttpServletResponse resp) {
    Map<String, Object> params = new HashMap<>();
    params.put("rootShopId", getCurrentIUser().getShopId());
    if (!orderNo.equals("")) {
      params.put("orderNo", orderNo);
    }
    if (!sellerPhone.equals("")) {
      params.put("sellerPhone", sellerPhone);
    }

    if (!startDate.equals("")) {
      params.put("startDate", startDate);
    }

    if (StringUtils.isNotBlank(endDate)) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(endDate, "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (!status.equals("") && !status.equals("ALL")) {
      List<String> statusList = new ArrayList<>();
      statusList.add(status);
      params.put("status", statusList);
    }

    List<OrderVO> result = orderService.listByTeam(params, null);
    for (OrderVO order : result) {
      String sellerName = userService.load(order.getSellerId()).getName();
      String buyerName = userService.load(order.getBuyerId()).getName();
      order.setSellerName(sellerName);
      order.setBuyerName(buyerName);
    }

    String sheetStr = "战队订单";
    String filePrefix = "teamOrderList";
    String[] secondTitle = new String[]{"创建时间", "队员名称", "队员手机号", "买家名称", "买家手机号", "订单信息", "付款金额",
        "状态"};
    String[] strBody = new String[]{"getCreatedAtStr", "getSellerName", "getSellerPhone",
        "getBuyerName", "getBuyerPhone", "getOrdrNo", "getTotalFee", "getStatusStr"};
    excelService
        .export(filePrefix, result, OrderVO.class, sheetStr, transParams2Title(params), secondTitle,
            strBody, resp,
            true);
  }

  private String transParams2Title(Map<String, Object> params) {
    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          result += "{" + key + "=" + value.toString() + "}";
          i++;
        }
      }
    }
    return result;
  }

  /**
   * 战队概况相关数据取值
   */
  @ResponseBody
  @RequestMapping("/team/getSummary")
  public ResponseObject<Map<String, Object>> getSummary() {
    Map<String, Object> summary = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    // 获取概况中的数据
    summary = teamService.getSummary(shopId);

    return new ResponseObject<>(summary);
  }

  /**
   * 队员是否在此战队中，或在其他战队中
   */
  @ResponseBody
  @RequestMapping(value = "/team/checkMember")
  public Json checkMember(@RequestParam("userId") String userId) {
    Json json = new Json();
    TeamVO vo = teamService.selectByUserId(userId);
    if (vo != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("队员已经存在于" + vo.getName() + "战队中");
    } else {
      json.setRc(Json.RC_SUCCESS);
    }
    return json;
  }

  /**
   * 新增战队队员
   */
  @ResponseBody
  @RequestMapping(value = "/team/addUser")
  public ResponseObject<Boolean> addUser(@RequestParam("userId") String userId,
      @RequestParam("id") String id) {
    String shopId = this.getCurrentIUser().getShopId();
    UserTeam userTeam = new UserTeam();
    userTeam.setShopId(shopId);
    userTeam.setUserId(userId);
    userTeam.setArchive(false);
    userTeam.setStatus(ShopStatus.ACTIVE);
    userTeam.setTeamId(id);
    int result = userTeamService.insert(userTeam);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 某个战队里的战队成员列表
   */
  @ResponseBody
  @RequestMapping("/team/teamMemberList")
  public ResponseObject<Map<String, Object>> memberList(
      @RequestParam(value = "teamId", required = false) String teamId, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("teamId", teamId);
    params.put("shopId", shopId);
    List<UserTeamVO> result = userTeamService.listByTeamId(pageable, params);
    Long total = userTeamService.selectCntByTeamId(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 意向客户查询
   */
  @ResponseBody
  @RequestMapping("/team/applyList")
  public ResponseObject<Map<String, Object>> applyList(
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "phone", required = false) String phone,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (StringUtils.isNotEmpty(phone)) {
      params.put("phone", "%" + phone + "%");
    }
    params.put("shopId", shopId);
    List<TeamApplyVO> result = teamApplyService.list(pageable, params);
    Long total = teamApplyService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 激活意向客户
   */
  @ResponseBody
  @RequestMapping("/team/enableApply/{id}")
  public ResponseObject<Boolean> enableApply(@PathVariable String id) {
    TeamApply store = new TeamApply();
    store.setId(id);
    store.setStatus(AgentStatus.ACTIVE);
    int result = teamApplyService.update(store);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 删除意向客户
   */
  @ResponseBody
  @RequestMapping("/team/deleteApply/{id}")
  public ResponseObject<Boolean> enable(@PathVariable String id) {
    int result = teamApplyService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 保存意向客户
   */
  @ResponseBody
  @RequestMapping("/team/saveApply")
  public Json saveApply(@RequestParam("shopId") String shopId, @RequestParam("name") String name,
      @RequestParam("phone") String phone) {
    String userId = getCurrentIUser().getId();
    Json json = new Json();
    TeamApply vo = teamApplyService.selectByUserId(userId);
    if (vo != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("您已经提交过申请，请不要重复提交");
      return json;
    }

    // shopid代表意向客户的上级战队队员
    Shop shop = shopService.load(shopId);
    if (shop == null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("队员店铺不存在，请确认");
      return json;
    }
    String parentUserId = shop.getOwnerId();
    Team team = teamService.selectByUserId(parentUserId);
    if (team == null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("战队不存在，请确认");
      return json;
    }
    TeamApply apply = new TeamApply();
    apply.setStatus(AgentStatus.APPLYING);
    apply.setArchive(false);
    apply.setPhone(phone);
    apply.setName(name);
    apply.setParentUserId(parentUserId);
    apply.setTeamId(team.getId());
    apply.setUserId(userId);
    teamApplyService.insert(apply);

    json.setRc(Json.RC_SUCCESS);
    json.setMsg("提交成功");
    return json;
  }

}