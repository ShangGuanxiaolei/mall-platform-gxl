package com.xquark.restapi.category;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CategoryStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.ProductBasicVO;
import com.xquark.dal.vo.ProductBlackListVO;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cache.GenerateKeyUtil;
import com.xquark.service.cache.GuavaCacheManager;
import com.xquark.service.category.CategoryService;
import com.xquark.service.category.TermTaxonomyService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductBlackListService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSearchVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.RolePriceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Controller
@Api(value = "category", description = "商品类别管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController extends BaseController {

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private TermTaxonomyService termTaxonomyService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ProductService productService;

  @Autowired
  private ActivityService activityService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private UrlHelper urlHelper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private ProductDistributorMapper productDistributorMapper;

  @Autowired
  private CategoryMapper categoryMapper;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private RolePriceService rolePriceService;

  @Autowired
  private ProductBlackListService productBlackListService;

  @Autowired
  private ShopService shopService;

  @Value("${site.web.host.name}")
  private String siteHost;

  @Autowired
  private GuavaCacheManager guavaCacheManager;

  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private PromotionConfigMapper promotionConfigMapper;
  /**
   * 取类目下面的商品,排序 http://localhost:8888/v2/category/product/list?id=1302
   */
  @ResponseBody
  @RequestMapping("/category/product/list")
  @ApiIgnore
  public ResponseObject<Map<String, Object>> products(@RequestParam String id, Boolean isDesc,
      String orderName, Integer size,
      Integer page) {
    Pageable pageable = initPage(page, size, orderName, isDesc);

    List<Product> prods = null;
    long count = 0;
    if ("0".equals(id)) {
      // 获取未分类的商品
      // http://localhost:8888/v2/category/product/list?id=0&page=0&size=4&orderName=price
      prods = categoryService.listUnCategoriedProducts(pageable);
      count = categoryService.countUnCategoriedProducts();
    } else if ("-1".equals(id)) {
      // http://localhost:8888/v2/category/product/list?id=-1&page=0&size=4&orderName=market_price&isDesc=false
      // 获取全部分类
      // 全部商品按上架时间显示
      prods = productService.listAllProductsByOnsaleAt(getCurrentUser().getShopId(), pageable);
      count = productService.countAllShopProduct(getCurrentUser().getShopId());
    } else {
      // 获取分类
      Category category = termTaxonomyService.load(id);
      if (category == null) {
        throw new BizException(GlobalErrorCode.NOT_FOUND, "分类不存在");
      }
      // 获取分类下的所有商品
      prods = categoryService.listUnCategoriedProducts(id, pageable);
      count = categoryService.countProductsUnderCategory(id);
    }

    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("categoryTotal", count);
    aRetMap.put("list", generateImgUrl(prods, id));
    aRetMap.put("page", pageable);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 取类目下面的商品,排序 http://localhost:8888/v2/category/product/listUnCategoried?id=1302
   */
  @ResponseBody
  @RequestMapping("/category/product/listUnCategoried")
  @ApiIgnore
  public ResponseObject<Map<String, Object>> productsUnCategoried(@RequestParam String id,
      Boolean isDesc, String orderName, Integer size,
      Integer page) {
    Pageable pageable = initPage(page, size, orderName, isDesc);
    List<Product> prods = null;
    long count = 0;
    // 获取分类
    Category category = termTaxonomyService.load(id);
    if (category == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "分类不存在");
    }
    // 获取分类下的所有商品
    prods = categoryService.listUnCategoryProducts(id, pageable);
    count = categoryService.countProductsUnderCategory(id);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("categoryTotal", count);
    aRetMap.put("list", generateImgUrl(prods, id));
    aRetMap.put("page", pageable);
    return new ResponseObject<>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/categories/{id}/products")
  @ApiIgnore
  public ResponseObject<List<ProductVO>> products2(@PathVariable String id,
      HttpServletRequest req, Pageable pageable) {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.error("user does not login");
    }
    Activity activity = activityService.load(id);
    if (activity == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "分类不存在");
    }
    // Pageable pageable = new PageRequest(page - 1, DEFAULT_PAGE_SIZE);
    List<Product> prods = productService.listProductsByActId(id, pageable);
    List<ProductVO> products = new ArrayList<>();
    for (Product p : prods) {
      ProductVO prodVO = new ProductVO(p);
      prodVO.setImgUrl(p.getImg());
      if (user != null) {
        prodVO.setUrl(siteHost + "/p/" + p.getId() + "?union_id=" + user.getId());
      } else {
        prodVO.setUrl(siteHost + "/p/" + p.getId());
      }
      prodVO.setSales(prodVO.getFakeSales()); // 使用fake数据
      prodVO.setCommission(prodVO.getPrice().multiply(prodVO.getCommissionRate()));
      products.add(prodVO);
    }
    return new ResponseObject<>(products);
  }

  @ResponseBody
  @RequestMapping("/category/{id}")
  @ApiOperation(value = "根据id获取分类信息", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Category> loadCategoryById(@PathVariable("id") String id) {
    Category ret = categoryService.load(id);
    if (ret == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "该分类不存在");
    }
    return new ResponseObject<>(ret);
  }

  /**
   * 编辑/修改商品的分类 http://localhost:8888/v2/category/product/add?categoryId=3&productIds[0]=1r69lc0p
   */
  @ResponseBody
  @RequestMapping("/category/product/add")
  @ApiIgnore
  public ResponseObject<Boolean> addProduct(@Valid @ModelAttribute AddOrRemoveProductForm form) {
    categoryService.addProductsCategory(form.getProductIds(), form.getCategoryId());
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/category/product/remove")
  @ApiIgnore
  public ResponseObject<Boolean> removeProduct(@Valid @ModelAttribute AddOrRemoveProductForm form) {
    categoryService.removeProductsCategory(form.getProductIds(), form.getCategoryId());
    return new ResponseObject<>(true);
  }


  /*
   * 封装page对象 sort结构示例 Iterator<org.springframework.data.domain.Sort.Order>
   * orders = page.getSort().iterator(); if(orders.hasNext()){
   * orders.next().getDirection(); orders.next().getProperty() }
   */
  protected Pageable initPage(Integer page, Integer size, String orderName,
      Boolean isDesc) {
    if (size == null) {
      size = 10;
    }

    if (page == null) {
      page = 0;
    }
    if (isDesc != null || !StringUtils.isBlank(orderName)) {
      Direction orderType = Direction.DESC;
      if (isDesc != null && !isDesc) {
        orderType = Direction.ASC;
      }

      return new PageRequest(page, size, orderType, orderName);
    }
    return new PageRequest(page, size);
  }

  /**
   * 返回类别上下级json数据
   */
  @ResponseBody
  @RequestMapping("/category/list")
  @ApiIgnore
  public List listCategory(@RequestParam(required = false) Taxonomy taxonomy) {
    taxonomy = Optional.fromNullable(taxonomy).or(Taxonomy.GOODS);
    return categoryService.listAllCategorys(null, taxonomy);
  }


  /**
   * 保存商品的分类信息
   */
  @ResponseBody
  @RequestMapping("/category/save")
  @ApiIgnore
  public ResponseObject<Category> save(@RequestParam String name, @RequestParam String parentid,
      @RequestParam(value = "taxonomy", required = false) Taxonomy taxonomy) {
    taxonomy = Optional.fromNullable(taxonomy).or(Taxonomy.GOODS);
    if ("0".equals(parentid)) {
      parentid = null;
    }
    Category goods = categoryService.saveUserGoods(name, parentid, taxonomy);
    return new ResponseObject<>(goods);
  }

  /**
   * 修改分类
   */
  @ResponseBody
  @RequestMapping("/category/updateName")
  @ApiIgnore
  public ResponseObject<CategoryVO> updateName(@RequestParam String id, @RequestParam String name) {
    Category cat = categoryService.updateUserGoodsName(id, Taxonomy.GOODS, name);
    // 总店类别更新后，同时更新店铺上架复制出来的品牌类别
    categoryService.updateCopyCategory(id);
    CategoryVO vo = new CategoryVO();
    BeanUtils.copyProperties(cat, vo);
    long productCount = categoryService.countUserGoodsProducts(cat.getId());
    vo.setProductCount(productCount);
    return new ResponseObject<>(vo);
  }

  /**
   * 删除分类
   */
  @ResponseBody
  @RequestMapping("/category/delete")
  @ApiIgnore
  public void delete(@RequestParam String id) {
    categoryService.delete(id);
    // 总店类别删除后，同时删除店铺上架复制出来的品牌类别
    categoryService.deleteCopyCategory(id);
  }

  /**
   * 移动分类
   */
  @ResponseBody
  @RequestMapping("/category/move")
  @ApiIgnore
  public void delete(@RequestParam String id, @RequestParam String parentid,
      @RequestParam String position, @RequestParam String sourceposition,
      @RequestParam String sourceparentid) {
    if ("0".equals(parentid)) {
      parentid = null;
    }
    if ("0".equals(sourceparentid)) {
      sourceparentid = null;
    }
    categoryService.move(id, parentid, position, sourceposition, sourceparentid);
  }


  /**
   * 上传分类图片
   */
  @ResponseBody
  @RequestMapping("/category/updateImg")
  @ApiIgnore
  public Map updateImg(@RequestParam("excelFile") MultipartFile file, @RequestParam String id)
      throws IOException {
    HashMap map = new HashMap();
    String imgurl = "";
    if (!file.isEmpty()) {
      InputStream fileInputStream = file.getInputStream();
      try {
        UpLoadFileVO vo = resourceFacade.uploadFileStream(fileInputStream, FileBelong.PRODUCT);
        String img = vo.getId();
        categoryService.updateImg(img, id);
        // 总店类别更新后，同时更新店铺上架复制出来的品牌类别
        categoryService.updateCopyCategory(id);
        imgurl = resourceFacade.resolveUrl(img);
      } catch (Exception e) {
        e.toString();
      }
    }
    map.put("img", imgurl);
    return map;
  }

  /**
   * 上传类别小图片
   */
  @ResponseBody
  @RequestMapping("/category/updateTinyImg")
  @ApiIgnore
  public Map updateTinyImg(@RequestParam("excelFile") MultipartFile file, @RequestParam String id)
      throws IOException {
    HashMap map = new HashMap();
    String imgurl = "";
    if (!file.isEmpty()) {
      InputStream fileInputStream = file.getInputStream();
      try {
        UpLoadFileVO vo = resourceFacade.uploadFileStream(fileInputStream, FileBelong.PRODUCT);
        String img = vo.getId();
        categoryService.updateTinyImg(img, id);
        // 总店类别更新后，同时更新店铺上架复制出来的品牌类别
        // categoryService.updateCopyCategory(id);
        imgurl = resourceFacade.resolveUrl(img);
      } catch (Exception e) {
        e.toString();
      }
    }
    map.put("img", imgurl);
    return map;
  }

  /**
   * v2/category/list/{level} level == 0 所有 level == 1 一级分类 level == 2 二级分类 获取总店分类列表
   */
  @ResponseBody
  @RequestMapping(value = "/category/list/{level}", method = RequestMethod.GET)
  @ApiIgnore
  public ResponseObject<List<CategoryVO>> categoryList(@PathVariable int level,
      @RequestParam(required = false) String shopId,
      @RequestParam(required = false) String parentCategoryId,
      @RequestParam(required = false) Taxonomy taxonomy) {
    taxonomy = Optional.fromNullable(taxonomy).or(Taxonomy.GOODS);
    List<CategoryVO> list = new ArrayList<>();
    List<Category> categories;
    String rootShopId = shopService.loadRootShop().getId();
    categories = categoryService.loadCategoriesByShopId(rootShopId, level, parentCategoryId,
        taxonomy);

    //是否过滤分类下为空的数据
    boolean categoryFlag = getCategoryProductSwitch();
    for (Category cat : categories) {
      CategoryVO vo = new CategoryVO();
      BeanUtils.copyProperties(cat, vo);
      vo.setCategoryImgUrl(cat.getImg());
      vo.setDisplayName(vo.getName());
      long productCount = categoryService.countUserGoodsProducts(cat.getId());
      vo.setProductCount(productCount);
      long sellerCount = categoryService.countUserCategorySellers(shopId, cat.getId());
      vo.setSellerCount(sellerCount);
      Category categoryOnsale = categoryMapper.loadBySourceIdAndShopId(cat.getId(), shopId);
      if (categoryOnsale != null && categoryOnsale.getStatus().equals(CategoryStatus.ONSALE)) {
        vo.setOnSaleForSeller(true);
      } else {
        vo.setOnSaleForSeller(false);
      }
      //分类下所有商品列表
      //http://51shop.mobi/shop/brand/cabvysu7/6689?root=true&currentSellerShopId=cabvysu7
      vo.setCategoryH5Url(
          siteHost + "/shop/brand/" + shopId + "/" + cat.getId() + "?root=true&currentSellerShopId="
              + shopId);

      if(categoryFlag){
        boolean b = this.checkCategoryIsNull(rootShopId,cat.getId(),level);
        if(b){
          continue;
        }
      }
      list.add(vo);
    }
    return new ResponseObject<>(list);
  }

  /**
   * 是否过滤的开关
   * @return
   */
  private boolean getCategoryProductSwitch(){
    Map<String, String> map = new HashMap<>();
    map.put("configType", "product");
    map.put("configName", "category_product_filter_flag");
    PromotionConfig p = promotionConfigMapper.selectByConfig(map);

    if(p != null && "true".equals(p.getConfigValue()))
      return true;

    return false;
  }
  private boolean checkCategoryIsNull(String rootShopId, String id,int level) {

    if(level == 1 && "0".equals(id))//id=0说明是全部
      return false;
    this.initPage(0, 1, Direction.DESC.name(), false);
    PageRequest pageRequest = new PageRequest(0, 1);
    // 默认查找商品
    ProductType[] type = new ProductType[]{ProductType.NORMAL, ProductType.COMBINE};
    try {
      List<Product> productList = productMapper
            .listProductsBySaleAt(rootShopId, id, null, pageRequest,
                    Direction.fromString(Direction.DESC.name()), null, null, null, null,
                    null, type);
      if(productList == null
      || productList.isEmpty() || productList.size() == 0){
        return true;
      }
    }catch (Exception e){
      return false;
    }
    return false;
  }


  /**
   * 品牌下分类查找 获取总店分类列表
   */
  @ResponseBody
  @RequestMapping("/category/search")
  @ApiIgnore
  public ResponseObject<List<CategoryVO>> categoryList(
      @RequestParam(required = false) String shopId, @RequestParam(required = true) String name) {
    String rootShopId;
    List<CategoryVO> list = new ArrayList<>();
    List<Category> categories;
    if (shopId == null || "".equals(shopId)) {
      shopId = this.getCurrentIUser().getShopId();
    }
    Category brand = categoryService.loadByName("品牌团");
    String parentCategoryId = brand.getId();
    rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    if (name != null) {
      name = "%" + name + "%";
    }
    categories = categoryService.listByName(rootShopId, name, parentCategoryId);

    for (Category cat : categories) {
      CategoryVO vo = new CategoryVO();
      BeanUtils.copyProperties(cat, vo);
      vo.setCategoryImgUrl(cat.getImg());
      vo.setDisplayName(vo.getName());
      long productCount = categoryService.countUserGoodsProducts(cat.getId());
      vo.setProductCount(productCount);
      long sellerCount = categoryService.countUserCategorySellers(shopId, cat.getId());
      vo.setSellerCount(sellerCount);
      vo.setCategoryH5Url(
          siteHost + "/shop/brand/" + shopId + "/" + cat.getId() + "?root=true&currentSellerShopId="
              + shopId);
      list.add(vo);
    }
    return new ResponseObject<>(list);
  }

  @ResponseBody
  @RequestMapping(value = "catalog/allBoundedProducts", method = RequestMethod.POST)
  public ResponseObject<Map<String, ?>> allBoundedProducts(Pageable pageable) {
    String userId = getCurrentIUser().getId();
    List<ProductBasicVO> list = productService.listBoundedProductsWithFilters(userId, pageable);
    Long total = productService.countBoundedProducts(userId);
    return new ResponseObject<Map<String, ?>>(ImmutableMap.of("list", list, "total", total));
  }

  /**
   * 查询某个分类下所有商品
   */
  @ResponseBody
  @RequestMapping(value = "/catalog/allProducts", method = RequestMethod.POST)
    @ApiOperation(value = "查询某个分类下所有商品", notes = "查询某个分类下所有商品", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> allProducts(
      @ApiParam(value = "分类id") @RequestParam(value = "categoryId", required = false) String categoryId,
      @ApiParam(value = "排序字段（default代表上架时间，sales代表销量，price代表价格，point积分）") @RequestParam(value = "order", required = false) String order,
      @ApiParam(value = "排序类型（desc降序，asc升序）") @RequestParam(value = "direction", required = false) String direction,
      @ApiParam(value = "不需要传入") @RequestParam(value = "includeBlackList", required = false) String includeBlackList,
      @ApiParam(value = "分类下面的小类（支持多个小类）") @RequestParam(value = "subCategoryIds", required = false) List<String> subCategoryIds,
      @ApiParam(value = "价格区间开始") @RequestParam(value = "priceStart", required = false) String priceStart,
      @ApiParam(value = "价格区间结束") @RequestParam(value = "priceEnd", required = false) String priceEnd,
      Pageable pageable, HttpServletRequest req,
      ProductSearchVO productSearchVO) {

      if(Objects.isNull(productSearchVO)){
          productSearchVO = new ProductSearchVO();
      }
      String  key = GenerateKeyUtil.generateProductKey(categoryId, order, direction, includeBlackList, subCategoryIds, priceStart, priceEnd, pageable);
    if (GenerateKeyUtil.FIRST_PAGE_KEY.equals(key)) {
        final Map<String, Object> retMap = (Map<String, Object>) guavaCacheManager.get(key);
        if (retMap != null) {
            return new ResponseObject(retMap) ;
        }
    }


    // 默认查找商品
    ProductType[] type = new ProductType[]{ProductType.NORMAL, ProductType.COMBINE};
    // 默认       default    销量
    // 利润高到低  cm
    // 价格高到低  price
    // 价格低到高  price
    List<Product> categoryProducts = new ArrayList<>();
    direction = StringUtils.defaultIfBlank(direction, "desc").toLowerCase();
    order = StringUtils.defaultIfBlank(order, "default").toLowerCase();
    User user = getCurrentUser();
    String sellerShopId = user.getCurrentSellerShopId();
    String rootShopId = shopService.loadRootShop().getId();
    String groupon = "";
    if (StringUtils.isEmpty(categoryId) || categoryId.equals("0")) {
      categoryId = "";
    }

    sellerShopId = "";

    switch (order) {
      case "default":
        categoryProducts = productService
            .listProductsBySaleAt(rootShopId, categoryId, groupon, pageable,
                Direction.fromString(direction), sellerShopId, subCategoryIds, priceStart, priceEnd,
                    productSearchVO,type);
        break;
      case "sales":
        categoryProducts = productService
            .listProductsBySales(rootShopId, categoryId, groupon, pageable,
                Direction.fromString(direction), sellerShopId, subCategoryIds, priceStart, priceEnd,
                null, null,productSearchVO, type);
        break;
      case "price":
        categoryProducts = productService
            .listProductsByPrice(rootShopId, categoryId, groupon, pageable,
                Direction.fromString(direction), sellerShopId, subCategoryIds, priceStart, priceEnd
                ,productSearchVO,type);
        break;
      case "point":
        categoryProducts = productService
                .listProductsByPoint(rootShopId, categoryId, groupon, pageable,
                        Direction.fromString(direction), sellerShopId, subCategoryIds, priceStart, priceEnd,
                        type);
        break;
    }

    //过滤是否有货（即校验库存）
      if(!Objects.isNull(productSearchVO.getStockKey())
              && productSearchVO.getStockKey() == 1
              && categoryProducts != null){
          categoryProducts = this.productService.filterIsStockByPVo(categoryProducts);
      }

    List<ProductVOEx> productVOExes = generateImgUrls(categoryProducts, rootShopId, req);
    // 判断商品是否已经在商品黑名单中
    if ("1".equals(includeBlackList)) {
      String userId = getCurrentIUser().getId();
      Map<String, Object> params = new HashMap<>();
      params.put("userId", userId);
      // 获取该用户设置的黑名单商品
      List<ProductBlackListVO> blackListVOs = productBlackListService.list(null, params);
      for (ProductVOEx ex : productVOExes) {
        String productId = ex.getId();
        ex.setInBlackList(isInBlackList(productId, blackListVOs));
      }
    }

    long count = productService
        .countProductsByCategoryAndPrice(rootShopId, categoryId, subCategoryIds, priceStart,
            priceEnd,
            type);

    Boolean isProxy = user.getIsProxy();
    Boolean isMember = user.getIsMember();

    for (ProductVOEx p : productVOExes) {
      String pid;
      if (StringUtils.isNotBlank(pid = p.getId())) {
        Sku sku=skuMapper.selectMinPriceSkuByProductId(pid);
        if (sku!=null) {
          p.setDeductionDPoint(sku.getDeductionDPoint());
          p.setPoint(sku.getPoint());
          p.setPrice(sku.getPrice());

          BigDecimal subtractValue = p.getPoint().add(sku.getServerAmt());
          BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2,1);
          p.setProxyPrice(proxyPrice);

          BigDecimal conversionPrice = p.getConversionPrice();
          p.setMemberPrice(sku.getPrice().subtract(p.getPoint()).setScale(2,1));

          if(isProxy) {
            p.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2,1));
          }else if(isMember){
            p.setChangePrice(conversionPrice.subtract(p.getPoint()).setScale(2,1));
          }else{
            p.setChangePrice(conversionPrice);
          }

          p.setAmount(sku.getAmount());
        }
      }
    }

    // 沁园分类查询除了返回结果还需返回商品总数量
    Map map = new HashMap();
    map.put("list", productVOExes);
    map.put("count", count);

    //缓存key
    if (GenerateKeyUtil.FIRST_PAGE_KEY.equals(key)) {
        guavaCacheManager.putIfExpire(key,map);
    }
    return new ResponseObject<>(map);
  }


  /**
   * 查询某个分类下商品总数
   */
  @ResponseBody
  @RequestMapping(value = "/catalog/countProductByCategory", method = RequestMethod.POST)
  @ApiOperation(value = "查询某个分类下商品总数", notes = "查询某个分类下商品总数", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Long> countProductByCategory(
      @ApiParam(value = "分类id") @RequestParam(value = "categoryId", required = false) String categoryId,
      HttpServletRequest req) {
    long count = 0;
    String rootShopId = shopService.loadRootShop().getId();
    productService.countCategoryProducts(rootShopId, categoryId);
    return new ResponseObject<>(count);
  }

  private boolean isInBlackList(String productId, List<ProductBlackListVO> blackListVOs) {
    boolean flag = false;
    for (ProductBlackListVO vo : blackListVOs) {
      if (vo.getProductId().equals(productId)) {
        flag = true;
        break;
      }
    }
    return flag;
  }

  private List<ProductVOEx> generateImgUrls(List<Product> products, String shopId,
      HttpServletRequest req) {
    List<ProductVOEx> exs = new ArrayList<>();
    ProductVOEx ex = null;
    Activity activity = null;
    if (products == null) {
      return exs;
    }
    for (Product product : products) {

      ex = new ProductVOEx(new ProductVO(product), getProductTargetUrl(req, product.getId()),
          product.getImg(), null);

      // 设置进货类型的商品详情页
      //ex.setB2bProductUrl(getB2bProductShareUrl(req,product.getCode()));

      //fixme
      if (product.getCommissionRate() != null) {
        ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
      }
      //商品卖家端是否上架
      ProductDistributor productDistributor = productDistributorMapper
          .selectByProductIdAndShopId(product.getId(), shopId);

      //有记录并且状态为上架
      if (productDistributor != null && ProductStatus.ONSALE
          .equals(productDistributor.getStatus())) {
        ex.setOnSaleForSeller(true);
      } else {
        ex.setOnSaleForSeller(false);
      }
      //多少卖家在卖
      long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
      ex.setCountSeller(count);

      // 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
      // 利润为商品原价和经销商价格之差
      String productId = product.getId();
      String userId = this.getCurrentIUser().getId();
      //判断当前用户是否在代理角色中
      UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
      if (userAgentVO != null) {
        String role = userAgentVO.getRole();
        RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
        if (rolePriceVO != null) {
          BigDecimal oriPrice = ex.getPrice();
          BigDecimal rolePrice = rolePriceVO.getPrice();
          ex.setPrice(rolePrice);
          ex.setCommission(oriPrice.subtract(rolePrice));
        }
      }

      //ex.setProductUrl(siteHost + "/p/" + ex.getId() + "?union_id=" + getCurrentUser().getId());
      exs.add(ex);
    }
    return exs;
  }

  private List<ProductVOEx> generateImgUrl(List<Product> products, String categoryId) {
    List<ProductVOEx> exs = new ArrayList<>();
    ProductVOEx ex = null;
    for (Product product : products) {
      ex = new ProductVOEx(new ProductVO(product), urlHelper.genProductUrl(product.getId()),
          product.getImg(), null);
      //ex.setCategory(categoryService.loadCategoryByProductId(product.getId()));
      ex.setCategory(categoryService.selectVoById(categoryId));
      exs.add(ex);
    }
    return exs;
  }

  /**
   * 分类上架
   */
  @ResponseBody
  @RequestMapping("/category/onsale")
  @ApiIgnore
  public ResponseObject<Boolean> categoryOnsale(@RequestParam String id,
      @RequestParam(required = false) String shopId) {
    return new ResponseObject<>(categoryService.addCategory(id, shopId));
  }

  /**
   * 分类下架
   */
  @ResponseBody
  @RequestMapping("/category/instock")
  @ApiIgnore
  public ResponseObject<Boolean> categoryUpdate(@RequestParam String id) {
    boolean result = false;
    String shopId = getCurrentUser().getShopId();
    Category category = categoryMapper.loadBySourceIdAndShopId(id, shopId);
    if (category != null) {
      result = categoryService.updateForInstock(category.getId());
    }
    return new ResponseObject<>(result);
  }

  private String getProductTargetUrl(HttpServletRequest req, String code) {
    return req.getScheme() + "://" + req.getServerName() + "/p/" + code;
  }

  /**
   * 保存类别颜色
   */
  @ResponseBody
  @RequestMapping("/category/updateColor")
  @ApiIgnore
  public ResponseObject<Boolean> updateColor(@RequestParam String id,
      @RequestParam(required = false) String color) {
    categoryService.updateFontColor(color, id);
    return new ResponseObject<>(true);
  }

  @ResponseBody
  @RequestMapping("/category/findChildren")
  @ApiOperation(value = "获取某个分类的下级所有分类", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Category>> findChildren(@RequestParam String id) {
    List<Category> ret = categoryService.loadSubs(id);
    return new ResponseObject<>(ret);
  }

  /**
   * 领取完成后商品推荐-销量最高
   */
  @ResponseBody
  @RequestMapping(value = "/salesproducts", method = RequestMethod.GET)
  public ResponseObject<Map> getSalesProducts(HttpServletRequest req) {

    Map resultMap = new HashMap(4);
    List<Product> productList = new ArrayList<>();
    Product product = null;
    User user = getCurrentUser();
    String rootShopId = shopService.loadRootShop().getId();

    List<Category> categoryList = categoryService.getAllCategoryIdList();
    for (Category category : categoryList) {
      product = productService.productsBySale(category.getId(), rootShopId);
      if (product == null){
        continue;
      }
      productList.add(product);
    }

    List<ProductVOEx> productVOExes = generateImgUrls(productList, rootShopId, req);

    Boolean isProxy = user.getIsProxy();
    Boolean isMember = user.getIsMember();

    for (ProductVOEx p : productVOExes) {
      String pid;
      if (StringUtils.isNotBlank(pid = p.getId())) {
        List<Sku> skus = skuMapper.selectByProductId(pid);
        if (CollectionUtils.isNotEmpty(skus)) {
          Sku sku = skus.get(0);
          p.setDeductionDPoint(sku.getDeductionDPoint());
          p.setPoint(sku.getPoint());
          p.setPrice(sku.getPrice());

          BigDecimal subtractValue = p.getPoint().add(sku.getServerAmt());
          BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2,1);
          p.setProxyPrice(proxyPrice);

          BigDecimal conversionPrice = p.getConversionPrice();
          p.setMemberPrice(sku.getPrice().subtract(p.getPoint()).setScale(2,1));

          if(isProxy) {
            p.setChangePrice(conversionPrice.subtract(subtractValue).setScale(2,1));
          }else if(isMember){
            p.setChangePrice(conversionPrice.subtract(p.getPoint()).setScale(2,1));
          }else{
            p.setChangePrice(conversionPrice);
          }
          p.setAmount(sku.getAmount());
        }
      }
    }

    //去重过滤
    Set<ProductVOEx> productVOExSet = new LinkedHashSet<>(productVOExes);

    resultMap.put("list", productVOExSet);
    resultMap.put("count", productVOExSet.size());
    return new ResponseObject<>(resultMap);
  }
}