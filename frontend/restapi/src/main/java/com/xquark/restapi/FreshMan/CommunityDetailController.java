package com.xquark.restapi.FreshMan;

import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.freshman.FirstOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 社区详情验证接口
 *
 * @author tanggb
 * @date 2019/03/08 09:58
 * @version 1.0
 */
@Controller
public class CommunityDetailController extends BaseController {

    @Autowired
    private FirstOrderService firstOrderService;

    @ResponseBody
    @RequestMapping(value = "/community/freshmanCheck", method = RequestMethod.GET)
    public ResponseObject<Map> getRecommendProducts(@RequestParam("cpId") Long cpId) {
        Map map = new HashMap<>();
        //验证是否为白人，首次下单，再确定是否弹出新人专区
        Boolean firstOrder = firstOrderService.firstOrder(cpId);
        map.put("isShow",firstOrder);
        return new ResponseObject<>(map);
    }
}
