package com.xquark.restapi.base;

import com.xquark.dal.BaseEntity;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.base.TemplateBaseService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User: huangjie Date: 2018/6/26. Time: 下午6:45
 */
@Validated
public abstract class BaseController<T extends BaseEntity, S extends TemplateBaseService<T>> {

  /***
   * hook,调用子类依赖的service
   * @return
   */
  abstract protected S getService();


  /**
   * 保存与修改
   */
  @RequestMapping(value = "/edit")
  public ResponseObject<Boolean> insert(@Valid @RequestBody T obj) {
    Boolean isSuccess;
    if (StringUtils.isNotBlank(obj.getId())) {
      isSuccess = getService().update(obj);
    } else {
      isSuccess = getService().insert(obj);
    }
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 删除
   */
  @RequestMapping(value = "/delete/{id}")
  public ResponseObject<Boolean> delete(@NotBlank @PathVariable("id") String id) {
    Boolean isSuccess = getService().delete(id);
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 批量删除
   */
  @RequestMapping(value = "/batchDelete")
  public ResponseObject<Boolean> batchDelete(@NotBlank @RequestParam("ids") String id) {
    String[] ids = id.split(",");
    Boolean isSuccess = getService().batchDelete(Arrays.asList(ids));
    return new ResponseObject<>(isSuccess);
  }

  /**
   * 根据id 查询仓库
   */

  @RequestMapping(value = "/{id}")
  public ResponseObject<? extends T> getByPrimaryKey(@NotBlank @PathVariable("id") String id) {
    T byPrimaryKey = getService().getByPrimaryKey(id);
    return new ResponseObject<>(byPrimaryKey);
  }

  /**
   * 计算数据的数量
   */
  @RequestMapping(value = "/count")
  public ResponseObject<Long> count(SearchForm searchForm, Pageable page) {
    Map<String, Object> params = SearchFormUtils.checkAndGenerateMapParams(searchForm, page, true);
    Long num = getService().count(params);
    return new ResponseObject<>(num);
  }

  /**
   * 分页查询
   */
  @RequestMapping(value = "/list")
  public ResponseObject<Map<String, Object>> list(SearchForm searchForm, Pageable page,
      boolean pageable) {
    Map<String, Object> params = SearchFormUtils
        .checkAndGenerateMapParams(searchForm, page, pageable);
    List<? extends T> list = getService().list(params);
    Long num = getService().count(params);
    Map<String, Object> listAndNum = new HashMap<String, Object>();
    listAndNum.put("list", list);
    listAndNum.put("total", num);
    return new ResponseObject<>(listAndNum);
  }


}
