package com.xquark.restapi.im;

public class ImRecvMsg {

  private String sessionId;
  private Integer ver;
  private String to_user_id;
  private String from_user_id;
  private String type = "1";
  private Integer hold = 0;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getTo_user_id() {
    return to_user_id;
  }

  public void setTo_user_id(String to_user_id) {
    this.to_user_id = to_user_id;
  }

  public String getFrom_user_id() {
    return from_user_id;
  }

  public void setFrom_user_id(String from_user_id) {
    this.from_user_id = from_user_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getHold() {
    return hold;
  }

  public void setHold(Integer hold) {
    this.hold = hold;
  }

  public Integer getVer() {
    return ver;
  }

  public void setVer(Integer ver) {
    this.ver = ver;
  }

}
