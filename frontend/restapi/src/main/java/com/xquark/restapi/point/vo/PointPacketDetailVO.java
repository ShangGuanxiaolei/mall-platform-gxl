package com.xquark.restapi.point.vo;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class PointPacketDetailVO {
    /**
     * 红包编号
     */
    private String pointGiftNo;

    /**
     * 领取金额
     */
    private Integer amount;

    /**
     * 红包类型
     */
    private String pointPacketType;

    /**
     * 业务类型
     */
    private String bizType;
    /**
     * 创建时间
     */
    private Long createAt;

    /**
     * 红包类型 定向 非定向
     */
    private String packetType;

    public String getPacketType() {
        return packetType;
    }

    public void setPacketType(String packetType) {
        this.packetType = packetType;
    }

    public String getPointGiftNo() {
        return pointGiftNo;
    }

    public void setPointGiftNo(String pointGiftNo) {
        this.pointGiftNo = pointGiftNo;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPointPacketType() {
        return pointPacketType;
    }

    public void setPointPacketType(String pointPacketType) {
        this.pointPacketType = pointPacketType;
    }

    public Long getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Long createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "PointPacketDetailVO{" +
                "pointGiftNo='" + pointGiftNo + '\'' +
                ", amount='" + amount + '\'' +
                ", pointPacketType='" + pointPacketType + '\'' +
                ", createAt=" + createAt +
                '}';
    }
}
