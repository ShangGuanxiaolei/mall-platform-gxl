package com.xquark.restapi.product;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.Sku;
import com.xquark.dal.vo.SkuBasicVO;
import com.xquark.helper.Functions;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 组合套餐api
 *
 * @author wangxinhua.
 * @date 2018/9/28
 */
@RestController
@RequestMapping("/productCombine")
public class ProductCombineController extends BaseController {

  private final ProductService productService;

  private final ProductCombineService productCombineService;

  @Autowired
  public ProductCombineController(ProductService productService,
      ProductCombineService productCombineService) {
    this.productService = productService;
    this.productCombineService = productCombineService;
  }

  @RequestMapping("/listSelected")
  public ResponseObject<List<String>> listSelected(String productId, String supplierId,
      Long categoryId, String keyword,
      String selectedIds) {
    List<Sku> skus = productService.listSkusOrig(productId);
    String skuId = CollectionUtils.isEmpty(skus) ? null : skus.get(0).getId();
    supplierId = Strings.emptyToNull(supplierId);
    List<String> selectedList = StringUtils.isBlank(selectedIds) ?
        null :
        Arrays.asList(selectedIds.split(","));
    List<? extends SkuBasicVO> skuBasicVOS = productService
        .listCombineSku(skuId, categoryId, supplierId, selectedList, keyword, null);
    List<String> ret = Transformer
        .fromIterable(skuBasicVOS, Functions.<SkuBasicVO>newIdPropertyFunction());
    return new ResponseObject<>(ret);
  }

  /**
   * 查询套餐的组合商品 product/combine 会被product/{id} 捕获
   */
  @RequestMapping("/list")
  public ResponseObject<Map<String, ?>> listCombine(
      @RequestParam Boolean combined,
      String productId, String supplierId,
      Long categoryId,
      String selectedIds,
      String keyword,
      Pageable pageable) {
    productId = Strings.emptyToNull(productId);
    List<? extends SkuBasicVO> list;
    Long total;
    List<Sku> skus = productService.listSkusOrig(productId);
    String skuId = CollectionUtils.isEmpty(skus) ? null : skus.get(0).getId();
    supplierId = Strings.emptyToNull(supplierId);
    List<String> selectedList = StringUtils.isBlank(selectedIds) ?
        null :
        Arrays.asList(selectedIds.split(","));
    if (StringUtils.isNotBlank(keyword)) {
      keyword = "%".concat(keyword).concat("%");
    }
    if (combined) {
      list = productService.listCombineSku(skuId, categoryId, supplierId, selectedList,
          keyword, pageable);
      total = productService.countCombineSku(skuId, categoryId, supplierId, selectedList, keyword);
    } else {
      list = productService.listUnCombineSku(skuId, supplierId, categoryId, selectedList,
          keyword, pageable);
      total = productService.countUnCombineSku(skuId, supplierId, categoryId, selectedList, keyword);
    }
    Map<String, ?> ret = ImmutableMap.of("list", list, "total", total);
    return new ResponseObject<Map<String, ?>>(ret);
  }

  /**
   * 针对表格逻辑特殊处理, 只有异常是才返回错误
   */
  @RequestMapping("/uncombineTable")
  public ResponseObject<Boolean> unCombineTable(@RequestParam String masterId,
      @RequestParam String slaveId) {
    boolean ret = true;
    try {
      productCombineService.unCombine(masterId, slaveId);
    } catch (Exception e) {
      ret = false;
      log.error("解绑sku {} -> {} 失败", masterId, slaveId);
    }
    return new ResponseObject<>(ret);
  }

}
