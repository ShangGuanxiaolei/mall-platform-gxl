package com.xquark.restapi.module;

import com.xquark.dal.model.MerchantRole;
import com.xquark.dal.model.MerchantRoleModule;
import com.xquark.dal.model.Module;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantRoleService;
import com.xquark.service.module.ModuleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
@RestController
public class ModuleController {

  @Autowired
  private ModuleService moduleService;

  @Autowired
  private MerchantRoleService merchantRoleService;

  @RequestMapping("/module/listTree")
  public List<Map<String, Object>> listTreeMenu() {
    return moduleService.listTreeMenu();
  }

  @RequestMapping("/module/save")
  public ResponseObject<Boolean> save(Module module, @RequestParam("isUpdate") Boolean isUpdate) {
    if (!isUpdate) {
      // 插入菜单
      // 传入数据为当前节点的下级
      String id = module.getId();
      if ("0".equals(id)) {
        id = null;
      }
      module.setParentId(id);
      module.setId(null);
      Module moduleExists = moduleService.loadChild(module.getParentId(), module.getName());
      if (moduleExists != null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要添加的菜单已存在");
      }
      return new ResponseObject<>(moduleService.save(module) > 0);
    } else {
      // 修改菜单
      String parentId = module.getParentId();
      if ("".equals(parentId) || "0".equals(parentId)) {
        module.setParentId(null);
      }
      return new ResponseObject<>(moduleService.update(module) > 0);
    }
  }

  @RequestMapping("/module/list")
  public ResponseObject<Map<String, Object>> list(String order, String parentId, Pageable pageable,
      String direction) {
    if (parentId == null || "0".equals(parentId)) {
      parentId = null;
    }
    order = StringUtils.defaultIfEmpty(order, "sort_no");
    direction = StringUtils.defaultIfEmpty(direction, "desc");
    List<Module> moduleList = moduleService
        .listByOrder(parentId, order, pageable, Sort.Direction.fromString(direction));

    Integer moduleTotal;
    Map<String, Object> result = new HashMap<>();
    if (parentId != null) {
      moduleTotal = moduleService.loadSubCounts(parentId);
      result.put("parentId", parentId);
    } else {
      moduleTotal = moduleService.loadCounts();
    }
    result.put("moduleTotal", moduleTotal);
    result.put("list", moduleList);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/view/{id}")
  public ResponseObject<Module> view(@PathVariable String id) {
    return new ResponseObject<>(moduleService.load(id));
  }

  @RequestMapping("/module/view/role/{roleId}")
  public ResponseObject<Map<String, Object>> listByRole(@PathVariable String roleId) {
    MerchantRole role = merchantRoleService.load(roleId);
    if (role == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "角色不存在");
    }
    List<Module> moduleList = moduleService.listByRole(roleId);
    Map<String, Object> result = new HashMap<>();
    result.put("modules", moduleList);
    return new ResponseObject<>(result);
  }

  @RequestMapping("/module/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    return new ResponseObject<>(moduleService.delete(id) > 0);
  }

  @RequestMapping(value = "/module/bindRole", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> bindRole(@RequestBody List<MerchantRoleModule> roleModuleList) {
    if (roleModuleList == null || roleModuleList.size() == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "角色菜单对应列表为空!");
    }
    return new ResponseObject<>(moduleService.bindRoleModule(roleModuleList) > 0);
  }

  @RequestMapping(value = "/module/unBindRole")
  public ResponseObject<Boolean> unBindRole(@RequestParam("roleId") String roleId,
      @RequestParam("moduleIds") String moduleIds) {
    String[] moduleIdList = moduleIds.split(",");
    return new ResponseObject<>(moduleService.unBindRoleModule(moduleIdList, roleId) > 0);
  }

}
