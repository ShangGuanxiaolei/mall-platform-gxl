package com.xquark.restapi.postAgeSet;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.mapper.PostAgeSetMapper;
import com.xquark.dal.model.PostAgeSet;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.shop.ShopPostAgeService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 运费模板controller Created by chh on 17-09-21.
 */
@Controller
@ApiIgnore
public class PostAgeSetController extends BaseController {

  @Autowired
  private ShopPostAgeService shopPostAgeService;

  @Autowired
  private PostAgeSetMapper postAgeSetMapper;

  /**
   * 运费模板列表
   */
  @ResponseBody
  @RequestMapping("/postAgeSet/list")
  public ResponseObject<Map<String, Object>> productList(Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("shopId", shopId);
    List<PostAgeSet> result = shopPostAgeService.list(pageable, params);
    Long total = shopPostAgeService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 删除运费模板
   */
  @ResponseBody
  @RequestMapping("/postAgeSet/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = shopPostAgeService.deleteByPrimaryKey(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 复制运费模板
   */
  @ResponseBody
  @RequestMapping("/postAgeSet/copy/{id}")
  public ResponseObject<Boolean> copy(@PathVariable String id) {
    PostAgeSet postAgeSet = postAgeSetMapper.selectByPrimaryKey(id);
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS"); //精确到毫秒
    String suffix = fmt.format(new Date());
    PostAgeSet newPostAgeSet = new PostAgeSet();
    newPostAgeSet.setPostageSet(postAgeSet.getPostageSet());
    newPostAgeSet.setShopId(postAgeSet.getShopId());
    newPostAgeSet.setName(postAgeSet.getName() + "_" + suffix);
    long result = postAgeSetMapper.insert(newPostAgeSet);
    return new ResponseObject<>(result > 0);
  }

}
