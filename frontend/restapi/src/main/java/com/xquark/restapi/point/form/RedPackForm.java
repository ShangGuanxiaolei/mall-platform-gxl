package com.xquark.restapi.point.form;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
public class RedPackForm {

    @NotNull
    private BigDecimal aggregateAmount;
    @NotNull
    private Integer peopleAmount;
    @NotNull
    private String requireIdentity;
    @NotNull
    private Integer divisionType;

    private String leaveWord;

    public BigDecimal getAggregateAmount() {
        return aggregateAmount;
    }

    public void setAggregateAmount(BigDecimal aggregateAmount) {
        this.aggregateAmount = aggregateAmount;
    }

    public Integer getPeopleAmount() {
        return peopleAmount;
    }

    public void setPeopleAmount(Integer peopleAmount) {
        this.peopleAmount = peopleAmount;
    }

    public String getRequireIdentity() {
        return requireIdentity;
    }

    public void setRequireIdentity(String requireIdentity) {
        this.requireIdentity = requireIdentity;
    }

    public String getLeaveWord() {
        return leaveWord;
    }

    public void setLeaveWord(String leaveWord) {
        this.leaveWord = leaveWord;
    }

    public Integer getDivisionType() {
        return divisionType;
    }

    public void setDivisionType(Integer divisionType) {
        this.divisionType = divisionType;
    }

}
