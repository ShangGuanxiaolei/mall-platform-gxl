package com.xquark.restapi.common;

import com.alibaba.fastjson.JSONObject;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.SfAppConfigMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.vo.LogisticsVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class LogisticsController {

  @ResponseBody
  @RequestMapping("/logistics/list")
  public ResponseObject<List<LogisticsVO>> list() {
    List<LogisticsVO> list = new ArrayList<>();

    LogisticsVO vo = null;
    LogisticsCompany[] logistics = LogisticsCompany.values();
    for (LogisticsCompany obj : logistics) {
      vo = new LogisticsVO();
      vo.setKey(obj.name());
      vo.setName(obj.toString());
      list.add(vo);
    }

    return new ResponseObject<>(list);
  }

  @Autowired
  private SfAppConfigMapper sfConfigMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  private Logger log = LoggerFactory.getLogger(getClass());

  @RequestMapping("/logistics/goodsQuery")
  @ResponseBody
  public ResponseObject<JSONObject> queryOrderLogInfo(String orderNo, ProductSource productSource) {
    ResponseObject<JSONObject> responseObject = new ResponseObject<>();
    if (!ProductSource.SF.equals(productSource)) {
      responseObject.setMoreInfo("请输入顺丰商品");
      return responseObject;
    }
    try {
      SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.PULL_LOGISTICS, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      Order order = orderMapper.getLogsticsByOrderNo(orderNo);
      JSONObject jsonObject = new JSONObject();
      responseObject.setMoreInfo("获取成功");
      jsonObject.put("orderSn", order.getPartnerOrderNo());
      jsonObject.put("billSn", order.getLogisticsOrderNo());
      jsonObject.put("shippingid", 2);
      String req = jsonObject.toString();
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, req);
      String content = res.getContent();
      JSONObject jsonObject1 = JSONObject.parseObject(content);
      responseObject.setData(jsonObject1);
    } catch (Exception e) {
      StringBuffer info = new StringBuffer();
      info.append("顺丰物流更新失败");
      log.info(info.toString());
      responseObject.setMoreInfo("获取失败");
    }
    return responseObject;
  }
}
