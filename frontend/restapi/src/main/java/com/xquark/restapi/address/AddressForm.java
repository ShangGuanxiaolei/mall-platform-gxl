package com.xquark.restapi.address;

import com.wordnik.swagger.annotations.ApiModelProperty;

public class AddressForm {

  @ApiModelProperty(value = "id")
  private String id;
  @ApiModelProperty(value = "收货人名字", required = true)
  private String consignee;
  @ApiModelProperty(value = "手机号", required = true)
  private String phone;
  @ApiModelProperty(value = "地区id", required = true)
  private String zoneId;
  @ApiModelProperty(value = "详细地址", required = true)
  private String street;
  @ApiModelProperty(value = "邮政编码")
  private String zipcode;
  @ApiModelProperty(value = "是否默认收货地址", required = true)
  private Boolean isDefault;

  private String certificateId;

  private Long provinceId;
  private Long cityId;

  public String getCertificateId() {
    return certificateId;
  }

  public void setCertificateId(String certificateId) {
    this.certificateId = certificateId;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Boolean getDefault() {
    return isDefault;
  }

  public void setDefault(Boolean aDefault) {
    isDefault = aDefault;
  }

  public Long getProvinceId() {
    return provinceId;
  }

  public void setProvinceId(Long provinceId) {
    this.provinceId = provinceId;
  }

  public Long getCityId() {
    return cityId;
  }

  public void setCityId(Long cityId) {
    this.cityId = cityId;
  }
}
