package com.xquark.restapi.packetRain.vo;

import java.util.Comparator;

/**
 * @author Jack Zhu
 * @date 2019/03/18
 */
public class StyleSort implements Comparator<StyleSort> {

    private Integer sortNumber;
    private Integer prizeType;
    private Integer prizeRank;
    private String image;
    private String name;

    public StyleSort() {
    }

    public StyleSort(Integer sortNumber, Integer prizeType, Integer prizeRank, String image, String name) {
        this.sortNumber = sortNumber;
        this.prizeType = prizeType;
        this.prizeRank = prizeRank;
        this.image = image;
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(Integer sortNumber) {
        this.sortNumber = sortNumber;
    }

    public Integer getPrizeType() {
        return prizeType;
    }

    public void setPrizeType(Integer prizeType) {
        this.prizeType = prizeType;
    }

    public Integer getPrizeRank() {
        return prizeRank;
    }

    public void setPrizeRank(Integer prizeRank) {
        this.prizeRank = prizeRank;
    }

    @Override
    public int compare(StyleSort o1, StyleSort o2) {
        return o2.sortNumber - o1.sortNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
