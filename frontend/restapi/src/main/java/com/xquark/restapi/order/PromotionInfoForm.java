package com.xquark.restapi.order;

import com.xquark.dal.type.PromotionType;
import com.xquark.service.pricing.base.FlashSalePromotionInfo;
import com.xquark.service.pricing.base.PgPromotionInfo;
import com.xquark.service.pricing.base.PromotionInfo;
import com.xquark.service.pricing.base.ReservePromotionInfo;

/**
 * @author wangxinhua
 * @since 18-11-1
 */
public abstract class PromotionInfoForm {

  private String promotionId;

  private Boolean isPayFree;
  private PromotionType promotionFrom;

  private String tranCode;

  private String detailCode;

  /**
   * 预约单号
   */
  private String preOrderNo;

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public PromotionType getPromotionFrom() {
    return promotionFrom;
  }

  public Boolean getIsPayFree() {
    return isPayFree;
  }

  public void setIsPayFree(Boolean payFree) {
    isPayFree = payFree;
  }

  public void setPromotionFrom(PromotionType promotionFrom) {
    this.promotionFrom = promotionFrom;
  }

  public String getTranCode() {
    return tranCode;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }

  public String getDetailCode() {
    return detailCode;
  }

  public void setDetailCode(String detailCode) {
    this.detailCode = detailCode;
  }

  /**
   * 从表单中获取活动信息
   */
  public PromotionInfo getPromotionInfo() {
    PromotionType promotionType = this.getPromotionFrom();
    String promotionId = this.getPromotionId();
    if (promotionType == null) {
      return null;
    }
    switch (promotionType) {
      case SALEZONE:
      case FLASHSALE:
        return new FlashSalePromotionInfo(promotionId, promotionType);
      case PIECE_GROUP:
        String tranCode = this.getTranCode();
        String detailCode = this.getDetailCode();
        final Boolean payFree = this.getIsPayFree();
        return new PgPromotionInfo(promotionId, promotionType, tranCode, detailCode, payFree);
      case RESERVE:
        return new ReservePromotionInfo(promotionId,promotionType,this.getPreOrderNo());
      default:
        return null;
    }

  }

  public String getPreOrderNo() {
    return preOrderNo;
  }

  public void setPreOrderNo(String preOrderNo) {
    this.preOrderNo = preOrderNo;
  }
}
