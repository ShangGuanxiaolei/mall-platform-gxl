package com.xquark.restapi.bank;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.verify.VerificationFacade;
import com.xquark.biz.verify.impl.VerificationFacadeImpl.SmsType;
import com.xquark.dal.model.PayBank;
import com.xquark.dal.model.UserAlipay;
import com.xquark.dal.model.UserBank;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.DealHistoryVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.alipay.UserAliPayVO;
import com.xquark.service.account.AccountService;
import com.xquark.service.account.SubAccountService;
import com.xquark.service.alipay.UserAlipayService;
import com.xquark.service.bank.UserBankService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.deal.DealService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.payBank.PayBankService;
import com.xquark.service.user.UserService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
@ApiIgnore
public class UserBankController extends BaseController {

  @Autowired
  UserBankService userBankService;

  @Autowired
  UserAlipayService userAlipayService;

  @Autowired
  WithdrawApplyService withdrawApplyService;

  @Autowired
  private VerificationFacade veriFacade;

  @Autowired
  DealService dealService;

  @Autowired
  AccountService accountService;

  @Autowired
  SubAccountService subAccountService;

  @Autowired
  UserService userService;

  @Autowired
  PayBankService paybankService;

  @Autowired
  CommissionService commissionService;

  @Autowired
  private ResourceFacade resourceFacade;


  @ResponseBody
  @RequestMapping("/userBank/save")
  public ResponseObject<UserBank> save(@Valid @ModelAttribute UserBankForm form, Errors errors,
      HttpServletRequest req) {
    ControllerHelper.checkException(errors);
    UserBank userBank = new UserBank();
    BeanUtils.copyProperties(form, userBank);
    userBank.setUserId(userBankService.getCurrentUser().getId());
    int rc = 0;
    RequestContext requestContext = new RequestContext(req);
    if (StringUtils.isBlank(form.getId())) {
      rc = userBankService.insert(userBank);
    } else {
      if (StringUtils.isBlank(form.getSmsCode())) {
        throw new BizException(GlobalErrorCode.UNKNOWN,
            requestContext.getMessage("userbank.smscode.not.null"));
      }

      boolean valid = veriFacade
          .verifyCode(userBankService.getCurrentUser().getPhone(), form.getSmsCode(),
              SmsType.MODIFY_BANK);
      if (!valid) {
        throw new BizException(GlobalErrorCode.UNKNOWN,
            requestContext.getMessage("userbank.smscode.not.valid"));
      }
      rc = userBankService.update(userBank);
    }
    if (rc == 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.bank.error.message"));
    }
    rc = userService
        .updateNameAndIdCardNumByPrimaryKey(userBank.getUserId(), userBank.getAccountName(),
            userBank.getIdCardNum());
    if (rc == 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.bank.error.message"));
    }
    return new ResponseObject<>(userBank);
  }

  @ResponseBody
  @RequestMapping("/userBank/update")
  public ResponseObject<UserBank> update(@Valid @ModelAttribute UserBankUpdateForm form,
      Errors errors, HttpServletRequest req) {
    ControllerHelper.checkException(errors);
    UserBank userBank = new UserBank();
    BeanUtils.copyProperties(form, userBank);
    userBank.setUserId(userBankService.getCurrentUser().getId());
    int rc = 0;
    RequestContext requestContext = new RequestContext(req);
    if (StringUtils.isBlank(form.getId())) {
      rc = userBankService.insert(userBank);
    } else {
      if (StringUtils.isBlank(form.getSmsCode())) {
        throw new BizException(GlobalErrorCode.UNKNOWN,
            requestContext.getMessage("userbank.smscode.not.null"));
      }

      boolean valid = veriFacade
          .verifyCode(userBankService.getCurrentUser().getPhone(), form.getSmsCode(),
              SmsType.MODIFY_BANK);
      if (!valid) {
        throw new BizException(GlobalErrorCode.UNKNOWN,
            requestContext.getMessage("userbank.smscode.not.valid"));
      }
      rc = userBankService.update(userBank);
    }
    if (rc == 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.bank.error.message"));
    }
    return new ResponseObject<>(userBank);
  }

  /**
   * 注册发送验证码
   *
   * @return 是否已发送sms
   */
  @ResponseBody
  @RequestMapping("/userBank/send-sms-code")
  public ResponseObject<Boolean> sendSmsCode() {
    if (StringUtils.isBlank(userBankService.getCurrentUser().getPhone())) {
      return new ResponseObject<>(Boolean.FALSE);
    }
    veriFacade.generateCode(userBankService.getCurrentUser().getPhone(), SmsType.MODIFY_BANK);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 我的银行卡
   */
  @ResponseBody
  @RequestMapping("/userBank/mine")
  public ResponseObject<UserBankVO> mine() {
    List<UserBank> banks = userBankService.mine();
    UserBankVO bank = null;
    if (banks.size() > 0) {
      bank = new UserBankVO();
      BeanUtils.copyProperties(banks.get(0), bank);
    }
    return new ResponseObject<>(bank);
  }

  /**
   * 我的收入汇总
   */
  @ResponseBody
  @RequestMapping("/userBank/mineAccount")
  public ResponseObject<Map<String, Object>> mineIncome() {
    String userId = userBankService.getCurrentUser().getId();
    Map<AccountType, BigDecimal> map = subAccountService.selectBalanceByUser(userId);

    BigDecimal balance = map.get(AccountType.AVAILABLE);
    balance = ObjectUtils.defaultIfNull(balance, BigDecimal.ZERO);

    BigDecimal warrant = map.get(AccountType.DANBAO);
    warrant = ObjectUtils.defaultIfNull(warrant, BigDecimal.ZERO);

    BigDecimal waitUpIncome = map.get(AccountType.COMMISSION);
    waitUpIncome = ObjectUtils.defaultIfNull(warrant, BigDecimal.ZERO);

//		BigDecimal frozen = map.get(AccountType.FROZEN);
//		frozen = ObjectUtils.defaultIfNull(frozen, BigDecimal.ZERO);

    BigDecimal withdraw = map.get(AccountType.WITHDRAW);
    withdraw = ObjectUtils.defaultIfNull(withdraw, BigDecimal.ZERO);

    BigDecimal withdrawAll = withdrawApplyService
        .totalWithdrawApplyByStatus(userId, WithdrawApplyStatus.SUCCESS);
    withdrawAll = ObjectUtils.defaultIfNull(withdrawAll, BigDecimal.ZERO);

    List<UserBank> banks = userBankService.mine();
    //当前版本只返回一个
    UserBankVO bank = null;
    if (banks.size() > 0) {
      bank = new UserBankVO();
      BeanUtils.copyProperties(banks.get(0), bank);
      bank.setAccountNumber(getAccountNumberForShow(bank.getAccountNumber()));
    }

    UserAliPayVO alipayVO = null;
    UserAlipay alipay = userAlipayService.loadByUserId(userId);
    if (alipay != null) {
      alipayVO = new UserAliPayVO();
      BeanUtils.copyProperties(alipay, alipayVO);
    }

    Map<String, Object> mineIncome = new HashMap<>();
    mineIncome.put("balance", balance); //未提现收入
    mineIncome.put("withdraw", withdraw); //提现中
    mineIncome.put("warrant", warrant);    //担保交易
    mineIncome.put("addUpIncome", withdrawAll); //累计收入（已提现的总金额）
    mineIncome.put("withdrawAll", balance); //未提现收入
    mineIncome.put("waitUpIncome", waitUpIncome); //待收收益

    mineIncome.put("type", userService.load(userId).getWithdrawType()); //我的绑定类型
    mineIncome.put("mineBank", bank); //我的银行卡
    mineIncome.put("mineAlipay", alipayVO);//我的支付宝信息
    return new ResponseObject<>(mineIncome);
  }

  /**
   * app:2c的累计收益页面
   */
  @ResponseBody
  @RequestMapping("/userBank/withdrawAll")
  public ResponseObject<Map<String, Object>> withdrawAll() {
    String userId = userBankService.getCurrentUser().getId();
    Map<AccountType, BigDecimal> map = subAccountService.selectBalanceByUser(userId);

    BigDecimal balance = map.get(AccountType.AVAILABLE);
    balance = ObjectUtils.defaultIfNull(balance, BigDecimal.ZERO);

    BigDecimal withdraw = map.get(AccountType.WITHDRAW);
    withdraw = ObjectUtils.defaultIfNull(withdraw, BigDecimal.ZERO);

    BigDecimal withdrawAll = withdrawApplyService
        .totalWithdrawApplyByStatus(userId, WithdrawApplyStatus.SUCCESS);
    withdrawAll = ObjectUtils.defaultIfNull(withdrawAll, BigDecimal.ZERO);

    Map<String, Object> mineIncome = new HashMap<>();
    //今日 today
    //本月 thisMonth
    //上个月 lastMonth
    //本周 thisWeek
    //累计收入 withdrawAll
    mineIncome.put("today", 0);
    mineIncome.put("thisMonth", 35);
    mineIncome.put("lastMonth", 58);
    mineIncome.put("thisWeek", 7);
    mineIncome.put("withdrawAll", balance.add(withdrawAll).add(withdraw));

    return new ResponseObject<>(mineIncome);
  }

  /**
   * app:2b的我的收益页面
   */
  @ResponseBody
  @RequestMapping("/userBank/withdrawAll/2b")
  public ResponseObject<Map<String, Object>> withdrawAllToB() {
    String userId = userBankService.getCurrentUser().getId();

    BigDecimal withdrawAll = commissionService.getWithdrawAll(userId);//获取累计收入
    List<BigDecimal> monthWithdrawList = commissionService.getFeeOfMonths(userId);//获取本年度每个月的收益

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, -1);

    String lastMonthStr = sdf.format(calendar.getTime());
    BigDecimal lastMonthWithdraw = commissionService
        .getWithdrawOfMonth(userId, lastMonthStr);//获取上个月的收益

    Map<String, Object> mineIncome = new HashMap<>();

    mineIncome.put("withdrawAll", withdrawAll);//累计收入 withdrawAll
    mineIncome.put("monthWithdrawList", monthWithdrawList);//本年度每个月的收益
    mineIncome.put("lastMonth", lastMonthWithdraw);//上个月收益

    return new ResponseObject<>(mineIncome);
  }


  /**
   * app:2b的本年度累计收益折线图，
   */
  @ResponseBody
  @RequestMapping("/userBank/monthlywithdraw")
  public ResponseObject<List> monthlywithdraw() {

    String userId = userBankService.getCurrentUser().getId();
    List<BigDecimal> monthWithdraw = commissionService.getFeeOfMonths(userId);

    return new ResponseObject<List>(monthWithdraw);
  }

  /**
   * app:累计收益明细列表
   */
  @ResponseBody
  @RequestMapping("/userBank/withdrawAll/list")
  public void withdrawAllList() {
    String userId = userService.getCurrentUser().getId();

  }


  @ResponseBody
  @RequestMapping("/user/incomeAccount/change")
  public ResponseObject<Boolean> incomeAccount(int type) {
    String userId = userService.getCurrentUser().getId();
    userService.saveWithDrawType(userId, type);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 获取银行卡显示
   */
  private String getAccountNumberForShow(String accountNumber) {
    if (StringUtils.isBlank(accountNumber)) {
      return "";
    }
    try {
      String temp = accountNumber;
      int len = temp.length();
      String start = temp.substring(0, 6);
      String end = temp.substring(len - 4);
      String center = "****";
      return start + center + end;
    } catch (Exception e) {
    }
    return "";
  }


  /**
   * 担保交易详细
   */
  @ResponseBody
  @RequestMapping("/userBank/mineWarrant")
  public ResponseObject<List<IncomeDetailVO>> warrant() {
    return new ResponseObject<List<IncomeDetailVO>>(new ArrayList<IncomeDetailVO>());
  }

  /**
   * 全部收入详细
   */
  @ResponseBody
  @RequestMapping("/userBank/mineDealHistory")
  public ResponseObject<List<DealHistoryVO>> allIncome(HttpServletRequest req, Pageable page) {
    RequestContext requestContext = new RequestContext(req);
    List<DealHistoryVO> vos = accountService.listDeal(page);
    String title = null, memo = null;
    for (DealHistoryVO vo : vos) {
      try {
        title = requestContext
            .getMessage("order.status." + vo.getOrderStatus().toString().toLowerCase());
      } catch (Exception e) {
        title = "未知";
      }
      memo = vo.getFeeFrom() + requestContext.getMessage("order.status.paid");
      vo.setTitle(title);
      vo.setMemo(memo);
    }

    return new ResponseObject<List<DealHistoryVO>>(new ArrayList<>(vos));
  }

  /**
   * 全部收入详细
   * @return
   */
//	@ResponseBody
//	@RequestMapping("/userBank/mineDealHistory")
//	public ResponseObject<List<DealHistoryVO>> allIncome(HttpServletRequest req, Pageable page) {
//		RequestContext requestContext = new RequestContext(req);
//		List<DealHistoryVO> vos = dealService.listDealByUserId( dealService.getCurrentUser().getId(), page );
//		String title = null, memo = null;
//		for(DealHistoryVO vo : vos){
//			if(vo.getDealType() == DealType.WITHDRAW){
//				title = requestContext.getMessage("withdraw.success");
//				memo = requestContext.getMessage("withdraw.describe", new Object[]{vo.getWithdrawBank()});
//				vo.setFee(new BigDecimal(-vo.getFee().doubleValue())); 
//			}else if(vo.getDealType() == DealType.DIRECT_PAY){
//				title = requestContext.getMessage("order.status."+vo.getOrderStatus().toString().toLowerCase());
//				memo = vo.getFeeFrom() + requestContext.getMessage("deal.type." + vo.getDealType().toString().toLowerCase());
//			}else if(vo.getDealType() == DealType.TRANSFER){
//				title = requestContext.getMessage("order.status."+vo.getOrderStatus().toString().toLowerCase());
//				memo = vo.getFeeFrom() + requestContext.getMessage("deal.type." + vo.getDealType().toString().toLowerCase());
//			}else if(vo.getDealType() == DealType.DEPOSIT){
//				title = requestContext.getMessage("order.status."+vo.getOrderStatus().toString().toLowerCase());
//				memo = vo.getFeeFrom() + requestContext.getMessage("deal.type." + vo.getDealType().toString().toLowerCase());
//			}
//			else{
//				continue;
//			}
//			vo.setTitle(title);
//			vo.setMemo(memo);
//		}
//		
//		return new ResponseObject<List<DealHistoryVO>>(new ArrayList<DealHistoryVO>(vos));
//	}

  /**
   * 获取常用银行列表
   */
  @ResponseBody
  @RequestMapping("/userBank/bankList")
  public ResponseObject<List<PayBank>> getCommonlyBankList() {
    List<PayBank> list = paybankService.obtainCommonlyBankList();
    for (PayBank aList : list) {
      aList.setImg(resourceFacade.resolveUrl(aList.getImg()));
    }
    return new ResponseObject<>(list);
  }

}
