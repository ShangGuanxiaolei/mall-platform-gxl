package com.xquark.restapi.productSales;

import com.xquark.dal.model.Promotion4ProductSalesVO;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.productSales.ProductSalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @类名: ProductSalesController
 * @描述: 商品促銷活動相关控制中心
 * @程序猿: LuXiaoLing
 * @日期: 2019/3/27 14:16
 * @版本号: V1.0 .
 */
@Controller("ProductSalesController")
public class ProductSalesController extends BaseController {
    @Autowired
    private ProductSalesService productSalesService;
    /**
     * 功能描述: 促销活动首页列表展示
     * @author Luxiaoling
     * @date 2019/3/27 14:57
     * @param    page, pageSize
     * @return  com.xquark.dal.page.PageHelper<com.xquark.dal.model.Promotion>
     */
    @ResponseBody
    @RequestMapping("/productSales/selectBeginningSalesProducts")
    public ResponseObject<PageHelper<Promotion4ProductSalesVO>> selectBeginningSalesProducts(@RequestParam(value = "pCode", required = false) String pCode, @RequestParam(value = "page",defaultValue = "0") Integer page, @RequestParam(value = "pageSize",defaultValue = "20")Integer pageSize) {
        User user = (User) getCurrentIUser();
        return new ResponseObject(productSalesService.selectBeginningSalesProducts(page,pageSize, user,pCode));
    }

    @ResponseBody
    @RequestMapping("/productSales/selectGrandSalesProducts")
    public ResponseObject<PageHelper<Promotion4ProductSalesVO>> selectGrandSalesProducts(@RequestParam(value = "grandSaleId") String grandSaleId, @RequestParam(value = "page",defaultValue = "0") Integer page, @RequestParam(value = "pageSize",defaultValue = "20")Integer pageSize) {
        User user = (User) getCurrentIUser();
        return new ResponseObject(productSalesService.selectGrandSalesProducts(page,pageSize, user,grandSaleId));
    }
}
