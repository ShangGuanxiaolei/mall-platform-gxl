package com.xquark.restapi.bank;

import com.xquark.dal.status.WithdrawApplyStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by dongsongjie on 16/7/20.
 */
public class WithdrawApplySearchForm {

  private String sellerName;

  private String sellerPhone;

  private WithdrawApplyStatus status;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date startDate;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date endDate;

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public WithdrawApplyStatus getStatus() {
    return status;
  }

  public void setStatus(WithdrawApplyStatus status) {
    this.status = status;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
}
