package com.xquark.restapi.yundou;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 * Created by wangxinhua on 17-12-4. DESC:
 */
@ApiModel("签到signIn/view返回数据")
public class SignInResultWrapper {

  @ApiModelProperty("积分天数列表")
  private final List<SignInDateEntry> list;

  @ApiModelProperty("今天签到的积分数")
  private final long todayPoints;

  @ApiModelProperty("明天签到的积分数")
  private final long tomorrowPoints;

  @ApiModelProperty("连续签到天数")
  private final long signCount;

  public SignInResultWrapper(List<SignInDateEntry> list, long todayPoints, long tomorrowPoints,
      long signCount) {
    this.list = list;
    this.todayPoints = todayPoints;
    this.tomorrowPoints = tomorrowPoints;
    this.signCount = signCount;
  }

  public List<SignInDateEntry> getList() {
    return list;
  }

  public long getTodayPoints() {
    return todayPoints;
  }

  public long getTomorrowPoints() {
    return tomorrowPoints;
  }

  public long getSignCount() {
    return signCount;
  }
}
