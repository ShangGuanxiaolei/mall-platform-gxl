package com.xquark.restapi.point;

import com.google.common.collect.ImmutableMap;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.service.point.PointServiceApi;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.model.*;
import com.xquark.dal.type.CareerLevelType;
import com.xquark.helper.Transformer;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.point.form.ReceivePointGiftForm;
import com.xquark.restapi.point.form.RedPackForm;
import com.xquark.restapi.point.vo.*;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.packetRain.PacketRainService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pointgift.PointGiftRecordService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.dto.PacketType;
import com.xquark.service.pointgift.dto.PointPacketDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * @author Jack Zhu
 * @date 2018/12/11
 */
@RestController
@RequestMapping("/point/packet")
public class PointGiftController extends BaseController {

    private final PointGiftRecordService pointGiftRecordService;
    private final PointGiftService pointGiftService;
    private final PointServiceApi pointService;
    private final CustomerProfileService customerProfileService;
    private final PacketRainService packetRainService;
    private final static String REQUIRE_IDENTITY_ALL = "RC|DS|SP|PW";
    private final static String ERROR_TYPE = "ErrorType";

    //红包雨
    private final static String PACKET_RAIN_LOTTERY_PERSON = "PACKET::RAIN::LOTTERY::PERSON::";

    private final static BiFunction<String, Long, String> GET_DAILY_USER_LOTTERY_KEY = (id, cpId) ->
            String.format("USER_LOTTERY_LIMIT::%s::%s", id, cpId);


    @Autowired
    public PointGiftController(
            PointGiftRecordService pointGiftRecordService,
            PointGiftService pointGiftService,
            PointContextInitialize pointContextInitialize,
            CustomerProfileService customerProfileService, PacketRainService packetRainService) {
        this.pointGiftRecordService = pointGiftRecordService;
        this.pointGiftService = pointGiftService;
        this.pointService = pointContextInitialize.getPointServiceApi();
        this.customerProfileService = customerProfileService;
        this.packetRainService = packetRainService;
    }

    /**
     * 发红包
     *
     * @param redPackForm
     * @return
     */
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ResponseObject<PointGiftNoVO> sendPacket(@Validated @RequestBody RedPackForm redPackForm) {
        final PointGift pointGift = Transformer.fromBean(redPackForm, PointGift.class);
          pointGift.setCpid(User().getCpId());
//        final PointTotal pointTotal = pointService.initTotal(User().getCpId());
//        ResponseObject<PointGiftNoVO> check = sendPointGiftPreCheck(pointGift, pointTotal);
//        if (check != null) {
//            return check;
//        }
        pointGiftService.sendPacket(pointGift);
        return new ResponseObject<>(new PointGiftNoVO(pointGift.getPointGiftNo()));
    }

    /**
     * 红包领取
     *
     * @return true-领取成功 flase-领取失败
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    public ResponseObject<PointPacketReceiveVO> receivePointPackage(@RequestBody ReceivePointGiftForm receivePointGiftForm) {

        Map<String, GlobalErrorCode> mapParam = new HashMap<>();
        Long cPid = ((User) getCurrentIUser()).getCpId();
        PointGift pointGift = pointGiftService.selectBypointGiftNo(receivePointGiftForm.getPointGiftNo());

        PointGiftRecord pointGiftRecord = new PointGiftRecord.Builder().
                pointGiftNo(pointGift.getPointGiftNo()).
                grantCpid(String.valueOf(pointGift.getCpid())).
                receiveCpid(String.valueOf(cPid)).
                build();

        //校验红包状态
        if (!pointGiftService.checkPointGift(pointGift, pointGiftRecord, mapParam)) {
            return new ResponseObject(new PointPacketReceiveVO(BigDecimal.ZERO,
                    String.valueOf(mapParam.get(ERROR_TYPE).getErrorCode()),
                    mapParam.get(ERROR_TYPE).name()));
        }

        //校验领取人身份
        CareerLevelType careerLevelType = customerProfileService.
                getLevelType(Long.valueOf(pointGiftRecord.getReceiveCpid()));
        String requireIdentity = pointGift.getRequireIdentity();
        if (StringUtils.isNotBlank(requireIdentity)) {
            requireIdentity = requireIdentity + "|PW";
        }
        if (!requireIdentity.contains(careerLevelType.name())) {
            return new ResponseObject(new PointPacketReceiveVO(BigDecimal.ZERO,
                    String.valueOf(GlobalErrorCode.PACKET_AUTH.getErrorCode()),
                    careerLevelType.name()));
        }

        //执行领取操作
        BigDecimal giftPointResult = pointGiftRecordService.operPointGiftRecord(pointGift, pointGiftRecord);
        if (giftPointResult.compareTo(BigDecimal.ZERO) < 1) {
            return new ResponseObject(new PointPacketReceiveVO(BigDecimal.ZERO,
                    String.valueOf(GlobalErrorCode.PACKAGE_GIFT_BROUGHT_OUT.getErrorCode()),
                    GlobalErrorCode.PACKAGE_GIFT_BROUGHT_OUT.name()));
        }

        return new ResponseObject(new PointPacketReceiveVO(giftPointResult,
                String.valueOf(GlobalErrorCode.SUCESS.getErrorCode()),
                GlobalErrorCode.SUCESS.name()));
    }

    /**
     * 定向红包赠送
     *
     * @param receivePointGiftForm form表单
     * @return true 赠送成功 false 赠送失败
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/directsend", method = RequestMethod.POST)
    public ResponseObject<Boolean> transferPoint(@RequestBody ReceivePointGiftForm receivePointGiftForm) {

        Long cpid = ((User) getCurrentIUser()).getCpId();
        //packetType 1-非定向赠送 status 3-领完状态
        PointGift pointGift = new PointGift.Builder().
                aggregateAmount(receivePointGiftForm.getGiftPoint()).
                peopleAmount(1).
                requireIdentity(REQUIRE_IDENTITY_ALL).
                packetType(PacketType.DIRECTIONAL.getCode()).
                status(3).
                expireAt(new Date()).
                leaveWord(receivePointGiftForm.getLeaveWord()).
                cpid(cpid).
                build();

        PointGiftRecord pointGiftRecord = new PointGiftRecord.Builder().
                pointGiftNo(pointGift.getPointGiftNo()).
                grantCpid(String.valueOf(cpid)).
                receiveCpid(receivePointGiftForm.getReceiveUserId()).
                amount(receivePointGiftForm.getGiftPoint()).
                build();

        //登记
        boolean saveFlag = pointGiftService.savePointGiftRecord(pointGift, pointGiftRecord);
        if (!saveFlag) {
            return new ResponseObject(false, "可用德分余额不够!", GlobalErrorCode.COMMISSION_NOT_ENOUGH);
        }

        return new ResponseObject(true);
    }

    /**
     * 1、首次查询取当前时间和上次登陆时间间隔
     * 2、并且把当前时间存放在redis里面,设置过期时间1天
     * 3、下次查询的时候取redis存放时间和当前时间间隔,并把当前时间更新到redis里面,以此类推
     *
     * @return giftPoint
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/periodpoint", method = RequestMethod.GET)
    public ResponseObject<Map> periodCountPoint() {
        Map<String, BigDecimal> resultMap = null;
        BigDecimal periodPoint = pointGiftRecordService.periodGiftPoint(getCurrentIUser().getId(),
                String.valueOf(((User) getCurrentIUser()).getCpId()));

        resultMap = ImmutableMap.of("giftPoint", periodPoint);
        return new ResponseObject(resultMap);
    }

    /**
     * @param type type - RECEIVE_CONSUMPTION  消费和接收的红包 SEND 发送的红包
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/list/detail/{type}", method = RequestMethod.GET)
    public ResponseObject<PointPacketVo> pointListPacketDetail(@PathVariable String type
            , @RequestParam(defaultValue = "0") Integer page
            , @RequestParam(defaultValue = "10") Integer size) {
        final PointPacketDTO packetDetail = pointGiftRecordService.getPointPacketDetail(
                User().getCpId(),
                type,
                new PageRequest(page, size));
        return new ResponseObject<>(PointPacketVo.valueOf(packetDetail, pointGiftService.isNew(User().getCpId())));
    }

    /**
     * 查看红包详情
     *
     * @param pointGiftNo
     * @param type        DIRECTIONAL  RANDOM
     * @return
     */
    @RequestMapping(value = "/detail/{pointGiftNo}", method = RequestMethod.GET)
    public ResponseObject<ReceivePacketDetailVO> pointPacketDetail(@PathVariable String pointGiftNo, @RequestParam(required = false) String bizType) {
        final PointGift pointGift = pointGiftService.getByPointGiftNo(pointGiftNo);
        boolean billingType = pointGiftRecordService.getBillingType(bizType, pointGiftNo, User().getCpId());
        final List<UserPointDTO> userPointDTOS = pointGiftRecordService.getPointPacketDetailByPointGiftNo(billingType, pointGiftNo, User().getCpId(), pointGift.getCpid());
        final UserDTO userDTO = pointGiftService.selectByUserInfoPointGiftNo(pointGiftNo);
        return new ResponseObject<>(ReceivePacketDetailVO.valueOf(userDTO, billingType, userPointDTOS, pointGift.getAggregateAmount()));
    }

    private ResponseObject<PointGiftNoVO> sendPointGiftPreCheck(PointGift pointGift, PointTotal pointTotal) {
        if(pointTotal.getUsablePointPacket()!=null && !pointTotal.getUsablePointPacket().equals("")){
            pointTotal.getTotalUsable().add(pointTotal.getUsablePointPacket());
        }
        if (pointTotal.getTotalUsable().compareTo(pointGift.getAggregateAmount()) < 0) {
            return new ResponseObject<>(GlobalErrorCode.COMMISSION_NOT_ENOUGH);
        }
        if (pointGiftService.isNew(User().getCpId())) {
            return new ResponseObject<>(GlobalErrorCode.NEW_MAN_ERROR);
        }
        return null;
    }

    @RequestMapping(value = "/show/{pointGiftNo}", method = RequestMethod.GET)
    public ResponseObject<PointGiftPacketVO> showPointPacketDetail(@PathVariable String pointGiftNo) {
        List<UserPointDTO> userPointDTOS = pointGiftRecordService.getPointPacketDetailByPointGiftNo(pointGiftNo);
        final UserDTO userDTO = pointGiftService.selectByUserInfoPointGiftNo(pointGiftNo);
        return new ResponseObject<>(new PointGiftPacketVO(userPointDTOS, userDTO));
    }

    /**
     * 抽奖德分消费
     */
    @RequestMapping(value = "/pointrain/consume", method = RequestMethod.POST)
    public ResponseObject<Boolean> checkConsumerPoint() {
        PromotionLottery promotionLottery = packetRainService.getCurrentPromotionLottery();
        if (promotionLottery == null) {
            return new ResponseObject<>(false);
        }
        Long timeConsumePoint = Long.valueOf(promotionLottery.getConsumptionScore());
        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);

        User user = User();
        //检查是否已经抽过五次上限
        String lotteryKey = GET_DAILY_USER_LOTTERY_KEY.apply(promotionLottery.getId(), user.getCpId());
        if (redisUtils.hasKey(lotteryKey) && redisUtils.get(lotteryKey) <= 0) {
            return new ResponseObject<>(GlobalErrorCode.PACKET_RAIN_LOTTERY_LIMIT);
        }

        Boolean consumerCheckBoolean = pointGiftService.consumePacketPoint(user.getCpId(), timeConsumePoint);
        //检查消费积分是否足够
        if (Boolean.FALSE.equals(consumerCheckBoolean)) {
            return new ResponseObject<>(GlobalErrorCode.PACKET_RAIN_COMMISSION_NOT_ENOUGH);
        }

        //检查是否有资格
        pointGiftService.prePacketRainCheck(promotionLottery.getId(), user.getCpId(), timeConsumePoint);

        return new ResponseObject<>();
    }
}
