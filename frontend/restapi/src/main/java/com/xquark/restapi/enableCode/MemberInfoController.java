package com.xquark.restapi.enableCode;

import com.alibaba.fastjson.JSONObject;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.dal.mapper.MemberInfoMapper;
import com.xquark.dal.model.Empower;
import com.xquark.dal.model.User;
import com.xquark.dal.status.EmpowerCode;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.wechat.WechatController;
import com.xquark.service.enableCode.EnableCodeService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.utils.SnowflakeIdWorker;
import com.xquark.utils.unionpay.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/empower")
public class MemberInfoController extends BaseController {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private EnableCodeService enableCodeService;

    @Autowired
    private WechatController wechatController;

    @Autowired
    private Qiniu qiniu;

    @Autowired
    private MemberInfoMapper memberInfoMapper;

    @Value("${empower.host.url}")
    private String host;

    private SnowflakeIdWorker snowflakeIdWorker;

    /**
     * 根据身份查询赋能卡数据
     * @return
     */
    @RequestMapping("/memberInfo")
    @ResponseBody
    public ResponseObject<Empower> memberInfo(@RequestParam(required = false) Long cpId,@RequestParam(required = false) Boolean isHyaline){

        log.info(new StringBuffer()
                .append("赋能接口获取商家信息调用接口参数")
                .append(cpId)
                .toString());

        //店铺id
        Integer shopId=-1;

        if(cpId==null || cpId.equals("")){
            User currentIUser = (User)getCurrentIUser();
            cpId=currentIUser.getCpId();
        }

        if(isHyaline==null || isHyaline.equals("")){
            isHyaline=false;
        }
        //商铺名称
        String shopName="";

        String path="/v1/ee/query_shop";
        Map<String,String> mapId=new HashMap<>();
        mapId.put("cpid",cpId.toString());

        try {
            log.info(new StringBuffer()
            .append("赋能接口获取商家信息的请求参数cpid")
            .append(mapId)
            .toString());
            HttpClient hc = new HttpClient(host+path, 30000, 30000);
            int status = hc.send(mapId, "UTF-8");

            if(status == 429)
                log.info(new StringBuffer().append("返回状态码是429").append("对应参数是").append(mapId).toString());

            log.info(new StringBuffer().append("赋能接口获取商家信息的状态码是").append(status)
                    .append("{}返回信息是")
                    .append(hc.getResult())
                    .toString());

            if (200 == status) {
                String resultString =  hc.getResult();
                if (null != resultString && !"".equals(resultString)) {

                    // 将返回结果转换为map
                    Map<String,Object> parse = (Map<String,Object>)JSONObject.parse(resultString);
                    Integer isOpen=(Integer) parse.get("is_open");
                    if(isOpen!=0 && isOpen!=-1) {
                        shopId = (Integer) parse.get("shop_id");
                        shopName = parse.get("shop_name").toString();
                    }
                }
            }
        }catch (Exception e){
            log.error("赋能接口获取商家信息调用接口出错", e.getMessage());
        }



        //获取用户信息
         Empower empower=enableCodeService.memberInfo(cpId,shopId,shopName);

        //如果为赋能个人，店铺名称赋值
        Long UpCpid =null;
        Long stroge=memberInfoMapper.selectStrogeCpId(cpId);
        Long subStroge=memberInfoMapper.selectSubStrogeCpId(cpId);
        Long shareCpid=memberInfoMapper.selectShareCpid(cpId);
        Long ToCpid=memberInfoMapper.selectToCpId(cpId);

        if(shareCpid!=null && !shareCpid.equals("")){
            UpCpid=shareCpid;
        }
        if(ToCpid!=null && !ToCpid.equals("")){
            UpCpid=ToCpid;
        }
        if(subStroge!=null && !subStroge.equals("")){
            UpCpid=subStroge;
        }
        if(stroge!=null && !stroge.equals("")){
            UpCpid=stroge;
        }

        if(UpCpid!=null && !UpCpid.equals("") && empower.getType()!=1) {

            Map<String, String> map = new HashMap<>();

            map.put("cpid", UpCpid.toString());

            try {
                log.info(new StringBuffer()
                        .append("赋能接口获取商家信息的请求参数upCpid")
                        .append(map)
                        .toString());
                HttpClient hc = new HttpClient(host+path, 30000, 30000);
                int status = hc.send(map, "UTF-8");
                log.info(new StringBuffer().append("赋能接口获取商家信息的状态码是").append(status)
                        .append("{}返回信息是")
                        .append(hc.getResult())
                        .toString());

                if(status == 429)
                    log.info(new StringBuffer().append("返回状态码是429").append("对应参数是").append(mapId).toString());

                if (200 == status) {
                    String resultString = hc.getResult();
                    if (null != resultString && !"".equals(resultString)) {

                        // 将返回结果转换为map
                        Map<String,Object> parse = (Map<String,Object>)JSONObject.parse(resultString);
                        Integer isOpen=(Integer) parse.get("is_open");
                        int shop_id=(Integer) parse.get("shop_id");
                        if((ToCpid==null || ToCpid.equals("")) && shop_id!=-1 ){

                        }else{
                            if( isOpen!=0 && isOpen!=-1) {
                                shopName = parse.get("shop_name").toString();
                                empower.setShopName(shopName);
                            }
                        }

                    }
                }
            }catch (Exception e){
                log.error("赋能接口获取商家信息调用接口出错", e.getMessage());
            }
        }


        //获取二维码base64图片
        ResponseObject<Map<String, Object>> mapResponseObject = wechatController.miniProgramCodeForSelfCode(null, null,cpId,isHyaline);
        Map<String, Object> qrImg=mapResponseObject.getData();
        empower.setEmpowerText(EmpowerCode.EmpowerText.getWord());
        empower.setQrCode(qrImg.get("imageData").toString());

        log.info(new StringBuffer().append("赋能扫码返回前端的数据是")
        .append(empower).toString());

        return new ResponseObject<>(empower);
    }


    /**
     * 根据cpid修改昵称
     * @param nickName
     * @return
     */
    @RequestMapping("/updateNickName")
    @ResponseBody
    public ResponseObject<Object> updateNickName( String nickName) {
        User currentIUser = (User)getCurrentIUser();
        if(nickName.length()>20){
            throw new BizException(GlobalErrorCode.ERROR_PARAM, "昵称不能超过20个字！");
        }
        boolean a=enableCodeService.updateNickName(nickName, currentIUser.getCpId());

        return new ResponseObject<>(a);
    }



}
