package com.xquark.restapi.logisticsPlatform;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.Supplier;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import com.xquark.openapi.polyapi.PolyapiOrderController;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponsePOrder;
import com.xquark.service.logisticsPlatform.LogisticsOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * author: liuwei Date: 18-9-19. Time: 上午10:58
 */
@RestController
public class LogisticsOrderController extends BaseController {

    @Autowired
    private LogisticsOrderService orderService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PolyapiOrderController.class);

    @RequestMapping(
            value = "/logistics/order",
            consumes = "application/x-www-form-urlencoded;charset=utf-8",
            produces = "application/x-www-form-urlencoded;charset=utf-8")
    public String getOrder(HttpServletRequest request) {
        LOGGER.info("========== 开始处理订单请求 ===========");
        ResponsePOrder<List<PolyapiOrderItemVO>> ro = new ResponsePOrder<>();
        try {
            String bizcontent = request.getParameter("bizcontent");
            JSONObject json = JSONObject.parseObject(bizcontent);
            String StartTime = json.getString("StartTime");
            String EndTime = json.getString("EndTime");
            String OrderStatus = json.getString("OrderStatus");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Supplier supplier = (Supplier) request.getAttribute("supplier");
            Date startTime = sdf.parse(StartTime);
            Date endTime = sdf.parse(EndTime);
            Integer pageIndex, pageSize;
            try {
                pageIndex = Optional.ofNullable(json.getInteger("PageIndex")).orElse(1);
                pageSize = Optional.ofNullable(json.getInteger("PageSize")).orElse(99999);
            } catch (Exception e) {
                log.error("分页参数错误", e);
                pageIndex = 1;
                pageSize = 99999;
            }
      if (OrderStatus.equalsIgnoreCase("JH_99")) { // 全部订单
                List<PolyapiOrderItemVO> OrderVO =
                        orderService.getAllOrder(pageIndex, pageSize, startTime, endTime, supplier.getCode());
                ro.setCode("10000");
                ro.setMessage("SUCCESS");
                ro.setOrders(OrderVO);
                ro.setNumtotalorder(
                        Integer.parseInt(
                                orderService.getNumTotalOrder(null, supplier.getCode(), startTime, endTime)));

      } else if (OrderStatus.equalsIgnoreCase("JH_02")) { // 代发货
                // 由于只有支付成功状态，而且支付成功一小时之内客户可取消订单，所以判断订单是否过了一个小时
                List<PolyapiOrderItemVO> OrderVO =
                        orderService.getOrder(
                                pageIndex, pageSize, startTime, endTime, "PAID", supplier.getCode());
                ro.setCode("10000");
                ro.setMessage("SUCCESS");
                ro.setOrders(OrderVO);
        ro.setNumtotalorder(orderService
                .countOrderItem(startTime, endTime, "PAID", supplier.getCode())
                .intValue());
            } else {
                ro.setCode("40000");
                ro.setMessage("该状态暂不支持");
            }
        } catch (Exception e) {
            ro.setCode("10000");
            ro.setMessage("内部错误, " + e.getMessage());
            log.error("订单拉取失败", e);
        }
        return JSONObject.toJSONString(ro);
    }
}
