package com.xquark.restapi.order;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.mapper.OrderSendMapper;
import com.xquark.dal.model.OrderSend;
import com.xquark.service.order.OrderSendService;
import com.xquark.service.yundou.OrderSendResult;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User: shihao
 * Date: 2018/6/17.
 * Time: 15:21
 */
@Controller
@RequestMapping("/openapi")
public class OrderSendController{
  @Autowired
  private OrderSendService orderSendService;

  @Autowired
  private OrderSendMapper orderSendMapper;

  private final static Logger LOGGER = LoggerFactory.getLogger(OrderSendController.class);

  /**
   * 增加
   *
   * @param request
   * @return
   */
  @Transactional
  @ResponseBody
  @RequestMapping(value = "/polyapi/ordersend",method = RequestMethod.POST,
      consumes = "application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  public String add(HttpServletRequest request) {
    LOGGER.info("========== 开始处理订单发货请求 ===========");
    OrderSend os = new OrderSend();
    OrderSendResult osr = new OrderSendResult();
    String bizcontent = request.getParameter("bizcontent");
    JSONObject json = JSONObject.parseObject(bizcontent);
    String orderNo = json.getString("PlatOrderNo");
    String logisticName = json.getString("LogisticName");
    os.setLogisticName(logisticName);
    os.setLogisticType(json.getString("LogisticType"));
    String logisticNo = json.getString("LogisticNo");
    os.setLogisticNo(logisticNo);
    os.setPlatOrderNo(orderNo);
    os.setIsSplit(Integer.parseInt(json.getString("IsSplit")));
//    os.setSubPlatOrderNo(json.getString("SubPlatOrderNo"));
    os.setSenderName(json.getString("SenderName"));
    os.setSenderTel(json.getString("SenderTel"));
    os.setSenderAddress(json.getString("SenderAddress"));
    try {
      if(json.getString("SendType") != null){
        os.setSendType(json.getString("SendType"));
      }
      if(json.getString("SenderName") != null){
        os.setSenderName(json.getString("SenderName"));
      }
      if(json.getString("SenderTel") != null){
        os.setSenderTel(json.getString("SenderTel"));
      }
      if(json.getString("SenderAddress") != null){
        os.setSenderAddress(json.getString("SenderAddress"));
      }
      if(json.getString("IsHwgFlag") != null){
        os.setIsHwgFlag(Integer.parseInt(json.getString("IsHwgFlag")));
      }
      if(json.getString("SubPlatOrderNo").contains("|")){
        String subPlatOrderNo = json.getString("SubPlatOrderNo");
//        String[] subPlatOrderNos = subPlatOrderNo.split("|");
//        for(int i=0;i<subPlatOrderNos.length;i++){
//          String[] subordernos = subPlatOrderNos[i].split(":");
//          String suborderno = subordernos[0];
//          String count = subordernos[1];
//        }
        os.setSubPlatOrderNo(subPlatOrderNo);
      }
        os.setSubPlatOrderNo("");
        os.setCreatedAt(new Date());
        os.setUpdatedAt(new Date());
        LOGGER.info("========== 调用service层 ===========");
//        orderSendService.add(os);
        orderSendMapper.updateLogisticsInfo(orderNo,logisticName,logisticNo);
        osr.setCode("10000");
        osr.setMessage("SUCCESS");
        osr.setSubcode(null);
        osr.setSubmessage(null);

    } catch (RuntimeException e) {
      osr.setCode("40000");
      osr.setMessage("System Error");
      osr.setSubcode("GSE.SYSTEM_ERROR");
      osr.setSubmessage("[20887]服务异常请稍后再试");
    }
    return JSONObject.toJSONString(osr);
  }

}
