package com.xquark.restapi.tweet;

import com.xquark.dal.model.MarketingTweet;
import com.xquark.dal.model.TweetImage;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * Created by chh on 16-10-27.
 */
public class MarketingTweetAppVO extends MarketingTweet {

  private String dayDesc;

  public String getDayDesc() {
    return dayDesc;
  }

  public void setDayDesc(String dayDesc) {
    this.dayDesc = dayDesc;
  }

  private List<TweetImage> imgs;

  public List<TweetImage> getImgs() {
    return imgs;
  }

  public void setImgs(List<TweetImage> imgs) {
    this.imgs = imgs;
  }

  public MarketingTweetAppVO(MarketingTweet marketingTweet) {
    BeanUtils.copyProperties(marketingTweet, this);
  }
}
