package com.xquark.restapi.merchant;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.MerchantRole;
import com.xquark.dal.vo.MerchantRoleVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.merchant.MerchantRoleService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 管理员角色controller Created by chh on 16-12-29.
 */
@Controller
@ApiIgnore
public class MerchantRoleController extends BaseController {

  @Autowired
  private MerchantRoleService merchantRoleService;

  /**
   * 获取用户对应角色列表
   */
  @ResponseBody
  @RequestMapping("/merchantRole/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword) {
    String shopId = this.getCurrentIUser().getShopId();
    List<MerchantRoleVO> roles = null;
    roles = merchantRoleService.list(pageable, keyword);

    Map<String, Object> aRetMap = new HashMap<>();
    // 查找除了ROOT和BASIC之外的角色总数
    aRetMap.put("total", merchantRoleService.selectNormalCnt());
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存用户对应角色
   */
  @ResponseBody
  @RequestMapping("/merchantRole/save")
  public ResponseObject<Boolean> savePromotion(MerchantRole role) {
    long result = 0;
    if (StringUtils.isNotEmpty(role.getId())) {
      result = merchantRoleService.update(role);
    } else {
      role.setArchive(false);
      result = merchantRoleService.create(role);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/merchantRole/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = merchantRoleService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的用户对应角色记录<br>
   */
  @ResponseBody
  @RequestMapping("/merchantRole/{id}")
  public ResponseObject<MerchantRole> view(@PathVariable String id, HttpServletRequest req) {
    MerchantRole role = merchantRoleService.load(id);
    return new ResponseObject<>(role);
  }


}
