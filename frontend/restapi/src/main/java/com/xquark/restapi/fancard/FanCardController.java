package com.xquark.restapi.fancard;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.model.FanCardApply;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fancard.FanCardService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 17-11-7. DESC:
 */
@RestController
@RequestMapping("/fanCard")
@Api(value = "fanCard", description = "范卡管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class FanCardController extends BaseController {

  private FanCardService fanCardService;

  @Autowired
  public void setFanCardService(FanCardService fanCardService) {
    this.fanCardService = fanCardService;
  }

  /**
   * 用户申请范卡号码
   *
   * @param applyForm 申请表单
   * @param errors 错误校验对象
   * @return 申请是否成功
   * @throws BizException 用户已经申请过或其他业务错误
   */
  @RequestMapping("/apply")
  public ResponseObject<Boolean> apply(@Validated FanCardApplyForm applyForm, Errors errors) {
    ControllerHelper.checkException(errors);
    IUser user = getCurrentIUser();
    String userId = user.getId();

    FanCardApply applied = fanCardService.loadByUserId(userId);
    if (applied != null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "您已经申请过范卡, 请不要重复提交");
    }
    FanCardApply apply = new FanCardApply();
    BeanUtils.copyProperties(applyForm, apply);
    boolean result = fanCardService.apply(userId, apply);
    return new ResponseObject<>(result);
  }

  /**
   * 查看已申请的范卡id
   *
   * @return 范卡id
   * @throws BizException 如果用户未申请过范卡
   */
  @RequestMapping("/viewCode")
  public ResponseObject<String> viewCode() {
    IUser user = getCurrentIUser();
    String userId = user.getId();
    User dbUser = userService.load(userId);
    if (dbUser == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "用户不存在");
    }
    String fanCardCode = dbUser.getFanCardNo();
    return new ResponseObject<>(fanCardCode);
  }

  /**
   * 通过code兑换积分
   *
   * @param code 邀请码
   * @return true or false
   */
  @RequestMapping("/exchange")
  @ApiOperation(value = "通过邀请码兑换积分", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> exchage(@RequestParam String code) {
    String userId = getCurrentIUser().getId();
    boolean result = fanCardService.exchange(userId, code);
    return new ResponseObject<>(result);
  }

}
