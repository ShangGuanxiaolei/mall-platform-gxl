package com.xquark.restapi.deleveryRegion;

import com.xquark.dal.vo.XquarkProduct;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.deleveryRegion.ProductDeliveryServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2018/12/24
 * Time: 9:49
 * Description: 区域配送
 */
@Controller
@RequestMapping("/delivery")
public class ProductDeleveryController {

    @Autowired
    private ProductDeliveryServiceImpl productDeliveryService;

    //更新商品区域配送省份
    @RequestMapping("/product/update/provinceid")
    @ResponseBody
    public ResponseObject<Boolean> updateProductDeleveryByProductid(@Param("id") Integer id, @Param("provinceid")String provinceid){
        Integer integer = productDeliveryService.updateProductDeliveryByid(id, provinceid);
        return new ResponseObject<>(integer>0);
    }

    //判断用户地址是否在配送范围内以及是否是丰顺发运

    @RequestMapping(value = "/product/query/provinceid",produces = "application/json; charset=utf-8")
    @ResponseBody
    //id:商品id,provinceid:省份id
    public ResponseObject<List<Map<String,Object>>> queryDeliveryByProductid(@Param("code")String code ,@Param("provincename")String provincename) {
        String[] split = code.split(",");
        List<String> code1 = Arrays.asList(split);
        List<XquarkProduct>  lista = new ArrayList<>();
        for (String s : code1) {
            XquarkProduct xquarkProduct = productDeliveryService.queryDeliveryByid(s);
//            String deliveryRegion = xquarkProduct.getDeliveryRegion();
//            System.out.println(deliveryRegion);
            lista.add(xquarkProduct);
        }
        List<String> list2 = new ArrayList<>();
        for (String s : code1) {
            List<String> source = productDeliveryService.querySfByCoid(s);
            list2.add(source.get(0));
        }
//        System.out.println(source);
//        System.out.println(xquarkProduct);
//        String str = xquarkProduct.getDeliveryRegion().replaceAll("[\\u4e00-\\u9fa5]", "");
//        String s = str.replaceAll(".,", ",");
//        String[] split = s.split(",");
//        Arrays.sort(split);
//        Integer integer = productDeliveryService.queryParentId(regionid);
        List<Map<String,Object>> map1 = new LinkedList<>();
        for (int i = 0; i < lista.size(); i++) {
            if ((lista.get(i).getDeliveryRegion().contains(provincename) || "全国".equals(lista.get(i).getDeliveryRegion())) && "SF".equals(list2.get(i))) {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("code",lista.get(i).getCode());
                map.put("provincename", true);
                map.put("source", true);
                map1.add(map);
            } else if (!(lista.get(i).getDeliveryRegion().contains(provincename) || "全国".equals(lista.get(i).getDeliveryRegion())) && !"SF".equals(list2.get(i))) {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("code",lista.get(i).getCode());
                map.put("provincename", false);
                map.put("source", false);
                //判断如果不在配送范围内前端可能传输是市级单位,找出父省级再进行校验
                String parent = productDeliveryService.getParent(provincename);
                if(null!=parent && !parent.equals("")){
                    if(lista.get(i).getDeliveryRegion().contains(parent)){
                        map.put("provincename", true);
                    }
                }
                map1.add(map);
            } else if ((lista.get(i).getDeliveryRegion().contains(provincename) || "全国".equals(lista.get(i).getDeliveryRegion())) && !"SF".equals(list2.get(i))) {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("code",lista.get(i).getCode());
                map.put("provincename", true);
                map.put("source", false);
                map1.add(map);
            } else if (!(lista.get(i).getDeliveryRegion().contains(provincename) ||  "全国".equals(lista.get(i).getDeliveryRegion())) && "SF".equals(list2.get(i))) {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("code",lista.get(i).getCode());
                map.put("provincename", false);
                map.put("source", true);
                map1.add(map);
                String parent = productDeliveryService.getParent(provincename);
                if(null!=parent && !parent.equals("")){
                    if(lista.get(i).getDeliveryRegion().contains(parent)){
                        map.put("provincename", true);
                    }
                }
            }
        }
        return new ResponseObject<>(map1);
    }
//        } else {
//            int i = Arrays.binarySearch(split, integer.toString());
//            if(i>=0 || xquarkProduct.getDeliveryRegion().equals("全国")){
//                return new ResponseObject<>(true);
//            }else {
//                return new ResponseObject<>(false);



    //查询区域配送省份
    @RequestMapping(value = "/product/query/region",produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public ResponseObject<List<Map<String,String>>> queryUserDefaultAddress(){
        List<Map<String, String>>maps = productDeliveryService.queryRegionById();
        System.out.println(maps);
        return new ResponseObject<>(maps);
    }

}