package com.xquark.restapi.brand;

import com.google.common.collect.ImmutableMap;
import com.xquark.dal.model.Brand;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.utils.SearchForm;
import com.xquark.restapi.utils.SearchFormUtils;
import com.xquark.service.brand.BrandService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * User: huangjie
 * Date: 2018/6/4.
 * Time: 上午11:20
 * 品牌
 */
@RestController
@RequestMapping("/brand")
public class BrandController {

    private BrandService brandService;

    @Autowired
    public void setBrandService(BrandService brandService) {
        this.brandService = brandService;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseObject<Boolean> save(@Valid Brand brand) {
        boolean ret = false;
        if (StringUtils.isNotBlank(brand.getId())) {
            ret = brandService.update(brand);
        } else {
            ret = brandService.save(brand);
        }
        return new ResponseObject<>(ret);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ResponseObject<Boolean> delete(@NotBlank @PathVariable("id") String id) {
        final boolean ret = brandService.deleteByPrimaryKey(id);
        return new ResponseObject<>(ret);
    }


    @RequestMapping(value = "/batchDelete", method = RequestMethod.GET)
    public ResponseObject<Boolean> batchDelete(@NotBlank @RequestParam("ids") String id) {
        String[] ids = id.split(",");
        boolean ret = brandService.batchDelete(Arrays.asList(ids));
        return new ResponseObject<>(ret);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseObject<Brand> getById(@NotBlank @PathVariable("id") String id) {
        Brand b = brandService.load(id);
        return new ResponseObject<>(b);
    }

    /**
     * 将商品与品牌关联
     *
     * @param productId 商品id
     * @param brandId   品牌id
     * @return
     */
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public ResponseObject<Boolean> addProductBrand(@NotBlank String productId, @NotBlank String brandId) {
        boolean ret = brandService.addProductBrand(productId, brandId);
        return new ResponseObject<>(ret);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseObject<Map<String, ? extends Object>> list(SearchForm searchForm, Pageable page,
        boolean pageable) {
        Map<String, Object> params = SearchFormUtils
            .checkAndGenerateMapParams(searchForm, page, pageable);
        List<? extends Brand> list = brandService.list(params);
        Long num = brandService.count(params);
        return new ResponseObject(ImmutableMap.of("list", list, "total", num));
    }

    /**
     * 查询商品关联的品牌
     */
    @RequestMapping(value = "/list/product/{id}", method = RequestMethod.GET)
    public ResponseObject<? extends List<? extends Brand>> list(
        @PathVariable("id") @NotBlank String id) {
        List<? extends Brand> list = brandService.listRelatedBrand(id);
        return new ResponseObject<>(list);
    }

}
