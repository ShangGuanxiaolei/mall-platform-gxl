package com.xquark.restapi.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.url.UrlHelper;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.WechatConstants;
import com.xquark.dal.mapper.CustomerCareerLevelMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.dal.vo.BaseCustomerVO;
import com.xquark.dal.vo.IdentityVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.user.UserVO;
import com.xquark.service.config.SystemConfigService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.UserLogService;
import com.xquark.service.user.UserScanService;
import com.xquark.service.user.impl.WechatThirdUserConverter;
import com.xquark.service.user.vo.ScanCodeType;
import com.xquark.service.user.vo.UserLogInfo;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.userAgent.impl.UserSigninLogFactory;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.service.wechat.WechatService;
import com.xquark.utils.EncryptUtil;
import com.xquark.utils.SnowflakeIdWorker;
import com.xquark.utils.StringUtil;
import com.xquark.utils.unionpay.HttpClient;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.oauth.OAuthException;
import com.xquark.wechat.oauth.OAuthManager;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import com.xquark.wechat.oauth.protocol.get_access_token.DecryptedResponse;
import com.xquark.wechat.oauth.protocol.get_session_key.GetSessionKeyRequest;
import com.xquark.wechat.oauth.protocol.get_session_key.GetSessionKeyResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkState;
import static com.xquark.service.error.GlobalErrorCode.ERROR_PARAM;
import static com.xquark.service.error.GlobalErrorCode.WECHAT_MINI_PROGRAM_LOGIN_ERROR;

@Controller
@RequestMapping("/wechat")
public class WechatController extends BaseController {

    // 小程序邀请码redis中key的关键字
    private final String MINI_PROGRAM_INVITE_CODE = "hw:cus:miniProgram:inviteCode:";
    private final WechatAppConfigService wechatAppConfigService;

    private final ShopTreeService shopTreeService;

    private final WechatService wechatService;

    private final CustomerProfileService customerProfileService;

    private final TinyUrlService tinyUrlService;

    private WechatThirdUserConverter wechatUserConverter;

    private SnowflakeIdWorker snowflakeIdWorker;
    @Autowired
    private FreshManService freshManService;

    private UserScanService userScanService;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SystemConfigService systemConfigService;

    @Value("${empower.host.url}")
    private String host;
    @Autowired
    public WechatController(
            WechatAppConfigService wechatAppConfigService,
            WechatService wechatService,
            ShopTreeService shopTreeService,
            CustomerProfileService customerProfileService,
            TinyUrlService tinyUrlService, SnowflakeIdWorker snowflakeIdWorker,
            UserScanService userScanService) {
        this.wechatAppConfigService = wechatAppConfigService;
        this.wechatService = wechatService;
        this.shopTreeService = shopTreeService;
        this.customerProfileService = customerProfileService;
        this.tinyUrlService = tinyUrlService;
        this.snowflakeIdWorker = snowflakeIdWorker;
        this.userScanService = userScanService;
    }

    @Autowired
    @Qualifier("jsonRedisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private ShopService shopService;

    @Autowired
    private UrlHelper urlHelper;

    @Autowired
    private Qiniu qiniu;

    @Autowired
    private CustomerCareerLevelMapper customerCareerLevelMapper;

    @Autowired
    public void setWechatUserConverter(WechatThirdUserConverter wechatUserConverter) {
        this.wechatUserConverter = wechatUserConverter;
    }

    @Autowired
    private UserSigninLogService userSigninLogService;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    UserLogService userLogService;

    @Autowired
    private PromotionConfigService promotionConfigService;

    @ResponseBody
    @RequestMapping("/isDefaultExist")
    public ResponseObject<Boolean> isDefaultExist() {
        Boolean isExist = false;
        String shopId = getCurrentIUser().getShopId();
        WechatAppConfig wechatAppConfig = wechatAppConfigService.load(shopId);
        if (wechatAppConfig != null) {
            isExist = true;
        }
        return new ResponseObject<>(isExist);
    }

    @ResponseBody
    @RequestMapping("/shareConfig")
    public ResponseObject<ShareConfigVO> getShareInfo(@RequestParam("shareUrl") String currUrl) {

        User user = (User) getCurrentIUser();
        String shopId = user.getCurrentSellerShopId();
        ShopTree shopTree = shopTreeService.selectRootShopByShopId(shopId);
        WechatAppConfig appConfig = wechatAppConfigService.load(shopTree.getRootShopId());
        String timestamp = Long.toString(new Date().getTime() / 1000);
        String noncestr = CommonUtil.CreateNoncestr();

        String url;
        try {
            url = StringUtils.substringBeforeLast(URLDecoder.decode(currUrl, CharEncoding.UTF_8), "#");
        } catch (UnsupportedEncodingException e) {
            String msg = "url编码不正确";
            log.error(msg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
        }
        // sign
        String jsApiTicket = wechatService.obtainJsapiTicket(appConfig);
        if (jsApiTicket == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "can not obtain jsapi ticket.");
        }
        String signValue =
                "jsapi_ticket="
                        + jsApiTicket
                        + "&noncestr="
                        + noncestr
                        + "&timestamp="
                        + timestamp
                        + "&url="
                        + url;
        String signature = SHA1Util.Sha1((signValue));

        ShareConfigVO configVO =
                new ShareConfigVO(appConfig.getAppId(), timestamp, noncestr, signature);
        return new ResponseObject<>(configVO);
    }

    /**
     * 开发用getSession, 正式发布时去除
     */
    @ResponseBody
    @RequestMapping("/getSessionDev")
    public ResponseObject<Map<String, Object>> getSessionIdDev(@RequestParam Long cpId,
        @RequestParam String token,
        HttpServletRequest request) {
        if (!StringUtils.equals(token, "xquark-dev")) {
            throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "token不正确");
        }
        String sessionId = request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);

        if (StringUtils.isBlank(sessionId)) {
            sessionId = String.valueOf(snowflakeIdWorker.nextId());
        }

        User user = userService.loadByCpId(cpId);
        Shop shop = shopService.findByUser(user.getId());
        if (shop != null) {
            user.setShopId(shop.getId());
            user.setCurrentSellerShopId(shop.getId());
        }

        if (!redisTemplate.hasKey(sessionId)) {
            // 生成一个32位的随机字符串作为key保存到redis, 有效期为29天
            // 参考 http://www.cnblogs.com/nosqlcoco/p/6105749.html
            redisTemplate.opsForValue().set(sessionId, user);
            redisTemplate.expire(sessionId, 29, TimeUnit.DAYS);
        }

        UserVO userVO = new UserVO(user, urlHelper.genShopUrl(user.getShopId()));
        userVO.setType(userService.getUserType(userVO.getId()));

        Map<String, Object> result = new HashMap<>();
        result.put("sessionId", sessionId);
        result.put("user", userVO);
        return new ResponseObject<>(result);
    }

    /**
     * 通过微信信息换取unionId, 再通过unionId找出对应的多个用户
     *
     * @see com.xquark.authentication.CustomerProfileAuthenticationProvider
     */
    @ResponseBody
    @RequestMapping("/profiles")
    public ResponseObject<Map<String, Object>> getCpUserByWechatCode(@RequestParam String code) {
        log.info("==== 请求profiles code: {} ====", code);
        log.info("==== 请求微信用户信息 ====");
        WechatUserBean wechatUserBean = wechatService.getWechatUserInfoByCode(code);
        assert wechatUserBean != null;
        String unionId = wechatUserBean.getUnionId();
        log.info("==== 微信用户信息获取成功, unionId: {} ====", unionId);
        boolean isUnionIdExists = customerProfileService.isWechatUnionExists(unionId);
        if (!isUnionIdExists) {
            log.info("==== unionId不存在, 创建用户 ====");
            User user = userService.createUserFromThirdUserIfNotExists(wechatUserConverter,
                    wechatUserBean, null, null);
            //汉薇用户表中更新一下状态，标记为发送德分 create by yarm
            if(user != null){
                this.grantRegisterPoint(user);
                // 绑关系
                this.boundSubStrong(user.getCpId(),null,null, null, "app");
            }
        }
        //todo 更新最新头像
        this.updateCustomerHeadImage(unionId,wechatUserBean.getAvatar(),wechatUserBean.getNickName());

        List<BaseCustomerVO> customerProfiles = customerProfileService.listCustomerByUnionId(unionId);
        checkState(CollectionUtils.isNotEmpty(customerProfiles), "用户创建失败");
        EncryptUtil des1;
        String cpToken;
        try {
            des1 = new EncryptUtil(); // 使用默认密钥
            // add salt
            cpToken = des1.encrypt(unionId);
        } catch (Exception e) {
            log.error("token加密初始化失败", e);
            throw new BizException(GlobalErrorCode.UNKNOWN, "内部错误, 请稍后再试");
        }
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        // 两分钟失效
        redisUtils.set("cpToken:" + code, cpToken, 2, TimeUnit.MINUTES);
        Map<String, Object> ret =
                ImmutableMap.of(
                        "cpToken", cpToken,
                        "profiles", customerProfiles);
        log.info("==== 微信信息请求成功, 返回cpToken: {}, profiles: {} ====", cpToken, customerProfiles);
        return new ResponseObject<>(ret);
    }

    //更新用户最新头像
    private void updateCustomerHeadImage(String unionId,String headUrl,String nickName){
        if (nickName == null || "".equals(nickName)) {
            nickName = "普通用户";
        }

        User dbUser = userService.loadByUnionId(unionId);

        if (dbUser != null)
            {
               if(StringUtils.isNotBlank(headUrl)){
                   String avatar = dbUser.getAvatar();
                   if(StringUtils.isNotBlank(avatar)){
                       //http://thirdwx.qlogo.cn
                       String replacethird = avatar.replace("http://thirdwx.qlogo.cn", "");
                       String replacewx = replacethird.replace("https://wx.qlogo.cn", "");

                       String replacehead = headUrl.replace("http://thirdwx.qlogo.cn", "");
                       String replacewxhead = replacehead.replace("https://wx.qlogo.cn", "");
                       if(!replacewxhead.equals(replacewx)){
                           //更新头像 customerwechatunion 和 xquark_user
                           userService.updateHeadImageByUnionId(headUrl,unionId);
                       }
                   }else{
                       //更新头像
                       userService.updateHeadImageByUnionId(headUrl,unionId);
                   }
               }
                //更新昵称
               if(StringUtils.isNotBlank(nickName)){
                   userService.updateNickNameByUnionId(unionId,nickName);
               }

            }
    }
   


    /**
     * 汉薇平台新注册用户发送德分
     * @param user
     * @return
     */
    private boolean grantRegisterPoint(User user) {
        try {
            boolean b = this.userService.updateRegisterPointFlag(user.getCpId(), 0);
            if(b){
                log.info(new StringBuffer().append("cpId:").append(user.getCpId().toString())
                        .append("为新人，更新德分发放状态为0").toString());
                boolean isGrantPoint = this.freshManService.grantRegisterPoint(user.getCpId());
                if(isGrantPoint){
                    log.info(new StringBuffer()
                            .append("新注册用户{")
                            .append(user.getCpId())
                            .append("}发送德分成功")
                            .toString());
                }
            }
            // 发送收益
            boolean b1 = this.freshManService.setFreshManCommission(user.getCpId());
            if(b1) log.info("新人" + user.getCpId() + "发送5收益成功");
        }catch (Exception e){
            log.error(new StringBuffer()
                    .append("在汉薇注册的新用户:{")
                    .append(user.getCpId())
                    .append("}发送德分失败")
                    .append("异常信息是{")
                    .append(e.getMessage())
                    .append("}")
                    .toString());
            return false;
        }

        return true;
    }

    @ResponseBody
    @RequestMapping("/miniProfiles")
    public ResponseObject<Map<String, Object>> miniProfiles(
            @Validated MiniProgramAuthTokenForm token, Errors errors,HttpServletRequest request) {

        log.info("小程序注册request参数：" + token);
        ControllerHelper.checkException(errors);
        WechatAppConfig appConfig = wechatAppConfigService.load();
        if (appConfig == null) {
            String msg = "appId设置未初始化";
            log.error(msg);
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
        }

        //若传入的fromCpId是空则获取保存在库里边的fromCpId
        if(StringUtils.isBlank(token.getFromCpId())
        && StringUtils.isNotBlank(token.getSceneKey())){
            try {
                String fromCpId = getFromCpIdBySceneKey(token.getSceneKey());

                log.info(new StringBuffer()
                        .append("sceneKey")
                        .append(token.getSceneKey())
                        .append("注册实际取到的fromCpId是：")
                        .append(fromCpId)
                        .toString());

                if(StringUtils.isNotBlank(fromCpId)){
                    token.setFromCpId(fromCpId);
                }
            }catch (Exception e){
                log.error("注册获取fromCpId出现异常{}", e.getMessage());
            }

        }

        String appId = appConfig.getAppMiniId();
        String secret = appConfig.getAppMiniSecret();

        Config config = new Config("", "", "", appId, secret, "", "", "", "");
        GetSessionKeyRequest keyRequest = new GetSessionKeyRequest(config, token.getCode());

        // TODO refactor
        GetSessionKeyResponse sessionKeyResponse;
        try {
            sessionKeyResponse = OAuthManager.getSessionKey(keyRequest);
        } catch (OAuthException e) {
            String msg = "微信返回错误, " + e;
            log.error(msg);
            throw new BizException(WECHAT_MINI_PROGRAM_LOGIN_ERROR, msg);
        }
        String sessionKey = sessionKeyResponse.getSession_key();
        String encryptData = token.getEncryptedData();
        String iv = token.getIv();
        DecryptedResponse wechatUserBean;
        try {
            wechatUserBean = DecryptedResponse.fromEncryptData(sessionKey, encryptData, iv);
        } catch (Exception e) {
            String msg = e.toString();
            log.error(msg);
            throw new BizException(WECHAT_MINI_PROGRAM_LOGIN_ERROR, msg);
        }
        if (wechatUserBean == null) {
            String msg = "解密微信用户信息失败";
            log.error(msg);
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, msg);
        }

        String unionId = wechatUserBean.getUnionId();
        log.info("==== 小程序unionId: {} ====", unionId);
        boolean isUnionIdExists = customerProfileService.isWechatUnionExists(unionId);
        if (!isUnionIdExists) {
            log.info("==== unionId不存在, 创建用户 ====");
            String upLineCpId = token.getFromCpId();
            if(StringUtils.isNotBlank(upLineCpId))
                log.info("绑定关系fromCpId:"+upLineCpId);
            if (StringUtil.isNull(upLineCpId) || "null".equals(upLineCpId)) {
                upLineCpId = null;
            }
            User user = userService.createUserFromThirdUserIfNotExists(wechatUserConverter,
                    wechatUserBean, upLineCpId, token.getChannel());
            if(user != null){
                //扫码注册重定向标记
                RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
                redisUtils.set("scanRedirect:" + user.getCpId(), new StringBuffer().append(user.getCpId())
                        .toString());
                //扫赋能提供的付款码，调用绑定次强关系之后删除
                redisUtils.set("scanPay4SubStrong:" + user.getCpId(), new StringBuffer().append(user.getCpId())
                        .toString());
                //注册发送德分
                boolean b = this.grantRegisterPoint(user);
                if(b){
                    log.info(new StringBuffer()
                    .append("cpId是：")
                    .append(user.getCpId())
                    .append("在小程序注册发送德分成功")
                    .toString());
                }

                // 注册绑定次强，只是本期需求临时启用的方法，后续统一维护到结算中心
                log.info("小程序注册绑定关系入参cpId{" + user.getCpId() + "}fromCpId{" + token.getFromCpId() + "}spreaderId{"
                        + token.getSpreaderId() + "}scanCodeType{" + token.getScanCodeType() + "}");
                this.boundSubStrong(user.getCpId(),token.getFromCpId(),
                        token.getSpreaderId(), token.getScanCodeType(),token.getScanCodeType());
            }
        }
        //更新最新头像
        this.updateCustomerHeadImage(unionId,wechatUserBean.getAvatar(),wechatUserBean.getNickName());

        List<BaseCustomerVO> customerProfiles =
                customerProfileService.listCustomerByUnionId(wechatUserBean.getUnionId());
        checkState(CollectionUtils.isNotEmpty(customerProfiles), "用户创建失败");

        EncryptUtil des1;
        String cpToken;
        try {
            des1 = new EncryptUtil(); // 使用默认密钥
            // add salt
            cpToken = des1.encrypt(wechatUserBean.getUnionId() + ":" + wechatUserBean.getOpenId());
        } catch (Exception e) {
            log.error("token加密初始化失败", e);
            throw new BizException(GlobalErrorCode.UNKNOWN, "内部错误, 请稍后再试");
        }
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        // 两分钟失效
        redisUtils.set("cpToken:" + token.getCode(), cpToken, 2, TimeUnit.MINUTES);
        Map<String, Object> ret =
                ImmutableMap.of(
                        "cpToken", cpToken,
                        "profiles", customerProfiles,"openId",sessionKeyResponse.getOpenid());
        log.info("==== 微信小程序信息请求成功, 返回cpToken: {}, profiles: {} ====", cpToken, customerProfiles);
        return new ResponseObject<>(ret);
    }

    private boolean boundSubStrong(Long cpId, String fromCpId, String spreaderId, String scanCodeType, String source) {
        log.info("注册绑定关系的入参cpId{" + cpId + "}fromCpId{" + fromCpId + "}spreaderId{"
        + spreaderId + "}scanCodeType{" + scanCodeType + "}source{" + source + "}");

        // 开关
        String value = this.systemConfigService.getValue("user_regist_send_commisssion_switch");
        // 关闭
        if("false".equals(value)) return false;

        Long upLine = null;
        if(StringUtils.isNotBlank(fromCpId))
            upLine = Long.valueOf(fromCpId);
        else {
            upLine = 2000002L;
        }
        try {
            // 记录绑定关系来源
            // 2来自小程序
            this.userMapper.updateFromSource(cpId, 2);
            // 汉薇绑定
            userScanService.weekToSubStrong(cpId, upLine);
            // 统一维护到计算中心，注册直接绑定到次强
            // 绑定次强
            // 上级是否是商家
            boolean b = this.userScanService.checkIsEffectShop(upLine);
            if(b){
                //赋能扫码注册用户
                userScanService.boundRelationShip4Empower(cpId, upLine, spreaderId);
            }else{
                //普通注册用户
                freshManService.addCustomerRelationship(cpId, upLine, "1");
            }
        }catch (Exception e){
            log.error("cpId是" + cpId + "绑定关系出现异常" + JSONObject.toJSON(e.getMessage()));
            return false;
        }
        return true;
    }

    /**
     * 新建用户来源，0老数据或未知来源，1app，2赋能个人码，3、赋能商家码，4赋能付款码
     * @param source
     * @return
     */
    private int getFromSourceValue(String source) {
        if(org.apache.commons.lang3.StringUtils.isBlank(source))
            return 0;
        switch (source){
            case "app":
                return 1;
            case "mini":
                return 2;
            case "shopCode":
                return 3;
            case "payCode":
                return 4;
            default:
                return 0;
        }
    }

    private String getFromCpIdBySceneKey(String sceneKey) {
        String fromCpId = "";
        String urlByKey = tinyUrlService.findUrlByKey(sceneKey);
        log.info(new StringBuffer()
        .append("注册获取sceneKey的数据库数据是：")
        .append(urlByKey)
        .toString());
        if(StringUtils.isBlank(urlByKey))
            return fromCpId;

        Map map = JSON.parseObject(urlByKey);

        if(map == null)
            return fromCpId;

        String dataStr =  ((JSONObject) map).getString("data");
        if(StringUtils.isNotBlank(dataStr)){
            Map dataMap = JSON.parseObject(dataStr);
            if(dataMap == null)
                return fromCpId;

            fromCpId = ((JSONObject) dataMap).getString("fromCpId");
            if(StringUtils.isBlank(fromCpId)){
                String id = ((JSONObject) dataMap).getString("id");
                //是否是数字
                boolean numeric = StringUtils.isNumeric(fromCpId);
                if(numeric)
                    fromCpId = id;
            }

            log.info(new StringBuffer()
                    .append("sceneKey")
                    .append(sceneKey)
                    .append("注册从数据库获取到的fromCpId是：")
                    .append(fromCpId)
                    .toString());
        }
        return fromCpId;
    }

    /**
     * 小程序扫码跳转页面
     * @param cpId
     * @param token
     * @return
     */
    private String scanCodeRedirectPageInfo(Long cpId, MiniProgramAuthTokenForm token) {
        if(token != null && StringUtils.isNotBlank(token.getScanCodeType())
        && StringUtils.isNotBlank(token.getFromCpId())){
            try {
                return this.userScanService.scanRedirectPage(cpId, Long.parseLong(token.getFromCpId()), token.getScanCodeType(),null);
            }catch (Exception e){
                log.error("扫码绑定关系异常，异常信息是：", e.getMessage());
                return "";
            }
        }
        return "";
    }

    @ResponseBody
    @RequestMapping("/getMiniSession")
    public ResponseObject<Map<String, Object>> getMiniSession(
            MiniProgramForm token, Errors errors, HttpServletRequest request) {

        ControllerHelper.checkException(errors);

        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String cpToken = token.getCpToken();

        //店铺id
        Integer shopId=null;

        String path="/v1/ee/query_shop";
        Map<String,String> mapId=new HashMap<>();
        if(token.getFromCpId()==null || token.getFromCpId().equals("")) {
            mapId.put("cpid", token.getCpId().toString());
        }else{
            mapId.put("cpid", token.getFromCpId().toString());
        }

        try {
            HttpClient hc = new HttpClient(host+path, 30000, 30000);
            int status = hc.send(mapId, "UTF-8");
            if (200 == status) {
                String resultString =  hc.getResult();
                if (null != resultString && !"".equals(resultString)) {

                    // 将返回结果转换为map
                    Map<String,Object> parse = (Map<String,Object>)JSONObject.parse(resultString);
                    shopId=(Integer) parse.get("shop_id");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("小程序登录调用安畅接口出错！获取用户信息失败！");
        }

        if(!Objects.isNull(shopId) && shopId!=-1){
            token.setScanCodeType("shopCode");
        }else{
            token.setScanCodeType("selfCode");
        }


        String key = "cpToken:" + token.getCode();
        // 校验token
        String dbToken = redisUtils.get(key);
        if (StringUtils.isBlank(dbToken) || !StringUtils.equals(token.getCpToken(), dbToken)) {
            throw new BadCredentialsException(GlobalErrorCode.CP_TOKEN_EXPIRED_OR_NOT_EXIST.getError());
        }
        EncryptUtil des1;
        String unionAndOpenId;
        try {
            des1 = new EncryptUtil();
            unionAndOpenId = des1.decrypt(cpToken);
        } catch (Exception e) {
            log.error("解密cpToken异常");
            throw new BadCredentialsException(GlobalErrorCode.UNKNOWN.getError());
        }
        // 取openId
        String openId = StringUtils.substringAfter(unionAndOpenId, ":");

        redisUtils.del(key);

        User user = null;
        String sessionId = request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
        if (StringUtils.isNotBlank(sessionId)) {
            user = getUserBySessionId(sessionId);
        }

        // 用户未登录过或者session已经过期
        // 调用微信接口获取用户数据
        if (user == null) {
            user = userService.loadByCpId(token.getCpId());
            if (user == null) {
                throw new BizException(GlobalErrorCode.AUTH_UNKNOWN, "用户不存在");
            }

            Shop shop = shopService.findByUser(user.getId());
            if (shop != null) {
                user.setShopId(shop.getId());
                user.setCurrentSellerShopId(shopService.loadRootShop().getId());
            }

            // 生成一个32位的随机字符串作为key保存到redis, 有效期为29天
            // 参考 http://www.cnblogs.com/nosqlcoco/p/6105749.html
            sessionId = String.valueOf(snowflakeIdWorker.nextId());
        }
        if (StringUtils.isNotBlank(openId)) {
            // 把openId放入上下文
            user.setLoginname(openId);
        }

        String customerNumber = user.getCustomerNumber();
        UserVO userVO = new UserVO(user, urlHelper.genShopUrl(user.getShopId()));
        userVO.setType(userService.getUserType(userVO.getId()));
        userVO.setCustomerNumber(customerNumber);

        Long cpId = userVO.getCpId();

        int expOfficer=userService.selectExpOfficerByCpId(cpId);
        userVO.setExpOfficer(expOfficer);
        String userType=userService.selectIdType(cpId.toString());
        String userViViType=userService.selectViViType(user.getCpId().toString());
        userVO.setIdentityName("顾客");
        if("DS".equals(userType) || "DS".equals(userViViType)){
            userVO.setIdentityName("会员");
        }
        if("SP".equals(userType) || "SP".equals(userViViType)){
            userVO.setIdentityName("代理");
        }

        //是否是纯白人
        userVO.setIsPrueWhite(customerProfileService.isNew(cpId.toString()));
        // 是否有身份
        userVO.setHasIdentity(customerProfileService.hasIdentity(cpId));
        // 是否点亮
        userVO.setIsLighten(customerProfileService.isLighten(cpId));
        if (StringUtils.isEmpty(user.getAvatar())) {
            // dts同步时不会同步头像信息
            String customerAvatar =
                    customerProfileService.loadAvatarByCpIdAndUnionId(cpId, user.getUnionId());
            userVO.setAvatar(customerAvatar);
        }

        // 是否是店主身份
        CustomerCareerLevel customerCareerLevel = customerCareerLevelMapper.selectByPrimaryKey(cpId);
        userVO.setIsShopOwner(false);
        userVO.setIsMember(false);
        userVO.setIsProxy(false);
        if (customerCareerLevel != null) {
            String sp = customerCareerLevel.getViviLifeType();
            if ("SP".equals(sp)) {
                userVO.setIsShopOwner(true);
            }

            // 是全国代理或店主，则显示代理价
            if (customerCareerLevel.getIsLightening()
                    || (!customerCareerLevel.getIsLightening() && "SP".equals(sp))) {
                userVO.setIsProxy(true);
            }
            if (userVO.isHasIdentity()) { // 是vip或超级会员，则显示会员价
                userVO.setIsMember(true);
            }
        }

        String identityKey = "login_return_" + cpId;
        redisUtils.set(
                identityKey,
                JSONObject.toJSONString(new IdentityVO(userVO.getIsProxy(), userVO.getIsMember())));
        redisUtils.expire(identityKey, 15, TimeUnit.DAYS);

        // 登记登陆日志
        UserSigninLog log = UserSigninLogFactory.createUserSigninLog(request, user);
        userSigninLogService.insert(log);

        //重新赋值user数据
        user.setIsProxy(userVO.getIsProxy());
        user.setIsMember(userVO.getIsMember());
        user.setIsLighten(userVO.getIsLighten());
        redisTemplate.opsForValue().set(sessionId, user);
        redisTemplate.expire(sessionId, 15, TimeUnit.DAYS);

        String cpIdS = String.valueOf(token.getCpId());
        redisUtils.set(cpIdS, user.getUnionId());
        redisTemplate.expire(cpIdS, 15, TimeUnit.DAYS);

        Map<String, Object> result = new HashMap<>();
        result.put("sessionId", sessionId);
        result.put("user", userVO);
        //String redirectType = this.userScanService.scanRedirectPage(user.getCpId(), token.getFromCpId(), token.getScanCodeType(),token.getSpId());
        //result.put("redirectType", redirectType);
        return new ResponseObject<>(result);
    }

    @ResponseBody
    @RequestMapping("/exchangeScene")
    public ResponseObject<Map<String, String>> getSceneByUrlKey(@Param("key") String key) {
        Map<String, String> ret = ImmutableMap.of("scene", tinyUrlService.findUrlByKey(key));
        return new ResponseObject<>(ret);
    }

    @ResponseBody
    @RequestMapping("/miniProgramCode")
    public ResponseObject<Map<String, Object>> miniProgramCode(String path, MiniProgramForm form) {
        GetAccessTokenRsp accessTokenRsp = getAccessTokenRsp();
        User user = (User) getCurrentIUser();
        String pCode = this.promotionService.findPcode4Meeting(form.getProductId());
        String qrPath =
                StringUtils.defaultIfBlank(
                        path, "pages/goods_detail?id=" + form.getProductId() + "&fromCpId=" + user.getCpId());
        if(StringUtils.isNotBlank(pCode)){
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(qrPath);
            stringBuffer.append("&pCode=");
            stringBuffer.append(pCode);
            qrPath = stringBuffer.toString();
        }
        qrPath = qrPath.startsWith("/") ? qrPath.substring(1) : qrPath;

        // [0]: path, [1]: scece
        String[] pathScene = qrPath.split("\\?");
        String scene;
        if (pathScene.length > 1) {
            scene = pathScene[1];
        } else {
            // 没有参数则默认生成fromCpId
            scene = "fromCpId=" + user.getCpId();
        }
        final String jsonIdentifier = "{";
        if (StringUtils.isNotEmpty(scene) && !scene.contains(jsonIdentifier)) {
            scene = jsonSense(scene);
        }
        String urlKey = tinyUrlService.insert(scene);
        String qrCode =
                WeiXinAccessTokenUtil.getQRCodeUnLimit(
                        accessTokenRsp.getAccessToken(),
                        ImmutableMap.<String, Object>of("page", pathScene[0], "scene", urlKey, "width", 200));
        Map<String, Object> ret =
                ImmutableMap.<String, Object>of("imageData", Optional.fromNullable(qrCode).or(""));
        return new ResponseObject<>(ret);
    }

    /**
     * 拼团分享生成二维码
     */
    @ResponseBody
    @RequestMapping("/miniProgramCodeForPintuan")
    public ResponseObject<Map<String, Object>> miniProgramCodeForPintuan(
            MiniProgramForm form, HttpServletRequest request) {

        User user = (User) getCurrentIUser();

        GetAccessTokenRsp tokenRsp = getAccessTokenRsp();

        String tranCode = form.getTranCode();

        String buffer =
                "pages/collageOk?{\"need\":"
                        + "{\"fromCpId\":\""
                        + user.getCpId()
                        + "\", \"tranCode\":\""
                        + tranCode
                        + "\", \"source\":\"APP\"}}";

        String[] pathScene = buffer.split("\\?");
        String scene = pathScene[1];
        String urlKey = tinyUrlService.insert(scene);
        String qrCode =
                WeiXinAccessTokenUtil.getQRCodeUnLimit(
                        tokenRsp.getAccessToken(),
                        ImmutableMap.<String, Object>of("page", pathScene[0], "scene", urlKey, "width", 200));
        Map<String, Object> ret =
                ImmutableMap.<String, Object>of("imageData", Optional.fromNullable(qrCode).or(""));
        return new ResponseObject<>(ret);
    }

    /**
     * 赋能个人码生成
     * @param path
     * @param form
     * @return
     */
    //@ResponseBody
    //@RequestMapping("/miniProgramCodeForSelf")
    public ResponseObject<Map<String, Object>> miniProgramCodeForSelfCode(String path, MiniProgramForm form, Long cpId,Boolean isHyaline) {
        GetAccessTokenRsp accessTokenRsp = getAccessTokenRsp();
        path="pages/seller/pages/guide";

        String scene = "qrCode=1&id=" + cpId;
        scene +="&fromCpId="+cpId;

        final String jsonIdentifier = "{";
        if (StringUtils.isNotEmpty(scene) && !scene.contains(jsonIdentifier)) {
            scene = jsonSense(scene);
        }
        String urlKey = tinyUrlService.insert(scene);

        // 日志
        log.info(new StringBuffer()
                .append("赋能生成二维码的参数")
                .append("page")
                .append(path)
                .append("{}")
                .append("参数scene:")
                .append(urlKey)
                .toString());

        String qrCode = "";
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        String redisQrCode = redisUtils.get(MINI_PROGRAM_INVITE_CODE + cpId + ":" + urlKey);

        // 走缓存
//        if(StringUtils.isNotBlank(redisQrCode))
//            qrCode = redisQrCode;
//        else {
            qrCode = WeiXinAccessTokenUtil.getQRCodeUnLimit(
                            accessTokenRsp.getAccessToken(),
                            ImmutableMap.<String, Object>of("page", path, "scene", urlKey, "width", 200,"is_hyaline",isHyaline));
            // 写入缓存
            if(StringUtils.isNotBlank(qrCode)){
                redisUtils.set(MINI_PROGRAM_INVITE_CODE + cpId + ":" + urlKey, qrCode);
            }
//        }

        Map<String, Object> ret =
                ImmutableMap.<String, Object>of("imageData", Optional.fromNullable(qrCode).or(""));

        log.info(new StringBuffer()
        .append("赋能生成二维码对应cpId是")
        .append(cpId)
        .append("base64的码imageData是：")
        .append(Optional.fromNullable(qrCode).or(""))
        .toString());

        return new ResponseObject<>(ret);
    }

    /**
     * 公共二维码生成
     * @param path
     * @return
     */
    @ResponseBody
    @RequestMapping("/createQrCode")
    public ResponseObject<Map<String, Object>> createQrCode(String path) {
        GetAccessTokenRsp accessTokenRsp = getAccessTokenRsp();

        String [] scene = path.split("\\?");

        String urlKey="";
        String scene1="";
        if(scene.length>1) {
            scene1 = scene[1];
        }

        final String jsonIdentifier = "{";
        if (StringUtils.isNotEmpty(scene1) && !scene1.contains(jsonIdentifier)) {
            scene1 = jsonSense(scene1);
        }
        urlKey = tinyUrlService.insert(scene1);

        //日志
        log.info(new StringBuffer()
                .append("二维码生成传入的参数")
                .append("page：")
                .append(path)
                .append("{}")
                .append("参数scene:")
                .append(scene1)
                .toString());

        String qrCode =
                WeiXinAccessTokenUtil.getQRCodeUnLimit(
                        accessTokenRsp.getAccessToken(),
                        ImmutableMap.<String, Object>of("page", scene[0], "scene",urlKey, "width", 200));
        Map<String, Object> ret =
                ImmutableMap.<String, Object>of("imageData", Optional.fromNullable(qrCode).or(""));

        log.info(new StringBuffer()
                .append("base64的码imageData是：")
                .append(Optional.fromNullable(qrCode).or(""))
                .toString());

        return new ResponseObject<>(ret);
    }





    @ResponseBody
    @RequestMapping("/miniProgramCode/freshManPage")
    public ResponseObject<Map<String, Object>> miniProgramFreshManPage(String path, MiniProgramForm form, Long cpId) {
        GetAccessTokenRsp accessTokenRsp = getAccessTokenRsp();
        //后台配置
        path = this.getRedirectPath();
        if(StringUtils.isBlank(path))
            path="pages/freshman/pages/channel";
        if (Objects.isNull(cpId)){
            User user = (User) this.getCurrentIUser();
            cpId = user.getCpId();
        }
        String scene = "fromCpId=" + cpId;
        final String jsonIdentifier = "{";
        if (StringUtils.isNotEmpty(scene) && !scene.contains(jsonIdentifier)) {
            scene = jsonSense(scene);
        }
        String urlKey = tinyUrlService.insert(scene);
        //日志
        log.info(new StringBuffer()
                .append("新人页面分享跳转页")
                .append("page")
                .append(path)
                .append("{}")
                .append("参数scene:")
                .append(urlKey)
                .toString());

        String qrCode =
                WeiXinAccessTokenUtil.getQRCodeUnLimit(
                        accessTokenRsp.getAccessToken(),
                        ImmutableMap.<String, Object>of("page", path, "scene", urlKey, "width", 200));
        Map<String, Object> ret =
                ImmutableMap.<String, Object>of("imageData", Optional.fromNullable(qrCode).or(""));

        log.info(new StringBuffer()
                .append("新人页面分享生成二维码对应cpId是")
                .append(cpId)
                .append("base64的码imageData是：")
                .append(Optional.fromNullable(qrCode).or(""))
                .toString());

        return new ResponseObject<>(ret);
    }

    /**
     * 获取跳转地址
     * @return
     */
    private String getRedirectPath() {
        Map map = new HashMap();
        map.put("configType", "code");
        map.put("configName", "miniProgramCodeNameFreshManPage");
        PromotionConfig p = promotionConfigService.selectByConfig(map);
        if(!Objects.isNull(p) && StringUtils.isNotBlank(p.getConfigValue())){
            return p.getConfigValue();
        }
        return null;
    }

    private static String jsonSense(String orgi) {
        final HashMap<String, Object> parent = new HashMap<>(16);
        final HashMap<String, Object> child = new HashMap<>(16);
        parent.put("data", child);
        final String splitter = "&";
        for (String keyValues : orgi.split(splitter)) {
            final String[] keyValue = keyValues.split("=");
            if (CollectionUtils.size(keyValue) != 2) {
                throw new BizException(ERROR_PARAM);
            }
            final String key = keyValue[0];
            final String value = keyValue[1];
            child.put(key, value);
        }
        return new JSONObject(parent).toJSONString();
    }

    @ResponseBody
    @RequestMapping("/getSession")
    public ResponseObject<Map<String, Object>> getSessionId(
            @Validated MiniProgramAuthTokenForm token, Errors errors, HttpServletRequest request) {

        ControllerHelper.checkException(errors);
        String sessionId = request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
        WechatAppConfig appConfig = wechatAppConfigService.load();
        if (appConfig == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "appId设置未初始化");
        }
        String appId = appConfig.getAppMiniId();
        String secret = appConfig.getAppMiniSecret();

        User user = getUserBySessionId(sessionId);

        // 用户未登录过或者session已经过期
        // 调用微信接口获取用户数据
        if (user == null) {
            // 通过code换取微信sessionKey
            Config config = new Config("", "", "", appId, secret, "", "", "", "");
            GetSessionKeyRequest keyRequest = new GetSessionKeyRequest(config, token.getCode());

            GetSessionKeyResponse sessionKeyResponse;
            try {
                sessionKeyResponse = OAuthManager.getSessionKey(keyRequest);
            } catch (OAuthException e) {
                String msg = "微信返回错误, " + e;
                log.error(msg);
                throw new BizException(WECHAT_MINI_PROGRAM_LOGIN_ERROR, msg);
            }
            String sessionKey = sessionKeyResponse.getSession_key();
            String openId = sessionKeyResponse.getOpenid();

            String encryptData = token.getEncryptedData();
            String iv = token.getIv();
            DecryptedResponse wechatUser;
            try {
                wechatUser = DecryptedResponse.fromEncryptData(sessionKey, encryptData, iv);
            } catch (Exception e) {
                String msg = "微信encryptData解密失败";
                log.error(msg);
                throw new BizException(WECHAT_MINI_PROGRAM_LOGIN_ERROR, msg);
            }
            // 如果用户unionId以存在则读取该unionId对于的用户，否则新建用户
            wechatUser.setAppId(appId);
            // 通过SCRM系统获取的用户在首次getSession时更新用户信息
            // 创建用户
            user = userService.createUserFromThirdUserIfNotExists(wechatUserConverter, wechatUser, null, null);
            assert user != null;
            Shop shop = shopService.findByUser(user.getId());
            if (shop != null) {
                user.setShopId(shop.getId());
                user.setCurrentSellerShopId(shopService.loadRootShop().getId());
            }

            // 生成一个32位的随机字符串作为key保存到redis, 有效期为29天
            // 参考 http://www.cnblogs.com/nosqlcoco/p/6105749.html
            sessionId = String.valueOf(snowflakeIdWorker.nextId());
            user.setLoginname(openId);
        } else {
            // 重新登录后刷新用户信息到缓存
            String userId = user.getId();
            User dbUser = userService.load(userId);
            dbUser.setLoginname(user.getLoginname());
            user = dbUser;
        }
        redisTemplate.opsForValue().set(sessionId, user);
        redisTemplate.expire(sessionId, 29, TimeUnit.DAYS);

        String customerNumber = user.getCustomerNumber();
        UserVO userVO = new UserVO(user, urlHelper.genShopUrl(user.getShopId()));
        userVO.setType(userService.getUserType(userVO.getId()));
        userVO.setCustomerNumber(customerNumber);

        Long cpId = userVO.getCpId();

        // 是否有身份
        userVO.setHasIdentity(customerProfileService.hasIdentity(cpId));
        // 是否点亮
        userVO.setIsLighten(customerProfileService.isLighten(cpId));
        if (StringUtils.isEmpty(user.getAvatar())) {
            // dts同步时不会同步头像信息
            String customerAvatar =
                    customerProfileService.loadAvatarByCpIdAndUnionId(cpId, user.getUnionId());
            userVO.setAvatar(customerAvatar);
        }

        // 是否是店主身份
        CustomerCareerLevel customerCareerLevel = customerCareerLevelMapper.selectByPrimaryKey(cpId);
        userVO.setIsShopOwner(false);
        if (customerCareerLevel != null) {
            String sp = customerCareerLevel.getViviLifeType();
            if ("SP".equals(sp)) {
                userVO.setIsShopOwner(true);
            }
        }

        Map<String, Object> result = new HashMap<>();
        result.put("sessionId", sessionId);
        result.put("user", userVO);
        return new ResponseObject<>(result);
    }

    @RequestMapping("/checkSession")
    @ResponseBody
    public ResponseObject<Boolean> checkSession(HttpServletRequest request) {
        String sessionId = request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
        if (StringUtil.isNull(sessionId)) {
            return new ResponseObject<>(false);
        }
        boolean result = redisTemplate.hasKey(sessionId);
        return new ResponseObject<>(result);
    }


    @ResponseBody
    @RequestMapping(value = "/user/log/submit")
    public ResponseObject<Boolean> insertLog(UserLogInfo userLogInfo) {
        boolean result = false;
        log.info(new StringBuffer().append("记录前端日志的入参是：").append(userLogInfo).toString());

        if(Objects.isNull(userLogInfo)){
            new ResponseObject<>(false);
        }

        try {
            if(Objects.isNull(userLogInfo.getCpId())){
                try {
                    User user = (User)this.getCurrentIUser();
                    if(!Objects.isNull(user))
                        userLogInfo.setCpId(user.getCpId());
                }catch (Exception e){
                    log.error("为获取用户登入信息", e.getMessage());
                }

            }
            //入库的信息
            log.info(new StringBuffer()
                    .append("cpId是")
                    .append(userLogInfo.getCpId())
                    .append("日志信息是")
                    .append(userLogInfo.getLogInfo())
                    .toString());
            result = this.userLogService.insertUserLog(userLogInfo);
        }catch (Exception e){
            log.error("前端上传日志出错", e.getMessage());
        }

        return new ResponseObject<>(result);
    }

    private User getUserBySessionId(String sessionId) {
        return (User) redisTemplate.opsForValue().get(sessionId);
    }

    private GetAccessTokenRsp getAccessTokenRsp() {
        WechatAppConfig appConfig = wechatAppConfigService.load();
        if (appConfig == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "appId设置未初始化");
        }

        String appId = appConfig.getAppMiniId();
        String secret = appConfig.getAppMiniSecret();

        return WeiXinAccessTokenUtil.getWeiXinAccessToken(appId, secret);
    }
}
