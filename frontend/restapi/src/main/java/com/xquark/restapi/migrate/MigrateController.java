package com.xquark.restapi.migrate;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.Product;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.product.ProductService;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 升级数据库数据
 *
 * @author tonghu
 */
@Controller
@ApiIgnore
public class MigrateController extends BaseController {

  @Autowired
  private ProductService productService;

  @Autowired
  private ActivityService activityService;

  @ResponseBody
  @RequestMapping("/migrate/20140808")
  public ResponseObject<Integer> migrate1(HttpServletRequest req) {

    // Pageable pageable = new PageRequest(page - 1, DEFAULT_PAGE_SIZE);
    int page = 0;
    int size = 50;
    int count = 0;
    Pageable pageable = new PageRequest(page, size);
    List<Product> prods = null;
    Random r = new Random();
    do {
      pageable = new PageRequest(page, size);
      prods = productService.listActivityProducts(pageable, 0, null);
      if (prods != null && prods.size() > 0) {
        for (Product prod : prods) {
          productService.setFakeSales(prod.getId(), r.nextInt(270) + 29);
          count++;
        }
      }
      page++;
    } while (prods != null && prods.size() > 0);
    return new ResponseObject<>(count);
  }
}
