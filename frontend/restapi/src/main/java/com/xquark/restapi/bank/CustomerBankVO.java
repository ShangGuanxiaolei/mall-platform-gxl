package com.xquark.restapi.bank;

/**
 * Created by jqx on 18/8/15.
 */
public class CustomerBankVO {
    private String imgStr;

    public String getImgStr() {
        return imgStr;
    }

    public void setImgStr(String imgStr) {
        this.imgStr = imgStr;
    }
}
