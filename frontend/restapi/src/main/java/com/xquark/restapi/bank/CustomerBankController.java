package com.xquark.restapi.bank;

import com.xquark.dal.model.CustomerBank;
import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.bank.CustomerBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jqx on 18/8/15.
 */
@RestController
@RequestMapping("/customerBank")
public class CustomerBankController extends BaseController {

    @Autowired
    private CustomerBankService customerBankService;

    /**
     * 识别银行卡图片，获取银行卡信息
     * @param customerBankVO
     * @return
     */
    @RequestMapping(value = "/parseBankImage",method = RequestMethod.POST)
    public ResponseObject<Map<String,Object>> parseBankImage(@RequestBody CustomerBankVO customerBankVO){
        CustomerBank customerBank = customerBankService.parseBankImage(customerBankVO.getImgStr());

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("bankFullName",customerBank.getBankFullName());
        map.put("bankAccount",customerBank.getBankAccount());
        map.put("cardName",customerBank.getCardName());
        map.put("cardType",customerBank.getCardType());
        map.put("isSupported",customerBank.isSupported());

        return new ResponseObject<>(map);
    }

    @RequestMapping(value = "/addCustomerBankInfo",method = RequestMethod.POST)
    public ResponseObject<Boolean> addCustomerBankInfo(@RequestBody CustomerBank customerBank){
        User user = (User) getCurrentIUser();
        customerBankService.addCustomerBankInfo(customerBank,user);
        return new ResponseObject<>(true);
    }
}
