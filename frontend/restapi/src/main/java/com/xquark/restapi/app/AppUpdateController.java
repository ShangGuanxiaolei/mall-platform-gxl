package com.xquark.restapi.app;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.AppVersion;
import com.xquark.dal.type.OSType;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.app.AppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class AppUpdateController extends BaseController {

  @Autowired
  private AppVersionService appVersionService;

  /**
   * @return 若需要升级，则返回最新版本信息
   */
  @ResponseBody
  @RequestMapping("/update/check")
  public ResponseObject<AppVersion> checkVersion(@Param("clientVersion") int clientVersion,
      OSType osType) {
    AppVersion curVer = appVersionService.findCurrentVersion(clientVersion, osType);
    return new ResponseObject<>(
        curVer != null && curVer.getClientVersion() > clientVersion ? curVer : null);
  }
}
