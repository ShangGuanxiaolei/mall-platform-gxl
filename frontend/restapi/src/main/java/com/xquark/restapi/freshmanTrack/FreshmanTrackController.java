package com.xquark.restapi.freshmanTrack;

import com.xquark.dal.model.User;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.dal.model.FreshmanTrackInfoVo;
import com.xquark.service.track.FreshmanTrackService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 新人轨迹展示层
 */
@RestController
@RequestMapping("/freshman")
public class FreshmanTrackController extends BaseController {

    private static final Log log = LogFactory.getLog(FreshmanTrackController.class);

    @Autowired
    private FreshmanTrackService freshmanTrackService;

    @Autowired
    private CustomerProfileService customerProfileService;

    @RequestMapping("/track")
    public ResponseObject<FreshmanTrackInfoVo> queryFreshmanTrack() {
        User user = (User) getCurrentIUser();
//        Long currentCpid = 3000003L;
        boolean hasIdentity = customerProfileService.hasIdentity(user.getCpId());
        if (hasIdentity) {
            return new ResponseObject<>();
        } else {
            FreshmanTrackInfoVo freshmanTrackInfo = freshmanTrackService.getFreshmanTrackInfo(user.getCpId());
            return new ResponseObject<>(freshmanTrackInfo);
        }
    }
}
