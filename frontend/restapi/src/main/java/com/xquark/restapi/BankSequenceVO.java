package com.xquark.restapi;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午8:52
 */
public class BankSequenceVO {

  private final Long bankId;

  public BankSequenceVO(Long bankId) {
    this.bankId = bankId;
  }

  public Long getBankId() {
    return bankId;
  }

}
