package com.xquark.restapi.commission;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.SubAccount;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.CommissionVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.commission.CommissionService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class CommissionController extends BaseController {

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private AccountApiService accountApiService;

  /**
   * 我的收益首页数据获取
   */
  @ResponseBody
  @RequestMapping("/commission/myCommission")
  public ResponseObject<Map<String, Object>> myCommission() {
    Map<String, Object> params = new HashMap<>();
    String userId = getCurrentIUser().getId();
    SubAccount avalidSubAccount = accountApiService
        .findSubAccountByUserId(userId, AccountType.AVAILABLE);
    SubAccount commissionSubAccount = accountApiService
        .findSubAccountByUserId(userId, AccountType.COMMISSION);
    SubAccount withdrawSubAccount = accountApiService
        .findSubAccountByUserId(userId, AccountType.WITHDRAW);
    // 累计收益等于可用账户加上佣金账户加上提现账户余额之和
    BigDecimal allCommission = avalidSubAccount.getBalance().add(commissionSubAccount.getBalance())
        .add(withdrawSubAccount.getBalance()).setScale(2);
    // 今日收益
    BigDecimal todayCommission = commissionService.getTodayWithdraw(userId);
    // 本月收益
    BigDecimal monthCommission = commissionService.getMonthWithdraw(userId);
    params.put("allCommission", allCommission);
    params.put("todayCommission", todayCommission);
    params.put("monthCommission", monthCommission);
    return new ResponseObject<>(params);
  }

  /**
   * 我的收益，根据类型分页获取明细收益信息
   */
  @ResponseBody
  @RequestMapping("/commission/listByApp")
  public ResponseObject<List<CommissionVO>> listByApp(Pageable pageable,
      @RequestParam String type) {
    List<CommissionVO> commissionVOs = null;
    String userId = getCurrentIUser().getId();
    List<String> types = new ArrayList<>();
    HashMap params = new HashMap();
    params.put("userId", userId);
    params.put("type", type);
    // type 分为buy(购物),twitter(推客),partner(合伙人),team(战队)
    if ("all".equals(type)) {
      types = null;
    } else if ("buy".equals(type)) {
      types.add(CommissionType.PRODUCT.toString());
    } else if ("twitter".equals(type)) {
      types.add(CommissionType.ONECOMMISSION.toString());
      types.add(CommissionType.TWOCOMMISSION.toString());
      types.add(CommissionType.THREECOMMISSION.toString());
    } else if ("partner".equals(type)) {
      types.add(CommissionType.SHAREHOLDER.toString());
      types.add(CommissionType.TEAM.toString());
      types.add(CommissionType.PLATFORM.toString());
    } else if ("team".equals(type)) {
      types.add(CommissionType.TEAM_LEADER.toString());
      types.add(CommissionType.TEAM_MEMBER.toString());
    }
    params.put("types", types);
    commissionVOs = commissionService.listByApp(params, pageable);
    return new ResponseObject<>(commissionVOs);
  }

}