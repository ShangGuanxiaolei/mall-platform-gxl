package com.xquark.restapi.sku;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.SkuAttributeVO;
import com.xquark.dal.vo.SkuBasicVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.product.ProductService;
import com.xquark.service.sku.SkuAttributeItemService;
import com.xquark.service.sku.SkuAttributeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * sku规格属性controller Created by chh on 17-11-22.
 */
@Controller
@ApiIgnore
public class SkuAttributeController extends BaseController {

  @Autowired
  private SkuAttributeService skuAttributeService;

  @Autowired
  private SkuAttributeItemService skuAttributeItemService;

  @Autowired
  private ProductService productService;


  @Autowired
  private  ResourceFacade resourceFacade;

  @Autowired
  private SkuMapper skuMapper;


	@ResponseBody
	@RequestMapping("/sku")
	public ResponseObject<Sku> test() {
		Sku sku=skuMapper.select("2","2");
		System.out.println(sku);
		return new ResponseObject<>(sku);

	}

/*	@ResponseBody
	@RequestMapping(value = "/sku1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject<Integer> test1(Sku sku){
		int result = skuMapper.insert(sku);
		return new ResponseObject<>(result);
	}*/

/*	@ResponseBody
	@RequestMapping("/sku2")
	public ResponseObject<SkuBasicVO> test2(String id){
		SkuBasicVO vo = skuMapper.selectBasicVOByPrimaryKey(id);
		return new ResponseObject<>(vo);
	}*/

	/*@ResponseBody
	@RequestMapping("/sku3")
	public ResponseObject<List> test3(String productId){
		List<Sku> skuList = skuMapper.getAllSkuByproductId(productId);
		return new ResponseObject<>(skuList);
	}*/


	/*@ResponseBody
	@RequestMapping(value = "/sku4", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject<Integer> test4(Sku sku){
		int result= skuMapper.updateByPrimaryKeySelective(sku);
		return new ResponseObject<>(result);
	}*/







	/**
	 * 获取多skus列表
	 */

	/*private List<String> skuImgList(String skuId){
		List<String> skuImgList = new ArrayList<>();
		List<Sku> skus = skuAttributeService
				.requestSkuImgs(String.valueOf(IdTypeHandler.decode(skuId)),"");
		for (Sku sku : skus) {
			skuImgList.add(resourceFacade.resolveUrl(sku.getSkuImg() + "|" + ResourceFacade.IMAGE_S1));
		}

		return null;
	}*/

  /**
   * 获取sku规格属性列表
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable) {
    List<SkuAttribute> roles = null;
    Map<String, Object> params = new HashMap<>();
    roles = skuAttributeService.list(pageable, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", skuAttributeService.selectCnt(params));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }


  /**
   * 保存sku规格属性
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/save")
  public Json savePromotion(SkuAttribute role) {
    Json json = new Json();
    int result = 0;

    if (StringUtils.isNotEmpty(role.getId())) {
      result = skuAttributeService.modify(role);
    } else {
      role.setArchive(false);
      result = skuAttributeService.insert(role);
    }
    if (result == 1) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("保存成功！");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("保存失败！");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping("/skuAttribute/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = skuAttributeService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的记录<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/{id}")
  public ResponseObject<SkuAttribute> view(@PathVariable String id, HttpServletRequest req) {
    SkuAttribute role = skuAttributeService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }

  /**
   * 获取sku规格属性对应的所有属性值列表
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/item/list")
  public ResponseObject<Map<String, Object>> itemList(Pageable pageable, @RequestParam @NotBlank  String id) {
    List<SkuAttributeItem> roles = null;
    Map<String, Object> params = new HashMap<>();
    params.put("attributeId", id);
    roles = skuAttributeItemService.list(pageable, params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", skuAttributeItemService.selectCnt(params));
    aRetMap.put("list", roles);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存sku规格属性
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/item/save")
  public Json saveItem(SkuAttributeItem role) {
    Json json = new Json();
    int result = 0;

    if (StringUtils.isNotEmpty(role.getId())) {
      result = skuAttributeItemService.modify(role);
    } else {
      role.setArchive(false);
      result = skuAttributeItemService.insert(role);
    }
    if (result == 1) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("保存成功！");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("保存失败！");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping("/skuAttribute/item/delete/{id}")
  public ResponseObject<Boolean> deleteItem(@PathVariable String id) {
    int result = skuAttributeItemService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 查看某个具体的记录<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/item/{id}")
  public ResponseObject<SkuAttributeItem> itemView(@PathVariable String id,
      HttpServletRequest req) {
    SkuAttributeItem role = skuAttributeItemService.selectByPrimaryKey(id);
    return new ResponseObject<>(role);
  }

  /**
   * 返回系统设置的所有属性规格和对应的属性值<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/findAttributeAndItems")
  public ResponseObject<Map> findAttributeAndItems(HttpServletRequest req) {
    Map result = new HashMap();
    List<SkuAttribute> attributes = skuAttributeService.findAll();
    for (SkuAttribute skuAttribute : attributes) {
      String attributeId = skuAttribute.getId();
      String name = skuAttribute.getName();
      List<SkuAttributeItem> items = skuAttributeItemService.findAllByAttributeId(attributeId);
      ArrayList itemList = new ArrayList();
      for (SkuAttributeItem item : items) {
        itemList.add(item.getName());
      }
      result.put(name, itemList);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 返回系统设置的所有属性规格名称与id的对应关系<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/findAllAttributes")
  public ResponseObject<Map> findAllAttributes(HttpServletRequest req) {
    Map result = new LinkedHashMap();
    List<SkuAttribute> attributes = skuAttributeService.findAll();
    for (SkuAttribute skuAttribute : attributes) {
      String attributeId = skuAttribute.getId();
      String name = skuAttribute.getName();
      result.put(name, attributeId);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 返回系统设置的所有属性值与id的对应关系<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/findAllItems")
  public ResponseObject<Map> findAllItems(HttpServletRequest req) {
    Map result = new HashMap();
    List<SkuAttributeItem> attributes = skuAttributeItemService.findAll();
    for (SkuAttributeItem skuAttributeItem : attributes) {
      String attributeItemId = skuAttributeItem.getId();
      String name = skuAttributeItem.getName();
      result.put(name, attributeItemId);
    }
    return new ResponseObject<>(result);
  }


  /**
   * 返回系统设置的所有属性值与id的对应关系<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/findAllItemsWithAttributeId")
  public ResponseObject<Map> findAllItemsWithAttributeId(HttpServletRequest req) {
    Map result = new HashMap();
    List<SkuAttributeItem> attributes = skuAttributeItemService.findAll();
    for (SkuAttributeItem skuAttributeItem : attributes) {
      String attributeItemId = skuAttributeItem.getId();
      String name = skuAttributeItem.getName();
      String attributeId = skuAttributeItem.getAttributeId();
      result.put(attributeId+"-"+name, attributeItemId);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 返回系统设置的所有属性规格和对应的属性值(返回vo类型)<br>
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/findAllAttributeAndItemsVO")
  public ResponseObject<List<SkuAttributeVO>> findAllAttributeAndItemsVO(HttpServletRequest req) {
    List<SkuAttributeVO> result = new ArrayList<>();
    List<SkuAttribute> attributes = skuAttributeService.findAll();
    for (SkuAttribute skuAttribute : attributes) {
      String attributeId = skuAttribute.getId();
      List<SkuAttributeItem> items = skuAttributeItemService.findAllByAttributeId(attributeId);
      SkuAttributeVO vo = new SkuAttributeVO();
      BeanUtils.copyProperties(skuAttribute, vo);
      vo.setItems(items);
      result.add(vo);
    }
    return new ResponseObject<>(result);
  }

  /**
   * 返回某个商品所有选中的规格属性和属性值
   */
  @ResponseBody
  @RequestMapping("/skuAttribute/getProductAttributeAndItems")
  public ResponseObject<Map> getProductAttributeAndItems(@RequestParam String productId,
      HttpServletRequest req) {
    Map result = new HashMap();
    Product product = productService.load(productId);
    String attributes = product.getAttributes();
    if (StringUtils.isNotEmpty(attributes)) {
      // 商品所有选中的规格属性
      List<String> productAttributes = Arrays.asList(attributes.split("-"));
      // 商品所有选中的规格属性值
      Set<String> skuAttributes = new HashSet<>();
      List<String> skuAttributes_ = productService.findAttributeByProductId(productId);
      for (String skuAttribute : skuAttributes_) {
        if (StringUtils.isNotEmpty(skuAttribute)) {
          String[] skus = skuAttribute.split("-");
          for (String sku : skus) {
            skuAttributes.add(sku);
          }
        }
      }
      result.put("productAttributes", productAttributes);
      result.put("skuAttributes", skuAttributes);
    }
    return new ResponseObject<>(result);
  }


}
