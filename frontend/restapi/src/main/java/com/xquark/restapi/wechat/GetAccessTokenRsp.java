package com.xquark.restapi.wechat;

/**
 * Created by root on 17-9-23.
 */
public class GetAccessTokenRsp {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7021131613095678023L;

    private String accessToken;

    public String getAccessToken()
    {
        return accessToken;
    }

    public void setAccessToken(String accessToken)
    {
        this.accessToken = accessToken;
    }

    @Override
    public String toString()
    {
        return "GetAccessTokenRsp [accessToken=" + accessToken + "]";
    }
}
