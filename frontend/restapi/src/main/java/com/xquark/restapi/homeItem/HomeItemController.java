package com.xquark.restapi.homeItem;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.dal.model.HomeItem;
import com.xquark.dal.model.TweetImage;
import com.xquark.dal.status.HomeItemBelong;
import com.xquark.dal.status.UserType;
import com.xquark.dal.vo.HomeItemVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.agent.UserAgentWhiteListService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.homeItem.HomeItemService;
import com.xquark.service.user.UserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

/**
 * 首页模块配置controller Created by chh on 16-12-6.
 */
@Controller
@ApiIgnore
public class HomeItemController extends BaseController {

  @Autowired
  private HomeItemService homeItemService;

  @Autowired
  private UserService userService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private UserAgentWhiteListService whiteListService;

  /**
   * 首页模块配置列表
   */
  @ResponseBody
  @RequestMapping("/homeItem/listByApp")
  public ResponseObject<Map<String, Object>> listByApp() {
    String shopId = this.getCurrentIUser().getShopId();
    UserType userType = userService.getUserType(this.getCurrentIUser().getId());
    HomeItemBelong belong = null;
    if (userType == UserType.B2B) {
      belong = HomeItemBelong.B2B;
    } else if (userType == UserType.B2C) {
      belong = HomeItemBelong.B2C;
    } else {
      belong = HomeItemBelong.DEFAULT;
    }

    List<HomeItemVO> homeItemVOs = null;
    Map<String, Object> aRetMap = new HashMap<>();
    homeItemVOs = homeItemService.listByApp(belong);
    aRetMap.put("list", homeItemVOs);

    //返回系统参数
    HashMap param = new HashMap();
    param.put("needUpdateLifeImg", false);
    param.put("needUpdateIdcardImg", false);
    UserAgentVO userAgentVO = userAgentService.selectByUserId(this.getCurrentIUser().getId());
    // 去掉登录时判断是否上传身份证和电子签名的弹框引导
    /**if(userAgentVO != null){
     String agentId = userAgentVO.getId();
     String lifeImg = userAgentVO.getLifeImg();
     String idcardImg = userAgentVO.getIdcardImg();
     // 是否需要更新签名和身份证照片
     // 如果代理在白名单内则跳过身份证上传
     if(StringUtils.isEmpty(lifeImg)){
     param.put("needUpdateLifeImg" , true);
     }
     if(StringUtils.isEmpty(idcardImg)  && !whiteListService.searchInListWithPhone(userAgentVO.getPhone())){
     param.put("needUpdateIdcardImg" , true);
     }
     }**/
    aRetMap.put("params", param);

    return new ResponseObject<>(aRetMap);
  }

  /**
   * 获取模块配置页面列表
   */
  @ResponseBody
  @RequestMapping("/homeItem/list")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword,
      @RequestParam String belong) {
    String shopId = this.getCurrentIUser().getShopId();
    List<HomeItemVO> viewPages = null;
    viewPages = homeItemService.list(pageable, belong);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", homeItemService.selectCnt(belong));
    aRetMap.put("list", viewPages);
    return new ResponseObject<>(aRetMap);
  }

  /**
   * 保存模块配置
   */
  @ResponseBody
  @RequestMapping("/homeItem/save")
  public ResponseObject<Boolean> save(HomeItemForm homeItemForm) {
    int result = 0;
    // 处理图片信息
    initImage(homeItemForm);
    HomeItem homeItem = new HomeItem();
    BeanUtils.copyProperties(homeItemForm, homeItem);
    if (StringUtils.isNotEmpty(homeItem.getId())) {
      result = homeItemService.modify(homeItem);
    } else {
      homeItem.setArchive(false);
      result = homeItemService.insert(homeItem);
    }
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/homeItem/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = homeItemService.delete(id);
    return new ResponseObject<>(result > 0);
  }


  /**
   * 查看某个具体的模块配置记录<br>
   */
  @ResponseBody
  @RequestMapping("/homeItem/{id}")
  public ResponseObject<HomeItemVO> view(@PathVariable String id, HttpServletRequest req) {
    HomeItemVO homeItemVO = homeItemService.selectByPrimaryKey(id);
    if (homeItemVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    return new ResponseObject<>(homeItemVO);
  }


  /**
   * 保存模块配置记录时的图片,把imageForm转化成model
   */
  private void initImage(HomeItemForm form) {
    List<TweetImage> imgs = new ArrayList<>();
    TweetImage pimg = null;
    int idx = 0;   // 从0开始
    if (null != form.getImgs() && !"".equals(form.getImgs())) {
      String[] fimgs = form.getImgs().split(",");
      for (String img : fimgs) {
        form.setImg(img);
      }
    }
  }

}
