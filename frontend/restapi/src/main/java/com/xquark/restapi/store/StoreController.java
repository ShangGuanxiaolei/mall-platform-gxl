package com.xquark.restapi.store;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.model.Store;
import com.xquark.dal.model.StoreMember;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.status.AgentStatus;
import com.xquark.dal.status.StoreMemberType;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.vo.StoreMemberVO;
import com.xquark.dal.vo.StoreVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.store.StoreMemberService;
import com.xquark.service.store.StoreService;
import com.xquark.service.team.TeamService;
import com.xquark.service.twitter.UserTwitterService;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ApiIgnore
public class StoreController extends BaseController {

  @Autowired
  private StoreService storeService;

  @Autowired
  private StoreMemberService storeMemberService;

  @Autowired
  private TeamService teamService;

  @Autowired
  private Qiniu qiniu;

  @Autowired
  private UserTwitterService userTwitterService;

  /**
   * 线下门店查询
   */
  @ResponseBody
  @RequestMapping("/store/list")
  public ResponseObject<Map<String, Object>> list(
      @RequestParam(value = "name", required = false) String name, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();

    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    params.put("shopId", shopId);
    List<StoreVO> result = storeService.list(pageable, params);
    Long total = storeService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  /**
   * 保存门店信息
   */
  @ResponseBody
  @RequestMapping("/store/saveInfo")
  public ResponseObject<Boolean> saveInfo(HttpServletRequest req,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "id", required = false) String id) {
    int result = 0;
    String shopId = getCurrentIUser().getShopId();
    if (StringUtils.isEmpty(id)) {
      String code = getCode();
      if (StringUtils.isNotEmpty(code)) {
        String url =
            req.getScheme() + "://" + req.getServerName() + "/store/applyMember?code_=" + code;
        // 生成url对应的二维码
        BufferedImage ImageQrcode = null;
        String qrcode = "";
        try {
          HashMap<EncodeHintType, Object> hints = new HashMap<>();
          // 指定纠错等级
          hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
          hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
          hints.put(EncodeHintType.MARGIN, 1);

          BitMatrix byteMatrix;
          byteMatrix = new MultiFormatWriter().encode(url, BarcodeFormat.QR_CODE, 240, 240, hints);
          ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
          ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
          java.util.List<InputStream> ins = new ArrayList<>();
          ImageIO.write(ImageQrcode, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
          InputStream is7 = new ByteArrayInputStream(os.toByteArray());
          ins.add(is7);
          // 将合成的图片上传到qiniu服务器
          final java.util.List<UpLoadFileVO> vos = qiniu.uploadImgStream(ins, FileBelong.PRODUCT);
          qrcode = qiniu.genQiniuFileUrl(vos.get(0).getKey());

        } catch (Exception e) {
          log.error("generateQrcode error", e);
          throw new RuntimeException(e);
        }

        Store store = new Store();
        store.setArchive(false);
        store.setShopId(shopId);
        store.setCode(code);
        store.setName(name);
        store.setQrcode(qrcode);
        store.setUrl(url);
        result = storeService.insert(store);
      }
    } else {
      Store store = new Store();
      store.setId(id);
      store.setName(name);
      result = storeService.update(store);
    }
    return new ResponseObject<>(result > 0);
  }

  private String getCode() {
    String code = "";
    int retCount = 0;
    int count = 0;
    /**
     * 通过数据库保证唯一性，如果出错重新生成一次，最多重试三次
     */
    while (retCount < 1) {
      count++;
      String key = getKey();
      Store store = storeService.selectByCode(key);
      if (store == null) {
        retCount = 1;
        code = key;
        break;
      }
      if (count >= 3) {
        retCount = 1;
        break;
      }
    }
    return code;
  }

  private String getKey() {
    String s = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    Random r = new Random();
    String result = "";
    for (int i = 0; i < 6; i++) {
      int n = r.nextInt(62);
      result += s.substring(n, n + 1);
    }
    return result;
  }

  /**
   * 线下门店店员查询
   */
  @ResponseBody
  @RequestMapping("/store/memberList")
  public ResponseObject<Map<String, Object>> memberList(
      @RequestParam(value = "storeId", required = false) String storeId, Pageable pageable) {
    Map<String, Object> params = new HashMap<>();
    String shopId = getCurrentIUser().getShopId();
    params.put("storeId", storeId);
    List<StoreMemberVO> result = storeMemberService.list(pageable, params);
    Long total = storeMemberService.selectCnt(params);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", total);
    aRetMap.put("list", result);
    return new ResponseObject<>(aRetMap);

  }

  @ResponseBody
  @RequestMapping("/store/getMember/{id}")
  public ResponseObject<StoreMember> getMember(@PathVariable String id) {
    StoreMember result = storeMemberService.load(id);
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping("/store/deleteMember/{id}")
  public ResponseObject<Boolean> deleteMember(@PathVariable String id) {
    StoreMember member = storeMemberService.load(id);
    if (member.getType() == StoreMemberType.LEADER) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "不允许删除老板，请联系系统管理员处理。");
    }
    // 删除店员后，同时删除该用户信息
    storeMemberService.deleteUserInfo(member.getUserId());

    int result = storeMemberService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * @return
   */
  @ResponseBody
  @RequestMapping("/store/saveMember")
  public ResponseObject<Boolean> saveMember(StoreMember member) {
    String shopId = getCurrentIUser().getShopId();
    int result = storeMemberService.update(member);

    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  @ResponseBody
  @RequestMapping("/store/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    // 删除门店后，需要删除该门店下所有店员信息
    List<StoreMember> storeMembers = storeMemberService.selectByStoreId(id);
    for (StoreMember member : storeMembers) {
      storeMemberService.deleteUserInfo(member.getUserId());
      storeMemberService.deleteForArchive(member.getId());
    }
    teamService.deleteByStoreId(id);
    int result = storeService.deleteForArchive(id);
    return new ResponseObject<>(result > 0);
  }

  @ResponseBody
  @RequestMapping("/store/getInfo/{id}")
  public ResponseObject<Store> getInfo(@PathVariable String id) {
    Store result = storeService.load(id);
    return new ResponseObject<>(result);
  }

  /**
   * 线下门店店员审核
   */
  @ResponseBody
  @RequestMapping("/store/auditMember/{id}")
  public Json auditMember(@PathVariable String id) {
    Json json = new Json();
    String rootShopId = getCurrentIUser().getShopId();
    StoreMember result = storeMemberService.load(id);
    StoreMember activeLeader = storeMemberService.selectActiveLeader(result.getStoreId());
    StoreMember leader = activeLeader;
    if (result.getStatus() == AgentStatus.ACTIVE) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("该店员已经是审核状态");
      return json;
    } else if (result.getType() == StoreMemberType.LEADER) {
      // 如果审核的是店长，则先判断是否已经有审核过的店长，如果有，不允许存在多个店长
      if (activeLeader != null) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("门店已经有审核过的店长");
        return json;
      }
    } else if (result.getType() == StoreMemberType.SENIOR) {
      // 如果审核的是店员，则先判断是否已经有审核过的店长，如果没有，则提示门店必须先有审核的店长
      if (activeLeader == null) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("门店必须先有审核的店长");
        return json;
      }
    } else if (result.getType() == StoreMemberType.MEMBER) {
      // 如果审核的是店员，则先判断是否已经有审核过的店长，如果没有，则提示门店必须先有审核的店长
      if (activeLeader == null) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("门店必须先有审核的店长");
        return json;
      }
      String parentId = result.getParentId();
      leader = storeMemberService.load(parentId);
      // 普通店员先要判断上级店员是否已审核
      if (leader == null || leader.getStatus() != AgentStatus.ACTIVE) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("对应的高级店员必须先审核");
        return json;
      }
    }
    // 插入推客信息
    storeMemberService.insertTwitter(result, leader, rootShopId);

    // 插入战队信息
    storeMemberService.insertTeam(result, rootShopId);

    // 如果是店长，则该店长默认是合伙人
    if (result.getType() == StoreMemberType.LEADER) {
      storeMemberService.insertPartner(result, rootShopId);
    }

    // 更新门店店员状态
    StoreMember updateMember = new StoreMember();
    updateMember.setId(result.getId());
    updateMember.setStatus(AgentStatus.ACTIVE);
    storeMemberService.update(updateMember);

    json.setRc(Json.RC_SUCCESS);
    json.setMsg("审核通过");
    return json;
  }

  /**
   * @return
   */
  @ResponseBody
  @RequestMapping("/store/api/saveMember")
  public Json apisaveMember(@RequestParam(value = "code", required = false) String code,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "phone", required = false) String phone,
      @RequestParam(value = "type", required = false) String type,
      @RequestParam(value = "teamName", required = false) String teamName,
      @RequestParam(value = "seniorId", required = false) String seniorId) {
    String userId = getCurrentIUser().getId();
    Json json = new Json();
    List<UserTwitter> twitters = userTwitterService.selectByUserId(userId);
    if (twitters != null && twitters.size() > 0 && twitters.get(0) != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("已经申请过推客，不能重复申请。");
      return json;
    }
    // 先通过门店code获取门店id
    Store store = storeService.selectByCode(code);
    if (store == null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("该门店不存在，请联系客服。");
      return json;
    }
    StoreMember userIdMember = storeMemberService.selectByUserId(userId);
    if (userIdMember != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("您已申请过信息，请不要重复申请");
      return json;
    }
    StoreMember phoneMember = storeMemberService.selectByPhone(phone);
    if (phoneMember != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("手机号已被" + phoneMember.getName() + "申请，请重新输入。");
      return json;
    }

    String storeId = store.getId();
    StoreMember member = new StoreMember();
    member.setName(name);
    member.setPhone(phone);
    member.setTeamName(teamName);
    member.setType(StoreMemberType.valueOf(type));
    member.setUserId(userId);
    member.setStatus(AgentStatus.APPLYING);
    member.setArchive(false);
    if (StringUtils.isNotEmpty(seniorId)) {
      member.setParentId(seniorId);
    }
    member.setStoreId(storeId);
    int result = storeMemberService.insert(member);
    if (result == 1) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("申请成功。");
      return json;
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("申请失败。");
      return json;
    }
  }

  /**
   * 查找某个门店下所有特约店员
   */
  @ResponseBody
  @RequestMapping("/store/getSenior")
  public ResponseObject<List<StoreMember>> getSenior(
      @RequestParam(value = "code", required = false) String code) {
    Store store = storeService.selectByCode(code);
    if (store != null) {
      List<StoreMember> result = storeMemberService.selectSeniorByStoreId(store.getId());
      return new ResponseObject<>(result);
    } else {
      return new ResponseObject<List<StoreMember>>(new ArrayList<StoreMember>());
    }

  }

  /**
   * 激活战队
   */
  @ResponseBody
  @RequestMapping("/store/enable/{id}")
  public ResponseObject<Boolean> enable(@PathVariable String id) {
    Store store = new Store();
    store.setId(id);
    store.setStatus(AgentStatus.ACTIVE);
    int result = storeService.update(store);
    return new ResponseObject<>(result > 0);
  }

}