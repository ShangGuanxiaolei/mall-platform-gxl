package com.xquark.restapi.org;

import com.xquark.dal.model.Org;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.merchant.MerchantRoleService;
import com.xquark.service.org.OrgService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 作者: wangxh 创建日期: 17-3-16 简介:
 */
@Controller
public class OrgController {

  @Autowired
  private OrgService orgService;

  @Autowired
  private MerchantRoleService roleService;

  @ResponseBody
  @RequestMapping("/org/{id}")
  public ResponseObject<Org> view(@PathVariable String id) {
    Org org = orgService.load(id);
    return new ResponseObject<>(org);
  }

  /**
   * 返回部门上下级json数据(jsTree格式)
   *
   * @return 部门集合
   */
  @ResponseBody
  @RequestMapping("/org/listTree")
  public List<Map<String, Object>> listOrgTree() {
    return orgService.listTree();
  }

  @ResponseBody
  @RequestMapping("/org/list")
  public ResponseObject<Map<String, Object>> list(String order, String parentId, Pageable pageable,
      String direction) {
    if ("".equals(parentId) || "0".equals(parentId)) {
      parentId = null;
    }
    order = StringUtils.defaultIfEmpty(order, "sort_no");
    direction = StringUtils.defaultIfEmpty(direction, "desc");
    List<Org> orgList;

    if ("name".equals(order)) {
      orgList = orgService.listByName(parentId, pageable, Sort.Direction.fromString(direction));
    } else if ("created_at".equals(order)) {
      orgList = orgService.listByTime(parentId, pageable, Sort.Direction.fromString(direction));
    } else {
      orgList = orgService.listBySortNo(parentId, pageable, Sort.Direction.fromString(direction));
    }

    Integer orgTotal;
    Map<String, Object> orgMap = new HashMap<>();
    if (parentId != null) {
      orgTotal = orgService.loadSubCounts(parentId);
      orgMap.put("parentId", parentId);
    } else {
      orgTotal = orgService.loadCounts();
    }
    orgMap.put("orgTotal", orgTotal);
    orgMap.put("list", orgList);
    return new ResponseObject<>(orgMap);
  }

  @ResponseBody
  @RequestMapping("/org/updateMerchantOrg")
  public ResponseObject<Boolean> updateMerchantOrg(@RequestParam String merchantId,
      @RequestParam String orgId) {
    return new ResponseObject<>(orgService.updateMerchantOrg(merchantId, orgId) != 0);
  }

  /**
   * 保存部门信息
   *
   * @param name 部门名称
   * @param parentId 父id
   */
  @ResponseBody
  @RequestMapping("/org/save")
  public ResponseObject<Org> save(@RequestParam String name, @RequestParam String parentId,
      String remark) {
    if ("0".equals(parentId)) {
      parentId = null;
    }
    Org org = orgService.save(parentId, name, remark);
    return new ResponseObject<>(org);
  }

  @ResponseBody
  @RequestMapping("/org/update")
  public ResponseObject<Org> updateName(@RequestParam String id, @RequestParam String parentId,
      @RequestParam String name, String remark) {
    if ("0".equals(parentId)) {
      parentId = null;
    }
    Org org = orgService.update(id, parentId, name, remark);
    return new ResponseObject<>(org);
  }

  @ResponseBody
  @RequestMapping("/org/delete")
  public void delete(@RequestParam String id) {
    orgService.remove(id);
  }

}
