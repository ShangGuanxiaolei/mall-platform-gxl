package com.xquark.restapi.common;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.Feedback;
import com.xquark.dal.model.FeedbackType;
import com.xquark.dal.vo.FeedbackVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.vo.Json;
import com.xquark.service.feedback.FeedbackService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tonghu
 */
@RestController
@RequestMapping("/feedback")
@Api(value = "反馈接口")
public class FeedbackController extends BaseController {

  @Autowired
  private FeedbackService feedbackService;

  /**
   * 用户反馈
   */
  @ApiOperation(value = "保存用户反馈", produces = MediaType.APPLICATION_JSON_VALUE)
  @RequestMapping("/save")
  public ResponseObject<Boolean> save(@Valid @ModelAttribute FeedbackForm form, Errors errors) {
    ControllerHelper.checkException(errors);
    Feedback feedback = new Feedback();
    feedback.setType(FeedbackType.USER);
    BeanUtils.copyProperties(form, feedback);
    List<String> tags = form.getTagIds();
    boolean result = feedbackService.saveWithTags(feedback, tags);
    return new ResponseObject<>(result);
  }

  /**
   * 报错反馈
   */
  @RequestMapping("/reportError")
  public ResponseObject<Boolean> reportError(@Valid @ModelAttribute FeedbackForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    Feedback feedback = new Feedback();
    feedback.setType(FeedbackType.ERROR);
    BeanUtils.copyProperties(form, feedback);
    int result = feedbackService.insert(feedback);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 用户反馈
   */
  @RequestMapping("/contact")
  public Json contact(@Valid @ModelAttribute FeedbackForm form, Errors errors) {
    ControllerHelper.checkException(errors);
    Json json = new Json();
    int result = 0;
    // 根据联系方式判断是否已经提交过
    Feedback existFeedback = feedbackService.selectByContact(form.getContact());
    if (existFeedback != null) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("请不要重复提交。");
      return json;
    }
    Feedback feedback = new Feedback();
    feedback.setType(FeedbackType.CONTACT);
    BeanUtils.copyProperties(form, feedback);
    result = feedbackService.insert(feedback);
    if (result == 1) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("提交成功。感谢您的关注，我们会尽快与您联系。");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("提交失败！");
    }
    return json;
  }

  /**
   * 获取用户对应角色列表
   */
  @RequestMapping("/contactList")
  public ResponseObject<Map<String, Object>> list(Pageable pageable, @RequestParam String keyword,
      @RequestParam String isShare) {
    List<FeedbackVO> feedbackVOs;
    Map<String, Object> params = new HashMap<>();
    params.put("type", FeedbackType.USER);
    params.put("share", isShare);
    feedbackVOs = feedbackService.listFeedbacksByAdmin(params, pageable);
    Map<String, Object> aRetMap = new HashMap<>();
    aRetMap.put("total", feedbackService.countFeedbacksByAdmin(params));
    aRetMap.put("list", feedbackVOs);
    return new ResponseObject<>(aRetMap);
  }

  @RequestMapping("/delete/{id}")
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int result = feedbackService.delete(id);
    return new ResponseObject<>(result > 0);
  }

}
