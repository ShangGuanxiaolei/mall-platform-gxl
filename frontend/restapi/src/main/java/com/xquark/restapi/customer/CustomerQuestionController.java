package com.xquark.restapi.customer;

import com.xquark.dal.model.QuestionTypeVo;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.customer.CustomerQuestionService;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/Question")
public class CustomerQuestionController {

    @Autowired
    private CustomerQuestionService customerQuestionService;


    /**
     * 查询当前类型下的问题详情
     * @return
     */
    @RequestMapping("/customerAll")
    @ResponseBody
    public ResponseObject<Map<String, Object>> selectAll(){
        Map<String, Object> objectMap = new HashMap<>();
        List<QuestionTypeVo> list=customerQuestionService.selectQuestionAll();
        objectMap.put("QuestionList", list);
        return new ResponseObject<>(objectMap);
    }



    /**
     * 根据问题详情id查询问题内容
     * @return
     */
    @RequestMapping("/selectContentById")
    @ResponseBody
    public ResponseObject<Map<String, Object>> selectContentById(String id){
        Map<String, Object> objectMap = new HashMap<>();
        String content=customerQuestionService.selectContentById(id);
        objectMap.put("content", content);
        return new ResponseObject<>(objectMap);
    }



    /**
     * 修改有无帮助
     * @return
     */
    @RequestMapping("/customerHelp")
    @ResponseBody
    public ResponseObject<Boolean> updateHelp(String help,int id){
        boolean update=customerQuestionService.updateHelpful(help,id);
        return new ResponseObject<>(update);
    }




}
