package com.xquark.restapi.pintuan;

import com.xquark.dal.model.mypiece.ConfirmAndCheckVo;
import com.xquark.restapi.BaseController;
import com.xquark.service.pintuan.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

/**
 * 开团条件判断-->返回结果给页面进行判断，条件通过进入订单确认页
 * 订单确认-->提交订单-->生成订单接口！-->调订单服务
 * 支付-->提交支付接口！-->支付是否成功的判断-->成功以后记录信息，短链及应用锁
 *
 *
 */
@RestController
@ResponseBody
@RequestMapping("/group")
public class GroupController extends BaseController {
    @Autowired
    private GroupService groupService;
    /**
     * 开团
     * 状态码的返回
     * pCode,groupSkuCode,skuCount
     */
    @RequestMapping("/create")
    public Map<String,Object> create(@RequestBody ConfirmAndCheckVo confirmAndCheckVo){
        Map<String,Object> map = groupService.createTeam(confirmAndCheckVo,getCurrentUser());
        return map;
    }

}
