package com.xquark.restapi.fancard;

import com.xquark.dal.type.GenderType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 17-11-7. DESC:
 */
public class FanCardApplyForm {

  /**
   * 申请人姓名
   */
  @NotBlank(message = "姓名不能为空")
  private String name;

  /**
   * 申请人性别
   */
  @NotNull(message = "性别不能为空")
  private GenderType gender;

  /**
   * 年龄
   */
  @NotNull(message = "年龄不能为空")
  @Digits(integer = 120, fraction = 1, message = "请输入正确的年龄")
  private Integer age;

  /**
   * 手机号
   */
  @NotBlank(message = "手机号不能为空")
  @Pattern(regexp = "(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$",
      message = "请输入正确的手机号码")
  private String phone;

  @NotBlank(message = "身份证号不能为空")
  @Pattern(regexp = "^[1-9]\\d{13,16}[a-zA-Z0-9]$", message = "请输入15到18位的身份证号码")
  private String idCard;

  /**
   * 邮箱
   */
  @NotBlank(message = "邮箱不能为空")
  @Email(message = "邮箱格式不正确")
  private String email;

  /**
   * 微信号
   */
  @NotBlank(message = "微信号不能为空")
  private String wechatNo;

  /**
   * 住址
   */
  @NotBlank(message = "地址不能为空")
  private String address;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public GenderType getGender() {
    return gender;
  }

  public void setGender(GenderType gender) {
    this.gender = gender;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getIdCard() {
    return idCard;
  }

  public void setIdCard(String idCard) {
    this.idCard = idCard;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getWechatNo() {
    return wechatNo;
  }

  public void setWechatNo(String wechatNo) {
    this.wechatNo = wechatNo;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public static void main(String[] args) {
    java.util.regex.Pattern pattern = java.util.regex.Pattern
        .compile("^[1-9]\\d{13,16}[a-zA-Z0-9]$");
    System.out.println(pattern.matcher("330621199404134").matches());

  }
}
