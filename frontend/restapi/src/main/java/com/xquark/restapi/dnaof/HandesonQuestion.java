package com.xquark.restapi.dnaof;

import java.io.Serializable;

/**
 * 商品主体信息
 * @author M
 *
 */
public class HandesonQuestion implements Serializable {
	
	private static final long serialVersionUID = 1362852746615397655L;
	
	private String sampleCode;  //样本编号
	
	private String HEIGHT;  //身高
	
	private String BODYWEIGHT;  //体重
	
	private String OVERNIGHT;   //加班熬夜
	
	private String EXERCISE;  //运动频率
	
	private String LIFE;  //生活习惯

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getHEIGHT() {
		return HEIGHT;
	}

	public void setHEIGHT(String hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public String getBODYWEIGHT() {
		return BODYWEIGHT;
	}

	public void setBODYWEIGHT(String bODYWEIGHT) {
		BODYWEIGHT = bODYWEIGHT;
	}

	public String getOVERNIGHT() {
		return OVERNIGHT;
	}

	public void setOVERNIGHT(String oVERNIGHT) {
		OVERNIGHT = oVERNIGHT;
	}

	public String getEXERCISE() {
		return EXERCISE;
	}

	public void setEXERCISE(String eXERCISE) {
		EXERCISE = eXERCISE;
	}

	public String getLIFE() {
		return LIFE;
	}

	public void setLIFE(String lIFE) {
		LIFE = lIFE;
	}

}
