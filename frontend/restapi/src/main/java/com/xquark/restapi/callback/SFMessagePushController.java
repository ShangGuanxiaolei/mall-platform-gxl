package com.xquark.restapi.callback;

import com.alibaba.fastjson.JSONObject;
import com.xquark.restapi.ResponseObject;
import org.springframework.web.bind.annotation.*;

/**
 *
 * 顺丰商品订单信息推送
 *
 * @author: tanggb
 * @date: 2019/05/15 10:16
 */
@RestController
@RequestMapping("/sfCallback")
public class SFMessagePushController {

    /**
     * 顺丰拆物流单消息推送
     * @return
     */
    @RequestMapping("/splitOrder")
    @ResponseBody
    public ResponseObject<String> splitOrder(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        return new ResponseObject<>("ok");
    }

    /**
     * 顺丰商品订单物流单号,签收推送
     * @return
     */
    @RequestMapping("/shipmentNum")
    @ResponseBody
    public ResponseObject<String> shipmentNumber(@RequestBody String json){

        return new ResponseObject<>("ok");
    }

    /**
     * 顺丰新增的商品推送
     * @return
     */
    @RequestMapping("/productMsg")
    @ResponseBody
    public ResponseObject<String> productMessage(@RequestBody String json){

        return new ResponseObject<>("");
    }

    /**
     * 顺丰商品修改及上下架推送
     * @return
     */
    @RequestMapping("/updateProductMsg")
    @ResponseBody
    public ResponseObject<String> updateProductMsg(@RequestBody String json){

        return new ResponseObject<>("");
    }
}
