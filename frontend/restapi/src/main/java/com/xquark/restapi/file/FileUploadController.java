package com.xquark.restapi.file;

import com.google.common.base.Preconditions;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.mapper.FileHeaderMapper;
import com.xquark.dal.mapper.JobSchedulerLogMapper;
import com.xquark.dal.model.FileHeader;
import com.xquark.dal.model.JobSchedulerLog;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.type.FileUploadType;
import com.xquark.dal.type.JobType;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.xquark.dal.type.FileUploadType.IMAGE;
import static com.xquark.dal.type.FileUploadType.VIDEO;
import static org.parboiled.common.Preconditions.checkNotNull;
import static org.parboiled.common.Preconditions.checkState;

/**
 * Created by wangxinhua on 17-10-19. DESC: 文件上传接口
 */
@RestController
@RequestMapping("/file")
public class FileUploadController extends BaseController {

  private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

  @Value("${site.web.host.name}")
  private String hostName;

  /**
   * CKEDITOR 回调JS模板 详细参考: https://docs.ckeditor.com/ckeditor4/docs/#!/guide/dev_file_browser_api
   */
  private final static String EDITOR_CALLBACK_TEMPLATE
      = "window.parent.CKEDITOR.tools.callFunction(%s, '%s', '%s')";

  private ResourceFacade resourceFacade;

  private ServletContext servletContext;

  private JobSchedulerLogMapper jobSchedulerLogMapper;

  private FileHeaderMapper fileHeaderMapper;

  @Autowired
  public void setResourceFacade(ResourceFacade resourceFacade) {
    this.resourceFacade = resourceFacade;
  }

  @Autowired
  public void setServletContext(ServletContext servletContext) {
    this.servletContext = servletContext;
  }

  @Autowired
  public void setJobSchedulerLogMapper(JobSchedulerLogMapper jobSchedulerLogMapper) {
    this.jobSchedulerLogMapper = jobSchedulerLogMapper;
  }

  @Autowired
  public void setFileHeaderMapper(FileHeaderMapper fileHeaderMapper) {
    this.fileHeaderMapper = fileHeaderMapper;
  }

  /**
   * 获取文件限制配置
   */
  @RequestMapping("/fileLimitation")
  public ResponseObject<Map<String, FileLimitationVO>> fileLimition() {
    FileUploadType[] types = FileUploadType.values();
    Map<String, FileLimitationVO> result = new LinkedHashMap<>();
    for (FileUploadType type : types) {
      if (type != FileUploadType.UNKNOWN) {
        result.put(type.name(), FileLimitationVO.of(type));
      }
    }
    return new ResponseObject<>(result);
  }

  @RequestMapping("/uploadMulti")
  public ResponseObject<List<UpLoadFileVO>> uploadMultipleFileToQiNiu(FileUploadForm form) {
    List<MultipartFile> files = form.getFile();
    if (files.size() <= 0) {
      throw new BizException(GlobalErrorCode.FILE_UPLOAD_ERROR, "请至少上传一个文件");
    }
    for (MultipartFile file : files) {
//            if (!checkFileType(form.getPromotionType(), file)) {
//                throw new BizException(GlobalErrorCode.INVALID_FILE_TYPE);
//            }
      if (!checkFileSize(form.getType(), file)) {
        throw new BizException(GlobalErrorCode.FILE_SIZE_TOO_LARGE);
      }
    }
    List<UpLoadFileVO> result;
    try {
      result = resourceFacade.uploadFile(files, FileBelong.RESOURCE);
    } catch (Exception e) {
      String msg = "文件上传失败";
      logger.error(msg, e);
      throw new BizException(GlobalErrorCode.FILE_UPLOAD_ERROR, "文件上传失败");
    }
    return new ResponseObject<>(result);
  }

  /**
   * 文件上传到七牛
   *
   * @param file 上传文件
   * @param type 上传文件类型
   * @param jobType 是否来自job请求, 若不为空, 则返回jobId进行异步处理
   * @return 七牛文件信息
   */
  @RequestMapping("/upload")
  public ResponseObject<UpLoadFileVO> uploadFileToQiNiu(@RequestParam("file") MultipartFile file,
      @RequestParam("type") FileUploadType type,
      JobType jobType) {
    if (!file.isEmpty()) {
      String fileName = file.getOriginalFilename();
             /*if (!checkFileType(type, file)) {
                throw new BizException(GlobalErrorCode.INVALID_FILE_TYPE);
            }*/
      if (!checkFileSize(type, file)) {
        throw new BizException(GlobalErrorCode.FILE_SIZE_TOO_LARGE);
      }
      try {
        UpLoadFileVO fileVo = this.uploadToQiNiu(file);
        // 暂时特殊处理，只有图片类型需要加图片后缀
        if (type != FileUploadType.IMAGE) {
          fileVo.setUrl(resourceFacade.resolveUrl2Orig(fileVo.getId()));
        }
        if (jobType != null) {
          Long jobId = startFileJob(fileName, fileVo.getUrl(), jobType);
          fileVo.setJobId(jobId);
        }
        return new ResponseObject<>(fileVo);
      } catch (Exception e) {
        String msg = String.format("文件 %s 上传至七牛服务器失败", fileName);
        logger.error(msg, e);
        throw new BizException(GlobalErrorCode.FILE_UPLOAD_ERROR);
      }
    }
    return new ResponseObject<>();
  }

  /**
   * 上传视频到7牛服务器并返回上传后的地址
   *
   * @param file 文件
   * @param request 请求
   * @param response 响应
   * @return 视频地址
   */
  @RequestMapping("/editor/upload/qiniuVideo")
  public void uploadMultiPartVideoToQiNiu(@RequestParam("upload") MultipartFile file,
      HttpServletRequest request, HttpServletResponse response) {
    String videoUrl;
    String callback = request.getParameter("CKEditorFuncNum");
    if (!file.isEmpty()) {
      PrintWriter out = null;
      response.setContentType("text/html;charset=UTF-8");
      try {
        out = response.getWriter();
        String fileName = file.getOriginalFilename();
        if (!VIDEO.matches(fileName)) {
          writeError(out, callback, "文件格式不支持");
        } else {
          String img = this.uploadToQiNiu(file).getId();
          videoUrl = resourceFacade.resolveUrl2Orig(img);
          writeOutput(out, callback, videoUrl);
        }
      } catch (IOException e) {
        log.error("文件上传失败", e);
      } catch (Exception e) {
        log.error("上传到七牛服务器失败");
      } finally {
        if (out != null) {
          out.close();
        }
      }
    }
  }


  /**
   * 上传富文本图片
   *
   * @param file 文件
   * @return 图片url
   */
  @RequestMapping("/editor/upload/image")
  public void uploadMultiPartImg(@RequestParam("upload") MultipartFile file,
      HttpServletRequest request, HttpServletResponse response) {
    String imgUrl = "";
    String callback = request.getParameter("CKEditorFuncNum");
    if (!file.isEmpty()) {
      PrintWriter out = null;
      response.setContentType("text/html;charset=UTF-8");
      try {
        out = response.getWriter();
        String fileName = file.getOriginalFilename();
        if (!IMAGE.matches(fileName)) {
          writeError(out, callback, "文件格式不支持");
        } else {
          String img = this.uploadToQiNiu(file).getId();
          imgUrl = resourceFacade.resolveUrl(img);
          writeOutput(out, callback, imgUrl);
        }
      } catch (IOException e) {
        log.error("文件上传失败", e);
      } catch (Exception e) {
        log.error("上传到七牛服务器失败");
      } finally {
        if (out != null) {
          out.close();
        }
      }
    }
  }

  /**
   * 上传七牛服务器返回Key
   *
   * @param file 上传文件
   * @return 七牛的文件key
   * @throws Exception IOException及其他异常
   */
  private UpLoadFileVO uploadToQiNiu(MultipartFile file) throws Exception {
    assert file != null;
    InputStream fileInputStream = file.getInputStream();
    return resourceFacade
        .uploadFileStream(fileInputStream, FileBelong.RESOURCE);
  }

  /**
   * 上传多个文件到七牛服务器
   *
   * @param files 文件集合
   * @return list of {@link UpLoadFileVO}
   * @throws NullPointerException 当 {@code files} 为空时
   * @throws IllegalStateException 当 {@code files} 为空集合时
   * @throws Exception 文件上传异常
   */
  private List<UpLoadFileVO> uploadToQiNiu(List<MultipartFile> files) throws Exception {
    Preconditions.checkNotNull(files);
    Preconditions.checkState(files.size() > 0, "文件数量必须大于0");
    return resourceFacade.uploadFile(files, FileBelong.RESOURCE);
  }

  private void writeOutput(PrintWriter writer, String callback, String content) {
    if (StringUtils.isNotBlank(content)) {
      writeMessage(writer, callback, content, "上传成功");
    } else {
      writeError(writer, callback, "上传失败");
    }
  }

  private void writeError(PrintWriter writer, String callback, String message) {
    writeMessage(writer, callback, "", message);
    writer.close();
  }

  private void writeMessage(PrintWriter writer, String callback, String content, String message) {
    writer.println("<script type=\"text/javascript\">");
    writer.println(String.format(EDITOR_CALLBACK_TEMPLATE, callback, content, message));
    writer.println("</script>");
  }

  public static void main(String[] args) {
    String[] a = new String[]{"31313", ".jpg"};
    a[0] = a[0] + new Date().getTime();
    System.out.println(StringUtils.join(a));
  }

  private boolean checkFileType(FileUploadType type, MultipartFile file) {
    checkNotNull(type);
    checkNotNull(file);
    checkState(!file.isEmpty(), "文件不能为空");
    return type.matches(file.getOriginalFilename());
  }

  private boolean checkFileSize(FileUploadType type, MultipartFile file) {
    checkNotNull(type);
    checkNotNull(file);
    checkState(!file.isEmpty(), "文件不能为空");
    long mbFileSize = file.getSize() / (1024 * 1024);
    return mbFileSize <= (long) type.getLimitSize();
  }

  /**
   * 在指定目录和前缀中查找相同文件
   *
   * @param dir 文件目录
   * @param prefix 查找文件前缀
   * @param file 目标文件
   * @return 查找结果，有相同的则只返回一个, 没有找到则返回null
   */
  private File findSameFile(String dir, final String prefix, final File file) {
    File dirFile = new File(dir);
    if (!dirFile.isDirectory()) {
      return null;
    }
    File[] files = dirFile.listFiles(new FileFilter() {
      @Override
      public boolean accept(File iterFile) {
        try {
          return iterFile.getName().startsWith(prefix) && FileUtils
              .contentEquals(iterFile, file);
        } catch (IOException e) {
          log.error("查找相同文件出错");
        }
        return false;
      }
    });
    if (!ArrayUtils.isEmpty(files)) {
      return files[0];
    }
    return null;
  }

  /**
   * 记录日志, 发起导入
   */
  private Long startFileJob(String name, String path, JobType jobType) {
    JobSchedulerLog jobSchedulerLog = JobSchedulerLog.empty(jobType);
    jobSchedulerLogMapper.insert(jobSchedulerLog);
    Long jobId = jobSchedulerLog.getJobId();

    FileHeader fileHeader = new FileHeader(jobId,
        name, path, 0);

    fileHeaderMapper.insert(fileHeader);
    log.info("=== 发起excel上传 {} ===", fileHeader);
    return jobId;
  }

}
