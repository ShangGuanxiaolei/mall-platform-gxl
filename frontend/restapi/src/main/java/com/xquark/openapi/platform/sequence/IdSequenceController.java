package com.xquark.openapi.platform.sequence;

import com.xquark.dal.mapper.CustomerluckylistMapper;
import com.xquark.dal.model.Customerluckylist;
import com.xquark.dal.type.IdSequenceType;
import com.xquark.restapi.AddressSequenceVO;
import com.xquark.restapi.BankSequenceVO;
import com.xquark.restapi.MemberSequenceVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.sequence.IdSequenceService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午1:15
 */
@RestController
@RequestMapping("/openapi/sequences")
@SuppressWarnings("unused")
public class IdSequenceController {

  private final IdSequenceService idSequenceService;

  private final CustomerluckylistMapper customerluckylistMapper;

  @Autowired
  public IdSequenceController(IdSequenceService idSequenceService,
      CustomerluckylistMapper customerluckylistMapper) {
    this.idSequenceService = idSequenceService;
    this.customerluckylistMapper = customerluckylistMapper;
  }

  @RequestMapping("/member/next")
  public ResponseObject<MemberSequenceVO> obtMemberId(
      @RequestParam(required = false)
      String tinCode) {
    if (StringUtils.isNoneBlank(tinCode)) {
      Customerluckylist luckyList = customerluckylistMapper.selectByTinCode(tinCode);
      if (luckyList != null) {
        // 直接返回已有的吉祥号
        Long cpId = luckyList.getCpId();
        return new ResponseObject<>(new MemberSequenceVO(cpId));
      }
    }
    Long id = idSequenceService.generateIdByType(IdSequenceType.MEMBER);
    return new ResponseObject<>(new MemberSequenceVO(id));
  }

  @RequestMapping("/address/next")
  public ResponseObject<AddressSequenceVO> obtainAddressId() {
    Long id = idSequenceService.generateIdByType(IdSequenceType.ADDRESS);
    return new ResponseObject<>(new AddressSequenceVO(id));
  }

  @RequestMapping("/bank/next")
  public ResponseObject<BankSequenceVO> obtainBankId() {
    Long id = idSequenceService.generateIdByType(IdSequenceType.BANK);
    return new ResponseObject<>(new BankSequenceVO(id));
  }

}
