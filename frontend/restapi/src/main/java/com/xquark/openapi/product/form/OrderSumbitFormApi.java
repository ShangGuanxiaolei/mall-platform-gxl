package com.xquark.openapi.product.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xquark.dal.model.AddedTaxBill;
import com.xquark.dal.model.CompanyBill;
import com.xquark.dal.model.PersonalBill;
import com.xquark.restapi.order.PromotionInfoForm;
import org.apache.commons.collections.CollectionUtils;

import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 提交订单Form表单逻辑 扩展下单Form，新增支付方式
 *
 * @author odin
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderSumbitFormApi extends PromotionInfoForm {

  private static final String MOBILE_MESSAGE = "{valid.mobile.message}";

  /**
   * 订单id
   */
  private String orderId;

  private String preOrderNo;

  /**
   * 购买购物车中的一个或多个项目
   */
  private List<String> skuIds;

  /**
   * 买家对卖家们的留言
   */
  private Map<String, String> shopRemarks = new HashMap<>();

  /**
   * 直接下单skuId
   */
  private String skuId;
  /**
   * 直接下单，购买数量
   */
  private int qty;

  /**
   * 是否使用积分抵扣
   */
  private boolean useDeduction;

  /**
   * 直接下单的代销商品id
   */
  private String proLiteralId;


  private BillFormType billFormType;

  private PersonalBill personalBill;

  private CompanyBill companyBill;

  private AddedTaxBill addedTaxBill;

  /**
   * 收货人
   */
  private String consignee;

  private String promotionProductId;

  /**
   * 活动商品编码列表
   */
  private List<String> promotionSkuIds;

  /**
   * 手机号码
   */
  @Pattern(regexp = "(13\\d|14[57]|15[^4,\\D]|17[678]|18\\d)\\d{8}|170[059]\\d{7}", message = MOBILE_MESSAGE)
  private String phone;

  /**
   * 地区数字
   */
  private String zoneId;

  /**
   * 详细地址
   */
  private String street;

  /**
   * 地址id
   */
  private String addressId;

  /**
   * 是否自提
   */
  private String ispickup;

  private String pDetailCode;
  private String pCode;

  // 是否来自特权码商品
  private String isPrivilege;

  // 是否开发票
  private Boolean needInvoice = false;

  private List<String> usingCoupons;

  private BigDecimal selectPoint;

  private BigDecimal selectCommission;

  private Long upLine;

  private String fromCpId;

  /**
   * 大会活动邀请码
   */
  private String inviteCode;

  private String remark;

  public String getFromCpId() {
    return fromCpId;
  }

  public void setFromCpId(String fromCpId) {
    this.fromCpId = fromCpId;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public String getIsPrivilege() {
    return isPrivilege;
  }

  public void setIsPrivilege(String isPrivilege) {
    this.isPrivilege = isPrivilege;
  }

  public String getIspickup() {
    return ispickup;
  }

  public void setIspickup(String ispickup) {
    this.ispickup = ispickup;
  }

  public String getPreOrderNo() {
    return preOrderNo;
  }

  public void setPreOrderNo(String preOrderNo) {
    this.preOrderNo = preOrderNo;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }


  public String getAddressId() {
    return addressId;
  }

  public void setAddressId(String addressId) {
    this.addressId = addressId;
  }


  public List<String> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<String> skuIds) {
    this.skuIds = skuIds;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }


  public PersonalBill getPersonalBill() {
    return personalBill;
  }

  public void setPersonalBill(PersonalBill personalBill) {
    this.personalBill = personalBill;
  }

  public CompanyBill getCompanyBill() {
    return companyBill;
  }

  public void setCompanyBill(CompanyBill companyBill) {
    this.companyBill = companyBill;
  }

  public AddedTaxBill getAddedTaxBill() {
    return addedTaxBill;
  }

  public void setBillFormType(BillFormType billFormType) {
    this.billFormType = billFormType;
  }

  public void setAddedTaxBill(AddedTaxBill addedTaxBill) {
    this.addedTaxBill = addedTaxBill;
  }

  /**
   * 直接下单，购买数量
   */
  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public boolean isUseDeduction() {
    return useDeduction;
  }

  public void setUseDeduction(boolean useDeduction) {
    this.useDeduction = useDeduction;
  }

  public String getProLiteralId() {
    return proLiteralId;
  }

  public void setProLiteralId(String proLiteralId) {
    this.proLiteralId = proLiteralId;
  }

  public Map<String, String> getShopRemarks() {
    return shopRemarks;
  }

  public void setShopRemarks(Map<String, String> shopRemarks) {
    this.shopRemarks = shopRemarks;
  }

  public boolean isDirectBuy() {
    return this.qty > 0 ? true : false;
  }

  public boolean isCartBuy() {
    return CollectionUtils.isNotEmpty(this.skuIds);
  }

  public String getPromotionProductId() {
    return promotionProductId;
  }

  public void setPromotionProductId(String promotionProductId) {
    this.promotionProductId = promotionProductId;
  }

  public Boolean isNeedInvoice() {
    return needInvoice;
  }

  public void setNeedInvoice(Boolean needInvoice) {
    this.needInvoice = needInvoice;
  }

  public BillFormType getBillFormType() {
    return billFormType;
  }

  public List<String> getUsingCoupons() {
    if (CollectionUtils.isNotEmpty(usingCoupons)) {
      // 移除无效数据
      usingCoupons.removeAll(Collections.singleton(null));
      usingCoupons.removeAll(Collections.singleton(""));
    }
    return usingCoupons;
  }

  public void setUsingCoupons(List<String> usingCoupons) {
    this.usingCoupons = usingCoupons;
  }

  public BigDecimal getSelectPoint() {
    return selectPoint;
  }

  public void setSelectPoint(BigDecimal selectPoint) {
    this.selectPoint = selectPoint;
  }

  public BigDecimal getSelectCommission() {
    return selectCommission;
  }

  public void setSelectCommission(BigDecimal selectCommission) {
    this.selectCommission = selectCommission;
  }

  public Long getUpLine() {
    if (upLine == null || upLine == 0) {
      return null;
    }
    return upLine;
  }

  public void setUpLine(Long upLine) {
    this.upLine = upLine;
  }

  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }

  public List<String> getPromotionSkuIds() {
    return promotionSkuIds;
  }

  public void setPromotionSkuIds(List<String> promotionSkuIds) {
    this.promotionSkuIds = promotionSkuIds;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  /**
   * 传递的发票表单的类型
   */
  public enum BillFormType {
    PERSONAL,
    COMPANY,
    ADDED;
  }
}