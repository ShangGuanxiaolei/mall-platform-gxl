package com.xquark.openapi.agent;

import com.alibaba.fastjson.JSON;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.*;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.UserAgentInfoVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.homeItem.HomeItemForm;
import com.xquark.restapi.shop.ShopVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.RoleService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.ImgUtils;
import com.xquark.utils.ResourceResolver;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 代理用户controller Created by chh on 16-12-6.
 */
@Controller
@RequestMapping(value = "/openapi")
@Api(value = "agent", description = "代理管理", produces = MediaType.APPLICATION_JSON_VALUE)
@ApiIgnore
public class UserAgentControllerApi extends BaseController {

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private UserService userService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private TinyUrlService tinyUrlService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ExcelService excelService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private RoleService roleService;

  @Autowired
  private AuditRuleService auditRuleService;

  @Autowired
  private Qiniu qiniu;

  /**
   * 后台手工录入代理
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/apply4Admin", method = RequestMethod.POST)
  @ApiOperation(value = "代理申请", notes = "接收前台录入代理申请相关信息，自动创建代理用户\n" +
      "返回结果中'1'代表\"上级代理手机号不存在,请修改确认后重新提交!\"，'2'代表\"申请的代理手机号已经存在，请不要重复提交!\",'3'代表\"申请的代理微信号已经存在，请不要重复提交!\",'4'代表\"该微信已经申请过代理，请不要重复提交!\",'5'代表\"代理级别不存在，请联系系统管理员!\",'6'代表\"申请的代理级别不正确，请重新选择!\",'7'代表\"已经存在对应的代理用户!\",'ok'代表创建成功", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<String> apply4Admin(@ModelAttribute UserAgentForm userAgentForm,
      HttpServletRequest request) {
    String result = "0";
    String parentPhone = userAgentForm.getParentPhone();
    String phone = userAgentForm.getPhone();
    String weixin = userAgentForm.getWeixin();

    // 汇购网用户id
    String extUid = request.getParameter("extUid") == null ? request.getParameter("ExtUid")
        : request.getParameter("extUid");

    // 如果是微信分享出来的链接进行申请，则下面两个参数会有值，否则就是在app中进行的申请
    String parentId = userAgentForm.getParentId();
    AgentType agentType = userAgentForm.getAgentType();
    if (agentType != null) {
      userAgentForm.setType(agentType);
    }

    User parent = null;
    if (StringUtils.isNotEmpty(parentPhone)) {
      // 根据手机号查找上级userid
      parent = userService.loadAnonymousByPhone(parentPhone);
      // 查询该手机号的上级代理是否存在
      UserAgentVO parentAgent = userAgentService.selectByPhone(parentPhone);
      if (parentAgent == null) {
        result = "1";
      } else if (parentAgent.getStatus() == AgentStatus.FROZEN) {
        result = "11";
      }
      /**else{
       // 判断上级代理角色类型与申请的代理角色是否匹配
       AgentType parentType = parentAgent.getPromotionType();
       if( (parentType == AgentType.SECOND && userAgentForm.getPromotionType() == AgentType.GENERAL) ||
       (parentType == AgentType.FIRST && userAgentForm.getPromotionType() != AgentType.FIRST)){
       result = "6";
       }

       }**/
    }

    // 如果parentId不为空，则先判断这个汇购网的userid是否已经申请过代理用户
    if (StringUtils.isNotEmpty(parentId)) {
      parent = userService.loadExtUserByUid(parentId);
      UserAgentVO parentAgent = null;
      if (parent != null) {
        parentAgent = userAgentService.selectByUserId(parent.getId());
      }
      if (parentAgent == null) {
        result = "1";
      }
      /**else{
       // 判断上级代理角色类型与申请的代理角色是否匹配
       AgentType parentType = parentAgent.getPromotionType();
       if( (parentType == AgentType.SECOND && userAgentForm.getPromotionType() == AgentType.GENERAL) ||
       (parentType == AgentType.FIRST && userAgentForm.getPromotionType() != AgentType.FIRST)){
       result = "6";
       }

       }**/
    }

    // 查询是否有相同申请手机号的代理
    UserAgentVO agent = userAgentService.selectByPhone(phone);
    if (agent != null) {
      result = "2";
    }
    // 查询是否有相同微信号的代理
    agent = userAgentService.selectByWeixin(weixin);
    if (agent != null) {
      result = "3";
    }

    // 汇购网用户id是否已经存在代理用户
    if (StringUtils.isNotEmpty(extUid)) {
      User user = userService.loadExtUserByUid(extUid);
      if (user != null) {
        result = "7";
      }
    }

    if ("0".equals(result)) {

      result = userAgentService.addAgent4Admin(userAgentForm, parentId, parent, extUid);
    }

    return new ResponseObject<>(result);
  }

  private void pushMessage(Long msgId, String userId, String url, Integer type, String relatId,
      String data) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setData(data);
    MessageNotifyEvent event = new MessageNotifyEvent(message, userId, url, type, relatId);
    applicationContext.publishEvent(event);
  }


  /**
   * 查看某个具体的代理用户记录<br>
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "根据代理id查看代理用户明细信息", notes = "根据代理id返回代理详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<UserAgentVO> view(@PathVariable String id, HttpServletRequest req) {
    UserAgentVO userAgentVO = userAgentService.selectByPrimaryKey(id);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

    // 显示*号的身份证号
    String idcard = userAgentVO.getIdcard();
    Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
    Matcher m = p.matcher(idcard);
    String hiddenIdcard = m.replaceAll("$1********$3");
    userAgentVO.setHiddenIdcard(hiddenIdcard);

    return new ResponseObject<>(userAgentVO);
  }


  /**
   * 根据userid查看某个具体的代理用户记录<br>
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/userId/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "根据用户id查看代理用户明细信息", notes = "根据用户id返回代理详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<UserAgentVO> viewUserId(@PathVariable String id, HttpServletRequest req) {
    UserAgentVO userAgentVO = userAgentService.selectByUserId(id);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

    // 显示*号的身份证号
    String idcard = userAgentVO.getIdcard();
    Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
    Matcher m = p.matcher(idcard);
    String hiddenIdcard = m.replaceAll("$1********$3");
    userAgentVO.setHiddenIdcard(hiddenIdcard);

    return new ResponseObject<>(userAgentVO);
  }

  /**
   * 查询某个代理下级各个代理类型人数汇总
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getTotalByParentId", method = RequestMethod.POST)
  @ApiOperation(value = "根据用户id查询某个代理下级各个代理类型人数汇总", notes = "根据用户id查询某个代理下级各个代理类型人数汇总", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Map>> getTotalByParentId(
      @ApiParam(value = "代理用户id") @RequestParam(required = false) String parentId,
      HttpServletRequest req) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<Map> maps = userAgentService.getTotalByParentId(parentId);

    return new ResponseObject<>(maps);
  }


  /**
   * 分页查询某个代理下级某种代理类型所有的明细人员信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/listByParentAndType", method = RequestMethod.POST)
  @ApiOperation(value = "分页查询某个代理下级某种代理类型所有的明细人员信息", notes =
      "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
          "parentId传递代理用户id,type传递代理类型(DIRECTOR,GENERAL,FIRST,SECOND)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UserAgentVO>> listByParentAndType(Pageable pageable,
      @ApiParam(value = "代理用户id") @RequestParam(required = false) String parentId,
      @ApiParam(value = "代理类型") @RequestParam String type, HttpServletRequest req) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<UserAgentVO> userAgentVOs = userAgentService.listByParentAndType(pageable, parentId, type);
    UserAgentVO parentAgent = userAgentService.selectByUserId(parentId);
    AgentType agentType = parentAgent.getType();
    // 查询代理明细是否能显示下级及当月佣金
    for (UserAgentVO vo : userAgentVOs) {
      long count = userAgentService.selectCntByParentId(vo.getUserId());
      vo.setIsLeaf(count > 0 ? false : true);
      vo.setCommissionFee(commissionService.getFeeByMonth(vo.getUserId()));
      // 获取用户详细地址信息
      String addressId = vo.getAddress();
      Address address = addressService.load(addressId);
      String details = "";
      if (address != null) {
        List<Zone> zoneList = zoneService.listParents(address.getZoneId());
        for (Zone zone : zoneList) {
          details += zone.getName();
        }
        details += address.getStreet();
      }
      vo.setAddressStreet(details);
    }

    return new ResponseObject<>(userAgentVOs);
  }

  /**
   * 分页查询某个代理所有待审核的代理申请人信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/listApplyingByParent", method = RequestMethod.POST)
  @ApiOperation(value = "分页查询某个代理所有待审核的代理申请人信息", notes =
      "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
          "parentId传递代理用户id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UserAgentVO>> listApplyingByParent(Pageable pageable,
      @ApiParam(value = "代理用户id") @RequestParam(required = false) String parentId,
      HttpServletRequest request) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<UserAgentVO> userAgentVOs = userAgentService.listApplyingByParent(pageable, parentId);

    for (UserAgentVO vo : userAgentVOs) {
      // 获取用户详细地址信息
      String addressId = vo.getAddress();
      Address address = addressService.load(addressId);
      String details = "";
      if (address != null) {
        List<Zone> zoneList = zoneService.listParents(address.getZoneId());
        for (Zone zone : zoneList) {
          details += zone.getName();
        }
        details += address.getStreet();
      }
      vo.setAddressStreet(details);

      // 默认代理审核由平台审核
      vo.setAuditType(AuditType.PLATFORM);
      AgentType type = vo.getType();
      String parentUserId = vo.getParentUserId();
      UserAgentVO parentAgentVO = userAgentService.selectByUserId(parentUserId);
      // 根据代理审核规则配置的规则，判断该代理是由平台还是上级进行审核
      if (parentAgentVO != null) {
        AuditRule auditRule = auditRuleService.selectByAgentType(type, parentAgentVO.getType());
        if (auditRule != null) {
          vo.setAuditType(auditRule.getAuditType());
        }
      }
    }

    return new ResponseObject<>(userAgentVOs);
  }

  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/countApplyingByParent", method = RequestMethod.POST)
  @ApiOperation(value = "查询某个代理所有待审核的代理申请人总数", notes = "parentId传递代理用户id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Long> countApplyingByParent(Pageable pageable,
      @ApiParam(value = "代理用户id") @RequestParam(required = false) String parentId,
      HttpServletRequest request) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    long count = userAgentService.countApplyingByParent(parentId);
    return new ResponseObject<>(count);
  }

  /**
   * 审核通过后发送自定义消息给小程序的申请代理人
   */
  private String getXcxData(UserAgentVO userAgent, long type, boolean isAudit) {
    String data = "";
    HashMap map = new HashMap();
    String parentId = userAgent.getParentUserId();
    User parent = userService.load(parentId);
    Shop shop = shopService.findByUser(userAgent.getUserId());
    Shop parentShop = shopService.findByUser(parentId);
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    // 发送相关json数据给小程序
    map.put("fromAccount", parent.getPhone());
    map.put("fromAccountName", parent.getName());
    map.put("fromAccountAvatar", resourceFacade.resolveUrl(parentShop.getImg()));
    map.put("toAccount", userAgent.getPhone());
    map.put("toAccountName", userAgent.getName());
    map.put("toAccountAvatar", resourceFacade.resolveUrl(shop.getImg()));
    map.put("id", userAgent.getId());
    map.put("cDate", (new Date()).getTime());
    map.put("type", type);
    map.put("isAudit", isAudit);
    data = JSON.toJSONString(map);
    return data;
  }

  /**
   * 审核代理申请人
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/audit", method = RequestMethod.POST)
  @ApiOperation(value = "审核通过某条代理申请", notes = "id传递代理id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> audit(@ApiParam(value = "代理id") @RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.ACTIVE);
    int result = userAgentService.modify(agent);

    UserAgentVO vo = userAgentService.selectByPrimaryKey(id);
    String userId = vo.getUserId();

    // 推送消息给代理人，提醒申请通过
    try {
      // json格式的数据字符串
      String data = getXcxData(vo, PushMsgType.MSG_AGENT_AUDIT.getValue(), true);
      pushMessage(PushMsgId.Agent_Audit.getId(), userId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_AUDIT.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给代理人，提醒申请通过 error " + e.toString());
    }

    return new ResponseObject<>(result > 0);
  }

  /**
   * 解冻账户
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/unFrozen", method = RequestMethod.POST)
  @ApiOperation(value = "解冻通过某条代理数据", notes = "id传递代理id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> unFrozen(@ApiParam(value = "代理id") @RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.ACTIVE);
    int result = userAgentService.modify(agent);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 解冻账户
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/frozen", method = RequestMethod.POST)
  @ApiOperation(value = "冻结通过某条代理数据", notes = "id传递代理id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> frozen(@ApiParam(value = "代理id") @RequestParam String id) {
    UserAgent agent = new UserAgent();
    agent.setId(id);
    agent.setStatus(AgentStatus.FROZEN);
    int result = userAgentService.modify(agent);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 拒绝代理申请人
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/reject", method = RequestMethod.POST)
  @ApiOperation(value = "拒绝某条代理申请", notes = "id传递代理id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> reject(@ApiParam(value = "代理id") @RequestParam String id) {
    UserAgentVO vo = userAgentService.selectByPrimaryKey(id);
    String userId = vo.getUserId();
    // 推送消息给代理人，提醒申请拒绝
    try {
      // json格式的数据字符串
      String data = getXcxData(vo, PushMsgType.MSG_AGENT_AUDIT.getValue(), false);
      pushMessage(PushMsgId.Agent_Audit.getId(), userId, Long.toString(new Date().getTime()),
          PushMsgType.MSG_AGENT_AUDIT.getValue(), null, data);
    } catch (Exception e) {
      log.error("推送消息给代理人，提醒申请拒绝 error " + e.toString());
    }
    int result = userAgentService.delete(id);
    return new ResponseObject<>(result > 0);
  }

  /**
   * 获取分享经销商链接
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getShareUrl", method = RequestMethod.POST)
  @ApiOperation(value = "获取需要分享的代理申请链接地址", notes = "通过这个地址，其他用户能申请成为下级代理", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> getShareUrl(HttpServletRequest req) {
    HashMap map = new HashMap();
    String parentId = this.getCurrentIUser().getId();
    //map.put("general", getUrl(req, "GENERAL", parentId));
    //map.put("first", getUrl(req, "FIRST", parentId));
    //map.put("second", getUrl(req, "SECOND", parentId));
    map.put("url", getUrl(req, parentId));
    return new ResponseObject<Map>(map);
  }

  /**
   * 保存代理用户的图片,把imageForm转化成model
   */
  private void initImage(HomeItemForm form) {
    List<TweetImage> imgs = new ArrayList<>();
    TweetImage pimg = null;
    int idx = 0;   // 从0开始
    if (null != form.getImgs() && !"".equals(form.getImgs())) {
      String[] fimgs = form.getImgs().split(",");
      for (String img : fimgs) {
        form.setImg(img);
      }
    }
  }

  private String getUrl(HttpServletRequest req, String parentId) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/userAgent/apply" + "?parentId="
            + parentId);
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 查找用户的相关twitter树结构信息
   */
  private UserAgentInfoVO getUserAgentInfoVO(String shopId, String rootShopId) {
    UserAgentInfoVO userAgentInfoVO = new UserAgentInfoVO();

    Shop shop = shopService.load(shopId);
    String desUserId = shopService.load(shopId).getOwnerId();
    User desUser = userService.load(desUserId);
    // 这个下级是否还有下级，以及还有多少个下级人员
    long count = shopTreeService.countChildrenAgent(rootShopId, shopId);
    userAgentInfoVO.setCount(count);
    if (count > 0) {
      userAgentInfoVO.setIsLeaf(false);
    } else {
      userAgentInfoVO.setIsLeaf(true);
    }
    userAgentInfoVO.setId(desUserId);
    userAgentInfoVO.setName(desUser.getName());
    // 树状图代理头像显示对应店铺的头像
    if (StringUtils.isNotEmpty(shop.getImg())) {
      userAgentInfoVO.setAvatar(shop.getImg());
    } else {
      userAgentInfoVO.setAvatar(desUser.getAvatar());
    }
    UserAgentVO userAgentVO = userAgentService.selectByUserId(desUserId);
    if (userAgentVO != null) {
      userAgentInfoVO.setRole(userAgentVO.getType().toString());
      userAgentInfoVO.setStatus(userAgentVO.getStatus().toString());
      if (userAgentInfoVO.getRate() == null) {
        userAgentInfoVO.setRate(new BigDecimal("10"));
      }
      if (userAgentInfoVO.getIncome() == null) {
        userAgentInfoVO.setIncome(new BigDecimal(0.00));
      }
    }
    return userAgentInfoVO;
  }

  /**
   * 根据手机号或微信号查询是否存在
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/selectByPhoneOrWeixin", method = RequestMethod.POST)
  @ApiOperation(value = "根据手机号或微信号查询代理", notes = "根据手机号或微信号查询代理", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<UserAgentVO> selectByPhoneOrWeixin(
      @ApiParam(value = "手机号或微信号") @RequestParam String keys) {
    UserAgentVO vo = userAgentService.selectByPhoneOrWeixin(keys);
    if (vo != null) {
      vo.setAgentTypeName(getAgentTypeName(vo.getType()));
    }
    return new ResponseObject<>(vo);
  }

  private String getAgentTypeName(AgentType type) {
    String agentTypeName = "";
    if (type == AgentType.FOUNDER) {
      agentTypeName = "联合创始人";
    } else if (type == AgentType.DIRECTOR) {
      agentTypeName = "董事";
    } else if (type == AgentType.GENERAL) {
      agentTypeName = "总顾问";
    } else if (type == AgentType.FIRST) {
      agentTypeName = "一星顾问";
    } else if (type == AgentType.SECOND) {
      agentTypeName = "二星顾问";
    } else if (type == AgentType.SPECIAL) {
      agentTypeName = "特约";
    }
    return agentTypeName;
  }

  private String getEndDate(Date start) {
    // 授权截止时间为申请日期的当前最后一天
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年");
    return formatter.format(start) + "12月31日";
  }

  /**
   * 生成代理申请分享二维码
   */
  @RequestMapping(value = "/userAgent/qrcode", method = RequestMethod.GET, produces = "image/png")
  @ApiOperation(value = "生成代理申请分享二维码", notes = "返回二维码图片的二进制流", httpMethod = "GET", produces = MediaType.IMAGE_PNG_VALUE)
  public @ResponseBody
  byte[] generateQrcode(HttpServletRequest req) {

    String parentId = this.getCurrentIUser().getId();
    String shareUrl = getUrl(req, parentId);
    int width = 800;
    int height = 800;
    BufferedImage ImageQrcode = null;
    try {
      HashMap<EncodeHintType, Object> hints = new HashMap<>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);

      BitMatrix byteMatrix;
      byteMatrix = new MultiFormatWriter()
          .encode(shareUrl, BarcodeFormat.QR_CODE, width, height, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (WriterException e) {
      log.error("generateQrcode error", e);
      throw new RuntimeException(e);
    }

    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {

      Font font = new Font("宋体", Font.BOLD, 20);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);
      g2.drawImage(ImageQrcode, 0, 0, null);
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。

  }

  /**
   * 获取代理用户角色信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getRoles", method = RequestMethod.POST)
  @ApiOperation(value = "获取代理用户角色信息", notes = "获取代理用户角色信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Role>> getRoles() {
    List<Role> roles = roleService.listByBelong(RoleBelong.B2B.toString());
    return new ResponseObject<>(roles);
  }

  public static void main(String[] args) {
    // 授权证书在qiliu上的地址
    String certImg = "qn|xaya|FvVEOIpMrCNs0doK4q5jBMA01BLI";
    // 生成经销商授权证书图片
    int width = 1063;
    int height = 1505;

//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
    String imgUrl = "http://images.handeson.com/FiyRKQtRk_de4cfZqgiVAMFluxpP";
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {

      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      Font font = new Font("宋体", Font.BOLD, 26);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);
      g2.drawString("曹洪辉", 355, 592);
      g2.drawString("420112198507251518", 355, 635);
      g2.drawString("fb_chh", 355, 678);

      g2.drawString("2016年11月07日", 380, 1040);
      g2.drawString("2017年11月07日", 620, 1040);
      g2.drawString("420125522", 380, 1080);

      g2.setFont(new Font("宋体", Font.BOLD, 35));
      g2.drawString("大区", 320, 785);

      File file = new File("/home/caohonghui/1.png");

      ImageIO.write(bi, "png", file);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 得到所有代理用户信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getAllUsers", method = RequestMethod.POST)
  @ApiOperation(value = "得到所有代理用户信息", notes = "得到所有代理用户信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UserAgentVO>> getAllUsers(HttpServletRequest request) {
    List<UserAgentVO> userAgentVOs = userAgentService.getAllUsers();
    return new ResponseObject<>(userAgentVOs);
  }

  /**
   * 查询手机号对应用户的所有上级和所有直接下级代理
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getAllFriends", method = RequestMethod.POST)
  @ApiOperation(value = "查询手机号对应用户的所有直接上级和所有直接下级代理", notes = "查询手机号对应用户的所有上级和所有直接下级代理", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<String>> getAllFriends(
      @ApiParam(value = "手机号") @RequestParam String phone, HttpServletRequest request) {
    List<String> phones = userAgentService.getAllFriends(phone);
    return new ResponseObject<>(phones);
  }

  /**
   * 得到所有代理用户信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/isParent", method = RequestMethod.POST)
  @ApiOperation(value = "判断两个手机号是否是上下级关系", notes = "判断两个手机号是否是上下级关系", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> isParent(HttpServletRequest request,
      @ApiParam(value = "上级手机号") @RequestParam String parentPhone,
      @ApiParam(value = "手机号") @RequestParam String phone) {
    UserAgentVO userAgentVO = userAgentService.selectByPhoneAndParentPhone(parentPhone, phone);
    return new ResponseObject<>(userAgentVO != null);
  }


  /**
   * 分页查询某个代理下级团队成员信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/listMembers", method = RequestMethod.POST)
  @ApiOperation(value = "分页查询某个代理下级团队成员信息", notes =
      "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
          "parentId传递代理用户id", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Map>> listMembers(Pageable pageable,
      @ApiParam(value = "代理用户id") @RequestParam(required = false) String parentId,
      HttpServletRequest req) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    List<UserAgentVO> userAgentVOs = userAgentService.listMembers(pageable, parentId);
    ArrayList result = new ArrayList();
    for (UserAgentVO userAgentVO : userAgentVOs) {
      HashMap map = new HashMap();
      map.put("userAgentVO", userAgentVO);
      User user = userService.load(userAgentVO.getUserId());
      Shop shop = shopService.load(user.getShopId());
      ShopVO shopVO = new ShopVO(shop);
      if (StringUtils.isNotBlank(shopVO.getImg())) {
        shopVO.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(shopVO.getBanner())) {
        shopVO.setBannerUrl(shop.getBanner());
      }
      map.put("shopVO", shopVO);
      ArrayList<Map> sales = (ArrayList<Map>) userAgentService
          .getMemberSales(userAgentVO.getUserId());
      ArrayList<Map> agents = (ArrayList<Map>) userAgentService
          .getMemberAgents(userAgentVO.getUserId());
      map.put("sales", sales);
      map.put("agents", agents);
      result.add(map);
    }
    return new ResponseObject<List<Map>>(result);
  }

  /**
   * 更新代理用户信息
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/update", method = RequestMethod.POST)
  public ResponseObject<Boolean> update(UserAgentForm userAgentForm) {
    UserAgent userAgent = new UserAgent();
    BeanUtils.copyProperties(userAgentForm, userAgent);
    int result = userAgentService.modify(userAgent);
    if (result == 1) {
      return new ResponseObject<>(true);
    } else {
      return new ResponseObject<>(false);
    }
  }

  /**
   * 获取代理的合同图片
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getContractImg", method = RequestMethod.POST)
  public ResponseObject<ArrayList> getContractImg() {
    ArrayList list = new ArrayList();
    String userId = getCurrentIUser().getId();
    UserType type = userService.getUserType(userId);
    // 已经申请通过的代理才能查看合同信息
    if (type == null || type != UserType.B2B) {
      return new ResponseObject<>(list);
    }
    String imgarr = userAgentService.getContractImg(userId);
    // 如果当前代理没有合同图片，则即时合成合同图片
    if (StringUtils.isEmpty(imgarr)) {
      genContractImg(userId);
      imgarr = userAgentService.getContractImg(userId);
    }
    String[] imgs = imgarr.split(",");
    for (String img : imgs) {
      list.add(img.trim());
    }
    return new ResponseObject<>(list);
  }

  /**
   * 生成代理的合同图片
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/genContractImg", method = RequestMethod.POST)
  public void genContractImgApi() {
    String userId = getCurrentIUser().getId();
    genContractImg(userId);
  }

  /**
   * 合成代理图片
   */
  private void genContractImg(String userId) {
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {
      return;
    }
    // 合同共有7页，对应qiniu服务器上的7张图片
    String contract_001 = "http://images.handeson.com/Fo4l_XRfLYVJdNY8rmb3hkf3Ggpk";
    String contract_002 = "http://images.handeson.com/FkSBrlbXabdrTWvt1b-c37et6bEs";
    String contract_003 = "http://images.handeson.com/Fihd9rJxGiF0Bynz8iQSrP_5qV9w";
    String contract_004 = "http://images.handeson.com/Fht-YgMJQsgu4P4sQgPGTvoug9pi";
    String contract_005 = "http://images.handeson.com/FpcCbvBS71UsqplIn2yNFZ9BV1W6";
    String contract_006 = "http://images.handeson.com/Fuc7TJ4VbgHVDitV8Wz2OFm-Ydas";
    String contract_007 = "http://images.handeson.com/FjY1DkdfkwZ25SoWvan_juEamPJA";
    ArrayList contractImgs = new ArrayList();
    // 第一张与第七张图片需要将用户信息合成一张新的图片
    int width = 1240;
    int height = 1753;

    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    ByteArrayOutputStream os7 = new ByteArrayOutputStream();//新建流。
    try {

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      // 第一张图片合成
      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(contract_001);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      Font font = new Font("宋体", Font.BOLD, 27);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = Color.BLACK;
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);
      g2.drawString("HT" + IdTypeHandler.decode(userAgentVO.getId()), 800, 150);
      g2.drawString(userAgentVO.getName(), 270, 688);
      g2.drawString(userAgentVO.getIdcard(), 330, 732);
      g2.drawString(userAgentVO.getWeixin(), 300, 773);
      g2.drawString(userAgentVO.getPhone(), 300, 814);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
      InputStream is1 = new ByteArrayInputStream(os.toByteArray());
      java.util.List<InputStream> ins = new ArrayList<>();
      ins.add(is1);

      // 第七张图片合成
      imageImg = ImgUtils.getImageFromNetByUrl(contract_007);
      //BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      imageImgRect = ImgUtils.drawRect(imageImg);
      width = imageImg.getWidth();
      height = imageImg.getHeight();

      bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);
      // 甲方信息
      g2.drawString("武汉巴巴贝尔信息科技有限公司", 260, 858);
      g2.drawString("武汉市硚口区同心健康产业园A座1303", 260, 900);
      g2.drawString("15827268585", 260, 942);
      g2.drawString("朱玲", 300, 982);
      g2.drawString(sdf.format(userAgentVO.getCreatedAt()), 260, 1025);

      // 乙方信息
      g2.drawString(userAgentVO.getName(), 815, 858);
      g2.drawString(userAgentVO.getIdcard(), 863, 900);
      g2.drawString(userAgentVO.getPhone(), 815, 942);
      g2.drawString(userAgentVO.getWeixin(), 835, 982);
      g2.drawString(sdf.format(userAgentVO.getCreatedAt()), 815, 1025);

      ImageIO.write(bi, "png", os7);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
      InputStream is7 = new ByteArrayInputStream(os7.toByteArray());
      ins.add(is7);

      // 将合成的图片上传到qiniu服务器
      final java.util.List<UpLoadFileVO> vos = qiniu.uploadImgStream(ins, FileBelong.PRODUCT);
      String contract_001_new = qiniu.genQiniuFileUrl(vos.get(0).getKey());
      String contract_007_new = qiniu.genQiniuFileUrl(vos.get(1).getKey());
      contractImgs.add(contract_001_new);
      contractImgs.add(contract_002);
      contractImgs.add(contract_003);
      contractImgs.add(contract_004);
      contractImgs.add(contract_005);
      contractImgs.add(contract_006);
      contractImgs.add(contract_007_new);

      //contractImgs.add(imgUrl);
      // 更新代理的合同图片字段
      UserAgent updateUserAgent = new UserAgent();
      updateUserAgent.setId(userAgentVO.getId());
      updateUserAgent.setContractImg(StringUtils.strip(contractImgs.toString(), "[]"));
      userAgentService.modify(updateUserAgent);

    } catch (Exception e) {
      log.error("生成代理合同图片报错", e);
    }
  }

  /**
   * 批量得到代理的状态与审核类型
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getStatusAndAuditType", method = RequestMethod.POST)
  @ApiOperation(value = "批量得到代理的状态与审核类型", notes = "批量得到代理的状态与审核类型", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UserAgentVO>> getStatusAndAuditType(Pageable pageable,
      @ApiParam(value = "代理用户ids") @RequestParam(required = false) List<String> ids,
      HttpServletRequest req) {
    ArrayList result = new ArrayList();
    for (String id1 : ids) {
      String id = (String) id1;
      UserAgentVO vo = userAgentService.selectByPrimaryKey(id);
      // 默认代理审核由平台审核
      vo.setAuditType(AuditType.PLATFORM);
      AgentType type = vo.getType();
      String parentUserId = vo.getParentUserId();
      UserAgentVO parentAgentVO = userAgentService.selectByUserId(parentUserId);
      // 根据代理审核规则配置的规则，判断该代理是由平台还是上级进行审核
      if (parentAgentVO != null) {
        AuditRule auditRule = auditRuleService.selectByAgentType(type, parentAgentVO.getType());
        if (auditRule != null) {
          vo.setAuditType(auditRule.getAuditType());
        }
      }
      result.add(vo);
    }
    return new ResponseObject<List<UserAgentVO>>(result);
  }

}
