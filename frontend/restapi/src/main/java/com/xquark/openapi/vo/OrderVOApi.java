package com.xquark.openapi.vo;

import com.xquark.dal.model.User;
import com.xquark.dal.vo.MainOrderVO;
import java.math.BigDecimal;

/**
 * 汇购网订单提交后返回详情相关的vo Created by chh on 17-2-16.
 */
public class OrderVOApi {

  private MainOrderVO mainOrderVO;
  private User buyer;
  private BigDecimal goodsFee;
  private BigDecimal totalFee;
  private BigDecimal logisticsFee;

  public MainOrderVO getMainOrderVO() {
    return mainOrderVO;
  }

  public void setMainOrderVO(MainOrderVO mainOrderVO) {
    this.mainOrderVO = mainOrderVO;
  }

  public User getBuyer() {
    return buyer;
  }

  public void setBuyer(User buyer) {
    this.buyer = buyer;
  }

  public BigDecimal getGoodsFee() {
    return goodsFee;
  }

  public void setGoodsFee(BigDecimal goodsFee) {
    this.goodsFee = goodsFee;
  }

  public BigDecimal getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(BigDecimal totalFee) {
    this.totalFee = totalFee;
  }

  public BigDecimal getLogisticsFee() {
    return logisticsFee;
  }

  public void setLogisticsFee(BigDecimal logisticsFee) {
    this.logisticsFee = logisticsFee;
  }
}
