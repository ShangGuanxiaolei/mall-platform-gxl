package com.xquark.openapi.polyapi;

/**
 * @auther liuwei
 * @date 2018/6/26 16:00
 */
import com.xquark.openapi.scrm.PolyControllerApi;
import com.xquark.utils.MD5Util;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther liuwei
 * @date 2018/6/18 14:08
 */
@RestController
@RequestMapping(value = "/openapi")
public class PolyapiBaseController extends HttpServlet {

  private final static Logger LOGGER = LoggerFactory.getLogger(PolyControllerApi.class);

  @RequestMapping(value = "/polyapi/auth",
      consumes="application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  public String PolySign(HttpServletRequest request,HttpServletResponse response) throws Exception {
    LOGGER.info("========== 开始验证菠萝派 ===========");
    String appkey = request.getParameter("appkey");
    String bizcontent = request.getParameter("bizcontent");
    String method = request.getParameter("method");
    String token = request.getParameter("token");
    String sign = request.getParameter("sign");
    String AppSecret = "f6f01b906f5b4167bfc630c338534d27"; //appSecret的值通过在菠萝派中申请应用获得
    String SignatureName = AppSecret + "appkey" + appkey + "bizcontent" + bizcontent +
        "method" + method + "token" + token + "" + AppSecret;
    String newsign = MD5Util.md5(SignatureName.toLowerCase());//转化成小写后获取MD5值
    System.out.println(newsign);
    LOGGER.info("========== 生成令牌 ===========");
    if (appkey.equals("")) {   //Appkey不能为空
      return "{\"code\": \"400001\", \"message\": \"System Error\",\"subcode\": "
          + "\"GSE.SYSTEM_ERROR\",\"submessage\": \"[20881]应用口令不能为空\"}";
    } else if (!sign.equals(newsign)) {  //验证失败
      LOGGER.info("========== 验证失败 ===========");
      return "{\"code\": \"400002\", \"message\": \"System Error\",\"subcode\": "
          + "\"GSE.SYSTEM_ERROR\",\"submessage\": \"[20882]签名验证失败\"}";
    } else {   //验证成功
      if (method.equals("Differ.JH.Business.GetOrder")) {
        LOGGER.info("========== 转发到订单同步 ==========");
        request.getRequestDispatcher("/openapi/polyapi/order")
            .forward(request, response);
        return null;
      }else if (method.equals("Differ.JH.Business.Send")) {
        LOGGER.info("========== 转发到订单发送 ==========");
        request.getRequestDispatcher("/openapi/polyapi/ordersend")
            .forward(request, response);
        return null;
      }else if (method.equals("Differ.JH.Business.DownloadProduct")) {
        LOGGER.info("========== 转发到商品下载 ==========");
        request.getRequestDispatcher("/openapi/polyapi/download/product")
            .forward(request, response);
        return null;
      }else if (method.equals("Differ.JH.Business.SyncStock")) {
        LOGGER.info("========== 转发到商品库存同步 ==========");
        request.getRequestDispatcher("/openapi/polyapi/sync/stock")
            .forward(request, response);
        return null;
      }
      LOGGER.info("========== 验证成功 ===========");
      return "{\"code\": \"10000\", \"message\": \"SUCCESS\",\"subcode\": "
          + "\"SUCCESS\",\"submessage\": \"签名验证成功\"}";
    }
  }
}

