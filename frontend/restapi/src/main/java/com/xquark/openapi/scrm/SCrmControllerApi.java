package com.xquark.openapi.scrm;

import com.xquark.dal.anno.SCrmAuth;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.ProductUser;
import com.xquark.dal.model.User;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.address.AddressService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.user.UserService;
import com.xquark.service.user.impl.SCrmThirdUserConverter;
import com.xquark.wechat.oauth.SCrmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangxinhua on 18-2-27. DESC: 与SCRM系统对接控制器 该类中大部分方法的校验都放在controllerAOP中处理
 *
 * @see com.xquark.aop.ControllerAop#sCrmAuthCheck(BaseAuthForm, Errors)
 */
@RestController
@RequestMapping("/openapi/scrm")
@SuppressWarnings("unused")
public class SCrmControllerApi {

  private UserService userService;

  private AddressService addressService;

  private ProductService productService;

  private SCrmThirdUserConverter sCrmConverter;

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setsCrmConverter(SCrmThirdUserConverter sCrmConverter) {
    this.sCrmConverter = sCrmConverter;
  }

  @Autowired
  public void setAddressService(AddressService addressService) {
    this.addressService = addressService;
  }

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }

  /**
   * 同步SCRM平台用户创建
   */
  @SCrmAuth
  @RequestMapping(value = "/user/sync", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> createUser(@RequestBody @Validated SCrmUserForm userForm,
      Errors errors) {
    SCrmUser sCrmUser = Transformer.fromBean(userForm, SCrmUser.class);
    userService.createUserFromThirdUserIfNotExists(sCrmConverter, sCrmUser, null,null);
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 同步SCRM平台地址
   */
  @SCrmAuth
  @RequestMapping(value = "/address/sync", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<String> createAddress(
      @RequestBody @Validated SCrmAddressForm addressForm, Errors errors) {
    String unionId = addressForm.getUnionId();
    User user = loadUser(unionId);
    Address address = new Address();
    address.setConsignee(addressForm.getConsignee());
    address.setZoneId(addressForm.getZoneId());
    address.setStreet(addressForm.getStreet());
    address.setPhone(addressForm.getPhone());
    address.setCommon(false);
    address.setIsDefault(addressForm.getIsDefault());
    address.setUserId(user.getId());
    address.setExtId(addressForm.getId());
    address.setId(addressForm.getaId());

    address = addressService.saveUserAddress(address, false);
    return new ResponseObject<>(address.getId());
  }

  /**
   * 删除对应地址
   */
  @SCrmAuth
  @RequestMapping(value = "/address/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> deleteAddress(@RequestBody @Validated SCrmAddressIdForm form,
      Errors errors) {
    String addressId = form.getaId();
    int result = addressService.archiveAddress(addressId, true);
    if (result > 0) {
      return new ResponseObject<>(Boolean.TRUE);
    }
    return new ResponseObject<>("地址删除失败", GlobalErrorCode.SCRM_USER_SYNC_FAIL);
  }

  /**
   * 设置默认地址
   */
  @SCrmAuth
  @RequestMapping(value = "/address/asDefault", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> setDefaultAddress(
      @RequestBody @Validated SCrmAddressIdForm form,
      Errors errors) {
    String addressId = form.getaId();
    boolean result = addressService.asDefaultByAddressId(addressId);
    return new ResponseObject<>(result);
  }

  /**
   * 绑定商品
   */
  @SCrmAuth
  @RequestMapping(value = "/product/bind", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> bindProduct(@RequestBody @Validated SCrmProductBindForm form,
      Errors errors) {
    ProductUser productUser = createProductUserFromForm(form);
    boolean result = productService.bindUserIfNotBounded(productUser);
    return new ResponseObject<>(result);
  }

  /**
   * 解除绑定商品
   */
  @SCrmAuth
  @RequestMapping(value = "/product/unbind", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> unbindProduct(@RequestBody @Validated SCrmProductBindForm form,
      Errors errors) {
    ProductUser productUser = createProductUserFromForm(form);
    boolean result = productService.unBindUserIfBounded(productUser);
    return new ResponseObject<>(result);
  }

  /**
   * 根据unionId查找用户, 若不存在则抛出异常
   */
  private User loadUser(String unionId) {
    User user = userService.loadByUnionId(unionId);
    if (user == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "unionId对应用户不存在");
    }
    return user;
  }

  /**
   * 从表单中提取出用户与商品信息
   */
  private ProductUser createProductUserFromForm(SCrmProductBindForm form) {
    String unionId = form.getUnionId();
    User user = loadUser(unionId);
    String userId = user.getId();
    String productId = productService.selectIdByEncode(form.getProductCode());
    return new ProductUser(productId, userId);
  }

}
