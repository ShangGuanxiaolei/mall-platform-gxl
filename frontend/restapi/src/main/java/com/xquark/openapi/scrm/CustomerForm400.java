package com.xquark.openapi.scrm;

import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author wangxinhua on 2018/7/20. DESC:
 */
public class CustomerForm400 extends BaseAuthForm {

  @NotBlank(message = "手机号不能为空")
  @Pattern(regexp = "(13\\d|14[57]|15[^4,\\D]|17[678]|18\\d)\\d{8}|170[059]\\d{7}", message = "手机号码格式错误")
  private String phone;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
