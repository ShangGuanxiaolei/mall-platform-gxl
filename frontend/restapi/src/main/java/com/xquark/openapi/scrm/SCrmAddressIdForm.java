package com.xquark.openapi.scrm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-2-28. DESC:
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SCrmAddressIdForm extends BaseAuthForm {

  @NotBlank(message = "地址id不能为空")
  private String aId;

  public String getaId() {
    return aId;
  }

  public void setaId(String aId) {
    this.aId = aId;
  }
}
