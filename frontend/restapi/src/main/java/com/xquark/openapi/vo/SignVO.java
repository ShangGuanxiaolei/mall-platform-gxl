package com.xquark.openapi.vo;

/**
 * Created by root on 17-1-23.
 */
public class SignVO {

  //当前调用方的域名
  private String Host;
  private String ExtUid;
  private String IP;
  private String Sign;

  public String getHost() {
    return Host;
  }

  public void setHost(String host) {
    Host = host;
  }

  public String getExtUid() {
    return ExtUid;
  }

  public void setExtUid(String extUid) {
    ExtUid = extUid;
  }

  public String getIP() {
    return IP;
  }

  public void setIP(String IP) {
    this.IP = IP;
  }

  public String getSign() {
    return Sign;
  }

  public void setSign(String sign) {
    Sign = sign;
  }
}
