package com.xquark.openapi.point;

import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangxinhua on 2018/5/22. DESC:
 */
public class UserPointForm {

  private String orderId;

  @NotNull(message = "平台不能为空")
  private Integer source;

  private BigDecimal points;

  private BigDecimal commission;

  private Date freezeTime;

  private Trancd trancd;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public Date getFreezeTime() {
    return freezeTime;
  }

  public void setFreezeTime(Date freezeTime) {
    this.freezeTime = freezeTime;
  }

  public PlatformType getPlatform() {
    PlatformType platform;
    try {
      platform = PlatformType.fromCode(source);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "平台代码不正确");
    }
    return platform;
  }

  public Integer getSource() {
    return source;
  }

  public void setSource(Integer source) {
    this.source = source;
  }

  public BigDecimal getPoints() {
    return points;
  }

  public void setPoints(BigDecimal points) {
    this.points = points;
  }

  public BigDecimal getCommission() {
    return commission;
  }

  public void setCommission(BigDecimal commission) {
    this.commission = commission;
  }

  public Trancd getTrancd() {
    return trancd;
  }

  public void setTrancd(Trancd trancd) {
    this.trancd = trancd;
  }
}
