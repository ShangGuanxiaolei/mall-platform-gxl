package com.xquark.openapi.scrm;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-2-11. DESC: 用户对接
 */
public class SCrmUserForm extends BaseAuthForm {

  @NotBlank(message = "unionId不能为空")
  private String unionId;

  @NotBlank(message = "手机号不能为空")
  @Size(max = 13, message = "手机号长度不正确")
  private String mobile;

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getPhone() {
    return getMobile();
  }

}
