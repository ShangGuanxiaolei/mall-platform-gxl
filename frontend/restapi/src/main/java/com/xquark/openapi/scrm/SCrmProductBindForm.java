package com.xquark.openapi.scrm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by wangxinhua on 18-3-2. DESC: SCRM商品绑定表单
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SCrmProductBindForm extends BaseAuthForm {

  private String productCode;

  private String unionId;

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

}
