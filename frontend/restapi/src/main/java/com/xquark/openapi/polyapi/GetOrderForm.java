package com.xquark.openapi.polyapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

/**
 * User: kong Date: 2018/6/17. Time: 0:12 菠萝派订单请求参数
 */
public class GetOrderForm {
  //订单交易状态
  @JsonProperty("OrderStatus")
  private String OrderStatus;
  //平台订单号
  @JsonProperty("PlatOrderNo")
  private String PlatOrderNo;
  //开始时间
  @JsonProperty("StartTime")
  private Date StartTime;
  //截止时间
  @JsonProperty("EndTime")
  private Date EndTime;
  //订单时间类别
  @JsonProperty("TimeType")
  private String TimeType;
  //页码
  @JsonProperty("PageIndex")
  private Integer PageIndex;
  //每页条数
  @JsonProperty("PageSize")
  private Integer PageSize;

  public String getOrderStatus() {
    return OrderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    OrderStatus = orderStatus;
  }

  public String getPlatOrderNo() {
    return PlatOrderNo;
  }

  public void setPlatOrderNo(String platOrderNo) {
    PlatOrderNo = platOrderNo;
  }

  public Date getStartTime() {
    return StartTime;
  }

  public void setStartTime(Date startTime) {
    StartTime = startTime;
  }

  public Date getEndTime() {
    return EndTime;
  }

  public void setEndTime(Date endTime) {
    EndTime = endTime;
  }

  public String getTimeType() {
    return TimeType;
  }

  public void setTimeType(String timeType) {
    TimeType = timeType;
  }

  public Integer getPageIndex() {
    return PageIndex;
  }

  public void setPageIndex(Integer pageIndex) {
    PageIndex = pageIndex;
  }

  public Integer getPageSize() {
    return PageSize;
  }

  public void setPageSize(Integer pageSize) {
    PageSize = pageSize;
  }

  @Override
  public String toString() {
    return "GetOrderForm{" +
        "OrderStatus='" + OrderStatus + '\'' +
        ", PlatOrderNo='" + PlatOrderNo + '\'' +
        ", StartTime=" + StartTime +
        ", EndTime=" + EndTime +
        ", TimeType='" + TimeType + '\'' +
        ", PageIndex=" + PageIndex +
        ", PageSize=" + PageSize +
        '}';
  }
}
