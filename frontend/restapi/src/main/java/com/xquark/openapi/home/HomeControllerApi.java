package com.xquark.openapi.home;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Carousel;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.MemberPromotion;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.MemberPromotionStatus;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.cache.annotation.DoGuavaCache;
import com.xquark.service.carousel.CarouselService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.memberPromotion.MemberPromotionService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.product.ProductTopService;
import com.xquark.service.view.MarketingTweetService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;

import static com.xquark.service.cache.constant.CacheKeyConstant.HOME_CAROUSELS;
import static com.xquark.service.cache.constant.CacheKeyConstant.PRODUCT_TOP;

/**
 * 客户端首页数据接口Controller Created by chh on 17-07-10.
 */
@Controller
@RequestMapping(value = "/openapi")
@Api(value = "home", description = "首页rest接口")
public class HomeControllerApi extends BaseController {

  private SkuMapper skuMapper;
  @Autowired
  public void setSkuMapper(SkuMapper skuMapper) {
      this.skuMapper = skuMapper;
  }

  @Autowired
  private CarouselService carouselService;

  @Autowired
  private ProductTopService productTopService;

  @Autowired
  private MarketingTweetService marketingTweetService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private MemberPromotionService memberPromotionService;



  /**
   * 获取首页轮播图
   */
  @ResponseBody
  @RequestMapping(value = "/home/carousels", method = RequestMethod.GET)
  @DoGuavaCache(key = HOME_CAROUSELS)
  public ResponseObject<Carousel> getCarousel() {
    Carousel result = carouselService.getHomeForApp();
    return new ResponseObject<>(result);
  }

  @ResponseBody
  @RequestMapping(value = "/home/getMemberPromotion", method = RequestMethod.POST)
  public ResponseObject<List<MemberPromotion>> getMemberPromotion() {
    List<MemberPromotion> list = memberPromotionService.list(MemberPromotionStatus.ACTIVE);
    return new ResponseObject<>(list);
  }

  /**
   * 获取首页活动列表
   */
  @ResponseBody
  @RequestMapping(value = "/home/getActivity", method = RequestMethod.POST)
  @ApiOperation(value = "获取首页活动列表", notes = "获取首页活动列表", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Carousel> getActivity() {
    Carousel result = carouselService.getHomeActivityForApp();
    return new ResponseObject<>(result);
  }

  /**
   * 获取首页商品类别信息
   */
  @ResponseBody
  @RequestMapping(value = "/home/getCategory", method = RequestMethod.POST)
  @ApiOperation(value = "获取首页商品分类", notes = "获取首页商品分类", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Category>> getHomeCategory() {
    List<Category> result = categoryService.getHomeForApp();
    return new ResponseObject<>(result);
  }


  /**
   * 获取首页商品类别信息
   */
  @ResponseBody
  @RequestMapping(value = "/home/category/getCategory", method = RequestMethod.POST)
  @ApiOperation(value = "获取类目界面商品分类（会过滤掉不显示的类别）", notes = "获取类目界面商品分类（会过滤掉不显示的类别）", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Category>> getCategory() {
    List<Category> result = categoryService.getHomeForApp();
    return new ResponseObject<>(result);
  }

  /**
   * 获取首页爆款推荐商品
   */
  @ResponseBody
  @RequestMapping(value = "/home/getProductTop")
  @DoGuavaCache(key = PRODUCT_TOP)
  public ResponseObject<List<ProductTopVO>> getProductTop() {
    Pageable pager = new PageRequest(0, 100);
    List<ProductTopVO> result = productTopService.list(pager, null);

    for (ProductTopVO product : result) {
      String pid;
      if(StringUtils.isNotBlank(pid=product.getProductId())){
        Sku sku = skuMapper.selectMinPriceSkuByProductId(pid);
        if(sku!=null){
          product.setDeductionDPoint(sku.getDeductionDPoint());
          product.setPoint(sku.getPoint());
          product.setProductPrice(sku.getPrice());
          product.setNetWorth(sku.getNetWorth());

          BigDecimal subtractValue = sku.getPoint().add(sku.getServerAmt());
          BigDecimal proxyPrice = sku.getPrice().subtract(subtractValue).setScale(2,1);
          product.setProxyPrice(proxyPrice);
          product.setMemberPrice(sku.getPrice().subtract(sku.getPoint()).setScale(2,1));

          BigDecimal conversionPrice = sku.getPrice().subtract(sku.getDeductionDPoint().divide(new BigDecimal(10))).setScale(2,1);
          product.setChangePrice(conversionPrice);
        }
      }

    }
    return new ResponseObject<>(result);
  }

}
