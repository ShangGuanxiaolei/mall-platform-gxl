/**
 * 关于用户的 open api
 *
 * @author xuebowen
 */
package com.xquark.openapi.user;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductDistributorMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.PromotionCouponMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.ProdSync;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ThirdCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.model.UserPartner;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.status.SyncAuditStatus;
import com.xquark.dal.status.UserType;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.ShopPostAge;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.shop.ShopForm;
import com.xquark.restapi.shop.ShopVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.category.TermTaxonomyService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.smstpl.SmsTplService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.UserCardService;
import com.xquark.utils.ImgUtils;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class UserControllerApi extends BaseController {

  @Autowired
  private UserService userService;

  @Autowired
  private PromotionCouponMapper promotionCouponMapper;
  @Autowired
  private SmsTplService smsTplService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private ProductService productService;
  @Autowired
  private UrlHelper urlHelper;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private ZoneService zoneService;
  @Autowired
  private CategoryService categoryService;
  @Autowired
  private TermTaxonomyService termTaxonomyService;
  @Autowired
  private OrderService orderService;
  @Autowired
  private ProdSyncMapper prodSyncMapper;
  @Autowired
  private ShopPostAgeService shopPostAgeService;
  @Autowired
  private PromotionCouponService promotionCouponService;
  @Autowired
  private ShopTreeService shopTreeService;
  @Autowired
  private ProductDistributorMapper productDistributorMapper;
  @Autowired
  private ProductMapper productMapper;
  @Autowired
  private TinyUrlService tinyUrlService;
  @Autowired
  private UserTwitterService userTwitterService;
  @Autowired
  private UserPartnerService userPartnerService;
  @Autowired
  private FamilyCardSettingService familyCardSettingService;
  @Autowired
  private UserCardService userCardService;
  @Autowired
  private PartnerSettingService partnerSettingService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private Qiniu qiniu;

  @Value("${shop.banners}")
  private String bannerKeys;

  @Value("${spider.url}")
  private String spiderurl;

  @Value("${site.web.host.name}")
  private String siteHost;

  /**
   * 返回当前用户的用户类型
   */
  @ResponseBody
  @RequestMapping(value = "/getUserType", method = RequestMethod.POST)
  @ApiOperation(value = "获得用户当前状态", notes = "B2B代表代理用户,\nB2B_APPLYING代表正在审核中的代理用户,\n" +
      "    B2B_FROZEN代表被冻结的代理用户,\n" +
      "    DEFAULT代表尚未申请代理的初始用户", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)

  public ResponseObject<UserType> getUserType(HttpServletRequest req,
      @RequestParam(required = false) String userId) {
    UserType type = null;
    if (StringUtils.isEmpty(userId)) {
      userId = this.getCurrentUser().getId();
    }
    try {
      type = userService.getUserType(userId);
    } catch (BizException e) {
      // 未登录状态
      return new ResponseObject(new RequestContext(req).getMessage("signin.failed"),
          GlobalErrorCode.UNAUTHORIZED);
    }
    return new ResponseObject<>(type);
  }

  /**
   * 获取我的店铺
   */
  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX + "/mine", method = RequestMethod.POST)
  @ApiOperation(value = "获得我的店铺信息", notes = "返回店铺头像，名称等信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<ShopVO> mine(HttpServletRequest req) {
    return new ResponseObject<>(shopToVo(shopService.mine(), req));
  }

  private Boolean updateThirdCommission(ThirdCommission atc, HttpServletRequest req) {
    Boolean ret = false;
    if (atc == null || atc.getThirdId() == null) {
      return ret;
    }

    Shop shop = shopService.load(this.getCurrentUser().getShopId());
    if (atc.getCommissionRate() != null) {
      if (atc.getCommissionRate().compareTo(BigDecimal.valueOf(0.05)) < 0 ||
          atc.getCommissionRate().compareTo(BigDecimal.valueOf(1)) > 0) {
        RequestContext requestContext = new RequestContext(req);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("invalid.third.commisionrate"));
      }

      Map<String, Object> params = new HashMap<>();
      params.put("shopId", shop.getId());
      params.put("unionId", atc.getThirdId());
      List<ProdSync> aList = prodSyncMapper.findByParmas(params, null);
      ProdSync aps = new ProdSync();
      if (aList != null && aList.size() != 0) {
        if (aList.size() == 1) {
          aps.setId(aList.get(0).getId());
          aps.setAuditSts(SyncAuditStatus.AUDITTING.toString());
          aps.setCommissionRate(atc.getCommissionRate());
          ret = prodSyncMapper.update(aps) == 1 ? true : false;
        } else {
          ret = false;
        }
      } else {
        aps.setShopId(shop.getId());
        aps.setName(shop.getName());
        aps.setSynced(false);
        aps.setAuditSts(SyncAuditStatus.AUDITTING.toString());
        aps.setCommissionRate(atc.getCommissionRate());
        User aUser = userService.load(atc.getThirdId());
        if (aUser != null) {
          aps.setUnionId(aUser.getId());
        } else {
          aps.setUnionId(IdTypeHandler.encode(16618945L));
        }
        ret = prodSyncMapper.insert(aps) == 1 ? true : false;
      }

      shop.setDanbao(true);
      shopService.update(shop);
    }
    return ret;
  }

  /**
   * b2b数据统计
   */
  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX + "/statistics/2b", method = RequestMethod.POST)
  @ApiOperation(value = "返回用户统计数据", notes = "thisMonth.order代表本月订单数\n" +
      "thisMonth.agentCount代表本月新增代理数\n" +
      "thisMonth.sale代表本月销售数\n" +
      "today.order代表今日订单数\n" +
      "teamCount代表团队人数", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> statisticsB2B() {
    return new ResponseObject<>(shopService.loadB2BStatistics());
  }

  /**
   * 获取未审核和待收货订单总数
   */
  @ResponseBody
  @RequestMapping(value = "/api/order/submittedCount", method = RequestMethod.POST)
  @ApiOperation(value = "返回用户未审核和待收货订单总数", notes = "返回用户未审核和待收货订单总数", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Long> submittedCount() {
    Map<String, Object> params = new HashMap<>();

    String userId = getCurrentUser().getId();
    params.put("userId", userId);
    //params.put("status",OrderStatus.SUBMITTED);
    //params.put("auditType","PARENT");

    long count = orderService.countOrder(params);
    return new ResponseObject<>(count);
  }


  /**
   * 查询某个代理所有待审核的代理申请人总数
   */
  @ResponseBody
  @RequestMapping(value = "/agent/countApplyingByParent", method = RequestMethod.POST)
  @ApiOperation(value = "返回用户所有待审核的代理申请人总数", notes = "返回用户所有待审核的代理申请人总数", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Long> countApplyingByParent(
      @ApiParam(value = "用户id") @RequestParam(required = false) String parentId,
      HttpServletRequest request) {
    if (StringUtils.isEmpty(parentId)) {
      parentId = this.getCurrentIUser().getId();
    }
    long count = userAgentService.countApplyingByParent(parentId);
    return new ResponseObject<>(count);
  }

  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX + "/update", method = RequestMethod.POST)
  @ApiOperation(value = "更新代理用户头像，名称等个人信息", notes = "更新代理用户头像，名称等个人信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<ShopVO> update(@Valid @ModelAttribute ShopForm form, Errors errors,
      HttpServletRequest req) {
    ControllerHelper.checkException(errors);
    Shop shop = new Shop();
    BeanUtils.copyProperties(form, shop);
    shop.setId(this.getCurrentUser().getShopId());

    if (form.getCommisionRate() != null) {
      if (form.getCommisionRate().compareTo(BigDecimal.valueOf(0)) < 0 ||
          form.getCommisionRate().compareTo(BigDecimal.valueOf(0.5)) > 0) {
        RequestContext requestContext = new RequestContext(req);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("invalid.commisionrate"));
      }
    }

    int rc = shopService.update(shop);
    if (1 != rc) {
      RequestContext requestContext = new RequestContext(req);
      if (0 == rc) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("invalid.argument"));
      } else if (-1 == rc) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("valid.shop.exist.message"));
      }
    }

    if (form.getThirdCommission() != null) {
      if (!updateThirdCommission(form.getThirdCommission(), req)) {
        RequestContext requestContext = new RequestContext(req);
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
            requestContext.getMessage("invalid.argument"));
      }
    }

    // 更新店铺名称的同时，也要更新user表和useragent表名称信息
    String userId = this.getCurrentUser().getId();
    User userUpdate = new User();
    userUpdate.setId(userId);
    userUpdate.setName(shop.getName());
    userService.updateByBosUserInfo(userUpdate);

    UserAgentVO vo = userAgentService.selectByUserId(userId);
    UserAgentVO agentUpdate = new UserAgentVO();
    agentUpdate.setId(vo.getId());
    agentUpdate.setName(shop.getName());
    userAgentService.modify(agentUpdate);

    return new ResponseObject<>(shopToVo(shop, req));
  }


  private ThirdCommission createThirdCommission(User aUser, String shopId) {
    if (aUser == null || shopId == null) {
      return null;
    }

    ThirdCommission atc = new ThirdCommission();
    atc.setThirdPartner(aUser.getName());
    atc.setAuditSts(SyncAuditStatus.UNAUDIT.toString());

    List<ProdSync> psList = prodSyncMapper.selectByShopId(shopId, null);
    if (psList != null && psList.size() != 0) {
      for (ProdSync aps : psList) {
        if (aps.getUnionId().equals(aUser.getId())) {
          atc.setCommissionRate(aps.getCommissionRate());
          atc.setAuditSts(aps.getAuditSts());
          atc.setFailedReason(aps.getAuditNote());
        }
      }
    }
    atc.setThirdId(aUser.getId());
    return atc;
  }

  private ShopVO shopToVo(Shop shop, HttpServletRequest req) {
    if (shop != null) {
      shop = shopService.load(shop.getId());
      ShopVO vo = new ShopVO(shop);
      vo.setShopUrl(getShareUrl(req));
      if (StringUtils.isNotBlank(vo.getImg())) {
        vo.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(vo.getBanner())) {
        vo.setBannerUrl(shop.getBanner());
      }

      vo.setCountDraft(productService.countProductsByStatus(vo.getId(), ProductStatus.DRAFT));// 草稿
      vo.setCountForsale(
          productService.countProductsByStatus(vo.getId(), ProductStatus.FORSALE));// 发布计划数
      vo.setCountOnsale(
          productService.countProductsByStatus(vo.getId(), ProductStatus.ONSALE));// 在售商品数
      vo.setCountOutofstock(productService.countProductsByOutofStock(vo.getId()));// 缺货
      vo.setLocalName(getLocalArea(shop.getProvinceId(), shop.getCityId()));  //加载所在区域
      vo.setPostFreeRegions(parserPostFreeStr(shop.getFreeZone())); // 免邮区域

      vo.setCountOfOrderClose(orderService.countSellerOrdersByStatus(OrderStatus.CLOSED));
      vo.setCountOfOrderPaid(orderService.countSellerOrdersByStatus(OrderStatus.PAID));
      vo.setCountOfOrderShipped(orderService.countSellerOrdersByStatus(OrderStatus.SHIPPED));
      vo.setCountOfOrderSubmitted(orderService.countSellerOrdersByStatus(OrderStatus.SUBMITTED));
      vo.setCountOfOrderSuccess(orderService.countSellerOrdersByStatus(OrderStatus.SUCCESS));

      List<ThirdCommission> aList = new ArrayList<>();
      List<User> feeAcctList = userService.getFeeSplitAcct();
      for (User aUser : feeAcctList) {  // 多个分润账号
        if (aUser.getLoginname().equalsIgnoreCase(UserPartnerType.XIANGQU.toString()))  // 暂时只开放"想去"
        {
          aList.add(createThirdCommission(aUser, vo.getId()));
        }
      }
      vo.setThirdCommissions(aList);
      ShopPostAge spa = shopPostAgeService.getPostAgeByShop(shop.getId());
      if (spa != null) {
        if (spa.getPostageStatus() != null) {
          vo.setPostageStatus(
              spa.getPostageStatus());  // 用记亲数据替换老数据, 老数据其实也被新流程更新过了, 为的是将来删除老字段不再维护
        }
        if (spa.getPostage() != null) {
          vo.setPostage(spa.getPostage());
        }
      }
      return vo;
    } else {
      return null;
    }
  }

  // 如果有多个名邮地区以坚线分隔 '|', 一期只会有一个免邮地区, 但可能会包括 浙江沪 长三角等多地区地名
  private List<String> parserPostFreeStr(String postStr) {
    if (null == postStr) {
      return null;
    }
    String[] regions = postStr.split("\\|");
    return Arrays.asList(regions);
  }

  private String getLocalArea(Long provId, Long cityId) {
    String localName = new String();
    Zone zone = null;

    if (provId != null && provId.intValue() > 0) {
      zone = zoneService.load(provId.toString());
      if (zone != null) {
        localName += zone.getName();
      }
    }

    if (cityId != null && cityId.intValue() > 0) {
      zone = zoneService.load(cityId.toString());
      if (zone != null) {
        localName += zone.getName();
      }
    }

    return localName;
  }

  private void sortList(List<Category> list, String id, List<Category> resultList) {
    for (Category node : list) {
      if ((node.getParentId() + "").equals("" + id)) {
        resultList.add(node);
        sortList(list, node.getId(), resultList);
      }
    }
  }

  private String getShareUrl(HttpServletRequest req) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/shop/" + getCurrentUser().getShopId()
            + "?currentSellerShopId=" + getCurrentUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }


  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX + "/user/userInfo", method = RequestMethod.POST)
  @ApiOperation(value = "获得我的代理用户信息", notes = "返回店铺头像，名称，代理级别等信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> getUserInfo(
      @ApiParam(value = "用户id", required = true) @RequestParam(required = false) String userId) {
    User user = new User();
    // 用户信息
    if (userId == null || userId.equals("")) {
      userId = this.getCurrentIUser().getId();
    }
    user = userService.load(userId);
    // 店铺信息
    Shop shop = shopService.load(user.getShopId());
    ShopVO shopVO = new ShopVO(shop);
    if (StringUtils.isNotBlank(shopVO.getImg())) {
      shopVO.setImgUrl(shop.getImg());
    }
    if (StringUtils.isNotBlank(shopVO.getBanner())) {
      shopVO.setBannerUrl(shop.getBanner());
    }
    HashMap map = new HashMap();
    map.put("user", user);
    map.put("shop", shopVO);

    UserAgentVO userAgentVO = userAgentService.selectByUserId(user.getId());
    map.put("userAgent", userAgentVO);

    // 是否合伙人
    String rootShopId = shopTreeService.selectRootShopByShopId(shop.getId()).getRootShopId();
    UserPartner userPartner = userPartnerService
        .selectUserPartnersByUserIdAndShopId(userId, rootShopId);
    if (userPartner != null) {
      map.put("isPartner", true);
    } else {
      map.put("isPartner", false);
    }
    return new ResponseObject<Map>(map);
  }

  private String getProductShareUrl(HttpServletRequest req, String code) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/p/" + code + "?currentSellerShopId="
            + getCurrentUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 生成经销商授权证书图片
   */
  @RequestMapping(value = "/userAgent/certImg", method = RequestMethod.GET, produces = "image/png")
  @ApiOperation(value = "获得我的代理授权证书图片", notes = "返回图片的二进制流数据", httpMethod = "GET", produces = MediaType.IMAGE_PNG_VALUE)
  public @ResponseBody
  byte[] generateCertImg(HttpServletRequest req) {
    // 授权证书在qiliu上的地址
    String certImgUrl = "http://images.handeson.com/FiyRKQtRk_de4cfZqgiVAMFluxpP";
    // 生成经销商授权证书图片
    int width = 1063;
    int height = 1505;
    // 获取当前用户的经销商信息
    String userId = this.getCurrentIUser().getId();
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          requestContext.getMessage("product.not.found"));
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
//		String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/186X186.png";
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      // 首先从qiliu上获取经销商原图
      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(certImgUrl);
      BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);

      Font font = new Font("宋体", Font.BOLD, 26);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);

      // 显示*号的身份证号
      String idcard = userAgentVO.getIdcard();
      Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
      Matcher m = p.matcher(idcard);
      String hiddenIdcard = m.replaceAll("$1********$3");

      // 将代理用户的信息写到图片固定的位置上
      g2.drawString(userAgentVO.getName(), 355, 592);
      g2.drawString(hiddenIdcard, 355, 635);
      g2.drawString(userAgentVO.getWeixin(), 355, 678);
      g2.drawString(userAgentVO.getStartDate(), 380, 1040);
      g2.drawString(userAgentVO.getEndDate(), 620, 1040);
      g2.drawString(userAgentVO.getCertNum() + "", 380, 1080);

      g2.setFont(new Font("宋体", Font.BOLD, 35));
      g2.drawString(userAgentVO.getAgentTypeName(), 320, 785);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。

  }


  /**
   * 获取代理的授权证书图片
   */
  @ResponseBody
  @RequestMapping(value = "/userAgent/getCertImg", method = RequestMethod.POST)
  @ApiOperation(value = "获得我的代理授权证书图片", notes = "生成的证书上传到qiniu服务器，返回qiniu的图片地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<String> getCertImg(HttpServletRequest req) {
    String certImg = "";
    String userId = getCurrentIUser().getId();
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    certImg = userAgentVO.getCertImg();
    // 如果当前代理没有合同图片，则即时合成合同图片
    if (StringUtils.isEmpty(certImg)) {
      genCertImg(userId);
      userAgentVO = userAgentService.selectByUserId(userId);
      certImg = userAgentVO.getCertImg();
    }
    return new ResponseObject<>(certImg);
  }

  /**
   * 生成经销商授权证书图片
   */
  private void genCertImg(String userId) {
    // 返回的qiniu的图片地址
    String certImg = "";
    // 授权证书在qiliu上的地址
    String certImgUrl = "http://images.handeson.com/FiyRKQtRk_de4cfZqgiVAMFluxpP";
    // 生成经销商授权证书图片
    int width = 1063;
    int height = 1505;
    // 获取当前用户的经销商信息
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    if (userAgentVO == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "product.not.found");
    }
    Calendar c = Calendar.getInstance();
    // 默认授权开始时间为用户申请时间
    Date start = userAgentVO.getCreatedAt();
    // 授权结束时间为开始时间加一年
    c.setTime(start);
    c.add(Calendar.YEAR, 1);
    Date end = c.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
    userAgentVO.setStartDate(formatter.format(start));
    // 授权截止时间为申请日期的当前最后一天
    userAgentVO.setEndDate(getEndDate(start));
    userAgentVO.setAgentTypeName(getAgentTypeName(userAgentVO.getType()));

//		String bannerUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/1080X384.png";
//		String imgUrl = "http://7xrw2e.com1.z0.glb.clouddn.com/186X186.png";
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      // 首先从qiliu上获取经销商原图
      BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(certImgUrl);
      BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, width, height);
      BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);

      Font font = new Font("宋体", Font.BOLD, 26);
      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2 = (Graphics2D) bi.getGraphics();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
      g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
          RenderingHints.VALUE_COLOR_RENDER_QUALITY);
      g2.setBackground(Color.WHITE);
      g2.clearRect(0, 0, width, height);
      Color color = new Color(128, 67, 53);
      g2.setPaint(color);
      g2.setFont(font);

      g2.drawImage(imageImgRect, 0, 0, null);

      // 显示*号的身份证号
      String idcard = userAgentVO.getIdcard();
      Pattern p = Pattern.compile("(\\w{6})(\\w+)(\\w{4})");
      Matcher m = p.matcher(idcard);
      String hiddenIdcard = m.replaceAll("$1********$3");

      // 将代理用户的信息写到图片固定的位置上
      g2.drawString(userAgentVO.getName(), 355, 592);
      g2.drawString(hiddenIdcard, 355, 635);
      g2.drawString(userAgentVO.getWeixin(), 355, 678);
      g2.drawString(userAgentVO.getStartDate(), 380, 1040);
      g2.drawString(userAgentVO.getEndDate(), 620, 1040);
      g2.drawString(userAgentVO.getCertNum() + "", 380, 1080);

      g2.setFont(new Font("宋体", Font.BOLD, 35));
      g2.drawString(userAgentVO.getAgentTypeName(), 320, 785);

      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
      InputStream is = new ByteArrayInputStream(os.toByteArray());
      java.util.List<InputStream> ins = new ArrayList<>();
      ins.add(is);
      final java.util.List<UpLoadFileVO> vos = qiniu.uploadImgStream(ins, FileBelong.PRODUCT);
      certImg = qiniu.genQiniuFileUrl(vos.get(0).getKey());

      // 更新代理的授权证书图片字段
      UserAgent updateUserAgent = new UserAgent();
      updateUserAgent.setId(userAgentVO.getId());
      updateUserAgent.setCertImg(certImg);
      userAgentService.modify(updateUserAgent);


    } catch (Exception e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

  }

  private String getAgentTypeName(AgentType type) {
    String agentTypeName = "";
    if (type == AgentType.FOUNDER) {
      agentTypeName = "联合创始人";
    } else if (type == AgentType.DIRECTOR) {
      agentTypeName = "董事";
    } else if (type == AgentType.GENERAL) {
      agentTypeName = "总顾问";
    } else if (type == AgentType.FIRST) {
      agentTypeName = "一星顾问";
    } else if (type == AgentType.SECOND) {
      agentTypeName = "二星顾问";
    } else if (type == AgentType.SPECIAL) {
      agentTypeName = "特约";
    }
    return agentTypeName;
  }

  private String getEndDate(Date start) {
    // 授权截止时间为申请日期的当前最后一天
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy年");
    return formatter.format(start) + "12月31日";
  }


  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX
      + "/user/getUserInfoByUnionId", method = RequestMethod.POST)
  @ApiOperation(value = "根据unionid返回对应用户信息", notes = "返回用户头像，名称，代理级别等信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  @Deprecated
  /**
   * 中太用户unionId不唯一
   */
  public ResponseObject<Map> getUserInfoByUnionId(
      @ApiParam(value = "unionId", required = true) @RequestParam(required = false) String unionId) {
    User user = new User();
    HashMap map = new HashMap();
    // 根据unionid返回对应用户
    user = userService.loadByUnionId(unionId);
    if (user != null) {
      // 店铺信息
      Shop shop = shopService.load(user.getShopId());
      ShopVO shopVO = new ShopVO(shop);
      if (StringUtils.isNotBlank(shopVO.getImg())) {
        shopVO.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(shopVO.getBanner())) {
        shopVO.setBannerUrl(shop.getBanner());
      }
      map.put("user", user);
      map.put("shop", shopVO);

      UserAgentVO userAgentVO = userAgentService.selectByUserId(user.getId());
      // 获取代理地址省市区文本信息
      setZoneName(userAgentVO);
      map.put("userAgent", userAgentVO);
      // 获取上级代理的相关信息
      setParentAgent(map, userAgentVO);
    }
    return new ResponseObject<Map>(map);
  }

  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX
      + "/user/getUserInfoByPhone", method = RequestMethod.POST)
  @ApiOperation(value = "根据phone返回对应用户信息", notes = "返回用户头像，名称，代理级别等信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> getUserInfoByPhone(
      @ApiParam(value = "phone", required = true) @RequestParam(required = false) String phone) {
    User user = new User();
    HashMap map = new HashMap();
    // 根据unionid返回对应用户
    user = userService.loadAnonymousByPhone(phone);
    if (user != null) {
      // 店铺信息
      Shop shop = shopService.load(user.getShopId());
      ShopVO shopVO = new ShopVO(shop);
      if (StringUtils.isNotBlank(shopVO.getImg())) {
        shopVO.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(shopVO.getBanner())) {
        shopVO.setBannerUrl(shop.getBanner());
      }
      map.put("user", user);
      map.put("shop", shopVO);

      UserAgentVO userAgentVO = userAgentService.selectByUserId(user.getId());
      // 获取代理地址省市区文本信息
      setZoneName(userAgentVO);
      map.put("userAgent", userAgentVO);
      // 获取上级代理的相关信息
      setParentAgent(map, userAgentVO);
    }
    return new ResponseObject<Map>(map);
  }

  @ResponseBody
  @RequestMapping(value = UrlHelper.SHOP_URL_PREFIX
      + "/user/bindPhoneByUnionId", method = RequestMethod.POST)
  @ApiOperation(value = "将uniondid与phone对应的代理用户进行绑定", notes = "绑定后返回用户头像，名称，代理级别等信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map> bindPhoneByUnionId(
      @ApiParam(value = "unionId", required = true) @RequestParam(required = false) String unionId,
      @ApiParam(value = "phone", required = true) @RequestParam(required = false) String phone,
      @ApiParam(value = "avatar", required = true) @RequestParam(required = false) String avatar) {
    HashMap map = new HashMap();
    // 根据unionid返回对应用户
    User user = userService.loadAnonymousByPhone(phone);
    UserAgentVO userAgentVO = userAgentService.selectByPhone(phone);
    // 如果该手机号存在代理信息，则将unionid，avatar更新到user表中
    if (user != null && userAgentVO != null) {
      User updateUser = new User();
      updateUser.setId(user.getId());
      if (StringUtils.isNotBlank(avatar)) {
        updateUser.setAvatar(avatar);
        user.setAvatar(avatar);
      }
      updateUser.setUnionId(unionId);
      userService.updateByBosUserInfo(updateUser);

      // 店铺信息
      Shop shop = shopService.load(user.getShopId());
      ShopVO shopVO = new ShopVO(shop);
      if (StringUtils.isNotBlank(shopVO.getImg())) {
        shopVO.setImgUrl(shop.getImg());
      }
      if (StringUtils.isNotBlank(shopVO.getBanner())) {
        shopVO.setBannerUrl(shop.getBanner());
      }
      user.setUnionId(unionId);
      map.put("user", user);
      map.put("shop", shopVO);

      // 获取代理地址省市区文本信息
      setZoneName(userAgentVO);
      map.put("userAgent", userAgentVO);
      // 获取上级代理的相关信息
      setParentAgent(map, userAgentVO);


    }
    return new ResponseObject<Map>(map);
  }

  /**
   * 获取代理地址省市区文本信息
   */
  private void setZoneName(UserAgentVO userAgentVO) {
    AddressVO addressVO = addressService.getDefault(userAgentVO.getUserId());
    String addressId = "";
    if (addressVO != null) {
      addressId = addressVO.getId();
    } else {
      addressId = userAgentVO.getAddress();
    }
    if (StringUtils.isNotEmpty(addressId)) {
      Address address = addressService.load(addressId);
      if (address != null) {
        String zoneId = address.getZoneId();
        Zone zone = zoneService.load(zoneId);
        List<Zone> zones = zoneService.listParents(zoneId);
        if (zones.size() >= 3) {
          userAgentVO.setProvinceName(zones.get(1).getName());
          userAgentVO.setCityName(zones.get(2).getName());
        }
        userAgentVO.setDistrictName(zone.getName());
        userAgentVO.setStreet(address.getStreet());
      }
    }
  }

  /**
   * 获取上级代理的相关信息
   */
  private void setParentAgent(HashMap map, UserAgentVO userAgentVO) {
    // 同时要获取上级代理的相关信息，小程序需要展示
    String parentId = userAgentVO.getParentUserId();
    if (StringUtils.isNotEmpty(parentId)) {
      User puser = userService.load(parentId);
      if (puser != null) {
        Shop pshop = shopService.load(puser.getShopId());
        ShopVO pshopVO = new ShopVO(pshop);
        if (StringUtils.isNotBlank(pshopVO.getImg())) {
          pshopVO.setImgUrl(pshopVO.getImg());
        }
        if (StringUtils.isNotBlank(pshopVO.getBanner())) {
          pshopVO.setBannerUrl(pshopVO.getBanner());
        }
        UserAgentVO puserAgentVO = userAgentService.selectByUserId(parentId);
        map.put("puser", puser);
        map.put("pshop", pshopVO);

        // 获取代理地址省市区文本信息
        setZoneName(userAgentVO);
        map.put("puserAgent", puserAgentVO);
      }
    }
  }


}
