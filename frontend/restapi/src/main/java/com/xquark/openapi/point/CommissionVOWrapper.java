package com.xquark.openapi.point;

import com.hds.xquark.dal.model.CommissionTotal;
import com.xquark.dal.vo.CommissionVO;
import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;

/**
 * Created by wangxinhua. Date: 2018/8/28 Time: 下午7:57
 * 扩展积分库返回的数据
 */
public class CommissionVOWrapper extends CommissionTotal {

  private final BigDecimal accumulation;

  public CommissionVOWrapper(CommissionTotal total, BigDecimal accumulation) {
    BeanUtils.copyProperties(total, this);
    this.accumulation = accumulation;
  }

  public BigDecimal getAccumulation() {
    return accumulation;
  }

}
