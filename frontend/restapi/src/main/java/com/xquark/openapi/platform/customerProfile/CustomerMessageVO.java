package com.xquark.openapi.platform.customerProfile;

/**
 * created by
 *
 * @author wangxinhua at 18-6-8 下午3:55
 */
public class CustomerMessageVO {

  private final String message;

  public CustomerMessageVO(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
