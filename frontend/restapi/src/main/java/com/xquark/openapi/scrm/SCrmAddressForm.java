package com.xquark.openapi.scrm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-2-27. DESC:
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SCrmAddressForm extends BaseAuthForm {

  private Integer id;

  private String aId;

  @NotBlank(message = "unionId不能为空")
  private String unionId;

  @NotBlank(message = "收件人不能为空")
  private String consignee;

  @NotBlank(message = "收件人电话不能为空")
  @Pattern(regexp = "(13\\d|14[57]|15[^4,\\D]|17[678]|18\\d)\\d{8}|170[059]\\d{7}", message = "手机号格式不正确")
  private String phone;

  @NotBlank(message = "地区id不能为空")
  private String zoneId;

  @NotBlank(message = "街道地址不能为空")
  private String street;

  private boolean isDefault;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getaId() {
    return aId;
  }

  public void setaId(String aId) {
    this.aId = aId;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(boolean aDefault) {
    isDefault = aDefault;
  }
}
