/**
 * 关于用户的 open api
 *
 * @author xuebowen
 */
package com.xquark.openapi.version;

import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.DeviceTypes;
import com.xquark.restapi.ResponseObject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/openapi")
public class VersionControllerApi extends BaseController {

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  /**
   * 供ios app访问是否要显示游客按钮
   */
  @ResponseBody
  @RequestMapping(value = "/version/showVisitor", method = RequestMethod.POST)
  public ResponseObject<Boolean> getUserType(HttpServletRequest request,
      @RequestParam(required = false) String versionNo) {
    WechatAppConfig appConfig = wechatAppConfigMapper.selectByProfile("test");
    boolean flag = false;
    if (appConfig != null) {
      String dbVersion = appConfig.getIosVersion();
      switch (DeviceTypes.getDeviceType(request)) {
        case 1:
          flag = dbVersion.contains("i" + versionNo);
          break;
        case 2:
          flag = dbVersion.contains("a" + versionNo);
          break;
        default:
      }
    }
    return new ResponseObject<>(flag);
  }


}
