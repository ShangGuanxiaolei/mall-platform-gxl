package com.xquark.openapi.scrm;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.model.GoodInfo;
import com.xquark.service.PolyProductService.PolyProductService;
import com.xquark.service.pricing.vo.ResponsePolySysnStock;
import com.xquark.service.pricing.vo.ResponseResult;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @auther liuwei
 * @date 2018/6/12 20:38
 */

@Controller
@RequestMapping(value = "/openapi")
public class PolyControllerApi {

  private final static Logger LOGGER = LoggerFactory.getLogger(PolyControllerApi.class);

  private PolyProductService polyProductService;

  @Autowired
  public void setPolyProductService(PolyProductService polyProductService){
    this.polyProductService = polyProductService;
  }

  @RequestMapping(value = "/polyapi/download/product",method = RequestMethod.POST,
      consumes = "application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  @ResponseBody
  public String downloadProduct(HttpServletRequest request){
    LOGGER.info("========== 开始处理商品请求 ===========");
    ResponseResult<List<GoodInfo>> rr = new ResponseResult<List<GoodInfo>>();
    String bizcontent = request.getParameter("bizcontent");
    JSONObject json = JSONObject.parseObject(bizcontent);
    try {
      //调用service的方法返回商品数据
      LOGGER.info("========== 开始获取商品相关信息 ===========");
      List<GoodInfo> goodslist = polyProductService.getPloyProductList(
          Integer.parseInt(json.getString("PageIndex")),
          Integer.parseInt(json.getString("PageSize")));
      Integer totalcount = polyProductService.getPolyCountTotal();
      rr.setCode("10000");
      rr.setMessage("SUCCESS");
      rr.setTotalcount(totalcount);
      rr.setGoodslist(goodslist);
    }catch (RuntimeException e){
      rr.setCode("40000");
      rr.setMessage("参数不符合要求");
      LOGGER.error("========== 请求参数错误 ===========");
    }
    return JSONObject.toJSONString(rr);
  }

  @RequestMapping(value = "/polyapi/sync/stock",method = RequestMethod.POST,
      consumes ="application/x-www-form-urlencoded;charset=utf-8",
      produces = "application/x-www-form-urlencoded;charset=utf-8")
  @ResponseBody
  public String syncStock(HttpServletRequest request){
    ResponsePolySysnStock<Integer> rp = new ResponsePolySysnStock<Integer>();
    String bizcontent = request.getParameter("bizcontent");
    JSONObject json = JSONObject.parseObject(bizcontent);
    Integer id = Integer.parseInt(json.getString("PlatProductID"));
    Integer quantity = Integer.parseInt(json.getString("Quantity"));
    if(polyProductService.checkProductId(id)){
      try {
        LOGGER.info("========== 开始同步商品库存 ===========");
        if (json.getString("SkuID") != null || json.getString("OuterID") != null
            || json.getString("OutSkuID") != null
            ) {
          String skuId;
          if (json.getString("SkuID") != null) {
            skuId = json.getString("SkuID");
            polyProductService.savePloyProductSkuQuantity(quantity, skuId);
          }
          if (json.getString("OuterID") != null) {
            skuId = json.getString("OuterID");
            polyProductService.savePloyProductSkuQuantity(quantity, skuId);
          }
          if (json.getString("OutSkuID") != null) {
            skuId = json.getString("OutSkuID");
            polyProductService.savePloyProductSkuQuantity(quantity, skuId);
          }
          rp.setCode("10000");
          rp.setMessage("SUCCESS");
          rp.setQuantity(quantity);
        }else {
          polyProductService.savePloyProductQuantity(quantity, id);
          LOGGER.info("========== 同步商品库存结束 ===========");
          rp.setCode("10000");
          rp.setMessage("SUCCESS");
          rp.setQuantity(quantity);
        }
      }catch (RuntimeException e){
        rp.setCode("40000");
        rp.setMessage("服务器异常，请稍后再试");
        rp.setQuantity(null);
      }
    }else {
      rp.setCode("40000");
      rp.setMessage("商品不存在");
      rp.setQuantity(null);
    }
    return JSONObject.toJSONString(rp);
  }
}


