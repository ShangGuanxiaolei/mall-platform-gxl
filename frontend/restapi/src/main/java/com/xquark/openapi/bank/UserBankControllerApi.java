package com.xquark.openapi.bank;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.verify.VerificationFacade;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.vo.CommissionVO;
import com.xquark.dal.vo.OrderItemVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.account.AccountService;
import com.xquark.service.account.SubAccountService;
import com.xquark.service.alipay.UserAlipayService;
import com.xquark.service.bank.UserBankService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.deal.DealService;
import com.xquark.service.order.OrderService;
import com.xquark.service.payBank.PayBankService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/openapi")
@Api(value = "bank", description = "收益管理", produces = MediaType.APPLICATION_JSON_VALUE)
@ApiIgnore
public class UserBankControllerApi extends BaseController {

  @Autowired
  UserBankService userBankService;

  @Autowired
  UserAlipayService userAlipayService;

  @Autowired
  WithdrawApplyService withdrawApplyService;

  @Autowired
  private VerificationFacade veriFacade;

  @Autowired
  DealService dealService;

  @Autowired
  AccountService accountService;

  @Autowired
  SubAccountService subAccountService;

  @Autowired
  UserService userService;

  @Autowired
  PayBankService paybankService;

  @Autowired
  CommissionService commissionService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ShopService shopService;

  @Autowired
  private OrderService orderService;

  /**
   * app:2b的我的收益页面
   */
  @ResponseBody
  @RequestMapping(value = "/userBank/withdrawAll/2b", method = RequestMethod.POST)
  @ApiOperation(value = "我的收益查询", notes = "withdrawAll代表累计收益，lastMonth代表上个月收益，monthWithdrawList代表本年度每个月的收益", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Map<String, Object>> withdrawAllToB() {
    String userId = userBankService.getCurrentUser().getId();

    BigDecimal withdrawAll = commissionService.getWithdrawAll(userId);//获取累计收入
    List<BigDecimal> monthWithdrawList = commissionService.getFeeOfMonths(userId);//获取本年度每个月的收益

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
    Calendar calendar = Calendar.getInstance();
    int nowMonth = calendar.get(Calendar.MONTH);
    if (nowMonth == 0) {//如果是一月份
      calendar.add(Calendar.MONTH, -1);
    } else {
      calendar.set(Calendar.MONTH, nowMonth);
    }

    String lastMonthStr = sdf.format(calendar.getTime());
    BigDecimal lastMonthWithdraw = commissionService
        .getWithdrawOfMonth(userId, lastMonthStr);//获取上个月的收益

    Map<String, Object> mineIncome = new HashMap<>();

    mineIncome.put("withdrawAll", withdrawAll);//累计收入 withdrawAll
    mineIncome.put("monthWithdrawList", monthWithdrawList);//本年度每个月的收益
    mineIncome.put("lastMonth", lastMonthWithdraw);//上个月收益

    return new ResponseObject<>(mineIncome);
  }

  /**
   * app:2b收益列表
   */
  @ResponseBody
  @RequestMapping(value = "/userBank/order/list/2b", method = RequestMethod.POST)
  @ApiOperation(value = "待收收益列表", notes = "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
      "showAll(true代表查询所有，false代表查询当天待收收益)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<OrderVO>> listTwitterCenterOrderTob(
      @RequestParam("showAll") boolean showAll, Pageable pageable) {

    String currentShopId = getCurrentUser().getShopId();
    String userId = shopService.load(currentShopId).getOwnerId();
    Map<String, Object> params = new HashMap<>();
    List<OrderVO> result = new ArrayList<>();
    params.put("showAll", showAll);
    params.put("userPhone", StringUtils.defaultIfBlank(userService.load(userId).getPhone(), null));
    List<CommissionVO> listCommissionVO = commissionService
        .listSuccessOrderNo(params, pageable);//查询当月收益的orderNo
    if (listCommissionVO != null) {
      for (CommissionVO commissionVO : listCommissionVO) {
        BigDecimal sumCommission = new BigDecimal(0);
        OrderVO order = orderService.loadByOrderNo(commissionVO.getOrderNo());
        List<OrderItemVO> orderItemVOs = orderService
            .selectVOByOrderIdAndUserId(order.getId(), userId);
        order.setOrderItemVOs(orderItemVOs);
        for (OrderItemVO orderItemVO : orderItemVOs) {
          sumCommission = sumCommission.add(orderItemVO.getCommission());
        }
        order.setCommissionFee(sumCommission);
        // 佣金列表返回的订单数据中，日期为佣金日期
        order.setCreatedAt(commissionVO.getUpdatedAt());
        result.add(order);
      }

      if (result != null) {
        for (OrderVO order : result) {
          String imgUrl = "";

          for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
          }
          order.setImgUrl(imgUrl);

        }
      }
    }
    return new ResponseObject<>(result);
  }


  /**
   * 获取银行卡显示
   */
  private String getAccountNumberForShow(String accountNumber) {
    if (StringUtils.isBlank(accountNumber)) {
      return "";
    }
    try {
      String temp = accountNumber;
      int len = temp.length();
      String start = temp.substring(0, 6);
      String end = temp.substring(len - 4);
      String center = "****";
      return start + center + end;
    } catch (Exception e) {
    }
    return "";
  }


}
