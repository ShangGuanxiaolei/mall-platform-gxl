package com.xquark.openapi.scrm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by wangxinhua on 18-2-11. DESC: 基本校验参数封装
 */
public class BaseAuthForm {

  // 时间戳
  @NotBlank(message = "时间戳不能为空")
  private String timestamp;

  // 签名
  @NotBlank(message = "签名不能为空")
  private String signature;

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }
}
