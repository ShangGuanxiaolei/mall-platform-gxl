package com.xquark.openapi.polyapi;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.vo.PolyapiOrderItemVO;
import com.xquark.restapi.ResponsePOrder;
import com.xquark.service.polyapiOrder.PolyapiOrderService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User: kong Date: 2018/6/17. Time: 0:05 菠萝派订单
 */
@RestController
@RequestMapping("/openapi")
public class PolyapiOrderController {

    @Autowired
    private PolyapiOrderService polyapiOrderService;

    private final static Logger LOGGER = LoggerFactory.getLogger(PolyapiOrderController.class);

    @RequestMapping(value = "/polyapi/order",
        consumes = "application/x-www-form-urlencoded;charset=utf-8",
        produces = "application/x-www-form-urlencoded;charset=utf-8")
    public String getOrder(HttpServletRequest request) {
        LOGGER.info("========== 开始处理订单请求 ===========");
        ResponsePOrder<List<PolyapiOrderItemVO>> ro = new ResponsePOrder<List<PolyapiOrderItemVO>>();
        String bizcontent = request.getParameter("bizcontent");
        JSONObject json = JSONObject.parseObject(bizcontent);
        String StartTime = json.getString("StartTime");
        String EndTime = json.getString("EndTime");
        String OrderStatus = json.getString("OrderStatus");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       try{
            Date startTime = sdf.parse(StartTime);
            Date endTime = sdf.parse(EndTime);
            if(OrderStatus.equals("JH_99")){//全部订单
                List<PolyapiOrderItemVO> polyapiOrderVO=polyapiOrderService.getPolyapiAllOrder(
                    Integer.parseInt(json.getString("PageIndex")),
                    Integer.parseInt(json.getString("PageSize")),
                    startTime,endTime
                );
                ro.setCode("10000");
                ro.setMessage("SUCCESS");
                ro.setOrders(polyapiOrderVO);
                ro.setNumtotalorder(Integer.parseInt(polyapiOrderService.getNumTotalAllOrder()));

            } else if (OrderStatus.equals("JH_02")) {//代发货
              //由于只有支付成功状态，而且支付成功一小时之内客户可取消订单，所以判断订单是否过了一个小时
              List<PolyapiOrderItemVO> polyapiOrderVO = polyapiOrderService.getPolyapiOrder(
                  Integer.parseInt(json.getString("PageIndex")),
                  Integer.parseInt(json.getString("PageSize")),
                  startTime, endTime, "PAID"
              );
              List<PolyapiOrderItemVO> polyapiOrderVOs = new ArrayList<>();
              for(PolyapiOrderItemVO orderItemVO : polyapiOrderVO){
                      polyapiOrderVOs.add(orderItemVO);
              }
              ro.setCode("10000");
              ro.setMessage("SUCCESS");
              ro.setOrders(polyapiOrderVOs);
              ro.setNumtotalorder(Integer.parseInt(polyapiOrderService.getNumTotalOrder("PAID")));
        }else if(OrderStatus.equals("JH_04")){//交易成功
                List<PolyapiOrderItemVO> polyapiOrderVO=polyapiOrderService.getPolyapiOrder(
                    Integer.parseInt(json.getString("PageIndex")),
                    Integer.parseInt(json.getString("PageSize")),
                    startTime,endTime,"SUCCESS"
                );
                ro.setCode("10000");
                ro.setMessage("SUCCESS");
                ro.setOrders(polyapiOrderVO);
                ro.setNumtotalorder(Integer.parseInt(polyapiOrderService.getNumTotalOrder("SUCCESS")));
        }else if (OrderStatus.equals("JH_99")){//交易关闭
                List<PolyapiOrderItemVO> polyapiOrderVO=polyapiOrderService.getPolyapiOrder(
                    Integer.parseInt(json.getString("PageIndex")),
                    Integer.parseInt(json.getString("PageSize")),
                    startTime,endTime,"CLOSED"
                );
                ro.setCode("10000");
                ro.setMessage("SUCCESS");
                ro.setOrders(polyapiOrderVO);
                ro.setNumtotalorder(Integer.parseInt(polyapiOrderService.getNumTotalOrder("CLOSED")));
        }else {

                ro.setCode("40000");
                ro.setMessage("该状态暂不支持");
            }
       } catch (NullPointerException e) {
         ro.setCode("10000");
         ro.setMessage("没有对应的订单");
       } catch (ParseException e) {
         e.printStackTrace();
       }
        return JSONObject.toJSONString(ro);
    }
}

