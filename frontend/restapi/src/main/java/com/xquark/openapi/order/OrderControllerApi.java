package com.xquark.openapi.order;

import com.google.common.base.Optional;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.OrderMessage;
import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.model.OrderUserDevice;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.type.OrderActionType;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.dal.vo.OrderItemCommentVO;
import com.xquark.dal.vo.OrderItemVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.dal.voex.OrderVOEx;
import com.xquark.openapi.product.form.OrderSumbitFormApi;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.order.OrderShippedForm;
import com.xquark.service.address.AddressService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.apiVisitorLog.ApiVisitorLogService;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderItemCommentService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderUserDeviceService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class OrderControllerApi extends BaseController {

    @Autowired
    private OrderItemCommentService orderItemCommentService;
    @Autowired
    private OrderService orderService;

    @Autowired
    private ZoneService zoneService;
    @Autowired
    private ResourceFacade resourceFacade;
    @Autowired
    private ApiVisitorLogService apiVisitorLogService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private OrderRefundService orderRefundService;

    @Autowired
    private UnionService unionService;
    @Autowired
    private CashierService cashierService;

    @Autowired
    private ShopService shopService;

    @Autowired
    private TinyUrlService tinyUrlService;

    @Autowired
    private ActivityGrouponService activityGrouponService;

    @Autowired
    private UrlHelper urlHelper;

    @Autowired
    private UserAgentService userAgentService;

    @Autowired
    private RolePriceService rolePriceService;

    @Autowired
    private AuditRuleService auditRuleService;

    @Autowired
    private ShopTreeService shopTreeService;

    @Autowired
    private MainOrderService mainOrderService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderUserDeviceService orderUserDeviceService;

    //private static final String ORDERNOTES = "{order.handingcharge}";
    private final String ORDERNOTES = "您的交易金额大于%s，需要支付%s手续费";

    @Value("${tech.serviceFee.standard}")
    private String serviceFeethreshold;

    @Value("${order.delaysign.date}")
    private int defDelayDate;

    @Autowired
    private SkuMapper skuMapper;
    @ResponseBody
    @RequestMapping(value = "/order/list/{status}", method = RequestMethod.POST)
    @ApiOperation(value = "我的订单列表查询", notes = "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n"
            +
            "status传递订单状态(SUBMITTED代表待审核，PAID代表待发货，SHIPPED代表待收货，SUCCESS代表已完成)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<OrderVO>> listOrder(@PathVariable String status, Pageable pageable) {
        List<OrderVO> result = new ArrayList<>();
        if (status == null) {
            return new ResponseObject<>();
        }
        OrderStatus os;
        if ("all".equalsIgnoreCase(status)) {

            result = orderService.listByStatus(null, pageable, "", "");

        } else {

            try {
                os = OrderStatus.valueOf(status.toUpperCase());
            } catch (Exception e) {
                return new ResponseObject<>();
            }
            //result = orderService.listByStatus4Seller(os, pageable);
            result = orderService.listByStatus(os, pageable, "", "");
        }
        for (OrderVO order : result) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }
            for (OrderItemVO item : order.getOrderItemVOs()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }
            order.setImgUrl(imgUrl);
            if (order.getOrderAddress() != null) {
                List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
                String addressDetails = "";
                for (Zone zone : zoneList) {
                    addressDetails += zone.getName();
                }
                addressDetails += order.getOrderAddress().getStreet();
                order.setAddressDetails(addressDetails);
            } else {
                order.setAddressDetails("自提");
            }

            // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
            AuditRule auditRule = auditRuleService.selectByOrderId(order.getId());
            if (auditRule != null) {
                order.setAuditType(auditRule.getAuditType());
            }

        }

//		if (result.size() > 0 && OrderStatus.PAID.equals(os)) {
//			OrderVO vo = result.get(0);
//			vo.setSeq(orderService.selectOrderSeqByShopId(vo.getShopId()));
//		}

        //
        apiVisitorLogService.visit(orderService.getCurrentUser().getId(), "/order/list/{status}");

        return new ResponseObject<>(result);
    }


    @ResponseBody
    @RequestMapping(value = "/order/listByKey/{status}", method = RequestMethod.POST)
    @ApiOperation(value = "我的订单搜索", notes = "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
            "key传递搜索关键词，startTime代表订单开始时间，endTime代表订单结束时间)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<OrderVO>> listOrderByKey(String key, @PathVariable String status,
                                                        Pageable pageable, @RequestParam(required = false) String startTime,
                                                        @RequestParam(required = false) String endTime) {
        //订单号 买家姓名 电话
        if (status == null) {
            return new ResponseObject<>();
        }
        OrderStatus os;
        List<OrderVO> result = new ArrayList<>();

        //todo 时间段查询
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);

        if ("all".equalsIgnoreCase(status)) {
            result = orderService.listByStatusKey4Seller(null, key, pageable, params, "");
        } else {
            try {
                os = OrderStatus.valueOf(status.toUpperCase());
            } catch (Exception e) {
                return new ResponseObject<>();
            }
            result = orderService.listByStatusKey4Seller(os, key, pageable, params, "");
        }
        for (OrderVO order : result) {
            String imgUrl = "";
            for (OrderItem item : order.getOrderItems()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }

            for (OrderItemVO item : order.getOrderItemVOs()) {
                imgUrl = item.getProductImg();
                item.setProductImgUrl(imgUrl);
            }

            order.setImgUrl(imgUrl);

            if (order.getOrderAddress() != null) {
                List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
                String addressDetails = "";
                for (Zone zone : zoneList) {
                    addressDetails += zone.getName();
                }
                addressDetails += order.getOrderAddress().getStreet();
                order.setAddressDetails(addressDetails);
            }
        }

        return new ResponseObject<>(result);
    }


    /**
     * 订单详情
     */
    @ResponseBody
    @RequestMapping(value = "/order/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "根据订单id获取订单详情", notes = "根据订单id获取订单详情", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<OrderVOEx> view(@PathVariable String id, HttpServletRequest req) {
        OrderVO order = orderService.loadVO(id);

        if (order == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单[id=" + id + "]不存在，或者没有权限访问");
        }

        // 设置物流公司官网
        for (LogisticsCompany logisticsCompany : LogisticsCompany.values()) {
            if (logisticsCompany.getName().equals(order.getLogisticsCompany())) {
                order.setLogisticsOfficial(logisticsCompany.getUrl());
            }
        }
        //TODO 兼容老版本处理
        if ("顺丰".equals(order.getLogisticsCompany()) || "顺丰快递".equals(order.getLogisticsCompany())
                || "SF_EXPRESS".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.sf-express.com/");
        } else if ("圆通".equals(order.getLogisticsCompany()) || "圆通快递"
                .equals(order.getLogisticsCompany()) || "YTO".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.yto.net.cn/");
        } else if ("申通".equals(order.getLogisticsCompany()) || "STO"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.sto.cn/");
        } else if ("中通".equals(order.getLogisticsCompany()) || "ZTO"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.zto.cn/");
        } else if ("百世汇通".equals(order.getLogisticsCompany()) || "BESTEX"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.htky365.com/");
        } else if ("韵达".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.yundaex.com/");
        } else if ("天天".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.ttkdex.com/");
        } else if ("全峰".equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.qfkd.com.cn/");
        } else if ("邮政EMS".equals(order.getLogisticsCompany()) || "中国邮政"
                .equals(order.getLogisticsCompany())) {
            order.setLogisticsOfficial("http://www.ems.com.cn/");
        } else {
            order.setLogisticsOfficial("");
        }

        Shop shop = shopService.load(order.getShopId());
        if (shop != null) {
            order.setShopName(shop.getName());
        }
        String userId = getCurrentUser().getId();
        if (userId.equals(order.getBuyerId())) {
            order.setFromType("in");
        } else {
            order.setFromType("out");
        }

        // 订单详情，增加买家和卖家对应的shop信息
        String buyerId = order.getBuyerId();
        String sellerId = order.getSellerId();
        Shop buyerShop = shopService.findByUser(buyerId);
        Shop sellerShop = shopService.findByUser(sellerId);
        order.setBuyerShop(buyerShop);
        order.setSellerShop(sellerShop);

        String imgUrl = "";
        List<OrderItemVO> orderItemVOs = new ArrayList<>();
        for (OrderItem item : order.getOrderItems()) {
            imgUrl = item.getProductImg();
            item.setProductImgUrl(imgUrl);
            OrderItemVO orderItemVO = new OrderItemVO();
            BeanUtils.copyProperties(item, orderItemVO);
            Commission commission = unionService.loadByOrderItemAndUserId(item.getId(), userId);
            if (commission != null) {
                orderItemVO.setCommission(commission.getFee());
            } else {
                orderItemVO.setCommission(new BigDecimal("0"));
            }

            //进货类型订单省赚逻辑处理
            if (order.getOrderType() == OrderSortType.PURCHASE) {
                BigDecimal marketPrice = orderItemVO.getMarketPrice();
                BigDecimal price = orderItemVO.getPrice();
                int amount = orderItemVO.getAmount();
                // 买入的省用商品原价减去商品买入的价格
                if ("in".equals(order.getFromType())) {
                    orderItemVO.setCommission(marketPrice.subtract(price).multiply(new BigDecimal(amount)));
                }
                // 卖出的赚用商品买入的价格减去此用户设置的经销商价格
                else {
                    //判断当前用户是否在代理角色中
                    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
                    if (userAgentVO != null) {
                        String role = userAgentVO.getRole();
                        RolePriceVO rolePriceVO = rolePriceService
                                .selectByProductAndRole(orderItemVO.getProductId(), role);
                        if (rolePriceVO != null) {
                            BigDecimal rolePrice = rolePriceVO.getPrice();
                            orderItemVO.setCommission(price.subtract(rolePrice).multiply(new BigDecimal(amount)));
                        }
                    }
                }
            }

            orderItemVO.setProductUrl(urlHelper.genProductUrl(orderItemVO.getProductId()));

            // 设置进货类型的商品详情页
            String productId = item.getProductId();

            orderItemVOs.add(orderItemVO);
        }
        order.setOrderItemVOs(orderItemVOs);
        order.setImgUrl(imgUrl);

        // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
        AuditRule auditRule = auditRuleService.selectByOrderId(order.getId());
        if (auditRule != null) {
            order.setAuditType(auditRule.getAuditType());
        }

        if (order.getOrderAddress() != null) {
            List<Zone> zoneList = zoneService.listParents(order.getOrderAddress().getZoneId());
            String addressDetails = "";
            for (Zone zone : zoneList) {
                addressDetails += zone.getName();
            }
            addressDetails += order.getOrderAddress().getStreet();
            order.setAddressDetails(addressDetails);
        }

        // 分佣
        List<Commission> commissions = unionService.listByOrderId(order.getId());
        BigDecimal cmFee = BigDecimal.ZERO;
        for (Commission cm : commissions) {
            cmFee = cmFee.add(cm.getFee());
        }
        order.setCommissionFee(cmFee);
        if (order.getDiscountFee() == null) {
            order.setDiscountFee(BigDecimal.ZERO);
        }
        BigDecimal handingFee = orderService.loadTechServiceFee(order);
        if (!handingFee.equals(BigDecimal.ZERO)) {
            try (Formatter fmt = new Formatter()) {
                fmt.format(ORDERNOTES, serviceFeethreshold,
                        handingFee.setScale(2, BigDecimal.ROUND_HALF_UP));
                order.setNotes(fmt.toString());
            }
        }

        // TODO ?
        order.setHongbaoAmount(handingFee);

        List<CouponInfoVO> orderCoupons = cashierService.loadCouponInfoByOrderNo(order.getOrderNo());
        order.setOrderCoupons(orderCoupons);

        OrderVOEx orderEx = new OrderVOEx(order, orderService.findOrderFees(order));
        BigDecimal discountFee = ObjectUtils.defaultIfNull(orderEx.getDiscountFee(), BigDecimal.ZERO);
//		BigDecimal paidFee = ObjectUtils.defaultIfNull(cashierService.loadPaidFee(order.getOrderNo()), BigDecimal.ZERO);
        orderEx.setDiscountFee(discountFee.add(handingFee));
        orderEx.setDefDelayDate(defDelayDate);
        orderEx.setRefundableFee(orderEx.getTotalFee().subtract(orderEx.getLogisticsFee()));

        orderEx.setShowRefundBtn(false);
        //快店IOS审核失败，暂时关闭买家端退款入口
        if (orderEx.getStatus() != OrderStatus.SUBMITTED && orderEx.getStatus() != OrderStatus.SUCCESS
                && orderEx.getStatus() != OrderStatus.CANCELLED) {

            if (orderEx.getStatus() == OrderStatus.CLOSED) {
                List<OrderRefund> refunds = orderRefundService.listByOrderId(order.getId());
                if (refunds.size() > 0) {
                    orderEx.setShowRefundBtn(true);
                }
            } else {
                orderEx.setShowRefundBtn(true);
            }
        }
        //TODO buyerName  from user
        orderEx.setBuyerName(userService.loadByAdmin(orderEx.getBuyerId()).getName());
        orderEx.setBuyerPhone(userService.loadByAdmin(orderEx.getBuyerId()).getPhone());
        orderEx.setBuyerImgUrl(userService.loadByAdmin(orderEx.getBuyerId()).getAvatar());
        return new ResponseObject<OrderVOEx>(orderEx);
    }

    private BigDecimal findRefundableFee(OrderVO order, OrderStatus orderStatus) {
        BigDecimal refundableFee = null;

//        if(orderStatus == OrderStatus.PAID || orderStatus == OrderStatus.SHIPPED){
//            refundableFee = order.getPaidFee();
////            refundableFee = order.getTotalFee().add(order.getDiscountFee());
//            if(orderStatus == OrderStatus.SHIPPED)
//                refundableFee = refundableFee.subtract(order.getLogisticsFee());
//        }

        refundableFee = order.getTotalFee();
        if (orderStatus != OrderStatus.PAID) {
            refundableFee = refundableFee.subtract(order.getLogisticsFee());
        }
        return refundableFee;
    }

    @ResponseBody
    @RequestMapping(value = "/api/order/cancel", method = RequestMethod.POST)
    @ApiOperation(value = "根据订单id取消某订单", notes = "根据订单id取消某订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> cancelOrder(
            @ApiParam(value = "订单id", required = true) @RequestParam String orderId) {
      //取消订单之前检查订单状态
      Optional<OrderVO> orderVoOptional = Optional.fromNullable(orderService.loadVO(orderId));
      if (orderVoOptional.isPresent()) {
        OrderStatus status = orderVoOptional.get().getStatus();
        if (status.equals(OrderStatus.SUBMITTED)) {
          //如果订单还未付款，则取消全部订单
          orderService.cancelMainOrder(orderVoOptional.get().getMainOrderId());
        } else {
          //如果已经付款了，则取消订单本身
          orderService.cancel(orderId);
        }
      } else {
        return new ResponseObject<>("订单未找到", GlobalErrorCode.NOT_FOUND);
      }
        return new ResponseObject<>(true);
    }

    /**
     * 卖家订单发货
     */
    @ResponseBody
    @RequestMapping(value = "/order/shipped", method = RequestMethod.POST)
    @ApiOperation(value = "订单发货", notes = "传入物流公司，物流单号，订单发货", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> shipped(@Valid @ModelAttribute OrderShippedForm form,
                                           Errors errors) {
        ControllerHelper.checkException(errors);
        String logisticComp = "";
        try {
            logisticComp = LogisticsCompany.valueOf(form.getLogisticsCompany()).toString();
        } catch (Exception ex) {
            logisticComp = form.getLogisticsCompany();
        }
        return new ResponseObject<>(
                orderService.ship(form.getOrderId(), logisticComp, form.getLogisticsOrderNo()));
    }


    private List<OrderMessage> filterBuyerCmt(List<OrderMessage> msgList) {
        if (msgList == null || msgList.size() == 0) {
            return null;
        }

        List<OrderMessage> retList = new ArrayList<>();
        for (OrderMessage om : msgList) {
            if (om.getGroupId().equals("0")) {
                retList.add(om);
            }
        }
        Collections.reverse(retList);
        return retList;
    }

    private List<OrderMessage> filterSellerReps(String groupId, List<OrderMessage> msgList) {
        if (msgList == null || msgList.size() == 0) {
            return null;
        }
        List<OrderMessage> retList = new ArrayList<>();

        for (OrderMessage om : msgList) {
            if (om.getGroupId().equals(groupId)) {
                retList.add(om);
            }
        }
        Collections.reverse(retList);
        return retList;
    }


    private void trans4Show(OrderItemCommentVO vo) {
        OrderItem item = orderItemMapper.selectByPrimaryKey(vo.getOrderItemId());
        vo.setPrice(item.getPrice());
        vo.setAmount(item.getAmount());
        vo.setProductImgUrl(item.getProductImg());
        String name = item.getSkuStr();
        if (name.contains("无")) {
            name = item.getProductName();
        }
        vo.setSkuStr(name);
    }


    /**
     * b2b进货订单审核操作
     */
    @ResponseBody
    @RequestMapping(value = "/api/order/audit", method = RequestMethod.POST)
    @ApiOperation(value = "订单审核", notes = "根据订单id审核通过某订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> audit(HttpServletRequest request,
                                         @ApiParam(value = "订单id", required = true) @RequestParam(required = true) String orderId) {
        orderService.audit(orderId);
        return new ResponseObject<>(true);
    }

    /**
     * 订单确认收货请求
     */
    @ResponseBody
    @RequestMapping(value = "/api/order/confirmShipped", method = RequestMethod.POST)
    @ApiOperation(value = "收货签收", notes = "根据订单id确认收货某订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> confirmShipped(HttpServletRequest request,
                                                  @ApiParam(value = "订单id", required = true) @RequestParam(required = true) String orderId) {
        OrderVO order = orderService.loadVO(orderId);
        if (order == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "订单["
                    + orderId + "]不存在");
        }

        if (order.getId() != null) {
            orderService.executeBySystem(order.getId(), OrderActionType.SIGN, null);
            log.info("订单  orderNo=[" + order.getOrderNo() + "]" + "以签收");
        }

        return new ResponseObject<>(true);
    }

    /**
     * 卖家审核发货接口（b2b经销商使用）
     */
    @ResponseBody
    @RequestMapping(value = "/order/auditAndShip", method = RequestMethod.POST)
    @ApiOperation(value = "审核发货", notes = "根据订单id审核发货某订单", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> auditAndShip(@Valid @ModelAttribute OrderShippedForm form,
                                                Errors errors) {
        ControllerHelper.checkException(errors);
        String logisticComp = form.getLogisticsCompany();
        return new ResponseObject<>(
                orderService.auditAndship(form.getOrderId(), logisticComp, form.getLogisticsOrderNo()));
    }

    /**
     * 获得当前订单的收货地址和联系方式
     */
    private OrderAddress getOrderAddress(OrderSumbitFormApi form) {
        OrderAddress oa = new OrderAddress();
        if ("1".equals(form.getIspickup())) {
            oa = null;
        } else {
            if (StringUtils.isNotEmpty(form.getAddressId())) {
                Address address = addressService.load(form.getAddressId());
                BeanUtils.copyProperties(address, oa);
            } else {
                form.setStreet(form.getStreet() == null ? "" : form.getStreet().trim());
                BeanUtils.copyProperties(form, oa);
            }
        }
        return oa;
    }

    private OrderUserDevice buildOrderUserDevice(HttpServletRequest request) {
        OrderUserDevice device = new OrderUserDevice();
        device.setUserAgent(request.getHeader("User-Agent"));
        device.setClientIp(request.getRemoteAddr());
        device.setServerIp(request.getServerName());
        return device;
    }

    /**
     * 批量得到订单的状态与审核类型
     */
    @ResponseBody
    @RequestMapping(value = "/order/getStatusAndAuditType", method = RequestMethod.POST)
    @ApiOperation(value = "批量得到订单的状态与审核类型", notes = "批量得到订单的状态与审核类型", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<OrderVO>> getStatusAndAuditType(Pageable pageable,
                                                               @ApiParam(value = "订单ids") @RequestParam(required = false) List<String> ids,
                                                               HttpServletRequest req) {
        ArrayList result = new ArrayList();
        for (String id1 : ids) {
            String id = (String) id1;
            OrderVO vo = orderService.loadVO(id);
            // 默认订单审核由平台审核
            vo.setAuditType(AuditType.PLATFORM);
            // b2b经销商订单需要判断哪些订单需要平台审核或上级审核
            AuditRule auditRule = auditRuleService.selectByOrderId(id);
            if (auditRule != null) {
                vo.setAuditType(auditRule.getAuditType());
            }
            result.add(vo);
        }
        return new ResponseObject<List<OrderVO>>(result);
    }
}