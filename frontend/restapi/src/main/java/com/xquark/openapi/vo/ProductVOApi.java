package com.xquark.openapi.vo;

import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.service.product.vo.ProductVO;

import java.util.List;

/**
 * 汇购网根据商品id查询商品详情vo Created by chh on 17-2-16.
 */
public class ProductVOApi {

  private ProductVO productVO;
  private Shop shop;
  private List<FragmentVO> fragmentList;
  private List<String> imgList;

  public ProductVO getProductVO() {
    return productVO;
  }

  public void setProductVO(ProductVO productVO) {
    this.productVO = productVO;
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public List<FragmentVO> getFragmentList() {
    return fragmentList;
  }

  public void setFragmentList(List<FragmentVO> fragmentList) {
    this.fragmentList = fragmentList;
  }

  public List<String> getImgList() {
    return imgList;
  }

  public void setImgList(List<String> imgList) {
    this.imgList = imgList;
  }
}
