package com.xquark.openapi.scrm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by wangxinhua on 18-2-28. DESC:
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SCrmAddressUserForm extends BaseAuthForm {

  private String aId;

  private String unionId;

  public String getAId() {
    return aId;
  }

  public void setAId(String aId) {
    this.aId = aId;
  }

  public String getUnionId() {
    return unionId;
  }

  public void setUnionId(String unionId) {
    this.unionId = unionId;
  }
}
