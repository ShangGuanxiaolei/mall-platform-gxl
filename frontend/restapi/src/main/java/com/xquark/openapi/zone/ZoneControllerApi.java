package com.xquark.openapi.zone;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.model.Zone;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.zone.ZoneService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class ZoneControllerApi extends BaseController {

  @Autowired
  private ZoneService zoneService;

  @ResponseBody
  @RequestMapping(value = "/zone/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "获取某个id对应的地区信息", notes = "获取某个id对应的地区信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Zone> view(@PathVariable String id) {
    return new ResponseObject<>(zoneService.load(id));
  }


  @ResponseBody
  @RequestMapping(value = "/zone/{id}/parent", method = RequestMethod.POST)
  @ApiOperation(value = "获取某个id对应的地区的直接上级地区", notes = "获取某个id对应的地区的直接上级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Zone> parent(@PathVariable String id) {
    return new ResponseObject<>(zoneService.findParent(id));
  }

  /**
   * 获取zoneId的所有父级
   */
  @ResponseBody
  @RequestMapping(value = "/zone/{id}/parents", method = RequestMethod.POST)
  @ApiOperation(value = "获取某个id对应的地区的所有上级地区", notes = "获取某个id对应的地区的所有上级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Zone>> parents(@PathVariable String id) {
    return new ResponseObject<>(zoneService.listParents(id));
  }

  @ResponseBody
  @RequestMapping(value = "/zone/{id}/children", method = RequestMethod.POST)
  @ApiOperation(value = "获取某个id对应的地区的所有下级地区", notes = "获取某个id对应的地区的所有下级地区", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<Zone>> children(@PathVariable String id) {
    return new ResponseObject<>(zoneService.listChildren(id));
  }

}
