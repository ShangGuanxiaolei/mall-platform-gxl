package com.xquark.openapi.point;

import com.google.common.collect.ImmutableMap;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.model.GradeCode;
import com.xquark.dal.type.CodeNameType;
import com.xquark.helper.Transformer;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangxinhua on 2018/5/19. DESC: 积分规则接口
 */
@RestController
@RequestMapping("/points/grade")
public class PointGradeController {

//  private PointGradeService pointGradeService;
//
//  @Autowired
//  public void setPointGradeService(PointGradeService pointGradeService) {
//    this.pointGradeService = pointGradeService;
//  }
//
//  /**
//   * 查询积分规则
//   */
//  @RequestMapping(value = "/", method = RequestMethod.GET)
//  public ResponseObject<Map<String, Object>> listGrades(Pageable pageable) {
//    List<GradeCode> list = pointGradeService.list(pageable);
//    Long total = pointGradeService.count();
//    Map<String, Object> ret = ImmutableMap.of("list", list, "total", total);
//    return new ResponseObject<>(ret);
//  }
//
//  /**
//   * 保存积分规则
//   */
//  @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//  public ResponseObject<Boolean> save(@RequestBody @Validated PointGradeForm form, Errors errors) {
//    ControllerHelper.checkException(errors);
//    GradeCode grade = Transformer.fromBean(form, GradeCode.class);
//    grade.setCodeName(CodeNameType.fromCode(form.getPointType()));
//    boolean ret = pointGradeService.save(grade);
//    return new ResponseObject<>(ret);
//  }
//
//  /**
//   * 根据id查询
//   */
//  @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
//  public ResponseObject<GradeCode> loadById(@PathVariable String id) {
//    GradeCode grade = pointGradeService.load(id);
//    return new ResponseObject<>(grade);
//  }
//
//  /**
//   * 根据code查询
//   */
//  @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
//  public ResponseObject<GradeCode> loadByCode(@PathVariable String code) {
//    GradeCode grade = pointGradeService.loadByFunctionCode(code);
//    return new ResponseObject<>(grade);
//  }
//
//  /**
//   * 更新积分规则
//   */
//  @RequestMapping(value = "/", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
//  public ResponseObject<Boolean> updateById(@Validated PointGradeForm form, Errors errors) {
//    ControllerHelper.checkException(errors);
//    String id = form.getId();
//    if (StringUtils.isBlank(id)) {
//      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规则id不能为空");
//    }
//    GradeCode grade = Transformer.fromBean(form, GradeCode.class);
//    boolean ret = pointGradeService.update(grade);
//    return new ResponseObject<>(ret);
//  }
//
//  /**
//   * 根据id查询
//   */
//  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
//  public ResponseObject<GradeCode> getById(@PathVariable String id) {
//    GradeCode grade = pointGradeService.load(id);
//    return new ResponseObject<>(grade);
//  }
//
//  /**
//   * 根据id删除
//   */
//  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//  public ResponseObject<Boolean> deleteById(@PathVariable String id) {
//    boolean ret = pointGradeService.delete(id);
//    return new ResponseObject<>(ret);
//  }

}
