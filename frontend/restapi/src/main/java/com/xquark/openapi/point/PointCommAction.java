package com.xquark.openapi.point;

import com.hds.xquark.dal.type.Trancd;

/**
 * created by
 *
 * @author wangxinhua at 18-6-19 上午10:15
 */
public class PointCommAction {

  private final String code;

  private final Trancd trancd;

  public PointCommAction(String code, Trancd trancd) {
    this.code = code;
    this.trancd = trancd;
  }

  public String getCode() {
    return code;
  }

  public Trancd getTrancd() {
    return trancd;
  }
}
