package com.xquark.openapi.point;

import com.xquark.dal.status.PointGradeStatus;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * @author wangxinhua on 2018/5/19. DESC: 积分规则表单
 */
public class PointGradeForm {

  /**
   * id
   */
  private String id;

  /**
   * 功能代码
   */
  @NotBlank(message = "功能代码不能为空")
  private String functionCode;

  /**
   * 积分类型 GRANT - 发放, CONSUME - 消费, ROLLBACK - 回退, FREEZE - 冻结
   */
  @NotNull(message = "积分类型不能为空")
  @Range(min = 1, max = 4, message = "积分类型不在范围内")
  private Integer pointType;

  /**
   * 优先级, 从1开始计数
   */
  private Integer priority = 1;

  /**
   * 是否全局
   */
  private Boolean isGlobal = false;

  /**
   * 是否活动使用
   */
  private Boolean isActiveUse = false;

  /**
   * 积分
   */
  private Long point = 0L;

  /**
   * 是否应用公式
   */
  private Boolean isUseForMula = false;

  /**
   * 公式
   */
  private String formula = "";

  /**
   * 日上限
   */
  private Long dayUpperLimit = 0L;

  /**
   * 月上限
   */
  private Long monthUpperLimit = 0L;

  /**
   * 总上限
   */
  private Long totalUpperLimit = 0L;

  /**
   * 状态 ENABLE, DISABLE
   */
  private PointGradeStatus status = PointGradeStatus.ENABLE;

  @NotBlank(message = "积分描述不能为空")
  @Length(max = 200, message = "描述不能超过200个字符")
  private String description;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFunctionCode() {
    return functionCode;
  }

  public void setFunctionCode(String functionCode) {
    this.functionCode = functionCode;
  }

  public Integer getPointType() {
    return pointType;
  }

  public void setPointType(Integer pointType) {
    this.pointType = pointType;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public Boolean getIsGlobal() {
    return isGlobal;
  }

  public void setIsGlobal(Boolean global) {
    isGlobal = global;
  }

  public Boolean getIsActiveUse() {
    return isActiveUse;
  }

  public void setIsActiveUse(Boolean activeUse) {
    isActiveUse = activeUse;
  }

  public Long getPoint() {
    return point;
  }

  public void setPoint(Long point) {
    this.point = point;
  }

  public Boolean getIsUseForMula() {
    return isUseForMula;
  }

  public void setIsUseForMula(Boolean useForMula) {
    isUseForMula = useForMula;
  }

  public String getFormula() {
    return formula;
  }

  public void setFormula(String formula) {
    this.formula = formula;
  }

  public Long getDayUpperLimit() {
    return dayUpperLimit;
  }

  public void setDayUpperLimit(Long dayUpperLimit) {
    this.dayUpperLimit = dayUpperLimit;
  }

  public Long getMonthUpperLimit() {
    return monthUpperLimit;
  }

  public void setMonthUpperLimit(Long monthUpperLimit) {
    this.monthUpperLimit = monthUpperLimit;
  }

  public Long getTotalUpperLimit() {
    return totalUpperLimit;
  }

  public void setTotalUpperLimit(Long totalUpperLimit) {
    this.totalUpperLimit = totalUpperLimit;
  }

  public PointGradeStatus getStatus() {
    return status;
  }

  public void setStatus(PointGradeStatus status) {
    this.status = status;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
