package com.xquark.openapi.image;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.file.ImageUploadForm;
import com.xquark.restapi.file.ImageUploadValidator;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.file.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class ImageControllerApi {

  @Autowired
  private ImageService imageService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Value("${product.size.limit}")
  private String sizeLimit;

  private Logger log = LoggerFactory.getLogger(getClass());

  @InitBinder
  protected void initBinder(WebDataBinder binder) {
    binder.setValidator(new ImageUploadValidator());
  }

  /**
   * pc 上传图片/_f/u
   */
  @ResponseBody
  @RequestMapping(value = ResourceFacade.RES_PREFIX_UP_FILE + "/u", method = RequestMethod.POST)
  @ApiOperation(value = "上传本地图片", notes = "上传本地图片，返回qiniu相关信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<UpLoadFileVO>> upload(@Valid @ModelAttribute ImageUploadForm form,
      Errors errors, HttpServletRequest request)
      throws Exception {
    //上传文件大小控制
    Long sizeLongLimit = Long.valueOf(sizeLimit);
    List<MultipartFile> listFiles = form.getFile();
    for (MultipartFile file : listFiles) {
      long fileSize = file.getSize();
      if (fileSize > sizeLongLimit) {
        throw new BizException(GlobalErrorCode.UNKNOWN, ("新增图片大小不能超过" + sizeLongLimit + "字节"));
      }

    }
    if (form.getBelong() == null) {
      log.warn("upload warning 文件belong为空  file length=[" + form.getFile().size() + "]");
      RequestContext requestContext = new RequestContext(request);
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.fileBelong.message"));
    }
    ControllerHelper.checkException(errors);
    return new ResponseObject<>(
        resourceFacade.uploadFile(form.getFile(), form.getBelong()));
  }
}
