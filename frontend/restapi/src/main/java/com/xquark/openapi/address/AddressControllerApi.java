package com.xquark.openapi.address;

import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.dal.model.Address;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.address.AddressForm;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.zone.ZoneService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class AddressControllerApi extends BaseController {

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  /**
   * @param args
   * @throws IOException
   * @throws ClientProtocolException
   */
  public static void main(String[] args) throws ClientProtocolException, IOException {
    // 创建HttpClient实例
    HttpClient httpclient = new DefaultHttpClient();
    // 创建Get方法实例

    Map<String, String> map = new HashMap<>();
    map.put(IdTypeHandler.encode(214722), "187302");
    map.put(IdTypeHandler.encode(213286), "137987");

    map.put(IdTypeHandler.encode(214723), "187302");
    map.put(IdTypeHandler.encode(213373), "137987");

    map.put(IdTypeHandler.encode(214724), "187302");
    map.put(IdTypeHandler.encode(214738), "187302");
    map.put(IdTypeHandler.encode(213942), "137987");
    map.put(IdTypeHandler.encode(213943), "137987");
    map.put(IdTypeHandler.encode(214739), "187302");
    map.put(IdTypeHandler.encode(214740), "187302");

    map.put(IdTypeHandler.encode(213995), "137987");
    map.put(IdTypeHandler.encode(213996), "137987");

    for (String m : map.keySet()) {
      HttpGet httpgets = new HttpGet(
          "http://localhost:8888/v2/openapi/order/viewTest?id=" + m + "&extUid=" + map.get(m));
      System.out.println("aaaaaaaaaaaaaaaaaa" + map.get(m) + " id=" + m);
      httpgets.setHeader("Domain", "xiangqu");
      //        HttpHeaders
      HttpResponse response = httpclient.execute(httpgets);
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        InputStream instreams = entity.getContent();
        String str = convertStreamToString(instreams);
        System.out.println("Do something");
        System.out.println("str=" + str);
        // Do not need the rest
        httpgets.abort();
      }
    }
  }

  public static String convertStreamToString(InputStream is) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();

    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        is.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return sb.toString();
  }

  @ResponseBody
  @RequestMapping(value = "/address/save", method = RequestMethod.POST)
  @ApiOperation(value = "保存收货地址", notes = "适用于用户新增收货地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Address> save(@ModelAttribute AddressForm form, Errors errors) {
    Address address = new Address();
    address.setId(form.getId());
    address.setConsignee(form.getConsignee());
    address.setZoneId(form.getZoneId());
    address.setStreet(form.getStreet());
    address.setPhone(form.getPhone());
    address.setZipcode(form.getZipcode());
    address.setCommon(false);
    address.setIsDefault(form.getIsDefault() == null ? false : form.getIsDefault());
    address = addressService.saveUserAddress(address, true);
    return new ResponseObject<>(address);
  }

  @ResponseBody
  @RequestMapping(value = "/address/list", method = RequestMethod.POST)
  @ApiOperation(value = "查询用户所有收货地址", notes = "查询用户所有收货地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<List<AddressVO>> mine() {
    List<AddressVO> list = addressService.listUserAddressesVo();
    return new ResponseObject<>(list);
  }

  @ResponseBody
  @RequestMapping(value = "/address/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "根据id查询收货地址信息", notes = "根据id查询收货地址信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Address> view(@PathVariable String id) {
    Address address = addressService.loadUserAddress(id);
    return new ResponseObject<>(address);
  }


  @ResponseBody
  @RequestMapping(value = "/address/{id}/update", method = RequestMethod.POST)
  @ApiOperation(value = "根据id更新收货地址信息", notes = "根据id更新收货地址信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Address> update(@PathVariable String id, @ModelAttribute AddressForm form) {
    Address address = new Address();
    address.setId(id);
    BeanUtils.copyProperties(form, address);
    address = addressService.saveUserAddress(address, true);
    return new ResponseObject<>(address);
  }

  @ResponseBody
  @RequestMapping(value = "/address/{id}/as-default", method = RequestMethod.POST)
  @ApiOperation(value = "设置指定id的地址为用户默认收货地址", notes = "设置指定id的地址为用户默认收货地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> Default(@PathVariable String id) {
    return new ResponseObject<>(addressService.asDefault(id));
  }

  @ResponseBody
  @RequestMapping(value = "/address/{id}/delete", method = RequestMethod.POST)
  @ApiOperation(value = "删除指定id的收货地址", notes = "删除指定id的收货地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> delete(@PathVariable String id) {
    int flag = addressService.archiveAddress(id, true);
    return new ResponseObject<>(flag > 0);
  }

  @ResponseBody
  @RequestMapping(value = "/address/getDefault", method = RequestMethod.POST)
  @ApiOperation(value = "获取默认收货地址", notes = "获取默认收货地址", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<AddressVO> getDefault() {
    return new ResponseObject<>(addressService.getDefault());
  }
}
