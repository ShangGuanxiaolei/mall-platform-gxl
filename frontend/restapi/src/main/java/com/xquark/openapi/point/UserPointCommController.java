package com.xquark.openapi.point;

import com.google.common.collect.ImmutableMap;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.constrant.GradeCodeConstrants;
import com.hds.xquark.dal.constrant.PointConstrants;
import com.hds.xquark.dal.model.BasePointCommTotal;
import com.hds.xquark.dal.model.CommissionTotal;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.CommissionServiceApi;
import com.hds.xquark.service.point.PointCommService;
import com.hds.xquark.service.point.PointServiceApi;
import com.hds.xquark.service.point.helper.PointCommCalHelper;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.mapper.JobSchedulerLogMapper;
import com.xquark.dal.mapper.PointGiftDetailMapper;
import com.xquark.dal.model.JobSchedulerLog;
import com.xquark.dal.type.JobStatus;
import com.xquark.dal.type.JobType;
import com.xquark.restapi.BaseController;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.dto.PointSendType;
import com.xquark.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import static com.hds.xquark.dal.type.TotalAuditType.API;

/**
 * @author wangxinhua on 2018/5/18. DESC: 用户积分/德分接口
 */
@RestController
@RequestMapping("/openapi/users")
@SuppressWarnings("unused")
public class UserPointCommController extends BaseController {

  @Value("${deposit.dayofmonth}")
  private Integer dayOfMonth;

  private PointCommService pointCommService;

  private PointServiceApi pointService;

  @Autowired
  private PointGiftService pointGiftService;

  @Autowired
  private PointGiftDetailMapper pointGiftDetailMapper;

  private CommissionServiceApi commissionService;

  private JobSchedulerLogMapper jobSchedulerLogMapper;

  private CustomerProfileService customerProfileService;

  private final static Map<String, PointCommAction> POINT_ACTION_MAP = ImmutableMap.of(
      "CONSUME", new PointCommAction(GradeCodeConstrants.CONSUME_POINT_CODE, Trancd.DEDUCT_P),
      "CANCEL", new PointCommAction(GradeCodeConstrants.CANCEL_POINT_CODE, Trancd.DEDUCT_P),
      "REVERT", new PointCommAction(GradeCodeConstrants.RETURN_POINT_CODE, Trancd.DEDUCT_P)
  );

  private final static Map<String, PointCommAction> COMMISSION_ACTION_MAP = ImmutableMap.of(
      "CONSUME", new PointCommAction(GradeCodeConstrants.CONSUME_COMMISSION_CODE, Trancd.DEDUCT_C),
      "CANCEL", new PointCommAction(GradeCodeConstrants.CANCEL_COMMISSION_CODE, Trancd.DEDUCT_C),
      "REVERT", new PointCommAction(GradeCodeConstrants.RETURN_COMMISSION_CODE, Trancd.DEDUCT_C)
  );

  @Autowired
  public void setJobSchedulerLogMapper(JobSchedulerLogMapper jobSchedulerLogMapper) {
    this.jobSchedulerLogMapper = jobSchedulerLogMapper;
  }

  @Autowired
  public void setPointCommService(PointContextInitialize pointContextInitialize) {
    this.pointCommService = pointContextInitialize.getPointService();
  }

  @Autowired
  public void setCustomerProfileService(
      CustomerProfileService customerProfileService) {
    this.customerProfileService = customerProfileService;
  }

  @Autowired
  public void setPointService(PointContextInitialize pointContextInitialize) {
    this.pointService = pointContextInitialize.getPointServiceApi();
  }

  @Autowired
  public void setCommissionService(PointContextInitialize pointContextInitialize) {
    this.commissionService = pointContextInitialize.getCommissionServiceApi();
  }

  @RequestMapping(value = "/{cpId}/points/total", method = RequestMethod.GET)
  public ResponseObject<Map<String, BasePointCommTotal>> loadUserPointAndComm(
      @PathVariable Long cpId) {
    PointTotal pointTotal = pointCommService.loadOrBuildInfo(cpId, PointTotal.class);
    CommissionTotal commissionTotal = pointCommService.loadOrBuildInfo(cpId, CommissionTotal.class);
    BigDecimal withDrawTotal = pointCommService.sumTotal(GradeCodeConstrants.RELEASE_COMMISSION_CODE, cpId,
        Trancd.DEPOSIT_C,
        CommissionTotal.class);
    CommissionVOWrapper voWrapper = new CommissionVOWrapper(commissionTotal, withDrawTotal);
    Map<String, BasePointCommTotal> ret = ImmutableMap.of("pointTotal", pointTotal,
        "commissionTotal", voWrapper);
    return new ResponseObject<>(ret);
  }

  /**
   * 获取用户积分
   */
  @RequestMapping(value = "/{cpId}/points", method = RequestMethod.GET)
  public ResponseObject<PointTotal> loadUserPointInfo(@PathVariable Long cpId) {
    PointTotal userInfo = pointCommService.loadOrBuildInfo(cpId, PointTotal.class);
    return new ResponseObject<>(userInfo);
  }

  /**
   * 查询用户德分
   */
  @RequestMapping(value = "/{cpId}/commission", method = RequestMethod.GET)
  public ResponseObject<? extends CommissionTotal> loadUserCommInfo(@PathVariable Long cpId) {
    CommissionTotal userInfo = pointCommService.loadOrBuildInfo(cpId, CommissionTotal.class);
    BigDecimal withDrawTotal = pointCommService.sumTotal(GradeCodeConstrants.RELEASE_COMMISSION_CODE, cpId,
        Trancd.DEPOSIT_C,
        CommissionTotal.class);
    CommissionVOWrapper voWrapper = new CommissionVOWrapper(userInfo, withDrawTotal);
    return new ResponseObject<>(voWrapper);
  }

  /**
   * 修改德分
   */
  @RequestMapping(value = "/{cpId}/points/consume", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> consumePoint(@PathVariable Long cpId,
      @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      pointCommService
          .modifyPoint(cpId, form.getOrderId(), GradeCodeConstrants.CONSUME_POINT_CODE,
              form.getPlatform(),
              form.getPoints(), Trancd.DEDUCT_P, API);
    } catch (Exception e) {
      log.info("德分消费失败, cpId: {}, 待消费德分: {}", form.getPoints());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 取消德分
   */
  @RequestMapping(value = "/{cpId}/points/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> cancelPoint(@PathVariable Long cpId,
      @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      pointCommService
          .modifyPoint(cpId, form.getOrderId(), GradeCodeConstrants.CANCEL_POINT_CODE,
              form.getPlatform(),
              form.getPoints(), Trancd.DEDUCT_P, API);
    } catch (Exception e) {
      log.info("德分取消失败, cpId: {}, 待取消德分: {}", form.getPoints());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 取消积分
   */
  @RequestMapping(value = "/{cpId}/commission/consume", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> consumeCommission(@PathVariable Long cpId,
      @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      pointCommService
          .modifyCommission(cpId, form.getOrderId(), GradeCodeConstrants.CONSUME_COMMISSION_CODE,
              form.getPlatform(),
              form.getCommission(), Trancd.DEDUCT_C, API);
    } catch (Exception e) {
      log.info("积分消费失败, cpId: {}, 待取消积分: {}", form.getCommission());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 德分转积分
   */
  @RequestMapping(value = "/{cpId}/points/transfer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> transformPoint(@PathVariable Long cpId, @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      pointService.transform(cpId, form.getPoints(), PlatformType.fromCode(form.getSource()),
          PointConstrants.POINT_TO_COMM_RATE, commissionService);
    } catch (Exception e) {
      log.info("德分转积分失败, cpId {}， 待转积分", form.getPoints());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  @RequestMapping(value = "/{cpId}/commission/transfer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> transformComm(@PathVariable Long cpId, @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      commissionService.transform(cpId, form.getCommission(), PlatformType.fromCode(form.getSource()),
          PointConstrants.COMM_TO_POINT_RATE, pointService);
    } catch (Exception e) {
      log.info("积分转德分失败, cpId {}， 待转积分", cpId, form.getCommission());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 取消积分
   */
  @RequestMapping(value = "/{cpId}/commission/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> cancelCommission(@PathVariable Long cpId,
      @Validated @RequestBody UserPointForm form,
      Errors errors) {
    ControllerHelper.checkException(errors);
    try {
      pointCommService
          .modifyCommission(cpId, form.getOrderId(), GradeCodeConstrants.CANCEL_COMMISSION_CODE,
              form.getPlatform(),
              form.getCommission(), Trancd.DEDUCT_C, API);
    } catch (Exception e) {
      log.info("积分取消失败, cpId: {}, 待取消积分: {}", form.getCommission());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  @RequestMapping(value = "/{cpId}/commission/withdraw", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseObject<Boolean> withDrawCommission(@PathVariable Long cpId,
      @Validated @RequestBody UserPointForm form,
      Errors errors) {

    boolean isCpIdLegal = customerProfileService.isProfileExists(cpId);
    if (!isCpIdLegal) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户不存在, 无法提现");
    }

    BigDecimal targetCommission = form.getCommission();
    if (targetCommission == null || targetCommission.signum() <= 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "提现金额不能小于0");
    }
    CommissionTotal commissionTotal = pointCommService.loadCommByCpId(cpId);
    if (commissionTotal == null) {
      throw new BizException(GlobalErrorCode.WITH_DRAW_NOT_ENOUGH);
    }
    BigDecimal usableTotal = commissionTotal.getTotalUsable();
    // 小于提现下限
    if (usableTotal.compareTo(PointConstrants.WITH_DRAW_LIMIT) < 0) {
      throw new BizException(GlobalErrorCode.WITH_DRAW_LIMIT_NOT_ENOUGH);
    }

    // 提现平台积分不足
    // CONSUME消费策略默认会从当前平台开始链式扣减, 需要在此处校验第一个平台的积分是否足够提现
    BigDecimal usablePlatForm = PointCommCalHelper.getUsable(commissionTotal, form.getPlatform());
    if (usablePlatForm.compareTo(PointConstrants.WITH_DRAW_LIMIT) < 0) {
      throw new BizException(GlobalErrorCode.WITH_DRAW_LIMIT_NOT_ENOUGH);
    }

    if (usablePlatForm.compareTo(targetCommission) < 0) {
      throw new BizException(GlobalErrorCode.WITH_DRAW_NOT_ENOUGH_PLATFORM);
    }

    try {
      pointCommService
          .modifyCommission(cpId, "withdraw", GradeCodeConstrants.WITH_DRAW_COMMISSION_CODE,
              form.getPlatform(),
              form.getCommission(), Trancd.WITHDRAW_C, API);
    } catch (Exception e) {
      log.info("积分提现失败, cpId: {}, 待提现积分: {}", form.getCommission());
      throw new BizException(GlobalErrorCode.UNKNOWN, e.getMessage());
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  /**
   * 查询用户积分记录
   */
  @RequestMapping(value = "/{cpId}/points/records", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listRecords(@PathVariable Long cpId,
      Integer source,
      Pageable pageable) {
    Integer offset = null, size = null;
    if (pageable != null) {
      offset = pageable.getOffset();
      size = pageable.getPageSize();
    }
    Map<String, Object> ret = pointCommService.listPointRecords(cpId, source, offset, size);
    return new ResponseObject<>(ret);
  }

  @RequestMapping(value = "/{cpId}/commission/records", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listCommRecords(@PathVariable Long cpId,
      Integer source,
      Pageable pageable) {
    Integer offset = null, size = null;
    if (pageable != null) {
      offset = pageable.getOffset();
      size = pageable.getPageSize();
    }
//    Map<String, Object> ret = pointCommService.listCommissionRecords(cpId, source, offset, size);
    // TODO 过滤用户的积分记录(2019-04-24) -- order_head 表 from_cpId 不是当前用户不显示
    Map<String, Object> ret = pointCommService.filterCommissionRecords(cpId, source, offset, size);
    return new ResponseObject<>(ret);
  }

  @RequestMapping("/triggerWithdraw/{month}")
  public ResponseObject<Integer> triggerWithdraw(@PathVariable String month, @RequestParam
      Integer source) {
    String dateFormat = source == PlatformType.V.getCode() ? "yyyyMMdd" : "yyyyMM";
    Date date;
    try {
      date = org.apache.commons.lang3.time.DateUtils.parseDate(month, dateFormat);
    } catch (ParseException e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "日期格式不正确");
    }
    int year = DateUtils.getTimeUnit(date, Calendar.YEAR);
    int monthOfYear = DateUtils.getTimeUnit(date, Calendar.MONTH);
    int realDayOfMonth = source == PlatformType.V.getCode()
        ? DateUtils.getTimeUnit(date, Calendar.DAY_OF_MONTH) : dayOfMonth;

    Calendar calendar = new GregorianCalendar(year, monthOfYear, realDayOfMonth);
    int effected = pointCommService.translateCommSuspendingToWithdraw(calendar.getTime(),
        PlatformType.fromCode(source));
    return new ResponseObject<>(effected);
  }

  @RequestMapping("/triggerRelease/{type}")
  public ResponseObject<Boolean> triggerRelease(@PathVariable String type) {
    if (StringUtils.equalsIgnoreCase(type, "point")) {
      callWithLog(JobType.RELEASE_POINT);
    } else if (StringUtils.equalsIgnoreCase(type, "commission")) {
      callWithLog(JobType.RELEASE_COMMISSION);
    } else {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "发放类型不正确");
    }
    return new ResponseObject<>(Boolean.TRUE);
  }

  @RequestMapping("/withdraw/months")
  public ResponseObject<List<String>> listWithdrawTopMonth(PlatformType platform) {
    List<String> ret;
    if (platform == null) {
      ret = Collections.emptyList();
    } else {
      ret = pointCommService.listWithdrawTopDate(8, platform);
    }
    return new ResponseObject<>(ret);
  }

  /**
   * code 1 积分 code 2 德分 FIXME 消除重复代码
   * @param type
   */
  @SuppressWarnings("all")
  private void callWithLog(JobType type) {
    JobSchedulerLog log = JobSchedulerLog.empty(type);
    log.setStartDate(new Date());
    try {
      if (type == JobType.RELEASE_COMMISSION) {
        pointCommService.releaseCommission(API);
      } else if (type == JobType.RELEASE_POINT) {
        pointCommService.releasePoints(API);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (Exception e) {
      log.setExceptionMsg(e.getMessage());
      log.setJobStatus(JobStatus.ERROR.getCode());
      jobSchedulerLogMapper.insert(log);
      return;
    }
    log.setJobStatus(JobStatus.COMPLETED.getCode());
    log.setCompletedDate(new Date());
    jobSchedulerLogMapper.insert(log);
  }

  @RequestMapping(value = "/point/refund",method =RequestMethod.POST)
  public Boolean pointRefund(Long cpId,String bizId){
      BigDecimal pointPacket=pointGiftDetailMapper.selectPointPacketByOrderNo(bizId);
      boolean istrue=pointGiftService.modifyWithDetail(cpId,pointPacket,bizId, PointSendType.ORDER_REFUND);
      return istrue;
  }



}
