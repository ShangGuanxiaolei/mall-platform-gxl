/**
 * 关于商品的 open api
 */
package com.xquark.openapi.product;

import com.alibaba.fastjson.JSON;
import com.mangofactory.swagger.annotations.ApiIgnore;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.ProdSyncMapper;
import com.xquark.dal.mapper.ProductDistributorMapper;
import com.xquark.dal.model.Activity;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductDistributor;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.dal.vo.RolePriceVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.openapi.vo.ProductVOApi;
import com.xquark.restapi.ResponseObject;
import com.xquark.restapi.product.FragmentAndDescController;
import com.xquark.restapi.product.ProductVOEx;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.vo.ProductSearchVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopPostAgeService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.zone.ZoneService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
@RequestMapping(value = "/openapi")
@ApiIgnore
public class ProductControllerApi extends FragmentAndDescController {

	@Autowired
    private ShopPostAgeService shopPostAgeService;
	@Autowired
	private ZoneService zoneService;
	@Autowired
	private UrlHelper urlHelper;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProdSyncMapper prodSyncMapper;

    @Autowired
    private ShopTreeService shopTreeService;

    @Autowired
    private ProductDistributorMapper productDistributorMapper;
	
	@Value("${xiangqu.cart.host.url}")
    String xiangquCartHost;

    @Autowired
    private TinyUrlService tinyUrlService;

    @Autowired
    private UserAgentService userAgentService;

    @Autowired
    private RolePriceService rolePriceService;

    @Value("${site.web.host.name}")
    private String siteHost;

    /**
     * 总店下全部商品页
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/catalog/allProducts", method = RequestMethod.POST)
    @ApiOperation(value = "获取所有在售商品列表", notes = "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
            "排序传递order(cm代表利润，price代表价格),direction(asc,desc)", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<ProductVOEx>> allProducts(@ApiParam(value = "不需要传入") @RequestParam(value = "categoryId" ,required = false) String categoryId,
                                                         @ApiParam(value = "排序字段") @RequestParam(value = "order" ,required = false)  String order,
                                                         @ApiParam(value = "降序，升序") @RequestParam(value = "direction" ,required = false)  String direction,
                                                         Pageable pageable, HttpServletRequest req) {
        // 默认       default    销量
        // 利润高到低  cm
        // 价格高到低  price
        // 价格低到高  price
        List<Product> categoryProducts = new ArrayList<Product> ();
        direction = StringUtils.defaultIfBlank(direction, "desc").toLowerCase();
        order = StringUtils.defaultIfBlank(order, "default").toLowerCase();
        User user = getCurrentUser();
        String shopId = user.getShopId();
        String sellerShopId = user.getCurrentSellerShopId();
        ShopTree shopTree = shopTreeService.selectRootShopByShopId(shopId);
        String rootShopId = shopTree.getRootShopId();
        String groupon = "";
        if (StringUtils.isEmpty(categoryId) || categoryId.equals("0")) {
            categoryId = "";
        }

        if (order.equals("default")) {
            categoryProducts = productService.listProductsBySales(rootShopId, categoryId, groupon, pageable, Sort.Direction.fromString(direction), sellerShopId,null , "", "",
                null, null);
        }else if (order.equals("cm")) {
            categoryProducts = productService.listProductsByCm(rootShopId, categoryId, groupon, pageable, Sort.Direction.fromString(direction), sellerShopId);
        }else if (order.equals("price")) {
            categoryProducts = productService.listProductsByPrice(rootShopId, categoryId, groupon, pageable, Sort.Direction.fromString(direction), sellerShopId,null , "", "", new ProductSearchVO(), ProductType.NORMAL);
        }
        List<ProductVOEx> productVOExes = generateImgUrls(categoryProducts, shopId,req);
        return new ResponseObject<List<ProductVOEx>>(productVOExes);
    }

    /**
     * app选品商品查询
     *
     *
     * @param key
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/product/search", method = RequestMethod.POST)
    @ApiOperation(value = "所有在售商品搜索", notes = "分页传递page(0代表第几页),size(10代表一页显示多少条数据)，pageable(true)\n" +
            "key代表搜索关键字", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<ProductVOEx>> search(@ApiParam(value = "搜索关键字") @RequestParam(value = "key" ,required = false)  String key, Pageable page, HttpServletRequest req) {
        Map<String, Object> map = new HashMap<String, Object>();
        String shopId = getCurrentIUser().getShopId();
        String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
        List<Product> list = productService.search(rootShopId, key, page, ProductType.NORMAL);
        List<ProductVOEx> voList = new ArrayList<ProductVOEx>();
        for (int i = 0; i < list.size(); i++) {
            Product product = list.get(i);
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()), product.getImg(), null);
            vo.setImgUrl(product.getImg());

            if(product.getCommissionRate()!=null) {
                voEX.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            //商品卖家端是否上架

            //Product productSource = productMapper.selectProductBySourceProductIdAndShopId(product.getSourceProductId(),shopId);
            ProductDistributor productDistributor = productDistributorMapper.selectByProductIdAndShopId(product.getId(), shopId);
            if(productDistributor!=null){
                voEX.setOnSaleForSeller(true);
            }else{
                voEX.setOnSaleForSeller(false);
            }
            //多少卖家在卖
            //long count = productDistributorMapper.countProductsSellerById(product.getSourceProductId());
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            voEX.setCountSeller(count);

            //voEX.setCountSeller(productMapper.countProductsSellerById(product.getId()));

            voEX.setProductUrl(siteHost + "/p/" + voEX.getId() + "?union_id=" + getCurrentUser().getId());

            voList.add(voEX);

        }
        map.put("list", voList);
        Long count = productService.CountTotalByName(shopId, key);
        map.put("categoryTotal", count);
        return new ResponseObject<List<ProductVOEx>>(voList);
    }

    private List<ProductVOEx> generateImgUrls(List<Product> products,String shopId,HttpServletRequest req) {
        List<ProductVOEx> exs = new ArrayList<ProductVOEx>();
        ProductVOEx ex = null;
        Activity activity = null;
        if (products == null) return exs;
        for (Product product : products) {

            ex = new ProductVOEx(new ProductVO(product), getProductShareUrl(req,product.getCode()),
                    product.getImg(), activity == null ? null : activity.getName());

            //fixme
            if(product.getCommissionRate()!=null) {
                ex.setCommission(product.getCommissionRate().multiply(product.getPrice()));
            }
            //商品卖家端是否上架
            ProductDistributor productDistributor = productDistributorMapper.selectByProductIdAndShopId(product.getId(), shopId);

            //有记录并且状态为上架
            if(productDistributor!=null && ProductStatus.ONSALE.equals(productDistributor.getStatus())){
                ex.setOnSaleForSeller(true);
            }else{
                ex.setOnSaleForSeller(false);
            }
            //多少卖家在卖
            long count = productDistributorMapper.countProductsSellerByProductId(product.getId());
            ex.setCountSeller(count);


            // 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
            // 利润为商品原价和经销商价格之差
            String productId = product.getId();
            String userId = this.getCurrentIUser().getId();
            //判断当前用户是否在代理角色中
            UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
            if(userAgentVO != null){
                String role = userAgentVO.getRole();
                RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
                if(rolePriceVO != null){
                    BigDecimal oriPrice = ex.getPrice();
                    BigDecimal rolePrice = rolePriceVO.getPrice();
                    ex.setPrice(rolePrice);
                    ex.setCommission(oriPrice.subtract(rolePrice));
                }
            }

            // 获取商品的sku信息
            ProductVO productVO = productService.load(productId, Boolean.TRUE);
            ex.setSkus(productVO.getSkus());

            //ex.setProductUrl(siteHost + "/p/" + ex.getId() + "?union_id=" + getCurrentUser().getId());
            exs.add(ex);
        }
        return exs;
    }


    /**
     * 查看商品详情
     * @param productId
     * @param req
     * @param resp
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/product/findById", method = RequestMethod.POST)
    @ApiOperation(value = "查看商品详情", notes = "根据productId返回商品详情相关的vo", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<ProductVOApi> findById(@ApiParam(value = "商品id") @RequestParam(value = "productId" ,required = false) String productId, HttpServletRequest req, HttpServletResponse resp) {
        ProductVOApi productVOApi = new ProductVOApi();
        String userId = getCurrentUser().getId();
        // 联合创始人或董事类型的经销商，上级默认为总部
        UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
        AgentType agentType = userAgentVO.getType();
        if(agentType == AgentType.DIRECTOR || agentType == AgentType.FOUNDER){
            getCurrentUser().setCurrentSellerShopId(userAgentService.getRootShopId());
        }else{
            String parentUserId = userAgentVO.getParentUserId();
            getCurrentUser().setCurrentSellerShopId(userService.load(parentUserId).getShopId());
        }
        // 获取商品Vo
        String realShopId = getCurrentUser().getCurrentSellerShopId();
        ProductVO productVO = productService.load(productId, Boolean.TRUE);
        productVO.setShopId(realShopId);
        if (productVO == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, new RequestContext(req).getMessage("proudct.not.found"));
        }

        // 判断商品是否在b2b经销商定价中
        checkB2bPurchase(productVO);

        // FIXME:out-sync case should not handle in here
        // need better control for distribution product info sync on writting but not reading
        // check source product
        Product prod = productService.findProductById(productId);
        log.info("sync dist prod" + JSON.toJSONString(prod));
        log.info("source prod id" + prod.getSourceProductId());
        if (!prod.getSourceProductId().isEmpty()) {
            // sync source product status and amount
            Product srcProduct = productService.findProductById(prod.getSourceProductId());
            if (srcProduct != null) {
                productVO.setStatus(srcProduct.getStatus());
                productVO.setAmount(srcProduct.getAmount());
                log.info("sync status" + srcProduct.getStatus() + " and amount:" + srcProduct.getAmount());
            }
        }

        productVOApi.setProductVO(productVO);

        // 获取店铺
        Shop shop = shopService.load(productVO.getShopId());



        if (shop == null || shop.getArchive() == Boolean.TRUE) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, new RequestContext(req).getMessage("shop.not.found"));
        }
        productVOApi.setShop(shop);

        // 是否开启片段
        List<FragmentVO> list = getProductFragmentList(productId);
        productVOApi.setFragmentList(list);
        if(!(list.size() > 0)) {
            List<String> imgList = imgsList(productId);
            productVOApi.setImgList(imgList);
        }
        return new ResponseObject<ProductVOApi>(productVOApi);
    }


    /**
     * 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
     * @param productVO
     */
    private void checkB2bPurchase(ProductVO productVO){
        String productId = productVO.getId();
        String userId = this.getCurrentUser().getId();
        //判断当前用户是否在代理角色中
        UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
        if(userAgentVO != null){
            String role = userAgentVO.getRole();
            RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
            if(rolePriceVO != null){
                productVO.setPrice(rolePriceVO.getPrice());
            }
        }
    }

    private List<String> imgsList(String productId) {
        List<String> imgsList = new ArrayList<String>();
        List<ProductImage> imgs = productService.requestImgs(String.valueOf(IdTypeHandler.decode(productId)), "");
        for(ProductImage img : imgs){
            imgsList.add(resourceFacade.resolveUrl(img.getImg() + "|" + ResourceFacade.IMAGE_S1));
        }

        return imgsList;
    }

    /**
     * 判断商品是否在b2b经销商定价中，如果是商品价格为经销商角色定价
     * @param cartItem
     */
    private void checkB2bPurchase(CartItemVO cartItem){
        String productId = cartItem.getProduct().getId();
        String userId = this.getCurrentUser().getId();
        //判断当前用户是否在代理角色中
        UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
        if(userAgentVO != null){
            String role = userAgentVO.getRole();
            RolePriceVO rolePriceVO = rolePriceService.selectByProductAndRole(productId, role);
            if(rolePriceVO != null){
                cartItem.getSku().setPrice(rolePriceVO.getPrice());
            }
        }
    }


    private String getProductShareUrl(HttpServletRequest req,String code) {
        String key = tinyUrlService.insert(req.getScheme() + "://" + req.getServerName() + "/p/" + code + "?currentSellerShopId=" + getCurrentUser().getShopId());
        return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
    }

    /**
     * b2b进货模式，所有商品的详情页url
     * @param req
     * @param code
     * @return
     */
    private String getB2bProductShareUrl(HttpServletRequest req,String code) {
        // b2b进货模式，所有商品的卖家默认为此用户的上级shop
        String shopId = getCurrentUser().getShopId();
        String parentShopId = shopTreeService.selectDirectParent(shopTreeService.selectRootShopByShopId(shopId).getRootShopId(), shopId).getAncestorShopId();
        String key = tinyUrlService.insert(req.getScheme() + "://" + req.getServerName() + "/p/purchase/" + code + "?currentSellerShopId=" + parentShopId);
        return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
    }

    private List<ProductVOEx> generateImgUrl(List<Product> products,String categoryId){
        List<ProductVOEx> exs = new ArrayList<ProductVOEx>();
        ProductVOEx ex = null;
        for(Product product : products){
            ex = new ProductVOEx(new ProductVO(product), urlHelper.genProductUrl(product.getId()), product.getImg(), null);
            //ex.setCategory(categoryService.loadCategoryByProductId(product.getId()));
            ex.setCategory(categoryService.selectVoById(categoryId));
            exs.add(ex);
        }
        return exs;
    }
}
