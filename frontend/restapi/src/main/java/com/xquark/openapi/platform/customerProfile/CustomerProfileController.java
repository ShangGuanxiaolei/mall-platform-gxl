package com.xquark.openapi.platform.customerProfile;

import com.google.common.base.Function;
import com.xquark.dal.anno.SCrmAuth;
import com.xquark.dal.model.CustomerMessage;
import com.xquark.dal.type.CareerTotalType;
import com.xquark.dal.type.PlatformType;
import com.xquark.dal.vo.CustomerByPhone;
import com.xquark.helper.Transformer;
import com.xquark.openapi.scrm.CustomerForm400;
import com.xquark.restapi.ResponseObject;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.xquark.service.platform.impl.CustomerProfileServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * created by
 *
 * @author wangxinhua at 18-6-7 下午1:39
 */
@RestController
@RequestMapping("/openapi/customers")
public class CustomerProfileController {

  private final static Logger LOGGER = LoggerFactory.getLogger(CustomerProfileController.class);

  private final CustomerProfileService customerProfileService;

  private final Function<CustomerMessage, CustomerMessageVO> MESSAGE_TRANSFORM_FUNC =
      new Function<CustomerMessage, CustomerMessageVO>() {
        @Override
        public CustomerMessageVO apply(CustomerMessage customerMessage) {
          return new CustomerMessageVO(customerMessage.getMessage());
        }
      };

  @Autowired
  public CustomerProfileController(
      CustomerProfileService customerProfileService) {
    this.customerProfileService = customerProfileService;
  }

  @RequestMapping(value = "/{cpId}/lighten", method = RequestMethod.POST)
  public ResponseObject<Boolean> lighten(@PathVariable("cpId") Long cpId) {
    CustomerProfileServiceImpl.LightenResult result =
            customerProfileService.lightenWrapped(cpId, PlatformType.V);

    return new ResponseObject<>(
            result.isSuccess(),result.getMessage(),result.getCode()
    );

//    boolean ret = customerProfileService.lighten(cpId, PlatformType.V);
//    return new ResponseObject<>(ret);
  }

  @RequestMapping(value = "/{cpId}/messages", method = RequestMethod.GET)
  public ResponseObject<List<CustomerMessageVO>> readMessages(@PathVariable("cpId") Long cpId,
      @RequestParam(required = false) String msgCode,
      @RequestParam(required = false) Integer source,
      @RequestParam(required = false) Integer limit) {
    List<CustomerMessage> messages = customerProfileService.readUnReadMessages(cpId,
        msgCode, source, limit);
    List<CustomerMessageVO> messageVOS = Transformer.fromIterable(messages, MESSAGE_TRANSFORM_FUNC);
    return new ResponseObject<>(messageVOS);
  }

  /**
   * 查询业绩
   *
   * @param cpId cpId
   * @param month 格式为yyyy-mm
   * @return 业绩map
   */
  @RequestMapping(value = "/{cpId}/career/{month}", method = RequestMethod.GET)
  public ResponseObject<Map<CareerTotalType, String>> listCareer(@PathVariable Long cpId,
      @PathVariable String month) {
    Map<CareerTotalType, String> retailMap = customerProfileService
        .listCareerTotal(cpId, month);
    return new ResponseObject<>(retailMap);
  }

  @RequestMapping(value = "/findCustomer", consumes = MediaType.APPLICATION_JSON_VALUE)
  @SCrmAuth
  public ResponseObject<List<CustomerByPhone>> getCustomer(@RequestBody @Validated CustomerForm400 form, Errors errors) {
    ResponseObject<List<CustomerByPhone>> customer = new ResponseObject<>();
    try {
      List<CustomerByPhone> customerByPhones = customerProfileService.getCustomerByTel(form.getPhone());
      customer.setData(customerByPhones);
      if (customerByPhones == null || customerByPhones.size() == 0) {
        customer = new ResponseObject<>("该用户不存在", GlobalErrorCode.USER_NOT_EXIST);
      }
    } catch (Exception e) {
      LOGGER.error("查找customer错误", e);
      customer = new ResponseObject<>("错误", GlobalErrorCode.USER_NOT_EXIST);
    }
    return customer;
  }
}
