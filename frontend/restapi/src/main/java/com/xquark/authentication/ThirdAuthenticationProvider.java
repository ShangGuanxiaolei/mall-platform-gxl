package com.xquark.authentication;

import com.xquark.biz.verify.VerificationFacade;
import com.xquark.biz.verify.impl.VerificationFacadeImpl;
import com.xquark.dal.model.User;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.user.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import static com.xquark.service.error.GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR;

/**
 * 汇购网用户调用restapi登录验证</br>
 *
 * 根据汇购网传递过来的Host,ExtUid,IP,SecretKey四个值按顺序拼接成新字符串，使用SHA1签名算法生成签名Sign
 * 与汇购网的sign进行对比，成功则将ExtUid对应的用户设置登陆态
 *
 * Created by chh on 17/1/20.
 */
public class ThirdAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  private VerificationFacade veriFacade;

  private UserService userService;

  public void setVeriFacade(VerificationFacade veriFacade) {
    this.veriFacade = veriFacade;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  private String SecretKey = "Xquark";

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String mobilePhoneNumber =
        (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
    String mobilePhoneVerifyCode = appAuthenticationToken.getMobilePhoneVerifyCode();

    boolean verified = veriFacade
        .verifyCode(mobilePhoneNumber, mobilePhoneVerifyCode, VerificationFacadeImpl.SmsType.SIGN);

    if (!verified) {
      throw new AppAuthenticationException(MOBILE_PHONE_VERIFY_CODE_ERROR.render());
    }
  }

  @Override
  protected UserDetails retrieveUser(String username,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    //忽略该请求
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String extUserId = appAuthenticationToken.getExtUserId();
    if (StringUtils.isEmpty(extUserId)) {
      throw new SkippingAuthenticationException("skipping third provider:");
    }

    String host = appAuthenticationToken.getHost();
    String ip = appAuthenticationToken.getIp();
    String sign = appAuthenticationToken.getSign();

    String signValue = host + extUserId;
    if (org.apache.commons.lang3.StringUtils.isNotEmpty(ip)) {
      signValue = signValue + ip;
    }
    signValue = signValue + SecretKey;
    String signature = SHA1Util.Sha1((signValue));

    // 判断签名是否正确
    if (!signature.equals(sign)) {
      throw new AppAuthenticationException("签名错误");
    }

    // 判断是否有对应代理用户
    User user = userService.loadExtUserByUid(extUserId);
    if (user == null) {
      throw new AppAuthenticationException("获取用户失败");
    }

      String userType=userService.selectIdTypeByExtUid(extUserId);
      if(null==userType || ("").equals(userType) || ("RC").equals(userType)){
          user.setIdentityName(null);
      }
      if("DS".equals(userType)){
          user.setIdentityName("会员");
      }
      if("SP".equals(userType)){
          user.setIdentityName("代理");
      }

      //在APP中的卖家,将当前的访问店铺自动设置为卖家自身的shopid
    user.setCurrentSellerShopId(user.getShopId());

    return user;
  }

  public static void main(String args[]) {
    String host = "hgw.51shop.mobi";
    String ip = "";
    String extUserId = "1";
    //String sign = appAuthenticationToken.getSign();

    String signValue = host + extUserId;
    if (org.apache.commons.lang3.StringUtils.isNotEmpty(ip)) {
      signValue = signValue + ip;
    }
    signValue = signValue + "Xquark";
    System.out.println("signValue = " + signValue);
    String signature = SHA1Util.Sha1((signValue));
    System.out.println("signature = " + signature);
  }
}
