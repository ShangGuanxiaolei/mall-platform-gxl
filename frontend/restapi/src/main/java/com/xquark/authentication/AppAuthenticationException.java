package com.xquark.authentication;

import org.springframework.security.authentication.AccountStatusException;

/**
 * Spring security 自定义组件的验证错误 Created by dongsongjie on 16/9/30.
 */
public class AppAuthenticationException extends AccountStatusException {

  public AppAuthenticationException(String msg) {
    super(msg);
  }

  public AppAuthenticationException(String msg, Throwable t) {
    super(msg, t);
  }
}
