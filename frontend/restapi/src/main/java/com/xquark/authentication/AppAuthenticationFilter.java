package com.xquark.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 该Filter替代了默认的FORM_LOGIN_FILTER,作用是替代默认的账号密码获取方式 增加对于第三方登录,手机验证码登录的参数解析并提交给authentication provider
 * 最终处理登录验证</br>
 *
 * <li>所有基于RestAPI的用户登录,目前适用于客户端登录</li>
 * <li>验证方式: 账号,手机号,第三方账号登录</li>
 *
 * Created by dongsongjie on 16/9/27.
 */
public class AppAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

  private static String MOBILE_PHONE_VERIFY_CODE = "mobilePhoneVerifyCode";

  private static String WECHAT_OAUTH_CODE = "wechatOauthCode";

  private static String AUTH_TYPE = "authType";

  public Authentication attemptAuthentication(HttpServletRequest request,
      HttpServletResponse response) throws AuthenticationException {

    //登录请求限定必须使用POST方法
    if (!request.getMethod().equals("POST")) {
      throw new AuthenticationServiceException(
          "Authentication method not supported: "
              + request.getMethod());
    }

    String authType = request.getParameter(AUTH_TYPE);
    if (StringUtils.isBlank(authType)) {
      throw new AuthenticationServiceException(
          "该请求未设置验证方式.");
    }

    String username = obtainUsername(request);
    String mobilePhoneVerifyCode = request.getParameter(MOBILE_PHONE_VERIFY_CODE);
    String wechatOauthCode = request.getParameter(WECHAT_OAUTH_CODE);
    String password = obtainPassword(request);

    // 汇购网用户调用restapi传递过来的参数
    String host = request.getHeader("Host");
    String extUserId = request.getParameter("ExtUid");
    String ip = request.getParameter("IP");
    String sign = request.getParameter("Sign");

    String cpId = request.getParameter("cpId");
    String cpToken = request.getParameter("cpToken");

    if (username == null) {
      username = "";
    }

    if (password == null) {
      password = "";
    }

    if (mobilePhoneVerifyCode == null) {
      mobilePhoneVerifyCode = "";
    }

    if (wechatOauthCode == null) {
      wechatOauthCode = "";
    }

    if (StringUtils.isEmpty(cpId)) {
      cpId = null;
    }
    Long cpIdL = cpId == null ? null : Long.valueOf(cpId);

    if (cpToken == null) {
      cpToken = "";
    }

    // 2017-10-23 新增小程序登录
    AppAuthenticationToken authRequest = new AppAuthenticationToken(
        username, password, mobilePhoneVerifyCode, wechatOauthCode, authType, host, extUserId, ip,
        sign, cpIdL, cpToken);
    setDetails(request, authRequest);
    return this.getAuthenticationManager().authenticate(authRequest);
  }

}
