package com.xquark.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

/**
 * 所有调用RestAPI验证时,需要用到的所有参数 Created by dongsongjie on 16/9/27.
 */
public class AppAuthenticationToken extends UsernamePasswordAuthenticationToken {


  /**
   * 登录验证的类型参数,value参考  {@link AuthenticationType}
   */
  private String authType;

  /**
   * 手机短信验证码
   */
  private String mobilePhoneVerifyCode;

  /**
   * 第三方:APP使用微信第三方登录的临时code,用于在服务端获取access_token,进而创建或验证用户信息
   */
  private String wechatOauthCode;

  // 汇购网用户调用restapi传递过来的参数
  private String host;
  private String extUserId;
  private String ip;
  private String sign;

  private Long cpId;
  private String cpToken;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getExtUserId() {
    return extUserId;
  }

  public void setExtUserId(String extUserId) {
    this.extUserId = extUserId;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getAuthType() {
    return authType;
  }

  public void setAuthType(String authType) {
    this.authType = authType;
  }

  public String getMobilePhoneVerifyCode() {
    return mobilePhoneVerifyCode;
  }

  public void setMobilePhoneVerifyCode(String mobilePhoneVerifyCode) {
    this.mobilePhoneVerifyCode = mobilePhoneVerifyCode;
  }

  public String getWechatOauthCode() {
    return wechatOauthCode;
  }

  public void setWechatOauthCode(String wechatOauthCode) {
    this.wechatOauthCode = wechatOauthCode;
  }

  public Long getCpId() {
    return cpId;
  }

  public void setCpId(Long cpId) {
    this.cpId = cpId;
  }

  public String getCpToken() {
    return cpToken;
  }

  public void setCpToken(String cpToken) {
    this.cpToken = cpToken;
  }

  public AppAuthenticationToken(Object principal, Object credentials, String mobilePhoneVerifyCode,
      String wechatOauthCode, String authType
      , String host, String extUserId, String ip, String sign, Long cpId, String cpToken) {
    super(principal, credentials);
    this.mobilePhoneVerifyCode = mobilePhoneVerifyCode;
    this.wechatOauthCode = wechatOauthCode;
    this.authType = authType;
    this.host = host;
    this.extUserId = extUserId;
    this.ip = ip;
    this.sign = sign;
    this.cpId = cpId;
    this.cpToken = cpToken;
  }

}
