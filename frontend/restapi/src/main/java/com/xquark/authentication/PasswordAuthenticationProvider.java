package com.xquark.authentication;

import com.xquark.dal.model.User;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.xquark.authentication.AuthenticationType.PASSWORD;
import static com.xquark.service.error.GlobalErrorCode.*;

/**
 * Created by wangxinhua on 17-6-7. DESC:
 */
public class PasswordAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  private UserService userService;
  private PasswordEncoder pwdEncoder;

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public void setPwdEncoder(PasswordEncoder pwdEncoder) {
    this.pwdEncoder = pwdEncoder;
  }

  @Override
  protected final void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    String presentedPassword = authentication.getCredentials().toString();
    String storedPassword = userDetails.getPassword();
    // 客户端需要显示登录失败消息，不显示错误码
    if (StringUtils.isBlank(presentedPassword)) {
      this.logger.debug("登录失败， 登录密码为空");
      throw new BadCredentialsException(BAD_CREDENTIALS.getError());
    }

    if (StringUtils.isBlank(storedPassword)) {
      this.logger.debug("登录失败， 未设置初始密码");
      throw new BadCredentialsException(BAD_CREDENTIALS.getError());
    }

    if (!this.pwdEncoder.matches(presentedPassword, storedPassword)) {
      this.logger.debug("密码错误");
      throw new BadCredentialsException(PASSWORD_VERIFY_ERROR.getError());
    }

  }

  @Override
  protected final UserDetails retrieveUser(String username,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    //忽略该请求
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String authType = appAuthenticationToken.getAuthType();
    if (!PASSWORD.name().equalsIgnoreCase(authType)) {
      throw new SkippingAuthenticationException("skipping current provider:" + PASSWORD.name());
    }

    //FIXME dongsongjie 需要手机号查询唯一用户的方法,本方法不可靠
    User user = userService.loadAnonymousByPhone(username);

    String userType=userService.selectIdTypeByPhone(username);
    if(null==userType || ("").equals(userType) || ("RC").equals(userType)){
      user.setIdentityName(null);
    }
    if("DS".equals(userType)){
      user.setIdentityName("会员");
    }
    if("SP".equals(userType)){
      user.setIdentityName("代理");
    }
    if (user == null) {
      throw new AppAuthenticationException(MOBILE_PHONE_USER_NOT_EXIST.render());
    }

    if (user == null) {
      throw new AppAuthenticationException(MOBILE_PHONE_USER_NOT_EXIST.render());
    }

    //在APP中的卖家,将当前的访问店铺自动设置为卖家自身的shopid
    user.setCurrentSellerShopId(user.getShopId());

    return user;
  }
}
