package com.xquark.authentication;

import com.xquark.biz.util.UserUtil;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import com.xquark.utils.EmojiFilter;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.oauth.OAuthException;
import com.xquark.wechat.oauth.OAuthManager;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenRequest;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenResponse;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoRequest;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.xquark.authentication.AuthenticationType.WECHAT;
import static com.xquark.service.error.GlobalErrorCode.WECHAT_OAUTH_CODE_NOT_VAILD;

/**
 * 微信第三方登录验证 Created by dongsongjie on 16/9/27.
 */
public class WechatAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  //TODO dongsongjie load by properties or db
  private static final String WECHAT_SECRET = "0bd91a60a818994dd3b8fd0e9b6a64df";
  private static final String WECHAT_APPID = "wxefd9e702b53e9256";

  private PasswordEncoder pwdEncoder;
  private UserService userService;
  private ShopService shopService;

  private TwitterShopComService twitterShopComService;

  private ShopAccessLogService shopAccessLogService;

  private ShopTreeService shopTreeService;

  private UserTwitterService userTwitterService;

  public void setUserTwitterService(UserTwitterService userTwitterService) {
    this.userTwitterService = userTwitterService;
  }

  public void setTwitterShopComService(TwitterShopComService twitterShopComService) {
    this.twitterShopComService = twitterShopComService;
  }

  public void setShopAccessLogService(ShopAccessLogService shopAccessLogService) {
    this.shopAccessLogService = shopAccessLogService;
  }

  public void setShopTreeService(ShopTreeService shopTreeService) {
    this.shopTreeService = shopTreeService;
  }


  public void setPwdEncoder(PasswordEncoder pwdEncoder) {
    this.pwdEncoder = pwdEncoder;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public void setShopService(ShopService shopService) {
    this.shopService = shopService;
  }

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    //通过retrieveUser方法, 拿到临时code, 已经校验了微信第三方登录APP
  }

  /**
   * 只根据code参数去创建或者查询获取当前微信User
   */
  @Override
  protected UserDetails retrieveUser(String username,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String authType = appAuthenticationToken.getAuthType();
    if (!WECHAT.name().equalsIgnoreCase(authType)) {
      throw new SkippingAuthenticationException("skipping current provider:" + WECHAT.name());
    }

    String wechatOauthCode = appAuthenticationToken.getWechatOauthCode();
    User user = retrieveUser0(wechatOauthCode);

    return user;
  }

  private User retrieveUser0(String wechatOauthCode) {
    Config config = new Config("", "", "", WECHAT_APPID, WECHAT_SECRET, "", "", "", "");
    GetAccessTokenRequest getAccessTokenRequest = new GetAccessTokenRequest(config,
        wechatOauthCode);

    try {
      GetAccessTokenResponse res = OAuthManager.getAccessToken(getAccessTokenRequest);
      GetUserinfoRequest userinfo = new GetUserinfoRequest(config, res.getAccess_token(),
          res.getOpenid());
      GetUserinfoResponse userInfoRes = OAuthManager.getUserinfo(userinfo);

      //根据oauth返回的结果查询数据,如果有此用户则返回,如果没有则自动创建该用户并执行注册新卖家的业务流程
      User user = userService.loadByUnionId(userInfoRes.getUnionId());
      if (user == null) {
        //FIXME liangfan 创建用户及推客店铺, 包在一个service方法里面, 保证事务一致, User表增加wechat_unionid字段
        user = new com.xquark.dal.model.User();

        String nickname = EmojiFilter.filterEmoji(userInfoRes.getNickName());
        // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
        if (nickname == null || "".equals(nickname)) {
          nickname = "普通用户";
        }
        user.setName(nickname);
        user.setLoginname(userInfoRes.getOpenId());
        user.setPassword(pwdEncoder.encode(userInfoRes.getOpenId()));
        user.setAvatar(userInfoRes.getAvatar());
        user.setWeixinCode(WECHAT_APPID);
        user.setWithdrawType(3); //微信提现
        user.setWechatName(nickname);
        // 设置用户unionid，这样其他开放平台下的应用（如小程序），该微信号均能登陆
        if (StringUtils.isNotEmpty(userInfoRes.getUnionId())) {
          user.setUnionId(userInfoRes.getUnionId());
        }
        userService.insert(user);

        // app微信登陆，创建用户的同时也要创建对应的店铺
        Shop shop = new Shop();
        shop.setName(nickname + "的小店");
        shop.setWechat(nickname);
        shop.setOwnerId(user.getId());
        shop.setImg(userInfoRes.getAvatar());
        shopService.create(shop);

        user.setShopId(shop.getId());
      }

      String userType=userService.selectIdTypeByUnionId(userInfoRes.getUnionId());
      if(null==userType || ("").equals(userType) || ("RC").equals(userType)){
        user.setIdentityName(null);
      }
      if("DS".equals(userType)){
        user.setIdentityName("会员");
      }
      if("SP".equals(userType)){
        user.setIdentityName("代理");
      }
      // 获取当前用户的卖家信息
      UserUtil util = new UserUtil();
      util.setCurrentSellerShopId(user, "", twitterShopComService, shopAccessLogService,
          shopTreeService, shopService, userTwitterService);

      return user;
    } catch (OAuthException e) {
      throw new AppAuthenticationException(WECHAT_OAUTH_CODE_NOT_VAILD.render() + ";微信返回错误," + e);
    }
  }
}
