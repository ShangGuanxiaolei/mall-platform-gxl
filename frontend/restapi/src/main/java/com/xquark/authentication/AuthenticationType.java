package com.xquark.authentication;

/**
 * Created by dongsongjie on 16/9/27.
 */
public enum AuthenticationType {

  MOBILE_PHONE,
  WECHAT,
  PASSWORD,
  CUSTOMER_PROFILE

}
