package com.xquark.authentication;

import org.springframework.security.core.AuthenticationException;

/**
 * 跳过当前Provider Created by dongsongjie on 16/9/27.
 */
public class SkippingAuthenticationException extends AuthenticationException {

  public SkippingAuthenticationException(String msg) {
    super(msg);
  }
}
