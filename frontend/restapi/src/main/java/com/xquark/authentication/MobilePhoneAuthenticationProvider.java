package com.xquark.authentication;

import com.xquark.biz.util.UserUtil;
import com.xquark.biz.verify.VerificationFacade;
import com.xquark.biz.verify.impl.VerificationFacadeImpl;
import com.xquark.dal.model.User;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import static com.xquark.authentication.AuthenticationType.MOBILE_PHONE;
import static com.xquark.service.error.GlobalErrorCode.MOBILE_PHONE_USER_NOT_EXIST;
import static com.xquark.service.error.GlobalErrorCode.MOBILE_PHONE_VERIFY_CODE_ERROR;

/**
 * 手机号登录验证</br>
 *
 * <li>实现对于短信验证码的校验</li>
 * <li>未实现对于用户密码的校验,实现方式需参考
 * {@link org.springframework.security.authentication.dao.DaoAuthenticationProvider#additionalAuthenticationChecks(UserDetails,
 * UsernamePasswordAuthenticationToken)}
 * </li>
 *
 * Created by dongsongjie on 16/9/27.
 */
public class MobilePhoneAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  private VerificationFacade veriFacade;

  private UserService userService;

  private TwitterShopComService twitterShopComService;

  private ShopAccessLogService shopAccessLogService;

  private ShopTreeService shopTreeService;

  private ShopService shopService;

  private UserTwitterService userTwitterService;

  public void setUserTwitterService(UserTwitterService userTwitterService) {
    this.userTwitterService = userTwitterService;
  }

  public void setTwitterShopComService(TwitterShopComService twitterShopComService) {
    this.twitterShopComService = twitterShopComService;
  }

  public void setShopAccessLogService(ShopAccessLogService shopAccessLogService) {
    this.shopAccessLogService = shopAccessLogService;
  }

  public void setShopTreeService(ShopTreeService shopTreeService) {
    this.shopTreeService = shopTreeService;
  }

  public void setShopService(ShopService shopService) {
    this.shopService = shopService;
  }

  public void setVeriFacade(VerificationFacade veriFacade) {
    this.veriFacade = veriFacade;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String mobilePhoneNumber =
        (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
    String mobilePhoneVerifyCode = appAuthenticationToken.getMobilePhoneVerifyCode();

    boolean verified = veriFacade
        .verifyCode(mobilePhoneNumber, mobilePhoneVerifyCode, VerificationFacadeImpl.SmsType.SIGN);

    if (!verified) {
      // 客户端不显示错误码
      throw new AppAuthenticationException(MOBILE_PHONE_VERIFY_CODE_ERROR.getError());
    }
  }

  @Override
  protected UserDetails retrieveUser(String username,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    //忽略该请求
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String authType = appAuthenticationToken.getAuthType();
    if (!MOBILE_PHONE.name().equalsIgnoreCase(authType)) {
      throw new SkippingAuthenticationException("skipping current provider:" + MOBILE_PHONE.name());
    }

    //FIXME dongsongjie 需要手机号查询唯一用户的方法,本方法不可靠
    User user = userService.loadAnonymousByPhone(username);

    String userType=userService.selectIdTypeByPhone(username);
    if(null==userType || ("").equals(userType) || ("RC").equals(userType)){
      user.setIdentityName(null);
    }
    if("DS".equals(userType)){
      user.setIdentityName("会员");
    }
    if("SP".equals(userType)){
      user.setIdentityName("代理");
    }
    if (user == null) {
      throw new AppAuthenticationException(MOBILE_PHONE_USER_NOT_EXIST.render());
    }

    // 获取当前用户的卖家信息
    UserUtil util = new UserUtil();
    util.setCurrentSellerShopId(user, "", twitterShopComService, shopAccessLogService,
        shopTreeService, shopService, userTwitterService);

    return user;
  }


}
