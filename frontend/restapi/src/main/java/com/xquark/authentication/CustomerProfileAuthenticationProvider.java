package com.xquark.authentication;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.model.CustomerCareerLevel;
import com.xquark.dal.model.User;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.utils.EncryptUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import static com.xquark.authentication.AuthenticationType.CUSTOMER_PROFILE;
import static com.xquark.service.error.GlobalErrorCode.CP_USER_NOT_EXISTS;

/**
 * created by
 *
 * @author wangxinhua at 18-6-25 下午9:35
 */
public class CustomerProfileAuthenticationProvider extends
    AbstractUserDetailsAuthenticationProvider {

  private final static Logger LOGGER = LoggerFactory
      .getLogger(CustomerProfileAuthenticationProvider.class);

  private final UserService userService;

  private final ShopService shopService;

  private final CareerLevelService careerLevelService;

  private final CustomerProfileService customerProfileService;

  public CustomerProfileAuthenticationProvider(UserService userService,
      ShopService shopService, CareerLevelService careerLevelService,
      CustomerProfileService customerProfileService) {
    this.userService = userService;
    this.shopService = shopService;
    this.careerLevelService = careerLevelService;
    this.customerProfileService = customerProfileService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String authType = appAuthenticationToken.getAuthType();
    if (!CUSTOMER_PROFILE.name().equalsIgnoreCase(authType)) {
      throw new SkippingAuthenticationException("skipping current provider:"
          + CUSTOMER_PROFILE.name());
    }
    return super.authenticate(authentication);
  }

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    String code = appAuthenticationToken.getWechatOauthCode();
    String cpToken = appAuthenticationToken.getCpToken();
    LOGGER.info("==== 开始customerProfile验证, code: {}, cpToken: {} ====", code, cpToken);

    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
    String key = "cpToken:" + code;
    // 校验token
    String dbToken = redisUtils.get(key);
    if (StringUtils.isBlank(dbToken) || !StringUtils.equals(cpToken, dbToken)) {
      LOGGER.error("cpToken: {} dbToken: {} 不匹配", cpToken, dbToken);
      throw new BadCredentialsException(GlobalErrorCode.CP_TOKEN_EXPIRED_OR_NOT_EXIST
          .getError());
    }
    // 消费掉key
    EncryptUtil des1;
    String unionId;
    try {
      des1 = new EncryptUtil();
      unionId = des1.decrypt(cpToken);
    } catch (Exception e) {
      logger.error("解密cpToken异常");
      throw new BadCredentialsException(GlobalErrorCode.UNKNOWN.getError());
    }
    // FIXME 在此处消费会导致后续流程异常后无法cpToken无效
    Long afterCpId = ((AppAuthenticationToken) authentication).getCpId();
    redisUtils.del(key);
    // 在登录会话期间缓存用户的unionId
    // 在logoutSuccess处移除该id
    LOGGER.info("==== 用户 [{}] customerProfile登录成功 ==== unionId: {}", afterCpId, unionId);
    redisUtils.set(String.valueOf(afterCpId)
        , unionId);
  }

  @Override
  protected UserDetails retrieveUser(String username,
      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    AppAuthenticationToken appAuthenticationToken = (AppAuthenticationToken) authentication;
    Long cpId = appAuthenticationToken.getCpId();
    User user = userService.loadByCpId(cpId);

    String userType=userService.selectIdType(cpId.toString());
    if(null==userType || ("").equals(userType) || ("RC").equals(userType)){
      user.setIdentityName(null);
    }
    if("DS".equals(userType)){
      user.setIdentityName("会员");
    }
    if("SP".equals(userType)){
      user.setIdentityName("代理");
    }

    logger.debug("cpId " + cpId + " 登录中");
    if (user == null) {
      logger.error("cpId " + cpId + " 登录失败, 未找到用户");
      throw new AppAuthenticationException(CP_USER_NOT_EXISTS.render());
    }
    user.setRootSellerShopId(shopService.loadRootShop().getId());
    setIdentity(user);
    return user;
  }

  /**
   * 登录成功后在用户上下文中存放用户的身份信息
   */
  private void setIdentity(User user) {
    Long cpId = user.getCpId();
    CustomerCareerLevel customerCareerLevel;
    boolean hasIdentity;
    boolean isLighten;
    try {
      customerCareerLevel = careerLevelService.load(cpId);
      // 是否有身份
      hasIdentity = customerProfileService.hasIdentity(cpId);
      // 是否点亮
      isLighten = customerProfileService.isLighten(cpId);
    } catch (Exception e) {
      logger.error("查询用户 " + cpId + " 身份信息失败");
      return;
    }
    user.setHasIdentity(hasIdentity);
    user.setIsLighten(isLighten);
    if (customerCareerLevel != null) {
      String sp = customerCareerLevel.getViviLifeType();
      if ("SP".equals(sp)) {
        user.setIsShopOwner(true);
      }
      //是全国代理或店主，则显示代理价
      if (customerCareerLevel.getIsLightening() ||
          (!customerCareerLevel.getIsLightening() && "SP".equals(sp))) {
        user.setIsProxy(true);
      }
      if (user.getHasIdentity()) { //是vip或超级会员，则显示会员价
        user.setIsMember(true);
      }
    }
  }

}
