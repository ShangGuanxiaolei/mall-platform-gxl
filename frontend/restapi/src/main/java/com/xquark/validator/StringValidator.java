package com.xquark.validator;

import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author wangxinhua
 * 枚举参数校验
 */
public class StringValidator implements ConstraintValidator<Strings, String> {

  private List<String> valueList;

  @Override
  public void initialize(Strings constraintAnnotation) {
    valueList = new ArrayList<>();
    for (String val : constraintAnnotation.acceptedValues()) {
      valueList.add(val.toUpperCase());
    }
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return valueList.contains(value.toUpperCase());
  }

}