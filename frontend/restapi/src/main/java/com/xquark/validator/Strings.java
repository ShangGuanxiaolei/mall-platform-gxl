package com.xquark.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE,
    ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Constraint(validatedBy = {StringValidator.class})
public @interface Strings {

  String[] acceptedValues();

  String message() default "参数不在范围内";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}