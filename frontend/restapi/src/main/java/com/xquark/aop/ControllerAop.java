package com.xquark.aop;

import com.google.common.base.Objects;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.openapi.scrm.BaseAuthForm;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * Created by wangxinhua on 17-12-18. DESC:
 */
@Component
@Aspect
public class ControllerAop {

  // 全局校验key
  private static final String SCRM_KEY = "HVMALL";


  @Pointcut("this(com.xquark.restapi.BaseController)")
  @SuppressWarnings("unused")
  public void baseControllerLayer() {
  }

//  @Pointcut("within(com.xquark.openapi.scrm.* || com.xquark.openapi.platform.customerProfile.*)")
  @Pointcut("within(com.xquark.openapi.platform.customerProfile.*)")
  public void scrmLayer() {
  }

  @Pointcut("@annotation(com.xquark.dal.anno.SCrmAuth)")
  public void sCrmAuthLayer() {
  }

  @Before(value = "scrmLayer() && sCrmAuthLayer() && args(authForm, errors)", argNames = "authForm, errors")
  public void sCrmAuthCheck(BaseAuthForm authForm, Errors errors) {
    ControllerHelper.checkException(errors);
    String timeStamp = Objects.firstNonNull(authForm.getTimestamp(), "");
    String signature = Objects.firstNonNull(authForm.getSignature(), "");

    // 校验签名
    String calSignature = DigestUtils.md5Hex(SCRM_KEY.concat(timeStamp));
    if (!StringUtils.equalsIgnoreCase(signature, calSignature)) {
      throw new BizException(GlobalErrorCode.SIGN_ERROR, "签名错误");
    }
  }

}