package com.xquark.config;

import com.vdlm.common.bus.BusSignalManager;
import com.xquark.authentication.AppAuthenticationFilter;
import com.xquark.authentication.AppTokenBasedRememberMeServices;
import com.xquark.authentication.CustomerProfileAuthenticationProvider;
import com.xquark.authentication.MobilePhoneAuthenticationProvider;
import com.xquark.authentication.PasswordAuthenticationProvider;
import com.xquark.authentication.ThirdAuthenticationProvider;
import com.xquark.authentication.TokenAuthenticationFilter;
import com.xquark.authentication.WechatAuthenticationProvider;
import com.xquark.biz.authentication.DBLoginStrategyBuilder;
import com.xquark.biz.authentication.DomainBasedLoginUrlEntryPoint;
import com.xquark.biz.authentication.LoginConfigurationException;
import com.xquark.biz.authentication.LoginStrategyBuilder;
import com.xquark.biz.authentication.LoginUrlEntryPoint;
import com.xquark.biz.verify.VerificationFacade;
import com.xquark.filter.MyAuthenticationSuccessHandler;
import com.xquark.service.platform.CareerLevelService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImplEx;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebMvcSecurity
@ImportResource("classpath:META-INF/spring-security-context.xml")
class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  DataSource dataSource;

  @Autowired
  UserService userService;

  @Autowired
  CustomerProfileService customerProfileService;

  @Autowired
  CareerLevelService careerLevelService;

  @Autowired
  VerificationFacade veriFacade;

  @Autowired
  PasswordEncoder pwdEncoder;

  @Autowired
  ShopService shopService;

  @Autowired
  TwitterShopComService twitterShopComService;

  @Autowired
  ShopAccessLogService shopAccessLogService;

  @Autowired
  ShopTreeService shopTreeService;

  @Autowired
  UserTwitterService userTwitterService;

  @Value("${rememberMeServices.key}")
  String remMeKey;

  @Value("${rememberMeServices.token.calidity.seconds}")
  int validSeconds;

  @Value("${rememberMeServices.token.concurrency.safe.seconds}")
  int concurrencySafeSeconds;

  @Value("${site.webapi.host.name}")
  String siteHost;

  @Bean
  public PersistentTokenRepository persistentTokenRepository() {
    return new JdbcTokenRepositoryImplEx(dataSource);
  }

  @Bean
  public RememberMeServices rememberMeServices() {
    AppTokenBasedRememberMeServices svc = new AppTokenBasedRememberMeServices(remMeKey,
        userService);
    svc.setUserService(this.userService);
    svc.setShopService(this.shopService);
    svc.setAlwaysRemember(true);
    svc.setTokenValiditySeconds(validSeconds);
    svc.setCookieName("AppAuthToken");
    if (this.siteHost.startsWith("http://")) {
      svc.setUseSecureCookie(false);
    } else if (this.siteHost.startsWith("https://")) {
      svc.setUseSecureCookie(true);
    }
    return svc;
  }

  @Autowired
  @Bean
  public LoginStrategyBuilder loginStrategyBuilder(BusSignalManager bsm) {
    return new DBLoginStrategyBuilder(bsm, "xquark-rest-api");
  }


  @Autowired
  @Bean(name = "loginEntryPoint")
  public LoginUrlEntryPoint loginEntryPoint(LoginStrategyBuilder loginStrategyBuilder)
      throws LoginConfigurationException {
    return new DomainBasedLoginUrlEntryPoint("/signin", loginStrategyBuilder);
  }

  @Bean
  @Autowired
  public TokenAuthenticationFilter tokenAuthenticationFilter(@Qualifier("jsonRedisTemplate")
      RedisTemplate<String, Object> redisTemplate) {
    return new TokenAuthenticationFilter(redisTemplate);
  }

  @Autowired
  @Bean(name = "appAuthenticationFilter")
  public AppAuthenticationFilter appAuthenticationFilter(
      MyAuthenticationSuccessHandler successHandler, RememberMeServices rememberMeServices) {

    MobilePhoneAuthenticationProvider mobilePhoneAuthenticationProvider = new MobilePhoneAuthenticationProvider();
    mobilePhoneAuthenticationProvider.setUserService(this.userService);
    mobilePhoneAuthenticationProvider.setVeriFacade(this.veriFacade);
    mobilePhoneAuthenticationProvider.setShopService(shopService);
    mobilePhoneAuthenticationProvider.setShopAccessLogService(shopAccessLogService);
    mobilePhoneAuthenticationProvider.setShopTreeService(shopTreeService);
    mobilePhoneAuthenticationProvider.setTwitterShopComService(twitterShopComService);
    mobilePhoneAuthenticationProvider.setUserTwitterService(userTwitterService);

    WechatAuthenticationProvider wechatAuthenticationProvider = new WechatAuthenticationProvider();
    wechatAuthenticationProvider.setUserService(this.userService);
    wechatAuthenticationProvider.setShopService(this.shopService);
    wechatAuthenticationProvider.setPwdEncoder(this.pwdEncoder);
    wechatAuthenticationProvider.setShopAccessLogService(shopAccessLogService);
    wechatAuthenticationProvider.setShopTreeService(shopTreeService);
    wechatAuthenticationProvider.setTwitterShopComService(twitterShopComService);
    wechatAuthenticationProvider.setUserTwitterService(userTwitterService);

    ThirdAuthenticationProvider thirdAuthenticationProvider = new ThirdAuthenticationProvider();
    thirdAuthenticationProvider.setUserService(this.userService);
    thirdAuthenticationProvider.setVeriFacade(this.veriFacade);

    PasswordAuthenticationProvider passwordAuthenticationProvider = new PasswordAuthenticationProvider();
    passwordAuthenticationProvider.setUserService(this.userService);
    passwordAuthenticationProvider.setPwdEncoder(this.pwdEncoder);

    CustomerProfileAuthenticationProvider customerProfileAuthenticationProvider
        = new CustomerProfileAuthenticationProvider(this.userService, shopService,
        careerLevelService, customerProfileService);

    List<AuthenticationProvider> providers = new ArrayList<>();
    providers.add(mobilePhoneAuthenticationProvider);
    providers.add(wechatAuthenticationProvider);
    providers.add(passwordAuthenticationProvider);
    providers.add(thirdAuthenticationProvider);
    providers.add(customerProfileAuthenticationProvider);

    AppAuthenticationFilter ret = new AppAuthenticationFilter();

    ret.setRememberMeServices(rememberMeServices);
    ret.setAuthenticationManager(new ProviderManager(providers));
    ret.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/signin_check", "POST"));
    ret.setAuthenticationSuccessHandler(successHandler);
    ret.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler("/signin_fail"));

    ret.setUsernameParameter("u");
    ret.setPasswordParameter("p");
    return ret;
  }

}