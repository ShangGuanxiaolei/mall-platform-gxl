package com.xquark.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Iterator;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by chh on 17-1-12.
 */
public class MySessionListener implements HttpSessionListener {

  private Logger log = LoggerFactory.getLogger(getClass());

  public void sessionCreated(HttpSessionEvent event) {
    HttpSession session = event.getSession();
    ServletContext application = session.getServletContext();

    // 在application范围由一个HashSet集保存所有的session
    HashSet sessions = (HashSet) application.getAttribute("sessions");
    if (sessions == null) {
      sessions = new HashSet();
      application.setAttribute("sessions", sessions);
    }

    // 新创建的session均添加到HashSet集中
    try {
      sessions.add(session);
    } catch (Exception e) {
      log.error("seesion create error", e);
    }

    // 可以在别处从application范围中取出sessions集合

    // 然后使用sessions.size()获取当前活动的session数，即为“在线人数”
  }

  public void sessionDestroyed(HttpSessionEvent event) {
    HttpSession session = event.getSession();
    ServletContext application = session.getServletContext();
    HashSet sessions = (HashSet) application.getAttribute("sessions");
    HttpSession removeSes = null;
    // 销毁的session均从HashSet集中移除
    try {
      for (Object session1 : sessions) {
        HttpSession ses = (HttpSession) session1;
        if (ses != null && session != null && ses.getId().equals(session.getId())) {
          removeSes = ses;
        }
      }
      sessions.remove(removeSes);
    } catch (Exception e) {
      log.error("seesion destroyed error", e);
    }
  }
}
