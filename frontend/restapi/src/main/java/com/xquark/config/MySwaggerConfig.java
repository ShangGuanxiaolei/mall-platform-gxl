package com.xquark.config;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;

/**
 * Created by xiaohui on 2016/1/14.
 */
//@Configuration
//@EnableSwagger
public class MySwaggerConfig {

  private SpringSwaggerConfig springSwaggerConfig;

  /**
   * Required to autowire SpringSwaggerConfig
   */
//    @Autowired
//    public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig)
//    {
//        this.springSwaggerConfig = springSwaggerConfig;
//    }

  /**
   * Every SwaggerSpringMvcPlugin bean is picked up by the swagger-mvc
   * framework - allowing for multiple swagger groups i.e. same code base
   * multiple swagger resource listings.
   */
//    @Bean
//    public SwaggerSpringMvcPlugin customImplementation()
//    {
//        return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
//                .apiInfo(apiInfo())
//                .includePatterns("/.*");
//    }
//
//    private ApiInfo apiInfo()
//    {
//        ApiInfo apiInfo = new ApiInfo(
//                "沁园商城系统restapi接口",
//                "提供详细的后台Rest接口",
//                "caohonghui@healthsource.com.cn",
//                "caohonghui@healthsource.com.cn",
//                "佰益源信息科技有限公司",
//                "mailto:account@healthsource.com.cn");
//        return apiInfo;
//    }
}