package com.xquark.interceptor;

import java.lang.reflect.Method;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xquark.cache.TokenCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TokenInterceptor extends HandlerInterceptorAdapter {

  @Autowired
  private TokenCache tokenCache;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    if (handler instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) handler;
      Method method = handlerMethod.getMethod();
      Token annotation = method.getAnnotation(Token.class);
      if (annotation != null) {
        boolean needSaveSession = annotation.save();
        if (needSaveSession) {
          request.getSession(false).setAttribute("token", UUID.randomUUID().toString());
        }
        boolean needRemoveSession = annotation.remove();
        if (needRemoveSession) {
          if (isRepeatSubmit(request)) {
            return false;
          }
          request.getSession(false).removeAttribute("token");
        }
      }
      return true;
    } else {
      return super.preHandle(request, response, handler);
    }
  }

  private boolean isRepeatSubmit(HttpServletRequest request) {
    String clinetToken = request.getParameter("token");
    if (clinetToken == null) {
      return true;
    }
    String serverToken = (String) tokenCache.load(clinetToken);
    if (serverToken == null) {
      return true;
    }
    if (!serverToken.equals(clinetToken)) {
      return true;
    }
    tokenCache.remove(clinetToken);
    return false;
  }
}
