package com.xquark.interceptor;

import com.xquark.utils.MD5Util;
import com.xquark.wechat.util.MD5;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * User: a9175
 * Date: 2018/6/17.  菠萝派 商城签名算法
 * Time: 16:44
 */

public class UserSignature {

  public Boolean getSignature(String method, String appkey, String token, String bizcontent,
      String sign) {
        String AppSecret = "5ee2084de90043be989d4d99d0dd0eaa"; //appSecret的值通过在菠萝派中申请应用获得

    String SignatureName = AppSecret + "appkey" + appkey + "bizcontent" + bizcontent +
        "method" + method + "token" + token + "" + AppSecret;
        String newsign = MD5Util.md5(SignatureName.toLowerCase());//转化成小写后获取MD5值
        System.out.println(newsign);
        if (appkey.equals("")) {   //Appkey不能为空
//            return "{\"code\": \"400001\", \"message\": \"System Error\",\"subcode\": "
//                + "\"GSE.SYSTEM_ERROR\",\"submessage\": \"[20887]服务异常请稍后再试\"}";
          return false;
        } else if (!sign.equals(newsign)) {  //验证失败

//            return "{\"code\": \"400002\", \"message\": \"System Error\",\"subcode\": "
//                + "\"GSE.SYSTEM_ERROR\",\"submessage\": \"[20887]服务异常请稍后再试\"}";
          return false;
        } else {   //验证成功
//            return "{\"code\": \"10000\", \"message\": \"SUCCESS\",\"subcode\": "
//                + "\"SUCCESS\",\"submessage\": \"验证成功\"}";
          return true;
        }

    }


}
