package com.xquark.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserSigninLog;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.userAgent.impl.UserSigninLogFactory;

public class MyAuthenticationSuccessHandler implements
    AuthenticationSuccessHandler {

  @Autowired
  private UserSigninLogService userSigninLogService;

  private String defaultTargetUrl;

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
      HttpServletResponse response, Authentication authentication)
      throws IOException, ServletException {

    User user = (User) authentication.getPrincipal();
    UserSigninLog log = UserSigninLogFactory.createUserSigninLog(request, user);
    userSigninLogService.insert(log);

    response.sendRedirect(request.getContextPath() + defaultTargetUrl);
  }

  public String getDefaultTargetUrl() {
    return defaultTargetUrl;
  }

  public void setDefaultTargetUrl(String defaultTargetUrl) {
    this.defaultTargetUrl = defaultTargetUrl;
  }
}
