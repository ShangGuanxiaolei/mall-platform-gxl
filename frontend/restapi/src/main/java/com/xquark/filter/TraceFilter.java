package com.xquark.filter;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.model.User;
import com.xquark.service.user.UserService;

public class TraceFilter extends OncePerRequestFilter {

  private Logger log = LoggerFactory.getLogger(getClass());
  public static final String TRACE_ID = "trace_id";

  @Autowired
  private UserService userService;

  @Override
  protected void doFilterInternal(HttpServletRequest request,
      HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String requestURI = request.getRequestURI();

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Object principal = auth.getPrincipal();
    if (principal != null && principal instanceof User) {
      User user = (User) principal;
      String trace = "TRACE: user[" + user.getLoginname() + "] : " + requestURI;
      if (StringUtils.isNotEmpty(request.getQueryString())) {
        trace += "?" + request.getQueryString();
      }
      log.info(trace);
    } else {
      String trace = "TRACE: anonymouse user access: " + requestURI;
      if (StringUtils.isNotEmpty(request.getQueryString())) {
        trace += "?" + request.getQueryString();
      }
      log.info(trace);
    }
    log.info("Params: " + JSON.toJSONString(request.getParameterMap()));
    MDC.put(TRACE_ID, UUID.randomUUID().toString());
    filterChain.doFilter(request, response);
  }
}
