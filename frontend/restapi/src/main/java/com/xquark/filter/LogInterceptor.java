package com.xquark.filter;

/**
 * Created by chh on 17-1-16. 统计每个restapi请求花费时间
 */

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.NamedThreadLocal;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 日志拦截器
 *
 * @author chh
 * @version 2017-01-16
 */
public class LogInterceptor implements HandlerInterceptor {

  protected Logger logger = LoggerFactory.getLogger(getClass());

  private static final ThreadLocal<Long> startTimeThreadLocal =
      new NamedThreadLocal<>("ThreadLocal StartTime");

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) throws Exception {

    if (logger.isInfoEnabled()) {
      long beginTime = System.currentTimeMillis();//1、开始时间
      startTimeThreadLocal.set(beginTime);    //线程绑定变量（该数据只有当前请求的线程可见）
      logger.info("开始计时: {}  URI: {}", new SimpleDateFormat("hh:mm:ss.SSS")
          .format(beginTime), request.getRequestURI());
    }
    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {
    if (modelAndView != null) {
      if (logger.isInfoEnabled()) {
        logger.info("ViewName: " + modelAndView.getViewName());
      }
    }
  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) throws Exception {

    // 打印JVM信息。
    if (logger.isInfoEnabled()) {
      long beginTime = startTimeThreadLocal.get();//得到线程绑定的局部变量（开始时间）
      long endTime = System.currentTimeMillis();  //2、结束时间
      logger.info("计时结束：{}  耗时：{}  URI: {}  最大内存: {}m  已分配内存: {}m  已分配内存中的剩余空间: {}m  最大可用内存: {}m",
          new SimpleDateFormat("hh:mm:ss.SSS").format(endTime), (endTime - beginTime) + "毫秒",
          request.getRequestURI(), Runtime.getRuntime().maxMemory() / 1024 / 1024,
          Runtime.getRuntime().totalMemory() / 1024 / 1024,
          Runtime.getRuntime().freeMemory() / 1024 / 1024,
          (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory() + Runtime
              .getRuntime().freeMemory()) / 1024 / 1024);
    }

  }

}
