package com.xquark.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.intercept.RunAsManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.xquark.service.user.UserService;

public class RunAsFilter extends OncePerRequestFilter {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private RunAsManager runAsManager;

  @Autowired
  private UserService userService;

  private String SecretKey = "Xquark";

  @Override
  protected void doFilterInternal(HttpServletRequest request,
      HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    WebApplicationContext webApplicationContext = WebApplicationContextUtils
        .getWebApplicationContext(getServletContext());
    UserService userService = (UserService) webApplicationContext.getBean("userService");
    String requestURI = request.getRequestURI();
    String uri = requestURI.replace(request.getContextPath(), "");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    // 代理申请接口不需要获取当前用户
    /**if(uri.startsWith("/openapi/") && uri.indexOf("openapi/userAgent/apply4Admin") == -1){
     // 汇购网请求restapi数据时，通过Host,ExtUid,IP,SecretKey四个值按顺序拼接成新字符串，使用SHA1签名算法生成签名Sign，IP如没有则无需拼接
     // 判断请求是否合法，如果合法且存在对应代理用户，则默认对应代理用户登陆
     if((auth == null || auth.getPrincipal() == null || !(auth.getPrincipal() instanceof  User))){
     String host = request.getHeader("Host");
     String extUserId = request.getParameter("ExtUid");
     String ip = request.getParameter("IP");
     String sign = request.getParameter("Sign");

     // 获取请求是从哪里来的
     String referer = request.getHeader("referer");
     // 如果是swagger_ui过来的请求,即swagger中的测试请求,则默认一个用户进行登陆,用于测试数据返回
     if (referer != null && referer.indexOf("swagger_ui") != -1) {
     host = "51shop.mobi";
     extUserId = "1";
     ip = "";
     sign = "4834bc8304db41fdaca29f34d97318cd9fc9d9e1";
     }

     String signValue = host + extUserId;
     if(StringUtils.isNotEmpty(ip)){
     signValue = signValue + ip;
     }
     signValue = signValue + SecretKey;
     String signature = SHA1Util.Sha1((signValue));

     // 判断签名是否正确
     if(!signature.equals(sign)) {
     request.setAttribute("errorType" , GlobalErrorCode.SIGN_ERROR);
     log.error("签名错误 host = " + host + " extUserId = " + extUserId + " IP = " + ip + " sign = " + sign);
     throw new ServletException("签名错误");
     }
     if(StringUtils.isNotBlank(extUserId)){
     User user = userService.loadExtUserByUid(extUserId);
     if(user == null){
     request.setAttribute("errorType" , GlobalErrorCode.USER_NOT_EXIST);
     log.error("获取用户失败 host = " + host + " extUserId = " + extUserId + " IP = " + ip + " sign = " + sign);
     throw new ServletException("获取用户失败");
     }
     auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
     SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
     SecurityContextHolder.getContext().setAuthentication(auth);
     }
     }
     }**/

    // Attempt to run as a different user
//        Authentication runAs = runAsManager.buildRunAs(user.getAuthorities(), null, attributes);

//	    SecurityContext origCtx = SecurityContextHolder.getContext();
//        SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
//        SecurityContextHolder.getContext().setAuthentication(runAs);

    filterChain.doFilter(request, response);
  }

  public static void main(String args[]) {
    String signature = SHA1Util.Sha1(("51shop.mobi1Xquark"));
    System.out.println(signature);
  }
}
