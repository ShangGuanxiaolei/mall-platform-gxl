package com.xquark.config;

import java.util.concurrent.Executors;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.xquark.task.Scanned;


@Configuration
@Import(ApplicationConfig.class)
@ImportResource("classpath:META-INF/applicationContext-task.xml")
@ComponentScan(basePackageClasses = Scanned.class)
class TaskConfig extends WebMvcConfigurationSupport implements SchedulingConfigurer {

  @Override
  public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
    scheduledTaskRegistrar
        .setScheduler(Executors.newScheduledThreadPool(3));
  }

}