package com.xquark.task.activityPtOrderEffect;

import com.xquark.service.activityPtOrderEffect.impl.PtOrderEffectServiceImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author liuyandong
 * @date 2018/9/18 17:21
 */
@Component
public class OrderEffectTask {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private PtOrderEffectServiceImp effectService;
//    @Scheduled(cron = "0 */1 * * * ?")
    public void autoUpdateOrderStatus() {
        log.info("自动生效订单");
        effectService.updateOrderStatu();
        log.info("订单生效完成");

    }
}

