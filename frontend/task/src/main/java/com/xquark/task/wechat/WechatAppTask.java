package com.xquark.task.wechat;

import com.xquark.dal.mapper.WechatCustomizedMenuMapper;
import com.xquark.dal.model.wechat.CustomizedMenu;
import com.xquark.dal.model.wechat.CustomizedMenuItem;
import com.xquark.service.wechat.WechatService;
import com.xquark.service.wechat.WechatUtils;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.wechat.common.Config;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by dongsongjie on 16/6/15.
 */
@SuppressWarnings("ALL")
@Component
public class WechatAppTask {

  private Logger log = LoggerFactory.getLogger(getClass());

  public static final int JSTICKET_RENEW_INTERVAL = 7200;


  @Autowired
  private WechatCustomizedMenuMapper wechatCustomizedMenuMapper;

  @Autowired
  private WechatService wechatService;

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Value("${profiles.active}")
  String profile;

  /**
   * 当前时间离过期时间相差的分钟数, 小于该值则重新获取相关的token
   */
  private static final int RENEW_INTERVAL_MIMUTES = 20;

  /**
   * 更新所有快过期的token,目前包括access_token, jsapi_ticket, 暂未支持oauth_access_token, authorizer_access_token
   */
  @Scheduled(cron = "0 */5 * * * ?")
  public void renewTokenJob() {

    log.info("============== 开始更新token renewTokenJob =============");
    //检查并更新access_token,jsapi_ticket
    Date now = new Date();
    List<WechatAppConfig> wechatAppConfigs = wechatAppConfigMapper
        .listByExpiredAt(profile, RENEW_INTERVAL_MIMUTES * 60, now);
    log.info("查询更新列表,待更新config共:" + wechatAppConfigs.size());
    for (WechatAppConfig wechatAppConfig : wechatAppConfigs) {
      try {
        log.info(
            "待更新的Appconfig名称:" + wechatAppConfig.getAppName() + " pk:" + wechatAppConfig.getId());
        Config config = WechatUtils.convertWechatAppConfig(wechatAppConfig);
        if (renewable(now, wechatAppConfig.getAccessTokenExpiredAt())) {
          log.info("开始更新accessToken");
          wechatService.renewAccessToken(wechatAppConfig);
        }

        if (renewable(now, wechatAppConfig.getJsapiTicketExpiredAt())) {
          log.info("更新jsapi ticket");
          wechatService.renewJsapiTicket(wechatAppConfig);
        }
        log.info("保存更新的config成功");
      } catch (Exception e) {
        log.error("更新Appconfig失败" + e.getMessage(), e);
      }
    }
    log.info("============== 结束更新token renewTokenJob =============");
  }

  private boolean renewable(Date now, Date expiredAt) {
    if (expiredAt == null) {
      return false;
    }
    return expiredAt.getTime() <= now.getTime() + RENEW_INTERVAL_MIMUTES * 60 * 1000;
  }

  @Scheduled(cron = "0 */5 * * * ?")
  public void renewWechatMenuJob() {
    log.info("============== 开始更新菜单 renewWechatMenuJob =============");
    List<WechatAppConfig> appConfigs = wechatAppConfigMapper.list(profile);
    log.info("查询更新列表,待更新config共:" + appConfigs.size());
    for (WechatAppConfig wechatAppConfig : appConfigs) {
      try {
        log.info(
            "待更新菜单的Appconfig名称:" + wechatAppConfig.getAppName() + " pk:" + wechatAppConfig.getId());
        boolean needCreateNewMenu = false;
        List<CustomizedMenuItem> items = wechatCustomizedMenuMapper
            .list(wechatAppConfig.getShopId(), null);
        if (CollectionUtils.isNotEmpty(items)) {
          for (CustomizedMenuItem item : items) {
            if (item.getRenewable() == true) {
              needCreateNewMenu = true;
              break;
            }
          }
          if (needCreateNewMenu) {
            boolean ret = wechatService.updateMenus(wechatAppConfig, new CustomizedMenu(items));
            log.info("调用接口更新菜单" + (ret == true ? "成功" : "失败"));
          }
        }
        log.info("更新菜单完成");
      } catch (Exception e) {
        log.error("更新菜单出错" + e.getMessage(), e);
      }
    }
    log.info("============== 结束更新菜单 renewWechatMenuJob =============");
  }
}
