package com.xquark.task.partner;

import com.xquark.dal.mapper.CommissionMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.xquark.service.partner.UserPartnerService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * 合伙人相关的定时程序
 *
 * @author hhcao
 */
@Component
public class PartnerMemberJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  UserPartnerService userPartnerService;

  @Autowired
  ShopService shopService;

  @Autowired
  ShopTreeService shopTreeService;

  @Autowired
  OrderService orderService;

  @Autowired
  PartnerSettingService partnerSettingService;

  @Autowired
  CommissionMapper commissionMapper;

  @Value("${profiles.active}")
  String profile;

  /**
   * 自动将上个月状态为申请中的合伙人状态变更为已生效 每月11号早上0点执行
   */
  //@Scheduled(cron = "0 0 0 11 * ?")
  public void autoAudit() {
    log.info("begin on PartnerMember autoAudit");
    long begin = System.currentTimeMillis();
    int result = userPartnerService.autoAudit();
    long end = System.currentTimeMillis();
    log.info("end on PartnerMember autoAudit");
    StringBuffer info = new StringBuffer();
    info.append("自动将状态为申请中的合伙人状态变更为已生效：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

  /**
   * 每月6号早上0点执行
   */
  //@Scheduled(cron = "0 0 0 10 * ?")
//    @Scheduled(cron = "0 */1 * * * ?")
  public void autoCommission() {
    log.info("begin on Partner autoCommission");
    long begin = System.currentTimeMillis();
    //根据userpartner表 查找合伙人团队上月所有订单根据分佣比例
    List<ShopTree> listRootShop = shopTreeService.listRootShopTree();
    for (ShopTree rootShopTree : listRootShop) {
      String rootShopId = rootShopTree.getRootShopId();
      List<String> shopIdList = new ArrayList<String>();
      List<UserPartner> listUserPartner = userPartnerService
          .selectActiveUserPartnersByShopId(rootShopId);
      PartnerSetting partnerSetting = partnerSettingService.loadByShopId(rootShopId);
      double partnerRate = partnerSetting.getCmRate();
      //找到这n个合伙人的层次关系
      for (UserPartner userPartner : listUserPartner) {
        String userId = userPartner.getUserId();
        String ownerShopId = shopService.findByUser(userId).getId();
        shopIdList.add(ownerShopId);
      }
      for (String currentShopId : shopIdList) {
        List<String> exceptList = new ArrayList<String>();
        exceptList.addAll(shopIdList);
        exceptList.remove(currentShopId);
        List<ShopTree> listShopTree = shopTreeService
            .listChildrenExceptShopList(rootShopId, currentShopId, exceptList);
        for (ShopTree shopTree : listShopTree) {
          String sellerShopId = shopTree.getDescendantShopId();
          List<OrderVO> orderList = orderService
              .selectLastMonthOrderByShop(rootShopId, sellerShopId);
          for (OrderVO orderVO : orderList) {
            BigDecimal totalFee = orderVO.getTotalFee().add(orderVO.getDiscountFee())
                .subtract(orderVO.getLogisticsFee())
                .subtract(orderVO.getRefundFee()).subtract(orderVO.getRefundPlatformFee());
            BigDecimal totalGoodsFee = BigDecimal.ZERO;
            List<OrderItem> listItem = orderVO.getOrderItems();
            for (OrderItem orderItem : listItem) {
              totalGoodsFee = totalGoodsFee
                  .add(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getAmount())));
            }
            for (OrderItem orderItem : listItem) {
              BigDecimal skuFee = orderItem.getPrice()
                  .multiply(BigDecimal.valueOf(orderItem.getAmount()))
                  .divide(totalGoodsFee, 4, BigDecimal.ROUND_DOWN).multiply(totalFee);
              BigDecimal cmFee = skuFee
                  .multiply(new BigDecimal(partnerRate).setScale(2, BigDecimal.ROUND_DOWN));
              Commission comm = new Commission(orderVO.getId(), orderItem.getId(),
                  orderItem.getSkuId(), orderItem.getPrice(),
                  new BigDecimal(partnerRate).doubleValue(), CommissionType.PARTNER,
                  orderItem.getAmount(), cmFee, orderVO.getSellerId(), CommissionStatus.NEW, true);
              commissionMapper.insert4Partner(comm);
            }
          }

        }
      }
    }
    long end = System.currentTimeMillis();
    log.info("end on Partner autoCommission");
    StringBuffer info = new StringBuffer();
    info.append("合伙人分佣已生成：");
    //info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

}
