package com.xquark.task.spider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xquark.service.shop.ShopService;

/**
 * 每天定时执行搬家
 *
 * @author Sallen
 */
@Component
public class SpiderJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ShopService shopService;


}
