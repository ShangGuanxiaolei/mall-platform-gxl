package com.xquark.task.activity;

import com.xquark.service.groupon.ActivityGrouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 活动相关定时任务，如团购、限时抢购等
 *
 * @author chh
 */
@Component
public class ActivityJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  ActivityGrouponService activityGrouponService;

  @Value("${profiles.active}")
  String profile;

  /**
   * 优惠活动时间到了后自动结束
   */
  @Scheduled(cron = "0 */10 * * * ?")
  public void autoPromotionClose() {
    log.info("begin on activity autoPromotionClose");
    long begin = System.currentTimeMillis();
    int result = activityGrouponService.autoPromotionClose();
    long end = System.currentTimeMillis();
    String info = "优惠活动时间到了后自动结束：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

  /**
   * 团购活动到了结束时间则自动更新未成团活动状态
   */
  @Scheduled(cron = "0 */10 * * * ?")
  public void autoGroupOnClose() {
    log.info("begin on activity autoGrouponClose");
    long begin = System.currentTimeMillis();
    int result = activityGrouponService.autoGrouponClose();
    long end = System.currentTimeMillis();
    String info = "团购活动到了结束时间则自动更新未成团活动状态：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }


  /**
   * 自动将未成团且已经付款的订单进行退款操作
   */
  @Scheduled(cron = "0 */30 * * * ?")
  public void autoGroupOnRefund() {
    log.info("begin on autoGrouponRefund");
    long begin = System.currentTimeMillis();
    int result = activityGrouponService.autoGrouponRefund();
    long end = System.currentTimeMillis();
    String info = "未成团且已经付款的订单进行退款操作：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

}
