package com.xquark.task.points;

import static com.hds.xquark.dal.type.TotalAuditType.JOB;
import static com.xquark.dal.consts.TimeSettingConstrant.*;

import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.ToastInfoType;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.dal.mapper.JobSchedulerLogMapper;
import com.xquark.dal.mapper.TimeSettingsMapper;
import com.xquark.dal.model.JobSchedulerLog;
import com.xquark.dal.model.TimeSettings;
import com.xquark.dal.type.JobStatus;
import com.xquark.dal.type.JobType;
import com.xquark.utils.DateUtils;

import io.vavr.Tuple2;
import io.vavr.control.Either;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * @author wangxinhua on 2018/5/23. DESC:
 */
@Component
public class PointTask {

  private final static Logger LOGGER = LoggerFactory.getLogger(PointTask.class);

  private PointCommService pointCommService;

  private TimeSettingsMapper timeSettingsMapper;

  @Value("${deposit.dayofmonth}")
  private Integer dayOfMonth;

  @Autowired
  private JobSchedulerLogMapper jobSchedulerLogMapper;

  @Autowired
  public void setTimeSettingsMapper(TimeSettingsMapper timeSettingsMapper) {
    this.timeSettingsMapper = timeSettingsMapper;
  }

  @Autowired
  public void setPointCommService(PointContextInitialize pointContextInitialize) {
    this.pointCommService = pointContextInitialize.getPointService();
  }

  @Scheduled(cron = "0 0 0 * * ?")
  public void releasePoints() {
    callWithLog(JobType.RELEASE_POINT);
  }

  @Scheduled(cron = "0 0 0 * * ?")
  public void releaseCommission() {
    callWithLog(JobType.RELEASE_COMMISSION);
  }

  /**
   * hds
   * 每月16,24日触发 从当前时间开始迁移数据到withdraw表
   */
  @Scheduled(cron = "0 0 0 11,24 * ?")
  public void translateWithdrawHds() {
    translateWithdraw(HDS_KEY,PlatformType.H);
  }


  /**
   * hv
   * 每月16,24日触发 从当前时间开始迁移数据到withdraw表
   *
   */
  @Scheduled(cron = "0 0 0 11,24 * ?")
  public void translateWithdrawHv() {
    translateWithdraw(HV_KEY,PlatformType.E);
  }

  private void translateWithdraw(String key,PlatformType platformType){
    Date now = new Date();
    Either<String, TimeSettings> settings = checkTimeSetting(key, now);
    if (settings.isLeft()){
      LOGGER.info(settings.getLeft());
      return;
    }
    TimeSettings timeSettings = settings.get();

    // 开始时间
    Date startOfToday = DateUtils.getStartOfDay(now);
    // 汉德森, 汉薇 每个月迁移
    Calendar c = Calendar.getInstance();
    c.setTime(now);
    ToastInfoType currType = ToastInfoType.byDepositDay(c.get(Calendar.DAY_OF_MONTH));
    pointCommService.translateCommSuspendingToWithdraw(timeSettings.getStartDate(), startOfToday, platformType,currType);
    // 进行税费计算 根据落账日期计算税费 新增税费统计

    // 获取下次提现时type
    ToastInfoType nextType = ToastInfoType.byDayOfMonth(c.get(Calendar.DAY_OF_MONTH));
    Tuple2<Date, Date> nextTime = nextType.currDateTuple2(c.get(Calendar.YEAR), c.get(Calendar.MONTH));

    // 设置下一次的时间设置
    timeSettings.setStartDate(nextTime._1);
    timeSettings.setEndDate(nextTime._2);
    timeSettingsMapper.update(timeSettings);
  }

  private Either<String,TimeSettings> checkTimeSetting(String key, Date now){
    TimeSettings timeSettings = timeSettingsMapper.selectByName(key);
    if (timeSettings == null) {
      return Either.left("timeSettings未初始化");
    }

    Date end = timeSettings.getEndDate();

    if (now.before(end)) {
      return Either.left(String.format("withdraw job: %s 未到开始时间, 跳过执行, 请检查timeSettings",key));
    }

    return Either.right(timeSettings);
  }


  /**
   * 每周三迁移viviLife
   */
  @Scheduled(cron = "0 0 0 ? * THU")
  public void translateWithdrawVivi() {
    TimeSettings timeSettings = timeSettingsMapper.selectByName(VIVI_KEY);
    if (timeSettings == null) {
      LOGGER.error("timeSettings未初始化");
      return;
    }

    Date now = new Date();

    Date start = timeSettings.getStartDate();
    Date end = timeSettings.getEndDate();

    if (now.before(end)) {
      LOGGER.error("withdraw job: {} 未到开始时间, 跳过执行, 请检查timeSettings",VIVI_KEY);
      return;
    }

    try {
      // 当前时间由于定时任务的延时肯定会大于26号
      Date startOfToday = DateUtils.getStartOfDay(now);
      // 汉德森, 汉薇 每个月迁移
      pointCommService.translateCommSuspendingToWithdraw(start, startOfToday, PlatformType.V);
    } catch (Exception e) {
      LOGGER.error("viviLife数据迁移失败", e);
      return;
    }
    Date newStart = DateUtils.addDay(start, 7);
    Date newEnd = DateUtils.addDay(end, 7);
    timeSettings.setStartDate(newStart);
    timeSettings.setEndDate(newEnd);
    timeSettingsMapper.update(timeSettings);
  }

  /**
   * code 1 积分 code 2 德分
   */
  @SuppressWarnings("all")
  private void callWithLog(JobType jobType) {
    JobSchedulerLog log = JobSchedulerLog.empty(jobType);
    log.setStartDate(new Date());
    try {
      if (jobType == JobType.RELEASE_COMMISSION) {
        pointCommService.releaseCommission(JOB);
      } else if (jobType == JobType.RELEASE_POINT) {
        pointCommService.releasePoints(JOB);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (Exception e) {
      log.setExceptionMsg(e.getMessage());
      log.setJobStatus(JobStatus.ERROR.getCode());
      jobSchedulerLogMapper.insert(log);
      return;
    }
    log.setJobStatus(JobStatus.COMPLETED.getCode());
    log.setCompletedDate(new Date());
    jobSchedulerLogMapper.insert(log);
  }

}
