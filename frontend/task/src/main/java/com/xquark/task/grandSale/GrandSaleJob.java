package com.xquark.task.grandSale;

import com.xquark.service.grandSale.GrandSaleModuleService;
import com.xquark.service.grandSale.GrandSalePromotionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 大促活动相关的定时程序
 *
 * @author odin
 */
@Component
public class GrandSaleJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private GrandSalePromotionService grandSalePromotionService;

  @Autowired
  private GrandSaleModuleService grandSaleModuleService;

  /**
   * 自动开始、结束大促活动，每五分种执行一次
   */
  @Scheduled(cron = "0 */5 * * * ?")
  public void autoEndPromotion() {
    log.info("begin on grandSale job");
    long begin = System.currentTimeMillis();
    int result = grandSalePromotionService.updateStatusByJob();
    long end = System.currentTimeMillis();
    log.info("end on order autoCancel");
    String info = "自动更新大促活动状态成功：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

  @Scheduled(cron = "0 */5 * * * ?")
  public void autoEndModule() {
    log.info("begin on grandSale end module job");
    long begin = System.currentTimeMillis();
    int result = grandSaleModuleService.updateModuleStatus();
    long end = System.currentTimeMillis();
    log.info("end on order autoCancel");
    String info = "自动更新大促活动状态成功：" +
        result +
        "条，花费：" +
        (end - begin) +
        "ms";
    log.info(info);
  }

}
