package com.xquark.task.activiyPtEnd;
import com.xquark.service.activityEnd.ActivityEndService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
@Component
public class ActivityEndTask {
    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private ActivityEndService endService;
    //5分钟扫描一次
    @Scheduled(cron = "0 */1 * * * ?")
    public void activityEnd() {
        log.info("下架任务监控");
        //1. 查询下架的活动-服务1
        //2. 轮训下架的活动-
        //3.  --检查剩余库存并准备释放库存，成功过后活动下架，记录活动库存扣减明细--服务2（事事务）
        endService.activityEndService();
        log.info("下架任务完成");
    }
}
