package com.xquark.task.wms;

import com.xquark.service.wms.WmsOrderPackageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * User: kong Date: 18-6-12. Time: 下午8:13 Wms订单包装信息表定时任务
 */
@Component
public class WmsOrderPackageJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WmsOrderPackageService wmsOrderPackageService;

  //  @Scheduled(cron = "0 0/30 * * * ?")
  public void getData() {
    StringBuffer info = new StringBuffer();
    Integer length = wmsOrderPackageService.getWmsOrderPackage(null);
    info.append("Wms订单包装信息表一共" + length + "条");
    log.info(info.toString());
  }


}
