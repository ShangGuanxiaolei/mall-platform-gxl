package com.xquark.task.activityPtOutTime;

import com.google.common.collect.ImmutableMap;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.mapper.CustomerModifyLinkDetailMapper;
import com.xquark.dal.mapper.PromotionGoodsMapper;
import com.xquark.dal.model.CustomerModifyLinkDetail;
import com.xquark.dal.model.OrderHeader;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.activityPtOutTime.PtlSelectOutTimeService;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.user.UserService;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class ActivityPtOutTImeTask {
    @Autowired
    private PtlSelectOutTimeService ptSelectOutTimeService;

    @Autowired
    private OrderHeaderService orderHeaderService;

    @Autowired
    private PromotionGoodsMapper promotionGoodsMapper;

    @Autowired
    private CustomerModifyLinkDetailMapper customerModifyLinkDetailMapper;

    @Autowired
    private CustomerProfileService customerProfileService;

    @Autowired
    private UserService userService;

    private final Lock lock = new ReentrantLock();

    /**
     * 调用结算中心报文
     */
    private final static String CPID = "cpid";
    private final static String OLD_UPLINE_CPID = "old_upline_cpid";
    private final static String NEW_UPLINE_CPID = "new_upline_cpid";
    private final static String SOURCE = "source";
    private final static String REASON = "reason";
    private final static int HVMALL = 3;

    private final static String SETTLEMENT_CENTER_KEYS = "settlement_center_keys";
    private static final Long COMPANY_CPID = 2000002L;

    @Value("${task.settlementCenter.url}")
    private String settlementUrl;

    private Logger log = LoggerFactory.getLogger(getClass()); //5分钟扫描一次 @Scheduled(cron = "0 */1 * * * ?")

    @Scheduled(cron = "0 */2 * * * ?")
    public void activityPtOutTime() {
        if (this.lock.tryLock()) {
            try {
                log.info("失效拼团处理");
                //查询所有已经开团,  时间到期的拼团
                //遍历拼团,查询每个拼团每个人的订单编码 是否退款
                //处理退款流程
                //处理结束后 修改状态
                //失效拼团逻辑处理
                ptSelectOutTimeService.PtOutTimeService();
                // 处理遗漏的 拼团订单
                ptSelectOutTimeService.clearUnrefundPieceOrders();
                log.info("失效拼团处理结束");
            } finally {
                lock.unlock();
            }
        } else {
            log.info("失效拼团处理中，稍后再试 ...");
        }
    }

    @Scheduled(fixedRate = 300000)
    public void changeUpLine() {

        log.info("白人首单上级变更处理开始...");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
        String strToDay = localDate.format(dateTimeFormatter);

        RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
        List<OrderHeader> orderHeaderList = orderHeaderService.selectWhiteOrderList(strToDay);

        for (OrderHeader orderHeader : orderHeaderList) {

            String redisKey = String.join(":", strToDay,
                    orderHeader.getOrderid(),
                    String.valueOf(orderHeader.getCpid()));

            if (isNew(orderHeader.getCpid()) ||
                isIdentity(orderHeader.getCpid()) ||
                isRepeatDeal(redisUtils, redisKey, strToDay)) {
                log.info("cpid-->{}跳过", orderHeader.getCpid());
                continue;
            }

            CustomerModifyLinkDetail customerModifyLinkDetail =
                    customerModifyLinkDetailMapper.selectUpgradeDetail(orderHeader.getCpid(), strToDay);
            if (Objects.isNull(customerModifyLinkDetail)){
                log.info("不存在变更轨迹cpid-->{}跳过",orderHeader.getCpid());
                continue;
            }

            log.info("白人首单上级变更调用接口组装报文...");
            Map<String,Object> mapParam = ImmutableMap.of(
                    CPID, customerModifyLinkDetail.getCpid(),
                    OLD_UPLINE_CPID, Optional.ofNullable(customerModifyLinkDetail.getFromCpid()).orElse(COMPANY_CPID),
                    NEW_UPLINE_CPID, Optional.ofNullable(customerModifyLinkDetail.getToCpid()).orElse(COMPANY_CPID),
                    SOURCE, HVMALL,
                    REASON,"白人更换上级");
            log.info("白人首单上级变更调用接口组装报文结束==>{}...", mapParam);

            try{
                sendMessageToSettlementCenter(mapParam);
            } catch (Exception e) {
                log.error("调用结算白人上级变更接口错误, 参数: {}", mapParam);
            }
        }

        log.info("白人首单上级变更处理结束...");
    }

    //如果不是首单跳过
    private Boolean isNew(Long cpId) {
        User user = userService.loadByCpId(cpId);
        return 1 != promotionGoodsMapper.selectIsNew(String.valueOf(IdTypeHandler.decode(user.getId())));
    }

    //是否是白人
    private Boolean isIdentity(Long cpId) {
       return customerProfileService.hasIdentity(cpId);
    }

    //是否重复处理
    private Boolean isRepeatDeal(RedisUtils<Integer> redisUtils, String redisKey,String strTime) {
        String key = String.join("-",strTime, SETTLEMENT_CENTER_KEYS);
        if (!redisUtils.hasKey(key)) {
            redisUtils.hSet(key, redisKey, 1);
            redisUtils.expire(key, 1, TimeUnit.DAYS);
            return Boolean.FALSE;
        }else {
            if (Objects.isNull(redisUtils.hGet(key, redisKey))) {
                redisUtils.hSet(key, redisKey, 1);
                return Boolean.FALSE;
            }else {
                return Boolean.TRUE;
            }
        }
    }

    //组装报文发送
    private void sendMessageToSettlementCenter(Map<String,Object> mapParam) {
        log.info("白人首单上级变更调用接口...");
        String url = settlementUrl + "/v1/api/changeUpline";
        HttpInvokeResult httpInvokeResult =
                PoolingHttpClients.postJSON(url, mapParam, 5000);
        log.info(httpInvokeResult.getContent());
    }
}