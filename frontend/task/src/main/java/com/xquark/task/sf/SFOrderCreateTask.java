package com.xquark.task.sf;

import com.xquark.dal.mapper.SfAppConfigMapper;
import com.xquark.dal.mapper.SystemRegionMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;
import com.xquark.dal.type.ProductSource;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.SfRegionService;
import com.xquark.service.order.impl.AsyncSubmitOrder;
import com.xquark.service.order.vo.OrderAddressVO;
import com.xquark.service.product.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SFOrderCreateTask {

  private final static Logger LOGGER = LoggerFactory.getLogger(SFOrderCreateTask.class);

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Autowired
  private SfAppConfigMapper sfConfigMapper;

  @Autowired
  private SfRegionService sfRegionService;

  @Autowired
  private SystemRegionMapper systemRegionMapper;

  @Autowired
  private OrderService orderService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private ProductService productService;

  @Autowired
  private LogisticsGoodsService logisticsGoodsService;

  @Scheduled(cron = "0 */1 * * * ?")
  public void orderCreateTask() {
    AsyncSubmitOrder asyncSubmitOrder = new AsyncSubmitOrder();
    List<Order> splitedOrders = orderService.queryOrderCanPush();

    for (Order order : splitedOrders) {
      List<OrderItem> orderItemList = orderService.listOrderItems(order.getId());
      OrderAddressVO orderAddressVO = orderAddressService.selectByOrderId(order.getId());
      if (orderItemList != null && !orderItemList.isEmpty()) {
        for (OrderItem orderItem : orderItemList) {
          Product product = productService.load(orderItem.getProductId());

          orderItem.setProduct(product);

          Sku sku = productService.loadSku(orderItem.getSkuId());
          orderItem.setSku(sku);

          order.setSource(ProductSource.valueOf(sku.getSkuCodeResources()));
        }

        Map<Order, List<OrderItem>> orderListMap = new HashMap<Order, List<OrderItem>>();
        orderListMap.put(order, orderItemList);

        asyncSubmitOrder.asyncSubmit(orderListMap, orderAddressVO, sfConfigMapper, profile, host,
            sfRegionService,
            systemRegionMapper, orderService,logisticsGoodsService);
      }
    }
  }
}
