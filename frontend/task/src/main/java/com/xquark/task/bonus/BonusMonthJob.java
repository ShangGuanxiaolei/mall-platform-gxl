package com.xquark.task.bonus;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.UserAgentMapper;
import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.BonusStatus;
import com.xquark.dal.status.BonusType;
import com.xquark.service.bonus.BonusService;
import com.xquark.service.order.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.ManagedProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 月度代理分红调度
 *
 * @author chh 2017-06-08
 */
@Component
public class BonusMonthJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  OrderService orderService;

  @Autowired
  BonusService bonusService;

  @Autowired
  UserAgentMapper userAgentMapper;

  @Autowired
  OrderMapper orderMapper;

  @Value("${profiles.active}")
  String profile;

  // 单笔进货量
  int perCount = 400;

  /**
   * 计算前一个月所有董事与联合执行人的月度分红 每个月的2号凌晨3点开始执行
   */
  //@Scheduled(cron = "0 0/3 * * * ?")
  public void autoCal() {
    log.info("begin on bonus month Cal");
    long begin = System.currentTimeMillis();
    // 首先查询是否有相同年度，月度的分红记录，如果有，将之前的记录删除，重新计算
    /**String year = getLastYear(new Date());
     String month = getLastMonth(new Date());
     Bonus bonus = bonusService.getBonusMonthByPeriod(year, month);
     if(bonus != null){
     bonusService.deleteBonus(bonus.getId());
     }
     // 新增代理分红记录，开始进行计算
     Bonus newBonus = new Bonus();
     newBonus.setArchive(false);
     newBonus.setMonth(month);
     newBonus.setYear(year);
     newBonus.setType(BonusType.MONTH);
     bonusService.insertBonus(newBonus);
     int result = 0;
     // 获取所有董事和联合创始人代理
     List<UserAgent> userAgents = userAgentMapper.listAllDirectorAndFounder();
     for(UserAgent userAgent : userAgents){
     String userId = userAgent.getUserId();
     // 直营店进货量
     long firstCount = 0;
     // 一级直营进货量
     long secondCount = 0;
     // 二级直营进货量
     long thirdCount = 0;
     // 先获取直营店上月销售量，即本人进货量
     firstCount = orderMapper.countSelfLastMonthOrder(userId);
     //if( (firstCount/ perCount) >= 1){
     secondCount = orderMapper.countDirectLastMonthOrder(userId);
     //if( (secondCount/ perCount) >= 1){
     thirdCount = orderMapper.countIndirectLastMonthOrder(userId);
     //}
     String level = "";
     BigDecimal rate = new BigDecimal("0");
     firstCount = firstCount/ perCount;
     secondCount = secondCount/ perCount;
     thirdCount = thirdCount/ perCount;
     if(secondCount >=12 && thirdCount >=36){
     level = "D";
     rate = new BigDecimal("6");
     }else if(secondCount >=9 && thirdCount >=27){
     level = "C";
     rate = new BigDecimal("5.1");
     }else if(secondCount >=5){
     level = "B";
     rate = new BigDecimal("4");
     }else if(secondCount >=1){
     level = "A";
     rate = new BigDecimal("2.6");
     }

     // 整个团队上月完成订单的总金额
     //if(StringUtils.isNotEmpty(level)){
     BigDecimal amount = orderMapper.countLastMonthOrderAmount(userId);
     // 将该代理月度分红数据插入数据库中
     BonusDetail bonusDetail = new BonusDetail();
     bonusDetail.setParentId(newBonus.getId());
     bonusDetail.setLevel(level);
     bonusDetail.setRate(rate);
     bonusDetail.setAmount(amount);
     //bonusDetail.setFee(amount.multiply(rate.divide(new BigDecimal("100"),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP));
     bonusDetail.setStatus(BonusStatus.NEW);
     bonusDetail.setUserId(userId);
     bonusDetail.setArchive(false);
     bonusDetail.setFirstNum("" + firstCount);
     bonusDetail.setSecondNum("" + secondCount);
     bonusDetail.setThirdNum("" + thirdCount);
     bonusService.insertBonusDetail(bonusDetail);
     result ++;
     //}
     //}
     }

     // 找出该月份每个等级对应的代理总数
     List<Map> months = bonusService.getTotalBonusMonthCount(year, month);
     for(Map monthCount : months){
     String level = (String)monthCount.get("level");
     String count = "" + monthCount.get("count");
     // 每个代理均分这个等级的分红金额
     if(StringUtils.isNotEmpty(level) && StringUtils.isNotEmpty(count) && !"0".equals(count)){
     bonusService.updateBonusMonthFee(year, month, level, count);
     }
     }

     bonusService.updateBonusEndTime(newBonus.getId());
     long end = System.currentTimeMillis();
     log.info("end on bonus month Cal");
     StringBuffer info = new StringBuffer();
     info.append("计算前一个月所有董事与联合执行人的月度分红");
     info.append(result);
     info.append("，花费：");
     info.append(end-begin);
     info.append("ms");
     log.info(info.toString());**/
  }

  private String getLastYear(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.add(Calendar.MONTH, -1);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    return sdf.format(cal.getTime());
  }

  private String getLastMonth(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.add(Calendar.MONTH, -1);
    SimpleDateFormat sdf = new SimpleDateFormat("MM");
    return sdf.format(cal.getTime());
  }


}
