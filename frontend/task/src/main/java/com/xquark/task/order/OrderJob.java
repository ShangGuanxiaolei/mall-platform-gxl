package com.xquark.task.order;

import com.xquark.dal.model.MainOrder;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.ThirdPaymentQueryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单相关的定时程序
 *
 * @author odin
 */
@Component
public class OrderJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  OrderService orderService;

  @Autowired
  CashierService cashierService;

  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;

  @Autowired
  MainOrderService mainOrderService;

  /**
   * PENDING检查处于PENDING状态的订单间隔时间
   */
  private static final int PENDING_SECONDS = 60 * 60;

  @Value("${profiles.active}")
  String profile;

  /**
   * 自动签收日提前两天提醒买家 每天上午10点检查一次
   */
  @Scheduled(cron = "0 0 10 * * ?")
  public void autoSignRemind() {
    log.info("begin on order autoSignRemind");
    long begin = System.currentTimeMillis();
    int result = orderService.autoRemindSign();
    long end = System.currentTimeMillis();
    log.info("end on order autoSignRemind");
    String info = "提醒买家未签收订单即将由系统自动签收：" +
            result +
            "条短信，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

  /**
   * 检查PENDING状态的订单
   *
   * 修改每5分钟执行一次，查询cashieritem表PENDING状态的订单
   */
  @Scheduled(cron = "0 */5 * * * ?")
  public void checkPending() {
    final List<MainOrder> orders = mainOrderService.listPendingByCashierItem(PENDING_SECONDS);
    for (MainOrder o : orders) {
      try{
        cashierService.handlePending(o);
      } catch (Exception e) {
        log.error("订单 {} 处理PENDING状态出错", o.getOrderNo(), e);
        if(e instanceof PayNotifyException) {
        	PayNotifyException e1 = (PayNotifyException)e;
        	log.info("订单 {} 处理PENDING状态,需要进行退款操作", o.getOrderNo(), e1);
        	thirdPaymentQueryService.doThirdRefund(e1);
        }
      }
    }
  }

  /**
   * 买家未付款超过30分钟自动取消订单 每五分种执行一次
   */
  @Scheduled(cron = "0 */5 * * * ?")
  public void autoCancel() {
    log.info("begin on order autoCancel");
    long begin = System.currentTimeMillis();
    int result = orderService.autoCancel();
    long end = System.currentTimeMillis();
    log.info("end on order autoCancel");
    String info = "买家未付款超过30分钟自动取消订单成功：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);
  }

  /**
   * 两分钟检查一下拼团订单
   */
  @Scheduled(cron = "0 */2 * * * ?")
  public void autoCancelPiece() {
    log.info("begin on piece order autoCancel");
    long begin = System.currentTimeMillis();
    int result = orderService.autoCancelPiece();
    long end = System.currentTimeMillis();
    log.info("end on piece order autoCancel");
    String info = "买家未付款拼团订单超过3分钟自动取消订单成功："
        + result
        + "条，花费："
        + (end - begin)
        + "ms";
    log.info(info);
  }

  @Scheduled(cron = "0 */5 * * * ?")
  public void autoSign() {
    log.info("begin on order autoSign");
    long begin = System.currentTimeMillis();
    int result = orderService.autoSign();
    long end = System.currentTimeMillis();
    String info = "卖家发货超过7天自动签收：" +
            result +
            "条，花费：" +
            (end - begin) +
            "ms";
    log.info(info);

  }

  @Scheduled(cron = "0 */5 * * * ?")
  public void autoShipAndSignForTest() {
    if (profile.toLowerCase().compareTo("test") == 0) {
      /**log.info("begin on order autoShipAndSignForTest");
       long begin = System.currentTimeMillis();
       int result = orderService.autoShipAndSignForTest();
       long end = System.currentTimeMillis();
       StringBuffer info = new StringBuffer();
       info.append("autoShipAndSignForTest：");
       info.append(result);
       info.append("条，花费：");
       info.append(end-begin);
       info.append("ms");
       log.info(info.toString());**/
    } else {
      log.info("autoShipAndSignForTest only excuted in dev env");
    }

  }

  /**
   * 未完成订单更新 erp系统更新状态 每30分种执行一次
   */
  //@Scheduled(cron = "0 */30 * * * ?" )
  public void autoRefreshOrderStatus() {
    long begin = System.currentTimeMillis();
    int result = orderService.autoRefreshOrder();
    long end = System.currentTimeMillis();
    StringBuffer info = new StringBuffer();
    info.append("同步 distribution erp订单状态：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }


}
