package com.xquark.task.team;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.Team;
import com.xquark.dal.model.TeamShopCommissionDe;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.status.TeamType;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.TeamShopCommissionVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 战队每周，每月佣金分红
 *
 * @author chh 2017-07-18
 */
@Component
public class TeamCommissionJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  TeamMapper teamMapper;

  @Autowired
  TeamShopCommissionMapper teamShopCommissionMapper;

  @Autowired
  TeamShopCommissionDeMapper teamShopCommissionDeMapper;

  @Autowired
  UserTeamMapper userTeamMapper;

  @Autowired
  OrderMapper orderMapper;

  @Autowired
  CommissionMapper commissionMapper;

  @Value("${profiles.active}")
  String profile;

  /**
   * 计算战队上周佣金 每周一凌晨两点执行
   */
  @Scheduled(cron = "0 0 2 ? * MON")
  public void autoWeekCal() {
    log.info("begin on team week Cal");
    long begin = System.currentTimeMillis();
    int result = 0;
    // 先获取系统中所有的战队数据
    List<Team> teams = teamMapper.getAllTeam();
    Commission comm = null;
    for (Team team : teams) {
      // 战队队长用户id
      String leaderUserId = team.getUserId();
      // 战队分组id
      String groupId = team.getGroupId();
      TeamShopCommissionVO teamShopCommissionVO = teamShopCommissionMapper
          .selectByPrimaryKey(groupId);
      // 如果分组不存在，则不进行计算
      if (teamShopCommissionVO == null) {
        continue;
      }
      // 得到该战队所有成员上周的所有订单总收益
      BigDecimal totalAmount = orderMapper.countLastWeekTeamOrderAmount(team.getId());
      // 如果该战队上周无订单收益，则不计算
      if (totalAmount == null || totalAmount.compareTo(BigDecimal.ZERO) == 0) {
        continue;
      }

      // 得到该战队分组的周达标设置,返回结果按照销售额降序排列
      TeamShopCommissionDe chooseDe = null;
      List<TeamShopCommissionDe> des = teamShopCommissionDeMapper
          .selectActivityByParentId(teamShopCommissionVO.getId(), TeamType.WEEK.toString());
      // 判断如果订单总额大于等于某个销售额，则以此标准进行分佣计算
      for (TeamShopCommissionDe de : des) {
        BigDecimal sales = de.getSales();
        if (totalAmount.compareTo(sales) >= 0) {
          chooseDe = de;
          break;
        }
      }
      if (chooseDe == null) {
        continue;
      }
      // 战队总提成
      BigDecimal teamTotalAmount = chooseDe.getTotalAmount();
      // 战队队长提成
      BigDecimal teamLeaderAmount = chooseDe.getLeaderAmount();
      // 战队队长佣金
      comm = new Commission("74", "74", "74", new BigDecimal("1"),
          1, CommissionType.TEAM_LEADER, 1, teamTotalAmount, leaderUserId, CommissionStatus.NEW);
      commissionMapper.insert(comm);

      // 战队队员佣金计算
      List<Map> teamMembers = orderMapper.getLastWeekTeamOrderAmount(team.getId());
      for (Map teamMember : teamMembers) {
        String userId = (String) teamMember.get("id");
        BigDecimal fee = new BigDecimal("" + teamMember.get("fee"));
        BigDecimal teamFee = fee.divide(totalAmount, 4, BigDecimal.ROUND_DOWN)
            .multiply(teamTotalAmount.subtract(teamLeaderAmount))
            .setScale(2, BigDecimal.ROUND_DOWN);
        comm = new Commission("74", "74", "74", new BigDecimal("1"),
            1, CommissionType.TEAM_MEMBER, 1, teamFee, userId, CommissionStatus.NEW);
        commissionMapper.insert(comm);
      }


    }

    long end = System.currentTimeMillis();
    log.info("end on team week Cal");
    StringBuffer info = new StringBuffer();
    info.append("计算战队每周佣金分红");
    info.append(result);
    info.append("，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

  /**
   * 计算战队上月佣金 每月一号凌晨三点执行
   */
  @Scheduled(cron = "0 0 3 1 * ?")
  public void autoMonthCal() {
    log.info("begin on team month Cal");
    long begin = System.currentTimeMillis();
    int result = 0;
    // 先获取系统中所有的战队数据
    List<Team> teams = teamMapper.getAllTeam();
    Commission comm = null;
    for (Team team : teams) {
      // 战队队长用户id
      String leaderUserId = team.getUserId();
      // 战队分组id
      String groupId = team.getGroupId();
      TeamShopCommissionVO teamShopCommissionVO = teamShopCommissionMapper
          .selectByPrimaryKey(groupId);
      // 如果分组不存在，则不进行计算
      if (teamShopCommissionVO == null) {
        continue;
      }
      // 得到该战队所有成员上周的所有订单总收益
      BigDecimal totalAmount = orderMapper.countLastMonthTeamOrderAmount(team.getId());
      // 如果该战队上周无订单收益，则不计算
      if (totalAmount == null || totalAmount.compareTo(BigDecimal.ZERO) == 0) {
        continue;
      }

      // 得到该战队分组的周达标设置,返回结果按照销售额降序排列
      TeamShopCommissionDe chooseDe = null;
      List<TeamShopCommissionDe> des = teamShopCommissionDeMapper
          .selectActivityByParentId(teamShopCommissionVO.getId(), TeamType.MONTH.toString());
      // 判断如果订单总额大于等于某个销售额，则以此标准进行分佣计算
      for (TeamShopCommissionDe de : des) {
        BigDecimal sales = de.getSales();
        if (totalAmount.compareTo(sales) >= 0) {
          chooseDe = de;
          break;
        }
      }
      if (chooseDe == null) {
        continue;
      }
      // 战队总提成
      BigDecimal teamTotalAmount = chooseDe.getTotalAmount();
      // 战队队长提成
      BigDecimal teamLeaderAmount = chooseDe.getLeaderAmount();
      // 战队队长佣金
      comm = new Commission("74", "74", "74", new BigDecimal("1"),
          1, CommissionType.TEAM_LEADER, 1, teamTotalAmount, leaderUserId, CommissionStatus.NEW);
      commissionMapper.insert(comm);

      // 战队队员佣金计算
      List<Map> teamMembers = orderMapper.getLastMonthTeamOrderAmount(team.getId());
      for (Map teamMember : teamMembers) {
        String userId = (String) teamMember.get("id");
        BigDecimal fee = new BigDecimal("" + teamMember.get("fee"));
        BigDecimal teamFee = fee.divide(totalAmount, 4, BigDecimal.ROUND_DOWN)
            .multiply(teamTotalAmount.subtract(teamLeaderAmount))
            .setScale(2, BigDecimal.ROUND_DOWN);
        comm = new Commission("74", "74", "74", new BigDecimal("1"),
            1, CommissionType.TEAM_MEMBER, 1, teamFee, userId, CommissionStatus.NEW);
        commissionMapper.insert(comm);
      }


    }

    long end = System.currentTimeMillis();
    log.info("end on team month Cal");
    StringBuffer info = new StringBuffer();
    info.append("计算战队每月佣金分红");
    info.append(result);
    info.append("，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }


}
