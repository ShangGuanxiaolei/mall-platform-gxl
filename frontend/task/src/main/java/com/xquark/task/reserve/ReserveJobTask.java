package com.xquark.task.reserve;

import com.xquark.dal.mapper.PromotionReserveMapper;
import com.xquark.service.reserve.PromotionReserveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 预约预售定时任务
 */
@Component
public class ReserveJobTask {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PromotionReserveMapper promotionReserveMapper;

	@Autowired
	private PromotionReserveService promotionReserveService;

	/**
	 * 将状态改为开始
	 * 到预约时间以后就改变状态　每五分钟执行一次
	 */
	@Scheduled(cron = "0 */2 * * * ?")
	public void startReserveStatus(){
		try{
			log.info("begin on start reserve status");
			long begin = System.currentTimeMillis();
			int result = promotionReserveMapper.updateReserveStartStatus();
			if(result > 0){
				long end = System.currentTimeMillis();
				log.info("end on start reserve status");
				String info = "自动改变活动状态：" + (end - begin) + "ms";
				log.info(info);
			}
		}catch (Exception e){
			log.error("更改预约状态失败");
		}
	}

	/**
	 * 将状态改为结束
	 */
	@Scheduled(cron = "0 */2 * * * ?")
	public void endReserveStatus(){
		try{
			log.info("begin on end reserve status");
			long begin = System.currentTimeMillis();
			boolean isBoolean = promotionReserveService.endReserveStatus();
			if(isBoolean){
				long end = System.currentTimeMillis();
				log.info("end on end reserve status");
				String info = "自动改变活动状态：" + (end - begin) + "ms";
				log.info(info);
			}
		}catch (Exception e){
			log.error("更改预约状态失败");
		}
	}

}
