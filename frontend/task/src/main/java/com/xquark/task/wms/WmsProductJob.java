package com.xquark.task.wms;

import com.xquark.dal.mapper.SkuCombineMapper;
import com.xquark.dal.model.WmsProduct;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.product.ProductService;
import com.xquark.service.wms.WmsProductService;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/** User: kong Date: 18-6-11. Time: 下午8:02 Wms产品表 */
@Component
public class WmsProductJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  private static final Lock LOCK = new ReentrantLock();

  @Autowired private ProductService productService;

  @Autowired private WmsProductService wmsProductService;

  @Autowired private SkuCombineMapper skuCombineMapper;

  /** 每隔半小时提供一次数据 */
  @Scheduled(cron = "0 0/10 * * * ?")
  public void addData() {
    if (LOCK.tryLock()) {
      try {
        Date start = new Date();
        log.info("=== 开始同步wms库存 ===");
        StringBuffer info = new StringBuffer();
        List<WmsProduct> list = productService.getWmsProduct();
        AddOrUpdate(list);
        info.append("上传Wms产品表" + list.size() + "条");
        log.info("=== 同步wms库存结束=== 耗时: {} 秒", (new Date().getTime() - start.getTime()) / 1000);
        log.info(info.toString());
      } finally {
        LOCK.unlock();
      }
    }
  }

  @Transactional
  public void AddOrUpdate(List<WmsProduct> list) {
    Set<String> codes = wmsProductService.getCodeSet();
    for (WmsProduct w : list) {
      try {
        // 套装sku不同步
        if (skuCombineMapper.isCombineSku(IdTypeHandler.encode(w.getSkuId()))) {
          continue;
        }
        if (codes.contains(w.getCode())) {
          /*更新数据待优化*/
          wmsProductService.update(w);
        } else {
          wmsProductService.insert(w);
        }
        w.setEdiStatus(1);
        wmsProductService.update(w);
      } catch (Exception e) {
        log.error("Wms产品表sku编号:{}", w.getCode(), e);
      }
    }
  }
}
