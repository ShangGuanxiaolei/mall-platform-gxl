package com.xquark.task.pg;

import com.xquark.dal.mapper.PromotionOrderDetailMapper;
import com.xquark.service.cantuan.PieceGroupPayCallBackService;
import com.xquark.service.groupon.ActivityGrouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 活动相关定时任务，如团购、限时抢购等
 *
 * @author chh
 */
@Component
public class CommunationPasteJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  PieceGroupPayCallBackService pieceGroupPayCallBackService;

  @Autowired
  PromotionOrderDetailMapper promotionOrderDetailMapper;
  @Value("${profiles.active}")
  String profile;

  /**
   * 优惠活动时间到了后自动结束
   */
  //@Scheduled(cron = "0 */10 * * * ?")
  public void autoPromotionClose() {
    log.info("begin on activity autoPromotionClose");

    // 查询待修复的积分用户的团
    List<Map<String, Object>> pgList = promotionOrderDetailMapper.findPieceGroupInfo();
    if (null != pgList) {
      for (Map<String, Object> map : pgList) {
        String tpCode = (String) map.get("p_code");
        String tpTranCode = (String) map.get("piece_group_tran_code");
        pieceGroupPayCallBackService.effectOrder(tpCode, tpTranCode);
        log.info("修复：" + tpCode);
      }
    }


  }

}
