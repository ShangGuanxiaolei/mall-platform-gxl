package com.xquark.task.sf.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFCategory {

  @JSONField(name = "categoryCode")
  private String categoryCode;

  @JSONField(name = "categoryName")
  private String categoryName;

  @JSONField(name = "categoryId")
  private int categoryId;

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryId(int categoryId) {
    this.categoryId = categoryId;
  }

  public int getCategoryId() {
    return categoryId;
  }

  @Override
  public String toString() {
    return
        "SFCategory{" +
            "categoryCode = '" + categoryCode + '\'' +
            ",categoryName = '" + categoryName + '\'' +
            ",categoryId = '" + categoryId + '\'' +
            "}";
  }
}