package com.xquark.task.bargain;

import com.xquark.dal.mapper.PromotionBargainMapper;
import com.xquark.dal.model.PromotionBargain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BargainEndJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired PromotionBargainMapper bargainMapper;

  /** 每10分钟检查砍价活动，将结束时间到期的活动状态改为已结束 活动状态 0-未开始 1-已生效 2-已过期 3-人工下线 */
//  @Scheduled(cron = "0 0/10 * * * ?")
  public void autoCal() {
    log.info("begin on bargain status check");
    long now = System.currentTimeMillis();
    boolean flag = false;
    List<PromotionBargain> bargainList = bargainMapper.selectAllBargainForJob();
    for (PromotionBargain bargain : bargainList) {
      int status = bargain.getState();
      long validTo = bargain.getValidTo().getTime();
      if (now - validTo > 0) {
        flag = true;
      }
      if (1 == status && flag) {
        bargain.setState(2);
        bargainMapper.updateByPrimaryKey(bargain);
      }
    }

    log.info("end on bargain status check");
  }
}
