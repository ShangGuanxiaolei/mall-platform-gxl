package com.xquark.task.wms;

import com.xquark.dal.mapper.MemberInfoMapper;
import com.xquark.dal.model.WmsOrder;
import com.xquark.dal.model.WmsSku;
import com.xquark.dal.status.OrderStatus;
import com.xquark.service.order.OrderService;
import com.xquark.service.wms.WmsOrderService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/** User: kong Date: 18-6-13. Time: 上午9:50 Wms订单表定时任务 */
@Component
public class WmsOrderJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired private WmsOrderService wmsOrderService;

  @Autowired private OrderService orderService;

  @Autowired private MemberInfoMapper memberInfoMapper;

  /** 每隔半小时提供一次数据 */
  @Scheduled(cron = "0 0/10 * * * ?")
  public void getData() throws ParseException {
    log.info("开始同步wms订单数据");
    Date start = new Date();
    List<WmsOrder> wmsOrders = orderService.getWmsOrder();
    Set<String> orderNos = wmsOrderService.loadWmsOrderSet();

    // 拦截指定sku的发货。硬编码
    wmsOrders = interceptSpecifiedSku(wmsOrders);
    for (WmsOrder o : wmsOrders) {

      try{
        wmsOrderService.handleWmsOrder(o, orderNos);
      } catch (Exception e) {
        log.error("订单{}同步wms数据错误", o.getOrderNo(), e);
      }
    }
    log.info("wms订单数据同步完成, 耗时: {} s", (new Date().getTime() - start.getTime()) / 1000);
  }


  /** 每隔半小时提供一次数据 ,拉取导入数据*/
//  @Scheduled(cron = "0 0/5 * * * ?")
  public void getData2() throws ParseException {
    log.info("开始同步wms订单数据");
    Date start = new Date();
    List<WmsOrder> wmsOrders = orderService.getWmsOrder2();
    List<WmsOrder> eso =  wmsOrders.stream().filter(x -> x.getOrderNo().equals("ESO190618134817003007")).collect(Collectors.toList());

    Set<String> orderNos = wmsOrderService.loadWmsOrderSet();
    for (WmsOrder o : eso) {
      try{
        wmsOrderService.handleWmsOrder(o, orderNos);
      } catch (Exception e) {
        log.error("订单{}同步wms数据错误", o.getOrderNo(), e);
      }
    }
    log.info("wms订单数据同步完成, 耗时: {} s", (new Date().getTime() - start.getTime()) / 1000);
  }

  /** 每隔半小时获取一次数据 */
  @Scheduled(cron = "0 0/10 * * * ?")
  public void readData() {
    StringBuffer info = new StringBuffer();
    List<WmsOrder> list = wmsOrderService.getByWmsEdiStatus(1);
    changeWmsEdiStatusAndUpdate(list);
    info.append("获取Wms订单表数据一共" + list.size() + "条");
    log.info(info.toString());
  }

  public void changeWmsEdiStatusAndUpdate(List<WmsOrder> list) {
    for (WmsOrder w : list) {
      try {
        if (w.getStatus() != 9) {
          continue;
        }
        w.setWmsEdiStatus(2);
        wmsOrderService.update(w);
        /*发货状态未定义*/
        OrderStatus status = OrderStatus.SHIPPED;
        orderService.updateStatusByOrderNo(status, w.getOrderNo());
        w.setWmsEdiStatus(3);
        wmsOrderService.update(w);
      } catch (Exception e) {
        log.error("获取Wms订单表编号出错:{}", w.getOrderNo(), e);
      }
    }
  }

  private List<WmsOrder> interceptSpecifiedSku(List<WmsOrder> wmsOrders) {
    String skuCodeList = memberInfoMapper.selectSkuSystemConfig();//查询限制的商品skuCode
    String amount = memberInfoMapper.selectAmountSystemConfig();//查询限制商品id的库存数量
    String starttime = memberInfoMapper.selectStartTimeSystemConfig();//查询下单开始时间
    String endtime = memberInfoMapper.selectEndTimeSystemConfig();//查询下单结束时间

//    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    Date startTime = format.parse(starttime);
//    Date endTime = format.parse(endtime);

    List<WmsOrder> wmsOrderBySku = new ArrayList<>();

//    if(startTime.getTime()<=new Date().getTime()&&new Date().getTime()<=endTime.getTime()){
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    if (StringUtils.isEmpty(endtime) || LocalDateTime.now().isBefore(LocalDateTime.parse(endtime,formatter))) {
      String[] split = skuCodeList.split(",");
      List<String> skuCode = Arrays.asList(split);
      for (String skucode : skuCode) {
        List<WmsSku> wmsSkus = orderService.getWmsOrderSku(starttime,skucode);//查询购买该商品的所有订单

        if (CollectionUtils.isEmpty(wmsSkus)) {
          return wmsOrders;
        }

        // 当前查询到的包括指定sku的订单列表
        List<String> specifiedSkuOrderNos =
                wmsSkus.stream().map(WmsSku::getOrderNo).collect(Collectors.toList());

        List<String> nonWmsOrderNo = new ArrayList<>();

        Integer totalAmount = 0;
        for (WmsSku wmsSku : wmsSkus) {
          // 判断是否已超过可发货数量
          totalAmount += wmsSku.getAmount();
          if (totalAmount > Integer.valueOf(amount)) {
            break;
          }
          // 可发货订单号
          nonWmsOrderNo.add(wmsSku.getOrderNo());
        }
        // 遍历推送订单列表，过滤不能推送的订单
        for (WmsOrder wmsOrder : wmsOrders) {
          if (specifiedSkuOrderNos.contains(wmsOrder.getOrderNo())
                  && !nonWmsOrderNo.contains(wmsOrder.getOrderNo())) {
            continue;
          }
          // 能推送的订单集合
          wmsOrderBySku.add(wmsOrder);
          }
        }
      return wmsOrderBySku;
    }
    return wmsOrders;
  }
//        Integer count = 0;
//        int index = 0;
//        while (count < Integer.valueOf(amount)) {
//          count += wmsSkus.get(index).getAmount();
//          wmsOrderBySku.add(wmsSkus.get(index));
//          index++;
//        }
//
//        for (WmsSku wmsSku : wmsSkus) {
//          if (wmsOrderBySku.contains(wmsSku))
//            break;
//          else
//            nonWmsOrderNo.add(wmsSku.getOrderNo());
//        }
//
//        for (int i = wmsOrders.size() - 1; i >= 0; i--) {
//          if (nonWmsOrderNo.contains(wmsOrders.get(i).getOrderNo()))
//            wmsOrders.remove(i);
//        }


}
