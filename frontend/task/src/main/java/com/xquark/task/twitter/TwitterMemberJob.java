package com.xquark.task.twitter;

import com.xquark.dal.mapper.CommissionMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.partner.UserPartnerService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterLevelRelationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * 推客会员定时程序
 *
 * @author hhcao
 */
@Component
public class TwitterMemberJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  TwitterLevelRelationService twitterLevelRelationService;

  @Value("${profiles.active}")
  String profile;

  /**
   * 自动删除到期的推客会员 每天凌晨1点执行
   */
  @Scheduled(cron = "0 0 1 * * ?")
  public void autoClose() {
    log.info("begin on TwitterMember autoClose");
    long begin = System.currentTimeMillis();
    int result = twitterLevelRelationService.autoClose();
    long end = System.currentTimeMillis();
    log.info("end on TwitterMember autoClose");
    StringBuffer info = new StringBuffer();
    info.append("自动删除到期的推客会员：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

}
