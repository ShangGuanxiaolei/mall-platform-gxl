package com.xquark.task.promotionSkuRedisStock;

import com.xquark.dal.model.PromotionConfig;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionTempStockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
/**
 * 功能描述:活动库存从redis同步到db
 * @author Luxiaoling
 * @date 2019/1/3 10:14
 */
@Component
public class PromotionSkuRedisStockJob {

  private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private PromotionTempStockService promotionTempStockService;
  @Autowired
  private PromotionConfigService promotionConfigService;
  /**
   * 大会活动商品库存从redis同步到db
   */
  @Scheduled(cron = "0 0/5 * * * ?")
  public void synchroMeetingStock2DB() {

    PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
    String lock_type = "";
    if (null == lock_type_config) {
      lock_type = "DB";
    }
    lock_type = lock_type_config.getConfigValue();

    if ("DB".equals(lock_type)) {

      log.info("配置为DB时无需同步");
      return;
    }

    try {

      boolean b = promotionTempStockService.synchroMeetingStock2DB();
      log.info("大会活动商品库存从redis同步到db结果"+b);

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

  }


}
