package com.xquark.task.sf;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 下午3:35
 */
public class BoundedExecutor {

  private final Executor executor;

  private final Semaphore semaphore;

  public BoundedExecutor(Executor executor, int bound) {
    this.executor = executor;
    this.semaphore = new Semaphore(bound);
  }

  public void submit(final Runnable task) throws InterruptedException {
    semaphore.acquire();
    try {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          try {
            task.run();
          } finally {
            semaphore.release();
          }
        }
      });
    } catch (RejectedExecutionException e) {
      semaphore.release();
    }
  }
}
