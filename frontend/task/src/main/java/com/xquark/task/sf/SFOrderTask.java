package com.xquark.task.sf;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.SfAppConfigMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.status.OrderStatus;
import com.xquark.service.logistics.LogisticsGoodsService;
import com.xquark.service.vo.SubOrder;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * User: kong Date: 2018/7/4. Time: 21:31
 */
@Component
public class SFOrderTask {

  @Autowired
  private OrderMapper orderMapper;

  @Value("${sf.api.token.profile}")
  private String profile;

  @Value("${sf.api.host.url}")
  private String host;

  @Autowired
  private SfAppConfigMapper sfConfigMapper;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private LogisticsGoodsService logisticsGoodsService;

  /**
   * 定时任务获取物流
   */
//  @Scheduled(cron = "0 0/30 * * * ?")
  public void getLoc() {
    StringBuffer info = new StringBuffer();
    List<String> orderNos = orderMapper.getOrderNoByLoc();
    for (String s : orderNos) {
      getOrderLoc(s);
    }
    info.append("顺丰更新物流信息一共获取数据" + orderNos.size() + "条");
    log.info(info.toString());
  }

  private void getOrderLoc(String orderNo) {
    try {
      SfAppConfig appConfig = sfConfigMapper.selectByProfile(profile);
      String url = String.format("%s?app_key=%s&access_token=%s&timestamp=%s",
          host + SFApiConstants.SELECT_ORDER, appConfig.getClientId(),
          appConfig.getAccessToken(),
          String.valueOf(new Date().getTime()));
      Order order = orderMapper.getLogsticsByOrderNo(orderNo);
      if (order == null || order.getPartnerOrderNo() == null) {
        return;
      }
      List<SubOrder> subOrders = logisticsGoodsService.findSubOrder(order.getPartnerOrderNo());
      if (subOrders != null && subOrders.size() != 0) {
        for (int i = 0; i < subOrders.size(); i++) {
          SFUpdateOrder(subOrders.get(i).getOrderSn(), url, orderNo);
        }
        return;
      }
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("page", 1);
      jsonObject.put("pageSize", 9);
      jsonObject.put("queryParam", order.getPartnerOrderNo());
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String content = res.getContent();
      JSONObject jsonValue = JSONObject.parseObject(content).getJSONObject("data");
      if (jsonValue == null) {
        return;
      }
      JSONArray jsonArray = jsonValue.getJSONArray("orderItems");
      if (jsonArray.size() == 0) {
        return;
      }
      String shippingSn = jsonArray.getJSONObject(0).getString("shippingSn");
      Long shippingTime = jsonArray.getJSONObject(0).getLong("shippingTime") * 1000L;
      if (StringUtils.isNotBlank(shippingSn)) {
        Order orderLoc = new Order();
        orderLoc.setOrderNo(orderNo);
        orderLoc.setLogisticsOrderNo(shippingSn);
        orderLoc.setStatus(OrderStatus.SHIPPED);
        orderLoc.setShippedAt(new Date(shippingTime));
        orderMapper.updateOrderByOrderNo(orderLoc);
      }
    } catch (Exception e) {
      log.error("获取顺丰物流单号错误,订单编号" + orderNo, e);
    }
  }

  public void SFUpdateOrder(String orderSn, String url, String orderNo) {
    try {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("page", 1);
      jsonObject.put("pageSize", 9);
      jsonObject.put("queryParam", orderSn);
      HttpInvokeResult res = PoolingHttpClients.postJSON(url, jsonObject);
      String content = res.getContent();
      JSONObject jsonValue = JSONObject.parseObject(content).getJSONObject("data");
      if (jsonValue == null) {
        return;
      }
      JSONArray jsonArray = jsonValue.getJSONArray("orderItems");
      if (jsonArray.size() == 0) {
        return;
      }
      String shippingSn = jsonArray.getJSONObject(0).getString("shippingSn");
      Long shippingTime = jsonArray.getJSONObject(0).getLong("shippingTime") * 1000L;
      if (StringUtils.isNotBlank(shippingSn)) {
        Order orderLoc = new Order();
        orderLoc.setOrderNo(orderNo);
        orderLoc.setLogisticsOrderNo(shippingSn);
        orderLoc.setStatus(OrderStatus.SHIPPED);
        orderLoc.setShippedAt(new Date(shippingTime));
        orderMapper.updateOrderByOrderNo(orderLoc);
      }
    } catch (Exception e) {
      log.error("子订单中获取顺丰物流单号错误,订单编号" + orderNo, e);
    }
  }

}
