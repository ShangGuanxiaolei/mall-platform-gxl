package com.xquark.task.coupon;

import com.xquark.service.coupon.PromotionCouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by jason on 15-11-21.
 */
@Component
public class PromotionCouponJob {

  private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private PromotionCouponService promotionCouponService;

  /**
   * 优惠券到期自动关闭 每天12点 零点到05分期间的每1分钟触发执行一次
   */
  @Scheduled(cron = "0 0-5 0,12 * * ? ")
  public void autoClosePromotionCoupon() {
    long begin = System.currentTimeMillis();
    int result = promotionCouponService.autoClosePromotionCoupon();
    long end = System.currentTimeMillis();
    StringBuffer info = new StringBuffer();
    info.append("关闭优惠活动：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }
}


