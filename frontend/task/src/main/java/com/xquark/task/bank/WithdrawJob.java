package com.xquark.task.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.union.UnionService;

@Component
public class WithdrawJob {

  private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private WithdrawApplyService withdrawApplyService;

  @Autowired
  private UnionService unionService;

  @Value("${profiles.active}")
  String profile;

  /**
   * 定时生成提现申请 每天凌晨5分，发起提现申请
   */
//	@Scheduled(cron = "0 30 0 * * ?")
  public void autoWithdraw() {

    long begin = System.currentTimeMillis();
    Long result = withdrawApplyService.autoWithdrawByTask();
    long end = System.currentTimeMillis();
    StringBuffer info = new StringBuffer();
    info.append("执行自动提取：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

  /**
   * 把订单成功超过1天的佣金账户转入到可用账户
   */
  @Scheduled(cron = "0 5 22 * * ?")
  public void commissionToAvailable() {
    long begin = System.currentTimeMillis();
    Long result = unionService.autoCommissionToAvailable();
    long end = System.currentTimeMillis();
    if (result > 0) {
      StringBuffer sumLog = new StringBuffer();
      sumLog.append("/----执行佣金账户转入可用账户：");
      sumLog.append(result);
      sumLog.append("条,花费");
      sumLog.append(end - begin);
      sumLog.append("ms----/");
      log.info(sumLog.toString());
    } else {
      log.info("执行佣金账户转入可用账户:无可用任务");
    }
  }

  /**
   * 微信用户无需同步,否则会报错 定時同步提現账号 每天凌晨2分执行
   */
  //@Scheduled(cron = "0 2 0 * * ?")
  public void autoSynchronousWithdraw() {
    long begin = System.currentTimeMillis();
    Long result = withdrawApplyService.SynchronousWithdrawByTask();
    long end = System.currentTimeMillis();
    StringBuffer info = new StringBuffer();
    info.append("执行同步提现账号：");
    info.append(result);
    info.append("条，花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

  /**
   * 把超过7分钟的佣金账户转入到可用账户
   */
  //@Scheduled(cron = "0 */7 * * * ?")
  public void commissionToAvailableTest() {

    if (profile.toLowerCase().compareTo("test") == 0) {
      long begin = System.currentTimeMillis();
      Long result = unionService.autoCommissionToAvailableTest();
      long end = System.currentTimeMillis();
      if (result > 0) {
        StringBuffer sumLog = new StringBuffer();
        sumLog.append("/----测试 执行佣金账户转入可用账户：");
        sumLog.append(result);
        sumLog.append("条,花费");
        sumLog.append(end - begin);
        sumLog.append("ms----/");
        log.info(sumLog.toString());
      } else {
        log.info("测试 执行佣金账户转入可用账户:无可用任务");
      }
      log.info("only excuted in dev env");

    }
  }

}
