package com.xquark.task.exclusive;

import com.google.common.collect.Lists;
import com.xquark.dal.mapper.PromotionExclusiveMapper;
import com.xquark.dal.mapper.PromotionFlashSaleMapper;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.PromotionType;
import io.vavr.Function3;
import io.vavr.Tuple2;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author wangxinhua
 * @since 1.0
 */
@Component
public class ExclusiveReleaseJob {

    private final PromotionFlashSaleMapper flashSaleMapper;

    private final PromotionExclusiveMapper exclusiveMapper;

    @Autowired
    public ExclusiveReleaseJob(PromotionFlashSaleMapper flashSaleMapper, PromotionExclusiveMapper exclusiveMapper) {
        this.flashSaleMapper = flashSaleMapper;
        this.exclusiveMapper = exclusiveMapper;
    }

    /**
     * 释放活动被排除的商品
     */
    @Scheduled(cron = "0 */10 * * * ?")
    public void release() {
        final List<Tuple2<Long, PromotionType>> flashSaleList
                = flashSaleMapper.listClosedProductId()
                .stream()
                .map(id -> new Tuple2<>(id, PromotionType.FLASHSALE))
                .collect(Collectors.toList());
        release(flashSaleList);
    }

    private void release(List<Tuple2<Long, PromotionType>> list) {
        for (Tuple2<Long, PromotionType> t : list) {
            exclusiveMapper.updateStatus(t._1, t._2.name(), 0);
        }
    }

}
