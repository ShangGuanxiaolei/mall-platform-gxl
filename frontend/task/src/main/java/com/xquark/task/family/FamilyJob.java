package com.xquark.task.family;

import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.service.order.OrderService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.UserCardService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单相关的定时程序
 *
 * @author odin
 */
@Component
public class FamilyJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  UserCardService userCardService;

  @Autowired
  ShopTreeService shopTreeService;

  @Autowired
  OrderService orderService;

  @Autowired
  FamilyCardSettingService familyCardSettingService;

  @Value("${profiles.active}")
  String profile;


  /**
   * 家族卡动态调整 每月5号0点检查一次
   */
  //@Scheduled(cron = "0 0 0 5 * ?")
  public void autoUpOrDown() {
    log.info("begin on family card autoUpOrDown");
    long begin = System.currentTimeMillis();
    List<ShopTree> listRootShop = shopTreeService.listRootShopTree();
    for (ShopTree rootShopTree : listRootShop) {
      String rootShopId = rootShopTree.getRootShopId();
      if (StringUtils.isNotBlank(rootShopId)) {
        List<ShopTree> listShopTree = shopTreeService.selectChildren(rootShopId);
        for (ShopTree shopTree : listShopTree) {
          String shopId = shopTree.getDescendantShopId();
          Order order = orderService.selectLastMonthTotalFeeByShopId(shopId);
          String sellerId = order.getSellerId();
          BigDecimal totalFee = order.getTotalFee();
          FamilyCardSetting familyCardSetting = familyCardSettingService
              .selectCardByShopIdAndFee(rootShopId, totalFee);
          String cardId = familyCardSetting.getId();
          UserCard userCard = userCardService
              .selectUserCardsByUserIdAndShopId(sellerId, rootShopId);
          if (userCard != null) {
            userCard.setCardId(cardId);
            userCardService.update(userCard);
          } else {
            userCard = new UserCard();
            userCard.setCardId(cardId);
            userCard.setUserId(sellerId);
            userCard.setOwnerShopId(rootShopId);
            userCardService.insert(userCard);
          }
        }
      }
    }
    long end = System.currentTimeMillis();
    log.info("end on family card autoUpOrDown");
    StringBuffer info = new StringBuffer();
    info.append("family card autoUpOrDown：花费：");
    info.append(end - begin);
    info.append("ms");
    log.info(info.toString());
  }

}
