package com.xquark.task.membercard;

import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.type.MemberUpgradeType;
import com.xquark.dal.vo.MemberCardUserVO;
import com.xquark.dal.vo.UserOrderDataVO;
import com.xquark.service.memberCard.MemberCardService;
import com.xquark.service.user.UserService;
import com.xquark.userFamily.UserCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wangxinhua on 17-9-4. DESC:
 */
@Component
public class MemberCardJob {

  private MemberCardService memberCardService;

  private UserService userService;

  private UserCardService userCardService;

  private static Logger logger = LoggerFactory.getLogger(MemberCardJob.class);

  @Autowired
  public void setMemberCardService(MemberCardService memberCardService) {
    this.memberCardService = memberCardService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setUserCardService(UserCardService userCardService) {
    this.userCardService = userCardService;
  }

  /**
   * 每天自动将符合条件的用户升级为对应会员卡的级别
   */
  @Scheduled(cron = "0 0/2 * * * ?")
  @Transactional
  public void autoUpgrade() {
    List<MemberCardUserVO> userCardVOs = memberCardService
        .listMemberCardUsersByUpgradeType(MemberUpgradeType.AUTO);
    for (MemberCardUserVO cardUserVO : userCardVOs) {
      // 所有满足自动升级条件的用户
      List<UserOrderDataVO> users = cardUserVO.getUsers();
      for (UserOrderDataVO user : users) {
        String userId = user.getId();
        UserCard userCard;
        try {
          userCard = userCardService.selectUserCardsByUserId(userId);
          if (userCard == null) {
            // 未绑定过会员卡, 将该卡与该用户绑定
            userCard = new UserCard(userId, cardUserVO.getId());
            userCardService.insert(userCard);
          } else {
            // 用户已经绑定过会员卡
            MemberCard bindedCard = memberCardService.load(userCard.getCardId());
            if (cardUserVO.getId().equals(bindedCard.getId())) {
              continue;
            }
            int bindedCardLevel = bindedCard.getLevel();
            int currLevel = cardUserVO.getLevel();
            // 绑定的会员卡级别如果小于当前满足条件的级别则更新为更高级的会员卡
            if (bindedCardLevel < currLevel) {
              userCard.setCardId(cardUserVO.getId());
              userCardService.update(userCard);
            }
          }
        } catch (Exception e) {
          logger.error("会员卡自动升级失败", e);
        }
      }
    }
  }

}
