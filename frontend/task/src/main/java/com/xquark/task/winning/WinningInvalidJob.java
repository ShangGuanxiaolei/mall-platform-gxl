package com.xquark.task.winning;

import com.xquark.service.winningList.WinningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 奖品失效的定时程序
 */
@Component
public class WinningInvalidJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WinningService winningService;

  /**
   * 如果超过24小时未领取奖品视为奖品失效 每5分钟执行一次
   */
  @Scheduled(cron = "0 */1 * * * ?")
  public void invalidWinning() {
    try {
      log.info("begin on winning invalid");
      long begin = System.currentTimeMillis();
      boolean result = winningService.isValid();
      if(result){
        long end = System.currentTimeMillis();
        log.info("end on winning invalid");
        String info = "自动将奖品的状态变更为已失效：" + (end - begin) + "ms";
        log.info(info);
      }
    }catch (Exception e){
      log.error("处理失效奖品失败");
    }
  }

}
