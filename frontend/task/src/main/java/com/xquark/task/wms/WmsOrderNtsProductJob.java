package com.xquark.task.wms;

import com.xquark.service.wms.WmsOrderNtsProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * User: kong Date: 18-6-12. Time: 下午4:36 Wms发货通知单产品表定时任务
 */
@Component
public class WmsOrderNtsProductJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WmsOrderNtsProductService wmsOrderNtsProductService;



  /**
   * 每隔半小时更新数据
   */
//  @Scheduled(cron = "0 0/30 * * * ?")
  public void getData() {
    StringBuffer info = new StringBuffer();
    Integer length = wmsOrderNtsProductService.getWmsOrderNtsProduct(null);
    info.append("更新数据Wms发货通知单表一共" + length + "条");
    log.info(info.toString());
  }


}
