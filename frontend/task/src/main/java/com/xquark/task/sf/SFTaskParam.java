package com.xquark.task.sf;

import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.product.SFProductService;
import java.util.Set;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 上午10:59
 */
public class SFTaskParam {

  private final String host;

  private final int page;

  private final int size;

  private final SfAppConfig appConfig;

  private final SFProductService sfProductService;

  private final Set<String> baseEncodes;

  private final User user;

  private final Shop shop;

  private SFTaskParam(String host, int page, int size, SfAppConfig appConfig,
      SFProductService sfProductService, Set<String> baseEncodes, User user,
      Shop shop) {
    this.host = host;
    this.page = page;
    this.size = size;
    this.appConfig = appConfig;
    this.sfProductService = sfProductService;
    this.baseEncodes = baseEncodes;
    this.user = user;
    this.shop = shop;
  }

  public String getHost() {
    return host;
  }

  public int getPage() {
    return page;
  }

  public int getSize() {
    return size;
  }

  public SfAppConfig getAppConfig() {
    return appConfig;
  }

  public SFProductService getSfProductService() {
    return sfProductService;
  }

  public Set<String> getBaseEncodes() {
    return baseEncodes;
  }

  public User getUser() {
    return user;
  }

  public Shop getShop() {
    return shop;
  }


  public static final class Builder {

    private String host;
    private int page;
    private int size;
    private SfAppConfig appConfig;
    private SFProductService sfProductService;
    private Set<String> baseEncodes;
    private User user;
    private Shop shop;

    private Builder() {
    }

    public static Builder aSFTaskParam() {
      return new Builder();
    }

    public Builder withHost(String host) {
      this.host = host;
      return this;
    }

    public Builder withPage(int page) {
      this.page = page;
      return this;
    }

    public Builder withSize(int size) {
      this.size = size;
      return this;
    }

    public Builder withAppConfig(SfAppConfig appConfig) {
      this.appConfig = appConfig;
      return this;
    }

    public Builder withSfProductService(SFProductService sfProductService) {
      this.sfProductService = sfProductService;
      return this;
    }

    public Builder withBaseEncodes(Set<String> baseEncodes) {
      this.baseEncodes = baseEncodes;
      return this;
    }

    public Builder withUser(User user) {
      this.user = user;
      return this;
    }

    public Builder withShop(Shop shop) {
      this.shop = shop;
      return this;
    }

    public SFTaskParam build() {
      return new SFTaskParam(host, page, size, appConfig, sfProductService, baseEncodes, user,
          shop);
    }
  }
}
