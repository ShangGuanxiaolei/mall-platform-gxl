package com.xquark.task.push;

import com.xquark.service.activity.ActivityPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author luqing
 * @since 2019-04-29
 */
@Component
public class ActivityPushJob {

  @Autowired
  private ActivityPushService activityPushService;

  @Scheduled(cron = "0 */2 * * * ?")
  public void sendMessage(){
    activityPushService.pushMessage();
  }

}
