package com.xquark.task.product;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xquark.dal.mapper.PromotionConfigMapper;
import com.xquark.dal.model.PromotionConfig;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.SFProductService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huxaya 示例：
 * @Scheduled(cron = "0 0/30 * * * ?") 每半小时执行一次自动上架
 * @Scheduled(fixedRate = 1000*3) 启动时执行一次，之后每隔3秒钟执行一次
 * @Scheduled(fixedDelay = 1000*2) 在每次调度完成后都delay相同时间
 */

@Component
public class ProductJob {

  private Logger log = LoggerFactory.getLogger(getClass());
  @Autowired
  private ProductService productService;
  @Autowired
  private SFProductService sFProductService;

  @Autowired
  private PromotionConfigMapper promotionConfigMapper;
  /**
   * 定时上架 每5分钟执行一次自动上架
   */
  //@Scheduled(cron = "0 */5 * * * ?")
  public void autoOnSale() {

//    long begin = System.currentTimeMillis();
//    int result = productService.autoOnSaleByTask();
//    long end = System.currentTimeMillis();
//    StringBuffer info = new StringBuffer();
//    info.append("执行自动上架：");
//    info.append(result);
//    info.append("条，花费：");
//    info.append(end - begin);
//    info.append("ms");
//    log.info(info.toString());
  }

  /**
   * 库存为0商品自动下架 每5分钟执行一次自动下架
   */
  //@Scheduled(cron = "0 */5 * * * ?")
  public void autoInstock() {
//    long begin = System.currentTimeMillis();
////    int result = productService.autoInstockByTask();
//    int result = productService.checkStockAndInStock();
//    long end = System.currentTimeMillis();
//    String info = "执行自动下架："
//        + result
//        + "条，花费："
//        + (end - begin)
//        + "ms";
//    log.info(info);
  }

  /**
   * 顺丰商品自动下架. 2.0版顺丰推送，关闭定时任务
   */
//  @Scheduled(cron = "0 0 0 * * ?")
  public void autoInstockSF() {
    this.beginInstockJob();
  }

  private boolean beginInstockJob() {
    boolean flag = this.autoInstockSwitch();
    if(flag){
      return false;
    }
    long begin = System.currentTimeMillis();
    log.info("顺丰商品自动下架job开始执行");
    boolean b = sFProductService.autoInstockSF();
    if(b){
      log.info("顺丰商品自动下架执行完毕");
    }else {
      log.error("顺丰商品自动下架执行出错");
    }
    long end = System.currentTimeMillis();
    log.info("顺丰商品自动下架job执行结束，花费："+(end - begin)+"ms");
    return true;
  }

  private boolean autoInstockSwitch() {
    Map map = new HashMap();
    map.put("configName","PRODUCT_SF_AUTO_INSTOCK_SWITCH");
    //是否开启顺丰同步已下架的开关,off关闭
    PromotionConfig config = promotionConfigMapper.selectByConfig(map);
    if(config != null){
      if(StringUtils.isNotBlank(config.getConfigValue())
              && "off".equals(config.getConfigValue())){
        return true;
      }
    }
    return false;
  }
}
