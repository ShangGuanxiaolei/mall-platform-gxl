package com.xquark.task.wms;

import com.xquark.dal.model.WmsBatch;
import com.xquark.service.wms.WmsBatchService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * User: kong Date: 18-6-12. Time: 下午3:44 Wms批次表定时任务
 */
@Component
public class WmsBatchJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WmsBatchService wmsBatchService;

  @Scheduled(cron = "0 0/10 * * * ?")
  public void readData() {
    StringBuffer info = new StringBuffer();
    List<WmsBatch> list = wmsBatchService.getByEdiStatus(1);
    changeEdiStatusAndUpdate(list);
    info.append("Wms批次表一共获取数据" + list.size() + "条");
    log.info(info.toString());
  }

  public void changeEdiStatusAndUpdate(List<WmsBatch> list) {
    for (WmsBatch w : list) {
      w.setEdiStatus(2);
      wmsBatchService.update(w);
      /*批次表未创建*/
      w.setEdiStatus(3);
      wmsBatchService.update(w);
    }
  }
}
