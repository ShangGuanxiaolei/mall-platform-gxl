package com.xquark.task.wms;

import com.xquark.dal.model.WmsExpress;
import com.xquark.service.order.OrderService;
import com.xquark.service.wms.WmsExpressService;
import com.xquark.service.wms.WmsOrderService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * User:kong  Date: 18-6-11. Time: 上午10:59 WMS快递信息定时任务
 */
@Component
public class WmsExpressJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private WmsExpressService wmsExpressService;

  @Autowired
  private OrderService orderService;

  /**
   * 每隔一小时获取数据
   */
  @Scheduled(cron = "0 0/10 * * * ?")
  public void readDate() {
    StringBuffer info = new StringBuffer();
    List<WmsExpress> list = wmsExpressService.getByEdiStatusAndStatus(1, 0);
    changeEdiStatusAndUpdate(list);
    info.append("获取Edi快递表数据一共" + list.size() + "条");
    log.info(info.toString());
  }

  /**
   * 每隔四小时获取数据
   */
//  @Scheduled(cron = "0 0 0/4 * * ?")
  public void readDateSlow() {
    StringBuffer info = new StringBuffer();
    List<WmsExpress> list = wmsExpressService.getByEdiStatusAndStatus(1, 1);
    changeEdiStatusAndUpdate(list);
    info.append("获取Edi快递表数据一共" + list.size() + "条");
    log.info(info.toString());
  }

  public void changeEdiStatusAndUpdate(List<WmsExpress> list) {
    for (WmsExpress w : list) {
      try {
        w.setEdiStatus(2);
        Boolean buttum = wmsExpressService.update(w);
        orderService.updateByOrderNo(w);
        w.setEdiStatus(3);
        wmsExpressService.update(w);
      } catch (Exception e) {
        log.error("快递表数据快递单号:{}", w.getOrderNo(), e);
      }
    }
  }


}
