package com.xquark.task.bonus;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.UserAgentMapper;
import com.xquark.dal.model.Bonus;
import com.xquark.dal.model.BonusDetail;
import com.xquark.dal.model.UserAgent;
import com.xquark.dal.status.BonusStatus;
import com.xquark.dal.status.BonusType;
import com.xquark.service.bonus.BonusService;
import com.xquark.service.order.OrderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 年度代理分红调度
 *
 * @author chh 2017-06-08
 */
@Component
public class BonusYearJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  OrderService orderService;

  @Autowired
  BonusService bonusService;

  @Autowired
  UserAgentMapper userAgentMapper;

  @Autowired
  OrderMapper orderMapper;

  @Value("${profiles.active}")
  String profile;

  // 单笔进货量
  int perCount = 400;

  /**
   * 计算上一个年度所有董事与联合执行人的年度分红 每年1月2号凌晨3点开始执行
   */
  //@Scheduled(cron = "0 0/3 * * * ?")
  public void autoCal() {
    log.info("begin on bonus year Cal");
    long begin = System.currentTimeMillis();
    // 首先查询是否有相同年度，年度的分红记录，如果有，将之前的记录删除，重新计算
    /**String year = getLastYear(new Date());
     Bonus bonus = bonusService.getBonusYearByPeriod(year);
     if(bonus != null){
     bonusService.deleteBonus(bonus.getId());
     }
     // 新增代理分红记录，开始进行计算
     Bonus newBonus = new Bonus();
     newBonus.setArchive(false);
     newBonus.setYear(year);
     newBonus.setType(BonusType.YEAR);
     bonusService.insertBonus(newBonus);
     // 获取所有董事和联合创始人代理
     List<UserAgent> userAgents = userAgentMapper.listAllDirectorAndFounder();
     int result = 0;
     for(UserAgent userAgent : userAgents){
     String userId = userAgent.getUserId();
     // 董事级别A达到的次数
     long aCount = 0;
     // 董事级别B达到的次数
     long bCount = 0;
     // 董事级别C达到的次数
     long cCount = 0;
     // 董事级别D达到的次数
     long dCount = 0;
     String level = "";
     // 先获取某个代理某年月度分红各个级别数量
     List<Map> months = bonusService.getBonusMonthCount(year, userId);
     aCount = getCount(months, "A");
     bCount = getCount(months, "B");
     cCount = getCount(months, "C");
     dCount = getCount(months, "D");
     BigDecimal rate = new BigDecimal("0");
     if(dCount >= 6){
     rate = new BigDecimal("4");
     level = "D";
     }else if(cCount >= 6){
     rate = new BigDecimal("3.45");
     level = "C";
     }else if(bCount >= 6){
     rate = new BigDecimal("2.8");
     level = "B";
     }else if(aCount >= 6){
     rate = new BigDecimal("2");
     level = "A";
     }else if((aCount + bCount + cCount + dCount) >= 6){
     rate = new BigDecimal("2");
     level = "A";
     }

     //if(StringUtils.isNotEmpty(level)){
     // 整个团队上一个年度完成订单的总金额
     BigDecimal amount = orderMapper.countLastYearOrderAmount(userId);
     // 将该代理月度分红数据插入数据库中
     BonusDetail bonusDetail = new BonusDetail();
     bonusDetail.setParentId(newBonus.getId());
     bonusDetail.setLevel(level);
     bonusDetail.setRate(rate);
     bonusDetail.setAmount(amount);
     //bonusDetail.setFee(amount.multiply(rate.divide(new BigDecimal("100"),4,BigDecimal.ROUND_HALF_UP)).setScale(2,BigDecimal.ROUND_HALF_UP));
     bonusDetail.setStatus(BonusStatus.NEW);
     bonusDetail.setUserId(userId);
     bonusDetail.setArchive(false);
     bonusService.insertBonusDetail(bonusDetail);
     result ++;
     //}
     }

     // 找出该年份每个等级对应的代理总数
     List<Map> years = bonusService.getTotalBonusYearCount(year);
     for(Map yearCount : years){
     String level = (String)yearCount.get("level");
     String count = "" + yearCount.get("count");
     // 每个代理均分这个等级的分红金额
     if(StringUtils.isNotEmpty(level) && StringUtils.isNotEmpty(count) && !"0".equals(count)){
     bonusService.updateBonusYearFee(year, level, count);
     }
     }

     bonusService.updateBonusEndTime(newBonus.getId());
     long end = System.currentTimeMillis();
     log.info("end on bonus year Cal");
     StringBuffer info = new StringBuffer();
     info.append("计算上一个年度所有董事与联合执行人的年度分红");
     info.append(result);
     info.append("，花费：");
     info.append(end-begin);
     info.append("ms");
     log.info(info.toString());**/
  }

  private int getCount(List<Map> months, String level) {
    int count = 0;
    for (Map month : months) {
      if (month.get("level") != null && month.get("level").equals(level)) {
        count = new BigDecimal("" + month.get("count")).intValue();
      }
    }
    return count;
  }

  private String getLastYear(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.add(Calendar.YEAR, -1);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    return sdf.format(cal.getTime());
  }


}
