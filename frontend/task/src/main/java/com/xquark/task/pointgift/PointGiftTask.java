package com.xquark.task.pointgift;

import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.service.point.PointServiceApi;
import com.hds.xquark.service.point.type.FunctionCodeType;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.model.PointGift;
import com.xquark.dal.model.PointGiftDetail;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.dto.PointSendType;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 过期的红包退回
 *
 * @author Jack Zhu
 * @date 2018/12/11
 */
@Component
public class PointGiftTask {
    private final PointGiftService pointGiftService;
    private final PointServiceApi pointService;
    private Lock lock = new ReentrantLock();
    private final RedisUtils<BigDecimal> redisUtils = RedisUtilFactory.getInstance(BigDecimal.class);

    @Autowired
    public PointGiftTask(PointGiftService pointGiftService, PointContextInitialize pointContextInitialize) {
        this.pointGiftService = pointGiftService;
        this.pointService = pointContextInitialize.getPointServiceApi();
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(PointGiftTask.class);

    /**
     * 过期的红包退回 一个小时执行一次
     */
    @Scheduled(fixedRate = 1000 * 60 * 60)
    public void pointGiftRefund() {
        if (lock.tryLock()) {
            try {
                LOGGER.info("------------------执行定时任务,退回红包---------------------");
                List<PointGift> pointGifts = pointGiftService.listExpiredPacket();
                //退回操作
                if (CollectionUtils.isNotEmpty(pointGifts)) {
                    for (PointGift pointGift : pointGifts) {
                        final String key = pointGift.getPointGiftNo();
                        BigDecimal number = BigDecimal.ZERO;
                        while (redisUtils.hasKey(key)) {
                            final List<BigDecimal> values = redisUtils.lRangeAll(key);
                            for (BigDecimal value : values) {
                                number = number.add(value);
                            }
                            redisUtils.del(key);
                        }
                        if(!number.equals(BigDecimal.ZERO)){
                            returnPoint(pointGift, number);
                        }
                    }
                    // 修改红包状态
                    pointGiftService.updatePointGiftToExpired(pointGifts);
                }
            } catch (Exception e) {
                LOGGER.info("退回操作失败 原因:{}", e.getMessage());
            } finally {
                lock.unlock();
            }
        }
    }

    private void returnPoint(PointGift pointGift, BigDecimal number) {
        pointService.modify(pointGift.getCpid(),
                pointGift.getPointGiftNo(),
                FunctionCodeType.getPacketReturn(),
                PlatformType.E, number.abs());
    }

    private PointGiftDetail getPointGiftDetail(PointGift pointGift, BigDecimal number) {
        final PointGiftDetail pointGiftDetail = new PointGiftDetail();
        pointGiftDetail.setBizType(PointSendType.REFUND.name());
        pointGiftDetail.setBizId(pointGift.getPointGiftNo());
        pointGiftDetail.setCpId(pointGift.getCpid());
        pointGiftDetail.setAmount(number);
        return pointGiftDetail;
    }
}
