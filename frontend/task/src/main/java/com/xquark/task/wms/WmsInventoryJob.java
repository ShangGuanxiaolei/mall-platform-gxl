package com.xquark.task.wms;

import com.xquark.dal.model.WmsInventory;
import com.xquark.service.wms.WmsInventoryService;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/** User: kong Date: 18-6-12. Time: 下午12:15 Wms库存信息表定时任务 */
@Component
public class WmsInventoryJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired private WmsInventoryService wmsInventoryService;

  /**
   * 每隔半小时获取数据
   */
  @Scheduled(cron = "0 0/60 * * * ?")
  public void readDate() {
    log.info("开始同步edi库存");
    Date start = new Date();
    List<WmsInventory> list = wmsInventoryService.getByEdiStatus(1, start);
    wmsInventoryService.changeEdiStatusAndUpdate(list, start);
    log.info("更新edi库存数" + list.size() + "条, 花费时间: {} s", (new Date().getTime() - start.getTime()) / 1000);
  }
}
