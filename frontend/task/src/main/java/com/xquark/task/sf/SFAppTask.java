package com.xquark.task.sf;

import static com.xquark.service.product.SFTaskGlobalContext.APP_CONFIG;
import static com.xquark.service.product.SFTaskGlobalContext.HOST;
import static com.xquark.service.product.SFTaskGlobalContext.SIZE;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.xquark.dal.consts.SFApiConstants;
import com.xquark.dal.mapper.RegionDict;
import com.xquark.dal.mapper.RegionMapper;
import com.xquark.dal.model.SfAppConfig;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.helper.Transformer;
import com.xquark.service.http.pojo.sf.product.SFRegion;
import com.xquark.service.http.pojo.sf.product.SFRegionWrapper;
import com.xquark.service.product.PullProductTask;
import com.xquark.service.product.SFProductService;
import com.xquark.service.product.SFTaskGlobalContext;
import com.xquark.service.product.SFTaskHelper;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * created by
 *
 * @author wangxinhua at 18-5-30 下午10:11
 */
@Component
public class SFAppTask {

  private final static Logger LOGGER = LoggerFactory.getLogger(SFAppTask.class);

  private final static Function<SFRegion, RegionDict> REGION_TRANSFORM_FUNC
      = new Function<SFRegion, RegionDict>() {
    @Override
    public RegionDict apply(SFRegion sfRegion) {
      return new RegionDict(sfRegion.getRegionName(), sfRegion.getRegionId(),
          sfRegion.getRegionType(), sfRegion.getRegionName(), sfRegion.getParentId());
    }
  };

  @Value("${sf.api.host.url}")
  private String host;

  @Value("${sf.api.token.profile}")
  private String profile;


  @Autowired
  private SFProductService sfProductService;

  @Autowired
  private RegionMapper regionMapper;

  @Scheduled(cron = "0 */30 * * * ?")
  public void renewToken() {
    sfProductService.renewToken();
  }


  /**
   * 每天12点定时拉取商品
   */
//  @Scheduled(cron = "0 */1 * * * ?")
//  @Scheduled(cron = "0 0 12 * * ?")
  public void pullProducts() {
    // 限制任务队列的大小, 防止内存溢出
    sfProductService.pullProducts(null);
  }

  /**
   * 供测试, 拉取单商品
   */
  public void syncSingleProduct() {
    Map<String, Object> userInfoMap = sfProductService.loadUserInfoForAdmin();
    User user = (User) userInfoMap.get("user");
    Shop shop = (Shop) userInfoMap.get("shop");

    APP_CONFIG = sfProductService.loadConfig();
    SIZE = 1;
    SFTaskGlobalContext.SF_PRODUCT_SERVICE = sfProductService;
    HOST = host;
    SFTaskGlobalContext.USER = user;
    SFTaskGlobalContext.SHOP = shop;
    SFTaskGlobalContext.BASE_ENCODES = ImmutableSet.of("11111");

    new PullProductTask(5).run();
    sfProductService.pullProductDetails(null);
  }

  /**
   * 拉取地区
   */
  public void pullRegion() {
    //获取请求的参数,最新的access_token
    SfAppConfig appConfig = sfProductService.loadConfig();
    //具体的请求地址
    String pullRegionUrl = String.format("%s?app_key=%s&access_token=%s",
        host + SFApiConstants.PULL_REGION, appConfig.getClientId(),
        appConfig.getAccessToken());
    SFRegionWrapper regionWrapper = SFTaskHelper.requestRegion(pullRegionUrl);
    List<SFRegion> regionList = regionWrapper.getRegionList();
    if (CollectionUtils.isNotEmpty(regionList)) {
      // 插入区域信息
      LOGGER.info("======= 开始插入区域信息, 共 {} 条 =======", regionList.size());
      List<RegionDict> regionToInsert = Transformer.fromIterable(regionList, REGION_TRANSFORM_FUNC);
      // 批量更新
      int ret = regionMapper.insertRegion(regionToInsert);
      LOGGER.info("======= 区域信息插入结束, 成功插入 {} 条 =======", ret);
    }
  }
}
