package com.xquark.task.packetRain;

import com.xquark.dal.mapper.PromotionLotteryMapper;
import com.xquark.dal.mapper.PromotionPacketRainMapper;
import com.xquark.dal.mapper.PromotionPacketRainTimeMapper;
import com.xquark.service.cache.GuavaCacheManager;
import com.xquark.service.lottery.LotteryJobService;
import com.xquark.service.lottery.LotteryService;
import com.xquark.service.packetRain.PacketRainJobService;
import com.xquark.service.packetRain.PacketRainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @author hs
 */
@Component
public class PacketRainPointLimitJob {

    private final Logger log = LoggerFactory.getLogger(PacketRainPointLimitJob.class);

    private Random random = new Random();

    private PacketRainService packetRainService;

    @Autowired
    PromotionPacketRainMapper promotionPacketRainMapper;

    @Autowired
    PromotionLotteryMapper promotionLotteryMapper;

    @Autowired
    private GuavaCacheManager guavaCacheManager;

    @Autowired
    public void setPacketRainService(PacketRainService packetRainService) {
        this.packetRainService = packetRainService;
    }

    @Autowired
    private PromotionPacketRainTimeMapper promotionPacketRainTimeMapper;

    @Autowired
    private LotteryJobService lotteryJobService;

    @Autowired
    private PacketRainJobService rainJobService;

    @Autowired
    private LotteryService lotteryService;

    @Scheduled(cron = "0 */5 * * * ?")
//    @Scheduled(fixedRate = 1000*60)
    public void lotteryInit() {
        lotteryJobService.init();
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void rainInit() {
        rainJobService.init(true);
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void lotteryPrizeInit() {
        lotteryJobService.initPrices(false)
                .orElseRun(err -> log.error("初始化奖品队列失败-> {}", err));
    }

    /**
     * 每5分钟发放部分奖品
     */
    @Scheduled(cron = "0 */5 * * * ?")
//    @Scheduled(fixedRate = 1000*60)
    public void putOrdinaryPrize() {
        lotteryJobService.pushPrices(false)
                .orElseRun(err -> log.error("发放奖品失败-> {}", err));
    }
}
