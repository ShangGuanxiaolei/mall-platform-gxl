package com.xquark.task.activity;

import com.xquark.dal.model.ActivityPush;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.model.PromotionPgDetail;
import com.xquark.dal.model.PromotionPgPrice;
import com.xquark.dal.type.ActionType;
import com.xquark.dal.vo.PromotionPgMemberInfoVO;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import com.xquark.service.pintuan.PromotionPgPriceService;
import com.xquark.service.pintuan.RecordPromotionPgDetail;
import com.xquark.service.pintuan.RecordPromotionPgTranInfoService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.push.type.CastType;
import com.xquark.service.push.type.Platform;
import com.xquark.utils.MessageCastUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * 活动相关定时任务，如团购、限时抢购等
 *
 * @author chh
 */
@Component
public class PiecePushJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private RecordPromotionPgDetail recordPromotionPgDetail;

  @Autowired
  private PromotionPgPriceService promotionPgPriceService;

  @Autowired
  private RecordPromotionPgTranInfoService recordPromotionPgTranInfoService;

  @Autowired
  private PromotionPgMemberInfoService promotionPgMemberInfoService;

  /**
   * 拼团活动时间前一天21点推送消息
   */
  @Scheduled(cron = "0 0 21 * * ?")
  public void autoPushPromotion() {
    log.info("begin on piece autoPush");
    List<PromotionBaseInfo> promotionBaseInfoList = promotionBaseInfoService.selectByPush();
    if (promotionBaseInfoList == null) {
      return;
    }
    List<BigDecimal> priceList = new ArrayList<>();
    for (PromotionBaseInfo promotionBaseInfo :
            promotionBaseInfoList) {
      if (promotionBaseInfo != null) {
        String pCode = promotionBaseInfo.getpCode();
        if (StringUtils.isBlank(pCode)) {
          continue;
        }
        List<PromotionPgDetail> promotionPgDetails = recordPromotionPgDetail.selectAllByPCode(pCode);
        if (promotionPgDetails == null) {
          continue;
        }
        for (PromotionPgDetail promotionPgDetail :
                promotionPgDetails) {
          String detailCode = promotionPgDetail.getpDetailCode();
          if (StringUtils.isBlank(detailCode)) {
            continue;
          }
          PromotionPgPrice promotionPgPrice = promotionPgPriceService.selectPromotionPgPriceByDetailCode(detailCode);
          if (promotionPgPrice == null) {
            continue;
          }
          BigDecimal promotionPrice = promotionPgPrice.getPromotionPrice();
          if (promotionPrice == null) {
            continue;
          }
          priceList.add(promotionPrice);
        }
      }
    }
    BigDecimal minPrice = Collections.min(priceList);
    if (minPrice != null) {
      Map<String, String> map = new HashMap<>();
      String str = minPrice.toString();
      map.put("Y", rvZeroAndDot(str));
      ActionType ap = ActionType.ASSEMBLE_BUR_WHOLE_NOTICE;
      String content = MessageCastUtils.replaceContent(ap.getContent(), map);
      CastType.BROADCAST.pushMassage(Platform.ALL, null, ap.getPriority(),"",
              ap.getAbbreviation(), content);
    }
  }

  /**
   * 品类日指定日期10点推送消息
   */
  @Scheduled(cron = "0 0 10 17,18,19,20 5 ?")
  public void autoPushPromotionCategory() {
    log.info("begin on piece category autoPush");
    ActionType ap = ActionType.ASSEMBLE_SINGLE_COUNTDOWN;
    Date date = new Date();
    ActivityPush activityPush = ap.createActivityPush(date, "");
    if (activityPush == null) {
      return;
    }
    CastType.BROADCAST.pushMassage(Platform.ALL, null, activityPush.getPriority(),"",
            activityPush.getAbbreviation(), activityPush.getContent());
  }

  @Scheduled(cron = "0 */5 * * * ?")
  public void autoPushPromotionCloseMessage() {
    log.info("begin on push promotion closeMessage");
    List<Map> mapList = recordPromotionPgTranInfoService.selectAllByStatusPush();
    if (mapList == null) {
      return;
    }
    for (Map pieceMap:
         mapList) {
      String pCode = (String) pieceMap.get("pCode");
      String tranCode = (String) pieceMap.get("tranCode");
      String name = (String) pieceMap.get("name");
      String detailCode = (String) pieceMap.get("detailCode");
      PromotionPgDetail promotionPgDetail = recordPromotionPgDetail.selectByCode(detailCode, pCode);
      if (promotionPgDetail == null) {
        return;
      }
      List<PromotionPgMemberInfoVO> promotionPgMemberInfoVOS = promotionPgMemberInfoService.selectMemberInfo(tranCode);
      if (promotionPgMemberInfoVOS == null || promotionPgMemberInfoVOS.isEmpty()) {
        return;
      }
      int num = promotionPgDetail.getPieceGroupNum() - promotionPgMemberInfoVOS.size();
      if (num < 0) {
        num = 0;
      }
      List<Long> cpIdList = new ArrayList<>();
      for(PromotionPgMemberInfoVO promotionPgMemberInfoVO:
        promotionPgMemberInfoVOS) {
        String memberId = promotionPgMemberInfoVO.getMemberId();
        cpIdList.add(Long.valueOf(memberId));
      }
      Map<String, String> map = new HashMap<>();
      map.put("B", Optional.ofNullable(name).orElse("商品"));
      map.put("Z", num + "");
      ActionType ap = ActionType.ASSEMBLE_BUR_WHOLE_NOTICE;
      String content = MessageCastUtils.replaceContent(ap.getContent(), map);
      CastType.CUSTOMIZEDCAST.pushMassage(Platform.ALL, cpIdList, ap.getPriority(),"",
          ap.getAbbreviation(), content);
    }
  }

  private static String rvZeroAndDot(String s) {
    if (s.isEmpty()) {
      return null;
    }
    if (s.indexOf(".") > 0) {
      s = s.replaceAll("0+?$", "");//去掉多余的0
      s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
    }
    return s;
  }

}
