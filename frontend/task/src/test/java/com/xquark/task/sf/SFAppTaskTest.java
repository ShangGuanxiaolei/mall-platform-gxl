package com.xquark.task.sf;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.CachingConfig;
import com.xquark.config.DalConfig;
import com.xquark.config.ServiceCommonConfig;
import com.xquark.config.ServiceConfig;
import com.xquark.service.product.SFProductService;
import com.xquark.task.order.OrderJob;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午8:18
 */
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@TransactionConfiguration(defaultRollback = false)
@ContextConfiguration(classes = {
    DalConfig.class,
    ServiceConfig.class,
    ServiceCommonConfig.class,
    CachingConfig.class,
    ApplicationConfig.class})
public class SFAppTaskTest {

  private SFAppTask sfAppTask;

  private SFProductService sfProductService;

  @Autowired
  public void setSfAppTask(SFAppTask sfAppTask) {
    this.sfAppTask = sfAppTask;
  }

  @Autowired
  public void setSfProductService(SFProductService sfProductService) {
    this.sfProductService = sfProductService;
  }

  @Test
  public void pullProducts() {
    sfAppTask.pullProducts();
  }

  @Test
  public void pullSingleProduct() {
    sfAppTask.syncSingleProduct();
  }

  @Test
  public void renewToken() {
    sfAppTask.renewToken();
  }

  @Test
  public void pullProudctDetails() {
    sfProductService.pullProductDetails(null);
  }

  @Test
  public void pullRegion() {
    sfAppTask.pullRegion();
  }
  
  @Autowired
  private OrderJob orderJob;
  
  @Test
  public void orderCheck() {
	  orderJob.checkPending();
  }
}