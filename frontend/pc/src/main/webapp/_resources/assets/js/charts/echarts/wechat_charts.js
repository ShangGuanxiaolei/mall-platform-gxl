/* ------------------------------------------------------------------------------
 *
 *  # Echarts - lines and areas
 *
 *  Lines and areas chart configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function() {


    // Set paths
    // ------------------------------

    require.config({
        baseUrl: 'http://51shop.mobi/sellerpc/_resources/new_assets/',
        paths: {
            echarts: 'js/plugins/visualization/echarts'
        }
    });

    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var materials = ec.init(document.getElementById('materials'), limitless);
            var fans = ec.init(document.getElementById('fans'), limitless);

            // Charts setup
            // ------------------------------

            //
            // fans options
            //

            fans_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['新增粉丝', '取消关注', '净增粉丝']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '新增粉丝',
                        type: 'line',
                        stack: 'Total',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: '取消关注',
                        type: 'line',
                        stack: 'Total',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: '净增粉丝',
                        type: 'line',
                        stack: 'Total',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    }
                ]
            };


            //
            // materials options
            //
            materials_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['图文阅读统计图']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '图文阅读',
                        type: 'line',
                        stack: 'Total',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    }
                ]
            };



            // Apply options
            // ------------------------------

            materials.setOption(materials_options);
            fans.setOption(fans_options);

            // Resize charts
            // ------------------------------
            window.onresize = function () {
                setTimeout(function () {
                    fans.resize();
                    materials.resize();
                }, 200);
            }
        }
    );
});
