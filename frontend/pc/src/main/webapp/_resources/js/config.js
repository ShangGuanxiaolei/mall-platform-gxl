//var dev = location.href.indexOf('51shop.mobi') == -1 && location.href.indexOf('test.51shop.mobi') == -1;
////dev=0; // 设置js=0后,js必须打包才能加载
////检测IE版本
//var userAgentInfo = navigator.userAgent;
//var w = window,
//    ver = w.opera ? (opera.version().replace(/\d$/, "") - 0) : parseFloat((/(?:IE |fox\/|ome\/|ion\/)(\d+\.\d)/.exec(userAgentInfo) || [, 0])[1]);
//var ie = !!w.VBArray && Math.max(document.documentMode || 0, ver);

require.config({
  baseUrl: '/sellerpc/_resources/js/',
  paths: {
    /************* third part **********/
    base: 'dist',
    jquery: 'libraries/jquery.min',
    ramda: 'libraries/ramda.min',
    jqueryui: 'libraries/jquery_ui',
    jquerySerializeObject: 'libraries/jquery.serialize-object.min',
    pace: 'plugins/pace.min',
    bootstrap: 'libraries/bootstrap.min',
    bootstrapTable: 'plugins/bootstrap-table',
    bootstrapTableEditable: 'plugins/bootstrap-table-editable',
    formSelects: 'plugins/forms/selects',
    blockui: 'plugins/blockui.min',
    app: '/sellerpc/_resources/js/libraries/app.min',
    echartsNoAMD: 'plugins/visualization/echarts/echarts-all',
    echartsTheme: 'plugins/visualization/echarts/theme/limitless',
    validate: 'plugins/forms/validation/validate.min',
    uniform: 'plugins/forms/styling/uniform.min',
    md5: 'third/md5',
    cookies: 'third/jquery.cookie',
    placeholder: 'third/jquery.placeholder',
    sweetAlert: 'plugins/notifications/sweet_alert.min',
    mustache: 'plugins/template/mustache/mustache.min',
    switchery: 'plugins/forms/styling/switchery.min',
    switch: 'plugins/forms/styling/switch.min',
    formRadio: 'plugins/forms/page/form_checkboxes_radios.min',
    dropzone: 'plugins/uploaders/dropzone.min',
    fileinput: 'plugins/uploaders/fileinput.min',
    fileinput_zh: 'plugins/uploaders/locales/zh',
    'datatables.net': 'plugins/table/datatables.min',
    datatables: 'plugins/table/fixedColumns.min',
    select2: 'plugins/forms/selects/select2.min',
    bootbox: 'plugins/notifications/bootbox.min',
    steps: 'plugins/forms/wizards/steps.min',
    daterangepicker: 'plugins/pickers/daterangepicker',
    daterangepicker_v2: 'plugins/pickers/daterangepicker_bak',
    datepicker: 'plugins/pickers/datepicker',
    // moment: 'third/moment.min',
    moment: 'plugins/pickers/moment.min',
    tree: 'plugins/tree/jstree.min',
    xlsx:'plugins/xlsx/xlsx.full.min',
    d3: 'plugins/visualization/d3/d3.min',
    d3tooltip: 'plugins/visualization/d3/d3_tooltip',
    multiselect: 'plugins/forms/selects/jquery-multi-select',
    bootstrapSwitch: 'plugins/bootstrap-switch.min',
    spectrum: 'plugins/pickers/color/spectrum',
    'ckeditor-core': 'plugins/editor/ckeditor',
    'ckEditor': 'plugins/editor/adapters/jquery',
    tokenfield: 'plugins/forms/tags/tokenfield.min',
    duallistbox: 'plugins/forms/selects/duallistbox.min'
    /************* third part **********/
  },

  shim: {
    echartsNoAMD: {
      exports: 'echarts'
    },
    pace: {
      deps: ['jquery']
    },
    bootstrap: {
      deps: ['jquery']
    },
    bootstrapSwitch: {
      deps: ['jquery'],
      exports: 'bootstrapSwitch'
    },
    blockui: {
      exports: 'blockui'
    },
    app: {
      deps: ['jquery', 'bootstrap'],
      exports: 'app'
    },
    validate: {
      deps: ['jquery'],
      exports: 'validate'
    },
    uniform: {
      deps: ['jquery'],
      exports: 'validate'
    },
    md5: {
      deps: ['jquery'],
      exports: 'md5'
    },
    cookies: {
      deps: ['jquery'],
      exports: 'cookies'
    },
    placeholder: {
      deps: ['jquery'],
      exports: 'placeholder'
    },
    switchery: {
      deps: ['jquery', 'uniform'],
      exports: 'switchery'
    },
    switch: {
      deps: ['jquery', 'switchery'],
      exports: 'switch'
    },
    tokenfield: {
      deps: ['jquery'],
      exports: 'tokenfield'
    },
    formRadio: {
      deps: ['jquery', 'switch'],
      exports: 'formRadio'
    },
    jquerySerializeObject: {
      deps: ['jquery'],
      exports: 'jquerySerializeObject'
    },
    dropzone: {
      deps: ['jquery'],
      exports: 'dropzone'
    },
    fileinput: {
      deps: ['jquery'],
      exports: 'fileinput'
    },
    fileinput_zh: {
      deps: ['fileinput'],
    },
    datatables: {
      deps: ['jquery', 'datatables.net'],
      exports: 'datatables'
    },
    select2: {
      deps: ['jquery'],
      exports: 'select2'
    },
    bootbox: {
      deps: ['jquery'],
      exports: 'bootbox'
    },
    steps: {
      deps: ['jquery'],
      exports: 'steps'
    },
    tree: {
      deps: ['jquery'],
      exports: 'tree'
    },
    d3: {
      exports: 'd3'
    },
    xlsx: {
      exports: 'xlsx'
    },
    d3tooltip: {
      exports: 'd3tooltip'
    },
    multiselect: {
      deps: ['jquery'],
      exports: 'multiselect'
    },
    spectrum: {
      deps: ['jquery'],
      exports: 'spectrum'
    },
    ckEditor: {
      deps: ['jquery', 'ckeditor-core'],
      exports: 'ckEditor'
    },
    ramda: {
      exports: 'ramda'
    }
  },
  urlArgs: 'v=226'
});

require(['app', 'pace', 'blockui']);