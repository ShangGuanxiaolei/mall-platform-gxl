define(['jquery', 'utils', 'jquerySerializeObject', 'switch', 'uniform', 'validate', 'daterangepicker_v2'], function ($, utils) {

    let isEdit=false;
    const limitedOption = $("input[name=limited]");
    const batchNoOption = $("input[name=batch_no]");
    const amountOption = $("input[name=amount]");
    const validOption=$("input[name=valid_type]");
    const validTimeType = $("input[name=valid_type][value=valid_time]");
    const validDayType = $("input[name=valid_type][value=valid_day]");
    const discountOption = $("input[name=discount]");
    const validDay = $("input[name=valid_day]");
    const validFrom = $("input[name=valid_from]");
    const validTo = $("input[name=valid_to]");
    const modal = $(".add-coupon-modal");
    const validDayBlock = "valid_day";
    const validRangeBlock = "valid_time";
    const couponForm = $("#coupon");
    const couponSubmitButton = $("#submit-coupon");
    const datePicker = $(".daterange-basic");
    const singleDatePicker = $(".daterange-single");
    const STATUS_VALID = 'VALID';
    //-----------
    const urlPrefix = '/bos';
    const createUrl = `${urlPrefix}/couponManager/create`;
    const loadUrl = `${urlPrefix}/couponManager/load`;

    let functionModule = (function () {
        return {
            submitCoupon: function (coupon) {
                $.post(createUrl, coupon).done(date => {
                    utils.tools.success("创建成功!");
                }).fail(data => {
                    utils.tools.error("创建出错!");
                });
            },
            editCoupon:function(id){
                $.get(loadUrl,{couponId:id}).done(date=>{
                    isEdit=true;
                    let coupon=date.coupon;
                    //命名转换下
                    coupon.apply_above=coupon.applyAbove;
                    utils.tools.syncForm(couponForm,date.coupon);
                    let picker=singleDatePicker.data('daterangepicker');
                    picker.setStartDate(new Date(coupon.distributedAt));
                    picker.setEndDate(new Date(coupon.distributedAt));
                    picker.updateFormInputs();
                    picker=datePicker.data('daterangepicker');
                    picker.setStartDate(new Date(coupon.validFrom));
                    picker.setEndDate(new Date(coupon.validTo));
                    validTimeType.prop("checked",true);
                    $.uniform.update();
                    modal.modal("show");
                }).fail(data=>{
                    utils.tools.error("获取出错!");
                });
            },
            switchBlock:function(type){
                if (type === validDayBlock) {
                    $(`#${validDayBlock}`).css("display", 'block');
                    $(`#${validRangeBlock}`).css("display", 'none');
                } else {
                    $(`#${validDayBlock}`).css("display", 'none');
                    $(`#${validRangeBlock}`).css("display", 'block');
                }
            }
        }
    })();

    let eventInitialization = (function () {
        return {
            initSwitch: function () {
                $(".switch").bootstrapSwitch();
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });
                return this;
            },
            initDatePicker: function () {
                let dateOption = {
                    timePicker: false,
                    startDate: moment(),
                    endDate: moment().subtract(0, 'month').endOf('month'),
                    locale: {
                        format: 'YYYY/MM/DD',
                        separator: ' - ',
                        applyLabel: '确定',
                        startLabel: '开始日期:',
                        endLabel: '结束日期:',
                        cancelLabel: '清空',
                        weekLabel: 'W',
                        customRangeLabel: '日期范围',
                        daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                        monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                        firstDay: 6
                    },
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small btn-default',
                    isInvalidDate: function (date) {
                        return !date.isAfter(moment());
                    },
                };
                datePicker.daterangepicker(dateOption);
                dateOption.singleDatePicker = true;
                singleDatePicker.daterangepicker(dateOption);
                return this;
            },
            initFormSwitch: function () {
                $('.switch').on('switchChange.bootstrapSwitch', function (e, data) {
                    if (!data) {
                        amountOption.attr("readonly", "readonly");
                    } else {
                        amountOption.removeAttr("readonly");
                    }
                });
                return this;
            },
            initValidSwitch: function () {
                validOption.on("click", function () {
                    let type = $(this).attr("value");
                    functionModule.switchBlock(type);
                });
                return this;
            },
            resetForm: function () {
                modal.on('hidden.bs.modal', function (e) {
                    couponForm[0].reset();
                    couponForm.validate().resetForm();
                });
                return this;
            },
            getBatchNo: function () {
                modal.on('show.bs.modal', function (e) {
                    if(!isEdit){
                        $.get(loadUrl).done((data) => {
                            batchNoOption.prop("value", data.batchNo);
                        }).fail((data => {
                            utils.tools.error("获取批次号出错");
                        }));
                    }else{
                        isEdit=false;
                    }
                });
                return this;
            },
            initSubmit: function () {
                couponSubmitButton.on("click", function () {
                    let result = couponForm.valid();
                    if (result) {
                        console.log("success valid");
                        let coupon = couponForm.serializeObject();
                        if (validRangeBlock === coupon.valid_type) {
                            let times = coupon.valid_range.split("-");
                            coupon.valid_from = times[0].trim();
                            coupon.valid_to = times[1].trim();
                        }
                        coupon.total_discount = coupon.discount * coupon.amount;
                        coupon.statu = STATUS_VALID;
                        //兼容性数据项
                        coupon.shop_select = '';
                        coupon.category_selelct = '';
                        coupon.product_selelct = '';
                        functionModule.submitCoupon(coupon);
                    }
                });
                return this;
            },
            initValidate: function () {
                couponForm.validate({
                    errorClass: 'validation-error-label',
                    successClass: 'validation-valid-label',
                    highlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    validClass: "validation-valid-label",
                    success: function (label) {
                        label.addClass("validation-valid-label").text("");
                    },
                    //copy-from-limitless/form_validation.html
                    errorPlacement: function (error, element) {
                        // Styled checkboxes, radios, bootstrap switch
                        if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                            if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                                error.appendTo(element.parent().parent().parent().parent());
                            }
                            else {
                                error.appendTo(element.parent().parent().parent().parent().parent());
                            }
                        }
                        else if (element.hasClass('left')) {
                            error.insertAfter(element);
                        }
                        // Unstyled checkboxes, radios
                        else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                            error.appendTo(element.parent().parent().parent());
                        }

                        // Inline checkboxes, radios
                        else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent());
                        }

                        // Input group, styled file input
                        else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                            error.appendTo(element.parent().parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        name: "required",
                        batch_no:"required",
                        discount: {
                            required: true,
                            number: true,
                            max: 500,
                            min: 1,
                        },
                        apply_above: {
                            required: true,
                            number: true,
                            min: 1,
                            checkApplyAbove: true,
                        },
                        amount: {
                            required: {
                                depends: function () {
                                    return limitedOption.prop("checked");
                                }
                            },
                            number: true,
                            min: 1,
                        },
                        acquire_limit: {
                            required: true,
                            number: true,
                            min: 1,
                        },
                        valid_day: {
                            required: {
                                depends: function () {
                                    return validTimeType.prop("checked") === false;
                                },
                            },
                            number: true,
                            min: 1,
                        },
                        valid_range: {
                            required: {
                                depends: function () {
                                    return validTimeType.prop("checked") === true;
                                },
                            },
                        },
                        notify_before: {
                            required: true,
                            number: true,
                            min: 1
                        },
                        description: {
                            maxlength: 200,
                        },
                        distributed_at:{
                            required:true,
                        }

                    },
                    messages: {
                        batch_no:"必须填写批次号",
                        name: "请输入名称",
                        discount: {
                            required: "请输入面值",
                            number: "请输入整数",
                            min: "最低面值为1",
                            max: "最高面值为500"
                        },
                        apply_above: {
                            required: "请输入订单金额",
                            number: "请输入整数",
                            min: "金额不得低于1",
                        },
                        amount: {
                            required: "请输入发行量",
                            number: "请输入整数",
                            min: "数量不得低于1",
                        },
                        acquire_limit: {
                            required: "请输入领取限量",
                            number: "请输入整数",
                            min: "数量不得低于1",
                        },
                        valid_day: {
                            required: "请输入有效期",
                            number: true,
                            min: 1,
                        },
                        valid_from: {
                            required: "",
                            date: "请输入时间类型",
                        },
                        valid_to: {
                            required: "请输入失效时间",
                            date: "请输入时间类型",
                        },
                        notify_before: {
                            required: "请输入提醒时间",
                            number: "请输入整数",
                            min: "数量不得低于1"
                        },
                        description: {
                            maxlength: "描述不得大于200字",
                        },
                        distributed_at:{
                            required:"请输入发行日期",
                        }
                    },
                    submitHandler: function (form) {
                    },
                    invalidHandler: function (event, validator) {
                        let errors = validator.numberOfInvalids();
                        console.log(`error count:${errors}`);
                    }
                });
                $.validator.addMethod("checkApplyAbove", function (value, element) {
                    let discount = Number.parseInt(discountOption.prop("value"));
                    return this.optional(element) || Number.parseInt(value) > discount;
                }, "使用上限不能小于折扣");
                return this;
            }
        }

    })();


    return {
        init:function(){
            eventInitialization.initSwitch().initFormSwitch().initValidSwitch().initValidate().initSubmit().initDatePicker().resetForm().getBatchNo();
        },
        edit:functionModule.editCoupon,
    }

});
