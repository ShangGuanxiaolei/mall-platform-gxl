define(['jquery', 'utils', 'datePicker', 'productModalSingle', 'productSaleZoneModal', 'fileUploader',
      'validator',
      'jquerySerializeObject',
      'datatables', 'blockui', 'select2'],
    function ($, utils, datePicker, productModal, productSaleZoneModal, uploader, validator) {

      const prefix = window.host + '/flashSale';
      const listUrl = prefix + '/listPc';
      const saveUrl = prefix + '/saveProduct';
      const saveAreaUrl = prefix + '/saveAreaProduct';
      const closeUrl = prefix + '/close';
      const viewUrl = prefix + '/view';
      const checkInPromotionUrl = prefix + '/checkInPromotion';
      const saveSaleZoneTitleUrl = prefix + '/saveSaleZoneTitle?saleZoneTitle=';
      const findSaleZoneTitleUrl = prefix + '/findSaleZoneTitle?promotionType=';
      const findDateByBatchNoUrl = prefix + '/findDateByBatchNo?batchNo=';

      var $dataTable;

      var globalDatePicker;

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

      const manager = (function () {

        const $addFlashSaleBtn = $('.flash-sale-add');
        const $addAreaGoodsBtn = $('.area-goods-add');
        const $saveFlashSaleBtn = $('.saveFlashSaleBtn');
        const $saveAreaGoodsBtn = $('.saveAreaGoodsBtn');
        const $modalFlashSale = $('#modal_flash_sale');
        const $modalAreaGoods = $('#modal_area_goods');

        const $validFrom = $('#valid_from');
        const $validTo = $('#valid_to');
        const $areaValidFrom = $('#area_valid_from');
        const $areaValidTo = $('#area_valid_to');

        const $form = $('#flashSaleForm');
        const $areaFrom = $('#areaGoodsForm');

        const $img = $('#img');
        const $areaImg = $('#area_img');

        let imgUploader = uploader.createUploader({
          dom: '#product_image_dropzone',
          onSuccess: function (img) {
            $img.val(img);
          }
        });

        let imgUploader2 = uploader.createUploader({
          dom: '#area_product_image_dropzone',
          onSuccess: function (img) {
            $areaImg.val(img);
          }
        });

        const notNull = function (field) {
          return field && field !== '';
        };

        const betweenOneToTen = function (field) {
          return field >= 1 && field <= 10;
        };

        const notNegative = function (field) {
          return field && parseFloat(field) > 0;
        };

        const validationRules = {
          title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          discount: [
            {
              checker: notNull,
              message: '请输入抢购价格'
            },
            {
              checker: betweenOneToTen,
              message: '抢购折扣必须在1到10折之间'
            }
          ],
          amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ],
          limitAmount: [
            {
              checker: notNull,
              message: '请输入限购库存'
            },
            {
              checker: notNegative,
              message: '限购库存不能小于0'
            }
          ],
          area_title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          batchNo: [
            {
              checker: notNull,
              message: '请输入活动批次'
            },
            {
              checker: notNegative,
              message: '活动批次不能小于0'
            }
          ],
          area_img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          area_validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          area_validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          area_productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          price: [
            {
              checker: notNull,
              message: '请输入活动价格'
            },
            {
              checker: notNegative,
              message: '活动价格不能小于0'
            }
          ],
          point: [
            {
              checker: notNegative,
              message: '德分不能小于0'
            }
          ],
          area_amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ],
          area_limitAmount: [
            {
              checker: notNull,
              message: '请输入限购数量'
            },
            {
              checker: notNegative,
              message: '限购数量不能小于0'
            }
          ]
        };

        const globalInstance = {

          bindEvent: function () {

            /* 点击显示增加预购商品弹窗 */
            $addFlashSaleBtn.on('click', function () {
              utils.tools.syncForm($form);
              // FIXME reset没有生效, 先手动设置
              $('#promotionId').val('');
              $('#id').val('');
              $('#productId').val('');
              $('#img').val('');
              $('#valid_from').val('');
              $('#valid_to').val('');
              imgUploader.clear();
              $modalFlashSale.modal('show');
            });

            $addAreaGoodsBtn.on('click', function () {
              utils.tools.syncForm($areaFrom);
              // FIXME reset没有生效, 先手动设置
              $('#area_promotionId').val('');
              $('#area_id').val('');
              $('#area_productId').val('');
              $('#area_img').val('');
              $('#area_valid_from').val('');
              $('#area_valid_to').val('');
              $('#promotionBatch').val('');
              document.getElementById("usedPoint").style.display="none";//隐藏
              $("#promotionDate").removeAttr("disabled");
              $('#logisticsFee').val('');
              document.getElementById('price').style['width'] = '';
              imgUploader2.clear();
              $modalAreaGoods.modal('show');
            });

            /* 保存预购商品事件 */
            $saveFlashSaleBtn.on('click', function (e) {
              e.preventDefault();
              const params = $form.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(
                  params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              utils.postAjax(saveUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $('#modal_flash_sale').modal('hide');
                    $datatables.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });

            /* 保存专区商品事件 */
            $saveAreaGoodsBtn.on('click', function (e) {
              e.preventDefault();
              const params = $areaFrom.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              let logisticsFee = $('#logisticsFee').val();
              if(logisticsFee != null && logisticsFee < 0){
                utils.tools.alert("活动运费不能小于0",
                    {timer: 1200, type: 'warning'});
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(
                  params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              utils.postAjax(saveAreaUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $('#modal_area_goods').modal('hide');
                    $datatables.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });

            return this;
          },
          makeDatePicker: function () {
            /* 初始话日期控件 */
            globalDatePicker = datePicker.createPicker({
              dom: '.daterange-time',
              onApply: function (start, end) {
                $validFrom.val(start);
                $validTo.val(end);
                $areaValidFrom.val(start);
                $areaValidTo.val(end);
              },
              onEmpty: function () {
                $validFrom.val('');
                $validTo.val('');
                $areaValidFrom.val('');
                $areaValidTo.val('');
              }
            });
            return this;
          },
          makeProductModal: function () {
            /**
             * 初始化商品选择表格
             */
            productModal.create({
              dom: '#productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'),
                    checkInPromotionUrl, {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#productId").val(id);
                            $("#productName").val(name);
                            $("#area_productId").val(id);
                            $("#area_productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }
            });

            productSaleZoneModal.create({
              dom: '#area_productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'),
                    checkInPromotionUrl, {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#area_productId").val(id);
                            $("#area_productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }});
          }
        };

        return {
          initGlobal: function () {
            globalInstance.bindEvent()
            .makeDatePicker()
            .makeProductModal();
            return this;
          },
          initTable: function () {
            if (!$dataTable) {

              $datatables = utils.createDataTable('#xquark_flash_sale_list', {

                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: true,
                ajax: function (data, callback) {
                  $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    batchNo: $('input[name="saleZoneBatchNo"]').val(),
                    promotionType: $('select[name="type"]').val(),
                    promotionStatus: $('select[name="status"]').val(),
                    pageable: true
                  }, function (res) {
                    if (!res.data) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    }
                    callback({
                      recordsTotal: res.data.total,
                      recordsFiltered: res.data.total,
                      data: res.data.list,
                      iTotalRecords: res.data.total,
                      iTotalDisplayRecords: res.data.total
                    });
                  })
                },
                rowId: 'id',
                columns: [
                  {
                    title: '批次号',
                    data: "promotion.batchNo",
                    width: "40px",
                    orderable: false,
                    name: "promotionBatchNo"
                  },
                  {
                    title: '活动图片',
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                      return '<img class="goods-image" src="'
                          + row.promotion.img + '" />';
                    }
                  },
                  {
                    title: '活动名称',
                    data: "promotion.title",
                    width: "80px",
                    orderable: false,
                    name: "promotionName"
                  },
                  {
                    title: '抢购商品名',
                    data: "product.name",
                    width: "80px",
                    orderable: false,
                    name: "productName"
                  },
                  {
                    title: '活动类型',
                    width: "80px",
                    orderable: false,
                    name: "promotionType",
                    render: function (data, type, row) {
                          var promotionType = '';
                          switch(row.promotion.type)
                          {
                              case 'SALEZONE':
                                  promotionType = '专区活动';
                                  break;
                              case 'FLASHSALE':
                                  promotionType = '抢购活动';
                                  break;
                              default:
                                  break;
                          }
                      return promotionType;
                    }
                  },
                  {
                    title: '状态',
                    orderable: false,
                    width: "80px",
                    render: function (data, type, row) {
                      let isClosed = row.promotion.closed;
                      let validTo = row.promotion.validTo;
                      let validFrom = row.promotion.validFrom;
                      let now = new Date().getTime();
                      if(validFrom > now){
                        return '未开始';
                      } else if(validTo < now || isClosed){
                        return '已结束';
                      }
                      return '进行中';
                    }
                  },
                  {
                    title: '开始日期',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validFrom);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validFrom"
                  },
                  {
                    title: '结束时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validTo);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validTo"
                  },
                  {
                    title: '发布时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.createdAt);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                  },
                  {
                    title: '管理',
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                      var html = '';
                      html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-pencil7" ></i>编辑</a>';
                      html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-trash"></i>结束</a>';
                      return html;
                    }
                  }
                ],
                select: {
                  style: 'multi'
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                  $('.del').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认结束该活动吗', function () {
                      utils.postAjax(closeUrl, {id: id}, function (res) {
                        if (res === -1) {
                          utils.tools.alert("网络问题，请稍后再试",
                              {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                          if (res.errorCode === 200) {
                            utils.tools.alert('操作成功',
                                {timer: 1200, type: 'success'});
                            $datatables.search('').draw();
                          } else {
                            if (res.moreInfo) {
                              utils.tools.alert(res.moreInfo,
                                  {timer: 1200, type: 'warning'});
                            } else {
                              utils.tools.alert('服务器错误',
                                  {timer: 1200, type: 'warning'});
                            }
                          }
                        }
                      });
                    }, function () {
                    });
                  });

                  $('.edit').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.postAjax(viewUrl + '/' + id, null, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if(res.data.promotionType == "SALEZONE"){
                            $('#area_product_image_dropzone').val('');
                            const data = res.data;
                            const validFrom = new Date(
                                parseInt(data.validFrom)).format(
                                'yyyy-MM-dd hh:mm');
                            const validTo = new Date(
                                parseInt(data.validTo)).format(
                                'yyyy-MM-dd hh:mm');
                            const img = data.product.img;
                            imgUploader2.clear()
                                .addImage(img);
                            globalDatePicker.resetStr(validFrom + ' - '
                                + validTo);

                            if (data.promotion.isPointUsed) {
                              $("input[name=isPointUsedForArea][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isPointUsedForArea][value=false]").prop("checked", true);
                            }
                            if (data.promotion.isCountEarning) {
                              $("input[name=isCalculateEarnings][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isCalculateEarnings][value=false]").prop("checked", true);
                            }
                            if (data.promotion.isShowTime) {
                              $("input[name=isShowTime][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isShowTime][value=false]").prop("checked", true);
                            }
                            $.uniform.update();

                            let formData = {
                              area_id: id,
                              area_productId: data.product.id,
                              area_promotionId: data.promotion.id,
                              area_title: data.promotion.title,
                              area_productName: data.product.name,
                              area_amount: data.amount,
                              area_limitAmount: data.limitAmount,
                              area_validFrom: validFrom,
                              area_validTo: validTo,
                              area_img: data.promotion.img,
                              price: data.promotion.price,
                              promotion_price: data.promotion.conversionPrice,
                              deduction_point: data.promotion.point,
                              batchNo: data.promotion.batchNo,
                              logisticsFee: data.promotion.logisticsFee
                            };
                            if(data.promotion.logisticsFee <= 0){
                              $('#logisticsFee').val('0');
                            }
                            utils.tools.syncForm($areaFrom, formData);
                            if(data.promotion.isPointUsed){
                              $('#usedPoint').show();
                            }else{
                              $('#usedPoint').hide();
                            }
                            $modalAreaGoods.modal('show');
                          }else{
                            $('#product_image_dropzone').val('');
                            const data = res.data;
                            const validFrom = new Date(
                                parseInt(data.validFrom)).format(
                                'yyyy-MM-dd hh:mm');
                            const validTo = new Date(
                                parseInt(data.validTo)).format(
                                'yyyy-MM-dd hh:mm');
                            const img = data.product.img;
                            imgUploader.clear()
                                .addImage(img);
                            globalDatePicker.resetStr(validFrom + ' - '
                                + validTo);

                            if (data.promotion.isPointUsed) {
                              $("input[name=isPointUsed][value=true]").prop("checked",
                                  true);
                            } else {
                              $("input[name=isPointUsed][value=false]").prop("checked",
                                  true);
                            }
                            $.uniform.update();

                            var formData = {
                              id: id,
                              productId: data.product.id,
                              promotionId: data.promotion.id,
                              title: data.promotion.title,
                              productName: data.product.name,
                              discount: data.discount,
                              amount: data.amount,
                              limitAmount: data.limitAmount,
                              validFrom: validFrom,
                              validTo: validTo,
                              img: data.promotion.img
                            };
                            utils.tools.syncForm($form, formData);
                            $modalFlashSale.modal('show');
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
                  })
                }

              });
            }
            return this;
          }
        }
      })();

      $(":radio[name='isPointUsedForArea']").click(function(){
        let index = $(":radio[name='isPointUsedForArea']").index($(this));
        if(index === 1) { //选中第1个时，div显示
          document.getElementById('price').style['width'] = '50%';
          $('#usedPoint').show();
        }
        else { //当被选中的不是第2个时，div隐藏
          $('#usedPoint').hide();
          document.getElementById('price').style['width'] = '';
        }
      });

      let domName = "";
      $("#area_productName").click(function () {
        domName = "#area_productName";
      });
      $("#productName").click(function () {
        domName = "#productName";
      });

      $('.btn-saveAreaTitle').on('click', function () {
        $.ajax({
          type: "POST",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: findSaleZoneTitleUrl + "SALEZONE",
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                $('#saleZoneTitle').val(data.data.promotionTitle);
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
        $('#modal_area_title').modal('show');
      });

      $("#saveSaleZoneTitleBtn").on('click', function () {
        let saleZoneTitle = $('#saleZoneTitle').val();
        $.ajax({
          type: "POST",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: saveSaleZoneTitleUrl + saleZoneTitle,
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                $('#modal_area_title').modal('hide');
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
      });

      /**
       * 获取系统时间（年月日）
       * @returns {number}
       */
      function getNowFormatDate() {
        let date = new Date();
        let month = date.getMonth() + 1;
        let strDate = date.getDate();
        if (month >= 1 && month <= 9) {
          month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
          strDate = "0" + strDate;
        }
        return date.getFullYear() + month + strDate;
      }

      $(".btn-search").off("click").on('click', function () {
        $datatables.search('').draw();
      });

      /**
       * 光标脱离事件
       */
      $("#batchNo").blur(function(){
        let batchNo = $('#batchNo').val();
        $.ajax({
          type: "GET",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: findDateByBatchNoUrl + batchNo,
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                let from = dateFormat(data.data.promotion.validFrom);
                $('#area_valid_from').val(from);
                let to = dateFormat(data.data.promotion.validTo);
                $('#area_valid_to').val(to);
                $('#promotionDate').val(from + " - " + to);
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
      });

      /**
       * 日期转换封装
       * @param date
       * @returns {*}
       */
      function dateFormat(date){
        let cDate = parseInt(date);
        let d = new Date(cDate);
        return d.format('yyyy-MM-dd hh:mm');
      }

      /* 初始化页面 */
      manager.initGlobal()
      .initTable();

    });
