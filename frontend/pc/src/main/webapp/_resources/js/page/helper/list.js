define(['jquery', 'utils', 'dataTable',
  'jquerySerializeObject', 'blockui',
  'select2'], function ($, utils, dataTable) {

  const prefix = window.host + '/helper';
  const listUrl = prefix + '/list';
  const editUrl = 'edit';
  const deleteUrl = prefix + '/delete';

  var helperTable;
  const manager = (function () {

    const $addBtn = $('.add-helper');

    const globalInstance = {
      bindEvent: function () {
        $addBtn.on('click', function () {
          location.href = editUrl;
        });
        return this;
      }
    };

    return {
      initGlobal: function () {
        globalInstance.bindEvent();
        return this;
      },
      initTable: function () {
        if (!helperTable) {
          helperTable = dataTable.newTable({
            url: listUrl,
            dom: '#xquark_helper_list',
            columns: [
              {
                title: '图标',
                width: "40px",
                render: function (data, type, row) {
                  return '<img class="goods-image" src="'
                      + row.icon + '" />';
                }
              },
              {
                title: '标题',
                data: "title",
                width: "50px",
                name: "title"
              },
              {
                title: '简介',
                data: "intro",
                width: "100px",
                name: "intro"
              },
              {
                title: '排序号',
                data: "sort",
                width: "20px",
                name: "sort"
              },
              {
                title: '创建时间',
                width: "120px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "createdAt"
              },
              {
                title: '管理',
                sClass: "right",
                width: "120px",
                render: function (data, type, row) {
                  var html = '';
                  html += '<a href="javascript:void(0);" class="edit" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
                  html += '<a href="javascript:void(0);" style="margin-left: 10px" class="remove" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>删除</a>';
                  return html;
                }
              }
            ],
            drawBack: function () {
              $('.edit').on('click', function () {
                var id = $(this).attr('rowId');
                location.href = editUrl + '?id=' + id;
              });
              $('.delete').on('click', function () {
                var id = $(this).attr('rowId');
                utils.postAjax(deleteUrl, {id: id}, function (res) {
                  if (res === -1) {
                    utils.tools.alert("网络问题，请稍后再试",
                        {timer: 1200, type: 'warning'});
                  }
                  if (typeof res === 'object') {
                    if (res.errorCode === 200) {
                      utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                    } else {
                      if (res.moreInfo) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                      } else {
                        utils.tools.alert('服务器错误',
                            {timer: 1200, type: 'warning'});
                      }
                    }
                  }
                });
              });
            }
          });
        }
      }
    }
  })();

  manager.initGlobal()
  .initTable();

});
