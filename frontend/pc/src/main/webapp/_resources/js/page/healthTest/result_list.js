define(['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';

    var listProducts = window.host + "/product/list";
    const listUrl = prefix + '/module/results';
    const bindProductUrl = prefix + '/result/addProduct';
    const unbindProductUrl = prefix + '/result/removeProduct';
    const deleteUrl = prefix + '/result/delete';
    const listProductsUrl = prefix + '/result/listProduct';

    const $dataTable = $('#xquark_result_tables');
    const $productModal = $('#modal_result_products');
    const $choosenProductModal = $('#modal_products');

    var moduleId = getParameterByName('id');
    var shopId = null;
    var resultId = null;
    var category = '';
    var order = '';

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "暂无相关数据",
            emptyTable: "暂无相关数据"
        }
    });

    const manager = (function () {
        const $addResult = $('#add_result');
        const $addProduct = $('.btn-addProduct');
        const $chooseModalClose = $('.close_product_choose');
        const globalInstance = {
            bindEvent: function () {
                $addResult.on('click', function () {
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId;
                });
                $addProduct.on('click', function () {
                    $productModal.modal('show');
                    $choosenProductModal.modal('hide');
                });
                $chooseModalClose.on('click', function () {
                    $productModal.modal('hide');
                    $choosenProductModal.modal('show');
                });
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.bindEvent();
            },
            initTable: function () {
                $('body').unbind('click');

                $('.edit_result').on('click', function () {
                    var id = $(this).attr('rowId');
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId + '&resultId=' + id;
                });
                $('.edit_product').on('click', function () {
                    resultId = $(this).attr('rowId');
                    $productDataTables.search('').draw();
                    // $productModal.modal('show');
                    $('#modal_products').modal('show');
                });

                /** 点击删除部门弹出框 **/
                $("[data-toggle='popover']").popover({
                    trigger: 'manual',
                    placement: 'left',
                    html: 'true',
                    animation: true,
                    content: function () {
                        var rowId = $(this).attr("rowId");
                        return '<span>确认删除？</span>' +
                            '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" rowId="' + rowId + '">确认</button>' +
                            '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                    }
                });

                $('[data-toggle="popover"]').popover() //弹窗
                    .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                        $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                    }).on('shown.bs.popover', function () {
                    var that = this;
                    $('.popover-btn-ok').on("click", function () {
                        var id = $(this).attr("rowId");
                        deleteResult(id);
                    });
                    $('.popover-btn-cancel').on("click", function () {
                        $(that).popover("hide");
                    });
                });

                $('body').on('click', function (event) {
                    var target = $(event.target);
                    if (!target.hasClass('popover') //弹窗内部点击不关闭
                        && target.parent('.popover-content').length === 0
                        && target.parent('.popover-title').length === 0
                        && target.parent('.popover').length === 0
                        && target.data("toggle") !== "popover") {
                        //弹窗触发列不关闭，否则显示后隐藏
                        $('[data-toggle="popover"]').popover('hide');
                    } else if (target.data("toggle") === "popover") {
                        target.popover("toggle");
                    }
                });
            },
            initSelectProductEvent: function () {
                $(".selectproduct").on("click", function () {
                    var productId = $(this).attr('rowId');
                    utils.postAjax(bindProductUrl, {resultId: resultId, productId: productId}, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                $productDataTables.search('').draw();
                                utils.tools.alert('添加成功', {timer: 1200, type: 'success'});
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
            }
        }
    })();

    manager.initGlobal();

    /** 初始化表格数据 **/
    const $resutlDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                moduleId: moduleId
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo);
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    return row.moduleName;
                }
            },
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    var physiqueId = row.physiqueId;
                    return physiqueId ? '体质测试' : '基础测试';
                }
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    return row.start + ' - ' + row.end;
                }
            },
            {
                width: '50',
                orderable: false,
                render: function (data, type, row) {
                    var description = row.description;
                    var maxLength = 20;
                    if (description) {
                        if (description.length > 20) {
                            description = description.substring(0, maxLength);
                        }
                    } else {
                        return '无';
                    }
                    return description;
                }
            },
            {
                width: '100',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_result" rowId="' + row.id + '" fid="edit_result"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del_result" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_result"><i class="icon-trash"></i>删除</a>';
                    html += '<a href="javascript:void(0);" class="edit_product" style="margin-left: 10px;"  rowId="' + row.id + '" fid="edit_product"><i class="icon-pencil7"></i>推荐商品</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    var $productDataTables = $('#xquark_list_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(listProductsUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                resultId: resultId
            }, function(res) {
                if (!res.data && !res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                title: '商品',
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                title: '状态',
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '价格',
                data: "price",
                width: "50px",
                orderable: true,
                name:"price"
            }, {
                title: '库存',
                data: "amount",
                orderable: true,
                width: "50px",
                name:"amount"
            },
            {
                title: '销量',
                data: "sales",
                orderable: true,
                width: "50px",
                name:"sales"
            },{
                title: '发布时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                title: '管理',
                orderable: false,
                width: '50px',
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="del" rowId="' + row.id + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            $('.del').on('click', function () {
                var productId = $(this).attr('rowId');
                var data = {
                    productId: productId,
                    resultId: resultId
                };
                utils.tools.confirm('确认删除吗', function () {
                    utils.postAjax(unbindProductUrl, data, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                utils.tools.alert('删除成功', { timer: 1200, type: 'success' });
                                $productDataTables.search('').draw();
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                }, function () {

                });
            });
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listProducts, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if (order !== '') {
                        return order;
                    } else {
                        var _index = data.order[0].column;
                        if (_index < 4) {
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else {
                    if (res.data.shopId) {
                        shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.categoryTotal,
                    iTotalDisplayRecords: res.data.categoryTotal
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="' + row.id + '" productImg="' + row.imgUrl + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            manager.initSelectProductEvent();
        }
    });

    function deleteResult(id) {
        utils.postAjaxWithBlock($(document), deleteUrl, {id: id}, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        utils.tools.alert('操作成功', { timer: 1200, type: 'success' });
                        $resutlDataTables.search('').draw();
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res === 0) {

            } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            listProducts = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            listProducts = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});
