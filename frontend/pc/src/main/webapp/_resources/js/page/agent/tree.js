define(['jquery','utils','datatables','blockui','select2','tree', 'fileinput_zh', 'fileinput'], function($,utils,datatabels,blockui,select2,tree,fileinput_zh,fileinput) {
    var _data;
    var _id;
    var _icon;
    var batch;
    var rootId = '';

    var twitterTreeManager = {/**jstree*/
    $container : $('#twitterTreeManagerContainer'),
        // currentNode: null,
        root: {},
        keys: '',
        eachPageSize: 15,
        createIcon : function (child) {
            var role = child['role'].toLowerCase();
            return '<div class="custom">' +
                '<img src="' + child['avatar'] + '" alt="" class="user-icon">' +
                '<em class="' + role + '">' +child['name'] + '</em>' +
                '&nbsp;&nbsp;<em style="color: red">' +child['count'] + '</em>' +
                '</div>'
        },
        config : {
            "core" : {
                "animation" : 200,
                "check_callback" : true,
                "themes" : { "stripes" : false, "dots" : false, "responsive" : true, "icons": false},
                'strings' : {
                    'Loading ...' : '正在加载...'
                },
                'data' : {
                    'url' : function(node) {
                        return window.host + '/userAgent/getDirectChildren' + "?" + Math.random();
                    },
                    'type' : 'GET',
                    'data' : function(node) {
                        //TODO 设置Root节点
                        console.log(node)
                        console.log(node.id)
                        console.log($.jstree.root)
                        var id = node.id === $.jstree.root ? '' : node.id;
                        node.page = node.page ? node.page : 0;
                        return {
                            userId: id,
                            page: node.page,
                            size: twitterTreeManager.eachPageSize,
                            keys: twitterTreeManager.keys
                        }
                    },
                    'dataFilter' : function(data) {
                        var dataInfo = JSON.parse(data).data;
                        console.log(JSON.parse(data))
                        if(JSON.parse(data).data) {
                            var total = JSON.parse(data).data.total,
                                parent =  JSON.parse(data).data.parent,
                                list = JSON.parse(data).data.list,
                                id = JSON.parse(data).data.id;
                            if(list.length ==0){
                                alert("没有找到相关代理数据");
                            }
                        } else {
                            alert('数据格式有误');
                        }
                        console.log(total);
                        console.log(parent);
                        console.log(list);
                        function renderChildData() {
                            var tmp = [];
                            list.forEach(function(value) {
                                tmp.push({
                                    'id' : value.id ,
                                    'text' : twitterTreeManager.createIcon(value), // 用户姓名
                                    'state' : { 'opened' : false, 'selected' : false },
                                    'children' : !value.isLeaf
                                })
                            });
                            if(total > 15) {
                                var node = twitterTreeManager.tree.get_node(parent.id);
                                var maxPage = parseInt(total/15);
                                node.maxPage = maxPage;
                                if(node && node.page) {
                                    var page =node.page +1;
                                    node.maxPage = maxPage;
                                }else {
                                    page = 1;
                                    twitterTreeManager.root.maxPage = maxPage;
                                }
                                console.log(twitterTreeManager.tree.get_node(parent.id))
                                tmp.push({
                                    'id' : parent.id + "pagingBtn",
                                    'text' : '<div class="flip-page">' +
                                    '<i class="previousPage">上页</i>' +
                                    '<em class="nextPage">下页</em>' +
                                    '<span>第' + (node.page+1) +
                                    '页</span>' +
                                    '</div>',
                                    'state' : { 'opened' : false, 'selected' : false },
                                    'children' : false
                                })
                            }
                            return tmp
                        }
                        if( parent && parent.id ) {
                            if( parent.id === twitterTreeManager.root.id && !twitterTreeManager.tree.get_node(parent.id)) {
                                //根节点
                                console.log('根节点')
                                return JSON.stringify([{
                                    'id' : parent.id ,
                                    'text' : twitterTreeManager.createIcon(parent), // 用户姓名
                                    'state' : { 'opened' : false, 'selected' : false },
                                    'children' : !!(list.length)
                                }])
                            } else {
                                if(list.length != 0 ) {
                                    console.log('子节点')
                                    return JSON.stringify( renderChildData() )
                                }
                            }
                        }else{
                            // 搜索结果展示后，清空keys，这样点下级展示时可以调用之前分页的逻辑
                            twitterTreeManager.keys = '';
                            showDetail(id);
                            return JSON.stringify(list);
                        }
                    }
                }
            },
            "types" : {
                "#" : {
                    "max_children" : 1,
                    "max_depth" : 10,
                }
            },
            "plugins" : [
                "contextmenu", "search",
                "state", "types", "sort", "Wholerow", "json_data"
            ],
            "contextmenu": {
                "items": {
                    "ccp": null,
                    "search" : {
                        "separator_before"	: false,
                        "separator_after"	: false,
                        "_disabled"			: false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
                        "label"				: "搜索",
                        /*!
                         "shortcut"			: 113,
                         "shortcut_label"	: 'F2',
                         "icon"				: "glyphicon glyphicon-leaf",
                         */
                        "action"			: function (data) {
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            var text = obj.text;
                            var name = $(text).find("em")[0].innerHTML;
                            $("#keys").val(name);
                            $(".btn-search").trigger("click");
                        }
                    }
                }
            }
        },

        init : function (userId, keys) {
            this.root.id = userId;
            this.keys = keys;
            this.root.page = 0;
            this.$container
            //初始化时绑定需要处理的事件
                .on('click.jstree', function (e) {
                    var className = e.target.getAttribute('class');
                    var tree = twitterTreeManager.tree;
                    if(className && className.match('previousPage')) {
                        var parentNode = tree.get_node(tree.get_parent(e.target));
                        parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                        if(parentNode.page > 0) {
                            parentNode.page = parentNode.page -1;
                            tree.load_node(parentNode)
                        }
                    } else if(className && className.match('nextPage')) {
                        var parentNode = tree.get_node(tree.get_parent(e.target));
                        parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                        if(parentNode.page < parentNode.maxPage ) {
                            parentNode.page = parentNode.page + 1;
                            tree.load_node(parentNode);
                            console.log(parentNode);
                        }
                    }

                    // 点击左侧缩放小箭头时不需要查看详情
                    var eventNodeName = e.target.nodeName;
                    if(eventNodeName == 'IMG' || eventNodeName == 'EM' ){
                        var id = $(e.target).parents('li').attr('id');
                        if(id.indexOf('pagingBtn') == -1){
                            showDetail(id);
                        }
                    }

                })
                //执行初始化
                .jstree(this.config);
            this.tree = this.$container.jstree(true);
        },

        clear: function() {
            this.$container.jstree('destroy');
            delete twitterTreeManager.tree;
            // delete twitterTreeManager
        }
    };

    $(".btn-search").click(function(){
        var keys = $("#keys").val();
        if(keys != ''){
            utils.postAjax(window.host + '/userAgent/searchByPhoneOrName', {keys: keys}, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        twitterTreeManager.clear();
                        twitterTreeManager.init('', keys);//初始化树
                    } else {
                        utils.tools.alert("没有找到相关代理数据!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }else{
            twitterTreeManager.clear();
            twitterTreeManager.init(rootId);//初始化树
        }
    });

    $(".btn-clear").click(function(){
        $("#keys").val('');
        twitterTreeManager.clear();
        twitterTreeManager.init(rootId);//初始化树
    });

    buttonRoleCheck('.hideClass');

    utils.postAjaxWithBlock($(document), window.host + '/userAgent/getRootUserId', [] , function(res) {
        if (typeof(res) === 'object') {
            switch (res.errorCode) {
                case 200:
                {
                    rootId = res.data;
                    twitterTreeManager.init(res.data);//初始化树
                    break;
                }
                default:
                {
                    utils.tools.alert(res.moreInfo, {timer: 1200});
                    break;
                }
            }
        } else if (res == 0) {
        } else if (res == -1) {
            utils.tools.alert("查看树状图失败", {timer: 1200});
        }
    });



    function showDetail(id) {
        if(id && id != ''){
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/userId/' + id, [] , function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            $("#name").html(res.data.name);
                            $("#weixin").html(res.data.weixin);
                            $("#idcard").html(res.data.idcard == '' ? '无' : res.data.idcard);
                            $("#certNum").html(res.data.certNumView);
                            $("#startDate").html(res.data.startDate);
                            $("#endDate").html(res.data.endDate);
                            $("#agentTypeName").html(res.data.agentTypeName);
                            $("#kfname").html(res.data.name);
                            $("#kfphone").html('19911111111');

                            $("#small_name").html(res.data.name);
                            $("#small_weixin").html(res.data.weixin);
                            $("#small_idcard").html(res.data.idcard == '' ? '无' : res.data.idcard);
                            $("#small_certNum").html(res.data.certNumView);
                            $("#small_startDate").html(res.data.startDate);
                            $("#small_endDate").html(res.data.endDate);
                            $("#small_agentTypeName").html(res.data.agentTypeName);
                            $("#small_kfname").html(res.data.name);
                            $("#small_kfphone").html('19911111111');

                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("查看详情失败", {timer: 1200});
                }
            });
        }
    }

    $(".small_bg").mouseover(function(){
        $(".small_bg").hide();
        $(".bg").show();
    });

    $(".bg").mouseleave(function(){
        $(".small_bg").show();
        $(".bg").hide();
    });

});