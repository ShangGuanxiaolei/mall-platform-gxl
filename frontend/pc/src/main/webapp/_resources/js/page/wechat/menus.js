define(['jquery','jqueryui/effects.min', 'jqueryui/interactions.min', 'jqueryui/widgets.min', 'formSelects/bootstrap_select.min', 'mustache', 'utils', 'form/validate'],
    function($, effects, interactions, widgets, bootstrap_select, mustache, utils, validate) {

        var accordion = {

            rootButtonTpl: $("#accordionItemTemplate").html(),
            subButtonGroupTpl: $("#subAccordionTemplate").html(),
            subButtonTpl: $("#subAccordionItemTemplate").html(),
            $accordionRootDiv: $("#jui-accordion-sortable"),

            init: function () {
                //模板预编译
                mustache.parse(this.rootButtonTpl);
                mustache.parse(this.subButtonGroupTpl);
                mustache.parse(this.subButtonTpl);

                //创建菜单列表
                this.createAccordion(accordion.$accordionRootDiv);
                $(".jui-sub-accordion-sortable").each(function () {
                    accordion.createAccordion($(this));
                });

                //联动表单内容
                accordion.bindActivate(accordion.$accordionRootDiv);
                $(".jui-sub-accordion-sortable").each( function () {
                    accordion.bindActivate(this)}
                );

                //绑定弹出菜单按钮
                accordion.$accordionRootDiv.on("click", ".addNewButton", function () {
                    if ($("#jui-accordion-sortable > .accordion-sortable-group").length >= 3) {
                        utils.tools.alert("超过一级菜单数量限制,最多添加三个一级菜单!");
                        return;
                    }

                    if (accordion.validateSaved() == false) {
                        return;
                    }

                    var lastButton = accordion.$accordionRootDiv.children().last();
                    var nextIndex = parseInt(lastButton.attr("button-index")) + 1;
                    $(lastButton).after(mustache.render(accordion.rootButtonTpl, {
                        buttonId: "button-" + nextIndex,
                        buttonContentId: "content-" + nextIndex,
                        saved: false,
                        buttonIndex: nextIndex,
                        parentId: '', //新增主菜单项无parentId
                        itemId: '', //主键为空
                        buttonType: "CLICK_RETURN_TEXT"
                    }));
                    accordion.$accordionRootDiv.accordion("refresh");
                    accordion.$accordionRootDiv.accordion("option", "active", nextIndex);
                    accordion.$accordionRootDiv.sortable("disable");
                });

                accordion.$accordionRootDiv.on("click",".addNewSubButton", function (event) {

                    var $subAccordion = $("#sub-accordion-of-" + menusForm.itemId.val());

                    if ($subAccordion.length != 0 && $subAccordion.children().length >= 5) {
                        utils.tools.alert("超过二级菜单数量限制,最多添加五个二级菜单!");
                        return;
                    }

                    var activeAccordionHeader = $(accordion.$accordionRootDiv.find("> div > span.ui-accordion-header-active")[0]);
                    if (activeAccordionHeader.attr("button-type") != "HAS_SUB_BUTTON") {
                        utils.tools.alert("当前菜单按钮类型不允许添加二级菜单,请选择当前菜单按钮类型为:包含二级菜单.");
                        return;
                    }

                    if (accordion.validateSaved() == false) {
                        return;
                    }

                    var lastButton = $subAccordion.length == 0 ? undefined : $subAccordion.children().last();
                    var nextIndex = $subAccordion.length == 0 ? 0 : parseInt(lastButton.attr("button-index")) + 1;

                    var renderParams = {
                        buttonId: "sub-button-" + nextIndex,
                        buttonContentId: "sub-content-" + nextIndex,
                        saved: false,
                        buttonIndex: nextIndex,
                        parentId: accordion.$accordionRootDiv.find("> div > span.ui-accordion-header-active")[0].getAttribute("item-id"),
                        itemId: '', //主键为空
                        buttonType: "CLICK_RETURN_TEXT"
                    };

                    if (lastButton != undefined) {
                        $(lastButton).after(mustache.render(accordion.subButtonTpl, renderParams));
                    } else {
                        $("#content-" + accordion.$accordionRootDiv.accordion("option", "active")).append(mustache.render(accordion.subButtonGroupTpl, renderParams));
                    }

                    accordion.$accordionRootDiv.accordion("refresh");

                    if (lastButton == undefined) {
                        $subAccordion = $("#sub-accordion-of-" + menusForm.itemId.val());
                        accordion.createAccordion($subAccordion);
                        accordion.bindActivate($subAccordion);
                    } else {
                        $subAccordion.accordion("refresh");
                    }
                    $subAccordion.accordion("option", "active", nextIndex);
                    $subAccordion.sortable("disable");

                    $subAccordion.find(">div > span.ui-accordion-header-active").trigger("click");
                    event.stopPropagation();
                });

                accordion.$accordionRootDiv.on("click",".removeCurrentButton", function () {
                    //TODO
                });

                //初始化form和无menu的情况
                if (accordion.$accordionRootDiv.find(".accordion-sortable-group").length == 0) {
                    accordion.$accordionRootDiv.append(mustache.render(accordion.rootButtonTpl, {
                        buttonId: "button-0",
                        buttonContentId: "content-0",
                        saved: false,
                        buttonIndex: 0,
                        parentId: '', //新增主菜单项无parentId
                        itemId: '', //主键为空
                        buttonType: "CLICK_RETURN_TEXT"
                    }));
                    accordion.$accordionRootDiv.accordion("refresh");
                    accordion.$accordionRootDiv.accordion("option", "active", 0);
                    accordion.$accordionRootDiv.sortable("disable");
                }
                accordion.$accordionRootDiv.find(">div > span.ui-accordion-header-active").trigger("click");
            },

            createAccordion: function (sortableAccordion) {
                var sortable = {
                    axis: "y",
                    handle: "span",
                    stop: function(event, ui) {
                        ui.item.children("span").triggerHandler("focusout");
                        $(this).accordion("refresh");

                        var ids = [];
                        var oldIndexes = [];
                        var newIndexes = [];
                        var $currentAccordionDiv = $(ui.item.parent());

                        $currentAccordionDiv.children().each(function() {
                            var header = $(this).children("span");
                            ids.push(header.attr("item-id"));
                            oldIndexes.push(header.attr("button-index"));
                        });

                        for (var i = 0; i < ids.length; i++) {
                            newIndexes.push("" + i);
                        }

                        var is_same = (oldIndexes.length == newIndexes.length) && oldIndexes.every(function(element, index) {
                                return element === newIndexes[index];
                            });
                        if (is_same) {
                            return;
                        }

                        var data = {
                            ids: ids,
                            newIndexes: newIndexes
                        };

                        utils.postAjaxWithBlock($(document), window.originalHost + "/wechat/modifyMenusOrder", data, function(res) {
                            if (typeof(res) === 'object') {
                                switch (res.errorCode) {
                                    case 200:
                                    {
                                        utils.tools.alert("菜单顺序调整完成", {timer: 1200, type: 'success'});
                                        $currentAccordionDiv.children().each(function(index) {
                                            var header = $(this).children("span");
                                            var content = $(this).children(".ui-accordion-content");
                                            $(this).attr("button-index", index);
                                            header.attr("button-index", index);
                                            header.attr("id", "button-" + index);
                                            header.attr("aria-controls", "content-" + index);
                                            content.attr("aria-controls", "button-" + index);
                                            content.attr("aria-labelledby", "button-" + index);
                                            content.attr("id", "content-" + index);
                                        });
                                        break;
                                    }
                                    default:
                                    {
                                        utils.tools.alert(res.moreInfo, {timer: 1200});
                                        break;
                                    }
                                }
                            } else if (res == 0) {

                            } else if (res == -1) {
                                utils.tools.alert("网络问题,请刷新页面重试", {timer: 1200});
                            }
                        });
                    }
                };

                sortableAccordion.accordion({
                    header:"> div > span",
                    active:0,
                    heightStyle: "auto",

                }).sortable(sortable);
            },

            validateSaved: function () {
                var allSaved = true;

                $(".accordion-sortable-group").each(function(index, button) {
                    if (this.getAttribute("saved") == "false") {
                        allSaved = false;
                        accordion.$accordionRootDiv.accordion("option", "active", parseInt(button.getAttribute("button-index")));
                        accordion.$accordionRootDiv.accordion("refresh");
                        return false;
                    }
                });

                $(".sub-accordion-sortable-group").each(function(index, subButton) {
                    if (this.getAttribute("saved") == "false") {
                        allSaved = false;
                        var subAccordion = $(subButton).parent();
                        var itemId = subAccordion.find(".ui-accordion-header-active")[0].getAttribute("parent-id");
                        var parentButtonIndex = accordion.$accordionRootDiv.find("[item-id=" + itemId + "]")[0].getAttribute("button-index");

                        accordion.$accordionRootDiv.accordion("option", "active", parseInt(parentButtonIndex));
                        accordion.$accordionRootDiv.accordion("refresh");
                        subAccordion.accordion("option", "active", parseInt(subButton.getAttribute("button-index")));
                        subAccordion.accordion("refresh");

                        return false;
                    }
                });

                if (!allSaved) {
                    utils.tools.alert("请先将新添加的菜单项保存");
                }

                return allSaved;
            },

            bindActivate: function (accordionRoot) {
                $(accordionRoot).on("accordionactivate", function( event, ui ) {
                    var newHeader = ui.newHeader;
                    var subUnsaved = newHeader.parent().find('.sub-accordion-sortable-group[saved="false"]');
                    if (subUnsaved.length != 0) {
                        newHeader = subUnsaved.children().first();
                    }

                    menusForm.form.trigger("reset");
                    menusForm.itemId.val(newHeader.attr('item-id'));
                    menusForm.index.val(newHeader.attr('button-index'));
                    menusForm.parentId.val(newHeader.attr('parent-id'));
                    menusForm.buttonName.val(newHeader.attr('button-name'));
                    menusForm.buttonType.val(newHeader.attr('button-type')).change();
                    menusForm.buttonType.selectpicker("refresh");
                    if (newHeader.attr('button-type') == 'CLICK_RETURN_TEXT') {
                        menusForm.buttonPushMsgContent.val(newHeader.attr('button-push-msg-content'));
                    } else if (newHeader.attr('button-type') == 'CLICK_RETURN_MEDIA_ID') {
                        menusForm.buttonPushMsgContent.val(newHeader.attr('button-push-msg-id'));
                    }
                    menusForm.redirectUrl.val(newHeader.attr('redirect-url'));
                });

                $(accordionRoot).on("click", ".ui-accordion-header", function( event ) {
                    menusForm.form.trigger("reset");
                    menusForm.itemId.val(this.getAttribute('item-id'));
                    menusForm.index.val(this.getAttribute('button-index'));
                    menusForm.parentId.val(this.getAttribute('parent-id'));
                    menusForm.buttonName.val(this.getAttribute('button-name'));
                    menusForm.buttonType.val(this.getAttribute('button-type')).change();
                    menusForm.buttonType.selectpicker("refresh");

                    if (this.getAttribute('button-type') == 'CLICK_RETURN_TEXT') {
                        menusForm.buttonPushMsgContent.val(this.getAttribute('button-push-msg-content'));
                    } else if (this.getAttribute('button-type') == 'CLICK_RETURN_MEDIA_ID') {
                        menusForm.buttonPushMsgContent.val(this.getAttribute('button-push-msg-id'));
                    }
                    menusForm.redirectUrl.val(this.getAttribute('redirect-url'));
                });
            }
        };

        // ------------------------------
        // 菜单按钮信息编辑
        // ------------------------------

        var menusForm = {
            form: $('.menus-form'),
            buttonName: $(".menus-form input[name=buttonName]"),
            buttonType: $(".menus-form select[name=buttonType]"),
            buttonPushMsgContent: $(".menus-form input[name=buttonPushMsgContent]"),
            redirectUrl: $(".menus-form input[name=redirectUrl]"),
            itemId: $(".menus-form input[name=id]"),
            parentId: $(".menus-form input[name=parentId]"),
            index: $(".menus-form input[name=index]"),
            saveMenu: $('.saveMenu'),
            url: window.originalHost + '/wechat/saveCustomizedMenuItem',
            rules: {
                buttonName: {
                    required: true,
                    minlength: 1,
                    maxlength: 7,
                },
                buttonType: {
                    required: true,
                },
                buttonPushMsgContent: {
                    required: true,
                    minlength: 1,
                    maxlength: 50,
                },
                redirectUrl: {
                    required: true,
                    url: true,
                    minlength: 1,
                    maxlength: 500,
                }
            },
            messages: {
                buttonName: {
                    required: '请输入菜单项名称',
                    minlength: '请至少输入一个字符',
                    maxlength:'菜单项名称最多七个字符',
                },
                buttonType: {
                    required: '请选择菜单项类型',
                },
                buttonPushMsgContent: {
                    required: '请输入菜单项名称',
                    minlength: '请至少输入一个字符',
                    maxlength: '内容不能超过50个字符',
                },
                redirectUrl: {
                    required: '请输入菜单项名称',
                    url: '链接格式不正确',
                    minlength: '请至少输入一个字符',
                    maxlength: '内容不能超过500个字符',
                }
            },


            bind2Accordion: function (res) {


                var accordionHeader;

                if (menusForm.itemId.val() == "" && res.data != "") {
                    menusForm.itemId.val(res.data);
                }

                if (menusForm.parentId.val() == "") {
                    accordionHeader = $(accordion.$accordionRootDiv.find("> div > span.ui-accordion-header-active")[0]);
                } else {
                    accordionHeader = $($("#sub-accordion-of-" + menusForm.parentId.val()).find("> div > span.ui-accordion-header-active")[0]);
                }

                accordionHeader.attr('item-id', menusForm.itemId.val());
                accordionHeader.attr('button-index', menusForm.index.val());
                accordionHeader.attr('parent-id', menusForm.parentId.val());
                accordionHeader.attr('button-name', menusForm.buttonName.val());
                accordionHeader.attr('button-type', menusForm.buttonType.val());
                accordionHeader.attr('button-push-msg-content', menusForm.buttonPushMsgContent.val());
                accordionHeader.attr('button-push-msg-id', menusForm.buttonPushMsgContent.val());
                accordionHeader.attr('redirect-url', menusForm.redirectUrl.val());
                accordionHeader.parent().attr("saved", "true");

                accordionHeader.contents().each(function () {
                    if (this.nodeType === 3 && $.trim(this.nodeValue) != "") {
                        this.data = menusForm.buttonName.val();
                    }
                });

                if (menusForm.parentId.val() === "") {
                    accordion.$accordionRootDiv.sortable("enable");
                } else {
                    $("#sub-accordion-of-" + menusForm.parentId.val()).sortable("enable");
                }
            },

            save: function() {

                var data = {
                    name: this.buttonName.val(),
                    type: this.buttonType.val(),
                    contentOrMediaId: this.buttonPushMsgContent.val(),
                    url: this.redirectUrl.val(),
                    id: this.itemId.val(),
                    parentId: this.parentId.val(),
                    index: this.index.val()
                };

                utils.postAjaxWithBlock($(document), this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("菜单项保存成功", {timer: 1200, type: 'success'});
                                menusForm.bind2Accordion(res);
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            },

            init: function() {
                $(this.saveMenu).on('click',function() {
                    $(this.form).submit();
                });
                var validateConfig = {
                    messages: this.messages,
                    rules: this.rules,
                    submitCallBack: function (form) {
                        menusForm.save();
                    }
                };
                validate(this.form, validateConfig);

                $.fn.selectpicker.defaults = {
                    iconBase: '',
                    tickIcon: 'icon-checkmark3'
                }

                $('#buttonTypeSelector').selectpicker();

                $('#buttonTypeSelector').on('change', function() {
                    var optionSelected = $(this).find('option:selected').val();
                    if (optionSelected == 'CLICK_RETURN_TEXT') {
                        $($(menusForm.buttonPushMsgContent).parents(".form-group")[0]).show();
                        $($(menusForm.redirectUrl).parents(".form-group")[0]).hide();
                    } else if (optionSelected == 'CLICK_RETURN_MEDIA_ID') {
                        $($(menusForm.buttonPushMsgContent).parents(".form-group")[0]).show();
                        $($(menusForm.redirectUrl).parents(".form-group")[0]).hide();
                    } else if (optionSelected == 'CLICK_REDIRECT_VIEW') {
                        $($(menusForm.buttonPushMsgContent).parents(".form-group")[0]).hide();
                        $($(menusForm.redirectUrl).parents(".form-group")[0]).show();
                    } else if (optionSelected == 'HAS_SUB_BUTTON') {
                        if (menusForm.parentId.val() != null && menusForm.parentId.val() != "") {
                            utils.tools.alert("二级菜单项类型不能为:包含二级菜单.");
                            //var $subAccordion = $("#sub-accordion-of-" + menusForm.itemId.val());
                            return false;
                        }
                        $($(menusForm.buttonPushMsgContent).parents(".form-group")[0]).hide();
                        $($(menusForm.redirectUrl).parents(".form-group")[0]).hide();
                    }
                })
            }
        };

        //初始化
        menusForm.init();
        accordion.init();

    });