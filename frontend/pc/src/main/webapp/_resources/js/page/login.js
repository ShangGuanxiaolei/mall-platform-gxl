//base requirements
require(['all']);

//module
require(['jquery','uniform','placeholder','login/signin','login/forgot'],function(signin,forgot){

        $('input').placeholder();
        $('#password').val('');

        $('.forgot-pwd').on('click', function() {
            $('.login-part').hide();
            $('.forgot-part').fadeIn();
            $('#captcha').val('');
            $('#newPassword').val('');
            $('#rePassword').val('');
        });


        $('.forgot-part .tip-doing').on('click', function() {
            $('.login-part').fadeIn();
            $('.forgot-part').hide();
            $('#password').val('');
        });

});

