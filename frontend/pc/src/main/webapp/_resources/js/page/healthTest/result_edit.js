define(['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'ckEditor'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';
    const physiqueListUrl = prefix + '/physique/list';
    const saveUrl = prefix + '/module/saveResult';

    // 体质测试类型DIV
    const $physiqueDiv = $('#physiqueDiv');
    const $physiqueSelector = $('#physiqueSelector');

    const $editor = $('#editor');
    const $editorWrapper = $('.editorWrapper');

    // 体质测试缓存
    var physiqueList = null;
    var hasInit = false;

    const formProfile = {
        debug: true,
        rules: {
            type: 'required',
            start: 'required',
            end: 'required',
            description: 'required'
        },
        messages: {
            type: {
                required: '请选择类型'
            },
            start: {
                required: '请输入开始范围'
            },
            end: {
                required: '请输入结束范围'
            },
            description: {
                required: '请输入体质描述'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            console.log(form);
            manager.submitForm(form);
        }
    };

    const manager = (function () {

        const $questionTypeSelect = $('#questionType');
        const $returnBtn = $('.btn-return');
        const $form = $('.result-form');

        const typeHandler = {
            BASIC: function () {
                $physiqueSelector.removeAttr('name');
                $editor.attr('name', 'description');
                $editorWrapper.show();
                $physiqueDiv.hide();
            },
            PHYSIQUE: function () {
                if (physiqueList) {
                    showPhysique();
                } else {
                    utils.postAjaxWithBlock($(document), physiqueListUrl, null, function (res) {
                        if (typeof(res) === 'object') {
                            switch (res.errorCode) {
                                case 200: {
                                    if (!res.data) {
                                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                        return;
                                    }
                                    if (!res.data.list) {
                                        utils.tools.alert('体质类型未配置，请先配置体质类型', {timer: 1200, type: 'warning'});
                                        return;
                                    }
                                    physiqueList = res.data.list;
                                    showPhysique();
                                    break;
                                }
                                default: {
                                    utils.tools.alert(res.moreInfo, {timer: 1200});
                                    break;
                                }
                            }
                        } else if (res === 0) {

                        } else if (res === -1) {
                            utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                        }
                    });
                }
            }
        };
        const globalInstance = {
            bindEvent: function () {
                $questionTypeSelect.change(function () {
                    var type = $(this).val();
                    // 如果选择了体质测试则显示体质测试类型
                    // 选择了基本类型则隐藏体质测试
                    typeHandler[type]();
                });
                $returnBtn.on('click', function () {
                    var moduleId = $('input[name=moduleId]').val();
                    var url = '/sellerpc/healthTest/module/result';
                    if (moduleId && moduleId !== '') {
                        url += '?id=' + moduleId;
                    }
                    location.href = url;
                });
                return this;
            },
            initEditor: function () {
                $editor.ckeditor();
                return this;
            },
            initValidate: function () {
                validate($form, formProfile);
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.bindEvent()
                    .initEditor()
                    .initValidate();
            },
            initTable: function () {

            },
            submitForm: function (form) {
                var data = $(form).serializeObject();
                utils.getJson(saveUrl, data, function (res) {
                    if (res.errorCode === 200) {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            // 修改问题后跳转
                            // 添加问题后重置
                            $form[0].reset();
                        } else {
                            utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    }
                });
            }
        }
    })();

    manager.initGlobal();

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * 显示体质类型
     */
    function showPhysique() {
        $physiqueDiv.show();
        $editor.removeAttr('name');
        $editorWrapper.hide();
        $physiqueSelector.attr('name', 'physiqueId');
        if (physiqueList && !hasInit) {
            physiqueList.forEach(function (item) {
                var value = item.id;
                var text = item.name;
                var option = '<option value="' + value + '">' + text + '</option>';
                $physiqueSelector.append($(option));
            });
            hasInit = true;
        }
    }

});
