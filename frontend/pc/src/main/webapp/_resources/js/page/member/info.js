define(['jquery', 'utils', 'echartsNoAMD', 'echartsTheme'], function ($, utils, echarts, theme) {

    // Initialize charts
    // ------------------------------
    var member = echarts.init(document.getElementById('member'), theme);
    var card = echarts.init(document.getElementById('card'), theme);

    const block = {
        fullBlock: function () {
            return $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        },
        unBlock: function () {
            return $.unblockUI();
        }
    };

    const chartOptions = {
        pieOptions: {
            // Add title
            title: {
                text: '会员等级分布图',
                x: 'center'
            },

            // Add tooltip
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },

            // Add legend
            legend: {
                orient: 'vertical',
                x: 'left',
                data: []
            },

            // Display toolbox
            toolbox: {
                show: true,
                orient: 'vertical',
                feature: {
                    restore: {
                        show: true,
                        title: '刷新'
                    },
                    saveAsImage: {
                        show: true,
                        title: '保存图片到本地',
                        lang: ['保存']
                    }
                }
            },

            // Enable drag recalculate
            calculable: true,

            // Add series
            series: [{
                name: '会员等级',
                type: 'pie',
                radius: '50%',
                center: ['50%', '57.5%'],
                data: []
            }]
        },
        memberOptions: {
            // Setup grid
            grid: {
                x: 40,
                x2: 20,
                y: 35,
                y2: 25
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },

            // Add legend
            legend: {
                data: ['新增用户', '新增成交用户']
            },

            // Enable drag recalculate
            calculable: true,

            // Hirozontal axis
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                data: []
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                {
                    name: '新增用户',
                    type: 'line',
                    stack: 'Total',
                    data: []
                },
                {
                    name: '新增成交用户',
                    type: 'line',
                    stack: 'Total',
                    data: []
                }
            ]
        }
    };

    const ajax = function ajax() {
        var instance = {
            /**
             * 会员等级统计图
             */
            initLevel: function () {
                utils.postAjax('/sellerpc/member/card/listWithMemberCounts', null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误，请稍候再试', {timer: 2000, type: 'warning'});
                        return;
                    }
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                                if (res.data) {
                                    var list = res.data.list;
                                    var data = [];
                                    if (list && list.length > 0) {
                                        list.filter(function (item) {
                                            return item.memberCount > 0;
                                        }).forEach(function (item) {
                                            var name = item.name;
                                            var counts = item.memberCount;
                                            var obj = {value: counts, name: name};
                                            data.push(obj);
                                        })
                                    }
                                    chartOptions.pieOptions.legend.data = data
                                        .map(function (item) {
                                            return item.name;
                                        });
                                    chartOptions.pieOptions.series[0].data = data;
                                    // 初始化会员卡等级统计
                                    card.setOption(chartOptions.pieOptions);
                                }
                                break;
                            default:
                                console.log('member');
                                utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                        }
                    }
                });
                return this;
            },
            initNewRegister: function () {
                utils.postAjax('/sellerpc/member/chart', null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误，请稍候再试', {timer: 2000, type: 'warning'});
                        return;
                    }
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                                if (res.data) {
                                    var data = res.data;
                                    if (data.member) {
                                        data.member.forEach(function (item) {
                                            chartOptions.memberOptions.xAxis[0].data.push(item.date);
                                            chartOptions.memberOptions.series[0].data.push(item.total);
                                        })
                                    }
                                    if (data.dealMember) {
                                        data.dealMember.forEach(function (item) {
                                            chartOptions.memberOptions.series[1].data.push(item.total);
                                        })
                                    }
                                    member.setOption(chartOptions.memberOptions);
                                }
                                break;
                            default:
                                console.log('chart');
                                utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                        }
                    }
                });
                return this;
            },
            initCountInfo: function () {
                block.fullBlock();
                utils.postAjax('/sellerpc/member/count', null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误，请稍候再试', {timer: 2000, type: 'warning'});
                        return;
                    }
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                                if (res.data) {
                                    var data = res.data;
                                    $('#members').text(data.members.newRegister);
                                    $('#deal_members').text(data.dealMembers.newRegister);
                                }
                                break;
                            default:
                                console.log('count');
                                utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                        }
                    }
                    block.unBlock();
                });
                return this;
            }
        };
        return {
            init: function () {
                instance.initLevel()
                    .initNewRegister()
                    .initCountInfo();
            }
        }
    };


    // 初始化统计数据
    ajax().init();

});
