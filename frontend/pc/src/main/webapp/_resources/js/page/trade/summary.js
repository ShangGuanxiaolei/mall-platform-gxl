define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/trade/getSummary';
    var data = {
    };
    // 本周交易额
    var dates;
    var values;
    var amount_area
    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
            // 取概况中的数据
            $("#day_in").html(result.data.info.totalIn);
            $("#day_out").html(result.data.info.totalOut);
            $("#day_wait").html(result.data.info.totalWaiting);

            // 取本周交易额
            dates = result.data.nums.dateList;
            values = result.data.nums.valueList;
            amount_area = echarts.init(document.getElementById('amount_area'), limitless);
            var amount_area_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['交易额'],
                    textStyle: {
                        fontSize: 16
                    }
                },

// Add toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    x: 'right',
                    y: 'center',
                    itemGap: 15,
                    padding: 0,
                    showTitle: false,
                    feature: {
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: '查看图表数据',
                            lang: ['查看图表数据', '关闭', '更新']
                        },
                        restore: {
                            show: true,
                            title: '刷新'
                        },
                        saveAsImage: {
                            show: true,
                            title: '另存为图片',
                            lang: ['保存']
                        }
                    }
                },
                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: dates
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '交易额',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                            show: true,
                            textStyle: {
                                fontSize: 16
                            }
                        }}},
                        data: values
                    }
                ]
            };
            amount_area.setOption(amount_area_options);

        }
    });



    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            amount_area.resize();
        }, 200);
    }

});
