/**
 * Created by chh on 16/10/18.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/viewPage/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    var $pageid = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增自定义页面
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#title").val('');
        $("#description").val('');
        $("#pageType").val('');
        $("#layout").val('');
        $("#backgroundImg").val('');
        $("#customizedUrl").val('');
        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
        $("#modal_component").modal("show");
    });

    // 选择页面组件
    $("#vcName").on('focus', function() {
        $selectcomponentdatatables.search('').draw();
        $("#modal_select_component").modal("show");
    });

    // 新增页面组件
    $(".btn-addcomponent").on('click', function() {
        $("#pageId").val($pageid);
        $("#pageComponentId").val('');
        $("#componentId").val('');
        $("#hidx").val('');
        $("#componentParams").val('');
        $("#vcName").val('');
        $("#modal_component_detail").modal("show");
    });

    // 保存页面组件
    $(".saveComponentBtn").on('click', function() {
        var url = window.host + '/viewPage/saveComponent';
        var componentId = $("#componentId").val();
        var hidx = $("#hidx").val();
        var componentParams = $("#componentParams").val();
        var pageId = $("#pageId").val();
        var id = $("#pageComponentId").val();
        if(!hidx || hidx == '' ){
            utils.tools.alert("请输入x轴!", {timer: 1200, type: 'warning'});
            return;
        }else if(!componentId || componentId == ''){
            utils.tools.alert("请选择页面组件!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            pageId: pageId,
            componentId: componentId,
            hidx: hidx,
            componentParams: componentParams
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        if(res.data){
                            alert("操作成功");
                            $("#modal_component_detail").modal("hide");
                            $componentdatatables.search('').draw();
                        }
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'+row.id+'"></label>';
                }
            },
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.pageType)
                    {
                        case 'TWEET':
                            status = '发现';
                            break;
                        case 'GROUPON':
                            status = '拼团';
                            break;
                        case 'FLASHSALE':
                            status = '特卖';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                data: "layout",
                width: "120px",
                orderable: false,
                name:"layout"
            }, {
                data: "backgroundImg",
                width: "120px",
                orderable: false,
                name:"backgroundImg"
            }, {
                data: "customizedUrl",
                width: "120px",
                orderable: false,
                name:"customizedUrl"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" title="'+row.title+'" description="'+row.description+'" pageType="'+row.pageType+'" layout="'+row.layout+'" backgroundImg="'+row.backgroundImg+'" customizedUrl="'+row.customizedUrl+'" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="components" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7"></i>页面组件管理</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $componentdatatables = $('#xquark_list_components_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/viewPage/component/list/" + $pageid, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "vcName",
                width: "120px",
                orderable: false,
                name:"vcName"
            }, {
                data: "hidx",
                width: "50px",
                orderable: false,
                name:"hidx"
            }, {
                data: "componentParams",
                width: "50px",
                orderable: false,
                name:"componentParams"
            },{
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="componentedit" style="margin-left: 10px;" vcName="'+row.vcName+'" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="componentdel" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initComponentEvent();
        }
    });

    var $selectcomponentdatatables = $('#xquark_select_component_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/viewComponent/list", {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                keyword: ''
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "vcName",
                width: "120px",
                orderable: false,
                name:"vcName"
            },{
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectcomponent" style="margin-left: 10px;" rowId="'+row.id+'" vcName="'+row.vcName+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectComponentEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        // 页面组件管理
        $(".components").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $pageid = rowId;
            $componentdatatables.search('').draw();
            $("#modal_components").modal("show");
        });

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           var title =  $(this).attr("title");
           var description =  $(this).attr("description");
           var pageType =  $(this).attr("pageType");
           var layout =  $(this).attr("layout");
           var backgroundImg =  $(this).attr("backgroundImg");
           var customizedUrl =  $(this).attr("customizedUrl");

           $("#id").val(id);
           $("#title").val(title);
           $("#description").val(description);
           $("#pageType").val(pageType);
           $("#layout").val(layout);
           $("#backgroundImg").val(backgroundImg);
           $("#customizedUrl").val(customizedUrl);

            /** 初始化选择框控件 **/
            $('.select').select2({
                minimumResultsForSearch: Infinity,
            });

           $("#modal_component").modal("show");
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }

    function initComponentEvent(){
        $(".componentedit").on("click",function(){
            var id =  $(this).attr("rowId");
            var vcName = $(this).attr("vcName");
            $.ajax({
                url: window.host + '/viewPageComponent/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var component = data.data;
                        $("#pageComponentId").val(component.id);
                        $("#componentId").val(component.componentId);
                        $("#hidx").val(component.hidx);
                        $("#componentParams").val(component.componentParams);
                        $("#vcName").val(vcName);
                        $("#modal_component_detail").modal("show");

                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        $(".componentdel").on("click",function(){
            var id =  $(this).attr("rowId");
            var r=confirm("确定删除此页面组件?");
            if (r==true)
            {
                deletePageComponent(id);
            }
        });

    }

    function initSelectComponentEvent(){
        $(".selectcomponent").on("click",function(){
            var componentId =  $(this).attr("rowId");
            var vcName =  $(this).attr("vcName");
            $("#componentId").val(componentId);
            $("#vcName").val(vcName);
            $("#modal_select_component").modal("hide");
        });
    }

    /**将页面组件从页面中删除掉 */
    function  deletePageComponent(pId){
        var url = window.host + "/viewPage/deletePageComponent/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $componentdatatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*将组件手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/viewPage/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存组件
    $(".saveBtn").on('click', function() {
        var url = window.host + '/viewPage/save';
        var id = $("#id").val();
        var title = $("#title").val();
        var description = $("#description").val();
        var pageType = $("#pageType").val();
        var layout = $("#layout").val();
        var backgroundImg = $("#backgroundImg").val();
        var customizedUrl = $("#customizedUrl").val();

        if(!title || title == '' ){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }else if(!layout || layout == ''){
            utils.tools.alert("请输入页面布局!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            title: title,
            description: description,
            pageType: pageType,
            layout: layout,
            backgroundImg: backgroundImg,
            customizedUrl: customizedUrl
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/view/viewpage';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});