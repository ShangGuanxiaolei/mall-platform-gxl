define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            ordering: false,
            sortable: false,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/bonus/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    name:$("#name").val(),
                    phone:$("#phone").val(),
                    year:$("#year").val(),
                    type:'YEAR',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId: 'id',
            columns: [{
                width: "15px",
                title: '<label class="checkbox"><input name="checkAll" type="checkbox" class="styled"></label>',
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkCommissions" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.id+'"></label>';
                    return str;
                }
            },{
                width:"80px",
                title: '分红年度',
                data: 'year',
                name: 'year'
            },{
                width:"80px",
                title: '返利人',
                data: 'userName',
                name: 'userName'
            },{
                width:"80px",
                title: '返利人手机号',
                data: 'userPhone',
                name: 'userPhone'
            },{
                width:"80px",
                title: '分红等级',
                data: 'level',
                name: 'level'

            },{
                width:"80px",
                title: '分红比例',
                data: 'rate',
                name: 'rate'
            },{
                width:"80px",
                title: '分红总金额',
                data: 'amount',
                name: 'amount'

            },{
                width:"80px",
                title: '分红金额',
                data: 'fee',
                name: 'fee'

            },{
                width:"80px",
                title: '发放状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if (value == 'NEW') {
                        return "未发放";
                    } else if (value == 'SUCCESS') {
                        return "已发放";
                    }
                }
            },{
                width: "100px",
                title: '发放分红',
                render: function (data, type, row) {
                    var text = (row.status == 'SUCCESS' ? '取消发放' : '确认发放');
                    var status = (row.status == 'SUCCESS' ? 'NEW' : 'SUCCESS');
                    var html = '';
                    html += '<a href="javascript:void(0);" rowId="'
                        + row.id
                        + '" fid="change_status" class="change_status" status="' + status + '"><i class="icon-coin-yen" >' +
                        '</i>' + text + '</a>';
                    return html;
                }
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

           // Handle click on "Select all" control
        $('input[name=checkAll]').on('click', function(){
      // Check/uncheck all checkboxes in the table
            var rows = $datatables.rows({ 'search': 'applied' }).nodes();
            $('input[name=checkCommissions]', rows).prop('checked', this.checked);
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            $(".btn-search").on('click', function () {
                $datatables.search('').draw();
            });

            $('.change_status').on('click', function () {
                var status = $(this).attr('status');
                var ids = $(this).attr('rowId');
                var data = {
                    ids: ids,
                    status: status
                };
                updateOffered(data);
            });

            tableRoleCheck('#guiderUserTable');

        }

        function updateOffered(data, callback) {
            var url = window.host + '/bonus/updateStatus';
            utils.postAjax(url, data, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data === true) {
                        // page参数在刷新时记住当前页码，否则会回到首页
                        $datatables.search('').draw('page');
                        if (callback) {
                            callback();
                        }
                        utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                    } else {
                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                    }
                }
            })
        }

        $(".exportBtn").on('click', function() {
            $('#exportForm').submit();
        });

        $('.offeredBtn').on('click', function () {
            var idsArr = [];
            $("[name=checkCommissions]:checkbox").each(function(){ //遍历table里的全部checkbox
                //如果被选中
                if($(this).prop("checked")){
                    idsArr.push($(this).val());
                }
            });
            if (idsArr.length === 0) {
                utils.tools.alert('请先勾选要操作的数据', {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                ids: idsArr.join(','),
                status: 'SUCCESS'
            };
            updateOffered(data, function () {
                // 成功后将checkAll全选框取消
                $('input[name=checkAll]').attr('checked', false);
            });
        });

        buttonRoleCheck('.hideClass');

});