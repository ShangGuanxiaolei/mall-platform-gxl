define(['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2',
      'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput',
      'jquerySerializeObject'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform,
        daterangepicker, moment, fileinput_zh, fileinput) {

      let updateStatusUrl = (id) => `${window.host}/order/refund/status/${id}`;
      let updateWmsStatusUrl = (id) => `${window.host}/order/refund/warehouse/status/${id}`;
      let wmsForm = $("#wms-check-form");
      let wmsCheckModal = $("#modal_wms_check");

      //判断字符是否为空的方法
      function isEmpty(obj) {
        if (typeof obj == "undefined" || obj == null || obj == "") {
          return true;
        } else {
          return false;
        }
      }

      //获取供应商信息并建立缓存
      let sourceMap = new Map();
      $.ajax({
            url: `${window.host}/supplier/list?type=override&pageable=false`,
            async: false,
          }
      ).done(
          data => {
            let results = [];
            data.data.list.forEach(e => {
              sourceMap.set(e.id, e.name);
              let supplier = {
                id: e.id,
                text: e.name,
              };
              results.push(supplier);
              //初始化select2
              $("#supplierType").select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            });
          }
      ).fail(
          data => {}
      );

      // 获取后台登陆用户的角色，会根据角色区分哪些按钮能显示
      var userRoles = $("#userRoles").val();
      /** 初始化日期控件 **/
      var options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'year').startOf('year'),
        endDate: moment().subtract(0, 'year').endOf('year'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var refund_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var return_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var change_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var $dateRangeBasic = $('.daterange-basic');
      $dateRangeBasic.daterangepicker(options, function (start, end) {
        if (start._isValid && end._isValid) {
          $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format(
              'YYYY-MM-DD'));
        } else {
          $dateRangeBasic.val('');
        }
      });
      $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
      $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

      var $refundDateRangeBasic = $('.refund_daterange-basic');
      $refundDateRangeBasic.daterangepicker(refund_options,
          function (start, end) {
            if (start._isValid && end._isValid) {
              $refundDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
                  + end.format('YYYY-MM-DD'));
            } else {
              $refundDateRangeBasic.val('');
            }
          });
      $("input[name='refund_startDate']").val(
          refund_options.startDate.format('YYYY-MM-DD'));
      $("input[name='refund_endDate']").val(
          refund_options.endDate.format('YYYY-MM-DD'));

      var $returnDateRangeBasic = $('.return_daterange-basic');
      $returnDateRangeBasic.daterangepicker(return_options,
          function (start, end) {
            if (start._isValid && end._isValid) {
              $returnDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
                  + end.format('YYYY-MM-DD'));
            } else {
              $returnDateRangeBasic.val('');
            }
          });
      $("input[name='return_startDate']").val(
          return_options.startDate.format('YYYY-MM-DD'));
      $("input[name='return_endDate']").val(
          return_options.endDate.format('YYYY-MM-DD'));

      var $changeDateRangeBasic = $('.change_daterange-basic');
      $changeDateRangeBasic.daterangepicker(change_options,
          function (start, end) {
            if (start._isValid && end._isValid) {
              $changeDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
                  + end.format('YYYY-MM-DD'));
            } else {
              $changeDateRangeBasic.val('');
            }
          });
      $("input[name='change_startDate']").val(
          change_options.startDate.format('YYYY-MM-DD'));
      $("input[name='change_endDate']").val(
          change_options.endDate.format('YYYY-MM-DD'));

      $dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        options.startDate = picker.startDate;
        options.endDate = picker.endDate;
        $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      /**
       * 清空按钮清空选框
       */
      $dateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        //do something, like clearing an input
        $dateRangeBasic.val('');
      });

      $refundDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        refund_options.startDate = picker.startDate;
        refund_options.endDate = picker.endDate;
        $("input[name='refund_startDate']").val(
            picker.startDate.format('YYYY-MM-DD'));
        $("input[name='refund_endDate']").val(
            picker.endDate.format('YYYY-MM-DD'));
      });

      /**
       * 清空按钮清空选框
       */
      $refundDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        //do something, like clearing an input
        $refundDateRangeBasic.val('');
      });

      $returnDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        return_options.startDate = picker.startDate;
        return_options.endDate = picker.endDate;
        $("input[name='return_startDate']").val(
            picker.startDate.format('YYYY-MM-DD'));
        $("input[name='return_endDate']").val(
            picker.endDate.format('YYYY-MM-DD'));
      });

      /**
       * 清空按钮清空选框
       */
      $returnDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        //do something, like clearing an input
        $returnDateRangeBasic.val('');
      });

      $changeDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        change_options.startDate = picker.startDate;
        change_options.endDate = picker.endDate;
        $("input[name='change_startDate']").val(
            picker.startDate.format('YYYY-MM-DD'));
        $("input[name='change_endDate']").val(
            picker.endDate.format('YYYY-MM-DD'));
      });

      /**
       * 清空按钮清空选框
       */
      $changeDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        //do something, like clearing an input
        $changeDateRangeBasic.val('');
      });

      $('#myTab a').click(function (e) {
        e.preventDefault();//阻止a链接的跳转行为
        $(this).tab('show');//显示当前选中的链接及关联的content
      })

      /** 初始化选择框控件 **/
      $('.select').select2({
        minimumResultsForSearch: Infinity,
      });

      var $refund_datatables;

      //申请退款订单
      $refund_datatables = utils.createDataTable(
          '#xquark_refund_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            destroy: true,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/merchant/order/list", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $('input[name="refund_orderNo"]').val(),
                status: 'REFUNDING',
                buyerRequire: '1',
                refundStatus: $('select[name="orderRefundStatus"]').val(),
                buyerName: $('input[name="buyerName"]').val(),
                logisticsNo: $('input[name="logisticsNo"]').val(),
                pageable: true
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else if (res.data.orderTotal > 0) {
                  $("#refundOrderCountDiv").css("display", "inline-block");
                  $("#refundOrderCount").html(res.data.orderTotal);
                } else if (res.data.orderTotal == 0) {
                  $("#refundOrderCountDiv").hide();
                }
                callback({
                  recordsTotal: res.data.orderTotal,
                  recordsFiltered: res.data.orderTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.orderTotal,
                  iTotalDisplayRecords: res.data.orderTotal
                });
              });
            },
            rowId: "id",
            columns: [

              {
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="#"><img class="goods-image" src="'
                      + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                title: "订单",
                orderable: false,
                render: function (data, type, row) {
                  return '订单编号: ' + row.orderNo + '<br/>'
                      + row.orderItems[0].productName + '等';
                }
              },
              /**{
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '';
                    }
                }, **/
              {
                title: "买家",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup == 1) {
                    userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                    != null ? row.buyerPhone : "");
                  } else {
                    userInfo = row.orderAddress.consignee + '<br/>'
                        + (row.buyerPhone != null ? row.buyerPhone : "");
                  }
                  return userInfo;
                }
              }, {
                title: "下单时间",
                orderable: false,
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
              }, {
                title: "订单类型",
                orderable: false,
                render: function (data, type, row) {
                  var status = "";
                  // 订单类型
                  switch (row.orderType) {
                    case 'NORMAL':
                      status = '普通';
                      break;
                    case 'INSIDE_BUY':
                      status = '内购';
                      break;
                    case 'COUPON':
                      status = '优惠卷';
                      break;
                    case 'GROUPON':
                      status = '团购';
                      break;
                    case 'BARGAIN':
                      status = '砍价';
                      break;
                    case 'YUNDOU':
                      status = '积分';
                      break;
                    case 'FLASHSALE':
                      status = '限量秒杀';
                      break;
                    case 'MEETING':
                      status = '大会商品';
                      break;
                    default:
                      break;
                  }
                  if (row.isPickup == 1) {
                    status = status + "【自提】";
                  }
                  var html = status;
                  return html;
                }
              }, {
                title: "订单状态",
                width: "80px",
                orderable: false,
                render: function (data, type, row) {
                  let status;
                  switch (row.refundStatus) {
                    case 'SUBMITTED':
                      status = '物流信息已提交</br>等待审核';
                      break;
                    case 'SUCCESS':
                      status = '退款成功';
                      break;
                    case 'CANCELLED':
                      status = '退款已取消';
                      break;
                    case 'APPLY_FOR_REFUND':
                      status = "退款申请中";
                      break;
                    case 'REJECT_REFUND':
                      status = "退款申请被拒绝</br>原因：";
                      break;
                    case 'ACCEPT_REFUND':
                      status = '退款申请通过</br>等待提交物流信息';
                      break;
                    case 'WMS_CHECKED':
                      status = 'WMS审核结束';
                      break;
                    default:
                      status = '';
                  }
                  return status;
                }
              }, {
                title: "实付金额",
                data: "totalFee",
                name: "totalFee",
                orderable: false,
              }, {
                title: "运费",
                data: "logisticsFee",
                name: "logisticsFee",
                orderable: false,
              }, {
                title: "退款原因",
                data: "refundReason",
                name: "refundReason",
                orderable: false,
              }, {
                title: "退款说明",
                data: "refundMemo",
                name: "refundMemo",
                orderable: false,
              },
              {
                title: "退款凭证",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  for (var i = 0; i < row.refundImgList.length; i++) {
                    html = html + '<a href="' + row.refundImgList[i].imageUrl
                        + '" target="_blank"><img class="goods-image" src="'
                        + row.refundImgList[i].imageUrl + '" /></a>'
                  }
                  return html;
                }

              },
              {
                title: "物流信息",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                      === 'WMS_CHECKED') {
                    let c = row.refundLogisticsCompany || '';
                    let no = row.refundLogisticsNo || '';
                    let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                    return logistics;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "入库审核状态",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED') {
                    return "等待审核";
                  }
                  if (row.refundStatus === 'WMS_CHECKED') {
                    let s = row.wmsCheckStatus || '';
                    let mark = row.wmsCheckRemark || '';
                    let time = row.wmsCheckTime || '';
                    let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常'
                        : '入库异常'}
                                      </br> 备注: ${mark}
                                      </br>审核时间:${new Date(
                        time).toLocaleString()} `;
                    return content;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "操作",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                  let html = '';
                  html = html
                      + '<a class="viewDetailBtn role_check_table" href="#" style="margin-right: 10px;" order-no="'
                      + row.orderNo + '" > 查看订单详情</a>';
                  if(row.refundStatus === 'ACCEPT_REFUND' || row.refundStatus === 'ACCEPT_CHANGE'
                      || row.refundStatus === 'ACCEPT_REISSUE'){
                    html += '<a class="submitWmsBtn" data-toggle="modal" data-target="#myModal" style="margin-right: 10px;" orderStatus="'
                        + row.orderStatus + '" order-no="' + row.orderNo
                        + '" order-id="' + row.id + '"> 填写物流信息</a>';
                  }
                  if (row.refundStatus === 'SUBMITTED') {
                    html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a>`;
                  }
                  return html;
                }
              }
            ],
            select: {

              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              initEvent();
            }

          });

      var $change_datatables; //定义换货订单列表tab

      // 申请换货订单
      $("#changeOrderTab").on("click", function () {
        if ($change_datatables != undefined) {
          return;
        }
        $change_datatables = utils.createDataTable(
            '#xquark_change_list_tables', {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              destroy: true,
              ajax: function (data, callback, settings) {
                $.get(window.host + "/merchant/order/list", {
                  size: data.length,
                  page: (data.start / data.length),
                  orderNo: $('input[name="change_orderNo"]').val(),
                  status: 'CHANGING',//状态为换货中
                  buyerRequire: '3', //查询出换货的订单
                  refundStatus: $('select[name="orderRefundStatus"]').val(),
                  buyerName: $('input[name="buyerName"]').val(),
                  logisticsNo: $('input[name="logisticsNo"]').val(),
                  pageable: true
                }, function (res) {
                  if (!res.data.list) {
                    res.data.list = [];
                  } else if (res.data.orderTotal > 0) {
                    $("#changeOrderCountDiv").css("display", "inline-block");
                    $("#changeOrderCount").html(res.data.orderTotal);
                  } else if (res.data.orderTotal == 0) {
                    $("#changeOrderCountDiv").hide();
                  }
                  callback({
                    recordsTotal: res.data.orderTotal,
                    recordsFiltered: res.data.orderTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.orderTotal,
                    iTotalDisplayRecords: res.data.orderTotal
                  });
                });
              },
              rowId: "id",
              columns: [
                {
                  orderable: false,
                  render: function (data, type, row) {
                    return '<a href="#"><img class="goods-image" src="'
                        + row.imgUrl
                        + '" /></a>';
                  }
                },
                {
                  title: "订单",
                  orderable: false,
                  render: function (data, type, row) {
                    return '订单编号: ' + row.orderNo + '<br/>'
                        + row.orderItems[0].productName + '等';
                  }
                },
                {
                  title: "买家",
                  orderable: false,
                  render: function (data, type, row) {
                    var userInfo = "";
                    if (row.isPickup == 1) {
                      userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                      != null ? row.buyerPhone : "");
                    } else {
                      userInfo = row.orderAddress.consignee + '<br/>'
                          + (row.buyerPhone != null ? row.buyerPhone : "");
                    }
                    return userInfo;
                  }
                },
                {
                  title: "下单时间",
                  orderable: false,
                  render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  }
                },
                {
                  title: "订单类型",
                  orderable: false,
                  render: function (data, type, row) {
                    var status = "";
                    // 订单类型
                    switch (row.orderType) {
                      case 'NORMAL':
                        status = '普通';
                        break;
                      case 'INSIDE_BUY':
                        status = '内购';
                        break;
                      case 'COUPON':
                        status = '优惠券';
                        break;
                      case 'GROUPON':
                        status = '团购';
                        break;
                      case 'BARGAIN':
                        status = '砍价';
                        break;
                      case 'YUNDOU':
                        status = '积分';
                        break;
                      case 'FLASHSALE':
                        status = '限量秒杀';
                        break;
                      case 'MEETING':
                        status = '大会商品';
                        break;
                      default:
                        break;
                    }
                    if (row.isPickup == 1) {
                      status = status + "【自提】";
                    }
                    var html = status;
                    return html;
                  }
                },
                {
                  title: "订单状态",
                  width: "80px",
                  orderable: false,
                  render: function (data, type, row) {
                    let status;
                    switch (row.refundStatus) {
                      case 'SUBMITTED':
                        status = '物流信息已提交</br>等待审核';
                        break;
                      case 'SUCCESS':
                        status = '换货成功';
                        break;
                      case 'CANCELLED':
                        status = '换货已取消';
                        break;
                      case 'APPLY_FOR_CHANGE':
                        status = "换货申请中";
                        break;
                      case 'REJECT_CHANGE':
                        status = "换货申请被拒绝</br>原因：";
                        break;
                      case 'ACCEPT_CHANGE':
                        status = '换货申请通过</br>等待提交物流信息';
                        break;
                      case 'WMS_CHECKED':
                        status = 'WMS审核结束';
                        break;
                      default:
                        status = '';
                    }
                    return status;
                  }
                }, {
                  title: "实付金额",
                  data: "totalFee",
                  name: "totalFee",
                  orderable: false,
                }, {
                  title: "运费",
                  data: "logisticsFee",
                  name: "logisticsFee",
                  orderable: false,
                }, {
                  title: "换货原因",
                  data: "refundReason",
                  name: "refundReason",
                  orderable: false,
                }, {
                  title: "换货说明",
                  data: "refundMemo",
                  name: "refundMemo",
                  orderable: false,
                },
                {
                  title: "换货凭证",
                  orderable: false,
                  render: function (data, type, row) {
                    var html = '';
                    if(row.refundImgList != null){
                      for (var i = 0; i < row.refundImgList.length; i++) {
                        html = html + '<a href="' + row.refundImgList[i].imageUrl
                            + '" target="_blank"><img class="goods-image" src="'
                            + row.refundImgList[i].imageUrl + '" /></a>'
                      }
                    }
                    return html;
                  }
                },
                {
                  title: "物流信息",
                  orderable: false,
                  render: function (data, type, row) {
                    if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                        === 'WMS_CHECKED') {
                      let c = row.refundLogisticsCompany || '';
                      let no = row.refundLogisticsNo || '';
                      let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                      return logistics;
                    }
                    return "物流信息未提交";
                  }
                },
                {
                  title: "WMS审核状态",
                  orderable: false,
                  render: function (data, type, row) {
                    if (row.refundStatus === 'SUBMITTED') {
                      return "等待WMS审核";
                    }
                    if (row.refundStatus === 'WMS_CHECKED') {
                      let s = row.wmsCheckStatus || '';
                      let mark = row.wmsCheckRemark || '';
                      let time = row.wmsCheckTime || '';
                      let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常' : '入库异常'}
                                   </br> 备注: ${mark}
                                   </br>审核时间:${new Date(time).toLocaleString()} `;
                      return content;
                    }
                    return "物流信息未提交";
                  }
                },
                {
                  title: "操作",
                  width: "100px",
                  orderable: false,
                  render: function (data, type, row) {
                    let html = '';
                    html = html
                        + '<a class="viewDetailBtnChange role_check_table" href="#" style="margin-right: 10px;" order-id="' + row.id + '" order-no="'
                        + row.orderNo + '" > 查看换货商品详情</a>';
                    if(row.refundStatus === 'ACCEPT_REFUND' || row.refundStatus === 'ACCEPT_CHANGE'
                        || row.refundStatus === 'ACCEPT_REISSUE'){
                      html += '<a class="submitWmsBtnForChange" data-toggle="modal" data-target="#myModal" style="margin-right: 10px;" orderStatus="'
                          + row.orderStatus + '" order-no="' + row.orderNo
                          + '" order-id="' + row.id + '"> 填写物流信息</a>';
                    }
                    if (row.refundStatus === 'SUBMITTED') {
                      html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a>`;
                    }
                    return html;
                  }
                }
              ],
              select: {
                style: 'multi'
              },
              drawCallback: function () {  //数据加载完成
                initEvent();
              }
            });
      });

      var $reissue_datatables;//定义补货tab

      // 申请补货订单
      $("#reissueOrderTab").on("click", function () {
        if ($reissue_datatables != undefined) {
          return;
        }
        $reissue_datatables = utils.createDataTable(
            '#xquark_reissue_list_tables', {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              destroy: true,
              ajax: function (data, callback, settings) {
                $.get(window.host + "/merchant/order/list", {
                  size: data.length,
                  page: (data.start / data.length),
                  orderNo: $('input[name="reissue_orderNo"]').val(),
                  status: 'REISSUING',//状态为补货中
                  buyerRequire: '4', //查询出补货的订单
                  refundStatus: $('select[name="orderRefundStatus"]').val(),
                  buyerName: $('input[name="buyerName"]').val(),
                  logisticsNo: $('input[name="logisticsNo"]').val(),
                  pageable: true
                }, function (res) {
                  if (!res.data.list) {
                    res.data.list = [];
                  } else if (res.data.orderTotal > 0) {
                    $("#reissueOrderCountDiv").css("display", "inline-block");
                    $("#reissueOrderCount").html(res.data.orderTotal);
                  } else if (res.data.orderTotal == 0) {
                    $("#reissueOrderCountDiv").hide();
                  }
                  callback({
                    recordsTotal: res.data.orderTotal,
                    recordsFiltered: res.data.orderTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.orderTotal,
                    iTotalDisplayRecords: res.data.orderTotal
                  });
                });
              },
              rowId: "id",
              columns: [
                {
                  orderable: false,
                  render: function (data, type, row) {
                    return '<a href="#"><img class="goods-image" src="'
                        + row.imgUrl
                        + '" /></a>';
                  }
                },
                {
                  title: "订单",
                  orderable: false,
                  render: function (data, type, row) {
                    return '订单编号: ' + row.orderNo + '<br/>'
                        + row.orderItems[0].productName + '等';
                  }
                },
                {
                  title: "买家",
                  orderable: false,
                  render: function (data, type, row) {
                    var userInfo = "";
                    if (row.isPickup == 1) {
                      userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                      != null ? row.buyerPhone : "");
                    } else {
                      userInfo = row.orderAddress.consignee + '<br/>'
                          + (row.buyerPhone != null ? row.buyerPhone : "");
                    }
                    return userInfo;
                  }
                },
                {
                  title: "下单时间",
                  orderable: false,
                  render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  }
                },
                {
                  title: "订单类型",
                  orderable: false,
                  render: function (data, type, row) {
                    var status = "";
                    // 订单类型
                    switch (row.orderType) {
                      case 'NORMAL':
                        status = '普通';
                        break;
                      case 'INSIDE_BUY':
                        status = '内购';
                        break;
                      case 'COUPON':
                        status = '优惠券';
                        break;
                      case 'GROUPON':
                        status = '团购';
                        break;
                      case 'BARGAIN':
                        status = '砍价';
                        break;
                      case 'YUNDOU':
                        status = '积分';
                        break;
                      case 'FLASHSALE':
                        status = '限量秒杀';
                        break;
                      case 'MEETING':
                        status = '大会商品';
                        break;
                      default:
                        break;
                    }
                    if (row.isPickup == 1) {
                      status = status + "【自提】";
                    }
                    var html = status;
                    return html;
                  }
                },
                {
                  title: "订单状态",
                  width: "80px",
                  orderable: false,
                  render: function (data, type, row) {
                    let status;
                    switch (row.refundStatus) {
                      case 'SUBMITTED':
                        status = '物流信息已提交</br>等待审核';
                        break;
                      case 'SUCCESS':
                        status = '补货成功';
                        break;
                      case 'CANCELLED':
                        status = '补货已取消';
                        break;
                      case 'APPLY_FOR_REISSUE':
                        status = "补货申请中";
                        break;
                      case 'REJECT_REISSUE':
                        status = "补货申请被拒绝</br>原因：";
                        break;
                      case 'ACCEPT_REISSUE':
                        status = '补货申请通过';
                        break;
                      default:
                        status = '';
                    }
                    return status;
                  }
                }, {
                  title: "实付金额",
                  data: "totalFee",
                  name: "totalFee",
                  orderable: false,
                }, {
                  title: "运费",
                  data: "logisticsFee",
                  name: "logisticsFee",
                  orderable: false,
                }, {
                  title: "补货原因",
                  data: "refundReason",
                  name: "refundReason",
                  orderable: false,
                }, {
                  title: "补货说明",
                  data: "refundMemo",
                  name: "refundMemo",
                  orderable: false,
                },
                {
                  title: "补货凭证",
                  orderable: false,
                  render: function (data, type, row) {
                    var html = '';
                    if(row.refundImgList != null){
                      for (var i = 0; i < row.refundImgList.length; i++) {
                        html = html + '<a href="' + row.refundImgList[i].imageUrl
                            + '" target="_blank"><img class="goods-image" src="'
                            + row.refundImgList[i].imageUrl + '" /></a>'
                      }
                    }
                    return html;
                  }
                },
                {
                  title: "物流信息",
                  orderable: false,
                  render: function (data, type, row) {
                    if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                        === 'WMS_CHECKED') {
                      let c = row.refundLogisticsCompany || '';
                      let no = row.refundLogisticsNo || '';
                      let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                      return logistics;
                    }
                    return "物流信息未提交";
                  }
                },
                {
                  title: "WMS审核状态",
                  orderable: false,
                  render: function (data, type, row) {
                    if (row.refundStatus === 'SUBMITTED') {
                      return "等待WMS审核";
                    }
                    if (row.refundStatus === 'WMS_CHECKED') {
                      let s = row.wmsCheckStatus || '';
                      let mark = row.wmsCheckRemark || '';
                      let time = row.wmsCheckTime || '';
                      let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常' : '入库异常'}
                                   </br> 备注: ${mark}
                                   </br>审核时间:${new Date(time).toLocaleString()} `;
                      return content;
                    }
                    return "物流信息未提交";
                  }
                },
                {
                  title: "操作",
                  width: "100px",
                  orderable: false,
                  render: function (data, type, row) {
                    let html = '';
                    html = html
                        + '<a class="viewDetailBtnChange role_check_table" id="reissueGoodsDetail" href="#" style="margin-right: 10px;" order-id="' + row.id + '" order-no="'
                        + row.orderNo + '" > 查看补货商品详情</a>';
                    if(row.refundStatus === 'ACCEPT_REFUND' || row.refundStatus === 'ACCEPT_CHANGE'
                        || row.refundStatus === 'ACCEPT_REISSUE'){
                      html += '<a class="submitWmsBtnForReissue" data-toggle="modal" data-target="#myModal" style="margin-right: 10px;" orderStatus="'
                          + row.orderStatus + '" order-no="' + row.orderNo
                          + '" order-id="' + row.id + '"> 填写物流信息</a>';
                    }
                    if (row.refundStatus === 'SUBMITTED') {
                      html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a>`;
                    }
                    return html;
                  }
                }
              ],
              select: {
                style: 'multi'
              },
              drawCallback: function () {  //数据加载完成
                initEvent();
              }
            });
      });

      //给Body加一个Click监听事件
      $('body').on('click', function (event) {
        var target = $(event.target);
        if (!target.hasClass('popover') //弹窗内部点击不关闭
            && target.parent('.popover-content').length === 0
            && target.parent('.popover-title').length === 0
            && target.parent('.popover').length === 0
            && target.data("toggle") !== "popover") {
          //弹窗触发列不关闭，否则显示后隐藏
          $('[data-toggle="popover"]').popover('hide');
        } else if (target.data("toggle") == "popover") {
          target.popover("toggle");
        }
      });

      //给Body加一个Click监听事件
      $('body').on('click', function (event) {
        var target = $(event.target);
        if (!target.hasClass('popover') //弹窗内部点击不关闭
            && target.parent('.popover-content').length === 0
            && target.parent('.popover-title').length === 0
            && target.parent('.popover').length === 0
            && target.data("toggle") !== "refundpopover") {
          //弹窗触发列不关闭，否则显示后隐藏
          $('[data-toggle="refundpopover"]').popover('hide');
        } else if (target.data("toggle") == "refundpopover") {
          target.popover("toggle");
        }
      });

      var shippingOrderId;
      var isPickup;
      var auditOrderId;

      function initEvent() {
        let orderIdSubmitWms;
        $(".submitWmsBtn").off("click").on("click", function() {
          orderIdSubmitWms = $(this).attr("order-id");
        });
        $(".submitWmsBtnForChange").off("click").on("click", function() {
          orderIdSubmitWms = $(this).attr("order-id");
        });
        $(".submitWmsBtnForReissue").off("click").on("click", function() {
          orderIdSubmitWms = $(this).attr("order-id");
        });
        $("#submitWmsMessage").off("click").on("click", function () {
          let logiCompany = $("#logiCompany").val();
          let logiNumber =  $("#logiNumber").val();
          if(isEmpty(logiCompany) || isEmpty(logiNumber)){
            alert("请填写完整的物流信息!");
          }
          //更新物流信息
          var a = confirm("确认提交吗？");
          if (a == true) {
            var data = {
              'orderId': orderIdSubmitWms,
              'logiCompany':logiCompany,
              'logiNumber':logiNumber
            };
            utils.postAjax(window.host + '/order/wms/update', data, function (res) {
              if (typeof(res) === 'object') {
                if (res.data === true) {
                  utils.tools.alert("提交成功", {timer: 1200});
                  window.location.reload();
                } else {
                  utils.tools.alert("操作失败!",
                      {timer: 1200, type: 'warning'});
                }
              }
            });
          }
        });


        $(".cancelRefundBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          utils.tools.confirmWihoutText('确定取消退款？', function () {
            cancelRefund(orderId);
          }, function () {
          })

        });

        $(".wmsCheck").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          wmsForm.find("input[name=id]").prop("value", id);
          wmsCheckModal.modal("show");
        });

        $(".cancelBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          cancelOrder(orderId);
        });

        $(".passRefundApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定通过退款审核？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "ACCEPT_REFUND"}
            ).done(
                data => {
              utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
            $refund_datatables.search('').draw();
          }
          ).
            fail(
                data => {
              utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
          })
            ;

          },
            500
          )
            ;
          }, function () {
          })
        });

        $(".failRefundApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定拒绝退款申请？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "REJECT_REFUND"}
            ).done(
                data => {
              utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
            $refund_datatables.search('').draw();
          }
          ).
            fail(
                data => {
              utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
          })
            ;
          },
            500
          )
            ;
          }, function () {
          })
        });

        $("select[name=orderStatus]").off("change").on("change", function () {
          $datatables.search('').draw();
        });

        $(".shipBtn").off("click").on("click", function () {
          shippingOrderId = $(this).attr('order-id');
          isPickup = $(this).attr('isPickup');
          if (isPickup == 1) {
            $("#logisticsTitle").html("确认发货");
            $("#logisticsCompany").hide();
            $("#logisticsOrderNo").hide();
            $("#logisticsDesc").show();
          }
          $("#modal_shipping").modal('show');
        });

        $(".auditBtn").off("click").on("click", function () {
          auditOrderId = $(this).attr('order-id');
          $("#modal_audit").modal('show');
        });

        // 修改订单数量
        $(".updateQtyBtn").off("click").on("click", function () {
          var orderNo = $(this).attr('order-no');
          var orderAmount = $(this).attr('order-amount');
          $("#update_orderNo").val(orderNo);
          $("#update_orderAmount").val(orderAmount);
          $("#modal_updateAmount").modal('show');
        });

        // 删除订单
        $(".delBtn").off("click").on("click", function () {
          var orderId = $(this).attr('order-id');
          var a = confirm("确定删除该订单吗？");
          if (a == true) {
            var data = {
              'orderId': orderId
            };
            utils.postAjax(window.host + '/order/delete', data,
                function (res) {
                  if (typeof(res) === 'object') {
                    if (res.data) {
                      utils.tools.alert("删除成功", {timer: 1200});
                      $datatables.search('').draw();
                    } else {
                      utils.tools.alert("删除订单失败!",
                          {timer: 1200, type: 'warning'});
                    }
                  }
                });
          }

        });

        // 确认订单
        $(".confirmBtn").off("click").on("click", function () {
          var orderId = $(this).attr('order-id');
          var a = confirm("确定确认该订单吗？");
          if (a == true) {
            var data = {
              'orderId': orderId
            };
            utils.postAjax(window.host + '/order/confirmOrder', data,
                function (res) {
                  if (typeof(res) === 'object') {
                    if (res.data) {
                      utils.tools.alert("确认成功", {timer: 1200});
                      $datatables.search('').draw();
                    } else {
                      utils.tools.alert("确认订单失败!",
                          {timer: 1200, type: 'warning'});
                    }
                  }
                });
          }

        });

        tableRoleCheck('#xquark_list_tables');

        //查看订单详情
        $(".viewDetailBtn").off("click").on("click", function () {
          var orderNo = $(this).attr("order-no");
          //查看订单详情
          if ($orderTables !== undefined) {
            $orderTables.search('').draw();
            return;
          }
          /** 页面表格默认配置 **/
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
              search: '<span>筛选:</span> _INPUT_',
              lengthMenu: '<span>显示:</span> _MENU_',
              info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
              paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
              },
              infoEmpty: "",
              emptyTable: "暂无相关数据"
            }
          });

          var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            destroy: true,
            ordering: false,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/twitter/order/orderItem", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: orderNo,
                pageable: true,
              }, function (res) {
                if (res.errorCode != '200') {
                  return;
                }

                if (!res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.itemTotal,
                  recordsFiltered: res.data.itemTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.itemTotal,
                  iTotalDisplayRecords: res.data.itemTotal
                });
              });
            },
            columns: [{
              data: 'productName',
              name: 'productName',
              orderable: false,
              title: "商品"
            },
              {
                data: 'marketPrice',
                name: 'marketPrice',
                orderable: false,
                title: "原价"
              },
              {
                data: 'discount',
                name: 'discount',
                orderable: false,
                title: "折扣"
              },
              {
                data: 'discountPrice',
                name: 'discountPrice',
                orderable: false,
                title: "优惠金额"
              },
              {
                data: 'price',
                name: 'price',
                orderable: false,
                title: "订单金额"
              }, {
                data: 'amount',
                name: 'amount',
                orderable: false,
                title: "数量"
              }, {
                data: 'addressDetails',
                name: 'addressDetails',
                orderable: false,
                title: "收货地址"
              },
              {
                orderable: false,
                title: "下单时间",
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
              },
              {
                data: 'logisticsOrderNo',
                name: 'logisticsOrderNo',
                orderable: false,
                title: "物流单号"
              }, {
                width: "150px",
                orderable: false,
                title: "收货时间",
                render: function (data, type, row) {
                  if (row.succeedAt && row.succeedAt != '') {
                    var cDate = parseInt(row.succeedAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  } else {
                    return "";
                  }
                },
              }],
            drawCallback: function () {  //数据加载完成
            }
          });
          $('#modal_orderInfo').modal('show');
        });

        //查看换货订单详情
        $(".viewDetailBtnChange").off("click").on("click", function () {

          let orderId = $(this).attr("order-id");
          //查看订单详情
          if ($orderTables !== undefined) {
            $orderTables.search('').draw();
            return;
          }
          /** 页面表格默认配置 **/
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
              search: '<span>筛选:</span> _INPUT_',
              lengthMenu: '<span>显示:</span> _MENU_',
              info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
              paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
              },
              infoEmpty: "",
              emptyTable: "暂无相关数据"
            }
          });
          var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            destroy: true,
            ordering: false,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/twitter/order/changeOrderItem", {
                size: data.length,
                page: (data.start / data.length),
                orderId: orderId,
                pageable: true,
              }, function (res) {
                if (res.errorCode != '200') {
                  return;
                }
                if (!res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.itemTotal,
                  recordsFiltered: res.data.itemTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.itemTotal,
                  iTotalDisplayRecords: res.data.itemTotal
                });
              });
            },
            columns: [
              {
                data: 'product.imgUrl',
                name: 'product.imgUrl',
                orderable: false,
                title: "商品图片",
                render: function (data) {
                  return data
                      ? `<img class="goods-image" src="${data}" />`
                      : `<img class="goods-image" src="${NOT_FOUND}" />`;
                }
              },
              {
                data: 'product.name',
                name: 'product.name',
                orderable: false,
                title: "商品名称"
              },
              {
                title: "数量",
                data: "amount",
                name: "amount",
                orderable: false,
              },
              {
                title: "类别",
                orderable: false,
                render: function (data, type, row) {
                  const orderType = row.orderType;
                  switch (orderType) {
                    case 'INSIDE_BUY':
                      return '内购订单';
                    case 'COUPON':
                      return '优惠券';
                    case 'FULLCUT':
                      return '会员活动';
                    default:
                      return '普通订单';
                  }
                }
              },
              {
                data: 'product.status',
                name: 'product.status',
                orderable: false,
                title: "商品状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '已上架';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '草稿';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.reviewStatus',
                name: 'product.reviewStatus',
                orderable: false,
                title: "审核状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'NO_NEED_TO_CHECK':
                      status = '未提交审核';
                      break;
                    case 'WAIT_CHECK':
                      status = '待审核';
                      break;
                    case 'CHECK_FAIL':
                      status = '审核未通过';
                      break;
                    case 'CHECK_PASS':
                      status = '审核通过';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.supplierId',
                name: 'product.supplierId',
                orderable: false,
                title: "供应商",
                render: function (data) {
                  const id = data;
                  if (id && id !== '') {
                    let name;
                    if (name = sourceMap.get(id)) {
                      return name;
                    }
                  }
                  return '无';
                }
              },
              {
                data: 'product.marketPrice',
                name: 'product.marketPrice',
                orderable: false,
                title: "原价"
              },
              {
                data: 'product.price',
                name: 'product.price',
                orderable: false,
                title: "优惠价"
              },
              {
                data: 'product.amount',
                name: 'product.amount',
                orderable: false,
                title: "库存"
              },
              {
                data: 'product.sales',
                name: 'product.sales',
                orderable: false,
                title: "销量"
              },
              {
                data: 'product.description',
                name: 'product.description',
                orderable: false,
                title: "商品描述"
              },
            ],
            drawCallback: function () {  //数据加载完成
            }
          });
          $('#modal_orderInfo').modal('show');
        });

        // 退款
        $(".refundBtn").off("click").on("click", function () {
          var id = $(this).attr("rowId");
          var refundStatus = $(this).attr("refundStatus");
          var totalFee = $(this).attr("totalFee");
          var logisticsFee = $(this).attr("logisticsFee");
          var refundFee = 0;
          if (refundStatus == 'SHIPPED') {
            refundFee = Number(totalFee) - Number(logisticsFee);
          } else {
            refundFee = Number(totalFee);
          }
          $("#refund_order_id").val(id);
          $("#refund_fee").val(refundFee);
          $("#total_fee").val(totalFee);

          $("#refund_info_body").children().remove()
          var data = $('#xquark_refund_list_tables').DataTable().row(
              this.parentElement.parentElement).data()//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
          for (var i = 0; i < data.orderItems.length; i++) {

            $("#refund_info_body").append(
                `<br/><div>${data.orderItems[i].productName}  实际支付金额：${data.orderItems[i].totalDiscountPrice} x${data.orderItems[i].amount} </div>`)
          }
          $('#modal_refund_logistic_info').modal('show');
        });

        // 换货
        $('.changeBtn').off("click", function () {
          var id = $(this).attr("rowId");
          var data = {
            'orderId': id
          };
          utils.postAjax(window.host + '/order/changeOrder', data,
              function (res) {
                if (typeof(res) === 'object') {
                  if (res.data) {
                    utils.tools.alert("操作成功", {timer: 1200});
                    $datatables.search('').draw();
                    $change_datatables.search('').draw();
                  } else {
                    utils.tools.alert("操作失败!",
                        {timer: 1200, type: 'warning'});
                  }
                }
              });
        });
      }

      $(".btn-search").off("click").on('click', function () {
        $listUrl = window.host + "/product/list";
        var keyword = $.trim($("#sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null) {
          $listUrl = window.host + '/product/searchbyPc/' + shopId + '/'
              + keyword;
        }
        $datatables.search(keyword).draw();
      });

      $(".refund_btn-search").off("click").on('click', function () {
        $refund_datatables.search('').draw();
      });

      $(".change_btn-search").off("click").on('click', function () {
        $change_datatables.search('').draw();
      });

      $(".reissue_btn-search").off("click").on('click', function () {
        $reissue_datatables.search('').draw();
      });

      $(".updateAmountBtn").off("click").on('click', function () {
        var orderAmount = $('input[name="update_orderAmount"]').val();
        var orderNo = $('input[name="update_orderNo"]').val();
        if (orderAmount == null || orderAmount == '') {
          utils.tools.alert('请填写订单数量');
        }
        var data = {
          'orderNo': orderNo,
          'orderAmount': orderAmount
        };
        utils.postAjax(window.host + '/order/updateAmount', data,
            function (res) {
              if (typeof(res) === 'object') {
                if (res.data) {
                  utils.tools.alert("修改成功", {timer: 1200});
                  $("#modal_updateAmount").modal('hide');
                  $datatables.search('').draw();
                } else {
                  utils.tools.alert("更新订单数量失败!",
                      {timer: 1200, type: 'warning'});
                }
              }
            });
      });

      $(".shippedBtn").off("click").on('click', function () {
        if (shippingOrderId != null) {
          var logisticsCompany = $('input[name="logisticsCompany"]').val();
          var logisticsOrderNo = $('input[name="logisticsOrderNo"]').val();
          if (isPickup != 1 && (logisticsCompany == null || logisticsOrderNo
              == null)) {
            utils.tools.alert('请填写物流信息');
          }
          var data = {
            'logisticsCompany': logisticsCompany,
            'logisticsOrderNo': logisticsOrderNo,
            'orderId': shippingOrderId
          };
          utils.postAjax(window.originalHost + '/order/shipped', data,
              function (res) {
                if (res == true) {
                  $datatables.search('').draw();
                } else if (res == 0) {

                } else if (res == -1) {
                  utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                } else {
                  utils.tools.alert("更新物流信息失败!",
                      {timer: 1200, type: 'warning'});
                }
              });
        }
      });

      $(".auditedBtn").off("click").on('click', function () {
        if (auditOrderId != null) {
          var logisticsCompany = $(
              'input[name="audit_logisticsCompany"]').val();
          var logisticsOrderNo = $(
              'input[name="audit_logisticsOrderNo"]').val();
          var data = {
            'logisticsCompany': logisticsCompany,
            'logisticsOrderNo': logisticsOrderNo,
            'orderId': auditOrderId
          };

          $.ajax({
            url: window.host + '/order/auditAndShip',
            data: data,
            type: "POST",
            success: function (data) {
              var _res = data;
              if (_res.rc == '1') {
                utils.tools.alert(_res.msg, {timer: 1200});
                $("#modal_audit").modal('hide');
                $datatables.search('').draw();
              } else if (_res.rc == '0') {
                utils.tools.alert(_res.msg, {timer: 4000});
              } else {
                alert("审核失败!");
              }
            },
            error: function (err) {
              alert("服务器暂时没有响应，请稍后重试。");
            }
          });

        }
      });

      /*商品删除会判断 userId 否则删除失败*/
      function deleteProduct(pId) {
        var url = window.host + "/product/delete/" + pId;
        utils.postAjax(url, {}, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              $datatables.search('').draw();
            } else {
              utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      // 订单退款
      // 根据退款前订单状态判断是否退全款或者扣除运费，默认算出一个退款金额，用户也可手动更改
      function refundOrder(pId, refundFee) {
        var data = {
          orderId: pId,
          refundment: refundFee
        };
        var url = window.host + "/order/refund";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("退款成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw('page');
              $refund_datatables.search('').draw('page');
            } else {
              utils.tools.alert("退款失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      function changeOrder(pId, refundFee) {
        var data = {
          orderId: pId,
          refundment: refundFee
        };
        var url = window.host + "/order/refund";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("退款成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw('page');
              $refund_datatables.search('').draw('page');
              $return_datatables.search('').draw('page');
            } else {
              utils.tools.alert("退款失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      //取消退款
      function cancelRefund(oId) {
        var data = {
          orderId: oId,
        };
        var url = window.host + "/order/refund/cancel";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data === true) {
              utils.tools.alert("取消成功!", {timer: 1200, type: 'success'});
              $refund_datatables.search('').draw();
            } else {
              utils.tools.alert("取消失败!", {timer: 1200, type: 'warning'});
            }
          }
        });

      }

      // 取消订单
      function cancelOrder(pId) {
        var data = {
          orderId: pId,
        };
        var url = window.host + "/api/order/cancel";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("取消成功!", {timer: 1200, type: 'warning'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert("取消失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      $('.file-input').fileinput({
        language: 'zh',
        uploadUrl: window.originalHost + '/order/import',
        previewFileType: 'text',
        browseLabel: '选择文件',
        removeLabel: '删除',
        uploadLabel: '上传',
        browseIcon: '<i class="icon-file-plus"></i>',
        uploadIcon: '<i class="icon-file-upload2"></i>',
        removeIcon: '<i class="icon-cross3"></i>',
        browseClass: 'btn btn-primary',
        uploadClass: 'btn btn-default',
        removeClass: 'btn btn-danger',
        initialCaption: '',
        maxFilesNum: 5,
        allowedFileExtensions: ["xls"],
        layoutTemplates: {
          icon: '<i class="icon-file-check"></i>',
          footer: '',
        },
      });

      $('#modal_import_logistic_info').on('hidden.bs.modal', function () {
        $('#file-input').fileinput('clear');
      });

      $('#modal_import_logistic_info').on('fileuploaded',
          function (event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
            if (response == '200') {
              utils.tools.alert('文件上传成功');
              $('#modal_import_logistic_info').modal('hide');
            } else if (response == '207') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('发货信息上传全部失败,请核对上传格式是否正确,或数据是否填写正确');
            } else if (response == '206') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('部分订单上传失败,请核对上传格式是否正确,或数据是否填写正确');
            } else if (response == '500') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
            }
          });
      $(".exportOrderBtn").on('click', function () {
        $('#exportOrderForm').submit();
      });

      /**
       * 监听物流审核窗口的变化
       */
      wmsCheckModal.on('hidden.bs.modal', function (e) {
        wmsForm[0].reset();
      });

      /**
       * 监听物流审核创口的提交按钮点击
       */
      $("#submit-wms-check").on("click", function () {
        let obj = wmsForm.serializeObject();
        $.get(updateWmsStatusUrl(obj.id), {
              status: "WMS_CHECKED",
              checkStatus: obj.wmsCheckStatus,
              remark: obj.wmsCheckRemark,
            }
        ).done(
            data => {
          if(data && data.data === true
      )
        {
          utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
          wmsCheckModal.modal("hide");
          var refundClick = document.getElementById("refundBtnSearch");
          refundClick.click();
          $reissue_datatables.search('').draw();
        }
      else
        {
          utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
        }
      }
      ).
        fail(
            data => {
          utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
      });
        $change_datatables.search('').draw();//新增的换货审核通过，重新加载界面
      });

      $("#refund_ok").off("click").on('click', function () {
        var orderid = $("#refund_order_id").val();
        var refund_fee = $("#refund_fee").val();
        var total_fee = $("#total_fee").val();
        if (Number(refund_fee) > Number(total_fee)) {
          utils.tools.alert('退款金额不能大于订单总金额');
          return;
        }
        $('#modal_refund_logistic_info').modal('hide');
        refundOrder(orderid, refund_fee);
      });

      buttonRoleCheck('.hideClass');

    }
);
