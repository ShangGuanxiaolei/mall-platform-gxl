define(['jquery', 'utils', 'datatables', 'jquerySerializeObject'], function ($, utils) {
  const prefix = window.host + '/yundou/signIn';
  const listUrl = prefix + '/rule/list';
  const saveUrl = prefix + '/rule/save';
  const viewUrl = prefix + '/rule/show';
  const deleteUrl = prefix + '/rule/delete';

  var $dataTable;

  const operationMapper = {
    'PLUS': '+',
    'MULTIPLY': '*'
  };

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const manager = (function () {

    const $saveBtnRule = $('#save_sign_rule');
    const $signRuleForm = $('#signRuleForm');
    const $preformSave = $('#preformSave');
    const $modalSignRule = $('#modal_sign_rule');

    const globalInstance = {
      initEvent: function () {
        /* 新增规则弹窗 */
        $saveBtnRule.on('click', function () {
          utils.tools.syncForm($signRuleForm);
          $modalSignRule.modal('show');
        });

        /* 确认保存 */
        $preformSave.on('click', function () {
          const data = $signRuleForm.serializeObject();
          utils.postAjax(saveUrl, data, function (res) {
              if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
              }
              if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                      utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                      $dataTable.search('').draw();
                      $modalSignRule.modal('hide');
                  } else {
                      if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                      } else {
                          utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                      }
                  }
              }
          });
        });

      }
    };

    return {
      initGlobal: function () {
        globalInstance.initEvent();
        return this;
      },
      initTable: function () {
        if (!$dataTable) {
          $dataTable = $('#xquark_rule_tables')
          .DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
              $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
              }, function (res) {
                if (!res.data) {
                  utils.tools.alert(res.moreInfo,
                      {timer: 1200, type: 'warning'});
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              })
            },
            rowId: 'id',
            columns: [
              {
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  return row.init + '积分';
                }
              },
              {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                  var operation = row.operation;
                  var symbol = operationMapper[operation];
                  return symbol + row.operationNumber;
                }
              },
              {
                orderable: false,
                width: "50px",
                data: 'max',
                name: 'max'
              },
              {
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  if (cDate) {
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  }
                  return '默认创建';

                },
                name: "createdAt"
              },
              {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  html += '<a href="javascript:void(0);" class="edit" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
                  html += '<a href="javascript:void(0);" style="margin-left: 10px" class="remove" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>删除</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
              $('.edit').on('click', function () {
                var id = $(this).attr('id');
                utils.getJson(viewUrl, id, function (res) {
                    if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          var data = res.data;
                          if (data) {
                            console.log(data);
                            utils.tools.syncForm($signRuleForm, data);
                            $modalSignRule.modal('show');
                          } else {
                            utils.tools.alert('查看规则失败', { timer: 1200, type: 'warning' });
                          }
                        } else {
                            if (res.moreInfo) {
                                utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                            } else {
                                utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                            }
                        }
                    }
                });
              });

              $('.remove').on('click', function () {
                var id = $(this).attr('id');
                utils.postAjax(deleteUrl, id, function (res) {
                    if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          utils.tools.alert('删除成功', { timer: 1200, type: 'success' });
                          $dataTable.search().draw();
                        } else {
                            if (res.moreInfo) {
                                utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                            } else {
                                utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                            }
                        }
                    }
                });
              });
            }
          });
        }
        return this;
      }
    }
  })();

  manager.initGlobal()
  .initTable();

});
