define(['jquery', 'validate', 'utils', 'jquerySerializeObject', 'select2',
      'uniform'],
    function ($, validate, utils) {
      const moduleUrl='merchant/supplier';
      const moduleName = 'merchantSupplier';
      const detailUrl = id => window.host + `/${moduleUrl}/${id}`;
      const saveUrl = window.host + `/${moduleUrl}/edit`;
      const MERCHANT_LIST_URL = '/sellerpc/org/listMerchant';
      const SUPPLIER_LIST_URL = window.host + '/supplier/list?type=override';

      const addButton = $(`#add-${moduleName}`);
      const supplierList = $("#supplierList");
      const merchantList = $("#merchantList");
      const form = $(`#${moduleName}-form`);
      const modal = $(`#modal_add_${moduleName}`);
      const submitButton = $(`#submit-${moduleName}`);
      let updateTable = '';
      let isSubmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();
        addButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitForm = function (form) {
        if (isSubmiting === true) {
          return;
        }
        if (!form.id) {
          delete form.id;
        }
        utils.postAjaxJson(saveUrl, form, date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        });
        isSubmiting = false;
      };

      const edit = function (id) {
        $.get(detailUrl(id)).done(date => {
          let obj = date.data;
          merchantList.val(obj.merchantId);
          merchantList.trigger('change');
          supplierList.val(obj.supplierId);
          supplierList.trigger('change');
          form.find("input[name=id]").prop("value",obj.id);
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              form[0].reset();
              merchantList.val(null).trigger('change');
              supplierList.val(null).trigger('change');
              form.validate().resetForm();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = form.valid();
              if (result) {
                let obj = form.serializeObject();
                console.log("success valid");
                obj.merchantId = merchantList.val();
                obj.supplierId = supplierList.val();
                submitForm(obj);
              }
            });
            return this;
          },
          initValidate: function () {

            form.validate({
              rules: {
                supplierId : {
                  required: true,
                },
                merchantId: {
                  required: true,
                },
              },
              messages: {
                name: {
                  required: "请选择后台账号",
                },
                code: {
                  required: "请选择供应商",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
          initSelect: function () {
            $.get(MERCHANT_LIST_URL, {
              ifPage:false
            }).done(function (data) {
              let results = [];
              let list = data.data.list;
              for (let i = 0; i < list.length; i++) {
                let user = {
                  id: list[i].id,
                  text: list[i].name,
                };
                results.push(user);
              }
              //初始化select2
              merchantList.select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            }).fail(function () {
              utils.tools.alert("获取账户数据出错");
            });
            $.get(SUPPLIER_LIST_URL, {
              pageable:false
            }).done(function (data) {
              let results = [];
              let list = data.data.list;
              for (let i = 0; i < list.length; i++) {
                let user = {
                  id: list[i].id,
                  text: list[i].name,
                };
                results.push(user);
              }
              //初始化select2
              supplierList.select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            }).fail(function () {
              utils.tools.alert("获取供应商数据出错");
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initValidate().initSubmit().initSelect().resetForm();

      return {
        edit: edit,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });
