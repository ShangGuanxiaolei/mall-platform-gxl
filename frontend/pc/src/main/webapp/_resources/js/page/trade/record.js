/**
 * Created by chh on 16/12/13.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

    var $listUrl = window.host + "/trade/recordList";

    var $rowId = '';

    var $keyword = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 初始化日期控件 **/
    var options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
            format: 'YYYY/MM/DD',
            separator: ' - ',
            applyLabel: '确定',
            fromLabel: '开始日期:',
            toLabel: '结束日期:',
            cancelLabel: '清空',
            weekLabel: 'W',
            customRangeLabel: '日期范围',
            daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
            monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
            firstDay: 6
        },
        ranges: {
            '今天': [moment(), moment()],
            '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '最近7天': [moment().subtract(6, 'days'), moment()],
            '最近15天': [moment().subtract(15, 'days'), moment()],
            '本月': [moment().startOf('month'), moment().endOf('month')],
            '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
    };

    var $dateRangeBasic = $('.daterange-basic');
    $dateRangeBasic.daterangepicker(options, function (start, end) {
        if (start._isValid && end._isValid) {
            $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        } else {
            $dateRangeBasic.val('');
        }
    });

    var $dateRangeOrder = $('.daterange-order');
    $dateRangeOrder.daterangepicker(options, function (start, end) {
        if (start._isValid && end._isValid) {
            $dateRangeOrder.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        } else {
            $dateRangeOrder.val('');
        }
    });

    /** 回调 **/
    $('.daterange-order').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.order_startDate = picker.startDate;
        options.order_endDate   = picker.endDate;
    });
    /** 回调 **/
    $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.startDate = picker.startDate;
        options.endDate   = picker.endDate;
    });

    /**
     * 清空按钮清空选框
     */
    $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        $dateRangeBasic.val('');
    });

    /**
     * 清空按钮清空选框
     */
    $dateRangeOrder.on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        $dateRangeOrder.val('');
    });

    // 时间区间默认为空
    options.startDate = '';
    options.endDate = '';
    $('.daterange-basic').val('');
    options.order_startDate = '';
    options.order_endDate = '';
    $('.daterange-order').val('');

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    var $datatables = $('#guiderUserTable').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $("#orderNo").val(),
                startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "60px",
                orderable: false,
                title: '类型',
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outpayType)
                    {
                        case 'ORDER':
                            status = '订单支付';
                            break;
                        case 'WITHDRAW':
                            status = '会员提现';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '单号',
                data: "billNo",
                width: "120px",
                orderable: false,
                name:"billNo"
            },{
                title: '金额',
                data: "amount",
                width: "100px",
                orderable: false,
                name:"amount"
            },{
                title: '支付方式',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outId)
                    {
                        case 'WEIXIN':
                            status = '微信';
                            break;
                        case 'WEIXIN_APP':
                            status = '微信';
                            break;
                        case 'ALIPAY':
                            status = '支付宝';
                            break;
                        case 'ALIPAY_APP':
                            status = '支付宝';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '支付单号',
                data: "tradeNo",
                width: "100px",
                orderable: false,
                name:"tradeNo"
            }, {
                title: '时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.updatedAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"updatedAt"
            },{
                title: '操作',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_agent_price"><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_agent_price"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $orderdatatables = $('#orderTable').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $("#order_orderNo").val(),
                startDate: $dateRangeOrder.val() !== '' && options.order_startDate !== '' ? options.order_startDate.format('YYYY-MM-DD') : '',
                endDate: $dateRangeOrder.val() !== '' && options.order_endDate !== '' ? options.order_endDate.format('YYYY-MM-DD') : '',
                outpayType: 'ORDER',
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "60px",
                orderable: false,
                title: '类型',
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outpayType)
                    {
                        case 'ORDER':
                            status = '订单支付';
                            break;
                        case 'WITHDRAW':
                            status = '会员提现';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '单号',
                data: "billNo",
                width: "120px",
                orderable: false,
                name:"billNo"
            },{
                title: '金额',
                data: "amount",
                width: "100px",
                orderable: false,
                name:"amount"
            },{
                title: '支付方式',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outId)
                    {
                        case 'WEIXIN':
                            status = '微信';
                            break;
                        case 'WEIXIN_APP':
                            status = '微信';
                            break;
                        case 'ALIPAY':
                            status = '支付宝';
                            break;
                        case 'ALIPAY_APP':
                            status = '支付宝';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '支付单号',
                data: "tradeNo",
                width: "100px",
                orderable: false,
                name:"tradeNo"
            }, {
                title: '时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.updatedAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"updatedAt"
            },{
                title: '操作',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_agent_price"><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_agent_price"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initOrderEvent();
        }
    });


    var $withdrawdatatables = $('#withdrawTable').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                outpayType: 'WITHDRAW',
                userName: $("#userName").val(),
                userPhone: $("#userPhone").val(),
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "60px",
                orderable: false,
                title: '类型',
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outpayType)
                    {
                        case 'ORDER':
                            status = '订单支付';
                            break;
                        case 'WITHDRAW':
                            status = '会员提现';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '金额',
                data: "amount",
                width: "100px",
                orderable: false,
                name:"amount"
            },{
                title: '支付方式',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.outId)
                    {
                        case 'WEIXIN':
                            status = '微信';
                            break;
                        case 'WEIXIN_APP':
                            status = '微信';
                            break;
                        case 'ALIPAY':
                            status = '支付宝';
                            break;
                        case 'ALIPAY_APP':
                            status = '支付宝';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                title: '支付单号',
                data: "tradeNo",
                width: "100px",
                orderable: false,
                name:"tradeNo"
            }, {
                title: '时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.updatedAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"updatedAt"
            },{
                title: '操作',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_agent_price"><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_agent_price"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initWithdrawEvent();
        }
    });

    function initOrderEvent(){

    }

    function initWithdrawEvent(){

    }

    function initEvent(){

    }

    $(".btn-search").on('click', function() {
       $datatables.search('').draw();
    });

    $(".btn-order-search").on('click', function() {
        $orderdatatables.search('').draw();
    });

    $(".btn-withdraw-search").on('click', function() {
        $withdrawdatatables.search('').draw();
    });


});