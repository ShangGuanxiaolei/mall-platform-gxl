define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/twitter/getSummary';
    var data = {
    };
    // 本周订单,推客,交易额,佣金数
    var amount;
    var commission;
    var order;
    var twitter;
    var member;
    var account;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
            // 取推客概况中的今日数据
            $("#day_amount").html(result.data.info.amount);
            $("#day_commission").html(result.data.info.commission);
            $("#day_order").html(result.data.info.order);
            $("#day_twitter").html(result.data.info.twitter);

            // 取本周订单,推客,交易额,佣金数
            amount = result.data.nums.amount;
            commission = result.data.nums.commission;
            order = result.data.nums.order;
            twitter = result.data.nums.twitter;

            // Initialize charts
            // ------------------------------
            member = echarts.init(document.getElementById('member'), limitless);
            account = echarts.init(document.getElementById('account'), limitless);

            // Charts setup
            // ------------------------------

            //
            // member options
            //

            member_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['订单数', '推客数']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '订单数',
                        type: 'line',
                        data: order
                    },
                    {
                        name: '推客数',
                        type: 'line',
                        data: twitter
                    }
                ]
            };

            //
            // account options
            //
            account_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['交易额','佣金']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '交易额',
                        type: 'line',
                        data: amount
                    },
                    {
                        name: '佣金',
                        type: 'line',
                        data: commission
                    }
                ]
            };



            // Apply options
            // ------------------------------

            member.setOption(member_options);
            account.setOption(account_options);

        }
    });


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            account.resize();
            member.resize();
        }, 200);
    }

});
