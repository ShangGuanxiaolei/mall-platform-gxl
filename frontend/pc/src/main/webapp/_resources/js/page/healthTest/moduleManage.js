define(['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils) {

    const prefix = window.host + '/healthTest';
    const treeList = prefix + '/module/jsTree';
    const saveUrl = prefix + '/module/save';
    const listUrl = prefix + '/module/list';
    const deleteUrl = prefix + '/module/delete';
    const viewUrl = prefix + '/module/view';

    /* 当前选中节点 */
    var selected = {
        'id': '0',
        'parent': null
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无数据"
        }
    });

    eventBind();

    const moduleTree = $('#module_tree');
    moduleTree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': treeList,
                'data': function (node) {
                    if (node === null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        moduleTree.jstree("select_node", "ul > li:first");
        var selectedNode = moduleTree.jstree("get_selected");
        moduleTree.jstree('open_node', selectedNode, false, true);
    }).on('delete_node.jstree', function (event, data) {
        deleteModule(data.node.id);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    }).on('create_node.jstree', function (event, data) {
        $dataTables.search('').draw();
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        selected = data.node;
        $dataTables.search('').draw();
    }

    /** 初始化表格数据 **/
    var $dataTables = $('#xquark_module_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                parentId: selected.id,
                pageable: true
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '50px',
                orderable: true,
                name: 'name'
            },
            {
                width: '20px',
                orderable: true,
                render: function (data, type, row) {
                    return row.sortNo;
                }
            },
            {
                width: '100px',
                orderable: false,
                render: function (data, type, row) {
                    return row.isLeaf ? '是' : '否';
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt == null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var staticName = row.staticName;
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" rowId="' + row.id + '" parent_id="' + row.parentId + '" fid="edit_module"><i class="icon-pencil7"></i>编辑</a>';
                    // 大标题菜单以及基础测试下不能添加题目
                    if (!(staticName)) {
                        html += '<a href="javascript:void(0);" style="margin-left: 10px" class="edit_result" rowId="' + row.id + '" fid="edit_result"><i class="icon-pencil7"></i>设置回答</a>';
                    }
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" parent_id="' + row.parentId + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    /**
     * 表格事件绑定
     */
    function initEvent() {
        $('body').unbind('click');

        /** 表格的编辑事件 **/
        $('.edit').on('click', function () {
            var id = $(this).attr("rowId");
            showUpdate(id);
        });

        $('.edit_result').on('click', function () {
            var id = $(this).attr("rowId");
            location.href = '/sellerpc/healthTest/module/result?id=' + id;
        });

        /** 点击删除部门弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId = $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("id");
                var tree = $('#module_tree').jstree(true);
                // 通过jstree的删除事件删除并刷新表格
                tree.delete_node(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") === "popover") {
                target.popover("toggle");
            }
        });
    }

    /**
     * 全局事件绑定
     */
    function eventBind() {

        /** 添加菜单 **/
        $('#add_module').on('click', function () {
            $('#id').val('');
            $('#parent_id').val(selected.id);
            $('#name').val('');
            $('#iconName').val('');
            $('#type').val('SUM');
            $('#requiredSex').val('0');
            $('#required').val('0');
            $('#modal_module_save').modal('show');
        });

        $('#update_module').on('click', function () {
            var url = window.host + '/cache/module';
            utils.postAjax(url, null, function (res) {
                if (res.errorCode === 200 && res.data) {
                    utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                } else {
                    utils.tools.alert(res.moreInfo);
                }
            });
        });

        $('#delete_module').on('click', function () {
            var id = selected.id;
            var tree = $('#module_tree').jstree(true);
            if (id === null || id === '0') {
                utils.tools.alert('请先在左侧选择菜单');
                return;
            }
            utils.tools.confirm("将同时删除菜单下所有子菜单，确定删除吗?", function () {
                tree.delete_node(selected);
            }, function () {

            });
        });

        $('#saveBtn_module').on('click', function () {
            var id = $('#id').val();
            var parent_id = $('#parent_id').val();
            var name = $('#name').val();
            var iconName = $('#iconName').val();
            var type = $('#type').val();
            var requiredSex = $('#requiredSex').val();
            var required = $('#required').val();
            if (!name || name === '') {
                utils.tools.alert("请输入菜单名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                id: id,
                parentId: parent_id,
                name: name,
                icon: iconName,
                type: type,
                requiredSex: requiredSex,
                required: required === '1'
            };
            utils.postAjaxWithBlock($(document), saveUrl, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            var data = res.data;
                            if (data && typeof data !== 'undefined') {
                                utils.tools.alert("操作成功");
                                $('#modal_module_save').modal('hide');
                                var node = {
                                    id: data.id,
                                    text: data.name,
                                    icon: data.icon
                                };
                                moduleTree.jstree().create_node(selected, node, 'last');
                                // window.location.href = window.originalHost + '/module/manage';
                            } else {
                                utils.tools.alert('操作失败', { timer: 1200, type: 'warning' });
                            }

                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

    }

    /*手动删除*/
    function deleteModule(id) {
        utils.postAjax(deleteUrl, {'id': id}, function (res) {
            if (res === -1) {
                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
            }
            if (typeof res === 'object') {
                if (res.errorCode === 200) {
                    if (res.data) {
                        utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                        $dataTables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败", {timer: 1200, type: 'warning'});
                    }
                } else {
                    if (res.moreInfo) {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    } else {
                        utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                    }
                }
            }
        });
    }

    /**
     * 显示更新弹窗
     * @param id
     */
    function showUpdate(id) {
        /* 查询要修改的数据 */
        $.ajax({
            url: viewUrl,
            type: 'POST',
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                if (data.errorCode === 200) {
                    var module = data.data;
                    $('#id').val(module.id);
                    $('#name').val(module.name);
                    $('#iconName').val(module.icon);
                    $('#type').val(module.type);
                    $('#requiredSex').val(module.requiredSex);
                    $('#required').val(module.required ? '1' : '0');
                    $('#modal_module_save').modal('show');
                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tools.goLogin();
                } else {
                    utils.tools.alert('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    }

});
