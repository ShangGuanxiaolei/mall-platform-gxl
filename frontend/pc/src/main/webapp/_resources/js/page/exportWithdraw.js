define(['jquery', 'utils/selectInitializer'], function ($, select) {
  select.init({
    dom: '.select2-platform',
    provider: function () {
      return `<option value="H">汉德森</option>
              <option value="V">viviLife</option>
              <option value="E">汉薇</option>`
    }
  })
});
