/**
 * Created by jitre on 18/2/8.
 */
define(['jquery', 'utils', 'datatables', 'select2', 'daterangepicker',
      'hotSearch/add'],
    function ($, utils, datatables, select2, daterangepicker, add) {
      const NOT_FOUND = "/_resources/images/404.png";
      const moduleName = 'product';
      const moduleDisplayName = '词条';
      const listUrl = window.host + `/${moduleName}/listHotSearchKeys2?type=override`;
      const batchDeleteUrl = ids => window.host
          + `/${moduleName}/hotSearchKeys/remove?ids=${ids}`;
      const deleteUrl = id => window.host + `/${moduleName}/hotSearchKeys/remove?ids=${id}`;
      const addUrl = window.host + `/${moduleName}/edit`;

      const checkAll = '#checkAllGoods';
      const searchButton = $('#search');
      const publishButton = $("#publishCase");
      const wordModal = $("#modal_add_word");
      const addHotButton = $("#add-hotSearch");
      const addWordNum = $("#add-word-num");
      const batchDeleteButton = $("#batchDel");
      const wordSubmitButton = $("#submit-hotSearch2");
      const form2 = $("#hotSearch-form2");
      const tableSource = '#guiderUserTable';
      var $datatables = '';

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          search: '<span>筛选:</span> _INPUT_',
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      /**
       *删除
       */
      const deleteComponent = function (pId) {
        $.get(deleteUrl(pId)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       * 批量删除
       * @param ids
       */
      const batchDelete = function (ids) {
        $.get(batchDeleteUrl(ids)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       *得到当前选中的行数据
       */
      const getTableContent = function () {
        let selectRows = [];
        for (let i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
          let value = {};
          let checkvalue = $("input[name='checkGoods']:checked")[i];
          value.id = $(checkvalue).attr("rowid");
          selectRows.push(value);
        }
        return selectRows;
      };

      /**
       * 更新表格
       */
      const updateTable = function () {
        let keyword = $.trim($("#keyWord").val());
        $datatables.search(keyword).draw();
      };

        const wordSubmitForm = function (form) {
            var wordNum = form.name;
            $.ajax({
                "url": window.host + "/product/updateDisplayLimit",
                "data": "limit=" + wordNum,
                "type": "get",
                "dataType": "json",
                "success": function (obj) {
                    wordModal.modal("hide");
                    utils.tools.success("操作成功!");
                },
                "error": function (obj) {
                    utils.tools.error("操作失败!");
                }
            });
        };

      const bindEvent = function () {
        /**
         *监听批删除
         */
        batchDeleteButton.on('click', function () {
          let updateds = getTableContent();
          if (updateds.length === 0) {
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return;
          }
          let ids = '';
          $.each(updateds, function (index, row) {
            if (ids !== '') {
              ids += ',';
            }
            ids += row.id;
          });
          batchDelete(ids);
        });

        /**
         * 监听搜索
         */
        searchButton.on("click", function () {
          updateTable();
        });

          addHotButton.on("click",function(){
              window.location.href = "/sellerpc/mall/addHot?type=0&id=999";
          });

          addWordNum.on("click",function(){
            $.ajax({
              "url": window.host + "/product/getHotLimit",
              "data": "",
              "type": "get",
              "dataType": "json",
              "success": function (obj) {
                $("#wordNum").val(obj.data);

                wordModal.modal("show");
              },
              "error": function (obj) {
                  utils.tools.error("获取词条数失败!");
              }
            });
          });

          wordSubmitButton.on("click", () => {
              let result = form2.valid();
              if (result) {
                  let obj = form2.serializeObject();
                  wordSubmitForm(obj);
              }
          });
      };

        function switchStatus(id, status) {
          $.ajax({
            "url": window.host + "/product/updateKeyState",
            "data": "id=" + id + "&state=" + status,
            "type": "get",
            "dataType": "json",
            "success": function (obj) {
                updateTable();
                utils.tools.success("操作成功!");
            },
            "error": function (obj) {
                utils.tools.error("操作失败!");
            }
          });
        }

      const bindDataTableEvent = function () {
        /**
         * 监听删除按钮的点击
         */
        $(".del").off("click").on('click', function () {
          let rowId = $(this).attr("rowId");
          utils.tools.confirm('确认删除吗？', function () {
            // deleteComponent(rowId);
              batchDelete(rowId);
          }, function () {

          });
        });

        /**
         * 监听全选
         */
        $(checkAll).on('click', function () {
          $("input[name='checkGoods']").prop("checked",
              $(this).prop("checked"));
        });

        /**
         * 监听编辑按钮的点积
         */
        $(".edit").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          add.edit(id);
        });

        $(".used").off("click").on("click",function(){
            let id = $(this).attr("rowId");
            let status = $(this).attr("data-status");

            switchStatus(id,status);
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          initDataTable() {
            $datatables = utils.createDataTable(tableSource, {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              ajax: function (data, callback, settings) {
                  var key = null;
                  if(data.search.value && data.search.value.length > 0){
                      key = data.search.value
                  }

                $.get(listUrl, {
                  size: data.length,
                  page: (data.start / data.length),
                  key: key,
                  pageable: true,
                }, function (res) {
                    var list = res.data.list;
                    if(!list){
                        list = [];
                    }

                  callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                  });
                });
              },
              columns: [
                {
                  title: '<label class="checkbox"><input id="checkAllGoods" name="checkAllGoods" type="checkbox" class="styled"></label>',
                  orderable: false,
                  render: function (data, type, row) {
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                        + row.id + '" value="' + row.id + '"></label>';
                  }
                },
                {
                  title: `${moduleDisplayName}名称`,
                  data: 'hkey',
                  name: 'hkey',
                  sortable: false,
                    width:'450px'
                },
                {
                  title: '发布时间',
                  orderable: false,
                  render: function (data, type, row) {
                    let cDate = parseInt(row.updatedAt);
                    let d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  },
                  name: "updatedAt"
                }, {
                      sortable: false,
                      title: '管理',
                      render: function (data, type, row) {
                          let html = '';
                          html += `<input name="open_${row.id}" rowId=${row.id} type="radio" `;
                          if(row.status == 1){
                            html += `checked="checked"`;
                          }
                          html += ` class="used" data-status="1"/>启用<span class="whiteSapce"></span>`;
                          html += `<input name="open_${row.id}" rowId=${row.id} type="radio" `;
                          if(row.status == 0){
                              html += `checked="checked"`;
                          }
                          html += ` class="used" data-status="0"/>关闭<span class="whiteSapce2"></span>`;
                          html += `<a href="javascript:void(0);" class="edit role_check_table" rowId=${row.id} fid="edit_audit_order"><i class="icon-pencil7"></i>编辑</a>`;
                          html += `<a href="javascript:void(0);" class="del role_check_table" rowId=${row.id} fid="delete_audit_order"><i class="icon-trash"></i>删除</a>`;
                          return html;
                      }
                  }],
              drawCallback: bindDataTableEvent,
            });
            return this;
          },
          initCallBack: function () {
            add.setUpdateFunc(updateTable);
            return this;
          }
        }
      })();

      eventInitialization.init().initDataTable().initCallBack();

    });