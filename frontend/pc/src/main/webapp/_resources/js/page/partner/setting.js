define(['jquery', 'utils', 'form/validate', 'switch', 'datatables', 'blockui','bootbox', 'select2'],
    function($, utils, validate, bswitch, datatabels, blockui, bootbox,select2) {


        var $productlistUrl = window.host + "/product/list";

        var $shopId = null;

        var $rowId = '';

        var $category = '';

        var $orders = ['price','amount','sales','onsale'];

        var $order = '';

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        // 商品佣金列表
        var $datatables = $('#productsCommission').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/productList", {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<img class="goods-image" src="'+row.productImg+'" />';
                    }
                },
                {
                    title: '商品信息',
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"productName"
                },
                {
                    title: '商品价格',
                    data: "productPrice",
                    width: "80px",
                    orderable: false,
                    name:"productPrice",
                    render: function(data, type, row){
                        return '<font color = "#53a000">￥' + row.productPrice + '</font>';
                    }
                },
                {
                    width:90,
                    name: 'rate',
                    title: '团队分红',
                    render: function(data, type, row){
                        return row.teamDe.rate +'%';
                    }
                },
                {
                    width:90,
                    name: 'rate',
                    title: '股东分红',
                    render: function(data, type, row){
                        return row.shareholderDe.rate +'%';
                    }
                },
                {
                    width:90,
                    name: 'rate',
                    title: '平台分红',
                    render: function(data, type, row){
                        return row.platformDe.rate +'%';
                    }
                },
                {
                    width:75,
                    title: '创建时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    title: '更新时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.updatedAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.updatedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="leveledit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="leveldel role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });


        function initEvent() {
            $(".leveledit").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/partner/getProductCommission/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;

                            $("#commission_productId").val(role.productId);
                            $("#commission_productName").html(role.productName);
                            $("#commission_productPrice").html(role.productPrice);
                            $("#commission_productImg").attr("src",role.productImg);

                            $("#commission_Id").val(role.id);
                            $("#commission_team_id").val(role.teamDe.id);
                            $("#commission_team_rate").val(role.teamDe.rate);

                            $("#commission_platform_id").val(role.platformDe.id);
                            $("#commission_platform_rate").val(role.platformDe.rate);

                            $("#commission_shareholder_id").val(role.shareholderDe.id);
                            $("#commission_shareholder_rate").val(role.shareholderDe.rate);


                            $("#modal_product_commission").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteProductCommission(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        function deleteProductCommission(id) {
            $.ajax({
                url: host + '/partner/deleteProductCommission/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增商品佣金
        $(document).on('click', '.addProduct', function() {
            /** 初始化选择框控件 **/
            $('.select').select2({
                minimumResultsForSearch: Infinity,
            });
            $("#modal_select_products").modal("show");
        });


        var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get($productlistUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    order: function () {
                        if($order != ''){
                            return $order;
                        }else{
                            var _index = data.order[0].column;
                            if ( _index < 4){
                                return '';
                            } else {
                                return $orders[_index - 4];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir :'asc',
                    category : $category,
                    isGroupon : ''
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.categoryTotal,
                        iTotalDisplayRecords:res.data.categoryTotal
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                    }
                },
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.status)
                        {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "price",
                    width: "50px",
                    orderable: true,
                    name:"price"
                }, {
                    data: "amount",
                    orderable: true,
                    width: "50px",
                    name:"amount"
                },
                {
                    data: "sales",
                    orderable: true,
                    width: "50px",
                    name:"sales"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"onsaleAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'+row.id+'" productImg="'+row.imgUrl+'" productPrice="'+row.price+'" productName="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectProductEvent();
            }
        });

        function initSelectProductEvent(){
            $(".selectproduct").on("click",function(){
                var productId =  $(this).attr("rowId");
                var productName =  $(this).attr("productName");
                var productImg =  $(this).attr("productImg");
                var productPrice =  $(this).attr("productPrice");
                // 查询是否已经有该商品已经设置过分佣设置
                $.ajax({
                    url: window.host + '/partner/checkProductCommission',
                    type: 'POST',
                    dataType: 'json',
                    data: {'productId':productId},
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            if(role){
                                utils.tools.alert("该商品已经设置过分佣!", {timer: 3000, type: 'warning'});
                                return;
                            }else{
                                $("#commission_productId").val(productId);
                                $("#commission_productName").html(productName);
                                $("#commission_productPrice").html(productPrice);
                                $("#commission_productImg").attr("src",productImg);


                                $("#commission_Id").val('');
                                $("#commission_team_id").val('');
                                $("#commission_team_rate").val('');

                                $("#commission_platform_id").val('');
                                $("#commission_platform_rate").val('');

                                $("#commission_shareholder_id").val('');
                                $("#commission_shareholder_rate").val('');

                                $("#modal_product_commission").modal("show");
                            }
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            });
        }

        $('body').on('change','select[name="categoryType"]', function(event) {
            $productlistUrl = window.host + "/product/list";
            $category = $(this).val();
            $selectproductdatatables.search('').draw();
        });

        $(".btn-search-products").on('click', function() {
            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null){
                $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
                $selectproductdatatables.search( keyword ).draw();
            }else if (keyword == '' || keyword.length == 0 ){
                $productlistUrl = window.host + "/product/list";
                $selectproductdatatables.search('').draw();
            }
        });

        // 保存商品佣金设置
        $(".btn-save-commission").on('click', function() {
            var commission_productId = $("#commission_productId").val();
            var commission_Id = $("#commission_Id").val();

            var commission_team_id = $("#commission_team_id").val();
            var commission_team_rate = $("#commission_team_rate").val();
            var commission_platform_id = $("#commission_platform_id").val();
            var commission_platform_rate = $("#commission_platform_rate").val();
            var commission_shareholder_id = $("#commission_shareholder_id").val();
            var commission_shareholder_rate = $("#commission_shareholder_rate").val();

            if(!commission_team_rate || commission_team_rate == ''){
                utils.tools.alert("请输入团队分红比例!", {timer: 1200, type: 'warning'});
                return;
            }else if(!commission_platform_rate || commission_platform_rate == ''){
                utils.tools.alert("请输入平台分红比例!", {timer: 1200, type: 'warning'});
                return;
            }else if(!commission_shareholder_rate || commission_shareholder_rate == ''){
                utils.tools.alert("请输入股东分红比例!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                id: commission_Id,
                productId: commission_productId,
                teamId: commission_team_id,
                teamRate: commission_team_rate,
                shareholderId: commission_shareholder_id,
                shareholderRate: commission_shareholder_rate,
                platformId: commission_platform_id,
                platformRate: commission_platform_rate
            };
            $.ajax({
                url: host + '/partner/saveProductCommission',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_product_commission").modal('hide');
                        $("#modal_select_products").modal('hide');
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        });

        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var partnerSettingForm = {
            form: $('#partnerSettingForm'),

            teamId: $("#partnerSettingForm input[name=team_id]"),
            teamName: $("#partnerSettingForm input[name=team_name]"),
            teamRate: $("#partnerSettingForm input[name=team_rate]"),

            shareholderId: $("#partnerSettingForm input[name=shareholder_id]"),
            shareholderName: $("#partnerSettingForm input[name=shareholder_name]"),
            shareholderRate: $("#partnerSettingForm input[name=shareholder_rate]"),

            platformId: $("#partnerSettingForm input[name=platform_id]"),
            platformName: $("#partnerSettingForm input[name=platform_name]"),
            platformRate: $("#partnerSettingForm input[name=platform_rate]"),


            savePartnerSettingBtn: $('#savePartnerSettingBtn'),
            url: window.originalHost + '/partner/setting/save',

            rules: {

            },

            save: function() {

                var data = {
                    teamId: this.teamId.val(),
                    teamName: this.teamName.val(),
                    teamRate: this.teamRate.val(),
                    shareholderId: this.shareholderId.val(),
                    shareholderName: this.shareholderName.val(),
                    shareholderRate: this.shareholderRate.val(),
                    platformId: this.platformId.val(),
                    platformName: this.platformName.val(),
                    platformRate: this.platformRate.val()
                };
                utils.postAjaxWithBlock($(document), this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                                //location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            },

            init: function() {

                $(this.savePartnerSettingBtn).on('click',function() {
                    $(this.form).submit();
                });
                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        partnerSettingForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        partnerSettingForm.init();
    });