/**
 * 封装表格控件
 */
define(['jquery', 'datatables'], function ($) {

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const defaultOption = {
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: true,
    rowId: 'id',
    select: {
      style: 'multi'
    }
  };

  /**
   * 创建一个表格对象
   * @param params.url 获取数据url
   * @param params.dom 需实例化的dom节点
   * @param params.columns 表格列数据
   * @param params.drawBack 表格callBack
   * @param params.options 覆盖默认配置
   * @constructor
   */
  function DataTable(params) {
    var tableOptions = params.options;
    if (!tableOptions) {tableOptions = {}}
    tableOptions = $.extend({}, defaultOption, tableOptions);
    // 直接在params中提供columns
    if (!tableOptions.columns) {
      tableOptions.columns = params.columns;
    }
    if (params.drawBack) {
      tableOptions.drawCallback = params.drawBack;
    }
    tableOptions.ajax = function (data, callback) {
      $.get(params.url, {
        size: data.length,
        page: data.start / data.length,
        pageable: true
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo,
              {timer: 1200, type: 'warning'});
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    };

    this.globalTable = $(params.dom).DataTable(tableOptions);

    /**
     * 绑定刷新方法
     */
    this.refresh = function () {
      this.globalTable.search('').draw();
    }

  }

  return {
    newTable: function (params) {
      return new DataTable(params);
    }
  }

});
