define(['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2'],
    function ($, utils, datatabels, blockui, bootbox, select2) {

        var $proId = utils.tools.request('pId');  //商品Id

        //参数三种情况【weight按重量，number按件数，volume按体积】
        function shipment(thead) {
            //重量表头
            this.thead = {
                "weight": '<tr class=""><th style="width:200px">可配送至</th><th>首重(g)</th><th>运费(元)</th><th>续重(g)</th><th>续费(元)</th><th style="width: 36px">管理</th></tr>',
                "number": '<tr class=""><th style="width:200px">可配送至</th><th>首件(个)</th><th>运费(元)</th><th>续件(个)</th><th>续费(元)</th><th style="width: 36px">管理</th></tr>',
                "volume": '<tr class=""><th style="width:200px">可配送至</th><th>首件(立方米)</th><th>运费(元)</th><th>续件(个)</th><th>续件(立方米)</th><th style="width: 36px">管理</th></tr>'
            };

            var html = '<div class="form-group">'
                + '<label class="col-sm-2 control-label text-right">配送区域设置</label>'
                + '<div class="col-sm-8">'
                + '<div class="postage-area" id="postage-area">'
                + '<table class="table table-bordered table-condensed table-middle">'
                + '<thead>';
            switch (thead) {
                case 'weight':
                    html += this.thead.weight;
                    break;
                case 'number':
                    html += this.thead.number;
                    break;
                case 'volume':
                    html += this.thead.volume;
                    break;
                default:
                    html += this.thead.weight;
            }

            html += '</thead>'
                + '<tbody>'
                + '<tr>'
                + '<td class="area-text">默认运费</td>'
                + '<td class="text-center"><input type="number" name="first" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="text" name="firstFee" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="number" name="second" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="text" name="secondFee" class="w90 form-control"></td>'
                + '<td></td>'
                + '</tr>'
                + '</tbody>'
                + '</table>'
                + '<div class="mt5"><a class="btn btn-sm btn-primary" id="add_shipment" data-toggle="modal" data-target="#modal_region"><i class="icon-plus3"></i> 新增配送区域</a></div>'
                + '</div>'
                + '</div>'
                + '</div>';
            return html;
        }

        //按重量
        $("#woman").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("weight");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.weight;
                    };
                    var theadWeight = new shipment("weight");
                    $("#postage-area thead").html(theadWeight.theadTem());
                }
            }
        });

        //按件数
        $("#goodsQuantity").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("number");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.number;
                    };
                    var theadNumber = new shipment("number");
                    $("#postage-area thead").html(theadNumber.theadTem());
                }
            }
        });

        //按件数
        $("#sex").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("volume");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.volume;
                    };
                    var theadVolume = new shipment("volume");
                    $("#postage-area thead").html(theadVolume.theadTem());
                }
            }
        });

        //新增配送区域
        $(document.body).on("click", "#add_shipment", function () {
            initRegion(this);
        });

        function initRegion(dom) {
            var thisDom = $($(dom).attr("data-target"));
            var html = '<ul class="areas-body clearfix">';

            var editOperation = false;
            //编辑操作
            if ($(dom).hasClass('editProvince')) {
                editOperation = true;
                var checkedProvs = $(dom).closest("tr").children(".area-text").text().trim();
                $(thisDom.find('.modal-footer button.btn-ok')[0]).attr('provRow', checkedProvs);
            }

            $.each(province, function (i, v) {
                html += '<li class="area-item">';
                html += '<div class=""><label><input type="checkbox" zone-id="' + province[i].id + '" name="province" class="styled" ' + (editOperation && checkedProvs.indexOf(province[i].name) >= 0 ? 'checked=true' : '') + '/>' + province[i].name + '</label></div>';
                html += '</div></li>';
            });
            html += '</ul>';
            thisDom.find('.modal-body').html(html);
        }

        //编辑当前行配送区域
        $(document.body).on("click", "#postage-area .editProvince", function () {
            initRegion(this);
        });

        //确定按钮
        $(document.body).on("click", ".modal-footer button.btn-ok", function () {
            var provinceStr = '';
            var zoneIds = [];
            //遍历每个checkbox，把选中的提取出来
            $.each($($("#add_shipment").attr("data-target")).find(".area-item input[type=checkbox]"), function (index, value) {
                var _this = $(this);
                if (_this.is(":checked")) {
                    zoneIds.push(_this.attr('zone-id'));
                    provinceStr += (provinceStr == '' ?  '' : ',') +  _this.parent().text().trim();
                }
            });
            console.log(provinceStr + ':' + zoneIds);
            var a = 'this';
            var modalBtnOk = this;
            var row = '<td class="area-text">' + provinceStr + '</td><td class="text-center"><input type="number" name="first" class="w90 form-control"></td><td class="text-center"><input type="text" name="firstFee" class="w90 form-control"></td><td class="text-center"><input type="number" name="second" class="w90 form-control"></td><td class="text-center"><input type="text" name="secondFee" class="w90 form-control"></td><td><a class="pull-right editProvince" zone-ids="' + zoneIds + '" zone-names="' + provinceStr + '" data-toggle="modal" data-target="#modal_region"><i class="icon-pencil7"></i> 编辑</a></td>';
            var editOperation = false;
            var editrow = null;
            $("#postage-area tbody").find(".area-text").each(function() {
                if ($(this).text().trim() == $(modalBtnOk).attr('provRow')) {
                    editOperation = true;
                    editrow = this;
                }
            });

            if (editrow != null) {
                $(editrow).parent().html(row);
            } else {
                $("#postage-area tbody").append('<tr>' + row + '</tr>');
            }
        });


        //保存店铺邮费设置
        $("#savePostAgeSet").on('click', function() {
            //compose json request
            var customizedPostage = [];
            var flag = false;

            var name = $("#name").val();
            if(name == ''){
                alert('请输入运费模板名称');
                return;
            }

            $("#postage-area tbody").find(".editProvince").each(function(i,item) {
                var _this = $(this);
                var ids = $(this).attr('zone-ids').split(',');
                var names = $(this).attr('zone-names').split(',');

                var areas = [];
                $.each(ids, function(index, zoneId) {
                    areas.push({"id":zoneId, "name":names[index]});
                });

                var first = $(_this.closest("tr")).find("input[name='first']")[0].value;
                var firstFee = $(_this.closest("tr")).find("input[name='firstFee']")[0].value;
                var second = $(_this.closest("tr")).find("input[name='second']")[0].value;
                var secondFee = $(_this.closest("tr")).find("input[name='secondFee']")[0].value;
                if(first == '' || first == '0'){
                    alert('请输入首件数量');
                    flag = true;
                    return;
                }
                if(firstFee == ''){
                    alert('请输入首件运费');
                    flag = true;
                    return;
                }
                if(second == '' || second == '0'){
                    alert('请输入续件数量');
                    flag = true;
                    return;
                }
                if(secondFee == ''){
                    alert('请输入续件运费');
                    flag = true;
                    return;
                }

                customizedPostage.push({
                    "first": parseFloat(first),
                    "firstFee": parseFloat(firstFee),
                    "second": parseFloat(second),
                    "secondFee": parseFloat(secondFee),
                    "postage": parseFloat($(_this.closest("tr")).find("input[name='firstFee']")[0].value),
                    "areas": areas
                });
            });

            if(flag) return;

            var data = {
                "id": $proId,
                "name": $("#name").val(),
                "shopId": $("#shopId").val(),
                "type": $("input[name='valuation_type']:checked").val(),
                "freeShippingPrice" : parseFloat($("#freeShippingPrice").val()),
                "freeShippingFirstPay": $('#freeShippingFirstPay').is(':checked'),
                "postageStatus": true,
                "postage": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='firstFee']")[0].value),
                "defaultFirst": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='first']")[0].value),
                "defaultFirstFee": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='firstFee']")[0].value),
                "defaultSecond": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='second']")[0].value),
                "defaultSecondFee": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='secondFee']")[0].value),
                "customizedPostage": customizedPostage
            };

            console.log(data);

            var config = {
                "json":true
            };

            utils.postAjaxWithBlock($(document), window.host + '/shop/updatePostageSet', JSON.stringify(data), function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("邮费设置保存成功", {timer: 1200, type: 'success'});
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("邮费设置保存失败,请稍后再试", {timer: 1200});
                }
            }, config);
        });

        function fillSet(postSet) {
            //设置最低包邮价格
            if (postSet.freeShippingPrice != null) {
                $("#freeShippingPrice").val(postSet.freeShippingPrice);
            }
            if (postSet.freeShippingFirstPay != null) {
                $('#freeShippingFirstPay').prop('checked', postSet.freeShippingFirstPay);
            }
            if (postSet.name != null && postSet.name != '') {
                $("#name").val(postSet.name);
            }

            if (postSet.type != null && postSet.type != '') {
                $(":radio[name='valuation_type'][value='" + postSet.type + "']").prop("checked", "checked");
            }

            if (postSet.type == '1') {
                $("#woman").attr("checked", "checked").trigger('change');
            }

            var html = '';
            //设置默认运费

            var defaultRow = '<tr>'
            + '<td class="area-text">默认运费</td>'
            + '<td class="text-center"><input type="number" name="first" class="w90 form-control" value="' + postSet.defaultFirst + '"></td>'
            + '<td class="text-center"><input type="text" name="firstFee" class="w90 form-control" value="' + (postSet.defaultFirstFee == null ? '0' : postSet.defaultFirstFee) + '"></td>'
            + '<td class="text-center"><input type="number" name="second" class="w90 form-control" value="' + postSet.defaultSecond + '"></td>'
            + '<td class="text-center"><input type="text" name="secondFee" class="w90 form-control" value="' + (postSet.defaultSecondFee == null ? '0' : postSet.defaultSecondFee) + '"></td>'
            + '<td></td>'
            + '</tr>';

            html += defaultRow;

            //设置特定区域运费
            $.each(postSet.customizedPostage, function(index, item) {
                var zoneIds = [];
                var provinceStr = '';
                $.each(item.areas, function(j, area) {
                    zoneIds.push(area.id);
                    provinceStr += (provinceStr == '' ?  '' : ',') +  area.name.trim();
                });

                var row = '<tr><td class="area-text">' + provinceStr + '</td><td class="text-center">' +
                    '<input type="number" name="first" class="w90 form-control" value="' + item.first+ '"></td><td class="text-center">' +
                    '<input type="text" name="firstFee" class="w90 form-control" value="' + item.firstFee + '"></td><td class="text-center">' +
                    '<input type="number" name="second" class="w90 form-control" value="' + item.second + '"></td><td class="text-center">' +
                    '<input type="text" name="secondFee" class="w90 form-control" value="' + item.secondFee + '"></td><td>' +
                    '<a class="pull-right editProvince" zone-ids="' + zoneIds + '" zone-names="' + provinceStr + '" data-toggle="modal" data-target="#modal_region"><i class="icon-pencil7"></i> 编辑</a></td></tr>';
                html += row;
            });

            $("#postage-area tbody").html(html);
        }

        var province = [];
        function init() {

            $("#woman").attr("checked", "checked").trigger('change');
            utils.postAjaxWithBlock($(document), window.host+'/zone/1/children', [] , function(res) {
                if (typeof(res) === 'object') {
                    $.each(res.data, function(index, item) {
                        province.push(item);
                    });
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("无法获取区域信息,请稍后再试", {timer: 1200});
                }
            });

            utils.postAjaxWithBlock($(document), window.host + '/shop/getPostageSet?id=' + $proId, [] , function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            fillSet(res.data);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("获取当前配送方式失败", {timer: 1200});
                }
            });
        }

        init();
    });


