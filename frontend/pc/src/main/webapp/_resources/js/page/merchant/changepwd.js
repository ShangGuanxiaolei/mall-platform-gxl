/**
 * Created by quguangming on 16/5/25.
 */
define(['form/validate','utils','md5','jquerySerializeObject'], function(validate,utils,md5) {

    var $form = $(".change-pwd-form");

    var url = "/sellerpc/profile/changepwd"

    function submitForm(form){

        var data = $(form).serializeObject();

        data.oldpwd = CryptoJS.MD5(data.oldpwd).toString()
        data.newpwd = CryptoJS.MD5(data.newpwd).toString()

        //初始化商品分类信息
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data) {
                    utils.logout();
                    alert("密码修改成功,请重新登录!");
                    location.href = '/sellerpc/pc/login.html';
                } else {
                    utils.tools.alert("密码修改失败.请重新填写!");
                }
            },
            error: function (state) {
                utils.tools.alert("密码修改失败.请重新填写!");
            }
        });
    }

    var pwdForm = {
        rules:{
                oldpwd: {
                    required: true,
                    pattern: /^[a-zA-Z0-9_]{6,16}$/
                },
                newpwd: {
                    required: true,
                    pattern: /^[a-zA-Z0-9_]{6,16}$/
                },
                repwd:{
                    required: true,
                    equalTo: "#newpwd"
                }
        },
        messages:{
            oldpwd:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            newpwd:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            repwd:{
                required:"确认密码不能为空.",
                equalTo:  "密码不一致."
            },
        },
        focusCleanup: true,
        submitCallBack: function(form) {
            submitForm(form);
        }
    }
    validate($form,pwdForm);
})