define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/twitter/getSummary';
    var data = {
    };
    // 本周订单,推客,交易额,佣金数
    var amount;
    var commission;
    var order;
    var twitter;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
        }
    });


    var trade_detail = echarts.init(document.getElementById('trade_detail_lines'), limitless);
    var trade_detail_options = {

        // Setup grid
        grid: {
            x: 40,
            x2: 20,
            y: 35,
            y2: 25
        },

        // Add tooltip
        tooltip: {
            trigger: 'axis'
        },

        // Add legend
        legend: {
            data: ['下单买家数', '下单金额', '支付买家数', '支付金额'],
            textStyle: {
                fontSize: 16
            }
        },
        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            padding: 0,
            feature: {
                dataView: {
                    show: true,
                    readOnly: false,
                    title: '查看图表数据',
                    lang: ['查看图表数据', '关闭', '更新']
                },
                restore: {
                    show: true,
                    title: '刷新'
                },
                saveAsImage: {
                    show: true,
                    title: '另存为图片',
                    lang: ['保存']
                }
            }
        },
        // Enable drag recalculate
        calculable: true,

        // Hirozontal axis
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: [
                '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
            ]
        }],

        // Vertical axis
        yAxis: [{
            type: 'value'
        }],

        // Add series
        series: [
            {
                name: '下单买家数',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [120, 132, 101, 134, 90, 230, 210]
            },
            {
                name: '下单金额',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '支付买家数',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '支付金额',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [320, 332, 301, 334, 390, 330, 320]
            }
        ]
    };
    trade_detail.setOption(trade_detail_options);

    var trade_total = echarts.init(document.getElementById('trade_total_lines'), limitless);
    var trade_total_options = {

        // Add colors
        color: [
            'rgba(255, 69, 0, 0.5)',
            'rgba(255, 150, 0, 0.5)',
            'rgba(255, 200, 0, 0.5)',
            'rgba(155, 200, 50, 0.5)',
            'rgba(55, 200, 100, 0.5)'
        ],

        // Add title
        title: {
            show: false,
            text: 'Browser popularity',
            subtext: 'Open source information',
            x: 'center'
        },

        // Add tooltip
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c}%"
        },

        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            padding: 0,
            feature: {
                dataView: {
                    show: true,
                    readOnly: false,
                    title: '查看图表数据',
                    lang: ['查看图表数据', '关闭', '更新']
                },
                restore: {
                    show: true,
                    title: '刷新'
                },
                saveAsImage: {
                    show: true,
                    title: '另存为图片',
                    lang: ['保存']
                }
            }
        },

        // Add legend
        legend: {
            show: false,
            data: ['Chrome','Opera','Safari','Firefox','IE'],
            orient: 'vertical',
            x: 'left',
            y: 75
        },

        // Enable drag recalculate
        calculable: true,

        // Add series
        series: [
            {
                name: '期望值',
                type: 'funnel',
                y: '0%',
                x: '10%',
                x2: '25%',
                width: '50%',
                height: '80%',
                itemStyle: {
                    normal: {
                        label: {
                            formatter: '{b}',
                            textStyle: {
                                fontSize: 16
                            }
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            position: 'inside',
                            formatter: '{b}: {c}%',
                            textStyle: {
                                fontSize: 16
                            }
                        }
                    }
                },
                data: [
                    {value: 20, name: ' 支付买家数'},
                    {value: 50, name: '下单买家数'},
                    {value: 100, name: '访客数'}
                ]
            },
            {
                name: '实际值',
                type: 'funnel',
                y: '0%',
                x: '10%',
                x2: '25%',
                width: '50%',
                height: '80%',
                maxSize: '100%',
                itemStyle: {
                    normal: {
                        borderColor: '#fff',
                        borderWidth: 2,
                        label: {
                            position: 'inside',
                            formatter: '{c}%',
                            textStyle: {
                                fontSize: 16,
                                color: '#FFFFFF'
                            }
                        }
                    },
                    emphasis: {
                        label: {
                            position: 'inside',
                            formatter: '{b}: {c}%',
                            textStyle: {
                                fontSize: 16
                            }
                        }
                    }
                },
                data: [
                    {value: 15, name: ' 支付买家数'},
                    {value: 40, name: '下单买家数'},
                    {value: 100, name: '访客数'}
                ]
            }
        ]
    };
    trade_total.setOption(trade_total_options);


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            trade_detail.resize();
            trade_total.resize();
        }, 200);
    }

});
