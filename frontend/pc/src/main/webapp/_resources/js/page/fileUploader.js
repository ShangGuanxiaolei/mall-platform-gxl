define(['jquery', 'dropzone'], function ($) {

  const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

  /**
   * 新建dropZone封装实例
   * @param params.onSuccess 成功后的回调函数，返回地址
   * @param params.dom drop的dom节点
   * @param params.url 上传文件地址，不指定则使用默认的
   * @constructor
   */
  function Uploader(params) {
    /* jquery 的dropZone实例 */
    this.jDropInstance = $(params.dom).dropzone({
      url: defaultUrl,
      paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
      dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
      maxFilesize: 10, // MB      //最大文件大小，单位是 MB
      maxFiles: 10,               //限制最多文件数量
      maxThumbnailFilesize: 10,
      addRemoveLinks: true,
      thumbnailWidth: "150",      //设置缩略图的缩略比
      thumbnailHeight: "150",     //设置缩略图的缩略比
      acceptedFiles: ".gif,.png,.jpg",
      uploadMultiple: false,
      dictInvalidFileType: "文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
      dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
      dictRemoveFile: "删除",                                        //移除文件链接的文本
      dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
      dictResponseError: "服务器暂无响应,请稍后再试!",
      dictCancelUpload: "取消上传",
      dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
      dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
      autoDiscover: false,
      //autoProcessQueue: false,
      init: function () {

        console.log(this);

        // var imgDropzone = this;
        //添加了文件的事件
        this.on("addedfile", function (file) {
          $(".btn-submit").addClass("disabled");
          if (file && file.dataImg && file.previewElement) { //是网络加载的数据
            $(file.previewElement).attr("data-img", file.dataImg);
            if (file.size === '' || file.length === 0) {
              $(file.previewElement).find(".dz-details").hide();
            }
          }
          //imgDropzone.processQueue();
        });
        this.on("success", function (file, data) {
          if (typeof(data) === 'object') {
            switch (data.errorCode) {
              case 200: {
                if (typeof(data.data) === 'object') {
                  var imgId = data.data[0].id;
                  if (file && file.previewElement) {
                    $(file.previewElement).attr("data-img", imgId);
                    if (params.onSuccess) {
                      params.onSuccess(imgId);
                    }
                  }
                }
                break;
              }
              default: {
                utils.tools.alert("图像上传失败,请重新选择!",
                    {timer: 1200, type: 'warning'});
                break;
              }
            }
          } else {
            if (data === -1) {
              utils.tools.alert("图像上传失败,请重新选择!",
                  {timer: 1200, type: 'warning'});
            }
          }
          $(".btn-submit").removeClass("disabled");
        });

        this.on("error", function () {
          utils.tools.alert("文件上传失败!", {timer: 1200, type: 'warning'});
          $(".dz-error-message").html("文件上传失败!");
          $(".btn-submit").removeClass("disabled");
        });

        this.on("complete", function () {   //上传完成,在success之后执行
          $(".btn-submit").removeClass("disabled");
        });

        this.on("thumbnail", function (file) {
          if (file && file.previewTemplate) {
            file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
            file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
          }
          const $dzImage = $('.dz-image');
          $dzImage.css("height", "150px;");
          $dzImage.css("width", "150px;");
        });

      }
    });

    /* 真实的dropZone实例 */
    this.dropZone = this.jDropInstance[0].dropzone;

    console.log(this.dropZone);

    /**
     * 清空dropZone的上传文件
     */
    this.clear = function () {
      this.dropZone.removeAllFiles(true);
      return this;
    };

    /**
     * 添加图片预览到dropZone
     * @param imgUrl 图片地址
     */
    this.addImage =  function(imgUrl) {
      if (imgUrl) {
        var mockFile = {name: "", size: "", dataImg: imgUrl};
        console.log(this);
        this.dropZone.emit("addedfile", mockFile);
        this.dropZone.emit("thumbnail", mockFile, imgUrl);
        this.dropZone.emit("complete", mockFile);
        this.dropZone.files.push( mockFile ); // 此处必须手动添加才可以用removeAllFiles移除
      }
      return this;
    }
  }

  return {
    /**
     * 新建一个文件上传实例并返回
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @returns {Uploader}
     */
    createUploader: function (params) {
      return new Uploader(params);
    }
  }

});
