define(['jquery','utils','datatables','blockui','select2','tree', 'fileinput_zh', 'fileinput'], function($,utils,datatabels,blockui,select2,tree,fileinput_zh,fileinput) {
    var _data;
    var _id;
    var _icon;
    var batch;
    var taxonomy = utils.tools.request('type') || 'GOODS';
    $('#jstree').jstree({
        "core" : {
            "animation" : 0,
            "check_callback" : true,
            "themes" : { "stripes" : true },
            'data' : {
                'url' : window.host + "/category/list?taxonomy=" + taxonomy,
                'data' : function (node) {
                    return { 'id' : node.id };
                }
            }
        },
        "types" : {
            "#" : {
                "max_children" : 1,
                "max_depth" : 9,
                "valid_children" : ["root"]
            },
            "root" : {
                "icon" : "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children" : ["default"]
            },
            "default" : {
                "valid_children" : ["default","file"]
            },
            "file" : {
                "icon" : "glyphicon glyphicon-file",
                "valid_children" : []
            }
        },
        "plugins" : [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "ccp": null,
                "create" : {
                    "separator_before"	: false,
                    "separator_after"	: true,
                    "_disabled"			: false, //(this.check("create_node", data.reference, {}, "last")),
                    "label"				: "新增分类",
                    "action"			: function (data) {
                        var inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.create_node(obj, {}, "last", function (new_node) {
                            setTimeout(function () { inst.edit(new_node); },0);
                        });
                    }
                },
                "rename" : {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
                    "label"				: "修改分类",
                    /*!
                     "shortcut"			: 113,
                     "shortcut_label"	: 'F2',
                     "icon"				: "glyphicon glyphicon-leaf",
                     */
                    "action"			: function (data) {
                        var inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.edit(obj);
                    }
                },
                "remove" : {
                    "separator_before"	: false,
                    "icon"				: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "删除分类",
                    "action"			: function (data) {

                        utils.tools.confirm("删除分类，将同时删除该分类所有下级分类，确定删除吗?", function(){
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            if(inst.is_selected(obj)) {
                                inst.delete_node(inst.get_selected());
                            }
                            else {
                                inst.delete_node(obj);
                            }
                        },function(){

                        });

                    }
                },
                "更新图片": {
                    "label": "更新图片",
                    "action": function (data) {
                        var inst = jQuery.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        _data = data;
                        _id = obj.id;
                        _icon = obj.icon;

                        $("#file-input").fileinput('destroy');
                        //初始化方法
                        initFileInput(_id, _icon);

                        $("#modal_upload_img").modal("show");
                    }
                },
                // "更新类别小图片": {
                //     "label": "更新类别小图片",
                //     "action": function (data) {
                //         var inst = jQuery.jstree.reference(data.reference),
                //             obj = inst.get_node(data.reference);
                //         _data = data;
                //         _id = obj.id;
                //         _icon = obj.tinyImg;
                //
                //         $("#file-input-tiny").fileinput('destroy');
                //         //初始化方法
                //         initFileInputTiny(_id, _icon);
                //
                //         $("#modal_upload_tiny_img").modal("show");
                //     }
                // },
                // "更新字体颜色": {
                //     "label": "更新字体颜色",
                //     "action": function (data) {
                //         var inst = jQuery.jstree.reference(data.reference),
                //             obj = inst.get_node(data.reference);
                //         _data = data;
                //         _id = obj.id;
                //
                //         $("#update_id").val(_id);
                //         $("#color").val(obj.fontColor);
                //
                //         $("#modal_color").modal("show");
                //     }
                // }
            }
        }
    }).bind("loaded.jstree", function () {
        jQuery("#jstree").jstree("open_all");
    }).bind("create_node.jstree",function(event,data){
        createCategory(event,data);
    }).bind("rename_node.jstree",function(event,data){
        rename(event,data);
    }).bind("delete_node.jstree",function(event,data){
        reMove(event,data);
    }).bind("move_node.jstree",function(event,data){
        movetree(event,data);
    });

    // 分类节点新增
    function createCategory(event,data){
        var parentId = data.parent;
        var newNodeName = data.node.text;
        var params = {"parentid":parentId,"name":newNodeName};
        $.ajax({
            'url': window.host + "/category/save?taxonomy=" + taxonomy,
            'type':"post",
            'dataType':'json',
            'cache':false,
            'data':params,
            'timeout':1000*10
        }).done(function(json){
            var id = data.node.id;
            var categoryId = json.data.id;
            data.instance.set_id(data.node, categoryId);
            //data.instance.set_icon(data.node, "http://7xl05n.com2.z0.glb.qiniucdn.com/FohXBIn3VCI7K2NNXgrzdZyE7HT7?imageView2/2/w/640/q/100");
        }).fail(function(e){
            Metronic.unblockUI();
            alert("亲出错了，请稍后再试");
        })
    }

    // 分类节点重命名
    function rename(event,data) {
        var str = data.node.id;
        if(str == '0') {
            alert("不能修改根节点");
            data.instance.set_text(data.node, '标签分类');
            return;
        }
        var id = str;
        var name = data.text;
        var params = {"id":id,"name":name};
        $.ajax({
            'url': window.host + '/category/updateName',
            'type':'post',
            'dataType':'json',
            'data':params,
            'cache':false,
            'timeout':1000*10,
            'success': function(data1) {
                if(data1.data == null){
                    alert('分类名称已经存在，不允许重复');
                    data.instance.set_text(data.node, data.old);
                }
            },
            'error': function(e) {
                alert(e);
            }
        }).fail(function(){
            alert("亲出错了，请稍后再试~");
        })
    }

    // 分类节点删除
    function reMove(event,data) {
        var str = data.node.id;
        if(str == '0') {
            alert("亲，不能删除根节点");
            return ;
        }
        var id = str;
        var params = {"id":id};
        $.ajax({
            'url': window.host + "/category/delete",
            'type':"post",
            'dataType':"json",
            'data':params,
            'cache':false,
          'timeout': 1000 * 10,
          'async': true,
          'success': function (json) {
            if (json.moreInfo != null && json.moreInfo.length > 0) {
              setTimeout(function () {
                utils.tools.alert(json.moreInfo, {timer: 2000});
                $.jstree.reference($('#jstree')).refresh();
              }, 500);
            } else if (json.ret == false) {
              alert(json.errmsg);
            }
          }
        });
    }

    // 分类节点移动
    function movetree(event,data) {
        var id = data.node.id;
        var parentid  = data.parent;
        var position = data.position;
        var sourceposition = data.old_position;
        var sourceparentid = data.old_parent;
        var params = {"id":id,"parentid":parentid,"position":position,"sourceposition":sourceposition,"sourceparentid":sourceparentid};
        $.ajax({
            'url': window.host + "/category/move",
            'type':"post",
            'dataType':"json",
            'data':params,
            'cache':false,
            'timeout':1000*10
        }).done(function(json){
            if(json.ret==false) {
                alert(json.errmsg);
            }
        }).fail(function(json){
            Metronic.unblockUI();
            alert("亲出错了，请稍后再试~");
        })

    }

    // 初始化上传图片控件
    function initFileInput(id, img){
        var op = {
            language: 'zh',
            uploadUrl: window.host + "/category/updateImg",
            uploadExtraData: {"id": id},
            previewFileType: 'text',
            browseLabel: '选择文件',
            removeLabel: '删除',
            uploadLabel: '上传',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            browseClass: 'btn btn-primary',
            uploadClass: 'btn btn-default',
            removeClass: 'btn btn-danger',
            initialCaption: '',
            maxFilesNum: 1,
            enctype: 'multipart/form-data',
            allowedFileExtensions: ["jpg","png","gif"],
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                footer: '',
            },

        };
        //如果img有值，则显示之前上传的图片
        if(img && img.toString().indexOf('http') != -1){
            op = $.extend({
                showPreview : true,
                initialPreview : [ // 预览图片的设置
                    "<img src= '" + img +  "' class='file-preview-image'>", ]
            }, op);
        }

        $('.file-input').fileinput(op);
        $('#file-input').fileinput('enable');

    }

    // 图片上传成功后更新节点图标
    $('#file-input').on('fileuploaded', function(event, data, msg) {
        $.jstree.reference(_data.reference).set_icon(_data.node, data.response.img);
        $.jstree.reference(_data.reference).refresh();
    });


    $('#modal_upload_img').on('hidden.bs.modal', function () {
        $('#file-input').fileinput('enable');
    });


    // 初始化上传图片控件
    function initFileInputTiny(id, img){
        var op = {
            language: 'zh',
            uploadUrl: window.host + "/category/updateTinyImg",
            uploadExtraData: {"id": id},
            previewFileType: 'text',
            browseLabel: '选择文件',
            removeLabel: '删除',
            uploadLabel: '上传',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            browseClass: 'btn btn-primary',
            uploadClass: 'btn btn-default',
            removeClass: 'btn btn-danger',
            initialCaption: '',
            maxFilesNum: 1,
            enctype: 'multipart/form-data',
            allowedFileExtensions: ["jpg","png","gif"],
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                footer: '',
            },

        };
        //如果img有值，则显示之前上传的图片
        if(img && img.toString().indexOf('http') != -1){
            op = $.extend({
                showPreview : true,
                initialPreview : [ // 预览图片的设置
                    "<img src= '" + img +  "' class='file-preview-image'>", ]
            }, op);
        }

        $('#file-input-tiny').fileinput(op);
        $('#file-input-tiny').fileinput('enable');

    }

    // 保存类别颜色
    $(document).on('click', '.saveBtn', function () {
        var id = $("#update_id").val();
        var color = $("#color").val();
        if(color == ''){
            utils.tools.alert("请输入颜色代码!", {timer: 2000, type: 'warning'});
            return;
        }

        var param = {
            id: id,
            color: color
        };
        utils.postAjaxWithBlock($(document), window.host + '/category/updateColor', param, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        utils.tools.alert("保存成功!", {timer: 2000, type: 'warning'});
                        $("#modal_color").modal("hide");
                        break;
                    }
                    default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {
            } else if (res == -1) {
                utils.tools.alert("保存失败", {timer: 1200});
            }
        });

    });


});