/**
 * Created by quguangming on 16/5/18.
 */
define(['form/validate','utils','cookies','md5'], function(validate,utils,cookies,md5) {

    var $form = $('.signin-form');

    var $login = $('.login');

    var url = window.host + '/pc/signin_check';

    var url_setting = 'http://' + window.location.host  + "/v2" +'/wechat/isDefaultExist';

    var isProcessing = false;

    $('#username').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $('#password').focus();
        }
    });

    $('#password').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $form.submit();
        }
    });

    $login.on('click',function(){
        $form.submit();
    });

    var loginForm = {
        rules:{
            username: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{1,16}$/
            },
            password: {
                required: true,
                pattern: /^.{6,50}$/
            }
        },
        messages:{
            username: {
                required: "账号不能为空.",
                pattern: "请输入有效的1~16位字母、数字和下划线的组合账号"
            },
            password: {
                required: "密码不能为空.",
                pattern: "请输入至少6位的密码."
            }
        },
        submitCallBack: function(form){

            login();
        }
        ,//invalidCallBack:function(form,validator){
        //
        //},
        //setSuccessText: function(label){
        //    $(label).text("验证成功");
        //},
        //successClass: "validation-success-label",
        //errorClass:""
    };

    //cookie
    $('.username').val($.cookie('username'));

    function login(){
        var txtUserName = $('#username');
        var txtPassword = $('#password');

        var username, password;

        username = txtUserName.val();
        password = txtPassword.val();

        txtPassword.on('keyup', function(ev){
            var evt = ev || event;
            if (evt.keyCode === 13) {
                $login.trigger('click');
                return false;
            }
        });

        if (isProcessing) {
            return false;
        } else {
            isProcessing = true;
        }

        var data = {
            u: username,
            p: CryptoJS.MD5(password).toString()
        };

        utils.postAjax(url, data, function(result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    { // 密码正确
                        saveCookies(username);
                        utils.postAjax(url_setting, data, function(result) {
                            if (typeof(result) === 'object') {
                                switch (result.data) {
                                    case true:
                                    {
                                      location.href = '/sellerpc/mall/route';
                                        break;
                                    }
                                    default:
                                    {
                                        location.href = '/sellerpc/wechat/config';
                                        break;
                                    }
                                }
                            } else{
                                if (result == -1){
                                  location.href = '/sellerpc/mall/route';
                                }
                            }

                        });

                        break;
                    }
                    default:
                    {
                        alert('登录失败，手机号码或密码错误!');
                        break;
                    }
                }
            } else{
                if (result == -1){
                    alert('登录失败，手机号码或密码错误!');
                }
            }
            isProcessing = false;
        });
        return false;
    }

    validate($form,loginForm);

    function saveCookies(username){
        $.cookie('username', username, {
            expires: 7
        });
    }

});