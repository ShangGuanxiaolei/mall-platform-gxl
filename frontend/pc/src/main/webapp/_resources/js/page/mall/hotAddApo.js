define('mall/hotAddApo',
    ['jquery', 'productModal', 'utils', 'datatables', 'blockui', 'bootbox',
      'select2', 'daterangepicker'],
    function ($, productModal, utils, datatabels, blockui, select2,
        daterangepicker) {

      const NOT_FOUND = "/_resources/images/404.png";

      // 商品类型categoryType
      var productType = $('#type').val();
      var taxonomy = (() => productType === 'FILTER' ? 'FILTER' : 'GOODS')();

      var productId;

      var $listUrl = window.host + "/product/list?type=" + productType;
      var $updateReviewStatusUrl = (id,
          status) => `${window.host}/product/review/${id}?status=${status}`;
      var $unBindUrl = (filterId, productId) => window.host
          + `/product/filter/unbind?filterId=${filterId}
        &productId=${productId}`;

      var $bindUrl = (filterId, productId) => window.host
          + `/product/filter/bind?filterId=${filterId}
        &productId=${productId}`;

      var $orders = ['price', 'amount', 'sales', 'onsale'];

      var $shopId = null;

      var $order = '';

      var $status = $("#productStatus").val();

      var $reviewStatus = '';

      var $brandType = '';

      var $supplierType = '';

      const checkAll = '#checkAllGoods';

      var $category = '';

      var $groupon = '';

      var $listHot = window.host + "/product/hotProduct";

      var ids = new Set();

      var hotType = window.location.search;

      var typeHot = "";

      var idHot = "";

      //获取品牌信息
      let brandMap = new Map();
      $.ajax({
            url: `${window.host}/brand/list`,
          }
      ).done(
          data => {
            let results = [];
            let list = data.data.list;
            for (let i = 0; i < list.length; i++) {
              brandMap.set(list[i].id, list[i].name);
              let brand = {
                id: list[i].id,
                text: list[i].name,
              };
              results.push(brand);
            }
            //初始化select2
            $("#brandType").select2({
              minimumResultsForSearch: Infinity,
              data: results
            });
          }
      ).fail(
          data => {
            utils.tools.alert("获取品牌信息出错",
                {timer: 1200, type: 'warning'});
          });

      //获取供应商信息并建立缓存
      let sourceMap = new Map();
      $.ajax({
            url: `${window.host}/supplier/list?type=override&pageable=false`,
            async: false,
          }
      ).done(
          data => {
            let results = [];
            data.data.list.forEach(e => {
              sourceMap.set(e.id, e.name);
              let supplier = {
                id: e.id,
                text: e.name,
              };
              results.push(supplier);
              //初始化select2
              $("#supplierType").select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            });
          }
      ).fail(
          data => {
          });

      /**
       * 动态构建表格列
       * @param base
       * @constructor
       */
      function ColumnBuilder(base) {
        this.columns = base && $.isArray(base) ? base : [];

        /**
         * 新增列
         * @param column 列对象
         * @param predicate 添加条件
         * @return {ColumnBuilder}
         */
        this.append = function (column, predicate) {
          if (!predicate || (predicate && predicate() === true)) {
            if ($.isArray(column)) {
              this.columns = this.columns.concat(column);
            } else {
              this.columns.push(column);
            }
          }
          return this;
        };

        /**
         * 返回构建好的列
         */
        this.build = function () {
          return this.columns;
        }
      }

      /** 初始化选择框控件 **/
      $('.select').select2({
        minimumResultsForSearch: Infinity,
      });

      /** 初始化日期控件 **/
      var options = {
        timePicker: true,
        dateLimit: {days: 60000},
        timePickerIncrement: 1,
        locale: {
          format: 'YYYY-MM-DD h:mm a',
          separator: ' - ',
          applyLabel: '确定',
          startLabel: '开始日期:',
          endLabel: '结束日期:',
          cancelLabel: '取消',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
      };

      $('.daterange-time').daterangepicker(options);

      /** 回调 **/
      $('.daterange-time').on('apply.daterangepicker', function (ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.startDate = picker.startDate;
        options.endDate = picker.endDate;
        $("#groupon_valid_from").val(
            picker.startDate.format('YYYY-MM-DD HH:mm'));
        $("#groupon_valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
      });

      // 全选
      $("#checkAllGoods").on('click', function () {
        $("input[name='checkGoods']").prop("checked", $(this).prop("checked"));
      });

      $(".btn-release").on('click', function () {
        window.location.href = '/sellerpc/mall/product/edit?type='
            + productType;
      });

      $(".btn-batchDel").on('click', function () {
        var updateds = getTableContent();
        if (updateds.length == 0) {
          utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
          return;
        }
        var ids = '';
        $.each(updateds, function (index, row) {
          if (ids != '') {
            ids += ',';
          }
          ids += row.id;
        });

        var url = window.host + "/product/batchDelete/" + ids;
        utils.postAjax(url, {}, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("删除成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
            }
          }
        });

      });

      $(".btn-batchUp").on('click', function () {
        var updateds = getTableContent();
        if (updateds.length == 0) {
          utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
          return;
        }
        $.each(updateds, function (index, row) {
          // if (ids != '') {
          //   ids += ',';
          // }
          // ids += row.id;
          ids.add(row.id);
        });
        // upProduct(ids);
        utils.tools.alert("一共" + updateds.length + "个添加成功!",
            {timer: 1200, type: 'success'});
      });

      $(".btn-batchDown").on('click', function () {
        var updateds = getTableContent();
        if (updateds.length == 0) {
          utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
          return;
        }
        var ids = '';
        $.each(updateds, function (index, row) {
          if (ids != '') {
            ids += ',';
          }
          ids += row.id;
        });
        downProduct(ids);
      });

      // 得到当前选中的行数据
      function getTableContent() {
        var selectRows = new Array();
        for (var i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
          var value = {};
          var checkvalue = $("input[name='checkGoods']:checked")[i];
          value.id = $(checkvalue).attr("rowid");
          selectRows.push(value);
        }
        return selectRows;
      }

      /**
       * 优先级
       */
      $(document).on('click', '#confirmPriority', function () {
        var $input = $(this).prev();
        var priority = $input.val();
        var id = $input.attr('rowId');
        utils.post(window.host + '/product/updatePriority', function (result) {
          if (result && result === true) {
            utils.tools.success('修改成功');
            return;
          }
          utils.tools.error('修改失败');
        }, {id: id, priority: priority})
      });

      //buttonRoleCheck('.hideClass');

      //初始化商品分类信息
      $.ajax({
        url: window.host + '/shop/category/list?taxonomy=' + taxonomy,
        type: 'POST',
        dataType: 'json',
        data: {},
        success: function (data) {
          if (data.errorCode == 200) {
            var dataLength = data.data.length;
            var $categoryType = $('#categoryType');
            $categoryType.empty();
            $categoryType.append(
                '<option value="" selected="selected">所有分类</option>');
            for (var i = 0; i < dataLength; i++) {
              $categoryType.append('<option value=' + data.data[i].id + '>'
                  + data.data[i].name + '</option>');
            }
          } else {
            utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
          }
        },
        error: function (state) {
          if (state.status == 401) {
          } else {
            utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
          }
        }
      });

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          search: '<span>筛选:</span> _INPUT_',
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      // 初始化滤芯选择
      if (productType === 'NORMAL') {
        productModal.create({
          triggerDom: '.bindFilter',
          type: 'FILTER',
          onConfirm: function (selectedFilter) {
            bindFilterAll(productId, selectedFilter);
          }
        });
      }

      var $datatables = utils.createDataTable('#xquark_list_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        ordering: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
          $.get($listHot, {
            size: data.length,
            page: (data.start / data.length),
            keyword: data.search.value,
            pageable: true,
            // status: $status,
            // reviewStatus: $reviewStatus,
            brand: $brandType,
            supplier: $supplierType,
            // order: function () {
            //   if ($order !== '') {
            //     return $order;
            //   } else {
            //     var _index = data.order[0].column;
            //     if (_index < 4) {
            //       return '';
            //     } else {
            //       return $orders[_index - 4];
            //     }
            //   }
            // },
            // direction: data.order ? data.order[0].dir : 'asc',
            category: $category,
            // isGroupon: $groupon
          }, function (res) {
            if (!res.data.list) {
              res.data.list = [];
            }
            callback({
              recordsTotal: res.data.categoryTotal,
              recordsFiltered: res.data.categoryTotal,
              data: res.data.list,
              iTotalRecords: res.data.categoryTotal,
              iTotalDisplayRecords: res.data.categoryTotal
            });
          });
        },
        rowId: "id",
        columns:
            [
              {
                title: '<label class="checkbox"><input id="checkAllGoods" name="checkAllGoods" type="checkbox" ></label>',
                width: "10px",
                orderable: false,
                render: function (data, type, row) {
                  return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                      + row.id + '" value="' + row.id + '"></label>';
                }
              },
              {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                  return row.img
                      ? `<img class="goods-image" src="${row.img}" />`
                      : `<img class="goods-image" src="${NOT_FOUND}" />`;
                }
              },
              {
                title: "商品名称",
                data: "name",
                orderable: false,
                name: "name"
              },
              {
                title: "售价",
                data: "price",
                orderable: false,
                name: "price"
              }, {
              title: "德分",
              data: "deductionDPoint",
              orderable: false,
              name: "deductionDPoint"
            }, {
              title: "供应商",
              data: "sName",
              orderable: false,
              name: "sName"
            }, {
              title: "分类",
              data: "cName",
              orderable: false,
              name: "cName"
            }, {
              title: "品牌",
              data: "aName",
              orderable: false,
              name: "aName"
            },
              {
                title: "库存",
                data: "amount",
                orderable: false,
                name: "amount"
              },
              {
                title: "销售量",
                data: "sales",
                orderable: false,
                name: "sales"
              },
              {
                title: "操作",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  // if (row.reviewStatus === 'WAIT_CHECK') {
                  html += '<a href="javascript:void(0);" class="pass-review role_check_table" style="margin-left: 10px;" data-toggle="pass" rowId="'
                      + row.id
                      + '" fid="up_shelves"><i class="icon-pencil7"></i>添加</a>';
                  // html += '<a href="javascript:void(0);" class="fail-review role_check_table" style="margin-left: 10px;" data-toggle="fail" rowId="'
                  //     + row.id
                  //     + '" fid="up_shelves"><i class="icon-pencil7"></i>未通过审核</a>';
                  // }
                  // if (row.reviewStatus === 'CHECK_PASS' && (row.status
                  //     === 'INSTOCK' || row.status === 'DRAFT')) {
                  //   html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="uppopover" rowId="'
                  //       + row.id
                  //       + '" fid="up_shelves"><i class="icon-pencil7"></i>上架</a>';
                  // }
                  // if (row.status === 'ONSALE') {
                  //   html += '<a href="javascript:void(0);" class="down role_check_table" style="margin-left: 10px;" data-toggle="downpopover" rowId="'
                  //       + row.id
                  //       + '" fid="off_shelves" ><i class="icon-pencil7"></zi>下架</a>';
                  // }
                  // html += '<a href="/sellerpc/mall/product/edit?type='
                  //     + productType
                  //     + '&pId=' + row.id
                  //     + '" target="_blank" class="edit role_check_table" style="margin-left: 10px;" rowId="'
                  //     + row.id
                  //     + '" fid="edit_product"><i class="icon-pencil7" ></i>编辑</a>';
                  // html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'
                  //     + row.id
                  //     + '" fid="delete_product"><i class="icon-trash"></i>删除</a>';
                  return html;

                }

              }
            ],
        select: {
          style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
          initEvent();
          $(".role_check_table").click(function () {
            var id = $(this).attr("rowId");
            ids.add(id);
            utils.tools.alert("添加成功!", {timer: 1200, type: 'success'});
          });
        }
      });

      function initEvent() {
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        /**$(".edit").on("click",function(){
            var pId =  $(this).attr("rowId");
            window.location.href = '/sellerpc/mall/product/edit?pId='+pId;
        });**/

        /*商品团购*/
        $(".groupon").on("click", function () {
          var id = $(this).attr("rowId");
          $('.daterange-time').val('');
          $("#groupon_product_id").val(id);
          $("#groupon_discount").val('');
          $("#groupon_numbers").val('');
          $("#groupon_amount").val('');
          $("#modal_grouponAdd").modal("show");
        });

        /** 点击上架弹出框 **/
        $("[data-toggle='uppopover']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认上架吗？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="uppopover"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="uppopover"]').popover('hide');

        }).on('shown.bs.popover', function () {
          var that = this;
          $('.popover-btn-ok').off("click").on("click", function () {
            var id = $(this).attr("mId");
            upProduct(id);
          });
          $('.popover-btn-cancel').off("click").on("click", function () {
            $(that).popover('hide');
          });
        });

        $('body').on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "uppopover") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="uppopover"]').popover('hide');
          } else if (target.data("toggle") == "uppopover") {
            target.popover("show");
          }
        });

        /** 点击下架弹出框 **/
        $("[data-toggle='downpopover']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认下架吗？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="downpopover"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="downpopover"]').popover('hide');

        }).on('shown.bs.popover', function () {
          var that = this;
          $('.popover-btn-ok').off("click").on("click", function () {
            var id = $(this).attr("mId");
            downProduct(id);
          });
          $('.popover-btn-cancel').off("click").on("click", function () {
            $(that).popover("hide");
          });
        });

        $('body').on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "downpopover") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="downpopover"]').popover('hide');
          } else if (target.data("toggle") == "downpopover") {
            target.popover("toggle");
          }
        });

        /** 点击审核通过弹出框 **/
        // $("[data-toggle='pass']").popover({
        //   trigger: 'manual',
        //   placement: 'left',
        //   html: 'true',
        //   animation: true,
        //   content: function () {
        //     var rowId = $(this).attr("rowId");
        //     return '<span>确认通过吗？</span>' +
        //         '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
        //         + rowId + '">确认</button>' +
        //         '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
        //   }
        // });

        $('[data-toggle="pass"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="pass"]').popover('hide');

        }).on('shown.bs.popover', function () {
          let that = this;
          $('.popover-btn-ok').off("click").on("click", function () {
            let pId = $(this).attr("pId");
            updateProductReviewStatus(pId, 'CHECK_PASS');
          });
          $('.popover-btn-cancel').off("click").on("click", function () {
            $(that).popover('hide');
          });
        });

        $('body').on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "pass") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="pass"]').popover('hide');
          } else if (target.data("toggle") == "pass") {
            target.popover("show");
          }
        });

        /** 点击审核失败弹出框 **/
        $("[data-toggle='fail']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认失败吗？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="fail"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="fail"]').popover('hide');

        }).on('shown.bs.popover', function () {
          let that = this;
          $('.popover-btn-ok').off("click").on("click", function () {
            let pId = $(this).attr("pId");
            updateProductReviewStatus(pId, 'CHECK_FAIL');
          });
          $('.popover-btn-cancel').off("click").on("click", function () {
            $(that).popover('hide');
          });
        });

        $('body').on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "fail") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="fail"]').popover('hide');
          } else if (target.data("toggle") == "fail") {
            target.popover("show");
          }
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认删除？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="popover"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="popover"]').popover('hide');

        }).on('shown.bs.popover', function () {
          var that = this;
          $('.popover-btn-ok').on("click", function () {
            var pId = $(this).attr("pId");
            deleteProduct(pId);
          });
          $('.popover-btn-cancel').on("click", function () {
            $(that).popover("hide");
          });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "popover") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="popover"]').popover('hide');
          } else if (target.data("toggle") == "popover") {
            target.popover("toggle");
          }
        });

        /**
         * 监听全选
         */
        $(checkAll).on('click', function () {
          $("input[name='checkGoods']").prop("checked",
              $(this).prop("checked"));
        });

      }

      $(".btn-search").on('click', function () {
        var keyword = $.trim($("#sKeyword").val());
        $datatables.search(keyword).draw();
      });

      $(document).on('click', '.bindFilter', function () {
        productId = $(this).attr('rowId');
      });

      /*商品删除会判断 userId 否则删除失败*/
      function deleteProduct(pId) {
        var url = window.host + "/product/delete/" + pId;
        utils.postAjax(url, {}, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              $datatables.search('').draw();
            } else {
              utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      /*上架商品*/
      function upProduct(pId) {
        var data = {
          'ids': pId
        };
        var url = window.host + "/product/batch-onsale";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("上架成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert("上架失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      /*改变商品的审核状态*/
      function updateProductReviewStatus(pId, status) {
        $.get($updateReviewStatusUrl(pId, status)
        ).done(
            data => {
              utils.tools.alert("改变审核状态成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw();
            }
        ).fail(
            data => {
              utils.tools.alert("改变审核状态失败!", {timer: 1200, type: 'warning'});
            });
      }

      /*下架商品*/
      function downProduct(pId) {
        var data = {
          'ids': pId
        };
        var url = window.host + "/product/batch-instock";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("下架成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert("下架失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      function bindFilterAll(pId, fIds) {
        utils.postAjaxJson(window.host + '/product/filter/bind',
            {productId: pId, filterIds: Array.from(fIds)}, function (res) {
              if (typeof res === 'object') {
                if (res.errorCode === 200) {
                  if (res.data && res.data === true) {
                    utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                  }
                } else {
                  const message = res.moreInfo || '服务器错误,请稍后再试';
                  utils.tools.alert(message, {timer: 1200, type: 'warning'});
                }
              } else if (res === -1) {
                utils.tools.alert('网络错误, 请稍后再试',
                    {timer: 1200, type: 'warning'});
              }
            });
      }

      function bindFilter(pId, fId) {
        utils.postAjax(window.host + '/product/filter/bind',
            {pId: pId, fId: fId}, function (res) {
              if (typeof res === 'object') {
                if (res.errorCode === 200) {

                } else {
                  const message = res.moreInfo || '服务器错误,请稍后再试';
                  utils.tools.alert(message, {timer: 1200, type: 'warning'});
                }
              } else {
                utils.tools.alert('网络错误, 请稍后再试',
                    {timer: 1200, type: 'warning'});
              }
            });
      }

      $('body').on('change', 'select[name="categoryType"]', function (event) {
        $category = $(this).val();
        $listUrl = window.host + "/product/list?type=" + productType;
        $datatables.search('').draw();
      });

      $('select[name=filterSpec]').on('change', function () {
        var spec = $(this).val();
        var slt = $('select[name="productStatus"]').val();
        if (spec === '') {
          $listUrl = window.host + "/product/list?type=" + productType;
        } else {
          $listUrl = window.host + `/filter/list?spec=${spec}&status=${slt}`;
        }
        $datatables.search('').draw();
      });

      $('select[name="reviewStatus"],select[name="productStatus"],select[name="productStatus"],select[name="brandType"],select[name="supplierType"]').on(
          'change', function () {
            $status = $("#productStatus").val();
            $reviewStatus = $('#reviewStatus').val();
            $brandType = $("#brandType").val();
            $supplierType = $("#supplierType").val();
            $datatables.search($("#sKeyword").val()).draw();
          });

      $(".saveGrouponBtn").on('click', function () {
        var url = window.host + '/product/groupon/save';
        var productId = $("#groupon_product_id").val();
        var validFrom = $("#groupon_valid_from").val();
        var validTo = $("#groupon_valid_to").val();
        var discount = $("#groupon_discount").val();
        var numbers = $("#groupon_numbers").val();
        var amount = $("#groupon_amount").val();
        if (!validFrom || validFrom == '' || !validTo || validTo == '') {
          utils.tools.alert("请选择团购时间!", {timer: 1200, type: 'warning'});
          return;
        } else if (!discount || discount == '') {
          utils.tools.alert("请输入团购价格!", {timer: 1200, type: 'warning'});
          return;
        } else if (!numbers || numbers == '') {
          utils.tools.alert("请输入参团人数!", {timer: 1200, type: 'warning'});
          return;
        } else if (!amount || amount == '') {
          utils.tools.alert("请输入团购库存!", {timer: 1200, type: 'warning'});
          return;
        }

        var data = {
          productId: productId,
          validFrom: validFrom,
          validTo: validTo,
          discount: discount,
          numbers: numbers,
          amount: amount,
        };
        utils.postAjaxWithBlock($(document), url, data, function (res) {
          if (typeof(res) === 'object') {
            switch (res.errorCode) {
              case 200: {
                alert("操作成功");
                window.location.href = window.originalHost + '/mall/groupon';
                break;
              }
              default: {
                utils.tools.alert(res.moreInfo, {timer: 1200});
                break;
              }
            }
          } else if (res == 0) {

          } else if (res == -1) {
            utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
          }
        });

      });

      $('#myTab a').click(function (e) {
        e.preventDefault();//阻止a链接的跳转行为
        $(this).tab('show');//显示当前选中的链接及关联的content
      });

      var $refund_datatables;

      $("#refundOrderTab").on("click", function () {
        // if ($refund_datatables != undefined) {
        //   return
        // }

        var idsToS = "";
        ids.forEach(function (value) {
          idsToS += value + ",";
        });
        idsToS += "999999";
        console.log(idsToS);

        $refund_datatables = utils.createDataTable(
            '#xquark_refund_list_tables', {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              ordering: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              destroy: true,
              ajax: function (data, callback, settings) {
                $.get($listHot, {
                  size: data.length,
                  page: (data.start / data.length),
                  keyword: data.search.value,
                  pageable: true,
                  // status: $status,
                  // reviewStatus: $reviewStatus,
                  brand: $brandType,
                  supplier: $supplierType,
                  idsToS: idsToS,
                  // order: function () {
                  //   if ($order !== '') {
                  //     return $order;
                  //   } else {
                  //     var _index = data.order[0].column;
                  //     if (_index < 4) {
                  //       return '';
                  //     } else {
                  //       return $orders[_index - 4];
                  //     }
                  //   }
                  // },
                  // direction: data.order ? data.order[0].dir : 'asc',
                  category: $category,
                  // isGroupon: $groupon
                }, function (res) {
                  if (!res.data.list) {
                    res.data.list = [];
                  }
                  callback({
                    recordsTotal: ids.size,
                    recordsFiltered: ids.size,
                    data: res.data.list,
                    iTotalRecords: ids.size,
                    iTotalDisplayRecords: ids.size
                  });
                });
              },
              rowId: "id",
              columns:
                  [
                    // {
                    //   title: '<label class="checkbox"><input id="checkAllGoodsS" name="checkAllGoods" type="checkbox" ></label>',
                    //   width: "10px",
                    //   orderable: false,
                    //   render: function (data, type, row) {
                    //     return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                    //         + row.id + '" value="' + row.id + '"></label>';
                    //   }
                    // },
                    {
                      width: "30px",
                      orderable: false,
                      render: function (data, type, row) {
                        return row.img
                            ? `<img class="goods-image" src="${row.img}" />`
                            : `<img class="goods-image" src="${NOT_FOUND}" />`;
                      }
                    },
                    {
                      title: "商品名称",
                      data: "name",
                      orderable: false,
                      name: "name"
                    },
                    {
                      title: "售价",
                      data: "price",
                      orderable: false,
                      name: "price"
                    }, {
                    title: "德分",
                    data: "deductionDPoint",
                    orderable: false,
                    name: "deductionDPoint"
                  }, {
                    title: "供应商",
                    data: "sName",
                    orderable: false,
                    name: "sName"
                  }, {
                    title: "分类",
                    data: "cName",
                    orderable: false,
                    name: "cName"
                  }, {
                    title: "品牌",
                    data: "aName",
                    orderable: false,
                    name: "aName"
                  },
                    {
                      title: "库存",
                      data: "amount",
                      orderable: false,
                      name: "amount"
                    },
                    {
                      title: "销售量",
                      data: "sales",
                      orderable: false,
                      name: "sales"
                    },
                    {
                      title: "操作",
                      orderable: false,
                      render: function (data, type, row) {
                        var html = '';
                        // if (row.reviewStatus === 'WAIT_CHECK') {
                        html += '<a href="javascript:void(0);" class="pass-review role_check_tableS" style="margin-left: 10px;" data-toggle="pass" rowId="'
                            + row.id
                            + '" fid="up_shelves"><i class="icon-pencil7"></i>删除</a>';
                        // html += '<a href="javascript:void(0);" class="fail-review role_check_table" style="margin-left: 10px;" data-toggle="fail" rowId="'
                        //     + row.id
                        //     + '" fid="up_shelves"><i class="icon-pencil7"></i>未通过审核</a>';
                        // }
                        // if (row.reviewStatus === 'CHECK_PASS' && (row.status
                        //     === 'INSTOCK' || row.status === 'DRAFT')) {
                        //   html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="uppopover" rowId="'
                        //       + row.id
                        //       + '" fid="up_shelves"><i class="icon-pencil7"></i>上架</a>';
                        // }
                        // if (row.status === 'ONSALE') {
                        //   html += '<a href="javascript:void(0);" class="down role_check_table" style="margin-left: 10px;" data-toggle="downpopover" rowId="'
                        //       + row.id
                        //       + '" fid="off_shelves" ><i class="icon-pencil7"></zi>下架</a>';
                        // }
                        // html += '<a href="/sellerpc/mall/product/edit?type='
                        //     + productType
                        //     + '&pId=' + row.id
                        //     + '" target="_blank" class="edit role_check_table" style="margin-left: 10px;" rowId="'
                        //     + row.id
                        //     + '" fid="edit_product"><i class="icon-pencil7" ></i>编辑</a>';
                        // html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'
                        //     + row.id
                        //     + '" fid="delete_product"><i class="icon-trash"></i>删除</a>';
                        return html;

                      }

                    }
                  ],
              select: {
                style: 'multi'
              },
              drawCallback: function () {  //数据加载完成
                initEvent();
                $("#checkAllGoodsS").on('click', function () {
                  $("input[name='checkGoods']").prop("checked",
                      $(this).prop("checked"));
                });

                $(".role_check_tableS").click(function () {
                  var id = $(this).attr("rowId");
                  ids.delete(id);
                  utils.tools.alert("删除成功", {timer: 1200, type: 'warning'});
                  $("#refundOrderTab").trigger("click");
                });
              }

            });
      });

      $(".add-hot-product").off('click').on('click',function () {
        var idsToS = "";
        ids.forEach(function (value) {
          idsToS += value + ",";
        });
        idsToS = idsToS.substr(0, idsToS.length - 1);
        var hot = $("#hot-word").val();

        if(hot.length == 0){
            utils.tools.alert("请输入热门词条!", {timer: 1200, type: 'warning'});
            return;
        }

        if (typeHot == 0) {
          utils.post(window.host + '/product/createHotSearchKey',
              function (res) {
                // if (typeof res === 'object') {
                if (res) {
                  utils.tools.alert("添加成功！", {timer: 1200, type: 'success'});
                  window.location.href = '/sellerpc/mall/hotSearch';
                }else {
                  const message = res.moreInfo;
                  if(message && message.length > 0){
                      utils.tools.alert(message, {timer: 1200, type: 'warning'});
                  }else{
                      utils.tools.alert("热门词条已经存在!", {timer: 1200, type: 'warning'});
                  }
                }
                // } else {
                //   utils.tools.alert('网络错误, 请稍后再试',
                //       {timer: 1200, type: 'warning'});
                // }
              },{key: hot, productIds: idsToS});
        } else if (typeHot == 1) {
          utils.post(window.host + '/product/updateHotSearchKey',
              function (res) {
                // if (typeof res === 'object') {
                if (res) {
                  utils.tools.alert("修改成功！", {timer: 1200, type: 'success'});
                  window.location.href = '/sellerpc/mall/hotSearch';
                } else {
                    const message = res.moreInfo;
                    if(message && message.length > 0){
                        utils.tools.alert(message, {timer: 1200, type: 'warning'});
                    }else{
                        utils.tools.alert("热门词条已经存在!", {timer: 1200, type: 'warning'});
                    }
                }
                // } else {
                //   utils.tools.alert('网络错误, 请稍后再试',
                //       {timer: 1200, type: 'warning'});
                // }
              },{key: hot, productIds: idsToS, id: idHot});
        }

      });

      $(function () {
        typeHot = hotType.substring(hotType.indexOf("=") + 1,
            hotType.indexOf("&"));
        idHot = hotType.substring(hotType.lastIndexOf("=") + 1);
        if (typeHot == 1) {
          $.ajax({
            "url": "/v2/product/hotSearchKeys/" + idHot,
            "data": "",
            "type": "get",
            "dataType": "json",
            "success": function (obj) {
              var product = obj.data.productId;
              var name = obj.data.hkey;
              // editHot(newId, name, product);
              $("#hot-word").val(name);
              var pIds = product.split(",");
              for (var i = 0; i < pIds.length; i++) {
                ids.add(pIds[i]);
              }
            },
            "error": function (obj) {
              alert("查询错误");
            }
          });
        }
      });
    });