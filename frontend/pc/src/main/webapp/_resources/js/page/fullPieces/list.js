define(['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui', 'select2'], function ($, utils) {

    const prefix = window.host + '/promotionFullPieces';
    const listUrl = prefix + '/listTable';
    const closeUrl = prefix + '/close';
    const listDetailUrl = prefix + '/orderDetail';

    const $dataTable = $('#xquark_full_cut_tables');
    const $detailTable = $('#xquark_order_detail_table');

    const typeMapper = {
        'FULLCUT': '满减优惠',
        'FULLPIECES': '满件优惠'
    };

    var keyWord = '';

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    const manager = (function () {

        const $addPromotionBtn = $('.btn-release');

        const globalInstance = {};

        return {
            initGlobal: function () {
                return this;
            },
            initEvent: function () {
                $addPromotionBtn.on('click', function () {
                    location.href = '/sellerpc/mall/fullPiecesEdit';
                });
                return this;
            },
            initTable: function () {
                $('.edit').on('click', function () {
                    var id = $(this).attr('rowId');
                    location.href = '/sellerpc/mall/fullPiecesEdit?id=' + id;
                });

                $('.del').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm("确认结束活动吗", function () {
                        utils.postAjaxWithBlock($(document), closeUrl, {id: id}, function (res) {
                            if (typeof(res) === 'object') {
                                switch (res.errorCode) {
                                    case 200: {
                                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                                        $fullCutDataTables.search('').draw();
                                        break;
                                    }
                                    default: {
                                        utils.tools.alert(res.moreInfo, {timer: 1200});
                                        break;
                                    }
                                }
                            } else if (res == 0) {

                            } else if (res == -1) {
                                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                            }
                        })
                    }, function () {

                    });
                });
            }
        }
    })();

    manager.initGlobal().initEvent();

    /** 初始化表格数据 **/
    const $fullCutDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                keyWord: keyWord
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                title: '',
                width: "10px",
                orderable: false,
                render: function (data, type, row) {
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="' + row.id + '"></label>';
                }
            },
            {
                title: '标题',
                data: "title",
                width: "120px",
                orderable: false,
                name: "title"
            }, {
                title: '开始时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validFrom"
            }, {
                title: '结束时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validTo"
            }, {
                title: '状态',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.archive) {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '发布时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            },
            {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="' + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" ><i class="icon-trash"></i>结束</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    const $detailDataTable = $detailTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listDetailUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                title: '活动名称',
                data: "title",
                width: "50px",
                orderable: false,
                name: "title"
            },
            {
                title: '优惠类型',
                width: "80px",
                orderable: false,
                name: "type",
                render: function (data, type, row) {
                    var type = row.type;
                    return typeMapper[type];
                }
            },
            {
                title: '买家',
                width: "100px",
                orderable: false,
                name: "buyer",
                render: function (data, type, row) {
                    var name = row.buyerName;
                    var phone = row.buyerPhone;
                    return (name ? name : '') + (phone ? '</br>' + phone : '');
                }
            },
            {
                title: '卖家',
                width: "100px",
                orderable: false,
                name: "seller",
                render: function (data, type, row) {
                    var name = row.sellerName;
                    var phone = row.sellerPhone;
                    return (name ? name : '') + (phone ? '</br>' + phone : '');
                }
            },
            {
                title: '订单号',
                data: "orderNo",
                width: "100px",
                orderable: false,
                name: "orderNo"
            },
            {
                title: '订单原价',
                data: "goodsFee",
                width: "50px",
                orderable: false,
                name: "goodsFee"
            },
            {
                title: '购买价',
                data: "totalFee",
                width: "50px",
                orderable: false,
                name: "totalFee"
            },
            {
                title: '优惠价',
                data: "discountFee",
                width: "50px",
                orderable: false,
                name: "discountFee"
            },
            {
                title: '开始时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validFrom"
            }, {
                title: '结束时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validTo"
            }, {
                title: '状态',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.archive) {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '发布时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            // manager.initTable();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});

