define(['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui',
  'select2', 'duallistbox'], function ($, utils) {

  const prefix = window.host + '/message';

  const listUrl = prefix + '/listByAdmin';
  const sendSmsUrl = prefix + '/sendNotification';
  const deleteUrl = prefix + '/delete';
  const editUrl = 'messageEdit';

  const $dataTable = $('#xquark_message_list_tables');
  const $agentModal = $("#modal_update_pred");
  const $body = $('body');

  const columns = ['name', 'type', 'created_at'];
  const smsTypeMapper = {
    'DELIVERY': '发货模板',
    'COMMON': '通用模板'
  };

  var $agentDualListBox;
  var agentList = [];
  var templateId = '';

  // 初始化短信类型为发货类型
  var notificationType = 'NOTIFICATION';

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const manager = (function () {

    const $addBtn = $('#add_button');
    const $sendSmsBtn = $('#send_sms_btn');
    const $agentDualListSelect = $('#agentDualListBox');

    const globalInstance = {
      bindEvent: function () {
        $addBtn.on('click', function () {
          window.location = 'messageEdit';
        });
        $sendSmsBtn.on('click', function () {
          var agentIds = $agentDualListSelect.val();
          utils.tools.confirm('将发送 ' + agentIds.length + '条消息，确认发送吗?',
              function () {
                utils.postAjaxWithBlock($(document), sendSmsUrl, {
                  msgId: templateId,
                  userIds: agentIds.join(',')
                }, function (res) {
                  if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                      case 200: {
                        utils.tools.alert('消息已发送',
                            {timer: 1200, type: 'success'});
                        $agentModal.modal('hide');
                        break;
                      }
                      default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                      }
                    }
                  } else if (res === 0) {

                  } else if (res === -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                  }
                });
              }, function () {
              });
        });
        return this;
      }
    };

    return {
      initGlobal: function () {
        globalInstance.bindEvent();
        return this;
      },
      initDualListBox: function () {
        utils.postAjaxWithBlock($(document), window.host + "/user/list",
            {showAll: true}, function (res) {
              if (typeof(res) === 'object') {
                switch (res.errorCode) {
                  case 200: {
                    agentList = res.data.list;
                    agentList.forEach(function (agent) {
                      var id = agent.id;
                      var name = agent.name;
                      var phone = agent.phone;
                      var $option = $('<option value="' + id + '">' + name
                          + ' : ' + phone + '</option>');
                      $agentDualListSelect.append($option);
                    });
                    $agentDualListBox = $(
                        'select[name="agentDualListBox"]').bootstrapDualListbox(
                        {
                          infoText: '未选中',
                          infoTextEmpty: '已选中',
                          filterPlaceHolder: '输入姓名或手机号',
                          infoTextFiltered: '4'
                        });
                    break;
                  }
                  default: {
                    utils.tools.alert(res.moreInfo, {timer: 1200});
                    break;
                  }
                }
              } else if (res === 0) {

              } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
              }
            });
        return this;
      },
      refreshDualListBox: function (data) {
        if ($agentDualListBox) {
          $agentDualListSelect.empty();
          if (data) {
            data.forEach(function (item) {
              var id = item.id;
              var name = item.name;
              var phone = item.phone;
              var $option = $('<option value="' + id + '">' + name + ' : '
                  + phone + '</option>');
              $agentDualListSelect.append($option);
            })
          }
          $agentDualListBox.bootstrapDualListbox('refresh', true);
        }
      },
      initTable: function () {

        $body.unbind('click');

        $('.edit_template').on('click', function () {
          var id = $(this).attr('rowId');
          window.location = editUrl + '?id=' + id;
        });

        $('.send_sms').on('click', function () {
          // templateId
          templateId = $(this).attr('rowId');
          $agentModal.modal('show');
          if (!$agentDualListBox) {
            manager.initDualListBox();
          }
        });
        /** 点击删除部门弹出框 **/
        $("[data-toggle='popover']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认删除？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="popover"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="popover"]').popover('hide');
        }).on('shown.bs.popover', function () {
          var that = this;
          $('.popover-btn-ok').on("click", function () {
            var id = $(this).attr("id");
            utils.postAjax(deleteUrl, {id: id}, function (res) {
              if (res === -1) {
                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
              }
              if (typeof res === 'object') {
                if (res.errorCode === 200) {
                  utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                  $smsDataTables.search('').draw();
                } else {
                  if (res.moreInfo) {
                    utils.tools.alert(res.moreInfo,
                        {timer: 1200, type: 'warning'});
                  } else {
                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                  }
                }
              }
            });
          });
          $('.popover-btn-cancel').on("click", function () {
            $(that).popover("hide");
          });
        });

        $body.on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "popover") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="popover"]').popover('hide');
          } else if (target.data("toggle") === "popover") {
            target.popover("toggle");
          }
        });
      }
    }
  })();

  manager.initGlobal();

  /** 初始化表格数据 **/
  const $smsDataTables = $dataTable.DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ajax: function (data, callback) {
      $.get(listUrl, {
        size: data.length,
        page: data.start / data.length,
        pageable: true,
        type: notificationType,
        order: columns[data.order[0].column],
        direction: data.order[0].dir
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo);
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    },
    rowId: 'id',
    columns: [
      {
        data: 'title',
        width: '50px',
        orderable: true,
        name: 'title'
      },
      {
        width: '100',
        orderable: true,
        render: function (data, type, row) {
          if (row.createdAt === null) {
            return '';
          }
          return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
        }
      },
      {
        sClass: "right",
        width: "100px",
        orderable: false,
        render: function (data, type, row) {
          var html = '';
          html += '<a href="javascript:void(0);" class="edit_template" rowId="'
              + row.id
              + '" fid="edit_template"><i class="icon-pencil7"></i>编辑</a>';
          html += '<a href="javascript:void(0);" class="send_sms" style="margin-left: 10px" rowId="'
              + row.id + '" fid="send_sms"><i class="icon-pencil7"></i>发送</a>';
          html += '<a href="javascript:void(0);" class="del_template" style="margin-left: 10px;" data-toggle="popover" rowId="'
              + row.id
              + '" fid="del_template"><i class="icon-trash"></i>删除</a>';
          return html;
        }
      }

    ],
    select: {
      style: 'multi'
    },
    drawCallback: function () {  //数据加载完成后初始化事件
      manager.initTable();
    }
  });

});
