define(['jquery', 'validate', 'utils', 'jquerySerializeObject', 'select2',
      'uniform'],
    function ($, validate, utils) {
      const moduleName = 'warehouse';
      const detailUrl = id => window.host + `/${moduleName}/${id}`;
      const saveUrl = window.host + `/${moduleName}/edit`;

      const addButton = $(`#add-${moduleName}`);
      const form = $(`#${moduleName}-form`);
      const modal = $(`#modal_add_${moduleName}`);
      const submitButton = $(`#submit-${moduleName}`);
      let updateTable = '';
      let isSubmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();

        addButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitForm = function (obj) {
        if (isSubmiting === true) {
          return;
        }
        if (!obj.id) {
          delete obj.id;
        }
        $.post(saveUrl, obj).done(date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(data => {
          modal.modal("hide");
          utils.tools.error("操作出错!");
        });
        isSubmiting = false;
      };

      const edit = function (id) {
        $.get(detailUrl(id)).done(date => {
          let obj = date.data;
          utils.tools.syncForm(form, obj);
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              form[0].reset();
              form.validate().resetForm();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = form.valid();
              if (result) {
                console.log("success valid");
                let obj = form.serializeObject();
                submitForm(obj);
              }
            });
            return this;
          },
          initValidate: function () {

            form.validate({
              rules: {
                name: {
                  required: true,
                },
              },
              messages: {
                name: {
                  required: "请输入仓库名称",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initValidate().initSubmit().resetForm();

      return {
        edit: edit,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });
