/**
 * Created by jitre on 18/2/8.
 */
define(['jquery', 'utils', 'datatables', 'select2', 'daterangepicker',
      'package/add'],
    function ($, utils, datatables, select2, daterangepicker, add) {
      const NOT_FOUND = "/_resources/images/404.png";
      const moduleName = 'package';
      const listUrl = window.host + `/${moduleName}/list`;
      const batchDeleteUrl = ids => window.host
          + `/${moduleName}/batchDelete?ids=${ids}`;
      const deleteUrl = id => window.host + `/${moduleName}/delete/${id}`;
      const addUrl = window.host + `/${moduleName}/edit`;

      const checkAll = '#checkAllGoods';
      const searchButton = $('#search');
      const publishButton = $("#publishCase");
      const modal = $(`#modal_add_${moduleName}`);
      const batchDeleteButton = $("#batchDel");
      const tableSource = '#guiderUserTable';
      var $datatables = '';

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          search: '<span>筛选:</span> _INPUT_',
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      /**
       *删除
       */
      const deleteComponent = function (pId) {
        $.get(deleteUrl(pId)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       * 批量删除
       * @param ids
       */
      const batchDelete = function (ids) {
        $.get(batchDeleteUrl(ids)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       *得到当前选中的行数据
       */
      const getTableContent = function () {
        let selectRows = new Array();
        for (let i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
          let value = {};
          let checkvalue = $("input[name='checkGoods']:checked")[i];
          value.id = $(checkvalue).attr("rowid");
          selectRows.push(value);
        }
        return selectRows;
      };

      /**
       * 更新表格
       */
      const updateTable = function () {
        let keyword = $.trim($("#keyWord").val());
        $datatables.search(keyword).draw();
      };

      const bindEvent = function () {
        /**
         *监听批删除
         */
        batchDeleteButton.on('click', function () {
          let updateds = getTableContent();
          if (updateds.length === 0) {
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return;
          }
          let ids = '';
          $.each(updateds, function (index, row) {
            if (ids != '') {
              ids += ',';
            }
            ids += row.id;
          });
          batchDelete(ids);
        });

        /**
         * 监听搜索
         */
        searchButton.on("click", function () {
          updateTable();
        });

        /**
         * 监听发布按钮的点击
         */
        publishButton.on("click", function () {
          window.location.href = addUrl;
        });

      };

      const bindDataTableEvent = function () {
        /**
         * 监听删除按钮的点击
         */
        $(".del").off("click").on('click', function () {
          let rowId = $(this).attr("rowId");
          utils.tools.confirm('确认删除吗？', function () {
            deleteComponent(rowId);
          }, function () {

          });
        });

        /**
         * 监听全选
         */
        $(checkAll).on('click', function () {
          $("input[name='checkGoods']").prop("checked",
              $(this).prop("checked"));
        });

        /**
         * 监听编辑按钮的点积
         */
        $(".edit").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          add.edit(id);
        });

      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          initDataTable() {
            $datatables = utils.createDataTable(tableSource, {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              ajax: function (data, callback, settings) {
                $.get(listUrl, {
                  size: data.length,
                  page: (data.start / data.length),
                  keyWord: data.search.value,
                  pageable: true,
                }, function (res) {
                  if (!res.data.list) {
                    res.data.list = [];
                  }
                  callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                  });
                });
              },
              columns: [
                {
                  title: '<label class="checkbox"><input id="checkAllGoods" name="checkAllGoods" type="checkbox" class="styled"></label>',
                  orderable: false,
                  render: function (data, type, row) {
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                        + row.id + '" value="' + row.id + '"></label>';
                  }
                },
                {
                  title: '名称',
                  data: 'name',
                  name: 'name',
                  sortable: false
                }, {
                  title: '重量(g)',
                  data: 'weight',
                  sortable: false
                }, {
                  title: '长度(mm)',
                  data: 'length',
                  sortable: false
                }, {
                  title: '宽度(mm)',
                  data: 'width',
                  sortable: false
                }, {
                  title: '高度(mm)',
                  data: 'height',
                  sortable: false
                }, {
                  title: '价格(元)',
                  data: 'price',
                  sortable: false
                }, {
                  title: '最后更新时间',
                  orderable: false,
                  render: function (data, type, row) {
                    let cDate = parseInt(row.updatedAt);
                    let d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  },
                  name: "updatedAt"
                }, {
                  sortable: false,
                  title: '操作',
                  render: function (data, type, row) {
                    let html = '';
                    html += `<a href="javascript:void(0);" class="edit role_check_table" rowId=${row.id} fid="edit_audit_order"><i class="icon-pencil7"></i>编辑</a>`;
                    html += `<a href="javascript:void(0);" class="del role_check_table" rowId=${row.id} fid="delete_audit_order"><i class="icon-trash"></i>删除</a>`;
                    return html;
                  }
                }],
              drawCallback: bindDataTableEvent,
            });
            return this;
          },
          initCallBack: function () {
            add.setUpdateFunc(updateTable);
            return this;
          }
        }
      })();

      eventInitialization.init().initDataTable().initCallBack();

    });