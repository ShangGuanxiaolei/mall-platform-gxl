define(['jquery', 'utils', 'jquerySerializeObject', 'select2',
  'ckEditor', 'blockui'], function ($, utils, selector) {

  const prefix = window.host + '/message';

  const saveUrl = prefix + '/save';
  const viewUrl = prefix + '/view';

  const id = utils.tools.request('id');

  const manager = (function () {

    // 保存按钮
    const $btnSave = $('.btn-submit');
    // 保存并发送
    const $btnSaveAndPush = $('.btn-push');

    const $btnSaveUserList = $('.btn-user-list');

    const $sendScope = $('input[name=scope]');

    const $modalSelectUser = $('#select_user_modal');

    const $form = $('#message_info_form');

    /**
     * 返回一个函数，通过用户数组生成 option
     * @param userList user数据
     * @returns {Function} 包含userList的生成option函数
     */
    const optionSupplier = function (userList) {
      return function () {
        return userList.map(function (user) {
          var id = user.id;
          var name = user.name;
          var phone = user.phone;
          return $('<option value="' + id + '">' + name + ' : ' + phone
              + '</option>');
        });
      };
    };

    return {
      initGlobal: function () {
        if (id && id !== '') {
          utils.postAjaxWithBlock($(document), viewUrl, { id : id }, function(res) {
              if (typeof(res) === 'object') {
                  switch (res.errorCode) {
                      case 200:
                      {
                          const data = res.data;
                          utils.tools.syncForm($form, data);
                          break;
                      }
                      default:
                      {
                          utils.tools.alert(res.moreInfo, {timer: 1200});
                          break;
                      }
                  }
              } else if (res === 0) {
              } else if (res === -1) {
                  utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
              }
          });
        }

        $btnSave.on('click', function (e) {
          e.preventDefault();
          var data = $form.serializeObject();
          data.id = id;
          utils.postAjax(saveUrl, data , function (res) {
              if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
              }
              if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                  } else {
                      if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                      } else {
                          utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                      }
                  }
              }
          });
        });
        return this;
      },
      initEditor: function () {
        $("#content").ckeditor();
        return this;
      }
    }
  })();

  manager.initGlobal()
  .initEditor();

});