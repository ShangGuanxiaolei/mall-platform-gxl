/**
 * Created by chh on 17/1/20.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/skuAttribute/list";

    var $itemListUrl = window.host + "/skuAttribute/item/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    var $attributeId = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#name").val('');
        $("#idx").val('');
        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
        $("#modal_component").modal("show");
    });

    buttonRoleCheck('.hideClass');

    var $datatables = utils.createDataTable('#xquark_list_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ordering: false,
        sortable: false,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                title:"名称",
                data:'name',
                name:'name',
                sortable: false
            },{
                title:"排序",
                data:'idx',
                name:'idx',
                sortable: false
            }, {
                title:"发布时间",
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                title:"管理",
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_audit_order"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="items" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7"></i>属性值管理</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_audit_order"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $itemdatatables = $('#xquark_list_item_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ordering: false,
        sortable: false,
        ajax: function(data, callback, settings) {
            $.get($itemListUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                id: $attributeId
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                title:"名称",
                data:'name',
                name:'name',
                sortable: false
            },{
                title:"排序",
                data:'idx',
                name:'idx',
                sortable: false
            }, {
                title:"发布时间",
                orderable: false,
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                title:"管理",
                sClass: "right",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_audit_order"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_audit_order"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initItemEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        // 属性值管理
        $(".items").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $attributeId = rowId;
            $itemdatatables.search('').draw();
            $("#modal_item_list").modal("show");
        });

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           $.ajax({
                url: window.host + '/skuAttribute/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#name").val(role.name);
                        $("#idx").val(role.idx);
                        $("#modal_component").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
           });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });

    }

    function initItemEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
            var id =  $(this).attr("rowId");
            $.ajax({
                url: window.host + '/skuAttribute/item/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#item_id").val(role.id);
                        $("#item_name").val(role.name);
                        $("#item_idx").val(role.idx);
                        $("#item_attribute_id").val(role.attributeId);
                        $("#modal_item").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteItemComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });

    }


    /*手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/skuAttribute/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    /*手动删除*/
    function  deleteItemComponent(pId){
        var url = window.host + "/skuAttribute/item/delete/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $itemdatatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    // 保存属性
    $(".saveBtn").on('click', function() {
        var url = window.host + '/skuAttribute/save';
        var id = $("#id").val();
        var name = $("#name").val();
        var idx = $("#idx").val();
        if(!name || name == '' ){
            utils.tools.alert("请输入名称!", {timer: 1200, type: 'warning'});
            return;
        }else if(!idx || idx == '' ){
            utils.tools.alert("请输入排序!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            name: name,
            idx: idx
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                if(res.rc==1){
                    alert(res.msg);
                    $("#modal_component").modal("hide");
                    $datatables.search('').draw();
                }else{
                    utils.tools.alert(res.msg, {timer: 1200});
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    // 保存属性值
    $(".saveItemBtn").on('click', function() {
        var url = window.host + '/skuAttribute/item/save';
        var id = $("#item_id").val();
        var name = $("#item_name").val();
        var idx = $("#item_idx").val();
        var attributeId = $("#item_attribute_id").val();
        if(!name || name == '' ){
            utils.tools.alert("请输入名称!", {timer: 1200, type: 'warning'});
            return;
        }else if(!idx || idx == '' ){
            utils.tools.alert("请输入排序!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            name: name,
            idx: idx,
            attributeId: attributeId
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                if(res.rc==1){
                    alert(res.msg);
                    $("#modal_item").modal("hide");
                    $itemdatatables.search('').draw();
                }else{
                    utils.tools.alert(res.msg, {timer: 1200});
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    // 新增属性值
    $(".btn-additem").on('click', function() {
        $("#item_id").val('');
        $("#item_name").val('');
        $("#item_idx").val('');
        $("#item_attribute_id").val($attributeId);
        $("#modal_item").modal("show");
    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});