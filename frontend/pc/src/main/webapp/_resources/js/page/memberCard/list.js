define(['jquery', 'utils', 'datatables', 'blockui', 'select2'], function ($, utils, datatabels, blockui, select2) {

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    const listCards = '/sellerpc/member/card/list';
    const listProducts = window.host + "/product/list";
    const listDiscountProducts = "/sellerpc/member/card/productList";
    var shopId = null;
    var cardId = '';
    var category = '';
    var order = '';

    const upgradeTypeMapping = {
        AUTO: '自动升级',
        MANUAL: '手动升级',
        BUY: '购买'
    };

    const $selectMemberCards = $('#selectMemberCards');

    const columns = ['name', 'level', 'upgrade_type', 'free_delivery'];

    const block = {
        fullBlock: function () {
            return $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        },
        unBlock: function () {
            return $.unblockUI();
        }
    };

    $('#addMemberCard').on('click', function () {
        window.location = '/sellerpc/member/editCard';
    });

    // 新增商品佣金
    $('#addDiscountProduct').on('click', function () {
        $("#modal_member_products").modal("show");
    });

    $('.btn-save-card').on('click', function () {
        $('#modal_product_level_setting').modal('hide');
        $('#modal_member_products').modal('hide');
    });


    const $cardTables = $('#xquark_cards_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listCards, {
                size: data.length,
                page: (data.start / data.length) + 1,
                order: columns[data.order[0].column],
                direction: data.order[0].dir,
                cardId: cardId
                //memberLevel: memberLevel.val()
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载出错');
                    return;
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            });
        },
        rowId: "id",
        columns: [
            {
                data: "name",
                width: "100px",
                orderable: true,
                name: "name"
            },
            {
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var level = row.level;
                    return level ? level : '默认';
                },
                name: 'level'
            },
            {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    var upgradeType = row.upgradeType;
                    var html = '';
                    if (upgradeType in upgradeTypeMapping) {
                        html += upgradeTypeMapping[upgradeType];
                    } else {
                        html += '无';
                    }
                    html += '</br>';
                    // html += '<a>规则详情</a>';
                    return html;
                },
                name: "upgradeType"
            },
            {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    // 折扣、是否包邮
                    var discount = row.discount;
                    var isDelivery = row.freeDelivery;
                    var html = '';
                    html += discount + '折';
                    html += '</br>';
                    html += isDelivery ? '包邮' : '不包邮';
                    return html;
                },
                name: "basicRights"
            },
            {
                width: "150px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="view_member" rowId="' + row.id + '" >查看会员</a>';
                    html += '<a href="javascript:void(0);" class="edit_member" style="margin-left: 10px;" rowId="' + row.id + '" >编辑</a>';
                    html += '<a href="javascript:void(0);" class="delete_member" style="margin-left: 10px;" rowId="' + row.id + '" >删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        order: [[3, 'asc']],
        drawCallback: function () {  //数据加载完成
            initTableEvent();
        }
    });

    function initTableEvent() {

        $('.view_member').on('click', function () {
            var id = $(this).attr('rowId');
            window.location = 'list?memberLevel=' + id;
        });

        $('.edit_member').on('click', function () {
            var id = $(this).attr('rowId');
            window.location = 'editCard?id=' + id;
        });

        $('.delete_member').on('click', function () {

        });

    }

    var $discountDatatables = $('#xquark_discount_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listDiscountProducts, {
                size: data.length,
                page: (data.start / data.length) + 1,
                pageable: true,
                direction: data.order ? data.order[0].dir : 'asc'
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<img class="goods-image" src="' + row.img + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            },
            {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                orderable: true,
                width: "50px",
                name: "cards",
                render: function (data, type, row) {
                    var cards = row.memberCards;
                    var html = '';
                    if (cards && cards.length > 0) {
                        cards.forEach(function (card) {
                            html += '<span>' + card.name + ':  <font>' + card.discount + '折 </font></span></br>'
                        })
                    } else {
                        html += '无';
                    }
                    return html;
                }
            },
            {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="edit_discount_product" style="margin-left: 10px;" rowId="' + row.id + '"  productImg="' + row.img + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="delete_discount_product" style="margin-left: 10px;" rowId="' + row.id + '" ><i class="icon-pencil7" ></i>删除</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            $('.delete_discount_product').on('click', function () {
                var url = "/sellerpc/member/card/delete";
                var productId = $(this).attr('rowId');
                utils.postAjax(url, {productId: productId}, function (res) {
                    if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                            utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                            $discountDatatables.search('').draw();
                        } else {
                            if (res.moreInfo) {
                                utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                            } else {
                                utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                            }
                        }
                    }
                });
            });
            $('.edit_discount_product').on('click', bindProductModalShow);
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listProducts, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if (order != '') {
                        return order;
                    } else {
                        var _index = data.order[0].column;
                        if (_index < 4) {
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
            }, function (res) {
                if (res.data && !res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.categoryTotal,
                    iTotalDisplayRecords: res.data.categoryTotal
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="' + row.id + '" productImg="' + row.imgUrl + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectProductEvent();
        }
    });

    function initSelectProductEvent() {
        $(".selectproduct").on("click", bindProductModalShow);
    }

    function bindProductModalShow() {
        var productId = $(this).attr("rowId");
        var productName = $(this).attr("productName");
        var productImg = $(this).attr("productImg");
        var productPrice = $(this).attr("productPrice");
        $("#card_productName").html(productName);
        $("#card_productPrice").html(productPrice);
        $('#productId').val(productId);
        $("#card_productImg").attr("src", productImg);

        // 初始化会员卡数据
        utils.postAjaxWithBlock($(document), '/sellerpc/member/card/listVO', {productId: productId}, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        var memberCards = res.data.list;
                        $selectMemberCards.empty();
                        if (memberCards && memberCards.length > 0) {
                            memberCards.forEach(function (card, index) {
                                var html = $('<label class="checkbox-inline">' +
                                    '<input type="checkbox" class="memberCard" name="memberCardsType" value="' + card.id + '" />' +
                                    '' + card.name + ' </label>');
                                if (card.checked) {
                                    html.find('input').attr('checked', 'checked');
                                }
                                $selectMemberCards.append(html);
                                if ((index + 1) % 4 === 0) $selectMemberCards.append('</br>')
                            });
                            $('.memberCard').change(function () {
                                var cardId = $(this).val();
                                var productId = $('#productId').val();
                                var url = this.checked ?
                                    '/sellerpc/member/card/bindProduct' : '/sellerpc/member/card/unbindProduct';
                                utils.postAjax(url, {productId: productId, cardId: cardId}, function (res) {
                                    if (typeof(res) === 'object') {
                                        console.log(res);
                                        $discountDatatables.search('').draw();
                                    }
                                });
                            })
                        }
                        break;
                    }
                    default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res === 0) {

            } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
        // $("#commission_Id").val('');
        // $("#commission_firstLevelRate").val('');
        // $("#commission_secondLevelRate").val('');
        // $("#commission_thirdLevelRate").val('');
        $("#modal_product_level_setting").modal("show");
    }
});

