/**
 * Created by jitre on 7/4/18.
 */
define(['jquery', 'utils', 'utils/fileUploader','sync/dataQuery', 'ramda', 'jquerySerializeObject'],
    function ($,utils, uploader,data, R) {

  // 临时处理, 只适用于表单上只有一个导入按钮的情况
  const fileUploadUrl = jobType => window.host + `/file/upload?type=DOC&jobType=${R.defaultTo('', jobType)}`;
  const dropZoneDom='#import';
  const form=$("#uploader-form");
  const modal=$("#modal-import-excel");
  let dropZoneInstance='';

  let eventInitialization = (function () {
    return {
      init: function () {
        return this;
      },
      listenSyncButton:function(){
        $("#sync").off("click").on("click",function(){
          if(dropZoneInstance.dropZone.getUploadingFiles().length>0){
            utils.tools.error("请等待上传完成!");
            return ;
          }
          let obj = form.serializeObject();
          if(!obj.key){
            utils.tools.error("请先上传!");
            return ;
          }
          data.sync(obj.key,obj.type,obj.jobId);
          modal.modal("hide");
        });
        return this;
      },
      listenImportButton:function(){
        $(".import").off("click").on("click",function(){
          let type=$(this).attr("data-type");
          $("input[name=type]").prop("value",type);
          modal.modal("show");
        });
        return this;
      },
      resetForm: function () {
        modal.on('hidden.bs.modal', function (e) {
          form[0].reset();
          dropZoneInstance.clear();
        });
        return this;
      },
      initDropZone:function(){
          const params = {
            dom: dropZoneDom,
            url: fileUploadUrl($('.import').attr('job-type')),
            allowTypes: '.xls,.xlsx',
            maxSize: 10,
            maxFiles: 1,
            onSuccess: (file) => {
              $("input[name=key]").prop("value",file.url);
              $("input[name=jobId]").prop("value", file.jobId);
              console.log(file);
            },
            onRemove: (file) => {
              console.log(file);
            }
          };
          dropZoneInstance = uploader.create(params);
          return this;
        },
    }
  })();
  //init
  eventInitialization.init().resetForm().initDropZone().listenImportButton().listenSyncButton();
});
