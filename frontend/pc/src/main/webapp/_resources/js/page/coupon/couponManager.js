/**
 * Created by chh on 17/07/20.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/auditRule/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function() {
        $("#id").val('');
        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
        $("#modal_component").modal("show");
    });

    buttonRoleCheck('.hideClass');

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ordering: false,
        sortable: false,
        ajax: function(data, callback, settings) {
            $.get('/bos/couponManager/list', {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns : [ [ {
            field : 'id',
            checkbox: true
        },{
            width : '180',
            title : '编号',
            align: 'left',
            field : 'batchNo'
        },{
            width : '120',
            title : '发行日期',
            align: 'center',
            field : 'distributedAt',
            formatter: function(value,row,index){
                var d = new Date(parseInt(value));
                return d.format('yyyy-MM-dd');
            }
        },{
            width : '150',
            title : '店铺',
            align: 'center',
            field : 'shopName'
        },{
            width : '70',
            title : '总数',
            align: 'center',
            field : 'amount'
        },{
            width : '40',
            title : '面值',
            align: 'center',
            field : 'discount'
        },{
            width : '55',
            title : '剩余数量',
            align: 'center',
            field : 'remainder'
        },{
            width : '70',
            title : '总金额',
            align: 'center',
            field : 'totalDiscount'
        },{
            width : '160',
            align: 'center',
            title : '时效',
            field : 'validFrom',
            formatter: function(value,row,index){
                if(row.validFrom != null && row.validTo !=null){
                    var f = new Date(parseInt(row.validFrom));
                    var t = new Date(parseInt(row.validTo));
                    var from = f.format('yyyy-MM-dd');
                    var to = t.format('yyyy-MM-dd');
                    return from + " ~ "  + to;
                }
            }
        },{
            width : '60',
            title : '状态',
            align: 'center',
            field : 'status',
            formatter: function(value,row,index){
                var d;
                switch(value){
                    case "VALID":
                        d="<font color=\"green\">可使用</font>";
                        break;
                    case "OVERDUE":
                        d="<font color=\"grey\">过期</font>";
                        break;
                    case "USED":
                        d="<font color=\"red\">已使用</font>";
                        break;
                    case "LOCKED":
                        d="<font color=\"red\">已有订单</font>";
                        break;
                    case "CLOSED":
                        d="<font color=\"black\">关闭</font>";
                        break;
                    default:
                        d="";
                        break;
                }
                return d;
            }
        }, {
            width: '180',
            title: '操作',
            align: 'left',
            field: 'opt',
            formatter: function(value,row,index){
                var showView = "<input type='button' onclick=\"jsShopQRCode('"+row.id+"','"+row.shopId+"','"+row.shopName+"','"+row.amount+"','"+row.discount+"','"+row.validFrom+"','"+row.validTo+"')\" value='二维码' style='margin-left: 10px;width:55px;'/>"
                    + "<input type='button' onclick=\"jsPreView('"+row.name+"','"+row.shopName+"','"+row.remainder+"','"+row.amount+"','"+row.discount+"','"+row.validFrom+"','"+row.validTo+"','"+row.applyAbove+"')\" value=' 预 览 ' style='margin-left: 10px;width:55px;'/>"
                return showView
            }
        }
        ]],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           $.ajax({
                url: window.host + '/auditRule/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#buyerType").val(role.buyerType);
                        $("#sellerType").val(role.sellerType);
                        $("#auditType").val(role.auditType);
                        $("#modal_component").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
           });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });

    }

    var jsShopQRCode = function(couponId,shopId,shopName,amount,discount,validFrom,validTo){
        $('#shopQRCode').dialog({
            title: '店铺优惠劵二维码',
            closed:true,
            buttons:[{
                text:'关闭',
                iconCls:'icon-save',
                handler:function(){ jsQRCodeClose(); }
            }]
        });
        $('#span_shop_name').html(shopName);
        $('#qrcode_amount').html(amount);
        $('#qrcode_discount').html(discount);
        var f = new Date(parseInt(validFrom));
        var t = new Date(parseInt(validTo));
        var from = f.format('yyyy-MM-dd');
        var to = t.format('yyyy-MM-dd');
        $('#qrcode_valid').html(from + "~" + to);
        var _path = "/bos/couponManager/shop/qrImage/"+shopId+"/"+couponId;
        $('#qrCodeImage').attr('src',_path);
        $('#shopQRCode').dialog('open');
    }

    var jsPreView = function(couponName,shopName,remainder,amount,discount,validFrom,validTo,applyAbove){
        $('#couponPreView').dialog({
            title: '店铺优惠劵预览',
            closed:true,
            buttons:[{
                text:'关闭',
                iconCls:'icon-save',
                handler:function(){ jsPreViewlose(); }
            }]
        });
        $('#coupon-money').html(discount);
        if ( couponName != null && couponName != 'null' ) {
            $('#coupon-name').html(couponName);
        }
        $('#coupon-sn').html(shopName);
        $('#coupon-apply-above').html(applyAbove);
        var f = new Date(parseInt(validFrom));
        var t = new Date(parseInt(validTo));
        var from = f.format('yyyy-MM-dd');
        var to = t.format('yyyy-MM-dd');
        $('#coupon-vf').html(from);
        $('#coupon-vt').html(to);
        $('#coupon-remainder').html(amount-remainder);
        $('#couponPreView').dialog('open');
    }


    /*手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/auditRule/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存
    $(".saveBtn").on('click', function() {
        var url = window.host + '/auditRule/save';
        var id = $("#id").val();
        var buyerType = $("#buyerType").val();
        var sellerType = $("#sellerType").val();
        var auditType = $("#auditType").val();
        var type = $("#type").val();
        if(!type || type == '' ){
            utils.tools.alert("请选择规则类型!", {timer: 1200, type: 'warning'});
            return;
        }else if(!buyerType || buyerType == '' ){
            utils.tools.alert("请选择买家级别!", {timer: 1200, type: 'warning'});
            return;
        }else if(!sellerType || sellerType == ''){
            utils.tools.alert("请选择卖家级别!", {timer: 1200, type: 'warning'});
            return;
        }else if(!auditType || auditType == ''){
            utils.tools.alert("请选择审核类型!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            type: type,
            buyerType: buyerType,
            sellerType: sellerType,
            auditType: auditType
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                if(res.rc==1){
                    alert(res.msg);
                    window.location.href = window.originalHost + '/auditRule/list';
                }else{
                    utils.tools.alert(res.msg, {timer: 1200});
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});