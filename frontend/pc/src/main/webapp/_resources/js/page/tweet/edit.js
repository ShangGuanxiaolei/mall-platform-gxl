/**
 * Created by chh on 16/10/19.
 */
define(['jquery', 'utils', 'product/upload', 'select2'],
    function ($, utils, uploader, select2) {

    var $category = '';

    var $orders = ['price','amount','sales','onsale'];

    var $order = '';

    var $productlistUrl = window.host + "/product/list";

    var $viewkeyword = '';

    var $grouponkeyword = '';

    var $pageId = utils.tools.request('pId');  //商品Id
    $(".btn-submit").on('click', function() {
        save();
    });

    $('body').on('change','select[name="categoryType"]', function(event) {
            $productlistUrl = window.host + "/product/list";
            $category = $(this).val();
            $selectproductdatatables.search('').draw();
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
    });

    //初始化商品分类信息
    $.ajax({
            url: window.host + '/shop/category/list',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.errorCode == 200) {
                    var dataLength = data.data.length;
                    for (var i = 0; i < dataLength; i++) {
                        $("#categoryType").append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
                    };
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                }
            },
            error: function (state) {
                if (state.status == 401) {
                } else {
                    utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
                }
            }
    });

    // 跳转类型改变后，清空之前选择的跳转目标
    $("#targetType").on('change', function() {
        $("#target").val('');
        $("#targetName").val('');
    });

    // 选择跳转目标
    $("#targetName").on('focus', function() {
        var targetType = $("#targetType").val();
        // 跳转目标为商品，则弹出商品选择界面
        if(targetType == 'PRODUCT'){
            $selectproductdatatables.search('').draw();
            $("#modal_select_products").modal("show");
        }
        // 自定义，则弹出自定义选择界面
        else if(targetType == 'CUSTOMIZED'){
            $selectviewdatatables.search('').draw();
            $("#modal_select_views").modal("show");
        }
        // 团购，则弹出团购选择界面
        else if(targetType == 'GROUPON'){
            $selectgroupondatatables.search('').draw();
            $("#modal_select_groupon").modal("show");
        }

    });

    //发现信息修改
    if ($pageId) {
        $("#pageId").val($pageId);

        $.ajax({
            url: window.host + '/tweet/' + $pageId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.errorCode == 200) {

                    var tweet = data.data;
                    //商品图片
                    if (tweet.imgs) {
                        $.each(tweet.imgs, function (i, img) {
                            var key = img.img,
                                url = img.imgUrl,
                                img = {
                                    imgUrl: url,
                                    id: key
                                };
                            addProductImage(img);
                        });
                    }
                    $("#title").val(tweet.title);
                    $("#description").val(tweet.description);
                    $("#target").val(tweet.target);
                    $("#targetName").val(tweet.targetName);
                    $("#targetType").val(tweet.targetType);
                    /** 初始化选择框控件 **/
                    $('.select').select2({
                        minimumResultsForSearch: Infinity,
                    });

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    } else { //发布商品操作

    }

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get($productlistUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    order: function () {
                        if($order != ''){
                            return $order;
                        }else{
                            var _index = data.order[0].column;
                            if ( _index < 3){
                                return '';
                            } else {
                                return $orders[_index - 3];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir :'asc',
                    category : $category,
                    isGroupon : ''
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.categoryTotal,
                        iTotalDisplayRecords:res.data.categoryTotal
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                    }
                },
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.status)
                        {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "price",
                    width: "50px",
                    orderable: true,
                    name:"price"
                }, {
                    data: "amount",
                    orderable: true,
                    width: "50px",
                    name:"amount"
                },
                {
                    data: "sales",
                    orderable: true,
                    width: "50px",
                    name:"sales"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"onsaleAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    var $selectviewdatatables = $('#xquark_select_views_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/viewPage/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: $viewkeyword,
                    keyword: data.search.value,
                    pageable: true
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    data: "title",
                    width: "120px",
                    orderable: false,
                    name:"title"
                },{
                    data: "description",
                    width: "120px",
                    orderable: false,
                    name:"description"
                },{
                    width: "60px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.pageType)
                        {
                            case 'TWEET':
                                status = '发现';
                                break;
                            case 'GROUPON':
                                status = '拼团';
                                break;
                            case 'FLASHSALE':
                                status = '特卖';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.title+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    var $selectgroupondatatables = $('#xquark_select_groupon_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/promotionGroupon/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    status: '',
                    keywords : $grouponkeyword
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
               {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<img class="goods-image" src="'+row.productImg+'" />';
                    }
                },
                {
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "80px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.archive)
                        {
                            case false:
                                status = '进行中';
                                break;
                            case true:
                                status = '已结束';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "productPrice",
                    width: "50px",
                    orderable: false,
                    name:"price"
                }, {
                    data: "discount",
                    width: "80px",
                    orderable: false,
                    name:"discount"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validFrom);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"validFrom"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validTo);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"validTo"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.productName+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    function initSelectTargetEvent(){
            $(".selectTarget").on("click",function(){
                var target =  $(this).attr("rowId");
                var targetName =  $(this).attr("name");
                $("#target").val(target);
                $("#targetName").val(targetName);
                $("#modal_select_products").modal("hide");
                $("#modal_select_views").modal("hide");
                $("#modal_select_groupon").modal("hide");
            });
    }

    //添加图片
    function addProductImage(img) {
        if (img) {
            var mockFile = {name: "", size: "", dataImg: img.id};
            uploader[0].dropzone.emit("addedfile", mockFile);
            uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
            uploader[0].dropzone.emit("complete", mockFile);
        }
    }

    //保存
    function save(){
            var imgArr = [];
            $('#product_image_dropzone > .dz-complete').each(function(i, item) {
                imgArr.push($(item).attr('data-img'));
            });

            var pId = $("#pageId").val();
            var title = $("#title").val();
            var description = $("#description").val();
            var target = $("#target").val();
            var targetType = $("#targetType").val();

            if(!title || title == '' ){
                utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
                return;
            }else if(!targetType || targetType == ''){
                utils.tools.alert("请选择跳转类型!", {timer: 1200, type: 'warning'});
                return;
            }else if(!target || target == ''){
                utils.tools.alert("请选择跳转目标!", {timer: 1200, type: 'warning'});
                return;
            }else if(imgArr.length == 0){
                utils.tools.alert("请选择图片!", {timer: 1200, type: 'warning'});
                return;
            }

            var param = {
                id:pId,
                imgs: imgArr.join(','),
                title: title,
                description: description,
                target: target,
                targetType: targetType
            };

            $.ajax({
                url: host + '/tweet/save',
                type: 'POST',
                data: param,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/tweet/list';
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        }

    $(".btn-search-products").on('click', function() {

            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null){
                $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
                $selectproductdatatables.search( keyword ).draw();
            }else if (keyword == '' || keyword.length == 0 ){
                $productlistUrl = window.host + "/product/list";
                $selectproductdatatables.search('').draw();
            }


    });

    $(".btn-search-views").on('click', function() {
        var keyword = $.trim($("#sviewKeyword").val());
        $viewkeyword = keyword;
        $selectviewdatatables.search( keyword ).draw();
    });

    $(".btn-search-groupon").on('click', function() {
        var keyword = $.trim($("#select_groupon_sKeyword").val());
        $grouponkeyword = keyword;
        $selectgroupondatatables.search( keyword ).draw();
    });

});