define(['jquery', 'validate', 'utils', 'jquerySerializeObject', 'select2',
      'uniform'],
    function ($, validate, utils) {
      const moduleName = 'package';
      const detailUrl = id => window.host + `/${moduleName}/${id}`;
      const saveUrl = window.host + `/${moduleName}/edit`;

      const addPackageButton = $(`#add-${moduleName}`);
      const packageForm = $(`#${moduleName}-form`);
      const modal = $(`#modal_add_${moduleName}`);
      const submitButton = $(`#submit-${moduleName}`);
      const isShowInput = $("#isShow");
      let updateTable = '';
      let isSbmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();

        /**
         * 点击新增商品
         */
        addPackageButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitPackage = function (brand) {
        if (isSbmiting === true) {
          return;
        }
        if (!brand.id) {
          delete brand.id;
        }
        $.post(saveUrl, brand).done(date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(data => {
          modal.modal("hide");
          utils.tools.error("操作出错!");
        });
        isSbmiting = false;
      };

      const editPackage = function (id) {
        $.get(detailUrl(id)).done(date => {
          let brand = date.data;
          utils.tools.syncForm(packageForm, brand);
          brand.isShow === true ? isShowInput.prop("checked", true)
              : isShowInput.prop("checked", false);
          $.uniform.update();
          imageInput.prop("value", brand.logo);
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              packageForm[0].reset();
              packageForm.validate().resetForm();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = packageForm.valid();
              if (result) {
                console.log("success valid");
                let packageObj = packageForm.serializeObject();
                packageObj.isShow = !!isShowInput.prop("checked");
                packageObj.source = "CLIENT";
                submitPackage(packageObj);
              }
            });
            return this;
          },
          initValidate: function () {

            packageForm.validate({
              rules: {
                name: {
                  required: true,
                },
                weight: {
                  required: true,
                  min: 0,
                  digits: true,
                },
                width: {
                  required: true,
                  min: 0,
                  digits: true,
                },
                height: {
                  required: true,
                  min: 0,
                  digits: true,
                },
                length: {
                  required: true,
                  min: 0,
                  digits: true,
                },
                price: {
                  required: true,
                  min: 0,
                  number: true,
                },
              },
              messages: {
                name: {
                  required: "请输入箱规名称",
                },
                weight: {
                  required: "请输入箱子重量",
                  min: "最小值不能小于0",
                  digits: "重量必须为整数",
                },
                width: {
                  required: "请输入箱子的宽度",
                  min: "最小值不能小于0",
                  digits: "宽度必须为整数",
                },
                height: {
                  required: "请输入箱子的高度",
                  min: "最小值不能小于0",
                  digits: "高度必须为整数",
                },
                length: {
                  required: "请输入箱子的长度",
                  min: "最小值不能小于0",
                  digits: "长度必须为整数",
                },
                price: {
                  required: "请输入箱子的价格",
                  min: "最小值不能小于0",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initValidate().initSubmit().resetForm();

      return {
        edit: editPackage,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });
