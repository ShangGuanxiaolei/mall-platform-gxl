define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            ordering: false,
            sortable: false,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/antifake/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo:$("#orderNo").val(),
                    code:$("#code").val(),
                    pageable: true,
                }, function(res) {
                    if(res.errorCode != 200){
                        utils.tools.alert(res.moreInfo, {timer: 4000});
                    }else{
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords:res.data.total,
                            iTotalDisplayRecords:res.data.total
                        });
                    }
                });
            },
            rowId: 'id',
            columns: [{
                width:"80px",
                title: '物流码',
                data: 'code',
                name: 'code'
            },{
                width:"80px",
                title: '物流码类型',
                render: function (data, type, row) {
                    var value = row.codeType;
                    if (value == 'TRUNK') {
                        return "箱标";
                    } else if (value == 'BOX') {
                        return "盒标";
                    }
                }
            },{
                width:"80px",
                title: '订单号',
                data: 'orderNo',
                name: 'orderNo',
                render: function (data, type, row) {
                    var html = '<a class="viewDetailBtn" href="javascript:void(0)" style="margin-right: 10px;" orderNo="'+row.orderNo+'" > ' + row.orderNo + '</a>';
                    return html;
                }
            }, {
                width: 75,
                title: '发货时间',
                sortable: false,
                render: function (data, type, row) {
                    if (row.createdAt == null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:"80px",
                title: '发货代理',
                data: 'sellerName',
                name: 'sellerName'

            }, {
                width: 75,
                data: 'sellerType',
                name: 'sellerType',
                title: '发货代理级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.sellerType;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"80px",
                title: '发货代理手机号',
                data: 'sellerPhone',
                name: 'sellerPhone'

            },{
                width:"80px",
                title: '进货代理',
                data: 'buyerName',
                name: 'buyerName'

            }, {
                width: 75,
                data: 'buyerType',
                name: 'buyerType',
                title: '进货代理级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.buyerType;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"80px",
                title: '进货代理手机号',
                data: 'buyerPhone',
                name: 'buyerPhone'

            },{
                width:"80px",
                title: '发货操作员',
                data: 'userName',
                name: 'userName'
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        //查看订单详情
        var orderNo = '';
        var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering:false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitter/order/orderItem", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo : orderNo,
                    pageable: true,
                }, function(res) {
                    if (res.errorCode != '200') {
                        return;
                    }

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.itemTotal,
                        recordsFiltered: res.data.itemTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.itemTotal,
                        iTotalDisplayRecords:res.data.itemTotal
                    });
                });
            },
            columns: [{
                data: 'productName',
                name: 'productName',
                width: 180,
                orderable: false,
                title: "商品"
            },{
                data: 'marketPrice',
                name: 'marketPrice',
                width: 100,
                orderable: false,
                title: "原价"
            },{
                data: 'price',
                name: 'price',
                width: 100,
                orderable: false,
                title: "订单价格"
            },{
                data: 'amount',
                name: 'amount',
                width: 100,
                orderable: false,
                title: "数量"
            },{
                data: 'addressDetails',
                name: 'addressDetails',
                width: 180,
                orderable: false,
                title: "收货地址"
            },{
                data: 'logisticsCompany',
                name: 'logisticsCompany',
                width: 120,
                orderable: false,
                title: "物流公司"
            },{
                data: 'logisticsOrderNo',
                name: 'logisticsOrderNo',
                width: 120,
                orderable: false,
                title: "物流单号"
            }, {
                width: "120px",
                orderable: false,
                title: "",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
            }, {
                width: "120px",
                orderable: false,
                title: "审核时间",
                render: function (data, type, row) {
                    if(row.shippedAt && row.shippedAt != ''){
                        var cDate = parseInt(row.shippedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }else{
                        return "";
                    }

                },
            }, {
                width: "120px",
                orderable: false,
                title: "收货时间",
                render: function (data, type, row) {
                    if(row.succeedAt && row.succeedAt != ''){
                        var cDate = parseInt(row.succeedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }else{
                        return "";
                    }
                },
            }]
        });

           // Handle click on "Select all" control
        $('input[name=checkAll]').on('click', function(){
      // Check/uncheck all checkboxes in the table
            var rows = $datatables.rows({ 'search': 'applied' }).nodes();
            $('input[name=checkCommissions]', rows).prop('checked', this.checked);
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            $(".viewDetailBtn").on("click",function() {
                orderNo = $(this).attr('orderNo');
                $('#modal_orderInfo').modal('show');
                $orderTables.search('').draw();
            });

            tableRoleCheck('#guiderUserTable');

        }

        // 订单退款
        // 根据退款前订单状态判断是否退全款或者扣除运费，默认算出一个退款金额，用户也可手动更改
        function viewDetail(pId, refundFee){
            var data = {
                orderId: pId,
                refundment : refundFee
            };
            var url = window.host + "/order/refund";
            utils.postAjax(url,data,function(res){
                if(typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("退款成功!", {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                        $refund_datatables.search('').draw();
                    } else {
                        utils.tools.alert("退款失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }

        $(".btn-search").on('click', function () {
            $datatables.search('').draw();
        });

        $(".btn-orgView").on('click', function () {
            var orderNo = $("#orderNo").val();
            var code = $("#code").val();
            if(orderNo == '' && code == ''){
                utils.tools.alert('无追溯记录，请输入相关查询条件', {timer: 1500});
                return;
            }
            window.open("/antifake/orgView?orderNo=" + orderNo + "&code=" + code,'_blank');
        });

        buttonRoleCheck('.hideClass');

});