define(['jquery', 'utils', 'datatables', 'blockui', 'select2'],
    function ($, utils, datatables, blockUI, select2) {
        const prefix = window.host + '/userAgent';

        const saveUrl = prefix + '/addWhiteList';
        const listUrl = prefix + '/viewWhiteList';
        const removeUrl = prefix + '/removeWhiteList';
        const updateUrl = prefix + '/updateWhiteList';
        const viewUrl = prefix + '/whitelist';

        const columns = ['phone', 'isEnable', 'created_at'];

        const ajax = {
            save: function (data) {
                utils.postAjax(saveUrl, data, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            $datatables.search('').draw();
                            $('#modal_white_list').modal('hide');
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                        } else {
                            utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            view: function (id) {
                utils.postAjax(viewUrl + '/' + id, null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data) {
                            $('#id').val(data.id);
                            $('#phone').val(data.phone);
                            block.unBlock();
                            $('#modal_white_list').modal('show');
                        } else {
                            console.log(data);
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            remove: function (id) {
                utils.postAjax(removeUrl + '/' + id, null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            console.log(data);
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            update: function (data, callback) {
                utils.postAjax(updateUrl, data, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            $('#modal_white_list').modal('hide');
                            $datatables.search('').draw();
                            if (callback) callback();
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                        } else {
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            }
        };

        const block = {
            fullBlock: function () {
                return $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });
            },
            unBlock: function () {
                return $.unblockUI();
            }
        };

        globalInit();

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                zeroRecords: '暂无相关数据',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "白名单未配置"
            }
        });

        const $datatables = $('#xquark_white_list_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            pageable: true,
            ajax: function (data, callback, settings) {
                $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true,
                    keyword: data.search.value,
                    order: columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (res.errorCode != '200') {
                        utils.tools.alert('数据加载失败');
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            agentId: 'agentId',
            columns: [
                {
                    data: 'phone',
                    width: '100px',
                    orderable: true,
                    name: 'phone'
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.enable ? '已启用' : '已禁用';
                    }
                },
                {
                    width: '80px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt === null) {
                            return '';
                        }
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var enable = row.enable ? '禁用' : '启用';
                        var agentId = row.agentId;
                        var html = '';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="edit_white_list" class="edit_white_list"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="change_white_list" class="change_white_list" style="margin-left: 15px" status="' + row.enable + '"><i class="icon-trash" >' +
                            '</i>' + enable + '</a>';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="delete_white_list" class="delete_white_List" data-toggle="popover" style="margin-left: 15px"><i class="icon-trash" ></i>删除</a>';
                        if (agentId) {
                            html += '<a href="javascript:void(0);" agentId="'
                                + agentId
                                + '" fid="view_agent_detail" class="view_agent_detail" style="margin-left: 15px"><i class="icon-grid-alt" ></i>查看详情</a>';
                        }
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });

        function globalInit() {
            var $modal = $('#modal_white_list');
            $('#add_button').on('click', function () {
                $('#id').val('');
                $('#phone').val('');
                $modal.modal('show');
            });

            $('#confirm_btn').on('click', function () {
                var phone = $('#phone').val();
                var id = $('#id').val();
                if (!checkPhone(phone)) {
                    utils.tools.alert('手机号码格式不正确', {timer: 1200, type: 'warning'});
                    return;
                }
                if (id) {
                    ajax.update({id: id, phone: phone});
                } else {
                    ajax.save({phone: phone})
                }
            });

            buttonRoleCheck('.hideClass');

            $('.btn-search').on('click', function () {
                var keyword = $.trim($("#sKeyword").val());
                // 根据搜索关键字刷新表格
                $datatables.search(keyword).draw();
            })
        }

        function initEvent() {
            $('body').unbind('click');

            $('.edit_white_list').on('click', function () {
                var id = $(this).attr('rowId');
                block.fullBlock();
                ajax.view(id);
            });

            $('.change_white_list').on('click', function () {
                var status = $(this).attr('status');
                var id = $(this).attr('rowId');
                console.log('status: ' + status);
                var data;
                if (status === 'true') {
                    data = {id: id, enable: false};
                } else {
                    data = {id: id, enable: true};
                }
                ajax.update(data);
            });

            $('.view_agent_detail').on('click', function () {
                var agentId = $(this).attr('agentId');
                if (!agentId) {
                    utils.tools.alert('代理信息不存在', {timer: 1200, type: 'warning'});
                    return;
                }
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + agentId, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#view_id").val(res.data.id);
                                $("#view_name").val(res.data.name);
                                $("#view_weixin").val(res.data.weixin);
                                $("#view_phone").val(res.data.phone);
                                //$("#view_email").val(res.data.email);
                                $("#view_idcard").val(res.data.idcard);
                                $("#view_type").val(res.data.type);
                                $("#modal_viewDetail").modal("show");
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });

            });
            /** 点击删除弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var id = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                        + id + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var param = $(this).attr("param");
                    ajax.remove(param);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_white_list_tables');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });
        }

        function checkPhone(num) {
            var regPhone = /^1\d{10}$/;
            return regPhone.test(num);
        }

    }
);
