define(['jquery', 'utils', 'moment', 'daterangepicker'],
    function ($, utils, moment) {

      const DATE_FORMAT = 'YYYY-MM-DD HH:mm';

      const options = {
        timePicker: true,
        dateLimit: {days: 60000},
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月",
            "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment().endOf('day')],
          '一星期': [moment(), moment().add(6, 'days')],
          '一个月': [moment(), moment().add(1, 'months')],
          '一年': [moment(), moment().add(1, 'year')]
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      /**
       * 构造一个datePricker实例
       * @param params.dom 选择器
       * @param params.onApply
       * @param params.onCancel
       */
      class Pricker {
        constructor(params) {
          const $dateRangePicker = $(params.dom);
          this.dom = params.dom;

          $dateRangePicker.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
              $dateRangePicker.val(start.format(DATE_FORMAT) + ' - '
                  + end.format(DATE_FORMAT));
            } else {
              $dateRangePicker.val('');
            }
          });

          const onApply = params.onApply || function (ev, picker) {
            if (picker.endDate.isBefore(moment(new Date()))) {
              utils.tools.error('活动结束时间不能早于当前时间');
              return;
            }
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;

            $('#valid_from').val(picker.startDate.format(DATE_FORMAT));
            $('#valid_to').val(picker.endDate.format(DATE_FORMAT));
          };

          $dateRangePicker.on('apply.daterangepicker', onApply);

          const onCancel = params.onCancel || function () {
            //do something, like clearing an input
            $dateRangePicker.val('');
            $('#valid_from').val('');
            $('#valid_to').val('');
          };

          /**
           * 清空按钮清空选框
           */
          $dateRangePicker.on('cancel.daterangepicker', onCancel);

          this.$dateRangePicker = $dateRangePicker;
        }

        /**
         * 传两个时间戳, 设置时间
         * @param from
         * @param end
         */
        setViewDate(from, end) {
          const $input = $(this.dom);
          from = moment(from).format(DATE_FORMAT);
          end = moment(end).format(DATE_FORMAT);
          $input.val(from + ' - ' + end);
          $('#valid_from').val(from);
          $('#valid_to').val(end);
        }

      }

      return {
        createPicker: function (params) {
          return new Pricker(params);
        }
      }
    });
