/**
 * Created by quguangming on 16/5/24.
 */
define(['jquery', 'utils'], function ($, utils) {

    var type = $('#type').val();
    var taxonomy = (() => type === 'FILTER' ? 'FILTER' : 'GOODS')();

    return {
        saveProduct: function (data, success, fail) {
            $.ajax({
                url: host + '/product/save',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });







            
        },
        getProductInfo: function (id, success, fail) {
            $.ajax({
                url: host + '/product/' + id,
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },

        /**
         * [新增分类]
         * @param {[String]} cateName [分类名称]
         * @param {[Function]} success  [成功回调]
         * @param {[Function]} fail     [失败回调]
         */
        addProCate: function (cateName, success, fail) {
            $.ajax({
                url: host + '/shop/category/save',
                type: 'POST',
                data: {
                    name: cateName
                },
                dataType: 'JSON',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取店铺分类信息失败！');
                    }
                }
            })
        },
        /**
         * 获取店铺分类信息
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getShopProCateList: function (shopId, success, fail) {
            var data = {
                shopid: shopId
            }
            $.ajax({
                url: window.host + '/shop/category/list?taxonomy=' + taxonomy,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取店铺分类信息失败！');
                    }
                }
            });
        },
      /**
       * 获取品牌列表
       * @success  {Function}  [成功回调]
       * @fail  {Function}     [失败回调]
       * author baize
       */
      getBrandList: function (success, fail) {
        $.ajax({
          url: window.host + '/brand/list',
          data: {
            pageable: false,
          },
          type: 'GET',
          success: function (data) {
            if (data.errorCode == 200) {
              success && success(data.data);
            } else {
              fail && fail(data.moreInfo);
            }
          },
          error: function (state) {
            if (state.status == 401) {
            } else {
              fail && fail('获取品牌信息失败！');
            }
          }
        });
      },
      /**
       * 获取仓库列表
       * @success  {Function}  [成功回调]
       * @fail  {Function}     [失败回调]
       * author baize
       */
      getWarehouseList: function (selectedWarehouse, success, fail) {
        $.ajax({
          url: window.host + '/warehouse/list',
          data: {
            pageable: false,
          },
          type: 'GET',
          success: function (data) {
            if (data.errorCode == 200) {
              success && success(selectedWarehouse, data.data);
            } else {
              fail && fail(data.moreInfo);
            }
          },
          error: function (state) {
            if (state.status == 401) {
            } else {
              fail && fail('获取品牌信息失败！');
            }
          }
        });
      },
      /**
       * 获取供应商列表
       * @success  {Function}  [成功回调]
       * @fail  {Function}     [失败回调]
       * author baize
       */
      getSupplierList: function (selectedSupplier, success, fail) {
        $.ajax({
          url: window.host + '/supplier/list?type=override',
          type: 'GET',
          data: {
            pageable: false,
          },
          success: function (data) {
            if (data.errorCode == 200) {
              success && success(selectedSupplier, data.data);
            } else {
              fail && fail(data.moreInfo);
            }
          },
          error: function (state) {
            if (state.status == 401) {
            } else {
              fail && fail('获取品牌信息失败！');
            }
          }
        });
      },
      /**
       * 获取商品关联的品牌列表
       * @success  {Function}  [成功回调]
       * @fail  {Function}     [失败回调]
       * author baize
       */
      getProductBrandList: function (id, success, fail) {
        $.ajax({
          url: `${window.host}/brand/list/product/${id}`,
          type: 'GET',
          success: function (data) {
            if (data.errorCode == 200) {
              success && success(data.data);
            } else {
              fail && fail(data.moreInfo);
            }
          },
          error: function (state) {
            if (state.status == 401) {
            } else {
              fail && fail('获取品牌信息失败！');
            }
          }
        });
      },
        //删除段落描述
        delDesc: function(id, success, fail) {
            $.ajax({
                url: host + '/fragment/delete',
                type: 'POST',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function(state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },
        saveProductFragment: function (data, success, fail) {
            $.ajax({
                url: host + '/productFragment/save',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },
        viewProductFragment: function (id, success, fail) {
          $.ajax({
                url: host + '/fragment/' + id,
                type: 'POST',
                data: null,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }
    }
});