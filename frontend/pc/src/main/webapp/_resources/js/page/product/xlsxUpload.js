/**
 * Created by jitre
 */
define(['jquery', 'utils', 'xlsx'], function ($, utils, xlsx) {

    let productDataPropertyClassMap=new Map([["价格","productPrice"],["兑换价","conversionPrice"],["立省","point"],["推广费","promoAmt"],["服务费","serverAmt"],
      ["净值","netWorth"],["SKU编码","skuCode"],["条形码","barCode"],["库存","productAmount"],["安全库存","secureAmount"],["长度","length"],
      ["宽度","width"],["高度","height"],["重量","weight"],["装箱数","numInPackage"]]);
    let skuModelInitFunc;
    let skuCellInitFunc;
    let attr_active;
    let skuAttributeItemIds;
    let skuAttributeIds;
    let dPointCalc;

    return {
        setSkuAttributeItemIds:function(data){
          skuAttributeItemIds=data;
        },
        setSkuAttributeIds:function(data){
          skuAttributeIds=data.data;
        },
        setSkuCellInitCallBack: function (callback) {
          skuCellInitFunc=callback;
        },
        setSkuModelInitCallBack: function (callback,attrs) {
          skuModelInitFunc=callback;
          attr_active=attrs;
        },
        setdPointCalc:function(func){
          dPointCalc=func;
        },
        /***
         * 注册上传事件的监听
         * @param element
         * @returns {addUploadListener}
         */
        addExcelUploadEventListener: function (element) {
            element.change(function (e) {
                let files = e.target.files;
                let fileReader = new FileReader();
                let dataResult = []; //获取数据
                fileReader.onload = function (ev) {
                    let workBook;
                    try {
                        let data = ev.target.result;
                        workBook = XLSX.read(data, {
                            type: 'binary'
                        }); //二进制读取表格内容        
                    } catch (e) {
                        utils.tools.alert('文件类型不正确', {
                        timer: 3600,
                        type: 'warning'
                      });
                        return;
                    }
                    //表格范围
                    let firstCol = '';
                    let readSuccess=true;
                    //读取表中数据
                    for (let sheet in workBook.Sheets) {
                        if (workBook.Sheets.hasOwnProperty(sheet)) {
                          if(!workBook.Sheets[sheet].hasOwnProperty('S1')){
                            firstCol = workBook.Sheets[sheet]['A1'].v;
                            dataResult = dataResult.concat(XLSX.utils.sheet_to_json(workBook.Sheets[sheet]));
                          }else{
                            utils.tools.alert('商品的规格不能大于三个', {
                              timer: 3600,
                              type: 'warning'
                            });
                            readSuccess=false;
                            break;
                          }
                        }
                    }
                    if(readSuccess&&dataResult.length>0){
                      let skuSpec=[];
                      console.log(dataResult);
                      //获取所有的规格数据,生成对应的skuModel
                      let properties=Object.getOwnPropertyNames(dataResult[0]);
                      properties.forEach(function(p){
                        if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                          skuSpec.push(p);
                        }
                      });
                      $(".sku_item").find('a').removeClass('itemactive');
                      $(".sku_item").find('.sku_index').remove();
                      skuSpec.forEach(function(element,index){
                        attr_active.push(element);
                        $(`a[data-sku=${element}]`).addClass("itemactive");
                        $(`a[data-sku=${element}]`).after(`<span class="sku_index">${++index}</span>`);
                      });
                      skuModelInitFunc();
                      //选中规格中的attribute,生成对应的skuCell
                      dataResult.forEach(function(row){
                        let properties=Object.getOwnPropertyNames(row);
                        properties.forEach(function(p){
                          if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                            let dataId=skuAttributeIds[p];
                            let id=skuAttributeItemIds[`${dataId}-${row[p]}`];
                            $(`.sku_item a[data-id=${id}]`).addClass("itemactive");
                          }
                        });
                      });
                      skuCellInitFunc();
                      //建立attribute Id拼接值到每一个row的map映射
                      let attributeIdsMap=new Map();
                      dataResult.forEach(function(row){
                        let properties=Object.getOwnPropertyNames(row);
                        let mapKeyArry=[];
                        properties.forEach(function(p){
                          if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                            console.log(`${p}:${row[p]}`);
                            mapKeyArry.push(skuAttributeItemIds[skuAttributeIds[p]+"-"+row[p]]);
                          }
                        });
                        attributeIdsMap.set(mapKeyArry.join("-"),row);
                      });
                      //赋值
                      attributeIdsMap.forEach(function(value,key,map){
                        let cell=$(`div[id=${key}]`);
                        let row=attributeIdsMap.get(key);
                        productDataPropertyClassMap.forEach(function(value,key){
                            cell.find(`input[name=${value}]`).prop("value",row[key]);
                        });
                      });
                      //动态计算德分
                      dPointCalc();
                      //删除多余的行
                      $(".sku_cell").each(function(index,element){
                        let id=$(element).prop("id");
                        if(!attributeIdsMap.has(id)){
                          $(element).remove();
                        }
                      });
                      //校验sku表格内的数据合法性
                      $(".sku_tablecell input").valid();
                    }else{
                      return ;
                    }

                };
                if(files.length>0){
                  fileReader.readAsBinaryString(files[0]);
                }
            })
        }
    }
});