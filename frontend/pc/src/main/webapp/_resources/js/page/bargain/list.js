define(['jquery', 'utils', 'daterangepicker', 'moment', 'product/upload',
        'jquerySerializeObject', 'datatables', 'blockui', 'select2'],
    function ($, utils, daterangepicker, moment, uploader) {

        var productListUrl = window.host + "/product/list";

        const prefix = window.host + '/promotionBargain';
        const listUrl = prefix + '/listPromotion';
        const viewUrl = prefix + '/viewVO';
        const listProductUrl = prefix + '/product/listTable';
        const deleteProductUrl = prefix + '/delete';
        const saveUrl = prefix + '/savePromotion';
        const saveProductUrl = prefix + '/save';
        const closeUrl = prefix + '/close';

        const $dataTable = $('#xquark_bargain_products_tables');
        const $dateRangePicker = $('.dateRange-time');
        const $searchBtn = $('.btn-search');
        const $body = $('body');

        var $category = '';

        var promotionId = '';
        var shopId = '';
        var keyWord = '';

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "暂无相关数据"
            }
        });

        /** 初始化日期控件 **/
        const options = {
            timePicker: true,
            dateLimit: {days: 60000},
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            autoApply: false,
            timePickerIncrement: 1,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月",
                    "9月", "10月", "11月", "12月"],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment().endOf('day')],
                '一星期': [moment(), moment().add(6, 'days')],
                '一个月': [moment(), moment().add(1, 'months')],
                '一年': [moment(), moment().add(1, 'year')]
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        const manager = (function () {
            const $saveBtn = $('.btn-release');
            const $saveBtnConfirm = $('.saveBtn');
            const $saveProductConfirm = $('.saveProductBtn');
            const $addProductBtn = $('.btn-addProduct');

            const globalInstance = {
                bindEvent: function () {
                    $saveBtn.on('click', function () {
                        $("#id").val('');
                        $("#title").val('');
                        $dateRangePicker.val('');
                        $("#modal_bargain").modal("show");
                    });
                    $addProductBtn.on('click', function () {
                        $("#promotionId").val(promotionId);
                        $("#bargainId").val('');
                        $("#productId").val('');
                        $("#productName").val('');
                        $('#originalPrice').val('');
                        $('#max_discount').val('');
                        $('#priceStart').val('');
                        $('#priceEnd').val('');
                        $('#amount').val('');
                        $('#max_times').val('');
                        $("#modal_products_detail").modal("show");
                    });
                    $saveProductConfirm.on('click', function () {
                        var bargainId = $('#bargainId').val();
                        var productId = $("#productId").val();

                        var maxDiscount = $('#max_discount').val();
                        var originalPrice = $('#originalPrice').val();
                        var maxTimes = $('#max_times').val();

                        var priceStart = $('#priceStart').val();
                        var priceEnd = $('#priceEnd').val();
                        var amount = $('#amount').val();

                        if (!maxDiscount || maxDiscount === '') {
                            utils.tools.alert("请输入最低价!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!maxTimes || maxTimes === '') {
                            utils.tools.alert("请输入最大砍价次数!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!productId || productId === '') {
                            utils.tools.alert("请选择商品!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (parseFloat(maxDiscount) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('商品最低价不能高于原价!',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!priceStart || !priceEnd || priceStart
                            === '' || priceEnd === '') {
                            utils.tools.alert('请输入砍价的折扣范围',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!amount || amount === '') {
                            utils.tools.alert('请输入商品库存信息',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        else if (parseFloat(priceStart) < 0 || parseFloat(priceEnd) < 0) {
                            utils.tools.alert('砍价范围不能为负数', {timer: 1200, type: 'warning'});
                            return;
                        }
                        else if (parseFloat(priceEnd) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('砍价最高范围不能高于商品原价',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (parseFloat(priceStart) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('砍价最低范围不能高于商品原价',
                                {timer: 1200, type: 'warning'});
                            return;
                        }

                        var data = {
                            id: bargainId,
                            promotionId: promotionId,
                            productId: productId,
                            maxDiscount: maxDiscount,
                            maxTimes: maxTimes,
                            priceStart: priceStart,
                            priceEnd: priceEnd,
                            amount: amount
                        };
                        utils.postAjaxWithBlock($(document), saveProductUrl,
                            data, function (res) {
                                if (typeof(res) === 'object') {
                                    switch (res.errorCode) {
                                        case 200: {
                                            if (res.data) {
                                                utils.tools.alert(
                                                    '操作成功', {
                                                        timer: 1200,
                                                        type: 'success'
                                                    });
                                                $("#modal_products_detail").modal(
                                                    "hide");
                                                $productDataTables.search(
                                                    '').draw();
                                            } else {
                                                alert("此商品已存在于活动中，不能重复添加");
                                            }
                                            break;
                                        }
                                        default: {
                                            utils.tools.alert(
                                                res.moreInfo,
                                                {timer: 1200});
                                            break;
                                        }
                                    }
                                } else if (res === 0) {

                                } else if (res === -1) {
                                    utils.tools.alert("网络问题,请稍后再试",
                                        {timer: 1200});
                                }
                            });
                    });
                    // 选择活动商品
                    $("#productName").on('focus', function () {
                        $selectProductDataTables.search('').draw();
                        $("#modal_select_products").modal("show");
                    });
                    $searchBtn.on('click', function () {
                        keyWord = $('#sKeyword').val();
                        $bargainDataTables.search(keyWord).draw();
                    });
                    $saveBtnConfirm.on('click', function () {
                        var id = $("#id").val();
                        var validFrom = $("#valid_from").val();
                        var validTo = $("#valid_to").val();
                        var title = $("#title").val();

                        var imgArr = [];
                        $('#product_image_dropzone > .dz-complete').each(
                            function (i, item) {
                                imgArr.push($(item).attr('data-img'));
                            });

                        if (!validFrom || validFrom === '' || !validTo
                            || validTo === '') {
                            utils.tools.alert("请选择活动时间!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!title || title === '') {
                            utils.tools.alert("请输入标题!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (imgArr.length > 1) {
                            utils.tools.alert("请不要选择多张图片!",
                                {timer: 1200, type: 'warning'});
                            return;
                        }

                        var data = {
                            id: id,
                            validFrom: validFrom,
                            validTo: validTo,
                            title: title,
                            img: imgArr.join(',')
                        };
                        utils.postAjaxWithBlock($(document), saveUrl, data,
                            function (res) {
                                if (typeof(res) === 'object') {
                                    switch (res.errorCode) {
                                        case 200: {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $bargainDataTables.search(
                                                '').draw();
                                            $('#modal_bargain').modal(
                                                'hide');
                                            break;
                                        }
                                        default: {
                                            utils.tools.alert(
                                                res.moreInfo,
                                                {timer: 1200});
                                            break;
                                        }
                                    }
                                } else if (res === 0) {

                                } else if (res === -1) {
                                    utils.tools.alert("网络问题,请稍后再试",
                                        {timer: 1200});
                                }
                            });
                    });

                    $dateRangePicker.on('apply.daterangepicker',
                        function (ev, picker) {
                            options.startDate = picker.startDate;
                            options.endDate = picker.endDate;
                            $("#valid_from").val(
                                picker.startDate.format(
                                    'YYYY-MM-DD HH:mm'));
                            $("#valid_to").val(picker.endDate.format(
                                'YYYY-MM-DD HH:mm'));
                        });

                    /**
                     * 清空按钮清空选框
                     */
                    $dateRangePicker.on('cancel.daterangepicker',
                        function (ev, picker) {
                            //do something, like clearing an input
                            $dateRangePicker.val('');
                            $("#valid_from").val('');
                            $("#valid_to").val('');
                        });

                    $searchBtn.on('click', function () {
                        keyWord = $('#sKeyword').val();
                        $bargainDataTables.search(keyWord).draw();
                    });

                    return this;
                },
                initDateRangePricker: function () {
                    $dateRangePicker.daterangepicker(options,
                        function (start, end) {
                            if (start._isValid && end._isValid) {
                                $dateRangePicker.val(start.format(
                                    'YYYY-MM-DD HH:mm') + ' - '
                                    + end.format(
                                        'YYYY-MM-DD HH:mm'));
                            } else {
                                $dateRangePicker.val('');
                            }
                        });
                    return this;
                },
                initProductCategory: function () {
                    //初始化商品分类信息
                    $.ajax({
                        url: window.host + '/shop/category/list',
                        type: 'POST',
                        dataType: 'json',
                        data: {},
                        success: function (data) {
                            if (data.errorCode == 200) {
                                var dataLength = data.data.length;
                                for (var i = 0; i < dataLength; i++) {
                                    $("#categoryType").append('<option value='
                                        + data.data[i].id + '>'
                                        + data.data[i].name
                                        + '</option>');
                                }
                                ;
                            } else {
                                utils.tools.alert(data.moreInfo,
                                    {timer: 1200, type: 'warning'});
                            }
                        },
                        error: function (state) {
                            if (state.status == 401) {
                            } else {
                                utils.tools.alert('获取店铺分类信息失败！',
                                    {timer: 1200, type: 'warning'});
                            }
                        }
                    });
                    $('body').on('change', 'select[name="categoryType"]', function (event) {
                        $category = $(this).val();
                        productListUrl = window.host + "/product/list";
                        $selectProductDataTables.search('').draw();
                    });
                    return this;
                }
            };

            return {
                initGlobal: function () {
                    globalInstance.initDateRangePricker()
                        .bindEvent().initProductCategory();
                },
                initTable: function () {

                    $body.unbind('click');

                    $(".edit").on("click", function () {

                        $("#product_image_dropzone").html('');

                        var id = $(this).attr("rowId");
                        var validFrom = $(this).attr("validFrom");
                        var validTo = $(this).attr("validTo");
                        var title = $(this).attr("title");

                        var originImg = $(this).attr("originImg");
                        var imgUrl = $(this).attr("img");

                        var d = new Date(parseInt(validFrom));
                        validFrom = d.format('yyyy-MM-dd hh:mm');
                        d = new Date(parseInt(validTo));
                        validTo = d.format('yyyy-MM-dd hh:mm');
                        $('.dateRange-time').val(validFrom + ' - '
                            + validTo);

                        $("#id").val(id);
                        $("#valid_from").val(validFrom);
                        $("#valid_to").val(validTo);
                        $("#title").val(title);

                        //图片
                        if (originImg) {
                            var key = originImg,
                                url = imgUrl,
                                imgObject = {
                                    imgUrl: url,
                                    id: key
                                };
                            var mockFile = {
                                name: "",
                                size: "",
                                dataImg: imgObject.id
                            };
                            uploader[0].dropzone.emit("addedfile",
                                mockFile);
                            uploader[0].dropzone.emit("thumbnail", mockFile,
                                imgObject.imgUrl);
                            uploader[0].dropzone.emit("complete", mockFile);
                        }

                        $("#modal_bargain").modal("show");
                    });

                    $(".products").on('click', function () {
                        promotionId = $(this).attr("rowId");
                        $productDataTables.search('').draw();
                        $("#modal_products").modal("show");
                    });

                    /** 点击删除弹出框 **/
                    $("[data-toggle='popover']").popover({
                        trigger: 'manual',
                        placement: 'left',
                        html: 'true',
                        animation: true,
                        content: function () {
                            var rowId = $(this).attr("rowId");
                            return '<span>确认结束？</span>' +
                                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                                + rowId + '">确认</button>' +
                                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                        }
                    });

                    $('[data-toggle="popover"]').popover() //弹窗
                        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                            $(this).parent().parent().siblings().find(
                                '[data-toggle="popover"]').popover('hide');
                        }).on('shown.bs.popover', function () {
                        var that = this;
                        $('.popover-btn-ok').on("click", function () {
                            var pId = $(this).attr("pId");
                            utils.postAjax(closeUrl, {id: pId},
                                function (res) {
                                    if (typeof(res) === 'object') {
                                        if (res.data) {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $bargainDataTables.search(
                                                '').draw();
                                        } else {
                                            utils.tools.alert("操作失败!", {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        }
                                    }
                                });
                        });

                        $('.popover-btn-cancel').on("click", function () {
                            $(that).popover("hide");
                        });
                    });

                    $body.on('click', function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('popover') //弹窗内部点击不关闭
                            && target.parent('.popover-content').length
                            === 0
                            && target.parent('.popover-title').length
                            === 0
                            && target.parent('.popover').length === 0
                            && target.data("toggle") !== "popover") {
                            //弹窗触发列不关闭，否则显示后隐藏
                            $('[data-toggle="popover"]').popover('hide');
                        } else if (target.data("toggle") === "popover") {
                            target.popover("toggle");
                        }
                    });
                },
                initProductEvent: function () {
                    $(".productedit").on("click", function () {
                        var id = $(this).attr("rowId");
                        utils.postAjax(viewUrl, {id: id}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试",
                                    {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    if (res.data) {
                                        var data = res.data;
                                        $("#bargainId").val(id);
                                        promotionId = data.promotionId;
                                        $("#promotionId").val(
                                            data.promotionId);
                                        $('#productId').val(data.productId);
                                        $("#productName").val(
                                            data.productName);
                                        $('#originalPrice').val(
                                            data.productPrice);
                                        $("#max_times").val(data.maxTimes);
                                        $('#max_discount').val(
                                            data.maxDiscount);
                                        $('#priceStart').val(
                                            data.priceStart);
                                        $('#priceEnd').val(data.priceEnd);
                                        $('#amount').val(data.amount);
                                        $("#modal_products_detail").modal(
                                            "show");
                                    }
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo, {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    } else {
                                        utils.tools.alert('服务器错误', {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    }
                                }
                            }
                        });

                    });

                    $(".productdel").on("click", function () {
                        var id = $(this).attr("rowId");
                        utils.tools.confirm('确定该商品不再参与此活动?', function () {
                            utils.postAjax(deleteProductUrl, {id: id},
                                function (res) {
                                    if (typeof(res) === 'object') {
                                        if (res.data) {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $productDataTables.search(
                                                '').draw();
                                        } else {
                                            utils.tools.alert("操作失败!", {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        }
                                    }
                                });
                        }, function () {

                        });

                    });
                },
                initSelectProductEvent: function () {
                    $(".selectproduct").on("click", function () {
                        var productId = $(this).attr("rowId");
                        var productName = $(this).attr("productName");
                        var originalPrice = $(this).attr('productPrice');
                        $("#productId").val(productId);
                        $("#productName").val(productName);
                        $('#originalPrice').val(originalPrice);
                        $("#modal_select_products").modal("hide");
                    });
                }
            }
        })();

        manager.initGlobal();

        /** 初始化表格数据 **/
        const $bargainDataTables = utils.createDataTable('#xquark_bargain_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true,
                    keyWord: keyWord
                    // order: $columns[data.order[0].column],
                    // direction: data.order[0].dir
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    width: "10px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'
                            + row.id + '"></label>';
                    }
                },
                {
                    title: "标题",
                    data: "title",
                    orderable: false,
                    name: "title"
                }, {
                    title: "开始时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validFrom);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validFrom"
                }, {
                    title: "结束时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validTo);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validTo"
                }, {
                    title: "状态",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch (row.archive) {
                            case false:
                                status = '进行中';
                                break;
                            case true:
                                status = '已结束';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "onsaleAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" validFrom="'
                            + row.validFrom + '" validTo="'
                            + row.validTo + '" img="' + row.img
                            + '" originImg="' + row.originImg
                            + '" title="' + row.title + '" rowId="'
                            + row.id
                            + '" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="products" style="margin-left: 10px;" rowId="'
                            + row.id
                            + '" ><i class="icon-pencil7"></i>商品管理</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'
                            + row.id
                            + '" ><i class="icon-trash"></i>结束</a>';
                        return html;
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                manager.initTable();
            }

        });

        var $productDataTables = utils.createDataTable('#xquark_list_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(listProductUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    id: promotionId,
                    pageable: true,
                    category: $category
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                    } else {
                        if (res.data.shopId) {
                            shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<img class="goods-image" src="'
                            + row.productImg + '" />';
                    }
                },
                {
                    title: "商品",
                    data: "productName",
                    orderable: false,
                    name: "title"
                }, {
                    title: "原价",
                    data: "productPrice",
                    orderable: false,
                    name: "price"
                }, {
                    title: "最低价",
                    data: "maxDiscount",
                    orderable: false,
                    name: "maxDiscount"
                }, {
                    title: "最大砍价次数",
                    data: "maxTimes",
                    orderable: false,
                    name: "maxTimes"
                }, {
                    title: "砍价金额范围",
                    orderable: false,
                    render: function (data, type, row) {
                        var priceStart = row.priceStart ? row.priceStart
                            : 0;
                        var priceEnd = row.priceEnd ? row.priceEnd : 0;
                        return priceStart + ' 元 - ' + priceEnd + ' 元';
                    }
                },
                {
                    title: "活动商品数",
                    orderable: false,
                    render: function (data, type, row) {
                        var amount = row.amount;
                        return amount + '件';
                    }
                },
                {
                    title: "已售商品数量",
                    orderable: false,
                    render: function (data, type, row) {
                        var sales = row.sales;
                        return sales + '件';
                    }
                },
                {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="productedit" style="margin-left: 10px;" productName="'
                            + row.productName + '" productId="'
                            + row.productId + '" rowId="' + row.id
                            + '" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="productdel" style="margin-left: 10px;" rowId="'
                            + row.id
                            + '" ><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                manager.initProductEvent();
            }
        });


        var $selectProductDataTables = utils.createDataTable('#xquark_select_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get(productListUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    // order: function () {
                    //     if($order !== ''){
                    //         return $order;
                    //     }else{
                    //         var _index = data.order[0].column;
                    //         if ( _index < 4){
                    //             return '';
                    //         } else {
                    //             return $orders[_index - 4];
                    //         }
                    //     }
                    // },
                    // direction: data.order ? data.order[0].dir :'asc',
                    category: $category,
                    isGroupon: ''
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert('产品列表加载失败',
                            {timer: 1200, type: 'warning'});
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                    } else {
                        if (res.data.shopId) {
                            shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.categoryTotal,
                        iTotalDisplayRecords: res.data.categoryTotal
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a href="' + row.productUrl
                            + '"><img class="goods-image" src="'
                            + row.imgUrl + '" /></a>';
                    }
                },
                {
                    title: "商品",
                    data: "name",
                    orderable: false,
                    name: "name"
                }, {
                    title: "状态",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch (row.status) {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    title: "价格",
                    data: "price",
                    orderable: true,
                    name: "price"
                }, {
                    title: "数量",
                    data: "amount",
                    orderable: true,
                    name: "amount"
                },
                {
                    title: "库存",
                    data: "sales",
                    orderable: true,
                    name: "sales"
                }, {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "onsaleAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        console.log('row: \n');
                        console.log(row);
                        var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                            + row.id + '" productName="' + row.name
                            + '"  productPrice="' + row.price
                            + '"><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                manager.initSelectProductEvent();
            }

        });

        $(".btn-search-products").on('click', function () {

            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null) {
                productListUrl = window.host + '/product/searchbyPc/'
                    + shopId + '/' + keyword;
                $selectProductDataTables.search(keyword).draw();
            } else if (keyword == '' || keyword.length == 0) {
                productListUrl = window.host + "/product/list";
                $selectProductDataTables.search('').draw();
            }
        });

    });

