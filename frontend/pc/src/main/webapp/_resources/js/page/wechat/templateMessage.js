define(['jquery', 'utils', 'form/validate'],
    function($, utils, validate) {

        var allRule = {
            required: true,
            minlength: 8,
        };
        var allMessage = {
            required: '请输入模板ID',
            minlength: '模板ID长度不应低于8位',
        };

        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var templateMessageForm = {
            form: $('#templateMessageForm'),

            financeEvent: $("#templateMessageForm input[name=financeEvent]"),
            pointEvent: $("#templateMessageForm input[name=pointEvent]"),
            orderEvent: $("#templateMessageForm input[name=orderEvent]"),
            orderDeliveryEvent: $("#templateMessageForm input[name=orderDeliveryEvent]"),
            refundEvent: $("#templateMessageForm input[name=refundEvent]"),
            distributorApplicationEvent: $("#templateMessageForm input[name=distributorApplicationEvent]"),
            commissionEvent: $("#templateMessageForm input[name=commissionEvent]"),
            productDetailEvent: $("#templateMessageForm input[name=productDetailEvent]"),
            newPartnerEvent: $("#templateMessageForm input[name=newPartnerEvent]"),
            partnerObtainCommissionEvent: $("#templateMessageForm input[name=partnerObtainCommissionEvent]"),

            saveTemplateMessage: $('#saveTemplateMessage'),
            url: window.originalHost + '/wechat/saveTemplateMessage',

            allRule: {
                required: true,
                minlength: 8,
            },
            allMessage: {
                required: '请输入模板ID',
                minlength: '模板ID长度不应低于8位',
            },

            rules: {
                financeEvent: allRule,
                pointEvent: allRule,
                orderEvent: allRule,
                orderDeliveryEvent: allRule,
                refundEvent: allRule,
                distributorApplicationEvent: allRule,
                commissionEvent: allRule,
                productDetailEvent: allRule,
                newPartnerEvent: allRule,
                partnerObtainCommissionEvent: allRule
            },

            messages: {
                financeEvent: allMessage,
                pointEvent: allMessage,
                orderEvent: allMessage,
                orderDeliveryEvent: allMessage,
                refundEvent: allMessage,
                distributorApplicationEvent: allMessage,
                commissionEvent: allMessage,
                productDetailEvent: allMessage,
                newPartnerEvent: allMessage,
                partnerObtainCommissionEvent: allMessage
            },

            save: function() {
                var data = {
                    financeEvent: this.financeEvent.val(),
                    pointEvent: this.pointEvent.val(),
                    orderEvent: this.orderEvent.val(),
                    orderDeliveryEvent: this.orderDeliveryEvent.val(),
                    refundEvent: this.refundEvent.val(),
                    distributorApplicationEvent: this.distributorApplicationEvent.val(),
                    commissionEvent: this.commissionEvent.val(),
                    productDetailEvent: this.productDetailEvent.val(),
                    newPartnerEvent: this.newPartnerEvent.val(),
                    partnerObtainCommissionEvent: this.partnerObtainCommissionEvent.val(),
                };

                utils.postAjaxWithBlock($(document), this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("模板消息保存成功", {timer: 1200, type: 'success'});
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            },

            init: function() {
                $(this.saveTemplateMessage).on('click',function() {
                    $(this.form).submit();
                });
                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        templateMessageForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        templateMessageForm.init();
    });