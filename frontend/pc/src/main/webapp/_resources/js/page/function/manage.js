define(['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree',
        'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, select2, tree, fileinput_zh,
              fileinput) {

        var $listUrl = window.host + '/function/listTable';

        var $columns = ['name', 'module_url', 'visRolesDesc',
            'visRolesDesc', 'created_at'];

        var $functionId = '';

        var $settings = '';

        var $isReload = '';

        // 角色表格是否为第一次加载
        var $isFirstLoad = true;

        /* 当前选中节点 */
        var $selected = {
            'id': null,
            'parent': null
        };

        /* 全局事件绑定 */
        eventBind();

        var $roleFunctionSelect = function () {
            var checked = [];
            var unchecked = [];

            return {
                addChecked: function (roleId) {
                    if (checked.indexOf(roleId) === -1) {
                        checked.push(roleId);
                    }
                },
                removeChecked: function (roleId) {
                    checked.splice(checked.indexOf(roleId), 1);
                },
                isChecked: function (roleId) {
                    return checked.indexOf(roleId) !== -1;
                },
                addUnckecked: function (roleId) {
                    if (unchecked.indexOf(roleId) === -1) {
                        unchecked.push(roleId);
                    }
                },
                removeUnchecked: function (roleId) {
                    unchecked.splice(checked.indexOf(roleId), 1);
                },
                isUnchecked: function (roleId) {
                    return unchecked.indexOf(roleId) !== -1;
                },
                getChecked: function () {
                    return checked.join(',');
                },
                getUnchecked: function () {
                    return unchecked.join(',');
                },
                clear: function () {
                    checked = [];
                    unchecked = [];
                }
            }
        }();

        var merchantId = '';

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "菜单下暂无按钮"
            }
        });

        /** 初始化表格数据 **/
        var $datatables = $('#xquark_function_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            pageable: true,
            ajax: function (data, callback, settings) {
                $.get($listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    moduleId: $selected.id,
                    pageable: true,
                    order: $columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (res.errorCode != '200') {
                        utils.tools.alert('数据加载失败');
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    data: 'name',
                    width: '60px',
                    orderable: true,
                    name: 'name'
                },
                {
                    data: 'htmlId',
                    width: '60px',
                    orderable: true,
                    name: 'htmlId'
                },
                {
                    width: '60px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.moduleName;
                    }
                },
                {
                    width: '120px',
                    orderable: false,
                    render: function (data, type, row) {
                        var visRoles = row.visRolesDesc;
                        if (visRoles) {
                            return visRoles.split(',').join(', \n');
                        }
                        return '默认 (全部可见)';
                    }
                },
                {
                    width: '120px',
                    orderable: false,
                    render: function (data, type, row) {
                        var avaRoles = row.avaRolesDesc;
                        if (avaRoles) {
                            return avaRoles.split(',').join(', \n');
                        }
                        return '默认 (全部可用)';
                    }
                },
                {
                    width: '80px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt === null) {
                            return '';
                        }
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit_setting_vis role_check_table" rowId="'
                            + row.id + '" fid="set_visible"><i class="icon-pencil7" ></i>可见角色</a>';
                        html += '<a href="javascript:void(0);" class="edit_setting_ava role_check_table" style="margin-left: 10px;" rowId="'
                            + row.id + '" fid="set_enable"><i class="icon-pencil7" ></i>可用角色</a>';
                        html += '<a href="javascript:void(0);" class="delete_function_role role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'
                            + row.id + '" functionId="' + row.id
                            + '" fid="delete_button"><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });

        var $roledatatables = $('#xquark_select_roles_tables').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get(window.host + '/function/listRoleTable', {
                    functionId: $functionId,
                    pageable: true,
                    settings: $settings
                }, function (res) {
                    if (res.errorCode != 200) {
                        res.list = [];
                        utils.tools.alert('选择角色表格数据加载失败');
                        return;
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "15px",
                    orderable: false,
                    render: function (data, type, row) {
                        var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled" ';
                        if (row.isSelect) {
                            str = str + ' checked="checked" ';
                        }
                        str = str + ' value="' + row.id + '"></label>';
                        return str;
                    }
                },
                {
                    data: "roleName",
                    width: "120px",
                    orderable: false,
                    name: "roleName"
                }, {
                    data: "roleDesc",
                    width: "120px",
                    orderable: false,
                    name: "roleDesc"
                }
                , {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                roleInitEvent();
            }
        });

        var jstree = $('#function_tree');
        jstree.jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {"stripes": true},
                'data': {
                    'url': window.host + '/module/listTree',
                    'data': function (node) {
                        if (node === null) {
                            return {'id': '0'};
                        }
                        return {'id': node.id};
                    }
                }
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 9,
                    "valid_children": ["root"]
                },
                "root": {
                    "icon": "/static/3.3.2/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "plugins": [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow"
            ],
            "contextmenu": {
                "items": {
                    "create": false,
                    "rename": false,
                    "remove": false
                }
            }
        }).on('loaded.jstree', function () {
            // 只展开第一级菜单
            jstree.jstree("select_node", "ul > li:first");
            var selectedNode = jstree.jstree("get_selected");
            jstree.jstree('open_node', selectedNode, false, true);
        }).on('delete_node.jstree', function (event, data) {
            // deleteModule(data.node.id);
        }).on('select_node.jstree', function (event, data) {
            onSelect(event, data);
        });

        /**
         * jsTree选中节点是刷新表格
         * @param event
         * @param data
         */
        function onSelect(event, data) {
            $selected = data.node;
            // console.log("当前节点: " + $selected.id + " : " + $selected.parent);
            $datatables.search('').draw();
        }

        /**
         * 全局事件绑定
         */
        function eventBind() {

            /** 添加按钮 **/
            $('#add_function').on('click', function () {
                if ($selected.id === '0' || $selected.parent === null
                    || $selected.parent === '0') {
                    utils.tools.alert('请选择子菜单');
                    return;
                }
                $('#id').val('');
                $('#htmlId').val('');
                $('#functionName').val('');
                $('#modal_function_save').modal('show');
            });

            $('#refresh_cache').on('click', function () {
                var url = window.host + '/cache/function';
                utils.postAjax(url, null, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.errorCode == 200) {
                            // 全局变量复位
                            utils.tools.alert('操作成功，配置已生效');
                        } else {
                            utils.tools.alert(res.msg,
                                {title: '刷新失败', timer: 1200, type: 'warning'});
                        }
                    }
                });
            });

            // 按钮角色绑定确认角色确认
            $(".changeRoleBtn").on('click', function () {
                modify();
            });

            /* 保存按钮配置 */
            $('#saveBtn_function').on('click', function () {
                var url_post = window.host + '/function/save';
                var moduleId = $selected.id;
                var htmlId = $('#htmlId').val();
                var name = $('#functionName').val();
                if (!name || name === null) {
                    utils.tools.alert("请输入按钮名称!", {timer: 1200, type: 'warning'});
                    return;
                }
                if (!htmlId || htmlId === null) {
                    utils.tools.alert('请输入按钮ID', {timer: 1200, type: 'warning'});
                    return;
                }
                var data = {
                    name: name,
                    htmlId: htmlId,
                    moduleId: moduleId
                };
                utils.postAjaxWithBlock($(document), url_post, data, function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $datatables.search('').draw();
                                $('#modal_function_save').modal('hide');
                                utils.tools.alert('操作成功');
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                })
            });

            buttonRoleCheck('.hideClass');
        }

        // 角色表格初始化事件
        function roleInitEvent() {

            $(".styled").change(function () {
                var roleId = $(this).val();
                if (this.checked == true) {
                    if ($roleFunctionSelect.isUnchecked(roleId)) {
                        $roleFunctionSelect.removeUnchecked(roleId);
                        // console.log('删除未选中');
                    }
                    else {
                        $roleFunctionSelect.addChecked(roleId);
                        // console.log('添加选中');
                    }
                } else {
                    if ($roleFunctionSelect.isChecked(roleId)) {
                        // console.log('删除选中');
                        $roleFunctionSelect.removeChecked(roleId);
                    }
                    else {
                        // console.log('添加未选中');
                        $roleFunctionSelect.addUnckecked(roleId);
                    }
                }
            });

            if ($isReload) {
                $.unblockUI();
                $("#modal_select_roles").modal("show");
            }
        }

        function initEvent() {

            $('body').unbind('click');

            $(".edit_setting_vis").on("click", function () {
                // 清除已选中
                $roleFunctionSelect.clear();
                $settings = 'show';
                $functionId = $(this).attr("rowId");
                $isReload = true;
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });
                $roledatatables.search('').draw();

            });

            $(".edit_setting_ava").on("click", function () {
                // 清除以选中
                $roleFunctionSelect.clear();
                $settings = 'enable';
                $functionId = $(this).attr("rowId");
                $isReload = true;
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

                $roledatatables.search('').draw();

            });


            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var id = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                        + id + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var param = $(this).attr("param");
                    deleteFunction(param);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_function_tables');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });
        }

        function modify() {
            var checked = $roleFunctionSelect.getChecked();
            var unChecked = $roleFunctionSelect.getUnchecked();
            var url = window.host + '/function/modifyRole';
            // console.log('functionId: ' + $functionId);
            // console.log("settings: " + $settings);
            // console.log('checkedRoles: \n');
            // console.log(checked);
            // console.log('uncheckedRoles: \n');
            // console.log(unChecked);
            var param = {
                'functionId': $functionId,
                'idsToSave': checked,
                'idsToRemove': unChecked,
                'settings': $settings
            };
            // 全局变量复位
            $settings = '';
            $roleFunctionSelect.clear();
            utils.postAjaxWithBlock($(document), url, param, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            if (res.data) {
                                $datatables.search('').draw();
                                utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                            } else {
                                utils.tools.alert("操作操作失败", {timer: 1200, type: 'success'});
                            }
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })


        }

        function deleteFunction(id) {
            var url = window.host + '/function/delete/' + id;

            $functionId = '';
            utils.postAjax(url, null, function (res) {
                if (typeof(res) === 'object') {
                    if (res.errorCode == 200) {
                        $datatables.search('').draw();
                        utils.tools.alert('操作成功');
                    } else {
                        utils.tools.alert(res.msg, {timer: 1200, type: 'warning'});
                    }
                }
            });
        }
    }
);

