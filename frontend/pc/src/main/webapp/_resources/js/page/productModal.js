define(['jquery', 'datatables'], function ($) {

  var $orders = ['price', 'amount', 'sales', 'onsale'];
  var $order = '';
  var pId = '';
  // 绑定商品上下文
  var $productListUrl = window.host + "/product/list";


  var productType = $('#type').val();
  var taxonomy = (() => productType === 'FILTER' ? 'FILTER' : 'GOODS')();

  var $shopId = null;
  var $category = '';
  var $pDataTable;

  var checkedBounded = false;

  var $modal = $('#choose-product-modal');

  // 缓存选中的商品
  var checkedSet = null;
  var checkedSetInited = false;

  /**
   * 切换商品后重置缓存状态
   */
  function resetCheckedStatus() {
    checkedSet = new Set();
    checkedSetInited = false;
  }

  function removeFromCheckedList(id) {
    checkedSet.delete(id);
    console.log('removed from checked, ', checkedSet);
  }

  function pushToCheckedList(id) {
    checkedSet.add(id);
    console.log('pushed to checked, ', checkedSet);
  }

  /**
   * 选中所有商品/滤芯
   */
  $(document).on('click', '#checkAllPd', function () {
    if ($(this).prop('checked')) {
      $(this).prop('checked', true);
      $('input[name=checkPd]').prop('checked', true).trigger('change');
    } else {
      $(this).prop('checked', false);
      $('input[name=checkPd]').prop('checked', false).trigger('change');
    }
  });

  //初始化商品分类信息
  $.ajax({
    url: window.host + '/shop/category/list?taxonomy=FILTER',
    type: 'POST',
    dataType: 'json',
    data: {},
    success: function (data) {
      if (data.errorCode === 200) {
        var dataLength = data.data.length;
        var $categoryType = $('#categoryTypeModal');
        $categoryType.empty();
        $categoryType.append('<option value="" selected="selected">所有分类</option>');
        for (var i = 0; i < dataLength; i++) {
          $categoryType.append('<option value=' + data.data[i].id + '>'
              + data.data[i].name + '</option>');
        }
      } else {
        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
      }
    },
    error: function (state) {
      if (state.status === 401) {
      } else {
        utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
      }
    }
  });

  /**
   * 标签搜索
   */
  $('body').on('change', 'select[name="categoryTypeModal"]', function () {
    $productListUrl = window.host + "/product/list";
    $category = $(this).val();
    $pDataTable.search('').draw();
  });

  /**
   * 关键字搜索
   */
  $(".btn-search-products").on('click', function () {
    var keyword = $.trim($("#select_products_sKeyword").val());
    if (keyword !== '' && keyword.length > 0 && shopId !== null) {
      $productListUrl = window.host + '/product/searchbyPc/' + shopId + '/'
          + keyword;
      $pDataTable.search(keyword).draw();
    } else if (keyword === '' || keyword.length === 0) {
      $productListUrl = window.host + "/product/list";
      $pDataTable.search('').draw();
    }
  });

  $('.btn-show-bounded-filter').on('click', function () {
    $productListUrl = window.host + '/product/listFilterTable';
    $pDataTable.search('').draw();
  });

  function buildColumns(type) {
    return [
      {
        width: "10px",
        orderable: false,
        render: function (data, type, row) {
          var $dom = $(`<label class="checkbox"><input name="checkPd" type="checkbox" class="styled"
            rowId=${row.id}></label>`);
          // if (row.select && row.select === true) {
          var isSelect = (checkedSet && checkedSet.size > 0) ? checkedSet.has(row.id)
              : (row.select && row.select === true);
          $dom.find('input[name=checkPd]').attr('checked', isSelect);
          // }
          return $dom.html();
        }
      },
      {
        width: "30px",
        orderable: false,
        render: function (data, type, row) {
          return '<a href="' + row.productUrl
              + '"><img class="goods-image" src="' + (row.imgUrl || row.img) + '" /></a>';
        }
      },
      {
        data: "name",
        width: "50px",
        orderable: false,
        name: "name"
      },
      {
        data: "encode",
        width: "30px",
        orderable: false,
        name: "encode"
      },
      {
        width: "40px",
        orderable: false,
        render: function (data, type, row) {
          var status = '';
          switch (row.status) {
            case 'INSTOCK':
              status = '下架';
              break;
            case 'ONSALE':
              status = '在售';
              break;
            case 'FORSALE':
              status = '待上架发布';
              break;
            case 'DRAFT':
              status = '未发布';
              break;
            default:
              break;
          }
          return status;
        },
      }, {
        data: "price",
        width: "50px",
        orderable: true,
        name: "price"
      }, {
        data: "amount",
        orderable: true,
        width: "50px",
        name: "amount"
      },
      {
        data: "sales",
        orderable: true,
        width: "50px",
        name: "sales"
      }, {
        orderable: false,
        width: "100px",
        render: function (data, type, row) {
          var cDate = parseInt(row.onsaleAt);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "onsaleAt"
      }
    ];
  }

  function initTable(params) {
    $pDataTable = $('#xquark_select_products_tables').DataTable({
      paging: true, //是否分页
      filter: false, //是否显示过滤
      lengthChange: false,
      processing: true,
      serverSide: true,
      deferRender: true,
      searching: true,
      ajax: function (data, callback, settings) {
        $.get($productListUrl, {
          size: data.length,
          page: (data.start / data.length),
          keyword: data.search.value,
          type: params.type || 'NORMAL',
          productId: pId,
          pageable: true,
          order: function () {
            if ($order !== '') {
              return $order;
            } else {
              var _index = data.order[0].column;
              if (_index < 4) {
                return '';
              } else {
                return $orders[_index - 4];
              }
            }
          },
          direction: data.order ? data.order[0].dir : 'asc',
          category: $category,
          isGroupon: ''
        }, function (res) {
          if (!res.data.list) {
            res.data.list = [];
          } else {
            if (res.data.shopId) {
              $shopId = res.data.shopId;
            }
          }
          // 表格刷新时合并新的选中项
          // TODO 已选中数据单独请求
          if (!checkedSetInited) {
            var newCheckedList = res.data.checkedList || [];
            newCheckedList.forEach(pushToCheckedList);
            checkedSetInited = true;
          }

          // 刷新后去掉全选
          // TODO 按照实际的该分页是否全选来设置
          $('input[id=checkAllPd]').prop('checked', false);
          // 表格翻页后合并所有选中项
          console.log('checked list: ', checkedSet);
          callback({
            recordsTotal: res.data.total,
            recordsFiltered: res.data.total,
            data: res.data.list,
            iTotalRecords: res.data.total,
            iTotalDisplayRecords: res.data.total
          });
        });
      },
      rowId: "id",
      columns: buildColumns(params.type),
      drawCallback: function () {  //数据加载完成
        initSelectProductEvent(params);
      }
    });
  }

  function initSelectProductEvent(params) {

    if (!checkedBounded) {
      $(document).on('change', 'input[name=checkPd]', function () {
        var $this = $(this);
        var isChecked = $this.is(':checked');
        var id = $this.attr('rowId');
        if (!isChecked) {
          removeFromCheckedList(id);
          if (params.unChecked) {
            params.unChecked(this);
          }
        } else {
          pushToCheckedList(id);
          if (params.checked) {
            params.checked(this);
          }
        }
      });
      checkedBounded = true;
    }

    $(".selectproduct").on("click", function () {
      var id = $(this).attr("rowId");
      var name = $(this).attr("productName");
      var price = $(this).attr("productName");
      //回调外部函数
      if (params && params.onSelect) {
        params.onSelect(id, name, price);
      }
      $modal.modal("hide");
    });
  }

  return {
    /**
     * 创建商品表格
     * @param params.type 类型 商品或滤芯
     * @param params.triggerDom 唤起选框dom
     * @param params.onConfirm 确认时回调
     * @param params.onSelect 选择时回调
     * @param params.onTrigger modal触发时的事件
     * @param params.unChecked 取消选框事件
     * @param params.checked 选框事件
     */
    create: function (params) {
      initTable(params);

      // dom名称兼容旧版本
      var dom = params.triggerDom || params.dom;

      $('.btn-ok').on('click', function () {
        if (params.onConfirm) {
          params.onConfirm(checkedSet);
        }
        $('#choose-product-modal').modal('hide');
      });

      /* 选择活动商品 */
      $(document).on('click', dom, function () {
        if (params.type === 'FILTER') {
          pId = $(this).attr('rowId');
          resetCheckedStatus();
          $pDataTable.search('').draw();
        }
        if (params.onTrigger) {
          params.onTrigger.call(this);
        }
        $modal.modal("show");
      });

    },
    refresh: function () {
      $pDataTable.search('').draw();
    }
  }
});
