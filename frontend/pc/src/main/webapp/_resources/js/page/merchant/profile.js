/**
 * Created by quguangming on 16/5/25.
 */
define(['form/validate', 'utils', 'dropzone', 'jquerySerializeObject'], function (validate, utils, upload) {

    var $form = $(".profile-form");

    var oldPhone = $("#phone").val();

    var saveUrl = "/sellerpc/profile/update";

    //图片上传
    // Defaults
    Dropzone.autoDiscover = false;

    var url = window.host + "/_f/u";

    // File limitations
    $(".avatar_dropzone").dropzone({
        url: url + "?belong=SHOP",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 2, // MB      //最大文件大小，单位是 MB
        maxFiles: 1,               //限制最多文件数量
        maxThumbnailFilesize: 1,
        addRemoveLinks: true,
        thumbnailWidth: "150",      //设置缩略图的缩略比
        thumbnailHeight: "150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: "false",
        dictInvalidFileType: "文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                     //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",            //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation: "你确定要取消上传吗？",        //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",              //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function () {

            var imgDropzone = this;
            this.on("success", function (file, data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200: {
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                $("#avatar").val(imgId);
                            }
                            break;
                        }
                        default: {
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else {
                    if (data == -1) {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function (file) {
                utils.tools.alert("文件上传失败!", {timer: 1200, type: 'warning'});
                $(".dz-error-message").html("文件上传失败!");
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function (file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("removedfile", function (file) {
                $("#avatar").val("");
            });


            this.on("thumbnail", function (file) {

            });


            //显示默认图片
            var imgId = $("#avatar").val();
            if (imgId != null && imgId.length > 0) {
                var imageUrl = $("#avatar").attr("src");
                var imgSize = $("#avatar").attr("img-size");
                var mockFile = {name: "", size: imgSize};
                imgDropzone.emit("addedfile", mockFile);
                imgDropzone.emit("thumbnail", mockFile, imageUrl);

            }

        }
    });


    function submitForm(form) {

        if ($(".btn-submit").hasClass("disabled")) return;

        var data = $(form).serializeObject();
        if (data.orgId == null || data.orgId == 74) {
            utils.tools.alert('请选择部门');
            return;
        }

        var imgId = $("#avatar").val();

        data.avatar = imgId;

        utils.getJson(saveUrl, data, function (result) {
            if (result) {
                utils.tools.alert("修改成功!", {timer: 1200, type: 'success'});
                //utils.logout();
            } else {
                utils.tools.alert("修改失败!", {timer: 1200, type: 'warning'});
            }
        });
    }

    var profileForm = {

        rules: {
            phone: {
                required: true,
                pattern: /^1\d{10}$/,
                remote: {
                    url: "/sellerpc/merchant/checkPhone",
                    type: "Post",
                    data: {
                        phone: function () {
                            var $phone = $("#phone");
                            return $phone.val() != oldPhone ? $phone.val() : 'phone';
                        }
                    }
                }
            },
            "roles[]": {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            "roles[]": {
                required: "至少选择一个角色."
            },
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone: {
                required: "手机号不能为空.",
                pattern: "手机格式不正确.",
                remote: "该手机号已注册,请重新输入."
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            submitForm(form);
        }
    };
    //initInfo();

    function initInfo() {
        initOrgList();
        // initRolesList();
    }

    /** 初始化部门信息 **/
    function initOrgList() {
        var url = window.host + '/org/list';
        utils.postAjaxWithBlock($(document), url, null, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        var orgTotal = res.data.orgTotal;
                        var select = $('#orgNames');
                        var selectedOption = select.find('option:selected');
                        if (orgTotal !== null && orgTotal !== 0) {
                            for (var i = 0; i < orgTotal; i++) {
                                select.append('<option value=' + res.data.list[i].id + '>' + res.data.list[i].name + '</option>');
                            }
                            //修改默认选中
                            var orgId = selectedOption.attr('value');
                            select.find("> option[value=" + orgId + "]").prop('selected', true)
                        } else {
                            selectedOption.text('请先创建部门');
                            $(".btn-submit").addClass("disabled");
                        }
                        break;
                    }
                    default: {
                        utils.tools.alert('获取部门信息失败！', {timer: 1200, type: 'warning'});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    // function initRolesList() {
    //     var url = window.host + '/merchantRole/list';
    //     utils.postAjaxWithBlock($(document), url, {'keyword' : ''}, function (res) {
    //         if (typeof(res) === 'object') {
    //             switch (res.errorCode) {
    //                 case 200: {
    //                     var total = res.data.total;
    //                     var selectedOption = $('#roles').find('option:selected');
    //                     if (total !== null && total != 0) {
    //                         var list = res.data.list;
    //                         for (var i = 0; i < total; i++) {
    //                             $("#roles").append('<option value=' + list[i].roleName + '>' + list[i].roleDesc + '</option>');
    //                         }
    //                     }
    //                     break;
    //                 }
    //                 default: {
    //                     break;
    //                 }
    //             }
    //         } else if (res == 0) {
    //
    //         } else if (res == -1) {
    //             utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
    //         }
    //     });
    // }

    validate($form, profileForm);
});
