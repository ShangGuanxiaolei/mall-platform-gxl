define(['jquery', 'utils', 'utils/fileUploader',
  'jquerySerializeObject', 'datatables'], function ($, utils, uploader) {

  const prefix = window.host + '/memberPromotion';
  const listUrl = prefix + '/list';
  const saveUrl = prefix + '/save';
  const viewUrl = id => prefix + `/view?id=${id}`;
  const delUrl = id => prefix + `/delete?id=${id}`;

  const $addPromotion = $('#add-member-promotion');
  const $editModal = $('#member-promotion-modal');
  const $banner = $('#banner');
  const $btnSubmit = $('.btn-submit');
  const $form = $('#form');

  const dropzone = uploader.create({
    dom: '#member_promotion_dropzone',
    onSuccess: function (data) {
      $banner.val(data.id);
    }
  });

  $btnSubmit.on('click', function () {
    const params = $form.serializeObject();
    utils.post(saveUrl, function (data) {
      if (data && data === true) {
        utils.tools.success('保存成功');
        $datatables.search('').draw();
        $editModal.modal('hide');
      } else {
        utils.tools.warn('保存失败');
      }
    }, params);
  });

  $addPromotion.on('click', function () {
    $editModal.modal('show');
  });

  $(document).on('click', '.edit', function () {
    const id = $(this).attr('rowId');
    utils.post(viewUrl(id), function (data) {
      if (!data) {
        utils.tools.error('加载失败, 请稍后再试');
        return;
      }
      utils.tools.syncForm($form, data);
      // 将隐藏的banner设置为原七牛路径
      $('#banner').val(data.sourceImg);
      dropzone.clear().addImage(data.banner);
      $editModal.modal('show');
    });
  });

  $(document).on('click', '.del', function () {
    const id = $(this).attr('rowId');
    utils.tools.confirm('', function () {
      utils.post(delUrl(id), function (result) {
        if (result && result === true) {
          utils.tools.success('删除成功');
          $datatables.search('').draw();
        } else {
          utils.tools.error('删除失败');
        }
      })
    }, function () {

    });
  });

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      search: '<span>筛选:</span> _INPUT_',
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  // 商品佣金列表
  var $datatables = $('#xquark_member_promotion_list').DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ajax: function (data, callback) {
      $.get(listUrl, {
        size: data.length,
        page: (data.start / data.length),
        pageable: true,
      }, function (res) {
        if (!res.data.list) {
          res.data.list = [];
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      });
    },
    columns: [
      {
        data: "title",
        width: "50px",
        orderable: false,
        name: "title"
      },
      {
        width: "50px",
        orderable: false,
        name: "status",
        render: function (data, type, row) {
          if (row.status === 'ACTIVE') {
            return '已启用';
          }
          return '已禁用';
        }
      },
      {
        width: "100px",
        orderable: false,
        name: "url",
        data: "url"
      },
      {
        width: "75px",
        title: '发布时间',
        sortable: false,
        render: function (data, type, row) {
          if (row.createdAt == null) {
            return '';
          }
          var cDate = parseInt(row.createdAt);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        }
      },
      {
        width: 150,
        sClass: 'styled text-center sorting_disabled',
        align: 'right',
        title: '操作',
        render: function (data, type, row) {
          var html = '';
          var changeStatusStr = (function () {
            if (row.status === 'ACTIVE') {
              return '禁用';
            }
            return '启用';
          })();
          html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'
              + row.id
              + '" ><i class="icon-pencil7" ></i>编辑</a>';
          html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" rowId="'
              + row.id + '"><i class="icon-trash"></i>删除</a>';
          return html;
        }
      }],
    drawCallback: function () {  //数据加载完成
    }
  });

});
