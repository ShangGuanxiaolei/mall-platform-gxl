define(['echartsNoAMD','echartsTheme','jquery', 'utils', 'd3', 'd3tooltip'], function(echarts, limitless, $, utils,d3, d3tooltip) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/yundou/getSummary';
    var data = {
    };
    // 交易额，订单数
    var weekDays;
    var weekOut;
    var weekIn;
    var order;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {

            // 取概况中的总发放/总消耗积分
            $("#total_out").html(result.data.info.total_out);
            $("#total_in").html(result.data.info.total_in);

            // 取最近七天交易额，订单数
            weekOut = result.data.nums.out;
            weekIn = result.data.nums.in;
            weekDays = result.data.nums.weekDays;

            //weekShipOrder = result.data.nums.weekShipOrder;

            order = echarts.init(document.getElementById('order_lines'), limitless);

            var stacked_lines_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['发放积分','消耗积分']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: weekDays
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '发放积分',
                        type: 'line',
                        stack: 'Total',
                        data: weekOut
                    },
                    {
                        name: '消耗积分',
                        type: 'line',
                        stack: 'Total',
                        data: weekIn
                    }
                ]
            };
            order.setOption(stacked_lines_options);

        }
    });


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            order.resize();
        }, 200);
    }

});
