define(['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'ckEditor'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';
    const saveUrl = prefix + '/physique/save';

    const $physiqueTable = $('#physiqueTable');

    const formProfile = {
        debug: true,
        rules: {
            name: 'required',
            description: 'required'
        },
        messages: {
            name: {
                required: '请输入问题名称'
            },
            description: {
                required: '请输入体质描述'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            console.log(form);
            manager.submitForm(form);
        }
    };

    const manager = (function () {

        const $editor = $('#editor');
        const $form = $('.physique-form');

        const globalInstance = {
            initEditor: function () {
                $editor.ckeditor();
                return this;
            },
            initValidate: function () {
                validate($form, formProfile);
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.initEditor()
                    .initValidate();
            },
            initTable: function () {

            },
            submitForm: function (form) {
                var data = $(form).serializeObject();
                utils.getJson(saveUrl, data, function (res) {
                    if (res.errorCode === 200) {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            // 修改问题后跳转
                            // 添加问题后重置
                            $form[0].reset();
                            var newPhysique = '<tr><td>' + data.name + '</td></tr>';
                            $physiqueTable.append($(newPhysique));
                        } else {
                            utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    }
                });
            }
        }
    })();

    manager.initGlobal();

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});
