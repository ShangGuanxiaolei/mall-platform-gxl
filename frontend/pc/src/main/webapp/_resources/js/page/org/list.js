define(['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, datatabels, blockui, select2, tree, fileinput_zh, fileinput) {

        var $listUrl = window.host + '/org/list';
        var $columns = ['name', 'sort_no', 'is_leaf', 'created_at', 'remark'];
        var selected = {
            'id': '0',
            'parent': null
        };

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无子部门"
            }
        });

        /** 添加部门 **/
        $('#add').on('click', function () {
            $('#id').val(selected.id);
            $('#modal_component_save').modal('show');
        });

        $('#update').on('click', function () {
            var id = selected.id;
            if (id === '0') {
                utils.tools.alert('请在左侧选择部门');
                return;
            }
            updateOrg(id);
        });

        $('#delete').on('click', function () {
            var id = selected.id;
            var tree = $('#tree_list').jstree(true);
            if (id === '0') {
                utils.tools.alert('请在左侧选择部门');
                return;
            }
            utils.tools.confirm("删除该部门，将同时删除该部门所有下级部门，确定删除吗?", function () {
                tree.delete_node(selected);
            }, function () {

            });
        });

        $('#saveBtn').on('click', function () {
            var url = window.host + '/org/save';
            var id = $('#id').val();
            var parent_id = $('#parent_id').val();
            var name = $('#name').val();
            var remark = $('#remark').val();
            if (!name || name == '') {
                utils.tools.alert("请输入部门名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                parentId: id,
                name: name,
                remark: remark
            };
            utils.postAjaxWithBlock($(document), url, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            utils.tools.alert('操作成功');
                            window.location.href = window.originalHost + '/org/list';
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

        $('#updateBtn').on('click', function () {
            var url = window.host + '/org/update';
            var id = $('#id_update').val();
            var parent_id = $('#parent_id_update').val();
            var name = $('#name_update').val();
            var remark = $('#remark_update').val();
            if (!name || name == '') {
                utils.tools.alert("请输入部门名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                id: id,
                parentId: parent_id,
                name: name,
                remark: remark
            };
            utils.postAjaxWithBlock($(document), url, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            alert("操作成功");
                            window.location.href = window.originalHost + '/org/list';
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

        buttonRoleCheck('.hideClass');

        /** 初始化表格数据 **/
        var $datatables = $('#xquark_list_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function (data, callback, settings) {
                $.get($listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    parentId: selected.id,
                    pageable: true,
                    order: $columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.orgTotal,
                        recordsFiltered: res.data.orgTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.orgTotal,
                        iTotalDisplayRecords: res.data.orgTotal
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    data: 'name',
                    width: '120px',
                    orderable: true,
                    name: 'name'
                },
                {
                    width: '40px',
                    orderable: true,
                    render: function (data, type, row) {
                        return row.sort_no;
                    }
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.is_leaf ? '是' : '否';
                    }
                },
                {
                    width: '60px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt == null) return '';
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                        ;
                    }
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.remark ? row.remark : '无';
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="' + row.id + '" parent_id="' + row.parent_id + '" fid="edit_org"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" parent_id="' + row.parent_id + '" fid="delete_org"><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });


        function initEvent() {
            $('body').unbind('click');

            /** 表格的编辑事件 **/
            $('.edit').on('click', function () {
                var id = $(this).attr("rowId");
                updateOrg(id);
            });

            /** 点击删除部门弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("id");
                    var tree = $('#tree_list').jstree(true);
                    tree.delete_node(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_list_tables');

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });

        }


        var jstree = $('#tree_list');
        jstree.jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {"stripes": true},
                'data': {
                    'url': window.host + '/org/listTree',
                    'data': function (node) {
                        if (node == null) return {'id': '0'};
                        return {'id': node.id};
                    }
                }
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 9,
                    "valid_children": ["root"]
                },
                "root": {
                    "icon": "/static/3.3.2/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "plugins": [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow"
            ],
            "contextmenu": {
                "items": {
                    "create": false,
                    "rename": false,
                    "remove": false
                }
            }
        }).on('loaded.jstree', function () {
            // 只展开第一级菜单
            jstree.jstree("select_node", "ul > li:first");
            var selectedNode = jstree.jstree("get_selected");
            jstree.jstree('open_node', selectedNode, false, true);
        }).on('create_node.jstree', function (event, data) {
            // 更新节点在rename事件中执行
        }).on('rename_node.jstree', function (event, data) {
            renameOrg(event, data);
        }).on('delete_node.jstree', function (event, data) {
            deleteOrgNode(event, data);
        }).on('select_node.jstree', function (event, data) {
            onSelect(event, data);
        });

        /**
         * 新增或修改节点
         * 根据 old 是否为New node判断
         * @param event
         * @param data
         */
        function renameOrg(event, data) {
            var text = data.text;
            var old = data.old;
            var parent = data.node.parent;
            var id = data.node.id;
            var mode = old === 'New node' ? 'create' : 'update';
            switch (mode) {
                case 'create':
                    createOrg(parent, text, function (result) {
                        // 后台判断已有重复节点时返回空
                        if (result.data == null) {
                            alert(result.moreInfo);
                            $('#' + id + '_anchor').remove();
                            return;
                        }
                        var orgId = result.data.id;
                        data.instance.set_id(data.node, orgId);
                    });
                    break;
                case 'update':
                    if (id === '0') {
                        utils.tools.alert("不能修改根节点");
                        data.instance.set_text(data.node, '所有部门');
                        return;
                    }
                    updateOrgName(id, parent, text, function (result) {
                        if (result.data == null) {
                            alert(result.moreInfo);
                            data.instance.set_text(data.node, data.old);
                        }
                    });
                    break;
                default:
                    throw 'Unsupported';
            }
        }

        /**
         * 新建节点步骤:
         * 1. create_node.jstree 事件, 新建一个 New node
         * 2. rename_node.jstree 事件, 重命名新建的节点
         * @param parentId 父节点id
         * @param newName 新节点名称
         * @param callBack 创建成功后的回调函数
         */
        function createOrg(parentId, newName, callBack) {
            var params = {'name': newName, 'parentId': parentId};
            $.ajax({
                'url': window.host + '/org/save',
                'type': 'post',
                'dataType': 'json',
                'cache': false,
                'data': params,
                'timeout': 1000 * 10
            }).done(function (json) {
                callBack(json);
            }).fail(function (e) {
                Metronic.unblockUI();
                alert("亲出错了，请稍后再试");
            });
        }

        /**
         * 更新部门名称
         * @param id 部门id
         * @param name 部门名称
         * @param parentId 父节点id
         * @param callBack 更新成功后的回调函数
         */
        function updateOrgName(id, parentId, name, callBack) {
            var params = {'id': id, 'parentId': parentId, 'name': name};
            $.ajax({
                'url': window.host + '/org/update',
                'type': 'post',
                'dataType': 'json',
                'cache': false,
                'data': params,
                'timeout': 1000 * 10
            }).done(function (json) {
                callBack(json);
            }).fail(function (e) {
                alert("亲出错了，请稍后再试");
            })
        }

        /**
         * 删除节点
         * @param event
         * @param data
         */
        function deleteOrgNode(event, data) {
            var id = data.node.id;
            console.log(id);
            var params = {'id': id};
            var url = window.host + "/org/delete";
            utils.postAjax(url, params, function (res) {
                utils.tools.alert('操作成功');
                $datatables.search('').draw();
            });
        }

        /*手动删除*/
        function deleteOrg(id) {
            var params = {'id': id};
            var url = window.host + "/org/delete";
            utils.postAjax(url, params, function (res) {
                utils.tools.alert('操作成功');
                $datatables.search('').draw();
            });
        }

        /**手动更新**/
        function updateOrg(id) {
            $.ajax({
                url: window.host + '/org/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var org = data.data;
                        $("#id_update").val(org.id);
                        $("#parent_id_update").val(org.parent_id);
                        $("#name_update").val(org.name);
                        $("#remark_update").val(org.remark);
                        $("#modal_component_update").modal("show");
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        utils.tools.alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        /**
         * jsTree选中节点是刷新表格
         * @param event
         * @param data
         */
        function onSelect(event, data) {
            selected = data.node;
            console.log(selected);
            $datatables.search('').draw();
        }


    }
);
