define(['jquery', 'utils', 'form/validate' , 'fileinput_zh', 'fileinput'],
    function($, utils, validate ,fileinput_zh,fileinput) {
        
        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var wechatConfigForm = {
            form: $('#wechatConfigForm'),

            configId: $("#wechatConfigForm input[name=configId]"),
            appName: $("#wechatConfigForm input[name=appName]"),
            appId: $("#wechatConfigForm input[name=appId]"),
            appSecret: $("#wechatConfigForm input[name=appSecret]"),
            mchId: $("#wechatConfigForm input[name=mchId]"),
            mchKey: $("#wechatConfigForm input[name=mchKey]"),
            cert_password : $("#wechatConfigForm input[name=cert_password]"),
            appAppId: $("#wechatConfigForm input[name=appAppId]"),
            appAppSecret: $("#wechatConfigForm input[name=appAppSecret]"),
            appMchId: $("#wechatConfigForm input[name=appMchId]"),
            appMchKey: $("#wechatConfigForm input[name=appMchKey]"),
            app_cert_password : $("#wechatConfigForm input[name=app_cert_password]"),

            saveWechatConfigBtn: $('#saveWechatConfigBtn'),
            url: window.originalHost + '/wechat/saveWechatConfig',

            rules: {
                appName: {
                    required: true,
                    minlength: 2,
                },
                appId: {
                    required: true,
                    minlength: 8,
                    maxlength: 128,
                },
                appSecret: {
                    required: true,
                    minlength: 16,
                    maxlength: 256,
                },
                mchId: {
                    required: true,
                    minlength: 8,
                },
                mchKey: {
                    required: true,
                    minlength: 16,
                },
            },

            messages: {
                appName: {
                    required: '请输入公众号名称',
                    minlength: '请至少输入两个字符',
                },
                appId: {
                    required: '请输入APPID',
                    minlength: 'APPID长度错误',
                    maxlength:'APPID长度错误',
                },
                appSecret: {
                    required: '请输入appSecret',
                    minlength: 'appSecret长度错误',
                    maxlength:'appSecret长度错误',
                },
                mchId: {
                    required: '请输入商户号ID',
                    minlength: '商户号ID长度错误',
                    maxlength:'商户号ID长度错误',
                },
                mchKey: {
                    required: '请输入商户号Key',
                    minlength: '商户号Key长度错误',
                    maxlength:'商户号Key长度错误',
                },
            },

            save: function() {
                var data = {
                    id: this.configId.val(),
                    appName: this.appName.val(),
                    appId: this.appId.val(),
                    appSecret: this.appSecret.val(),
                    mchId: this.mchId.val(),
                    mchKey: this.mchKey.val(),
                    cert_password: this.cert_password.val(),
                    appAppId: this.appAppId.val(),
                    appAppSecret: this.appAppSecret.val(),
                    appMchId: this.appMchId.val(),
                    appMchKey: this.appMchKey.val(),
                    app_cert_password: this.app_cert_password.val()
                };

                utils.postAjaxWithBlock($(document), this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("微信配置成功", {timer: 1200, type: 'success'});
                                location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            },

            init: function() {
                $(this.saveWechatConfigBtn).on('click',function() {
                    $(this.form).submit();
                });

                var cert_file_value = $("#cert_file_value").val();
                var fileoptions = {
                    language: 'zh',
                    uploadUrl: window.originalHost + '/wechat/saveWechatCert',
                    previewFileType: 'text',
                    browseLabel: '选择文件',
                    removeLabel: '删除',
                    uploadLabel: '上传',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    browseClass: 'btn btn-primary',
                    uploadClass: 'btn btn-default',
                    removeClass: 'btn btn-danger',
                    initialCaption: '',
                    maxFilesNum: 1,
                    allowedFileExtensions: ["p12"],
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        footer: '',
                    }
                };
                var op = fileoptions;
                //如果cert_file_value有值，则说明之前上传过证书，file控件显示个默认值
                if(cert_file_value && cert_file_value != ''){
                    var op = $.extend({
                        initialPreview : [ // 预览图片的设置
                            "<img src= '" + window.originalHost +  "/_resources/images/p12.jpg' class='file-preview-image'>", ]
                    }, fileoptions);
                }
                $('#cert_file').fileinput(op);

                $('#cert_form').on('fileuploaded', function(event, data, previewId, index) {
                    var form = data.form, files = data.files, extra = data.extra,
                        response = data.response, reader = data.reader;
                    if (response == '200') {
                        utils.tools.alert('文件上传成功');
                    } else if (response == '500') {
                        utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
                    }
                });


                var app_cert_file_value = $("#app_cert_file_value").val();
                var app_fileoptions = {
                    language: 'zh',
                    uploadUrl: window.originalHost + '/wechat/saveWechatAppCert',
                    previewFileType: 'text',
                    browseLabel: '选择文件',
                    removeLabel: '删除',
                    uploadLabel: '上传',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    browseClass: 'btn btn-primary',
                    uploadClass: 'btn btn-default',
                    removeClass: 'btn btn-danger',
                    initialCaption: '',
                    maxFilesNum: 1,
                    allowedFileExtensions: ["p12"],
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        footer: '',
                    }
                };
                var app_op = app_fileoptions;
                //如果cert_file_value有值，则说明之前上传过证书，file控件显示个默认值
                if(app_cert_file_value && app_cert_file_value != ''){
                    var app_op = $.extend({
                        initialPreview : [ // 预览图片的设置
                            "<img src= '" + window.originalHost +  "/_resources/images/p12.jpg' class='file-preview-image'>", ]
                    }, app_fileoptions);
                }
                $('#app_cert_file').fileinput(app_op);

                $('#app_cert_form').on('fileuploaded', function(event, data, previewId, index) {
                    var form = data.form, files = data.files, extra = data.extra,
                        response = data.response, reader = data.reader;
                    if (response == '200') {
                        utils.tools.alert('文件上传成功');
                    } else if (response == '500') {
                        utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
                    }
                });

                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        wechatConfigForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        wechatConfigForm.init();
    });