define(['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform, daterangepicker, moment) {

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: true,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            dateLimit: {days: 600},
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoApply: true,
            opens: 'left',
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        $('.daterange-basic').daterangepicker(options);

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;
        });

        // 时间区间默认为空
        options.startDate = '';
        options.endDate = '';
        $('.daterange-basic').val('');

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering: false,
            sortable: false,
            ajax: function (data, callback, settings) {
                $.get(window.host + "/userAgent/apply/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#status").val(),
                    startDate: options.startDate != '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: options.endDate != '' ? options.endDate.format('YYYY-MM-DD') : '',
                    parentName: $("#parentName").val(),
                    name: $("#name").val(),
                    type: $("#type").val(),
                    phone: $("#phone").val(),
                    pageable: true,
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.applyTotal,
                        recordsFiltered: res.data.applyTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.applyTotal,
                        iTotalDisplayRecords: res.data.applyTotal
                    });
                });
            },
            columns: [{
                width: "120px",
                title: '姓名',
                data: 'name',
                name: 'name'
            }, {
                width: "120px",
                title: '微信号',
                data: 'weixin',
                name: 'weixin'
            }, {
                width: "120px",
                title: '电话',
                data: 'phone',
                name: 'phone'

            }, {
                width: 75,
                data: 'type',
                name: 'type',
                title: '申请级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.type;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            }, {
                width: '75px',
                title: '上级名称',
                data: 'parentVo.name',
                name: 'parentVo.name',
                sortable: false
            }, {
                width: "120px",
                title: '身份证号',
                data: 'idcard',
                name: 'idcard'

            }, {
                width: "180px",
                title: '详细地址',
                data: 'addressStreet',
                name: 'addressStreet'

            }, {
                width: "160px",
                title: '申请时间',
                render: function (data, type, row) {
                    if (row.createdAt == null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            }, {
                width: "120px",
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if (value == 'ACTIVE') {
                        return "已审核通过";
                    } else if (value == 'FROZEN') {
                        return "已冻结";
                    }
                    {
                        return "待审核";
                    }
                }
            }, {
                width: "180px",
                sClass: 'styled text-center',
                align: 'right',
                title: '操作',
                render: function (data, type, row) {
                    var value = row.status;
                    var result = '<a href="javascript:void(0);" class="btn-sm btn btn-primary viewDetail role_check_table" rowId="' + row.id + '" fid="account_detail">查看详情</a> ';
                    if (value == 'APPLYING') {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table" data-toggle="enablepopover" rowId="' + row.id + '" fid="pass_agent">审核通过</a> '
                            + '<a href="javascript:void(0);" class="btn-sm btn btn-primary" data-toggle="disablepopover" rowId="' + row.id + '" fid="delete_agent_apply">删除申请</a> ';
                    } else if (value == 'ACTIVE') {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table"  data-toggle="frozenpopover" rowId="' + row.id + '" fid="account_freeze">冻结账户</a> ';
                    } else {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table" data-toggle="unfrozenpopover" rowId="' + row.id + '" fid="account_unfreeze">解冻账户</a> ';
                    }
                    return result;
                }
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            // 查看详情
            $(".viewDetail").on('click', function () {
                var id = $(this).attr("rowId");
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + id, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#view_name").val(res.data.name);
                                $("#view_weixin").val(res.data.weixin);
                                $("#view_phone").val(res.data.phone);
                                //$("#view_email").val(res.data.email);
                                $("#view_idcard").val(res.data.idcard);
                                $("#view_address").val(res.data.address);
                                $("#view_type").val(res.data.type);
                                $("#view_id").val(res.data.id);

                                // $("#life_img").attr('src', res.data.lifeImg);
                                // $("#idcard_img").attr('src', res.data.idcardImg);
                                initFileInput('lifeCard', res.data.id, res.data.lifeImg);
                                initFileInput('idCard', res.data.id, res.data.idcardImg);
                                var addressId = res.data.address;
                                $.ajax({
                                    url: 'http://' + window.location.host + '/v2/address/api/' + addressId,
                                    method: 'POST',
                                    success: function (data) {
                                        console.log(data);
                                        renderingData(data);
                                    },
                                    error: function (err) {
                                        console.log('error ')
                                    }
                                });

                                //$("#view_idcard_img").attr("src",res.data.idcardImgUrl);
                                //$("#view_life_img").attr("src",res.data.lifeImgUrl);
                                $("#modal_viewDetail").modal("show");
                                /** 初始化选择框控件 **/
                                $('.select').select2({
                                    minimumResultsForSearch: Infinity,
                                });
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });

            });

            // 审核通过
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认审核通过吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "enablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="enablepopover"]').popover('hide');
                } else if (target.data("toggle") == "enablepopover") {
                    target.popover("toggle");
                }
            });


            // 删除申请
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除申请吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    deleteUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "disablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="disablepopover"]').popover('hide');
                } else if (target.data("toggle") == "disablepopover") {
                    target.popover("toggle");
                }
            });

            // 冻结账户
            $("[data-toggle='frozenpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认冻结账户吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="frozenpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="frozenpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    frozenUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "frozenpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="frozenpopover"]').popover('hide');
                } else if (target.data("toggle") == "frozenpopover") {
                    target.popover("toggle");
                }
            });

            // 解冻账户
            $("[data-toggle='unfrozenpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认解冻账户吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="unfrozenpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="unfrozenpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    unfrozenUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#guiderUserTable');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "unfrozenpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="unfrozenpopover"]').popover('hide');
                } else if (target.data("toggle") == "unfrozenpopover") {
                    target.popover("toggle");
                }
            });

        }

        /**
         * 审核通过
         * @param id
         */
        function enableUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/audit?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("审核失败", {timer: 1200});
                }
            });
        }

        /**
         * 删除申请
         * @param id
         */
        function deleteUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("删除失败", {timer: 1200});
                }
            });
        }

        /**
         * 冻结账户
         * @param id
         */
        function frozenUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/reject?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("冻结账户失败", {timer: 1200});
                }
            });
        }

        /**
         * 解冻账户
         * @param id
         */
        function unfrozenUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/unFrozen?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("解冻账户失败", {timer: 1200});
                }
            });
        }

        function renderingData(data) {
            if (data.errorCode === 200 && data.data) {
                var zones = data.data.zones;

                $('#view_detail').val(data.data.street);

                $('#province').html('<option value="' + zones[1].id + '">' + zones[1].name + '</option>')
                $('#city').html('<option value="' + zones[2].id + '">' + zones[2].name + '</option>')
                $('#district').html('<option value="' + zones[3].id + '">' + zones[3].name + '</option>')
                initEditZones(zones[1].id, zones[2].id, zones[3].id);

            } else console.log('数据格式错误');
        }

        function initZones() {
            buildChildrenZone(1, document.getElementById('province'), '选择省份');
            $('#province').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('city'), '选择市');
            });
            $('#city').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
            });
        }

        function initEditZones(provinceId, cityId, districtId) {
            buildChildrenZone(1, document.getElementById('province'), '选择省份', function () {
                document.getElementById('province').value = provinceId;
                $('#province').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('city'), '选择市');
                });
            });
            buildChildrenZone(provinceId, document.getElementById('city'), '选择市', function () {
                document.getElementById('city').value = cityId;
                $('#city').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
                });
            });
            buildChildrenZone(cityId, document.getElementById('district'), '选择县区', function () {
                document.getElementById('district').value = districtId;
            });
        }

        // 初始化上传图片控件
        function initFileInput(name, id, img) {
            var $fileInput = $('#' + name + '-input');
            $fileInput.fileinput('destroy');
            var op = {
                language: 'zh',
                uploadUrl: window.host + '/userAgent/updateMultiPartImg',
                uploadExtraData: {"id": id, "type": name},
                showUpload: false,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                browseIcon: '<i class="icon-file-plus"></i>',
                uploadIcon: '<i class="icon-file-upload2"></i>',
                removeIcon: '<i class="icon-cross3"></i>',
                browseClass: 'btn btn-primary',
                uploadClass: 'btn btn-default',
                removeClass: 'btn btn-danger',
                initialCaption: '',
                maxFilesNum: 1,
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"],
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    footer: ''
                }

            };
            //如果img有值，则显示之前上传的图片
            if (img && img.toString().indexOf('http') != -1) {
                op = $.extend({
                    showPreview: true,
                    initialPreview: [ // 预览图片的设置
                        "<img src= '" + img + "' class='file-preview-image'>"]
                }, op);
            }

            $fileInput.fileinput(op);
            $fileInput.on('fileselect', function(event, numFiles, label) {
                $fileInput.fileinput('upload');
            });
            // $fileInput.fileinput('enable');
        }

        function buildChildrenZone(zoneId, insertDom, firstOption, callback) {
            if(zoneId == '0'){
                var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                insertDom.innerHTML = frag;
                if (callback) {
                    callback();
                }
            }else{
                $.ajax({
                    url: 'http://' + window.location.host + '/v2/zone/' + zoneId + '/children',
                    method: 'POST',
                    success: function (data) {
                        if (data.data && data.errorCode) {
                            var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                            data.data.forEach(function (value) {
                                frag = frag + '<option value="' + value.id + '">' + value.name + '</option>';
                            });
                            insertDom.innerHTML = frag;
                            if (callback) {
                                callback();
                            }
                        } else console.log(data.error);
                    },
                    error: function (err) {
                        console.log('error ')
                    }
                })
            }
            if(firstOption == '选择市'){
                var frag = '<option value="' + 0 + '">选择县区</option>';
                document.getElementById('district').innerHTML = frag;
            }
        }


        // 修改保存用户代理信息
        $(document).on('click', '.saveAgentBtn', function () {

            var id = $("#view_id").val();
            var name = $("#view_name").val();
            var weixin = $("#view_weixin").val();
            var phone = $("#view_phone").val();
            var idcard = $("#view_idcard").val();
            var address = $("#view_address").val();

            var district = $("#district").val();
            var addressDetail = $("#view_detail").val();

            var type = $("#view_type").val();

            if(district == 0){
                utils.tools.alert("请选择地区!", {timer: 2000, type: 'warning'});
                return;
            }else if(addressDetail == ''){
                utils.tools.alert("请输入街道门牌信息!", {timer: 2000, type: 'warning'});
                return;
            }

            var param = {
                id: id,
                name: name,
                weixin: weixin,
                phone: phone,
                idcard: idcard,
                address: address,
                type: type,
                district: district,
                addressDetail: addressDetail
            };

            utils.postAjax(window.host + '/userAgent/checkPhone', param, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.confirm('此代理的手机号已经存在于其他用户，修改后手机号对应的之前用户的相关信息将被删除,是否确认修改？', function () {
                            utils.postAjax(window.host + '/userAgent/save?updatePhone=1', param, function (res) {
                                if (typeof(res) === 'object') {
                                    if (res.data) {
                                        utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                        $datatables.search('').draw();
                                    } else {
                                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                    }
                                }
                                $("#modal_viewDetail").modal("hide");
                            });
                        }, function () {

                        });
                    } else {
                        utils.postAjax(window.host + '/userAgent/save', param, function (res) {
                            if (typeof(res) === 'object') {
                                if (res.data) {
                                    utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                    $datatables.search('').draw();
                                } else {
                                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                }
                            }
                            $("#modal_viewDetail").modal("hide");
                        });
                    }
                }
            });

        });

        $(".btn-search").on('click', function () {
            $datatables.search('').draw();
        });

        initZones();

    });