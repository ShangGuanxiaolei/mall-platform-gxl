/**
 * Created by jitre on 7/4/18.
 */
define(['jquery', 'utils', 'utils/fileUploader', 'sync/dataQuery', 'ramda',
      'jquerySerializeObject'],
    function ($, utils, uploader, data, R) {

      // 临时处理, 只适用于表单上只有一个导入按钮的情况
      const fileUploadUrl = jobType => window.host
          + `/file/upload?type=DOC&jobType=${R.defaultTo('', jobType)}`;
      const checkJobUrl = jobId => `${window.host}/point/balance/jobFileInfo?jobId=${jobId}`;
      const dropZoneDom = '#import';
      const form = $("#uploader-form");
      const modal = $("#modal-import-excel");
      let dropZoneInstance = '';

      let currJobId = '';

      let eventInitialization = (function () {
        return {
          init: function () {
            return this;
          },
          listenSyncButton: function () {
            $("#sync").off("click").on("click", function () {
              if (dropZoneInstance.dropZone.getUploadingFiles().length > 0) {
                utils.tools.error("请等待上传完成!");
                return;
              }
              let obj = form.serializeObject();
              if (!obj.key) {
                utils.tools.error("请先上传!");
                return;
              }
              // FIXME 最后的时间参数只在withdraw时有效, 校验再提交处已处理
              data.sync(obj.key, obj.type, obj.jobId, $('.select2-time').val(),
                  $('.select2-platform').val());
              modal.modal("hide");
            });
            return this;
          },
          listenImportButton: function () {
            $(".import").off("click").on("click", function () {
              let type = $(this).attr("data-type");
              if (type === 'withdraw') {
                const month = $('.select2-time').val();
                if (!month || month === '') {
                  utils.tools.error('请选择落账月份');
                  return;
                }
              }
              $("input[name=type]").prop("value", type);
              modal.modal("show");
            });
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              form[0].reset();
              dropZoneInstance.clear();
            });
            return this;
          },
          initDropZone: function () {
            const params = {
              dom: dropZoneDom,
              url: fileUploadUrl($('.import').attr('job-type')),
              allowTypes: '.xls,.xlsx',
              maxSize: 10,
              maxFiles: 1,
              onSuccess: (file) => {
                const jobId = file.jobId;
                $("input[name=key]").prop("value", file.url);
                $("input[name=jobId]").prop("value", jobId);
                currJobId = jobId;
                console.log(file);
              },
              onRemove: console.log
            };
            dropZoneInstance = uploader.create(params);
            return this;
          },
          jobListener: function () {
            setInterval(function () {
              if (!R.isEmpty(currJobId)) {
                utils.post(checkJobUrl(currJobId), data => {
                  if (data == null) {
                    utils.tools.error('任务查询失败');
                    return;
                  }
                  if (data.jobStatus === 3) {
                    utils.tools.alert(`文件 ${data.fileName} 导入成功`, {timer: 5000, type: 'success'});
                    currJobId = '';
                    return;
                  }
                  if (data.jobStatus === 4) {
                    utils.tools.alert(`文件 ${data.fileName} 导入失败:\n ${data.exceptionMsg}`, {timer: 5000, type: 'warning'});
                    currJobId = '';
                  }
                })
              }
            }, 4000)
          }
        }
      })();

      //init
      eventInitialization.init().resetForm().initDropZone()
      .listenImportButton().listenSyncButton();
    });
