/**
 * Created by chh on 16/10/19.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/tweet/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增发现记录
    $(".btn-release").on('click', function() {
        window.location.href = '/sellerpc/tweet/edit';
    });

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'+row.id+'"></label>';
                }
            },
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.targetType)
                    {
                        case 'PRODUCT':
                            status = '商品';
                            break;
                        case 'GROUPON':
                            status = '团购';
                            break;
                        case 'ACTIVITY':
                            status = '活动';
                            break;
                        case 'CATEGORY':
                            status = '类别';
                            break;
                        case 'CUSTOMIZED':
                            status = '自定义';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                data: "targetName",
                width: "120px",
                orderable: false,
                name:"targetName"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
            var pId =  $(this).attr("rowId");
            window.location.href = '/sellerpc/tweet/edit?pId='+pId;
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteTweet(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*将发现数据手动删除*/
    function  deleteTweet(pId){
          var url = window.host + "/tweet/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存组件
    $(".saveBtn").on('click', function() {
        var url = window.host + '/viewPage/save';
        var id = $("#id").val();
        var title = $("#title").val();
        var description = $("#description").val();
        var pageType = $("#pageType").val();
        var layout = $("#layout").val();
        var backgroundImg = $("#backgroundImg").val();
        var customizedUrl = $("#customizedUrl").val();

        if(!title || title == '' ){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }else if(!layout || layout == ''){
            utils.tools.alert("请输入页面布局!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            title: title,
            description: description,
            pageType: pageType,
            layout: layout,
            backgroundImg: backgroundImg,
            customizedUrl: customizedUrl
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/view/viewpage';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});