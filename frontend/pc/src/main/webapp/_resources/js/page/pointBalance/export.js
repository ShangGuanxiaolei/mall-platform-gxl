define(['jquery', 'ramda', 'utils', 'moment',  'utils/dateRangePicker',
  'utils/fileUploader',
  'jquerySerializeObject',
  'select2'], function ($, R, utils, moment,datePricker) {


  const DATE_FORMAT = "YYYY-MM-DD";
  /**
   * 初始化时间控件
   */
  const picker = datePricker.createPicker({
    dom: '.daterange-basic',
    onApply:function (ev, picker) {
      $('#valid_from').val(picker.startDate.format(DATE_FORMAT));
      $('#valid_to').val(picker.endDate.format(DATE_FORMAT));
    }
  });

  const key = $('#key').val();
  const orderExportUrl = {
    'info':(month, platform) => `/v2/point/exportExcel/${key}?month=${month}&platform=${platform}`,
    'detail':(month, platform) => `/v2/point/exportExcel/${key}/detail?month=${month}&platform=${platform}`
  };

  const laborExportUrl = (start,end) => `/v2/point/exportExcel/laborExpenses?start=${start}&end=${end}`;

  const withdrawMonthUrl =
      platform => `/v2/openapi/users/withdraw/months?platform=${platform}`;

  const monthType = $('#monthType').val();

  const $selectMonth = $('.select2-time');
  const $selectPlatform = $('.select2-platform').select2({
    minimumResultsForSearch: Infinity
  });
  const $expotBtn = $('#export-order');
  const $exportLaborBtn = $('#export-order-labor');

  const indexedMap = R.addIndex(R.map);

  /**
   * 根据月份生成option的html
   */
  const initOptionHtml = R.compose(
      R.join(''),
      R.map(m => `<option value="${m.format('YYYYMM')}">${m.format(
          'YYYYMM')}</option>`),
      indexedMap((m, idx) => {
        const cMoment = moment(m);
        return cMoment.subtract(idx, 'months');
      }),
      R.repeat(moment())
  );

  // 生成月份html
  const monthHtml = initOptionHtml(13);

  /**
   * 提供月份html
   * @type {{NORMAL: Promise<any>, WITHDRAW: Promise<any>}}
   */
  const monthProviderMapping = {
    normal: () => new Promise(function (resolve) {
      resolve(monthHtml)
    }),
    withdraw: (platform = '') => new Promise(function (resolve) {
      utils.post(withdrawMonthUrl(platform), months => {
        if (!months || months.length === 0) {
          resolve('<option value="">暂无可选择月份</option>');
        } else {
          resolve(R.map(m => `<option value="${m}">${m}</option>`)(months))
        }
      })
    })
  };

  // 初始化select2
  monthProviderMapping[monthType]().then(function (options) {
    $selectMonth.append(options).select2({
      minimumResultsForSearch: Infinity,
    });
  }).catch(console.log);

  /**
   * 月份在选择平台后重新初始化
   */
  $selectPlatform.on('change', e => {
    const $this = $(e.currentTarget);
    // 月份选框初始化
    const platform = $this.val();
    const func = monthProviderMapping[monthType];
    if (func) {
      func(platform).then(options => {
        $selectMonth.empty().append('<option>请选择时间</option>')
        .append(options).select2({
          minimumResultsForSearch: Infinity,
        });
      });
    }
  });

  $expotBtn.on('click', e => {
    // const $this = $(e.currentTarget);
    const selectMonth = $selectMonth.val();
    if (!selectMonth || selectMonth === '') {
      utils.tools.error('请选择导出月份');
      return;
    }
    let platform;
    if ($selectPlatform.length > 0) {
      platform = $selectPlatform.val();
      if (!platform || platform === '') {
        utils.tools.error('请选择导出平台');
        return;
      }
    }
    console.log($(e.currentTarget).attr("type"));
    location.href = orderExportUrl[$(e.currentTarget).attr("type")](selectMonth, platform);
  });

  $exportLaborBtn.on('click', e => {
    const start = $("#valid_from").val();
    const end = $("#valid_to").val();

    if (!start || start.trim() === '' || !end || end.trim() === '') {
      utils.tools.error('请选择导出时间范围');
      return;
    }

    utils.postAjax(laborExportUrl(start,end),null, ret => {
      if (ret === -1) {
        location.href = laborExportUrl(start,end)
      } else {
        utils.tools.error(ret.moreInfo)
      }
    })
  });

});