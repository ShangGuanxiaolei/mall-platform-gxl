define(['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui',
  'select2'], function ($, utils) {

  const prefix = window.host;
  const listPreOrder = prefix + '/preOrder/listApply';
  const listFlashSale = prefix + '/flashSale/listApply';

  const auditPreOrderUrl = prefix + '/preOrder/audit';
  const auditFlashSale = prefix + '/preOrder/audit';

  var $preOrderTable;
  var $flashSaleTable;

  const statusMapper = {
    APPLYING: '申请中，待审核',
    REJECTED: '已拒绝',
    SUCCESS: '已完成',
    PASS: '审核通过',
  };

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const manager = (function () {

    const globalInstance = {
      initPreOrderTable: function () {
        if (!$preOrderTable) {
          $preOrderTable = $('#xquark_preorder_audit_list')
          .DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
              $.get(listPreOrder, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
              }, function (res) {
                if (!res.data) {
                  utils.tools.alert(res.moreInfo,
                      {timer: 1200, type: 'warning'});
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              })
            },
            rowId: 'id',
            columns: [
              {
                title: '活动名称',
                data: "promotionName",
                width: "60px",
                orderable: false,
                name: "promotionName"
              },
              {
                title: '购买商品',
                data: "productName",
                width: "60px",
                orderable: false,
                name: "productName"
              },
              {
                title: '订单号',
                data: "orderNo",
                width: "60px",
                orderable: false,
                name: "orderNo"
              },
              {
                title: '申请人',
                data: "userName",
                width: "60px",
                orderable: false,
                name: "userName"
              },
              {
                title: '申请人手机号',
                orderable: false,
                width: "80px",
                render: function (data, type, row) {
                  var phone = row.userPhone;
                  return phone || '无';
                },
                name: "userPhone"
              },
              {
                title: '状态',
                orderable: false,
                width: "80px",
                render: function (data, type, row) {
                  var status = row.status;
                  return statusMapper[status];
                },
                name: "status"
              },
              {
                title: '申请时间',
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.applyTime);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "applyTime"
              },
              {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  var status = row.status;
                  if (!(status === 'PASS' || status === 'SUCCESS')) {
                    html += '<a href="javascript:void(0);" style="margin-right: 10px" class="pass" rowId="'
                        + row.id + '" ><i class="icon-pencil7" ></i>审核通过</a>';
                  }
                  html += '<a href="javascript:void(0);" class="reject" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>拒绝申请</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
              $('.pass').on('click', function () {
                var id = $(this).attr('rowId');
                utils.tools.confirm('确认审核通过吗?', function () {
                  changeStatus(auditPreOrderUrl, {applyId: id, status: 'PASS'}, $preOrderTable.search('').draw);
                }, function () {
                });
              });
              $('.reject').on('click', function () {
                var id = $(this).attr('rowId');
                utils.tools.confirm('确认拒绝该申请吗', function () {
                  changeStatus(auditPreOrderUrl, {applyId: id, status: 'REJECTED'}, $preOrderTable.search('').draw);
                }, function () {
                });
              });
            }
          });
        }
        return this;
      },
      initFlashSaleTable: function () {
        if (!$flashSaleTable) {
          $flashSaleTable = $('#xquark_flashsale_audit_list')
          .DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
              $.get(listFlashSale, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
              }, function (res) {
                if (!res.data) {
                  utils.tools.alert(res.moreInfo,
                      {timer: 1200, type: 'warning'});
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              })
            },
            rowId: 'id',
            columns: [
              {
                title: '活动名称',
                data: "promotionName",
                width: "60px",
                orderable: false,
                name: "promotionName"
              },
              {
                title: '购买商品',
                data: "productName",
                width: "60px",
                orderable: false,
                name: "productName"
              },
              {
                title: '订单号',
                data: "orderNo",
                width: "60px",
                orderable: false,
                name: "orderNo"
              },
              {
                title: '申请人',
                data: "userName",
                width: "60px",
                orderable: false,
                name: "userName"
              },
              {
                title: '申请人手机号',
                orderable: false,
                width: "80px",
                render: function (data, type, row) {
                  var phone = row.userPhone;
                  return phone || '无';
                },
                name: "userPhone"
              },
              {
                title: '状态',
                orderable: false,
                width: "80px",
                render: function (data, type, row) {
                  var status = row.status;
                  return statusMapper[status];
                },
                name: "status"
              },
              {
                title: '申请时间',
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.applyTime);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "applyTime"
              },
              {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  var status = row.status;
                  if (!(status === 'PASS' || status === 'SUCCESS')) {
                    html += '<a href="javascript:void(0);" style="margin-right: 10px" class="pass" rowId="'
                        + row.id + '" ><i class="icon-pencil7" ></i>审核通过</a>';
                  }
                  html += '<a href="javascript:void(0);" class="reject" rowId="'
                        + row.id + '" ><i class="icon-pencil7" ></i>拒绝申请</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
              $('.pass').on('click', function () {
                var id = $(this).attr('rowId');
                utils.tools.confirm('确认审核通过吗?', function () {
                  changeStatus(auditFlashSale, {applyId: id, status: 'PASS'}, $flashSaleTable.search('').draw);
                }, function () {
                });
              });
              $('.reject').on('click', function () {
                var id = $(this).attr('rowId');
                utils.tools.confirm('确认拒绝该申请吗?', function () {
                  changeStatus(auditFlashSale, {applyId: id, status: 'REJECTED'}, $flashSaleTable.search('').draw);
                }, function () {
                });
              });
            }
          });
        }
        return this;
      }
    };

    return {
      initGlobal: function () {
        return this;
      },
      initTable: function () {
        globalInstance.initFlashSaleTable()
        .initPreOrderTable();
        return this;
      }
    }
  })();

  /**
   * 封装修改审核状态
   * @param url
   * @param data
   * @param callBack
   */
  function changeStatus(url, data, callBack) {
    utils.postAjax(url, data, function (res) {
        if (res === -1) {
            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
        }
        if (typeof res === 'object') {
            if (res.errorCode === 200) {
              utils.tools.alert('操作成功', { timer: 1200, type: 'success' });
              if (callBack) {
                callBack();
              }
            } else {
                if (res.moreInfo) {
                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                } else {
                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                }
            }
        }
    });
  }

  manager.initGlobal()
  .initTable();

});
