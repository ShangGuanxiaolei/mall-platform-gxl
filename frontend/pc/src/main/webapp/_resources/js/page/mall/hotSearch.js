define(['jquery', 'utils', 'datatables'], function ($, utils, datatabels) {

  // $dataTable = '';
  // const tableId = "#couponTable";
  // //-------------------
  // const urlPrefix = '/bos';
  // const listUrl = `${urlPrefix}/couponManager/list`;
  //
  //
  // let eventInitialization = function () {
  //   return {
  //     withSearchEnable: function () {
  //       searchButton.on("click", function () {
  //         $dataTable.search('').draw();
  //       });
  //       return this;
  //     },
  //     initDataTable: function () {
  //       /** 页面表格默认配置 **/
  //       $.extend($.fn.dataTable.defaults, {
  //         autoWidth: false,
  //         dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
  //         language: {
  //           search: '<span>筛选:</span> _INPUT_',
  //           lengthMenu: '<span>显示:</span> _MENU_',
  //           info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
  //           paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
  //           infoEmpty: "",
  //           emptyTable: "暂无相关数据"
  //         }
  //       });
  //       $dataTable = $(tableId).DataTable({
  //         paging: true, //是否分页
  //         filter: false, //是否显示过滤
  //         lengthChange: false,
  //         processing: false,
  //         serverSide: true,
  //         deferRender: false,
  //         searching: false,
  //         ordering: false,
  //         sortable: false,
  //         ajax: function (data, callback, settings) {
  //           $.post(listUrl, {
  //             rows: data.length,
  //             page: function () {
  //               let page = (data.start / data.length);
  //               return page + 1;
  //             },
  //             status_kwd: couponSelect.val(),
  //             name_kwd: couponName.prop("value"),
  //             valid_from_kwd: function () {
  //               let range = couponRange.prop("value");
  //               if (range) {
  //                 return range.split("-")[0].trim();
  //               }
  //               return '';
  //
  //             },
  //             valid_to_kwd: function () {
  //               let range = couponRange.prop("value");
  //               if (range) {
  //                 return range.split("-")[1].trim();
  //               }
  //               return '';
  //             },
  //             pageable: true,
  //           }, function (res) {
  //             if (!res.rows) {
  //               res.rows = [];
  //             }
  //             callback({
  //               recordsTotal: res.total,
  //               recordsFiltered: res.total,
  //               data: res.rows,
  //               iTotalRecords: res.total,
  //               iTotalDisplayRecords: res.total
  //             });
  //           });
  //         },
  //         rowId: "id",
  //         columns: [
  //           {
  //             width: "10px",
  //             orderable: false,
  //             render: function (data, type, row) {
  //               return `<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="${row.id}" value="${row.id}"></label>`;
  //             }
  //           },
  //           {
  //             data: "name",
  //             orderable: false,
  //             title: "优惠券名称"
  //           },
  //           {
  //             data: "batchNo",
  //             orderable: false,
  //             title: "批次号"
  //           },
  //           {
  //             data: "distributedAt",
  //             orderable: false,
  //             title: "发行日期",
  //             render: function (date, type, row) {
  //               let data = parseInt(row.distributedAt);
  //               let d = new Date(data);
  //               return d.format('yyyy-MM-dd');
  //             }
  //           },
  //           {
  //             data: "amount",
  //             orderable: false,
  //             title: "总数"
  //           },
  //           {
  //             data: "discount",
  //             orderable: false,
  //             title: "面值"
  //           },
  //           {
  //             data: "remainder",
  //             orderable: false,
  //             title: "剩余"
  //           },
  //           {
  //             data: "totalDiscount",
  //             orderable: false,
  //             title: "总金额"
  //           },
  //           {
  //             data: "validFrom",
  //             orderable: false,
  //             render: function (date, type, row) {
  //               let from = parseInt(row.validFrom);
  //               let to = parseInt(row.validTo);
  //               return new Date(from).format('yyyy-MM-dd') + '~' + new Date(to).format('yyyy-MM-dd');
  //             },
  //             title: "时效"
  //           },
  //           {
  //             data: "status",
  //             orderable: false,
  //             render: function (date, type, row) {
  //               return statusCache.get(row.status);
  //             },
  //             title: "状态"
  //           },
  //           {
  //             title: '操作',
  //             orderable: false,
  //             render: function (data, type, row) {
  //               let html = '';
  //               html += `<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="${row.id}" ><i class="icon-pencil7" ></i>编辑</a>`;
  //               html += `<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="${row.id}" ><i class="icon-trash"></i>使失效</a>`;
  //               return html;
  //             }
  //           }
  //         ],
  //         drawCallback: function () {  //数据加载完成回调
  //           $(".edit").on("click",function(){
  //             let id=$(this).attr("rowId");
  //             addCoupon.edit(id);
  //           });
  //         }
  //       });
  //       return this;
  //     },
  //   }
  //
  // }();
  //
  // return {
  //   init: function () {
  //     eventInitialization.initDataTable().withSearchEnable();
  //   },
  // };

  // var pageS = 0;
  //
  // var sizeS = 9;
  //
  // var total = 0;
  //
  // function allStart(pageFront, page) {
  //   $.ajax({
  //     "url": "/v2/product/listHotSearchKeys",
  //     "data": "page=" + pageFront + "&size=" + page,
  //     "type": "get",
  //     "dataType": "json",
  //     "success": function (obj) {
  //       allClear();
  //       tableStart(obj);
  //       pageStart(obj, pageFront, page);
  //       eventStart(pageFront, page);
  //     },
  //     "error": function () {
  //       alert(21515);
  //     }
  //   });
  // }
  //
  // function tableStart(obj) {
  //   $(".hot-result").append("<tr class=\"hot-each hot-title\">\n"
  //       + "              <td class=\"first-hot\" ><input type=\"checkbox\" id='checkAllGoods'/>全选</td>\n"
  //       + "              <td>标题</td>\n"
  //       + "              <td class=\"third-hot\">发布时间</td>\n"
  //       + "              <td class=\"fourth-hot\">管理</td>\n"
  //       + "            </tr>");
  //   for (var i = 0; i < obj.data.length; i++) {
  //     if (obj.data[i].status == 1) {
  //       var btn = " checked ";
  //       var btnSw = "";
  //     } else {
  //       var btn = "";
  //       var btnSw = " checked ";
  //     }
  //     var id = obj.data[i].id;
  //     $(".hot-result").append("<tr class=\"hot-each\">\n"
  //         + "              <td class=\"first-hot\"><input type=\"checkbox\" name='checkGoods' rowId='"
  //         + id + "'/></td>\n"
  //         + "              <td>" + obj.data[i].hkey + "</td>\n"
  //         + "              <td class=\"third-hot\">"
  //         + timestampToTime(obj.data[i].createdAt / 1000) + "</td>\n"
  //         + "              <td class=\"fourth-hot\">\n"
  //         + "                <input type=\"radio\" name=\"open" + i + "\" "
  //         + btn + " class='open-hot'/>启用\n"
  //         + "                <input type=\"radio\" name=\"open" + i + "\" "
  //         + btnSw + " class='close-hot'/>关闭\n"
  //         + "                <a href=\"#\" class=\"hot-edit-radio\">编辑</a>\n"
  //         + "                <a href=\"#\" class=\"hot-delete-radio\" rowid='"
  //         + id + "'>删除</a>\n"
  //         + "              </td>\n"
  //         + "              <input type='hidden' value='" + id
  //         + "'    class='hotGo" + i + "'/>\n"
  //         + "            </tr>");
  //   }
  //
  //   $(".open-hot").click(function (e) {
  //     // console.log(e.target);
  //     var newId = $(e.target).parent().next().val();
  //     switchStatus(newId, 1);
  //   });
  //
  //   $(".close-hot").click(function (e) {
  //     var newId = $(e.target).parent().next().val();
  //     switchStatus(newId, 0);
  //   });
  //
  //   $(".hot-edit-radio").click(function (e) {
  //     var newId = $(e.target).parent().next().val();
  //     // $.ajax({
  //     //   "url": "/v2/product/hotSearchKeys/" + newId,
  //     //   "data": "",
  //     //   "type": "get",
  //     //   "dataType": "json",
  //     //   "success": function (obj) {
  //     //     var product = obj.data.productId;
  //     //     var name = obj.data.hkey;
  //     //     editHot(newId, name, product);
  //     //   },
  //     //   "error": function (obj) {
  //     //     alert("查询错误");
  //     //   }
  //     // });
  //     window.location.href = "/sellerpc/mall/addHot?type=1&id=" + newId;
  //   });
  //
  //   $("#checkAllGoods").on('click', function () {
  //     $("input[name='checkGoods']").prop("checked",
  //         $(this).prop("checked"));
  //   });
  //
  //   $(".hot-delete-radio").click(function (e) {
  //     var id = $(e.target).parent().next().val();
  //     $.ajax({
  //       "url": "/v2/product/hotSearchKeys/remove",
  //       "data": "ids=" + id,
  //       "type": "post",
  //       "dataType": "json",
  //       "success": function (obj) {
  //         // var product = obj.data.productId;
  //         // var name = obj.data.hkey;
  //         // editHot(newId, name, product);
  //         // $("#hot-word").val(name);
  //         // var pIds=product.split(",");
  //         // for(var i=0;i<pIds.length;i++){
  //         //   ids.add(pIds[i]);
  //         // }
  //         utils.tools.alert("删除成功!",
  //             {timer: 1200, type: 'success'});
  //       },
  //       "error": function (obj) {
  //         alert("查询错误");
  //       }
  //     });
  //   });
  // }
  //
  // function allClear() {
  //   $(".hot-result").html("");
  // }
  //
  // function pageStart(obj) {
  //
  // }
  //
  // var totalPage;
  //
  // function getHotTotal() {
  //   $.ajax({
  //     "url": "/v2/wmsupdate/hotSearch",
  //     "data": "",
  //     "type": "get",
  //     "dataType": "json",
  //     "success": function (obj) {
  //       totalPage = obj.data.total;
  //     },
  //     "error": function () {
  //       alert(12333141);
  //     }
  //   });
  // }
  //
  // function eventStart(pageFront, page) {
  //   $.ajax({
  //     "url": "/v2/product/getHotLimit",
  //     "data": "",
  //     "type": "get",
  //     "dataType": "json",
  //     "success": function (obj) {
  //       // var product = obj.data.productId;
  //       // var name = obj.data.hkey;
  //       // editHot(newId, name, product);
  //       // $("#hot-word").val(name);
  //       // var pIds=product.split(",");
  //       // for(var i=0;i<pIds.length;i++){
  //       //   ids.add(pIds[i]);
  //       // }
  //       // utils.tools.alert("一共" + updateds.length + "个删除成功!",
  //       //     {timer: 1200, type: 'success'});
  //       // allStart(pageS,sizeS);
  //       $(".max-hot").val(obj.data);
  //     },
  //     "error": function (obj) {
  //       alert("查询错误");
  //     }
  //   });
  // }
  //
  // $(function () {
  //   console.log(pageS + "," + sizeS);
  //   allStart(pageS, sizeS);
  //   $(".new-hot-btn").click(function () {
  //     window.location.href = "/sellerpc/mall/addHot?type=0&id=999";
  //   });
  //   $(".max-hot").blur(function () {
  //     var number = $(".max-hot").val();
  //     $.ajax({
  //       "url": "/v2/product/updateDisplayLimit",
  //       "data": "limit=" + number,
  //       "type": "get",
  //       "dataType": "json",
  //       "success": function (obj) {
  //         // var product = obj.data.productId;
  //         // var name = obj.data.hkey;
  //         // editHot(newId, name, product);
  //         // $("#hot-word").val(name);
  //         // var pIds=product.split(",");
  //         // for(var i=0;i<pIds.length;i++){
  //         //   ids.add(pIds[i]);
  //         // }
  //         // utils.tools.alert("一共" + updateds.length + "个删除成功!",
  //         //     {timer: 1200, type: 'success'});
  //         // allStart(pageS,sizeS);
  //       },
  //       "error": function (obj) {
  //         alert("查询错误");
  //       }
  //     });
  //   });
  //
  //   $.ajax({
  //     "url": "/v2/product/getNumOfHot",
  //     "data": "",
  //     "type": "get",
  //     "dataType": "json",
  //     "success": function (obj) {
  //       // var product = obj.data.productId;
  //       // var name = obj.data.hkey;
  //       // editHot(newId, name, product);
  //       // $("#hot-word").val(name);
  //       // var pIds=product.split(",");
  //       // for(var i=0;i<pIds.length;i++){
  //       //   ids.add(pIds[i]);
  //       // }
  //       // utils.tools.alert("一共" + updateds.length + "个删除成功!",
  //       //     {timer: 1200, type: 'success'});
  //       // allStart(pageS,sizeS);
  //       total = obj.data;
  //     },
  //     "error": function (obj) {
  //       alert("查询错误");
  //     }
  //   });
  // });
  //
  // function pageStart(obj, pageFront, page) {
  //   $(".hot-page").html("当前" + (pageFront + 1) + "-" + obj.data.length
  //       + "条 一共" + Math.ceil(total / sizeS) + "页");
  //   $(".hot-page-total").html("<li><a class=\"hot-goup\">上一页</a></li>\n"
  //       + "            <li><a class=\"hot-godown\">下一页</a></li>");
  //   $(".hot-goup").click(function () {
  //     if (pageS <= 0) {
  //       pageS = 0;
  //     } else {
  //       pageS--;
  //     }
  //     allStart(pageS, sizeS);
  //   });
  //   $(".hot-godown").click(function () {
  //     var pageC = (pageS + 1) * sizeS;
  //     if (pageC > total) {
  //       utils.tools.alert("最后一页", {timer: 1200, type: 'warning'});
  //       return;
  //     }
  //     pageS++;
  //     allStart(pageS, sizeS);
  //   });
  // }
  //
  // function timestampToTime(timestamp) {
  //   var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
  //   var Y = date.getFullYear() + '-';
  //   var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1)
  //       : date.getMonth() + 1) + '-';
  //   var D = date.getDate() + ' ';
  //   var h = date.getHours() + ':';
  //   var m = date.getMinutes() + ':';
  //   var s = date.getSeconds();
  //   return Y + M + D + h + m + s;
  // }
  //
  // // $(".new-hot-btn").click(function () {
  // //   allClear();
  // //   $(".hot-result").append("<tr class=\"hot-each hot-title\">\n"
  // //       + "              <td class=\"first-hot\"><input type=\"checkbox\"/>全选</td>\n"
  // //       + "              <td>热词名称</td>\n"
  // //       + "              <td class=\"third-hot\">产品信息</td>\n"
  // //       + "              <td class=\"fourth-hot\">管理</td>\n"
  // //       + "            </tr>");
  // //   $(".hot-result").append("<tr class=\"hot-each\">\n"
  // //       + "              <td class=\"first-hot\"><input type=\"checkbox\"/></td>\n"
  // //       + "              <td><input type=\"text\" class=\"hot-input-add\" placeholder=\"热词名称\" name=\"hotName\"/></td>\n"
  // //       + "              <td class=\"third-hot\"><input type=\"text\" class=\"hot-input-add-product\" placeholder=\"产品id，多个用逗号隔开\" name=\"hotProduct\"/></td>\n"
  // //       + "              <td class=\"fourth-hot\">\n"
  // //       + "                <a href=\"#\" class=\"hot-save\">保存</a>\n"
  // //       + "              </td>\n"
  // //       + "            </tr>");
  // //
  // //   $(".hot-save").click(function () {
  // //     var name = $(".hot-input-add").val();
  // //     var product = $(".hot-input-add-product").val();
  // //     var newProduct = product.split(",");
  // //     var productGo = "";
  // //     for (var i = 0; i < newProduct.length; i++) {
  // //       productGo += "&"
  // //     }
  // //     $.ajax({
  // //       "url": "/v2/product/createHotSearchKey",
  // //       "data": "key=" + name + "&productIds=" + product,
  // //       "type": "post",
  // //       "dataType": "json",
  // //       "success": function (obj) {
  // //         if (obj.data) {
  // //           alert("添加成功");
  // //           allStart(0, 9);
  // //         } else {
  // //           alert("商品存在");
  // //         }
  // //       },
  // //       "error": function () {
  // //         alert("添加失败");
  // //       }
  // //     });
  // //   });
  // // });
  //
  // function switchStatus(id, btn) {
  //   $.ajax({
  //     "url": "/v2/product/updateKeyState",
  //     "data": "id=" + id + "&state=" + btn,
  //     "type": "get",
  //     "dataType": "json",
  //     "success": function (obj) {
  //
  //     },
  //     "error": function (obj) {
  //
  //     }
  //   });
  // }
  //
  // function editHot(id, name, product) {
  //   allClear()

  //   $(".hot-result").append("<tr class=\"hot-each hot-title\">\n"
  //       + "              <td class=\"first-hot\"><input type=\"checkbox\"/>全选</td>\n"
  //       + "              <td>热词名称</td>\n"
  //       + "              <td class=\"third-hot\">产品信息</td>\n"
  //       + "              <td class=\"fourth-hot\">管理</td>\n"
  //       + "            </tr>");
  //   $(".hot-result").append("<tr class=\"hot-each\">\n"
  //       + "              <td class=\"first-hot\"><input type=\"checkbox\"/></td>\n"

  //       + "              <td><input type=\"text\" class=\"hot-input-edit\" placeholder=\"热词名称\" name=\"hotName\" value='"
  //       + name + "'/></td>\n"
  //       + "              <td class=\"third-hot\"><input type=\"text\" class=\"hot-input-add-edit\" placeholder=\"产品id，多个用逗号隔开\" name=\"hotProduct\" value='"
  //       + product + "'/></td>\n"
  //       + "              <input type='hidden' value='" + id
  //       + "'    class='hotGo'/>\n"
  //       + "              <td class=\"fourth-hot\">\n"
  //       + "                <a href=\"#\" class=\"hot-edit\">保存</a>\n"
  //       + "              </td>\n"
  //       + "            </tr>");
  //
  //   $(".hot-edit").click(function (e) {
  //     var id = $(".hotGo").val();
  //     var name = $(".hot-input-edit").val();
  //     var product = $(".hot-input-add-edit").val();
  //     $.ajax({
  //       "url": "/v2/product/updateHotSearchKey",
  //       "data": "id=" + id + "&key=" + name + "&productIds=" + product,
  //       "type": "get",
  //       "dataType": "json",
  //       "success": function (obj) {
  //         if (obj.data) {
  //           alert("修改成功");
  //           allStart(page, size);
  //         } else {
  //           alert("未知原因");
  //         }
  //       },
  //       "error": function (obj) {
  //         alert("error");
  //       }
  //     });
  //   });
  // }
  //
  // function getTableContent() {
  //   var selectRows = new Array();
  //   for (var i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
  //     var value = {};
  //     var checkvalue = $("input[name='checkGoods']:checked")[i];
  //     value.id = $(checkvalue).attr("rowid");
  //     selectRows.push(value);
  //   }
  //   return selectRows;
  // }
  //
  // var ids = new Set();
  //
  // $(".hot-delete").click(function () {
  //   var updateds = getTableContent();
  //   if (updateds.length == 0) {
  //     utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
  //     return;
  //   }
  //   $.each(updateds, function (index, row) {
  //     ids.add(row.id);
  //   });
  //   console.log(updateds);
  //   var idsToS = "";
  //   ids.forEach(function (value) {
  //     idsToS += value + ",";
  //   });
  //   idsToS = idsToS.substr(0, idsToS.length - 1);
  //   console.log(idsToS);
  //   $.ajax({
  //     "url": "/v2/product/hotSearchKeys/remove",
  //     "data": "ids=" + idsToS,
  //     "type": "post",
  //     "dataType": "json",
  //     "success": function (obj) {
  //       // var product = obj.data.productId;
  //       // var name = obj.data.hkey;
  //       // editHot(newId, name, product);
  //       // $("#hot-word").val(name);
  //       // var pIds=product.split(",");
  //       // for(var i=0;i<pIds.length;i++){
  //       //   ids.add(pIds[i]);
  //       // }
  //       utils.tools.alert("一共" + updateds.length + "个删除成功!",
  //           {timer: 1200, type: 'success'});
  //       allStart(pageS, sizeS);
  //     },
  //     "error": function (obj) {
  //       alert("查询错误");
  //     }
  //   });
  //
  // });

  $(function(){
    console.log('ok=====')
  })
});
