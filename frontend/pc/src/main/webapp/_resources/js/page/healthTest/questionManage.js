define(['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils) {

    const prefix = window.host + '/healthTest';
    const listUrl = prefix + '/question/listTable';
    const treeList = prefix + '/module/jsTree';
    const importUrl = prefix + '/question/import';

    const $dataTables = $('#xquark_question_tables');

    /* 当前选中节点 */
    var selected = {
        'id': '0',
        'parent': null
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "未配置问题"
        }
    });

    const typeMapper = {
        SINGLE_CHOICE: '单选题',
        MULTIPLE_CHOICE: '多选题',
        BLANK_FILLING: '填空题'
    };

    const eventManager = (function () {
        const $addBtn = $('#add_question');
        const $importModal = $('#modal_import_logistic_info');
        const globalInstance = {
            bindEvent: function () {
                $addBtn.on('click', function () {
                    var id = selected.id;
                    utils.postAjax(prefix + '/module/subCounts', {id: id}, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                var subCounts = res.data;
                                if (subCounts === 0 && id !== '0') {
                                    window.location = '/sellerpc/healthTest/question/edit?moduleId=' + id;
                                } else {
                                    utils.tools.alert("父类菜单不允许添加问题", {timer: 1200, type: 'warning'});
                                }
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
                return this;
            },
            initFile: function () {
                $('.file-input').fileinput({
                    language: 'zh',
                    uploadUrl: importUrl,
                    previewFileType: 'text',
                    browseLabel: '选择文件',
                    removeLabel: '删除',
                    uploadLabel: '上传',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    browseClass: 'btn btn-primary',
                    uploadClass: 'btn btn-default',
                    removeClass: 'btn btn-danger',
                    initialCaption: '',
                    maxFilesNum: 5,
                    allowedFileExtensions: ["xls"],
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        footer: ''
                    }
                });

                $importModal.on('hidden.bs.modal', function () {
                    $('#file-input').fileinput('clear');
                });

                $importModal.on('fileuploaded', function (event, data, previewId, index) {
                    var form = data.form, files = data.files, extra = data.extra,
                        response = data.response, reader = data.reader;
                    console.log(data);
                    console.log(response);
                    $questionDataTables.search('').draw();
                    // if (response == '200') {
                    //     utils.tools.alert('文件上传成功');
                    //     $('#modal_import_logistic_info').modal('hide');
                    // } else if (response == '207') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('发货信息上传全部失败,请核对上传格式是否正确,或数据是否填写正确');
                    // } else if (response == '206') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('部分订单上传失败,请核对上传格式是否正确,或数据是否填写正确');
                    // } else if (response == '500') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
                    // }
                });
                return this;
            }
        };
        return {
            initGlobal: function () {
                globalInstance.bindEvent()
                    .initFile();
            },
            initTable: function () {
                $('.edit_question').on('click', function () {
                    var id = $(this).attr('rowId');
                    var moduleId = $(this).attr('moduleId');
                    var groupId = $(this).attr('groupId');
                    window.location = '/sellerpc/healthTest/question/edit?id=' + id + '&moduleId=' + moduleId + '&groupId=' + groupId;
                });

                $('.del_question').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认要删除吗', function () {
                        utils.postAjax(prefix + '/question/delete', {id: id}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    var data = res.data;
                                    if (data) {
                                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                                        $questionDataTables.search('').draw();
                                    } else {
                                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
                                    }
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                    } else {
                                        utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                    }
                                }
                            }
                        });
                    }, function () {

                    });
                });
            }
        }
    })();

    eventManager.initGlobal();

    const questionModuleTree = $('#question_module_tree');
    questionModuleTree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': treeList,
                'data': function (node) {
                    if (node === null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        questionModuleTree.jstree("select_node", "ul > li:first");
        var selectedNode = questionModuleTree.jstree("get_selected");
        questionModuleTree.jstree('open_node', selectedNode, false, true);
    }).on('delete_node.jstree', function (event, data) {
        // deleteModule(data.node.id);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    }).on('create_node.jstree', function (event, data) {
        $questionDataTables.search('').draw();
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        selected = data.node;
        $questionDataTables.search('').draw();
    }

    /** 初始化表格数据 **/
    const $questionDataTables = $dataTables.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                moduleId: selected.id,
                pageable: true
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                    return;
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '200px',
                orderable: true,
                name: 'name'
            },
            {
                width: '20px',
                orderable: true,
                render: function (data, type, row) {
                    var mapper = typeMapper[row.type];
                    return mapper ? mapper : '无';
                }
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    var correctAnswerId = row.answerId;
                    var answers = row.answers;
                    if (correctAnswerId && answers && answers.length > 0) {
                        var result = $.grep(answers, function (item) {
                            return item.id = correctAnswerId;
                        });
                        if (result.length === 1) {
                            return result[0].content;
                        }
                    } else {
                        return '无';
                    }
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_question" rowId="' + row.id + '" moduleId="' + row.moduleId + '" groupId="' + row.groupId + '" fid="edit_question"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del_question" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_question"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            eventManager.initTable();
        }
    });

});
