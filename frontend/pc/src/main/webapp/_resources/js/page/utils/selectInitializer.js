define(['jquery', 'select2'], function ($) {

  class Select2Wrapper {
    /**
     * 初始化select2
     * @param params
     * @param params.dom dom元素
     * @param params.provider 提供选择数据
     */
    constructor(params) {
      const $dom = $(params.dom);
      if ($dom.length === 0) {
        throw `元素 ${params.dom} 不存在`;
      }
      const provider = params.provider || (() => '');
      this.$select2 = $dom.append(provider()).select2({
        minimumResultsForSearch: Infinity,
      });
    }

    /**
     * 清空select2
     */
    clear() {
      this.$select2.empty();
      return this;
    }

    /**
     * 增加属性
     * @param provider 提供html的函数
     */
    append(provider) {
      if (!provider || !$.isFunction(provider)) {
        throw 'provider 必须是一个函数';
      }
      this.$select2.append(provider);
      return this;
    }

  }

  return {
    /**
     * 初始化select2
     * @param params
     * @param params.dom dom元素
     * @param params.provider 提供选择数据
     */
    init: function (params) {
      return new Select2Wrapper(params);
    }
  }

});
