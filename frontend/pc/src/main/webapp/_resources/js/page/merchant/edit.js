/**
 * Created by quguangming on 16/5/25.
 */
define(['form/validate','utils','md5'], function(validate,utils,md5) {

    var $form = $(".merchant-edit-form");

    var $loginName = $("#loginName");

    var $id = $("#id").val();

    var checkNameUrl = "/sellerpc/pc/registered";

    var saveUrl = "/sellerpc/merchant/admin/save";

    var checkPhoneUrl = "/sellerpc/merchant/checkPhone";

    function saveMerchant(form){

        var data = $(form).serializeObject();

        /**if (data.orgId == null || data.orgId == 74) {
            utils.tools.alert('请选择部门');
            return;
        }
        console.log(data.orgId);**/

        // TODO wangxh 暂时不设置角色
        // var strRoles = "";
        //for(var i = 0; i < data.roles .length; i++){
        //   strRoles += data.roles [i] + ",";
        //}
        // data.roles = strRoles;
        if (data.password != '' && data.password.length > 0) {
            data.password = CryptoJS.MD5(data.password).toString()
        }

        utils.getJson(saveUrl, data, function (result) {
            console.log($id);
            if(!$id) {
                utils.tools.alert("创建成功!", {timer: 1200, type:'success'});
            } else{
                utils.tools.alert("修改成功!", {timer: 1200, type:'success'});
            }
          window.location.href = "/sellerpc/org/edit";
        });
    }

    var createForm = {

        rules:{
            loginname: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{1,16}$/,
                remote: {
                    url: checkNameUrl,
                    type: "Post",
                    data: {
                        loginName: function () { return $loginName.val(); }
                    }
                }
            },
            password: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{6,16}$/
            },
            rePassword:{
                required: true,
                equalTo: "#password"
            },
            phone:{
                required: true,
                pattern: /^1\d{10}$/,
                remote: {
                    url: checkPhoneUrl,
                    type: "Post",
                    data: {
                        phone: function () {
                            var $phone = $("#phone");
                            return $phone.val();
                        }
                    }
                }
            },
            //"roles[]":{
            //    required: true
            //},
            email:{
                required: true,
                email: true
            }
        },
        messages:{
          loginname:{
                required:"账号不能为空.",
                pattern: "必须是 1~16位字母、数字和下划线的组合.",
                remote:  "该账号已注册,请重新输入."
            },
            password:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            rePassword:{
                required:"确认密码不能为空.",
                equalTo:  "密码不一致."
            },
            //"roles[]": {
            //    required: "至少选择一个角色."
            // },
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone:{
                required: "手机号不能为空.",
                pattern: "手机格式不正确.",
                remote: "该手机号已注册,请重新输入."
            }
        },
        focusCleanup: true,
        submitCallBack: function(form) {

            saveMerchant(form);
        }
    };


    var editForm = {

        rules:{
            password: {
                pattern: /^[a-zA-Z0-9_]{6,16}$/
            },
            rePassword:{
                equalTo: "#password"
            },
            phone:{
                required: true,
                pattern: /^1\d{10}$/,
            },
            //"roles[]":{
            //    required: true
            //},
            email:{
                required: true,
                email: true
            }
        },
        messages:{
            password:{
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            rePassword:{
                equalTo:  "密码不一致."
            },
            // "roles[]": {
            //    required: "至少选择一个角色."
            //},
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone:{
                required: "手机号不能为空.",
                pattern: "手机格式不正确."
            }
        },
        focusCleanup: true,
        submitCallBack: function(form) {
            saveMerchant(form);
        }
    }

    if ($id){
        validate($form,editForm);
    } else{
        validate($form,createForm);
    }

});