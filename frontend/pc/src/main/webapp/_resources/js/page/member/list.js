define(['jquery', 'utils', 'datatables', 'blockui', 'select2'],
    function ($, utils, datatabels, blockui, select2) {

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          search: '<span>筛选:</span> _INPUT_',
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      /** 初始化选择框控件 **/
      $('.select').select2({
        minimumResultsForSearch: Infinity,
      });

      var account = $('input[name="account"]');
      var phone = $('input[name="phone"]');
      var memberLevel = $('select[name="memberLevel"]');
      var cardId = getParameterByName("memberLevel");
      var memberLevelJSON = null;
      var _userId = null;
      var $editLevelDiv = $('#level');
      var $modalEditLevel = $('#modal_edit_level');
      var removeEmployeeUrl = id => window.host
          + '/user/iEmployee/remove?employeeId=' + id;
      var updateEmployeeUrl = (id, userId) => window.host + `/user/iEmployee/update?id=${id}&userId=${userId}`;

      // 初始化会员卡选框
      initMemberLevel();

      const upgradeTypeMapping = {
        AUTO: '自动升级',
        MANUAL: '手动升级',
        BUY: '购买'
      };
      var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback) {
          $.get('/sellerpc/member/listAllData', {
            size: data.length,
            page: (data.start / data.length) + 1,
            orderIndex: data.order[0].column,
            isAsc: data.order ? data.order[0].dir == 'asc' : false,
            account: account.val(),
            memberLevel: cardId ? cardId : memberLevel.val(),
            phone: phone.val(),
            employeeId: $('#employeeId').val(),
            scope: $('select[name=userType]').val()
            //memberLevel: memberLevel.val()
          }, function (res) {
            if (!res.data.rows) {
              res.data.rows = [];
            }
            callback({
              recordsTotal: res.data.total,
              recordsFiltered: res.data.total,
              data: res.data.rows,
              iTotalRecords: res.data.total,
              iTotalDisplayRecords: res.data.total
            });
          });
        },
        rowId: "id",
        columns: [
          {
            width: "10px",
            orderable: false,
            render: function (data, type, row) {
              return '<label class="checkbox"><input name="checkMember" type="checkbox" class="styled" value="'
                  + row.id + '"></label>';
            }
          },
          {
            width: "50px",
            orderable: false,
            render: function (data, type, row) {
              if (row.avatar == null) {
                return '<i class="icon-user-minus icon-2x position-center"></i>';
              } else {
                return '<img class="sm-goods-image" src="' + row.avatar
                    + '" />';
              }
            }
          },
          {
            data: "name",
            width: "200px",
            orderable: false,
            name: "name"
          },
          {
            data: "phone",
            width: "180px",
            orderable: false,
            name: "phone"
          },
          {
            width: "50px",
            orderable: false,
            name: "userType",
            render: function (data, type, row) {
              return (row.employeeId && row.employeeId !== '') ? '内部员工'
                  : '普通会员';
            }
          },
          {
            orderable: true,
            width: "80px",
            name: "employeeId",
            render: function (data, type, row) {
              if (row.employeeId && row.employeeId !== '') {
                return `<div class="left">
                <input style="width: 50px;"
                  rowId="${row.id}" type="text" value="${row.employeeId}">
                <a href="javascript:void(0)" class="modifyEmployee" userId="${row.id}"><i class="icon-pencil7" ></i>修改</a>
                </div>`;
              }
              return '无';
            }
          },
          {
            width: "50px",
            orderable: false,
            name: "employeeName",
            render: function (data, type, row) {
              return row.employeeName || '无'
            }
          },
          {
            width: "80px",
            orderable: false,
            name: "operation",
            render: function (data, type, row) {
              const isEmployee = row.employeeId && row.employeeId !== '';
              if (isEmployee) {
                return '<a href="javascript:void(0);"    class="closeInside" employeeId="'
                    + row.employeeId + '">关闭员工资格</a> ';
              }
              return '';
            }
          }
        ],
        select: {
          style: 'multi'
        },
        order: [[3, 'asc']],
        drawCallback: function () {  //数据加载完成
          console.log('edit_level');
          $('.edit_level').on('click', function () {
            console.log('edit_level');
            var userId = $(this).attr('rowId');
            $('#userId').val(userId);
            $editLevelDiv.empty();
            var html = '';
            memberLevelJSON.filter(function (item) {
              return item.upgradeType.toString() === 'MANUAL';
            }).forEach(function (item) {
              html += '<label class="radio-inline"><input name="cardId" type="radio" value="'
                  + item.id + '"> ' + item.name + ' </label>';
            });
            $editLevelDiv.append($(html));
            $modalEditLevel.modal('show');
          });

          $('.closeInside').on('click', function () {
            const employeeId = $(this).attr('employeeId');
            utils.tools.confirm('确认撤销该员工资格吗?', function () {
              utils.post(removeEmployeeUrl(employeeId), res => {
                if (res && res === true) {
                  utils.tools.success('撤销成功');
                  $datatables.search('').draw();
                  return;
                }
                utils.tools.error('撤销失败');
              })
            });
          });

          $('.changeYundouBtn').on('click', function () {
            var userId = $(this).attr('userId');
            $("#y_userId").val(userId);
            $("#modal_edit_yundou").modal('show');
          });

          $('.modifyEmployee').on('click', function () {
            const userId = $(this).attr('userId');
            const $input = $(this).prev();
            const newEmployeeId = $input.val();
            utils.post(updateEmployeeUrl(newEmployeeId, userId), res => {
              if (res && res === true) {
                utils.tools.success('操作成功');
                return;
              }
              utils.tools.error('操作失败');
            })
          });

        }
      });

      $('.btn-search').on('click', function () {
        $datatables.search('').draw();
      });

      $('.editYundouBtn').on('click', function () {
        var userId = $('#y_userId').val();
        var yundou = $('#yundou').val();
        if (!yundou) {
          utils.tools.alert('请输入赠送积分', {timer: 1200, type: 'warning'});
          return;
        }
        var data = {
          userId: userId,
          yundou: yundou
        };
        console.log(data);
        utils.postAjax('/sellerpc/member/yundou/edit', data, function (res) {
          if (res === -1) {
            utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
            return;
          }
          if (typeof res === 'object') {
            var data = res.data;
            if (data) {
              utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
              $datatables.search('').draw('page');
              $("#modal_edit_yundou").modal('hide');
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
            }
          }
        })
      });

      $('.editLevelBtn').on('click', function () {
        var userId = $('#userId').val();
        var cardId = $('input[name=cardId]:checked').val();
        if (!cardId) {
          utils.tools.alert('请选择等级', {timer: 1200, type: 'warning'});
          return;
        }
        var data = {
          userId: userId,
          cardId: cardId
        };
        console.log(data);
        utils.postAjax('/sellerpc/member/card/bind', data, function (res) {
          if (res === -1) {
            utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
            return;
          }
          if (typeof res === 'object') {
            var data = res.data;
            if (data) {
              utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
              $datatables.search('').draw();
              $modalEditLevel.modal('hide');
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
            }
          }
        })
      });

      function initMemberLevel() {
        utils.postAjaxWithBlock($(document), '/sellerpc/member/card/list', null,
            function (res) {
              if (typeof(res) === 'object') {
                switch (res.errorCode) {
                  case 200: {
                    var total = res.data.total;
                    memberLevelJSON = res.data.list;
                    if (total && total > 0) {
                      memberLevelJSON.forEach(function (item) {
                        var option = '<option value="' + item.id + '">'
                            + item.name + '</option>';
                        memberLevel.append($(option));
                      });
                    }
                    if (cardId) {
                      console.log(cardId);
                      memberLevel.val(cardId)
                      .change();
                    }
                    break;
                  }
                  default: {
                    utils.tools.alert(res.moreInfo, {timer: 1200});
                    break;
                  }
                }
              } else if (res === 0) {

              } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
              }
            })
      }

      /**
       * 从URL中提取出参数
       * @param name
       * @param url
       * @returns {*}
       */
      function getParameterByName(name, url) {
        if (!url) {
          url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) {
          return null;
        }
        if (!results[2]) {
          return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      $(".exportBtn").on('click', function () {
        $('#exportForm').submit();
      });

    });