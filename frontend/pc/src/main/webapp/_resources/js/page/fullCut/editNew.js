define(['jquery', 'utils',
      'utils/dateRangePicker', 'utils/steps',
      'utils/dataTable',
      'jquerySerializeObject', 'form/validate'],
    function ($, utils, datePricker, steps, dataTable, validate) {

      const $productModal = $('#choose-product-modal');
      const $setDiscountModal = $('#setDiscountModal');
      const $setBatchDiscountModal = $('#setBatchDiscountModal');
      const $form = $('.full-cut-form');

      const productListUrl = window.host + '/promotion/listProductSelective';
      const savePromotionUrl = window.host + '/promotionFullCut/savePromotion';
      const viewPromotionUrl = id => window.host
          + `/promotionFullCut/view/${id}`;
      const hasActivatePromotionUrl = type => window.host
          + '/promotion/hasActivate?type=' + type;

      let isSelectedProduct = false;
      let settedGlobalDiscount = false;

      // 表格中被禁用了的按钮集合
      const disabledSelector = new Set();

      const id = utils.tools.request('id');
      if (id && id !== '') {
        // 编辑恢复数据
        resumeData(id);
      }

      /**
       * 初始化商品分类
       */
      refreshCategory();

      const validateForm = {
        rules: {
          title: {
            required: true
          },
          validFrom: {
            required: true
          },
          validTo: {
            required: true
          }
        },
        messages: {
          title: {
            required: '请输入活动标题',
          },
          validFrom: {
            required: '请选择活动日期'
          },
          validTo: {
            required: '请选择活动日期'
          }
        }
      };

      /**
       * 初始化流程控制
       */
      steps.create({
        dom: '.steps',
        headerTag: 'h6',
        bodyTag: 'fieldset',
        form: $form,
        validate: validateForm,
        // 表单提交
        onFinished: () => {
          const data = $form.serializeObject();

          const userScope = data.userScope;
          data.type = (userScope === 'ALL') ? 'FULLCUT' : 'INSIDE_BUY';

          // 是否包邮
          data.isFreeDelivery = $('input[name=isFreeDelivery]').is(':checked');
          // 没有选择全局折扣则获取自定义折扣
          if (!data.discount || data.discount === '' || data.discount === '0') {
            const productDetails = serializePromotionTable();
            if (!productDetails) {
              return;
            }
            data.productDetails = productDetails;
          }
          if ((!data.discount || data.discount === '')
              && data.productDetails.length
              === 0) {
            utils.tools.error('请选择商品折扣');
            return;
          }

          utils.post(savePromotionUrl, data => {
            if (data && data === true) {
              utils.tools.success('保存成功');
              location.href = 'fullCutList';
              return;
            }
            utils.tools.error('保存失败');
          }, {params: data});
          console.log(data);
        }
      });

      /**
       * 初始化时间控件
       */
      const picker = datePricker.createPicker({
        dom: '.daterange-time'
      });

      // 需要在jQuery上下文环境调用
      function tableSelectbind() {
        const $this = $(this);
        if ($this.hasClass('disabled')) {
          return;
        }
        const rowData = productSearchTable.rowData($this.parents('tr'));
        promotionViewTable.addRow(buildViewRow(rowData));
        if (!isSelectedProduct) {
          showTableContainer();
          isSelectedProduct = true;
        }
        disableRowSelect(rowData.id);
      }

      /**
       * 禁用指定id的表格选择按钮
       * @param id rowId
       */
      function disableRowSelect(id) {
        $(`.add_promotion_product[rowId=${id}]`).addClass('disabled')
        .off('click');
        disabledSelector.add(id);
      }

      /**
       * 启用指定id的表格选择按钮
       * @param id rowId
       */
      function enableRowSelect(id) {
        $(`.add_promotion_product[rowId=${id}]`).removeClass('disabled')
        .on('click', tableSelectbind);
        disabledSelector.delete(id);
      }

      /**
       * 创建商品搜索表格
       */
      const productSearchTable = dataTable.create({
        dom: '#xquark_select_products_tables',
        lazyInit: true,
        url: productListUrl,
        reqParams: getReqParams(),
        autoFit: false,
        drawBack: function () {
          $('.add_promotion_product').on('click', function () {
            const _this = this;
            if (settedGlobalDiscount) {
              utils.tools.confirm('您已设置了全场优惠, 要取消吗?', function () {
                unSetGlobalDiscount.call($('#setting-global'));
                tableSelectbind.call(_this);
              });
            } else {
              tableSelectbind.call(_this);
            }
          });
        },
        columns: [
          {
            title: '商品图片',
            render: function (data, type, row) {
              return '<a href="' + row.productUrl
                  + '"><img class="goods-image" src="' + row.img + '" /></a>';
            }
          },
          {
            title: '名称',
            data: "name",
            name: "name"
          },
          {
            title: 'U9编码',
            data: "encode",
            name: "encode"
          },
          {
            title: 'U8编码',
            data: "u8Encode",
            name: "u8Encode"
          },
          {
            title: '状态',
            render: function (data, type, row) {
              var status = '';
              switch (row.status) {
                case 'INSTOCK':
                  status = '下架';
                  break;
                case 'ONSALE':
                  status = '在售';
                  break;
                case 'FORSALE':
                  status = '待上架发布';
                  break;
                case 'DRAFT':
                  status = '未发布';
                  break;
                default:
                  break;
              }
              return status;
            },
          },
          {
            title: '价格',
            data: "price",
            name: "price"
          }, {
            title: '库存',
            data: "amount",
            name: "amount"
          }, {
            title: '商品状态',
            width: 100,
            render: function (data, type, row) {
              let html = '';
              const isDisabled = row.isSelect || disabledSelector.has(row.id);
              const classVal = "add_promotion_product" + (isDisabled
                  ? ' disabled'
                  : '');
              html += `<a href="javascript:void(0);" class="${classVal} nowrap"
            rowId="${row.id}"><i class="icon-pointer"></i>添加</a>`;
              return html;
            }
          }
        ]
      });

      const promotionViewTable = dataTable.create({
        dom: '#xquark_promotion_view_tables',
        autoFit: false,
        showSelector: true,
        columns: [
          {
            title: '商品名称',
            data: 'name',
            name: 'name',
            width: 120
          },
          {
            title: '店铺价',
            data: "price",
            name: "price",
            width: 60
          },
          {
            title: '折扣',
            data: "discount",
            name: "discount",
            width: 100
          },
          {
            title: '减价',
            data: "discountFee",
            name: "discountFee",
            width: 100
          },
          {
            title: '促销价',
            data: "afterPrice",
            name: "afterPrice",
            width: 80
          },
          {
            title: '优惠合计',
            data: "totalDiscount",
            name: "totalDiscount",
            width: 80
          },
          {
            title: '操作',
            data: 'operation',
            name: 'operation',
            width: 100
          }
        ]
      });

      /**
       * 修改折扣后重新计算价格
       */
      $(document).on('blur', '.table-discount', function () {
        const rowData = promotionViewTable.rowData($(this).parents('tr'));
        const value = $(this).val();
        const newData = reCalculate(value, rowData, 'DISCOUNT');
        /**
         * 根据名称修改列数据
         */
        promotionViewTable.modifyRow(column => column.name === rowData.name,
            () => newData);
      });

      /**
       * 修改折扣减价后重新计算价格
       */
      $(document).on('blur', '.table-discount-fee', function () {
        const rowData = promotionViewTable.rowData($(this).parents('tr'));
        const value = $(this).val();

        const newData = reCalculate(value, rowData, 'DIRECT');
        /**
         * 根据名称修改列数据
         */
        promotionViewTable.modifyRow(column => column.name === rowData.name,
            () => newData);
      });

      // 事件 ---

      $('.btn-trigger').on('click', function () {
        productSearchTable.update(getReqParams()).refreshAll();
        $productModal.modal('show');
      });

      /**
       * 设置全局折扣
       */
      $('#setting-global').on('click', function () {
        if (settedGlobalDiscount) {
          unSetGlobalDiscount.call(this);
          return;
        }
        $setDiscountModal.modal('show');
      });

      $('input[name=userScope]').on('change', function () {
        const $type = $('input[name=type]');
        if ($(this).val() === 'ALL') {
          $type.val('FULLCUT');
          $('#setting-global').hide();
        } else {
          $type.val('INSIDE_BUY');
          $('#setting-global').show();
        }
      });

      /**
       * 确认设置全场折扣
       */
      $('#setDiscountModalBtn').on('click', function () {
        utils.post(hasActivatePromotionUrl($('input[name=type]').val()),
            ret => {
              if (ret && ret === true) {
                utils.tools.error('您已经配置该类型活动，请先结束其他活动再设置全场活动');
              } else {
                const tableCount = promotionViewTable.rowCount();
                if (tableCount !== 0) {
                  utils.tools.confirm('您选择了特定商品优惠, 确认要设置为全场优惠吗?', () => {
                    modifyGlobalDiscount();
                    disabledSelector.clear();
                    promotionViewTable.removeRows();
                    hideTableContainer();
                  });
                } else {
                  modifyGlobalDiscount();
                }
              }
            });
      });

      /**
       * 确认批量设置折扣
       */
      $('#setBatchDiscountModalBtn').on('click', function () {
        const discount = checkDiscount($('#batchDiscount').val());
        if (!discount) {
          return;
        }
        // 批量修改折扣
        promotionViewTable.modifyRowByNode(rowNode => $(rowNode)
            .find('input[type=checkbox]').is(':checked'),
            rowData => reCalculate(discount, rowData, 'DISCOUNT'));

        // 设置完毕后将选框都取消
        $('input[type=checkbox]').prop('checked', false);
      });

      /**
       * 批量设置折扣
       */
      $('.btn-batch-setting').on('click', function () {
        if (promotionViewTable.checkedRowNum() <= 0) {
          utils.tools.error('请先选择折扣');
          return;
        }
        $setBatchDiscountModal.modal('show');
      });

      $('.btn-batch-remove').on('click', function () {
        utils.tools.confirm('确认撤出选中商品吗?', function () {
          if (promotionViewTable.checkedRowNum() <= 0) {
            utils.tools.error('请先选择折扣');
            return;
          }
          // 撤出所有选中的活动, 并恢复商品表格的选中
          promotionViewTable.removeRows(checkbox => {
            const $checkbox = $(checkbox);
            const isChecked = $checkbox.is(':checked');
            if (isChecked) {
              const id = $checkbox.val();
              enableRowSelect(id);
            }
            return isChecked;
          });
          // 设置完毕后将选框都取消
          $('input[type=checkbox]').prop('checked', false);
        });
      });

      /**
       * 撤出预览商品
       */
      $(document).on('click', '.rowRemove', function () {
        const $this = $(this);
        const rowSelector = $this.parents('tr');
        promotionViewTable.removeRow(rowSelector);
        enableRowSelect($this.attr('rowId'));
      });

      /**
       * 设置全场折扣
       * @param discount
       */
      function setGlobalDiscount(discount) {
        // 设置隐藏域的值
        $('#discount').val(discount);
        $('#setting-global').text(`取消设置全场${discount}折`);
        settedGlobalDiscount = true;
        $('#scope').val('ALL');
        hideDiscountModal();
      }

      /**
       * 修改全局折扣
       */
      const modifyGlobalDiscount = function () {
        const discount = checkDiscount($('#globalDiscount').val());
        if (!discount) {
          return;
        }
        setGlobalDiscount(discount);
      };

      /**
       * 取消设置全店折扣
       */
      function unSetGlobalDiscount() {
        $(this).text('设置全店折扣');
        settedGlobalDiscount = false;
        $('#discount').val('');
        $('#scope').val('PRODUCT');
        hideDiscountModal();
      }

      function hideDiscountModal() {
        $setDiscountModal.modal('hide');
      }

      // 事件 END ---

      // 全局函数 ---

      /**
       * 刷新表格请求参数
       * @returns {{keyword: jQuery, type: string, promotionType: string, scope: jQuery, categoryId: jQuery, status: jQuery}}
       */
      function getReqParams() {
        return {
          promotionId: id,
          keyword: $('#select_products_sKeyword').val(),
          type: 'NORMAL',
          promotionType: $('input[name=type]').val(),
          scope: $('input[name=userScope]:checked').val(),
          categoryId: $('#categoryTypeModal').val(),
          status: $('#pStatus').val()
        };
      }

      /**
       * 刷新商品分类
       */
      function refreshCategory() {
        /**
         * 初始化商品分类
         */
        utils.post(window.host + '/shop/category/list', function (categories) {
          const $categoryType = $('#categoryTypeModal');
          $categoryType.empty();
          $categoryType.append(
              '<option value="" selected="selected">所有分类</option>');
          categories.forEach(function (category) {
            const html = `<option value='${category.id}'>${category.name}</option>`;
            $categoryType.append(html);
          });
        });
      }

      /**
       * 构建展示表格列
       * @param rowData
       * @returns {{name: *, price: *, discount: string, discountFee: string, afterPrice: string, totalDiscount: string, operation: string}}
       */
      function buildViewRow(rowData) {
        return {
          id: rowData.id,
          name: rowData.name,
          price: rowData.price,
          discount: buildDiscountInput(rowData.discount || ''),
          // discountFee: buildDiscountFeeInput(rowData.cutDownPrice || 0),
          discountFee: '¥' + (rowData.cutDownPrice || 0),
          afterPrice: '¥' + (rowData.afterPrice || rowData.price),
          totalDiscount: rowData.cutDownPrice || '¥0',
          operation: `<a class="rowRemove" rowId="${rowData.id}"><i class="icon-trash"></i>撤出</a>`
        };
      }

      /**
       * 将表格数据构建成带输入框的数据
       * @param value
       * @param prefix
       * @param suffix
       * @param cl
       * @returns {string}
       */
      function buildRowInput(value, prefix, suffix, cl) {
        return `<div class="nowrap">${prefix ? prefix
            : ''}<input style="width: 50px" class="${cl}"
      type="text" value="${value ? value : ''}">${suffix ? suffix : ''}</div>`
      }

      function buildDiscountInput(discount) {
        return buildRowInput(discount, '', '折', 'table-discount');
      }

      function buildDiscountFeeInput(discountFee) {
        // 暂时固定减价值不能输入
        return '¥' + discountFee;
        // return buildRowInput(discountFee, '¥', '', 'table-discount-fee');
      }

      /**
       * 根据折扣或减价重新计算表格中的价格
       * @param calValue 折扣或减价 数值
       * @param rowData 表格行数据
       * @param from 折扣或减价 类型
       */
      function reCalculate(calValue, rowData, from) {
        const price = parseFloat(rowData.price);
        let discount = rowData.discount;
        let discountFee = rowData.discountFee;
        if (from === 'DISCOUNT') {
          discount = checkDiscount(calValue);
          if (!discount) {
            utils.tools.error('请设置大于0的优惠值');
            return;
          }
          discountFee = (price - price * discount / 10).toFixed(2);
        }
        if (from === 'DIRECT') {
          discountFee = checkDirectCut(price, calValue);
          if (!discountFee) {
            return;
          }
          discount = (discountFee / price * 10).toFixed(2);
        }
        let afterPrice = price - discountFee;
        rowData.discount = buildDiscountInput(discount);
        rowData.discountFee = buildDiscountFeeInput(discountFee);
        rowData.afterPrice = '¥' + afterPrice.toFixed(2);
        rowData.totalDiscount = '¥' + discountFee;
        return rowData;
      }

      /**
       * 校验表格输入同时转为保留两位小数的数字
       * @param discount
       * @returns {number | *}
       */
      function checkDiscount(discount) {
        if (!discount || discount === '') {
          utils.tools.error('请输入折扣');
          return;
        }
        if (isNaN(discount)) {
          utils.tools.error('请输入数字');
          return;
        }
        discount = parseFloat(discount);
        if (discount > 10) {
          utils.tools.error('请输入正确的折扣');
        }
        return Math.round(discount * 100) / 100;
      }

      /**
       * 校验表格输入同时转为数字
       * @param originalPrice
       * @param directCutPrice
       * @returns {number | *}
       */
      function checkDirectCut(originalPrice, directCutPrice) {
        if (!directCutPrice || directCutPrice === '') {
          utils.tools.error('请输入减价金额');
          return;
        }
        if (isNaN(directCutPrice)) {
          utils.tools.error('请输入数字');
          return;
        }
        directCutPrice = parseFloat(directCutPrice);
        if (directCutPrice > originalPrice) {
          utils.tools.error('减价金额不能大于原价');
        }
        return Math.round(directCutPrice * 100) / 100;
      }

      /**
       * 序列化展示表格获取数据
       * @returns {*}
       */
      function serializePromotionTable() {
        let errorMsg = '';
        const data = promotionViewTable.getDataAll(row => {
          const newRow = {
            productId: row.id,
            discount: $(`<div>${row.discount}</div>`).find('input').val()
          };
          if (!newRow.discount || newRow.discount === '') {
            errorMsg = '请填写折扣';
          }
          newRow.discount = parseFloat(newRow.discount);
          if (newRow.discount > 10 || newRow.discount <= 0) {
            errorMsg = '请输入正确的折扣';
          }
          return newRow;
        });
        if (errorMsg !== '') {
          utils.tools.error(errorMsg);
          return null;
        }
        return data;
      }

      /**
       * 恢复数据
       * @param id
       */
      function resumeData(id) {
        utils.post(viewPromotionUrl(id), data => {
          utils.tools.syncForm($form, data);
          picker.setViewDate(data.validFrom, data.validTo);

          $('input[name=isFreeDelivery]').prop('checked', data.isFreeDelivery);
          const globalDiscount = data.discount;
          if (globalDiscount && globalDiscount !== '' && data.scope === 'ALL') {
            setGlobalDiscount(globalDiscount);
          }
          const products = data.products;
          if (products && products.length > 0) {
            showTableContainer();
            products.forEach(item => {
              // 放到以选择商品set中
              disabledSelector.add(item.productId);
              promotionViewTable.addRow(buildViewRow({
                id: item.productId,
                name: item.productName,
                price: item.productPrice,
                discount: item.discount,
                cutDownPrice: item.cutDownPrice,
                afterPrice: item.afterPrice
              }))
            });
          }

        })
      }

      function showTableContainer() {
        $('.table-container').show('slow');
      }

      function hideTableContainer() {
        $('.table-container').hide('slow');
      }

    });
