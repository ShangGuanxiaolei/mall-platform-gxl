/**
 * Created by chh on 16/12/06.
 */
define(['jquery', 'utils', 'product/upload', 'select2'],
    function ($, utils, uploader, select2) {

    var $category = '';

    var $order = '';

    var $productlistUrl = window.host + "/product/list";

    var $homeItemId = utils.tools.request('pId');  //Id
    $(".btn-submit").on('click', function() {
        save();
    });

    //首页模块信息修改
    if ($homeItemId) {
        $("#homeItemId").val($homeItemId);

        $.ajax({
            url: window.host + '/homeItem/' + $homeItemId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.errorCode == 200) {

                    var homeItem = data.data;
                    //商品图片
                    if (homeItem.img) {
                        var key = homeItem.img,
                            url = homeItem.imgUrl,
                            img = {
                                imgUrl: url,
                                id: key
                            };
                        addProductImage(img);
                    }
                    $("#code").val(homeItem.code);
                    $("#name").val(homeItem.name);
                    $("#url").val(homeItem.url);
                    $("#idx").val(homeItem.idx);
                    $("#belong").val(homeItem.belong);
                    $("#type").val(homeItem.type);
                    $("#description").val(homeItem.description);
                    /** 初始化选择框控件 **/
                    $('.select').select2({
                        minimumResultsForSearch: Infinity,
                    });

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    } else { //发布商品操作

    }

    //添加图片
    function addProductImage(img) {
        if (img) {
            var mockFile = {name: "", size: "", dataImg: img.id};
            uploader[0].dropzone.emit("addedfile", mockFile);
            uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
            uploader[0].dropzone.emit("complete", mockFile);
        }
    }

    //保存
    function save(){
            var imgArr = [];
            $('#product_image_dropzone > .dz-complete').each(function(i, item) {
                imgArr.push($(item).attr('data-img'));
            });

            var pId = $("#homeItemId").val();
            var name = $("#name").val();
            var code = $("#code").val();
            var idx = $("#idx").val();
            var belong = $("#belong").val();
            var url = $("#url").val();
            var type = $("#type").val();
            var description = $("#description").val();

            if(!name || name == '' ){
                utils.tools.alert("请输入显示名称!", {timer: 1200, type: 'warning'});
                return;
            }else if(!code || code == ''){
                utils.tools.alert("请选择模块编码!", {timer: 1200, type: 'warning'});
                return;
            }else if(!idx || idx == ''){
                utils.tools.alert("请输入显示顺序!", {timer: 1200, type: 'warning'});
                return;
            }else if(!belong || belong == ''){
                utils.tools.alert("请选择模块归属!", {timer: 1200, type: 'warning'});
                return;
            }else if(!type || type == ''){
                utils.tools.alert("请选择模块类型!", {timer: 1200, type: 'warning'});
                return;
            }else if(imgArr.length == 0){
                utils.tools.alert("请选择图片!", {timer: 1200, type: 'warning'});
                return;
            }

            if(type == 'H5' && (!url || url == '')){
                utils.tools.alert("请输入url路径!", {timer: 1200, type: 'warning'});
                return;
            }

            var param = {
                id:pId,
                imgs: imgArr.join(','),
                name: name,
                code: code,
                idx: idx,
                belong: belong,
                url: url,
                type: type,
                description: description
            };

            $.ajax({
                url: host + '/homeItem/save',
                type: 'POST',
                data: param,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/homeItem/list';
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        }

});