define(['echartsNoAMD','echartsTheme'], function(echarts, limitless) {

        var fans_columns = echarts.init(document.getElementById('fans_columns'), limitless);
        var fans_columns_options = {

            // Setup grid
            grid: {
                x: 40,
                x2: 47,
                y: 35,
                y2: 25
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow' // 'line' | 'shadow'
                }
            },

            // Add legend
            legend: {
                data: ['新增粉丝', '取消关注', '净增粉丝'],
                textStyle: {
                    fontSize: 16
                }
            },

            // Add toolbox
            toolbox: {
                show: true,
                orient: 'vertical',
                x: 'right',
                y: 'center',
                itemGap: 15,
                showTitle: false,
                feature: {
                    dataView: {
                        show: true,
                        readOnly: false,
                        title: '查看图表数据',
                        lang: ['查看图表数据', '关闭', '更新']
                    },
                    restore: {
                        show: true,
                        title: '刷新'
                    },
                    saveAsImage: {
                        show: true,
                        title: '另存为图片',
                        lang: ['保存']
                    }
                }
            },

            // Enable drag recalculate
            calculable: true,

            // Horizontal axis
            xAxis: [{
                type: 'category',
                data: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天']
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                {
                    name: '新增粉丝',
                    type: 'bar',
                    data: [320, 332, 301, 334, 390, 330, 320],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontSize: 16
                                }
                            }
                        }
                    }
                },
                {
                    name: '取消关注',
                    type: 'bar',
                    stack: 'Advertising',
                    data: [120, 132, 101, 134, 90, 230, 210],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontSize: 16
                                }
                            }
                        }
                    }
                },
                {
                    name: '净增粉丝',
                    type: 'bar',
                    data: [862, 1018, 964, 1026, 1679, 1600, 1570],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontSize: 16
                                }
                            }
                        }
                    },
                    markLine: {
                        itemStyle: {
                            normal: {
                                lineStyle: {
                                    type: 'dashed'
                                }
                            }
                        },
                        data: [
                            [{type: 'min'}, {type: 'max'}]
                        ]
                    }
                }
            ]
        };
        fans_columns.setOption(fans_columns_options);

        var read_columns = echarts.init(document.getElementById('read_columns'), limitless);
        var read_columns_options = {

            // Setup grid
            grid: {
                x: 40,
                x2: 40,
                y: 35,
                y2: 25
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },

            // Add legend
            legend: {
                data: ['图文阅读'],
                textStyle: {
                    fontSize: 16
                }
            },
            // Add toolbox
            toolbox: {
                show: true,
                orient: 'vertical',
                x: 'right',
                y: 'center',
                itemGap: 15,
                showTitle: false,
                feature: {
                    dataView: {
                        show: true,
                        readOnly: false,
                        title: '查看图表数据',
                        lang: ['查看图表数据', '关闭', '更新']
                    },
                    restore: {
                        show: true,
                        title: '刷新'
                    },
                    saveAsImage: {
                        show: true,
                        title: '另存为图片',
                        lang: ['保存']
                    }
                }
            },
            // Enable drag recalculate
            calculable: true,

            // Horizontal axis
            xAxis: [{
                type: 'category',
                data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                {
                    name: '图文阅读',
                    type: 'bar',
                    data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontSize: 16
                                }
                            }
                        }
                    },
                    markLine: {
                        data: [{type: 'average', name: 'Average'}]
                    }
                }
            ]
        };
        read_columns.setOption(read_columns_options);


        // Resize charts
        // ------------------------------
        window.onresize = function () {
            setTimeout(function () {
                fans_columns.resize();
                read_columns.resize();
            }, 200);
        }

});
