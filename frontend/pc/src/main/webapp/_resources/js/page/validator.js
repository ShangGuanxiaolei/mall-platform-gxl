define(['utils'], function (utils) {

  /**
   * 参数校验
   * @param rules 校验规则
   * @param data 参数对象
   * @param messageHandler 处理错误消息的函数
   * @returns {boolean}
   */
  function validateData(rules, data, messageHandler) {
    for (var k in data) {
      if (data.hasOwnProperty(k)) {
        if (rules.hasOwnProperty(k)) {
          var value = data[k];
          // 循环参数校验
          for (var i in rules[k]) {
            if (rules[k].hasOwnProperty(i)) {
              var checkObj = rules[k][i];
              var checker = checkObj.checker;
              if (!checker.call(null, value)) {
                if (!messageHandler) {
                  messageHandler = function (message) {
                    utils.tools.alert(message, { timer: 1200, type: 'warning' });
                  }
                }
                messageHandler(checkObj.message);
                return false;
              }
            }
          }
        }
      }
    }
    return true
  }

  return {
    validate: validateData
  }

});
