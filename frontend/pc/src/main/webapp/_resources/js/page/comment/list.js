/**
 * Created by quguangming on 16/5/24.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'daterangepicker'], function($,utils,datatabels,blockui,select2,daterangepicker) {

    var $listUrl = window.host + "/comment/view/pc";

    var $orders = ['time'];

    //var $shopId = null;

    var $order = '';

    var $commentType = '';

    var $isBlocked = '';

    //replysdatables初始化的时候,如果传递一个空值,则会转向一个错误接口 Comment/{id}/reply ->Comment/reply ,所以这里给定一个默认值
    var $commentId='initID';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 初始化日期控件 **/
    var options = {
        timePicker: true,
        dateLimit: { days: 60000 },
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY-MM-DD h:mm a',
            separator: ' - ',
            applyLabel: '确定',
            startLabel: '开始日期:',
            endLabel: '结束日期:',
            cancelLabel: '取消',
            weekLabel: 'W',
            customRangeLabel: '日期范围',
            daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
            monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
            firstDay: 6
        },
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
    };

    $('.daterange-time').daterangepicker(options);

    /** 回调 **/
    $('.daterange-time').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.startDate = picker.startDate;
        options.endDate   = picker.endDate;
        $("#groupon_valid_from").val(picker.startDate.format('YYYY-MM-DD HH:mm'));
        $("#groupon_valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    // 全选
    $("#checkAllGoods").on('click', function() {
        $("input[name='checkGoods']").prop("checked", $(this).prop("checked"));
    });


    $(".btn-batchDel").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });

        var url = window.host + "/comment/batchDelete/" + ids;
        utils.postAjax(url,{},function(res){
                if(typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("删除成功!", {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                    }
                }
        });

    });

    $(".btn-batchBlock").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });
        //batchBlock(ids);
        var url = window.host + "/comment/batchBlock/" + ids;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    utils.tools.alert("屏蔽成功!", {timer: 1200, type: 'warning'});
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });

    });

    $(".btn-batchUnBlock").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });
        //batchUnblock(ids);

        var url = window.host + "/comment/batchUnBlock/" + ids;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    utils.tools.alert("撤销屏蔽成功!", {timer: 1200, type: 'warning'});
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    });

    // 得到当前选中的行数据
    function getTableContent(){
        var selectRows = new Array();
        for(var i = 0; i < $("input[name='checkGoods']:checked").length; i++){
            var value = {};
            var checkvalue = $("input[name='checkGoods']:checked")[i];
            value.id = $(checkvalue).attr("rowid");
            selectRows.push(value);
        }
        return selectRows;
    }



    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });


    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                // keyword: data.search.value,
                // pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 1){
                            return '';
                        } else {
                            return $orders[_index - 1];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                type : $commentType,
                blocked:$isBlocked,
            }, function(res) {
                if (!res.data) {
                    res.data = [];
                }
                callback({
                   data:res.data.list,
                    recordsTotal: res.data.count,
                    recordsFiltered: res.data.count,
                    data: res.data.list,
                    iTotalRecords:res.data.count,
                    iTotalDisplayRecords:res.data.count
                });


            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'+row.id+'" value="'+row.id+'"></label>';
                }
            },
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.avatar+'" />';
                }
            },
            {
                data: "loginname",
                width: "30px",
                orderable: false,
                name:"loginname"
            },

            {
                data: "objId",
                width: "30px",
                orderable: false,
                name:"objId"
            },

            {
                data: "content",
                width: "120px",
                orderable: false,
                name:"content"
            },
            {
                data: "likeCount",
                width: "30px",
                orderable: false,
                name:"likeCount"
            },
            {
                orderable: true,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },
            {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    if(row.blocked == true){
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="downpopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>撤销屏蔽</a>';
                    }else{
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="uppopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>屏蔽</a>';
                    }
                    html += '<a href="javascript:void(0);" class="replys" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7"></i>查看回复</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_product"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $replysdatatables = $('#xquark_list_replys_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/comment/"+$commentId+"/reply", {
                size: data.length,
                page: (data.start / data.length),
                // keyword: data.search.value,
                pageable: true,
            }, function(res) {
                if (!res.data) {
                    res.data = [];
                }
                callback({
                    recordsTotal: res.data.count,
                    recordsFiltered: res.data.count,
                    data: res.data.list,
                    iTotalRecords:res.data.count,
                    iTotalDisplayRecords:res.data.count
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "loginname",
                width: "120px",
                orderable: false,
                name:"title"
            }, {
                data: "content",
                width: "50px",
                orderable: false,
                name:"price"
            },
            {
                orderable: true,
                width: "100px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    if(row.blocked == true){
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="replyDownpopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>撤销屏蔽</a>';
                    }else{
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="replyUppopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>屏蔽</a>';
                    }
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="replyPopover" rowId="'+row.id+'" fid="delete_product"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initReplyEvent();
        }
    });


    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');



        // 回复管理
        $(".replys").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $commentId = rowId;
            $replysdatatables.search('').draw();
            $("#modal_replys").modal("show");
        });



        /** 点击屏蔽弹出框 **/
        $("[data-toggle='uppopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="uppopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="uppopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                blockComment(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover('hide');
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "uppopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="uppopover"]').popover('hide');
            } else if (target.data("toggle") == "uppopover") {
                target.popover("show");
            }
        });

        /** 点击撤销屏蔽弹出框 **/
        $("[data-toggle='downpopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认撤销屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="downpopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="downpopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                unblockComment(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "downpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="downpopover"]').popover('hide');
            } else if (target.data("toggle") == "downpopover") {
                target.popover("show");
            }
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComment(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("show");
            }
        });


    }

    function initReplyEvent() {

        /** 点击屏蔽弹出框 **/
        $("[data-toggle='replyUppopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyUppopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyUppopover"]').popover('hide');
            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                blockReply(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover('hide');
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyUppopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyUppopover"]').popover('hide');
            } else if (target.data("toggle") == "replyUppopover") {
                target.popover("show");
            }
        });

        /** 点击撤销屏蔽弹出框 **/
        $("[data-toggle='replyDownpopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认撤销屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyDownpopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyDownpopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                unblockReply(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyDownpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyDownpopover"]').popover('hide');
            } else if (target.data("toggle") == "replyDownpopover") {
                target.popover("show");
            }
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='replyPopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyPopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyPopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteReply(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_replys_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyPopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyPopover"]').popover('hide');
            } else if(target.data("toggle") == "replyPopover"){
                target.popover("show");
            }
        });

    }



    /*删除评论*/
    function  deleteComment(cId){
        var url = window.host + "/comment/delete/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }


    /*删除回复*/
    function  deleteReply(rId){
        var url = window.host + "/comment/reply/delete/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }




    /*屏蔽一条评论*/
    function  blockComment(cId){
        var url = window.host + "/comment/block/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*撤销对评论的屏蔽*/
    function  unblockComment(cId){
        var url = window.host + "/comment/unblock/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*屏蔽一条回复*/
    function  blockReply(rId){
        var url = window.host + "/comment/reply/block/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*撤销对回复的屏蔽*/
    function  unblockReply(rId){
        var url = window.host + "/comment/reply/unblock/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }



    $('body').on('change','select[name="commentType"]', function(event) {
        $commentType = $(this).val();
        $listUrl = window.host + "/comment/view/pc";
        $datatables.search('').draw();
    });


    $('body').on('change','select[name="isBlocked"]', function(event) {
        $isBlocked = $(this).val();
        $listUrl = window.host + "/comment/view/pc";
        $datatables.search('').draw();
    });



});