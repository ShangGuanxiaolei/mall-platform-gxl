define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            opens: 'right',
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-left');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });
        $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

        $('.daterange-left').val('');
        $("input[name='startDate']").val('');
        $("input[name='endDate']").val('');
        options.startDate = '';
        options.endDate   = '';

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-left').on('apply.daterangepicker', function(ev, picker) {
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
            $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
            $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        var $datatables = $('#withdrawApplyList').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/withdraw/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#selectStatus").val(),
                    sellerPhone: $("#sellerPhone").val(),
                    sellerName: $("#sellerName").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.withdrawTotal.totalCount,
                        recordsFiltered: res.data.withdrawTotal.totalCount,
                        data: res.data.list,
                        iTotalRecords:res.data.withdrawTotal.totalCount,
                        iTotalDisplayRecords:res.data.withdrawTotal.totalCount
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "15px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<label class="checkbox"><input name="checkOrders" rowid="'+row.id+'" rowstatus="'+row.status+'" rowaccountName="'+row.accountName+'" rowapplyMoney="'+row.applyMoney+'"  type="checkbox" class="styled" value="'+row.id+'"></label>';
                    }
                },
                {
                width: '160px',
                data: 'createdAt',
                name: 'createdAt',
                title: "创建时间",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm');
                }
            },{
                width: '160px',
                data: 'accountName',
                name: 'accountName',
                title: '推客名称'
            },{
                width: '120px',
                title: '联系电话',
                data: 'phone',
                name: 'phone',
            },{
                data: "applyMoney",
                name: "applyMoney",
                width: '120px',
                title: '申请金额',
            },{
                width: '120px',
                title: '支付金额',
                data: "confirmMoney",
                name: "confirmMoney",
            },{
                width: '160px',
                title: '结算时间',
                render: function (data, type, row) {
                    if (row.payAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.payAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm');
                }
            },{
                width: '160px',
                title: '支付方式',
                data: "openingBank",
                name: "openingBank",
            },{
                width: '120px',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if(value =='NEW'){
                        return '<a class="btn btn-primary btn-sm applyBtnPay" style="margin-right: 10px;"' +
                            'apply-money="' + row.applyMoney + '" ' +
                            'account-name="' + row.accountName + '" ' +
                            'opening-bank="' + row.openingBank + '" ' +
                            'withdraw-id="' + row.id + '"> 审核</a>';
                    } else if(value =='SUCCESS') {
                        return "已支付";
                    } else if(value =='PENDING') {
                        return "支付中";
                    } else if(value =='FAILED') {
                        return "支付失败";
                    } else {
                        return "已关闭";
                    }
                }
            }],

            drawCallback: function () {  //数据加载完成
                $("#checkAllGoods").prop("checked",false);
            }
        });

        // 审核
        $(document).on('click', '.applyBtnPay', function() {
            var applyMoney = $(this).attr('apply-money');
            var accountName = $(this).attr('account-name');
            var openingBank = $(this).attr('opening-bank');
            var withdrawId = $(this).attr('withdraw-id');
            var msg = '向用户: ' + accountName + ' 的' + openingBank + '账户打款:' + applyMoney + '元, 请再次核对金额';
            utils.tools.confirm(msg, function(){
                var url = window.host + "/withdraw/withdrawpay?withdrawApplyIds="+withdrawId+"&withdrawconfirmMoneys="+applyMoney;
                utils.postAjax(url,{},function(res){
                    if(typeof(res) === 'object') {
                        if (res.data) {
                            utils.tools.alert("打款成功!", {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("打款失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });

            },function(){

            });
        });

        $(document).on('click', '.backBtn', function() {
            var applyMoney = $(this).attr('apply-money');
            var accountName = $(this).attr('account-name');
            var openingBank = $(this).attr('opening-bank');
            var withdrawId = $(this).attr('withdraw-id');
            var msg = '打回用户: ' + accountName + ' 的提现申请';
            utils.tools.confirm(msg, function(){

            },function(){

            });
        });

        // 批量审核
        $(".btn-audit").on('click', function() {
            var updateds = getTableContent();
            if(updateds.length == 0 ){
                utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
                return ;
            }

            var b = true;
            var confirmStr = '';
            var ids = '', confirmMoneys = '';
            $.each(updateds, function(index, row){
                if(row.status!="NEW"  && row.status != 'FAILED'&& row.status!='PENDING'){
                    utils.tools.alert(row.accountName + '已完成提现，无法继续操作!');
                    b = false;
                    return false;
                }
                confirmStr += '用户：' + row.accountName + ' 金额：' + row.applyMoney + '  ';
                if(ids != '') {
                    ids += ',';
                    confirmMoneys += ',';
                }
                ids += row.id;
                confirmMoneys += row.applyMoney;
            });
            if(!b)
                return ;

            var msg = confirmStr + '  将对以上用户进行打款, 请再次核对金额';
            utils.tools.confirm(msg, function(){
                var url = window.host + "/withdraw/withdrawpay?withdrawApplyIds="+ids+"&withdrawconfirmMoneys="+confirmMoneys;
                utils.postAjax(url,{},function(res){
                    if(typeof(res) === 'object') {
                        if (res.data) {
                            utils.tools.alert("打款成功!", {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("打款失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });

            },function(){

            });
        });

        $(".btn-search").on('click', function() {
            $datatables.search('').draw();
        });

        // 得到当前选中的行数据
        function getTableContent(){
            var selectRows = new Array();
            for(var i = 0; i < $("input[name='checkOrders']:checked").length; i++){
                var value = {};
                var checkvalue = $("input[name='checkOrders']:checked")[i];
                value.id = $(checkvalue).attr("rowid");
                value.applyMoney = $(checkvalue).attr("rowapplyMoney");
                value.status = $(checkvalue).attr("rowstatus");
                value.accountName = $(checkvalue).attr("rowaccountName");
                selectRows.push(value);
            }
            return selectRows;
        }

        // 全选
        $("#checkAllGoods").on('click', function() {
            $("input[name='checkOrders']").prop("checked", $(this).prop("checked"));
        });

        $(".exportWithdrawBtn").on('click', function() {
            $('#exportWithdrawForm').submit();
        });
});