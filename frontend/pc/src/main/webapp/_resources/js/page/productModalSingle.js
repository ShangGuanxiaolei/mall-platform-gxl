define(['jquery', 'datatables'], function ($) {

  var $orders = ['price', 'amount', 'sales', 'onsale'];
  var $order = '';
  var $productListUrl = window.host + "/product/list";

  var $shopId = null;
  var $category = '';
  var $pDataTable;

  const $modal = $('#choose-product-modal');

  //初始化商品分类信息
  $.ajax({
    url: window.host + '/shop/category/list',
    type: 'POST',
    dataType: 'json',
    data: {},
    success: function (data) {
      if (data.errorCode === 200) {
        var dataLength = data.data.length;
        for (var i = 0; i < dataLength; i++) {
          $("#categoryType").append('<option value=' + data.data[i].id + '>'
              + data.data[i].name + '</option>');
        }
        ;
      } else {
        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
      }
    },
    error: function (state) {
      if (state.status === 401) {
      } else {
        utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
      }
    }
  });

  /**
   * 标签搜索
   */
  $('body').on('change', 'select[name="categoryType"]', function () {
    $productListUrl = window.host + "/product/list";
    $category = $(this).val();
    $pDataTable.search('').draw();
  });

  /**
   * 关键字搜索
   */
  $(".btn-search-products").on('click', function () {
    var keyword = $.trim($("#select_products_sKeyword").val());
    if (keyword !== '' && keyword.length > 0 && shopId !== null) {
      $productListUrl = window.host + '/product/searchbyPc/' + shopId + '/'
          + keyword;
      $pDataTable.search(keyword).draw();
    } else if (keyword === '' || keyword.length === 0) {
      $productListUrl = window.host + "/product/list";
      $pDataTable.search('').draw();
    }

  });

  function initTable(params) {
    $pDataTable = $('#xquark_select_products_tables').DataTable({
      paging: true, //是否分页
      filter: false, //是否显示过滤
      lengthChange: false,
      processing: true,
      serverSide: true,
      deferRender: true,
      searching: true,
      ajax: function (data, callback, settings) {
        $.get($productListUrl, {
          size: data.length,
          page: (data.start / data.length),
          keyword: data.search.value,
          pageable: true,
          order: function () {
            if ($order !== '') {
              return $order;
            } else {
              var _index = data.order[0].column;
              if (_index < 4) {
                return '';
              } else {
                return $orders[_index - 4];
              }
            }
          },
          direction: data.order ? data.order[0].dir : 'asc',
          category: $category,
          isGroupon: ''
        }, function (res) {
          if (!res.data.list) {
            res.data.list = [];
          } else {
            if (res.data.shopId) {
              $shopId = res.data.shopId;
            }
          }
          callback({
            recordsTotal: res.data.categoryTotal,
            recordsFiltered: res.data.categoryTotal,
            data: res.data.list,
            iTotalRecords: res.data.categoryTotal,
            iTotalDisplayRecords: res.data.categoryTotal
          });
        });
      },
      rowId: "id",
      columns: [
        {
          width: "30px",
          orderable: false,
          render: function (data, type, row) {
            return '<a href="' + row.productUrl
                + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
          }
        },
        {
          data: "name",
          width: "120px",
          orderable: false,
          name: "name"
        },
        {
          data: "code",
          width: "40px",
          orderable: false,
          name: "code"
        },
        {
          width: "40px",
          orderable: false,
          render: function (data, type, row) {
            var status = '';
            switch (row.status) {
              case 'INSTOCK':
                status = '下架';
                break;
              case 'ONSALE':
                status = '在售';
                break;
              case 'FORSALE':
                status = '待上架发布';
                break;
              case 'DRAFT':
                status = '未发布';
                break;
              default:
                break;
            }
            return status;
          },
        }, {
          data: "price",
          width: "50px",
          orderable: true,
          name: "price"
        }, {
          data: "amount",
          orderable: true,
          width: "50px",
          name: "amount"
        },
        {
          data: "sales",
          orderable: true,
          width: "50px",
          name: "sales"
        }, {
          orderable: false,
          width: "180px",
          render: function (data, type, row) {
            if(row.createAt == null){
              return "";
            }
            var cDate = parseInt(row.createAt);
            var d = new Date(cDate);
            return d.format('yyyy-MM-dd hh:mm:ss');
          },
          name: "onsaleAt"
        }, {
          sClass: "right",
          width: "120px",
          orderable: false,
          render: function (data, type, row) {
            var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                + row.id + '" productName="' + row.name + '"productPrice="' + row.price
                + '" ><i class="icon-pencil7" ></i>选择</a>';
            return html;
          }
        }
      ],
      drawCallback: function () {  //数据加载完成
        initSelectProductEvent(params);
      }
    });
  }

  function initSelectProductEvent(params) {
    $(".selectproduct").on("click", function () {
      var id = $(this).attr("rowId");
      var name = $(this).attr("productName");
      var price = $(this).attr("productName");
      //回调外部函数
      if (params && params.onSelect) {
        params.onSelect(id, name, price);
      }
      $modal.modal("hide");
    });
  }

  return {
    /**
     * 创建商品表格
     * @param params
     */
    create: function (params) {
      initTable(params);

      /* 选择活动商品 */
      $(params.dom).on('focus', function () {
        $modal.modal("show");
      });
    },
    refresh: function () {
      $pDataTable.search('').draw();
    }
  }
});