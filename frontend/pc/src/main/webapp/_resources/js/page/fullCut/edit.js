define(['jquery', 'utils', 'moment', 'daterangepicker', 'jquerySerializeObject', 'blockui', 'select2', 'datatables'], function ($, utils, moment) {

  // TODO wangxinhua 跨级删除时level显示不正确

  const prefix = window.host + '/promotionFullCut';
  const listGiftProducts = window.host + '/product/listGift';
  const saveUrl = prefix + '/savePromotion';
  const preSaveUrl = prefix + '/preSave';

  const listProductsUrl = prefix + '/listProduct';
  const deleteProductUrl = prefix + '/deleteProduct';
  const addProductUrl = prefix + '/addProduct';
  const delDiscountUrl = prefix + '/deleteDiscount';

  const $addProduct = $('.btn-addProduct');
  const $productModal = $('#modal_result_products');
  const $chosenProductModal = $('#modal_products');
  const $chooseModalClose = $('.close_product_choose');
  const $productTitle = $('#product-title');

  let listProducts = window.host + "/product/list";
  var $productDataTable;
  var $selectProductDataTables;
  var $currentGiftDiv = null;
  var preSaved = false;

  var category = '';
  var order = '';
  var gift = false;
  var promotionId = $('#promotionId').val();
  const commaAppender = createStrAppender(',');

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "暂无相关数据",
      emptyTable: "暂无相关数据"
    }
  });

  /* 参数校验 */
  const validate = (function () {
    const obj = {
      title: function (title) {
        if (!method.nonCheck(title, '请输入活动名称')) {
          return false;
        }
      },
      validFrom: function (validFrom) {
        if (!method.nonCheck(validFrom, '请选择日期')) {
          return false;
        }
      },
      validTo: function (validTo) {
        if (!method.nonCheck(validTo, '请选择日期')) {
          return false;
        }
      },
      discounts: function (discounts) {
        // checkBox 没选默认设置为false
        for (var i in discounts) {
          if (discounts.hasOwnProperty(i)) {
            if (!method.nonCheck(discounts[i].minConsume, '请输入优惠门槛')) {
              return false;
            }
            if (!method.nonCheck(discounts[i].discountType) && !method.nonCheck(
                    discounts[i].gift) && !method.nonCheck(
                    discounts[i].freeDelivery)) {
              utils.tools.alert('请选择优惠方式', {timer: 1200, type: 'warning'});
              return false;
            }
          }
        }
      }
    };
    const method = {
      nonCheck: function (value, message) {
        if (!value || (typeof value) === 'undefined' || value === '') {
          if (message) {
            utils.tools.alert(message, {timer: 1200, type: 'warning'});
          }
          return false;
        }
        return true;
      }
    };
    return {
      do: function (data) {
        for (var key in obj) {
          if (obj.hasOwnProperty(key) && data.hasOwnProperty(key)) {
            if (obj[key](data[key]) === false) {
              return false;
            }
          }
        }
        return true;
      }
    }
  })();

  const options = {
    timePicker: true,
    dateLimit: {days: 60000},
    startDate: moment().subtract(0, 'month').startOf('month'),
    endDate: moment().subtract(0, 'month').endOf('month'),
    autoApply: false,
    locale: {
      format: 'YYYY/MM/DD',
      separator: ' - ',
      applyLabel: '确定',
      fromLabel: '开始日期:',
      toLabel: '结束日期:',
      cancelLabel: '清空',
      weekLabel: 'W',
      customRangeLabel: '日期范围',
      daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月",
        "11月", "12月"],
      firstDay: 6
    },
    ranges: {
      '今天': [moment(), moment().endOf('day')],
      '一星期': [moment(), moment().add(6, 'days')],
      '一个月': [moment(), moment().add(1, 'months')],
      '一年': [moment(), moment().add(1, 'year')]
    },
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small btn-default'
  };

  const manager = (function () {
    const $dateRangePicker = $('.daterange-time');
    const $addLevel = $('#addLevel');
    const $submitBtn = $('.btn-submit');

    const $scopeInputs = $('input[name=scope]');

    return {
      initGlobal: function () {
        return this;
      },
      initDateRangePricker: function () {
        $dateRangePicker.daterangepicker(options, function (start, end) {
          if (start._isValid && end._isValid) {
            $dateRangePicker.val(start.format('YYYY-MM-DD HH:mm') + ' - '
                + end.format('YYYY-MM-DD HH:mm'));
          } else {
            $dateRangePicker.val('');
          }
        });
        return this;
      },
      bindEvent: function () {

        $addProduct.on('click', function () {
          $productModal.modal('show');
          $chosenProductModal.modal('hide');
        });

        $chooseModalClose.on('click', function () {
          $productModal.modal('hide');
          if (!gift) {
            $chosenProductModal.modal('show');
          }
        });

        $dateRangePicker.on('apply.daterangepicker', function (ev, picker) {
          options.startDate = picker.startDate;
          options.endDate = picker.endDate;
          $("#valid_from").val(picker.startDate.format('YYYY-MM-DD HH:mm'));
          $("#valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangePicker.on('cancel.daterangepicker', function () {
          //do something, like clearing an input
          $dateRangePicker.val('');
          $("#valid_from").val('');
          $("#valid_to").val('');
        });

        $scopeInputs.on('click', function () {
          var value = $scopeInputs.filter(':checked').val();
          scopeCallBack(value)();
        });

        /* 点击赠品checkbox */
        $(document).on('click', '.toggleGift', function () {
          $currentGiftDiv = $(this).parent();
          if ($(this).is(':checked')) {
            gift = true;
            $selectProductDataTables.search('').draw();
            $productTitle.text('选择赠品');
            $productModal.modal('show');
          } else {
            $(this).siblings('input[type=hidden]').val('');
            $(this).siblings('span[class=productNames]').text('');
          }
        });

        $(document).on('click', '.delDiscount', function () {
          var discountId = $(this).attr('id');
          var self = this;
          if (discountId) {
            utils.postAjax(delDiscountUrl,
                {promotionId: promotionId, id: discountId}, function (res) {
                  if (res === -1) {
                    utils.tools.alert("网络问题，请稍后再试",
                        {timer: 1200, type: 'warning'});
                  }
                  if (typeof res === 'object') {
                    if (res.errorCode === 200) {
                      utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                      $(self).closest('tr').remove();
                    } else {
                      if (res.moreInfo) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                      } else {
                        utils.tools.alert('服务器错误',
                            {timer: 1200, type: 'warning'});
                      }
                    }
                  }
                });
          } else {
            $(this).closest('tr').remove();
          }
        });

        /* 将模板中的 $index$ 替换为数组相应的索引，并将模板插入到table中 */
        $addLevel.on('click', function () {
          // 最大层级为5级
          var currLevel = getMaxLevel();
          if (currLevel >= 5) {
            return;
          }
          var html = $('#table-template').html()
          .replace(/\$index\$/g, currLevel)
          .replace(/\$level\$/g, currLevel + 1);
          $('tbody').append(html);
        });

        $submitBtn.on('click', function (e) {
          e.preventDefault();
          var data = $('#form').serializeObject();
          if (validate.do(data) === false) {
            return;
          }
          data.validFrom = new Date(data.validFrom);
          data.validTo = new Date(data.validTo);
          $.ajax({
            url: saveUrl,
            type: 'POST',
            async: false,
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (res) {
              if (res.errorCode === 200 && res.data) {
                promotionId = res.data;
                $('#promotionId').val(promotionId);
                utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                location.href = '/sellerpc/mall/fullCutList';
              } else {
                utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
              }
            },
            error: function (res) {
              utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
            }
          });
        });

        $(".btn-search-products").on('click', function () {
          var keyword = $.trim($("#select_products_sKeyword").val());
          if (keyword != '' && keyword.length > 0) {
            listProducts = window.host + '/product/searchbyPc/' + shopId + '/'
                + keyword;
            $selectProductDataTables.search(keyword).draw();
          } else if (keyword == '' || keyword.length == 0) {
            listProducts = window.host + "/product/list";
            $selectProductDataTables.search('').draw();
          }
        });

        return this;
      },
      initProductDataTable: function () {
        if (!$productDataTable) {
          $productDataTable = $('#xquark_list_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function (data, callback, settings) {
              $.get(listProductsUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                id: promotionId,
                gift: gift
              }, function (res) {
                if (!res.data && !res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              });
            },
            rowId: "id",
            columns: [
              {
                title: '商品',
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
              }, {
                title: '状态',
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                  var status = '';
                  switch (row.status) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '在售';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '未发布';
                      break;
                    default:
                      break;
                  }
                  return status;
                },
              }, {
                title: '价格',
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
              }, {
                title: '库存',
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
              },
              {
                title: '销量',
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
              }, {
                title: '发布时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.onsaleAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
              }, {
                title: '管理',
                orderable: false,
                width: '50px',
                render: function (data, type, row) {
                  var html = '';
                  html += '<a href="javascript:void(0);" class="del" rowId="'
                      + row.id
                      + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              $('.del').on('click', function () {
                var productId = $(this).attr('rowId');
                var data = {
                  productId: productId,
                  promotionId: promotionId
                };
                utils.tools.confirm('确认删除吗', function () {
                  utils.postAjax(deleteProductUrl, data, function (res) {
                    if (res === -1) {
                      utils.tools.alert("网络问题，请稍后再试",
                          {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                      if (res.errorCode === 200) {
                        utils.tools.alert('删除成功',
                            {timer: 1200, type: 'success'});
                        $productDataTable.search('').draw();
                      } else {
                        if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo,
                              {timer: 1200, type: 'warning'});
                        } else {
                          utils.tools.alert('服务器错误',
                              {timer: 1200, type: 'warning'});
                        }
                      }
                    }
                  });
                }, function () {

                });
              });
            }
          });
        }
        return this;
      }, initSelectProductDataTables: function () {
        if (!$selectProductDataTables) {
          $selectProductDataTables = $(
              '#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
              $.get(getProductUrl(), {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                  if (order !== '') {
                    return order;
                  } else {
                    var _index = data.order[0].column;
                    if (_index < 4) {
                      return '';
                    } else {
                      return $orders[_index - 4];
                    }
                  }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else {
                  if (res.data.shopId) {
                    shopId = res.data.shopId;
                  }
                }
                callback({
                  recordsTotal: res.data.categoryTotal,
                  recordsFiltered: res.data.categoryTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.categoryTotal,
                  iTotalDisplayRecords: res.data.categoryTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="' + row.productUrl
                      + '"><img class="goods-image" src="' + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
              }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                  var status = '';
                  switch (row.status) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '在售';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '未发布';
                      break;
                    default:
                      break;
                  }
                  return status;
                },
              }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
              }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
              },
              {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
              }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.onsaleAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
              }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                      + row.id + '" productImg="' + row.imgUrl
                      + '" productPrice="' + row.price + '" productName="'
                      + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                  return html;
                }
              }
            ],
            drawCallback: function () {  //数据加载完成
              $(".selectproduct").on("click", function () {
                var productId = $(this).attr('rowId');
                var productName = $(this).attr('productname');
                // 选择赠品则暂存赠品id
                if (gift) {
                  // 赠品id
                  var $currGiftIdInput = $currentGiftDiv.find(
                      'input[type=hidden]');
                  // 赠品名称
                  var $currGiftNameSpan = $currentGiftDiv.find(
                      'span[class=productNames]');

                  var currGiftIds = $currGiftIdInput.val();
                  var currGiftNames = $currGiftNameSpan.text();
                  if (currGiftIds.indexOf(productId) !== -1) {
                    utils.tools.alert('该商品已设置为赠品',
                        {timer: 1200, type: 'warning'});
                    return;
                  }
                  $currGiftIdInput.val(commaAppender(currGiftIds, productId));
                  $currGiftNameSpan.text(
                      commaAppender(currGiftNames, productName));
                  utils.tools.alert('设置成功', {timer: 1200, type: 'success'});
                } else {
                  utils.postAjax(addProductUrl, {
                    promotionId: promotionId,
                    productId: productId
                  }, function (res) {
                    if (res === -1) {
                      utils.tools.alert("网络问题，请稍后再试",
                          {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                      if (res.errorCode === 200) {
                        $productDataTable.search('').draw();
                        utils.tools.alert('添加成功',
                            {timer: 1200, type: 'success'});
                      } else {
                        if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo,
                              {timer: 1200, type: 'warning'});
                        } else {
                          utils.tools.alert('服务器错误',
                              {timer: 1200, type: 'warning'});
                        }
                      }
                    }
                  });
                }
              });
            }
          });
        }
        return this;
      }
    }
  })();

  manager.initGlobal() // 全局初始化
  .initDateRangePricker() // 时间控件初始化
  .bindEvent() // 全局时间绑定
  .initProductDataTable() // 初始化以选择商品表格
  .initSelectProductDataTables(); // 初始化全部商品表格

  function scopeCallBack(scope) {
    var scopeMapper = {
      'PRODUCT': function () {
        gift = false;
        preSaveForm(function () {
          if ($productDataTable) {
            $productDataTable.search('').draw();
            $selectProductDataTables.search('').draw();
          }
          $productTitle.text('选择商品');
          $chosenProductModal.modal('show');
        });
      },
      'ALL': function () {
        $chosenProductModal.modal('hide');
      }
    };
    return scopeMapper[scope];
  }

  /**
   * 获取当前的最大层级
   */
  function getMaxLevel() {
    var maxLevel = 0;
    $('#form').find('.level').each(function () {
      var level = parseInt($(this).text());
      if (level > maxLevel) {
        maxLevel = level;
      }
    });
    return maxLevel;
  }

  function preSaveForm(callback) {
    var data = $('#form').serializeObject();
    data.validFrom = new Date(data.validFrom);
    data.validTo = new Date(data.validTo);
    delete data['discounts'];
    $.ajax({
      url: preSaveUrl,
      type: 'POST',
      async: false,
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(data),
      success: function (res) {
        if (res.errorCode === 200 && res.data) {
          var promotion = res.data;
          promotionId = promotion.id;
          $('#promotionId').val(promotionId);
          // utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
          console.log(promotionId);
          preSaved = true;
          if (callback) {
            callback();
          }
        } else {
          utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
        }
      },
      error: function (res) {
        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
      }
    });
  }

  function getProductUrl() {
    if (gift) {
      return listGiftProducts;
    } else {
      return listProducts;
    }
  }

  function createStrAppender(split) {
    return function (str1, str2) {
      return appendStr(str1, str2, split);
    }
  }

  function appendStr(str1, str2, split) {
    return str1 === '' ? str1 + str2 : str1 + (split ? split : '') + str2;
  }

});
