define(['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';
    const listUrl = prefix + '/answer/listByGroup';
    const listGroup = prefix + '/answer/listGroup';
    const saveUrl = prefix + '/question/save';

    const $dataTable = $('#xquark_answer_tables');
    const $answerModal = $('#modal_answer');
    const $addQuestionTable = $('#added_questions');

    var groupId = getParameterByName("groupId");

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "未配置答案"
        }
    });

    const manager = (function () {
        const $addAnswerBtn = $('.btnAddAnswer');
        const $returnBtn = $('.btn-return');
        const $form = $('.question-form');
        const $groupSelector = $('#answer_group');
        const saveAnswerBtn = $('#save_btn_answer');

        const globalInstance = {
            bindEvent: function () {
                $addAnswerBtn.on('click', function () {
                    $answerModal.modal('show');
                });
                $groupSelector.on('change', function () {
                    groupId = $(this).val();
                    $anserDataTables.search('').draw();
                });
                $returnBtn.on('click', function () {
                    utils.tools.confirm('确认返回？当前修改将不会被保存', function () {
                        location.href = '/sellerpc/healthTest/question';
                    }, function () {

                    });
                });
                saveAnswerBtn.on('click', function () {
                    var answerContent = $('#answer_content').val();
                    var data = {
                        content: answerContent,
                        groupId: groupId
                    };
                    utils.postAjax(prefix + '/answer/save', data, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                if (res.data) {
                                    groupId = res.data;
                                    var option = '<option value="' + groupId + '" >新建</option>';
                                    var $select = $('select[name=groupId]');
                                    $select.append($(option));
                                    $select.val(groupId);
                                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                                    $anserDataTables.search('').draw();
                                } else {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                }
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
                return this;
            },
            initGroup: function () {
                utils.postAjaxWithBlock($(document), listGroup, null, function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if (res.data) {
                                    var list = res.data.list;
                                    if (list && list.length > 0) {
                                        list.forEach(function (item) {
                                            var option = '<option value="' + item.id + '"> ' + item.name + ' </option>';
                                            $groupSelector.append($(option));
                                        });
                                        $groupSelector.val(groupId);
                                    }
                                }
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res === 0) {

                    } else if (res === -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
                return this;
            },
            initValidate: function () {
                validate($form, formProfile);
                return this;
            }
        };
        return {
            initGlobal: function () {
                // 初始化事件绑定、答案组、表单校验
                globalInstance.bindEvent()
                    .initGroup()
                    .initValidate();
            },
            initTable: function () {
                // $('.edit_question').on('click', function () {
                //     var id = $(this).attr('rowId');
                //     window.location = '/sellerpc/healthTest/question/edit?id=' + id;
                // });
            },
            submitForm: function submitForm(form) {
                var data = $(form).serializeObject();
                if (!data.type || data.type === '') {
                    utils.tools.alert("请选择问题类型", {timer: 1200, type: 'warning'});
                    return;
                }
                console.log(data);
                utils.getJson(saveUrl, data, function (res) {
                    if (res.errorCode === 200) {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            // 修改问题后跳转
                            if (data.id && data.id !== '') {
                                window.setTimeout(function () {
                                    location.href = '/sellerpc/healthTest/question';
                                }, 4);
                            } else {
                                // 添加问题后重置
                                $form[0].reset();
                                groupId = '';
                                $anserDataTables.search('').draw();
                                var newQuestion = '<tr><td>' + data.name + '</td></tr>';
                                $addQuestionTable.append($(newQuestion));
                            }
                        } else {
                            utils.tools.alert("保存失败", {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                    }
                });
                // if (!data.bgColor) data.bgColor = '#008000';
                // if (!data.freeDelivery) data.freeDelivery = false;
                // utils.getJson(saveUrl, data, function (res) {
                //     if (res.errorCode === 200) {
                //         if (res.data) {
                //             utils.tools.alert("保存成功", {timer: 1200});
                //             window.setTimeout(window.location.reload, 2)
                //         } else {
                //             utils.tools.alert("保存失败", {timer: 1200, type: 'warning'});
                //         }
                //     } else {
                //         utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                //     }
                // });
            }
        }
    })();

    const formProfile = {
        rules: {
            name: 'required',
            type: 'required'
        },
        messages: {
            name: {
                required: '请输入问题名称'
            },
            type: {
                required: '请选择问题类型'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            manager.submitForm(form);
        }
    };

    manager.initGlobal();

    /** 初始化表格数据 **/
    const $anserDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                groupId: groupId
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo);
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    var option = row.option;
                    return option ? option : '无';
                }
            },
            {
                width: '200px',
                orderable: false,
                render: function (data, type, row) {
                    var content = row.content;
                    return content ? content : '无';
                }
            },
            {
                width: '50',
                orderable: false,
                render: function (data, type, row) {
                    var score = row.score;
                    return score ? score + '分' : '默认';
                }
            },
            {
                width: '100',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            }
            // {
            //     sClass: "right",
            //     width: "100px",
            //     orderable: false,
            //     render: function (data, type, row) {
            //         var html = '';
            //         html += '<a href="javascript:void(0);" class="edit_answer" rowId="' + row.id + '" fid="edit_answer"><i class="icon-pencil7"></i>编辑</a>';
            //         html += '<a href="javascript:void(0);" class="del_answer" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_answer"><i class="icon-trash"></i>删除</a>';
            //         return html;
            //     }
            // }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});
