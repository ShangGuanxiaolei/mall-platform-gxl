define(['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui',
  'select2'], function ($, utils) {

  const prefix = window.host + '/tags';
  const listUrl = prefix + '/list';

  var $dataTable;

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const manager = (function () {

    const globalInstance = {};

    return {
      initGlobal: function () {
        return this;
      },
      initTable: function () {
        if (!$dataTable) {
          $dataTable = $('#xquark_product_tags')
          .DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
              $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
              }, function (res) {
                if (!res.data) {
                  utils.tools.alert(res.moreInfo,
                      {timer: 1200, type: 'warning'});
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              })
            },
            rowId: 'id',
            columns: [
              {
                title: '标签名',
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
              },
              {
                title: '标签类型',
                data: "categoryStr",
                width: "120px",
                orderable: false,
                name: "categoryStr"
              },
              {
                title: '创建时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "createdAt"
              },
              {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  html += '<a href="javascript:void(0);" class="remove" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>删除</a>';
                  html += '<a href="javascript:void(0);" class="list" style="margin-left: 10px;" rowId="'
                      + row.id + '" ><i class="icon-pencil7" ></i>查看关联商品</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
            }
          });
        }
        return this;
      }
    }
  })();

  // 初始化页面数据
  manager.initGlobal()
  .initTable();

});
