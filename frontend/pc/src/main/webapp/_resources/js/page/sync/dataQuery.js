/**
 * Created by jitre on 7/4/18.
 */
define(['jquery', 'utils', 'ramda'], function ($, utils, R) {

  const SYNC_AMOUNT_URL = window.originalHost + "/third/supplier/sku/import";
  const SYNC_ROUTER_URL = window.originalHost
      + "/third/supplier/logistics/import";
  const SYNC_POINT_URL = window.host + "/point/balance/import";
  const SYNC_PRODUCT_URL = window.host + "/mall/import/product";
  const SYNC_WITHDRAW_URL = window.host + "/point/balance/import/withdraw";

  const urlCache = new Map();
  urlCache.set("router", SYNC_ROUTER_URL);
  urlCache.set("amount", SYNC_AMOUNT_URL);
  urlCache.set('point', SYNC_POINT_URL);
  urlCache.set('withdraw', SYNC_WITHDRAW_URL);
  urlCache.set('product',SYNC_PRODUCT_URL);

  function defaultDone(data) {
    $.unblockUI();
    if (data.errorCode === 200 || data.status === 200 || data.data) {
      if(data.moreInfo && data.moreInfo !== ''){
        utils.tools.alert(data.moreInfo,{type: 'success'});
      }else {
        utils.tools.alert('导入成功', {type: 'success'});
      }
    } else {
      if(data.moreInfo && data.moreInfo !== ''){
        utils.tools.alert(data.moreInfo);
      }else {
        utils.tools.alert(data.error);
      }
    }
  }

  function defaultFail() {
    $.unblockUI();
    utils.tools.error("同步数据出错!");
  }

  return {
    sync: function (file, type, jobId, month, platform) {
      utils.blockPage();
      $.get(urlCache.get(type), {
        url: file,
        jobId: jobId,
        month: month,
        platform: platform,
        usedType: $('#usedType option:selected').val()//选中的值
        // 如果silence则不处理
      }).done(defaultDone)
      .fail(defaultFail);
    }
  }

});