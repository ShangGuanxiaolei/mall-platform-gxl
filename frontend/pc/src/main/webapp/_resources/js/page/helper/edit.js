define(['jquery', 'utils', 'fileUploader', 'jquerySerializeObject', 'ckEditor'],
    function ($, utils, fileUploader) {

      const prefix = window.host + '/helper';
      const saveUrl = prefix + '/save';
      const viewUrl = prefix + '/view';

      const id = utils.tools.request('id');

      var uploader;

      const manager = (function () {

        const $iconInput = $('#icon');
        const $submitBtn = $('.btn-submit');
        const $form = $('#helper-form');

        const globalInstance = {
          syncData: function () {
            if (id && id !== '') {
              utils.postAjaxWithBlock($(document), viewUrl, { id : id }, function(res) {
                  if (typeof(res) === 'object') {
                      switch (res.errorCode) {
                          case 200:
                          {
                              const data = res.data;
                              utils.tools.syncForm($form, data);
                              const icon = data.icon;
                              uploader.addImage(icon);
                              break;
                          }
                          default:
                          {
                              utils.tools.alert(res.moreInfo, {timer: 1200});
                              break;
                          }
                      }
                  } else if (res === 0) {

                  } else if (res === -1) {
                      utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                  }
              });
            }
            return this;
          },
          bindEvent: function () {
            /* 表单提交 */
            $submitBtn.on('click', function (e) {
              e.preventDefault();
              const data = $form.serializeObject();
              console.log(data);
              utils.postAjax(saveUrl, data, function (res) {
                  if (res === -1) {
                      utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                  }
                  if (typeof res === 'object') {
                      if (res.errorCode === 200) {
                          utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                      } else {
                          if (res.moreInfo) {
                              utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                          } else {
                              utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                          }
                      }
                  }
              });
            });
            return this;
          },
          createUploader: function () {
            uploader = fileUploader.createUploader({
              dom: '#icon-dropzone',
              onSuccess: function (img) {
                $iconInput.val(img);
              }
            });
            return this;
          },
          initEditor: function () {
            $("#content").ckeditor();
            return this;
          }
        };

        return {
          initGlobal: function () {
            globalInstance
            .bindEvent()
            .createUploader()
            .syncData()
            .initEditor();
            return this;
          }
        }
      })();

      manager.initGlobal();
    });
