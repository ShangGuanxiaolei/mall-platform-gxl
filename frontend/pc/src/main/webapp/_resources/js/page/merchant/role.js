/**
 * Created by chh on 16/12/29.
 */
define(['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2', 'multiselect'], function ($, utils, datatabels, blockui, bootbox, select2, multiselect) {

    var $listUrl = window.host + "/merchantRole/list";

    var $menuSelect = jQuery('#menu-select');

    // 菜单编辑被点击时初始化,记录当前角色
    var $roleId = null;

    var $shopId = null;

    var $keyword = '';

    var $roleModules = function () {
        var newModules = {};
        var selected = {};
        return {
            bind: function (rId, mId) {
                newModules[mId] = rId;
            },
            addSelected: function (mId, isSelected) {
                selected[mId] = isSelected;
            },
            removeSelected: function (mid) {
                if (selected[mid]) selected[mid] = false;
            },
            unBind: function (rId, mId) {
                if (newModules[mId]) delete newModules[mId];
            },
            getBindPostData: function () {
                var postData = [];
                for (var key in newModules) {
                    postData.push({
                        'roleId': newModules[key],
                        'moduleId': key
                    })
                }
                return postData;
            },
            getUnBindPostData: function () {
                var postData = [];
                for (var key in selected) {
                    if (!selected[key]) postData.push(key);
                }
                return postData.join(",");
            },
            clear: function () {
                newModules = {};
                selected = {};
            }
        }
    }();

    /* 初始化菜单数据 */
    initModules();

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function () {
        $("#id").val('');
        $("#name").val('');
        $("#desc").val('');
        $("#modal_component").modal("show");
    });

    buttonRoleCheck('.hideClass');

  var $datatables = utils.createDataTable('#xquark_list_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                merchantId: '',
                pageable: true
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else {
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            });
        },
        rowId: "id",
        columns: [
            {
                data: "roleName",
                orderable: false,
                name: "roleName"
            }, {
                data: "roleDesc",
                orderable: false,
                name: "roleDesc"
            }
            , {
                orderable: false,
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "createdAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_menu role_check_table" rowId="' + row.id + '" fid="edit_modules"><i class="icon-pencil7" ></i>配置菜单</a>';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="' + row.id + '" fid="edit_role"><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_role"><i class="icon-trash" ></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initModules() {

        // 初始化菜单选框
        $menuSelect.multiSelect({
            selectableOptgroup: true,
            keepOrder: true,
            selectableOptgroup: true,
            afterSelect: function (values) {
                if (values) {
                    values.forEach(function (mId) {
                        $roleModules.bind($roleId, mId);
                    });
                }
            },
            afterDeselect: function (values) {
                if (values) {
                    values.forEach(function (mId) {
                        $roleModules.unBind($roleId, mId);
                        $roleModules.removeSelected(mId);
                    })
                }
            },
            afterInit: function () {
            }
        });


    }

    function initEvent() {
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $('.edit_menu').on('click', function () {
            $roleModules.clear();
            $roleId = $(this).attr('rowId');
            /**
             * 初始化菜单数据
             */
            $.ajax({
                url: window.host + '/module/listTree',
                type: 'POST',
                dataType: 'json',
                data: {},
                success: function (data) {
                    /* 初始化已选择菜单 */
                    $.ajax({
                        url: window.host + '/module/view/role/' + $roleId,
                        type: 'POST',
                        dataType: 'json',
                        success: function (res) {
                            // 清除选定
                            $menuSelect.empty();
                            var selectedList = res.data.modules;
                            if (res.errorCode == 200) {
                                var moduleList = data[0].children;
                                /* 生成option */

                              $.each(moduleList, function (i) {
                                var opt = $('<option></option>');
                                var cid = moduleList[i].id;
                                var ctext = moduleList[i].text;
                                opt.val(cid);
                                opt.text(ctext);
                                // 选择已有权限
                                if (selectedList) {
                                  selectedList.forEach(function (item) {
                                    if (cid == item.id) {
                                      opt.attr('selected',
                                          'selected');
                                    }
                                    $roleModules.addSelected(item.id, true);
                                  });
                                }
                                $menuSelect.append(opt);
                              });

                              $menuSelect.multiSelect('refresh');
                                $('#modal_menu').modal('show');
                            } else {
                                utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                            }
                        },
                        error: function () {
                            utils.tools.alert('网络问题，请稍后再试', {timer: 1200, type: 'warning'});
                        }
                    });

                },
                error: function () {
                    utils.tools.alert('网络问题，请稍后再试', {timer: 1200, type: 'warning'});
                }
            });

        });

        $(".edit").on("click", function () {
            var id = $(this).attr("rowId");
            $.ajax({
                url: window.host + '/merchantRole/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#name").val(role.roleName);
                        $("#desc").val(role.roleDesc);
                        $("#modal_component").modal("show");
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId = $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") == "popover") {
                target.popover("toggle");
            }
        });
    }

    /*手动删除*/
    function deleteComponent(pId) {
        var url = window.host + "/merchantRole/delete/" + pId;
        utils.postAjax(url, {}, function (res) {
            if (typeof(res) === 'object') {
                if (res.data) {
                  utils.tools.alert('保存成功!', {timer: 1200, type: 'success'});
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    // 保存
    $("#merchant_save").on('click', function () {
        var url = window.host + '/merchantRole/save';
        var id = $("#id").val();
        var name = $("#name").val();
        var desc = $("#desc").val();
        if (!name || name == '') {
            utils.tools.alert("请输入编码!", {timer: 1200, type: 'warning'});
            return;
        } else if (!desc || desc == '') {
            utils.tools.alert("请输入名称!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            roleName: name,
            roleDesc: desc
        };
        utils.postAjaxWithBlock($(document), url, data, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                      utils.tools.alert('保存成功!',
                          {timer: 1200, type: 'success'});
                        window.location.href = window.originalHost + '/merchant/role';
                        break;
                    }
                    default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    // TODO wangxh 步骤太多，重构到一起
    $('#role_model_save').on('click', function () {
        var bindData = $roleModules.getBindPostData();
        var unBindData = $roleModules.getUnBindPostData();
        var isSuccess = true;
        var netWorkError = true;
        var info = '';
        if (bindData && bindData.length) {
            $.ajax({
                url: window.host + '/module/bindRole',
                type: 'POST',
                async: false,
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(bindData),
                success: function (res) {
                    if (res.errorCode == 200 && res.data) {
                        console.log('添加成功');
                    } else {
                        isSuccess = false;
                        info += '新增菜单失败\n';
                    }
                },
                error: function () {
                    netWorkError = false;
                }
            });
        }
        if (unBindData && unBindData.length) {
            console.log('roleId:' + $roleId);
            console.log('moduleIdList:');
            console.log(unBindData);
            $.ajax({
                url: window.host + '/module/unBindRole',
                type: 'POST',
                dataType: 'json',
                async: false,
                data: {'roleId': $roleId, 'moduleIds': unBindData},
                success: function (res) {
                    console.log(res);
                    if (res.errorCode == 200 && res.data) {
                        if (unBindData && unBindData.length) {
                            console.log('删除成功');
                        } else {
                            isSuccess = false;
                            info += '删除菜单失败';
                        }
                    } else {
                        isSuccess = false;
                        info += res.moreInfo + '\n';
                    }
                },
                error: function () {
                    netWorkError = false;
                }
            });
        }
        var moduleCacheUrl = window.host + '/cache/module';
        utils.postAjaxWithBlock($(document), moduleCacheUrl, null, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            break;
                        }
                        default: {
                            isSuccess = false;
                            info += '刷新菜单缓存失败';
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    netWorkError = false;
                }
        });

        var roleCacheUrl = window.host + '/cache/role';
        utils.postAjaxWithBlock($(document), roleCacheUrl, null, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            break;
                        }
                        default: {
                            isSuccess = false;
                            info += '刷新角色缓存失败';
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    netWorkError = false;
                }
        });

        if (netWorkError && isSuccess) utils.tools.alert('操作成功');
        $('#modal_menu').modal('hide');
    });

    $('#role_model_cancel').on('click', function () {
        $roleModules.clear();
    });

    $('#grant_all_btn').on('click', function () {
        $menuSelect.multiSelect('select_all');
    });

    $('#revoke_all_btn').on('click', function () {
        $menuSelect.multiSelect('deselect_all');
    });

    $(".btn-search").on('click', function () {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search(keyword).draw();
    });

});