define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/twitter/getSummary';
    var data = {
    };
    // 本周订单,推客,交易额,佣金数
    var amount;
    var commission;
    var order;
    var twitter;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
        }
    });


    var time = echarts.init(document.getElementById('time_lines'), limitless);
    var time_options = {

        // Setup grid
        grid: {
            x: 55,
            x2: 45,
            y: 35,
            y2: 25
        },

        // Add tooltip
        tooltip: {
            trigger: 'axis'
        },

        // Enable drag recalculate
        calculable: true,
        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            padding: 0,
            feature: {
                dataView: {
                    show: true,
                    readOnly: false,
                    title: '查看图表数据',
                    lang: ['查看图表数据', '关闭', '更新']
                },
                restore: {
                    show: true,
                    title: '刷新'
                },
                saveAsImage: {
                    show: true,
                    title: '另存为图片',
                    lang: ['保存']
                }
            }
        },
        // Add legend
        legend: {
            data: ['时段平均访客数','时段平均浏览量'],
            textStyle: {
                fontSize: 16
            }
        },

        // Horizontal axis
        xAxis: [{
            type: 'category',
            data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
        }],

        // Vertical axis
        yAxis: [
            {
                type: 'value',
                name: '访客数',
                axisLabel: {
                    formatter: '{value}个'
                }
            },
            {
                type: 'value',
                name: '浏览量',
                axisLabel: {
                    formatter: '{value}次'
                }
            }
        ],

        // Add series
        series: [
            {
                name: '时段平均访客数',
                type: 'bar',
                data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
            },
            {
                name: '时段平均浏览量',
                type: 'line',
                yAxisIndex: 1,
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
            }
        ]
    };
    time.setOption(time_options);


    var region = echarts.init(document.getElementById('region_pies'), limitless);
    var region_options = {

        // Add title
        title: {
            text: '访客地域分布情况',
            subtext: '',
            x: 'center'
        },

        // Add tooltip
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },

        // Add legend
        legend: {
            orient: 'vertical',
            x: 'left',
            data: ['湖北省', '湖南省', '江苏省', '河北省', '青海省', '其他'],
            textStyle: {
                fontSize: 16
            }
        },

        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            padding: 0,
            feature: {
                dataView: {
                    show: true,
                    readOnly: false,
                    title: '查看图表数据',
                    lang: ['查看图表数据', '关闭', '更新']
                },
                magicType: {
                    show: true,
                    title: {
                        pie: '切换饼图样式',
                        funnel: '切换漏斗样式',
                    },
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            y: '20%',
                            width: '50%',
                            height: '70%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore: {
                    show: true,
                    title: '刷新'
                },
                saveAsImage: {
                    show: true,
                    title: '另存为图片',
                    lang: ['保存']
                }
            }
        },

        // Enable drag recalculate
        calculable: true,

        // Add series
        series: [{
            name: 'Browsers',
            type: 'pie',
            radius: '70%',
            center: ['50%', '57.5%'],
            itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                textStyle: {
                    fontSize: 16
                }
            }}},
            data: [
                {value: 1548, name: '湖北省'},
                {value: 310, name: '湖南省'},
                {value: 234, name: '江苏省'},
                {value: 135, name: '河北省'},
                {value: 335, name: '青海省'},
                {value: 50, name: '其他'}
            ]
        }]
    };
    region.setOption(region_options);


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            time.resize();
            region.resize();
        }, 200);
    }

});
