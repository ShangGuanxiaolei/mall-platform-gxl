define(['echartsNoAMD','echartsTheme','jquery', 'utils', 'd3', 'd3tooltip'], function(echarts, limitless, $, utils,d3, d3tooltip) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/team/getSummary';
    var data = {
    };
    // 交易额，订单数
    var weekDays;
    var weekNum;
    var weekAmount;
    var order;
    var amount;
    var team;
    var teamAmounts;
    var teamNames;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {

            // 取商城概况中的今日数据
            $("#today_num").html(result.data.info.today_num);
            $("#today_amount").html(result.data.info.today_amount);
            $("#total_team").html(result.data.info.total_team);
            $("#lastMonth_amount").html(result.data.info.lastMonth_amount);

            // 取最近七天交易额，订单数
            weekDays = result.data.nums.weekDays;
            weekNum = result.data.nums.week_num;
            weekAmount = result.data.nums.week_amount;
            teamAmounts = result.data.nums.teamAmounts;
            teamNames = result.data.nums.teamNames;

            //weekShipOrder = result.data.nums.weekShipOrder;

            order = echarts.init(document.getElementById('order_lines'), limitless);

            var stacked_lines_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['订单数']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: weekDays
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '订单数',
                        type: 'line',
                        stack: 'Total',
                        data: weekNum
                    }
                ]
            };
            order.setOption(stacked_lines_options);

            amount = echarts.init(document.getElementById('amount_lines'), limitless);

            var amount_lines_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['交易总额']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: weekDays
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '交易总额',
                        type: 'line',
                        stack: 'Total',
                        data: weekAmount
                    }
                ]
            };
            amount.setOption(amount_lines_options);

            team = echarts.init(document.getElementById('team_pie'), limitless);
            var basic_pie_options = {

                // Add title
                title: {
                    text: '战队概况',
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: teamNames
                },

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: 'Browsers',
                    type: 'pie',
                    radius: '70%',
                    data: teamAmounts
                }]
            };
            team.setOption(basic_pie_options);


        }
    });


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            order.resize();
            amount.resize();
            team.resize();
        }, 200);
    }

});
