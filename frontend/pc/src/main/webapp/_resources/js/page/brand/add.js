define(['jquery', 'validate', 'utils', 'utils/fileUploader',
      'jquerySerializeObject', 'select2', 'uniform'],
    function ($, validate, utils, uploader) {
      const moduleName = 'brand';
      const detailUrl = id => window.host + `/${moduleName}/${id}`;
      const saveUrl = window.host + `/${moduleName}/edit`;

      const addBrandButton = $("#addBrand");
      const brandForm = $('#brand-form');
      const modal = $("#modal_add_brand");
      const imageInput = $("#logo");
      const submitButton = $("#submit-brand");
      const isShowInput = $("#isShow");
      const dropZoneDom = "#brand-logo";
      let dropZoneInstance = '';
      let updateTable = '';
      let isSbmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();

        /**
         * 点击新增商品
         */
        addBrandButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitBrand = function (brand) {
        if (isSbmiting === true) {
          return;
        }
        if (!brand.id) {
          delete brand.id;
        }
        $.post(saveUrl, brand).done(date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(data => {
          modal.modal("hide");
          utils.tools.error("操作出错!");
        });
        isSbmiting = false;
      };

      const editBrand = function (id) {
        $.get(detailUrl(id)).done(date => {
          let brand = date.data;
          utils.tools.syncForm(brandForm, brand);
          brand.isShow === true ? isShowInput.prop("checked", true)
              : isShowInput.prop("checked", false);
          $.uniform.update();
          imageInput.prop("value", brand.logo);
          dropZoneInstance.addImage(brand.logoUrl, '', '', brand.logo);
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              brandForm[0].reset();
              brandForm.validate().resetForm();
              dropZoneInstance.clear();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = brandForm.valid();
              if (result) {
                console.log("success valid");
                let brand = brandForm.serializeObject();
                brand.isShow = !!isShowInput.prop("checked");
                brand.source = "CLIENT";
                submitBrand(brand);
              }
            });
            return this;
          },
          initDropZone: function () {
            const params = {
              dom: dropZoneDom,
              maxFiles: 1,
              onSuccess: (data) => {
                imageInput.attr("value", data.id);
              },
              onRemove: () => {
                imageInput.attr("value", '');
              }
            };
            dropZoneInstance = uploader.create(params);
            return this;
          },
          initValidate: function () {

            brandForm.validate({
              rules: {
                logo: {
                  required: true,
                },
                name: {
                  required: true,
                },
                sortOrder: {
                  required: true,
                  number: true,
                },
                description: {
                  required: false,
                },
                siteHose: {
                  required: false,
                },
              },
              messages: {
                logo: {
                  required: "请上传商品logo"
                },
                name: {
                  required: "请输入商品名称",
                },
                sortOrder: {
                  required: "请输入排序号",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initDropZone().initValidate().initSubmit().resetForm();

      return {
        edit: editBrand,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });
