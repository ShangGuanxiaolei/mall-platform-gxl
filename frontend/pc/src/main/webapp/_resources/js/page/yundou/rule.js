define(['jquery', 'utils', 'datatables', 'blockui', 'bootstrapSwitch', 'select2'], function ($, utils, datatables, blockUI, select2) {

    const prefix = window.host + '/yundou/rules';

    const roleUrl = window.host + '/role/list';
    const listOperation = window.host + '/yundou/operation/list';

    const listUrl = prefix + '/list';
    const saveUrl = prefix + '/save';
    const deleteUrl = prefix + '/remove';

    const columns = ['name', 'code', 'description', 'created_at'];

    var operationId = '';

    const ruleAjax = {
        save: function (data) {
            utils.postAjax(saveUrl, data, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data === true) {
                        utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                        $('#modal_rule_save').modal('hide');
                    } else {
                        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'})
                    }
                }
            })
        },
        remove: function (id) {
            utils.postAjax(deleteUrl + '/' + id, null, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data === true) {
                        utils.tools.alert('操作成功', {timer: 1200, type: 'warning'})
                        $datatables.search('').draw();
                    } else {
                        console.log(data);
                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                    }
                }
            })
        },
        update: function (data) {
            utils.postAjax(saveUrl, data, function (res) {
                if (res === -1) {
                    utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                    if (res.errorCode === 200) {
                        utils.tools.alert('操作成功', { timer: 1200, type: 'success' });
                        $('#modal_rule_detail').modal('hide');
                    } else {
                        if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                        } else {
                            utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                        }
                    }
                }
            });
        }
    };

    const block = {
        fullBlock: function () {
            return $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        },
        unBlock: function () {
            return $.unblockUI();
        }
    };

    const functions = {
        roleAmount: function (details) {
            var result = {};
            for (var i in details) {
                if (details.hasOwnProperty(i)) {
                    result[details[i].roleId] = details[i].amount;
                }
            }
            return result;
        }
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            infoEmpty: "",
            emptyTable: "暂无操作"
        }
    });

    globalInit();

    const $datatables = $('#xquark_rule_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        pageable: true,
        ajax: function (data, callback) {
            console.log(data.order[0].column);
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                order: columns[data.order[0].column],
                direction: data.order[0].dir,
                operationId: operationId
            }, function (res) {
                if (res.errorCode != '200') {
                    utils.tools.alert('数据加载失败');
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '60px',
                orderable: true,
                name: 'name'
            },
            {
                data: 'code',
                width: '60px',
                orderable: false,
                name: 'code'
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    return row.description ? row.description : '无';
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) {
                        return '';
                    }
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" rowId="'
                        + row.id
                        + '" fid="edit_yundou_rule" class="edit_yundou_rule"><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" rowId="'
                        + row.id
                        + '" fid="delete_yundou_rule" class="delete_yundou_rule" data-toggle="popover" style="margin-left: 15px"><i class="icon-trash" ></i>删除</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    function initEvent() {
        $('body').unbind('click');

        $('.edit_yundou_rule').on("click", function () {
            var id = $(this).attr('rowId');
            var url = window.host + '/yundou/rules/show/' + id;
            block.fullBlock();
            utils.postAjax(url, null, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误，请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                var data = res.data;
                if (data) {
                    block.unBlock();
                    $('#ruleId_detail').val(data.id);
                    $('#ruleName_detail').val(data.name);
                    $('#ruleCode_detail').val(data.code);
                    $('#ruleDescription_detail').val(data.description);
                    operationId = data.operationId;
                    $('#operationId').val(operationId);

                    // 设置开关状态
                    $('#directionBox_detail').bootstrapSwitch('state', data.direction);

                    if (data.details) {
                        var details = data.details;
                        for (var i in details) {
                            if (details.hasOwnProperty(i)) {
                                $('#amount_detail').val(details[i].amount);
                            }
                        }
                    }
                    $('#modal_rule_detail').modal('show');
                }
            })
        });

        /** 点击删除弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var id = $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                    + id + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find(
                    '[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var param = $(this).attr("param");
                ruleAjax.remove(param);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_rule_tables');

        //给Body加一个Click监听事件
        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") === "popover") {
                target.popover("toggle");
            }
        });
    }

    function globalInit() {

        /* 选择下拉操作时刷新表格数据 */
        $('select[name=operation]').change(function () {
            operationId = $(this).val();
            $datatables.search('').draw();
        });

        var $directionBox = $("input[name='direction-checkbox']");
        $directionBox.bootstrapSwitch();
        $directionBox.on('switchChange.bootstrapSwitch', function (e, state) {
            $('#direction').val(state === true ? '+' : '-');
        });

        $('#save_btn').on('click', function (e) {
            e.preventDefault();
            $('#ruleId').val('');
            $('#ruleName').val('');
            $('#ruleCode').val('');
            $('#amount').val('');
            $('#roleIdSelect').val('');
            $('#roleSelect').val('');
            $('#ruleDescription').val('');
            $('#modal_rule_save').modal('show');
        });

        $('#confirm_btn').on('click', function (e) {
            e.preventDefault();
            var id = $('#ruleId').val();
            var operationId = $('#operation').val();
            var name = $('#ruleName').val();
            var code = $('#ruleCode').val();
            var directionStr = $('#direction').val();

            var amount = $('#amount').val();
            var description = $('#ruleDescription').val();

            if (!directionStr in ['+', '-']) {
                utils.tools.alert('云豆方向不正确', {timer: 1200, type: 'warning'});
                return;
            }
            if (operationId === '') {
                utils.tools.alert('请先选择操作', {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                id: id,
                operationId: operationId,
                // roleId: roleId,
                name: name,
                code: code,
                direction: directionStr === '+',
                amount: amount,
                description: description
            };
            ruleAjax.save(data);
        });

        $('#confirm_btn_detail').on('click', function (e) {
            e.preventDefault();
            var amount_detail = $('#amount_detail').val();
            var ruleId = $('#ruleId_detail').val();
            var name = $('#ruleName_detail').val();
            var code = $('#ruleCode_detail').val();
            var description = $('#ruleDescription_detail').val();
            var directionStr = $('#direction').val();

            var data = {
                id: ruleId,
                operationId: operationId,
                amount: amount_detail,
                // roleId: roleId,
                name: name,
                code: code,
                direction: directionStr === '+',
                description: description
            };
            ruleAjax.update(data);
        });
        dataInit();
    }

    /* 初始化数据 */
    function dataInit() {
        block.fullBlock();
        var isDown = 0;
        utils.postAjax(roleUrl, {keyword: ''}, function (res) {
            if (res === -1) {
                utils.tools.alert('网络错误, 请稍候再试', {timer: 1200, type: 'warning'});
                return
            }
            var data = res.data;
            if (data) {
                var total = data.total;
                if (total && total !== 0) {
                    for (var i = 0; i < total; i++) {
                        var code = data.list[i].code.toLowerCase();
                        $('#' + code + '_amount').attr('roleId', data.list[i].id);
                    }
                }
                if (isDown === 1) {
                    block.unBlock();
                }
                isDown += 1;
            }
        });
        utils.postAjax(listOperation, null, function (res) {
            if (res === -1) {
                utils.tools.alert('网络错误, 请稍候再试', {timer: 1200, type: 'warning'});
                return
            }
            var data = res.data;
            if (data) {
                var total = data.total;
                if (total && total !== 0) {
                    var $select = $('#operation');
                    for (var i = 0; i < total; i++) {
                        $select.append(
                            '<option value=' + data.list[i].id + '>' + data.list[i].name
                            + '</option>');
                    }
                    var operationId = getParameterByName('operationId');
                    if (operationId && operationId !== '') {
                        $select.val(operationId).change();
                        $datatables.search('').draw();
                    }
                }
                if (isDown === 1) {
                    block.unBlock();
                }
                isDown += 1;
            }
        })
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
});
