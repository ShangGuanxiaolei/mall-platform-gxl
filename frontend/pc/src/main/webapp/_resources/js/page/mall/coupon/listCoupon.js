define(['jquery', 'utils', 'datatables','mall/coupon/addCoupon','select2'], function ($, utils, datatabels,addCoupon,select2) {

    $dataTable = '';
    const tableId = "#couponTable";
    const couponSelect = $('#coupon-status');
    const couponName = $('#coupon-name');
    const couponRange = $('#coupon-range');
    const kvArray = [['VALID', '可使用'], ['OVERDUE', '已过期'], ['LOCKED', '已有订单使用'], ['USED', '已使用'], ['CLOSED', '关闭']];
    const statusCache = new Map(kvArray);
    const searchButton = $("#search");
    //-------------------
    const urlPrefix = '/bos';
    const listUrl = `${urlPrefix}/couponManager/list`;


    let eventInitialization = function () {
        return {
            initDatePicker: function () {
                let dateOption = {
                    timePicker: false,
                    startDate: moment(),
                    endDate: moment().subtract(0, 'month').endOf('month'),
                    locale: {
                        format: 'YYYY/MM/DD',
                        separator: ' - ',
                        applyLabel: '确定',
                        startLabel: '开始日期:',
                        endLabel: '结束日期:',
                        cancelLabel: '清空',
                        weekLabel: 'W',
                        customRangeLabel: '日期范围',
                        daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                        monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                        firstDay: 6
                    },
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small btn-default',
                };
                couponRange.daterangepicker(dateOption);
                couponRange.val('');
                /**
                 * 清空按钮清空选框
                 */
                couponRange.on('cancel.daterangepicker', function(ev, picker) {
                    couponRange.val('');
                });

                return this;
            },
            initSearch: function () {
                searchButton.on("click", function () {
                    $dataTable.search('').draw();
                });
                return this;
            },
            initSelect: function () {
                couponSelect.select2({
                    minimumResultsForSearch: Infinity,
                });
                return this;
            },
            initDataTable: function () {
                /** 页面表格默认配置 **/
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>筛选:</span> _INPUT_',
                        lengthMenu: '<span>显示:</span> _MENU_',
                        info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                        paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                        infoEmpty: "",
                        emptyTable: "暂无相关数据"
                    }
                });
                $dataTable = $(tableId).DataTable({
                    paging: true, //是否分页
                    filter: false, //是否显示过滤
                    lengthChange: false,
                    processing: false,
                    serverSide: true,
                    deferRender: false,
                    searching: false,
                    ordering: false,
                    sortable: false,
                    ajax: function (data, callback, settings) {
                        $.post(listUrl, {
                            rows: data.length,
                            page: function () {
                                let page = (data.start / data.length);
                                return page + 1;
                            },
                            status_kwd: couponSelect.val(),
                            name_kwd: couponName.prop("value"),
                            valid_from_kwd: function () {
                                let range = couponRange.prop("value");
                                if (range) {
                                    return range.split("-")[0].trim();
                                }
                                return '';

                            },
                            valid_to_kwd: function () {
                                let range = couponRange.prop("value");
                                if (range) {
                                    return range.split("-")[1].trim();
                                }
                                return '';
                            },
                            pageable: true,
                        }, function (res) {
                            if (!res.rows) {
                                res.rows = [];
                            }
                            callback({
                                recordsTotal: res.total,
                                recordsFiltered: res.total,
                                data: res.rows,
                                iTotalRecords: res.total,
                                iTotalDisplayRecords: res.total
                            });
                        });
                    },
                    rowId: "id",
                    columns: [
                        {
                            width: "10px",
                            orderable: false,
                            render: function (data, type, row) {
                                return `<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="${row.id}" value="${row.id}"></label>`;
                            }
                        },
                        {
                            data: "name",
                            orderable: false,
                            title: "优惠券名称"
                        },
                        {
                            data: "batchNo",
                            orderable: false,
                            title: "批次号"
                        },
                        {
                            data: "distributedAt",
                            orderable: false,
                            title: "发行日期",
                            render: function (date, type, row) {
                                let data = parseInt(row.distributedAt);
                                let d = new Date(data);
                                return d.format('yyyy-MM-dd');
                            }
                        },
                        {
                            data: "amount",
                            orderable: false,
                            title: "总数"
                        },
                        {
                            data: "discount",
                            orderable: false,
                            title: "面值"
                        },
                        {
                            data: "remainder",
                            orderable: false,
                            title: "剩余"
                        },
                        {
                            data: "totalDiscount",
                            orderable: false,
                            title: "总金额"
                        },
                        {
                            data: "validFrom",
                            orderable: false,
                            render: function (date, type, row) {
                                let from = parseInt(row.validFrom);
                                let to = parseInt(row.validTo);
                                return new Date(from).format('yyyy-MM-dd') + '~' + new Date(to).format('yyyy-MM-dd');
                            },
                            title: "时效"
                        },
                        {
                            data: "status",
                            orderable: false,
                            render: function (date, type, row) {
                                return statusCache.get(row.status);
                            },
                            title: "状态"
                        },
                        {
                            title: '操作',
                            orderable: false,
                            render: function (data, type, row) {
                                let html = '';
                                html += `<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="${row.id}" ><i class="icon-pencil7" ></i>编辑</a>`;
                                html += `<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="${row.id}" ><i class="icon-trash"></i>使失效</a>`;
                                return html;
                            }
                        }
                    ],
                    drawCallback: function () {  //数据加载完成回调
                        $(".edit").on("click",function(){
                            let id=$(this).attr("rowId");
                            addCoupon.edit(id);
                        });
                    }
                });
                return this;
            },
        }

    }();

    return {
        init: function () {
          eventInitialization.initSelect().initDataTable().withSearchEnable().initDatePicker();
        },
    }

});
