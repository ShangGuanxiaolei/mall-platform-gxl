define(['jquery', 'utils', 'product/product-data', 'datatables', 'dropzone'], function(jquery, utils, proData, datatables, dropzone){
    Dropzone.autoDiscover = false;

          /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "",
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

    var $datatables = $('#fragment_data_table').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: false,
        deferRender: true,
        searching: true,
        rowId: "id",
        columns: [
          {
            title: "图片",
            width: "60px",
            render: function (data, type, row) {
              return '<img class="goods-image" src="' + row.imgs[0].imgUrl
                  + '"/>';
            }
          },
          {
            title: "名称",
            data: "name",
            name: "name",
            width: "60px",
          },
          {
            title: "描述",
            data: "description",
            name: "description",
            width: "60px",
          },
          {
            title: "操作",
            width: "120px",
            render: function (data, type, row) {
              var html = '';
              html += '<a class="fragmentEdit" style="margin-right: 5px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
              html += '<a class="fragmentDel" data-toggle="popover" rowId="'
                  + row.id + '" ><i class="icon-trash"></i>删除</a>';
              return html;
            }
          }
        ]
      });

      function setFragments(fragments) {
        $.each(fragments, function (index, fragment) {
          $datatables.row.add(
              {
                "imgs": fragment.imgs,
                "name": fragment.name,
                "description": fragment.description,
                "id": fragment.id
              }
          );
        });
        $datatables.search('').draw();
      }

      /**
       * 编辑段落描述
       */
      $('#fragment_data_table').on('click', '.fragmentEdit', function () {
        var fragmentRow = $(this).closest("tr").get(0);
        proData.viewProductFragment($(fragmentRow).attr('id'),
          function (data) {
            var d = data.data;
            $('#fragmentId').val(d.id);
            clearFragment();
            $('#fDescribeName').val(d.name);
            $('#fDescribeText').val(d.description);
            d.imgs.forEach(addFragmentImage);
            console.log(data);
          }, utils.tools.alert);
        $("#modal_descAdd").modal('show');
      });

      function clearFragment() {
        $('#fDescribeText').val('');
        $('#fDescribeName').val('');
        $('#fragmentImg').val('');
        removeFragmentImgs();
      }


      //添加图片
      function addFragmentImage(img) {
        if (img) {
          var mockFile = {name: "", size: "", dataImg: img.id};
          descImgDropzone[0].dropzone.emit("addedfile", mockFile);
          descImgDropzone[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
          descImgDropzone[0].dropzone.emit("complete", mockFile);
          descImgDropzone[0].dropzone.files.push( mockFile ); // 此处必须手动添加才可以用removeAllFiles移除
          $('#fragmentImg').val(img.img);
        }
      }

      // 删除段落描述图片
      function removeFragmentImgs() {
        descImgDropzone[0].dropzone.removeAllFiles(true);
      }

    var descImgDropzone = $("#desc_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",
        paramName: "file",
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',
        maxFilesize: 10, //MB
        maxFiles: 20,
        maxThumbnailFilesize: 1,
        addRemoveLinks: true,
        thumbnailWidth:"80",
        thumbnailHeight:"80",
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",
        dictRemoveFile: "删除",
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",
        init: function() {
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".saveDescBtn").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
            });
            this.on("success", function(file,data) {
                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".saveDescBtn").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".saveDescBtn").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 80;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 80;
                }
                $(".dz-image").css("height","80px;");
                $(".dz-image").css("width","80px;");
            });
        }
    });

    var descrip = {
        init: function () {
            var that = this;
            that.delete();
            that.editor();
            that.descModalInit();
            that.submitDesc();
        },
        descModalInit: function() {
            $("#btnDescAdd").on('click', function() {
                $('#fDescribeText').val('');
                $('#fDescribeName').val('');
                $('#fragmentImg').val('');
                $('#fragmentId').val('');
                descImgDropzone[0].dropzone.removeAllFiles(true);
                $("#modal_descAdd").modal('show');
            });
        },

        delete: function () {
            $(".edit-choose").delegate('.delete', 'click', function() {
                var _this = this;
                utils.tools.confirm("确认删除此段落描述？", {
                    fnConfirm: function() {
                        if ($(".descrip").length == 1) {
                            $(_this).parent().parent().addClass("fn-hide");
                        } else {
                            $(_this).parent().parent().remove();
                        }
                    }
                });
            });
        },
        editor: function() {
            $(".edit-type-list").delegate('.input-edit', 'click', function() {
                $(".pageEditor").hide();
                $(".sortDrag").hide();
                if ($("#editWeb").attr("checked")) {
                    $(".sortDrag").hide();
                    $(".pageEditor").show();
                } else if ($("#editApp").attr("checked")) {
                    $(".pageEditor").hide();
                    $(".sortDrag").show();
                }
            })
        },

        //填充段落描述弹出层
        fillDesc: function(data) {
            $('#describeAdd .imgBox').empty();
            if (data) {
                //编辑状态
                $('#describeName').val(data.name);
                $('#describeText').val(data.description);
                if (data.imgs[0]) {
                    for (var i = 0; i < data.imgs.length; i++) {
                        $('#describeAdd .imgBox').append('<span><img src="' + data.imgs[i] + '" key="' + data.keys[i] + '" /><i>&#xe602;</i></span>');
                    }
                }
                $('.pageFormContent [name="paraPlace"]').filter('[value=' + data.showModel + ']').attr('checked', true);
                $("#describeAdd").attr('state', 'modify').attr('_from', data.index);
            } else {
                //新增状态
                $('#describeName').val('');
                $('#describeText').val('');
                $('.pageFormContent [name="paraPlace"]:eq(0)').attr('checked', "checked");
                $("#describeAdd").attr('state', '').attr('_from', '');
                $('#describeAdd .imgBox').empty();
            }
        },

        modifyDesc: function() {
            var that = this;
            $('body').off('click', '.pro-desibe .edit')
            $('body').on('click', '.pro-desibe .edit', function() {
                //$('#skuEdit').after('<div id="dialogMask"></div>');
                var $this = $(this).closest('.pro-desibe');
                var data = {
                    name: $this.find('.pro-name span').text(),
                    description: $this.find('.pro-text').text(),
                    imgs: $this.find('.pro-img img').attr('srcs').split(','),
                    keys: $this.find('.pro-img img').attr('keys').split(','),
                    showModel: $this.attr('data-showModel'),
                    index: $this.attr('data-id')
                }
                that.fillDesc(data);
                $("#describeAdd").show();
            });

        },

        submitDesc: function() {
            var that = this;
            $('body').on('click', '.saveDescBtn', function() {
                var pop = $("#modal_descAdd");
                for (var i = 0; i < pop.find('.required').length; i++) {
                    if (pop.find('.required').eq(i).val() == '') {
                        utils.tools.alert('标有*号的项不能为空~');
                        return;
                    }
                }

                var imgs = [];
                $('#desc_image_dropzone').find('.dz-success').each(function(i, el) {
                    imgs.push($(el).attr('data-img'));
                });

                var img = $('#fragmentImg').val();
                // 从编辑过来的
                if (img && img !== '') {
                  imgs.push(img);
                }

              if (!imgs.length) {
                    utils.tools.alert('段落描述图片为必选项~');
                    return;
                }

                var data = {
                    id: $('#fragmentId').val(),
                    name: pop.find('[name="describeName"]').val(),
                    description: pop.find('[name="describeText"]').val(),
                    imgs: imgs.join(','),
                    showModel: pop.find('[name="paraPlace"]:checked').val()
                };
                //保存或新建在这里处理
                //if (pop.attr('state') == 'modify') {
                //    //如果是修改
                //    ajaxData.id = data.id = pop.attr('_from');
                //    var str = doTtmpl(data);
                //    $('.pro-desibe[data-id="' + data.id + '"]').replaceWith(str);
                //} else {
                //    //如果是新建
                //    delete ajaxData.id;
                //    delete data.id;
                //    $('.descList').append(doTtmpl(data));
                //}
                //发送ajax请求
                utils.postAjax(window.host + '/fragment/save', data, function(res) {
                    if (typeof(res) == 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("添加段落描述成功", {timer: 1200, type: 'success'});
                                $("#modal_descAdd").modal("hide");
                                if (data.id && data.id !== '') {
                                  // 编辑
                                  var newData = null;
                                  $datatables.rows().every(function () {
                                    var d = this.data();
                                    if (d.id === data.id) {
                                      newData = d;
                                      newData.description = res.data.description;
                                      newData.name = res.data.name;
                                    }
                                  });
                                  if (newData === null) {
                                    utils.tools.alert('保存失败', { timer: 1200, type: 'warning' });
                                    return;
                                  }
                                  $datatables.row().data(newData).draw();
                                } else {
                                  // 新增row
                                  // datatable为静态表格, 不能刷新!
                                  // datatable为静态表格, 不能刷新!
                                  // datatable为静态表格, 不能刷新!
                                  $datatables.row.add({
                                    "imgs": res.data.imgs,
                                    "name": res.data.name,
                                    "description": res.data.description,
                                    "id": res.data.id
                                }).draw();
                                }

                                //$(".descListData").append("<tr class='append-desc-data' data-id='" + res.data.id + "'>"
                                //        + "<td><img class='goods-image' src='" + res.data.imgs[0].imgUrl + "' /></td>"
                                //        + "<td>" + res.data.name + "</td>"
                                //        + "<td>" + res.data.description + "</td>"
                                //        + "<td>" + res.data.id + "</td>"
                                //        + "</tr>"
                                //);
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("段落保存失败", {timer: 1200});
                    }
                });
            });
        },

        delImg: function() {
            var that = this;
            $('body').off('click', '#describeAdd .imgBox i');
            $('body').on('click', '#describeAdd .imgBox i', function() {
                var _this = this;
                utils.tools.confirm("确认删除此段落描述图片？", {
                    fnConfirm: function() {
                        $(_this).parent('span').remove();
                    }
                });
            });
        },

        delProImg: function() {
            var that = this;
            $('body').off('click', '.productImgs .imgBox i');
            $('body').on('click', '.productImgs .imgBox i', function() {
                var _this = this;
                utils.tools.confirm("确认删除此商品图片？", {
                    fnConfirm: function() {
                        $(_this).parent('span').remove();
                    }
                });
            });
        },
    }
    descrip.init();

    return {
        dropzone: descImgDropzone[0],
        setFragments: setFragments
    };
});