define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment,fileinput_zh, fileinput) {
        var firstRate = 0;
        var secondRate = 0;
        var thirdRate = 0;
        var $teamId;
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            opens: 'left',
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-basic');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });

        var singleOptions = {
            singleDatePicker: true,
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' - ',
                cancelLabel: '取消',
                weekLabel: 'W',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            }
        };
        $('.daterange-single').daterangepicker(singleOptions);

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        options.startDate = '';
        options.endDate   = '';
        $('.daterange-basic').val('');

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        // 图片上传
        var $fileUpload = $('#logo-file');
        var $logo = $('#setting_logo');
        var fileUploadManager = {
            uploadOption: {
                uploadUrl: '/sellerpc/member/card/upload',
                showCaption: false,
                showUpload: false,
                uploadAsync: true,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"]
            },
            init: function () {
                var defaultImg = $logo.val();
                if (defaultImg && defaultImg.toString().startsWith('http')) {
                    // 如果已经有图片则显示默认
                    this.uploadOption = $.extend({
                        showPreview: true,
                        initialPreview: [ // 预览图片的设置
                            "<img src= '" + defaultImg + "' class='file-preview-image'>"]
                    }, this.uploadOption);
                }
                // 初始化文件上传控件
                $fileUpload.fileinput(this.uploadOption);
                // 上传之前
                $fileUpload.on('filepreajax', function () {
                    //$btnSubmit.addClass('disabled');
                });

                // 选中后立即上传
                $fileUpload.on('filebatchselected', function () {
                    $fileUpload.fileinput('upload');
                });

                // 上传成功
                $fileUpload.on('fileuploaded', function (event, data) {
                    var res = data.response;
                    if (res.errorCode && res.errorCode === 200) {
                        $logo.val(res.img);
                    } else {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                });
                return this;
            },
            destroy: function () {
                $fileUpload.fileinput('destroy');
                return this;
            },
            refresh: function () {
                this.destroy().init();
            }
        };

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/team/memberList", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#status").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    userPhone:$("#userPhone").val(),
                    userName:$("#userName").val(),
                    name:$("#name").val(),
                    groupId: $("#groupId").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    sClass: 'sorting',
                    title: '战队',
                    data:'name',
                    name:'name',
                    sortable: true
                },{
                    width:'75px',
                    sClass: 'sorting',
                    title: '队长',
                    data:'userName',
                    name:'userName',
                    sortable: true
                },{
                    width:'75px',
                    sClass: 'sorting',
                    title: '手机号',
                    data:'userPhone',
                    name:'userPhone',
                    sortable: true
                },{
                    width:'75px',
                    sClass: 'sorting',
                    title: '队员数',
                    render: function(data, type, row){
                        return '<font color= "#07d"><a href="javascript:void(0);" class="viewMemberBtn" rowId="'+row.id + '">' + row.teamNum + '</a></font> / ' + row.num;
                    }
                },
                {
                    width:'75px',
                    sClass: 'sorting',
                    title: '分组',
                    data:'groupName',
                    name:'groupName',
                },{
                    width:75,
                    sClass: 'sorting',
                    data: 'status',
                    name: 'status',
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if(value =='ACTIVE'){
                            return "正常";
                        }else{
                            return "禁用";
                        }
                    }
                },
                {
                width:75,
                sClass: 'sorting',
                title: '创建时间',
                sortable: true,
                render: function (data, type, row) {
                    if (row.createdAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:150,
                sClass: 'styled text-center sorting_disabled',
                align: 'right',
                title: '操作',
                render: function(data, type, row) {
                    return '<a href="javascript:void(0);" style="margin-left: 10px;" class="settingBtn" rowId="'+row.id + '">设置</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="setMemberBtn" rowId="'+row.id + '">新增队员</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="disableBtn" data-toggle="disablepopover" rowId="'+row.id + '">禁用</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="enableBtn" data-toggle="enablepopover" rowId="'+row.id + '">启用</a>  ';
                }
            }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });


        // 战队分组列表
        var $groupdatatables = $('#userGroupTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/team/groupList", {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    title: '分组名',
                    data:'name',
                    name:'name',
                    sortable: false
                },{
                    width:'100px',
                    title: '队员数量',
                    data:'num',
                    name:'num',
                    sortable: false
                },{
                    width:'100px',
                    title: '战队数',
                    data:'teamNum',
                    name:'teamNum',
                    sortable: false
                },{
                    width:'300px',
                    data:'content',
                    name:'content',
                    title: '考核内容',
                    sortable: false
                },
                {
                    width:'100px',
                    title: '创建时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:'150px',
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="groupedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="groupdel role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initLevelEvent();
            }
        });
        
        function initLevelEvent() {
            $(".groupedit").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/team/getGroup/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            $("#settingId").val(role.id);
                            $("#settingNum").val(role.num);
                            $("#settingName").val(role.name);

                            // 重置按自然周设置信息
                            $("#weekSetting").empty();
                            var weeks = role.weeks;
                            var months = role.months;
                            $.each(weeks, function(i, value) {
                                $("#weekSetting").append('<li>'
                                    +' <div class="checkbox inline"><label>'
                                    +'  <input class="input-sm" name="weekCheckbox" type="checkbox" ' + (value.defaultStatus ? 'checked=true' : '') + ' />按自然周</label>'
                                    +' </div>'
                                    +'  <div><span>'
                                    +' <span class="control-label">销售满&nbsp;<input name="week_sale" id="week_sale" value="' + value.sales + '" type="text"/>元，&nbsp;战队总提成&nbsp;'
                                    +' <input name="week_totalAmount" id="week_totalAmount" type="text" value="' + value.totalAmount + '"/>元，&nbsp; 其中队长提&nbsp;'
                                    +' <input name="week_leaderAmount" id="week_leaderAmount" type="text" value="' + value.leaderAmount + '"/>元。&nbsp;</span></span>'
                                    +'<span class="btn btn-danger btn-sm ng-scope delSetting">删除</span>'
                                    +' </div>'
                                    +'  </li>');
                            });

                            // 重置按自然月设置信息
                            $("#monthSetting").empty();
                            $.each(months, function(i, value) {
                                $("#monthSetting").append('<li>'
                                    +' <div class="checkbox inline"><label>'
                                    +'  <input class="input-sm" name="monthCheckbox" type="checkbox" ' + (value.defaultStatus ? 'checked=true' : '') + ' />按自然月</label>'
                                    +' </div>'
                                    +'  <div><span>'
                                    +' <span class="control-label">销售满&nbsp;<input name="month_sale" id="month_sale" value="' + value.sales + '" type="text"/>元，&nbsp;战队总提成&nbsp;'
                                    +' <input name="month_totalAmount" id="month_totalAmount" type="text" value="' + value.totalAmount + '"/>元，&nbsp; 其中队长提&nbsp;'
                                    +' <input name="month_leaderAmount" id="month_leaderAmount" type="text" value="' + value.leaderAmount + '"/>元。&nbsp;</span></span>'
                                    +'<span class="btn btn-danger btn-sm ng-scope delSetting">删除</span>'
                                    +' </div>'
                                    +'  </li>');
                            });


                            $("#modal_addGroup").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteGroup(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增战队分组
        $(document).on('click', '.btnAddGroup', function() {
            $("#settingId").val('');
            $("#settingNum").val('');
            $("#settingName").val('');

            // 重置按自然周设置信息
            $("#weekSetting").empty();
            weekAdd();

            // 重置按自然月设置信息
            $("#monthSetting").empty();
            monthAdd();

            /** 初始化选择框控件 **/
            $('.select').select2({
                minimumResultsForSearch: Infinity,
            });
            $("#modal_addGroup").modal("show");
        });


        function deleteGroup(id) {
            $.ajax({
                url: host + '/team/deleteGroup/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $groupdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        // 删除设置
        $(document).on('click', '.delSetting', function() {
            $(this).closest("li").remove();
        });

        function weekAdd() {
            $("#weekSetting").append('<li>'
                +' <div class="checkbox inline"><label>'
                +'  <input class="input-sm" name="weekCheckbox" type="checkbox"/>按自然周</label>'
                +' </div>'
                +'  <div><span>'
                +' <span class="control-label">销售满&nbsp;<input name="week_sale" id="week_sale" value="" type="text"/>元，&nbsp;战队总提成&nbsp;'
                +' <input name="week_totalAmount" id="week_totalAmount" type="text" value=""/>元，&nbsp; 其中队长提&nbsp;'
                +' <input name="week_leaderAmount" id="week_leaderAmount" type="text" value=""/>元。&nbsp;</span></span>'
                +'<span class="btn btn-danger btn-sm ng-scope delSetting">删除</span>'
                +' </div>'
                +'  </li>');
        }

        function monthAdd() {
            $("#monthSetting").append('<li>'
                +' <div class="checkbox inline"><label>'
                +'  <input class="input-sm" name="monthCheckbox" type="checkbox"/>按自然月</label>'
                +' </div>'
                +'  <div><span>'
                +' <span class="control-label">销售满&nbsp;<input name="month_sale" id="month_sale" value="" type="text"/>元，&nbsp;战队总提成&nbsp;'
                +' <input name="month_totalAmount" id="month_totalAmount" type="text" value=""/>元，&nbsp; 其中队长提&nbsp;'
                +' <input name="month_leaderAmount" id="month_leaderAmount" type="text" value=""/>元。&nbsp;</span></span>'
                +'<span class="btn btn-danger btn-sm ng-scope delSetting">删除</span>'
                +' </div>'
                +'  </li>');
        }

        // 按自然周新增
        $(".addWeek").on('click', function() {
            weekAdd();
        });

        // 按自然月新增
        $(".addMonth").on('click', function() {
            monthAdd();
        });

        // 保存战队分组
        $(".saveGroupBtn").on('click', function() {
            var id = $("#settingId").val();
            var num = $("#settingNum").val();
            var name = $("#settingName").val();

            if(name == ''){
                utils.tools.alert("请输入战队别名", {timer: 1200, type: 'success'});
                return;
            }else if(num == ''){
                utils.tools.alert("请输入默认队员数量", {timer: 1200, type: 'success'});
                return;
            }

            var weekCheckbox = [];
            var weekSale = [];
            var weekTotalAmount = [];
            var weekLeaderAmount = [];

            var monthCheckbox = [];
            var monthSale = [];
            var monthTotalAmount = [];
            var monthLeaderAmount = [];

            var array=$("[name='weekCheckbox']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).prop("checked");
                weekCheckbox.push(value);
            }
            array=$("[name='week_sale']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然周销售", {timer: 1200, type: 'success'});
                    return;
                }
                weekSale.push(value);
            }
            array=$("[name='week_totalAmount']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然周战队总提成", {timer: 1200, type: 'success'});
                    return;
                }
                weekTotalAmount.push(value);
            }
            array=$("[name='week_leaderAmount']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然周队长提", {timer: 1200, type: 'success'});
                    return;
                }
                weekLeaderAmount.push(value);
            }

            array=$("[name='monthCheckbox']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).prop("checked");
                monthCheckbox.push(value);
            }
            array=$("[name='month_sale']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然月销售", {timer: 1200, type: 'success'});
                    return;
                }
                monthSale.push(value);
            }
            array=$("[name='month_totalAmount']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然月战队总提成", {timer: 1200, type: 'success'});
                    return;
                }
                monthTotalAmount.push(value);
            }
            array=$("[name='month_leaderAmount']");
            for(var i=0;i<array.length;i++){
                var value = $(array[i]).val();
                if(value == ''){
                    utils.tools.alert("请输入自然月队长提", {timer: 1200, type: 'success'});
                    return;
                }
                monthLeaderAmount.push(value);
            }

            var data = {
                'id' : id,
                'name' : name,
                'num' : num,
                'defaultStatus' : 'false',
                'weekCheckbox' :weekCheckbox,
                'weekSale' :weekSale,
                'weekTotalAmount' :weekTotalAmount,
                'weekLeaderAmount' :weekLeaderAmount,
                'monthCheckbox' :monthCheckbox,
                'monthSale' :monthSale,
                'monthTotalAmount' :monthTotalAmount,
                'monthLeaderAmount' :monthLeaderAmount
            }

            utils.postAjax(window.host+ "/team/saveSetting", data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                        $("#modal_addGroup").modal("hide");
                        $groupdatatables.search('').draw();
                    } else {
                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });

        });

        // 保存战队设置
        $(".saveSetting").on('click', function() {
            var id = $("#setting_id").val();
            var groupId = $("#setting_group").val();
            var name = $("#setting_name").val();
            var logo = $logo.val();
            if(name == ''){
                utils.tools.alert("请输入战队名", {timer: 1200, type: 'success'});
                return;
            }

            var data = {
                'id' : id,
                'name' : name,
                'groupId' : groupId,
                'logo' : logo
            }

            utils.postAjax(window.host+ "/team/saveInfo", data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                        $("#modal_setting").modal("hide");
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });

        });



        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });

        function initEvent() {

            $(".settingBtn").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/team/getInfo/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            $("#setting_id").val(role.id);
                            $("#setting_userName").html(role.userName);
                            $("#setting_name").val(role.name);
                            $("#setting_group").val(role.groupId);
                            $logo.val(role.logoUrl);
                            // 文件上传控件初始化
                            fileUploadManager.init();
                            $("#modal_setting").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            // 查看队员信息
            $(".viewMemberBtn").on("click",function() {
                var id = $(this).attr("rowId");
                $teamId = id;
                $memberdatatables.search('').draw();
                $("#modal_member").modal("show");
            });

            // 新增战队队员
            $(".setMemberBtn").on("click",function(){
                var id =  $(this).attr("rowId");
                $("#team_id").val(id);
                $("#team_keys").val('');
                $teamdatatables.search('').draw();
                $("#modal_select_members").modal("show");
            });


            // 启用
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认启用吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });



            // 禁用
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认禁用吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    disableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            // 推客设置等级
            $(".setLevelBtn").on('click', function () {
                var userId = $(this).attr('rowId');
                $("#set_level_user_id").val(userId);
                $("#modal_setlevel").modal('show');
            });

            $("#modal_update_pred").on('hidden.bs.modal', function() {
                $guiderUserTable.destroy();
            });



            function disableShop(id) {
                var url = window.host + "/team/disableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function enableShop(id) {
                var url = window.host + "/team/enableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }
        }

        // 选择队员，查询所有推客信息
        var $teamdatatables = $('#guiderTeamTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitterMember/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keys:$("#team_keys").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    sClass: 'sorting',
                    title: '推客',
                    data:'name',
                    name:'name',
                    sortable: true
                },{
                    width:200,
                    sClass: 'styled text-center sorting_disabled',
                    data:'phone',
                    name:'phone',
                    sortable: true
                },{
                    width:'75px',
                    sClass: 'sorting',
                    title: '等级',
                    data:'levelName',
                    name:'levelName',
                    sortable: true
                },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:75,
                    sClass: 'sorting',
                    data: 'archive',
                    name: 'archive',
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.archive;
                        if(value =='0'){
                            return "正常";
                        }else{
                            return "禁用";
                        }
                    }
                },{
                    width:150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        return '<a href="javascript:void(0);" class="setMemberBtn" rowId="'+row.id + '">选择</a>  ' ;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initMemberEvent();
            }
        });
        
        function initMemberEvent() {
            $(".setMemberBtn").on("click",function(){
                var userId =  $(this).attr("rowId");
                // 查询队员是否在此战队中，或在其他战队中
                $.ajax({
                    url: window.host + '/team/checkMember',
                    type: 'POST',
                    dataType: 'json',
                    data: {'userId':userId},
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            if(role){
                                utils.tools.alert("该商品已经设置过黑名单!", {timer: 3000, type: 'warning'});
                                return;
                            }else{
                                saveProductCommission(productId);
                            }
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            });
        }

        // 队员列表
        var $memberdatatables = $('#userMemberTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/team/teamMemberList", {
                    size: data.length,
                    page: (data.start / data.length),
                    teamId: $teamId,
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    title: '队员名称',
                    data:'userName',
                    name:'userName',
                    sortable: false
                },{
                    width:'100px',
                    title: '队员手机号',
                    data:'userPhone',
                    name:'userPhone',
                    sortable: false
                },
                {
                    width:'100px',
                    title: '加入时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                }],

            drawCallback: function () {  //数据加载完成
                //initLevelEvent();
            }
        });

});

