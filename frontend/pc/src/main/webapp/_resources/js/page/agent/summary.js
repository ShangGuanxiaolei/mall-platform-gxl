define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/userAgent/getSummary';
    var data = {
    };
    // 本周订单,代理,交易额,佣金数
    var amount;
    var commission;
    var order;
    var agent;
    var account_area;
    var commission_area;
    var zone_area;
    var zoneName;
    var zoneCount;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
            // 取代理概况中的今日数据
            $("#day_amount").html(result.data.info.amount);
            $("#day_commission").html(result.data.info.commission);
            $("#day_order").html(result.data.info.order);
            $("#day_agent").html(result.data.info.agent);

            // 取本周订单,代理,交易额,佣金数
            amount = result.data.nums.amount;
            commission = result.data.nums.commission;
            order = result.data.nums.order;
            agent = result.data.nums.agent;

            // 代理地区分布信息
            zoneName = result.data.zoneName;
            zoneCount = result.data.zoneCount;

            zone_area = echarts.init(document.getElementById('zone_area'), limitless);
            var zone_area_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 40,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['地区']
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: zoneName,
                    axisLabel :{
                        interval:0
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '人数',
                        type: 'bar',
                        data: zoneCount,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        }
                    }
                ]
            };
            zone_area.setOption(zone_area_options);

            account_area = echarts.init(document.getElementById('account_area'), limitless);
            var account_area_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['交易额'],
                    textStyle: {
                        fontSize: 16
                    }
                },

// Add toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    x: 'right',
                    y: 'center',
                    itemGap: 15,
                    padding: 0,
                    showTitle: false,
                    feature: {
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: '查看图表数据',
                            lang: ['查看图表数据', '关闭', '更新']
                        },
                        restore: {
                            show: true,
                            title: '刷新'
                        },
                        saveAsImage: {
                            show: true,
                            title: '另存为图片',
                            lang: ['保存']
                        }
                    }
                },
                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '交易额',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                            show: true,
                            textStyle: {
                                fontSize: 16
                            }
                        }}},
                        data: amount
                    }
                ]
            };
            account_area.setOption(account_area_options);


            commission_area = echarts.init(document.getElementById('commission_area'), limitless);
            var commission_area_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['佣金'],
                    textStyle: {
                        fontSize: 16
                    }
                },
// Add toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    x: 'right',
                    y: 'center',
                    itemGap: 15,
                    padding: 0,
                    showTitle: false,
                    feature: {
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: '查看图表数据',
                            lang: ['查看图表数据', '关闭', '更新']
                        },
                        restore: {
                            show: true,
                            title: '刷新'
                        },
                        saveAsImage: {
                            show: true,
                            title: '另存为图片',
                            lang: ['保存']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: [
                        '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
                    ]
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '佣金',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                            show: true,
                            textStyle: {
                                fontSize: 16
                            }
                        }}},
                        data: commission
                    }
                ]
            };
            commission_area.setOption(commission_area_options);


        }
    });






    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            zone_area.resize();
            account_area.resize();
            commission_area.resize();
        }, 200);
    }

});
