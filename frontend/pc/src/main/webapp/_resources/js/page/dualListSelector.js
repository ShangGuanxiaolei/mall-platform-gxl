define(['jquery', 'duallistbox'], function ($) {

  /**
   * 构造方法 构造一个dualListBox
   * @constructor
   */
  function Instance() {
    // 双选框实例
    this.$dualListInstance = null;

    // 双选框dom
    this.$dualListSelector = null;

    this.optionSupplier = null;

    this.defaultParams = {
      infoText: '未选中',
      infoTextEmpty: '已选中',
      filterPlaceHolder: '输入搜索关键字',
      infoTextFiltered: '4'
    };

    /**
     * selector 和 data初始化dualListBox
     * @param selector dom节点id或class
     * @param optionSupplier 函数，生成option
     * @param params 初始化参数
     */
    this.init = function (selector, optionSupplier, params) {
      var that = this;
      if (!this.$dualListSelector) {
        this.$dualListSelector = $(selector);
      }
      var options = optionSupplier();
      Array.prototype.forEach.call(options, function (option) {
        that.$dualListSelector.append(option);
      });
      // 覆盖默认配置
      if (params) {
        params = $.extend({}, this.defaultParams, params);
      }
      this.$dualListInstance = this.$dualListSelector.bootstrapDualListbox(params
          || this.defaultParams);
      return this;
    };

    this.refresh =  function (optionSupplier) {
      if (this.$dualListInstance) {
        this.$dualListSelector.empty();
        var options = optionSupplier();
        var that = this;
        Array.prototype.forEach.call(options, function (option) {
          that.$dualListSelector.append(option);
        });
        this.$dualListInstance.bootstrapDualListbox('refresh', true);
      } else {
        throw 'selector not initialized';
      }
    };

    this.isInitialized = function () {
      return this.$dualListInstance !== null;
    }

  }

  return {
    create: function () {
      return new Instance();
    }
  };


});
