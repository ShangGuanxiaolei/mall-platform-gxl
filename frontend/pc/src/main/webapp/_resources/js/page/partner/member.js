define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        var default_teamRate = 0;
        var default_shareholderRate = 0;
        var default_platformRate = 0

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-basic');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        $('.daterange-basic').val('');
        options.startDate = '';
        options.endDate = '';

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';
        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partnerMember/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#selectStatus ").val(),
                    partner_status: $("#partner_status").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    phone:$("#phone").val(),
                    shopName:$("#shopName").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "合伙人",
            },{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                title: "合伙人信息",
                render: function(data, type, row){
                    return '<a href="#" user-id="' + row.id + '" class="partnerChildrenBtn" data-toggle="modal" >' +row.phone+ ' <i class="icon-search4"></i></a>';
                }
            },{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                data: 'typeStr',
                name: 'typeStr',
                title: "合伙模式",
            },
                //    {
                //    width:45,
                //    sClass: 'text-center sorting_disabled',
                //    data: 'buyerId',
                //    name: 'buyerId',
                //    title: '等级'
                //},
                //    {
                //    width:85,
                //    sClass: 'text-center sorting_disabled',
                //    data: 'buyerId',
                //    name: 'buyerId',
                //    title: '会员账号'
                //},
                //{
                //    width:90,
                //    data: 'fee',
                //    name: 'fee',
                //    title: '已结算佣金',
                //    render: function(data, type, row){
                //        return '<a href="#">￥'+row.fee+'</a>';
                //    }
                //},
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:75,
                    sClass: 'sorting',
                    data: 'status',
                    name: 'status',
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if(value =='ACTIVE'){
                            return "正常";
                        }else{
                            return "禁用";
                        }
                    }
                },{
                    width:80,
                    sClass: 'sorting',
                    data: 'partner_status',
                    name: 'partner_status',
                    title: '申请状态',
                    render: function (data, type, row) {
                        var value = row.partner_status;
                        if(value =='ACTIVE'){
                            return "正常";
                        }else{
                            return "申请中";
                        }
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var str = '<a href="javascript:void(0);" class="setTypeBtn" style="margin-left: 10px;" rowId="'+row.id + '" userId="'+row.userId + '">设置</a></br> ' +
                            '<a href="javascript:void(0);" class="disableBtn" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id + '">禁用</a></br> ';
                        if(row.blackList == true){
                            str += '<a href="javascript:void(0);" class="viewBlackListBtn" style="margin-left: 10px;" rowId="'+row.id + '" userId="'+row.userId + '">黑名单商品</a> ';
                        }
                        return str;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        var userId =  null;
        //查看推客下级
        var $partnerChildrenTables = $('#partnerChildrenInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partnerMember/children", {
                    size: data.length,
                    page: (data.start / data.length),
                    userId : userId,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                data: 'name',
                name: 'name',
                width: 310,
                title: "下级店铺"

            },{
                title: "店铺状态",
                width: 150,
                data: 'archive',
                name: 'archive',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.archive;
                    if(value =='0'){
                        return "正常";
                    }else{
                        return "禁用";
                    }
                }
            }]
        })
        $(document).on('click', '.partnerChildrenBtn', function() {
            userId = $(this).attr("user-id");
            $('#modal_partnerChildrenInfo').modal('show');
            $partnerChildrenTables.search(userId).draw();
        });

        $('.exportPartnerChildrenBtn').on('click', function() {
            if (userId != null) {
                $('#exportPartnerChildrenForm input[name="userId"]').val(userId);
                $('#exportPartnerChildrenForm').submit();
            }
        });

        function initEvent() {

            $(".disableBtn").on("click", function () {
                var mId = $(this).attr("rowId");
                $("[data-toggle='popover']").popover({
                    trigger: 'click',
                    placement: 'left',
                    html: 'true',
                    animation: true,
                    content: function () {
                        var rowId = $(this).attr("rowId");
                        return '<span>确认禁用吗？</span>' +
                            '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                            '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                    }
                });

                $('[data-toggle="popover"]').popover() //弹窗
                    .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                        $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                    }).on('shown.bs.popover', function () {
                        var that = this;
                        $('.popover-btn-ok').on("click", function () {
                            var id = $(this).attr("mId");
                            disableShop(id);
                        });
                        $('.popover-btn-cancel').on("click", function () {
                            $(that).popover("hide");
                        });
                    });
                //给Body加一个Click监听事件
                $('body').on('click', function (event) {
                    var target = $(event.target);
                    if (!target.hasClass('popover') //弹窗内部点击不关闭
                        && target.parent('.popover-content').length === 0
                        && target.parent('.popover-title').length === 0
                        && target.parent('.popover').length === 0
                        && target.data("toggle") !== "popover") {
                        //弹窗触发列不关闭，否则显示后隐藏
                        $('[data-toggle="popover"]').popover('hide');
                    } else if (target.data("toggle") == "popover") {
                        target.popover("toggle");
                    }
                });

            });




            function disableShop(id) {
                var url = window.host + "/partnerMember/disableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            // 设置合伙人类型
            $(".setTypeBtn").on("click", function () {
                var userId = $(this).attr("userId");
                $("#type_user_id").val(userId);
                $.ajax({
                    url: host + '/partner/getTypeSet',
                    type: 'POST',
                    data: {'userId' : userId},
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var platformType = data.data.platformType;
                            var teamType = data.data.teamType;
                            var shareholderType = data.data.shareholderType;
                            if(platformType && platformType != null){
                                $("#platformSet").show();
                                $("#platform_type").val(platformType.typeId == '0' ? '' : platformType.typeId);
                                $("#platformCheckbox").prop("checked",true);
                            }else{
                                $("#platformSet").hide();
                                $("#platform_type").val('');
                                $("#platformCheckbox").prop("checked",false);
                            }

                            if(teamType && teamType != null){
                                $("#teamSet").show();
                                $("#team_type").val(teamType.typeId == '0' ? '' : teamType.typeId);
                                $("#teamCheckbox").prop("checked",true);
                            }else{
                                $("#teamSet").hide();
                                $("#team_type").val('');
                                $("#teamCheckbox").prop("checked",false);
                            }

                            if(shareholderType && shareholderType != null){
                                $("#shareholderSet").show();
                                $("#shareholder_type").val(shareholderType.typeId == '0' ? '' : shareholderType.typeId);
                                $("#shareholderCheckbox").prop("checked",true);
                            }else {
                                $("#shareholderSet").hide();
                                $("#shareholder_type").val('');
                                $("#shareholderCheckbox").prop("checked",false);
                            }

                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                            $("#modal_setType").modal("show");
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tools.goLogin();
                        } else {
                            alert('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            });


            // 查看黑名单商品列表信息
            $(".viewBlackListBtn").on("click", function () {
                userId = $(this).attr("userId");
                $selectproductdatatables.search('').draw();
                $("#modal_select_products").modal("show");
            });

        }


        var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/productBlack/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    userId: userId
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productImg+'"><img class="goods-image" src="'+row.productImg+'" /></a>';
                    }
                },
                {
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"productName"
                }, {
                    data: "productPrice",
                    width: "50px",
                    orderable: true,
                    name:"productPrice"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                }
            ],
            drawCallback: function () {  //数据加载完成

            }
        });


        // 类型勾选框选中取消后需要同步隐藏类型设置框
        $("#teamCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#teamSet").show();
            }else{
                $("#teamSet").hide();
            }
        });
        $("#platformCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#platformSet").show();
            }else{
                $("#platformSet").hide();
            }
        });
        $("#shareholderCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#shareholderSet").show();
            }else{
                $("#shareholderSet").hide();
            }
        });

        // 保存合伙人类型设置
        $(".saveSetTypeBtn").on('click', function () {
            var data = {};
            var typeList = [];
            if ($('#teamCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#team_type").val();
                var type = 'TEAM';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if ($('#platformCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#platform_type").val();
                var type = 'PLATFORM';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if ($('#shareholderCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#shareholder_type").val();
                var type = 'SHAREHOLDER';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if(typeList.length == 0){
                utils.tools.alert("请至少选择一种合伙人模式!", {timer: 1200, type: 'warning'});
                return;
            }
            data.userId = $("#type_user_id").val();
            data.types = JSON.stringify(typeList);
            $.ajax({
                url: host + '/partner/saveTypeSet',
                type: 'POST',
                data: data,
                dataType:"json",
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_setType").modal('hide');
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        });


        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });


        // 团队类型
        var $teamdatatables = $('#teamTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'TEAM',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        // 股东类型
        var $shareholderdatatables = $('#shareholderTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'SHAREHOLDER',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        // 平台类型
        var $platformdatatables = $('#platformTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'PLATFORM',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        function initTypeEvent() {
            $(".typeedit").on("click",function(){
                var id =  $(this).attr("rowId");
                var type = $(this).attr("rowType");
                $.ajax({
                    url: window.host + '/partner/getType/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;

                            $("#typeId").val(role.id);
                            $("#type").val(role.type);
                            $("#name").val(role.name);
                            $("#rate").val(role.rate);

                            var default_rate = 0;
                            if(type == 'PLATFORM'){
                                default_rate = default_platformRate;
                            }else if(type == 'TEAM'){
                                default_rate = default_teamRate;
                            }else if(type == 'SHAREHOLDER'){
                                default_rate = default_shareholderRate;
                            }
                            $("#rateBase").html(default_rate);
                            if(role.rate && role.rate != ''){
                                var total = Number(role.rate) * Number(default_rate);
                                $("#rateTotal").html(total.toFixed(2));
                            }else{
                                $("#rateTotal").html('');
                            }


                            $("#modal_addType").modal("show");
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteType(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增团队类型
        $(document).on('click', '.addTeamType', function() {
            $("#typeId").val('');
            $("#type").val('TEAM');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_teamRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 新增股东类型
        $(document).on('click', '.addShareholderType', function() {
            $("#typeId").val('');
            $("#type").val('SHAREHOLDER');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_shareholderRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 新增平台类型
        $(document).on('click', '.addPlatformType', function() {
            $("#typeId").val('');
            $("#type").val('PLATFORM');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_platformRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 佣金比例变化时，对应的总和也需要一起变化
        $("#rate").on('input',function(e){
            var rate = $("#rate").val();
            var rateBase = $("#rateBase").html();
            if(rate && rate != ''){
                var total = Number(rate) * Number(rateBase);
                $("#rateTotal").html(total.toFixed(2));
            }else{
                $("#rateTotal").html('');
            }
        });


        function deleteType(id) {
            $.ajax({
                url: host + '/partner/deleteType/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $teamdatatables.search('').draw();
                        $shareholderdatatables.search('').draw();
                        $platformdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        // 保存推客等级
        $(".saveTypeBtn").on('click', function() {
            var id = $("#typeId").val();
            var name = $("#name").val();
            var type = $("#type").val();
            var rate = $("#rate").val();


            if(!name || name == '' ){
                utils.tools.alert("请输入等级名称!", {timer: 1200, type: 'warning'});
                return;
            }else if(!rate || rate == ''){
                utils.tools.alert("请输入分红系数!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                id: id,
                name: name,
                type: type,
                rate: rate
            };
            $.ajax({
                url: host + '/partner/saveType',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_addType").modal('hide');
                        $teamdatatables.search('').draw();
                        $shareholderdatatables.search('').draw();
                        $platformdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        // 获取合伙人设置中默认设置的各种类型的分红比例
        $.ajax({
            url: window.host + '/partner/getDefaultCommission',
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.errorCode == 200) {
                    var role = data.data;
                    if(role){
                        default_teamRate = role.team.rate;
                        default_shareholderRate = role.shareholder.rate;
                        default_platformRate = role.platform.rate;
                    }

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    });