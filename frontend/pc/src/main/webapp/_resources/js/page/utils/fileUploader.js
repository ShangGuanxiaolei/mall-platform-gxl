define(['jquery', 'utils', 'dropzone'], function ($, utils) {

  Dropzone.autoDiscover = false;
  const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

  /**
   * 新建dropZone封装实例
   * @param params.onSuccess 成功后的回调函数，返回地址
   * @param params.dom drop的dom节点
   * @param params.url 上传文件地址，不指定则使用默认的
   * @param params.allowTypes 允许上传的文件类型
   * @param params.maxSize 最大上传大小，单位MB
   * @param params.onRemove 客户端移除文件事件
   * @param params.onError 出错时回调
   * @param params.maxFiles 最大文件数量
   * @param params.onProcess 上传时事件
   * @param params.onComplete 上传完成时事件
   * @constructor
   */
  function Uploader(params) {
    /* jquery 的dropZone实例 */
    this.jDropInstance = $(params.dom).dropzone({
      url: params.url || defaultUrl,
      paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
      dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
      maxFilesize: params.maxSize || 2, // MB      //最大文件大小，单位是 MB
      maxFiles: params.maxFiles||10,               //限制最多文件数量
      maxThumbnailFilesize: 10,
      addRemoveLinks: true,
      thumbnailWidth: "150",      //设置缩略图的缩略比
      thumbnailHeight: "150",     //设置缩略图的缩略比
      acceptedFiles: params.allowTypes || ".gif,.png,.jpg",
      uploadMultiple: false,
      dictInvalidFileType: "文件格式错误:建议文件格式:" + params.allowTypes
      || "gif, png, jpg",//文件类型被拒绝时的提示文本。
      dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
      dictRemoveFile: "删除",                                        //移除文件链接的文本
      dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
      dictResponseError: "服务器暂无响应,请稍后再试!",
      dictCancelUpload: "取消上传",
      dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
      dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
      //autoProcessQueue: false,
      init: function () {

        // var imgDropzone = this;
        //添加了文件的事件
        this.on("addedfile", function (file) {
          if (file && file.dataImg && file.previewElement) { //是网络加载的数据
            $(file.previewElement).attr("data-img", file.dataImg);
            if (file.size === '' || file.length === 0) {
              $(file.previewElement).find(".dz-details").hide();
            }
          }
          if (params.onProcess) {
            params.onProcess();
          }
          //imgDropzone.processQueue();
        });
        this.on("success", function (file, data) {
          if (typeof(data) === 'object') {
            switch (data.errorCode) {
              case 200: {
                if (typeof(data.data) === 'object') {
                  if(data.data[0]) {
                    const resData = data.data[0];
                    const imgId = resData.id;
                    if (file && file.previewElement) {
                      $(file.previewElement).attr("data-img", imgId);
                      if (params.onSuccess) {
                        resData.name = file.name;
                        resData.size = file.size;
                        console.log(file);
                        console.log(resData);
                        params.onSuccess(resData);
                      }
                    }
                  }else {
                      if (file && file.previewElement) {
                        if (params.onSuccess) {
                          params.onSuccess(data.data);
                        }
                      }
                    }
                  }
                break;
              }
              default: {
                utils.tools.alert("文件上传失败,请重新选择!",
                    {timer: 1200, type: 'warning'});
                break;
              }
            }
          } else {
            if (data === -1) {
              utils.tools.alert("文件上传失败,请重新选择!",
                  {timer: 1200, type: 'warning'});
            }
          }
        });

        this.on("error", function (file, message) {
          if (params.onError) {
            params.onError(message);
          } else {
            $(".dz-error-message").html(message);
          }
        });

        // 上传完成事件
        this.on("complete", function () {
          if (params.onComplete) {
            params.onComplete();
          }
        });

        this.on("thumbnail", function (file) {
          if (file && file.previewTemplate) {
            file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
            file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
          }
          const $dzImage = $('.dz-image');
          $dzImage.css("height", "150px;");
          $dzImage.css("width", "150px;");
        });

        this.on('removedfile', function (file) {
          if (params.onRemove) {
            params.onRemove(file);
          }
        });

      }
    });

    /* 真实的dropZone实例 */
    this.dropZone = this.jDropInstance[0].dropzone;

    /**
     * 清空dropZone的上传文件
     */
    this.clear = function () {
      this.dropZone.removeAllFiles(true);
      return this;
    };

    /**
     * 添加图片预览到dropZone
     * @param imgUrl 图片地址
     * @param name 文件名
     * @param size 文件大小
     */
    this.addImage = function (imgUrl, name = '', size = '') {
      if (imgUrl) {
        const mockFile = {name: name, size: size, dataImg: imgUrl};
        this.dropZone.emit("addedfile", mockFile);
        this.dropZone.emit("thumbnail", mockFile, imgUrl);
        this.dropZone.emit("complete", mockFile);
        this.dropZone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
      }
      return this;
    };

    this.destroy = function () {
      this.dropZone.destroy();
    };

    this.disable = function () {
      this.dropZone.disable();
    };

    this.enable = function () {
      this.dropZone.enable();
    };

    this.disableClick = function () {
      $(".dz-hidden-input").prop("disabled", true);
    };

    this.enableClick = function () {
      $(".dz-hidden-input").prop("disabled", false);
    }
  }

  return {

    /**
     * 新建dropZone封装实例
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @param params.allowTypes 允许上传的文件类型
     * @param params.maxSize 最大上传大小，单位MB
     * @param params.onRemove 客户端移除文件事件
     * @param params.onError 出错时回调
     * @param params.onProcess 上传时事件
     * @param params.onComplete 上传完成时事件
     * @constructor
     */
    create: function (params) {
      return new Uploader(params);
    }
  }

});