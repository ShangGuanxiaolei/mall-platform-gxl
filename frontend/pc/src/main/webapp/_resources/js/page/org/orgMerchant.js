/**
 * Created by wangxinhua on 17-3-22.
 */

define(['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, datatabels, blockui, select2, tree, fileinput_zh, fileinput) {

    /** 表格列名数组,根据索引确定排序 **/
    /** 第一个位置存放默认值 **/
    var $columns = ['name', 'loginname', 'name', 'phone', 'orgName', 'createdAt', 'rolesDesc'];

    var $listUrl = '/sellerpc/org/listMerchant';

    var merchantId = '';

    var $userId = null;

    /** jsTree当前选中的节点 **/
    var $selected = {
        'id': null,
        'parent': '#'
    };

    initPage();

    /** 初始化表格数据 **/
    var $merchant_table = utils.createDataTable('#xquark_merchant_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length) +1,
                orgId: $selected.id,
                pageable: true,
                keyword: data.search.value,
            }, function (res) {
                if (res.data) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    if (res.data.orgName) $selected.orgName = res.data.orgName;
                    $userId = res.data.userId;
                } else {
                    utils.tools.alert('数据加载失败');
                }
                callback({
                    recordsTotal: res.data.merchantTotal,
                    recordsFiltered: res.data.merchantTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.merchantTotal,
                    iTotalDisplayRecords: res.data.merchantTotal,
                });
            })
        },
        rowId: 'id',
        columns: [
            {
              title: "账号",
                orderable: false,
                render: function (data, type, row) {
                    return row.loginname;
                }
            },
            {
              title: "昵称",
                data: 'name',
                orderable: true,
                name: 'name'
            },
            {
              title: "手机号",
                orderable: false,
                render: function (data, type, row) {
                    return row.phone;
                }
            },
            {
              title: "创建时间",
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt == null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
              title: "角色",
                orderable: false,
                render: function (data, type, row) {
                    return row.rolesDesc && row.rolesDesc != '' ? row.rolesDesc : '无';
                }
            },
            {
                sClass: "right",
                width: "140px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-right: 10px;" rowId="'+row.id+'" fid="edit_merchant"><i class="icon-pencil7" ></i>编辑</a>';
                  if ($userId && $userId != row.id && !row.admin) {
                        html += '<a href="javascript:void(0);" class="settings role_check_table"  style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_merchant_role"><i class="icon-checkbox-checked"></i>角色</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_merchant"><i class="icon-trash" ></i>删除</a>';
                    }
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    // 选择角色表单
    var $roledatatables = $('#xquark_select_roles_tables').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get("/sellerpc/merchantRole/allList", {
                keyword: '',
                keyword: data.search.value,
                merchantId: merchantId,
                pageable: false
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    // if (res.data.shopId) {
                    //     $shopId = res.data.shopId;
                    // }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "15px",
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.roleName+'"></label>';
                    return str;
                }
            },
            {
                data: "roleName",
                width: "120px",
                orderable: false,
                name:"roleName"
            },{
                data: "roleDesc",
                width: "120px",
                orderable: false,
                name:"roleDesc"
            }
            , {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
        }
    });

    function initEvent() {
        $('body').unbind('click');

        $('.move').on('click', function () {
            var id = $(this).attr('rowId');
            var orgId = $(this).attr('orgId');
            var orgName = $(this).attr('orgName');
            $('#currentOrg').val(orgName != null && orgName != 'null' ? orgName : '无');
            $('#merchantId').val(id);
            $('#oldOrgId').val(orgId);
            $('#modal_merchant_update').modal('show');
        });

        $(".edit").on("click",function(){
            var id =  $(this).attr("rowId");
            window.location.href="/sellerpc/merchant/admin/edit?merchantId="+id;
        });

        $(".settings").on("click",function(){
            merchantId =  $(this).attr("rowId");
            $roledatatables.search('').draw();
            $("#modal_select_roles").modal("show");
            // 全选
            $("#checkAllRoles").on('click', function() {
                $("[name=checkRoles]:checkbox").prop('checked', $(this).prop('checked'));
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var param = $(this).attr("param");
                deleteMerchant(param);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_merchant_tables');

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") == "popover") {
                target.popover("toggle");
            }
        });
    }

    var jstree = $('#tree_merchant');
    jstree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': window.host + '/org/listTree',
                'data': function (node) {
                    if (node == null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        jstree.jstree("select_node", "ul > li:first");
        var selectedNode = jstree.jstree("get_selected");
        jstree.jstree('open_node', selectedNode, false, true);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        $selected = data.node;
        $merchant_table.search('').draw();
    }

    /**
     * ajax更新部门信息
     * @param url
     * @param id
     * @param org_id
     */
    function updateMerchantOrg(id, orgId, callback) {
        var url = window.host + '/org/updateMerchantOrg';
        var params = {
            'merchantId': id,
            'orgId': orgId ? orgId : '0'
        };
        $.ajax(url, {
            'type': 'post',
            'dataType': 'json',
            'cache': false,
            'data': params,
            'timeout': 1000 * 10
        }).done(function (result) {
            if (result.data == null) {
                utils.tools.alert(result.moreInfo);
                return;
            }
            if (callback) callback(result);
        }).fail(function (e) {
            Metronic.unblockUI();
            utils.tools.alert("亲出错了，请稍后再试");
        });
    }

    /** 初始化页面信息 **/
    function initPage() {

        $(".btn-admin-search").on('click', function() {
            var keyword = $.trim($("#sKeyword").val());
            $merchant_table.search( keyword ).draw();
        });

        $(".btn-admin-add").on('click', function() {
            location.href = '/sellerpc/merchant/admin/add';
        });

        buttonRoleCheck('.hideClass');

        // 初始化部门选择框
        $.ajax({
            url: window.host + '/org/list',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.errorCode == 200) {
                    var dataLength = data.data.orgTotal;
                    var orgNames = $('#orgNames');
                    orgNames.empty();
                    orgNames.append('<option selected="selected">请选择新部门</option>');
                    for (var i = 0; i < dataLength; i++) {
                        orgNames.append('<option value=' + data.data.list[i].id + '>' + data.data.list[i].name + '</option>');
                    }
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                }
            },
            error: function (state) {
                if (state.status == 401) {
                } else {
                    utils.tools.alert('获取部门信息失败！', {timer: 1200, type: 'warning'});
                }
            }
        });

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "部门下暂无用户"
            }
        });

        $('#update_merchant_btn').on('click', function () {
            var merchantId = $('#merchantId').val();
            var oldOrgId = $('#oldOrgId').val();
            var newOrgId = $('#orgNames').val();
            if (newOrgId == '请选择新部门') {
                utils.tools.alert('请先选择部门');
                return;
            }
            if (newOrgId == oldOrgId) {
                utils.tools.alert('用户已经在该部门中');
                return;
            }
            updateMerchantOrg(merchantId, newOrgId, function (data) {
                if (data.errorCode == '200') {
                    utils.tools.alert('操作成功');
                    $merchant_table.search('').draw();
                    $('#modal_merchant_update').modal('hide');
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200});
                }
            })
        });

        // 管理员选择角色确认
        $(".changeRoleBtn").on('click', function() {
            var roles = "";
            $("[name=checkRoles]:checkbox").each(function(){ //遍历table里的全部checkbox
                //如果被选中
                if($(this).prop("checked")){
                    if(roles == ''){
                        roles = $(this).val();
                    }else{
                        roles = roles + ',' + $(this).val();
                    }
                }
            });
            var url = "/sellerpc/merchantRole/update";
            var data = {
                merchantId: merchantId,
                roles: roles
            };
            utils.postAjax(url,data,function(res){
                if(typeof(res) === 'object'){
                    $("#modal_select_roles").modal("hide");
                    if (res.rc == 1){
                        utils.tools.alert('设置成功',{timer: 1200, type: 'success'});
                        $merchant_table.search('').draw();
                    } else{
                        utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                    }
                }
            });
        });
    }

    function  deleteMerchant(paramId){
        var url = "/sellerpc/merchant/admin/delete";
        var data = {
            merchantId: paramId
        };
        utils.postAjax(url,data,function(res){
            if(typeof(res) === 'object'){
                if (res.rc == 1){
                    $merchant_table.search('').draw();
                    utils.tools.alert('删除成功');
                } else{
                    utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                }
            }
        });
    }

});
