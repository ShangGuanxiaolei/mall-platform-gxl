/**
 * Created by chh on 16/12/13.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/ruleProduct/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    var $order = '';

    var $productlistUrl = window.host + "/product/list";

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#productId").val('');
        $("#ruleId").val('');
        $("#ruleName").val('');
        $("#productName").val('');
        $("#extConditions").val('');
        $("#priority").val('');
        $("#commissionFee").val('');
        $("#commissionRate").val('');
        $("#modal_component").modal("show");

    });

    buttonRoleCheck('.hideClass');

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productImgUrl+'"><img class="goods-image" src="'+row.productImgUrl+'" /></a>';
                }
            },
            {
                data: "productName",
                width: "120px",
                orderable: false,
                name:"productName"
            },{
                data: "productPrice",
                width: "100px",
                orderable: false,
                name:"productPrice"
            },{
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.ruleName)
                    {
                        case 'GENERAL_DIRECT_TO_DIRECTOR':
                            status = '董事的直接下级总顾问订单分润给董事';
                            break;
                        case 'GENERAL_INDIRECT_TO_DIRECTOR':
                            status = '董事的间接下级总顾问订单分润给董事';
                            break;
                        case 'DIRECTORS_TEAM_TO_DIRECTORS':
                            status = '董事下属团队(包括董事本身)订单需要分润给上级和上上级董事';
                            break;
                        case 'GENERAL_DIRECT_TO_GENERAL':
                            status = '总顾问直接推荐一位总顾问一次性返利';
                            break;
                        case 'TEAM_ORDER':
                            status = '团队订单返利';
                            break;
                        case 'AREA_ORDER':
                            status = '区域订单返利';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.policyName)
                    {
                        case 'DEFAULT':
                            status = '默认设置分佣规则集';
                            break;
                        case 'USER_AGENT_DEFAULT':
                            status = '代理商默认分佣规则集';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                data: "extConditions",
                width: "100px",
                orderable: false,
                name:"extConditions"
            },{
                data: "priority",
                width: "100px",
                orderable: false,
                name:"priority"
            }, {
                data: "commissionFee",
                width: "100px",
                orderable: false,
                name:"commissionFee"
            },
            {
                data: "commissionRate",
                width: "100px",
                orderable: false,
                name:"commissionRate"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_product_price_rule"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_product_price_rule"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($productlistUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 4){
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                category : '',
                isGroupon : ''
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords:res.data.categoryTotal,
                    iTotalDisplayRecords:res.data.categoryTotal
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: false,
                name:"price"
            }, {
                data: "amount",
                orderable: false,
                width: "50px",
                name:"amount"
            },
            {
                data: "sales",
                orderable: false,
                width: "50px",
                name:"sales"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'+row.id+'" productName="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectProductEvent();
        }
    });

    var $selectruledatatables = $('#xquark_select_rules_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/rule/list", {
                size: data.length,
                page: (data.start / data.length),
                keyword: '',
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.ruleName)
                    {
                        case 'GENERAL_DIRECT_TO_DIRECTOR':
                            status = '董事的直接下级总顾问订单分润给董事';
                            break;
                        case 'GENERAL_INDIRECT_TO_DIRECTOR':
                            status = '董事的间接下级总顾问订单分润给董事';
                            break;
                        case 'DIRECTORS_TEAM_TO_DIRECTORS':
                            status = '董事下属团队(包括董事本身)订单需要分润给上级和上上级董事';
                            break;
                        case 'GENERAL_DIRECT_TO_GENERAL':
                            status = '总顾问直接推荐一位总顾问一次性返利';
                            break;
                        case 'TEAM_ORDER':
                            status = '团队订单返利';
                            break;
                        case 'AREA_ORDER':
                            status = '区域订单返利';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },
            {
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.policyName)
                    {
                        case 'DEFAULT':
                            status = '默认设置分佣规则集';
                            break;
                        case 'USER_AGENT_DEFAULT':
                            status = '代理商默认分佣规则集';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },
            {
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                data: "index",
                width: "80px",
                orderable: false,
                name:"index"
            }
            , {
                data: "params",
                width: "120px",
                orderable: false,
                name:"params"
            }, {
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectrule" style="margin-left: 10px;" rowId="'+row.id+'" ruleName="'+row.ruleName+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initRuleEvent();
        }
    });

    function initRuleEvent(){
        $(".selectrule").on("click",function(){
            var ruleId =  $(this).attr("rowId");
            var ruleName =  $(this).attr("ruleName");
            $("#ruleId").val(ruleId);
            $("#ruleName").val(ruleName);
            $("#modal_select_rules").modal("hide");
        });
    }

    function initSelectProductEvent(){
        $(".selectproduct").on("click",function(){
            var productId =  $(this).attr("rowId");
            var productName =  $(this).attr("productName");
            $("#productId").val(productId);
            $("#productName").val(productName);
            $("#modal_select_products").modal("hide");
        });
    }

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           $.ajax({
                url: window.host + '/ruleProduct/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#productId").val(role.productId);
                        $("#ruleId").val(role.commissionRuleId);
                        $("#productName").val(role.productName);
                        $("#ruleName").val(role.ruleName);

                        $("#extConditions").val(role.extConditions);
                        $("#priority").val(role.priority);
                        $("#commissionFee").val(role.commissionFee);
                        $("#commissionRate").val(role.commissionRate);
                        $("#modal_component").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
           });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/ruleProduct/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存
    $(".saveBtn").on('click', function() {
        var url = window.host + '/ruleProduct/save';
        var id = $("#id").val();
        var ruleId = $("#ruleId").val();
        var productId = $("#productId").val();
        var priority = $("#priority").val();
        var extConditions = $("#extConditions").val();
        var commissionFee = $("#commissionFee").val();
        var commissionRate = $("#commissionRate").val();

        if(!ruleId || ruleId == '' ){
            utils.tools.alert("请选择佣金规则!", {timer: 1200, type: 'warning'});
            return;
        }else if(!productId || productId == ''){
            utils.tools.alert("请选择商品!", {timer: 1200, type: 'warning'});
            return;
        }else if(!priority || priority == ''){
            utils.tools.alert("请输入优先级!", {timer: 1200, type: 'warning'});
            return;
        }else if( (!commissionFee || commissionFee == '') && (!commissionRate || commissionRate == '')){
            utils.tools.alert("佣金金额，佣金比例不能同时为空!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            commissionRuleId: ruleId,
            productId: productId,
            priority: priority,
            extConditions: extConditions,
            commissionFee: commissionFee,
            commissionRate: commissionRate
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/ruleProduct/list';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });

    // 选择活动商品
    $("#productName").on('focus', function() {
        $selectproductdatatables.search('').draw();
        $("#modal_select_products").modal("show");
    });

    // 选择角色
    $("#ruleName").on('focus', function() {
        $selectruledatatables.search('').draw();
        $("#modal_select_rules").modal("show");
    });

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            $productlistUrl = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }
    });


});