/**
 * 封装日期控件
 */
define(['jquery', 'daterangepicker', 'moment'], function ($) {

  /** 初始化日期控件 **/
  const options = {
    timePicker: true,
    dateLimit: {days: 60000},
    startDate: moment().subtract(0, 'month').startOf('month'),
    endDate: moment().subtract(0, 'month').endOf('month'),
    autoApply: false,
    timePickerIncrement: 1,
    locale: {
      format: 'YYYY/MM/DD',
      separator: ' - ',
      applyLabel: '确定',
      fromLabel: '开始日期:',
      toLabel: '结束日期:',
      cancelLabel: '清空',
      weekLabel: 'W',
      customRangeLabel: '日期范围',
      daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月",
        "11月", "12月"],
      firstDay: 6
    },
    ranges: {
      '今天': [moment(), moment().endOf('day')],
      '一星期': [moment(), moment().add(6, 'days')],
      '一个月': [moment(), moment().add(1, 'months')],
      '一年': [moment(), moment().add(1, 'year')]
    },
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small btn-default'

  };

  /* 单时间控件 */
  const singlePickerOptions = {
    singleDatePicker: true,
    showDropdowns: true
  };

  /**
   * 构造函数，创建一个dateRangePicker
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function DatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(options, function (start, end) {
      if (start._isValid && end._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm') + ' - '
            + end.format(
                'YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      console.log(picker.endDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      options.endDate = picker.endDate;
      params.onApply(picker.startDate.format('YYYY-MM-DD HH:mm'),
          picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
      //do something, like clearing an input
      $(this).val('');
      params.onEmpty();
    });

  }

  /**
   * 构造函数， 构造单时间控件
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function SingleDatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(singlePickerOptions, function (start) {
      if (start._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      var dateStr = picker.startDate.format('YYYY-MM-DD HH:mm');
      if (params.onApply) {
        params.onApply(dateStr);
      } else {
        $(this).val(dateStr)
      }
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function () {
      //do something, like clearing an input
      $(this).val('');
      if (params.onEmpty) {
        params.onEmpty();
      }
    });
  }

  /**
   * 原型，所有类型日期控件共有
   * @type {{resetStr: resetStr}}
   */
  const commonPrototype = {
    /**
     * 重置日期字符串
     * @param str
     */
    resetStr: function (str) {
      this.$dateRangeBasic.val(str);
    }
  };

  /**
   * 继承commonPrototype
   * @type {{resetStr: resetStr}}
   */
  DatePicker.prototype = SingleDatePicker.prototype = commonPrototype;

  return {

    /**
     * 初始化日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createPicker: function (params) {
      return new DatePicker(params);
    },

    /**
     * 初始化单日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createSinglePicker: function (params) {
      return new SingleDatePicker(params);
    }

  }

});
