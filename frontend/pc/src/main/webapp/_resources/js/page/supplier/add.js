define(['jquery', 'validate', 'utils', 'jquerySerializeObject', 'select2',
      'uniform'],
    function ($, validate, utils) {
      const moduleName = 'supplier';
      const detailUrl = id => window.host + `/${moduleName}/${id}`;
      const saveUrl = window.host + `/${moduleName}/edit?type=override`;
      const HOUSE_LIST_URL = window.host + '/warehouse/list';

      const addButton = $(`#add-${moduleName}`);
      const select = $("#houseList");
      const form = $(`#${moduleName}-form`);
      const modal = $(`#modal_add_${moduleName}`);
      const submitButton = $(`#submit-${moduleName}`);
      let updateTable = '';
      let isSubmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();
        addButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitForm = function (form) {
        if (isSubmiting === true) {
          return;
        }
        if (!form.id) {
          delete form.id;
        }
        utils.postAjaxJson(saveUrl, form, date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        });
        isSubmiting = false;
      };

      const edit = function (id) {
        $.get(detailUrl(id)).done(date => {
          let obj = date.data;
          utils.tools.syncForm(form, obj);
          let houses = obj.wareHouseList;
          if (houses) {
            let selected = [];
            houses.forEach(element => {
              selected.push(element.warehouseId);
            });
            select.val(selected);
            select.trigger('change');
          }
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              form[0].reset();
              select.val(null).trigger('change');
              form.validate().resetForm();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = form.valid();
              if (result) {
                console.log("success valid");
                let obj = form.serializeObject();
                let houses = select.val();
                delete obj.houseList;
                obj.wareHouseList = [];
                if (houses) {
                  houses.forEach(function (element) {
                    obj.wareHouseList.push({
                      warehouseId: element,
                    })
                  });
                }
                submitForm(obj);
              }
            });
            return this;
          },
          initValidate: function () {

            form.validate({
              rules: {
                name: {
                  required: true,
                },
                code: {
                  required: true,
                },
              },
              messages: {
                name: {
                  required: "请输入供应商名称",
                },
                code: {
                  required: "请输入供应商编码",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
          initSelect: function () {
            //获取仓库数据
            $.get(HOUSE_LIST_URL, {
              offset: 0,
              size: 1000,
            }).done(function (data) {
              let results = [];
              let houses = data.data.list;
              for (let i = 0; i < houses.length; i++) {
                let user = {
                  id: houses[i].id,
                  text: houses[i].name,
                };
                results.push(user);
              }
              //初始化select2
              select.select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            }).fail(function () {
              utils.tools.alert("获取仓库数据出错");
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initValidate().initSubmit().initSelect().resetForm();

      return {
        edit: edit,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });
