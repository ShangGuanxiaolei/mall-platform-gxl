/**
 * Created by chh on 16/10/13.
 */
define(['jquery','utils','datatables', 'moment', 'blockui','bootbox', 'select2', 'daterangepicker', 'product/upload'], function($,utils,datatabels, moment, blockui,bootbox, select2,daterangepicker,uploader) {

    var $listUrl = window.host + "/promotionGroupon/list";

    var $productlistUrl = window.host + "/product/list";

    var $shopId = null;

    var $rowId = '';

    var $promotionid = '';

    var $category = '';

    var $orders = ['price','amount','sales','onsale'];

    var $order = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 初始化日期控件 **/
    var options = {
        timePicker: true,
        dateLimit: { days: 60000 },
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        autoApply: false,
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY/MM/DD',
            separator: ' - ',
            applyLabel: '确定',
            fromLabel: '开始日期:',
            toLabel: '结束日期:',
            cancelLabel: '清空',
            weekLabel: 'W',
            customRangeLabel: '日期范围',
            daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
            monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
            firstDay: 6
        },
        ranges: {
            '今天': [moment(), moment().endOf('day')],
            '一星期': [moment(), moment().add(6, 'days')],
            '一个月': [moment(), moment().add(1, 'months')],
            '一年': [moment(), moment().add(1, 'year')]
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
    };
    var $dateRangeBasic = $('.daterange-time');
    $dateRangeBasic.daterangepicker(options, function (start, end) {
        if (start._isValid && end._isValid) {
            $dateRangeBasic.val(start.format('YYYY-MM-DD HH:mm') + ' - ' + end.format('YYYY-MM-DD HH:mm'));
        } else {
            $dateRangeBasic.val('');
        }
    });

    /** 回调 **/
    $dateRangeBasic.on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.startDate = picker.startDate;
        options.endDate   = picker.endDate;
        $("#valid_from").val(picker.startDate.format('YYYY-MM-DD HH:mm'));
        $("#valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    /**
     * 清空按钮清空选框
     */
    $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
        //do something, like clearing an input
        $dateRangeBasic.val('');
        $("#valid_from").val('');
        $("#valid_to").val('');
    });

    //初始化商品分类信息
    $.ajax({
        url: window.host + '/shop/category/list',
        type: 'POST',
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.errorCode == 200) {
                var dataLength = data.data.length;
                for (var i = 0; i < dataLength; i++) {
                    $("#categoryType").append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
                };
            } else {
                utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
            }
        },
        error: function (state) {
            if (state.status == 401) {
            } else {
                utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
            }
        }
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增活动
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#title").val('');
        $("#name").val('');
        $("#details").val('');
        $('.daterange-time').val('');
        $("#modal_diamond").modal("show");
    });

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                keywords: '',
                pageable: true,
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'+row.id+'"></label>';
                }
            },
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.img+'" />';
                }
            },
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"validFrom"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"validTo"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.archive)
                    {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" name="'+row.name+'" img="'+row.img+'" originImg="'+row.originImg+'" details="'+row.details+'" validFrom="'+row.validFrom+'" validTo="'+row.validTo+'" title="'+row.title+'" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="products" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7"></i>商品管理</a>';
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>结束</a>';

                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $productdatatables = $('#xquark_list_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/promotionGroupon/product/list/" + $promotionid, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.productImg+'" />';
                }
            },
            {
                data: "productName",
                width: "120px",
                orderable: false,
                name:"title"
            }, {
                data: "productPrice",
                width: "50px",
                orderable: false,
                name:"price"
            }, {
                data: "discount",
                width: "80px",
                orderable: false,
                name:"discount"
            }, {
                data: "numbers",
                width: "80px",
                orderable: false,
                name:"numbers"
            }, {
                data: "amount",
                width: "80px",
                orderable: false,
                name:"amount"
            },{
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="productedit" style="margin-left: 10px;" numbers="'+row.numbers+'" productName="'+row.productName+'" productId="'+row.productId+'" amount="'+row.amount+'" discount="'+row.discount+'" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="productdel" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initProductEvent();
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($productlistUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 4){
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                category : $category,
                isGroupon : ''
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords:res.data.categoryTotal,
                    iTotalDisplayRecords:res.data.categoryTotal
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name:"price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name:"amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name:"sales"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'+row.id+'" productName="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectProductEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        // 商品管理
        $(".products").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $promotionid = rowId;
            $productdatatables.search('').draw();
            $("#modal_products").modal("show");
        });

        $(".edit").on("click",function(){

            $("#product_image_dropzone").html('');

            var id =  $(this).attr("rowId");
            var validFrom =  $(this).attr("validFrom");
            var validTo =  $(this).attr("validTo");
            var title =  $(this).attr("title");
            var name =  $(this).attr("name");
            var details =  $(this).attr("details");
            var originImg = $(this).attr("originImg");
            var imgUrl = $(this).attr("img");

            var d = new Date(parseInt(validFrom));
            validFrom = d.format('yyyy-MM-dd hh:mm');
            d = new Date(parseInt(validTo));
            validTo = d.format('yyyy-MM-dd hh:mm');
            $('.daterange-time').val(validFrom + ' - ' +validTo );

            $("#id").val(id);
            $("#valid_from").val(validFrom);
            $("#valid_to").val(validTo);
            $("#title").val(title);
            $("#name").val(name);
            $("#details").val(details);

            //图片
            if (originImg) {
                var key = originImg,
                    url = imgUrl,
                    imgObject = {
                        imgUrl: url,
                        id: key
                    };
                var mockFile = {name: "", size: "", dataImg: imgObject.id};
                uploader[0].dropzone.emit("addedfile", mockFile);
                uploader[0].dropzone.emit("thumbnail", mockFile, imgObject.imgUrl);
                uploader[0].dropzone.emit("complete", mockFile);
            }

            $("#modal_diamond").modal("show");
        });



        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认结束？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deletePromotion(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }

    function initProductEvent(){
        $(".productedit").on("click",function(){
            var id =  $(this).attr("rowId");
            var discount =  $(this).attr("discount");
            var amount =  $(this).attr("amount");
            var productId =  $(this).attr("productId");
            var productName =  $(this).attr("productName");
            var numbers = $(this).attr("numbers");


            $("#diamondId").val(id);
            $("#promotionId").val($promotionid);
            $("#discount").val(discount);
            $("#amount").val(amount);
            $("#numbers").val(numbers);
            $("#productId").val(productId);
            $("#productName").val(productName);

            $("#modal_products_detail").modal("show");
        });

        $(".productdel").on("click",function(){
            var id =  $(this).attr("rowId");
            var r=confirm("确定该商品不再参与此活动?");
            if (r==true)
            {
                deletePromotionDiamond(id);
            }

            //utils.tool.confirm('确定该商品不参与此活动?', function() {
            //    deletePromotionDiamond(id);
            //});
        });

    }

    function initSelectProductEvent(){
        $(".selectproduct").on("click",function(){
            var productId =  $(this).attr("rowId");
            var productName =  $(this).attr("productName");
            $("#productId").val(productId);
            $("#productName").val(productName);
            $("#modal_select_products").modal("hide");
        });
    }


    /*将活动手动结束*/
    function  deletePromotion(pId){
        var url = window.host + "/promotionGroupon/close/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /**从活动中下架删除该商品 */
    function  deletePromotionDiamond(pId){
        var url = window.host + "/promotionGroupon/delete/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $productdatatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    // 保存活动
    $(".saveBtn").on('click', function() {
        var url = window.host + '/promotionGroupon/savePromotion';
        var id = $("#id").val();
        var validFrom = $("#valid_from").val();
        var validTo = $("#valid_to").val();
        var title = $("#title").val();

        var imgArr = [];
        $('#product_image_dropzone > .dz-complete').each(function(i, item) {
            imgArr.push($(item).attr('data-img'));
        });

        if(!validFrom || validFrom == '' || !validTo || validTo == '' ){
            utils.tools.alert("请选择活动时间!", {timer: 1200, type: 'warning'});
            return;
        }else if(!title || title == ''){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }else if(imgArr.length > 1){
            utils.tools.alert("请不要选择多张图片!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            validFrom: validFrom,
            validTo: validTo,
            title: title,
            img: imgArr.join(',')
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/mall/groupon';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    // 新增活动商品
    $(".btn-addproduct").on('click', function() {
        $("#promotionId").val($promotionid);
        $("#discount").val('');
        $("#amount").val('');
        $("#numbers").val('');
        $("#diamondId").val('');
        $("#productId").val('');
        $("#productName").val('');
        $("#modal_products_detail").modal("show");
    });

    // 保存活动商品
    $(".saveProductBtn").on('click', function() {
        var url = window.host + '/promotionGroupon/save';
        var promotionId = $("#promotionId").val();
        var discount = $("#discount").val();
        var productId = $("#productId").val();
        var id = $("#diamondId").val();
        var numbers = $("#numbers").val();
        var amount = $("#amount").val();
        if(!discount || discount == '' ){
            utils.tools.alert("请输入活动价格!", {timer: 1200, type: 'warning'});
            return;
        }else if(!productId || productId == ''){
            utils.tools.alert("请选择商品!", {timer: 1200, type: 'warning'});
            return;
        }else if(!numbers || numbers == ''){
            utils.tools.alert("请输入参团人数!", {timer: 1200, type: 'warning'});
            return;
        }else if(!amount || amount == ''){
            utils.tools.alert("请输入团购库存!", {timer: 1200, type: 'warning'});
            return;
        }


        var data = {
            id: id,
            promotionId: promotionId,
            discount: discount,
            productId: productId,
            numbers: numbers,
            amount: amount
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        if(res.data){
                            alert("操作成功");
                            $("#modal_products_detail").modal("hide");
                            $productdatatables.search('').draw();
                        }else{
                            alert("此商品已存在于活动中，不能重复添加");
                        }
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });

    $('body').on('change','select[name="categoryType"]', function(event) {
        $productlistUrl = window.host + "/product/list";
        $category = $(this).val();
        $selectproductdatatables.search('').draw();
    });
    // 选择活动商品
    $("#productName").on('focus', function() {
        $selectproductdatatables.search('').draw();
        $("#modal_select_products").modal("show");
    });

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            $productlistUrl = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }


    });

    $(".btn-search").on('click', function() {

        var keyword = $.trim($("#sKeyword").val());
        if (keyword != '' && keyword.length > 0){
            $listUrl = window.host + "/promotionDiamond/list/" + keyword;
            $datatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            $listUrl = window.host + "/promotionDiamond/list";
            $datatables.search( '' ).draw();
        }


    });


});