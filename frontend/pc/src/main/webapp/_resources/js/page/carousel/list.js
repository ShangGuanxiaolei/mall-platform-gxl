/**
 * Created by chh on 16/10/13.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'daterangepicker', 'product/upload'], function($,utils,datatabels,blockui,bootbox,select2,daterangepicker,uploader) {

    var $listUrl = window.host + "/carousel/list";

    var $productlistUrl = window.host + "/product/list";

    var $shopId = null;

    var $rowId = '';

    var $promotionid = '';

    var $category = '';

    var $orders = ['price','amount','sales','onsale'];

    var $order = '';

    var $carouselId = '';

    var $viewkeyword = '';

    var $grouponkeyword = '';

    var $type = '';

    let prefix = 'hanwei://meetingPlace?validAt=';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    //初始化商品分类信息
    $.ajax({
        url: window.host + '/shop/category/list',
        type: 'POST',
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.errorCode == 200) {
                var dataLength = data.data.length;
                for (var i = 0; i < dataLength; i++) {
                    $("#categoryType").append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
                };
            } else {
                utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
            }
        },
        error: function (state) {
            if (state.status == 401) {
            } else {
                utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
            }
        }
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增轮播图
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#title").val('');
        $("#type").val('');
        $("#description").val('');
        $("#modal_carouse").modal("show");
    });

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.type)
                    {
                        case 'HOME':
                            status = '首页轮播';
                            break;
                        case 'HOME_ACTIVITY':
                            status = '首页活动';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="products" style="margin-left: 10px;" type="'+row.type+'" rowId="'+row.id+'" ><i class="icon-pencil7"></i>内容管理</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        // 内容管理
        $(".products").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $type = $(this).attr("type");
            $carouselId = rowId;
            $imagedatatables.search('').draw();
            $("#modal_carouse_detail").modal("show");
        });


        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认结束？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deletePromotion(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }

    /*删除轮播图*/
    function  deletePromotion(pId){
          var url = window.host + "/carousel/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    /**从活动中下架删除该商品 */
    function  deletePromotionFlashsale(pId){
        var url = window.host + "/promotionFlashsale/delete/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $productdatatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    // 保存轮播图
    $(".saveBtn").on('click', function() {
        var title = $("#title").val();
        var type = $("#type").val();
        var description = $("#description").val();
        if(!type || type == ''){
            utils.tools.alert("请选择类型!", {timer: 1200, type: 'warning'});
            return;
        }else if(!title || title == ''){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }

        $.ajax({
            url: window.host + '/carousel/check',
            type: 'POST',
            dataType: 'json',
            data: {'type':type},
            success: function (data) {
                if (data.errorCode == 200) {
                    var role = data.data;
                    if(role){
                        utils.tools.alert("该类型已经设置过!", {timer: 3000, type: 'warning'});
                        return;
                    }else{
                        saveCarousel(type, title, description);
                    }
                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    });


    function saveCarousel(type, title, description) {
        var url = window.host + '/carousel/save';
        var data = {
            type: type,
            title: title,
            description: description
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        $("#modal_carouse").modal("hide");
                        $datatables.search('').draw();
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    var $imagedatatables = $('#xquark_image_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/carousel/listImage", {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                carouselId : $carouselId,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.imgUrl+'" />';
                }
            },
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                data: "idx",
                width: "100px",
                orderable: false,
                name:"idx"
            },{
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.targetType)
                    {
                        case 'PRODUCT':
                            status = '商品';
                            break;
                        case 'GROUPON':
                            status = '团购';
                            break;
                        case 'ACTIVITY':
                            status = '活动';
                            break;
                        case 'CATEGORY':
                            status = '类别';
                            break;
                        case 'CUSTOMIZED':
                            status = '自定义';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initImageEvent();
        }
    });

    function initImageEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
            var rowId =  $(this).attr("rowId");
            $("#carouselImageId").val(rowId);
            $("#product_image_dropzone").html('');
            $.ajax({
                url: window.host + '/carousel/getImage/' + rowId,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {

                        var tweet = data.data;
                        //商品图片
                        var key = tweet.img,
                            url = tweet.imgUrl,
                            img = {
                                imgUrl: url,
                                id: key
                            };
                        addProductImage(img);

                        $("#carouselId").val(tweet.carouselId);
                        $("#add_title").val(tweet.title);
                        $("#add_description").val(tweet.description);
                        $("#add_idx").val(tweet.idx);
                        $("#targetType").val(tweet.targetType);
                        $("#target").val(tweet.target);
                        if(tweet.targetType == 'CUSTOMIZED'){
                            $("#targetName").val(tweet.target);
                            $("#targetName").attr("readonly", false);
                        }else{
                            $("#targetName").val(tweet.targetName);
                            $("#targetName").attr("readonly", true);
                        }

                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                        $("#modal_carouse_detail_add").modal("show");
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteImage(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*将明细数据手动删除*/
    function  deleteImage(pId){
        var url = window.host + "/carousel/deleteImage/"+pId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    alert('操作成功');
                    $imagedatatables.search('').draw();
                } else {
                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    $('body').on('change','select[name="categoryType"]', function(event) {
        $productlistUrl = window.host + "/product/list";
        $category = $(this).val();
        $selectproductdatatables.search('').draw();
    });

    // 跳转类型改变后，清空之前选择的跳转目标
    $("#targetType").on('change', function() {
        $("#target").val('');
        $("#targetName").val('');
    });

    // 选择跳转目标
    $("#targetName").on('focus', function() {
        var targetType = $("#targetType").val();
        // 跳转目标为商品，则弹出商品选择界面
        if(targetType == 'PRODUCT'){
            $selectproductdatatables.search('').draw();
            $("#modal_select_products").modal("show");
        }
        // 自定义，则弹出自定义选择界面
        else if(targetType == 'CUSTOMIZED'){
            $("#targetName").attr("readonly", false);
        }
        // 团购，则弹出团购选择界面
        else if(targetType == 'GROUPON'){
            $selectgroupondatatables.search('').draw();
            $("#modal_select_groupon").modal("show");
        }
        // 跳转目标为大促活动，则弹出大促主会场和分会场选择界面
        else if(targetType === 'ACTIVITY'){
            $selectgrandsaledatatables.search('').draw();
            $("#modal_select_grandsale").modal("show");
        }

    });

    $(".addImage").on('click', function() {
        $("#product_image_dropzone").html('');
        $("#carouselImageId").val('');
        $("#carouselId").val($carouselId);
        $("#add_title").val('');
        $("#add_description").val('');
        $("#add_idx").val('');
        $("#targetType").val('');
        $("#target").val('');
        $("#targetName").val('');
        $("#modal_carouse_detail_add").modal("show");
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($productlistUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 3){
                            return '';
                        } else {
                            return $orders[_index - 3];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                category : $category,
                isGroupon : ''
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords:res.data.categoryTotal,
                    iTotalDisplayRecords:res.data.categoryTotal
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name:"price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name:"amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name:"sales"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectTargetEvent();
        }
    });

    var $selectviewdatatables = $('#xquark_select_views_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/viewPage/list", {
                size: data.length,
                page: (data.start / data.length),
                keyword: $viewkeyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.pageType)
                    {
                        case 'TWEET':
                            status = '发现';
                            break;
                        case 'GROUPON':
                            status = '拼团';
                            break;
                        case 'FLASHSALE':
                            status = '特卖';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.title+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectTargetEvent();
        }
    });

    var $selectgroupondatatables = $('#xquark_select_groupon_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/promotionGroupon/list", {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                status: '',
                keywords : $grouponkeyword
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.productImg+'" />';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                width: "80px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.archive)
                    {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                orderable: false,
                width: "140px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"validFrom"
            },{
                orderable: false,
                width: "140px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"validTo"
            },{
                orderable: false,
                width: "140px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.productName+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectTargetEvent();
        }
    });

  let $selectgrandsaledatatables = utils.createDataTable('#xquark_select_grandsale_tables', {
    paging: false,
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    ajax: function (data, callback, settings) {
      $.get(window.host + "/grandSale/getAllStadium", {
      }, function (res) {
        if (!res.data) {
          res.data = [];
        }
        callback({
          recordsTotal: res.data.length,
          recordsFiltered: res.data.length,
          data: res.data,
          iTotalRecords:res.data.length,
          iTotalDisplayRecords:res.data.length
        });
      });
    },
    rowId: "id",
    columns: [
      {
        data: "title",
        width: "120px",
        orderable: false,
        name: "title"
      }, {
        orderable: false,
        width: "140px",
        render: function (data, type, row) {
          let cDate = parseInt(row.beginTime);
          let d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "beginTime"
      }, {
        orderable: false,
        width: "140px",
        render: function (data, type, row) {
          let cDate = parseInt(row.endTime);
          let d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "endTime"
      }, {
        orderable: false,
        width: "140px",
        render: function (data, type, row) {
          let cDate = parseInt(row.createdAt);
          let d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "createdAt"
      }, {
        sClass: "right",
        width: "120px",
        orderable: false,
        render: function (data, type, row) {
          return '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="' + prefix + row.beginTime + '&expiredAt=' + row.endTime + '&url=' + row.path + '" name="' + row.title + '" ><i class="icon-pencil7" ></i>选择</a>';
        }
      }
    ],
    drawCallback: function () {  //数据加载完成
      initSelectTargetEvent();
    }
  });

    function initSelectTargetEvent(){
        $(".selectTarget").on("click",function(){
            var target =  $(this).attr("rowId");
            var targetName =  $(this).attr("name");
            $("#target").val(target);
            $("#targetName").val(targetName);
            $("#modal_select_products").modal("hide");
            $("#modal_select_views").modal("hide");
            $("#modal_select_groupon").modal("hide");
            $("#modal_select_grandsale").modal("hide");
        });
    }

    //添加图片
    function addProductImage(img) {
        if (img) {
            var mockFile = {name: "", size: "", dataImg: img.id};
            uploader[0].dropzone.emit("addedfile", mockFile);
            uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
            uploader[0].dropzone.emit("complete", mockFile);
        }
    }

    $(".btn-submit").on('click', function() {
        saveImage();
    });

    //保存
    function saveImage(){
        var imgArr = [];
        $('#product_image_dropzone > .dz-complete').each(function(i, item) {
            imgArr.push($(item).attr('data-img'));
        });

        if(imgArr.length != 1){
            utils.tools.alert("请有且仅选择一张图片!", {timer: 1200, type: 'warning'});
            return;
        }
        var pId = $("#carouselImageId").val();
        var carouselId = $("#carouselId").val();
        var title = $("#add_title").val();
        var description = $("#add_description").val();
        var idx = $("#add_idx").val();
        var target = $("#target").val();
        var targetType = $("#targetType").val();

        if(targetType == 'CUSTOMIZED'){
            target = $("#targetName").val();
        }


        if(!title || title == '' ){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }else if(!idx || idx == ''){
            utils.tools.alert("请输入显示顺序!", {timer: 1200, type: 'warning'});
            return;
        }else if(!targetType || targetType == ''){
            utils.tools.alert("请选择跳转类型!", {timer: 1200, type: 'warning'});
            return;
        }else if(!target || target == ''){
            utils.tools.alert("请选择跳转目标!", {timer: 1200, type: 'warning'});
            return;
        }else if(imgArr.length == 0){
            utils.tools.alert("请选择图片!", {timer: 1200, type: 'warning'});
            return;
        }

        var param = {
            id:pId,
            carouselId:carouselId,
            img: imgArr.join(','),
            title: title,
            description: description,
            idx: idx,
            target: target,
            targetType: targetType
        };

        $.ajax({
            url: host + '/carousel/saveImage',
            type: 'POST',
            data: param,
            dataType: 'json',
            success: function (data) {
                if (data.errorCode == 200) {
                    if(data.data){
                        alert("操作成功");
                        $("#modal_carouse_detail_add").modal("hide");
                        $imagedatatables.search('').draw();
                    }else{
                        if($type == 'HOME_COUPON'){
                            alert("只能有一条明细数据!");
                        }else{
                            alert("操作失败");
                        }
                    }

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tools.goLogin();
                } else {
                    alert('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    }

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            $productlistUrl = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }


    });

    $(".btn-search-views").on('click', function() {
        var keyword = $.trim($("#sviewKeyword").val());
        $viewkeyword = keyword;
        $selectviewdatatables.search( keyword ).draw();
    });

    $(".btn-search-groupon").on('click', function() {
        var keyword = $.trim($("#select_groupon_sKeyword").val());
        $grouponkeyword = keyword;
        $selectgroupondatatables.search( keyword ).draw();
    });


});