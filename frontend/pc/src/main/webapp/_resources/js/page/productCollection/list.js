define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

    const NOT_FOUND="/_resources/images/404.png";

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = utils.createDataTable('#guiderUserTable', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/productCollection/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    phone:$("#phone").val(),
                    name:$("#name").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    orderable: false,
                    render: function(data, type, row){
                        return  row.userAvatar?`<img class="goods-image" src="${row.userAvatar}" />`:`<img class="goods-image" src="${NOT_FOUND}" />`;
                    }
                },
                {
                    sClass: 'styled text-center sorting_disabled',
                    name: 'userName',
                    title: "昵称",
                    render: function(data, type, row){
                        return row.userName?row.userName:'无' ;
                    }
                },{
                    sClass: 'styled text-center sorting_disabled',
                    data: 'userPhone',
                    name: 'userPhone',
                    title: "手机号",
                    render: function(data, type, row){
                        return row.userPhone?row.userPhone:'无' ;
                    }
                },{
                    sClass: 'styled text-center sorting_disabled',
                    name: 'productName',
                    title: "商品名称",
                    render: function(data, type, row){
                        return row.productName?row.productName:'无' ;
                    }
                },
                {
                    sClass: 'sorting',
                    title: '创建时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {

        }


        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });

    });