define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment,fileinput_zh, fileinput) {
        var $storeId = ''
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: true,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            dateLimit: { days: 600 },
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoApply: true,
            opens: 'left',
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        $('.daterange-basic').daterangepicker(options);

        var singleOptions = {
            singleDatePicker: true,
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' - ',
                cancelLabel: '取消',
                weekLabel: 'W',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            }
        };
        $('.daterange-single').daterangepicker(singleOptions);

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });

        options.startDate = '';
        options.endDate   = '';
        $('.daterange-basic').val('');

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        // 图片上传
        var $fileUpload = $('#logo-file');
        var $logo = $('#setting_logo');
        var fileUploadManager = {
            uploadOption: {
                uploadUrl: '/sellerpc/member/card/upload',
                showCaption: false,
                showUpload: false,
                uploadAsync: true,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"]
            },
            init: function () {
                var defaultImg = $logo.val();
                if (defaultImg && defaultImg.toString().startsWith('http')) {
                    // 如果已经有图片则显示默认
                    this.uploadOption = $.extend({
                        showPreview: true,
                        initialPreview: [ // 预览图片的设置
                            "<img src= '" + defaultImg + "' class='file-preview-image'>"]
                    }, this.uploadOption);
                }
                // 初始化文件上传控件
                $fileUpload.fileinput(this.uploadOption);
                // 上传之前
                $fileUpload.on('filepreajax', function () {
                    //$btnSubmit.addClass('disabled');
                });

                // 选中后立即上传
                $fileUpload.on('filebatchselected', function () {
                    $fileUpload.fileinput('upload');
                });

                // 上传成功
                $fileUpload.on('fileuploaded', function (event, data) {
                    var res = data.response;
                    if (res.errorCode && res.errorCode === 200) {
                        $logo.val(res.img);
                    } else {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                });
                return this;
            },
            destroy: function () {
                $fileUpload.fileinput('destroy');
                return this;
            },
            refresh: function () {
                this.destroy().init();
            }
        };

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/store/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    name:$("#name").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    title: '门店名称',
                    data:'name',
                    name:'name',
                    sortable: false
                },{
                    width:'75px',
                    title: '唯一码',
                    data:'code',
                    name:'code',
                    sortable: false
                },{
                    width:'75px',
                    title: '访问链接',
                    data:'url',
                    name:'url',
                    sortable: false
                },{
                    width:'75px',
                    title: '店员数',
                    data:'memberNum',
                    name:'memberNum',
                    sortable: false
                },
                {
                    width:'100px',
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if(value =='ACTIVE'){
                            return "已激活";
                        }else{
                            return "未激活";
                        }
                    }
                },

                {
                width:75,
                title: '创建时间',
                sortable: false,
                render: function (data, type, row) {
                    if (row.createdAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:150,
                sClass: 'styled text-center sorting_disabled',
                align: 'right',
                title: '操作',
                render: function(data, type, row) {
                    return '<a href="javascript:void(0);" style="margin-left: 10px;" class="viewBtn" rowId="'+row.id + '">查看二维码</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="setMemberBtn" rowId="'+row.id + '">店员管理</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="enableBtn" data-toggle="enablepopover" rowId="'+row.id + '">激活</a>  ' +
                        '<a href="javascript:void(0);" style="margin-left: 10px;" class="disableBtn" data-toggle="disablepopover" rowId="'+row.id + '">删除</a>  ';
                }
            }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });


        // 战队分组列表
        var $groupdatatables = $('#userGroupTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/store/memberList", {
                    size: data.length,
                    page: (data.start / data.length),
                    storeId: $storeId,
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'100px',
                    title: '名称',
                    data:'name',
                    name:'name',
                    sortable: false
                },{
                    width:'100px',
                    title: '手机号',
                    data:'phone',
                    name:'phone',
                    sortable: false
                },{
                    width:'100px',
                    title: '类型',
                    render: function (data, type, row) {
                        var value = row.type;
                        if(value =='LEADER'){
                            return "老板";
                        }else if(value =='MEMBER'){
                            return "普通店员";
                        }else if(value =='SENIOR'){
                            return "高级店员";
                        }
                    }
                },
                {
                    width:'100px',
                    title: '上级名称',
                    data:'parentName',
                    name:'parentName',
                    sortable: false
                },{
                    width:'100px',
                    title: '上级手机号',
                    data:'parentPhone',
                    name:'parentPhone',
                    sortable: false
                },
                {
                    width:'100px',
                    title: '申请状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if(value =='APPLYING'){
                            return "待审核";
                        }else if(value =='ACTIVE'){
                            return "已审核";
                        }
                    }
                },
                {
                    width:'100px',
                    title: '申请时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:'200px',
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="groupedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="groupaudit role_check_table" style="margin-left: 10px;" data-toggle="auditpopover" rowId="'+row.id+'" fid="edit_item"><i class="icon-pencil7"></i>审核</a>';
                        html += '<a href="javascript:void(0);" class="groupdel role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initLevelEvent();
            }
        });
        
        function initLevelEvent() {
            $(".groupedit").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/store/getMember/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            $("#memberId").val(role.id);
                            $("#member_phone").val(role.phone);
                            $("#member_name").val(role.name);
                            $("#member_type").val(role.type);

                            $("#modal_addMember").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteMember(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='auditpopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认审核通过？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="auditpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="auditpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    auditMember(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "auditpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="auditpopover"]').popover('hide');
            } else if (target.data("toggle") == "auditpopover") {
                target.popover("toggle");
            }
        });

        // 审核门店店员
        function auditMember(id) {
            $.getJSON(host + '/store/auditMember/' + id, {}, function(json) {
                alert(json.msg);
                if (json.rc == '1') {
                    $groupdatatables.search('').draw();
                }
            });
        }

        function deleteMember(id) {
            $.ajax({
                url: host + '/store/deleteMember/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $groupdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        // 保存店员信息
        $(".saveMemberBtn").on('click', function() {
            var id = $("#memberId").val();
            var phone = $("#member_phone").val();
            var name = $("#member_name").val();
            var type = $("#member_type").val();

            if(name == ''){
                utils.tools.alert("请输入店员名称", {timer: 1200, type: 'success'});
                return;
            }else if(phone == ''){
                utils.tools.alert("请输入店员手机号", {timer: 1200, type: 'success'});
                return;
            }else if(type == ''){
                utils.tools.alert("请输入店员类型", {timer: 1200, type: 'success'});
                return;
            }

            var data = {
                'id' : id,
                'name' : name,
                'phone' : phone,
                'type' : type
            }

            utils.postAjax(window.host+ "/store/saveMember", data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                        $("#modal_addMember").modal("hide");
                        $groupdatatables.search('').draw();
                    } else {
                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });

        });

        // 新增门店
        $(".btn-release").on('click', function() {
            $("#storeId").val('');
            $("#store_name").val('');
            $("#qrcodeDiv").hide();
            $("#modal_addStore").modal("show");
        });

        // 保存门店
        $(".saveStoreBtn").on('click', function() {
            var id = $("#storeId").val();
            var name = $("#store_name").val();
            if(name == ''){
                utils.tools.alert("请输入门店名称", {timer: 1200, type: 'success'});
                return;
            }

            var data = {
                'id' : id,
                'name' : name
            }

            utils.postAjax(window.host+ "/store/saveInfo", data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                        $("#modal_addStore").modal("hide");
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });

        });



        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });

        function initEvent() {

            $(".viewBtn").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/store/getInfo/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            $("#storeId").val(role.id);
                            $("#store_name").val(role.name);
                            $("#store_qrcode").attr('src',role.qrcode);
                            $("#qrcodeDiv").show();
                            $("#modal_addStore").modal("show");

                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            // 门店店员信息
            $(".setMemberBtn").on("click",function() {
                var id = $(this).attr("rowId");
                $storeId = id;
                $groupdatatables.search('').draw();
                $("#modal_member").modal('show');
            });

            // 激活
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认激活吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });


            // 禁用
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    disableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });



            function disableShop(id) {
                var url = window.host + "/store/delete/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function enableShop(id) {
                var url = window.host + "/store/enable/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

        }

});

