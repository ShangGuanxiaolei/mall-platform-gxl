define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment','bootstrapTable','bootstrapTableEditable'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            opens: 'left',
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-left');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });
        $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
        /** 回调 **/
        $('.daterange-left').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            //alert($("#selectStatus").find("option:selected").text());
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
            $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
            $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据",
                loadingRecords: "载入中...",
            }
        });

        var $datatables = $('#guiderOrder').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/team/order/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    key: data.search.value,
                    //status:   $("#selectStatus").find("option:selected").text(),
                    status:   $("#status").val(),
                    orderNo : $("#orderNo").val(),
                    sellerPhone:$("#sellerPhone").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.orderTotal,
                        recordsFiltered: res.data.orderTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.orderTotal,
                        iTotalDisplayRecords:res.data.orderTotal
                    });
                });
            },
            columns: [{
                width: '160px',
                height: '30px',
                data: 'createdAt',
                name: 'createdAt',
                title: "创建时间",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width: '160px',
                data: 'sellerName',
                name: 'sellerName',
                title: '队员名称'
            },{
                width: '160px',
                data: 'sellerPhone',
                name: 'sellerPhone',
                title: '队员手机号'
            },
                {
                width: '160px',
                data: 'buyerName',
                name: 'buyerName',
                title: '买家名称'
            },{
                    width: '160px',
                    data: 'buyerPhone',
                    name: 'buyerPhone',
                    title: '买家手机号'
                },{
                width: '220px',
                title: '订单信息',
                render: function(data, type, row){
                    return '<a href="#" order-id="' + row.orderNo + '" class="orderInfoBtn" data-toggle="modal" >' +row.orderNo+ ' <i class="icon-search4"></i></a>';
                }
            },{
                data: "totalFee",
                name: "totalFee",
                width: '125px',
                title: '付款金额'
            },
            //    {
            //    width: '125px',
            //    title: '佣金',
            //    render: function(data, type, row){
            //        return '<a href="#" order-id="' + row.orderNo + '" class="commissionInfoBtn" data-toggle="modal" >' + row.commissionFee + ' <i class="icon-search4"></i></a>';
            //    }
            //
            //},
            //    {
            //    width: '100px',
            //    data: 'sellerName',
            //    name: 'sellerName',
            //    title: '来源推客'
            //},
            //    {
            //    width: '160px',
            //    title: '结算时间',
            //    render: function (data, type, row) {
            //        if (row.paidAt ==  null) {
            //            return '';
            //        }
            //        var cDate = parseInt(row.paidAt);
            //        var d = new Date(cDate);
            //        return d.format('yyyy-MM-dd hh:mm:ss');
            //    }
            //},
                {
                width: '80px',
                data: 'status',
                name: 'status',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if(value =='SUBMITTED'){
                        return "待付款";
                    }else if(value =='CANCELLED'){
                        return "已取消";
                    }else if(value =='PAID'){
                        return "已付款";
                    }else if(value =='SHIPPED'){
                        return "待收货";
                    }else if(value =='SUCCESS'){
                        return "已完成";
                    }else if(value =='REFUNDING'){
                        return "退款中";
                    }else if(value =='CLOSED'){
                        return "已关闭";
                    }else{
                        return "";
                    }
                }
            }]
        });
        var orderNo =  null;
        //查看订单详情
        var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitter/order/orderItem", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo : orderNo,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.itemTotal,
                        recordsFiltered: res.data.itemTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.itemTotal,
                        iTotalDisplayRecords:res.data.itemTotal
                    });
                });
            },
            columns: [{
                data: 'productName',
                name: 'productName',
                width: 310,
                title: "商品"
            },{
                data: 'price',
                name: 'price',
                width: 150,
                title: "价格"
            },{
                data: 'amount',
                name: 'amount',
                width: 150,
                title: "数量"
            }]
        })

        $(document).on('click', '.orderInfoBtn', function() {
            orderNo = $(this).attr("order-id");
            $('#modal_orderInfo').modal('show');
            $orderTables.search(orderNo).draw();

        });
        //查看佣金详情
        var $commissionTables = $('#commissionInfo').DataTable({
            ppaging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitter/order/commission", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo : orderNo,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.commissionTotal,
                        recordsFiltered: res.data.commissionTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.commissionTotal,
                        iTotalDisplayRecords:res.data.commissionTotal
                    });
                });
            },
            columns: [{
                data: 'orderNo',
                name: 'orderNo',
                width: 310,
                title: "订单号"
            },{
                data: 'fee',
                name: 'fee',
                width: 150,
                title: "对应佣金"
            }]
        })

        $(document).on('click', '.commissionInfoBtn', function() {
            orderNo = $(this).attr("order-id");
            $('#modal_commission').modal('show');
            $commissionTables.search(orderNo).draw();

        });

        $(".btn-search").on('click', function() {
            $datatables.search(orderNo).draw();
        });

        $(".exportOrderBtn").on('click', function() {
            $('#exportOrderForm').submit();
        });
    });