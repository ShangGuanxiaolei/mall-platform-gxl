define(['echartsNoAMD','echartsTheme','jquery', 'utils', 'd3', 'd3tooltip'], function(echarts, limitless, $, utils,d3, d3tooltip) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/mall/getSummary';
    var data = {
    };
    // 本年总订单,已成功订单,已发货订单
    var weekOrder;
    var weekSuccessOrder;
    var weekShipOrder;
    var topProduct;
    var order;
    var weekDays;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {

            // 取商城概况中的今日数据
            $("#day_todayAmount").html(result.data.info.todayAmount);
            $("#day_todayOrder").html(result.data.info.todayOrder);
            $("#day_submitOrder").html(result.data.info.submitOrder);
            $("#day_paidOrder").html(result.data.info.paidOrder);
            $("#day_refundOrder").html(result.data.info.refundOrder);

            // 取本周总订单,已成功订单,已发货订单
            weekOrder = result.data.nums.weekOrder;
            weekSuccessOrder = result.data.nums.weekSuccessOrder;
            weekShipOrder = result.data.nums.weekShipOrder;
            weekDays = result.data.nums.weekDays;
            topProduct = result.data.topProduct;

            order = echarts.init(document.getElementById('order_lines'), limitless);

            var stacked_lines_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 20,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    data: ['总订单', '已成功', '已发货']
                },

                // Enable drag recalculate
                calculable: true,

                // Hirozontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    data: weekDays
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    {
                        name: '总订单',
                        type: 'line',
                        stack: 'Total',
                        data: weekOrder
                    },
                    {
                        name: '已成功',
                        type: 'line',
                        stack: 'Total',
                        data: weekSuccessOrder
                    },
                    {
                        name: '已发货',
                        type: 'line',
                        stack: 'Total',
                        data: weekShipOrder
                    }
                ]
            };
            order.setOption(stacked_lines_options);


            // 销量排行商品
            if(topProduct){
                $.each(topProduct, function(i, value) {
                    $("#top_products").append("<tr><td>" + value.name + "</td><td>" + value.sales + "</td><td>" + value.created_at + "</td></tr>");
                });
            }

        }
    });


    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            order.resize();
        }, 200);
    }

});
