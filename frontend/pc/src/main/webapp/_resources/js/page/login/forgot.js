/**
 * Created by quguangming on 16/5/18.
 */
define(['form/validate','utils'], function(validate,utils) {

    var $form = $('.forgot-form');

    var $divForgot = $('.forgot-part');

    var $send = $('.send');

    var $phone = $('#phone');

    var $password = $('#newPassword');

    var $rePassword = $('#rePassword');

    var $captcha =  $('#captcha');

    var isProcessing = false;


    $('.modify-pwd').on('click',function(){
        $form.submit();
    });

    $send.on('click',function(){
        if(!$send.hasClass('disabled')){
            sendCaptcha();
        }
    });

    function checkPhone(num) {
        var regPhone = /^1\d{10}$/;

        return regPhone.test(num);
    }


    if (($phone.val().length > 0) && checkPhone($phone.val())) {
        $send.removeClass('disabled').text('发送验证码');
    } else {
        $send.addClass('disabled');
    }

    $phone.on('keyup', function() {
        if (($phone.val().length > 0) && checkPhone($phone.val())) {
            $send.removeClass('disabled').text('发送验证码');
        } else {
            $send.addClass('disabled');
        }

        return false;
    });

    $password.add($rePassword).on('keydown', function(ev){
        var evt = ev || event;
        if (evt.keyCode === 32) {
            return false;
        }
    }).on('paste',function(){
        return false;
    });


    var forgotForm = {
        rules:{
            phone: {
                required: true,
                pattern: /^1\d{10}$/
                //depends:function(element) {
                //    var val = $(element).val();
                //    var reg = /^1\d{10}$/;
                //    if(reg.test(val)){
                //        if($send.hasClass('disabled')) {
                //            $send.removeClass('disabled');
                //        }
                //    } else {
                //        $send.addClass('disabled')
                //    }
                //}
            },
            captcha:{
                required: true,
                pattern: /^\d{6}$/
            },
            newPassword:{
                required: true,
                pattern: /^.{6,50}$/
            },
            repeatPassword: {
                required: true,
                equalTo: "#password"
            }
        },
        messages:{
            phone: {
                required: "手机号码不能为空.",
                pattern: "请输入正确的11位手机号码."
            },
            captcha:{
                required: "验证码不能为空。",
                pattern: "验证码格式不对。"
            },
            newPassword:{
                required: "新密码不能为空.",
                pattern:  "请输入至少6位的新密码."
            },
            repeatPassword: {
                required: "重复密码不能为空.",
                equalTo: "两次输入的密码不同."
            }
        },
        submitCallBack: function(form){
            modifyPwd();
        }
    };

    function sendCaptcha(){

        var waitTime = 60;

        function countdownTimer() {
            if (waitTime === 0) {

                if (($phone.val().length > 0) && checkPhone($phone.val())) {
                    $send.removeClass('disabled').text('发送验证码');
                } else {
                    $send.addClass('disabled').text('发送验证码');
                }
                waitTime = 60;
            } else {
                $send.text('剩余' + waitTime + 's...');
                waitTime--;
                setTimeout(function() {
                    countdownTimer();
                }, 1000);
            }
        }

        countdownTimer();

        var url = window.host + '/forget-pwd';
        var data = {
            mobile: $phone.val()
        };

        if (!$divForgot.is(':visible')) {
            return false;
        }

        utils.postAjax(url, data, function(result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    {
                        alert('验证码发送成功，请查收短信。');
                        break;
                    }
                    default:
                    {
                        alert('验证码发送失败！');
                        //that.alertMethod(result.moreInfo);
                    }
                }
            }
        });

    }

    function modifyPwd() {

        var captcha = $('#captcha');

        var username, code, password;

        username = $phone.val();
        code = $captcha.val();
        password = $password.val();


        if (isProcessing) {
            return false;
        } else {
            isProcessing = true;
        }

        var url = window.host + '/validate-forget-pwd';
        var data = {
            mobile: username,
            smsCode: code,
            pwd: CryptoJS.MD5(password).toString()
        };

        that.postMethod(url, data, function (result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    {
                        alert('修改密码成功！');
                        $divForgot.hide();
                        $('.login-part').fadeIn();
                        break;
                    }
                    case -1:
                    {
                        alert(" 修改失败" + result + ":" + moreInfo);
                        break;
                    }
                    default:
                    {
                        alert(" 修改失败" + result + ":" + moreInfo);
                    }
                }
            }
            isProcessing = false;
        });

        return false;
    }

    validate($form,forgotForm);

});