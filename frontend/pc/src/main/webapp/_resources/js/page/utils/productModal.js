define(['jquery', 'utils', 'ramda', 'utils/dataTable'],
    function ($, utils, R, dataTable) {

      const modalId = 'choose-product-modal';
      const modalTableId = 'choose-product-table';
      const searchBtnId = 'searchBtn';
      const filterSelectId = 'chooseBtn';
      const filterUnSelectId = 'unChooseBtn';
      const selectClass = 'selectPd';
      const unselectClass = 'unSelectPd';
      const confirmBtnId = 'confirmBtn';

      /**
       * 构建弹窗html
       * @type {string}
       */
      const modalTemplate = `
      <div class="modal fade" id="${modalId}">
        <div class="modal-dialog" style="width:1000px;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="modal-title">选择商品</h5>
            </div>
            <div class="modal-body form-horizontal">
              <!-- search bar -->
              <div id="inner-search-bar" hidden="hidden">
                <div class="form-group" style="margin-bottom:0;margin-left:5px;">
                  <div class="panel-heading">
                    <div class="row" style="margin-bottom: 10px">
                      <div id="choose-container" hidden="hidden">
                        <div class="col-md-1" style="margin-right: 10px;">
                          <a href="javascript:" class="btn btn-sm btn-default active" id="${filterSelectId}"><i
                            class="position-left"></i>已选择</a>
                        </div>
                        <div class="col-md-1">
                          <a href="javascript:" class="btn btn-sm btn-default" id="${filterUnSelectId}"><i
                            class="position-left"></i>未选择</a>
                        </div>
                       </div>
                    </div>
                    <div class="row">
                      <div id="search-container">
                        <!-- append component here -->
                      </div>
                      <div class="col-md-2">
                        <a href="javascript:" class="btn btn-sm btn-default" id="${searchBtnId}"><i
                          class="icon-search4 position-left"></i> 搜索</a>
                      </div>
                    </div>
                  </div>
                </div>             
              </div>
              <!-- dataTable -->
              <div id="inner-table">
                <div class="form-group">
                  <div class="datatable-scroll">
                    <table class="table datatable-basic dataTable no-footer " id="${modalTableId}"
                           role="grid" data-page-length='8'>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-primary btn-ok" data-dismiss="modal" id="${confirmBtnId}">确认</button>
            </div>
          </div>
        </div>
       </div>`;

      /**
       * 构造modal html
       */
      class TemplateBuilder {

        constructor(parent, selectedUrl, maxColumns) {
          this.$template = $(modalTemplate);
          this.$searchContainer = this.$template.find('#search-container');
          // 搜索请求参数
          this.searchParams = {};
          this.selected = {};
          this.seletedUrl = selectedUrl;
          this.maxColumns = maxColumns;

          parent.append(this.$template);
        }

        /**
         * 配置开启搜索栏
         * @returns {TemplateBuilder}
         */
        enableSearchBar() {
          this.$template.find('#inner-search-bar').show();
          return this;
        }

        /**
         * 启用搜索功能
         */
        enableSelect(onSelect = R.always, onUnSelect = R.always) {
          const _this = this;

          const refreshTable = combined => {
            const selectedIds = R.keys(this.selected);
            let params = {selectedIds: R.join(',', selectedIds)};
            if (combined !== undefined && combined !== null) {
              // 通过按钮筛选时同时将表格页数复位
              params = $.extend(params, {combined: combined});
              // 从头开始刷新
              _this.$dataTable.refreshAll(params);
              return;
            }
            // 记住刷新状态
            _this.$dataTable.refresh(params);
          };

          const clickFunc = callback => e => {
            const $this = $(e.currentTarget);
            const id = $this.attr('rowId');
            callback(id, $this).then(refreshTable)
              .then(() => console.log('now selected: ', _this.selected));
          };
          this.$template.find('#choose-container').show();

          // 绑定表格中的选择/未选中事件
          $(document).on('click', `.${selectClass}`,
              clickFunc((id, $this) => {
                return new Promise(resolve => {
                  const data = _this.$dataTable.rowData($this.parents('tr'));
                  if (data) {
                    if (onSelect && $.isFunction(onSelect)) {
                      const ret = onSelect(data, $this);
                      if (!ret || ret === false) {
                        return;
                      }
                    }
                    if (_this.maxColumns &&
                        R.keys(_this.selected).length >= _this.maxColumns) {
                      utils.tools.error('已超过可选择数量');
                      return;
                    }
                    _this.selected[data.id] = data;
                  }
                  // TODO 完善onSelect的执行顺序
                  resolve();
                });
              }));
          $(document).on('click', `.${unselectClass}`,
              clickFunc((id, $this) => {
                return new Promise(resolve => {
                  const data = _this.$dataTable.rowData($this.parents('tr'));
                  if (data) {
                    const callbackRet = onUnSelect(data);
                    // 如果回调函数返回promise, 则要保证回调在回调函数跟服务器交互后删除本地缓存
                    if (callbackRet && callbackRet instanceof Promise) {
                      callbackRet.then(ret => {
                        if (ret) {
                          delete _this.selected[data.id];
                          resolve(ret);
                        }
                      })
                    } else {
                      delete _this.selected[data.id];
                      resolve(true);
                    }
                  }
                });
              }));

          // 绑定已选中/未选中的筛选按钮, 搜索时带上当前选中的参数, 供后台过滤
          this.$template.find(`#${filterSelectId}`).on(
              'click', e => {
                $(e.currentTarget).addClass('active');
                $(`#${filterUnSelectId}`).removeClass('active');
                refreshTable(true);
              });
          this.$template.find(`#${filterUnSelectId}`).on(
              'click', e => {
                $(e.currentTarget).addClass('active');
                $(`#${filterSelectId}`).removeClass('active');
                refreshTable(false)
              });
          return this;
        }

        /**
         * 启用分类搜索按钮
         */
        withCategoryBtn() {
          const id = 'categorySelector';
          this.categorySelector = id;
          utils.genOptions(`${window.host}/shop/category/list`, R.prop('id'),
              R.prop('name'))
          .then(this._fedSelector(id, '所有分类', 'categoryId'));
          return this;
        }

        /**
         * 构造供应商搜索按钮
         */
        withSupplierBtn() {
          const id = 'supplierSelector';
          this.supplierSelector = id;
          utils.genOptions(
              `${window.host}/supplier/list?pageable=false&type=override`,
              R.prop('id'),
              R.prop('name'),
              R.prop('list'))
          .then(this._fedSelector(id, '所有供应商', 'supplierId'));
          return this;
        }

        /**
         * 构造搜索框
         * @returns {TemplateBuilder}
         */
        withKeyword() {
          const searchId = 'searchkey';
          const $html = `
          <div class="col-md-3">
            <input type="search" class="form-control" placeholder="请输入名称或SKU编码" id="${searchId}"
              name="${searchId}" value="">
          </div>`;
          this.$searchContainer.append($html);
          this.searchParams.keyword = () => $(`#${searchId}`).val();
          return this;
        }

        /**
         * 启用表格
         */
        withDataTable(params, selectable) {
          // 初始化表格
          const _this = this;
          const columns = params.columns || [];

          // 1. 启用选择列
          if (selectable) {
            // 全局数组, 保存选中id
            columns.push({
              title: '操作',
              name: "operation",
              render: function (data, type, row) {
                const rowId = row.id;
                if (row.select || _this.selected[rowId]) {
                  return `<a href="javascript:void(0);" class="unSelectPd" rowId="${rowId}">
                    <i class="icon-pencil7" ></i>取消</a>`;
                }
                return `<a href="javascript:void(0);" class="selectPd" rowId="${rowId}">
                    <i class="icon-pencil7" ></i>选择</a>`;
              }
            });
          }

          if (params.columnNumber) {
            // 控制表格每页数量
            _this.$template.find(`#${modalTableId}`).attr('data-page-length',
                params.columnNumber);
          }

          this.$dataTable = dataTable.create({
            dom: `#${modalTableId}`,
            url: params.url,
            autoFit: false,
            drawBack: () => {
              _this.tableInited = true;
              const data = R.filter(x => x.select === true,
                  _this.$dataTable.getDataAll());
              data.forEach(item => {
                _this.selected[item.id] = item;
              });
              console.log('now selected: ', _this.selected);
            },
            columns: columns,
            reqParams: params.reqParams
          });
          return this;
        }

        /**
         * 绑定搜索事件
         */
        withSearchEnable() {
          const _this = this;
          this.$template.find(`#${searchBtnId}`).on('click', () => {
            // 执行闭包, 获取实时值
            const params = R.map(R.call, _this.searchParams);
            if (!_this.$dataTable) {
              throw '请先初始化表格';
            }
            _this.$dataTable.refresh(params);
          });
          return this;
        }

        /**
         * 绑定确认事件, 输出选中的skuId
         */
        withConfirm(onConfirm) {
          const _this = this;
          $(`#${confirmBtnId}`).on('click', () => {
            if (onConfirm && $.isFunction(onConfirm)) {
              onConfirm(R.values(_this.selected));
            }
          });
          return this;
        }

        /**
         * 私有方法, 用来构造selector
         * @param selectorId 选择框id
         * @param selectorName 选择框名称
         * @param param 请求参数名称
         */
        _fedSelector(selectorId, selectorName, param) {
          const _this = this;
          return function (options) {
            const $html = $(`
              <div class="col-md-2">
                <select class="select form-control" id="${selectorId}">
                  <option value="" selected="selected">${selectorName}</option>
                </select>
              </div>
            `);
            const $selector = $html.find(`#${selectorId}`);
            $selector.append(options);
            _this.$searchContainer.append($html);
            // 放到请求参数列表
            if (param && param !== '') {
              // 值必须为一个闭包, 在点击时执行实时获取
              _this.searchParams[param] = () => $selector.val()
            }
          }
        }

      }

      /**
       * modal实现类
       */
      class PdModal {

        /**
         * 构造商品选框
         * 约定后台接口必须支持相关的查询参数
         * 如果要实现搜索功能, 则后台返回对象中必须包含select属性, 指定当前对象是否
         * 已在数据库中被选中
         * 同时后台接口需要接受selectedIds参数, 针对该参数
         * 在选择过滤时将用户选中的值同时返回
         * 在未选择过滤时将用户数据中由于用户操作选中的部分过滤
         *
         * @param params.dom modal的dom地址
         * @param params.selectable 启用选择组件
         * @param params.onSelect [optional] 选择后的回调函数
         * @param params.onUnSelect [optional] 取消选择后的回调函数
         * @param params.selectParamSupplier 选中参数supplier
         * @param params.unSelectParamSupplier 未选中参数supplier
         * @param params.onConfirm modal点击确认的事件
         * @param params.selectedUrl 查询已选中id的url
         * @param params.table 表格参数
         * @param params.table.url 表格刷新地址
         * @param params.table.drawback 表格回调地址
         * @param params.table.columns 表格列数据
         * @param params.table.columnNumber 表格列数
         * @param params.table.reqParams 表格请求参数
         */
        constructor(params) {
          this.dom = params.dom;
          this.$dom = $(params.dom);
          // init modal html first

          const builder = new TemplateBuilder(this.$dom, params.selectedUrl,
              params.table.columnNumber)
          .enableSearchBar()
          .withDataTable(params.table, params.selectable)
          .withSearchEnable()
          .withKeyword()
          .withCategoryBtn()
          .withSupplierBtn()
          .withConfirm(params.onConfirm);

          if (params.selectable) {
            builder.enableSelect(params.onSelect, params.onUnSelect);
          }

        }

        /**
         * 显示弹框
         */
        show() {
          $(`#${modalId}`).modal('show');
        }

        /**
         * 隐藏弹框
         */
        hide() {
          $(`#${modalId}`).modal('hide');
        }

      }

      return {
        create: function (params) {
          return new PdModal(params);
        }
      }

    });