/**
 * Created by quguangming on 16/5/25.
 */
define(
  ['jquery', 'utils', 'utils/productModal', 'product/xlsxUpload', 'utils/storageHelper',
    'product/form', 'product/upload', 'product/multiUpload',
    'product/product-data', 'product/describe', 'select2',
    'tokenfield', 'ckEditor', 'product/xlsxUpload'
  ],
  function ($, utils, productModal, xlsxUpload, storageHelper, form, uploader, multiUpload, proData,
    describe,
    select2,
    tokenfield) {

    var $proId = utils.tools.request('pId'); //商品Id

    var $skuId = $('#skuId').val();

    var $shopId = $("#shopId").val(); //店铺Id

    var $productType = $('#type').val();

    var $proCateId = new Array(); //商品类目Id

    var tagInited = false;
    var tags;

    const baseTagUrl = window.host + '/tags';
    const getTagsUrl = baseTagUrl + '/list/' + $proId;
    const tagProductUrl = baseTagUrl + '/tagProduct/' + $proId;
    const unTagUrl = baseTagUrl + '/unTag/' + $proId;
    const combineListUrl = window.host + `/productCombine/list`;

    const ckeditor = 'editor';

    CKEDITOR.replace(ckeditor, {
      language: 'zh-cn',
    });

    $(".styled, .multiselect-container input").uniform({
      radioClass: 'choice'
    });

    // 返回商品所有选中的规格属性和属性值
    var chooseAttributeAndItems;

    //存放选中sku数组
    var attr_active = [];

    // 修改商品时，商品的skus信息
    var $skus;

    var pdSourceMap = {
      'SF': '顺丰',
      'CLIENT': '汉薇'
    };

    //商品修改
    if ($proId) {
      $("#productId").val($proId);
      proData.getProductInfo($proId, function (data) {
        if (typeof (data) === 'object') {
          if (data.errorCode == 200) {
            var product = data.data;
            $skus = product.skus;
            //商品图片
            if (product.imgs) {
              $.each(product.imgs, function (i, img) {
                var key = img.img,
                  url = img.imgUrl,
                  img = {
                    imgUrl: url,
                    id: key
                  };
                addProductImage(img);
              });
            }

            $("#productName").val(product.name);
            $("#productEncode").val(product.encode);
            $("#productModel").val(product.model);
            $("#singleSkuId").val(product.skus[0].id);
            $("#height").val(product.height);
            $("#width").val(product.width);
            $("#length").val(product.length);
            $("#kind").val(product.kind);
            $("#numInPackage").val(product.numInPackage);
            $("#packageId").val(product.packageId);
            $("#reviewStatus").val(product.reviewStatus);
            $("#productInfo").val(product.description);
            $("#proRecomend").bootstrapSwitch('state', product.recommend);
            $("#proOneyuanPurchase").bootstrapSwitch('state',
              product.oneyuanPurchase);
            $("#proOneyuanPostage").val(product.oneyuanPostage);
            $("#proSpcialRate").val(product.specialRate);
            $("#commissionRate").val(product.commissionRate);
            $("#weight").val(product.weight);
            if (product.source && product.source !== '') {
              $('#pdSource').val(pdSourceMap[product.source]);
            }
            if (product.supportRefund) {
              $("input[name=supportRefund][value=true]").prop("checked",
                true);
            } else {
              $("input[name=supportRefund][value=false]").prop("checked",
                true);
            }
            $("input[name=gift][value=" + (product.gift ? 'true' : 'false') +
              "]").attr('checked', true);
            var yundouSacle = product.yundouScale;
            var settedScale = product.yundouScale && product.yundouScale !==
              0;
            $("input[name=setScale][value=" + (settedScale ? 'true' : 'false') +
                "]")
              .attr('checked', true);
            $('#yundouScale').val(yundouSacle);
            if (settedScale) {
              $('#scaleSetting').show();
            }
            console.log(product.minYundouPrice);
            $('#minYundouPrice').val(product.minYundouPrice);

            $("#productOriginalPrice").val(product.originalPrice);
            $("#refundAddress").val(product.refundAddress);
            $("#refundName").val(product.refundName);
            $("#refundTel").val(product.refundTel);
            $("#templateValue").val(
              product.templateValue == null ? '' : product.templateValue);

            if (product.categorys) {
              $.each(product.categorys, function (i, category) {
                $proCateId.push(category.id);
              });
            }
            if (product.detailH5) {
              CKEDITOR.instances.editor.on("instanceReady", function (event) {
                CKEDITOR.instances.editor.setData(product.detailH5);
              })
            }

            // 判断商品规格类型显示
            if (product.attributes) {
              $("input[name='skuType'][value='muliSku']").prop("checked",
                "checked");
              let skuAttributes = product.skuAttributes;
              skuAttributes.forEach(function (element) {
                attr_active.push(element.name);
              })
              $("#uniformSkuDiv").hide();
              $("#muliSkuDiv").show();
              $("#add_multi_sku_btn").show();
            } else {
              $(":radio[name='skuType'][value='uniformSku']").prop("checked",
                "checked");
              let price = $skus[0].price;
              let deductionDPoint = $skus[0].deductionDPoint;
              $("#productPrice").prop("value", price);
              $("#deductionDPoint").prop("value", deductionDPoint);
              let conversionPrice = (Number.parseFloat(price) -
                (Number.parseFloat(deductionDPoint) / 10)).toFixed(1);
              $("#conversionPrice").prop("value", conversionPrice);
              //$("#marketPrice").val(product.marketPrice);
              $("#productAmount").val($skus[0].amount);
              $("#secureAmount").val($skus[0].secureAmount);
              $("#skuCode").val($skus[0].skuCode);
              $("#skuCodeOrigin").val($skus[0].skuCode);
              $("#barCode").val($skus[0].barCode);
              $("#point").val($skus[0].point);
              $("#promoAmt").val($skus[0].promoAmt);
              $("#serverAmt").val($skus[0].serverAmt);
              $("#netWorth").val($skus[0].netWorth);
              $("#length").val($skus[0].length);
              $("#width").val($skus[0].width);
              $("#height").val($skus[0].height);
              $("#weight").val($skus[0].weight);
              $("#numInPackage").val($skus[0].numInPackage);
              $("#muliSkuDiv").hide();
              $("#add_multi_sku_btn").hide();
              $("#uniformSkuDiv").show();
            }

            $.uniform.update();

            // 滤芯规格
            if (product.filterSpec) {
              $(`:radio[name='filterSpec'][value=${product.filterSpec}]`)
                .prop('checked', true);
            }

            // $("input[name='prudctStatus'][value='" + product.status
            //     + "']").attr("checked", true);

            $("input[name='status']").prop("value", product.status);

            if (product.fragments) {
              describe.setFragments(product.fragments);
            }
            /** 初始化选择框控件 **/
            $('#templateValue').select2({
              minimumResultsForSearch: Infinity,
              width: '100%'
            });
            proData.getShopProCateList($shopId, setProCateForm,
              utils.tools.alert);
            proData.getBrandList(setProBrandForm, utils.tools.alert);
            proData.getWarehouseList(product.wareHouseId, setWarehouseForm,
              utils.tools.alert);
            proData.getSupplierList(product.supplierId, setSupplierForm,
              utils.tools.alert);
          }
        }
      }, function () {
        utils.tools.alert('获取商品数据出错!', {
          timer: 3600,
          type: 'warning'
        });
      });

    } else { //发布商品操作
      proData.getShopProCateList($shopId, setProCateForm, utils.tools.alert);
      proData.getBrandList(setProBrandForm, utils.tools.alert);
      proData.getWarehouseList('', setWarehouseForm, utils.tools.alert);
      proData.getSupplierList('', setSupplierForm, utils.tools.alert);
    }

    //添加图片
    function addProductImage(img) {
      if (img) {
        var mockFile = {
          name: "",
          size: "",
          dataImg: img.id
        };
        uploader[0].dropzone.emit("addedfile", mockFile);
        uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
        uploader[0].dropzone.emit("complete", mockFile);
        uploader[0].dropzone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
      }
    }

    // 不管是不是套装商品都要清空缓存
    const key = 'combineInfo';
    // 清空本地存储
    const storage = storageHelper.local(key);
    storage.remove(key);
    if ($productType && $productType === 'COMBINE') {

      // 解除绑定关系
      const unCombineUrl = (masterId, slaveId) =>
        `${window.host}/productCombine/uncombineTable?masterId=${masterId}&slaveId=${slaveId}`;

      // 暂存用户修改的数量
      const tmpAmt = {};

      /**
       * 组合选框初始化
       */
      (function initModal() {
        const modal = productModal.create({
          dom: '#modal-container',
          selectable: true,
          onSelect: data => {
            const val = Number(tmpAmt[data.id] || 1);
            if (val <= 0) {
              utils.tools.error('套装数量不能小于0');
              return false;
            }
            let pdAmount = $('#productAmount').val();
            if (!pdAmount || pdAmount === '') {
              utils.tools.error('请先输入套装库存');
              return false;
            }
            pdAmount = Number(pdAmount);
            const canUseAmt = data.amount;
            // 校验套装数量 * 输入数量不能大于sku库存
            if (val * pdAmount > canUseAmt) {
              utils.tools.error(`组合件数 ${val} * ${pdAmount} = ${val
                * pdAmount} 大于可用库存 ${canUseAmt}`);
              return false;
            }
            return true;
          },
          onUnSelect: data => {
            // 如果处于编辑状态才取消后台关系
            // 返回promise, 确保本地缓存跟服务器的数据一致
            return new Promise(resolve => {
              if (!$skuId) {
                resolve(true);
                return;
              }
              utils.post(unCombineUrl($skuId, data.id), ret => {
                if (ret === false) {
                  utils.tools.error('取消失败');
                }
                resolve(ret);
              });
            });
          },
          onConfirm: selected => {
            if (selected) {
              selected = selected.map(item => {
                const tmpVal = tmpAmt[item.id] || 1;
                if (tmpVal) {
                  item.number = tmpVal;
                }
                return item;
              });
              storage.set(selected);
            }
          },
          table: {
            url: combineListUrl,
            reqParams: {
              combined: true,
              productId: $proId
            },
            columnNumber: 50,
            columns: [{
                title: '名称',
                data: 'fullName',
                name: 'fullName',
              },
              {
                title: '编码',
                data: "skuCode",
                name: "skuCode",
              },
              {
                title: '数量',
                name: 'number',
                // 优先从用户更改的数据中读取
                render: (d, t, row) => `<input class="combineNum" style="width: 50px;"
                    rowId="${row.id}" amount="${row.amount}" type="number" min="1" step="1" value="${tmpAmt[row.id]
                  || row.number || 1}">`
              },
              {
                title: '库存',
                data: "amount",
                name: "amount",
              },
              {
                title: '状态',
                data: 'statusStr',
                name: 'statusStr'
              },
              {
                title: '价格',
                name: "price",
                render: (d, t, row) => `${row.price}元`
              }
            ]
          }
        });

        $('.btn-combine').on('click', () => {
          const supplier = $('.product-supplier').val();
          if (!supplier || supplier === '') {
            utils.tools.error('请先选择供应商');
            return;
          }
          const amt = $('#productAmount').val();
          if (!amt || amt === '') {
            utils.tools.error('请先输入套装库存');
            return;
          }
          modal.show();
        });

        $(document).on('blur', '.combineNum', e => {
          const $this = $(e.currentTarget);
          const val = Number($this.val());
          if (val <= 0) {
            utils.tools.error('套装数量不能小于0');
            return;
          }
          let pdAmount = $('#productAmount').val();
          if (!pdAmount || pdAmount === '') {
            utils.tools.error('请先输入套装库存');
            return;
          }
          pdAmount = Number(pdAmount);
          const canUseAmt = Number($this.attr('amount'));
          // 校验套装数量 * 输入数量不能大于sku库存
          if (val * pdAmount > canUseAmt) {
            utils.tools.error(`组合件数 ${val} * ${pdAmount} = ${val
              * pdAmount} 大于可用库存 ${canUseAmt}`);
            return;
          }
          const id = $this.attr('rowId');
          tmpAmt[id] = val;
        });

      })();
    }

    /* 标签初始化 */
    (function initTags() {
      const $tagsInput = $('#tags-input');
      $tagsInput.on('tokenfield:initialize', function (e) {
          console.log('inited');
        })
        .on('tokenfield:createtoken', function (e) {
          var data = e.attrs.value;
          // 第一次初始化时也会调用该方法，所以做如下判断
          var result = true;
          if (tagInited) {
            // tokenfield要求失败时返回false, 该请求必须同步处理
            // 查看 http://sliptree.github.io/bootstrap-tokenfield/
            if ($proId && $proId !== '') {
              utils.postAjaxSync(tagProductUrl, {
                name: data
              }, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试", {
                    timer: 1200,
                    type: 'warning'
                  });
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    console.log('tag' + data + ' added');
                  } else {
                    result = false;
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo, {
                        timer: 1200,
                        type: 'warning'
                      });
                    } else {
                      utils.tools.alert('服务器错误', {
                        timer: 1200,
                        type: 'warning'
                      });
                    }
                  }
                }
              });
            }
          }
          return result;
        })
        .on('tokenfield:edittoken', function (e) {
          if (e.attrs.label !== e.attrs.value) {
            // var label = e.attrs.label.split(' (')
            // e.attrs.value = label[0] + '|' + e.attrs.value
          }
        })
        .on('tokenfield:removedtoken', function (e) {
          var data = e.attrs.value;
          if ($proId && $proId !== '') {
            utils.postAjax(unTagUrl, {
              name: data
            }, function (res) {
              if (res === -1) {
                utils.tools.alert("网络问题，请稍后再试", {
                  timer: 1200,
                  type: 'warning'
                });
              }
              if (typeof res === 'object') {
                if (res.errorCode === 200) {
                  console.log('tag ' + data + 'removed');
                } else {
                  if (res.moreInfo) {
                    utils.tools.alert(res.moreInfo, {
                      timer: 1200,
                      type: 'warning'
                    });
                  } else {
                    utils.tools.alert('服务器错误', {
                      timer: 1200,
                      type: 'warning'
                    });
                  }
                }
              }
            });
          }
        }).tokenfield();
      if ($proId && $proId !== '') {
        utils.postAjax(getTagsUrl, null, function (res) {
          if (res === -1) {
            utils.tools.alert("网络问题，请稍后再试", {
              timer: 1200,
              type: 'warning'
            });
          }
          if (typeof res === 'object') {
            if (res.errorCode === 200) {
              tags = res.data;
              const tagStrs = tags.map(function (item) {
                return item.name;
              });
              console.log(tagStrs);
              $tagsInput.tokenfield('setTokens', tagStrs);
              tagInited = true;
            } else {
              if (res.moreInfo) {
                utils.tools.alert(res.moreInfo, {
                  timer: 1200,
                  type: 'warning'
                });
              } else {
                utils.tools.alert('服务器错误', {
                  timer: 1200,
                  type: 'warning'
                });
              }
            }
          }
        });
      }
    })();

    function setProCateForm(data) {
      var strArray = [];
      var dataLength = data.data.length;
      for (var i = 0; i < dataLength; i++) {
        if ($proCateId && ($proCateId.indexOf(data.data[i].id) >= 0)) {
          // 只能选择叶子节点
          if (data.data[i].isleaf == '0') {
            strArray.push('<option value=' + data.data[i].id +
              ' selected="selected" disabled="">' + data.data[i].name +
              '</option>');
          } else {
            strArray.push('<option value=' + data.data[i].id +
              ' selected="selected">' + data.data[i].name + '</option>');
          }
          $(".product-Category .select2-selection__rendered").text(
            data.data[i].name);
        } else {
          if (data.data[i].isleaf == '0') {
            strArray.push('<option value=' + data.data[i].id + ' disabled="">' +
              data.data[i].name + '</option>');
          } else {
            strArray.push('<option value=' + data.data[i].id + '>' +
              data.data[i].name + '</option>');
          }
        }
      };
      $('.product-Category').append(strArray.join(''));
      $('.product-Category').trigger("chosen:updated.chosen");
      /** 初始化选择框控件 **/
      $('.product-Category').select2({
        minimumResultsForSearch: Infinity,
        width: '35%'
      });
    }

    function setProBrandForm(data) {
      let brandSelect = $(".product-brand");
      var strArray = [];
      var dataLength = data.list.length;
      if ($proId) {
        proData.getProductBrandList($proId, selectedBrandList => {
          let selectedBrand = '';
          if (selectedBrandList.length !== 0) {
            selectedBrandList.forEach(function (element, index, array) {
              selectedBrand += element.id + ',';
            });
          }
          for (var i = 0; i < dataLength; i++) {
            if (selectedBrand && (selectedBrand.indexOf(data.list[i].id) >=
                0)) {
              strArray.push('<option value=' + data.list[i].id +
                ' selected="selected">' + data.list[i].name +
                '</option>');
              $(".product-brand .select2-selection__rendered").text(
                data.list[i].name);
            } else {
              strArray.push('<option value=' + data.list[i].id + '>' +
                data.list[i].name + '</option>');
            }
          }
          brandSelect.append(strArray.join(''));
          brandSelect.trigger("chosen:updated.chosen");
          /** 初始化选择框控件 **/
          brandSelect.select2({
            width: '35%'
          });
        }, utils.tools.alert)
      } else {
        for (var i = 0; i < dataLength; i++) {
          strArray.push('<option value=' + data.list[i].id + '>' +
            data.list[i].name + '</option>');
        }
        brandSelect.append(strArray.join(''));
        brandSelect.trigger("chosen:updated.chosen");
        /** 初始化选择框控件 **/
        brandSelect.select2({
          width: '35%'
        });
      }
    }

      function setSupplierForm(selectedSupplier, data) {
        let select = $(".product-supplier");
        let suppliers = [];
        data.list = data.list.filter(element => {
          return true;
        });
        data.list.forEach(element => {
          suppliers.push({
            id: element.id,
            text: element.name,
          })
        });
        /** 初始化选择框控件 **/
        select.select2({
          width: '35%',
          minimumResultsForSearch: Infinity,
          data: suppliers,
        });

      if ($proId && selectedSupplier) {
        select.val(selectedSupplier);
      }
      select.trigger("change");
    }

    function setWarehouseForm(selectedWarehouse, data) {
      let select = $(".product-ware-house");
      var strArray = [];
      var dataLength = data.list.length;
      for (var i = 0; i < dataLength; i++) {
        strArray.push('<option value=' + data.list[i].id + '>' +
          data.list[i].name + '</option>');
      }
      select.append(strArray.join(''));
      select.trigger("chosen:updated.chosen");
      /** 初始化选择框控件 **/
      select.select2({
        width: '35%',
        minimumResultsForSearch: Infinity,
      });

      if ($proId && selectedWarehouse) {
        select.val(selectedWarehouse);
        select.trigger("change");
      }
    }

    $("[name=logisticsType]").change(function () {
      var typeValue = $(this).val();
      if (typeValue == 'TEMPLATE') {
        $("#uniformValueDiv").hide();
        $("#templateValueDiv").show();
      } else {
        $("#uniformValueDiv").show();
        $("#templateValueDiv").hide();
      }
    });

    $("[name=skuType]").change(function () {
      var typeValue = $(this).val();
      if (typeValue == 'uniformSku') {
        $("#muliSkuDiv").hide();
        $("#add_multi_sku_btn").hide();
        $("#uniformSkuDiv").show();
      } else {
        $("#muliSkuDiv").show();
        $("#add_multi_sku_btn").show();
        $("#uniformSkuDiv").hide();
      }
    });

    $('input[name=setScale]').on('change', function () {
      var choose = $(this).val();
      if (choose === 'true') {
        $('#scaleSetting').show();
      } else {
        $('#scaleSetting').hide();
        $('#yundouScale').val('');
      }
    });

    /**
     * 动态计算德分
     */
    $('#conversionPrice,#productPrice').on('change', function () {
      let conversionPriceValid = $('#conversionPrice').valid();
      let productPriceValid = $('#productPrice').valid();
      if (!conversionPriceValid || !productPriceValid) {
        return;
      }
      let conversionPrice = $("#conversionPrice").prop("value");
      let price = $("#productPrice").prop("value");
      let paresedPrice = Number.parseFloat(price);
      let paresedConverPrice = Number.parseFloat(conversionPrice);
      let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
      console.log(result);
      $("#deductionDPoint").prop("value", ((Number.parseFloat(price) -
        Number.parseFloat(conversionPrice)) * 10).toFixed(2));
    });

    let registerSkuPriceListener = function () {
      /**
       * 动态计算sku中的德分
       */
      $('.conversionPrice,.productPrice').off("change").on('change', function () {
        let tr = $(this).parents(".sku_cell");
        let conversionPriceValid = tr.find(".conversionPrice").valid();
        let priceValid = tr.find(".productPrice").valid();
        if (!conversionPriceValid || !priceValid) {
          return;
        }
        let conversionPrice = tr.find(".conversionPrice").prop("value");
        let price = tr.find(".productPrice").prop("value");
        let paresedPrice = Number.parseFloat(price);
        let paresedConverPrice = Number.parseFloat(conversionPrice);
        let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
        tr.find(".deductionDPoint").prop("value",
          result === 0 ? 0.00 : result);
      });
    };

    /*
      判断sku唯一性
    */
    //  let checkMultiSkuUnique = function () {
    //  $(".skuCode").on("change",function(){
    //    console.log("i am moveing!!!");
    //    $.ajax({
    //     url: `${window.host}/product/sku/exit/${code}`,
    //     async: false,
    //   }
    //   ).done(
    //       data => {
    //         console.log(data.data);
    //       }
    //   )
    //  })
    //  }
    // -------------------------------  以下为商品sku规格使用的js  -------------------------------
    //var size = ['XXXL', "XXL", 'L', "M", "S"];
    //var color = ["红色", "黄色", "橘色", "黑色", "白色"];
    //var sizes = {
    //   "颜色": color,
    //    "尺码": size
    //}
    xlsxUpload.addExcelUploadEventListener($("#excel_file"));
    // 缓存所有商品规格属性名称与id关系,如{"颜色": "1acb","大小": "23cd"}
    var attributes = {};
    // 缓存所有商品规格属性值名称与id关系,如{"白色": "1acb","M码": "23cd"}
    var attributeItems = {};
    var sizes = {};
    var tabledata = []
    var selectedArr = {};
    var hasdid = {};
    var hastext = [];
    /***
     * Skumodel 为生成规格类
     * title为规格名字  string  例：尺码
     * times将要生成的规格项目  Array  例如：尺码：xxl,xl,m,s
     * dataid为产生型号的标识最外层元素上的id   string
     * **/
    var dropzoneInstance;
    var attributeDropZoneIndexMap = new Map();
    var Skumodel = function (title, items, dataId, index) {
      //最外层div+标题栏
      this.title = title || "";
      this.items = items || [];
      this.index = index || "";
      this.container = $('<div class="sku_container" id="' + dataId +
        '"><div class="sku_modellist_title">' + "规格-" + index + "：" + this.title +
        '</div></div>');
      //模型列表
      this.skumodels = $('<div class="sku_models"></div>');
      this.skumlist = $('<div class="sku_list"></div>')
      this.skuinputcon = $('<div class="sku_add"></div>');
      //输入框
      this.skuinput = $('<input type="text" placeholder="请输入型号属性">');
      //新建按钮
      this.addbtn = $('<a>新建</a>');
      this.init(this.items, title,dataId)
    }
    Skumodel.prototype = {
      //初始化显示组件
      init: function (items, title,dataId) {
        var html = ""
        for (var i = 0; i < items.length; i++) {
          html += `<span class="sku_item"><a data-id=${attributeItems[dataId+'-'+items[i]]} data-sku="${items[i]}"`;
          // 如果是修改商品，且有属性值是之前选中过的
          if (chooseAttributeAndItems && chooseAttributeAndItems.skuAttributes && chooseAttributeAndItems.skuAttributes.indexOf(attributeItems[dataId+'-'+items[i]]) != -1) {
            html += ' class="itemactive" ';
          }
          html += '>' + items[i] +
            '</a><i class="sku_item_close">×</i></span>';
        }

        //获取所有生成按钮
        this.skumlist.append($(html));
        this.skumodels.append(this.skumlist);
        this.container.append(this.skumodels);
        this.skuinputcon.append(this.skuinput);
        this.skuinputcon.append(this.addbtn);
        // 目前不需要商品新增页新增规格属性功能
        //this.skumodels.append(this.skuinputcon)
        $(".sku_modellist").append(this.container);
        //第一组sku添加图片
        if (attr_active && attr_active.length > 0 && title == attr_active[0]) {
          let firstSkuList = $(".sku_modellist>.sku_container:first .sku_list>.sku_item a"); //确认添加图片框位子
          let skuImgBox = $("<div  action = '#' class = 'skuImgBox dropzone' enctype = 'multipart/form-data' method = 'post'></div>");
          skuImgBox.insertBefore(firstSkuList);
        }
        this.bindEvent()
      },

      bindEvent: function () {
        var self = this;
        //点击新建按钮产生
        this.addbtn.click(function () {
          self.createItem();
        });
        this.activeItem();
        //点击删除按钮删除
        this.deleteItem();
        //控制删除符号
        //this.toggleCloseEle();
      },
      //创建sku子元素
      createItem: function () {
        var value = $.trim(this.skuinput.val())
        if (value.length <= 0) {
          layer.alert("请输入内容");
          return
        }
        if (sizes[this.title].indexOf(value) >= 0) {
          layer.msg("请勿重复创建")
          return;
        }
        sizes[this.title].push(this.skuinput.val())
        this.skumlist.append(
          $('<span class="sku_item"><a data-id="' + getIndex() + '">' +
            value + '</a><i class="sku_item_close">×</i></span>'))
        this.skuinput.val("")
      },
      //子元素是否选中事件
      activeItem: function () {
        this.skumlist.on("click", "a", function () {
          $(this).toggleClass("itemactive");
          tableContent()
        });
      },
      //监听删除元素
      deleteItem: function () {
        var self = this;
        this.skumlist.on("click", ".sku_item_close", function () {
          $(this).parent().remove();
          var text = $(this).parent().find("a").text();
          var textarr = sizes[self.title];
          textarr.splice(textarr.indexOf(text), 1);
          tableContent();
        });
      },
      //控制删除符号的显示
      toggleCloseEle: function () {
        //显示删除符号
        this.skumlist.on("mouseover", ".sku_item", function () {
          $(this).find(".sku_item_close").css({
            display: "inline-block"
          })
        });
        //显示删除符号
        this.skumlist.on("mouseout", ".sku_item", function () {
          $(this).find(".sku_item_close").css({
            display: "none"
          })
        });
      }
    };

    /****
     * SkuCell动态产生表格内容类
     * cellist为表格内部元素    Array   如["红色","xxl"]
     * dataid为行表格id 产生元素的唯一标识   string
     * ***/
    var SkuCell = function (celllist, dataid) {
      //每行表格的父元素
      this.cellcon = $('<div id="' + dataid +
        '" class="sku_cell clearfix"></div>');
      //价格输入
      this.moneyInput = $(
        '<div class="sku_t_title"><input type="number" min="0" class="productPrice" name = "productPrice"/></div>');
      //兑换价
      this.conversionPrice = $(
        '<div class="sku_t_title"><input type="number" min="0" class="conversionPrice skuInput" name = "conversionPrice"/></div>');
      //德分
      this.deductionDPoint = $(
        '<div class="sku_t_title"><input class="deductionDPoint" readonly="readonly" name = "deductionDPoint"/></div>');
      //积分
      this.point = $(
        '<div class="sku_t_title"><input class="point" min="0" type="number" name = "point"/ ></div>');
      //推广费
      this.promoAmt = $(
        '<div class="sku_t_title"><input class="promoAmt" min="0" type="number" name = "promoAmt"/></div>');
      //推广费
      this.serverAmt = $(
        '<div class="sku_t_title"><input class="serverAmt" min="0" type="number" name = "serverAmt"/></div>');
      //净值
      this.netWorth = $(
        '<div class="sku_t_title"><input class="netWorth" min="0" type="number" name = "netWorth"/></div>');
      //sku编码
      this.skuCode = $(
        '<div class="sku_t_title"><input class="skuCode" name = "skuCode" /><input type="hidden" class="tableSkuCodeOrigin" name="skuCodeOrigin"/></div>');
      //条形码
      this.barCode = $(
        '<div class="sku_t_title"><input class="barCode" type="number" name = "barCode" /></div>');
      //库存输入
      this.leftInput = $(
        '<div class="sku_t_title"><input  type="number" min="0" class="productAmount" name = "productAmount"/></div>');
      //安全库存输入
      this.secureInput = $(
        '<div class="sku_t_title"><input type="number" min ="0" class="secureAmount" name = "secureAmount"/></div>');

      //长度
      this.length = $(
        '<div class="sku_t_title"><input type="number" min ="0" class = "length" name = "length"/></div>');
      //宽度
      this.width = $(
        '<div class="sku_t_title"><input type="number" min ="0"  class = "width" name = "width"/></div>');
      //高度
      this.height = $(
        '<div class="sku_t_title"><input type="number" min ="0" class = "height" name = "height"/></div>');
      //重量
      this.weight = $(
        '<div class="sku_t_title"><input type="number" min ="0" class = "weight" name = "weight"/></div>');
      //装箱数
      this.numInPackage = $(
        '<div class="sku_t_title"><input type="number" min ="0" class = "numInPackage" name="numInPackage"/></div>');
      //操作
      this.operation = $(
        '<div class="sku_t_title operation"><span class = "delete">删除</span></div>');

      this.init(celllist);
      $(".productPrice,.conversionPrice,.deductionDPoint,.point,.promoAmt,.serverAmt,.netWorth,.skuCode,.barCode,.productAmount,.secureAmount,.length,.width,.height,.weight,.numInPackage").css(
        '-webkit-appearance', 'textarea');
    };
    SkuCell.prototype = {
      constructor: SkuCell,
      init: function (celllist) {
        var html = "";
        for (var i = 0; i < celllist.length; i++) {
          html += '<div class="sku_t_title spec">' + celllist[i] + '</div>'
        }
        this.cellcon.append($(html));
        this.cellcon.append(this.moneyInput);
        this.cellcon.append(this.conversionPrice);
        this.cellcon.append(this.deductionDPoint);
        this.cellcon.append(this.point);
        this.cellcon.append(this.promoAmt);
        this.cellcon.append(this.serverAmt);
        this.cellcon.append(this.netWorth);
        this.cellcon.append(this.skuCode);
        this.cellcon.append(this.barCode);
        this.cellcon.append(this.leftInput);
        this.cellcon.append(this.secureInput);
        this.cellcon.append(this.length);
        this.cellcon.append(this.width);
        this.cellcon.append(this.height);
        this.cellcon.append(this.weight);
        this.cellcon.append(this.numInPackage);
        this.cellcon.append(this.operation);

        $('.sku_tablecell').append(this.cellcon);
        registerSkuPriceListener();
        //checkMultiSkuUnique();
        $(".sku_tablecell").css({
          "width": tableWidth + 'px'
        });
        /* 表格删除按钮 */
        $(".sku_tablecell").find(".operation").off("click").on('click', function () {
          let skuDel = confirm("该组规格删除后,将不可恢复,确认要删除吗?");
          skuDel == true ? $(this).parents(".sku_cell").remove() : console.log("取消删除");
        });
      },
    };
    /****
     * 创建表格头部
     * arr 将要创建的表头内容 Arr ["颜色"，"尺码"]
     * **/
    var mustArr = [];
    var tableWidth;

    function createTablehead(arr) {
      mustArr = ["价格", "兑换价", "德分", "立省", "推广费", "服务费", "净值", "SKU编码", "条形码", "库存",
        "安全库存", "长度", "宽度", "高度", "重量", "装箱数", "操作"
      ];
      var relayArr = arr.concat(mustArr);
      html = "";
      for (var i = 0, len = relayArr.length; i < len; i++) {
        html += '<div class="sku_t_title">' + relayArr[i] + '</div>'
      }
      tableWidth = (attr_active.length + mustArr.length) * 100;
      $(".sku_tableHead").html("").append($(html));
      $(".sku_tableHead").css({
        "width": tableWidth + 'px'
      });
      createBulkOperation(skuOptArr);
      $(".sku_detail").next(".sku_detail").remove();
    }

    /* 创建批量操作 */
    //批量操作输入框数组
    var skuOptArr = ["价格", "兑换价", "立省", "推广费", "服务费", "净值", "库存", "安全库存", "长度", "宽度", "高度", "重量", "装箱数"];

    function createBulkOperation(arr) {
      let bulkOperationBox = $("<div class = 'sku_detail'><p class='detail_title'>规格明细：</p></div>");
      let detail_p = $("<p>批量操作</p>");
      let detail_list = $("<div class = 'detail_list'></div>");
      bulkOperationBox.append(detail_p);
      detail_p.after(detail_list);
      $(".sku_guige").after(bulkOperationBox);
      for (let i = 0; i < arr.length; i++) {
        // let detail_inp = $("<input placeholder = '"+arr[i]+"' type = 'number' min = '0'/>");
        let detail_inp = $("<div class='listDiv'><span>" + arr[i] + "</span><input type = 'number' min = '0'/></div>")
        detail_list.append(detail_inp);
      }
      let add_btn = $("<span>确认</span>");
      detail_list.append(add_btn);
      add_btn.on("click", function () {
        let optArr = [];
        let detaliInp = $(".detail_list input");
        detaliInp.each(function (index, value) {
          optArr.push(value.value);
        });
        optArr[0] != "" ? $(".productPrice").val(optArr[0]) : ""; //价格
        optArr[1] != "" ? $(".conversionPrice").val(optArr[1]) : ""; //兑换价
        optArr[2] != "" ? $(".point").val(optArr[2]) : ""; //立省
        optArr[3] != "" ? $(".promoAmt").val(optArr[3]) : ""; //推广费
        optArr[4] != "" ? $(".serverAmt").val(optArr[4]) : ""; //服务费
        optArr[5] != "" ? $(".netWorth").val(optArr[5]) : ""; //净值
        optArr[6] != "" ? $(".productAmount").val(optArr[6]) : ""; //库存
        optArr[7] != "" ? $(".secureAmount").val(optArr[7]) : ""; //安全库存
        optArr[8] != "" ? $(".length").val(optArr[8]) : ""; //长度
        optArr[9] != "" ? $(".width").val(optArr[9]) : ""; //宽度
        optArr[10] != "" ? $(".height").val(optArr[10]) : ""; //高度
        optArr[11] != "" ? $(".weight").val(optArr[11]) : ""; //重量
        optArr[12] != "" ? $(".numInPackage").val(optArr[12]) : ""; //装箱数
        //如果价格和兑换价的数据格式符合标准，则计算德分
        if (optArr[0] != "" || optArr[1] != "") {
          calcDPoint();
        }
        //手动做表格内的数据校验
        $(".sku_tablecell input").valid();
      })
    }
    /***
     * 排列组合计算出选择的规格型号的组合方式
     *
     * */
    function getResult() {

      var head = arguments[0][0];
      for (var i in arguments[0]) {
        if (i != 0) {
          head = group(head, arguments[0][i])
        }
      }
      tabledata = [];
      $(".sku_cell").each(function (index) {
        tabledata.push($(this).attr("id"));
      }).hide()
      for (var j = 0, len = head.length; j < len; j++) {
        var newcell = head[j]["datatext"].split(',')
        var dataid = head[j]["dataid"];
        if (tabledata.indexOf(dataid) < 0) {
          new SkuCell(newcell, dataid)
        } else {
          $("#" + dataid).show()
        }
      }
    };

    //组合前两个数据
    function group(first, second) {
      var result = [];
      for (var i = 0, leni = first.length; i < leni; i++) {
        for (var j = 0, len = second.length; j < len; j++) {
          result.push({
            dataid: first[i]["dataid"] + "-" + second[j]["dataid"],
            datatext: first[i]["datatext"] + "," + second[j]["datatext"]
          })
        }
      }
      return result
    }

    //动态产生一个索引，用于后续操作
    var i = 3;

    function getIndex() {
      return "d" + i++;
    };

    //控制表格内容
    function tableContent() {
      $(".sku_modellist .sku_models").each(function (index, ele) {
        var aa = $(this).find(".itemactive");
        selectedArr[index] = [];
        for (var i = 0; i < aa.length; i++) {
          selectedArr[index][i] = {};
          selectedArr[index][i]["dataid"] = $(aa[i]).attr("data-id");
          selectedArr[index][i]["datatext"] = $(aa[i]).text();
        }
      })
      getResult(selectedArr);
    }

    $(function () {
      // 将系统所有规格属性初始化到界面上
      $.ajax({
        url: window.host + '/skuAttribute/findAllAttributes',
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (data) {
          if (data.errorCode == 200) {
            attributes = data.data;
            xlsxUpload.setSkuAttributeIds(data);
            $.each(attributes, function (i, item) {
              $(".sku_content_sku_list").append(
                "<span class='sku_item'><a data-id='" +
                item + "' data-sku='" +
                i + "'>" +
                i + "</a></span>");
            });
          } else {
            alert(data.moreInfo);
          }
        },
        error: function (state) {
          if (state.status == 401) {
            utils.tool.goLogin();
          } else {
            alert('服务器暂时没有响应，请稍后重试...');
          }
        }
      });

      // 初始化规格属性值
      $.ajax({
        url: window.host + '/skuAttribute/findAllItemsWithAttributeId',
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (data) {
          if (data.errorCode == 200) {
            attributeItems = data.data;
            xlsxUpload.setSkuAttributeItemIds(attributeItems);
          } else {
            alert(data.moreInfo);
          }
        },
        error: function (state) {
          if (state.status == 401) {
            utils.tool.goLogin();
          } else {
            alert('服务器暂时没有响应，请稍后重试...');
          }
        }
      });

      // 初始化规格属性与其对应所有属性值
      $.ajax({
        url: window.host + '/skuAttribute/findAttributeAndItems',
        type: 'POST',
        dataType: 'json',
        async: false,
        success: function (data) {
          if (data.errorCode == 200) {
            sizes = data.data;
          } else {
            alert(data.moreInfo);
          }
        },
        error: function (state) {
          if (state.status == 401) {
            utils.tool.goLogin();
          } else {
            alert('服务器暂时没有响应，请稍后重试...');
          }
        }
      });

      // 如果是修改商品，则需要处理之前选中的商品规格属性
      if ($proId) {
        proData.getProductInfo($proId, function (data) {
          if (typeof (data) === 'object') {
            if (data.errorCode == 200) {
              let product = data.data;
              $skus = product.skus;
              let skuAttributes = product.skuAttributes;
              skuAttributes.forEach(function (element, index) {
                let skuItem = $(".sku_list").find(`.sku_item a[data-id=${element.id}]`);
                skuItem.addClass("itemactive");
                skuItem.after(`<span class="sku_index">${index+1}</span>`)
              });
              console.log($skus);
            }
          }
        });

        $.ajax({
          url: window.host +
            '/skuAttribute/getProductAttributeAndItems?productId=' +
            $proId,
          type: 'POST',
          dataType: 'json',
          async: false,
          success: function (data) {
            if (data.errorCode == 200) {
              chooseAttributeAndItems = data.data;
            } else {
              alert(data.moreInfo);
            }
          },
          error: function (state) {
            if (state.status == 401) {
              utils.tool.goLogin();
            } else {
              alert('服务器暂时没有响应，请稍后重试...');
            }
          }
        });
        console.log(chooseAttributeAndItems);
        // 如果商品有选择规格属性，则显示出之前选中的商品规格属性
        if (chooseAttributeAndItems.productAttributes) {
          $.ajax({
            url: window.host + '/skuAttribute/findAllAttributeAndItemsVO',
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (data) {
              if (data.errorCode == 200) {
                let datas = data.data;
                console.log(datas);
                let arrs = [];
                let skuModels=[];
                // 如果规格是之前选中的，则显示
                let index = 1;
                $.each(datas, function (i, item) {
                  var skuAttributeId = item.id;
                  var text = item.name;
                  if (chooseAttributeAndItems.productAttributes.indexOf(
                      skuAttributeId) != -1) {
                    var arr = sizes[text] || [];
                    sizes[text] = arr;
                    //创建规格
                    skuModels.push({
                      text:text,
                      arr:arr,
                      skuAttributeId:skuAttributeId,
                      items:item.items
                    });
                    arrs.push(text);
                    index++;
                  }
                });
                skuModels.sort(function(v1,v2){
                  let selectedAttributeList=chooseAttributeAndItems.productAttributes;
                  let index1=selectedAttributeList.indexOf(v1.skuAttributeId);
                  let index2=selectedAttributeList.indexOf(v2.skuAttributeId);
                  return index1-index2;

                });
                skuModels.forEach(function (value, index) {
                  new Skumodel(value.text, value.arr, value.skuAttributeId, skuModels.indexOf(value)+1);
                });

                initDropzone();
                // 初始化表头
                clearTable(arrs);
                // 根据之前选中的规格属性，初始化表格行数据
                tableContent();
                // 根据sku信息，初始化表格行内的价格，库存等数据
                // 先按照sku的顺序重新对表格内进行排序
                var skuRows = '';
                $.each($skus, function (i, item) {
                  if(item.amount!==-1001){//排查假数据
                    var skuAttribute = item.attributes;
                    var skuRow = $(`.sku_tablecell #${skuAttribute}`);
                    skuRows += skuRow.prop("outerHTML");
                  }
                });
                $(".sku_tablecell").html(skuRows);
                /* 表格删除按钮 */
                $(".sku_tablecell").find(".operation").on('click', function () {
                  let skuDel = confirm("该组规格删除后,将不可恢复,确认要删除吗?");
                  skuDel == true ? $(this).parents(".sku_cell").remove() : console.log("取消删除");
                });
                // 初始化表格行内的价格，库存等数据
                $.each($skus, function (i, item) {
                  if(item.amount===-1001) {//排查假数据
                    return ;
                  }
                  var skuId = item.id;
                  var skuPrice = item.originalPrice;
                  var deductionDPoint = item.deductionDPoint.toFixed(2);
                  var point = item.point;
                  var promotAmt = item.promoAmt;
                  var serverAmt = item.serverAmt;
                  var skuCode = item.skuCode;
                  var barCode = item.barCode;
                  var netWorth = item.netWorth;
                  var conversionPrice = Number.parseFloat(skuPrice) - Number.parseFloat(deductionDPoint) / 10;
                  var skuAmount = item.amount;
                  var length = item.length;
                  var width = item.width;
                  var height = item.height;
                  var weight = item.weight;
                  var numInPackage = item.numInPackage;
                  var skuImgUrl = item.skuImgUrl;
                  var skuSecureAmount = item.secureAmount;
                  var skuAttribute = item.attributes;
                  var skuRow = $(`.sku_tablecell #${skuAttribute}`);
                  skuRow.attr("sku_id", skuId);
                  skuRow.find(".productPrice").val(skuPrice);
                  skuRow.find(".conversionPrice").val(conversionPrice);
                  skuRow.find(".deductionDPoint").val(deductionDPoint);
                  skuRow.find(".point").val(point);
                  skuRow.find(".promoAmt").val(promotAmt);
                  skuRow.find(".serverAmt").val(serverAmt);
                  skuRow.find(".skuCode").val(skuCode);
                  skuRow.find(".tableSkuCodeOrigin").val(skuCode);
                  skuRow.find(".barCode").val(barCode);
                  skuRow.find(".netWorth").val(netWorth);
                  skuRow.find(".productAmount").val(skuAmount);
                  skuRow.find(".secureAmount").val(skuSecureAmount);
                  skuRow.find(".length").val(length);
                  skuRow.find(".width").val(width);
                  skuRow.find(".height").val(height);
                  skuRow.find(".weight").val(weight);
                  skuRow.find(".numInPackage").val(numInPackage);

                  console.log(skuImgUrl);
                  //恢复对应图片上传框里面的图片和key
                  let spec = item.spec;
                  if (spec) {
                    let firstSku = spec.split(",")[0];
                    let index = attributeDropZoneIndexMap.get(firstSku);
                    if ((index || index === 0) && item.skuImgUrl) {
                      dropzoneInstance.addImage({
                        fileName: "",
                        dataSuffix: "",
                        fileUrl: item.skuImgUrl,
                        size: 2000,
                        dataFile: item.skuImg
                      }, index);
                      attributeDropZoneIndexMap.delete(firstSku);
                    }
                  }

                  $(`.sku_item a[data-id=${item.attributes.split("-")[0]}]`).prev().find(".dz-preview").attr("data-img", item.skuImg);


                });
                registerSkuPriceListener();

              } else {
                alert(data.moreInfo);
              }
            },
            error: function (state) {
              if (state.status == 401) {
                utils.tool.goLogin();
              } else {
                alert('服务器暂时没有响应，请稍后重试...');
              }
            }
          });
        }

      }

      /***
       * 规格选择确认按钮事件回调
       */
      function skuConfirmCallBack(){
        //清空规格规格
        $(".sku_modellist").html("");
        var arrs = [];
        selectedArr = {}; //清空
        //获取被选中多级型号元素
        // $(".sku_content_sku_list .itemactive").each(function () {
        //   var text = $(this).text(); //选中元素的文字
        //   var dataid = $(this).attr("data-id"); //选中的元素上的参数用于创建规格时候的唯一标识
        //   var arr = sizes[text] || [];
        //   sizes[text] = arr;
        //   //创建规格
        //   new Skumodel(text, arr, dataid)
        //   arrs.push(text)
        // })
        for (let i = 0; i < attr_active.length; i++) {
          var text = attr_active[i]; //选中元素的文字
          var dataid = attributes[attr_active[i]]; //选中的元素上的参数用于创建规格时候的唯一标识
          var arr = sizes[text] || [];
          sizes[text] = arr;
          new Skumodel(text, arr, dataid, i + 1); //创建规格
          arrs.push(text);
        }
        initDropzone();
        //根据arrs数据判断出是否显示表格同时清空表格
        clearTable(arrs);
        $(".sku_content").hide();
      }
      //初始化xlsx模块
      xlsxUpload.setSkuModelInitCallBack(skuConfirmCallBack,attr_active);
      xlsxUpload.setSkuCellInitCallBack(tableContent);
      xlsxUpload.setdPointCalc(calcDPoint);

      //点击添加多级型号事件 layer弹出层
      $("#add_multi_sku").click(function () {
        //layer详细用法 http://www.layui.com/doc/modules/layer.html
        layer.open({
          type: 1,
          resize: false,
          title: "选择商品型号",
          area: ["600px", "256px"],
          btn: ["取消", "确定"],
          content: $(".sku_content"), //此处后放置到弹出层内部的内容
          yes: function (index, layero) { //取消按钮对应回调函数
            layer.close(index);
            $(".sku_content").hide();
          },
          btn2: skuConfirmCallBack,
        })
      });

      function clearTable(arrs) {
        if (arrs.length) {
          $(".sku_guige").show()
          $(".sku_table").show();
          $(".sku_tableHead").html('')
          $(".sku_tablecell").html("")
          //$(".sku_container .itemactive").toggleClass("itemactive")
          createTablehead(arrs);
        } else {
          $(".sku_table").hide()
          $(".sku_guige").hide()
        }
      }

      //弹窗中的新建sku
      $("#sku_addbtn").click(function () {
        var haveit = false;
        var value = $.trim($(".sku_input").val())
        if (value.length <= 0) {
          layer.alert("请输入内容");
          return
        }
        $(".sku_content_sku_list a").each(function () {
          if ($(this).text() == value) {
            layer.msg('新建的已存在,请勿重复创建');
            haveit = true;
            $(".sku_input").val("")
          }
        })
        if (haveit) {
          return;
        }
        var skuitem = '<span class="sku_item"><span class="sku_index"></span><a data-id="' + getIndex() +
          '>' + value + '</a><i class="sku_item_close">×</i></span>'
        $(".sku_content_sku_list").prepend(skuitem);
        $(".sku_input").val("")
      });
      //显示删除符号
      /**$(".sku_content_sku_list").on("mouseover", ".sku_item", function() {
              $(this).find(".sku_item_close").css({
                  display: "inline-block"
              })
          });
       //显示删除符号
       $(".sku_content_sku_list").on("mouseout", ".sku_item", function() {
              $(this).find(".sku_item_close").css({
                  display: "none"
              })
          });**/
      //删除添加的型号
      $(".sku_content_sku_list").on("click", ".sku_item_close", function () {
        $(this).parent().remove();
      })

      $(".sku_content_sku_list > .sku_item").on("click", "a", function (e) {
        let addAction = !$(this).hasClass('itemactive');
        if (addAction && attr_active.length > 2) {
          layer.alert('每件商品最多只能添加三种规格');
          return;
        }
        $(this).toggleClass("itemactive");

        if (addAction) {
          attr_active.push(e.target.dataset.sku); //点击push进数组
        } else {
          var index = attr_active.indexOf(e.target.dataset.sku);
          attr_active.splice(index, 1);
          $(this).siblings().remove();
        }
        $(".sku_content_sku_list .itemactive").each(function (idx, item) {
          let attribute = $(item).text();
          let index = attr_active.indexOf(attribute) + 1;
          let bro = $(item).siblings();
          if (bro.length > 0) {
            $(bro.get(0)).text(index);
          } else {
            $(item).parent().append("<span class = 'sku_index'>" + index + "</span>");
          }
        });
        console.log(attr_active);
      });
    })

    function initDropzone() {
      //初始化dropzone
      dropzoneInstance = multiUpload.create({
        dom: ".skuImgBox",
        maxFiles: 1,
      })
      //维护每个规格对应的dropzone实例的索引
      let imageBoxs = $(".skuImgBox");
      for (let i = 0; i < imageBoxs.length; i++) {
        let attribute = $(imageBoxs.get(i)).next().text();
        if (attribute) {
          attributeDropZoneIndexMap.set(attribute, i);
        }
      }
    }

    function calcDPoint(){
      let cells = $(".sku_table").find(".sku_cell");
      $.each(cells, function (index, value) {
        let element = $(value);
        let conversionPriceValid = element.find(".conversionPrice").valid();
        let priceValid = element.find(".productPrice").valid();
        if (!conversionPriceValid || !priceValid) {
          return;
        }
        let conversionPrice = element.find(".conversionPrice").prop("value");
        let price = element.find(".productPrice").prop("value");
        let paresedPrice = Number.parseFloat(price);
        let paresedConverPrice = Number.parseFloat(conversionPrice);
        let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
        element.find(".deductionDPoint").prop("value",
            result === 0 ? 0.00 : result);
      });
    }

  });