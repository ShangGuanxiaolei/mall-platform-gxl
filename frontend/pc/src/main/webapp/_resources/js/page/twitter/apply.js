define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-basic');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $dateRangeBasic.on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });


        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitter/apply/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#status").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    name:$("#name").val(),
                    phone:$("#phone").val(),
                    shopName:$("#shopName").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.applyTotal,
                        recordsFiltered: res.data.applyTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.applyTotal,
                        iTotalDisplayRecords:res.data.applyTotal
                    });
                });
            },
            columns: [{
                width: "220px",
                sClass: 'styled',
                title: "微信",
                render: function(data, type, row){
                    return '<img class="sm-goods-image" src="' + row.avatar + '" />' + row.userName;
                }
            },{
                width:"120px",
                title: '电话',
                data: 'phone',
                name: 'phone'
            },{
                width:"120px",
                title: '来源店铺',
                data: 'name',
                name: 'name'

            },{
                width:"160px",
                title: '申请时间',
                render: function (data, type, row) {
                    if (row.createdAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:"120px",
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if(value =='ACTIVE'){
                        return "已审核通过";
                    }else if(value =='FROZEN'){
                        return "已冻结";
                    }{
                        return "待审核";
                    }
                }
            },{
                width:"120px",
                sClass: 'styled text-center',
                align: 'right',
                title: '操作',
                render: function(data, type, row) {
                    return '<a href="javascript:void(0);" class="btn-sm btn btn-primary verifyBtn" data-toggle="popover" rowId="'+row.id + '">审核通过</a> ';
                }
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {
            $(".btn-search").on('click', function () {
                $datatables.search(status).draw();
            });

            $(".verifyBtn").on('click', function () {
                var applyId = $(this).attr('rowId');
                utils.postAjaxWithBlock($(document), window.host + '/twitter/apply/' + applyId, [] , function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("获取当前配送方式失败", {timer: 1200});
                    }
                });
            });


        }
});