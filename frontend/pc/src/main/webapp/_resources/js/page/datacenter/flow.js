define(['echartsNoAMD','echartsTheme','jquery', 'utils'], function(echarts, limitless, $, utils) {

    // 今日相关及本周图表数据取值
    var url = window.host + '/twitter/getSummary';
    var data = {
    };
    // 本周订单,推客,交易额,佣金数
    var amount;
    var commission;
    var order;
    var twitter;

    // 取数渲染页面
    utils.postAjax(url, data, function(result) {
        if (typeof(result) === 'object') {
        }
    });

debugger;
    var flow = echarts.init(document.getElementById('flow_lines'), limitless);
    var flow_options = {

        // Setup grid
        grid: {
            x: 40,
            x2: 20,
            y: 35,
            y2: 25
        },

        // Add tooltip
        tooltip: {
            trigger: 'axis'
        },

        // Add legend
        legend: {
            data: ['访客数', '浏览量', '跳失率', '人均浏览量'],
            textStyle: {
                fontSize: 16
            }
        },
        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            padding: 0,
            feature: {
                dataView: {
                    show: true,
                    readOnly: false,
                    title: '查看图表数据',
                    lang: ['查看图表数据', '关闭', '更新']
                },
                restore: {
                    show: true,
                    title: '刷新'
                },
                saveAsImage: {
                    show: true,
                    title: '另存为图片',
                    lang: ['保存']
                }
            }
        },
        // Enable drag recalculate
        calculable: true,

        // Hirozontal axis
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: [
                '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天'
            ]
        }],

        // Vertical axis
        yAxis: [{
            type: 'value'
        }],

        // Add series
        series: [
            {
                name: '访客数',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [120, 132, 101, 134, 90, 230, 210]
            },
            {
                name: '浏览量',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '跳失率',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '人均浏览量',
                type: 'line',
                stack: 'Total',
                itemStyle: {normal: {areaStyle: {type: 'default'},label: {
                    show: true,
                    textStyle: {
                        fontSize: 16
                    }
                }}},
                data: [320, 332, 301, 334, 390, 330, 320]
            }
        ]
    };
    flow.setOption(flow_options);




    // Resize charts
    // ------------------------------
    window.onresize = function () {
        setTimeout(function () {
            flow.resize();
        }, 200);
    }

});
