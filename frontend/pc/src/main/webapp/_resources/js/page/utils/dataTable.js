/**
 * 封装表格基本参数
 */
define(['jquery', 'utils', 'ramda', 'datatables'], function ($, utils, R) {

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无数据"
    }
  });

  const basicOption = {
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ordering: false,
    sortable: false,
    scrollX: true,
    scrollCollapse: true,
    fixedColumns: {
      leftColumns: 0,
      rightColumns: 1
    },
    rowId: 'id',
    select: {
      style: 'multi'
    }
  };

  class DataTable {
    /**
     * 构造函数
     * @param params
     * @param params.lazyInit 懒加载, 若指定该属性则在第一次刷新时才加载
     * @param params.dom
     * @param params.url
     * @param params.option
     * @param params.reqParams //请求参数
     * @param params.drawBack // 完成回掉
     * @param params.columns // 表格列
     * @param params.autoFit // 表格是否子适应
     * @param params.showSelector // 是否需要选择框
     */
    constructor(params) {
      this.$dom = $(params.dom);
      const option = $.extend({}, basicOption, params.option || {});

      // 部分界面需要关闭自适应大小
      if (params.autoFit === false) {
        option.fixedColumns = 0;
        option.scrollX = false;
        option.scrollCollapse = false;
      }

      if (params.reqParams) {
        this.reqParams = params.reqParams;
      }

      // 如果没有url参数则作为展示表格
      let isView = true;
      if (params.url && params.url !== '') {
        isView = false;
        option.ajax = (data, callback) => {
          const reqParams = $.extend({
            size: data.length,
            page: data.start / data.length,
          }, this.reqParams || {});
          $.get(params.url, reqParams, function (res) {
            if (res.errorCode !== 200) {
              utils.tools.alert('数据加载失败');
              return;
            }
            callback({
              recordsTotal: res.data.total,
              recordsFiltered: res.data.total,
              data: res.data.list,
              iTotalRecords: res.data.total,
              iTotalDisplayRecords: res.data.total
            });
          })
        };
      }
      if (isView) {
        option.serverSide = false;
        option.paging = false;
        option.searching = false;
      }

      const showSelector = params.showSelector;
      // 列头加上选择框
      if (showSelector && showSelector === true
          && params.columns) {
        params.columns.unshift({
          title: '<span>全选<label class="checkbox"><input name="checkAll" type="checkbox" class="styled"></label></span>',
          name: 'check',
          width: 15,
          render: function (data, type, row) {
            return '<label class="checkbox"><input name="checkColumn" type="checkbox" class="styled" value="'
                + row.id + '"></label>';
          }
        });

        $(document).on('click', 'input[name=checkAll]', function () {
          const isSelfChecked = $(this).is(':checked');
          $('input[name=checkColumn]').prop('checked', isSelfChecked);
        })
      }

      option.columns = params.columns;
      option.drawCallback = params.drawBack || function () {
      };
      this.isView = isView;
      this.option = option;
      if (!params.lazyInit) {
        this.init();
      }
    }

    init() {
      this.$tableInstence = this.$dom.DataTable(this.option);
      this.inited = true;
    }

    /**
     * 更新参数
     * @param params
     * @returns {DataTable}
     */
    update(params) {
      this.reqParams = params;
      return this;
    }

    /**
     * 刷新当前页
     */
    refresh(reqParams) {
      if (this.isView) {
        throw Error('展示表格无法刷新');
      }
      if (!this.inited) {
        this.init();
        return;
      }
      if (reqParams) {
        this.reqParams = $.extend(this.reqParams, reqParams);
      }
      return new Promise((resolve => {
        resolve(this.$tableInstence.search('').draw('page'));
      }));
    }

    /**
     * 刷新全部
     */
    refreshAll(reqParams) {
      if (this.isView) {
        throw Error('展示表格无法刷新');
      }
      if (!this.inited) {
        this.init();
        return;
      }
      if (reqParams) {
        this.reqParams = $.extend(this.reqParams, reqParams);
      }
      this.$tableInstence.search('').draw();
      return this;
    }

    /**
     * 列表获取表格全部数据
     * @param mapFunc 数据映射函数
     */
    getDataAll(mapFunc = x => x) {
      const data = this.$tableInstence.rows().data();
      return Array.prototype.map.call(data, mapFunc);
    }

    /**
     * 为当前表格添加一列
     */
    addRow(row, callback) {
      this.$tableInstence.row.add(row).draw();
      if (callback) {
        callback();
      }
      return this;
    }

    /**
     * 根据函数修改行数据
     * @param match 查找函数
     * @param modify 修改函数
     */
    modifyRow(match, modify) {
      this.$tableInstence.rows().every(function () {
        const d = this.data();
        if (match && match(d) === true) {
          if (modify) {
            const newRow = modify(d);
            this.data(newRow);
          }
        }
      });
      return this;
    }

    /**
     * 根据行节点来匹配修改
     * @param match
     * @param modify
     * @returns {DataTable}
     */
    modifyRowByNode(match, modify) {
      this.$tableInstence.rows().every(function () {
        const node = this.node();
        const data = this.data();
        if (match && match(node) === true) {
          if (modify) {
            const newRow = modify(data);
            this.data(newRow);
          }
        }
      });
      return this;
    }

    /**
     * 删除某一行
     * @param selector
     */
    removeRow(selector) {
      this.row(selector)
      .remove()
      .draw();
    }

    /**
     * 删除所有选中行
     */
    removeRows(filter = () => true) {
      const _this = this;
      $('input[name=checkColumn]').each(function (i, item) {
        if(filter(item)) {
          _this.removeRow($(item).parents('tr'));
        }
      });
    }

    /**
     * 选中的行数
     * @returns {jQuery}
     */
    checkedRowNum() {
      return $('input[name=checkColumn]:checked').length;
    }

    /**
     * 获取表格行对象
     * @param selector 行选择器
     * 参考 https://datatables.net/reference/type/row-selector;
     * https://datatables.net/examples/ajax/null_data_source.html
     * @returns {*}
     */
    row(selector) {
      return this.$tableInstence.row(selector);
    }

    /**
     * 获取表格行数据
     * @param selector
     * @returns {*}
     */
    rowData(selector) {
      return this.row(selector).data();
    }

    /**
     * 返回表格行数
     */
    rowCount() {
      if (!this.inited) {
        return 0;
      }
      return this.$tableInstence.data().count();
    }

  }

  return {
    /**
     * 构造一个新的表格实例
     * @param params
     * @returns {DataTable}
     */
    create: function (params) {
      return new DataTable(params);
    }
  }

});
