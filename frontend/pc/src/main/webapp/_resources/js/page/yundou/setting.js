define(['jquery', 'utils', 'form/validate', 'switch', 'datatables', 'blockui','bootbox', 'select2'],
    function($, utils, validate, bswitch, datatabels, blockui, bootbox,select2) {

        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var twitterSettingForm = {
            form: $('#yundouSettingForm'),

            amount: $("#yundouSettingForm input[name=amount]"),
            id: $("#yundouSettingForm input[name=settingId]"),

            saveFamilyCardBtn: $('#saveFamilyCardBtn'),
            url: window.host + '/yundou/saveSetting',

            rules: {
                amount: {
                    required: true
                }
            },

            messages: {
                amount: {
                    required: '请输入默认积分抵扣比例'
                }
            },

            save: function() {
                var amount = this.amount.val();
                var id = this.id.val();


                var familyCardSettings = {};

                var familyCardSettings = {
                    'amount': amount ,
                    'id': id
                };

                utils.postAjaxWithBlock($(document), this.url, familyCardSettings, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                                //location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            },

            init: function() {

                $(this.saveFamilyCardBtn).on('click',function() {
                    $(this.form).submit();
                });
                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        twitterSettingForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        twitterSettingForm.init();
    });