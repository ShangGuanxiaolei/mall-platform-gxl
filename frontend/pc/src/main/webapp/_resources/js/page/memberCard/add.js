define(['jquery', 'utils', 'blockui', 'spectrum', 'form/validate', 'datatables', 'jquerySerializeObject', 'fileinput_zh', 'fileinput'], function ($, utils, blockui, spectrum, validate) {

    // 会员卡表单
    const $form = $('.member-card-form');
    // 表单提交
    const $btnSubmit = $('.btn-submit');
    // color-selector
    const $cardColor = $("#card-color");
    // 图片上传
    const $fileUpload = $('#background-file');

    // 背景图片
    const $background = $('#background');

    const saveUrl = '/sellerpc/member/card/save';

    const fileUploadManager = {
            uploadOption: {
                uploadUrl: '/sellerpc/member/card/upload',
                showCaption: false,
                showUpload: false,
                uploadAsync: true,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"]
            },
            init: function () {
                var defaultImg = $background.val();
                if (defaultImg && defaultImg.toString().startsWith('http')) {
                    // 如果已经有图片则显示默认
                    this.uploadOption = $.extend({
                        showPreview: true,
                        initialPreview: [ // 预览图片的设置
                            "<img src= '" + defaultImg + "' class='file-preview-image'>"]
                    }, this.uploadOption);
                }
                // 初始化文件上传控件
                $fileUpload.fileinput(this.uploadOption);
                // 上传之前
                $fileUpload.on('filepreajax', function () {
                    $btnSubmit.addClass('disabled');
                });

                // 选中后立即上传
                $fileUpload.on('filebatchselected', function () {
                    $fileUpload.fileinput('upload');
                });

                // 上传成功
                $fileUpload.on('fileuploaded', function (event, data) {
                    var res = data.response;
                    if (res.errorCode && res.errorCode === 200) {
                        $background.val(res.img);
                        $btnSubmit.removeClass('disabled');
                    } else {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                });
                return this;
            },
            destroy: function () {
                $fileUpload.fileinput('destroy');
                return this;
            },
            refresh: function () {
                this.destroy().init();
            }
        };

    // validate 表单配置
    const formProfile = {
        rules: {
            name: 'required',
            level: {
                required: true,
                pattern: /^[0-9]*$/
            },
            discount: {
                required: true,
                pattern: /^[0-9]?(.[0-9]{1,2})?$/
            },
            remark: 'required'
        },
        messages: {
            name: {
                required: '请输入会员卡名称'
            },
            level: {
                required: '请输入会员卡等级',
                pattern: '请输入数字'
            },
            discount: {
                required: '请输入折扣',
                pattern: '请输入正确的折扣格式'
            },
            remark: {
                required: '请输入使用须知'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            globalFunctions.submitForm(form);
        }
    };

    const globalFunctions = {
        submitForm: function submitForm(form) {
            if ($btnSubmit.hasClass("disabled")) {
                utils.tools.alert("请等待文件上传完成", {timer: 1200, type: 'warning'});
                return;
            }
            var data = $(form).serializeObject();
            // 设置默认颜色
            if (!data.bgColor) data.bgColor = '#008000';
            if (!data.freeDelivery) data.freeDelivery = false;
            utils.getJson(saveUrl, data, function (res) {
                if (res.errorCode === 200) {
                    if (res.data) {
                        utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                    } else {
                        utils.tools.alert("保存失败", {timer: 1200, type: 'warning'});
                    }
                } else {
                    utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                }
            });
        }
    };

    // 初始化颜色选择框
    $cardColor.spectrum({
        showPaletteOnly: true,
        showPalette: true,
        hideAfterPaletteSelect: true,
        color: 'green',
        palette: [
            ['black', 'white', 'blanchedalmond',
                'rgb(255, 128, 0);', 'hsv 100 70 50'],
            ['red', 'yellow', 'green', 'blue', 'violet']
        ],
        change: function (color) {
            $cardColor.val(color.toHexString());
            console.log(color.toHexString());
        }
    });

    // 文件上传控件初始化
    fileUploadManager.init();
    // 参数校验初始化
    validate($form, formProfile);
});
