define(['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: true,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            dateLimit: { days: 600 },
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoApply: true,
            opens: 'left',
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        $('.daterange-basic').daterangepicker(options);
        $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
            $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
            $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
        });


        // 时间区间默认为空
        options.startDate = '';
        options.endDate   = '';
        $("input[name='startDate']").val('');
        $("input[name='endDate']").val('');
        $('.daterange-basic').val('');

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            ordering: false,
            sortable: false,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/userAgent/listCommission", {
                    size: data.length,
                    page: (data.start / data.length),
                    startDate: options.startDate != '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate:options.endDate != '' ? options.endDate.format('YYYY-MM-DD') : '',
                    sellerName:$("#sellerName").val(),
                    sellerPhone:$("#sellerPhone").val(),
                    buyerName:$("#buyerName").val(),
                    buyerPhone:$("#buyerPhone").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId: 'commissionId',
            columns: [{
                width: "15px",
                title: '<label class="checkbox"><input name="checkAll" type="checkbox" class="styled"></label>',
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkCommissions" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.commissionId+'"></label>';
                    return str;
                }
            },{
                width:"120px",
                title: '订单号',
                data: 'orderNo',
                name: 'orderNo'
            },{
                width:"80px",
                title: '订单价格',
                data: 'price',
                name: 'price'
            },{
                width:"80px",
                title: '订单数量',
                data: 'amount',
                name: 'amount'

            },{
                width:"80px",
                title: '单盒返利',
                data: 'rate',
                name: 'rate'
            },{
                width:"120px",
                title: '下单经销商',
                data: 'buyerName',
                name: 'buyerName'

            },{
                width:"120px",
                title: '经销商电话',
                data: 'buyerPhone',
                name: 'buyerPhone'

            },{
                width:"120px",
                data: 'buyerType',
                name: 'buyerType',
                title: '下单经销商级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.buyerType;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"100px",
                title: '收利人',
                data: 'name',
                name: 'name'

            },{
                width:"120px",
                title: '收利人电话 ',
                data: 'phone',
                name: 'phone'

            },{
                width:"100px",
                data: 'type',
                name: 'type',
                title: '收利人级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.type;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"120px",
                title: '返利金额 ',
                data: 'fee',
                name: 'fee'

            },{
                width:"120px",
                title: '返利时间',
                render: function (data, type, row) {
                    if (row.createdAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:"120px",
                title: '发放状态',
                render: function (data, type, row) {
                    return row.offered ? '已发放' : '未发放';
                }
            },{
                width: "100px",
                title: '发放佣金',
                render: function (data, type, row) {
                    var text = row.offered ? '取消发放' : '确认发放';
                    var html = '';
                    html += '<a href="javascript:void(0);" rowId="'
                        + row.commissionId
                        + '" fid="change_status" class="change_status" offered="' + row.offered + '"><i class="icon-coin-yen" >' +
                        '</i>' + text + '</a>';
                    return html;
                }
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

           // Handle click on "Select all" control
        $('input[name=checkAll]').on('click', function(){
      // Check/uncheck all checkboxes in the table
            var rows = $datatables.rows({ 'search': 'applied' }).nodes();
            $('input[name=checkCommissions]', rows).prop('checked', this.checked);
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            $(".btn-search").on('click', function () {
                $datatables.search('').draw();
            });

            $('.change_status').on('click', function () {
                var offered = $(this).attr('offered');
                var ids = $(this).attr('rowId');
                var data = {
                    ids: ids,
                    offered: offered !== 'true'
                };
                updateOffered(data);
            });

            tableRoleCheck('#guiderUserTable');

        }

        function updateOffered(data, callback) {
            var url = window.host + '/userAgent/updateOffered';
            utils.postAjax(url, data, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data === true) {
                        // page参数在刷新时记住当前页码，否则会回到首页
                        $datatables.search('').draw('page');
                        if (callback) {
                            callback();
                        }
                        utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                    } else {
                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                    }
                }
            })
        }

        $(".exportBtn").on('click', function() {
            $('#exportForm').submit();
        });

        $('.offeredBtn').on('click', function () {
            var idsArr = [];
            $("[name=checkCommissions]:checkbox").each(function(){ //遍历table里的全部checkbox
                //如果被选中
                if($(this).prop("checked")){
                    idsArr.push($(this).val());
                }
            });
            if (idsArr.length === 0) {
                utils.tools.alert('请先勾选要操作的订单', {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                ids: idsArr.join(','),
                offered: true
            };
            updateOffered(data, function () {
                // 成功后将checkAll全选框取消
                $('input[name=checkAll]').attr('checked', false);
            });
        });

        buttonRoleCheck('.hideClass');

});