/**
 * Created by quguangming on 16/5/24.
 */
define(['jquery','utils','datatables','blockui','bootbox'], function($,utils,datatabels,blockui) {


    $(".btn-admin-add").on('click', function() {
        location.href = '/sellerpc/merchant/admin/add';
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    var _userId = null;

    var merchantId = '';

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            // make a regular ajax request using data.start and data.length
            $.get('/sellerpc/merchant/admin/list', {
                size: data.length,
                page: (data.start / data.length) +1,
                keyword: data.search.value,
                orderIndex: data.order[0].column,
                isAsc: data.order ? data.order[0].dir == 'asc':false
            }, function(res) {
                // map your server's response to the DataTables format and pass it to
                // DataTables' callback
                if (!res.data.rows) {
                    res.data.rows = [];
                }
                _userId = res.data.userId;
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.rows,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "loginname",
                width: "180px",
                orderable: true,
                name:"loginname"
            }, {
                data: "name",
                width: "200px",
                orderable: true,
                name:"name"
            }, {
                data: "phone",
                width: "180px",
                orderable: true,
                name:"phone"
            }
            /**, {
                data: "wechat",
                orderable: true,
                width: "180px",
                name:"wechat"
            }**/
             , {
                orderable: true,
                width: "320px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"created_at"
            },
            /**{
                data: "rolesDesc",
                orderable: true,
                width: "180px",
                name:"rolesDesc"
            },**/
            {
                width: "220px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="edit" style="margin-right: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    if ( _userId != row.id ) {
                        /**html += '<a hhref="javascript:void(0);" class="settings"  style="margin-right: 10px;" rowId="'+row.id+'"  ><i class="icon-checkbox-checked"></i>角色设置</a>';**/
                        html += '<a href="javascript:void(0);" class="del" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';
                    }

                    return html;
                }
            }
        ],
        select: {
            //style: 'os',
            //selector: 'td:first-child'
            style: 'multi'
        },
        order: [[0, 'asc']],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $roledatatables = $('#xquark_select_roles_tables').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get("/sellerpc/merchantRole/allList", {
                keyword: '',
                keyword: data.search.value,
                merchantId: merchantId,
                pageable: false
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "15px",
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.roleName+'"></label>';
                    return str;
                }
            },
            {
                data: "roleName",
                width: "120px",
                orderable: false,
                name:"roleName"
            },{
                data: "roleDesc",
                width: "120px",
                orderable: false,
                name:"roleDesc"
            }
            , {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId")
            window.location.href="/sellerpc/merchant/admin/edit?merchantId="+id;
        });

        $(".settings").on("click",function(){
            merchantId =  $(this).attr("rowId");
            $roledatatables.search('').draw();
            $("#modal_select_roles").modal("show");

            // 全选
            $("#checkAllRoles").on('click', function() {
                $("[name=checkRoles]:checkbox").prop('checked', $(this).prop('checked'));
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'+rowId+'">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

         $('[data-toggle="popover"]').popover() //弹窗
             .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                 $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

             }).on('shown.bs.popover', function () {
                     var that = this;
                     $('.popover-btn-ok').on("click",function(){
                         var param = $(this).attr("param");
                         deleteMerchant(param);
                     });
                     $('.popover-btn-cancel').on("click",function(){
                         $(that).popover("hide");
                     });
            });
        //给Body加一个Click监听事件
        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });

    }

    $(".btn-admin-search").on('click', function() {

        var keyword = $.trim($("#sKeyword").val());

        $datatables.search( keyword ).draw();

    });

    // 管理员选择角色确认
    $(".changeRoleBtn").on('click', function() {

        var roles = "";
        $("[name=checkRoles]:checkbox").each(function(){ //遍历table里的全部checkbox
            //如果被选中
            if($(this).prop("checked")){
                if(roles == ''){
                    roles = $(this).val();
                }else{
                    roles = roles + ',' + $(this).val();
                }
            }
        });


        var url = "/sellerpc/merchantRole/update";

        var data = {
            merchantId: merchantId,
            roles: roles
        }
        utils.postAjax(url,data,function(res){
            if(typeof(res) === 'object'){
                $("#modal_select_roles").modal("hide");
                if (res.rc == 1){
                    utils.tools.alert('设置成功',{timer: 1200, type: 'success'});
                    $datatables.search('').draw();
                } else{
                    utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                }
            }
        });
    });



    function  deleteMerchant(paramId){

          var url = "/sellerpc/merchant/admin/delete";

          var data = {
              merchantId: paramId
          }
          utils.postAjax(url,data,function(res){
              if(typeof(res) === 'object'){
                  if (res.rc == 1){
                      $datatables.search('').draw();
                  } else{
                      utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                  }
              }
          });
    }


});