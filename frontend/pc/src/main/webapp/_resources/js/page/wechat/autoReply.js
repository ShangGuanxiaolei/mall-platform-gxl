define(['jquery','utils','datatables','bootbox','select2','blockui', 'form/validate', 'switch'],
    function($, utils, validate, bswitch) {

        $('#myTab a').click(function (e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
        })

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        getAutoBack();
        getAddBack();

        // 获取微信自动回复
        function getAutoBack() {
            $.get(window.originalHost + '/wechat/getAutoBack', {}, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            $("#autoReplyContent").val(res.data.replyContent);
                            $("#autoAutoReplyId").val(res.data.id);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        }

        // 获取被添加自动回复
        function getAddBack() {
            $.get(window.originalHost + '/wechat/getAddBack', {}, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            $("#addReplyContent").val(res.data.replyContent);
                            $("#addAutoReplyId").val(res.data.id);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        }




        // 所有自动回复
        var $datatables = $('#guiderUserTable').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ordering: false,
            sortable: false,
            ajax: function(data, callback, settings) {
                $.get(window.originalHost + '/wechat/listWechatAutoReply', {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    data: "name",
                    name: "name",
                    width: "100px",
                    orderable: false,
                },{
                    orderable: false,
                    width:'75px',
                    data: 'matchType',
                    name: 'matchType',
                    render: function (data, type, row) {
                        var value = row.matchType;
                        if (value == 'LITERAL') {
                            return "精确匹配";
                        }else if (value == 'LIKE') {
                            return "模糊匹配";
                        } else{
                            return "";
                        }
                    }
                }, {
                    data: "condition",
                    name: "condition",
                    width: "100px",
                    orderable: false,
                }, {
                    data: "replyContent",
                    name: "replyContent",
                    width: "200px",
                    orderable: false,
                }, {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                }
                , {
                    width: "180px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html = html + '<a class="editBtn role_check_table" href="#"  style="margin-right: 10px;" rowId="' + row.id + '" fid="delete_order"> 修改</a>';
                        html = html + '<a class="delBtn role_check_table" href="#" style="margin-right: 10px;" rowId="' + row.id + '" fid="delete_order"> 删除</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });
        
        function initEvent() {
            $(".editBtn").on('click', function () {
                var id = $(this).attr("rowId");
                $.get(window.originalHost + '/wechat/viewDetail', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#autoReplyName").val(res.data.name);
                                $("#keyword").val(res.data.condition);
                                $("#matchType").val(res.data.matchType);
                                $("#autoReplyId").val(res.data.id);
                                $("#replyContent").val(res.data.replyContent);
                                /** 初始化选择框控件 **/
                                $('.select').select2({
                                    minimumResultsForSearch: Infinity,
                                });
                                $("#modal_newInfo").modal('show');
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });
            });

            $(".delBtn").on('click', function () {
                var id = $(this).attr("rowId");
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $datatables.search('').draw();
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            });

        }

        $(".btn-search").on('click', function() {
            $datatables.search('').draw();
        });

        $(".btn-release").on('click', function() {
            $("#autoReplyName").val('');
            $("#keyword").val('');
            $("#autoReplyId").val('');
            $("#replyContent").val('');
            $("#modal_newInfo").modal("show");
        });

        // 保存被添加自动回复内容
        $("#saveWechatAddReplyBtn").on('click', function() {
            if($("#addReplyContent").val() == ''){
                utils.tools.alert("请输入回复内容", {timer: 1200, type: 'success'});
                return;
            }
            var data = {
                id: $("#addAutoReplyId").val(),
                name: 'ADD',
                condition: 'ADD',
                conditionType: $("#addConditionType").val(),
                matchType: 'ADD',
                replyContent: $("#addReplyContent").val(),
            };

            $.post(window.originalHost + '/wechat/saveWechatAutoReply', data, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            getAddBack();
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        });

        // 删除被添加自动回复内容
        $("#delWechatAddReplyBtn").on('click', function() {
            var id = $("#addAutoReplyId").val();
            if(id != ''){
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $("#addAutoReplyId").val('')
                                    $("#addReplyContent").val('')
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            }

        });

        // 保存自动回复内容
        $("#saveWechatAutoReplyBtn").on('click', function() {
            if($("#autoReplyContent").val() == ''){
                utils.tools.alert("请输入回复内容", {timer: 1200, type: 'success'});
                return;
            }
            var data = {
                id: $("#autoAutoReplyId").val(),
                name: 'AUTO',
                condition: 'AUTO',
                conditionType: $("#autoConditionType").val(),
                matchType: 'AUTO',
                replyContent: $("#autoReplyContent").val(),
            };

            $.post(window.originalHost + '/wechat/saveWechatAutoReply', data, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            getAutoBack();
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        });

        // 删除自动回复内容
        $("#delWechatAutoReplyBtn").on('click', function() {
            var id = $("#autoAutoReplyId").val();
            if(id != ''){
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $("#autoAutoReplyId").val('');
                                    $("#autoReplyContent").val('');
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            }

        });
        
        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var autoReplyForm = {
            form: $('#autoReplyForm'),

            autoReplyName: $("#autoReplyForm input[name=autoReplyName]"),
            keyword: $("#autoReplyForm input[name=keyword]"),
            conditionType: $("#autoReplyForm input[name=conditionType]"),
            matchType: $("#autoReplyForm select[name=matchType]"),
            replyContent: $("#autoReplyForm input[name=replyContent]"),
            autoReplyId: $("#autoReplyForm input[name=autoReplyId]"),

            saveWechatKeywordReplyBtn: $('#saveWechatKeywordReplyBtn'),
            url: window.originalHost + '/wechat/saveWechatAutoReply',

            rules: {
                autoReplyName: {
                    required: true,
                    minlength: 1,
                    maxlength: 32,
                },
                keyword: {
                    required: true,
                    minlength: 1,
                    maxlength: 32,
                },
                matchType: {
                    required: true,
                },
                replyContent: {
                    required: true,
                    minlength: 1,
                    maxlength: 512,
                }
            },

            messages: {
                autoReplyName: {
                    required: '请输入公众号名称',
                    minlength: '请至少输入一个字符',
                    maxlength:'公众号名称长度错误',
                },
                keyword: {
                    required: '请输入关键字',
                    minlength: '请至少输入一个字符',
                    maxlength:'关键字长度错误',
                },
                matchType: {
                    required: '请选择匹配类型',
                },
                replyContent: {
                    required: '请输入返回内容',
                    minlength: '请至少输入一个字符',
                    maxlength:'返回内容长度错误',
                },
            },

            save: function() {

                if(this.autoReplyName.val() == ''){
                    utils.tools.alert("请输入规则名称", {timer: 1200, type: 'success'});
                    return;
                }
                if(this.keyword.val() == ''){
                    utils.tools.alert("请输入规则关键字", {timer: 1200, type: 'success'});
                    return;
                }
                if(this.replyContent.val() == ''){
                    utils.tools.alert("请输入返回内容", {timer: 1200, type: 'success'});
                    return;
                }

                var data = {
                    id: this.autoReplyId.val(),
                    name: this.autoReplyName.val(),
                    condition: this.keyword.val(),
                    conditionType: this.conditionType.val(),
                    matchType: this.matchType.val(),
                    replyContent: this.replyContent.val(),
                };

                $.post(this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("自动回复规则保存成功", {timer: 1200, type: 'success'});
                                $datatables.search('').draw();
                                //location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                $datatables.search('').draw();
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });

            },

            init: function() {

                $(this.saveWechatKeywordReplyBtn).on('click',function() {
                    autoReplyForm.save();
                });
                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        autoReplyForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        autoReplyForm.init();
    });