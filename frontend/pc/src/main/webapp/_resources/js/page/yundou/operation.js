define(['jquery', 'utils', 'datatables', 'blockui', 'select2'], function ($, utils, datatables, blockUI, select2) {

    const prefix = window.host + '/yundou/operation';

    const listUrl = prefix + '/list';
    const typesUrl = prefix + '/types';
    const saveUrl = prefix + '/save';
    const deleteUrl = prefix + '/remove';

    const columns = ['name', 'code', 'description', 'created_at'];

    const operation = {
        save: function (data) {
            utils.postAjax(saveUrl, data, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data) {
                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                        $datatables.search('').draw();
                        $('#modal_operation_save').modal('hide');
                    } else {
                        console.log(data);
                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                    }
                }
            })
        },
        remove: function (id) {
            utils.postAjax(deleteUrl + '/' + id, null, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                if (typeof res === 'object') {
                    var data = res.data;
                    if (data === true) {
                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'})
                        $datatables.search('').draw();
                    } else {
                        console.log(data);
                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                    }
                }
            })
        }
    };

    const block = {
        fullBlock: function () {
            return $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        },
        unBlock: function () {
            return $.unblockUI();
        }
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            infoEmpty: "",
            emptyTable: "暂无操作"
        }
    });

    globalInit();

    const $datatables = $('#xquark_operation_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        pageable: true,
        ajax: function (data, callback, settings) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                order: columns[data.order[0].column],
                direction: data.order[0].dir
            }, function (res) {
                if (res.errorCode != '200') {
                    utils.tools.alert('数据加载失败');
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '100px',
                orderable: true,
                name: 'name'
            },
            {
                data: 'code',
                width: '100px',
                orderable: false,
                name: 'code'
            },
            {
                width: '120px',
                orderable: false,
                render: function (data, type, row) {
                    return row.description ? row.description : '无';
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) {
                        return '';
                    }
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" rowId="'
                        + row.id + '" fid="edit_operation" class="edit_operation"><i class="icon-pencil7" ></i>修改</a>';
                    html += '<a href="javascript:void(0);" style="margin-left: 10px;" data-toggle="popover" rowId="'
                        + row.id + '" fid="delete_operation" class="delete_operation"><i class="icon-trash" ></i>删除</a>';
                    html += '<a href="javascript:void(0);" style="margin-left: 10px;" rowId="'
                        + row.id + '" fid="edit_rules" class="edit_rules"><i class="icon-trash" ></i>配置规则</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    function initEvent() {
        $('body').unbind('click');

        $('.edit_operation').on("click", function () {
            var id = $(this).attr('rowId');
            var url = window.host + '/yundou/operation/show/' + id;
            block.fullBlock();
            utils.postAjax(url, null, function (res) {
                if (res === -1) {
                    utils.tools.alert('网络错误，请稍后再试', {timer: 1200, type: 'warning'});
                    return;
                }
                var data = res.data;
                if (data) {
                    block.unBlock();
                    console.log(data);
                    $('#operationName').val(data.name);
                    $('#operationCode').val(data.code);
                    $('#operationDescription').val(data.description);
                    $('#operationSelect').val(data.typeId);
                    $('#operationId').val(data.id);
                    $('#modal_operation_save').modal('show');
                }
            })
        });

        $('.edit_rules').on('click', function () {
            var id = $(this).attr('rowId');
            window.location.href = '/sellerpc/yundou/rule?operationId=' + id;
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var id = $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                    + id + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find(
                    '[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var param = $(this).attr("param");
                operation.remove(param);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_operation_tables');

        //给Body加一个Click监听事件
        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") === "popover") {
                target.popover("toggle");
            }
        });
    }

    function globalInit() {

        $('#save_btn').on('click', function (e) {
            e.preventDefault();
            $('#operationId').val('');
            $('#operationName').val('');
            $('#operationCode').val('');
            $('#operationSelect').val('');
            $('#operationDescription').val('');
            $('#modal_operation_save').modal('show');
        });

        $('#confirm_btn').on('click', function (e) {
            e.preventDefault();
            var id = $('#operationId').val();
            var name = $('#operationName').val();
            var code = $('#operationCode').val();
            var type = $('#operationSelect').val();
            var description = $('#operationDescription').val();
            var data = {
                id: id,
                name: name,
                code: code,
                typeId: type,
                description: description
            };
            operation.save(data);
        });
        dataInit();
    }

    /* 初始化数据 */
    function dataInit() {
        block.fullBlock();
        utils.postAjax(typesUrl, null, function (res) {
            if (res === -1) {
                utils.tools.alert('网络错误, 请稍候再试', {timer: 1200, type: 'warning'});
                return
            }
            var data = res.data;
            if (data) {
                var total = data.total;
                if (total && total !== 0) {
                    var $select = $('#operationSelect');
                    for (var i = 0; i < total; i++) {
                        $select.append('<option value=' + data.list[i].id + '>' + data.list[i].name + '</option>');
                    }
                }
                block.unBlock();
            }
        })
    }
});
