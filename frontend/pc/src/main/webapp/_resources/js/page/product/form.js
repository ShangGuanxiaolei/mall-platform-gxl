/**
 * Created by quguangming on 16/5/24.
 */
define(['jquery', 'utils', 'form/validate', 'utils/storageHelper',
      'jquerySerializeObject', 'switch',
      'select2', 'steps', 'product/product-data'],
    function ($, utils, validate, storageHelper, serializeObject, sw, sel,
        steps, proData) {

      $('.product-Category').select2({width: '35%'});

      $("#proRecomend").bootstrapSwitch();

      var $form = $("#product_info_form");

      var isSubmit = false;

      var productType = $('#type').val();

      $(".btn-submit").on("click", function () {
        let id = $(this).prop("id");//返回被选中元素的属性值  id
        isSubmit = "save" !== id;
      });

      $('body').on('click', '.select2-container .addcate', function () {
        $('.select2-container .addcate').hide();
        $('.select2-container .addcate-editbox').css('display', 'list-item');
        var rowpos = ($('.select2-results__option').length + 2) * 60;
        $('.select2-results').scrollTop(rowpos);
      });

      //新增分类的确定按钮
      $('body').on('click', '.select2-container ul li.addcate-editbox .yesbtn',
          function () {
            var tempCate = $(
                '.select2-container ul li.addcate-editbox input').val();
            if (!tempCate || utils.tools.getStringLength(tempCate, 1) > 20) {
              utils.tool.alert('分类名称最多10个汉字或者20个字母。');
              return false;
            }
            proData.addProCate(tempCate, function (data) {
              if (data.errorCode == 200) {
                $('.product-Category').append($("<option></option>")
                .attr("value", data.data.id)
                .text(data.data.name));
                $('.product-Category').select2({width: '35%'});
                $('.product-Category').val(data.data.id).trigger("change");
              }
            }, utils.tools.alert);
            return false;
          });

      //新增分类的取消按钮
      $('body').on('click', '.select2-container ul li.addcate-editbox .nobtn',
          function () {
            $('.select2-container ul li.addcate').show();
            $('.select2-container ul li.addcate-editbox').hide();
          });

      //当选择商品分类的“未分类”选项后，清空之前选择的所有项
      $('body').on('change', '.product-Category', function () {
        var productCategory = $("#productCategory").val();
        if (productCategory && (productCategory == 0 || (productCategory
            + '').indexOf('0,') >= 0)) {
          $('#productCategory').val("");
          $(".select2-selection__rendered .select2-selection__choice").remove();
          $(".select2-search__field").attr("placeholder", "未分类");
          $(".select2-search__field").css("width", "100%");
        }
      });

      // 编辑段落描述需要操作图片而图片的dropzone引用在edit.js
      // 编辑功能在edit.js中
      $('#fragment_data_table').on('click', '.fragmentDel', function () {
        var fragmentRow = $(this).closest("tr").get(0);
        utils.tools.confirm("删除该段落描述", function () {
          proData.delDesc($(fragmentRow).attr("id"),
              function (data) {
                var $fragmentDatatables = $('#fragment_data_table').DataTable();
                $fragmentDatatables
                .row(fragmentRow)
                .remove()
                .draw();
              },
              function (msg) {
                utils.tools.alert(msg);
              });
        }, function () {
        });
      });

      //保存商品
      function saveProduct(form) {
        var formData = $(form).serializeObject();
        var imgArr = [];
        $('#product_image_dropzone > .dz-complete').each(function (i, item) {
          imgArr.push($(item).attr('data-img'));
        });
        var pId = $("#productId").val();
        var productCategory = '';
        if ($("#productCategory").val()) {
          productCategory = $("#productCategory").val().join(",");
        }
        var productBrand = '';
        if ($("#productBrand").val()) {
          productBrand = $("#productBrand").val().join(",");
        }

        var logisticsType = $("input[name='logisticsType']:checked").val();
        var uniformValue = $("#uniformValue").val();
        var templateValue = $("#templateValue").val();
        var gift = $('input[name=gift]:checked').val();
        if (logisticsType == 'UNIFORM' && uniformValue == '') {
          alert('请输入价格');
          return;
        }
        // if (logisticsType == 'TEMPLATE' && !templateValue) {
        //   alert('请选择运费模版');
        //   return;
        // }

        // 根据选择的型号，传入sku的值
        var skus = [];
        /**if($skuLists.length == 0){
            var sku = {};
            sku.marketPrice = formData.productMarketPrice;
            sku.price = formData.productPrice;
            sku.amount = formData.productAmount;
            sku.spec = '无';
            sku.id = '';
            skus.push(sku);
        }else{
            $($skuLists).each(function(index,item){
                        var sku = {};
                        sku.marketPrice = formData.productMarketPrice;
                        sku.price = formData.productPrice;
                        //sku.amount = item.amount;
                        sku.amount = formData.productAmount;
                        sku.spec = item.spec;
                        sku.id = item.id;
                        skus.push(sku);
                    }
            );
        }**/

        var skuType = $("input[name='skuType']:checked").val();
        // 商品选中的规格属性
        var productAttributes = '';
        if (skuType == 'muliSku') {
          $(".sku_container").each(function () {
            if (productAttributes == '') {
              productAttributes = $(this).attr("id");
            } else {
              productAttributes = productAttributes + "-" + $(this).attr("id");
            }
          });

          //遍历imgBox,建立所有图片与规格的映射关系
          var skuImgMap=new Map();
          $(".skuImgBox.dz-started").each(function(){
             let key=$(this).find(".dz-preview").attr("data-img");
             let attributeDom=$(this).next();
             let attribute=attributeDom.text();
             skuImgMap.set(attribute,key);
          });


          // 获取每个sku中的价格，数量，attribute等信息
          var flag = false;
          $(".sku_tablecell .sku_cell").each(function () {
            if ($(this).is(':visible')) {
              var spec = '';
              var id = $(this).attr("sku_id") == undefined ? '' : $(this).attr(
                  "sku_id");
              var attributes = $(this).attr("id");
              var price = $(this).find('.productPrice').val();
              var marketPrice = price;
              var deductionDPoint = $(this).find(".deductionDPoint").val();
              var promoAmt = $(this).find(".promoAmt").val();
              var serverAmt = $(this).find(".serverAmt").val();
              var point = $(this).find(".point").val();
              var netWorth = $(this).find(".netWorth").val();
              var skuCode = $(this).find(".skuCode").val();
              var barCode = $(this).find(".barCode").val();
              var amount = $(this).find('.productAmount').val();
              var secureAmount = $(this).find('.secureAmount').val();
              var length = $(this).find(".length").val();
              var width = $(this).find(".width").val();
              var height = $(this).find(".height").val();
              var weight = $(this).find(".weight").val();
              var numInPackage = $(this).find(".numInPackage").val();
                
              let specs=$(this).find('.spec');
              //获取第一个规格的attribute,从而获取他的skuImg信息
              let imgSpecValue=specs.get(0).innerHTML;
              let skuImg=typeof(skuImgMap.get(imgSpecValue)) === "undefined"?'':skuImgMap.get(imgSpecValue);
              
              specs.each(function () {
                if (spec == '') {
                  spec = $(this).text();
                } else {
                  spec = spec + "," + $(this).text();
                }
              }); 
              if (!price) {
                alert('请输入商品规格售价');
                flag = true;
                return;
              }
              if (!deductionDPoint) {
                alert('请输入商品德分!');
                flag = true;
                return;
              }
              if (!skuCode) {
                alert('请输入sku编码!');
                $(this).find(".skuCode").css("background","#df6c6c");
                flag = true;
                return;
              }else{
                $(this).find(".skuCode").css("background","#fff");
              }
              if (!barCode) {
                alert('请输入商品条形码!');
                flag = true;
                return;
              }
              if (!amount) {
                alert('请输入商品规格库存');
                flag = true;
                return;
              }
              if (!secureAmount) {
                alert('请输入商品规格的安全库存');
                flag = true;
                return;
              }
              if(!length){
                alert('请输入商品长度');
                flag = true;
                return;
              }
              if(!width){
                alert('请输入商品宽度');
                flag = true;
                return;
              }
              if(!height){
                alert('请输入商品高度');
                flag = true;
                return;
              }
              if(!weight){
                alert('请输入商品重量');
                flag = true;
                return;
              }
              if(!numInPackage){
                alert('请输入商品装箱数');
                flag = true;
                return;
              }
              
              var sku = {};
              sku.marketPrice = price;
              sku.price = price;
              sku.amount = amount;
              sku.secureAmount = secureAmount;
              sku.spec = spec;
              sku.deductionDPoint = deductionDPoint;
              sku.point = point;
              sku.promoAmt = promoAmt;
              sku.serverAmt = serverAmt;
              sku.netWorth = netWorth;
              sku.skuCode = skuCode;
              sku.barCode = barCode;
              sku.id = id;
              sku.attributes = attributes;
              sku.length = length;
              sku.width = width;
              sku.height = height;
              sku.weight = weight;
              sku.numInPackage = numInPackage;
              sku.skuImg = skuImg;
              skus.push(sku);
            }
          });
          if (flag) {
            return;
          }
        }

        if (skuType == 'muliSku' && productAttributes == '') {
          alert('请选择商品规格');
          return;
        }

        // 如果skus为0,则代表该商品没有选择多规格
        if (skus.length === 0) {
          var sku = {};
          sku.marketPrice = formData.price;
          sku.deductionDPoint = formData.deductionDPoint;
          sku.point = formData.point;
          sku.netWorth = formData.netWorth;
          sku.skuCode = formData.skuCode;
          sku.barCode = formData.barCode;
          sku.price = formData.productPrice;
          sku.amount = formData.productAmount;
          sku.secureAmount = formData.secureAmount;
          sku.promoAmt = formData.promoAmt;
          sku.length = typeof(formData.length)=== "undefined"?0:formData.length;
          sku.width = typeof(formData.width)=== "undefined"?0:formData.width;
          sku.height = formData.height;
          sku.weight = typeof(formData.weight)=== "undefined"?0:formData.weight;
          sku.numInPackage = formData.numInPackage;
          sku.serverAmt = formData.serverAmt;
          sku.spec = '无';
          sku.id = formData.singleSkuId;
          sku.attributes = '';
          sku.skuImg='';
          skus.push(sku);
        }

        var setScale = formData.setScale;
        var yundouScale = formData.yundouScale;
        var minYundouPrice = formData.minYundouPrice;
        // if (setScale === 'true') {
        //   if (!yundouScale || yundouScale === '') {
        //     alert('请输入积分兑换比例');
        //     return;
        //   }
        // } else {
        //     yundouScale = null;
        // }
        //
        // if (!minYundouPrice || minYundouPrice === '') {
        //     alert('请输入最低需支付的价格');
        //     return;
        // }
        //
        // const illegalPrice = skus.find(function (t) {
        //   return parseFloat(t.price) < parseFloat(minYundouPrice);
        // });
        // if (illegalPrice) {
        //     alert('商品原价不能低于最小支付价格');
        //     return;
        // }

        // 从本地存储中获取组合信息
        const storage = storageHelper.local('combineInfo');
        let combineInfo = storage.get();

        var param = {
          id: pId,
          imgs: imgArr.join(','),
          name: formData.productName,
          model: formData.productModel,
          description: formData.productInfo,
          recommend: formData.proRecomend ? 1 : 0,
          status: formData.status,
          originalPrice: formData.productOriginalPrice || formData.productPrice,
          category: productCategory,
          brand: productBrand,
          //'skus[0].price': formData.productPrice,
          //'skus[0].amount': formData.productAmount,
          //'skus[0].spec1': '无',
          // oneyuanPostage: formData.proOneyuanPostage,
          oneyuanPurchase: formData.proOneyuanPurchase ? 1 : 0,
          specialRate: formData.proSpecialRate || 0,
          commissionRate: formData.commissionRate || 0,
          refundAddress: formData.refundAddress || '',
          refundName: formData.refundName || '',
          refundTel: formData.refundTel || '',
          width: formData.width || '0',
          height: formData.height || '0',
          length: formData.length || '0',
          kind: formData.kind || '',
          point: formData.point || '',
          secureAmount: formData.secureAmount,
          skuImg: formData.skuImg,
          promoAmt: formData.promoAmt,
          serverAmt: formData.serverAmt,
          deductionDPoint: formData.deductionDPoint || '',
          netWorth: formData.netWorth || '',
          numInPackage: formData.numInPackage || 0,
          barCode: formData.barCode || '',
          wareHouseId: formData.wareHouseId || '',
          supplierId: formData.supplierId,
          logisticsType: logisticsType,
          uniformValue: uniformValue,
          supportRefund: formData.supportRefund || true,
          detailH5: CKEDITOR.instances.editor.getData(),
          templateValue: templateValue,
          gift: gift || false,
          weight: formData.weight || "0",
          attributes: productAttributes,
          tagList: formData.tagList || [],
          yundouScale: yundouScale || 1,
          minYundouPrice: minYundouPrice || 0,
          type: productType
        };

        if (isSubmit) {
          param.reviewStatus = "WAIT_CHECK";
        } else {
          param.reviewStatus = $("#reviewStatus").prop("value");
        }

        // 多sku情况下重写商品价格、配置字段为首个sku
        if (skuType === 'muliSku') {
          param.price = skus[0].price;
          param.point = skus[0].point;
          param.deductionDPoint = skus[0].deductionDPoint;
          param.netWorth = skus[0].netWorth;
          param.serverAmt = skus[0].serverAmt;
          param.promoAmt = skus[0].promoAmt;
          param.skuImg = skus[0].skuImg;
        }

        if (combineInfo) {
          $(combineInfo).each(function (index, item) {
            eval("param['slaves[" + index + "].id']='" + item.id + "'");
            eval("param['slaves[" + index + "].number']='" + item.number + "'");
          });
        }

        $(skus).each(function (index, item) {
              eval("param['skus[" + index + "].price']='" + item.price + "'");
              eval("param['skus[" + index + "].point']='" + item.point + "'");
              eval("param['skus[" + index + "].promoAmt']='" + item.promoAmt + "'");
              eval("param['skus[" + index + "].serverAmt']='" + item.serverAmt+ "'");
              eval("param['skus[" + index + "].netWorth']='" + item.netWorth + "'");
              eval("param['skus[" + index + "].skuCode']='" + item.skuCode + "'");
              eval("param['skus[" + index + "].barCode']='" + item.barCode + "'");
              eval("param['skus[" + index + "].marketPrice']='" + item.price + "'");
              eval("param['skus[" + index + "].deductionDPoint']='"+ item.deductionDPoint + "'");
              eval("param['skus[" + index + "].amount']='" + item.amount + "'");
              eval("param['skus[" + index + "].secureAmount']='" + item.secureAmount+ "'");
              eval("param['skus[" + index + "].spec']='" + item.spec + "'");
              eval("param['skus[" + index + "].id']='" + item.id + "'");
              eval("param['skus[" + index + "].attributes']='" + item.attributes+ "'");
              eval("param['skus[" + index + "].skuImg']='"+ item.skuImg + "'");
              eval("param['skus[" + index + "].length']='"+ item.length + "'");
              eval("param['skus[" + index + "].width']='"+ item.width + "'");
              eval("param['skus[" + index + "].height']='"+ item.height + "'");
              eval("param['skus[" + index + "].weight']='"+ item.weight + "'");
              eval("param['skus[" + index + "].numInPackage']='"+ item.numInPackage + "'");
            }
        );

        proData.saveProduct(param, function (data) {
          if (typeof(data) === 'object') {
            if (data.errorCode == 200) {
              var fragmentList = [];
              var proId = data.data.id;

              $('.fragmentDel').each(function () {
                fragmentList.push($(this).attr('rowid'));
              });

              var fraData = {
                fragmentIds: fragmentList.join(','),
                productId: proId
              };
              proData.saveProductFragment(fraData, function (data) {
                utils.tools.alert('保存成功!', {timer: 1200, type: 'success'});
                setTimeout(() => {
                  const suffix = productType === 'NORMAL' ? 'product' : 'combine';
                  window.location.href = `/sellerpc/mall/${suffix}`;
                }, 2000);
              }, function (moreInfo) {
                utils.tools.alert(moreInfo, {timer: 2000, type: 'warning'});
              });
            }
          }
        }, function () {
          utils.tools.alert("保存失败!", {timer: 2000, type: 'warning'});
        });

      }


      $form.validate(
          {
            rules: {
              'productName': {
                required: true,
                maxlength: 50
              },
              'productInfo': {
                maxlength: 5000
              },
              'productPrice': {
                required: true,
                min: 0.000000001,
                max: 99999000
              },
              'productAmount': {
                required: true,
                min: 0,
                digits: true,
                max: 99999999
              },
              'height': {
                required: true,
                min: 1,
                digits: true,
                max: 99999999
              },
              'width': {
                required: true,
                min: 1,
                digits: true,
                max: 99999999
              },
              'length': {
                required: true,
                min: 1,
                digits: true,
                max: 99999999
              },
              'productCategory': {
                required: true,
              },
              'kind': {
                required: false
              },
              'supplierId': {
                required: true
              },
              'point': {
                required: true,
                min: 0,
                number: true,
                max: 99999999
              },
              'promoAmt': {
                min: 0,
                max: 9999999
              },
              'serverAmt': {
                min: 0,
                max: 9999999
              },
              'netWorth': {
                required: true,
                min: 0,
                number: true,
                max: 99999999
              },
              // 'moralityPointRate': {
              //   required: true,
              //   min: 0,
              //   digits: true,
              //   max: 99
              // },
              'secureAmount': {
                required: true,
                min: 0,
                digits: true,
                max: 100000
              },
              'deductionDPoint': {
                required: true,
                min: 0,
                number: true,
              },
              'numInPackage': {
                required: false,
                min: 1,
                digits: true,
                max: 100
              },
              'barCode': {
                required: true,
                digits: true,
              },
              'wareHouseId': {
                required: false,
              },
              'productModel': {
                required: false
              },
              'weight': {
                required: true,
                digits: true,
                min: 0,
              },
              'skuCode': {
                required: true,
                checkSkuCodeUnique: true,
                checkSkuCodeUniqueInTable:true
              },
              'conversionPrice': {
                required: true,
                number: true,
                max: 100000,
                checkConversionPrice: true,
                checkConversionPrecision: true,
              }
            },
            messages: {
              'productCategory': {
                require: "请输入商品的分类",
              },
              'productName': {
                required: '商品名称不能为空',
                maxlength: '长度不能多于50个字符',
              },
              'supplierId': {
                required: "请选择供应商",
              },
              'productInfo': {
                maxlength: '商品描述最多5000字'
              },
              'weight': {
                required: "请输入商品的重量",
                digits: "请输入整数",
                min: 0,
              },
              'productPrice': {
                required: '商品价格不能为空',
                min: '商品价格必须大于0',
                max: '商品价格不能超过99999000'
              },

              'productAmount': {
                required: '库存数量不能为空',
                min: '库存数量必须大于0',
                digits: '库存必须为整数',
                max: '商品库存不能超过99999999'
              },
              'secureAmount': {
                required: '安全库存数量不能为空',
                min: '安全库存数量必须大于0',
                digits: '安全库存必须为整数',
                max: '安全库存不能超过99999999'
              },

              'point': {
                required: "请输入商品返利积分",
                min: "返利积分的最小值为0",
                max: "返利积分的最大值为100000"
              },
              'promoAmt': {
                min: "推广费的最小值为0",
                max: "推广费的最大值为100000"
              },
              'serverAmt': {
                min: "服务费的最小值为0",
                max: "服务费的最大值为100000"
              },
              'netWorth': {
                required: "请输入商品净值",
                min: "净值的最小值为0",
                max: "净值的最大值为100000"
              },
              // 'moralityPointRate': {
              //   required: "请输入德分比例",
              //   min: "德分比例的最小值为0",
              //   digits: "德分比例必须为整数",
              //   max: "德分比例的最大值为99"
              // },
              'height': {
                required: "请输入商品高度",
                min: "高度的最小值为1",
                digits: "只能输入整数",
              },
              'width': {
                required: "请输入商品宽度",
                min: "宽度的最小值为1",
                digits: "只能输入整数",
              },
              'length': {
                required: "请输入商品长度",
                min: "长度的最小值为1",
                digits: "只能输入整数",
              },
              'numInPackage': {
                required:"index",
                min: "装箱数的的最小值为1",
                digits: "装箱数必须为整数",
                max: "装箱数的最大值为100"
              },
              'barCode': {
                required: "请输入商品条形码",
              },
              'deductionDPoint': {
                required: "请输入商品的兑换价以计算对应的德分",
                min: "德分不能为负数",
              },
              'skuCode': {
                required: "请输入规格编码",
              },
              'conversionPrice': {
                required: "请输入商品的兑换价",
              },
            },
            focusCleanup: true,
            onkeyup: false,
            onclick: false,
            errorPlacement: function(error, element) {
              console.log(this);
            },
            showErrors: function(errorMap, errorList) {
              errorList.forEach(function(element,index,array){
                $(element.element).attr("data-toggle","tooltip");
                $(element.element).attr("data-placement","bottom");
                $(element.element).attr("title",element.message);
                $(element.element).tooltip();
              });
              this.defaultShowErrors();
            },
            unhighlight: function(element, errorClass, validClass) {
              $(element).removeClass(errorClass).addClass(validClass);
              $(element).tooltip('destroy');
            },
            submitHandler: function (form) {
              console.log("submitCallBack");
              //jquery 不支持校验多个 名字相同的字段，所以规格表里面的需要手动校验
              let skuType = $("input[name='skuType']:checked").val();
              let mutiSkuValidation=true;
              if (skuType === 'muliSku') {
                mutiSkuValidation=$(".sku_tablecell input").valid();
              }
              if(mutiSkuValidation){
                saveProduct(form);
              }else{
                utils.tools.alert("多规格数据不符合规范", {
                  timer: 1200,
                  type: 'warning'
                });
              }
            },
          }
      );

      $.validator.addMethod("checkConversionPrice", function (value, element) {
        //判断是统一规格还是多规格
        let isUnion=$(element).hasClass("skuInput");
        let conversionPrice=value;
        let price;
        if(isUnion){
          let tr = $(element).parents(".sku_cell");
          price = tr.find(".productPrice").prop("value");
        }else{
          price= $("#productPrice").prop("value");
        }
        let paresedPrice = Number.parseFloat(price);
        let paresedConverPrice = Number.parseFloat(conversionPrice);
        return this.optional(element) || (paresedConverPrice > 0 && paresedPrice >= paresedConverPrice);
      }, "兑换价不能小于0或大于售价");

      $.validator.addMethod("checkConversionPrecision",function (value, element) {
        let result = /^-?\d+\.?\d{0,2}$/.test(value);
        return this.optional(element) || result;
      }, "兑换价不能输入超过1位小数");

      $.validator.addMethod("checkSkuCodeUnique", function (value, element) {
        //判断是统一规格还是多规格
        let tagId=$(element).prop("id");
        let code;
        let originCode;
        if(tagId){
          code = $("#skuCode").prop("value");
          originCode = $("#skuCodeOrigin").prop("value");
        }else{
          code=value;
          originCode=$(element).next().prop("value");
        }
        if (originCode === code) {
          return this.optional(element) || true;
        }
        let result = false;
        $.ajax({
              url: `${window.host}/product/sku/exit/${value}`,
              async: false,
            }
        ).done(
            data => {
              result = !data.data;
            }
        ).fail(
            data => {
            });
        return this.optional(element) || result;
      }, "SKU编码已存在");

      /***
       * 判断sku在表格内是否是唯一的
       */
      $.validator.addMethod("checkSkuCodeUniqueInTable", function (value, element) {
        let id=$(element).prop("id");
        if(id=='skuCode'){
          return true;
        }else{
          let skuCodes=$(".sku_table").find(".skuCode");
          let repeatCount=0;
          for(let i=0;i<skuCodes.length;i++){
            let v=$(skuCodes.get(i)).prop("value");
            if(v){
              if(v==value){
                ++repeatCount;
              }
            }
          }
          return this.optional(element) || repeatCount===1;
        }
      }, "SKU编码在表格内已存在");
    });