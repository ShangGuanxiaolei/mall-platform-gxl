define(['jquery', 'utils', 'datePicker', 'productModal', 'fileUploader',
      'validator',
      'jquerySerializeObject',
      'datatables', 'blockui', 'select2'],
    function ($, utils, datePicker, productModal, uploader, validator) {

      const prefix = window.host + '/preOrder';
      const listUrl = prefix + '/list';
      const saveUrl = prefix + '/saveProduct';
      const closeUrl = prefix + '/close';
      const viewUrl = prefix + '/view';
      const checkInPromotinUrl = prefix + '/checkInPromotion';

      var $dataTable;

      var globalDatePicker;

      var globalSingleDatePicker;

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      const manager = (function () {

        const $addPreOrderBtn = $('.pre-order-add');
        const $savePreOrderBtn = $('.savePreOrderBtn');
        const $modalPreOrder = $('#modal_pre_order');

        const $validFrom = $('#valid_from');
        const $validTo = $('#valid_to');
        const $canPayTime = $('#canPayTime');

        const $form = $('#preOrderForm');

        const $img = $('#img');

        var imgUploader = uploader.createUploader({
          dom: '#product_image_dropzone',
          onSuccess: function (img) {
            $img.val(img);
          }
        });

        const notNull = function (field) {
          return field && field !== '';
        };

        const notNegative = function (field) {
          return field && parseFloat(field) > 0;
        };

        const validationRules = {
          title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          preOrderPrice: [
            {
              checker: notNull,
              message: '请输入预购定金'
            },
            {
              checker: notNegative,
              message: '预购金额不能小于0'
            }
          ],
          discount: [
            {
              checker: notNull,
              message: '请输入购买价格'
            },
            {
              checker: notNegative,
              message: '购买价格不能小于0'
            }
          ],
          freePoints: [
            {
              checker: notNull,
              message: '请输入定金所需积分'
            },
            {
              checker: notNegative,
              message: '定金所需积分不能小于0'
            }
          ],
          amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ]
        };

        const globalInstance = {

          bindEvent: function () {

            /* 点击显示增加预购商品弹窗 */
            $addPreOrderBtn.on('click', function () {
              utils.tools.syncForm($form);
              imgUploader.clear();
              $modalPreOrder.modal('show');
            });

            /* 保存预购商品事件 */
            $savePreOrderBtn.on('click', function (e) {
              e.preventDefault();
              const params = $form.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              console.log(params);
              utils.postAjax(saveUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $dataTable.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });
            return this;
          },
          makeDatePicker: function () {
            /* 初始话日期控件 */
            globalDatePicker = datePicker.createPicker({
              dom: '.daterange-time',
              onApply: function (start, end) {
                $validFrom.val(start);
                $validTo.val(end);
              },
              onEmpty: function () {
                $validFrom.val('');
                $validTo.val('');
              }
            });
            return this;
          },
          makeSingleDatePicker: function () {
            globalSingleDatePicker = datePicker.createSinglePicker(
                {
                  dom: '#single-picker',
                  onApply: function (start) {
                    $canPayTime.val(start);
                  }
                });
            return this;
          },
          makeProductModal: function () {
            /**
             * 初始化商品选择表格
             */
            productModal.create({
              dom: '#productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'), checkInPromotinUrl,
                    {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#productId").val(id);
                            $("#productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }
            });
          }
        };

        return {
          initGlobal: function () {
            globalInstance.bindEvent()
            .makeDatePicker()
            .makeSingleDatePicker()
            .makeProductModal();
            return this;
          },
          initTable: function () {
            if (!$dataTable) {
              $dataTable = $('#xquark_preorder_list')
              .DataTable({
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: true,
                ajax: function (data, callback) {
                  $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true
                  }, function (res) {
                    if (!res.data) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    }
                    callback({
                      recordsTotal: res.data.total,
                      recordsFiltered: res.data.total,
                      data: res.data.list,
                      iTotalRecords: res.data.total,
                      iTotalDisplayRecords: res.data.total
                    });
                  })
                },
                rowId: 'id',
                columns: [
                  {
                    title: '活动图片',
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                      return '<img class="goods-image" src="'
                          + row.promotion.img + '" />';
                    }
                  },
                  {
                    title: '活动名称',
                    data: "promotion.title",
                    width: "80px",
                    orderable: false,
                    name: "promotionName"
                  },
                  {
                    title: '预购商品名',
                    data: "product.name",
                    width: "80px",
                    orderable: false,
                    name: "productName"
                  },
                  {
                    title: '状态',
                    orderable: false,
                    width: "80px",
                    render: function (data, type, row) {
                      var isClosed = row.promotion.closed;
                      if (isClosed) {
                        return '已结束';
                      }
                      return '进行中';
                    }
                  },
                  {
                    title: '开始日期',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validFrom);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validFrom"
                  },
                  {
                    title: '结束时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validTo);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validTo"
                  },
                  {
                    title: '发布时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.createdAt);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                  },
                  {
                    title: '管理',
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                      var html = '';
                      html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-pencil7" ></i>编辑</a>';
                      html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-trash"></i>结束</a>';
                      return html;
                    }
                  }
                ],
                select: {
                  style: 'multi'
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                  $('.del').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.postAjax(closeUrl, {id: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          utils.tools.alert('操作成功',
                              {timer: 1200, type: 'success'});
                          $dataTable.search('').draw();
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
                  });

                  $('.edit').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.postAjax(viewUrl + '/' + id, null, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          $('#product_image_dropzone').val('');
                          const data = res.data;
                          const validFrom = new Date(
                              parseInt(data.validFrom)).format(
                              'yyyy-MM-dd hh:mm');
                          const validTo = new Date(
                              parseInt(data.validTo)).format(
                              'yyyy-MM-dd hh:mm');
                          const canPayTime = new Date(parseInt(data.canPayTime))
                          .format('yyyy-MM-dd hh:mm');
                          const img = data.product.img;
                          imgUploader.clear().
                            addImage(img);
                          globalDatePicker.resetStr(validFrom + ' - '
                              + validTo);
                          globalSingleDatePicker.resetStr(canPayTime);
                          var formData = {
                            id: id,
                            productId: data.product.id,
                            promotionId: data.promotion.id,
                            title: data.promotion.title,
                            productName: data.product.name,
                            discount: data.discount,
                            amount: data.amount,
                            preOrderPrice: data.preOrderPrice,
                            freePoints: data.freePoints,
                            validFrom: validFrom,
                            validTo: validTo,
                            img: data.promotion.img
                          };
                          utils.tools.syncForm($form, formData);
                          $modalPreOrder.modal('show');
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
                  })
                }
              });
            }
            return this;
          }
        }
      })();

      /* 初始化页面 */
      manager.initGlobal()
      .initTable();

    });
