define(['jquery', 'utils', 'ramda'], function ($, utils, R) {

  class Storage {

    constructor(name) {
      if (name === 'local') {
        this._storage = localStorage;
      } else if (name === 'session') {
        this._storage = sessionStorage
      } else {
        throw `storage name ${name} not support`
      }
    }

    get(key) {
      if (!key || key === '') {
        return null;
      }
      const ret = this._storage.getItem(key);
      if (!ret || ret === '') {
        return ret;
      }
      return JSON.parse(ret);
    }

    remove(key) {
      this._storage.removeItem(key);
    }

    set(key, value) {
      if (!key || key === '') {
        return;
      }
      if (!value || value === '') {
        return;
      }
      this._storage.setItem(key, JSON.stringify(value));
    }

  }

  function buildRet(name) {
    return function (key) {
      const storage = new Storage(name);
      return {
        get: () => storage.get(key),
        set: val => storage.set(key, val),
        remove: () => storage.remove(key)
      }
    }
  }

  return {

    local: buildRet('local'),

    session: buildRet('session')

  }

});
