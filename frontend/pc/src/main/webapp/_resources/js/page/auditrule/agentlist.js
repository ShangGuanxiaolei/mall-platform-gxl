/**
 * Created by chh on 17/1/20.
 */
define(['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/auditRule/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#buyerType").val('');
        $("#sellerType").val('');
        $("#auditType").val('');
        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
        $("#modal_component").modal("show");
    });

    buttonRoleCheck('.hideClass');

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ordering: false,
        sortable: false,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                type:'AGENT',
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.buyerType)
                    {
                        case 'SPECIAL':
                            status = '特约';
                            break;
                        case 'FIRST':
                            status = '一星顾问';
                            break;
                        case 'SECOND':
                            status = '二星顾问';
                            break;
                        case 'GENERAL':
                            status = '总顾问';
                            break;
                        case 'DIRECTOR':
                            status = '董事';
                            break;
                        case 'FOUNDER':
                            status = '联合创始人';
                            break;
                        case 'ALL':
                            status = '所有';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.sellerType)
                    {
                        case 'SPECIAL':
                            status = '特约';
                            break;
                        case 'FIRST':
                            status = '一星顾问';
                            break;
                        case 'SECOND':
                            status = '二星顾问';
                            break;
                        case 'GENERAL':
                            status = '总顾问';
                            break;
                        case 'DIRECTOR':
                            status = '董事';
                            break;
                        case 'FOUNDER':
                            status = '联合创始人';
                            break;
                        case 'ALL':
                            status = '所有';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }
             , {
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.auditType)
                    {
                        case 'PARENT':
                            status = '上级';
                            break;
                        case 'PLATFORM':
                            status = '平台';
                            break;
                        case 'ALL':
                            status = '所有';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_audit"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_audit"><i class="icon-trash" ></i>删除</a>';

                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           $.ajax({
                url: window.host + '/auditRule/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#buyerType").val(role.buyerType);
                        $("#sellerType").val(role.sellerType);
                        $("#auditType").val(role.auditType);
                        $("#modal_component").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
           });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/auditRule/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存
    $(".saveBtn").on('click', function() {
        var url = window.host + '/auditRule/save';
        var id = $("#id").val();
        var buyerType = $("#buyerType").val();
        var sellerType = $("#sellerType").val();
        var auditType = $("#auditType").val();
        var type = $("#type").val();
        if(!type || type == '' ){
            utils.tools.alert("请选择规则类型!", {timer: 1200, type: 'warning'});
            return;
        }else if(!buyerType || buyerType == '' ){
            utils.tools.alert("请选择买家级别!", {timer: 1200, type: 'warning'});
            return;
        }else if(!sellerType || sellerType == ''){
            utils.tools.alert("请选择卖家级别!", {timer: 1200, type: 'warning'});
            return;
        }else if(!auditType || auditType == ''){
            utils.tools.alert("请选择审核类型!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            type: type,
            buyerType: buyerType,
            sellerType: sellerType,
            auditType: auditType
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                if(res.rc==1){
                    alert(res.msg);
                    window.location.href = window.originalHost + '/agent/auditRule';
                }else{
                    utils.tools.alert(res.msg, {timer: 1200});
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});