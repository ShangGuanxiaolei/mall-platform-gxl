define(['jquery', 'steps'], function ($) {

  /**
   * 创建jquery_steps实例
   * @param params
   * @param params.dom
   * @param params.headerTag
   * @param params.bodyTag
   * @param params.onFinished
   * @param params.onStepChanged
   * @param params.showFinishButton
   * @param params.nextBtn
   * @param params.previousBtn
   * @param params.form
   * @param params.validate
   */
  function createSteps(params) {

    const _this = this;
    const $next = params.nextBtn;
    const $previous = params.previousBtn;

    if (params.form && params.validate) {
      const $form = params.form;
      // 表单校验
      $form.validate(params.validate);
      this.$form = $form;
    }

    const $step = $(params.dom).steps({
      headerTag: params.headerTag,
      bodyTag: params.bodyTag,
      transitionEffect: "fade",
      titleTemplate: '<span class="number">#index#</span> #title#',
      enableAllSteps: true,
      enablePagination: true,
      showFinishButtonAlways: params.showFinishButton | true,
      labels: {
        cancel: "取消",
        current: "current step:",
        pagination: "Pagination",
        next: "下一步",
        previous: "上一步",
        finish: '保存'
      },
      onStepChanging: function () {
        _this.$form.validate().settings.ignore = ":disabled,:hidden";
        return _this.$form.valid();
      },
      onFinishing: function () {
        _this.$form.validate().settings.ignore = ":disabled";
        return _this.$form.valid();
      },
      onFinished: function (event, currentIndex) {
        if (params && params.onFinished) {
          params.onFinished(event, currentIndex);
        }
      },
      onStepChanged: function (event, currentIndex) {
        if (params && params.onStepChanged) {
          params.onStepChanged(event, currentIndex);
        }
        // $($('fieldset').get(currentIndex)).tab("show");
      },
    });

    if ($next && ($next instanceof $)) {
      $next.on('click', function () {
        $step.steps('next');
      })
    }

    if ($previous && ($previous instanceof $)) {
      $step.steps('pre');
    }

    return $step;

  }

  return {
    create: createSteps
  }

});
