/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('partner/member',['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        var default_teamRate = 0;
        var default_shareholderRate = 0;
        var default_platformRate = 0

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-basic');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        $('.daterange-basic').val('');
        options.startDate = '';
        options.endDate = '';

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';
        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partnerMember/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#selectStatus ").val(),
                    partner_status: $("#partner_status").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    phone:$("#phone").val(),
                    shopName:$("#shopName").val(),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "合伙人",
            },{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                title: "合伙人信息",
                render: function(data, type, row){
                    return '<a href="#" user-id="' + row.id + '" class="partnerChildrenBtn" data-toggle="modal" >' +row.phone+ ' <i class="icon-search4"></i></a>';
                }
            },{
                width:200,
                sClass: 'styled text-center sorting_disabled',
                data: 'typeStr',
                name: 'typeStr',
                title: "合伙模式",
            },
                //    {
                //    width:45,
                //    sClass: 'text-center sorting_disabled',
                //    data: 'buyerId',
                //    name: 'buyerId',
                //    title: '等级'
                //},
                //    {
                //    width:85,
                //    sClass: 'text-center sorting_disabled',
                //    data: 'buyerId',
                //    name: 'buyerId',
                //    title: '会员账号'
                //},
                //{
                //    width:90,
                //    data: 'fee',
                //    name: 'fee',
                //    title: '已结算佣金',
                //    render: function(data, type, row){
                //        return '<a href="#">￥'+row.fee+'</a>';
                //    }
                //},
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:75,
                    sClass: 'sorting',
                    data: 'status',
                    name: 'status',
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if(value =='ACTIVE'){
                            return "正常";
                        }else{
                            return "禁用";
                        }
                    }
                },{
                    width:80,
                    sClass: 'sorting',
                    data: 'partner_status',
                    name: 'partner_status',
                    title: '申请状态',
                    render: function (data, type, row) {
                        var value = row.partner_status;
                        if(value =='ACTIVE'){
                            return "正常";
                        }else{
                            return "申请中";
                        }
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var str = '<a href="javascript:void(0);" class="setTypeBtn" style="margin-left: 10px;" rowId="'+row.id + '" userId="'+row.userId + '">设置</a></br> ' +
                            '<a href="javascript:void(0);" class="disableBtn" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id + '">禁用</a></br> ';
                        if(row.blackList == true){
                            str += '<a href="javascript:void(0);" class="viewBlackListBtn" style="margin-left: 10px;" rowId="'+row.id + '" userId="'+row.userId + '">黑名单商品</a> ';
                        }
                        return str;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        var userId =  null;
        //查看推客下级
        var $partnerChildrenTables = $('#partnerChildrenInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partnerMember/children", {
                    size: data.length,
                    page: (data.start / data.length),
                    userId : userId,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                data: 'name',
                name: 'name',
                width: 310,
                title: "下级店铺"

            },{
                title: "店铺状态",
                width: 150,
                data: 'archive',
                name: 'archive',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.archive;
                    if(value =='0'){
                        return "正常";
                    }else{
                        return "禁用";
                    }
                }
            }]
        })
        $(document).on('click', '.partnerChildrenBtn', function() {
            userId = $(this).attr("user-id");
            $('#modal_partnerChildrenInfo').modal('show');
            $partnerChildrenTables.search(userId).draw();
        });

        $('.exportPartnerChildrenBtn').on('click', function() {
            if (userId != null) {
                $('#exportPartnerChildrenForm input[name="userId"]').val(userId);
                $('#exportPartnerChildrenForm').submit();
            }
        });

        function initEvent() {

            $(".disableBtn").on("click", function () {
                var mId = $(this).attr("rowId");
                $("[data-toggle='popover']").popover({
                    trigger: 'click',
                    placement: 'left',
                    html: 'true',
                    animation: true,
                    content: function () {
                        var rowId = $(this).attr("rowId");
                        return '<span>确认禁用吗？</span>' +
                            '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                            '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                    }
                });

                $('[data-toggle="popover"]').popover() //弹窗
                    .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                        $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                    }).on('shown.bs.popover', function () {
                        var that = this;
                        $('.popover-btn-ok').on("click", function () {
                            var id = $(this).attr("mId");
                            disableShop(id);
                        });
                        $('.popover-btn-cancel').on("click", function () {
                            $(that).popover("hide");
                        });
                    });
                //给Body加一个Click监听事件
                $('body').on('click', function (event) {
                    var target = $(event.target);
                    if (!target.hasClass('popover') //弹窗内部点击不关闭
                        && target.parent('.popover-content').length === 0
                        && target.parent('.popover-title').length === 0
                        && target.parent('.popover').length === 0
                        && target.data("toggle") !== "popover") {
                        //弹窗触发列不关闭，否则显示后隐藏
                        $('[data-toggle="popover"]').popover('hide');
                    } else if (target.data("toggle") == "popover") {
                        target.popover("toggle");
                    }
                });

            });




            function disableShop(id) {
                var url = window.host + "/partnerMember/disableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            // 设置合伙人类型
            $(".setTypeBtn").on("click", function () {
                var userId = $(this).attr("userId");
                $("#type_user_id").val(userId);
                $.ajax({
                    url: host + '/partner/getTypeSet',
                    type: 'POST',
                    data: {'userId' : userId},
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var platformType = data.data.platformType;
                            var teamType = data.data.teamType;
                            var shareholderType = data.data.shareholderType;
                            if(platformType && platformType != null){
                                $("#platformSet").show();
                                $("#platform_type").val(platformType.typeId == '0' ? '' : platformType.typeId);
                                $("#platformCheckbox").prop("checked",true);
                            }else{
                                $("#platformSet").hide();
                                $("#platform_type").val('');
                                $("#platformCheckbox").prop("checked",false);
                            }

                            if(teamType && teamType != null){
                                $("#teamSet").show();
                                $("#team_type").val(teamType.typeId == '0' ? '' : teamType.typeId);
                                $("#teamCheckbox").prop("checked",true);
                            }else{
                                $("#teamSet").hide();
                                $("#team_type").val('');
                                $("#teamCheckbox").prop("checked",false);
                            }

                            if(shareholderType && shareholderType != null){
                                $("#shareholderSet").show();
                                $("#shareholder_type").val(shareholderType.typeId == '0' ? '' : shareholderType.typeId);
                                $("#shareholderCheckbox").prop("checked",true);
                            }else {
                                $("#shareholderSet").hide();
                                $("#shareholder_type").val('');
                                $("#shareholderCheckbox").prop("checked",false);
                            }

                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                            $("#modal_setType").modal("show");
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tools.goLogin();
                        } else {
                            alert('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            });


            // 查看黑名单商品列表信息
            $(".viewBlackListBtn").on("click", function () {
                userId = $(this).attr("userId");
                $selectproductdatatables.search('').draw();
                $("#modal_select_products").modal("show");
            });

        }


        var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/productBlack/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    userId: userId
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productImg+'"><img class="goods-image" src="'+row.productImg+'" /></a>';
                    }
                },
                {
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"productName"
                }, {
                    data: "productPrice",
                    width: "50px",
                    orderable: true,
                    name:"productPrice"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                }
            ],
            drawCallback: function () {  //数据加载完成

            }
        });


        // 类型勾选框选中取消后需要同步隐藏类型设置框
        $("#teamCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#teamSet").show();
            }else{
                $("#teamSet").hide();
            }
        });
        $("#platformCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#platformSet").show();
            }else{
                $("#platformSet").hide();
            }
        });
        $("#shareholderCheckbox").change(function () {
            if($(this).is(':checked')){
                $("#shareholderSet").show();
            }else{
                $("#shareholderSet").hide();
            }
        });

        // 保存合伙人类型设置
        $(".saveSetTypeBtn").on('click', function () {
            var data = {};
            var typeList = [];
            if ($('#teamCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#team_type").val();
                var type = 'TEAM';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if ($('#platformCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#platform_type").val();
                var type = 'PLATFORM';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if ($('#shareholderCheckbox').prop('checked')) {
                var object = {};
                var typeId = $("#shareholder_type").val();
                var type = 'SHAREHOLDER';
                object.typeId = typeId;
                object.type = type;
                typeList.push(object);
            }
            if(typeList.length == 0){
                utils.tools.alert("请至少选择一种合伙人模式!", {timer: 1200, type: 'warning'});
                return;
            }
            data.userId = $("#type_user_id").val();
            data.types = JSON.stringify(typeList);
            $.ajax({
                url: host + '/partner/saveTypeSet',
                type: 'POST',
                data: data,
                dataType:"json",
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_setType").modal('hide');
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        });


        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });


        // 团队类型
        var $teamdatatables = $('#teamTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'TEAM',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        // 股东类型
        var $shareholderdatatables = $('#shareholderTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'SHAREHOLDER',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        // 平台类型
        var $platformdatatables = $('#platformTypeTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/partner/typeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    type: 'PLATFORM',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'name',
                name: 'name',
                title: "类型名称",
            },{
                width:100,
                sClass: 'styled text-center sorting_disabled',
                data: 'rate',
                name: 'rate',
                title: "分红系数",
            },
                {
                    width:75,
                    sClass: 'sorting',
                    title: '加入时间',
                    sortable: true,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row){
                        var html = '';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" rowType="'+row.type+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="typeedit role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;

                    }
                }],
            drawCallback: function () {  //数据加载完成
                initTypeEvent();
            }
        });

        function initTypeEvent() {
            $(".typeedit").on("click",function(){
                var id =  $(this).attr("rowId");
                var type = $(this).attr("rowType");
                $.ajax({
                    url: window.host + '/partner/getType/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;

                            $("#typeId").val(role.id);
                            $("#type").val(role.type);
                            $("#name").val(role.name);
                            $("#rate").val(role.rate);

                            var default_rate = 0;
                            if(type == 'PLATFORM'){
                                default_rate = default_platformRate;
                            }else if(type == 'TEAM'){
                                default_rate = default_teamRate;
                            }else if(type == 'SHAREHOLDER'){
                                default_rate = default_shareholderRate;
                            }
                            $("#rateBase").html(default_rate);
                            if(role.rate && role.rate != ''){
                                var total = Number(role.rate) * Number(default_rate);
                                $("#rateTotal").html(total.toFixed(2));
                            }else{
                                $("#rateTotal").html('');
                            }


                            $("#modal_addType").modal("show");
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteType(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增团队类型
        $(document).on('click', '.addTeamType', function() {
            $("#typeId").val('');
            $("#type").val('TEAM');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_teamRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 新增股东类型
        $(document).on('click', '.addShareholderType', function() {
            $("#typeId").val('');
            $("#type").val('SHAREHOLDER');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_shareholderRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 新增平台类型
        $(document).on('click', '.addPlatformType', function() {
            $("#typeId").val('');
            $("#type").val('PLATFORM');
            $("#name").val('');
            $("#rate").val('');
            $("#rateBase").html(default_platformRate);
            $("#rateTotal").html('');
            $("#modal_addType").modal("show");
        });

        // 佣金比例变化时，对应的总和也需要一起变化
        $("#rate").on('input',function(e){
            var rate = $("#rate").val();
            var rateBase = $("#rateBase").html();
            if(rate && rate != ''){
                var total = Number(rate) * Number(rateBase);
                $("#rateTotal").html(total.toFixed(2));
            }else{
                $("#rateTotal").html('');
            }
        });


        function deleteType(id) {
            $.ajax({
                url: host + '/partner/deleteType/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $teamdatatables.search('').draw();
                        $shareholderdatatables.search('').draw();
                        $platformdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        // 保存推客等级
        $(".saveTypeBtn").on('click', function() {
            var id = $("#typeId").val();
            var name = $("#name").val();
            var type = $("#type").val();
            var rate = $("#rate").val();


            if(!name || name == '' ){
                utils.tools.alert("请输入等级名称!", {timer: 1200, type: 'warning'});
                return;
            }else if(!rate || rate == ''){
                utils.tools.alert("请输入分红系数!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                id: id,
                name: name,
                type: type,
                rate: rate
            };
            $.ajax({
                url: host + '/partner/saveType',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_addType").modal('hide');
                        $teamdatatables.search('').draw();
                        $shareholderdatatables.search('').draw();
                        $platformdatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        // 获取合伙人设置中默认设置的各种类型的分红比例
        $.ajax({
            url: window.host + '/partner/getDefaultCommission',
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.errorCode == 200) {
                    var role = data.data;
                    if(role){
                        default_teamRate = role.team.rate;
                        default_shareholderRate = role.shareholder.rate;
                        default_platformRate = role.platform.rate;
                    }

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    });
//微信账号绑定
require(['all']);

require(['partner/member']);

define("partnerMember", function(){});

