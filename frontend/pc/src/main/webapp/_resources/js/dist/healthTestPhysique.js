/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('healthTest/physique_edit',['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'ckEditor'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';
    const saveUrl = prefix + '/physique/save';

    const $physiqueTable = $('#physiqueTable');

    const formProfile = {
        debug: true,
        rules: {
            name: 'required',
            description: 'required'
        },
        messages: {
            name: {
                required: '请输入问题名称'
            },
            description: {
                required: '请输入体质描述'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            console.log(form);
            manager.submitForm(form);
        }
    };

    const manager = (function () {

        const $editor = $('#editor');
        const $form = $('.physique-form');

        const globalInstance = {
            initEditor: function () {
                $editor.ckeditor();
                return this;
            },
            initValidate: function () {
                validate($form, formProfile);
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.initEditor()
                    .initValidate();
            },
            initTable: function () {

            },
            submitForm: function (form) {
                var data = $(form).serializeObject();
                utils.getJson(saveUrl, data, function (res) {
                    if (res.errorCode === 200) {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            // 修改问题后跳转
                            // 添加问题后重置
                            $form[0].reset();
                            var newPhysique = '<tr><td>' + data.name + '</td></tr>';
                            $physiqueTable.append($(newPhysique));
                        } else {
                            utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    }
                });
            }
        }
    })();

    manager.initGlobal();

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});

require(['all']);

require(['healthTest/physique_edit']);

define("healthTestPhysique", function(){});

