/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
/**
 * Created by quguangming on 16/5/24.
 */
define('product/upload',['jquery','utils','dropzone'], function($,utils) {
    Dropzone.autoDiscover = false;

    var imgDropzone =  $("#product_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 100, // MB      //最大文件大小，单位是 MB ,不限制
        maxFiles: 10,               //限制最多文件数量
        maxThumbnailFilesize: 10,
        addRemoveLinks: true,
        thumbnailWidth:"150",      //设置缩略图的缩略比
        thumbnailHeight:"150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                        //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",              //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function() {

            // var imgDropzone = this;
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".btn-submit").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
                //imgDropzone.processQueue();
            });
            this.on("success", function(file,data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                }
                $(".dz-image").css("height","150px;");
                $(".dz-image").css("width","150px;");

            });

        }
    });

    return imgDropzone;
});
define('product/privilege',['jquery', 'utils', 'form/validate', 'switch', 'datatables', 'blockui','bootbox', 'select2', 'product/upload'],
    function($, utils, validate, bswitch, datatabels, blockui, bootbox,select2,uploader) {

        var $productlistUrl = window.host + "/product/list";

        var $shopId = null;

        var $rowId = '';

        var $category = '';

        var $orders = ['price','amount','sales','onsale'];

        var $order = '';

        var $isNew = false;

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        // 商品佣金列表
        var $datatables = $('#productsCommission').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/product/privilegeList", {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<img class="goods-image" src="'+row.productImg+'" />';
                    }
                },
                {
                    title: '商品信息',
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"productName"
                },
                {
                    title: '商品价格',
                    data: "productPrice",
                    width: "80px",
                    orderable: false,
                    name:"productPrice",
                    render: function(data, type, row){
                        return '<font color = "#53a000">￥' + row.productPrice + '</font>';
                    }
                },
                {
                    width:75,
                    title: '创建时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    width:75,
                    title: '更新时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.updatedAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.updatedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" logoUrl="' + row.logoUrl + '" productName="' + row.productName + '" logo="' + row.logo + '" rowId="' + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="leveldel role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteProductCommission(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });

            $(".edit").on("click", function () {

                $("#product_image_dropzone").html('');
                $isNew = false;

                var id = $(this).attr("rowId");
                var logo = $(this).attr("logo");
                var logoUrl = $(this).attr("logoUrl");
                var productName = $(this).attr("productName");
                $("#productName").val(productName);
                $("#commission_Id").val(id);

                //图片
                if (logo) {
                    var key = logo,
                        url = logoUrl,
                        imgObject = {
                            imgUrl: url,
                            id: key
                        };
                    var mockFile = {name: "", size: "", dataImg: imgObject.id};
                    uploader[0].dropzone.emit("addedfile", mockFile);
                    uploader[0].dropzone.emit("thumbnail", mockFile, imgObject.imgUrl);
                    uploader[0].dropzone.emit("complete", mockFile);
                }
                $("#productInfo").hide();
                $("#chooseProductInfo").show();
                $("#modal_product_commission").modal("show");
            });


        }

        function deleteProductCommission(id) {
            $.ajax({
                url: host + '/product/deletePrivilege/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增商品佣金
        $(document).on('click', '.addProduct', function() {
            /** 初始化选择框控件 **/
            $('.select').select2({
                minimumResultsForSearch: Infinity,
            });
            $("#modal_select_products").modal("show");
            $("#commission_Id").val('');
            $isNew = true;
        });


        var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get($productlistUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    order: function () {
                        if($order != ''){
                            return $order;
                        }else{
                            var _index = data.order[0].column;
                            if ( _index < 4){
                                return '';
                            } else {
                                return $orders[_index - 4];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir :'asc',
                    category : $category,
                    isGroupon : ''
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.categoryTotal,
                        iTotalDisplayRecords:res.data.categoryTotal
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                    }
                },
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.status)
                        {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "price",
                    width: "50px",
                    orderable: true,
                    name:"price"
                }, {
                    data: "amount",
                    orderable: true,
                    width: "50px",
                    name:"amount"
                },
                {
                    data: "sales",
                    orderable: true,
                    width: "50px",
                    name:"sales"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"onsaleAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'+row.id+'" productImg="'+row.imgUrl+'" productPrice="'+row.price+'" productName="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectProductEvent();
            }
        });

        function initSelectProductEvent(){
            $(".selectproduct").on("click",function(){

                if($isNew){
                    $("#product_image_dropzone").html('');
                    $("#productInfo").show();
                    $("#chooseProductInfo").hide();
                }else{
                    $("#productInfo").hide();
                    $("#chooseProductInfo").show();
                }

                var productId =  $(this).attr("rowId");
                var productName =  $(this).attr("productName");
                var productImg =  $(this).attr("productImg");
                var productPrice =  $(this).attr("productPrice");
                var commission_Id = $("#commission_Id").val();

                // 查询是否已经有该商品已经设置过分佣设置
                $.ajax({
                    url: window.host + '/product/checkPrivilege',
                    type: 'POST',
                    dataType: 'json',
                    data: {'productId':productId, 'id' : commission_Id},
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            if(role){
                                alert("该商品已经设置过!");
                                return;
                            }else{

                                $("#commission_productId").val(productId);
                                $("#commission_productName").html(productName);
                                $("#commission_productPrice").html(productPrice);
                                $("#commission_productImg").attr("src",productImg);
                                $("#productName").val(productName);
                                $("#modal_select_products").modal("hide");
                                $("#modal_product_commission").modal("show");

                                //saveProductCommission(productId);
                            }
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            });
        }

        $('body').on('change','select[name="categoryType"]', function(event) {
            $productlistUrl = window.host + "/product/list";
            $category = $(this).val();
            $selectproductdatatables.search('').draw();
        });

        $(".btn-search-products").on('click', function() {
            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null){
                $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
                $selectproductdatatables.search( keyword ).draw();
            }else if (keyword == '' || keyword.length == 0 ){
                $productlistUrl = window.host + "/product/list";
                $selectproductdatatables.search('').draw();
            }
        });

        // 保存商品佣金设置
        $(".btn-save-commission").on('click', function() {
            var commission_productId = $("#commission_productId").val();
            var commission_Id = $("#commission_Id").val();

            var imgArr = [];
            $('#product_image_dropzone > .dz-complete').each(function(i, item) {
                imgArr.push($(item).attr('data-img'));
            });
            if(imgArr.length > 1){
                utils.tools.alert("请不要选择多张图片!", {timer: 1200, type: 'warning'});
                return;
            }
            if(imgArr.length == 0){
                utils.tools.alert("请选择图片!", {timer: 1200, type: 'warning'});
                return;
            }
            var logo = imgArr.join(',');

            saveProductCommission(commission_Id, commission_productId, logo);
        });

        // 保存商品佣金设置
        function saveProductCommission(id, productId, logo) {
            var data = {
                id: id,
                productId: productId,
                logo: logo
            };
            $.ajax({
                url: host + '/product/savePrivilege',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_select_products").modal('hide');
                        $("#modal_product_commission").modal("hide");
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        $("#productName").on('focus', function() {
            $selectproductdatatables.search('').draw();
            $("#modal_select_products").modal("show");
        });

    });
//微信账号绑定
require(['all']);

require(['product/privilege']);

define("privilegeProduct", function(){});

