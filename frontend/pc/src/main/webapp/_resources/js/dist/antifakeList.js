/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('antifake/list',['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            ordering: false,
            sortable: false,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/antifake/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo:$("#orderNo").val(),
                    code:$("#code").val(),
                    pageable: true,
                }, function(res) {
                    if(res.errorCode != 200){
                        utils.tools.alert(res.moreInfo, {timer: 4000});
                    }else{
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords:res.data.total,
                            iTotalDisplayRecords:res.data.total
                        });
                    }
                });
            },
            rowId: 'id',
            columns: [{
                width:"80px",
                title: '物流码',
                data: 'code',
                name: 'code'
            },{
                width:"80px",
                title: '物流码类型',
                render: function (data, type, row) {
                    var value = row.codeType;
                    if (value == 'TRUNK') {
                        return "箱标";
                    } else if (value == 'BOX') {
                        return "盒标";
                    }
                }
            },{
                width:"80px",
                title: '订单号',
                data: 'orderNo',
                name: 'orderNo',
                render: function (data, type, row) {
                    var html = '<a class="viewDetailBtn" href="javascript:void(0)" style="margin-right: 10px;" orderNo="'+row.orderNo+'" > ' + row.orderNo + '</a>';
                    return html;
                }
            }, {
                width: 75,
                title: '发货时间',
                sortable: false,
                render: function (data, type, row) {
                    if (row.createdAt == null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:"80px",
                title: '发货代理',
                data: 'sellerName',
                name: 'sellerName'

            }, {
                width: 75,
                data: 'sellerType',
                name: 'sellerType',
                title: '发货代理级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.sellerType;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"80px",
                title: '发货代理手机号',
                data: 'sellerPhone',
                name: 'sellerPhone'

            },{
                width:"80px",
                title: '进货代理',
                data: 'buyerName',
                name: 'buyerName'

            }, {
                width: 75,
                data: 'buyerType',
                name: 'buyerType',
                title: '进货代理级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.buyerType;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            },{
                width:"80px",
                title: '进货代理手机号',
                data: 'buyerPhone',
                name: 'buyerPhone'

            },{
                width:"80px",
                title: '发货操作员',
                data: 'userName',
                name: 'userName'
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        //查看订单详情
        var orderNo = '';
        var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering:false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitter/order/orderItem", {
                    size: data.length,
                    page: (data.start / data.length),
                    orderNo : orderNo,
                    pageable: true,
                }, function(res) {
                    if (res.errorCode != '200') {
                        return;
                    }

                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.itemTotal,
                        recordsFiltered: res.data.itemTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.itemTotal,
                        iTotalDisplayRecords:res.data.itemTotal
                    });
                });
            },
            columns: [{
                data: 'productName',
                name: 'productName',
                width: 180,
                orderable: false,
                title: "商品"
            },{
                data: 'marketPrice',
                name: 'marketPrice',
                width: 100,
                orderable: false,
                title: "原价"
            },{
                data: 'price',
                name: 'price',
                width: 100,
                orderable: false,
                title: "订单价格"
            },{
                data: 'amount',
                name: 'amount',
                width: 100,
                orderable: false,
                title: "数量"
            },{
                data: 'addressDetails',
                name: 'addressDetails',
                width: 180,
                orderable: false,
                title: "收货地址"
            },{
                data: 'logisticsCompany',
                name: 'logisticsCompany',
                width: 120,
                orderable: false,
                title: "物流公司"
            },{
                data: 'logisticsOrderNo',
                name: 'logisticsOrderNo',
                width: 120,
                orderable: false,
                title: "物流单号"
            }, {
                width: "120px",
                orderable: false,
                title: "",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
            }, {
                width: "120px",
                orderable: false,
                title: "审核时间",
                render: function (data, type, row) {
                    if(row.shippedAt && row.shippedAt != ''){
                        var cDate = parseInt(row.shippedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }else{
                        return "";
                    }

                },
            }, {
                width: "120px",
                orderable: false,
                title: "收货时间",
                render: function (data, type, row) {
                    if(row.succeedAt && row.succeedAt != ''){
                        var cDate = parseInt(row.succeedAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }else{
                        return "";
                    }
                },
            }]
        });

           // Handle click on "Select all" control
        $('input[name=checkAll]').on('click', function(){
      // Check/uncheck all checkboxes in the table
            var rows = $datatables.rows({ 'search': 'applied' }).nodes();
            $('input[name=checkCommissions]', rows).prop('checked', this.checked);
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            $(".viewDetailBtn").on("click",function() {
                orderNo = $(this).attr('orderNo');
                $('#modal_orderInfo').modal('show');
                $orderTables.search('').draw();
            });

            tableRoleCheck('#guiderUserTable');

        }

        // 订单退款
        // 根据退款前订单状态判断是否退全款或者扣除运费，默认算出一个退款金额，用户也可手动更改
        function viewDetail(pId, refundFee){
            var data = {
                orderId: pId,
                refundment : refundFee
            };
            var url = window.host + "/order/refund";
            utils.postAjax(url,data,function(res){
                if(typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("退款成功!", {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                        $refund_datatables.search('').draw();
                    } else {
                        utils.tools.alert("退款失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }

        $(".btn-search").on('click', function () {
            $datatables.search('').draw();
        });

        $(".btn-orgView").on('click', function () {
            var orderNo = $("#orderNo").val();
            var code = $("#code").val();
            if(orderNo == '' && code == ''){
                utils.tools.alert('无追溯记录，请输入相关查询条件', {timer: 1500});
                return;
            }
            window.open("/antifake/orgView?orderNo=" + orderNo + "&code=" + code,'_blank');
        });

        buttonRoleCheck('.hideClass');

});
//微信账号绑定
require(['all']);

require(['antifake/list']);

define("antifakeList", function(){});

