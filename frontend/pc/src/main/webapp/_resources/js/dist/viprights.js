/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data) {
                            res.data = [];
                        }
                        callback({
                            recordsTotal: 10,
                            recordsFiltered: 10,
                            data: res.data,
                            iTotalRecords: 10,
                            iTotalDisplayRecords: 10
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * 封装日期控件
 */
define('datePicker',['jquery', 'daterangepicker', 'moment'], function ($) {

  /** 初始化日期控件 **/
  const options = {
    timePicker: true,
    dateLimit: {days: 60000},
    startDate: moment().subtract(0, 'month').startOf('month'),
    endDate: moment().subtract(0, 'month').endOf('month'),
    autoApply: false,
    timePickerIncrement: 1,
    locale: {
      format: 'YYYY/MM/DD',
      separator: ' - ',
      applyLabel: '确定',
      fromLabel: '开始日期:',
      toLabel: '结束日期:',
      cancelLabel: '清空',
      weekLabel: 'W',
      customRangeLabel: '日期范围',
      daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月",
        "11月", "12月"],
      firstDay: 6
    },
    ranges: {
      '今天': [moment(), moment().endOf('day')],
      '一星期': [moment(), moment().add(6, 'days')],
      '一个月': [moment(), moment().add(1, 'months')],
      '一年': [moment(), moment().add(1, 'year')]
    },
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small btn-default'

  };

  /* 单时间控件 */
  const singlePickerOptions = {
    singleDatePicker: true,
    showDropdowns: true
  };

  /**
   * 构造函数，创建一个dateRangePicker
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function DatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(options, function (start, end) {
      if (start._isValid && end._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm') + ' - '
            + end.format(
                'YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      console.log(picker.endDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      options.endDate = picker.endDate;
      params.onApply(picker.startDate.format('YYYY-MM-DD HH:mm'),
          picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
      //do something, like clearing an input
      $(this).val('');
      params.onEmpty();
    });

  }

  /**
   * 构造函数， 构造单时间控件
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function SingleDatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(singlePickerOptions, function (start) {
      if (start._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      var dateStr = picker.startDate.format('YYYY-MM-DD HH:mm');
      if (params.onApply) {
        params.onApply(dateStr);
      } else {
        $(this).val(dateStr)
      }
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function () {
      //do something, like clearing an input
      $(this).val('');
      if (params.onEmpty) {
        params.onEmpty();
      }
    });
  }

  /**
   * 原型，所有类型日期控件共有
   * @type {{resetStr: resetStr}}
   */
  const commonPrototype = {
    /**
     * 重置日期字符串
     * @param str
     */
    resetStr: function (str) {
      this.$dateRangeBasic.val(str);
    }
  };

  /**
   * 继承commonPrototype
   * @type {{resetStr: resetStr}}
   */
  DatePicker.prototype = SingleDatePicker.prototype = commonPrototype;

  return {

    /**
     * 初始化日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createPicker: function (params) {
      return new DatePicker(params);
    },

    /**
     * 初始化单日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createSinglePicker: function (params) {
      return new SingleDatePicker(params);
    }

  }

});

define('productModalSingle',['jquery', 'datatables'], function ($) {

  var $orders = ['price', 'amount', 'sales', 'onsale'];
  var $order = '';
  var $productListUrl = window.host + "/product/list";

  var $shopId = null;
  var $category = '';
  var $pDataTable;

  const $modal = $('#choose-product-modal');

  //初始化商品分类信息
  $.ajax({
    url: window.host + '/shop/category/list',
    type: 'POST',
    dataType: 'json',
    data: {},
    success: function (data) {
      if (data.errorCode === 200) {
        var dataLength = data.data.length;
        for (var i = 0; i < dataLength; i++) {
          $("#categoryType").append('<option value=' + data.data[i].id + '>'
              + data.data[i].name + '</option>');
        }
        ;
      } else {
        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
      }
    },
    error: function (state) {
      if (state.status === 401) {
      } else {
        utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
      }
    }
  });
    //初始化品牌
    $.ajax({
        url: window.host + '/brand/list',
        type: 'GET',
        dataType: 'json',
        data: {
            pageable: false,
        },
        success: function (data) {
            if (data.errorCode === 200) {
                var dataLength = data.data.list.length;
                for (var i = 0; i < dataLength; i++) {
                    $("#brandType").append('<option value=' + data.data.list[i].id + '>'
                        +data.data.list[i].name + '</option>');
                }
                ;
            } else {
                utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
            }
        },
        error: function (state) {
            if (state.status === 401) {
            } else {
                utils.tools.alert('获取品牌信息失败！', {timer: 1200, type: 'warning'});
            }
        }
    });
    //初始化供应商
    $.ajax({
        url: window.host + '/supplier/list?type=override',
        type: 'GET',
        dataType: 'json',
        data: {
            pageable: false,
        },
        success: function (data) {
            if (data.errorCode === 200) {
                var dataLength = data.data.list.length;
                for (var i = 0; i < dataLength; i++) {
                    $("#supplierType").append('<option value=' + data.data.list[i].id + '>'
                        +data.data.list[i].name + '</option>');
                }
                ;
            } else {
                utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
            }
        },
        error: function (state) {
            if (state.status === 401) {
            } else {
                utils.tools.alert('获取供应商信息失败！', {timer: 1200, type: 'warning'});
            }
        }
    });

  /**
   * 商品搜索
   */
 var newArr = {
      category:"",
      brand:"",
      supplier:"",
      reviewStatus:"",
      status:"",
      type:"NORMAL",
      brand:""
  }
  $('body').on('change', 'select[name="categoryType"]', function () {
    $productListUrl = window.host + "/product/list";
      category  = $(this).val();
      newArr.category  = category ;
  });
    $('body').on('change', 'select[name="brandType"]', function () {
        $productListUrl = window.host + "/product/list";
        brandType = $(this).val();
        newArr.brand = brandType;
    });
    $('body').on('change', 'select[name="supplierType"]', function () {
        $productListUrl = window.host + "/product/list";
        supplierType = $(this).val();
        newArr.supplier = supplierType;
    });
    $('body').on('change', 'select[name="reviewStatus"]', function () {
        $productListUrl = window.host + "/product/list";
        reviewStatus = $(this).val();
        newArr.reviewStatus = reviewStatus;
    });
    $('body').on('change', 'select[name="productStatus"]', function () {
        $productListUrl = window.host + "/product/list";
        productStatus = $(this).val();
        newArr.status = productStatus;
    });
    $('body').on('change', 'select[name="type"]', function () {
        $productListUrl = window.host + "/product/list";
        type = $(this).val();
        newArr.type = type;
    });

  $(".btn-search-products").on('click', function () {
    console.log(newArr)

    var keyword = $.trim($("#select_products_sKeyword").val());

    if (keyword !== '' && keyword.length > 0 && shopId !== null) {
      $productListUrl = window.host + '/product/list?status=' + newArr.status + "&reviewStatus=" + newArr.reviewStatus + 
      "&brand=" + newArr.brand + "&supplier=" + newArr.supplier + "&category=" + newArr.category + "&type=" +newArr.type;
      $pDataTable.search(keyword).draw();
    } else if (keyword === '' || keyword.length === 0) {
      $productListUrl = window.host + "/product/list?category=" + newArr.category + "&status=" +
          newArr.status + "&reviewStatus=" + newArr.reviewStatus + "&brand=" + newArr.brand + "&supplier=" + newArr.supplier + "&type=" +newArr.type;
      $pDataTable.search('').draw();
    }

  });

      /*
      * 弹出层的商品
      * */
  function initTable(params) {
    $pDataTable = $('#xquark_list_tables').DataTable({
      paging: true, //是否分页
      filter: false, //是否显示过滤
      lengthChange: false,
      processing: true,
      serverSide: true,
      deferRender: true,
      searching: true,
      ajax: function (data, callback, settings) {
        $.get($productListUrl, {
          size: data.length,
          page: (data.start / data.length),
          keyword: data.search.value,
          pageable: true,
          order: function () {
            if ($order !== '') {
              return $order;
            } else {
              var _index = data.order[0].column;
              if (_index < 4) {
                return '';
              } else {
                return $orders[_index - 4];
              }
            }
          },
          direction: data.order ? data.order[0].dir : 'asc',
          type: $category,
          isGroupon: ''
        }, function (res) {
          if (!res.data) {
            res.data = [];
          } else {
              console.log(res,"请求的res")
            if (res.data.shopId) {
              $shopId = res.data.shopId;
            }
          }
          callback({
            recordsTotal: res.data.categoryTotal,
            recordsFiltered: res.data.categoryTotal,
            data: res.data.list,
            iTotalRecords: res.data.categoryTotal,
            iTotalDisplayRecords: res.data.categoryTotal
          });
        });
      },
      rowId: "id",
      columns: [
          //选择商品
          {
              title: "选择",
              width: "20px",
              orderable: false,
              render: function (data, type, row) {
                  return '<input name="checkbox" rowId="'+ row.id+'" type="checkbox" value="'+ row.id+'" class="checkbox"/>';
              }
          },
          //商品图片
        {
            title: "商品图片",
          width: "30px",
          orderable: false,
          render: function (data, type, row) {
            return '<img class="goods-image" src="' + row.imgUrl + '" />';
          }
        },
          //商品ID
          {
              title: "商品ID",
              data: "id",
              orderable: true,
              width: "50px",
              name: "id"
          },
          //商品名称
          {
              title: "商品名称",
              title: "商品名称",
              data: "name",
              orderable: true,
              width: "50px",
              name: "name"
          },
          //商品状态
         {
             title: "商品状态",
          width: "40px",
          orderable: false,
          render: function (data, type, row) {
            var status = '';
            switch (row.status) {
              case 'DELAY':
                status = '计划发布';
                break;
              case 'ONSALE':
                status = '已上架';
                break;
              case 'INSTOCK':
                status = '已下架';
                break;
              case 'DRAFT':
                status = '草稿箱';
                break;
              default:
                break;
            }
            return status;
          },
        },
          //售价
          {
              title: "售价",
          data: "marketPrice",
          orderable: true,
          width: "50px",
          name: "marketPrice"
        },
          //库存
        {
            title: "库存",
          data: "amount",
          orderable: true,
          width: "50px",
          name: "amount"
        },
      ],
      drawCallback: function () {  //数据加载完成
        initSelectProductEvent(params);
      }
    });
      /*
       * 点击确定，商品成为VIP选项
       * */
      var checkedArray = new Array();//放已经选择的checkbox的value
      var count;//已经选择的个数
      $('.btn-primary').click(function() {
          checkedArray.length=0;
          count=0;
          $('[name=checkbox]:checkbox:checked').each(function() {
                  checkedArray.push($(this).val());
                  count++;
              });
          if (checkedArray.length==0) {
              alert("Please check one at least.");
              return;
          }
          console.log("已选复选框的值",checkedArray);
          var comfireIds = checkedArray.toString();
          console.log(comfireIds)
          /*
          *  点击向后台
          * */
          $.ajax({
              url: window.host + '/rights/product/addVipsuits',
              type: 'POST',
              dataType: 'json',
              data: {
                  productCodeStr: comfireIds,
              },
              success: function (data) {
                  if (data.errorCode == 200) {
                      console.log("OK")
                  } else {
                      data.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                      console.log("222")
                  }
              },
              error: function (state) {
                  if (state.errorCode === 200) {
                      console.log("err")
                  }
              }
          });
          });
  }




  function initSelectProductEvent(params) {
    $(".selectproduct").on("click", function () {
      var id = $(this).attr("rowId");
      var name = $(this).attr("productName");
      var price = $(this).attr("productName");
      //回调外部函数
      if (params && params.onSelect) {
        params.onSelect(id, name, price);
      }
      $modal.modal("hide");
    });
  }

  return {
    /**
     * 创建商品表格
     * @param params
     */
    create: function (params) {
      initTable(params);

      /* 选择活动商品 */
      $(params.dom).on('focus', function () {
        $modal.modal("show");
      });
    },
    refresh: function () {
      $pDataTable.search('').draw();
    }
  }
});
define('fileUploader',['jquery', 'dropzone'], function ($) {
    var productType = $('#type').val();
    var $listUrl = window.host + "/product/list?type=" + productType;
  const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

  /**
   * 新建dropZone封装实例
   * @param params.onSuccess 成功后的回调函数，返回地址
   * @param params.dom drop的dom节点
   * @param params.url 上传文件地址，不指定则使用默认的
   * @constructor
   */
  function Uploader(params) {
    /* jquery 的dropZone实例 */
    this.jDropInstance = $(params.dom).dropzone({
      url: defaultUrl,
      paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
      dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
      maxFilesize: 10, // MB      //最大文件大小，单位是 MB
      maxFiles: 10,               //限制最多文件数量
      maxThumbnailFilesize: 10,
      addRemoveLinks: true,
      thumbnailWidth: "150",      //设置缩略图的缩略比
      thumbnailHeight: "150",     //设置缩略图的缩略比
      acceptedFiles: ".gif,.png,.jpg",
      uploadMultiple: false,
      dictInvalidFileType: "文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
      dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
      dictRemoveFile: "删除",                                        //移除文件链接的文本
      dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
      dictResponseError: "服务器暂无响应,请稍后再试!",
      dictCancelUpload: "取消上传",
      dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
      dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
      autoDiscover: false,
      //autoProcessQueue: false,
      init: function () {

        // console.log(this);

        // var imgDropzone = this;
        //添加了文件的事件
        this.on("addedfile", function (file) {
          $(".btn-submit").addClass("disabled");
          if (file && file.dataImg && file.previewElement) { //是网络加载的数据
            $(file.previewElement).attr("data-img", file.dataImg);
            if (file.size === '' || file.length === 0) {
              $(file.previewElement).find(".dz-details").hide();
            }
          }
          //imgDropzone.processQueue();
        });
        this.on("success", function (file, data) {
          if (typeof(data) === 'object') {
            switch (data.errorCode) {
              case 200: {
                if (typeof(data.data) === 'object') {
                  var imgId = data.data[0].id;
                  if (file && file.previewElement) {
                    $(file.previewElement).attr("data-img", imgId);
                    if (params.onSuccess) {
                      params.onSuccess(imgId);
                    }
                  }
                }
                break;
              }
              default: {
                utils.tools.alert("图像上传失败,请重新选择!",
                    {timer: 1200, type: 'warning'});
                break;
              }
            }
          } else {
            if (data === -1) {
              utils.tools.alert("图像上传失败,请重新选择!",
                  {timer: 1200, type: 'warning'});
            }
          }
          $(".btn-submit").removeClass("disabled");
        });

        this.on("error", function () {
          utils.tools.alert("文件上传失败!", {timer: 1200, type: 'warning'});
          $(".dz-error-message").html("文件上传失败!");
          $(".btn-submit").removeClass("disabled");
        });

        this.on("complete", function () {   //上传完成,在success之后执行
          $(".btn-submit").removeClass("disabled");
        });

        this.on("thumbnail", function (file) {
          if (file && file.previewTemplate) {
            file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
            file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
          }
          const $dzImage = $('.dz-image');
          $dzImage.css("height", "150px;");
          $dzImage.css("width", "150px;");
        });

      }
    });

    /* 真实的dropZone实例 */
    this.dropZone = this.jDropInstance[0].dropzone;

    // console.log(this.dropZone);

    /**
     * 清空dropZone的上传文件
     */
    this.clear = function () {
      this.dropZone.removeAllFiles(true);
      return this;
    };

    /**
     * 添加图片预览到dropZone
     * @param imgUrl 图片地址
     */
    this.addImage =  function(imgUrl) {
      if (imgUrl) {
        var mockFile = {name: "", size: "", dataImg: imgUrl};
        console.log(this);
        this.dropZone.emit("addedfile", mockFile);
        this.dropZone.emit("thumbnail", mockFile, imgUrl);
        this.dropZone.emit("complete", mockFile);
        this.dropZone.files.push( mockFile ); // 此处必须手动添加才可以用removeAllFiles移除
      }
      return this;
    }
  }

  return {
    /**
     * 新建一个文件上传实例并返回
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @returns {Uploader}
     */
    createUploader: function (params) {
      return new Uploader(params);
    }
  }

});

define('validator',['utils'], function (utils) {

  /**
   * 参数校验
   * @param rules 校验规则
   * @param data 参数对象
   * @param messageHandler 处理错误消息的函数
   * @returns {boolean}
   */
  function validateData(rules, data, messageHandler) {
    for (var k in data) {
      if (data.hasOwnProperty(k)) {
        if (rules.hasOwnProperty(k)) {
          var value = data[k];
          // 循环参数校验
          for (var i in rules[k]) {
            if (rules[k].hasOwnProperty(i)) {
              var checkObj = rules[k][i];
              var checker = checkObj.checker;
              if (!checker.call(null, value)) {
                if (!messageHandler) {
                  messageHandler = function (message) {
                    utils.tools.alert(message, { timer: 1200, type: 'warning' });
                  }
                }
                messageHandler(checkObj.message);
                return false;
              }
            }
          }
        }
      }
    }
    return true
  }

  return {
    validate: validateData
  }

});

define('flashSale/list',['jquery', 'utils', 'datePicker', 'productModalSingle', 'fileUploader',
      'validator',
      'jquerySerializeObject',
      'datatables', 'blockui', 'select2'],
    function ($, utils, datePicker, productModal, uploader, validator) {
        // console.log(window.host,"域名")
      const prefix = window.host + '/flashSale';
      // const listUrl = window.host + '/listPc';

        const listUrl = prefix + '/listPc';
      const saveUrl = prefix + '/saveProduct';
        const vipUrl = window.host + '/rights/product/vipsuits/list';  //vip商品列表
      const vipsuitUrl = window.host + '/rights/product/updateVipsuits';//更改VIP状态
      const viewUrl = prefix + '/view';
      const checkInPromotionUrl = prefix + '/checkInPromotion';

      var $dataTable;

      var globalDatePicker;

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });


        var $datatables = utils.createDataTable('#xquark_flash_sale_list', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            ordering: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                console.log(data,"data")
                $.get(vipUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                    }
                    // console.log(res.data,"列表")
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.vipsuits,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            status: 'vipsuitStatus',
            columns: [
                {
                    title: '活动图片',
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<img class="goods-image" src="'
                            + row.img + '" />';
                    }
                },
                {
                    title: '商品ID',
                    data: "productId",
                    width: "80px",
                    orderable: false,
                    name: "promotionName"
                },
                {
                    title: '商品名称',
                    data: "productName",
                    width: "80px",
                    orderable: false,
                    name: "productName"
                },
                {
                    title: '状态',
                    orderable: false,
                    width: "80px",
                    data: "vipsuitStatus",
                    render: function (data, type, row) {
                        if (data == 0) {
                            return '<span style="color: red;">已失效</span>';
                        }else if(data == 1){
                            return '<span style="color: red;">待生效</span>';
                        }else{
                            return '已生效';
                        }
                    }
                },
                {
                    title: '商品供应商',
                    orderable: false,
                    width: "80px",
                    data:"supplierName",
                    name: "supplierName"
                },
                {
                    title: '售价',
                    orderable: false,
                    width: "80px",
                    data:"marketPrice",
                    name: "marketPrice"
                },
                {
                    title: '库存',
                    orderable: false,
                    width: "80px",
                    data:"skuNum",
                    name: "skuNum"
                },
                {
                    title: '操作',
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    data: "vipsuitStatus",
                    render: function (data, type, row) {
                        // console.log(data,"data")
                        //     console.log(row,"row")
                        var html = '';
                        if(data == 2){
                            html += '<button class="effect" rowId="'
                                + row.productId
                                + '" >失效</button>';
                        }else if(data == 0){
                            html += '<button class="ok" rowId="'
                                + row.productId
                                + '">生效</button>';
                        }else{
                            html += '<button class="ok" rowId="'
                                + row.productId
                                + '">生效</button>';
                            html += '<button class="dele" style="margin-left: 10px; color: red;" rowId="'
                                + row.productId
                                + '">删除</button>';
                        }
                        return html;
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件

                $('.effect').on('click',function (e) {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认失效吗', function () {
                        utils.postAjax(vipsuitUrl, {productid: id,status: 0}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试",
                                    {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    utils.tools.alert('操作成功',
                                        {timer: 1200, type: 'success'});
                                    $datatables.search('').draw();
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo,
                                            {timer: 1200, type: 'warning'});
                                    } else {
                                        utils.tools.alert('服务器错误',
                                            {timer: 1200, type: 'warning'});
                                    }
                                }
                            }
                        });
                    },function () {
                    });
                    return this
                });

                $('.dele').on('click',function (e) {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认删除吗', function () {
                        utils.postAjax(vipsuitUrl, {productid: id,status: 3}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试",
                                    {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    utils.tools.alert('操作成功',
                                        {timer: 1200, type: 'success'});
                                    $datatables.search('').draw();
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo,
                                            {timer: 1200, type: 'warning'});
                                    } else {
                                        utils.tools.alert('服务器错误',
                                            {timer: 1200, type: 'warning'});
                                    }
                                }
                            }
                        });
                    },function () {
                    });
                    return this
                })
                $('.ok').on('click', function (e) {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认生效吗', function () {
                        utils.postAjax(vipsuitUrl, {productid: id,status: 2}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试",
                                    {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    utils.tools.alert('操作成功',
                                        {timer: 1200, type: 'success'});
                                    $datatables.search('').draw();
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo,
                                            {timer: 1200, type: 'warning'});
                                    } else {
                                        utils.tools.alert('服务器错误',
                                            {timer: 1200, type: 'warning'});
                                    }
                                }
                            }
                        });
                    }, function () {
                    });
                    return this
                });
            }

        });
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

      const manager = (function () {

        const $addFlashSaleBtn = $('.flash-sale-add');
        const $saveFlashSaleBtn = $('.saveFlashSaleBtn');
        const $modalFlashSale = $('#modal_flash_sale');

        const $validFrom = $('#valid_from');
        const $validTo = $('#valid_to');

        const $form = $('#flashSaleForm');

        const $img = $('#img');

        var imgUploader = uploader.createUploader({
          dom: '#product_image_dropzone',
          onSuccess: function (img) {
            $img.val(img);
          }
        });

        const notNull = function (field) {
          return field && field !== '';
        };

        const betweenOneToTen = function (field) {
          return field >= 1 && field <= 10;
        };

        const notNegative = function (field) {
          return field && parseFloat(field) > 0;
        };

        const validationRules = {
          title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          discount: [
            {
              checker: notNull,
              message: '请输入抢购价格'
            },
            {
              checker: betweenOneToTen,
              message: '抢购折扣必须在1到10折之间'
            }
          ],
          amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ],
          limitAmount: [
            {
              checker: notNull,
              message: '请输入限购库存'
            },
            {
              checker: notNegative,
              message: '限购库存不能小于0'
            }
          ]
        };

        const globalInstance = {

          bindEvent: function () {

            /* 点击显示增加预购商品弹窗 */
            $addFlashSaleBtn.on('click', function () {
              utils.tools.syncForm($form);
              // FIXME reset没有生效, 先手动设置
              $('#promotionId').val('');
              $('#id').val('');
              $('#productId').val('');
              $('#img').val('');
              $('#valid_from').val('');
              $('#valid_to').val('');
              imgUploader.clear();
              $modalFlashSale.modal('show');
            });

            /* 保存预购商品事件 */
            $saveFlashSaleBtn.on('click', function (e) {
              e.preventDefault();
              const params = $form.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(
                  params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              console.log(params);
              utils.postAjax(saveUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $datatables.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });

            return this;
          },
          makeDatePicker: function () {
            /* 初始话日期控件 */
            globalDatePicker = datePicker.createPicker({
              dom: '.daterange-time',
              onApply: function (start, end) {
                $validFrom.val(start);
                $validTo.val(end);
              },
              onEmpty: function () {
                $validFrom.val('');
                $validTo.val('');
              }
            });
            return this;
          },
          makeProductModal: function () {
            /**
             * 初始化商品选择表格
             */
            productModal.create({
              dom: '#productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'),
                    checkInPromotionUrl, {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#productId").val(id);
                            $("#productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }
            });
          }
        };

        return {
          initGlobal: function () {
            globalInstance.bindEvent()
            .makeDatePicker()
            .makeProductModal();
            return this;
          },
          initTable: function () {
            if (!$dataTable) {


            }
            return this;
              /* 初始化页面 */
              manager.initGlobal()
                  .initTable();
          }
        }
      })();

      function addProductImage(imgUrl) {
        if (imgUrl) {
          var mockFile = {name: "", size: "", dataImg: imgUrl};
          uploader[0].dropzone.emit("addedfile", mockFile);
          uploader[0].dropzone.emit("thumbnail", mockFile, imgUrl);
          uploader[0].dropzone.emit("complete", mockFile);
        }
      }

      /* 初始化页面 */
      manager.initGlobal()
      .initTable();

    });

require(['all']);

//module
require(['jquery', 'uniform', 'placeholder', 'flashSale/list'], function () {

});
define("flashSale", function(){});

