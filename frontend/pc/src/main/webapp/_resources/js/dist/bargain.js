/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/24.
 */
define('product/upload',['jquery','utils','dropzone'], function($,utils) {
    Dropzone.autoDiscover = false;

    var imgDropzone =  $("#product_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 100, // MB      //最大文件大小，单位是 MB ,不限制
        maxFiles: 10,               //限制最多文件数量
        maxThumbnailFilesize: 10,
        addRemoveLinks: true,
        thumbnailWidth:"150",      //设置缩略图的缩略比
        thumbnailHeight:"150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                        //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",              //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function() {

            // var imgDropzone = this;
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".btn-submit").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
                //imgDropzone.processQueue();
            });
            this.on("success", function(file,data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                }
                $(".dz-image").css("height","150px;");
                $(".dz-image").css("width","150px;");

            });

        }
    });

    return imgDropzone;
});
define('bargain/list',['jquery', 'utils', 'daterangepicker', 'moment', 'product/upload',
        'jquerySerializeObject', 'datatables', 'blockui', 'select2'],
    function ($, utils, daterangepicker, moment, uploader) {

        var productListUrl = window.host + "/product/list";

        const prefix = window.host + '/promotionBargain';
        const listUrl = prefix + '/listPromotion';
        const viewUrl = prefix + '/viewVO';
        const listProductUrl = prefix + '/product/listTable';
        const deleteProductUrl = prefix + '/delete';
        const saveUrl = prefix + '/savePromotion';
        const saveProductUrl = prefix + '/save';
        const closeUrl = prefix + '/close';

        const $dataTable = $('#xquark_bargain_products_tables');
        const $dateRangePicker = $('.dateRange-time');
        const $searchBtn = $('.btn-search');
        const $body = $('body');

        var $category = '';

        var promotionId = '';
        var shopId = '';
        var keyWord = '';

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "暂无相关数据"
            }
        });

        /** 初始化日期控件 **/
        const options = {
            timePicker: true,
            dateLimit: {days: 60000},
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            autoApply: false,
            timePickerIncrement: 1,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月",
                    "9月", "10月", "11月", "12月"],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment().endOf('day')],
                '一星期': [moment(), moment().add(6, 'days')],
                '一个月': [moment(), moment().add(1, 'months')],
                '一年': [moment(), moment().add(1, 'year')]
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        const manager = (function () {
            const $saveBtn = $('.btn-release');
            const $saveBtnConfirm = $('.saveBtn');
            const $saveProductConfirm = $('.saveProductBtn');
            const $addProductBtn = $('.btn-addProduct');

            const globalInstance = {
                bindEvent: function () {
                    $saveBtn.on('click', function () {
                        $("#id").val('');
                        $("#title").val('');
                        $dateRangePicker.val('');
                        $("#modal_bargain").modal("show");
                    });
                    $addProductBtn.on('click', function () {
                        $("#promotionId").val(promotionId);
                        $("#bargainId").val('');
                        $("#productId").val('');
                        $("#productName").val('');
                        $('#originalPrice').val('');
                        $('#max_discount').val('');
                        $('#priceStart').val('');
                        $('#priceEnd').val('');
                        $('#amount').val('');
                        $('#max_times').val('');
                        $("#modal_products_detail").modal("show");
                    });
                    $saveProductConfirm.on('click', function () {
                        var bargainId = $('#bargainId').val();
                        var productId = $("#productId").val();

                        var maxDiscount = $('#max_discount').val();
                        var originalPrice = $('#originalPrice').val();
                        var maxTimes = $('#max_times').val();

                        var priceStart = $('#priceStart').val();
                        var priceEnd = $('#priceEnd').val();
                        var amount = $('#amount').val();

                        if (!maxDiscount || maxDiscount === '') {
                            utils.tools.alert("请输入最低价!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!maxTimes || maxTimes === '') {
                            utils.tools.alert("请输入最大砍价次数!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!productId || productId === '') {
                            utils.tools.alert("请选择商品!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (parseFloat(maxDiscount) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('商品最低价不能高于原价!',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!priceStart || !priceEnd || priceStart
                            === '' || priceEnd === '') {
                            utils.tools.alert('请输入砍价的折扣范围',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!amount || amount === '') {
                            utils.tools.alert('请输入商品库存信息',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        else if (parseFloat(priceStart) < 0 || parseFloat(priceEnd) < 0) {
                            utils.tools.alert('砍价范围不能为负数', {timer: 1200, type: 'warning'});
                            return;
                        }
                        else if (parseFloat(priceEnd) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('砍价最高范围不能高于商品原价',
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (parseFloat(priceStart) > parseFloat(
                            originalPrice)) {
                            utils.tools.alert('砍价最低范围不能高于商品原价',
                                {timer: 1200, type: 'warning'});
                            return;
                        }

                        var data = {
                            id: bargainId,
                            promotionId: promotionId,
                            productId: productId,
                            maxDiscount: maxDiscount,
                            maxTimes: maxTimes,
                            priceStart: priceStart,
                            priceEnd: priceEnd,
                            amount: amount
                        };
                        utils.postAjaxWithBlock($(document), saveProductUrl,
                            data, function (res) {
                                if (typeof(res) === 'object') {
                                    switch (res.errorCode) {
                                        case 200: {
                                            if (res.data) {
                                                utils.tools.alert(
                                                    '操作成功', {
                                                        timer: 1200,
                                                        type: 'success'
                                                    });
                                                $("#modal_products_detail").modal(
                                                    "hide");
                                                $productDataTables.search(
                                                    '').draw();
                                            } else {
                                                alert("此商品已存在于活动中，不能重复添加");
                                            }
                                            break;
                                        }
                                        default: {
                                            utils.tools.alert(
                                                res.moreInfo,
                                                {timer: 1200});
                                            break;
                                        }
                                    }
                                } else if (res === 0) {

                                } else if (res === -1) {
                                    utils.tools.alert("网络问题,请稍后再试",
                                        {timer: 1200});
                                }
                            });
                    });
                    // 选择活动商品
                    $("#productName").on('focus', function () {
                        $selectProductDataTables.search('').draw();
                        $("#modal_select_products").modal("show");
                    });
                    $searchBtn.on('click', function () {
                        keyWord = $('#sKeyword').val();
                        $bargainDataTables.search(keyWord).draw();
                    });
                    $saveBtnConfirm.on('click', function () {
                        var id = $("#id").val();
                        var validFrom = $("#valid_from").val();
                        var validTo = $("#valid_to").val();
                        var title = $("#title").val();

                        var imgArr = [];
                        $('#product_image_dropzone > .dz-complete').each(
                            function (i, item) {
                                imgArr.push($(item).attr('data-img'));
                            });

                        if (!validFrom || validFrom === '' || !validTo
                            || validTo === '') {
                            utils.tools.alert("请选择活动时间!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (!title || title === '') {
                            utils.tools.alert("请输入标题!",
                                {timer: 1200, type: 'warning'});
                            return;
                        } else if (imgArr.length > 1) {
                            utils.tools.alert("请不要选择多张图片!",
                                {timer: 1200, type: 'warning'});
                            return;
                        }

                        var data = {
                            id: id,
                            validFrom: validFrom,
                            validTo: validTo,
                            title: title,
                            img: imgArr.join(',')
                        };
                        utils.postAjaxWithBlock($(document), saveUrl, data,
                            function (res) {
                                if (typeof(res) === 'object') {
                                    switch (res.errorCode) {
                                        case 200: {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $bargainDataTables.search(
                                                '').draw();
                                            $('#modal_bargain').modal(
                                                'hide');
                                            break;
                                        }
                                        default: {
                                            utils.tools.alert(
                                                res.moreInfo,
                                                {timer: 1200});
                                            break;
                                        }
                                    }
                                } else if (res === 0) {

                                } else if (res === -1) {
                                    utils.tools.alert("网络问题,请稍后再试",
                                        {timer: 1200});
                                }
                            });
                    });

                    $dateRangePicker.on('apply.daterangepicker',
                        function (ev, picker) {
                            options.startDate = picker.startDate;
                            options.endDate = picker.endDate;
                            $("#valid_from").val(
                                picker.startDate.format(
                                    'YYYY-MM-DD HH:mm'));
                            $("#valid_to").val(picker.endDate.format(
                                'YYYY-MM-DD HH:mm'));
                        });

                    /**
                     * 清空按钮清空选框
                     */
                    $dateRangePicker.on('cancel.daterangepicker',
                        function (ev, picker) {
                            //do something, like clearing an input
                            $dateRangePicker.val('');
                            $("#valid_from").val('');
                            $("#valid_to").val('');
                        });

                    $searchBtn.on('click', function () {
                        keyWord = $('#sKeyword').val();
                        $bargainDataTables.search(keyWord).draw();
                    });

                    return this;
                },
                initDateRangePricker: function () {
                    $dateRangePicker.daterangepicker(options,
                        function (start, end) {
                            if (start._isValid && end._isValid) {
                                $dateRangePicker.val(start.format(
                                    'YYYY-MM-DD HH:mm') + ' - '
                                    + end.format(
                                        'YYYY-MM-DD HH:mm'));
                            } else {
                                $dateRangePicker.val('');
                            }
                        });
                    return this;
                },
                initProductCategory: function () {
                    //初始化商品分类信息
                    $.ajax({
                        url: window.host + '/shop/category/list',
                        type: 'POST',
                        dataType: 'json',
                        data: {},
                        success: function (data) {
                            if (data.errorCode == 200) {
                                var dataLength = data.data.length;
                                for (var i = 0; i < dataLength; i++) {
                                    $("#categoryType").append('<option value='
                                        + data.data[i].id + '>'
                                        + data.data[i].name
                                        + '</option>');
                                }
                                ;
                            } else {
                                utils.tools.alert(data.moreInfo,
                                    {timer: 1200, type: 'warning'});
                            }
                        },
                        error: function (state) {
                            if (state.status == 401) {
                            } else {
                                utils.tools.alert('获取店铺分类信息失败！',
                                    {timer: 1200, type: 'warning'});
                            }
                        }
                    });
                    $('body').on('change', 'select[name="categoryType"]', function (event) {
                        $category = $(this).val();
                        productListUrl = window.host + "/product/list";
                        $selectProductDataTables.search('').draw();
                    });
                    return this;
                }
            };

            return {
                initGlobal: function () {
                    globalInstance.initDateRangePricker()
                        .bindEvent().initProductCategory();
                },
                initTable: function () {

                    $body.unbind('click');

                    $(".edit").on("click", function () {

                        $("#product_image_dropzone").html('');

                        var id = $(this).attr("rowId");
                        var validFrom = $(this).attr("validFrom");
                        var validTo = $(this).attr("validTo");
                        var title = $(this).attr("title");

                        var originImg = $(this).attr("originImg");
                        var imgUrl = $(this).attr("img");

                        var d = new Date(parseInt(validFrom));
                        validFrom = d.format('yyyy-MM-dd hh:mm');
                        d = new Date(parseInt(validTo));
                        validTo = d.format('yyyy-MM-dd hh:mm');
                        $('.dateRange-time').val(validFrom + ' - '
                            + validTo);

                        $("#id").val(id);
                        $("#valid_from").val(validFrom);
                        $("#valid_to").val(validTo);
                        $("#title").val(title);

                        //图片
                        if (originImg) {
                            var key = originImg,
                                url = imgUrl,
                                imgObject = {
                                    imgUrl: url,
                                    id: key
                                };
                            var mockFile = {
                                name: "",
                                size: "",
                                dataImg: imgObject.id
                            };
                            uploader[0].dropzone.emit("addedfile",
                                mockFile);
                            uploader[0].dropzone.emit("thumbnail", mockFile,
                                imgObject.imgUrl);
                            uploader[0].dropzone.emit("complete", mockFile);
                        }

                        $("#modal_bargain").modal("show");
                    });

                    $(".products").on('click', function () {
                        promotionId = $(this).attr("rowId");
                        $productDataTables.search('').draw();
                        $("#modal_products").modal("show");
                    });

                    /** 点击删除弹出框 **/
                    $("[data-toggle='popover']").popover({
                        trigger: 'manual',
                        placement: 'left',
                        html: 'true',
                        animation: true,
                        content: function () {
                            var rowId = $(this).attr("rowId");
                            return '<span>确认结束？</span>' +
                                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                                + rowId + '">确认</button>' +
                                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                        }
                    });

                    $('[data-toggle="popover"]').popover() //弹窗
                        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                            $(this).parent().parent().siblings().find(
                                '[data-toggle="popover"]').popover('hide');
                        }).on('shown.bs.popover', function () {
                        var that = this;
                        $('.popover-btn-ok').on("click", function () {
                            var pId = $(this).attr("pId");
                            utils.postAjax(closeUrl, {id: pId},
                                function (res) {
                                    if (typeof(res) === 'object') {
                                        if (res.data) {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $bargainDataTables.search(
                                                '').draw();
                                        } else {
                                            utils.tools.alert("操作失败!", {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        }
                                    }
                                });
                        });

                        $('.popover-btn-cancel').on("click", function () {
                            $(that).popover("hide");
                        });
                    });

                    $body.on('click', function (event) {
                        var target = $(event.target);
                        if (!target.hasClass('popover') //弹窗内部点击不关闭
                            && target.parent('.popover-content').length
                            === 0
                            && target.parent('.popover-title').length
                            === 0
                            && target.parent('.popover').length === 0
                            && target.data("toggle") !== "popover") {
                            //弹窗触发列不关闭，否则显示后隐藏
                            $('[data-toggle="popover"]').popover('hide');
                        } else if (target.data("toggle") === "popover") {
                            target.popover("toggle");
                        }
                    });
                },
                initProductEvent: function () {
                    $(".productedit").on("click", function () {
                        var id = $(this).attr("rowId");
                        utils.postAjax(viewUrl, {id: id}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试",
                                    {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    if (res.data) {
                                        var data = res.data;
                                        $("#bargainId").val(id);
                                        promotionId = data.promotionId;
                                        $("#promotionId").val(
                                            data.promotionId);
                                        $('#productId').val(data.productId);
                                        $("#productName").val(
                                            data.productName);
                                        $('#originalPrice').val(
                                            data.productPrice);
                                        $("#max_times").val(data.maxTimes);
                                        $('#max_discount').val(
                                            data.maxDiscount);
                                        $('#priceStart').val(
                                            data.priceStart);
                                        $('#priceEnd').val(data.priceEnd);
                                        $('#amount').val(data.amount);
                                        $("#modal_products_detail").modal(
                                            "show");
                                    }
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo, {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    } else {
                                        utils.tools.alert('服务器错误', {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    }
                                }
                            }
                        });

                    });

                    $(".productdel").on("click", function () {
                        var id = $(this).attr("rowId");
                        utils.tools.confirm('确定该商品不再参与此活动?', function () {
                            utils.postAjax(deleteProductUrl, {id: id},
                                function (res) {
                                    if (typeof(res) === 'object') {
                                        if (res.data) {
                                            utils.tools.alert('操作成功', {
                                                timer: 1200,
                                                type: 'success'
                                            });
                                            $productDataTables.search(
                                                '').draw();
                                        } else {
                                            utils.tools.alert("操作失败!", {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        }
                                    }
                                });
                        }, function () {

                        });

                    });
                },
                initSelectProductEvent: function () {
                    $(".selectproduct").on("click", function () {
                        var productId = $(this).attr("rowId");
                        var productName = $(this).attr("productName");
                        var originalPrice = $(this).attr('productPrice');
                        $("#productId").val(productId);
                        $("#productName").val(productName);
                        $('#originalPrice').val(originalPrice);
                        $("#modal_select_products").modal("hide");
                    });
                }
            }
        })();

        manager.initGlobal();

        /** 初始化表格数据 **/
        const $bargainDataTables = utils.createDataTable('#xquark_bargain_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true,
                    keyWord: keyWord
                    // order: $columns[data.order[0].column],
                    // direction: data.order[0].dir
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    width: "10px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'
                            + row.id + '"></label>';
                    }
                },
                {
                    title: "标题",
                    data: "title",
                    orderable: false,
                    name: "title"
                }, {
                    title: "开始时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validFrom);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validFrom"
                }, {
                    title: "结束时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validTo);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validTo"
                }, {
                    title: "状态",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch (row.archive) {
                            case false:
                                status = '进行中';
                                break;
                            case true:
                                status = '已结束';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "onsaleAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" validFrom="'
                            + row.validFrom + '" validTo="'
                            + row.validTo + '" img="' + row.img
                            + '" originImg="' + row.originImg
                            + '" title="' + row.title + '" rowId="'
                            + row.id
                            + '" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="products" style="margin-left: 10px;" rowId="'
                            + row.id
                            + '" ><i class="icon-pencil7"></i>商品管理</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'
                            + row.id
                            + '" ><i class="icon-trash"></i>结束</a>';
                        return html;
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                manager.initTable();
            }

        });

        var $productDataTables = utils.createDataTable('#xquark_list_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(listProductUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    id: promotionId,
                    pageable: true,
                    category: $category
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                    } else {
                        if (res.data.shopId) {
                            shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<img class="goods-image" src="'
                            + row.productImg + '" />';
                    }
                },
                {
                    title: "商品",
                    data: "productName",
                    orderable: false,
                    name: "title"
                }, {
                    title: "原价",
                    data: "productPrice",
                    orderable: false,
                    name: "price"
                }, {
                    title: "最低价",
                    data: "maxDiscount",
                    orderable: false,
                    name: "maxDiscount"
                }, {
                    title: "最大砍价次数",
                    data: "maxTimes",
                    orderable: false,
                    name: "maxTimes"
                }, {
                    title: "砍价金额范围",
                    orderable: false,
                    render: function (data, type, row) {
                        var priceStart = row.priceStart ? row.priceStart
                            : 0;
                        var priceEnd = row.priceEnd ? row.priceEnd : 0;
                        return priceStart + ' 元 - ' + priceEnd + ' 元';
                    }
                },
                {
                    title: "活动商品数",
                    orderable: false,
                    render: function (data, type, row) {
                        var amount = row.amount;
                        return amount + '件';
                    }
                },
                {
                    title: "已售商品数量",
                    orderable: false,
                    render: function (data, type, row) {
                        var sales = row.sales;
                        return sales + '件';
                    }
                },
                {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="productedit" style="margin-left: 10px;" productName="'
                            + row.productName + '" productId="'
                            + row.productId + '" rowId="' + row.id
                            + '" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="productdel" style="margin-left: 10px;" rowId="'
                            + row.id
                            + '" ><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                manager.initProductEvent();
            }
        });


        var $selectProductDataTables = utils.createDataTable('#xquark_select_products_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get(productListUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    // order: function () {
                    //     if($order !== ''){
                    //         return $order;
                    //     }else{
                    //         var _index = data.order[0].column;
                    //         if ( _index < 4){
                    //             return '';
                    //         } else {
                    //             return $orders[_index - 4];
                    //         }
                    //     }
                    // },
                    // direction: data.order ? data.order[0].dir :'asc',
                    category: $category,
                    isGroupon: ''
                }, function (res) {
                    if (!res.data) {
                        utils.tools.alert('产品列表加载失败',
                            {timer: 1200, type: 'warning'});
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                    } else {
                        if (res.data.shopId) {
                            shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.categoryTotal,
                        iTotalDisplayRecords: res.data.categoryTotal
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a href="' + row.productUrl
                            + '"><img class="goods-image" src="'
                            + row.imgUrl + '" /></a>';
                    }
                },
                {
                    title: "商品",
                    data: "name",
                    orderable: false,
                    name: "name"
                }, {
                    title: "状态",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch (row.status) {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    title: "价格",
                    data: "price",
                    orderable: true,
                    name: "price"
                }, {
                    title: "数量",
                    data: "amount",
                    orderable: true,
                    name: "amount"
                },
                {
                    title: "库存",
                    data: "sales",
                    orderable: true,
                    name: "sales"
                }, {
                    title: "发布时间",
                    orderable: false,
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "onsaleAt"
                }, {
                    title: "管理",
                    sClass: "right",
                    orderable: false,
                    render: function (data, type, row) {
                        console.log('row: \n');
                        console.log(row);
                        var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                            + row.id + '" productName="' + row.name
                            + '"  productPrice="' + row.price
                            + '"><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                manager.initSelectProductEvent();
            }

        });

        $(".btn-search-products").on('click', function () {

            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null) {
                productListUrl = window.host + '/product/searchbyPc/'
                    + shopId + '/' + keyword;
                $selectProductDataTables.search(keyword).draw();
            } else if (keyword == '' || keyword.length == 0) {
                productListUrl = window.host + "/product/list";
                $selectProductDataTables.search('').draw();
            }
        });

    });


require(['all']);

//module
require(['jquery','uniform','placeholder','bargain/list'],function(){


});

define("bargain", function(){});

