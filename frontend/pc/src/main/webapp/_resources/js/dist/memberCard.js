/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('memberCard/list',['jquery', 'utils', 'datatables', 'blockui', 'select2'], function ($, utils, datatabels, blockui, select2) {

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    const listCards = '/sellerpc/member/card/list';
    const listProducts = window.host + "/product/list";
    const listDiscountProducts = "/sellerpc/member/card/productList";
    var shopId = null;
    var cardId = '';
    var category = '';
    var order = '';

    const upgradeTypeMapping = {
        AUTO: '自动升级',
        MANUAL: '手动升级',
        BUY: '购买'
    };

    const $selectMemberCards = $('#selectMemberCards');

    const columns = ['name', 'level', 'upgrade_type', 'free_delivery'];

    const block = {
        fullBlock: function () {
            return $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });
        },
        unBlock: function () {
            return $.unblockUI();
        }
    };

    $('#addMemberCard').on('click', function () {
        window.location = '/sellerpc/member/editCard';
    });

    // 新增商品佣金
    $('#addDiscountProduct').on('click', function () {
        $("#modal_member_products").modal("show");
    });

    $('.btn-save-card').on('click', function () {
        $('#modal_product_level_setting').modal('hide');
        $('#modal_member_products').modal('hide');
    });


    const $cardTables = $('#xquark_cards_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listCards, {
                size: data.length,
                page: (data.start / data.length) + 1,
                order: columns[data.order[0].column],
                direction: data.order[0].dir,
                cardId: cardId
                //memberLevel: memberLevel.val()
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载出错');
                    return;
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            });
        },
        rowId: "id",
        columns: [
            {
                data: "name",
                width: "100px",
                orderable: true,
                name: "name"
            },
            {
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var level = row.level;
                    return level ? level : '默认';
                },
                name: 'level'
            },
            {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    var upgradeType = row.upgradeType;
                    var html = '';
                    if (upgradeType in upgradeTypeMapping) {
                        html += upgradeTypeMapping[upgradeType];
                    } else {
                        html += '无';
                    }
                    html += '</br>';
                    // html += '<a>规则详情</a>';
                    return html;
                },
                name: "upgradeType"
            },
            {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    // 折扣、是否包邮
                    var discount = row.discount;
                    var isDelivery = row.freeDelivery;
                    var html = '';
                    html += discount + '折';
                    html += '</br>';
                    html += isDelivery ? '包邮' : '不包邮';
                    return html;
                },
                name: "basicRights"
            },
            {
                width: "150px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="view_member" rowId="' + row.id + '" >查看会员</a>';
                    html += '<a href="javascript:void(0);" class="edit_member" style="margin-left: 10px;" rowId="' + row.id + '" >编辑</a>';
                    html += '<a href="javascript:void(0);" class="delete_member" style="margin-left: 10px;" rowId="' + row.id + '" >删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        order: [[3, 'asc']],
        drawCallback: function () {  //数据加载完成
            initTableEvent();
        }
    });

    function initTableEvent() {

        $('.view_member').on('click', function () {
            var id = $(this).attr('rowId');
            window.location = 'list?memberLevel=' + id;
        });

        $('.edit_member').on('click', function () {
            var id = $(this).attr('rowId');
            window.location = 'editCard?id=' + id;
        });

        $('.delete_member').on('click', function () {

        });

    }

    var $discountDatatables = $('#xquark_discount_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listDiscountProducts, {
                size: data.length,
                page: (data.start / data.length) + 1,
                pageable: true,
                direction: data.order ? data.order[0].dir : 'asc'
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<img class="goods-image" src="' + row.img + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            },
            {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                orderable: true,
                width: "50px",
                name: "cards",
                render: function (data, type, row) {
                    var cards = row.memberCards;
                    var html = '';
                    if (cards && cards.length > 0) {
                        cards.forEach(function (card) {
                            html += '<span>' + card.name + ':  <font>' + card.discount + '折 </font></span></br>'
                        })
                    } else {
                        html += '无';
                    }
                    return html;
                }
            },
            {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="edit_discount_product" style="margin-left: 10px;" rowId="' + row.id + '"  productImg="' + row.img + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="delete_discount_product" style="margin-left: 10px;" rowId="' + row.id + '" ><i class="icon-pencil7" ></i>删除</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            $('.delete_discount_product').on('click', function () {
                var url = "/sellerpc/member/card/delete";
                var productId = $(this).attr('rowId');
                utils.postAjax(url, {productId: productId}, function (res) {
                    if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                            utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                            $discountDatatables.search('').draw();
                        } else {
                            if (res.moreInfo) {
                                utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                            } else {
                                utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                            }
                        }
                    }
                });
            });
            $('.edit_discount_product').on('click', bindProductModalShow);
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listProducts, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if (order != '') {
                        return order;
                    } else {
                        var _index = data.order[0].column;
                        if (_index < 4) {
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
            }, function (res) {
                if (res.data && !res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.categoryTotal,
                    iTotalDisplayRecords: res.data.categoryTotal
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="' + row.id + '" productImg="' + row.imgUrl + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectProductEvent();
        }
    });

    function initSelectProductEvent() {
        $(".selectproduct").on("click", bindProductModalShow);
    }

    function bindProductModalShow() {
        var productId = $(this).attr("rowId");
        var productName = $(this).attr("productName");
        var productImg = $(this).attr("productImg");
        var productPrice = $(this).attr("productPrice");
        $("#card_productName").html(productName);
        $("#card_productPrice").html(productPrice);
        $('#productId').val(productId);
        $("#card_productImg").attr("src", productImg);

        // 初始化会员卡数据
        utils.postAjaxWithBlock($(document), '/sellerpc/member/card/listVO', {productId: productId}, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        var memberCards = res.data.list;
                        $selectMemberCards.empty();
                        if (memberCards && memberCards.length > 0) {
                            memberCards.forEach(function (card, index) {
                                var html = $('<label class="checkbox-inline">' +
                                    '<input type="checkbox" class="memberCard" name="memberCardsType" value="' + card.id + '" />' +
                                    '' + card.name + ' </label>');
                                if (card.checked) {
                                    html.find('input').attr('checked', 'checked');
                                }
                                $selectMemberCards.append(html);
                                if ((index + 1) % 4 === 0) $selectMemberCards.append('</br>')
                            });
                            $('.memberCard').change(function () {
                                var cardId = $(this).val();
                                var productId = $('#productId').val();
                                var url = this.checked ?
                                    '/sellerpc/member/card/bindProduct' : '/sellerpc/member/card/unbindProduct';
                                utils.postAjax(url, {productId: productId, cardId: cardId}, function (res) {
                                    if (typeof(res) === 'object') {
                                        console.log(res);
                                        $discountDatatables.search('').draw();
                                    }
                                });
                            })
                        }
                        break;
                    }
                    default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res === 0) {

            } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
        // $("#commission_Id").val('');
        // $("#commission_firstLevelRate").val('');
        // $("#commission_secondLevelRate").val('');
        // $("#commission_thirdLevelRate").val('');
        $("#modal_product_level_setting").modal("show");
    }
});


/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('memberCard/add',['jquery', 'utils', 'blockui', 'spectrum', 'form/validate', 'datatables', 'jquerySerializeObject', 'fileinput_zh', 'fileinput'], function ($, utils, blockui, spectrum, validate) {

    // 会员卡表单
    const $form = $('.member-card-form');
    // 表单提交
    const $btnSubmit = $('.btn-submit');
    // color-selector
    const $cardColor = $("#card-color");
    // 图片上传
    const $fileUpload = $('#background-file');

    // 背景图片
    const $background = $('#background');

    const saveUrl = '/sellerpc/member/card/save';

    const fileUploadManager = {
            uploadOption: {
                uploadUrl: '/sellerpc/member/card/upload',
                showCaption: false,
                showUpload: false,
                uploadAsync: true,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"]
            },
            init: function () {
                var defaultImg = $background.val();
                if (defaultImg && defaultImg.toString().startsWith('http')) {
                    // 如果已经有图片则显示默认
                    this.uploadOption = $.extend({
                        showPreview: true,
                        initialPreview: [ // 预览图片的设置
                            "<img src= '" + defaultImg + "' class='file-preview-image'>"]
                    }, this.uploadOption);
                }
                // 初始化文件上传控件
                $fileUpload.fileinput(this.uploadOption);
                // 上传之前
                $fileUpload.on('filepreajax', function () {
                    $btnSubmit.addClass('disabled');
                });

                // 选中后立即上传
                $fileUpload.on('filebatchselected', function () {
                    $fileUpload.fileinput('upload');
                });

                // 上传成功
                $fileUpload.on('fileuploaded', function (event, data) {
                    var res = data.response;
                    if (res.errorCode && res.errorCode === 200) {
                        $background.val(res.img);
                        $btnSubmit.removeClass('disabled');
                    } else {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                });
                return this;
            },
            destroy: function () {
                $fileUpload.fileinput('destroy');
                return this;
            },
            refresh: function () {
                this.destroy().init();
            }
        };

    // validate 表单配置
    const formProfile = {
        rules: {
            name: 'required',
            level: {
                required: true,
                pattern: /^[0-9]*$/
            },
            discount: {
                required: true,
                pattern: /^[0-9]?(.[0-9]{1,2})?$/
            },
            remark: 'required'
        },
        messages: {
            name: {
                required: '请输入会员卡名称'
            },
            level: {
                required: '请输入会员卡等级',
                pattern: '请输入数字'
            },
            discount: {
                required: '请输入折扣',
                pattern: '请输入正确的折扣格式'
            },
            remark: {
                required: '请输入使用须知'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            globalFunctions.submitForm(form);
        }
    };

    const globalFunctions = {
        submitForm: function submitForm(form) {
            if ($btnSubmit.hasClass("disabled")) {
                utils.tools.alert("请等待文件上传完成", {timer: 1200, type: 'warning'});
                return;
            }
            var data = $(form).serializeObject();
            // 设置默认颜色
            if (!data.bgColor) data.bgColor = '#008000';
            if (!data.freeDelivery) data.freeDelivery = false;
            utils.getJson(saveUrl, data, function (res) {
                if (res.errorCode === 200) {
                    if (res.data) {
                        utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                    } else {
                        utils.tools.alert("保存失败", {timer: 1200, type: 'warning'});
                    }
                } else {
                    utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                }
            });
        }
    };

    // 初始化颜色选择框
    $cardColor.spectrum({
        showPaletteOnly: true,
        showPalette: true,
        hideAfterPaletteSelect: true,
        color: 'green',
        palette: [
            ['black', 'white', 'blanchedalmond',
                'rgb(255, 128, 0);', 'hsv 100 70 50'],
            ['red', 'yellow', 'green', 'blue', 'violet']
        ],
        change: function (color) {
            $cardColor.val(color.toHexString());
            console.log(color.toHexString());
        }
    });

    // 文件上传控件初始化
    fileUploadManager.init();
    // 参数校验初始化
    validate($form, formProfile);
});

require(['all']);

require(['memberCard/list', 'memberCard/add']);

define("memberCard", function(){});

