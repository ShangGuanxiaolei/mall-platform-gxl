/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
/**
 * Created by quguangming on 16/5/18.
 */
define('login/signin',['form/validate','utils','cookies','md5'], function(validate,utils,cookies,md5) {

    var $form = $('.signin-form');

    var $login = $('.login');

    var url = window.host + '/pc/signin_check';

    var url_setting = 'http://' + window.location.host  + "/v2" +'/wechat/isDefaultExist';

    var isProcessing = false;

    $('#username').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $('#password').focus();
        }
    });

    $('#password').bind('keypress', function (event) {
        if (event.keyCode == "13") {
            $form.submit();
        }
    });

    $login.on('click',function(){
        $form.submit();
    });

    var loginForm = {
        rules:{
            username: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{1,16}$/
            },
            password: {
                required: true,
                pattern: /^.{6,50}$/
            }
        },
        messages:{
            username: {
                required: "账号不能为空.",
                pattern: "请输入有效的1~16位字母、数字和下划线的组合账号"
            },
            password: {
                required: "密码不能为空.",
                pattern: "请输入至少6位的密码."
            }
        },
        submitCallBack: function(form){

            login();
        }
        ,//invalidCallBack:function(form,validator){
        //
        //},
        //setSuccessText: function(label){
        //    $(label).text("验证成功");
        //},
        //successClass: "validation-success-label",
        //errorClass:""
    };

    //cookie
    $('.username').val($.cookie('username'));

    function login(){
        var txtUserName = $('#username');
        var txtPassword = $('#password');

        var username, password;

        username = txtUserName.val();
        password = txtPassword.val();

        txtPassword.on('keyup', function(ev){
            var evt = ev || event;
            if (evt.keyCode === 13) {
                $login.trigger('click');
                return false;
            }
        });

        if (isProcessing) {
            return false;
        } else {
            isProcessing = true;
        }

        var data = {
            u: username,
            p: CryptoJS.MD5(password).toString()
        };

        utils.postAjax(url, data, function(result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    { // 密码正确
                        saveCookies(username);
                        utils.postAjax(url_setting, data, function(result) {
                            if (typeof(result) === 'object') {
                                switch (result.data) {
                                    case true:
                                    {
                                      location.href = '/sellerpc/mall/route';
                                        break;
                                    }
                                    default:
                                    {
                                        location.href = '/sellerpc/wechat/config';
                                        break;
                                    }
                                }
                            } else{
                                if (result == -1){
                                  location.href = '/sellerpc/mall/route';
                                }
                            }

                        });

                        break;
                    }
                    default:
                    {
                        alert('登录失败，手机号码或密码错误!');
                        break;
                    }
                }
            } else{
                if (result == -1){
                    alert('登录失败，手机号码或密码错误!');
                }
            }
            isProcessing = false;
        });
        return false;
    }

    validate($form,loginForm);

    function saveCookies(username){
        $.cookie('username', username, {
            expires: 7
        });
    }

});
/**
 * Created by quguangming on 16/5/18.
 */
define('login/forgot',['form/validate','utils'], function(validate,utils) {

    var $form = $('.forgot-form');

    var $divForgot = $('.forgot-part');

    var $send = $('.send');

    var $phone = $('#phone');

    var $password = $('#newPassword');

    var $rePassword = $('#rePassword');

    var $captcha =  $('#captcha');

    var isProcessing = false;


    $('.modify-pwd').on('click',function(){
        $form.submit();
    });

    $send.on('click',function(){
        if(!$send.hasClass('disabled')){
            sendCaptcha();
        }
    });

    function checkPhone(num) {
        var regPhone = /^1\d{10}$/;

        return regPhone.test(num);
    }


    if (($phone.val().length > 0) && checkPhone($phone.val())) {
        $send.removeClass('disabled').text('发送验证码');
    } else {
        $send.addClass('disabled');
    }

    $phone.on('keyup', function() {
        if (($phone.val().length > 0) && checkPhone($phone.val())) {
            $send.removeClass('disabled').text('发送验证码');
        } else {
            $send.addClass('disabled');
        }

        return false;
    });

    $password.add($rePassword).on('keydown', function(ev){
        var evt = ev || event;
        if (evt.keyCode === 32) {
            return false;
        }
    }).on('paste',function(){
        return false;
    });


    var forgotForm = {
        rules:{
            phone: {
                required: true,
                pattern: /^1\d{10}$/
                //depends:function(element) {
                //    var val = $(element).val();
                //    var reg = /^1\d{10}$/;
                //    if(reg.test(val)){
                //        if($send.hasClass('disabled')) {
                //            $send.removeClass('disabled');
                //        }
                //    } else {
                //        $send.addClass('disabled')
                //    }
                //}
            },
            captcha:{
                required: true,
                pattern: /^\d{6}$/
            },
            newPassword:{
                required: true,
                pattern: /^.{6,50}$/
            },
            repeatPassword: {
                required: true,
                equalTo: "#password"
            }
        },
        messages:{
            phone: {
                required: "手机号码不能为空.",
                pattern: "请输入正确的11位手机号码."
            },
            captcha:{
                required: "验证码不能为空。",
                pattern: "验证码格式不对。"
            },
            newPassword:{
                required: "新密码不能为空.",
                pattern:  "请输入至少6位的新密码."
            },
            repeatPassword: {
                required: "重复密码不能为空.",
                equalTo: "两次输入的密码不同."
            }
        },
        submitCallBack: function(form){
            modifyPwd();
        }
    };

    function sendCaptcha(){

        var waitTime = 60;

        function countdownTimer() {
            if (waitTime === 0) {

                if (($phone.val().length > 0) && checkPhone($phone.val())) {
                    $send.removeClass('disabled').text('发送验证码');
                } else {
                    $send.addClass('disabled').text('发送验证码');
                }
                waitTime = 60;
            } else {
                $send.text('剩余' + waitTime + 's...');
                waitTime--;
                setTimeout(function() {
                    countdownTimer();
                }, 1000);
            }
        }

        countdownTimer();

        var url = window.host + '/forget-pwd';
        var data = {
            mobile: $phone.val()
        };

        if (!$divForgot.is(':visible')) {
            return false;
        }

        utils.postAjax(url, data, function(result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    {
                        alert('验证码发送成功，请查收短信。');
                        break;
                    }
                    default:
                    {
                        alert('验证码发送失败！');
                        //that.alertMethod(result.moreInfo);
                    }
                }
            }
        });

    }

    function modifyPwd() {

        var captcha = $('#captcha');

        var username, code, password;

        username = $phone.val();
        code = $captcha.val();
        password = $password.val();


        if (isProcessing) {
            return false;
        } else {
            isProcessing = true;
        }

        var url = window.host + '/validate-forget-pwd';
        var data = {
            mobile: username,
            smsCode: code,
            pwd: CryptoJS.MD5(password).toString()
        };

        that.postMethod(url, data, function (result) {
            if (typeof(result) === 'object') {
                switch (result.errorCode) {
                    case 200:
                    {
                        alert('修改密码成功！');
                        $divForgot.hide();
                        $('.login-part').fadeIn();
                        break;
                    }
                    case -1:
                    {
                        alert(" 修改失败" + result + ":" + moreInfo);
                        break;
                    }
                    default:
                    {
                        alert(" 修改失败" + result + ":" + moreInfo);
                    }
                }
            }
            isProcessing = false;
        });

        return false;
    }

    validate($form,forgotForm);

});
//base requirements
require(['all']);

//module
require(['jquery','uniform','placeholder','login/signin','login/forgot'],function(signin,forgot){

        $('input').placeholder();
        $('#password').val('');

        $('.forgot-pwd').on('click', function() {
            $('.login-part').hide();
            $('.forgot-part').fadeIn();
            $('#captcha').val('');
            $('#newPassword').val('');
            $('#rePassword').val('');
        });


        $('.forgot-part .tip-doing').on('click', function() {
            $('.login-part').fadeIn();
            $('.forgot-part').hide();
            $('#password').val('');
        });

});


define("login", function(){});

