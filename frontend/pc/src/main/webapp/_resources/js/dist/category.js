/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('category/list',['jquery','utils','datatables','blockui','select2','tree', 'fileinput_zh', 'fileinput'], function($,utils,datatabels,blockui,select2,tree,fileinput_zh,fileinput) {
    var _data;
    var _id;
    var _icon;
    var batch;
    var taxonomy = utils.tools.request('type') || 'GOODS';
    $('#jstree').jstree({
        "core" : {
            "animation" : 0,
            "check_callback" : true,
            "themes" : { "stripes" : true },
            'data' : {
                'url' : window.host + "/category/list?taxonomy=" + taxonomy,
                'data' : function (node) {
                    return { 'id' : node.id };
                }
            }
        },
        "types" : {
            "#" : {
                "max_children" : 1,
                "max_depth" : 9,
                "valid_children" : ["root"]
            },
            "root" : {
                "icon" : "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children" : ["default"]
            },
            "default" : {
                "valid_children" : ["default","file"]
            },
            "file" : {
                "icon" : "glyphicon glyphicon-file",
                "valid_children" : []
            }
        },
        "plugins" : [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "ccp": null,
                "create" : {
                    "separator_before"	: false,
                    "separator_after"	: true,
                    "_disabled"			: false, //(this.check("create_node", data.reference, {}, "last")),
                    "label"				: "新增分类",
                    "action"			: function (data) {
                        var inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.create_node(obj, {}, "last", function (new_node) {
                            setTimeout(function () { inst.edit(new_node); },0);
                        });
                    }
                },
                "rename" : {
                    "separator_before"	: false,
                    "separator_after"	: false,
                    "_disabled"			: false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
                    "label"				: "修改分类",
                    /*!
                     "shortcut"			: 113,
                     "shortcut_label"	: 'F2',
                     "icon"				: "glyphicon glyphicon-leaf",
                     */
                    "action"			: function (data) {
                        var inst = $.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        inst.edit(obj);
                    }
                },
                "remove" : {
                    "separator_before"	: false,
                    "icon"				: false,
                    "separator_after"	: false,
                    "_disabled"			: false,
                    "label"				: "删除分类",
                    "action"			: function (data) {

                        utils.tools.confirm("删除分类，将同时删除该分类所有下级分类，确定删除吗?", function(){
                            var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                            if(inst.is_selected(obj)) {
                                inst.delete_node(inst.get_selected());
                            }
                            else {
                                inst.delete_node(obj);
                            }
                        },function(){

                        });

                    }
                },
                "更新图片": {
                    "label": "更新图片",
                    "action": function (data) {
                        var inst = jQuery.jstree.reference(data.reference),
                            obj = inst.get_node(data.reference);
                        _data = data;
                        _id = obj.id;
                        _icon = obj.icon;

                        $("#file-input").fileinput('destroy');
                        //初始化方法
                        initFileInput(_id, _icon);

                        $("#modal_upload_img").modal("show");
                    }
                },
                // "更新类别小图片": {
                //     "label": "更新类别小图片",
                //     "action": function (data) {
                //         var inst = jQuery.jstree.reference(data.reference),
                //             obj = inst.get_node(data.reference);
                //         _data = data;
                //         _id = obj.id;
                //         _icon = obj.tinyImg;
                //
                //         $("#file-input-tiny").fileinput('destroy');
                //         //初始化方法
                //         initFileInputTiny(_id, _icon);
                //
                //         $("#modal_upload_tiny_img").modal("show");
                //     }
                // },
                // "更新字体颜色": {
                //     "label": "更新字体颜色",
                //     "action": function (data) {
                //         var inst = jQuery.jstree.reference(data.reference),
                //             obj = inst.get_node(data.reference);
                //         _data = data;
                //         _id = obj.id;
                //
                //         $("#update_id").val(_id);
                //         $("#color").val(obj.fontColor);
                //
                //         $("#modal_color").modal("show");
                //     }
                // }
            }
        }
    }).bind("loaded.jstree", function () {
        jQuery("#jstree").jstree("open_all");
    }).bind("create_node.jstree",function(event,data){
        createCategory(event,data);
    }).bind("rename_node.jstree",function(event,data){
        rename(event,data);
    }).bind("delete_node.jstree",function(event,data){
        reMove(event,data);
    }).bind("move_node.jstree",function(event,data){
        movetree(event,data);
    });

    // 分类节点新增
    function createCategory(event,data){
        var parentId = data.parent;
        var newNodeName = data.node.text;
        var params = {"parentid":parentId,"name":newNodeName};
        $.ajax({
            'url': window.host + "/category/save?taxonomy=" + taxonomy,
            'type':"post",
            'dataType':'json',
            'cache':false,
            'data':params,
            'timeout':1000*10
        }).done(function(json){
            var id = data.node.id;
            var categoryId = json.data.id;
            data.instance.set_id(data.node, categoryId);
            //data.instance.set_icon(data.node, "http://7xl05n.com2.z0.glb.qiniucdn.com/FohXBIn3VCI7K2NNXgrzdZyE7HT7?imageView2/2/w/640/q/100");
        }).fail(function(e){
            Metronic.unblockUI();
            alert("亲出错了，请稍后再试");
        })
    }

    // 分类节点重命名
    function rename(event,data) {
        var str = data.node.id;
        if(str == '0') {
            alert("不能修改根节点");
            data.instance.set_text(data.node, '标签分类');
            return;
        }
        var id = str;
        var name = data.text;
        var params = {"id":id,"name":name};
        $.ajax({
            'url': window.host + '/category/updateName',
            'type':'post',
            'dataType':'json',
            'data':params,
            'cache':false,
            'timeout':1000*10,
            'success': function(data1) {
                if(data1.data == null){
                    alert('分类名称已经存在，不允许重复');
                    data.instance.set_text(data.node, data.old);
                }
            },
            'error': function(e) {
                alert(e);
            }
        }).fail(function(){
            alert("亲出错了，请稍后再试~");
        })
    }

    // 分类节点删除
    function reMove(event,data) {
        var str = data.node.id;
        if(str == '0') {
            alert("亲，不能删除根节点");
            return ;
        }
        var id = str;
        var params = {"id":id};
        $.ajax({
            'url': window.host + "/category/delete",
            'type':"post",
            'dataType':"json",
            'data':params,
            'cache':false,
          'timeout': 1000 * 10,
          'async': true,
          'success': function (json) {
            if (json.moreInfo != null && json.moreInfo.length > 0) {
              setTimeout(function () {
                utils.tools.alert(json.moreInfo, {timer: 2000});
                $.jstree.reference($('#jstree')).refresh();
              }, 500);
            } else if (json.ret == false) {
              alert(json.errmsg);
            }
          }
        });
    }

    // 分类节点移动
    function movetree(event,data) {
        var id = data.node.id;
        var parentid  = data.parent;
        var position = data.position;
        var sourceposition = data.old_position;
        var sourceparentid = data.old_parent;
        var params = {"id":id,"parentid":parentid,"position":position,"sourceposition":sourceposition,"sourceparentid":sourceparentid};
        $.ajax({
            'url': window.host + "/category/move",
            'type':"post",
            'dataType':"json",
            'data':params,
            'cache':false,
            'timeout':1000*10
        }).done(function(json){
            if(json.ret==false) {
                alert(json.errmsg);
            }
        }).fail(function(json){
            Metronic.unblockUI();
            alert("亲出错了，请稍后再试~");
        })

    }

    // 初始化上传图片控件
    function initFileInput(id, img){
        var op = {
            language: 'zh',
            uploadUrl: window.host + "/category/updateImg",
            uploadExtraData: {"id": id},
            previewFileType: 'text',
            browseLabel: '选择文件',
            removeLabel: '删除',
            uploadLabel: '上传',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            browseClass: 'btn btn-primary',
            uploadClass: 'btn btn-default',
            removeClass: 'btn btn-danger',
            initialCaption: '',
            maxFilesNum: 1,
            enctype: 'multipart/form-data',
            allowedFileExtensions: ["jpg","png","gif"],
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                footer: '',
            },

        };
        //如果img有值，则显示之前上传的图片
        if(img && img.toString().indexOf('http') != -1){
            op = $.extend({
                showPreview : true,
                initialPreview : [ // 预览图片的设置
                    "<img src= '" + img +  "' class='file-preview-image'>", ]
            }, op);
        }

        $('.file-input').fileinput(op);
        $('#file-input').fileinput('enable');

    }

    // 图片上传成功后更新节点图标
    $('#file-input').on('fileuploaded', function(event, data, msg) {
        $.jstree.reference(_data.reference).set_icon(_data.node, data.response.img);
        $.jstree.reference(_data.reference).refresh();
    });


    $('#modal_upload_img').on('hidden.bs.modal', function () {
        $('#file-input').fileinput('enable');
    });


    // 初始化上传图片控件
    function initFileInputTiny(id, img){
        var op = {
            language: 'zh',
            uploadUrl: window.host + "/category/updateTinyImg",
            uploadExtraData: {"id": id},
            previewFileType: 'text',
            browseLabel: '选择文件',
            removeLabel: '删除',
            uploadLabel: '上传',
            browseIcon: '<i class="icon-file-plus"></i>',
            uploadIcon: '<i class="icon-file-upload2"></i>',
            removeIcon: '<i class="icon-cross3"></i>',
            browseClass: 'btn btn-primary',
            uploadClass: 'btn btn-default',
            removeClass: 'btn btn-danger',
            initialCaption: '',
            maxFilesNum: 1,
            enctype: 'multipart/form-data',
            allowedFileExtensions: ["jpg","png","gif"],
            layoutTemplates: {
                icon: '<i class="icon-file-check"></i>',
                footer: '',
            },

        };
        //如果img有值，则显示之前上传的图片
        if(img && img.toString().indexOf('http') != -1){
            op = $.extend({
                showPreview : true,
                initialPreview : [ // 预览图片的设置
                    "<img src= '" + img +  "' class='file-preview-image'>", ]
            }, op);
        }

        $('#file-input-tiny').fileinput(op);
        $('#file-input-tiny').fileinput('enable');

    }

    // 保存类别颜色
    $(document).on('click', '.saveBtn', function () {
        var id = $("#update_id").val();
        var color = $("#color").val();
        if(color == ''){
            utils.tools.alert("请输入颜色代码!", {timer: 2000, type: 'warning'});
            return;
        }

        var param = {
            id: id,
            color: color
        };
        utils.postAjaxWithBlock($(document), window.host + '/category/updateColor', param, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        utils.tools.alert("保存成功!", {timer: 2000, type: 'warning'});
                        $("#modal_color").modal("hide");
                        break;
                    }
                    default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {
            } else if (res == -1) {
                utils.tools.alert("保存失败", {timer: 1200});
            }
        });

    });


});
//微信账号绑定
require(['all']);

require(['category/list']);
define("category", function(){});

