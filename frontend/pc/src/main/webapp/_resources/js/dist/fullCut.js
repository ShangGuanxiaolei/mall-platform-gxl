/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('utils/dateRangePicker',['jquery', 'utils', 'moment', 'daterangepicker'],
    function ($, utils, moment) {

      const DATE_FORMAT = 'YYYY-MM-DD HH:mm';

      const options = {
        timePicker: true,
        dateLimit: {days: 60000},
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月",
            "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment().endOf('day')],
          '一星期': [moment(), moment().add(6, 'days')],
          '一个月': [moment(), moment().add(1, 'months')],
          '一年': [moment(), moment().add(1, 'year')]
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      /**
       * 构造一个datePricker实例
       * @param params.dom 选择器
       * @param params.onApply
       * @param params.onCancel
       */
      class Pricker {
        constructor(params) {
          const $dateRangePicker = $(params.dom);
          this.dom = params.dom;

          $dateRangePicker.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
              $dateRangePicker.val(start.format(DATE_FORMAT) + ' - '
                  + end.format(DATE_FORMAT));
            } else {
              $dateRangePicker.val('');
            }
          });

          const onApply = params.onApply || function (ev, picker) {
            if (picker.endDate.isBefore(moment(new Date()))) {
              utils.tools.error('活动结束时间不能早于当前时间');
              return;
            }
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;

            $('#valid_from').val(picker.startDate.format(DATE_FORMAT));
            $('#valid_to').val(picker.endDate.format(DATE_FORMAT));
          };

          $dateRangePicker.on('apply.daterangepicker', onApply);

          const onCancel = params.onCancel || function () {
            //do something, like clearing an input
            $dateRangePicker.val('');
            $('#valid_from').val('');
            $('#valid_to').val('');
          };

          /**
           * 清空按钮清空选框
           */
          $dateRangePicker.on('cancel.daterangepicker', onCancel);

          this.$dateRangePicker = $dateRangePicker;
        }

        /**
         * 传两个时间戳, 设置时间
         * @param from
         * @param end
         */
        setViewDate(from, end) {
          const $input = $(this.dom);
          from = moment(from).format(DATE_FORMAT);
          end = moment(end).format(DATE_FORMAT);
          $input.val(from + ' - ' + end);
          $('#valid_from').val(from);
          $('#valid_to').val(end);
        }

      }

      return {
        createPicker: function (params) {
          return new Pricker(params);
        }
      }
    });

define('utils/steps',['jquery', 'steps'], function ($) {

  /**
   * 创建jquery_steps实例
   * @param params
   * @param params.dom
   * @param params.headerTag
   * @param params.bodyTag
   * @param params.onFinished
   * @param params.onStepChanged
   * @param params.showFinishButton
   * @param params.nextBtn
   * @param params.previousBtn
   * @param params.form
   * @param params.validate
   */
  function createSteps(params) {

    const _this = this;
    const $next = params.nextBtn;
    const $previous = params.previousBtn;

    if (params.form && params.validate) {
      const $form = params.form;
      // 表单校验
      $form.validate(params.validate);
      this.$form = $form;
    }

    const $step = $(params.dom).steps({
      headerTag: params.headerTag,
      bodyTag: params.bodyTag,
      transitionEffect: "fade",
      titleTemplate: '<span class="number">#index#</span> #title#',
      enableAllSteps: true,
      enablePagination: true,
      showFinishButtonAlways: params.showFinishButton | true,
      labels: {
        cancel: "取消",
        current: "current step:",
        pagination: "Pagination",
        next: "下一步",
        previous: "上一步",
        finish: '保存'
      },
      onStepChanging: function () {
        _this.$form.validate().settings.ignore = ":disabled,:hidden";
        return _this.$form.valid();
      },
      onFinishing: function () {
        _this.$form.validate().settings.ignore = ":disabled";
        return _this.$form.valid();
      },
      onFinished: function (event, currentIndex) {
        if (params && params.onFinished) {
          params.onFinished(event, currentIndex);
        }
      },
      onStepChanged: function (event, currentIndex) {
        if (params && params.onStepChanged) {
          params.onStepChanged(event, currentIndex);
        }
        // $($('fieldset').get(currentIndex)).tab("show");
      },
    });

    if ($next && ($next instanceof $)) {
      $next.on('click', function () {
        $step.steps('next');
      })
    }

    if ($previous && ($previous instanceof $)) {
      $step.steps('pre');
    }

    return $step;

  }

  return {
    create: createSteps
  }

});

/**
 * 封装表格基本参数
 */
define('utils/dataTable',['jquery', 'utils', 'ramda', 'datatables'], function ($, utils, R) {

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无数据"
    }
  });

  const basicOption = {
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ordering: false,
    sortable: false,
    scrollX: true,
    scrollCollapse: true,
    fixedColumns: {
      leftColumns: 0,
      rightColumns: 1
    },
    rowId: 'id',
    select: {
      style: 'multi'
    }
  };

  class DataTable {
    /**
     * 构造函数
     * @param params
     * @param params.lazyInit 懒加载, 若指定该属性则在第一次刷新时才加载
     * @param params.dom
     * @param params.url
     * @param params.option
     * @param params.reqParams //请求参数
     * @param params.drawBack // 完成回掉
     * @param params.columns // 表格列
     * @param params.autoFit // 表格是否子适应
     * @param params.showSelector // 是否需要选择框
     */
    constructor(params) {
      this.$dom = $(params.dom);
      const option = $.extend({}, basicOption, params.option || {});

      // 部分界面需要关闭自适应大小
      if (params.autoFit === false) {
        option.fixedColumns = 0;
        option.scrollX = false;
        option.scrollCollapse = false;
      }

      if (params.reqParams) {
        this.reqParams = params.reqParams;
      }

      // 如果没有url参数则作为展示表格
      let isView = true;
      if (params.url && params.url !== '') {
        isView = false;
        option.ajax = (data, callback) => {
          const reqParams = $.extend({
            size: data.length,
            page: data.start / data.length,
          }, this.reqParams || {});
          $.get(params.url, reqParams, function (res) {
            if (res.errorCode !== 200) {
              utils.tools.alert('数据加载失败');
              return;
            }
            callback({
              recordsTotal: res.data.total,
              recordsFiltered: res.data.total,
              data: res.data.list,
              iTotalRecords: res.data.total,
              iTotalDisplayRecords: res.data.total
            });
          })
        };
      }
      if (isView) {
        option.serverSide = false;
        option.paging = false;
        option.searching = false;
      }

      const showSelector = params.showSelector;
      // 列头加上选择框
      if (showSelector && showSelector === true
          && params.columns) {
        params.columns.unshift({
          title: '<span>全选<label class="checkbox"><input name="checkAll" type="checkbox" class="styled"></label></span>',
          name: 'check',
          width: 15,
          render: function (data, type, row) {
            return '<label class="checkbox"><input name="checkColumn" type="checkbox" class="styled" value="'
                + row.id + '"></label>';
          }
        });

        $(document).on('click', 'input[name=checkAll]', function () {
          const isSelfChecked = $(this).is(':checked');
          $('input[name=checkColumn]').prop('checked', isSelfChecked);
        })
      }

      option.columns = params.columns;
      option.drawCallback = params.drawBack || function () {
      };
      this.isView = isView;
      this.option = option;
      if (!params.lazyInit) {
        this.init();
      }
    }

    init() {
      this.$tableInstence = this.$dom.DataTable(this.option);
      this.inited = true;
    }

    /**
     * 更新参数
     * @param params
     * @returns {DataTable}
     */
    update(params) {
      this.reqParams = params;
      return this;
    }

    /**
     * 刷新当前页
     */
    refresh(reqParams) {
      if (this.isView) {
        throw Error('展示表格无法刷新');
      }
      if (!this.inited) {
        this.init();
        return;
      }
      if (reqParams) {
        this.reqParams = $.extend(this.reqParams, reqParams);
      }
      return new Promise((resolve => {
        resolve(this.$tableInstence.search('').draw('page'));
      }));
    }

    /**
     * 刷新全部
     */
    refreshAll(reqParams) {
      if (this.isView) {
        throw Error('展示表格无法刷新');
      }
      if (!this.inited) {
        this.init();
        return;
      }
      if (reqParams) {
        this.reqParams = $.extend(this.reqParams, reqParams);
      }
      this.$tableInstence.search('').draw();
      return this;
    }

    /**
     * 列表获取表格全部数据
     * @param mapFunc 数据映射函数
     */
    getDataAll(mapFunc = x => x) {
      const data = this.$tableInstence.rows().data();
      return Array.prototype.map.call(data, mapFunc);
    }

    /**
     * 为当前表格添加一列
     */
    addRow(row, callback) {
      this.$tableInstence.row.add(row).draw();
      if (callback) {
        callback();
      }
      return this;
    }

    /**
     * 根据函数修改行数据
     * @param match 查找函数
     * @param modify 修改函数
     */
    modifyRow(match, modify) {
      this.$tableInstence.rows().every(function () {
        const d = this.data();
        if (match && match(d) === true) {
          if (modify) {
            const newRow = modify(d);
            this.data(newRow);
          }
        }
      });
      return this;
    }

    /**
     * 根据行节点来匹配修改
     * @param match
     * @param modify
     * @returns {DataTable}
     */
    modifyRowByNode(match, modify) {
      this.$tableInstence.rows().every(function () {
        const node = this.node();
        const data = this.data();
        if (match && match(node) === true) {
          if (modify) {
            const newRow = modify(data);
            this.data(newRow);
          }
        }
      });
      return this;
    }

    /**
     * 删除某一行
     * @param selector
     */
    removeRow(selector) {
      this.row(selector)
      .remove()
      .draw();
    }

    /**
     * 删除所有选中行
     */
    removeRows(filter = () => true) {
      const _this = this;
      $('input[name=checkColumn]').each(function (i, item) {
        if(filter(item)) {
          _this.removeRow($(item).parents('tr'));
        }
      });
    }

    /**
     * 选中的行数
     * @returns {jQuery}
     */
    checkedRowNum() {
      return $('input[name=checkColumn]:checked').length;
    }

    /**
     * 获取表格行对象
     * @param selector 行选择器
     * 参考 https://datatables.net/reference/type/row-selector;
     * https://datatables.net/examples/ajax/null_data_source.html
     * @returns {*}
     */
    row(selector) {
      return this.$tableInstence.row(selector);
    }

    /**
     * 获取表格行数据
     * @param selector
     * @returns {*}
     */
    rowData(selector) {
      return this.row(selector).data();
    }

    /**
     * 返回表格行数
     */
    rowCount() {
      if (!this.inited) {
        return 0;
      }
      return this.$tableInstence.data().count();
    }

  }

  return {
    /**
     * 构造一个新的表格实例
     * @param params
     * @returns {DataTable}
     */
    create: function (params) {
      return new DataTable(params);
    }
  }

});

/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('fullCut/editNew',['jquery', 'utils',
      'utils/dateRangePicker', 'utils/steps',
      'utils/dataTable',
      'jquerySerializeObject', 'form/validate'],
    function ($, utils, datePricker, steps, dataTable, validate) {

      const $productModal = $('#choose-product-modal');
      const $setDiscountModal = $('#setDiscountModal');
      const $setBatchDiscountModal = $('#setBatchDiscountModal');
      const $form = $('.full-cut-form');

      const productListUrl = window.host + '/promotion/listProductSelective';
      const savePromotionUrl = window.host + '/promotionFullCut/savePromotion';
      const viewPromotionUrl = id => window.host
          + `/promotionFullCut/view/${id}`;
      const hasActivatePromotionUrl = type => window.host
          + '/promotion/hasActivate?type=' + type;

      let isSelectedProduct = false;
      let settedGlobalDiscount = false;

      // 表格中被禁用了的按钮集合
      const disabledSelector = new Set();

      const id = utils.tools.request('id');
      if (id && id !== '') {
        // 编辑恢复数据
        resumeData(id);
      }

      /**
       * 初始化商品分类
       */
      refreshCategory();

      const validateForm = {
        rules: {
          title: {
            required: true
          },
          validFrom: {
            required: true
          },
          validTo: {
            required: true
          }
        },
        messages: {
          title: {
            required: '请输入活动标题',
          },
          validFrom: {
            required: '请选择活动日期'
          },
          validTo: {
            required: '请选择活动日期'
          }
        }
      };

      /**
       * 初始化流程控制
       */
      steps.create({
        dom: '.steps',
        headerTag: 'h6',
        bodyTag: 'fieldset',
        form: $form,
        validate: validateForm,
        // 表单提交
        onFinished: () => {
          const data = $form.serializeObject();

          const userScope = data.userScope;
          data.type = (userScope === 'ALL') ? 'FULLCUT' : 'INSIDE_BUY';

          // 是否包邮
          data.isFreeDelivery = $('input[name=isFreeDelivery]').is(':checked');
          // 没有选择全局折扣则获取自定义折扣
          if (!data.discount || data.discount === '' || data.discount === '0') {
            const productDetails = serializePromotionTable();
            if (!productDetails) {
              return;
            }
            data.productDetails = productDetails;
          }
          if ((!data.discount || data.discount === '')
              && data.productDetails.length
              === 0) {
            utils.tools.error('请选择商品折扣');
            return;
          }

          utils.post(savePromotionUrl, data => {
            if (data && data === true) {
              utils.tools.success('保存成功');
              location.href = 'fullCutList';
              return;
            }
            utils.tools.error('保存失败');
          }, {params: data});
          console.log(data);
        }
      });

      /**
       * 初始化时间控件
       */
      const picker = datePricker.createPicker({
        dom: '.daterange-time'
      });

      // 需要在jQuery上下文环境调用
      function tableSelectbind() {
        const $this = $(this);
        if ($this.hasClass('disabled')) {
          return;
        }
        const rowData = productSearchTable.rowData($this.parents('tr'));
        promotionViewTable.addRow(buildViewRow(rowData));
        if (!isSelectedProduct) {
          showTableContainer();
          isSelectedProduct = true;
        }
        disableRowSelect(rowData.id);
      }

      /**
       * 禁用指定id的表格选择按钮
       * @param id rowId
       */
      function disableRowSelect(id) {
        $(`.add_promotion_product[rowId=${id}]`).addClass('disabled')
        .off('click');
        disabledSelector.add(id);
      }

      /**
       * 启用指定id的表格选择按钮
       * @param id rowId
       */
      function enableRowSelect(id) {
        $(`.add_promotion_product[rowId=${id}]`).removeClass('disabled')
        .on('click', tableSelectbind);
        disabledSelector.delete(id);
      }

      /**
       * 创建商品搜索表格
       */
      const productSearchTable = dataTable.create({
        dom: '#xquark_select_products_tables',
        lazyInit: true,
        url: productListUrl,
        reqParams: getReqParams(),
        autoFit: false,
        drawBack: function () {
          $('.add_promotion_product').on('click', function () {
            const _this = this;
            if (settedGlobalDiscount) {
              utils.tools.confirm('您已设置了全场优惠, 要取消吗?', function () {
                unSetGlobalDiscount.call($('#setting-global'));
                tableSelectbind.call(_this);
              });
            } else {
              tableSelectbind.call(_this);
            }
          });
        },
        columns: [
          {
            title: '商品图片',
            render: function (data, type, row) {
              return '<a href="' + row.productUrl
                  + '"><img class="goods-image" src="' + row.img + '" /></a>';
            }
          },
          {
            title: '名称',
            data: "name",
            name: "name"
          },
          {
            title: 'U9编码',
            data: "encode",
            name: "encode"
          },
          {
            title: 'U8编码',
            data: "u8Encode",
            name: "u8Encode"
          },
          {
            title: '状态',
            render: function (data, type, row) {
              var status = '';
              switch (row.status) {
                case 'INSTOCK':
                  status = '下架';
                  break;
                case 'ONSALE':
                  status = '在售';
                  break;
                case 'FORSALE':
                  status = '待上架发布';
                  break;
                case 'DRAFT':
                  status = '未发布';
                  break;
                default:
                  break;
              }
              return status;
            },
          },
          {
            title: '价格',
            data: "price",
            name: "price"
          }, {
            title: '库存',
            data: "amount",
            name: "amount"
          }, {
            title: '商品状态',
            width: 100,
            render: function (data, type, row) {
              let html = '';
              const isDisabled = row.isSelect || disabledSelector.has(row.id);
              const classVal = "add_promotion_product" + (isDisabled
                  ? ' disabled'
                  : '');
              html += `<a href="javascript:void(0);" class="${classVal} nowrap"
            rowId="${row.id}"><i class="icon-pointer"></i>添加</a>`;
              return html;
            }
          }
        ]
      });

      const promotionViewTable = dataTable.create({
        dom: '#xquark_promotion_view_tables',
        autoFit: false,
        showSelector: true,
        columns: [
          {
            title: '商品名称',
            data: 'name',
            name: 'name',
            width: 120
          },
          {
            title: '店铺价',
            data: "price",
            name: "price",
            width: 60
          },
          {
            title: '折扣',
            data: "discount",
            name: "discount",
            width: 100
          },
          {
            title: '减价',
            data: "discountFee",
            name: "discountFee",
            width: 100
          },
          {
            title: '促销价',
            data: "afterPrice",
            name: "afterPrice",
            width: 80
          },
          {
            title: '优惠合计',
            data: "totalDiscount",
            name: "totalDiscount",
            width: 80
          },
          {
            title: '操作',
            data: 'operation',
            name: 'operation',
            width: 100
          }
        ]
      });

      /**
       * 修改折扣后重新计算价格
       */
      $(document).on('blur', '.table-discount', function () {
        const rowData = promotionViewTable.rowData($(this).parents('tr'));
        const value = $(this).val();
        const newData = reCalculate(value, rowData, 'DISCOUNT');
        /**
         * 根据名称修改列数据
         */
        promotionViewTable.modifyRow(column => column.name === rowData.name,
            () => newData);
      });

      /**
       * 修改折扣减价后重新计算价格
       */
      $(document).on('blur', '.table-discount-fee', function () {
        const rowData = promotionViewTable.rowData($(this).parents('tr'));
        const value = $(this).val();

        const newData = reCalculate(value, rowData, 'DIRECT');
        /**
         * 根据名称修改列数据
         */
        promotionViewTable.modifyRow(column => column.name === rowData.name,
            () => newData);
      });

      // 事件 ---

      $('.btn-trigger').on('click', function () {
        productSearchTable.update(getReqParams()).refreshAll();
        $productModal.modal('show');
      });

      /**
       * 设置全局折扣
       */
      $('#setting-global').on('click', function () {
        if (settedGlobalDiscount) {
          unSetGlobalDiscount.call(this);
          return;
        }
        $setDiscountModal.modal('show');
      });

      $('input[name=userScope]').on('change', function () {
        const $type = $('input[name=type]');
        if ($(this).val() === 'ALL') {
          $type.val('FULLCUT');
          $('#setting-global').hide();
        } else {
          $type.val('INSIDE_BUY');
          $('#setting-global').show();
        }
      });

      /**
       * 确认设置全场折扣
       */
      $('#setDiscountModalBtn').on('click', function () {
        utils.post(hasActivatePromotionUrl($('input[name=type]').val()),
            ret => {
              if (ret && ret === true) {
                utils.tools.error('您已经配置该类型活动，请先结束其他活动再设置全场活动');
              } else {
                const tableCount = promotionViewTable.rowCount();
                if (tableCount !== 0) {
                  utils.tools.confirm('您选择了特定商品优惠, 确认要设置为全场优惠吗?', () => {
                    modifyGlobalDiscount();
                    disabledSelector.clear();
                    promotionViewTable.removeRows();
                    hideTableContainer();
                  });
                } else {
                  modifyGlobalDiscount();
                }
              }
            });
      });

      /**
       * 确认批量设置折扣
       */
      $('#setBatchDiscountModalBtn').on('click', function () {
        const discount = checkDiscount($('#batchDiscount').val());
        if (!discount) {
          return;
        }
        // 批量修改折扣
        promotionViewTable.modifyRowByNode(rowNode => $(rowNode)
            .find('input[type=checkbox]').is(':checked'),
            rowData => reCalculate(discount, rowData, 'DISCOUNT'));

        // 设置完毕后将选框都取消
        $('input[type=checkbox]').prop('checked', false);
      });

      /**
       * 批量设置折扣
       */
      $('.btn-batch-setting').on('click', function () {
        if (promotionViewTable.checkedRowNum() <= 0) {
          utils.tools.error('请先选择折扣');
          return;
        }
        $setBatchDiscountModal.modal('show');
      });

      $('.btn-batch-remove').on('click', function () {
        utils.tools.confirm('确认撤出选中商品吗?', function () {
          if (promotionViewTable.checkedRowNum() <= 0) {
            utils.tools.error('请先选择折扣');
            return;
          }
          // 撤出所有选中的活动, 并恢复商品表格的选中
          promotionViewTable.removeRows(checkbox => {
            const $checkbox = $(checkbox);
            const isChecked = $checkbox.is(':checked');
            if (isChecked) {
              const id = $checkbox.val();
              enableRowSelect(id);
            }
            return isChecked;
          });
          // 设置完毕后将选框都取消
          $('input[type=checkbox]').prop('checked', false);
        });
      });

      /**
       * 撤出预览商品
       */
      $(document).on('click', '.rowRemove', function () {
        const $this = $(this);
        const rowSelector = $this.parents('tr');
        promotionViewTable.removeRow(rowSelector);
        enableRowSelect($this.attr('rowId'));
      });

      /**
       * 设置全场折扣
       * @param discount
       */
      function setGlobalDiscount(discount) {
        // 设置隐藏域的值
        $('#discount').val(discount);
        $('#setting-global').text(`取消设置全场${discount}折`);
        settedGlobalDiscount = true;
        $('#scope').val('ALL');
        hideDiscountModal();
      }

      /**
       * 修改全局折扣
       */
      const modifyGlobalDiscount = function () {
        const discount = checkDiscount($('#globalDiscount').val());
        if (!discount) {
          return;
        }
        setGlobalDiscount(discount);
      };

      /**
       * 取消设置全店折扣
       */
      function unSetGlobalDiscount() {
        $(this).text('设置全店折扣');
        settedGlobalDiscount = false;
        $('#discount').val('');
        $('#scope').val('PRODUCT');
        hideDiscountModal();
      }

      function hideDiscountModal() {
        $setDiscountModal.modal('hide');
      }

      // 事件 END ---

      // 全局函数 ---

      /**
       * 刷新表格请求参数
       * @returns {{keyword: jQuery, type: string, promotionType: string, scope: jQuery, categoryId: jQuery, status: jQuery}}
       */
      function getReqParams() {
        return {
          promotionId: id,
          keyword: $('#select_products_sKeyword').val(),
          type: 'NORMAL',
          promotionType: $('input[name=type]').val(),
          scope: $('input[name=userScope]:checked').val(),
          categoryId: $('#categoryTypeModal').val(),
          status: $('#pStatus').val()
        };
      }

      /**
       * 刷新商品分类
       */
      function refreshCategory() {
        /**
         * 初始化商品分类
         */
        utils.post(window.host + '/shop/category/list', function (categories) {
          const $categoryType = $('#categoryTypeModal');
          $categoryType.empty();
          $categoryType.append(
              '<option value="" selected="selected">所有分类</option>');
          categories.forEach(function (category) {
            const html = `<option value='${category.id}'>${category.name}</option>`;
            $categoryType.append(html);
          });
        });
      }

      /**
       * 构建展示表格列
       * @param rowData
       * @returns {{name: *, price: *, discount: string, discountFee: string, afterPrice: string, totalDiscount: string, operation: string}}
       */
      function buildViewRow(rowData) {
        return {
          id: rowData.id,
          name: rowData.name,
          price: rowData.price,
          discount: buildDiscountInput(rowData.discount || ''),
          // discountFee: buildDiscountFeeInput(rowData.cutDownPrice || 0),
          discountFee: '¥' + (rowData.cutDownPrice || 0),
          afterPrice: '¥' + (rowData.afterPrice || rowData.price),
          totalDiscount: rowData.cutDownPrice || '¥0',
          operation: `<a class="rowRemove" rowId="${rowData.id}"><i class="icon-trash"></i>撤出</a>`
        };
      }

      /**
       * 将表格数据构建成带输入框的数据
       * @param value
       * @param prefix
       * @param suffix
       * @param cl
       * @returns {string}
       */
      function buildRowInput(value, prefix, suffix, cl) {
        return `<div class="nowrap">${prefix ? prefix
            : ''}<input style="width: 50px" class="${cl}"
      type="text" value="${value ? value : ''}">${suffix ? suffix : ''}</div>`
      }

      function buildDiscountInput(discount) {
        return buildRowInput(discount, '', '折', 'table-discount');
      }

      function buildDiscountFeeInput(discountFee) {
        // 暂时固定减价值不能输入
        return '¥' + discountFee;
        // return buildRowInput(discountFee, '¥', '', 'table-discount-fee');
      }

      /**
       * 根据折扣或减价重新计算表格中的价格
       * @param calValue 折扣或减价 数值
       * @param rowData 表格行数据
       * @param from 折扣或减价 类型
       */
      function reCalculate(calValue, rowData, from) {
        const price = parseFloat(rowData.price);
        let discount = rowData.discount;
        let discountFee = rowData.discountFee;
        if (from === 'DISCOUNT') {
          discount = checkDiscount(calValue);
          if (!discount) {
            utils.tools.error('请设置大于0的优惠值');
            return;
          }
          discountFee = (price - price * discount / 10).toFixed(2);
        }
        if (from === 'DIRECT') {
          discountFee = checkDirectCut(price, calValue);
          if (!discountFee) {
            return;
          }
          discount = (discountFee / price * 10).toFixed(2);
        }
        let afterPrice = price - discountFee;
        rowData.discount = buildDiscountInput(discount);
        rowData.discountFee = buildDiscountFeeInput(discountFee);
        rowData.afterPrice = '¥' + afterPrice.toFixed(2);
        rowData.totalDiscount = '¥' + discountFee;
        return rowData;
      }

      /**
       * 校验表格输入同时转为保留两位小数的数字
       * @param discount
       * @returns {number | *}
       */
      function checkDiscount(discount) {
        if (!discount || discount === '') {
          utils.tools.error('请输入折扣');
          return;
        }
        if (isNaN(discount)) {
          utils.tools.error('请输入数字');
          return;
        }
        discount = parseFloat(discount);
        if (discount > 10) {
          utils.tools.error('请输入正确的折扣');
        }
        return Math.round(discount * 100) / 100;
      }

      /**
       * 校验表格输入同时转为数字
       * @param originalPrice
       * @param directCutPrice
       * @returns {number | *}
       */
      function checkDirectCut(originalPrice, directCutPrice) {
        if (!directCutPrice || directCutPrice === '') {
          utils.tools.error('请输入减价金额');
          return;
        }
        if (isNaN(directCutPrice)) {
          utils.tools.error('请输入数字');
          return;
        }
        directCutPrice = parseFloat(directCutPrice);
        if (directCutPrice > originalPrice) {
          utils.tools.error('减价金额不能大于原价');
        }
        return Math.round(directCutPrice * 100) / 100;
      }

      /**
       * 序列化展示表格获取数据
       * @returns {*}
       */
      function serializePromotionTable() {
        let errorMsg = '';
        const data = promotionViewTable.getDataAll(row => {
          const newRow = {
            productId: row.id,
            discount: $(`<div>${row.discount}</div>`).find('input').val()
          };
          if (!newRow.discount || newRow.discount === '') {
            errorMsg = '请填写折扣';
          }
          newRow.discount = parseFloat(newRow.discount);
          if (newRow.discount > 10 || newRow.discount <= 0) {
            errorMsg = '请输入正确的折扣';
          }
          return newRow;
        });
        if (errorMsg !== '') {
          utils.tools.error(errorMsg);
          return null;
        }
        return data;
      }

      /**
       * 恢复数据
       * @param id
       */
      function resumeData(id) {
        utils.post(viewPromotionUrl(id), data => {
          utils.tools.syncForm($form, data);
          picker.setViewDate(data.validFrom, data.validTo);

          $('input[name=isFreeDelivery]').prop('checked', data.isFreeDelivery);
          const globalDiscount = data.discount;
          if (globalDiscount && globalDiscount !== '' && data.scope === 'ALL') {
            setGlobalDiscount(globalDiscount);
          }
          const products = data.products;
          if (products && products.length > 0) {
            showTableContainer();
            products.forEach(item => {
              // 放到以选择商品set中
              disabledSelector.add(item.productId);
              promotionViewTable.addRow(buildViewRow({
                id: item.productId,
                name: item.productName,
                price: item.productPrice,
                discount: item.discount,
                cutDownPrice: item.cutDownPrice,
                afterPrice: item.afterPrice
              }))
            });
          }

        })
      }

      function showTableContainer() {
        $('.table-container').show('slow');
      }

      function hideTableContainer() {
        $('.table-container').hide('slow');
      }

    });

define('fullCut/list',['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui',
  'select2'], function ($, utils) {

  const prefix = window.host + '/promotionFullCut';
  const listUrl = prefix + '/listPromotion';
  const listDetailUrl = prefix + '/orderDetail';
  const closeUrl = prefix + '/close';

  const listProductUrl = id => prefix + `/listProduct?id=${id}`;

  const $dataTable = $('#xquark_full_cut_tables');
  const $detailTable = $('#xquark_order_detail_table');
  const $viewProductModal = $('#view-product-modal');

  let id = '';

  var keyWord = '';

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const typeMapper = {
    'FULLCUT': '满减优惠',
    'FULLPIECES': '满件优惠'
  };

  const manager = (function () {

    const $addPromotionBtn = $('.btn-release');

    const globalInstance = {};

    return {
      initGlobal: function () {
        return this;
      },
      initEvent: function () {
        $addPromotionBtn.on('click', function () {
          location.href = '/sellerpc/mall/fullCutEdit';
        });
        return this;
      },
      initTable: function () {
        $('.edit').on('click', function () {
          var id = $(this).attr('rowId');
          location.href = '/sellerpc/mall/fullCutEdit?id=' + id;
        });

        $('.del').on('click', function () {
          var id = $(this).attr('rowId');
          utils.tools.confirm("确认结束活动吗", function () {
            utils.postAjaxWithBlock($(document), closeUrl, {id: id},
                function (res) {
                  if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                      case 200: {
                        utils.tools.alert('操作成功',
                            {timer: 1200, type: 'success'});
                        $fullCutDataTables.search('').draw();
                        break;
                      }
                      default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                      }
                    }
                  } else if (res == 0) {

                  } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                  }
                })
          }, function () {

          });
        });

        $('.showProduct').on('click', function () {
          id = $(this).attr('rowId');
          $viewProductModal.modal('show');
          $viewProductTable.search('').draw();
        })
      }
    }
  })();

  manager.initGlobal().initEvent();

  const $viewProductTable = $('#xquark_view_products_tables').DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ajax: function (data, callback) {
      $.get(listProductUrl(id), {
        size: data.length,
        page: data.start / data.length,
        pageable: true
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    },
    rowId: 'id',
    columns: [
      {
        title: '商品名称',
        data: "productName",
        width: "50px",
        orderable: false,
        name: "productName"
      },
      {
        title: '店铺价',
        data: "productPrice",
        width: "50px",
        orderable: false,
        name: "productPrice"
      },
      {
        title: '折扣',
        width: "50px",
        orderable: false,
        name: "discount",
        render: function (data, type, row) {
          return row.discount + '折';
        }
      },
      {
        title: '减价',
        width: "50px",
        orderable: false,
        name: "cutDownPrice",
        data: "cutDownPrice"
      },
      {
        title: '折后价',
        width: "50px",
        orderable: false,
        name: "afterPrice",
        data: "afterPrice"
      }
    ],
    select: {
      style: 'multi'
    },
    drawCallback: function () {  //数据加载完成后初始化事件
      // manager.initTable();
    }
  });

  /** 初始化表格数据 **/
  const $fullCutDataTables = $dataTable.DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: true,
    ajax: function (data, callback) {
      $.get(listUrl, {
        size: data.length,
        page: data.start / data.length,
        pageable: true,
        keyWord: keyWord
        // order: $columns[data.order[0].column],
        // direction: data.order[0].dir
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    },
    rowId: 'id',
    columns: [
/*      {
        title: '',
        width: "10px",
        orderable: false,
        render: function (data, type, row) {
          return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'
              + row.id + '"></label>';
        }
      },*/
      {
        title: '标题',
        data: "title",
        width: "120px",
        orderable: false,
        name: "title"
      }, {
        title: '开始时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.validFrom);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "validFrom"
      }, {
        title: '结束时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.validTo);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "validTo"
      }, {
        title: '状态',
        width: "60px",
        orderable: false,
        render: function (data, type, row) {
          var status = '';
          switch (row.archive) {
            case false:
              status = '进行中';
              break;
            case true:
              status = '已结束';
              break;
            default:
              break;
          }
          return status;
        },
      },
      {
        title: '活动范围',
        width: "60px",
        orderable: false,
        render: function (data, type, row) {
          const scope = row.scope;
          if (scope === 'PRODUCT') {
            return `<a href="javascript:void(0);" rowId="${row.id}" class="showProduct">自选范围</a>`;
          }
          return '全部'
        },
      },
      {
        title: '发布时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.createdAt);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "onsaleAt"
      },
      {
        title: '管理',
        sClass: "right",
        width: "120px",
        orderable: false,
        render: function (data, type, row) {
          var html = '';
          html += '<a href="javascript:void(0);" class="edit" rowId="'
              + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
          html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'
              + row.id + '" ><i class="icon-trash"></i>结束</a>';
          return html;
        }
      }
    ],
    select: {
      style: 'multi'
    },
    drawCallback: function () {  //数据加载完成后初始化事件
      manager.initTable();
    }
  });

  const $detailDataTable = $detailTable.DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ajax: function (data, callback) {
      $.get(listDetailUrl, {
        size: data.length,
        page: data.start / data.length,
        pageable: true
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    },
    rowId: 'id',
    columns: [
      {
        title: '活动名称',
        data: "title",
        width: "50px",
        orderable: false,
        name: "title"
      },
      {
        title: '优惠类型',
        width: "80px",
        orderable: false,
        name: "type",
        render: function (data, type, row) {
          var type = row.type;
          return typeMapper[type];
        }
      },
      {
        title: '订单号',
        data: "orderNo",
        width: "100px",
        orderable: false,
        name: "orderNo"
      },
      {
        title: '买家',
        width: "100px",
        orderable: false,
        name: "buyer",
        render: function (data, type, row) {
          var name = row.buyerName;
          var phone = row.buyerPhone;
          return (name ? name : '') + (phone ? '</br>' + phone : '');
        }
      },
      {
        title: '卖家',
        width: "100px",
        orderable: false,
        name: "seller",
        render: function (data, type, row) {
          var name = row.sellerName;
          var phone = row.sellerPhone;
          return (name ? name : '') + (phone ? '</br>' + phone : '');
        }
      },
      {
        title: '订单号',
        data: "orderNo",
        width: "100px",
        orderable: false,
        name: "orderNo"
      },
      {
        title: '订单原价',
        data: "goodsFee",
        width: "50px",
        orderable: false,
        name: "goodsFee"
      },
      {
        title: '购买',
        data: "totalFee",
        width: "50px",
        orderable: false,
        name: "totalFee"
      },
      {
        title: '优惠价',
        data: "discountFee",
        width: "50px",
        orderable: false,
        name: "discountFee"
      },
      {
        title: '开始时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.validFrom);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "validFrom"
      }, {
        title: '结束时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.validTo);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "validTo"
      }, {
        title: '状态',
        width: "60px",
        orderable: false,
        render: function (data, type, row) {
          var status = '';
          switch (row.archive) {
            case false:
              status = '进行中';
              break;
            case true:
              status = '已结束';
              break;
            default:
              break;
          }
          return status;
        },
      }, {
        title: '发布时间',
        orderable: false,
        width: "120px",
        render: function (data, type, row) {
          var cDate = parseInt(row.createdAt);
          var d = new Date(cDate);
          return d.format('yyyy-MM-dd hh:mm:ss');
        },
        name: "onsaleAt"
      }
    ],
    select: {
      style: 'multi'
    },
    drawCallback: function () {  //数据加载完成后初始化事件
      // manager.initTable();
    }
  });

  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

});


require(['all']);

//module
require(['fullCut/editNew', 'fullCut/list'],function(){
});

define("fullCut", function(){});

