/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('twitter/withdraw',['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment) {
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            opens: 'right',
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-left');
        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });
        $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

        $('.daterange-left').val('');
        $("input[name='startDate']").val('');
        $("input[name='endDate']").val('');
        options.startDate = '';
        options.endDate   = '';

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-left').on('apply.daterangepicker', function(ev, picker) {
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
            $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
            $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        var $datatables = $('#withdrawApplyList').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/withdraw/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#selectStatus").val(),
                    sellerPhone: $("#sellerPhone").val(),
                    sellerName: $("#sellerName").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.withdrawTotal.totalCount,
                        recordsFiltered: res.data.withdrawTotal.totalCount,
                        data: res.data.list,
                        iTotalRecords:res.data.withdrawTotal.totalCount,
                        iTotalDisplayRecords:res.data.withdrawTotal.totalCount
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "15px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<label class="checkbox"><input name="checkOrders" rowid="'+row.id+'" rowstatus="'+row.status+'" rowaccountName="'+row.accountName+'" rowapplyMoney="'+row.applyMoney+'"  type="checkbox" class="styled" value="'+row.id+'"></label>';
                    }
                },
                {
                width: '160px',
                data: 'createdAt',
                name: 'createdAt',
                title: "创建时间",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm');
                }
            },{
                width: '160px',
                data: 'accountName',
                name: 'accountName',
                title: '推客名称'
            },{
                width: '120px',
                title: '联系电话',
                data: 'phone',
                name: 'phone',
            },{
                data: "applyMoney",
                name: "applyMoney",
                width: '120px',
                title: '申请金额',
            },{
                width: '120px',
                title: '支付金额',
                data: "confirmMoney",
                name: "confirmMoney",
            },{
                width: '160px',
                title: '结算时间',
                render: function (data, type, row) {
                    if (row.payAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.payAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm');
                }
            },{
                width: '160px',
                title: '支付方式',
                data: "openingBank",
                name: "openingBank",
            },{
                width: '120px',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if(value =='NEW'){
                        return '<a class="btn btn-primary btn-sm applyBtnPay" style="margin-right: 10px;"' +
                            'apply-money="' + row.applyMoney + '" ' +
                            'account-name="' + row.accountName + '" ' +
                            'opening-bank="' + row.openingBank + '" ' +
                            'withdraw-id="' + row.id + '"> 审核</a>';
                    } else if(value =='SUCCESS') {
                        return "已支付";
                    } else if(value =='PENDING') {
                        return "支付中";
                    } else if(value =='FAILED') {
                        return "支付失败";
                    } else {
                        return "已关闭";
                    }
                }
            }],

            drawCallback: function () {  //数据加载完成
                $("#checkAllGoods").prop("checked",false);
            }
        });

        // 审核
        $(document).on('click', '.applyBtnPay', function() {
            var applyMoney = $(this).attr('apply-money');
            var accountName = $(this).attr('account-name');
            var openingBank = $(this).attr('opening-bank');
            var withdrawId = $(this).attr('withdraw-id');
            var msg = '向用户: ' + accountName + ' 的' + openingBank + '账户打款:' + applyMoney + '元, 请再次核对金额';
            utils.tools.confirm(msg, function(){
                var url = window.host + "/withdraw/withdrawpay?withdrawApplyIds="+withdrawId+"&withdrawconfirmMoneys="+applyMoney;
                utils.postAjax(url,{},function(res){
                    if(typeof(res) === 'object') {
                        if (res.data) {
                            utils.tools.alert("打款成功!", {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("打款失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });

            },function(){

            });
        });

        $(document).on('click', '.backBtn', function() {
            var applyMoney = $(this).attr('apply-money');
            var accountName = $(this).attr('account-name');
            var openingBank = $(this).attr('opening-bank');
            var withdrawId = $(this).attr('withdraw-id');
            var msg = '打回用户: ' + accountName + ' 的提现申请';
            utils.tools.confirm(msg, function(){

            },function(){

            });
        });

        // 批量审核
        $(".btn-audit").on('click', function() {
            var updateds = getTableContent();
            if(updateds.length == 0 ){
                utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
                return ;
            }

            var b = true;
            var confirmStr = '';
            var ids = '', confirmMoneys = '';
            $.each(updateds, function(index, row){
                if(row.status!="NEW"  && row.status != 'FAILED'&& row.status!='PENDING'){
                    utils.tools.alert(row.accountName + '已完成提现，无法继续操作!');
                    b = false;
                    return false;
                }
                confirmStr += '用户：' + row.accountName + ' 金额：' + row.applyMoney + '  ';
                if(ids != '') {
                    ids += ',';
                    confirmMoneys += ',';
                }
                ids += row.id;
                confirmMoneys += row.applyMoney;
            });
            if(!b)
                return ;

            var msg = confirmStr + '  将对以上用户进行打款, 请再次核对金额';
            utils.tools.confirm(msg, function(){
                var url = window.host + "/withdraw/withdrawpay?withdrawApplyIds="+ids+"&withdrawconfirmMoneys="+confirmMoneys;
                utils.postAjax(url,{},function(res){
                    if(typeof(res) === 'object') {
                        if (res.data) {
                            utils.tools.alert("打款成功!", {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("打款失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });

            },function(){

            });
        });

        $(".btn-search").on('click', function() {
            $datatables.search('').draw();
        });

        // 得到当前选中的行数据
        function getTableContent(){
            var selectRows = new Array();
            for(var i = 0; i < $("input[name='checkOrders']:checked").length; i++){
                var value = {};
                var checkvalue = $("input[name='checkOrders']:checked")[i];
                value.id = $(checkvalue).attr("rowid");
                value.applyMoney = $(checkvalue).attr("rowapplyMoney");
                value.status = $(checkvalue).attr("rowstatus");
                value.accountName = $(checkvalue).attr("rowaccountName");
                selectRows.push(value);
            }
            return selectRows;
        }

        // 全选
        $("#checkAllGoods").on('click', function() {
            $("input[name='checkOrders']").prop("checked", $(this).prop("checked"));
        });

        $(".exportWithdrawBtn").on('click', function() {
            $('#exportWithdrawForm').submit();
        });
});
//微信账号绑定
require(['all']);

require(['twitter/withdraw']);

define("twitterWithdraw", function(){});

