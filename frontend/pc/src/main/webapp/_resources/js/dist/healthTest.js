/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('healthTest/moduleManage',['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils) {

    const prefix = window.host + '/healthTest';
    const treeList = prefix + '/module/jsTree';
    const saveUrl = prefix + '/module/save';
    const listUrl = prefix + '/module/list';
    const deleteUrl = prefix + '/module/delete';
    const viewUrl = prefix + '/module/view';

    /* 当前选中节点 */
    var selected = {
        'id': '0',
        'parent': null
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无数据"
        }
    });

    eventBind();

    const moduleTree = $('#module_tree');
    moduleTree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': treeList,
                'data': function (node) {
                    if (node === null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        moduleTree.jstree("select_node", "ul > li:first");
        var selectedNode = moduleTree.jstree("get_selected");
        moduleTree.jstree('open_node', selectedNode, false, true);
    }).on('delete_node.jstree', function (event, data) {
        deleteModule(data.node.id);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    }).on('create_node.jstree', function (event, data) {
        $dataTables.search('').draw();
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        selected = data.node;
        $dataTables.search('').draw();
    }

    /** 初始化表格数据 **/
    var $dataTables = $('#xquark_module_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                parentId: selected.id,
                pageable: true
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '50px',
                orderable: true,
                name: 'name'
            },
            {
                width: '20px',
                orderable: true,
                render: function (data, type, row) {
                    return row.sortNo;
                }
            },
            {
                width: '100px',
                orderable: false,
                render: function (data, type, row) {
                    return row.isLeaf ? '是' : '否';
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt == null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var staticName = row.staticName;
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" rowId="' + row.id + '" parent_id="' + row.parentId + '" fid="edit_module"><i class="icon-pencil7"></i>编辑</a>';
                    // 大标题菜单以及基础测试下不能添加题目
                    if (!(staticName)) {
                        html += '<a href="javascript:void(0);" style="margin-left: 10px" class="edit_result" rowId="' + row.id + '" fid="edit_result"><i class="icon-pencil7"></i>设置回答</a>';
                    }
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" parent_id="' + row.parentId + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    /**
     * 表格事件绑定
     */
    function initEvent() {
        $('body').unbind('click');

        /** 表格的编辑事件 **/
        $('.edit').on('click', function () {
            var id = $(this).attr("rowId");
            showUpdate(id);
        });

        $('.edit_result').on('click', function () {
            var id = $(this).attr("rowId");
            location.href = '/sellerpc/healthTest/module/result?id=' + id;
        });

        /** 点击删除部门弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId = $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("id");
                var tree = $('#module_tree').jstree(true);
                // 通过jstree的删除事件删除并刷新表格
                tree.delete_node(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") === "popover") {
                target.popover("toggle");
            }
        });
    }

    /**
     * 全局事件绑定
     */
    function eventBind() {

        /** 添加菜单 **/
        $('#add_module').on('click', function () {
            $('#id').val('');
            $('#parent_id').val(selected.id);
            $('#name').val('');
            $('#iconName').val('');
            $('#type').val('SUM');
            $('#requiredSex').val('0');
            $('#required').val('0');
            $('#modal_module_save').modal('show');
        });

        $('#update_module').on('click', function () {
            var url = window.host + '/cache/module';
            utils.postAjax(url, null, function (res) {
                if (res.errorCode === 200 && res.data) {
                    utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                } else {
                    utils.tools.alert(res.moreInfo);
                }
            });
        });

        $('#delete_module').on('click', function () {
            var id = selected.id;
            var tree = $('#module_tree').jstree(true);
            if (id === null || id === '0') {
                utils.tools.alert('请先在左侧选择菜单');
                return;
            }
            utils.tools.confirm("将同时删除菜单下所有子菜单，确定删除吗?", function () {
                tree.delete_node(selected);
            }, function () {

            });
        });

        $('#saveBtn_module').on('click', function () {
            var id = $('#id').val();
            var parent_id = $('#parent_id').val();
            var name = $('#name').val();
            var iconName = $('#iconName').val();
            var type = $('#type').val();
            var requiredSex = $('#requiredSex').val();
            var required = $('#required').val();
            if (!name || name === '') {
                utils.tools.alert("请输入菜单名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                id: id,
                parentId: parent_id,
                name: name,
                icon: iconName,
                type: type,
                requiredSex: requiredSex,
                required: required === '1'
            };
            utils.postAjaxWithBlock($(document), saveUrl, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            var data = res.data;
                            if (data && typeof data !== 'undefined') {
                                utils.tools.alert("操作成功");
                                $('#modal_module_save').modal('hide');
                                var node = {
                                    id: data.id,
                                    text: data.name,
                                    icon: data.icon
                                };
                                moduleTree.jstree().create_node(selected, node, 'last');
                                // window.location.href = window.originalHost + '/module/manage';
                            } else {
                                utils.tools.alert('操作失败', { timer: 1200, type: 'warning' });
                            }

                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

    }

    /*手动删除*/
    function deleteModule(id) {
        utils.postAjax(deleteUrl, {'id': id}, function (res) {
            if (res === -1) {
                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
            }
            if (typeof res === 'object') {
                if (res.errorCode === 200) {
                    if (res.data) {
                        utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                        $dataTables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败", {timer: 1200, type: 'warning'});
                    }
                } else {
                    if (res.moreInfo) {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    } else {
                        utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                    }
                }
            }
        });
    }

    /**
     * 显示更新弹窗
     * @param id
     */
    function showUpdate(id) {
        /* 查询要修改的数据 */
        $.ajax({
            url: viewUrl,
            type: 'POST',
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                if (data.errorCode === 200) {
                    var module = data.data;
                    $('#id').val(module.id);
                    $('#name').val(module.name);
                    $('#iconName').val(module.icon);
                    $('#type').val(module.type);
                    $('#requiredSex').val(module.requiredSex);
                    $('#required').val(module.required ? '1' : '0');
                    $('#modal_module_save').modal('show');
                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tools.goLogin();
                } else {
                    utils.tools.alert('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    }

});

define('healthTest/questionManage',['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils) {

    const prefix = window.host + '/healthTest';
    const listUrl = prefix + '/question/listTable';
    const treeList = prefix + '/module/jsTree';
    const importUrl = prefix + '/question/import';

    const $dataTables = $('#xquark_question_tables');

    /* 当前选中节点 */
    var selected = {
        'id': '0',
        'parent': null
    };

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "未配置问题"
        }
    });

    const typeMapper = {
        SINGLE_CHOICE: '单选题',
        MULTIPLE_CHOICE: '多选题',
        BLANK_FILLING: '填空题'
    };

    const eventManager = (function () {
        const $addBtn = $('#add_question');
        const $importModal = $('#modal_import_logistic_info');
        const globalInstance = {
            bindEvent: function () {
                $addBtn.on('click', function () {
                    var id = selected.id;
                    utils.postAjax(prefix + '/module/subCounts', {id: id}, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                var subCounts = res.data;
                                if (subCounts === 0 && id !== '0') {
                                    window.location = '/sellerpc/healthTest/question/edit?moduleId=' + id;
                                } else {
                                    utils.tools.alert("父类菜单不允许添加问题", {timer: 1200, type: 'warning'});
                                }
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
                return this;
            },
            initFile: function () {
                $('.file-input').fileinput({
                    language: 'zh',
                    uploadUrl: importUrl,
                    previewFileType: 'text',
                    browseLabel: '选择文件',
                    removeLabel: '删除',
                    uploadLabel: '上传',
                    browseIcon: '<i class="icon-file-plus"></i>',
                    uploadIcon: '<i class="icon-file-upload2"></i>',
                    removeIcon: '<i class="icon-cross3"></i>',
                    browseClass: 'btn btn-primary',
                    uploadClass: 'btn btn-default',
                    removeClass: 'btn btn-danger',
                    initialCaption: '',
                    maxFilesNum: 5,
                    allowedFileExtensions: ["xls"],
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        footer: ''
                    }
                });

                $importModal.on('hidden.bs.modal', function () {
                    $('#file-input').fileinput('clear');
                });

                $importModal.on('fileuploaded', function (event, data, previewId, index) {
                    var form = data.form, files = data.files, extra = data.extra,
                        response = data.response, reader = data.reader;
                    console.log(data);
                    console.log(response);
                    $questionDataTables.search('').draw();
                    // if (response == '200') {
                    //     utils.tools.alert('文件上传成功');
                    //     $('#modal_import_logistic_info').modal('hide');
                    // } else if (response == '207') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('发货信息上传全部失败,请核对上传格式是否正确,或数据是否填写正确');
                    // } else if (response == '206') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('部分订单上传失败,请核对上传格式是否正确,或数据是否填写正确');
                    // } else if (response == '500') {
                    //     $('#file-input').fileinput('enable');
                    //     utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
                    // }
                });
                return this;
            }
        };
        return {
            initGlobal: function () {
                globalInstance.bindEvent()
                    .initFile();
            },
            initTable: function () {
                $('.edit_question').on('click', function () {
                    var id = $(this).attr('rowId');
                    var moduleId = $(this).attr('moduleId');
                    var groupId = $(this).attr('groupId');
                    window.location = '/sellerpc/healthTest/question/edit?id=' + id + '&moduleId=' + moduleId + '&groupId=' + groupId;
                });

                $('.del_question').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认要删除吗', function () {
                        utils.postAjax(prefix + '/question/delete', {id: id}, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    var data = res.data;
                                    if (data) {
                                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                                        $questionDataTables.search('').draw();
                                    } else {
                                        utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
                                    }
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                    } else {
                                        utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                    }
                                }
                            }
                        });
                    }, function () {

                    });
                });
            }
        }
    })();

    eventManager.initGlobal();

    const questionModuleTree = $('#question_module_tree');
    questionModuleTree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': treeList,
                'data': function (node) {
                    if (node === null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        questionModuleTree.jstree("select_node", "ul > li:first");
        var selectedNode = questionModuleTree.jstree("get_selected");
        questionModuleTree.jstree('open_node', selectedNode, false, true);
    }).on('delete_node.jstree', function (event, data) {
        // deleteModule(data.node.id);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    }).on('create_node.jstree', function (event, data) {
        $questionDataTables.search('').draw();
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        selected = data.node;
        $questionDataTables.search('').draw();
    }

    /** 初始化表格数据 **/
    const $questionDataTables = $dataTables.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                moduleId: selected.id,
                pageable: true
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert('数据加载失败', {timer: 2000, type: 'warning'});
                    return;
                }
                if (!res.data.list) {
                    res.data.list = [];
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                data: 'name',
                width: '200px',
                orderable: true,
                name: 'name'
            },
            {
                width: '20px',
                orderable: true,
                render: function (data, type, row) {
                    var mapper = typeMapper[row.type];
                    return mapper ? mapper : '无';
                }
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    var correctAnswerId = row.answerId;
                    var answers = row.answers;
                    if (correctAnswerId && answers && answers.length > 0) {
                        var result = $.grep(answers, function (item) {
                            return item.id = correctAnswerId;
                        });
                        if (result.length === 1) {
                            return result[0].content;
                        }
                    } else {
                        return '无';
                    }
                }
            },
            {
                width: '80px',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_question" rowId="' + row.id + '" moduleId="' + row.moduleId + '" groupId="' + row.groupId + '" fid="edit_question"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del_question" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_question"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            eventManager.initTable();
        }
    });

});

/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('healthTest/result_list',['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';

    var listProducts = window.host + "/product/list";
    const listUrl = prefix + '/module/results';
    const bindProductUrl = prefix + '/result/addProduct';
    const unbindProductUrl = prefix + '/result/removeProduct';
    const deleteUrl = prefix + '/result/delete';
    const listProductsUrl = prefix + '/result/listProduct';

    const $dataTable = $('#xquark_result_tables');
    const $productModal = $('#modal_result_products');
    const $choosenProductModal = $('#modal_products');

    var moduleId = getParameterByName('id');
    var shopId = null;
    var resultId = null;
    var category = '';
    var order = '';

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "暂无相关数据",
            emptyTable: "暂无相关数据"
        }
    });

    const manager = (function () {
        const $addResult = $('#add_result');
        const $addProduct = $('.btn-addProduct');
        const $chooseModalClose = $('.close_product_choose');
        const globalInstance = {
            bindEvent: function () {
                $addResult.on('click', function () {
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId;
                });
                $addProduct.on('click', function () {
                    $productModal.modal('show');
                    $choosenProductModal.modal('hide');
                });
                $chooseModalClose.on('click', function () {
                    $productModal.modal('hide');
                    $choosenProductModal.modal('show');
                });
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.bindEvent();
            },
            initTable: function () {
                $('body').unbind('click');

                $('.edit_result').on('click', function () {
                    var id = $(this).attr('rowId');
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId + '&resultId=' + id;
                });
                $('.edit_product').on('click', function () {
                    resultId = $(this).attr('rowId');
                    $productDataTables.search('').draw();
                    // $productModal.modal('show');
                    $('#modal_products').modal('show');
                });

                /** 点击删除部门弹出框 **/
                $("[data-toggle='popover']").popover({
                    trigger: 'manual',
                    placement: 'left',
                    html: 'true',
                    animation: true,
                    content: function () {
                        var rowId = $(this).attr("rowId");
                        return '<span>确认删除？</span>' +
                            '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" rowId="' + rowId + '">确认</button>' +
                            '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                    }
                });

                $('[data-toggle="popover"]').popover() //弹窗
                    .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                        $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                    }).on('shown.bs.popover', function () {
                    var that = this;
                    $('.popover-btn-ok').on("click", function () {
                        var id = $(this).attr("rowId");
                        deleteResult(id);
                    });
                    $('.popover-btn-cancel').on("click", function () {
                        $(that).popover("hide");
                    });
                });

                $('body').on('click', function (event) {
                    var target = $(event.target);
                    if (!target.hasClass('popover') //弹窗内部点击不关闭
                        && target.parent('.popover-content').length === 0
                        && target.parent('.popover-title').length === 0
                        && target.parent('.popover').length === 0
                        && target.data("toggle") !== "popover") {
                        //弹窗触发列不关闭，否则显示后隐藏
                        $('[data-toggle="popover"]').popover('hide');
                    } else if (target.data("toggle") === "popover") {
                        target.popover("toggle");
                    }
                });
            },
            initSelectProductEvent: function () {
                $(".selectproduct").on("click", function () {
                    var productId = $(this).attr('rowId');
                    utils.postAjax(bindProductUrl, {resultId: resultId, productId: productId}, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                $productDataTables.search('').draw();
                                utils.tools.alert('添加成功', {timer: 1200, type: 'success'});
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
            }
        }
    })();

    manager.initGlobal();

    /** 初始化表格数据 **/
    const $resutlDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                moduleId: moduleId
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo);
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    return row.moduleName;
                }
            },
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    var physiqueId = row.physiqueId;
                    return physiqueId ? '体质测试' : '基础测试';
                }
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    return row.start + ' - ' + row.end;
                }
            },
            {
                width: '50',
                orderable: false,
                render: function (data, type, row) {
                    var description = row.description;
                    var maxLength = 20;
                    if (description) {
                        if (description.length > 20) {
                            description = description.substring(0, maxLength);
                        }
                    } else {
                        return '无';
                    }
                    return description;
                }
            },
            {
                width: '100',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_result" rowId="' + row.id + '" fid="edit_result"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del_result" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_result"><i class="icon-trash"></i>删除</a>';
                    html += '<a href="javascript:void(0);" class="edit_product" style="margin-left: 10px;"  rowId="' + row.id + '" fid="edit_product"><i class="icon-pencil7"></i>推荐商品</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    var $productDataTables = $('#xquark_list_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(listProductsUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                resultId: resultId
            }, function(res) {
                if (!res.data && !res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                title: '商品',
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                title: '状态',
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '价格',
                data: "price",
                width: "50px",
                orderable: true,
                name:"price"
            }, {
                title: '库存',
                data: "amount",
                orderable: true,
                width: "50px",
                name:"amount"
            },
            {
                title: '销量',
                data: "sales",
                orderable: true,
                width: "50px",
                name:"sales"
            },{
                title: '发布时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                title: '管理',
                orderable: false,
                width: '50px',
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="del" rowId="' + row.id + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            $('.del').on('click', function () {
                var productId = $(this).attr('rowId');
                var data = {
                    productId: productId,
                    resultId: resultId
                };
                utils.tools.confirm('确认删除吗', function () {
                    utils.postAjax(unbindProductUrl, data, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                utils.tools.alert('删除成功', { timer: 1200, type: 'success' });
                                $productDataTables.search('').draw();
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                }, function () {

                });
            });
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listProducts, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if (order !== '') {
                        return order;
                    } else {
                        var _index = data.order[0].column;
                        if (_index < 4) {
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else {
                    if (res.data.shopId) {
                        shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.categoryTotal,
                    iTotalDisplayRecords: res.data.categoryTotal
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="' + row.id + '" productImg="' + row.imgUrl + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            manager.initSelectProductEvent();
        }
    });

    function deleteResult(id) {
        utils.postAjaxWithBlock($(document), deleteUrl, {id: id}, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        utils.tools.alert('操作成功', { timer: 1200, type: 'success' });
                        $resutlDataTables.search('').draw();
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res === 0) {

            } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            listProducts = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            listProducts = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});

require(['all']);

require(['healthTest/moduleManage', 'healthTest/questionManage', 'healthTest/result_list']);

define("healthTest", function(){});

