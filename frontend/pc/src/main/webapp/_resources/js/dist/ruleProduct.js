/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by chh on 16/12/13.
 */
define('rule/ruleProduct',['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/ruleProduct/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    var $order = '';

    var $productlistUrl = window.host + "/product/list";

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增
    $(".btn-release").on('click', function() {
        $("#id").val('');
        $("#productId").val('');
        $("#ruleId").val('');
        $("#ruleName").val('');
        $("#productName").val('');
        $("#extConditions").val('');
        $("#priority").val('');
        $("#commissionFee").val('');
        $("#commissionRate").val('');
        $("#modal_component").modal("show");

    });

    buttonRoleCheck('.hideClass');

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productImgUrl+'"><img class="goods-image" src="'+row.productImgUrl+'" /></a>';
                }
            },
            {
                data: "productName",
                width: "120px",
                orderable: false,
                name:"productName"
            },{
                data: "productPrice",
                width: "100px",
                orderable: false,
                name:"productPrice"
            },{
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.ruleName)
                    {
                        case 'GENERAL_DIRECT_TO_DIRECTOR':
                            status = '董事的直接下级总顾问订单分润给董事';
                            break;
                        case 'GENERAL_INDIRECT_TO_DIRECTOR':
                            status = '董事的间接下级总顾问订单分润给董事';
                            break;
                        case 'DIRECTORS_TEAM_TO_DIRECTORS':
                            status = '董事下属团队(包括董事本身)订单需要分润给上级和上上级董事';
                            break;
                        case 'GENERAL_DIRECT_TO_GENERAL':
                            status = '总顾问直接推荐一位总顾问一次性返利';
                            break;
                        case 'TEAM_ORDER':
                            status = '团队订单返利';
                            break;
                        case 'AREA_ORDER':
                            status = '区域订单返利';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.policyName)
                    {
                        case 'DEFAULT':
                            status = '默认设置分佣规则集';
                            break;
                        case 'USER_AGENT_DEFAULT':
                            status = '代理商默认分佣规则集';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                data: "extConditions",
                width: "100px",
                orderable: false,
                name:"extConditions"
            },{
                data: "priority",
                width: "100px",
                orderable: false,
                name:"priority"
            }, {
                data: "commissionFee",
                width: "100px",
                orderable: false,
                name:"commissionFee"
            },
            {
                data: "commissionRate",
                width: "100px",
                orderable: false,
                name:"commissionRate"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_product_price_rule"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_product_price_rule"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($productlistUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 4){
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                category : '',
                isGroupon : ''
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords:res.data.categoryTotal,
                    iTotalDisplayRecords:res.data.categoryTotal
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: false,
                name:"price"
            }, {
                data: "amount",
                orderable: false,
                width: "50px",
                name:"amount"
            },
            {
                data: "sales",
                orderable: false,
                width: "50px",
                name:"sales"
            },{
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'+row.id+'" productName="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initSelectProductEvent();
        }
    });

    var $selectruledatatables = $('#xquark_select_rules_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/rule/list", {
                size: data.length,
                page: (data.start / data.length),
                keyword: '',
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.ruleName)
                    {
                        case 'GENERAL_DIRECT_TO_DIRECTOR':
                            status = '董事的直接下级总顾问订单分润给董事';
                            break;
                        case 'GENERAL_INDIRECT_TO_DIRECTOR':
                            status = '董事的间接下级总顾问订单分润给董事';
                            break;
                        case 'DIRECTORS_TEAM_TO_DIRECTORS':
                            status = '董事下属团队(包括董事本身)订单需要分润给上级和上上级董事';
                            break;
                        case 'GENERAL_DIRECT_TO_GENERAL':
                            status = '总顾问直接推荐一位总顾问一次性返利';
                            break;
                        case 'TEAM_ORDER':
                            status = '团队订单返利';
                            break;
                        case 'AREA_ORDER':
                            status = '区域订单返利';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },
            {
                width: "180px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.policyName)
                    {
                        case 'DEFAULT':
                            status = '默认设置分佣规则集';
                            break;
                        case 'USER_AGENT_DEFAULT':
                            status = '代理商默认分佣规则集';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },
            {
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                data: "index",
                width: "80px",
                orderable: false,
                name:"index"
            }
            , {
                data: "params",
                width: "120px",
                orderable: false,
                name:"params"
            }, {
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectrule" style="margin-left: 10px;" rowId="'+row.id+'" ruleName="'+row.ruleName+'" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initRuleEvent();
        }
    });

    function initRuleEvent(){
        $(".selectrule").on("click",function(){
            var ruleId =  $(this).attr("rowId");
            var ruleName =  $(this).attr("ruleName");
            $("#ruleId").val(ruleId);
            $("#ruleName").val(ruleName);
            $("#modal_select_rules").modal("hide");
        });
    }

    function initSelectProductEvent(){
        $(".selectproduct").on("click",function(){
            var productId =  $(this).attr("rowId");
            var productName =  $(this).attr("productName");
            $("#productId").val(productId);
            $("#productName").val(productName);
            $("#modal_select_products").modal("hide");
        });
    }

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId");
           $.ajax({
                url: window.host + '/ruleProduct/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var role = data.data;
                        $("#id").val(role.id);
                        $("#productId").val(role.productId);
                        $("#ruleId").val(role.commissionRuleId);
                        $("#productName").val(role.productName);
                        $("#ruleName").val(role.ruleName);

                        $("#extConditions").val(role.extConditions);
                        $("#priority").val(role.priority);
                        $("#commissionFee").val(role.commissionFee);
                        $("#commissionRate").val(role.commissionRate);
                        $("#modal_component").modal("show");
                        /** 初始化选择框控件 **/
                        $('.select').select2({
                            minimumResultsForSearch: Infinity,
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
           });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComponent(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*手动删除*/
    function  deleteComponent(pId){
          var url = window.host + "/ruleProduct/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存
    $(".saveBtn").on('click', function() {
        var url = window.host + '/ruleProduct/save';
        var id = $("#id").val();
        var ruleId = $("#ruleId").val();
        var productId = $("#productId").val();
        var priority = $("#priority").val();
        var extConditions = $("#extConditions").val();
        var commissionFee = $("#commissionFee").val();
        var commissionRate = $("#commissionRate").val();

        if(!ruleId || ruleId == '' ){
            utils.tools.alert("请选择佣金规则!", {timer: 1200, type: 'warning'});
            return;
        }else if(!productId || productId == ''){
            utils.tools.alert("请选择商品!", {timer: 1200, type: 'warning'});
            return;
        }else if(!priority || priority == ''){
            utils.tools.alert("请输入优先级!", {timer: 1200, type: 'warning'});
            return;
        }else if( (!commissionFee || commissionFee == '') && (!commissionRate || commissionRate == '')){
            utils.tools.alert("佣金金额，佣金比例不能同时为空!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            commissionRuleId: ruleId,
            productId: productId,
            priority: priority,
            extConditions: extConditions,
            commissionFee: commissionFee,
            commissionRate: commissionRate
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/ruleProduct/list';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });

    // 选择活动商品
    $("#productName").on('focus', function() {
        $selectproductdatatables.search('').draw();
        $("#modal_select_products").modal("show");
    });

    // 选择角色
    $("#ruleName").on('focus', function() {
        $selectruledatatables.search('').draw();
        $("#modal_select_rules").modal("show");
    });

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            $productlistUrl = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }
    });


});
/**
 * Created by chh on 16/12/13.
 */
require(['all']);

//module
require(['jquery','uniform','placeholder','rule/ruleProduct'],function(){


});
define("ruleProduct", function(){});

