/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('healthTest/result_list',['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';

    var listProducts = window.host + "/product/list";
    const listUrl = prefix + '/module/results';
    const bindProductUrl = prefix + '/result/addProduct';
    const unbindProductUrl = prefix + '/result/removeProduct';
    const deleteUrl = prefix + '/result/delete';
    const listProductsUrl = prefix + '/result/listProduct';

    const $dataTable = $('#xquark_result_tables');
    const $productModal = $('#modal_result_products');
    const $choosenProductModal = $('#modal_products');

    var moduleId = getParameterByName('id');
    var shopId = null;
    var resultId = null;
    var category = '';
    var order = '';

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "暂无相关数据",
            emptyTable: "暂无相关数据"
        }
    });

    const manager = (function () {
        const $addResult = $('#add_result');
        const $addProduct = $('.btn-addProduct');
        const $chooseModalClose = $('.close_product_choose');
        const globalInstance = {
            bindEvent: function () {
                $addResult.on('click', function () {
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId;
                });
                $addProduct.on('click', function () {
                    $productModal.modal('show');
                    $choosenProductModal.modal('hide');
                });
                $chooseModalClose.on('click', function () {
                    $productModal.modal('hide');
                    $choosenProductModal.modal('show');
                });
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.bindEvent();
            },
            initTable: function () {
                $('body').unbind('click');

                $('.edit_result').on('click', function () {
                    var id = $(this).attr('rowId');
                    location.href = '/sellerpc/healthTest/module/resultEdit?moduleId=' + moduleId + '&resultId=' + id;
                });
                $('.edit_product').on('click', function () {
                    resultId = $(this).attr('rowId');
                    $productDataTables.search('').draw();
                    // $productModal.modal('show');
                    $('#modal_products').modal('show');
                });

                /** 点击删除部门弹出框 **/
                $("[data-toggle='popover']").popover({
                    trigger: 'manual',
                    placement: 'left',
                    html: 'true',
                    animation: true,
                    content: function () {
                        var rowId = $(this).attr("rowId");
                        return '<span>确认删除？</span>' +
                            '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" rowId="' + rowId + '">确认</button>' +
                            '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                    }
                });

                $('[data-toggle="popover"]').popover() //弹窗
                    .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                        $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                    }).on('shown.bs.popover', function () {
                    var that = this;
                    $('.popover-btn-ok').on("click", function () {
                        var id = $(this).attr("rowId");
                        deleteResult(id);
                    });
                    $('.popover-btn-cancel').on("click", function () {
                        $(that).popover("hide");
                    });
                });

                $('body').on('click', function (event) {
                    var target = $(event.target);
                    if (!target.hasClass('popover') //弹窗内部点击不关闭
                        && target.parent('.popover-content').length === 0
                        && target.parent('.popover-title').length === 0
                        && target.parent('.popover').length === 0
                        && target.data("toggle") !== "popover") {
                        //弹窗触发列不关闭，否则显示后隐藏
                        $('[data-toggle="popover"]').popover('hide');
                    } else if (target.data("toggle") === "popover") {
                        target.popover("toggle");
                    }
                });
            },
            initSelectProductEvent: function () {
                $(".selectproduct").on("click", function () {
                    var productId = $(this).attr('rowId');
                    utils.postAjax(bindProductUrl, {resultId: resultId, productId: productId}, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                $productDataTables.search('').draw();
                                utils.tools.alert('添加成功', {timer: 1200, type: 'success'});
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                });
            }
        }
    })();

    manager.initGlobal();

    /** 初始化表格数据 **/
    const $resutlDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                moduleId: moduleId
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo);
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    return row.moduleName;
                }
            },
            {
                width: '20px',
                orderable: false,
                render: function (data, type, row) {
                    var physiqueId = row.physiqueId;
                    return physiqueId ? '体质测试' : '基础测试';
                }
            },
            {
                width: '80px',
                orderable: false,
                render: function (data, type, row) {
                    return row.start + ' - ' + row.end;
                }
            },
            {
                width: '50',
                orderable: false,
                render: function (data, type, row) {
                    var description = row.description;
                    var maxLength = 20;
                    if (description) {
                        if (description.length > 20) {
                            description = description.substring(0, maxLength);
                        }
                    } else {
                        return '无';
                    }
                    return description;
                }
            },
            {
                width: '100',
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt === null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
                sClass: "right",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit_result" rowId="' + row.id + '" fid="edit_result"><i class="icon-pencil7"></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del_result" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" fid="delete_result"><i class="icon-trash"></i>删除</a>';
                    html += '<a href="javascript:void(0);" class="edit_product" style="margin-left: 10px;"  rowId="' + row.id + '" fid="edit_product"><i class="icon-pencil7"></i>推荐商品</a>';
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    var $productDataTables = $('#xquark_list_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(listProductsUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                resultId: resultId
            }, function(res) {
                if (!res.data && !res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                title: '商品',
                data: "name",
                width: "120px",
                orderable: false,
                name:"name"
            }, {
                title: '状态',
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.status)
                    {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '价格',
                data: "price",
                width: "50px",
                orderable: true,
                name:"price"
            }, {
                title: '库存',
                data: "amount",
                orderable: true,
                width: "50px",
                name:"amount"
            },
            {
                title: '销量',
                data: "sales",
                orderable: true,
                width: "50px",
                name:"sales"
            },{
                title: '发布时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"onsaleAt"
            },{
                title: '管理',
                orderable: false,
                width: '50px',
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="del" rowId="' + row.id + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            $('.del').on('click', function () {
                var productId = $(this).attr('rowId');
                var data = {
                    productId: productId,
                    resultId: resultId
                };
                utils.tools.confirm('确认删除吗', function () {
                    utils.postAjax(unbindProductUrl, data, function (res) {
                        if (res === -1) {
                            utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                            if (res.errorCode === 200) {
                                utils.tools.alert('删除成功', { timer: 1200, type: 'success' });
                                $productDataTables.search('').draw();
                            } else {
                                if (res.moreInfo) {
                                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                } else {
                                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                                }
                            }
                        }
                    });
                }, function () {

                });
            });
        }
    });

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get(listProducts, {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                    if (order !== '') {
                        return order;
                    } else {
                        var _index = data.order[0].column;
                        if (_index < 4) {
                            return '';
                        } else {
                            return $orders[_index - 4];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
            }, function (res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else {
                    if (res.data.shopId) {
                        shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.categoryTotal,
                    recordsFiltered: res.data.categoryTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.categoryTotal,
                    iTotalDisplayRecords: res.data.categoryTotal
                });
            });
        },
        rowId: "id",
        columns: [
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
                }
            },
            {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
            }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="' + row.id + '" productImg="' + row.imgUrl + '" productPrice="' + row.price + '" productName="' + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            manager.initSelectProductEvent();
        }
    });

    function deleteResult(id) {
        utils.postAjaxWithBlock($(document), deleteUrl, {id: id}, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        utils.tools.alert('操作成功', { timer: 1200, type: 'success' });
                        $resutlDataTables.search('').draw();
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res === 0) {

            } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    $(".btn-search-products").on('click', function() {

        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null){
            listProducts = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
            $selectproductdatatables.search( keyword ).draw();
        }else if (keyword == '' || keyword.length == 0 ){
            listProducts = window.host + "/product/list";
            $selectproductdatatables.search('').draw();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});

define('healthTest/result_edit',['jquery', 'utils', 'form/validate', 'jquerySerializeObject', 'datatables', 'blockui', 'select2', 'ckEditor'], function ($, utils, validate) {

    const prefix = window.host + '/healthTest';
    const physiqueListUrl = prefix + '/physique/list';
    const saveUrl = prefix + '/module/saveResult';

    // 体质测试类型DIV
    const $physiqueDiv = $('#physiqueDiv');
    const $physiqueSelector = $('#physiqueSelector');

    const $editor = $('#editor');
    const $editorWrapper = $('.editorWrapper');

    // 体质测试缓存
    var physiqueList = null;
    var hasInit = false;

    const formProfile = {
        debug: true,
        rules: {
            type: 'required',
            start: 'required',
            end: 'required',
            description: 'required'
        },
        messages: {
            type: {
                required: '请选择类型'
            },
            start: {
                required: '请输入开始范围'
            },
            end: {
                required: '请输入结束范围'
            },
            description: {
                required: '请输入体质描述'
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            console.log(form);
            manager.submitForm(form);
        }
    };

    const manager = (function () {

        const $questionTypeSelect = $('#questionType');
        const $returnBtn = $('.btn-return');
        const $form = $('.result-form');

        const typeHandler = {
            BASIC: function () {
                $physiqueSelector.removeAttr('name');
                $editor.attr('name', 'description');
                $editorWrapper.show();
                $physiqueDiv.hide();
            },
            PHYSIQUE: function () {
                if (physiqueList) {
                    showPhysique();
                } else {
                    utils.postAjaxWithBlock($(document), physiqueListUrl, null, function (res) {
                        if (typeof(res) === 'object') {
                            switch (res.errorCode) {
                                case 200: {
                                    if (!res.data) {
                                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                                        return;
                                    }
                                    if (!res.data.list) {
                                        utils.tools.alert('体质类型未配置，请先配置体质类型', {timer: 1200, type: 'warning'});
                                        return;
                                    }
                                    physiqueList = res.data.list;
                                    showPhysique();
                                    break;
                                }
                                default: {
                                    utils.tools.alert(res.moreInfo, {timer: 1200});
                                    break;
                                }
                            }
                        } else if (res === 0) {

                        } else if (res === -1) {
                            utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                        }
                    });
                }
            }
        };
        const globalInstance = {
            bindEvent: function () {
                $questionTypeSelect.change(function () {
                    var type = $(this).val();
                    // 如果选择了体质测试则显示体质测试类型
                    // 选择了基本类型则隐藏体质测试
                    typeHandler[type]();
                });
                $returnBtn.on('click', function () {
                    var moduleId = $('input[name=moduleId]').val();
                    var url = '/sellerpc/healthTest/module/result';
                    if (moduleId && moduleId !== '') {
                        url += '?id=' + moduleId;
                    }
                    location.href = url;
                });
                return this;
            },
            initEditor: function () {
                $editor.ckeditor();
                return this;
            },
            initValidate: function () {
                validate($form, formProfile);
                return this;
            }
        };

        return {
            initGlobal: function () {
                globalInstance.bindEvent()
                    .initEditor()
                    .initValidate();
            },
            initTable: function () {

            },
            submitForm: function (form) {
                var data = $(form).serializeObject();
                utils.getJson(saveUrl, data, function (res) {
                    if (res.errorCode === 200) {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            // 修改问题后跳转
                            // 添加问题后重置
                            $form[0].reset();
                        } else {
                            utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                    }
                });
            }
        }
    })();

    manager.initGlobal();

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /**
     * 显示体质类型
     */
    function showPhysique() {
        $physiqueDiv.show();
        $editor.removeAttr('name');
        $editorWrapper.hide();
        $physiqueSelector.attr('name', 'physiqueId');
        if (physiqueList && !hasInit) {
            physiqueList.forEach(function (item) {
                var value = item.id;
                var text = item.name;
                var option = '<option value="' + value + '">' + text + '</option>';
                $physiqueSelector.append($(option));
            });
            hasInit = true;
        }
    }

});

require(['all']);

require(['healthTest/result_list', 'healthTest/result_edit']);

define("healthTestResult", function(){});

