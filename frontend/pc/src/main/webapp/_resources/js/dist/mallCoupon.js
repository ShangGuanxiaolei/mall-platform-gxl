/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('mall/coupon/addCoupon',['jquery', 'utils', 'jquerySerializeObject', 'switch', 'uniform', 'validate', 'daterangepicker_v2'], function ($, utils) {

    let isEdit=false;
    const limitedOption = $("input[name=limited]");
    const batchNoOption = $("input[name=batch_no]");
    const amountOption = $("input[name=amount]");
    const validOption=$("input[name=valid_type]");
    const validTimeType = $("input[name=valid_type][value=valid_time]");
    const validDayType = $("input[name=valid_type][value=valid_day]");
    const discountOption = $("input[name=discount]");
    const validDay = $("input[name=valid_day]");
    const validFrom = $("input[name=valid_from]");
    const validTo = $("input[name=valid_to]");
    const modal = $(".add-coupon-modal");
    const validDayBlock = "valid_day";
    const validRangeBlock = "valid_time";
    const couponForm = $("#coupon");
    const couponSubmitButton = $("#submit-coupon");
    const datePicker = $(".daterange-basic");
    const singleDatePicker = $(".daterange-single");
    const STATUS_VALID = 'VALID';
    //-----------
    const urlPrefix = '/bos';
    const createUrl = `${urlPrefix}/couponManager/create`;
    const loadUrl = `${urlPrefix}/couponManager/load`;

    let functionModule = (function () {
        return {
            submitCoupon: function (coupon) {
                $.post(createUrl, coupon).done(date => {
                    utils.tools.success("创建成功!");
                }).fail(data => {
                    utils.tools.error("创建出错!");
                });
            },
            editCoupon:function(id){
                $.get(loadUrl,{couponId:id}).done(date=>{
                    isEdit=true;
                    let coupon=date.coupon;
                    //命名转换下
                    coupon.apply_above=coupon.applyAbove;
                    utils.tools.syncForm(couponForm,date.coupon);
                    let picker=singleDatePicker.data('daterangepicker');
                    picker.setStartDate(new Date(coupon.distributedAt));
                    picker.setEndDate(new Date(coupon.distributedAt));
                    picker.updateFormInputs();
                    picker=datePicker.data('daterangepicker');
                    picker.setStartDate(new Date(coupon.validFrom));
                    picker.setEndDate(new Date(coupon.validTo));
                    validTimeType.prop("checked",true);
                    $.uniform.update();
                    modal.modal("show");
                }).fail(data=>{
                    utils.tools.error("获取出错!");
                });
            },
            switchBlock:function(type){
                if (type === validDayBlock) {
                    $(`#${validDayBlock}`).css("display", 'block');
                    $(`#${validRangeBlock}`).css("display", 'none');
                } else {
                    $(`#${validDayBlock}`).css("display", 'none');
                    $(`#${validRangeBlock}`).css("display", 'block');
                }
            }
        }
    })();

    let eventInitialization = (function () {
        return {
            initSwitch: function () {
                $(".switch").bootstrapSwitch();
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });
                return this;
            },
            initDatePicker: function () {
                let dateOption = {
                    timePicker: false,
                    startDate: moment(),
                    endDate: moment().subtract(0, 'month').endOf('month'),
                    locale: {
                        format: 'YYYY/MM/DD',
                        separator: ' - ',
                        applyLabel: '确定',
                        startLabel: '开始日期:',
                        endLabel: '结束日期:',
                        cancelLabel: '清空',
                        weekLabel: 'W',
                        customRangeLabel: '日期范围',
                        daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                        monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                        firstDay: 6
                    },
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small btn-default',
                    isInvalidDate: function (date) {
                        return !date.isAfter(moment());
                    },
                };
                datePicker.daterangepicker(dateOption);
                dateOption.singleDatePicker = true;
                singleDatePicker.daterangepicker(dateOption);
                return this;
            },
            initFormSwitch: function () {
                $('.switch').on('switchChange.bootstrapSwitch', function (e, data) {
                    if (!data) {
                        amountOption.attr("readonly", "readonly");
                    } else {
                        amountOption.removeAttr("readonly");
                    }
                });
                return this;
            },
            initValidSwitch: function () {
                validOption.on("click", function () {
                    let type = $(this).attr("value");
                    functionModule.switchBlock(type);
                });
                return this;
            },
            resetForm: function () {
                modal.on('hidden.bs.modal', function (e) {
                    couponForm[0].reset();
                    couponForm.validate().resetForm();
                });
                return this;
            },
            getBatchNo: function () {
                modal.on('show.bs.modal', function (e) {
                    if(!isEdit){
                        $.get(loadUrl).done((data) => {
                            batchNoOption.prop("value", data.batchNo);
                        }).fail((data => {
                            utils.tools.error("获取批次号出错");
                        }));
                    }else{
                        isEdit=false;
                    }
                });
                return this;
            },
            initSubmit: function () {
                couponSubmitButton.on("click", function () {
                    let result = couponForm.valid();
                    if (result) {
                        console.log("success valid");
                        let coupon = couponForm.serializeObject();
                        if (validRangeBlock === coupon.valid_type) {
                            let times = coupon.valid_range.split("-");
                            coupon.valid_from = times[0].trim();
                            coupon.valid_to = times[1].trim();
                        }
                        coupon.total_discount = coupon.discount * coupon.amount;
                        coupon.statu = STATUS_VALID;
                        //兼容性数据项
                        coupon.shop_select = '';
                        coupon.category_selelct = '';
                        coupon.product_selelct = '';
                        functionModule.submitCoupon(coupon);
                    }
                });
                return this;
            },
            initValidate: function () {
                couponForm.validate({
                    errorClass: 'validation-error-label',
                    successClass: 'validation-valid-label',
                    highlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    unhighlight: function (element, errorClass) {
                        $(element).removeClass(errorClass);
                    },
                    validClass: "validation-valid-label",
                    success: function (label) {
                        label.addClass("validation-valid-label").text("");
                    },
                    //copy-from-limitless/form_validation.html
                    errorPlacement: function (error, element) {
                        // Styled checkboxes, radios, bootstrap switch
                        if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                            if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                                error.appendTo(element.parent().parent().parent().parent());
                            }
                            else {
                                error.appendTo(element.parent().parent().parent().parent().parent());
                            }
                        }
                        else if (element.hasClass('left')) {
                            error.insertAfter(element);
                        }
                        // Unstyled checkboxes, radios
                        else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                            error.appendTo(element.parent().parent().parent());
                        }

                        // Inline checkboxes, radios
                        else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent());
                        }

                        // Input group, styled file input
                        else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                            error.appendTo(element.parent().parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    },
                    rules: {
                        name: "required",
                        batch_no:"required",
                        discount: {
                            required: true,
                            number: true,
                            max: 500,
                            min: 1,
                        },
                        apply_above: {
                            required: true,
                            number: true,
                            min: 1,
                            checkApplyAbove: true,
                        },
                        amount: {
                            required: {
                                depends: function () {
                                    return limitedOption.prop("checked");
                                }
                            },
                            number: true,
                            min: 1,
                        },
                        acquire_limit: {
                            required: true,
                            number: true,
                            min: 1,
                        },
                        valid_day: {
                            required: {
                                depends: function () {
                                    return validTimeType.prop("checked") === false;
                                },
                            },
                            number: true,
                            min: 1,
                        },
                        valid_range: {
                            required: {
                                depends: function () {
                                    return validTimeType.prop("checked") === true;
                                },
                            },
                        },
                        notify_before: {
                            required: true,
                            number: true,
                            min: 1
                        },
                        description: {
                            maxlength: 200,
                        },
                        distributed_at:{
                            required:true,
                        }

                    },
                    messages: {
                        batch_no:"必须填写批次号",
                        name: "请输入名称",
                        discount: {
                            required: "请输入面值",
                            number: "请输入整数",
                            min: "最低面值为1",
                            max: "最高面值为500"
                        },
                        apply_above: {
                            required: "请输入订单金额",
                            number: "请输入整数",
                            min: "金额不得低于1",
                        },
                        amount: {
                            required: "请输入发行量",
                            number: "请输入整数",
                            min: "数量不得低于1",
                        },
                        acquire_limit: {
                            required: "请输入领取限量",
                            number: "请输入整数",
                            min: "数量不得低于1",
                        },
                        valid_day: {
                            required: "请输入有效期",
                            number: true,
                            min: 1,
                        },
                        valid_from: {
                            required: "",
                            date: "请输入时间类型",
                        },
                        valid_to: {
                            required: "请输入失效时间",
                            date: "请输入时间类型",
                        },
                        notify_before: {
                            required: "请输入提醒时间",
                            number: "请输入整数",
                            min: "数量不得低于1"
                        },
                        description: {
                            maxlength: "描述不得大于200字",
                        },
                        distributed_at:{
                            required:"请输入发行日期",
                        }
                    },
                    submitHandler: function (form) {
                    },
                    invalidHandler: function (event, validator) {
                        let errors = validator.numberOfInvalids();
                        console.log(`error count:${errors}`);
                    }
                });
                $.validator.addMethod("checkApplyAbove", function (value, element) {
                    let discount = Number.parseInt(discountOption.prop("value"));
                    return this.optional(element) || Number.parseInt(value) > discount;
                }, "使用上限不能小于折扣");
                return this;
            }
        }

    })();


    return {
        init:function(){
            eventInitialization.initSwitch().initFormSwitch().initValidSwitch().initValidate().initSubmit().initDatePicker().resetForm().getBatchNo();
        },
        edit:functionModule.editCoupon,
    }

});

define('mall/coupon/listCoupon',['jquery', 'utils', 'datatables','mall/coupon/addCoupon','select2'], function ($, utils, datatabels,addCoupon,select2) {

    $dataTable = '';
    const tableId = "#couponTable";
    const couponSelect = $('#coupon-status');
    const couponName = $('#coupon-name');
    const couponRange = $('#coupon-range');
    const kvArray = [['VALID', '可使用'], ['OVERDUE', '已过期'], ['LOCKED', '已有订单使用'], ['USED', '已使用'], ['CLOSED', '关闭']];
    const statusCache = new Map(kvArray);
    const searchButton = $("#search");
    //-------------------
    const urlPrefix = '/bos';
    const listUrl = `${urlPrefix}/couponManager/list`;


    let eventInitialization = function () {
        return {
            initDatePicker: function () {
                let dateOption = {
                    timePicker: false,
                    startDate: moment(),
                    endDate: moment().subtract(0, 'month').endOf('month'),
                    locale: {
                        format: 'YYYY/MM/DD',
                        separator: ' - ',
                        applyLabel: '确定',
                        startLabel: '开始日期:',
                        endLabel: '结束日期:',
                        cancelLabel: '清空',
                        weekLabel: 'W',
                        customRangeLabel: '日期范围',
                        daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                        monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                        firstDay: 6
                    },
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small btn-default',
                };
                couponRange.daterangepicker(dateOption);
                couponRange.val('');
                /**
                 * 清空按钮清空选框
                 */
                couponRange.on('cancel.daterangepicker', function(ev, picker) {
                    couponRange.val('');
                });

                return this;
            },
            initSearch: function () {
                searchButton.on("click", function () {
                    $dataTable.search('').draw();
                });
                return this;
            },
            initSelect: function () {
                couponSelect.select2({
                    minimumResultsForSearch: Infinity,
                });
                return this;
            },
            initDataTable: function () {
                /** 页面表格默认配置 **/
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>筛选:</span> _INPUT_',
                        lengthMenu: '<span>显示:</span> _MENU_',
                        info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                        paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                        infoEmpty: "",
                        emptyTable: "暂无相关数据"
                    }
                });
                $dataTable = $(tableId).DataTable({
                    paging: true, //是否分页
                    filter: false, //是否显示过滤
                    lengthChange: false,
                    processing: false,
                    serverSide: true,
                    deferRender: false,
                    searching: false,
                    ordering: false,
                    sortable: false,
                    ajax: function (data, callback, settings) {
                        $.post(listUrl, {
                            rows: data.length,
                            page: function () {
                                let page = (data.start / data.length);
                                return page + 1;
                            },
                            status_kwd: couponSelect.val(),
                            name_kwd: couponName.prop("value"),
                            valid_from_kwd: function () {
                                let range = couponRange.prop("value");
                                if (range) {
                                    return range.split("-")[0].trim();
                                }
                                return '';

                            },
                            valid_to_kwd: function () {
                                let range = couponRange.prop("value");
                                if (range) {
                                    return range.split("-")[1].trim();
                                }
                                return '';
                            },
                            pageable: true,
                        }, function (res) {
                            if (!res.rows) {
                                res.rows = [];
                            }
                            callback({
                                recordsTotal: res.total,
                                recordsFiltered: res.total,
                                data: res.rows,
                                iTotalRecords: res.total,
                                iTotalDisplayRecords: res.total
                            });
                        });
                    },
                    rowId: "id",
                    columns: [
                        {
                            width: "10px",
                            orderable: false,
                            render: function (data, type, row) {
                                return `<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="${row.id}" value="${row.id}"></label>`;
                            }
                        },
                        {
                            data: "name",
                            orderable: false,
                            title: "优惠券名称"
                        },
                        {
                            data: "batchNo",
                            orderable: false,
                            title: "批次号"
                        },
                        {
                            data: "distributedAt",
                            orderable: false,
                            title: "发行日期",
                            render: function (date, type, row) {
                                let data = parseInt(row.distributedAt);
                                let d = new Date(data);
                                return d.format('yyyy-MM-dd');
                            }
                        },
                        {
                            data: "amount",
                            orderable: false,
                            title: "总数"
                        },
                        {
                            data: "discount",
                            orderable: false,
                            title: "面值"
                        },
                        {
                            data: "remainder",
                            orderable: false,
                            title: "剩余"
                        },
                        {
                            data: "totalDiscount",
                            orderable: false,
                            title: "总金额"
                        },
                        {
                            data: "validFrom",
                            orderable: false,
                            render: function (date, type, row) {
                                let from = parseInt(row.validFrom);
                                let to = parseInt(row.validTo);
                                return new Date(from).format('yyyy-MM-dd') + '~' + new Date(to).format('yyyy-MM-dd');
                            },
                            title: "时效"
                        },
                        {
                            data: "status",
                            orderable: false,
                            render: function (date, type, row) {
                                return statusCache.get(row.status);
                            },
                            title: "状态"
                        },
                        {
                            title: '操作',
                            orderable: false,
                            render: function (data, type, row) {
                                let html = '';
                                html += `<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="${row.id}" ><i class="icon-pencil7" ></i>编辑</a>`;
                                html += `<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="${row.id}" ><i class="icon-trash"></i>使失效</a>`;
                                return html;
                            }
                        }
                    ],
                    drawCallback: function () {  //数据加载完成回调
                        $(".edit").on("click",function(){
                            let id=$(this).attr("rowId");
                            addCoupon.edit(id);
                        });
                    }
                });
                return this;
            },
        }

    }();

    return {
        init: function () {
          eventInitialization.initSelect().initDataTable().withSearchEnable().initDatePicker();
        },
    }

});

/**
 * Created by jitre on 18/4/17.
 */
require(['all']);

//module
require(['mall/coupon/addCoupon','mall/coupon/listCoupon'],function(add,list){
    list.init();
    add.init();
});
define("mallCoupon", function(){});

