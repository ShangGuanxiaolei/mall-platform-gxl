/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by chh on 16/12/09.
 */
define('utils', ['jquery', 'sweetAlert', 'blockui'], function () {


    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }


    var utils = {
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(/\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            confirm: function (sMsg, fnConfirm, fnCancel) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0)
                                str_cut = str_cut.concat(more);
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            }
        }
    };
    return utils;

});
/**
 * Created by chh on 16/12/09.
 */

//公共组件定义
require(['utils']);

//登出
define('all', ['jquery', 'utils'], function (jquery, utils) {
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            utils.logout();
        });
    });
});
define('agent/founder', ['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'tree', 'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform, daterangepicker, moment, tree) {

        var userId = '';
        var parentName = '';

        buttonRoleCheck('.hideClass');

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无待审核下级"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        // function ()
        var twitterTreeManager = {
            /**jstree*/
            $container: $('#twitterTreeManagerContainer'),
            // currentNode: null,
            root: {},
            eachPageSize: 15,
            createIcon: function (child) {
                var role = child['role'].toLowerCase();
                return '<div class="custom">' +
                    '<img src="' + child['avatar'] + '" alt="" class="user-icon">' +
                    '<em class="' + role + '">' + child['name'] + '</em>' +
                    '&nbsp;&nbsp;<em style="color: red">' + child['count'] + '</em>' +
                    '</div>'
            },
            config: {
                "core": {
                    "animation": 200,
                    "check_callback": true,
                    "themes": {"stripes": false, "dots": false, "responsive": true, "icons": false},
                    'strings': {
                        'Loading ...': '正在加载...'
                    },
                    'data': {
                        'url': function (node) {
                            return window.host + '/userAgent/getDirectChildren'
                        },
                        'type': 'GET',
                        'data': function (node) {
                            //TODO 设置Root节点
                            console.log(node)
                            console.log(node.id)
                            console.log($.jstree.root)
                            var id = node.id === $.jstree.root ? twitterTreeManager.root.id : node.id;
                            node.page = node.page ? node.page : 0;
                            return {
                                userId: id,
                                page: node.page,
                                size: twitterTreeManager.eachPageSize
                            }
                        },
                        'dataFilter': function (data) {
                            var dataInfo = JSON.parse(data).data;
                            console.log(JSON.parse(data))
                            if (JSON.parse(data).data) {
                                var total = JSON.parse(data).data.total,
                                    parent = JSON.parse(data).data.parent,
                                    list = JSON.parse(data).data.list;
                            } else alert('数据格式有误')
                            console.log(total);
                            console.log(parent);
                            console.log(list);
                            function renderChildData() {
                                var tmp = [];
                                list.forEach(function (value) {
                                    tmp.push({
                                        'id': value.id,
                                        'text': twitterTreeManager.createIcon(value), // 用户姓名
                                        'state': {'opened': false, 'selected': false},
                                        'children': !value.isLeaf
                                    })
                                });
                                if (total > 15) {
                                    var node = twitterTreeManager.tree.get_node(parent.id);
                                    var maxPage = parseInt(total / 15);
                                    node.maxPage = maxPage;
                                    if (node && node.page) {
                                        var page = node.page + 1;
                                        node.maxPage = maxPage;
                                    } else {
                                        page = 1;
                                        twitterTreeManager.root.maxPage = maxPage;
                                    }
                                    console.log(twitterTreeManager.tree.get_node(parent.id))
                                    tmp.push({
                                        'id': parent.id + "pagingBtn",
                                        'text': '<div class="flip-page">' +
                                        '<i class="previousPage">上页</i>' +
                                        '<em class="nextPage">下页</em>' +
                                        '<span>第' + (node.page + 1) +
                                        '页</span>' +
                                        '</div>',
                                        'state': {'opened': false, 'selected': false},
                                        'children': false
                                    })
                                }
                                return tmp
                            }

                            if (parent && parent.id) {
                                if (parent.id === twitterTreeManager.root.id && !twitterTreeManager.tree.get_node(parent.id)) {
                                    //根节点
                                    console.log('根节点')
                                    return JSON.stringify([{
                                        'id': parent.id,
                                        'text': twitterTreeManager.createIcon(parent), // 用户姓名
                                        'state': {'opened': false, 'selected': false},
                                        'children': !!(list.length)
                                    }])
                                } else {
                                    if (list.length != 0) {
                                        console.log('子节点')
                                        return JSON.stringify(renderChildData())
                                    }
                                }
                            }
                        }
                    }
                },
                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 6,
                    }
                },
                "plugins": [
                    "contextmenu", "search",
                    "state", "types", "sort", "Wholerow", "json_data"
                ]
            },

            init: function (userId) {
                this.root.id = userId;
                this.root.page = 0;
                this.$container
                //初始化时绑定需要处理的事件
                    .on('click.jstree', function (e) {
                        var className = e.target.getAttribute('class');
                        var tree = twitterTreeManager.tree;
                        if (className && className.match('previousPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if (parentNode.page > 0) {
                                parentNode.page = parentNode.page - 1;
                                tree.load_node(parentNode)
                            }
                        } else if (className && className.match('nextPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if (parentNode.page < parentNode.maxPage) {
                                parentNode.page = parentNode.page + 1;
                                tree.load_node(parentNode);
                                console.log(parentNode);
                            }
                        }

                        // 点击左侧缩放小箭头时不需要查看详情
                        var eventNodeName = e.target.nodeName;
                        if (eventNodeName == 'IMG' || eventNodeName == 'EM') {
                            var id = $(e.target).parents('li').attr('id');
                            if (id.indexOf('pagingBtn') == -1) {
                                showDetail(id);
                            }
                        }
                    })
                    //执行初始化
                    .jstree(this.config);
                this.tree = this.$container.jstree(true);
            },

            clear: function () {
                this.$container.jstree('destroy');
                delete twitterTreeManager.tree;
                // delete twitterTreeManager
            }
        };

        function showDetail(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/userId/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            $("#name").html(res.data.name);
                            $("#weixin").html(res.data.weixin);
                            $("#idcard").html(res.data.idcard == '' ? '无' : res.data.idcard);
                            $("#certNum").html(res.data.certNum);
                            $("#startDate").html(res.data.startDate);
                            $("#endDate").html(res.data.endDate);
                            $("#agentTypeName").html(res.data.agentTypeName);
                            $("#modal_cert_manager").modal('show');
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("查看详情失败", {timer: 1200});
                }
            });
        }

        function showApplyingChildren(name, callBack) {
            $.get(window.host + "/userAgent/apply/list", {
                status: 'APPLYING',
                parentName: name
            }, function (res) {
                var list = res.data.list;
                callBack(list);
            }).fail(function () {
                utils.tools.alert("查找未审核下级失败", {timer: 1200});
            });
        }

        /* 生成未申请用户的节点 */
        function generateApplyingDOM(listResult) {
            var listGroup = $('#listGroup');
            // 清除用户列表
            listGroup.empty();
            if (!listResult || listResult.length === 0) {
                return;
            }
            var out = listResult.map(function (result) {
                return '<li class="media-header"></li><li class="media">' + result.name + '<a href="javascript:void(0);" aid="' + result.id + '" class="deleteBtn pull-right deleteApplying" fid="delete_applying">删除申请</a>' + '</li>'
            }).join('');
            listGroup.append('<div class="panel-heading"><h6 class="panel-title">待审核下级代理</h6></div>');
            listGroup.append('<div style="margin: 35px 35px"><ul class="media-list media-list-bordered">' + out + '</ul></div>');

            $('.deleteApplying').on('click', function () {
                var id = $(this).attr('aid');
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                // 删除后重新请求数据并生成
                                showApplyingChildren(parentName, generateApplyingDOM);
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            })
        }

        // 当前登陆用户的角色，如果为客户管理员角色，则需要隐藏某些功能
        var roles = $("#roles").val();

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering: false,
            ajax: function (data, callback, settings) {
                $.get(window.host + "/userAgent/listFounder", {
                    size: data.length,
                    page: (data.start / data.length),
                    phone: $("#phone").val(),
                    name: $("#agentName").val(),
                    pageable: true,
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            columns: [
                {
                    width: '75px',
                    title: '代理姓名',
                    data: 'name',
                    name: 'name',
                    sortable: false
                }, {
                    width: 100,
                    sClass: 'styled text-center sorting_disabled',
                    title: "代理信息",
                    sortable: false,
                    render: function (data, type, row) {
                        if ('CUSTOMER' == roles) {
                            return row.phone;
                        } else {
                            return '<a href="#" user-id="' + row.id + '" class="twitterChildrenBtn" data-toggle="modal" >' + row.phone + ' <i class="icon-search4"></i></a>'
                                + '&nbsp;<a href="#" user-name="' + row.name + '" user-id="' + row.userId + '" class="twitterChildrenTreeManager" data-toggle="modal" ><i class="icon-tree6"></i></a>';
                        }
                    }
                }, {
                    width: '100px',
                    title: '身份证号',
                    data: 'idcard',
                    name: 'idcard',
                    sortable: false
                }, {
                    width: '75px',
                    title: '上级名称',
                    data: 'parentVo.name',
                    name: 'parentVo.name',
                    sortable: false
                },
                {
                    width: 180,
                    data: 'areaName',
                    name: 'areaName',
                    sortable: false,
                    title: '封地区域'
                },
                {
                    width: 75,
                    title: '加入时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt == null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                }, {
                    width: 75,
                    data: 'status',
                    name: 'status',
                    sortable: false,
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if (value == 'ACTIVE') {
                            return "已审核";
                        } else if (value == 'FROZEN') {
                            return "已冻结";
                        }
                    }
                }, {
                    width: 150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    sortable: false,
                    title: '操作',
                    render: function (data, type, row) {
                        return '<a href="javascript:void(0);" class="changeAreaBtn" userId="' + row.userId + '" style="margin-left: 3px" rowId="' + row.id + '" >修改封地区域</a>';
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        // 选择角色表单
        var $applyingTables = $('#xquark_applying_tables').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(window.host + "/userAgent/apply/list", {
                    parentName: parentName,
                    status: 'APPLYING',
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else {
                        // if (res.data.shopId) {
                        //     $shopId = res.data.shopId;
                        // }
                    }
                    callback({
                        recordsTotal: res.data.applyTotal,
                        recordsFiltered: res.data.applyTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.applytotal,
                        iTotalDisplayRecords: res.data.applyTotal
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name: "name"
                }, {
                    data: "phone",
                    width: "120px",
                    orderable: false,
                    name: "phone"
                }
                , {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="deleteApplying" class="deleteApplying" aid="' + row.id + '" data-toggle="popover"><i class="icon-trash" ></i>删除</a>';
                        return html;
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                $('.deleteApplying').on('click', function () {
                    var id = $(this).attr('aid');
                    utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                        if (typeof(res) === 'object') {
                            switch (res.errorCode) {
                                case 200: {
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    // 删除后重新请求数据并生成
                                    // showApplyingChildren(parentName, generateApplyingDOM);
                                    $applyingTables.search('').draw();
                                    break;
                                }
                                default: {
                                    utils.tools.alert(res.moreInfo, {timer: 1200});
                                    break;
                                }
                            }
                        } else if (res == 0) {
                        } else if (res == -1) {
                            utils.tools.alert("删除失败", {timer: 1200});
                        }
                    });
                })
            }
        });


        // 查看详情
        $(document).on('click', '.twitterChildrenBtn', function () {

            var id = $(this).attr("user-id");
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            $("#view_id").val(res.data.id);
                            $("#view_name").val(res.data.name);
                            $("#view_weixin").val(res.data.weixin);
                            $("#view_phone").val(res.data.phone);
                            //$("#view_email").val(res.data.email);
                            $("#view_idcard").val(res.data.idcard);
                            $("#view_address").val(res.data.address);
                            $("#view_type").val(res.data.type);

                            // $("#life_img").attr('src', res.data.lifeImg);
                            // $("#idcard_img").attr('src', res.data.idcardImg);
                            initFileInput('lifeCard', res.data.id, res.data.lifeImg);
                            initFileInput('idCard', res.data.id, res.data.idcardImg);

                            var addressId = res.data.address;
                            $.ajax({
                                url: 'http://' + window.location.host + '/v2/address/api/' + addressId,
                                method: 'POST',
                                success: function (data) {
                                    console.log(data);
                                    renderingData(data);
                                },
                                error: function (err) {
                                    console.log('error ')
                                }
                            });

                            //$("#view_idcard_img").attr("src",res.data.idcardImgUrl);
                            //$("#view_life_img").attr("src",res.data.lifeImgUrl);
                            $("#modal_viewDetail").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("查看详情失败", {timer: 1200});
                }
            });

            //$('#modal_twitterChildrenInfo').modal('show');
            //$twitterChildrenTables.search(userId).draw();
        });


        $(document).on('click', '.twitterChildrenTreeManager', function () {
            userId = $(this).attr("user-id");
            parentName = $(this).attr('user-name');
            twitterTreeManager.init(userId);//初始化树
            // 刷新未审核代理表格
            $applyingTables.search('').draw();
            // 显示审核未通过下级代理
            // showApplyingChildren(parentName, generateApplyingDOM);
            $('#modal_tree_manager').modal('show');
        });

        $("#modal_tree_manager").on('hidden.bs.modal', function () {
            twitterTreeManager.clear();
        });

        $('.exportTwitterChildrenBtn').on('click', function () {
            if (userId != null) {
                $('#exportTwitterChildrenForm input[name="userId"]').val(userId);
                $('#exportTwitterChildrenForm').submit();
            }
        });

        function initEvent() {
            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');


            tableRoleCheck('#guiderUserTable');
        }

        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });

        $('#searchTwitter4ChangeTree').on('click', function () {
            twitterName4ChangeTree = $('#twitterName4ChangeTree').val();
            twitterPhone4ChangeTree = $('#twitterPhone4ChangeTree').val();
            //alert('把查询的必要条件去掉,否则查不到数据' + '条件:' + twitterName4ChangeTree + ',' + twitterPhone4ChangeTree);
            $guiderUserTable.search('').draw();
        });


        // 初始化上传图片控件
        function initFileInput(name, id, img) {
            var $fileInput = $('#' + name + '-input');
            $fileInput.fileinput('destroy');
            var op = {
                language: 'zh',
                uploadUrl: window.host + '/userAgent/updateMultiPartImg',
                uploadExtraData: {"id": id, "type": name},
                showUpload: false,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                browseIcon: '<i class="icon-file-plus"></i>',
                uploadIcon: '<i class="icon-file-upload2"></i>',
                removeIcon: '<i class="icon-cross3"></i>',
                browseClass: 'btn btn-primary',
                uploadClass: 'btn btn-default',
                removeClass: 'btn btn-danger',
                initialCaption: '',
                maxFilesNum: 1,
                indicatorSuccessTitle: '上传成功',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"],
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    footer: ''
                },

            };
            //如果img有值，则显示之前上传的图片
            if (img && img.toString().indexOf('http') != -1) {
                op = $.extend({
                    showPreview: true,
                    initialPreview: [ // 预览图片的设置
                        "<img src= '" + img + "' class='file-preview-image'>"]
                }, op);
            }

            $fileInput.fileinput(op);
            $fileInput.on('fileselect', function (event, numFiles, label) {
                $fileInput.fileinput('upload');
            });
            // $fileInput.fileinput('enable');
        }

        function renderingData(data) {
            if (data.errorCode === 200 && data.data) {
                var zones = data.data.zones;

                $('#view_detail').val(data.data.street);

                $('#province').html('<option value="' + zones[1].id + '">' + zones[1].name + '</option>')
                $('#city').html('<option value="' + zones[2].id + '">' + zones[2].name + '</option>')
                $('#district').html('<option value="' + zones[3].id + '">' + zones[3].name + '</option>')
                initEditZones(zones[1].id, zones[2].id, zones[3].id);

            } else console.log('数据格式错误');
        }

        function initZones() {
            buildChildrenZone(1, document.getElementById('province'), '选择省份');
            $('#province').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('city'), '选择市');
            });
            $('#city').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
            });

            buildChildrenZone(1, document.getElementById('province_area'), '选择省份');
            $('#province_area').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('city_area'), '选择市');
            });
            $('#city_area').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('district_area'), '选择县区');
            });
        }

        function initEditZones(provinceId, cityId, districtId) {
            buildChildrenZone(1, document.getElementById('province'), '选择省份', function () {
                document.getElementById('province').value = provinceId;
                $('#province').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('city'), '选择市');
                });
            });
            buildChildrenZone(provinceId, document.getElementById('city'), '选择市', function () {
                document.getElementById('city').value = cityId;
                $('#city').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
                });
            });
            buildChildrenZone(cityId, document.getElementById('district'), '选择县区', function () {
                document.getElementById('district').value = districtId;
            });
        }

        function buildChildrenZone(zoneId, insertDom, firstOption, callback) {
            if (zoneId == '0') {
                var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                insertDom.innerHTML = frag;
                if (callback) {
                    callback();
                }
            } else {
                $.ajax({
                    url: 'http://' + window.location.host + '/v2/zone/' + zoneId + '/children',
                    method: 'POST',
                    success: function (data) {
                        if (data.data && data.errorCode) {
                            var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                            data.data.forEach(function (value) {
                                frag = frag + '<option value="' + value.id + '">' + value.name + '</option>';
                            });
                            insertDom.innerHTML = frag;
                            if (callback) {
                                callback();
                            }
                        } else console.log(data.error);
                    },
                    error: function (err) {
                        console.log('error ')
                    }
                })
            }
            if (firstOption == '选择市') {
                var frag = '<option value="' + 0 + '">选择县区</option>';
                document.getElementById('district').innerHTML = frag;
            }

        }


        $(document).on('click', '.changeAreaBtn', function () {
            $("#modal_areaDetail").modal('show');
            changeAgentId = $(this).attr('rowId');
        });

        $(document).on('click', '.updateAreaBtn', function () {
            var area;
            var province_area = document.getElementById('province_area').value;
            var city_area = document.getElementById('city_area').value;
            var district_area = document.getElementById('district_area').value;
            if(district_area && district_area != '0'){
                area = district_area;
            }else if(city_area && city_area != '0'){
                area = city_area;
            }else if(province_area && province_area != '0'){
                area = province_area;
            }else{
                utils.tools.alert("请选择地区!", {timer: 1200, type: 'warning'});
                return;
            }

            utils.postAjaxWithBlock($(document), window.host + '/userAgent/updateArea?id=' + changeAgentId + '&area=' + area, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                            $("#modal_areaDetail").modal('hide');
                            $datatables.search('').draw();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("修改失败", {timer: 1200});
                }
            });

            //$('#modal_twitterChildrenInfo').modal('show');
            //$twitterChildrenTables.search(userId).draw();
        });


        initZones();

        var twitterName4ChangeTree;
        var twitterPhone4ChangeTree;
        var changeAgentId;
        var changeAgentUserId;
        var $guiderUserTable;
        var parentShopName;
    });
//微信账号绑定
require(['all']);

require(['agent/founder']);

define("agentFounder", [],function () {
});


//微信账号绑定
require(['all']);

require(['agent/founder']);

define("agentFounder", function(){});

