/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('agent/whiteList',['jquery', 'utils', 'datatables', 'blockui', 'select2'],
    function ($, utils, datatables, blockUI, select2) {
        const prefix = window.host + '/userAgent';

        const saveUrl = prefix + '/addWhiteList';
        const listUrl = prefix + '/viewWhiteList';
        const removeUrl = prefix + '/removeWhiteList';
        const updateUrl = prefix + '/updateWhiteList';
        const viewUrl = prefix + '/whitelist';

        const columns = ['phone', 'isEnable', 'created_at'];

        const ajax = {
            save: function (data) {
                utils.postAjax(saveUrl, data, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            $datatables.search('').draw();
                            $('#modal_white_list').modal('hide');
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                        } else {
                            utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            view: function (id) {
                utils.postAjax(viewUrl + '/' + id, null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data) {
                            $('#id').val(data.id);
                            $('#phone').val(data.phone);
                            block.unBlock();
                            $('#modal_white_list').modal('show');
                        } else {
                            console.log(data);
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            remove: function (id) {
                utils.postAjax(removeUrl + '/' + id, null, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                            $datatables.search('').draw();
                        } else {
                            console.log(data);
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            },
            update: function (data, callback) {
                utils.postAjax(updateUrl, data, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof res === 'object') {
                        var data = res.data;
                        if (data === true) {
                            $('#modal_white_list').modal('hide');
                            $datatables.search('').draw();
                            if (callback) callback();
                            utils.tools.alert('操作成功', {timer: 1200, type: 'warning'});
                        } else {
                            utils.tools.alert('操作失败', {timer: 1200, type: 'warning'})
                        }
                    }
                })
            }
        };

        const block = {
            fullBlock: function () {
                return $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });
            },
            unBlock: function () {
                return $.unblockUI();
            }
        };

        globalInit();

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                zeroRecords: '暂无相关数据',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "白名单未配置"
            }
        });

        const $datatables = $('#xquark_white_list_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            pageable: true,
            ajax: function (data, callback, settings) {
                $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true,
                    keyword: data.search.value,
                    order: columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (res.errorCode != '200') {
                        utils.tools.alert('数据加载失败');
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            agentId: 'agentId',
            columns: [
                {
                    data: 'phone',
                    width: '100px',
                    orderable: true,
                    name: 'phone'
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.enable ? '已启用' : '已禁用';
                    }
                },
                {
                    width: '80px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt === null) {
                            return '';
                        }
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var enable = row.enable ? '禁用' : '启用';
                        var agentId = row.agentId;
                        var html = '';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="edit_white_list" class="edit_white_list"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="change_white_list" class="change_white_list" style="margin-left: 15px" status="' + row.enable + '"><i class="icon-trash" >' +
                            '</i>' + enable + '</a>';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="delete_white_list" class="delete_white_List" data-toggle="popover" style="margin-left: 15px"><i class="icon-trash" ></i>删除</a>';
                        if (agentId) {
                            html += '<a href="javascript:void(0);" agentId="'
                                + agentId
                                + '" fid="view_agent_detail" class="view_agent_detail" style="margin-left: 15px"><i class="icon-grid-alt" ></i>查看详情</a>';
                        }
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });

        function globalInit() {
            var $modal = $('#modal_white_list');
            $('#add_button').on('click', function () {
                $('#id').val('');
                $('#phone').val('');
                $modal.modal('show');
            });

            $('#confirm_btn').on('click', function () {
                var phone = $('#phone').val();
                var id = $('#id').val();
                if (!checkPhone(phone)) {
                    utils.tools.alert('手机号码格式不正确', {timer: 1200, type: 'warning'});
                    return;
                }
                if (id) {
                    ajax.update({id: id, phone: phone});
                } else {
                    ajax.save({phone: phone})
                }
            });

            buttonRoleCheck('.hideClass');

            $('.btn-search').on('click', function () {
                var keyword = $.trim($("#sKeyword").val());
                // 根据搜索关键字刷新表格
                $datatables.search(keyword).draw();
            })
        }

        function initEvent() {
            $('body').unbind('click');

            $('.edit_white_list').on('click', function () {
                var id = $(this).attr('rowId');
                block.fullBlock();
                ajax.view(id);
            });

            $('.change_white_list').on('click', function () {
                var status = $(this).attr('status');
                var id = $(this).attr('rowId');
                console.log('status: ' + status);
                var data;
                if (status === 'true') {
                    data = {id: id, enable: false};
                } else {
                    data = {id: id, enable: true};
                }
                ajax.update(data);
            });

            $('.view_agent_detail').on('click', function () {
                var agentId = $(this).attr('agentId');
                if (!agentId) {
                    utils.tools.alert('代理信息不存在', {timer: 1200, type: 'warning'});
                    return;
                }
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + agentId, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#view_id").val(res.data.id);
                                $("#view_name").val(res.data.name);
                                $("#view_weixin").val(res.data.weixin);
                                $("#view_phone").val(res.data.phone);
                                //$("#view_email").val(res.data.email);
                                $("#view_idcard").val(res.data.idcard);
                                $("#view_type").val(res.data.type);
                                $("#modal_viewDetail").modal("show");
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });

            });
            /** 点击删除弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var id = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                        + id + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var param = $(this).attr("param");
                    ajax.remove(param);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_white_list_tables');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });
        }

        function checkPhone(num) {
            var regPhone = /^1\d{10}$/;
            return regPhone.test(num);
        }

    }
);

require(['all']);

require(['agent/whiteList']);

define("agentWhiteList", function(){});

