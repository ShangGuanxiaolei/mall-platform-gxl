/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'],function() {


    Date.prototype.format = function(fmt) {
        var o = {
            "M+" : this.getMonth() + 1, // 月份
            "d+" : this.getDate(), // 日
            "h+" : this.getHours(), // 小时
            "m+" : this.getMinutes(), // 分
            "s+" : this.getSeconds(), // 秒
            "q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
            "S" : this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        for ( var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }


    var utils = {
        postAjax: function (url, data, callback){
            $.ajax({
                    url: url,
                    data: data,
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
            });
        },
        getJson: function(url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function(res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function(success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function(state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request : function(param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?")+1,url.length).split(/\&|\#/g);
                var paraObj = {}
                for (i=0; j=paraString[i]; i++){
                    paraObj[j.substring(0,j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=")+1,j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if(typeof(returnValue)=="undefined"){
                    return "";
                }else{
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            confirm: function(sMsg, fnConfirm,fnCancel) {
                swal({
                    title: "确认操作",
                    text: sMsg,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#FF7043",
                    confirmButtonText: "是",
                    cancelButtonText: "否"
                },
                function(isConfirm){
                    if (isConfirm) {
                        fnConfirm();
                    }
                    else {
                        fnCancel();
                    }
                });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function(str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                };
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0)
                                str_cut = str_cut.concat(more);
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

//登出
define('all',['jquery', 'utils'], function(jquery, utils) {
    $('.j_logout').on('click', function() {
        utils.tools.confirm('确认退出当前账户？', function() {
            utils.logout();
        });
    });
});
define('twitter/member',['jquery','utils','datatables','blockui','bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'tree', 'fileinput_zh', 'fileinput', 'datepicker'],
    function($,utils,datatabels,blockui,bootbox,select2,uniform,daterangepicker,moment,tree,fileinput_zh, fileinput, datepicker) {
        var firstRate = 0;
        var secondRate = 0;
        var thirdRate = 0;
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: false,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            timePicker: false,
            autoApply: false,
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                fromLabel: '开始日期:',
                toLabel: '结束日期:',
                cancelLabel: '清空',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        var $dateRangeBasic = $('.daterange-basic');

        $dateRangeBasic.daterangepicker(options, function (start, end) {
            if (start._isValid && end._isValid) {
                $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            } else {
                $dateRangeBasic.val('');
            }
        });

        var singleOptions = {
            singleDatePicker: true,
            format: 'yyyy-mm-dd',
            locale: {
                separator: ' - ',
                cancelLabel: '取消',
                weekLabel: 'W',
                daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
                monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
                firstDay: 6
            }
        };
        $('.daterange-single').datepicker(singleOptions);

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate   = picker.endDate;
        });


        /**
         * 清空按钮清空选框
         */
        $dateRangeBasic.on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $dateRangeBasic.val('');
        });

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        // 图片上传
        var $fileUpload = $('#logo-file');
        var $logo = $('#setting_logo');
        var fileUploadManager = {
            uploadOption: {
                uploadUrl: '/sellerpc/member/card/upload',
                showCaption: false,
                showUpload: false,
                uploadAsync: true,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"]
            },
            init: function () {
                var defaultImg = $logo.val();
                if (defaultImg && defaultImg.toString().startsWith('http')) {
                    // 如果已经有图片则显示默认
                    this.uploadOption = $.extend({
                        showPreview: true,
                        initialPreview: [ // 预览图片的设置
                            "<img src= '" + defaultImg + "' class='file-preview-image'>"]
                    }, this.uploadOption);
                }
                // 初始化文件上传控件
                $fileUpload.fileinput(this.uploadOption);
                // 上传之前
                $fileUpload.on('filepreajax', function () {
                    //$btnSubmit.addClass('disabled');
                });

                // 选中后立即上传
                $fileUpload.on('filebatchselected', function () {
                    $fileUpload.fileinput('upload');
                });

                // 上传成功
                $fileUpload.on('fileuploaded', function (event, data) {
                    var res = data.response;
                    if (res.errorCode && res.errorCode === 200) {
                        $logo.val(res.img);
                    } else {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                });
                return this;
            },
            destroy: function () {
                $fileUpload.fileinput('destroy');
                return this;
            },
            refresh: function () {
                this.destroy().init();
            }
        };

        // function ()
        var twitterTreeManager = {/**jstree*/
            $container : $('#twitterTreeManagerContainer'),
            // currentNode: null,
            root: {},
            eachPageSize: 10,
            createIcon : function (child) {
              return '<div class="custom">' +
                      '<img src="' + child['avatar'] + '" alt="" class="user-icon">' +
                      '<em>' +child['name'] + '</em>' +
                      '</div>'
            },
            config : {
                "core" : {
                    "animation" : 200,
                    "check_callback" : true,
                    "themes" : { "stripes" : false, "dots" : false, "responsive" : true, "icons": false},
                    'strings' : {
                        'Loading ...' : '正在加载...'
                    },
                    'data' : {
                        'url' : function(node) {
                            return window.host + '/twitter/getDirectChildren'
                        },
                        'type' : 'GET',
                        'data' : function(node) {
                            //TODO 设置Root节点
                            console.log(node)
                            console.log(node.id)
                            console.log($.jstree.root)
                            var id = node.id === $.jstree.root ? twitterTreeManager.root.id : node.id;
                            node.page = node.page ? node.page : 0;
                            return {
                                userId: id,
                                page: node.page,
                                size: twitterTreeManager.eachPageSize
                            }
                        },
                        'dataFilter' : function(data) {
                            var dataInfo = JSON.parse(data).data;
                            console.log(JSON.parse(data))
                            if(JSON.parse(data).data) {
                                var total = JSON.parse(data).data.total,
                                    parent =  JSON.parse(data).data.parent,
                                    list = JSON.parse(data).data.list;
                            } else alert('数据格式有误')
                            console.log(total);
                            console.log(parent);
                            console.log(list);
                            function renderChildData() {
                                var tmp = [];
                                list.forEach(function(value) {
                                    tmp.push({
                                        'id' : value.id ,
                                        'text' : twitterTreeManager.createIcon(value), // 用户姓名
                                        'state' : { 'opened' : false, 'selected' : false },
                                        'children' : !value.isLeaf
                                    })
                                });
                                if(total > 10) {
                                    var node = twitterTreeManager.tree.get_node(parent.id);
                                    var maxPage = parseInt(total/10);
                                    node.maxPage = maxPage;
                                    if(node && node.page) {
                                        var page =node.page +1;
                                        node.maxPage = maxPage;
                                    }else {
                                        page = 1;
                                        twitterTreeManager.root.maxPage = maxPage;
                                    }
                                    console.log(twitterTreeManager.tree.get_node(parent.id))
                                    tmp.push({
                                        'id' : parent.id + "pagingBtn",
                                        'text' : '<div class="flip-page">' +
                                                 '<i class="previousPage">上页</i>' +
                                                 '<em class="nextPage">下页</em>' +
                                                 '<span>第' + (node.page+1) +
                                                 '页</span>' +
                                                 '</div>',
                                        'state' : { 'opened' : false, 'selected' : false },
                                        'children' : false
                                    })
                                }
                                return tmp
                            }
                            if( parent && parent.id ) {
                                if( parent.id === twitterTreeManager.root.id && !twitterTreeManager.tree.get_node(parent.id)) {
                                    //根节点
                                    console.log('根节点')
                                    return JSON.stringify([{
                                        'id' : parent.id ,
                                        'text' : twitterTreeManager.createIcon(parent), // 用户姓名
                                        'state' : { 'opened' : false, 'selected' : false },
                                        'children' : !!(list.length)
                                    }])
                                } else {
                                    if(list.length != 0 ) {
                                        console.log('子节点')
                                        return JSON.stringify( renderChildData() )
                                    }
                                }
                            }
                        }
                    }
                },
                "types" : {
                    "#" : {
                        "max_children" : 1,
                        "max_depth" : 6,
                    }
                },
                "plugins" : [
                    "contextmenu", "search",
                    "state", "types", "sort", "Wholerow", "json_data"
                ]
            },

            init : function (userId) {
                this.root.id = userId;
                this.root.page = 0;
                this.$container
                    //初始化时绑定需要处理的事件
                    .on('click.jstree', function (e) {
                        var className = e.target.getAttribute('class');
                        var tree = twitterTreeManager.tree;
                        if(className && className.match('previousPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if(parentNode.page > 0) {
                                parentNode.page = parentNode.page -1;
                                tree.load_node(parentNode)
                            }
                        } else if(className && className.match('nextPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if(parentNode.page < parentNode.maxPage ) {
                                parentNode.page = parentNode.page + 1;
                                tree.load_node(parentNode);
                                console.log(parentNode);
                            }
                        }
                    })
                    //执行初始化
                    .jstree(this.config);
                    this.tree = this.$container.jstree(true);
            },

            clear: function() {
                this.$container.jstree('destroy');
                delete twitterTreeManager.tree;
                // delete twitterTreeManager
            }
        };

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitterMember/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#selectStatus").val(),
                    startDate: $dateRangeBasic.val() !== '' && options.startDate !== '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: $dateRangeBasic.val() !== '' && options.endDate !== '' ? options.endDate.format('YYYY-MM-DD') : '',
                    phone:$("#phone").val(),
                    name:$("#name").val(),
                    pageable: true
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                //{
                //    width:'30px',
                //    sortable: true,
                //    render: function(data, type, row) {
                //        return '<img class="sm-goods-image" src="'+row.avatar+'" />';
                //    }
                //},
                {
                    width:'75px',
                    sClass: 'sorting',
                    title: '推客',
                    data:'name',
                    name:'name',
                    sortable: true
                },{
                    width:200,
                    sClass: 'styled text-center sorting_disabled',
                    title: "推客信息",
                    render: function(data, type, row){
                        return '<a href="#" user-id="' + row.id + '" class="twitterChildrenBtn" data-toggle="modal" >' +row.phone+ ' <i class="icon-search4"></i></a>'
                                    + '<a href="#" user-id="' + row.id + '" class="twitterChildrenTreeManager" data-toggle="modal" ><i class="icon-tree6"></i></a>';
                    }
                },{
                    width:'75px',
                    sClass: 'sorting',
                    title: '等级',
                    data:'levelName',
                    name:'levelName',
                    sortable: true
                },
            //    {
            //    width:45,
            //    sClass: 'text-center sorting_disabled',
            //    data: 'buyerId',
            //    name: 'buyerId',
            //    title: '等级'
            //},
            //    {
            //    width:85,
            //    sClass: 'text-center sorting_disabled',
            //    data: 'buyerId',
            //    name: 'buyerId',
            //    title: '会员账号'
            //},
                {
                width:90,
                data: 'unCmFee',
                name: 'unCmFee',
                title: '未结算佣金',
                render: function(data, type, row){
                    if (row.unCmFee==null){
                        return '￥' + '0';
                    }else{
                        return '￥' + row.unCmFee;
                    }
                }
            },{
                width:90,
                data: 'cmFee',
                name: 'cmFee',
                title: '已结算佣金',
                    render: function(data, type, row){
                        if (row.cmFee==null){
                            return '￥' + '0';
                        }else{
                            return '￥' + row.cmFee;
                        }
                    }
            },
            //    {
            //    width:80,
            //    sClass: 'sorting',
            //    //data: 'extension_order_total',
            //    //name: 'extension_order_total',
            //    title: '推广订单额'
            //},{
            //    width:80,
            //    sClass: 'sorting',
            //    //data: 'recommend',
            //    //name: 'recommend',
            //    title: '推荐人'
            //},{
            //    width:75,
            //    sClass: 'sorting',
            //    //data: 'recommend_num',
            //    //name: 'recommend_num',
            //    title: '推荐人数',
            //    sortable: true
            //},{
            //    width:100,
            //    sClass: 'sorting',
            //    //data: 'other_info',
            //    //name: 'other_info',
            //    title: '其它信息'
            //},
                {
                width:75,
                sClass: 'sorting',
                title: '加入时间',
                sortable: true,
                render: function (data, type, row) {
                    if (row.createdAt ==  null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            },{
                width:75,
                sClass: 'sorting',
                data: 'status',
                name: 'status',
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if(value =='ACTIVE'){
                        return "正常";
                    }else{
                        return "禁用";
                    }
                }
            },{
                width:150,
                sClass: 'styled text-center sorting_disabled',
                align: 'right',
                title: '操作',
                render: function(data, type, row) {
                    return '<a href="javascript:void(0);" class="setLevelBtn" rowId="'+row.id + '">设置等级</a>  ' +
                        '<a href="javascript:void(0);" class="disableBtn" data-toggle="disablepopover" rowId="'+row.twitterId + '">禁用</a>  ' +
                        '<a href="javascript:void(0);" class="enableBtn" data-toggle="enablepopover" rowId="'+row.twitterId + '">启用</a>  ' +
                        '<a href="javascript:void(0);"    class="changeTreeBtn" rowId="'+row.id + '">改变上级</a>  ' +
                        '<a href="javascript:void(0);"    class="setPartnerBtn"     data-toggle="setPartnerpopover" rowId="'+row.id+'"><i class="icon-users4"></i>设为合伙人</a>' +
                        '<a href="javascript:void(0);"    class="setTeamBtn" rowId="'+row.id+'" userName="'+row.name+'"><i class="icon-users4"></i>设为队长</a>';
                }
            }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        // 保存推客等级
        $(".saveLevelBtn").on('click', function() {
            var id = $("#levelId").val();
            var name = $("#levelName").val();
            var type = $("#levelType").val();
            var conditions = $("#levelConditions").val();
            var firstLevelRate = $("#levelFirstLevelRate").val();
            var secondLevelRate = $("#levelSecondLevelRate").val();
            var thirdLevelRate = $("#levelThirdLevelRate").val();
            var description = $("#levelDescription").val();


            if(!name || name == '' ){
                utils.tools.alert("请输入等级名称!", {timer: 1200, type: 'warning'});
                return;
            }else if(type == 'COMMISSION' && (!conditions || conditions == '') ){
                utils.tools.alert("请输入等级条件!", {timer: 1200, type: 'warning'});
                return;
            }else if(!firstLevelRate || firstLevelRate == ''){
                utils.tools.alert("请输入一级佣金比例!", {timer: 1200, type: 'warning'});
                return;
            }else if(!secondLevelRate || secondLevelRate == ''){
                utils.tools.alert("请输入二级佣金比例!", {timer: 1200, type: 'warning'});
                return;
            }else if(!thirdLevelRate || thirdLevelRate == ''){
                utils.tools.alert("请输入三级佣金比例!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                id: id,
                name: name,
                type: type,
                conditions: conditions,
                firstLevelRate: firstLevelRate,
                secondLevelRate: secondLevelRate,
                thirdLevelRate: thirdLevelRate,
                description: description
            };
            $.ajax({
                url: host + '/twitterMember/saveLevel',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_addlevel").modal('hide');
                        $leveldatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        // 获取推客设置中默认设置的三级分佣比例
        $.ajax({
            url: window.host + '/twitterMember/getDefaultCommission',
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.errorCode == 200) {
                    var role = data.data;
                    if(role){
                        firstRate = role.firstLevelRate;
                        secondRate = role.secondLevelRate;
                        thirdRate = role.thirdLevelRate;
                    }
                    $("#firstRate").html(firstRate);
                    $("#secondRate").html(secondRate);
                    $("#thirdRate").html(thirdRate);

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

        // 佣金比例变化时，对应的总和也需要一起变化
        $("#levelFirstLevelRate").on('input',function(e){
            var first = $("#levelFirstLevelRate").val();
            if(first && first != ''){
                var total = Number(first) * Number(firstRate);
                $("#firstRateTotal").html(total.toFixed(2));
            }else{
                $("#firstRateTotal").html('');
            }
        });
        $("#levelSecondLevelRate").on('input',function(e){
            var second = $("#levelSecondLevelRate").val();
            if(second && second != ''){
                var total = Number(second) * Number(secondRate);
                $("#secondRateTotal").html(total.toFixed(2));
            }else{
                $("#secondRateTotal").html('');
            }
        });
        $("#levelThirdLevelRate").on('input',function(e){
            var third = $("#levelThirdLevelRate").val();
            if(third && third != ''){
                var total = Number(third) * Number(thirdRate);
                $("#thirdRateTotal").html(total.toFixed(2));
            }else{
                $("#thirdRateTotal").html('');
            }
        });

        // 推客等级列表
        var $leveldatatables = $('#userLevelTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitterMember/levelList", {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [
                {
                    width:'75px',
                    title: '等级名称',
                    data:'name',
                    name:'name',
                    sortable: false
                },{
                    width:'100px',
                    title: '等级条件',
                    data:'conditions',
                    name:'conditions',
                    sortable: false
                },
                {
                    width:75,
                    data: 'type',
                    name: 'type',
                    title: '佣金类型',
                    render: function (data, type, row) {
                        var value = row.type;
                        if(value =='COMMISSION'){
                            return "按佣金";
                        }else{
                            return "自定义";
                        }
                    }
                },{
                    width:90,
                    name: 'rate',
                    title: '佣金比例',
                    render: function(data, type, row){
                        return '1级 : ' + row.firstLevelRate +'倍</br>' +
                               '2级 : ' + row.secondLevelRate +'倍</br>' +
                               '3级 : ' + row.thirdLevelRate +'倍</br>';
                    }
                },
                {
                    width:75,
                    title: '创建时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt ==  null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                },{
                    width:150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    title: '操作',
                    render: function(data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="leveledit role_check_table" style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_item"><i class="icon-pencil7"></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="leveldel role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_item"><i class="icon-trash"></i>删除</a>';

                        return html;
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initLevelEvent();
            }
        });
        
        function initLevelEvent() {
            $(".leveledit").on("click",function(){
                var id =  $(this).attr("rowId");
                $.ajax({
                    url: window.host + '/twitterMember/getLevel/' + id,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            var role = data.data;
                            $("#levelId").val(role.id);
                            $("#levelName").val(role.name);
                            $("#levelType").val(role.type);
                            $("#levelConditions").val(role.conditions);
                            $("#levelFirstLevelRate").val(role.firstLevelRate);
                            $("#levelSecondLevelRate").val(role.secondLevelRate);
                            $("#levelThirdLevelRate").val(role.thirdLevelRate);
                            $("#levelDescription").val(role.description);
                            if (role.type == "COMMISSION") {
                                $("#levelConditionsDiv").show();
                            } else if (role.type == "CUSTOM") {
                                $("#levelConditionsDiv").hide();
                            }

                            if(role.firstLevelRate && role.firstLevelRate != ''){
                                var total = Number(role.firstLevelRate) * Number(firstRate);
                                $("#firstRateTotal").html(total.toFixed(2));
                            }else{
                                $("#firstRateTotal").html('');
                            }
                            if(role.secondLevelRate && role.secondLevelRate != ''){
                                var total = Number(role.secondLevelRate) * Number(secondRate);
                                $("#secondRateTotal").html(total.toFixed(2));
                            }else{
                                $("#secondRateTotal").html('');
                            }
                            if(role.thirdLevelRate && role.thirdLevelRate != ''){
                                var total = Number(role.thirdLevelRate) * Number(thirdRate);
                                $("#thirdRateTotal").html(total.toFixed(2));
                            }else{
                                $("#thirdRateTotal").html('');
                            }


                            $("#modal_addlevel").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });

            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation:true,
                content: function() {
                    var rowId =  $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click",function(){
                    var pId = $(this).attr("pId");
                    deleteLevel(pId);
                });
                $('.popover-btn-cancel').on("click",function(){
                    $(that).popover("hide");
                });
            });


        }

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "enablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="enablepopover"]').popover('hide');
            } else if (target.data("toggle") == "enablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "disablepopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="disablepopover"]').popover('hide');
            } else if (target.data("toggle") == "disablepopover") {
                target.popover("toggle");
            }

            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "setPartnerpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="setPartnerpopover"]').popover('hide');
            } else if (target.data("toggle") == "setPartnerpopover") {
                target.popover("toggle");
            }
        });

        // 新增推客等级
        $(document).on('click', '.btnAddLevel', function() {
            $("#levelId").val('');
            $("#levelName").val('');
            $("#levelType").val('COMMISSION');
            $("#levelConditions").val('');
            $("#levelFirstLevelRate").val('');
            $("#levelSecondLevelRate").val('');
            $("#levelThirdLevelRate").val('');
            $("#levelDescription").val('');
            $("#levelConditionsDiv").show();
            /** 初始化选择框控件 **/
            $('.select').select2({
                minimumResultsForSearch: Infinity,
            });
            $("#modal_addlevel").modal("show");
        });

        /**
         * 自定义类型的推客等级，将等级条件隐藏
         */
        $("#levelType").change(function () {
            var ss = $(this).children('option:selected').val();
            if (ss == "COMMISSION") {
                $("#levelConditionsDiv").show();
            } else if (ss == "CUSTOM") {
                $("#levelConditionsDiv").hide();
            }
        });

        function deleteLevel(id) {
            $.ajax({
                url: host + '/twitterMember/deleteLevel/' + id,
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $leveldatatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        var userId =  null;
        //查看推客下级
        var $twitterChildrenTables = $('#twitterChildrenInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitterMember/children", {
                    size: data.length,
                    page: (data.start / data.length),
                    userId : userId,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }

                    parentShopName = res.data.parentShopName;
                    //document.getElementById("parentShopName").innerText = parentShopName ;
                    $("#parentShopName").html(parentShopName );
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                data: 'name',
                name: 'name',
                width: 310,
                title: "推客店铺"

            },{
                data: 'archive',
                name: 'archive',
                width: 150,
                title: "店铺状态",
                render: function (data, type, row) {
                    var value = row.archive;
                    if(value =='0'){
                        return "正常";
                    }else{
                        return "禁用";
                    }
                }
            }]
        });

        $(document).on('click', '.twitterChildrenBtn', function() {
            userId = $(this).attr("user-id");
            $('#modal_twitterChildrenInfo').modal('show');
            $twitterChildrenTables.search(userId).draw();
        });

        $(document).on('click', '.twitterChildrenTreeManager', function() {
            userId = $(this).attr("user-id");
            twitterTreeManager.init(userId);//初始化树
            $('#modal_tree_manager').modal('show');
        });

        $("#modal_tree_manager").on('hidden.bs.modal', function() {
            twitterTreeManager.clear();
        });

        $('.exportTwitterChildrenBtn').on('click', function() {
            if (userId != null) {
                $('#exportTwitterChildrenForm input[name="userId"]').val(userId);
                $('#exportTwitterChildrenForm').submit();
            }
        });

        // 保存推客设置等级
        $(".saveSetLevelBtn").on('click', function() {
            var userId = $("#set_level_user_id").val();
            var levelId = $("#set_level_type").val();
            var validAt = $("#set_level_valid_at").val();

            if(!levelId || levelId == '' ){
                utils.tools.alert("请选择等级!", {timer: 1200, type: 'warning'});
                return;
            }else if(!validAt || validAt == '' ){
                utils.tools.alert("请选择过期时间!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                userId: userId,
                levelId: levelId,
                validAt: validAt
            };
            $.ajax({
                url: host + '/twitterMember/saveLevelRelation',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        $("#modal_setlevel").modal('hide');
                        $datatables.search('').draw();
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        });

        function initEvent() {

            // 启用
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认启用吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });



            // 禁用
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认禁用吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    disableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });


            // 提升为合伙人
            $("[data-toggle='setPartnerpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认提升为合伙人吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="setPartnerpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="setPartnerpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    setPartner(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            // 推客设置等级
            $(".setLevelBtn").on('click', function () {
                var userId = $(this).attr('rowId');
                $("#set_level_user_id").val(userId);
                $("#modal_setlevel").modal('show');
            });

            // 设为队长
            $(".setTeamBtn").on('click', function () {
                var userId = $(this).attr('rowId');
                var userName = $(this).attr('userName');
                $("#setting_userName").html(userName);
                $("#setting_user_id").val(userId);
                $("#setting_name").val('');
                fileUploadManager.init();
                $("#modal_setting").modal("show");
                /** 初始化选择框控件 **/
                $('.select').select2({
                    minimumResultsForSearch: Infinity,
                });
            });

            // 保存设为队长设置
            $(".saveSetting").on('click', function() {
                var id = $("#setting_id").val();
                var groupId = $("#setting_group").val();
                var name = $("#setting_name").val();
                var userId = $("#setting_user_id").val();
                var logo = $logo.val();
                if(name == ''){
                    utils.tools.alert("请输入战队名", {timer: 1200, type: 'success'});
                    return;
                }

                var data = {
                    'id' : id,
                    'name' : name,
                    'userId' : userId,
                    'groupId' : groupId,
                    'logo' : logo
                }

                utils.postAjax(window.host+ "/team/saveInfo", data, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            $("#modal_setting").modal("hide");
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });

            });


            $(".changeTreeBtn").on('click', function () {
                $("#modal_update_pred").modal('show');
                changeTwitterTarget = $(this).attr('rowId');


                //alert('待变更的rowid:' + changeTwitterTarget);

                //var testData = [{"id":"71l0vl7g","createdAt":1469598337000,"updatedAt":null,"phone":"13774307010","cmFee":0.0000,"unCmFee":0.0000,"name":"董董的科学发展观的小店","status":"ACTIVE"}];

                $guiderUserTable = $('#guiderLeaderTable').DataTable({
                    paging: true, //是否分页
                    filter: false, //是否显示过滤
                    lengthChange: false,
                    processing: true,
                    serverSide: true,
                    deferRender: true,
                    searching: false,
                    ajax: function(data, callback, settings) {
                        $.get(window.host + "/twitterMember/userslist", {
                            size: data.length,
                            page: (data.start / data.length),
                            name: twitterName4ChangeTree,
                            phone:twitterPhone4ChangeTree,
                            status: 'ALL',
                            pageable: true
                        }, function(res) {
                            if (!res.data.list) {
                                res.data.list = [];
                            }
                            callback({
                                recordsTotal: res.data.total,
                                recordsFiltered: res.data.total,
                                data: res.data.list,
                                iTotalRecords:res.data.total,
                                iTotalDisplayRecords:res.data.total
                            });
                        });
                    },
                    columns: [
                        {
                            width:'75px',
                            title: '推客',
                            data:'name',
                            name:'name',
                            sortable: true
                        },{
                            width:200,
                            sClass: 'styled text-center sorting_disabled',
                            title: "手机",
                            render: function(data, type, row) {
                                return '<a href="#" user-id="' + row.id + '" >' +row.phone+ '</a>';
                            }
                        },{
                            width:75,
                            sClass: 'sorting',
                            title: '加入时间',
                            sortable: true,
                            render: function (data, type, row) {
                                if (row.createdAt ==  null) {
                                    return '';
                                }
                                var cDate = parseInt(row.createdAt);
                                var d = new Date(cDate);
                                return d.format('yyyy-MM-dd hh:mm:ss');
                            }
                        },{
                            width:75,
                            sClass: 'sorting',
                            data: 'archive',
                            name: 'archive',
                            title: '状态',
                            render: function (data, type, row) {
                                var value = row.archive;
                                if(value =='0'){
                                    return "正常";
                                }else{
                                    return "禁用";
                                }
                            }
                        },{
                            width:150,
                            sClass: 'styled text-center sorting_disabled',
                            align: 'right',
                            title: '操作',
                            render: function(data, type, row) {
                                return '<a href="javascript:void(0);"    class="changeTreeConfirmBtn" data-toggle="popover" rowId="'+row.id + '">变更</a>';

                            }
                        }],
                    drawCallback: function () {  //数据加载完成
                        initEventForChangeTreeModal();
                    }
                });
            });

            $("#modal_update_pred").on('hidden.bs.modal', function() {
                $guiderUserTable.destroy();
            });


            /*商品删除会判断 userId 否则删除失败*/
            function setPartner(id) {
                // 判断该推客是否已经是合伙人
                var url = window.host + "/partnerMember/existShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (!res.data) {
                            url = window.host + "/twitterMember/setPartner/" + id;
                            utils.postAjax(url, {}, function (res) {
                                if (typeof(res) === 'object') {
                                    if (res.data) {
                                        $datatables.search('').draw();
                                    } else {
                                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                    }
                                }
                            });
                        } else {
                            utils.tools.alert("该推客已经是合伙人会员!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function disableShop(id) {
                var url = window.host + "/twitterMember/disableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function enableShop(id) {
                var url = window.host + "/twitterMember/enableShop/" + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }
        }

        $('#searchTwitter4ChangeTree').on('click', function() {
            twitterName4ChangeTree = $('#twitterName4ChangeTree').val();
            twitterPhone4ChangeTree = $('#twitterPhone4ChangeTree').val();
            //alert('把查询的必要条件去掉,否则查不到数据' + '条件:' + twitterName4ChangeTree + ',' + twitterPhone4ChangeTree);
            $guiderUserTable.search('').draw();
        });

        $('#quickChooseRootShop').on('click', function() {
            alert('写提交的代码');
        });

        $(".btn-search").on('click', function () {
            $datatables.search('').draw();
        });

        function initEventForChangeTreeModal() {
            $('.changeTreeConfirmBtn').on('click', function() {
                var rowId = $(this).attr('rowId');
                //alert('将 ' + changeTwitterTarget + ' 的上级' + '变更为' + rowId +'   写提交的代码');
                var data = {
                    parentUserId: rowId,
                    userId: changeTwitterTarget,
                };
                var url = window.host  + '/twitterMember/changeParent';
                //alert(url);

                utils.postAjaxWithBlock($(document), url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("上级变更成功", {timer: 1200, type: 'success'});
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            });
        }

        var twitterName4ChangeTree;
        var twitterPhone4ChangeTree;
        var changeTwitterTarget;
        var $guiderUserTable;
        var parentShopName;
});
//微信账号绑定
require(['all']);

require(['twitter/member']);

define("twitterMember", [],function(){});


//微信账号绑定
require(['all']);

require(['twitter/member']);

define("twitterMember", function(){});

