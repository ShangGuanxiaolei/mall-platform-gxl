/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {
    sessionStorage.removeItem('sKeywordId')
    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
        /**
         * 获取options html
         * @param url 服务器地址
         * @param valueMapper 函数, 获取对象中的值作为option的value
         * @param contentMapper 函数, 获取对象中的值作为option的content
         * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
         * @returns {Promise<any>} promise, 可取出结果
         */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
            _this.post(url, data => {
                if (dataObtainer && $.isFunction(dataObtainer)) {
                data = dataObtainer(data) || [];
            }
            try {
                resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
            } catch (e) {
                console.log(e);
                reject(e)
            }
        })
        })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
        }) {
        swal({
                title: "确认操作",
                text: sMsg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#FF7043",
                confirmButtonText: "是",
                cancelButtonText: "否"
            },
            function (isConfirm) {
                if (isConfirm) {
                    fnConfirm();
                }
                else {
                    fnCancel();
                }
            });
    },
    /**
     * [获得字符串的字节长度，超出一定长度在后面加符号]
     * @param  {[String]} str  [待查字符串]
     * @param  {[Number]} len  [指定长度]
     * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
     * @param  {[String]} more [替换超出字符的符号]
     */
    getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
            for (var i = 0; i < str_len; i++) {
                a = str.charAt(i);
                str_length++;
                if (escape(a).length > 4) {
                    str_length++;
                }
            }
            return str_length;
        }
        ;
        if (type = 2) {
            for (var i = 0; i < str_len; i++) {
                a = str.charAt(i);
                str_length++;
                if (escape(a).length > 4) {
                    str_length++;
                }
                str_cut = str_cut.concat(a);
                if (str_length >= len) {
                    if (more && more.length > 0) {
                        str_cut = str_cut.concat(more);
                    }
                    return str_cut;
                }
            }
            if (str_length < len) {
                return str;
            }
        }
    },
    confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
        swal({
                title: sMsg,
                text: '',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#FF7043",
                confirmButtonText: "是",
                cancelButtonText: "否"
            },
            function (isConfirm) {
                if (isConfirm) {
                    fnConfirm();
                }
                else {
                    fnCancel();
                }
            });
    },
    /**
     * 同步数据到form
     * 要求form中input的name属性跟data中key的值对应
     * @param $form 需要同步的表单jquery对象
     * @param data 同步的json数据, 可选参数，不传则清空表单
     */
    syncForm: function ($form, data) {
        if (data) {
            $.each($form.find(':input'), function (index, item) {
                var $item = $(item);
                var name = $item.attr('name');
                var value = data[name];
                var $this = $(this);
                if (value) {
                    if ($this.is(':radio')) {
                        if ($this.val() === value) {
                            $this.prop('checked', true);
                        }
                    } else {
                        $item.val(value);
                    }
                }
            });
        } else {
            $form[0].reset();
        }
    }
}
};
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('productModal',['jquery', 'datatables'], function ($) {

    var $orders = ['price', 'amount', 'sales', 'onsale'];
    var $order = '';
    var pId = '';
    // 绑定商品上下文
    var $productListUrl = window.host + "/product/list";


    var productType = $('#type').val();
    var taxonomy = (() => productType === 'FILTER' ? 'FILTER' : 'GOODS')();

    var $shopId = null;
    var $category = '';
    var $pDataTable;

    var checkedBounded = false;

    var $modal = $('#choose-product-modal');

    // 缓存选中的商品
    var checkedSet = null;
    var checkedSetInited = false;

    /**
     * 切换商品后重置缓存状态
     */
    function resetCheckedStatus() {
        checkedSet = new Set();
        checkedSetInited = false;
    }

    function removeFromCheckedList(id) {
        checkedSet.delete(id);
        console.log('removed from checked, ', checkedSet);
    }

    function pushToCheckedList(id) {
        checkedSet.add(id);
        console.log('pushed to checked, ', checkedSet);
    }

    /**
     * 选中所有商品/滤芯
     */
    $(document).on('click', '#checkAllPd', function () {
        if ($(this).prop('checked')) {
            $(this).prop('checked', true);
            $('input[name=checkPd]').prop('checked', true).trigger('change');
        } else {
            $(this).prop('checked', false);
            $('input[name=checkPd]').prop('checked', false).trigger('change');
        }
    });

    //初始化商品分类信息
    $.ajax({
        url: window.host + '/shop/category/list?taxonomy=FILTER',
        type: 'POST',
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.errorCode === 200) {
                var dataLength = data.data.length;
                var $categoryType = $('#categoryTypeModal');
                $categoryType.empty();
                $categoryType.append('<option value="" selected="selected">所有分类</option>');
                for (var i = 0; i < dataLength; i++) {
                    $categoryType.append('<option value=' + data.data[i].id + '>'
                        + data.data[i].name + '</option>');
                }
            } else {
                utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
            }
        },
        error: function (state) {
            if (state.status === 401) {
            } else {
                utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
            }
        }
    });

    /**
     * 标签搜索
     */
    $('body').on('change', 'select[name="categoryTypeModal"]', function () {
        $productListUrl = window.host + "/product/list";
        $category = $(this).val();
        $pDataTable.search('').draw();
    });

    /**
     * 关键字搜索
     */
    $(".btn-search-products").on('click', function () {
        var keyword = $.trim($("#select_products_sKeyword").val());
        if (keyword !== '' && keyword.length > 0 && shopId !== null) {
            $productListUrl = window.host + '/product/searchbyPc/' + shopId + '/'
                + keyword;
            $pDataTable.search(keyword).draw();
        } else if (keyword === '' || keyword.length === 0) {
            $productListUrl = window.host + "/product/list";
            $pDataTable.search('').draw();
        }
    });

    $('.btn-show-bounded-filter').on('click', function () {
        $productListUrl = window.host + '/product/listFilterTable';
        $pDataTable.search('').draw();
    });

    function buildColumns(type) {
        return [
            {
                width: "10px",
                orderable: false,
                render: function (data, type, row) {
                    var $dom = $(`<label class="checkbox"><input name="checkPd" type="checkbox" class="styled"
            rowId=${row.id}></label>`);
                    // if (row.select && row.select === true) {
                    var isSelect = (checkedSet && checkedSet.size > 0) ? checkedSet.has(row.id)
                        : (row.select && row.select === true);
                    $dom.find('input[name=checkPd]').attr('checked', isSelect);
                    // }
                    return $dom.html();
                }
            },
            {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                    return '<a href="' + row.productUrl
                        + '"><img class="goods-image" src="' + (row.imgUrl || row.img) + '" /></a>';
                }
            },
            {
                data: "name",
                width: "50px",
                orderable: false,
                name: "name"
            },
            {
                data: "encode",
                width: "30px",
                orderable: false,
                name: "encode"
            },
            {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.status) {
                        case 'INSTOCK':
                            status = '下架';
                            break;
                        case 'ONSALE':
                            status = '在售';
                            break;
                        case 'FORSALE':
                            status = '待上架发布';
                            break;
                        case 'DRAFT':
                            status = '未发布';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
            }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
            },
            {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
            }, {
                orderable: false,
                width: "100px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.onsaleAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }
        ];
    }

    function initTable(params) {
        $pDataTable = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get($productListUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    type: params.type || 'NORMAL',
                    productId: pId,
                    pageable: true,
                    order: function () {
                        if ($order !== '') {
                            return $order;
                        } else {
                            var _index = data.order[0].column;
                            if (_index < 4) {
                                return '';
                            } else {
                                return $orders[_index - 4];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir : 'asc',
                    category: $category,
                    isGroupon: ''
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else {
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    // 表格刷新时合并新的选中项
                    // TODO 已选中数据单独请求
                    if (!checkedSetInited) {
                        var newCheckedList = res.data.checkedList || [];
                        newCheckedList.forEach(pushToCheckedList);
                        checkedSetInited = true;
                    }

                    // 刷新后去掉全选
                    // TODO 按照实际的该分页是否全选来设置
                    $('input[id=checkAllPd]').prop('checked', false);
                    // 表格翻页后合并所有选中项
                    console.log('checked list: ', checkedSet);
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId: "id",
            columns: buildColumns(params.type),
            drawCallback: function () {  //数据加载完成
                initSelectProductEvent(params);
            }
        });
    }

    function initSelectProductEvent(params) {

        if (!checkedBounded) {
            $(document).on('change', 'input[name=checkPd]', function () {
                var $this = $(this);
                var isChecked = $this.is(':checked');
                var id = $this.attr('rowId');
                if (!isChecked) {
                    removeFromCheckedList(id);
                    if (params.unChecked) {
                        params.unChecked(this);
                    }
                } else {
                    pushToCheckedList(id);
                    if (params.checked) {
                        params.checked(this);
                    }
                }
            });
            checkedBounded = true;
        }

        $(".selectproduct").on("click", function () {
            var id = $(this).attr("rowId");
            var name = $(this).attr("productName");
            var price = $(this).attr("productName");
            //回调外部函数
            if (params && params.onSelect) {
                params.onSelect(id, name, price);
            }
            $modal.modal("hide");
        });
    }

    return {
        /**
         * 创建商品表格
         * @param params.type 类型 商品或滤芯
         * @param params.triggerDom 唤起选框dom
         * @param params.onConfirm 确认时回调
         * @param params.onSelect 选择时回调
         * @param params.onTrigger modal触发时的事件
         * @param params.unChecked 取消选框事件
         * @param params.checked 选框事件
         */
        create: function (params) {
            initTable(params);

            // dom名称兼容旧版本
            var dom = params.triggerDom || params.dom;

            $('.btn-ok').on('click', function () {
                if (params.onConfirm) {
                    params.onConfirm(checkedSet);
                }
                $('#choose-product-modal').modal('hide');
            });

            /* 选择活动商品 */
            $(document).on('click', dom, function () {
                if (params.type === 'FILTER') {
                    pId = $(this).attr('rowId');
                    resetCheckedStatus();
                    $pDataTable.search('').draw();
                }
                if (params.onTrigger) {
                    params.onTrigger.call(this);
                }
                $modal.modal("show");
            });

        },
        refresh: function () {
            $pDataTable.search('').draw();
        }
    }
});

/**
 * Created by quguangming on 16/5/24.
 */
define('product/list',['jquery', 'productModal', 'utils', 'datatables', 'blockui', 'bootbox',
        'select2', 'daterangepicker'],
    function ($, productModal, utils, datatabels, blockui, select2,
              daterangepicker) {

        const NOT_FOUND = "/_resources/images/404.png";

        // 商品类型
        var productType = $('#type').val();
        var taxonomy = (() => productType === 'FILTER' ? 'FILTER' : 'GOODS')();

        var productId;

        var $listUrl = window.host + "/product/list?type=" + productType;
        var $updateReviewStatusUrl = (id,
            status) => `${window.host}/product/review/${id}?status=${status}`;
        var $unBindUrl = (filterId, productId) => window.host
        + `/product/filter/unbind?filterId=${filterId}
        &productId=${productId}`;

        var $bindUrl = (filterId, productId) => window.host
        + `/product/filter/bind?filterId=${filterId}
        &productId=${productId}`;

        var $orders = ['price', 'amount', 'sales', 'onsale'];

        var $shopId = null;

        var $order = '';

        var $status =$("#productStatus").val();

        var $reviewStatus = '';

        var $brandType = '';

        var $supplierType = '';

        const checkAll = '#checkAllGoods';

        var $category = '';

        var $groupon = '';


        //获取品牌信息
        $.ajax({
                url: `${window.host}/brand/list`,
            }
        ).done(
            data => {
            let results = [];
        let list = data.data.list;
        for (let i = 0; i < list.length; i++) {
            let brand = {
                id: list[i].id,
                text: list[i].name,
            };
            results.push(brand);
        }
        //初始化select2
        $("#brandType").select2({
            minimumResultsForSearch: Infinity,
            data: results
        });
    }
    ).fail(
            data => {
            utils.tools.alert("获取品牌信息出错",
            {timer: 1200, type: 'warning'});
    });

        //获取供应商信息并建立缓存
        let sourceMap = new Map();
        $.ajax({
                url: `${window.host}/supplier/list?type=override&pageable=false`,
                async: false,
            }
        ).done(
            data => {
            let results = [];
        data.data.list.forEach(e => {
            sourceMap.set(e.id, e.name);
        let supplier = {
            id: e.id,
            text: e.name,
            code: e.code
        };
        results.push(supplier);
        //初始化select2
        $("#supplierType").select2({
            minimumResultsForSearch: Infinity,
            data: results
        });
    });
        sessionStorage.setItem('listDatas', JSON.stringify(results))
    }
    ).fail(
            data => {
        });

        /**
         * 动态构建表格列
         * @param base
         * @constructor
         */
        function ColumnBuilder(base) {
            this.columns = base && $.isArray(base) ? base : [];

            /**
             * 新增列
             * @param column 列对象
             * @param predicate 添加条件
             * @return {ColumnBuilder}
             */
            this.append = function (column, predicate) {
                if (!predicate || (predicate && predicate() === true)) {
                    if ($.isArray(column)) {
                        this.columns = this.columns.concat(column);
                    } else {
                        this.columns.push(column);
                    }
                }
                return this;
            };

            /**
             * 返回构建好的列
             */
            this.build = function () {
                return this.columns;
            }
        }

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 初始化日期控件 **/
        var options = {
            timePicker: true,
            dateLimit: {days: 60000},
            timePickerIncrement: 1,
            locale: {
                format: 'YYYY-MM-DD h:mm a',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
                    "10月", "11月", "12月"],
                firstDay: 6
            },
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default',
        };

        $('.daterange-time').daterangepicker(options);

        /** 回调 **/
        $('.daterange-time').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;
            $("#groupon_valid_from").val(
                picker.startDate.format('YYYY-MM-DD HH:mm'));
            $("#groupon_valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
        });

        // 全选
        $("#checkAllGoods").on('click', function () {
            $("input[name='checkGoods']").prop("checked", $(this).prop("checked"));
        });

        $(".btn-release").on('click', function () {
            window.location.href = '/sellerpc/mall/product/edit?type='
                + productType;
        });

        $(".btn-batchDel").on('click', function () {
            var updateds = getTableContent();
            if (updateds.length == 0) {
                utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
                return;
            }
            var ids = '';
            $.each(updateds, function (index, row) {
                if (ids != '') {
                    ids += ',';
                }
                ids += row.id;
            });

            var url = window.host + "/product/batchDelete/" + ids;
            utils.postAjax(url, {}, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("删除成功!", {timer: 1200, type: 'success'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });

        });

        $(".btn-batchUp").on('click', function () {
            var updateds = getTableContent();
            if (updateds.length == 0) {
                utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
                return;
            }
            var ids = '';
            $.each(updateds, function (index, row) {
                if (ids != '') {
                    ids += ',';
                }
                ids += row.id;
            });
            upProduct(ids);
        });

        $(".btn-batchDown").on('click', function () {
            var updateds = getTableContent();
            if (updateds.length == 0) {
                utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
                return;
            }
            var ids = '';
            $.each(updateds, function (index, row) {
                if (ids != '') {
                    ids += ',';
                }
                ids += row.id;
            });
            downProduct(ids);
        });

        // 得到当前选中的行数据
        function getTableContent() {
            var selectRows = new Array();
            for (var i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
                var value = {};
                var checkvalue = $("input[name='checkGoods']:checked")[i];
                value.id = $(checkvalue).attr("rowid");
                selectRows.push(value);
            }
            return selectRows;
        }

        /**
         * 优先级
         */
        $(document).on('click', '#confirmPriority', function () {
            var $input = $(this).prev();
            var priority = $input.val();
            var id = $input.attr('rowId');
            utils.post(window.host + '/product/updatePriority', function (result) {
                if (result && result === true) {
                    utils.tools.success('修改成功');
                    return;
                }
                utils.tools.error('修改失败');
            }, {id: id, priority: priority})
        });

        //buttonRoleCheck('.hideClass');

        //初始化商品分类信息
        $.ajax({
            url: window.host + '/shop/category/list?taxonomy=' + taxonomy,
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.errorCode == 200) {
                    var dataLength = data.data.length;
                    var $categoryType = $('#categoryType');
                    $categoryType.empty();
                    $categoryType.append(
                        '<option value="" selected="selected">所有分类</option>');
                    for (var i = 0; i < dataLength; i++) {
                        $categoryType.append('<option value=' + data.data[i].id + '>'
                            + data.data[i].name + '</option>');
                    }
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                }
            },
            error: function (state) {
                if (state.status == 401) {
                } else {
                    utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
                }
            }
        });

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "暂无相关数据"
            }
        });

        // 初始化滤芯选择
        if (productType === 'NORMAL') {
            productModal.create({
                triggerDom: '.bindFilter',
                type: 'FILTER',
                onConfirm: function (selectedFilter) {
                    bindFilterAll(productId, selectedFilter);
                }
            });
        }

        var $datatables = utils.createDataTable('#xquark_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            ordering: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get($listUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    decodeId: sessionStorage.getItem('sKeywordId') ? sessionStorage.getItem('sKeywordId') : '',
                    keyword: data.search.value,
                    pageable: true,
                    status: $status,
                    reviewStatus: $reviewStatus,
                    brand: $brandType,
                    supplier: $supplierType,
                    order: function () {
                        if ($order !== '') {
                            return $order;
                        } else {
                            var _index = data.order[0].column;
                            if (_index < 4) {
                                return '';
                            } else {
                                return $orders[_index - 4];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir : 'asc',
                    category: $category,
                    isGroupon: $groupon
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else {
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.categoryTotal,
                        iTotalDisplayRecords: res.data.categoryTotal
                    });
                });
            },
            rowId: "id",
            columns:
                [
                    {
                        title: '<label class="checkbox"><input id="checkAllGoods" name="checkAllGoods" type="checkbox" ></label>',
                        width: "10px",
                        orderable: false,
                        render: function (data, type, row) {
                            return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                                + row.id + '" value="' + row.id + '"></label>';
                        }
                    },
                    {
                        width: "30px",
                        orderable: false,
                        render: function (data, type, row) {
                            return row.imgUrl
                                ? `<img class="goods-image" src="${row.imgUrl}" />`
                                : `<img class="goods-image" src="${NOT_FOUND}" />`;
                        }
                    },
                    {
                        title: "ID",
                        data: "decodeId",
                        orderable: false,
                        name: "decodeId"
                    },
                    {
                        title: "商品名称",
                        data: "name",
                        orderable: false,
                        name: "name"
                    },
                    {
                        title: "商品状态",
                        orderable: false,
                        render: function (data, type, row) {
                            var status = '';
                            switch (row.status) {
                                case 'INSTOCK':
                                    status = '下架';
                                    break;
                                case 'ONSALE':
                                    status = '已上架';
                                    break;
                                case 'FORSALE':
                                    status = '待上架发布';
                                    break;
                                case 'DRAFT':
                                    status = '草稿';
                                    break;
                                case 'SOLDOUT':
                                    status = '已售罄';
                                    break;
                                default:
                                    break;
                            }
                            return status;
                        },
                    },
                    {
                        title: "审核状态",
                        orderable: false,
                        render: function (data, type, row) {
                            var status = '';
                            switch (row.reviewStatus) {
                                case 'NO_NEED_TO_CHECK':
                                    status = '未提交审核';
                                    break;
                                case 'WAIT_CHECK':
                                    status = '待审核';
                                    break;
                                case 'CHECK_FAIL':
                                    status = '审核未通过';
                                    break;
                                case 'CHECK_PASS':
                                    status = '审核通过';
                                    break;
                                default:
                                    break;
                            }
                            return status;
                        }
                    }
                    ,
                    {
                        title: "商品供应商",
                        orderable: false,
                        name: 'source',
                        render: function (data, type, row) {
                            const id = row.supplierId;
                            if (id && id !== '') {
                                let name;
                                if (name=sourceMap.get(id)) {
                                    return name;
                                }
                            }
                            return '无';
                        }
                    },
                    {
                        title: "售价",
                        data: "price",
                        orderable: false,
                        name: "price"
                    }, {
                    title: "库存",
                    data: "amount",
                    orderable: false,
                    name: "amount"
                },
                    {
                        title: "销售量",
                        data: "sales",
                        orderable: false,
                        name: "sales"
                    },
                    {
                        title: "操作",
                        orderable: false,
                        render: function (data, type, row) {
                            var html = '';
                            if (row.reviewStatus === 'WAIT_CHECK') {
                                html += '<a href="javascript:void(0);" class="pass-review role_check_table" style="margin-left: 10px;" data-toggle="pass" rowId="'
                                    + row.id
                                    + '" fid="up_shelves"><i class="icon-pencil7"></i>通过审核</a>';
                                html += '<a href="javascript:void(0);" class="fail-review role_check_table" style="margin-left: 10px;" data-toggle="fail" rowId="'
                                    + row.id
                                    + '" fid="up_shelves"><i class="icon-pencil7"></i>未通过审核</a>';
                            }
                            if (row.reviewStatus === 'CHECK_PASS' && (row.status
                                === 'INSTOCK' || row.status === 'DRAFT')) {
                                html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="uppopover" rowId="'
                                    + row.id
                                    + '" fid="up_shelves"><i class="icon-pencil7"></i>上架</a>';
                            }
                            if (row.status === 'ONSALE') {
                                html += '<a href="javascript:void(0);" class="down role_check_table" style="margin-left: 10px;" data-toggle="downpopover" rowId="'
                                    + row.id
                                    + '" fid="off_shelves" ><i class="icon-pencil7"></i>下架</a>';
                            }
                            html += '<a href="/sellerpc/mall/product/edit?type='
                                + productType
                                + '&pId=' + row.id
                                + '" target="_blank" class="edit role_check_table" style="margin-left: 10px;" rowId="'
                                + row.id
                                + '" fid="edit_product"><i class="icon-pencil7" ></i>编辑</a>';
                            html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'
                                + row.id
                                + '" fid="delete_product"><i class="icon-trash"></i>删除</a>';
                            return html;

                        }

                    }
                ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {
            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            /**$(".edit").on("click",function(){
            var pId =  $(this).attr("rowId");
            window.location.href = '/sellerpc/mall/product/edit?pId='+pId;
        });**/

            /*商品团购*/
            $(".groupon").on("click", function () {
                var id = $(this).attr("rowId");
                $('.daterange-time').val('');
                $("#groupon_product_id").val(id);
                $("#groupon_discount").val('');
                $("#groupon_numbers").val('');
                $("#groupon_amount").val('');
                $("#modal_grouponAdd").modal("show");
            });

            /** 点击上架弹出框 **/
            $("[data-toggle='uppopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认上架吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="'
                        + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="uppopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="uppopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').off("click").on("click", function () {
                    var id = $(this).attr("mId");
                    upProduct(id);
                });
                $('.popover-btn-cancel').off("click").on("click", function () {
                    $(that).popover('hide');
                });
            });

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "uppopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="uppopover"]').popover('hide');
                } else if (target.data("toggle") == "uppopover") {
                    target.popover("show");
                }
            });

            /** 点击下架弹出框 **/
            $("[data-toggle='downpopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认下架吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="'
                        + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="downpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="downpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').off("click").on("click", function () {
                    var id = $(this).attr("mId");
                    downProduct(id);
                });
                $('.popover-btn-cancel').off("click").on("click", function () {
                    $(that).popover("hide");
                });
            });

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "downpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="downpopover"]').popover('hide');
                } else if (target.data("toggle") == "downpopover") {
                    target.popover("toggle");
                }
            });

            /** 点击审核通过弹出框 **/
            $("[data-toggle='pass']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认通过吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                        + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="pass"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="pass"]').popover('hide');

                }).on('shown.bs.popover', function () {
                let that = this;
                $('.popover-btn-ok').off("click").on("click", function () {
                    let pId = $(this).attr("pId");
                    updateProductReviewStatus(pId, 'CHECK_PASS');
                });
                $('.popover-btn-cancel').off("click").on("click", function () {
                    $(that).popover('hide');
                });
            });

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "pass") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="pass"]').popover('hide');
                } else if (target.data("toggle") == "pass") {
                    target.popover("show");
                }
            });

            /** 点击审核失败弹出框 **/
            $("[data-toggle='fail']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认失败吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                        + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="fail"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="fail"]').popover('hide');

                }).on('shown.bs.popover', function () {
                let that = this;
                $('.popover-btn-ok').off("click").on("click", function () {
                    let pId = $(this).attr("pId");
                    updateProductReviewStatus(pId, 'CHECK_FAIL');
                });
                $('.popover-btn-cancel').off("click").on("click", function () {
                    $(that).popover('hide');
                });
            });

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "fail") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="fail"]').popover('hide');
                } else if (target.data("toggle") == "fail") {
                    target.popover("show");
                }
            });

            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'
                        + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var pId = $(this).attr("pId");
                    deleteProduct(pId);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_list_tables');

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });

            /**
             * 监听全选
             */
            $(checkAll).on('click', function () {
                $("input[name='checkGoods']").prop("checked",
                    $(this).prop("checked"));
            });

        }

        $(".btn-search").on('click', function () {
            var keyword = $.trim($("#sKeyword").val());
            var sKeywordId = $.trim($("#sKeywordId").val());
            var regsKeywordId=/^[0-9]*$/
            sessionStorage.setItem('sKeywordId', sKeywordId)
            if (sKeywordId !== '') {
                if (!sKeywordId.match(regsKeywordId)) {
                    utils.tools.alert('id仅支持纯数字，请重新输入', {timer: 1200, type: 'warning'});
                } else $datatables.search(keyword).draw()
            }
            if (keyword !== '' && sKeywordId === '') $datatables.search(keyword).draw();
        });

        $(document).on('click', '.bindFilter', function () {
            productId = $(this).attr('rowId');
        });

        /*商品删除会判断 userId 否则删除失败*/
        function deleteProduct(pId) {
            var url = window.host + "/product/delete/" + pId;
            utils.postAjax(url, {}, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }

        /*上架商品*/
        function upProduct(pId) {
            var data = {
                'ids': pId
            };
            var url = window.host + "/product/batch-onsale";
            utils.postAjax(url, data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("上架成功!", {timer: 1200, type: 'success'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("上架失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }

        /*改变商品的审核状态*/
        function updateProductReviewStatus(pId, status) {
            $.get($updateReviewStatusUrl(pId, status)
            ).done(
                data => {
                utils.tools.alert("改变审核状态成功!", {timer: 1200, type: 'success'});
            $datatables.search('').draw();
        }
        ).fail(
                data => {
                utils.tools.alert("改变审核状态失败!", {timer: 1200, type: 'warning'});
        });
        }

        /*下架商品*/
        function downProduct(pId) {
            var data = {
                'ids': pId
            };
            var url = window.host + "/product/batch-instock";
            utils.postAjax(url, data, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("下架成功!", {timer: 1200, type: 'success'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("下架失败!", {timer: 1200, type: 'warning'});
                    }
                }
            });
        }

        function bindFilterAll(pId, fIds) {
            utils.postAjaxJson(window.host + '/product/filter/bind',
                {productId: pId, filterIds: Array.from(fIds)}, function (res) {
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                            if (res.data && res.data === true) {
                                utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                            }
                        } else {
                            const message = res.moreInfo || '服务器错误,请稍后再试';
                            utils.tools.alert(message, {timer: 1200, type: 'warning'});
                        }
                    } else if (res === -1) {
                        utils.tools.alert('网络错误, 请稍后再试',
                            {timer: 1200, type: 'warning'});
                    }
                });
        }

        function bindFilter(pId, fId) {
            utils.postAjax(window.host + '/product/filter/bind',
                {pId: pId, fId: fId}, function (res) {
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {

                        } else {
                            const message = res.moreInfo || '服务器错误,请稍后再试';
                            utils.tools.alert(message, {timer: 1200, type: 'warning'});
                        }
                    } else {
                        utils.tools.alert('网络错误, 请稍后再试',
                            {timer: 1200, type: 'warning'});
                    }
                });
        }

        $('body').on('change', 'select[name="categoryType"]', function (event) {
            $category = $(this).val();
            $listUrl = window.host + "/product/list?type=" + productType;
            $datatables.search('').draw();
        });

        $('select[name=filterSpec]').on('change', function () {
            var spec = $(this).val();
            var slt = $('select[name="productStatus"]').val();
            if (spec === '') {
                $listUrl = window.host + "/product/list?type=" + productType;
            } else {
                $listUrl = window.host + `/filter/list?spec=${spec}&status=${slt}`;
            }
            $datatables.search('').draw();
        });

        $('select[name="reviewStatus"],select[name="productStatus"],select[name="productStatus"],select[name="brandType"],select[name="supplierType"]').on('change', function () {
            $status = $("#productStatus").val();
            $reviewStatus = $('#reviewStatus').val();
            $brandType = $("#brandType").val();
            $supplierType = $("#supplierType").val();
            $datatables.search($("#sKeyword").val()).draw();
        });


        $(".saveGrouponBtn").on('click', function () {
            var url = window.host + '/product/groupon/save';
            var productId = $("#groupon_product_id").val();
            var validFrom = $("#groupon_valid_from").val();
            var validTo = $("#groupon_valid_to").val();
            var discount = $("#groupon_discount").val();
            var numbers = $("#groupon_numbers").val();
            var amount = $("#groupon_amount").val();
            if (!validFrom || validFrom == '' || !validTo || validTo == '') {
                utils.tools.alert("请选择团购时间!", {timer: 1200, type: 'warning'});
                return;
            } else if (!discount || discount == '') {
                utils.tools.alert("请输入团购价格!", {timer: 1200, type: 'warning'});
                return;
            } else if (!numbers || numbers == '') {
                utils.tools.alert("请输入参团人数!", {timer: 1200, type: 'warning'});
                return;
            } else if (!amount || amount == '') {
                utils.tools.alert("请输入团购库存!", {timer: 1200, type: 'warning'});
                return;
            }

            var data = {
                productId: productId,
                validFrom: validFrom,
                validTo: validTo,
                discount: discount,
                numbers: numbers,
                amount: amount,
            };
            utils.postAjaxWithBlock($(document), url, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            alert("操作成功");
                            window.location.href = window.originalHost + '/mall/groupon';
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });

        });

    });
/**
 * 封装表格基本参数
 */
define('utils/dataTable',['jquery', 'utils', 'ramda', 'datatables'], function ($, utils, R) {

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            infoEmpty: "",
            emptyTable: "暂无数据"
        }
    });

    const basicOption = {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ordering: false,
        sortable: false,
        scrollX: true,
        scrollCollapse: true,
        fixedColumns: {
            leftColumns: 0,
            rightColumns: 1
        },
        rowId: 'id',
        select: {
            style: 'multi'
        }
    };

    class DataTable {
        /**
         * 构造函数
         * @param params
         * @param params.lazyInit 懒加载, 若指定该属性则在第一次刷新时才加载
         * @param params.dom
         * @param params.url
         * @param params.option
         * @param params.reqParams //请求参数
         * @param params.drawBack // 完成回掉
         * @param params.columns // 表格列
         * @param params.autoFit // 表格是否子适应
         * @param params.showSelector // 是否需要选择框
         */
        constructor(params) {
            this.$dom = $(params.dom);
            const option = $.extend({}, basicOption, params.option || {});

            // 部分界面需要关闭自适应大小
            if (params.autoFit === false) {
                option.fixedColumns = 0;
                option.scrollX = false;
                option.scrollCollapse = false;
            }

            if (params.reqParams) {
                this.reqParams = params.reqParams;
            }

            // 如果没有url参数则作为展示表格
            let isView = true;
            if (params.url && params.url !== '') {
                isView = false;
                option.ajax = (data, callback) => {
                    const reqParams = $.extend({
                        size: data.length,
                        page: data.start / data.length,
                    }, this.reqParams || {});
                    $.get(params.url, reqParams, function (res) {
                        if (res.errorCode !== 200) {
                            utils.tools.alert('数据加载失败');
                            return;
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    })
                };
            }
            if (isView) {
                option.serverSide = false;
                option.paging = false;
                option.searching = false;
            }

            const showSelector = params.showSelector;
            // 列头加上选择框
            if (showSelector && showSelector === true
                && params.columns) {
                params.columns.unshift({
                    title: '<span>全选<label class="checkbox"><input name="checkAll" type="checkbox" class="styled"></label></span>',
                    name: 'check',
                    width: 15,
                    render: function (data, type, row) {
                        return '<label class="checkbox"><input name="checkColumn" type="checkbox" class="styled" value="'
                            + row.id + '"></label>';
                    }
                });

                $(document).on('click', 'input[name=checkAll]', function () {
                    const isSelfChecked = $(this).is(':checked');
                    $('input[name=checkColumn]').prop('checked', isSelfChecked);
                })
            }

            option.columns = params.columns;
            option.drawCallback = params.drawBack || function () {
            };
            this.isView = isView;
            this.option = option;
            if (!params.lazyInit) {
                this.init();
            }
        }

        init() {
            this.$tableInstence = this.$dom.DataTable(this.option);
            this.inited = true;
        }

        /**
         * 更新参数
         * @param params
         * @returns {DataTable}
         */
        update(params) {
            this.reqParams = params;
            return this;
        }

        /**
         * 刷新当前页
         */
        refresh(reqParams) {
            if (this.isView) {
                throw Error('展示表格无法刷新');
            }
            if (!this.inited) {
                this.init();
                return;
            }
            if (reqParams) {
                this.reqParams = $.extend(this.reqParams, reqParams);
            }
            return new Promise((resolve => {
                resolve(this.$tableInstence.search('').draw('page'));
        }));
        }

        /**
         * 刷新全部
         */
        refreshAll(reqParams) {
            if (this.isView) {
                throw Error('展示表格无法刷新');
            }
            if (!this.inited) {
                this.init();
                return;
            }
            if (reqParams) {
                this.reqParams = $.extend(this.reqParams, reqParams);
            }
            this.$tableInstence.search('').draw();
            return this;
        }

        /**
         * 列表获取表格全部数据
         * @param mapFunc 数据映射函数
         */
        getDataAll(mapFunc = x => x) {
        const data = this.$tableInstence.rows().data();
        return Array.prototype.map.call(data, mapFunc);
    }

    /**
     * 为当前表格添加一列
     */
    addRow(row, callback) {
        this.$tableInstence.row.add(row).draw();
        if (callback) {
            callback();
        }
        return this;
    }

    /**
     * 根据函数修改行数据
     * @param match 查找函数
     * @param modify 修改函数
     */
    modifyRow(match, modify) {
        this.$tableInstence.rows().every(function () {
            const d = this.data();
            if (match && match(d) === true) {
                if (modify) {
                    const newRow = modify(d);
                    this.data(newRow);
                }
            }
        });
        return this;
    }

    /**
     * 根据行节点来匹配修改
     * @param match
     * @param modify
     * @returns {DataTable}
     */
    modifyRowByNode(match, modify) {
        this.$tableInstence.rows().every(function () {
            const node = this.node();
            const data = this.data();
            if (match && match(node) === true) {
                if (modify) {
                    const newRow = modify(data);
                    this.data(newRow);
                }
            }
        });
        return this;
    }

    /**
     * 删除某一行
     * @param selector
     */
    removeRow(selector) {
        this.row(selector)
            .remove()
            .draw();
    }

    /**
     * 删除所有选中行
     */
    removeRows(filter = () => true) {
        const _this = this;
        $('input[name=checkColumn]').each(function (i, item) {
            if(filter(item)) {
                _this.removeRow($(item).parents('tr'));
            }
        });
    }

    /**
     * 选中的行数
     * @returns {jQuery}
     */
    checkedRowNum() {
        return $('input[name=checkColumn]:checked').length;
    }

    /**
     * 获取表格行对象
     * @param selector 行选择器
     * 参考 https://datatables.net/reference/type/row-selector;
     * https://datatables.net/examples/ajax/null_data_source.html
     * @returns {*}
     */
    row(selector) {
        return this.$tableInstence.row(selector);
    }

    /**
     * 获取表格行数据
     * @param selector
     * @returns {*}
     */
    rowData(selector) {
        return this.row(selector).data();
    }

    /**
     * 返回表格行数
     */
    rowCount() {
        if (!this.inited) {
            return 0;
        }
        return this.$tableInstence.data().count();
    }

}

    return {
        /**
         * 构造一个新的表格实例
         * @param params
         * @returns {DataTable}
         */
        create: function (params) {
            return new DataTable(params);
        }
    }

});

define('utils/productModal',['jquery', 'utils', 'ramda', 'utils/dataTable'],
    function ($, utils, R, dataTable) {

        const modalId = 'choose-product-modal';
        const modalTableId = 'choose-product-table';
        const searchBtnId = 'searchBtn';
        const filterSelectId = 'chooseBtn';
        const filterUnSelectId = 'unChooseBtn';
        const selectClass = 'selectPd';
        const unselectClass = 'unSelectPd';
        const confirmBtnId = 'confirmBtn';

        /**
         * 构建弹窗html
         * @type {string}
         */
        const modalTemplate = `
      <div class="modal fade" id="${modalId}">
        <div class="modal-dialog" style="width:1000px;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="modal-title">选择商品</h5>
            </div>
            <div class="modal-body form-horizontal">
              <!-- search bar -->
              <div id="inner-search-bar" hidden="hidden">
                <div class="form-group" style="margin-bottom:0;margin-left:5px;">
                  <div class="panel-heading">
                    <div class="row" style="margin-bottom: 10px">
                      <div id="choose-container" hidden="hidden">
                        <div class="col-md-1" style="margin-right: 10px;">
                          <a href="javascript:" class="btn btn-sm btn-default active" id="${filterSelectId}"><i
                            class="position-left"></i>已选择</a>
                        </div>
                        <div class="col-md-1">
                          <a href="javascript:" class="btn btn-sm btn-default" id="${filterUnSelectId}"><i
                            class="position-left"></i>未选择</a>
                        </div>
                       </div>
                    </div>
                    <div class="row">
                      <div id="search-container">
                        <!-- append component here -->
                      </div>
                      <div class="col-md-2">
                        <a href="javascript:" class="btn btn-sm btn-default" id="${searchBtnId}"><i
                          class="icon-search4 position-left"></i> 搜索</a>
                      </div>
                    </div>
                  </div>
                </div>             
              </div>
              <!-- dataTable -->
              <div id="inner-table">
                <div class="form-group">
                  <div class="datatable-scroll">
                    <table class="table datatable-basic dataTable no-footer " id="${modalTableId}"
                           role="grid" data-page-length='8'>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-primary btn-ok" data-dismiss="modal" id="${confirmBtnId}">确认</button>
            </div>
          </div>
        </div>
       </div>`;

        /**
         * 构造modal html
         */
        class TemplateBuilder {

            constructor(parent, selectedUrl, maxColumns) {
                this.$template = $(modalTemplate);
                this.$searchContainer = this.$template.find('#search-container');
                // 搜索请求参数
                this.searchParams = {};
                this.selected = {};
                this.seletedUrl = selectedUrl;
                this.maxColumns = maxColumns;

                parent.append(this.$template);
            }

            /**
             * 配置开启搜索栏
             * @returns {TemplateBuilder}
             */
            enableSearchBar() {
                this.$template.find('#inner-search-bar').show();
                return this;
            }

            /**
             * 启用搜索功能
             */
            enableSelect(onSelect = R.always, onUnSelect = R.always) {
                const _this = this;

                const refreshTable = combined => {
                    const selectedIds = R.keys(this.selected);
                    let params = {selectedIds: R.join(',', selectedIds)};
                    if (combined !== undefined && combined !== null) {
                        // 通过按钮筛选时同时将表格页数复位
                        params = $.extend(params, {combined: combined});
                        // 从头开始刷新
                        _this.$dataTable.refreshAll(params);
                        return;
                    }
                    // 记住刷新状态
                    _this.$dataTable.refresh(params);
                };

                const clickFunc = callback => e => {
                    const $this = $(e.currentTarget);
                    const id = $this.attr('rowId');
                    callback(id, $this).then(refreshTable)
                        .then(() => console.log('now selected: ', _this.selected));
                };
                this.$template.find('#choose-container').show();

                // 绑定表格中的选择/未选中事件
                $(document).on('click', `.${selectClass}`,
                    clickFunc((id, $this) => {
                        return new Promise(resolve => {
                            const data = _this.$dataTable.rowData($this.parents('tr'));
                if (data) {
                    if (onSelect && $.isFunction(onSelect)) {
                        const ret = onSelect(data, $this);
                        if (!ret || ret === false) {
                            return;
                        }
                    }
                    if (_this.maxColumns &&
                        R.keys(_this.selected).length >= _this.maxColumns) {
                        utils.tools.error('已超过可选择数量');
                        return;
                    }
                    _this.selected[data.id] = data;
                }
                // TODO 完善onSelect的执行顺序
                resolve();
            });
            }));
                $(document).on('click', `.${unselectClass}`,
                    clickFunc((id, $this) => {
                        return new Promise(resolve => {
                            const data = _this.$dataTable.rowData($this.parents('tr'));
                if (data) {
                    const callbackRet = onUnSelect(data);
                    // 如果回调函数返回promise, 则要保证回调在回调函数跟服务器交互后删除本地缓存
                    if (callbackRet && callbackRet instanceof Promise) {
                        callbackRet.then(ret => {
                            if (ret) {
                                delete _this.selected[data.id];
                                resolve(ret);
                            }
                        })
                    } else {
                        delete _this.selected[data.id];
                        resolve(true);
                    }
                }
            });
            }));

                // 绑定已选中/未选中的筛选按钮, 搜索时带上当前选中的参数, 供后台过滤
                this.$template.find(`#${filterSelectId}`).on(
                    'click', e => {
                    $(e.currentTarget).addClass('active');
                $(`#${filterUnSelectId}`).removeClass('active');
                refreshTable(true);
            });
                this.$template.find(`#${filterUnSelectId}`).on(
                    'click', e => {
                    $(e.currentTarget).addClass('active');
                $(`#${filterSelectId}`).removeClass('active');
                refreshTable(false)
            });
                return this;
            }

            /**
             * 启用分类搜索按钮
             */
            withCategoryBtn() {
                const id = 'categorySelector';
                this.categorySelector = id;
                utils.genOptions(`${window.host}/shop/category/list`, R.prop('id'),
                    R.prop('name'))
                    .then(this._fedSelector(id, '所有分类', 'categoryId'));
                return this;
            }

            /**
             * 构造供应商搜索按钮
             */
            withSupplierBtn() {
                const id = 'supplierSelector';
                this.supplierSelector = id;
                utils.genOptions(
                    `${window.host}/supplier/list?pageable=false&type=override`,
                    R.prop('id'),
                    R.prop('name'),
                    R.prop('list'))
                    .then(this._fedSelector(id, '所有供应商', 'supplierId'));
                return this;
            }

            /**
             * 构造搜索框
             * @returns {TemplateBuilder}
             */
            withKeyword() {
                const searchId = 'searchkey';
                const $html = `
          <div class="col-md-3">
            <input type="search" class="form-control" placeholder="请输入名称或SKU编码" id="${searchId}"
              name="${searchId}" value="">
          </div>`;
                this.$searchContainer.append($html);
                this.searchParams.keyword = () => $(`#${searchId}`).val();
                return this;
            }

            /**
             * 启用表格
             */
            withDataTable(params, selectable) {
                // 初始化表格
                const _this = this;
                const columns = params.columns || [];

                // 1. 启用选择列
                if (selectable) {
                    // 全局数组, 保存选中id
                    columns.push({
                        title: '操作',
                        name: "operation",
                        render: function (data, type, row) {
                            const rowId = row.id;
                            if (row.select || _this.selected[rowId]) {
                                return `<a href="javascript:void(0);" class="unSelectPd" rowId="${rowId}">
                    <i class="icon-pencil7" ></i>取消</a>`;
                            }
                            return `<a href="javascript:void(0);" class="selectPd" rowId="${rowId}">
                    <i class="icon-pencil7" ></i>选择</a>`;
                        }
                    });
                }

                if (params.columnNumber) {
                    // 控制表格每页数量
                    _this.$template.find(`#${modalTableId}`).attr('data-page-length',
                        params.columnNumber);
                }

                this.$dataTable = dataTable.create({
                    dom: `#${modalTableId}`,
                    url: params.url,
                    autoFit: false,
                    drawBack: () => {
                    _this.tableInited = true;
                const data = R.filter(x => x.select === true,
                    _this.$dataTable.getDataAll());
                data.forEach(item => {
                    _this.selected[item.id] = item;
            });
                console.log('now selected: ', _this.selected);
            },
                columns: columns,
                    reqParams: params.reqParams
            });
                return this;
            }

            /**
             * 绑定搜索事件
             */
            withSearchEnable() {
                const _this = this;
                this.$template.find(`#${searchBtnId}`).on('click', () => {
                    // 执行闭包, 获取实时值
                    const params = R.map(R.call, _this.searchParams);
                if (!_this.$dataTable) {
                    throw '请先初始化表格';
                }
                _this.$dataTable.refresh(params);
            });
                return this;
            }

            /**
             * 绑定确认事件, 输出选中的skuId
             */
            withConfirm(onConfirm) {
                const _this = this;
                $(`#${confirmBtnId}`).on('click', () => {
                    if (onConfirm && $.isFunction(onConfirm)) {
                    onConfirm(R.values(_this.selected));
                }
            });
                return this;
            }

            /**
             * 私有方法, 用来构造selector
             * @param selectorId 选择框id
             * @param selectorName 选择框名称
             * @param param 请求参数名称
             */
            _fedSelector(selectorId, selectorName, param) {
                const _this = this;
                return function (options) {
                    const $html = $(`
              <div class="col-md-2">
                <select class="select form-control" id="${selectorId}">
                  <option value="" selected="selected">${selectorName}</option>
                </select>
              </div>
            `);
                    const $selector = $html.find(`#${selectorId}`);
                    $selector.append(options);
                    _this.$searchContainer.append($html);
                    // 放到请求参数列表
                    if (param && param !== '') {
                        // 值必须为一个闭包, 在点击时执行实时获取
                        _this.searchParams[param] = () => $selector.val()
                    }
                }
            }

        }

        /**
         * modal实现类
         */
        class PdModal {

            /**
             * 构造商品选框
             * 约定后台接口必须支持相关的查询参数
             * 如果要实现搜索功能, 则后台返回对象中必须包含select属性, 指定当前对象是否
             * 已在数据库中被选中
             * 同时后台接口需要接受selectedIds参数, 针对该参数
             * 在选择过滤时将用户选中的值同时返回
             * 在未选择过滤时将用户数据中由于用户操作选中的部分过滤
             *
             * @param params.dom modal的dom地址
             * @param params.selectable 启用选择组件
             * @param params.onSelect [optional] 选择后的回调函数
             * @param params.onUnSelect [optional] 取消选择后的回调函数
             * @param params.selectParamSupplier 选中参数supplier
             * @param params.unSelectParamSupplier 未选中参数supplier
             * @param params.onConfirm modal点击确认的事件
             * @param params.selectedUrl 查询已选中id的url
             * @param params.table 表格参数
             * @param params.table.url 表格刷新地址
             * @param params.table.drawback 表格回调地址
             * @param params.table.columns 表格列数据
             * @param params.table.columnNumber 表格列数
             * @param params.table.reqParams 表格请求参数
             */
            constructor(params) {
                this.dom = params.dom;
                this.$dom = $(params.dom);
                // init modal html first

                const builder = new TemplateBuilder(this.$dom, params.selectedUrl,
                    params.table.columnNumber)
                    .enableSearchBar()
                    .withDataTable(params.table, params.selectable)
                    .withSearchEnable()
                    .withKeyword()
                    .withCategoryBtn()
                    .withSupplierBtn()
                    .withConfirm(params.onConfirm);

                if (params.selectable) {
                    builder.enableSelect(params.onSelect, params.onUnSelect);
                }

            }

            /**
             * 显示弹框
             */
            show() {
                $(`#${modalId}`).modal('show');
            }

            /**
             * 隐藏弹框
             */
            hide() {
                $(`#${modalId}`).modal('hide');
            }

        }

        return {
            create: function (params) {
                return new PdModal(params);
            }
        }

    });
/**
 * Created by jitre
 */
define('product/xlsxUpload',['jquery', 'utils', 'xlsx'], function ($, utils, xlsx) {

    let productDataPropertyClassMap=new Map([["价格","productPrice"],["兑换价","conversionPrice"],["立省","point"],["推广费","promoAmt"],["服务费","serverAmt"],
        ["净值","netWorth"],["SKU编码","skuCode"],["条形码","barCode"],["库存","productAmount"],["安全库存","secureAmount"],["长度","length"],
        ["宽度","width"],["高度","height"],["重量","weight"],["装箱数","numInPackage"]]);
    let skuModelInitFunc;
    let skuCellInitFunc;
    let attr_active;
    let skuAttributeItemIds;
    let skuAttributeIds;
    let dPointCalc;

    return {
        setSkuAttributeItemIds:function(data){
            skuAttributeItemIds=data;
        },
        setSkuAttributeIds:function(data){
            skuAttributeIds=data.data;
        },
        setSkuCellInitCallBack: function (callback) {
            skuCellInitFunc=callback;
        },
        setSkuModelInitCallBack: function (callback,attrs) {
            skuModelInitFunc=callback;
            attr_active=attrs;
        },
        setdPointCalc:function(func){
            dPointCalc=func;
        },
        /***
         * 注册上传事件的监听
         * @param element
         * @returns {addUploadListener}
         */
        addExcelUploadEventListener: function (element) {
            element.change(function (e) {
                let files = e.target.files;
                let fileReader = new FileReader();
                let dataResult = []; //获取数据
                fileReader.onload = function (ev) {
                    let workBook;
                    try {
                        let data = ev.target.result;
                        workBook = XLSX.read(data, {
                            type: 'binary'
                        }); //二进制读取表格内容
                    } catch (e) {
                        utils.tools.alert('文件类型不正确', {
                            timer: 3600,
                            type: 'warning'
                        });
                        return;
                    }
                    //表格范围
                    let firstCol = '';
                    let readSuccess=true;
                    //读取表中数据
                    for (let sheet in workBook.Sheets) {
                        if (workBook.Sheets.hasOwnProperty(sheet)) {
                            if(!workBook.Sheets[sheet].hasOwnProperty('S1')){
                                firstCol = workBook.Sheets[sheet]['A1'].v;
                                dataResult = dataResult.concat(XLSX.utils.sheet_to_json(workBook.Sheets[sheet]));
                            }else{
                                utils.tools.alert('商品的规格不能大于三个', {
                                    timer: 3600,
                                    type: 'warning'
                                });
                                readSuccess=false;
                                break;
                            }
                        }
                    }
                    if(readSuccess&&dataResult.length>0){
                        let skuSpec=[];
                        console.log(dataResult);
                        //获取所有的规格数据,生成对应的skuModel
                        let properties=Object.getOwnPropertyNames(dataResult[0]);
                        properties.forEach(function(p){
                            if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                                skuSpec.push(p);
                            }
                        });
                        $(".sku_item").find('a').removeClass('itemactive');
                        $(".sku_item").find('.sku_index').remove();
                        skuSpec.forEach(function(element,index){
                            attr_active.push(element);
                            $(`a[data-sku=${element}]`).addClass("itemactive");
                            $(`a[data-sku=${element}]`).after(`<span class="sku_index">${++index}</span>`);
                        });
                        skuModelInitFunc();
                        //选中规格中的attribute,生成对应的skuCell
                        dataResult.forEach(function(row){
                            let properties=Object.getOwnPropertyNames(row);
                            properties.forEach(function(p){
                                if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                                    let dataId=skuAttributeIds[p];
                                    let id=skuAttributeItemIds[`${dataId}-${row[p]}`];
                                    $(`.sku_item a[data-id=${id}]`).addClass("itemactive");
                                }
                            });
                        });
                        skuCellInitFunc();
                        //建立attribute Id拼接值到每一个row的map映射
                        let attributeIdsMap=new Map();
                        dataResult.forEach(function(row){
                            let properties=Object.getOwnPropertyNames(row);
                            let mapKeyArry=[];
                            properties.forEach(function(p){
                                if(!productDataPropertyClassMap.has(p)&&p!=='__rowNum__'){
                                    console.log(`${p}:${row[p]}`);
                                    mapKeyArry.push(skuAttributeItemIds[skuAttributeIds[p]+"-"+row[p]]);
                                }
                            });
                            attributeIdsMap.set(mapKeyArry.join("-"),row);
                        });
                        //赋值
                        attributeIdsMap.forEach(function(value,key,map){
                            let cell=$(`div[id=${key}]`);
                            let row=attributeIdsMap.get(key);
                            productDataPropertyClassMap.forEach(function(value,key){
                                cell.find(`input[name=${value}]`).prop("value",row[key]);
                            });
                        });
                        //动态计算德分
                        dPointCalc();
                        //删除多余的行
                        $(".sku_cell").each(function(index,element){
                            let id=$(element).prop("id");
                            if(!attributeIdsMap.has(id)){
                                $(element).remove();
                            }
                        });
                        //校验sku表格内的数据合法性
                        $(".sku_tablecell input").valid();
                    }else{
                        return ;
                    }

                };
                if(files.length>0){
                    fileReader.readAsBinaryString(files[0]);
                }
            })
        }
    }
});
define('utils/storageHelper',['jquery', 'utils', 'ramda'], function ($, utils, R) {

    class Storage {

        constructor(name) {
            if (name === 'local') {
                this._storage = localStorage;
            } else if (name === 'session') {
                this._storage = sessionStorage
            } else {
                throw `storage name ${name} not support`
            }
        }

        get(key) {
            if (!key || key === '') {
                return null;
            }
            const ret = this._storage.getItem(key);
            if (!ret || ret === '') {
                return ret;
            }
            return JSON.parse(ret);
        }

        remove(key) {
            this._storage.removeItem(key);
        }

        set(key, value) {
            if (!key || key === '') {
                return;
            }
            if (!value || value === '') {
                return;
            }
            this._storage.setItem(key, JSON.stringify(value));
        }

    }

    function buildRet(name) {
        return function (key) {
            const storage = new Storage(name);
            return {
                get: () => storage.get(key),
                set: val => storage.set(key, val),
                remove: () => storage.remove(key)
        }
        }
    }

    return {

        local: buildRet('local'),

        session: buildRet('session')

    }

});

/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


    return function(formObj,config) {

        formObj.validate({
            errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
            successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
            highlight: function (element, errorClass, validClass) {
                //$(errorLabel).addClass(errorClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                //$(errorLabel).removeClass(errorClass);
            },
            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                    if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent().parent().parent());
                    }
                    else {
                        error.appendTo(element.parent().parent().parent().parent().parent());
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo(element.parent().parent().parent());
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent());
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
            success: function (label) {
                $(label).addClass(this.validClass);
                if ( !(config && config.successClass && config.successClass.length > 0)) {
                    $(label).css("display", "block");
                } else{
                    if (config.setSuccessText){
                        config.setSuccessText(label);
                    } else{
                        $(label).text("ok");
                    }
                }
            },
            showErrors: function (errorMap, errorList) {
                this.defaultShowErrors();
                $.each(errorList, function (i, error) {
                    $(error).css("display", "block");
                });
            },
            focusCleanup: false,
            rules: config.rules,
            messages: config.messages,
            submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
            invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
        });
    }

});
/**
 * Created by quguangming on 16/5/24.
 */
define('product/product-data',['jquery', 'utils'], function ($, utils) {

    var type = $('#type').val();
    var taxonomy = (() => type === 'FILTER' ? 'FILTER' : 'GOODS')();

    return {
        saveProduct: function (data, success, fail) {
            const res = JSON.parse(sessionStorage.getItem('listDatas'))
            let alertStr = ''
            res.map(item=>{
                if (item.id === data.supplierId) {
                if(item.text === '汉德森'){
                    if (data.selfOperated === 0) alertStr = '此商品的供应商是' + item.text + '，确定为非自营商品吗'
                } else  {
                    if (data.selfOperated === 0) {
                        // alertStr = '此商品的供应商是' + item.text + '，确定为非自营商品吗'
                    } else  {
                        alertStr = '此商品的供应商是' + item.text + '，确定为自营商品吗'
                    }
                }
            }
        })
            if (alertStr.length > 8) {
                swal({
                        title: "确认操作",
                        text: alertStr,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: host + '/product/save',
                                type: 'POST',
                                data: data,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.errorCode == 200) {
                                        success && success(data);
                                    } else {
                                        fail && fail(data.moreInfo);
                                    }
                                },
                                error: function (state) {
                                    if (state.status == 401) {
                                        utils.tools.goLogin();
                                    } else {
                                        fail && fail('服务器暂时没有响应，请稍后重试...');
                                    }
                                }
                            });
                        }
                        else {
                            console.log('我不同意')
                        }
                    });
            } else {
                $.ajax({
                    url: host + '/product/save',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        if (data.errorCode == 200) {
                            success && success(data);
                        } else {
                            fail && fail(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tools.goLogin();
                        } else {
                            fail && fail('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
            }
        },
        getProductInfo: function (id, success, fail) {
            $.ajax({
                url: host + '/product/' + id,
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },

        /**
         * [新增分类]
         * @param {[String]} cateName [分类名称]
         * @param {[Function]} success  [成功回调]
         * @param {[Function]} fail     [失败回调]
         */
        addProCate: function (cateName, success, fail) {
            $.ajax({
                url: host + '/shop/category/save',
                type: 'POST',
                data: {
                    name: cateName
                },
                dataType: 'JSON',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取店铺分类信息失败！');
                    }
                }
            })
        },
        /**
         * 获取店铺分类信息
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getShopProCateList: function (shopId, success, fail) {
            var data = {
                shopid: shopId
            }
            $.ajax({
                url: window.host + '/shop/category/list?taxonomy=' + taxonomy,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取店铺分类信息失败！');
                    }
                }
            });
        },
        /**
         * 获取品牌列表
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getBrandList: function (success, fail) {
            $.ajax({
                url: window.host + '/brand/list',
                data: {
                    pageable: false,
                },
                type: 'GET',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data.data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取品牌信息失败！');
                    }
                }
            });
        },
        /**
         * 获取仓库列表
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getWarehouseList: function (selectedWarehouse, success, fail) {
            $.ajax({
                url: window.host + '/warehouse/list',
                data: {
                    pageable: false,
                },
                type: 'GET',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(selectedWarehouse, data.data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取品牌信息失败！');
                    }
                }
            });
        },
        /**
         * 获取供应商列表
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getSupplierList: function (selectedSupplier, success, fail) {
            $.ajax({
                url: window.host + '/supplier/list?type=override',
                type: 'GET',
                data: {
                    pageable: false,
                },
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(selectedSupplier, data.data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取品牌信息失败！');
                    }
                }
            });
        },
        /**
         * 获取商品关联的品牌列表
         * @success  {Function}  [成功回调]
         * @fail  {Function}     [失败回调]
         * author baize
         */
        getProductBrandList: function (id, success, fail) {
            $.ajax({
                url: `${window.host}/brand/list/product/${id}`,
                type: 'GET',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data.data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                    } else {
                        fail && fail('获取品牌信息失败！');
                    }
                }
            });
        },
        //删除段落描述
        delDesc: function(id, success, fail) {
            $.ajax({
                url: host + '/fragment/delete',
                type: 'POST',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function(state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },
        saveProductFragment: function (data, success, fail) {
            $.ajax({
                url: host + '/productFragment/save',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        },
        viewProductFragment: function (id, success, fail) {
            $.ajax({
                url: host + '/fragment/' + id,
                type: 'POST',
                data: null,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        success && success(data);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        fail && fail('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }
    }
});
/**
 * Created by quguangming on 16/5/24.
 */
define('product/form',['jquery', 'utils', 'form/validate', 'utils/storageHelper',
        'jquerySerializeObject', 'switch',
        'select2', 'steps', 'product/product-data'],
    function ($, utils, validate, storageHelper, serializeObject, sw, sel,
              steps, proData) {

        $('.product-Category').select2({width: '35%'});

        $("#proRecomend").bootstrapSwitch();

        var $form = $("#product_info_form");

        var isSubmit = false;

        var productType = $('#type').val();

        $(".btn-submit").on("click", function () {
            let id = $(this).prop("id");//返回被选中元素的属性值  id
            isSubmit = "save" !== id;
        });

        $('body').on('click', '.select2-container .addcate', function () {
            $('.select2-container .addcate').hide();
            $('.select2-container .addcate-editbox').css('display', 'list-item');
            var rowpos = ($('.select2-results__option').length + 2) * 60;
            $('.select2-results').scrollTop(rowpos);
        });

        //新增分类的确定按钮
        $('body').on('click', '.select2-container ul li.addcate-editbox .yesbtn',
            function () {
                var tempCate = $(
                    '.select2-container ul li.addcate-editbox input').val();
                if (!tempCate || utils.tools.getStringLength(tempCate, 1) > 20) {
                    utils.tool.alert('分类名称最多10个汉字或者20个字母。');
                    return false;
                }
                proData.addProCate(tempCate, function (data) {
                    if (data.errorCode == 200) {
                        $('.product-Category').append($("<option></option>")
                            .attr("value", data.data.id)
                            .text(data.data.name));
                        $('.product-Category').select2({width: '35%'});
                        $('.product-Category').val(data.data.id).trigger("change");
                    }
                }, utils.tools.alert);
                return false;
            });

        //新增分类的取消按钮
        $('body').on('click', '.select2-container ul li.addcate-editbox .nobtn',
            function () {
                $('.select2-container ul li.addcate').show();
                $('.select2-container ul li.addcate-editbox').hide();
            });

        //当选择商品分类的“未分类”选项后，清空之前选择的所有项
        $('body').on('change', '.product-Category', function () {
            var productCategory = $("#productCategory").val();
            if (productCategory && (productCategory == 0 || (productCategory
                + '').indexOf('0,') >= 0)) {
                $('#productCategory').val("");
                $(".select2-selection__rendered .select2-selection__choice").remove();
                $(".select2-search__field").attr("placeholder", "未分类");
                $(".select2-search__field").css("width", "100%");
            }
        });

        // 编辑段落描述需要操作图片而图片的dropzone引用在edit.js
        // 编辑功能在edit.js中
        $('#fragment_data_table').on('click', '.fragmentDel', function () {
            var fragmentRow = $(this).closest("tr").get(0);
            utils.tools.confirm("删除该段落描述", function () {
                proData.delDesc($(fragmentRow).attr("id"),
                    function (data) {
                        var $fragmentDatatables = $('#fragment_data_table').DataTable();
                        $fragmentDatatables
                            .row(fragmentRow)
                            .remove()
                            .draw();
                    },
                    function (msg) {
                        utils.tools.alert(msg);
                    });
            }, function () {
            });
        });

        //保存商品
        function saveProduct(form) {
            var formData = $(form).serializeObject();
            var imgArr = [];
            $('#product_image_dropzone > .dz-complete').each(function (i, item) {
                imgArr.push($(item).attr('data-img'));
            });
            var pId = $("#productId").val();
            var productCategory = '';
            if ($("#productCategory").val()) {
                productCategory = $("#productCategory").val().join(",");
            }
            var productBrand = '';
            if ($("#productBrand").val()) {
                productBrand = $("#productBrand").val().join(",");
            }

            var logisticsType = $("input[name='logisticsType']:checked").val();
            var uniformValue = $("#uniformValue").val();
            var templateValue = $("#templateValue").val();
            var gift = $('input[name=gift]:checked').val();
            if (logisticsType == 'UNIFORM' && uniformValue == '') {
                alert('请输入价格');
                return;
            }
            // if (logisticsType == 'TEMPLATE' && !templateValue) {
            //   alert('请选择运费模版');
            //   return;
            // }

            // 根据选择的型号，传入sku的值
            var skus = [];
            /**if($skuLists.length == 0){
            var sku = {};
            sku.marketPrice = formData.productMarketPrice;
            sku.price = formData.productPrice;
            sku.amount = formData.productAmount;
            sku.spec = '无';
            sku.id = '';
            skus.push(sku);
        }else{
            $($skuLists).each(function(index,item){
                        var sku = {};
                        sku.marketPrice = formData.productMarketPrice;
                        sku.price = formData.productPrice;
                        //sku.amount = item.amount;
                        sku.amount = formData.productAmount;
                        sku.spec = item.spec;
                        sku.id = item.id;
                        skus.push(sku);
                    }
            );
        }**/

            var skuType = $("input[name='skuType']:checked").val();
            // 商品选中的规格属性
            var productAttributes = '';
            if (skuType == 'muliSku') {
                $(".sku_container").each(function () {
                    if (productAttributes == '') {
                        productAttributes = $(this).attr("id");
                    } else {
                        productAttributes = productAttributes + "-" + $(this).attr("id");
                    }
                });

                //遍历imgBox,建立所有图片与规格的映射关系
                var skuImgMap=new Map();
                $(".skuImgBox.dz-started").each(function(){
                    let key=$(this).find(".dz-preview").attr("data-img");
                    let attributeDom=$(this).next();
                    let attribute=attributeDom.text();
                    skuImgMap.set(attribute,key);
                });


                // 获取每个sku中的价格，数量，attribute等信息
                var flag = false;
                $(".sku_tablecell .sku_cell").each(function () {
                    if ($(this).is(':visible')) {
                        var spec = '';
                        var id = $(this).attr("sku_id") == undefined ? '' : $(this).attr(
                            "sku_id");
                        var attributes = $(this).attr("id");
                        var price = $(this).find('.productPrice').val();
                        var marketPrice = price;
                        var deductionDPoint = $(this).find(".deductionDPoint").val();
                        var promoAmt = $(this).find(".promoAmt").val();
                        var serverAmt = $(this).find(".serverAmt").val();
                        var point = $(this).find(".point").val();
                        var netWorth = $(this).find(".netWorth").val();
                        var skuCode = $(this).find(".skuCode").val();
                        var barCode = $(this).find(".barCode").val();
                        var amount = $(this).find('.productAmount').val();
                        var secureAmount = $(this).find('.secureAmount').val();
                        var length = $(this).find(".length").val();
                        var width = $(this).find(".width").val();
                        var height = $(this).find(".height").val();
                        var weight = $(this).find(".weight").val();
                        var numInPackage = $(this).find(".numInPackage").val();

                        let specs=$(this).find('.spec');
                        //获取第一个规格的attribute,从而获取他的skuImg信息
                        let imgSpecValue=specs.get(0).innerHTML;
                        let skuImg=typeof(skuImgMap.get(imgSpecValue)) === "undefined"?'':skuImgMap.get(imgSpecValue);

                        specs.each(function () {
                            if (spec == '') {
                                spec = $(this).text();
                            } else {
                                spec = spec + "," + $(this).text();
                            }
                        });
                        if (!price) {
                            alert('请输入商品规格售价');
                            flag = true;
                            return;
                        }
                        if (!deductionDPoint) {
                            alert('请输入商品德分!');
                            flag = true;
                            return;
                        }
                        if (!skuCode) {
                            alert('请输入sku编码!');
                            $(this).find(".skuCode").css("background","#df6c6c");
                            flag = true;
                            return;
                        }else{
                            $(this).find(".skuCode").css("background","#fff");
                        }
                        if (!barCode) {
                            alert('请输入商品条形码!');
                            flag = true;
                            return;
                        }
                        if (!amount) {
                            alert('请输入商品规格库存');
                            flag = true;
                            return;
                        }
                        if (!secureAmount) {
                            alert('请输入商品规格的安全库存');
                            flag = true;
                            return;
                        }
                        if(!length){
                            alert('请输入商品长度');
                            flag = true;
                            return;
                        }
                        if(!width){
                            alert('请输入商品宽度');
                            flag = true;
                            return;
                        }
                        if(!height){
                            alert('请输入商品高度');
                            flag = true;
                            return;
                        }
                        if(!weight){
                            alert('请输入商品重量');
                            flag = true;
                            return;
                        }
                        if(!numInPackage){
                            alert('请输入商品装箱数');
                            flag = true;
                            return;
                        }

                        var sku = {};
                        sku.marketPrice = price;
                        sku.price = price;
                        sku.amount = amount;
                        sku.secureAmount = secureAmount;
                        sku.spec = spec;
                        sku.deductionDPoint = deductionDPoint;
                        sku.point = point;
                        sku.promoAmt = promoAmt;
                        sku.serverAmt = serverAmt;
                        sku.netWorth = netWorth;
                        sku.skuCode = skuCode;
                        sku.barCode = barCode;
                        sku.id = id;
                        sku.attributes = attributes;
                        sku.length = length;
                        sku.width = width;
                        sku.height = height;
                        sku.weight = weight;
                        sku.numInPackage = numInPackage;
                        sku.skuImg = skuImg;
                        skus.push(sku);
                    }
                });
                if (flag) {
                    return;
                }
            }

            if (skuType == 'muliSku' && productAttributes == '') {
                alert('请选择商品规格');
                return;
            }

            // 如果skus为0,则代表该商品没有选择多规格
            if (skus.length === 0) {
                var sku = {};
                sku.marketPrice = formData.price;
                sku.deductionDPoint = formData.deductionDPoint;
                sku.point = formData.point;
                sku.netWorth = formData.netWorth;
                sku.skuCode = formData.skuCode;
                sku.barCode = formData.barCode;
                sku.price = formData.productPrice;
                sku.amount = formData.productAmount;
                sku.secureAmount = formData.secureAmount;
                sku.promoAmt = formData.promoAmt;
                sku.length = typeof(formData.length)=== "undefined"?0:formData.length;
                sku.width = typeof(formData.width)=== "undefined"?0:formData.width;
                sku.height = formData.height;
                sku.weight = typeof(formData.weight)=== "undefined"?0:formData.weight;
                sku.numInPackage = formData.numInPackage;
                sku.serverAmt = formData.serverAmt;
                sku.spec = '无';
                sku.id = formData.singleSkuId;
                sku.attributes = '';
                sku.skuImg='';
                skus.push(sku);
            }

            var setScale = formData.setScale;
            var yundouScale = formData.yundouScale;
            var minYundouPrice = formData.minYundouPrice;
            // if (setScale === 'true') {
            //   if (!yundouScale || yundouScale === '') {
            //     alert('请输入积分兑换比例');
            //     return;
            //   }
            // } else {
            //     yundouScale = null;
            // }
            //
            // if (!minYundouPrice || minYundouPrice === '') {
            //     alert('请输入最低需支付的价格');
            //     return;
            // }
            //
            // const illegalPrice = skus.find(function (t) {
            //   return parseFloat(t.price) < parseFloat(minYundouPrice);
            // });
            // if (illegalPrice) {
            //     alert('商品原价不能低于最小支付价格');
            //     return;
            // }

            // 从本地存储中获取组合信息
            const storage = storageHelper.local('combineInfo');
            let combineInfo = storage.get();
            var  deliveryRegion= JSON.parse(localStorage.getItem('address')).address
            var param = {
                id: pId,
                imgs: imgArr.join(','),
                name: formData.productName,
                model: formData.productModel,
                description: formData.productInfo,
                recommend: formData.proRecomend ? 1 : 0,
                status: formData.status,
                originalPrice: formData.productOriginalPrice || formData.productPrice,
                category: productCategory,
                brand: productBrand,
                //'skus[0].price': formData.productPrice,
                //'skus[0].amount': formData.productAmount,
                //'skus[0].spec1': '无',
                // oneyuanPostage: formData.proOneyuanPostage,
                oneyuanPurchase: formData.proOneyuanPurchase ? 1 : 0,
                specialRate: formData.proSpecialRate || 0,
                commissionRate: formData.commissionRate || 0,
                refundAddress: formData.refundAddress || '',
                refundName: formData.refundName || '',
                refundTel: formData.refundTel || '',
                width: formData.width || '0',
                height: formData.height || '0',
                length: formData.length || '0',
                kind: formData.kind || '',
                point: formData.point || '',
                secureAmount: formData.secureAmount,
                skuImg: formData.skuImg,
                promoAmt: formData.promoAmt,
                serverAmt: formData.serverAmt,
                deductionDPoint: formData.deductionDPoint || '',
                netWorth: formData.netWorth || '',
                numInPackage: formData.numInPackage || 0,
                barCode: formData.barCode || '',
                wareHouseId: formData.wareHouseId || '',
                supplierId: formData.supplierId,
                logisticsType: logisticsType,
                uniformValue: uniformValue,
                supportRefund: formData.supportRefund || true,
                selfOperated: formData.selfOperated,
                detailH5: CKEDITOR.instances.editor.getData(),
                templateValue: templateValue,
                gift: gift || false,
                weight: formData.weight || "0",
                attributes: productAttributes,
                tagList: formData.tagList || [],
                yundouScale: yundouScale || 1,
                minYundouPrice: minYundouPrice || 0,
                type: productType,
                deliveryRegion:deliveryRegion
            };

            if (isSubmit) {
                param.reviewStatus = "WAIT_CHECK";
            } else {
                param.reviewStatus = $("#reviewStatus").prop("value");
            }

            // 多sku情况下重写商品价格、配置字段为首个sku
            if (skuType === 'muliSku') {
                param.price = skus[0].price;
                param.point = skus[0].point;
                param.deductionDPoint = skus[0].deductionDPoint;
                param.netWorth = skus[0].netWorth;
                param.serverAmt = skus[0].serverAmt;
                param.promoAmt = skus[0].promoAmt;
                param.skuImg = skus[0].skuImg;
            }

            if (combineInfo) {
                $(combineInfo).each(function (index, item) {
                    eval("param['slaves[" + index + "].id']='" + item.id + "'");
                    eval("param['slaves[" + index + "].number']='" + item.number + "'");
                });
            }

            $(skus).each(function (index, item) {
                    eval("param['skus[" + index + "].price']='" + item.price + "'");
                    eval("param['skus[" + index + "].point']='" + item.point + "'");
                    eval("param['skus[" + index + "].promoAmt']='" + item.promoAmt + "'");
                    eval("param['skus[" + index + "].serverAmt']='" + item.serverAmt+ "'");
                    eval("param['skus[" + index + "].netWorth']='" + item.netWorth + "'");
                    eval("param['skus[" + index + "].skuCode']='" + item.skuCode + "'");
                    eval("param['skus[" + index + "].barCode']='" + item.barCode + "'");
                    eval("param['skus[" + index + "].marketPrice']='" + item.price + "'");
                    eval("param['skus[" + index + "].deductionDPoint']='"+ item.deductionDPoint + "'");
                    eval("param['skus[" + index + "].amount']='" + item.amount + "'");
                    eval("param['skus[" + index + "].secureAmount']='" + item.secureAmount+ "'");
                    eval("param['skus[" + index + "].spec']='" + item.spec + "'");
                    eval("param['skus[" + index + "].id']='" + item.id + "'");
                    eval("param['skus[" + index + "].attributes']='" + item.attributes+ "'");
                    eval("param['skus[" + index + "].skuImg']='"+ item.skuImg + "'");
                    eval("param['skus[" + index + "].length']='"+ item.length + "'");
                    eval("param['skus[" + index + "].width']='"+ item.width + "'");
                    eval("param['skus[" + index + "].height']='"+ item.height + "'");
                    eval("param['skus[" + index + "].weight']='"+ item.weight + "'");
                    eval("param['skus[" + index + "].numInPackage']='"+ item.numInPackage + "'");
                }
            );
            if (param.selfOperated == 'false') {
                param.selfOperated = 0
            } else  param.selfOperated = 1
            proData.saveProduct(param, function (data) {
                if (typeof(data) === 'object') {
                    if (data.errorCode == 200) {
                        var fragmentList = [];
                        var proId = data.data.id;

                        $('.fragmentDel').each(function () {
                            fragmentList.push($(this).attr('rowid'));
                        });

                        var fraData = {
                            fragmentIds: fragmentList.join(','),
                            productId: proId
                        };
                        proData.saveProductFragment(fraData, function (data) {
                            utils.tools.alert('保存成功!', {timer: 1200, type: 'success'});
                            setTimeout(() => {
                                const suffix = productType === 'NORMAL' ? 'product' : 'combine';
                            window.location.href = `/sellerpc/mall/${suffix}`;
                        }, 2000);
                        }, function (moreInfo) {
                            utils.tools.alert(moreInfo, {timer: 2000, type: 'warning'});
                        });
                    }
                }
            }, function () {
                utils.tools.alert("保存失败!", {timer: 2000, type: 'warning'});
            });

        }


        $form.validate(
            {
                rules: {
                    'productName': {
                        required: true,
                        maxlength: 50
                    },
                    'productInfo': {
                        maxlength: 5000
                    },
                    'productPrice': {
                        required: true,
                        min: 0.000000001,
                        max: 99999000
                    },
                    'productAmount': {
                        required: true,
                        min: 0,
                        digits: true,
                        max: 99999999
                    },
                    'height': {
                        required: true,
                        min: 1,
                        digits: true,
                        max: 99999999
                    },
                    'width': {
                        required: true,
                        min: 1,
                        digits: true,
                        max: 99999999
                    },
                    'length': {
                        required: true,
                        min: 1,
                        digits: true,
                        max: 99999999
                    },
                    'productCategory': {
                        required: true,
                    },
                    'kind': {
                        required: false
                    },
                    'supplierId': {
                        required: true
                    },
                    'point': {
                        required: true,
                        min: 0,
                        number: true,
                        max: 99999999
                    },
                    'promoAmt': {
                        min: 0,
                        max: 9999999
                    },
                    'serverAmt': {
                        min: 0,
                        max: 9999999
                    },
                    'netWorth': {
                        required: true,
                        min: 0,
                        number: true,
                        max: 99999999
                    },
                    // 'moralityPointRate': {
                    //   required: true,
                    //   min: 0,
                    //   digits: true,
                    //   max: 99
                    // },
                    'secureAmount': {
                        required: true,
                        min: 0,
                        digits: true,
                        max: 100000
                    },
                    'deductionDPoint': {
                        required: true,
                        min: 0,
                        number: true,
                    },
                    'numInPackage': {
                        required: false,
                        min: 1,
                        digits: true,
                        max: 100
                    },
                    'barCode': {
                        required: true,
                        digits: true,
                    },
                    'wareHouseId': {
                        required: false,
                    },
                    'productModel': {
                        required: false
                    },
                    'weight': {
                        required: true,
                        digits: true,
                        min: 0,
                    },
                    'skuCode': {
                        required: true,
                        checkSkuCodeUnique: true,
                        checkSkuCodeUniqueInTable:true
                    },
                    'conversionPrice': {
                        required: true,
                        number: true,
                        max: 100000,
                        checkConversionPrice: true,
                        checkConversionPrecision: true,
                    }
                },
                messages: {
                    'productCategory': {
                        require: "请输入商品的分类",
                    },
                    'productName': {
                        required: '商品名称不能为空',
                        maxlength: '长度不能多于50个字符',
                    },
                    'supplierId': {
                        required: "请选择供应商",
                    },
                    'productInfo': {
                        maxlength: '商品描述最多5000字'
                    },
                    'weight': {
                        required: "请输入商品的重量",
                        digits: "请输入整数",
                        min: 0,
                    },
                    'productPrice': {
                        required: '商品价格不能为空',
                        min: '商品价格必须大于0',
                        max: '商品价格不能超过99999000'
                    },

                    'productAmount': {
                        required: '库存数量不能为空',
                        min: '库存数量必须大于0',
                        digits: '库存必须为整数',
                        max: '商品库存不能超过99999999'
                    },
                    'secureAmount': {
                        required: '安全库存数量不能为空',
                        min: '安全库存数量必须大于0',
                        digits: '安全库存必须为整数',
                        max: '安全库存不能超过99999999'
                    },

                    'point': {
                        required: "请输入商品返利积分",
                        min: "返利积分的最小值为0",
                        max: "返利积分的最大值为100000"
                    },
                    'promoAmt': {
                        min: "推广费的最小值为0",
                        max: "推广费的最大值为100000"
                    },
                    'serverAmt': {
                        min: "服务费的最小值为0",
                        max: "服务费的最大值为100000"
                    },
                    'netWorth': {
                        required: "请输入商品净值",
                        min: "净值的最小值为0",
                        max: "净值的最大值为100000"
                    },
                    // 'moralityPointRate': {
                    //   required: "请输入德分比例",
                    //   min: "德分比例的最小值为0",
                    //   digits: "德分比例必须为整数",
                    //   max: "德分比例的最大值为99"
                    // },
                    'height': {
                        required: "请输入商品高度",
                        min: "高度的最小值为1",
                        digits: "只能输入整数",
                    },
                    'width': {
                        required: "请输入商品宽度",
                        min: "宽度的最小值为1",
                        digits: "只能输入整数",
                    },
                    'length': {
                        required: "请输入商品长度",
                        min: "长度的最小值为1",
                        digits: "只能输入整数",
                    },
                    'numInPackage': {
                        required:"index",
                        min: "装箱数的的最小值为1",
                        digits: "装箱数必须为整数",
                        max: "装箱数的最大值为100"
                    },
                    'barCode': {
                        required: "请输入商品条形码",
                    },
                    'deductionDPoint': {
                        required: "请输入商品的兑换价以计算对应的德分",
                        min: "德分不能为负数",
                    },
                    'skuCode': {
                        required: "请输入规格编码",
                    },
                    'conversionPrice': {
                        required: "请输入商品的兑换价",
                    },
                },
                focusCleanup: true,
                onkeyup: false,
                onclick: false,
                errorPlacement: function(error, element) {
                    console.log(this);
                },
                showErrors: function(errorMap, errorList) {
                    errorList.forEach(function(element,index,array){
                        $(element.element).attr("data-toggle","tooltip");
                        $(element.element).attr("data-placement","bottom");
                        $(element.element).attr("title",element.message);
                        $(element.element).tooltip();
                    });
                    this.defaultShowErrors();
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
                    $(element).tooltip('destroy');
                },
                submitHandler: function (form) {
                    console.log("submitCallBack");
                    //jquery 不支持校验多个 名字相同的字段，所以规格表里面的需要手动校验
                    let skuType = $("input[name='skuType']:checked").val();
                    let mutiSkuValidation=true;
                    if (skuType === 'muliSku') {
                        mutiSkuValidation=$(".sku_tablecell input").valid();
                    }
                    if(mutiSkuValidation){
                        saveProduct(form);
                    }else{
                        utils.tools.alert("多规格数据不符合规范", {
                            timer: 1200,
                            type: 'warning'
                        });
                    }
                },
            }
        );

        $.validator.addMethod("checkConversionPrice", function (value, element) {
            //判断是统一规格还是多规格
            let isUnion=$(element).hasClass("skuInput");
            let conversionPrice=value;
            let price;
            if(isUnion){
                let tr = $(element).parents(".sku_cell");
                price = tr.find(".productPrice").prop("value");
            }else{
                price= $("#productPrice").prop("value");
            }
            let paresedPrice = Number.parseFloat(price);
            let paresedConverPrice = Number.parseFloat(conversionPrice);
            return this.optional(element) || (paresedConverPrice > 0 && paresedPrice >= paresedConverPrice);
        }, "兑换价不能小于0或大于售价");

        $.validator.addMethod("checkConversionPrecision",function (value, element) {
            let result = /^-?\d+\.?\d{0,2}$/.test(value);
            return this.optional(element) || result;
        }, "兑换价不能输入超过1位小数");

        $.validator.addMethod("checkSkuCodeUnique", function (value, element) {
            //判断是统一规格还是多规格
            let tagId=$(element).prop("id");
            let code;
            let originCode;
            if(tagId){
                code = $("#skuCode").prop("value");
                originCode = $("#skuCodeOrigin").prop("value");
            }else{
                code=value;
                originCode=$(element).next().prop("value");
            }
            if (originCode === code) {
                return this.optional(element) || true;
            }
            let result = false;
            $.ajax({
                    url: `${window.host}/product/sku/exit/${value}`,
                    async: false,
                }
            ).done(
                data => {
                result = !data.data;
        }
        ).fail(
                data => {
            });
            return this.optional(element) || result;
        }, "SKU编码已存在");

        /***
         * 判断sku在表格内是否是唯一的
         */
        $.validator.addMethod("checkSkuCodeUniqueInTable", function (value, element) {
            let id=$(element).prop("id");
            if(id=='skuCode'){
                return true;
            }else{
                let skuCodes=$(".sku_table").find(".skuCode");
                let repeatCount=0;
                for(let i=0;i<skuCodes.length;i++){
                    let v=$(skuCodes.get(i)).prop("value");
                    if(v){
                        if(v==value){
                            ++repeatCount;
                        }
                    }
                }
                return this.optional(element) || repeatCount===1;
            }
        }, "SKU编码在表格内已存在");
    });
/**
 * Created by quguangming on 16/5/24.
 */
define('product/upload',['jquery','utils','dropzone'], function($,utils) {
    Dropzone.autoDiscover = false;

    var imgDropzone =  $("#product_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 100, // MB      //最大文件大小，单位是 MB ,不限制
        maxFiles: 10,               //限制最多文件数量
        maxThumbnailFilesize: 10,
        addRemoveLinks: true,
        thumbnailWidth:"150",      //设置缩略图的缩略比
        thumbnailHeight:"150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                        //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",              //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function() {

            // var imgDropzone = this;
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".btn-submit").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
                //imgDropzone.processQueue();
            });
            this.on("success", function(file,data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                }
                $(".dz-image").css("height","150px;");
                $(".dz-image").css("width","150px;");

            });

        }
    });

    return imgDropzone;
});
define('product/multiUpload',['jquery', 'utils', 'dropzone'], function ($, utils) {

    Dropzone.autoDiscover = false;
    const defaultUrl = window.host + '/_f/u?belong=PRODUCT';
    const qiniuUploadHuadong = 'http://upload.qiniup.com';

    /**
     * 新建dropZone封装实例
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @param params.allowTypes 允许上传的文件类型
     * @param params.maxSize 最大上传大小，单位MB
     * @param params.onRemove 客户端移除文件事件
     * @param params.onError 出错时回调
     * @param params.maxFiles 最大文件数量
     * @param params.onProcess 上传时事件
     * @param params.onComplete 上传完成时事件
     * @constructor
     */
    function Uploader(params) {
        /* jquery 的dropZone实例 */
        this.jDropInstance = $(params.dom).dropzone({
            url: params.url || defaultUrl,
            paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
            dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
            maxFilesize: params.maxSize || 3, // MB      //最大文件大小，单位是 MB
            maxFiles: params.maxFiles || 10,               //限制最多文件数量
            maxThumbnailFilesize: 10,
            addRemoveLinks: true,
            thumbnailWidth: "150",      //设置缩略图的缩略比
            thumbnailHeight: "150",     //设置缩略图的缩略比
            acceptedFiles: params.allowTypes || ".gif,.png,.jpg",
            uploadMultiple: false,
            dictInvalidFileType: "文件格式错误:建议文件格式:" + params.allowTypes
                || "gif, png, jpg",//文件类型被拒绝时的提示文本。
            dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
            dictRemoveFile: "删除",                                        //移除文件链接的文本
            dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
            dictResponseError: "服务器暂无响应,请稍后再试!",
            dictCancelUpload: "取消上传",
            dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
            dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
            //autoProcessQueue: false,
            init: function () {

                // var imgDropzone = this;
                //添加了文件的事件
                this.on("addedfile", function (file) {
                    if (file && file.dataImg && file.previewElement) { //是网络加载的数据
                        //$(file.previewElement).attr("data-img", file.dataImg);
                        if (file.size === '' || file.length === 0) {
                            $(file.previewElement).find(".dz-details").hide();
                        }
                    }
                    if (params.onProcess) {
                        params.onProcess();
                    }
                    //imgDropzone.processQueue();
                });
                this.on("success", function (file, data) {
                    if (typeof(data) === 'object') {
                        // 上传到自己服务器
                        if (params.url !== qiniuUploadHuadong) {
                            const imgId = data.data[0].id;
                            if (file && file.previewElement) {
                                $(file.previewElement).attr("data-img", imgId);
                                if (params.onSuccess) {
                                    data.data.name = file.name;
                                    data.data.size = file.size;
                                    file.dataFile = data.data.id;
                                    console.log(file);
                                    console.log(data.data);
                                    params.onSuccess(data.data);
                                }
                            }
                        }
                        // 直接上传7牛
                        else {
                            //TODO 客户端直传返回数据处理
                            if (params.onSuccess) {
                                const ret = {};
                                ret.height = data.h;
                                ret.width = data.w;
                                ret.name = data.name||'';
                                // FIXME 消除硬编码
                                ret.key = 'qn|hs-resources|' + data.hash;
                                ret.id = 'qn|hs-resources|' + data.hash;
                                ret.url = 'http://images.handeson.com/' + data.hash;
                                ret.size = data.size||0;
                                params.onSuccess(ret);
                            }
                        }

                    } else {
                        if (data === -1) {
                            utils.tools.alert("文件上传失败,请重新选择!",
                                {timer: 1200, type: 'warning'});
                        }
                    }
                });

                this.on("error", function (file, message) {
                    if (params.onError) {
                        params.onError(message);
                    } else {
                        $(".dz-error-message").html(message);
                    }
                });

                // 上传完成事件
                this.on("complete", function () {
                    if (params.onComplete) {
                        params.onComplete();
                    }
                });

                this.on("thumbnail", function (file) {
                    if (file && file.previewTemplate) {
                        file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                        file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                    }
                    const $dzImage = $('.dz-image');
                    let flag = true;
                    $dzImage.css("height", "150px;");
                    $dzImage.css("width", "150px;");
                });

                this.on('removedfile', function (file) {
                    if (params.onRemove) {
                        params.onRemove(file);
                    }
                });

                //客户端直传oss时, 写入表单上传参数
                this.on("sending", function (file, xhr, formData) {
                    if (params.url != qiniuUploadHuadong) {
                        return
                    }
                    formData.append("token", params.token);
                    //formData.append("x:customizedParams", params.xxxx);
                });

            }
        });

        /* 真实的dropZone实例 */
        this.dropZone = this.jDropInstance;

        /**
         * 清空dropZone的上传文件
         */
        this.clear = function () {
            this.dropZone.removeAllFiles(true);
            return this;
        };

        /**
         * 添加图片预览到dropZone
         * @param imgUrl 图片地址
         */
        this.addImage = function (imgData,index) {
            if (imgData) {
                const mockFile = {name: imgData.fileName,dataSuffix: imgData.fileSuffix,dataFile:imgData.file,size:imgData.size};
                this.jDropInstance[index].dropzone.emit("addedfile", mockFile);
                this.jDropInstance[index].dropzone.emit("thumbnail", mockFile, imgData.fileUrl);
                this.jDropInstance[index].dropzone.emit("complete", mockFile);
                this.jDropInstance[index].dropzone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
            }
            return this;
        };

        this.destroy = function () {
            this.dropZone.destroy();
        };

        this.disable = function () {
            this.dropZone.disable();
        };

        this.enable = function () {
            this.dropZone.enable();
        };

        this.disableClick = function () {
            $(".dz-hidden-input").prop("disabled", true);
        };

        this.enableClick = function () {
            $(".dz-hidden-input").prop("disabled", false);
        }
    }

    return {

        /**
         * 新建dropZone封装实例
         * @param params.onSuccess 成功后的回调函数，返回地址
         * @param params.dom drop的dom节点
         * @param params.url 上传文件地址，不指定则使用默认的
         * @param params.allowTypes 允许上传的文件类型
         * @param params.maxSize 最大上传大小，单位MB
         * @param params.onRemove 客户端移除文件事件
         * @param params.onError 出错时回调
         * @param params.onProcess 上传时事件
         * @param params.onComplete 上传完成时事件
         * @constructor
         */
        create: function (params) {
            return new Uploader(params);
        }
    }

});
define('product/describe',['jquery', 'utils', 'product/product-data', 'datatables', 'dropzone'], function(jquery, utils, proData, datatables, dropzone){
    Dropzone.autoDiscover = false;

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "",
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    var $datatables = $('#fragment_data_table').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: false,
        deferRender: true,
        searching: true,
        rowId: "id",
        columns: [
            {
                title: "图片",
                width: "60px",
                render: function (data, type, row) {
                    return '<img class="goods-image" src="' + row.imgs[0].imgUrl
                        + '"/>';
                }
            },
            {
                title: "名称",
                data: "name",
                name: "name",
                width: "60px",
            },
            {
                title: "描述",
                data: "description",
                name: "description",
                width: "60px",
            },
            {
                title: "操作",
                width: "120px",
                render: function (data, type, row) {
                    var html = '';
                    html += '<a class="fragmentEdit" style="margin-right: 5px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a class="fragmentDel" data-toggle="popover" rowId="'
                        + row.id + '" ><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ]
    });

    function setFragments(fragments) {
        $.each(fragments, function (index, fragment) {
            $datatables.row.add(
                {
                    "imgs": fragment.imgs,
                    "name": fragment.name,
                    "description": fragment.description,
                    "id": fragment.id
                }
            );
        });
        $datatables.search('').draw();
    }

    /**
     * 编辑段落描述
     */
    $('#fragment_data_table').on('click', '.fragmentEdit', function () {
        var fragmentRow = $(this).closest("tr").get(0);
        proData.viewProductFragment($(fragmentRow).attr('id'),
            function (data) {
                var d = data.data;
                $('#fragmentId').val(d.id);
                clearFragment();
                $('#fDescribeName').val(d.name);
                $('#fDescribeText').val(d.description);
                d.imgs.forEach(addFragmentImage);
                console.log(data);
            }, utils.tools.alert);
        $("#modal_descAdd").modal('show');
    });

    function clearFragment() {
        $('#fDescribeText').val('');
        $('#fDescribeName').val('');
        $('#fragmentImg').val('');
        removeFragmentImgs();
    }


    //添加图片
    function addFragmentImage(img) {
        if (img) {
            var mockFile = {name: "", size: "", dataImg: img.id};
            descImgDropzone[0].dropzone.emit("addedfile", mockFile);
            descImgDropzone[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
            descImgDropzone[0].dropzone.emit("complete", mockFile);
            descImgDropzone[0].dropzone.files.push( mockFile ); // 此处必须手动添加才可以用removeAllFiles移除
            $('#fragmentImg').val(img.img);
        }
    }

    // 删除段落描述图片
    function removeFragmentImgs() {
        descImgDropzone[0].dropzone.removeAllFiles(true);
    }

    var descImgDropzone = $("#desc_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",
        paramName: "file",
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',
        maxFilesize: 10, //MB
        maxFiles: 20,
        maxThumbnailFilesize: 1,
        addRemoveLinks: true,
        thumbnailWidth:"80",
        thumbnailHeight:"80",
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",
        dictRemoveFile: "删除",
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",
        init: function() {
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".saveDescBtn").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
            });
            this.on("success", function(file,data) {
                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".saveDescBtn").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".saveDescBtn").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 80;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 80;
                }
                $(".dz-image").css("height","80px;");
                $(".dz-image").css("width","80px;");
            });
        }
    });

    var descrip = {
        init: function () {
            var that = this;
            that.delete();
            that.editor();
            that.descModalInit();
            that.submitDesc();
        },
        descModalInit: function() {
            $("#btnDescAdd").on('click', function() {
                $('#fDescribeText').val('');
                $('#fDescribeName').val('');
                $('#fragmentImg').val('');
                $('#fragmentId').val('');
                descImgDropzone[0].dropzone.removeAllFiles(true);
                $("#modal_descAdd").modal('show');
            });
        },

        delete: function () {
            $(".edit-choose").delegate('.delete', 'click', function() {
                var _this = this;
                utils.tools.confirm("确认删除此段落描述？", {
                    fnConfirm: function() {
                        if ($(".descrip").length == 1) {
                            $(_this).parent().parent().addClass("fn-hide");
                        } else {
                            $(_this).parent().parent().remove();
                        }
                    }
                });
            });
        },
        editor: function() {
            $(".edit-type-list").delegate('.input-edit', 'click', function() {
                $(".pageEditor").hide();
                $(".sortDrag").hide();
                if ($("#editWeb").attr("checked")) {
                    $(".sortDrag").hide();
                    $(".pageEditor").show();
                } else if ($("#editApp").attr("checked")) {
                    $(".pageEditor").hide();
                    $(".sortDrag").show();
                }
            })
        },

        //填充段落描述弹出层
        fillDesc: function(data) {
            $('#describeAdd .imgBox').empty();
            if (data) {
                //编辑状态
                $('#describeName').val(data.name);
                $('#describeText').val(data.description);
                if (data.imgs[0]) {
                    for (var i = 0; i < data.imgs.length; i++) {
                        $('#describeAdd .imgBox').append('<span><img src="' + data.imgs[i] + '" key="' + data.keys[i] + '" /><i>&#xe602;</i></span>');
                    }
                }
                $('.pageFormContent [name="paraPlace"]').filter('[value=' + data.showModel + ']').attr('checked', true);
                $("#describeAdd").attr('state', 'modify').attr('_from', data.index);
            } else {
                //新增状态
                $('#describeName').val('');
                $('#describeText').val('');
                $('.pageFormContent [name="paraPlace"]:eq(0)').attr('checked', "checked");
                $("#describeAdd").attr('state', '').attr('_from', '');
                $('#describeAdd .imgBox').empty();
            }
        },

        modifyDesc: function() {
            var that = this;
            $('body').off('click', '.pro-desibe .edit')
            $('body').on('click', '.pro-desibe .edit', function() {
                //$('#skuEdit').after('<div id="dialogMask"></div>');
                var $this = $(this).closest('.pro-desibe');
                var data = {
                    name: $this.find('.pro-name span').text(),
                    description: $this.find('.pro-text').text(),
                    imgs: $this.find('.pro-img img').attr('srcs').split(','),
                    keys: $this.find('.pro-img img').attr('keys').split(','),
                    showModel: $this.attr('data-showModel'),
                    index: $this.attr('data-id')
                }
                that.fillDesc(data);
                $("#describeAdd").show();
            });

        },

        submitDesc: function() {
            var that = this;
            $('body').on('click', '.saveDescBtn', function() {
                var pop = $("#modal_descAdd");
                for (var i = 0; i < pop.find('.required').length; i++) {
                    if (pop.find('.required').eq(i).val() == '') {
                        utils.tools.alert('标有*号的项不能为空~');
                        return;
                    }
                }

                var imgs = [];
                $('#desc_image_dropzone').find('.dz-success').each(function(i, el) {
                    imgs.push($(el).attr('data-img'));
                });

                var img = $('#fragmentImg').val();
                // 从编辑过来的
                if (img && img !== '') {
                    imgs.push(img);
                }

                if (!imgs.length) {
                    utils.tools.alert('段落描述图片为必选项~');
                    return;
                }

                var data = {
                    id: $('#fragmentId').val(),
                    name: pop.find('[name="describeName"]').val(),
                    description: pop.find('[name="describeText"]').val(),
                    imgs: imgs.join(','),
                    showModel: pop.find('[name="paraPlace"]:checked').val()
                };
                //保存或新建在这里处理
                //if (pop.attr('state') == 'modify') {
                //    //如果是修改
                //    ajaxData.id = data.id = pop.attr('_from');
                //    var str = doTtmpl(data);
                //    $('.pro-desibe[data-id="' + data.id + '"]').replaceWith(str);
                //} else {
                //    //如果是新建
                //    delete ajaxData.id;
                //    delete data.id;
                //    $('.descList').append(doTtmpl(data));
                //}
                //发送ajax请求
                utils.postAjax(window.host + '/fragment/save', data, function(res) {
                    if (typeof(res) == 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("添加段落描述成功", {timer: 1200, type: 'success'});
                                $("#modal_descAdd").modal("hide");
                                if (data.id && data.id !== '') {
                                    // 编辑
                                    var newData = null;
                                    $datatables.rows().every(function () {
                                        var d = this.data();
                                        if (d.id === data.id) {
                                            newData = d;
                                            newData.description = res.data.description;
                                            newData.name = res.data.name;
                                        }
                                    });
                                    if (newData === null) {
                                        utils.tools.alert('保存失败', { timer: 1200, type: 'warning' });
                                        return;
                                    }
                                    $datatables.row().data(newData).draw();
                                } else {
                                    // 新增row
                                    // datatable为静态表格, 不能刷新!
                                    // datatable为静态表格, 不能刷新!
                                    // datatable为静态表格, 不能刷新!
                                    $datatables.row.add({
                                        "imgs": res.data.imgs,
                                        "name": res.data.name,
                                        "description": res.data.description,
                                        "id": res.data.id
                                    }).draw();
                                }

                                //$(".descListData").append("<tr class='append-desc-data' data-id='" + res.data.id + "'>"
                                //        + "<td><img class='goods-image' src='" + res.data.imgs[0].imgUrl + "' /></td>"
                                //        + "<td>" + res.data.name + "</td>"
                                //        + "<td>" + res.data.description + "</td>"
                                //        + "<td>" + res.data.id + "</td>"
                                //        + "</tr>"
                                //);
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("段落保存失败", {timer: 1200});
                    }
                });
            });
        },

        delImg: function() {
            var that = this;
            $('body').off('click', '#describeAdd .imgBox i');
            $('body').on('click', '#describeAdd .imgBox i', function() {
                var _this = this;
                utils.tools.confirm("确认删除此段落描述图片？", {
                    fnConfirm: function() {
                        $(_this).parent('span').remove();
                    }
                });
            });
        },

        delProImg: function() {
            var that = this;
            $('body').off('click', '.productImgs .imgBox i');
            $('body').on('click', '.productImgs .imgBox i', function() {
                var _this = this;
                utils.tools.confirm("确认删除此商品图片？", {
                    fnConfirm: function() {
                        $(_this).parent('span').remove();
                    }
                });
            });
        },
    }
    descrip.init();

    return {
        dropzone: descImgDropzone[0],
        setFragments: setFragments
    };
});
/**
 * Created by quguangming on 16/5/25.
 */
define(
    'product/edit',['jquery', 'utils', 'utils/productModal', 'product/xlsxUpload', 'utils/storageHelper',
        'product/form', 'product/upload', 'product/multiUpload',
        'product/product-data', 'product/describe', 'select2',
        'tokenfield', 'ckEditor', 'product/xlsxUpload'
    ],
    function ($, utils, productModal, xlsxUpload, storageHelper, form, uploader, multiUpload, proData,
              describe,
              select2,
              tokenfield) {

        var $proId = utils.tools.request('pId'); //商品Id

        var $skuId = $('#skuId').val();

        var $shopId = $("#shopId").val(); //店铺Id

        var $productType = $('#type').val();

        var $proCateId = new Array(); //商品类目Id

        var tagInited = false;
        var tags;

        const baseTagUrl = window.host + '/tags';
        const getTagsUrl = baseTagUrl + '/list/' + $proId;
        const tagProductUrl = baseTagUrl + '/tagProduct/' + $proId;
        const unTagUrl = baseTagUrl + '/unTag/' + $proId;
        const combineListUrl = window.host + `/productCombine/list`;

        const ckeditor = 'editor';

        CKEDITOR.replace(ckeditor, {
            language: 'zh-cn',
        });

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        // 返回商品所有选中的规格属性和属性值
        var chooseAttributeAndItems;

        //存放选中sku数组
        var attr_active = [];

        // 修改商品时，商品的skus信息
        var $skus;

        var pdSourceMap = {
            'SF': '顺丰',
            'CLIENT': '汉薇'
        };

        //商品修改
        if ($proId) {
            $("#productId").val($proId);
            proData.getProductInfo($proId, function (data) {
                if (typeof (data) === 'object') {
                    if (data.errorCode == 200) {
                        var product = data.data;
                        $skus = product.skus;
                        //商品图片
                        if (product.imgs) {
                            $.each(product.imgs, function (i, img) {
                                var key = img.img,
                                    url = img.imgUrl,
                                    img = {
                                        imgUrl: url,
                                        id: key
                                    };
                                addProductImage(img);
                            });
                        }


                        if(product.deliveryRegion !== '全国'){
                            let deliveryRegionIndex = product.deliveryRegion.indexOf(',');
                            let deliveryRegionP, deliveryRegionItem, addressSelect = ''
                            document.querySelector('.local').click()
                            const selectModal = $('#modal-local-excel');
                            selectModal.modal("hide");
                            if(deliveryRegionIndex > 0) {
                                deliveryRegionP = product.deliveryRegion.split(',')
                                deliveryRegionP.map(item=>{
                                    if(item.length > 1) {
                                    let deliveryRegionI = item.indexOf('.') + 1
                                    deliveryRegionItem = item.substr(deliveryRegionI)
                                    addressSelect += `${deliveryRegionItem},`
                                    let deliveryRegionHtml = `<span>${deliveryRegionItem}，</span>`
                                    $('#selected-area').append(deliveryRegionHtml)
                                }
                            })
                                $.ajax({
                                    type:'GET',
                                    url:'/v2/delivery/product/query/region',
                                    success:function(res){
                                        let html = '';
                                        console.log(deliveryRegionP, 'deliveryRegionP')
                                        for(let ele  of res.data){
                                            let isChecked = addressSelect.split(',').includes(ele.name)
                                            if (isChecked) {
                                                html += `<span style="display:inline-block;width:130px;margin:4px;"><input type="checkbox" checked name="${ele.name}" class="select-area" name1="${ele.id}.${ele.name}"/>${ele.name}</span>`
                                            } else {
                                                html += `<span style="display:inline-block;width:130px;margin:4px;"><input type="checkbox" name="${ele.name}" class="select-area" name1="${ele.id}.${ele.name}"/>${ele.name}</span>`
                                            }
                                        }
                                        $('.cityList').append(html)  ;
                                    },
                                    error:function(e){
                                        console.log(e);
                                    }
                                })
                                localStorage.setItem(
                                    'address',
                                    JSON.stringify({
                                        address: addressSelect
                                    })
                                )
                            }
                        } else {
                            $.ajax({
                                type:'GET',
                                url:'/v2/delivery/product/query/region',
                                success:function(res){
                                    let html = '';
                                    for(let ele  of res.data){
                                        html += `<span style="display:inline-block;width:130px;margin:4px;"><input type="checkbox" name="${ele.name}" class="select-area" name1="${ele.id}.${ele.name}"/>${ele.name}</span>`
                                    }
                                    $('.cityList').append(html)  ;
                                },
                                error:function(e){
                                    console.log(e);
                                }
                            })
                        }

                        $("#productName").val(product.name);
                        $("#productEncode").val(product.encode);
                        $("#productModel").val(product.model);
                        $("#singleSkuId").val(product.skus[0].id);
                        $("#height").val(product.height);
                        $("#width").val(product.width);
                        $("#length").val(product.length);
                        $("#kind").val(product.kind);
                        $("#numInPackage").val(product.numInPackage);
                        $("#packageId").val(product.packageId);
                        $("#reviewStatus").val(product.reviewStatus);
                        $("#productInfo").val(product.description);
                        $("#proRecomend").bootstrapSwitch('state', product.recommend);
                        $("#proOneyuanPurchase").bootstrapSwitch('state',
                            product.oneyuanPurchase);
                        $("#proOneyuanPostage").val(product.oneyuanPostage);
                        $("#proSpcialRate").val(product.specialRate);
                        $("#commissionRate").val(product.commissionRate);
                        $("#weight").val(product.weight);
                        if (product.source && product.source !== '') {
                            $('#pdSource').val(pdSourceMap[product.source]);
                        }
                        if (product.supportRefund) {
                            $("input[name=supportRefund][value=true]").prop("checked",
                                true);
                        } else {
                            $("input[name=supportRefund][value=false]").prop("checked",
                                true);
                        }
                        if (product.selfOperated) {
                            $("input[name=selfOperated][value=true]").prop("checked",
                                true);
                        } else {
                            $("input[name=selfOperated][value=false]").prop("checked",
                                true);
                        }
                        $("input[name=gift][value=" + (product.gift ? 'true' : 'false') +
                            "]").attr('checked', true);
                        var yundouSacle = product.yundouScale;
                        var settedScale = product.yundouScale && product.yundouScale !==
                            0;
                        $("input[name=setScale][value=" + (settedScale ? 'true' : 'false') +
                            "]")
                            .attr('checked', true);
                        $('#yundouScale').val(yundouSacle);
                        if (settedScale) {
                            $('#scaleSetting').show();
                        }
                        console.log(product.minYundouPrice);
                        $('#minYundouPrice').val(product.minYundouPrice);

                        $("#productOriginalPrice").val(product.originalPrice);
                        $("#refundAddress").val(product.refundAddress);
                        $("#refundName").val(product.refundName);
                        $("#refundTel").val(product.refundTel);
                        $("#templateValue").val(
                            product.templateValue == null ? '' : product.templateValue);

                        if (product.categorys) {
                            $.each(product.categorys, function (i, category) {
                                $proCateId.push(category.id);
                            });
                        }
                        if (product.detailH5) {
                            CKEDITOR.instances.editor.on("instanceReady", function (event) {
                                CKEDITOR.instances.editor.setData(product.detailH5);
                            })
                        }

                        // 判断商品规格类型显示
                        if (product.attributes) {
                            $("input[name='skuType'][value='muliSku']").prop("checked",
                                "checked");
                            let skuAttributes = product.skuAttributes;
                            skuAttributes.forEach(function (element) {
                                attr_active.push(element.name);
                            })
                            $("#uniformSkuDiv").hide();
                            $("#muliSkuDiv").show();
                            $("#add_multi_sku_btn").show();
                        } else {
                            $(":radio[name='skuType'][value='uniformSku']").prop("checked",
                                "checked");
                            let price = $skus[0].price;
                            let deductionDPoint = $skus[0].deductionDPoint;
                            $("#productPrice").prop("value", price);
                            $("#deductionDPoint").prop("value", deductionDPoint);
                            let conversionPrice = (Number.parseFloat(price) -
                                (Number.parseFloat(deductionDPoint) / 10)).toFixed(1);
                            $("#conversionPrice").prop("value", conversionPrice);
                            //$("#marketPrice").val(product.marketPrice);
                            $("#productAmount").val($skus[0].amount);
                            $("#secureAmount").val($skus[0].secureAmount);
                            $("#skuCode").val($skus[0].skuCode);
                            $("#skuCodeOrigin").val($skus[0].skuCode);
                            $("#barCode").val($skus[0].barCode);
                            $("#point").val($skus[0].point);
                            $("#promoAmt").val($skus[0].promoAmt);
                            $("#serverAmt").val($skus[0].serverAmt);
                            $("#netWorth").val($skus[0].netWorth);
                            $("#length").val($skus[0].length);
                            $("#width").val($skus[0].width);
                            $("#height").val($skus[0].height);
                            $("#weight").val($skus[0].weight);
                            $("#numInPackage").val($skus[0].numInPackage);
                            $("#muliSkuDiv").hide();
                            $("#add_multi_sku_btn").hide();
                            $("#uniformSkuDiv").show();
                        }

                        $.uniform.update();

                        // 滤芯规格
                        if (product.filterSpec) {
                            $(`:radio[name='filterSpec'][value=${product.filterSpec}]`)
                                .prop('checked', true);
                        }

                        // $("input[name='prudctStatus'][value='" + product.status
                        //     + "']").attr("checked", true);

                        $("input[name='status']").prop("value", product.status);

                        if (product.fragments) {
                            describe.setFragments(product.fragments);
                        }
                        /** 初始化选择框控件 **/
                        $('#templateValue').select2({
                            minimumResultsForSearch: Infinity,
                            width: '100%'
                        });
                        proData.getShopProCateList($shopId, setProCateForm,
                            utils.tools.alert);
                        proData.getBrandList(setProBrandForm, utils.tools.alert);
                        proData.getWarehouseList(product.wareHouseId, setWarehouseForm,
                            utils.tools.alert);
                        proData.getSupplierList(product.supplierId, setSupplierForm,
                            utils.tools.alert);
                    }
                }
            }, function () {
                utils.tools.alert('获取商品数据出错!', {
                    timer: 3600,
                    type: 'warning'
                });
            });

        } else { //发布商品操作
            proData.getShopProCateList($shopId, setProCateForm, utils.tools.alert);
            proData.getBrandList(setProBrandForm, utils.tools.alert);
            proData.getWarehouseList('', setWarehouseForm, utils.tools.alert);
            proData.getSupplierList('', setSupplierForm, utils.tools.alert);
            $.ajax({
                type:'GET',
                url:'/v2/delivery/product/query/region',
                success:function(res){
                    let html = '';
                    for(let ele  of res.data){
                        html += `<span style="display:inline-block;width:130px;margin:4px;"><input type="checkbox" name="${ele.name}" class="select-area" name1="${ele.id}.${ele.name}"/>${ele.name}</span>`
                    }
                    $('.cityList').append(html)  ;
                },
                error:function(e){
                    console.log(e);
                }
            })
        }

        //添加图片
        function addProductImage(img) {
            if (img) {
                var mockFile = {
                    name: "",
                    size: "",
                    dataImg: img.id
                };
                uploader[0].dropzone.emit("addedfile", mockFile);
                uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
                uploader[0].dropzone.emit("complete", mockFile);
                uploader[0].dropzone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
            }
        }

        // 不管是不是套装商品都要清空缓存
        const key = 'combineInfo';
        // 清空本地存储
        const storage = storageHelper.local(key);
        storage.remove(key);
        if ($productType && $productType === 'COMBINE') {

            // 解除绑定关系
            const unCombineUrl = (masterId, slaveId) =>
            `${window.host}/productCombine/uncombineTable?masterId=${masterId}&slaveId=${slaveId}`;

            // 暂存用户修改的数量
            const tmpAmt = {};

            /**
             * 组合选框初始化
             */
            (function initModal() {
                const modal = productModal.create({
                    dom: '#modal-container',
                    selectable: true,
                    onSelect: data => {
                    const val = Number(tmpAmt[data.id] || 1);
                if (val <= 0) {
                    utils.tools.error('套装数量不能小于0');
                    return false;
                }
                let pdAmount = $('#productAmount').val();
                if (!pdAmount || pdAmount === '') {
                    utils.tools.error('请先输入套装库存');
                    return false;
                }
                pdAmount = Number(pdAmount);
                const canUseAmt = data.amount;
                // 校验套装数量 * 输入数量不能大于sku库存
                if (val * pdAmount > canUseAmt) {
                    utils.tools.error(`组合件数 ${val} * ${pdAmount} = ${val
                    * pdAmount} 大于可用库存 ${canUseAmt}`);
                    return false;
                }
                return true;
            },
                onUnSelect: data => {
                    // 如果处于编辑状态才取消后台关系
                    // 返回promise, 确保本地缓存跟服务器的数据一致
                    return new Promise(resolve => {
                        if (!$skuId) {
                        resolve(true);
                        return;
                    }
                    utils.post(unCombineUrl($skuId, data.id), ret => {
                        if (ret === false) {
                        utils.tools.error('取消失败');
                    }
                    resolve(ret);
                });
                });
                },
                onConfirm: selected => {
                    if (selected) {
                        selected = selected.map(item => {
                            const tmpVal = tmpAmt[item.id] || 1;
                        if (tmpVal) {
                            item.number = tmpVal;
                        }
                        return item;
                    });
                        storage.set(selected);
                    }
                },
                table: {
                    url: combineListUrl,
                        reqParams: {
                        combined: true,
                            productId: $proId
                    },
                    columnNumber: 50,
                        columns: [{
                        title: '名称',
                        data: 'fullName',
                        name: 'fullName',
                    },
                        {
                            title: '编码',
                            data: "skuCode",
                            name: "skuCode",
                        },
                        {
                            title: '数量',
                            name: 'number',
                            // 优先从用户更改的数据中读取
                            render: (d, t, row) => `<input class="combineNum" style="width: 50px;"
                    rowId="${row.id}" amount="${row.amount}" type="number" min="1" step="1" value="${tmpAmt[row.id]
                        || row.number || 1}">`
                },
                    {
                        title: '库存',
                            data: "amount",
                        name: "amount",
                    },
                    {
                        title: '状态',
                            data: 'statusStr',
                        name: 'statusStr'
                    },
                    {
                        title: '价格',
                            name: "price",
                        render: (d, t, row) => `${row.price}元`
                    }
                ]
                }
            });

                $('.btn-combine').on('click', () => {
                    const supplier = $('.product-supplier').val();
                if (!supplier || supplier === '') {
                    utils.tools.error('请先选择供应商');
                    return;
                }
                const amt = $('#productAmount').val();
                if (!amt || amt === '') {
                    utils.tools.error('请先输入套装库存');
                    return;
                }
                modal.show();
            });

                $(document).on('blur', '.combineNum', e => {
                    const $this = $(e.currentTarget);
                const val = Number($this.val());
                if (val <= 0) {
                    utils.tools.error('套装数量不能小于0');
                    return;
                }
                let pdAmount = $('#productAmount').val();
                if (!pdAmount || pdAmount === '') {
                    utils.tools.error('请先输入套装库存');
                    return;
                }
                pdAmount = Number(pdAmount);
                const canUseAmt = Number($this.attr('amount'));
                // 校验套装数量 * 输入数量不能大于sku库存
                if (val * pdAmount > canUseAmt) {
                    utils.tools.error(`组合件数 ${val} * ${pdAmount} = ${val
                    * pdAmount} 大于可用库存 ${canUseAmt}`);
                    return;
                }
                const id = $this.attr('rowId');
                tmpAmt[id] = val;
            });

            })();
        }

        /* 标签初始化 */
        (function initTags() {
            const $tagsInput = $('#tags-input');
            $tagsInput.on('tokenfield:initialize', function (e) {
                console.log('inited');
            })
                .on('tokenfield:createtoken', function (e) {
                    var data = e.attrs.value;
                    // 第一次初始化时也会调用该方法，所以做如下判断
                    var result = true;
                    if (tagInited) {
                        // tokenfield要求失败时返回false, 该请求必须同步处理
                        // 查看 http://sliptree.github.io/bootstrap-tokenfield/
                        if ($proId && $proId !== '') {
                            utils.postAjaxSync(tagProductUrl, {
                                name: data
                            }, function (res) {
                                if (res === -1) {
                                    utils.tools.alert("网络问题，请稍后再试", {
                                        timer: 1200,
                                        type: 'warning'
                                    });
                                }
                                if (typeof res === 'object') {
                                    if (res.errorCode === 200) {
                                        console.log('tag' + data + ' added');
                                    } else {
                                        result = false;
                                        if (res.moreInfo) {
                                            utils.tools.alert(res.moreInfo, {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        } else {
                                            utils.tools.alert('服务器错误', {
                                                timer: 1200,
                                                type: 'warning'
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                    return result;
                })
                .on('tokenfield:edittoken', function (e) {
                    if (e.attrs.label !== e.attrs.value) {
                        // var label = e.attrs.label.split(' (')
                        // e.attrs.value = label[0] + '|' + e.attrs.value
                    }
                })
                .on('tokenfield:removedtoken', function (e) {
                    var data = e.attrs.value;
                    if ($proId && $proId !== '') {
                        utils.postAjax(unTagUrl, {
                            name: data
                        }, function (res) {
                            if (res === -1) {
                                utils.tools.alert("网络问题，请稍后再试", {
                                    timer: 1200,
                                    type: 'warning'
                                });
                            }
                            if (typeof res === 'object') {
                                if (res.errorCode === 200) {
                                    console.log('tag ' + data + 'removed');
                                } else {
                                    if (res.moreInfo) {
                                        utils.tools.alert(res.moreInfo, {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    } else {
                                        utils.tools.alert('服务器错误', {
                                            timer: 1200,
                                            type: 'warning'
                                        });
                                    }
                                }
                            }
                        });
                    }
                }).tokenfield();
            if ($proId && $proId !== '') {
                utils.postAjax(getTagsUrl, null, function (res) {
                    if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试", {
                            timer: 1200,
                            type: 'warning'
                        });
                    }
                    if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                            tags = res.data;
                            const tagStrs = tags.map(function (item) {
                                return item.name;
                            });
                            console.log(tagStrs);
                            $tagsInput.tokenfield('setTokens', tagStrs);
                            tagInited = true;
                        } else {
                            if (res.moreInfo) {
                                utils.tools.alert(res.moreInfo, {
                                    timer: 1200,
                                    type: 'warning'
                                });
                            } else {
                                utils.tools.alert('服务器错误', {
                                    timer: 1200,
                                    type: 'warning'
                                });
                            }
                        }
                    }
                });
            }
        })();

        function setProCateForm(data) {
            var strArray = [];
            var dataLength = data.data.length;
            for (var i = 0; i < dataLength; i++) {
                if ($proCateId && ($proCateId.indexOf(data.data[i].id) >= 0)) {
                    // 只能选择叶子节点
                    if (data.data[i].isleaf == '0') {
                        strArray.push('<option value=' + data.data[i].id +
                            ' selected="selected" disabled="">' + data.data[i].name +
                            '</option>');
                    } else {
                        strArray.push('<option value=' + data.data[i].id +
                            ' selected="selected">' + data.data[i].name + '</option>');
                    }
                    $(".product-Category .select2-selection__rendered").text(
                        data.data[i].name);
                } else {
                    if (data.data[i].isleaf == '0') {
                        strArray.push('<option value=' + data.data[i].id + ' disabled="">' +
                            data.data[i].name + '</option>');
                    } else {
                        strArray.push('<option value=' + data.data[i].id + '>' +
                            data.data[i].name + '</option>');
                    }
                }
            };
            $('.product-Category').append(strArray.join(''));
            $('.product-Category').trigger("chosen:updated.chosen");
            /** 初始化选择框控件 **/
            $('.product-Category').select2({
                minimumResultsForSearch: Infinity,
                width: '35%'
            });
        }

        function setProBrandForm(data) {
            let brandSelect = $(".product-brand");
            var strArray = [];
            var dataLength = data.list.length;
            if ($proId) {
                proData.getProductBrandList($proId, selectedBrandList => {
                    let selectedBrand = '';
                if (selectedBrandList.length !== 0) {
                    selectedBrandList.forEach(function (element, index, array) {
                        selectedBrand += element.id + ',';
                    });
                }
                for (var i = 0; i < dataLength; i++) {
                    if (selectedBrand === data.list[i].id + ',') {
                        strArray.push('<option value=' + data.list[i].id +
                            ' selected="selected">' + data.list[i].name +
                            '</option>');
                        $(".product-brand .select2-selection__rendered").text(
                            data.list[i].name);
                    } else {
                        strArray.push('<option value=' + data.list[i].id + '>' +
                            data.list[i].name + '</option>');
                    }
                }
                brandSelect.append(strArray.join(''));
                brandSelect.trigger("chosen:updated.chosen");
                /** 初始化选择框控件 **/
                brandSelect.select2({
                    width: '35%'
                });
            }, utils.tools.alert)
            } else {
                for (var i = 0; i < dataLength; i++) {
                    strArray.push('<option value=' + data.list[i].id + '>' +
                        data.list[i].name + '</option>');
                }
                brandSelect.append(strArray.join(''));
                brandSelect.trigger("chosen:updated.chosen");
                /** 初始化选择框控件 **/
                brandSelect.select2({
                    width: '35%'
                });
            }
        }

        function setSupplierForm(selectedSupplier, data) {
            let select = $(".product-supplier");
            let suppliers = [];
            data.list = data.list.filter(element => {
                return true;
        });
            data.list.forEach(element => {
                suppliers.push({
                id: element.id,
                text: element.name,
            })
        });
            /** 初始化选择框控件 **/
            select.select2({
                width: '35%',
                minimumResultsForSearch: Infinity,
                data: suppliers,
            });

            if ($proId && selectedSupplier) {
                select.val(selectedSupplier);
            }
            select.trigger("change");
        }

        function setWarehouseForm(selectedWarehouse, data) {
            let select = $(".product-ware-house");
            var strArray = [];
            var dataLength = data.list.length;
            for (var i = 0; i < dataLength; i++) {
                strArray.push('<option value=' + data.list[i].id + '>' +
                    data.list[i].name + '</option>');
            }
            select.append(strArray.join(''));
            select.trigger("chosen:updated.chosen");
            /** 初始化选择框控件 **/
            select.select2({
                width: '35%',
                minimumResultsForSearch: Infinity,
            });

            if ($proId && selectedWarehouse) {
                select.val(selectedWarehouse);
                select.trigger("change");
            }
        }

        $("[name=logisticsType]").change(function () {
            var typeValue = $(this).val();
            if (typeValue == 'TEMPLATE') {
                $("#uniformValueDiv").hide();
                $("#templateValueDiv").show();
            } else {
                $("#uniformValueDiv").show();
                $("#templateValueDiv").hide();
            }
        });

        $("[name=skuType]").change(function () {
            var typeValue = $(this).val();
            if (typeValue == 'uniformSku') {
                $("#muliSkuDiv").hide();
                $("#add_multi_sku_btn").hide();
                $("#uniformSkuDiv").show();
            } else {
                $("#muliSkuDiv").show();
                $("#add_multi_sku_btn").show();
                $("#uniformSkuDiv").hide();
            }
        });

        $('input[name=setScale]').on('change', function () {
            var choose = $(this).val();
            if (choose === 'true') {
                $('#scaleSetting').show();
            } else {
                $('#scaleSetting').hide();
                $('#yundouScale').val('');
            }
        });

        /**
         * 动态计算德分
         */
        $('#conversionPrice,#productPrice').on('change', function () {
            let conversionPriceValid = $('#conversionPrice').valid();
            let productPriceValid = $('#productPrice').valid();
            if (!conversionPriceValid || !productPriceValid) {
                return;
            }
            let conversionPrice = $("#conversionPrice").prop("value");
            let price = $("#productPrice").prop("value");
            let paresedPrice = Number.parseFloat(price);
            let paresedConverPrice = Number.parseFloat(conversionPrice);
            let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
            console.log(result);
            $("#deductionDPoint").prop("value", ((Number.parseFloat(price) -
                Number.parseFloat(conversionPrice)) * 10).toFixed(2));
        });

        let registerSkuPriceListener = function () {
            /**
             * 动态计算sku中的德分
             */
            $('.conversionPrice,.productPrice').off("change").on('change', function () {
                let tr = $(this).parents(".sku_cell");
                let conversionPriceValid = tr.find(".conversionPrice").valid();
                let priceValid = tr.find(".productPrice").valid();
                if (!conversionPriceValid || !priceValid) {
                    return;
                }
                let conversionPrice = tr.find(".conversionPrice").prop("value");
                let price = tr.find(".productPrice").prop("value");
                let paresedPrice = Number.parseFloat(price);
                let paresedConverPrice = Number.parseFloat(conversionPrice);
                let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
                tr.find(".deductionDPoint").prop("value",
                    result === 0 ? 0.00 : result);
            });
        };

        /*
      判断sku唯一性
    */
        //  let checkMultiSkuUnique = function () {
        //  $(".skuCode").on("change",function(){
        //    console.log("i am moveing!!!");
        //    $.ajax({
        //     url: `${window.host}/product/sku/exit/${code}`,
        //     async: false,
        //   }
        //   ).done(
        //       data => {
        //         console.log(data.data);
        //       }
        //   )
        //  })
        //  }
        // -------------------------------  以下为商品sku规格使用的js  -------------------------------
        //var size = ['XXXL', "XXL", 'L', "M", "S"];
        //var color = ["红色", "黄色", "橘色", "黑色", "白色"];
        //var sizes = {
        //   "颜色": color,
        //    "尺码": size
        //}
        xlsxUpload.addExcelUploadEventListener($("#excel_file"));
        // 缓存所有商品规格属性名称与id关系,如{"颜色": "1acb","大小": "23cd"}
        var attributes = {};
        // 缓存所有商品规格属性值名称与id关系,如{"白色": "1acb","M码": "23cd"}
        var attributeItems = {};
        var sizes = {};
        var tabledata = []
        var selectedArr = {};
        var hasdid = {};
        var hastext = [];
        /***
         * Skumodel 为生成规格类
         * title为规格名字  string  例：尺码
         * times将要生成的规格项目  Array  例如：尺码：xxl,xl,m,s
         * dataid为产生型号的标识最外层元素上的id   string
         * **/
        var dropzoneInstance;
        var attributeDropZoneIndexMap = new Map();
        var Skumodel = function (title, items, dataId, index) {
            //最外层div+标题栏
            this.title = title || "";
            this.items = items || [];
            this.index = index || "";
            this.container = $('<div class="sku_container" id="' + dataId +
                '"><div class="sku_modellist_title">' + "规格-" + index + "：" + this.title +
                '</div></div>');
            //模型列表
            this.skumodels = $('<div class="sku_models"></div>');
            this.skumlist = $('<div class="sku_list"></div>')
            this.skuinputcon = $('<div class="sku_add"></div>');
            //输入框
            this.skuinput = $('<input type="text" placeholder="请输入型号属性">');
            //新建按钮
            this.addbtn = $('<a>新建</a>');
            this.init(this.items, title,dataId)
        }
        Skumodel.prototype = {
            //初始化显示组件
            init: function (items, title,dataId) {
                var html = ""
                for (var i = 0; i < items.length; i++) {
                    html += `<span class="sku_item"><a data-id=${attributeItems[dataId+'-'+items[i]]} data-sku="${items[i]}"`;
                    // 如果是修改商品，且有属性值是之前选中过的
                    if (chooseAttributeAndItems && chooseAttributeAndItems.skuAttributes && chooseAttributeAndItems.skuAttributes.indexOf(attributeItems[dataId+'-'+items[i]]) != -1) {
                        html += ' class="itemactive" ';
                    }
                    html += '>' + items[i] +
                        '</a><i class="sku_item_close">×</i></span>';
                }

                //获取所有生成按钮
                this.skumlist.append($(html));
                this.skumodels.append(this.skumlist);
                this.container.append(this.skumodels);
                this.skuinputcon.append(this.skuinput);
                this.skuinputcon.append(this.addbtn);
                // 目前不需要商品新增页新增规格属性功能
                //this.skumodels.append(this.skuinputcon)
                $(".sku_modellist").append(this.container);
                //第一组sku添加图片
                if (attr_active && attr_active.length > 0 && title == attr_active[0]) {
                    let firstSkuList = $(".sku_modellist>.sku_container:first .sku_list>.sku_item a"); //确认添加图片框位子
                    let skuImgBox = $("<div  action = '#' class = 'skuImgBox dropzone' enctype = 'multipart/form-data' method = 'post'></div>");
                    skuImgBox.insertBefore(firstSkuList);
                }
                this.bindEvent()
            },

            bindEvent: function () {
                var self = this;
                //点击新建按钮产生
                this.addbtn.click(function () {
                    self.createItem();
                });
                this.activeItem();
                //点击删除按钮删除
                this.deleteItem();
                //控制删除符号
                //this.toggleCloseEle();
            },
            //创建sku子元素
            createItem: function () {
                var value = $.trim(this.skuinput.val())
                if (value.length <= 0) {
                    layer.alert("请输入内容");
                    return
                }
                if (sizes[this.title].indexOf(value) >= 0) {
                    layer.msg("请勿重复创建")
                    return;
                }
                sizes[this.title].push(this.skuinput.val())
                this.skumlist.append(
                    $('<span class="sku_item"><a data-id="' + getIndex() + '">' +
                        value + '</a><i class="sku_item_close">×</i></span>'))
                this.skuinput.val("")
            },
            //子元素是否选中事件
            activeItem: function () {
                this.skumlist.on("click", "a", function () {
                    $(this).toggleClass("itemactive");
                    tableContent()
                });
            },
            //监听删除元素
            deleteItem: function () {
                var self = this;
                this.skumlist.on("click", ".sku_item_close", function () {
                    $(this).parent().remove();
                    var text = $(this).parent().find("a").text();
                    var textarr = sizes[self.title];
                    textarr.splice(textarr.indexOf(text), 1);
                    tableContent();
                });
            },
            //控制删除符号的显示
            toggleCloseEle: function () {
                //显示删除符号
                this.skumlist.on("mouseover", ".sku_item", function () {
                    $(this).find(".sku_item_close").css({
                        display: "inline-block"
                    })
                });
                //显示删除符号
                this.skumlist.on("mouseout", ".sku_item", function () {
                    $(this).find(".sku_item_close").css({
                        display: "none"
                    })
                });
            }
        };

        /****
         * SkuCell动态产生表格内容类
         * cellist为表格内部元素    Array   如["红色","xxl"]
         * dataid为行表格id 产生元素的唯一标识   string
         * ***/
        var SkuCell = function (celllist, dataid) {
            //每行表格的父元素
            this.cellcon = $('<div id="' + dataid +
                '" class="sku_cell clearfix"></div>');
            //价格输入
            this.moneyInput = $(
                '<div class="sku_t_title"><input type="number" min="0" class="productPrice" name = "productPrice"/></div>');
            //兑换价
            this.conversionPrice = $(
                '<div class="sku_t_title"><input type="number" min="0" class="conversionPrice skuInput" name = "conversionPrice"/></div>');
            //德分
            this.deductionDPoint = $(
                '<div class="sku_t_title"><input class="deductionDPoint" readonly="readonly" name = "deductionDPoint"/></div>');
            //积分
            this.point = $(
                '<div class="sku_t_title"><input class="point" min="0" type="number" name = "point"/ ></div>');
            //推广费
            this.promoAmt = $(
                '<div class="sku_t_title"><input class="promoAmt" min="0" type="number" name = "promoAmt"/></div>');
            //推广费
            this.serverAmt = $(
                '<div class="sku_t_title"><input class="serverAmt" min="0" type="number" name = "serverAmt"/></div>');
            //净值
            this.netWorth = $(
                '<div class="sku_t_title"><input class="netWorth" min="0" type="number" name = "netWorth"/></div>');
            //sku编码
            this.skuCode = $(
                '<div class="sku_t_title"><input class="skuCode" name = "skuCode" /><input type="hidden" class="tableSkuCodeOrigin" name="skuCodeOrigin"/></div>');
            //条形码
            this.barCode = $(
                '<div class="sku_t_title"><input class="barCode" type="number" name = "barCode" /></div>');
            //库存输入
            this.leftInput = $(
                '<div class="sku_t_title"><input  type="number" min="0" class="productAmount" name = "productAmount"/></div>');
            //安全库存输入
            this.secureInput = $(
                '<div class="sku_t_title"><input type="number" min ="0" class="secureAmount" name = "secureAmount"/></div>');

            //长度
            this.length = $(
                '<div class="sku_t_title"><input type="number" min ="0" class = "length" name = "length"/></div>');
            //宽度
            this.width = $(
                '<div class="sku_t_title"><input type="number" min ="0"  class = "width" name = "width"/></div>');
            //高度
            this.height = $(
                '<div class="sku_t_title"><input type="number" min ="0" class = "height" name = "height"/></div>');
            //重量
            this.weight = $(
                '<div class="sku_t_title"><input type="number" min ="0" class = "weight" name = "weight"/></div>');
            //装箱数
            this.numInPackage = $(
                '<div class="sku_t_title"><input type="number" min ="0" class = "numInPackage" name="numInPackage"/></div>');
            //操作
            this.operation = $(
                '<div class="sku_t_title operation"><span class = "delete">删除</span></div>');

            this.init(celllist);
            $(".productPrice,.conversionPrice,.deductionDPoint,.point,.promoAmt,.serverAmt,.netWorth,.skuCode,.barCode,.productAmount,.secureAmount,.length,.width,.height,.weight,.numInPackage").css(
                '-webkit-appearance', 'textarea');
        };
        SkuCell.prototype = {
            constructor: SkuCell,
            init: function (celllist) {
                var html = "";
                for (var i = 0; i < celllist.length; i++) {
                    html += '<div class="sku_t_title spec">' + celllist[i] + '</div>'
                }
                this.cellcon.append($(html));
                this.cellcon.append(this.moneyInput);
                this.cellcon.append(this.conversionPrice);
                this.cellcon.append(this.deductionDPoint);
                this.cellcon.append(this.point);
                this.cellcon.append(this.promoAmt);
                this.cellcon.append(this.serverAmt);
                this.cellcon.append(this.netWorth);
                this.cellcon.append(this.skuCode);
                this.cellcon.append(this.barCode);
                this.cellcon.append(this.leftInput);
                this.cellcon.append(this.secureInput);
                this.cellcon.append(this.length);
                this.cellcon.append(this.width);
                this.cellcon.append(this.height);
                this.cellcon.append(this.weight);
                this.cellcon.append(this.numInPackage);
                this.cellcon.append(this.operation);

                $('.sku_tablecell').append(this.cellcon);
                registerSkuPriceListener();
                //checkMultiSkuUnique();
                $(".sku_tablecell").css({
                    "width": tableWidth + 'px'
                });
                /* 表格删除按钮 */
                $(".sku_tablecell").find(".operation").off("click").on('click', function () {
                    let skuDel = confirm("该组规格删除后,将不可恢复,确认要删除吗?");
                    skuDel == true ? $(this).parents(".sku_cell").remove() : console.log("取消删除");
                });
            },
        };
        /****
         * 创建表格头部
         * arr 将要创建的表头内容 Arr ["颜色"，"尺码"]
         * **/
        var mustArr = [];
        var tableWidth;

        function createTablehead(arr) {
            mustArr = ["价格", "兑换价", "德分", "立省", "推广费", "服务费", "净值", "SKU编码", "条形码", "库存",
                "安全库存", "长度", "宽度", "高度", "重量", "装箱数", "操作"
            ];
            var relayArr = arr.concat(mustArr);
            html = "";
            for (var i = 0, len = relayArr.length; i < len; i++) {
                html += '<div class="sku_t_title">' + relayArr[i] + '</div>'
            }
            tableWidth = (attr_active.length + mustArr.length) * 100;
            $(".sku_tableHead").html("").append($(html));
            $(".sku_tableHead").css({
                "width": tableWidth + 'px'
            });
            createBulkOperation(skuOptArr);
            $(".sku_detail").next(".sku_detail").remove();
        }

        /* 创建批量操作 */
        //批量操作输入框数组
        var skuOptArr = ["价格", "兑换价", "立省", "推广费", "服务费", "净值", "库存", "安全库存", "长度", "宽度", "高度", "重量", "装箱数"];

        function createBulkOperation(arr) {
            let bulkOperationBox = $("<div class = 'sku_detail'><p class='detail_title'>规格明细：</p></div>");
            let detail_p = $("<p>批量操作</p>");
            let detail_list = $("<div class = 'detail_list'></div>");
            bulkOperationBox.append(detail_p);
            detail_p.after(detail_list);
            $(".sku_guige").after(bulkOperationBox);
            for (let i = 0; i < arr.length; i++) {
                // let detail_inp = $("<input placeholder = '"+arr[i]+"' type = 'number' min = '0'/>");
                let detail_inp = $("<div class='listDiv'><span>" + arr[i] + "</span><input type = 'number' min = '0'/></div>")
                detail_list.append(detail_inp);
            }
            let add_btn = $("<span>确认</span>");
            detail_list.append(add_btn);
            add_btn.on("click", function () {
                let optArr = [];
                let detaliInp = $(".detail_list input");
                detaliInp.each(function (index, value) {
                    optArr.push(value.value);
                });
                optArr[0] != "" ? $(".productPrice").val(optArr[0]) : ""; //价格
                optArr[1] != "" ? $(".conversionPrice").val(optArr[1]) : ""; //兑换价
                optArr[2] != "" ? $(".point").val(optArr[2]) : ""; //立省
                optArr[3] != "" ? $(".promoAmt").val(optArr[3]) : ""; //推广费
                optArr[4] != "" ? $(".serverAmt").val(optArr[4]) : ""; //服务费
                optArr[5] != "" ? $(".netWorth").val(optArr[5]) : ""; //净值
                optArr[6] != "" ? $(".productAmount").val(optArr[6]) : ""; //库存
                optArr[7] != "" ? $(".secureAmount").val(optArr[7]) : ""; //安全库存
                optArr[8] != "" ? $(".length").val(optArr[8]) : ""; //长度
                optArr[9] != "" ? $(".width").val(optArr[9]) : ""; //宽度
                optArr[10] != "" ? $(".height").val(optArr[10]) : ""; //高度
                optArr[11] != "" ? $(".weight").val(optArr[11]) : ""; //重量
                optArr[12] != "" ? $(".numInPackage").val(optArr[12]) : ""; //装箱数
                //如果价格和兑换价的数据格式符合标准，则计算德分
                if (optArr[0] != "" || optArr[1] != "") {
                    calcDPoint();
                }
                //手动做表格内的数据校验
                $(".sku_tablecell input").valid();
            })
        }
        /***
         * 排列组合计算出选择的规格型号的组合方式
         *
         * */
        function getResult() {

            var head = arguments[0][0];
            for (var i in arguments[0]) {
                if (i != 0) {
                    head = group(head, arguments[0][i])
                }
            }
            tabledata = [];
            $(".sku_cell").each(function (index) {
                tabledata.push($(this).attr("id"));
            }).hide()
            for (var j = 0, len = head.length; j < len; j++) {
                var newcell = head[j]["datatext"].split(',')
                var dataid = head[j]["dataid"];
                if (tabledata.indexOf(dataid) < 0) {
                    new SkuCell(newcell, dataid)
                } else {
                    $("#" + dataid).show()
                }
            }
        };

        //组合前两个数据
        function group(first, second) {
            var result = [];
            for (var i = 0, leni = first.length; i < leni; i++) {
                for (var j = 0, len = second.length; j < len; j++) {
                    result.push({
                        dataid: first[i]["dataid"] + "-" + second[j]["dataid"],
                        datatext: first[i]["datatext"] + "," + second[j]["datatext"]
                    })
                }
            }
            return result
        }

        //动态产生一个索引，用于后续操作
        var i = 3;

        function getIndex() {
            return "d" + i++;
        };

        //控制表格内容
        function tableContent() {
            $(".sku_modellist .sku_models").each(function (index, ele) {
                var aa = $(this).find(".itemactive");
                selectedArr[index] = [];
                for (var i = 0; i < aa.length; i++) {
                    selectedArr[index][i] = {};
                    selectedArr[index][i]["dataid"] = $(aa[i]).attr("data-id");
                    selectedArr[index][i]["datatext"] = $(aa[i]).text();
                }
            })
            getResult(selectedArr);
        }

        $(function () {
            // 将系统所有规格属性初始化到界面上
            $.ajax({
                url: window.host + '/skuAttribute/findAllAttributes',
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.errorCode == 200) {
                        attributes = data.data;
                        xlsxUpload.setSkuAttributeIds(data);
                        $.each(attributes, function (i, item) {
                            $(".sku_content_sku_list").append(
                                "<span class='sku_item'><a data-id='" +
                                item + "' data-sku='" +
                                i + "'>" +
                                i + "</a></span>");
                        });
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

            // 初始化规格属性值
            $.ajax({
                url: window.host + '/skuAttribute/findAllItemsWithAttributeId',
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.errorCode == 200) {
                        attributeItems = data.data;
                        xlsxUpload.setSkuAttributeItemIds(attributeItems);
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

            // 初始化规格属性与其对应所有属性值
            $.ajax({
                url: window.host + '/skuAttribute/findAttributeAndItems',
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    if (data.errorCode == 200) {
                        sizes = data.data;
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tool.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

            // 如果是修改商品，则需要处理之前选中的商品规格属性
            if ($proId) {
                proData.getProductInfo($proId, function (data) {
                    if (typeof (data) === 'object') {
                        if (data.errorCode == 200) {
                            let product = data.data;
                            $skus = product.skus;
                            let skuAttributes = product.skuAttributes;
                            skuAttributes.forEach(function (element, index) {
                                let skuItem = $(".sku_list").find(`.sku_item a[data-id=${element.id}]`);
                                skuItem.addClass("itemactive");
                                skuItem.after(`<span class="sku_index">${index+1}</span>`)
                            });
                        }
                    }
                });

                $.ajax({
                    url: window.host +
                        '/skuAttribute/getProductAttributeAndItems?productId=' +
                        $proId,
                    type: 'POST',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        if (data.errorCode == 200) {
                            chooseAttributeAndItems = data.data;
                        } else {
                            alert(data.moreInfo);
                        }
                    },
                    error: function (state) {
                        if (state.status == 401) {
                            utils.tool.goLogin();
                        } else {
                            alert('服务器暂时没有响应，请稍后重试...');
                        }
                    }
                });
                console.log(chooseAttributeAndItems);
                // 如果商品有选择规格属性，则显示出之前选中的商品规格属性
                if (chooseAttributeAndItems.productAttributes) {
                    $.ajax({
                        url: window.host + '/skuAttribute/findAllAttributeAndItemsVO',
                        type: 'POST',
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            if (data.errorCode == 200) {
                                let datas = data.data;
                                console.log(datas);
                                let arrs = [];
                                let skuModels=[];
                                // 如果规格是之前选中的，则显示
                                let index = 1;
                                $.each(datas, function (i, item) {
                                    var skuAttributeId = item.id;
                                    var text = item.name;
                                    if (chooseAttributeAndItems.productAttributes.indexOf(
                                        skuAttributeId) != -1) {
                                        var arr = sizes[text] || [];
                                        sizes[text] = arr;
                                        //创建规格
                                        skuModels.push({
                                            text:text,
                                            arr:arr,
                                            skuAttributeId:skuAttributeId,
                                            items:item.items
                                        });
                                        arrs.push(text);
                                        index++;
                                    }
                                });
                                // skuModels.sort(function(v1,v2){ 修复某些产品详情bug
                                //     let index1=0;
                                //     let index2=0;
                                //     for(let item of v1.items){
                                //         if(chooseAttributeAndItems.skuAttributes.indexOf(item.id)!==-1){
                                //             index1=chooseAttributeAndItems.skuAttributes.indexOf(item.id);
                                //             break;
                                //         }
                                //     }
                                //     for(let item of v2.items){
                                //         if(chooseAttributeAndItems.skuAttributes.indexOf(item.id)!==-1){
                                //             index2=chooseAttributeAndItems.skuAttributes.indexOf(item.id);
                                //             break;
                                //         }
                                //     }
                                //     return  index2-index1;
                                // });
                                skuModels.forEach(function (value, index) {
                                    new Skumodel(value.text, value.arr, value.skuAttributeId, skuModels.indexOf(value)+1);
                                });

                                initDropzone();
                                // 初始化表头
                                clearTable(arrs);
                                // 根据之前选中的规格属性，初始化表格行数据
                                tableContent();
                                // 根据sku信息，初始化表格行内的价格，库存等数据
                                // 先按照sku的顺序重新对表格内进行排序
                                var skuRows = '';
                                $.each($skus, function (i, item) {
                                    if(item.amount!==-1001){//排查假数据
                                        var skuAttribute = item.attributes;
                                        var skuRow = $(`.sku_tablecell #${skuAttribute}`);
                                        skuRows += skuRow.prop("outerHTML");
                                    }
                                });
                                $(".sku_tablecell").html(skuRows);
                                /* 表格删除按钮 */
                                $(".sku_tablecell").find(".operation").on('click', function () {
                                    let skuDel = confirm("该组规格删除后,将不可恢复,确认要删除吗?");
                                    skuDel == true ? $(this).parents(".sku_cell").remove() : console.log("取消删除");
                                });
                                // 初始化表格行内的价格，库存等数据
                                $.each($skus, function (i, item) {
                                    if(item.amount===-1001) {//排查假数据
                                        return ;
                                    }
                                    var skuId = item.id;
                                    var skuPrice = item.originalPrice;
                                    var deductionDPoint = item.deductionDPoint.toFixed(2);
                                    var point = item.point;
                                    var promotAmt = item.promoAmt;
                                    var serverAmt = item.serverAmt;
                                    var skuCode = item.skuCode;
                                    var barCode = item.barCode;
                                    var netWorth = item.netWorth;
                                    var conversionPrice = Number.parseFloat(skuPrice) - Number.parseFloat(deductionDPoint) / 10;
                                    var skuAmount = item.amount;
                                    var length = item.length;
                                    var width = item.width;
                                    var height = item.height;
                                    var weight = item.weight;
                                    var numInPackage = item.numInPackage;
                                    var skuImgUrl = item.skuImgUrl;
                                    var skuSecureAmount = item.secureAmount;
                                    var skuAttribute = item.attributes;
                                    var skuRow = $(`.sku_tablecell #${skuAttribute}`);
                                    skuRow.attr("sku_id", skuId);
                                    skuRow.find(".productPrice").val(skuPrice);
                                    skuRow.find(".conversionPrice").val(conversionPrice);
                                    skuRow.find(".deductionDPoint").val(deductionDPoint);
                                    skuRow.find(".point").val(point);
                                    skuRow.find(".promoAmt").val(promotAmt);
                                    skuRow.find(".serverAmt").val(serverAmt);
                                    skuRow.find(".skuCode").val(skuCode);
                                    skuRow.find(".tableSkuCodeOrigin").val(skuCode);
                                    skuRow.find(".barCode").val(barCode);
                                    skuRow.find(".netWorth").val(netWorth);
                                    skuRow.find(".productAmount").val(skuAmount);
                                    skuRow.find(".secureAmount").val(skuSecureAmount);
                                    skuRow.find(".length").val(length);
                                    skuRow.find(".width").val(width);
                                    skuRow.find(".height").val(height);
                                    skuRow.find(".weight").val(weight);
                                    skuRow.find(".numInPackage").val(numInPackage);

                                    console.log(skuImgUrl);
                                    //恢复对应图片上传框里面的图片和key
                                    let spec = item.spec;
                                    if (spec) {
                                        let firstSku = spec.split(",")[0];
                                        let index = attributeDropZoneIndexMap.get(firstSku);
                                        if ((index || index === 0) && item.skuImgUrl) {
                                            dropzoneInstance.addImage({
                                                fileName: "",
                                                dataSuffix: "",
                                                fileUrl: item.skuImgUrl,
                                                size: 2000,
                                                dataFile: item.skuImg
                                            }, index);
                                            attributeDropZoneIndexMap.delete(firstSku);
                                        }
                                    }

                                    $(`.sku_item a[data-id=${item.attributes.split("-")[0]}]`).prev().find(".dz-preview").attr("data-img", item.skuImg);


                                });
                                registerSkuPriceListener();

                            } else {
                                alert(data.moreInfo);
                            }
                        },
                        error: function (state) {
                            if (state.status == 401) {
                                utils.tool.goLogin();
                            } else {
                                alert('服务器暂时没有响应，请稍后重试...');
                            }
                        }
                    });
                }

            }

            /***
             * 规格选择确认按钮事件回调
             */
            function skuConfirmCallBack(){
                //清空规格规格
                $(".sku_modellist").html("");
                var arrs = [];
                selectedArr = {}; //清空
                //获取被选中多级型号元素
                // $(".sku_content_sku_list .itemactive").each(function () {
                //   var text = $(this).text(); //选中元素的文字
                //   var dataid = $(this).attr("data-id"); //选中的元素上的参数用于创建规格时候的唯一标识
                //   var arr = sizes[text] || [];
                //   sizes[text] = arr;
                //   //创建规格
                //   new Skumodel(text, arr, dataid)
                //   arrs.push(text)
                // })
                for (let i = 0; i < attr_active.length; i++) {
                    var text = attr_active[i]; //选中元素的文字
                    var dataid = attributes[attr_active[i]]; //选中的元素上的参数用于创建规格时候的唯一标识
                    var arr = sizes[text] || [];
                    sizes[text] = arr;
                    new Skumodel(text, arr, dataid, i + 1); //创建规格
                    arrs.push(text);
                }
                initDropzone();
                //根据arrs数据判断出是否显示表格同时清空表格
                clearTable(arrs);
                $(".sku_content").hide();
            }
            //初始化xlsx模块
            xlsxUpload.setSkuModelInitCallBack(skuConfirmCallBack,attr_active);
            xlsxUpload.setSkuCellInitCallBack(tableContent);
            xlsxUpload.setdPointCalc(calcDPoint);

            //点击添加多级型号事件 layer弹出层
            $("#add_multi_sku").click(function () {
                //layer详细用法 http://www.layui.com/doc/modules/layer.html
                layer.open({
                    type: 1,
                    resize: false,
                    title: "选择商品型号",
                    area: ["600px", "256px"],
                    btn: ["取消", "确定"],
                    content: $(".sku_content"), //此处后放置到弹出层内部的内容
                    yes: function (index, layero) { //取消按钮对应回调函数
                        layer.close(index);
                        $(".sku_content").hide();
                    },
                    btn2: skuConfirmCallBack,
                })
            });

            function clearTable(arrs) {
                if (arrs.length) {
                    $(".sku_guige").show()
                    $(".sku_table").show();
                    $(".sku_tableHead").html('')
                    $(".sku_tablecell").html("")
                    //$(".sku_container .itemactive").toggleClass("itemactive")
                    createTablehead(arrs);
                } else {
                    $(".sku_table").hide()
                    $(".sku_guige").hide()
                }
            }

            //弹窗中的新建sku
            $("#sku_addbtn").click(function () {
                var haveit = false;
                var value = $.trim($(".sku_input").val())
                if (value.length <= 0) {
                    layer.alert("请输入内容");
                    return
                }
                $(".sku_content_sku_list a").each(function () {
                    if ($(this).text() == value) {
                        layer.msg('新建的已存在,请勿重复创建');
                        haveit = true;
                        $(".sku_input").val("")
                    }
                })
                if (haveit) {
                    return;
                }
                var skuitem = '<span class="sku_item"><span class="sku_index"></span><a data-id="' + getIndex() +
                    '>' + value + '</a><i class="sku_item_close">×</i></span>'
                $(".sku_content_sku_list").prepend(skuitem);
                $(".sku_input").val("")
            });
            //显示删除符号
            /**$(".sku_content_sku_list").on("mouseover", ".sku_item", function() {
              $(this).find(".sku_item_close").css({
                  display: "inline-block"
              })
          });
             //显示删除符号
             $(".sku_content_sku_list").on("mouseout", ".sku_item", function() {
              $(this).find(".sku_item_close").css({
                  display: "none"
              })
          });**/
            //删除添加的型号
            $(".sku_content_sku_list").on("click", ".sku_item_close", function () {
                $(this).parent().remove();
            })

            $(".sku_content_sku_list > .sku_item").on("click", "a", function (e) {
                let addAction = !$(this).hasClass('itemactive');
                if (addAction && attr_active.length > 2) {
                    layer.alert('每件商品最多只能添加三种规格');
                    return;
                }
                $(this).toggleClass("itemactive");

                if (addAction) {
                    attr_active.push(e.target.dataset.sku); //点击push进数组
                } else {
                    var index = attr_active.indexOf(e.target.dataset.sku);
                    attr_active.splice(index, 1);
                    $(this).siblings().remove();
                }
                $(".sku_content_sku_list .itemactive").each(function (idx, item) {
                    let attribute = $(item).text();
                    let index = attr_active.indexOf(attribute) + 1;
                    let bro = $(item).siblings();
                    if (bro.length > 0) {
                        $(bro.get(0)).text(index);
                    } else {
                        $(item).parent().append("<span class = 'sku_index'>" + index + "</span>");
                    }
                });
                console.log(attr_active);
            });
        })

        function initDropzone() {
            //初始化dropzone
            dropzoneInstance = multiUpload.create({
                dom: ".skuImgBox",
                maxFiles: 1,
            })
            //维护每个规格对应的dropzone实例的索引
            let imageBoxs = $(".skuImgBox");
            for (let i = 0; i < imageBoxs.length; i++) {
                let attribute = $(imageBoxs.get(i)).next().text();
                if (attribute) {
                    attributeDropZoneIndexMap.set(attribute, i);
                }
            }
        }

        function calcDPoint(){
            let cells = $(".sku_table").find(".sku_cell");
            $.each(cells, function (index, value) {
                let element = $(value);
                let conversionPriceValid = element.find(".conversionPrice").valid();
                let priceValid = element.find(".productPrice").valid();
                if (!conversionPriceValid || !priceValid) {
                    return;
                }
                let conversionPrice = element.find(".conversionPrice").prop("value");
                let price = element.find(".productPrice").prop("value");
                let paresedPrice = Number.parseFloat(price);
                let paresedConverPrice = Number.parseFloat(conversionPrice);
                let result = ((paresedPrice - paresedConverPrice) * 10).toFixed(2);
                element.find(".deductionDPoint").prop("value",
                    result === 0 ? 0.00 : result);
            });
        }

    });
define('utils/fileUploader',['jquery', 'utils', 'dropzone'], function ($, utils) {

    Dropzone.autoDiscover = false;
    const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

    /**
     * 新建dropZone封装实例
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @param params.allowTypes 允许上传的文件类型
     * @param params.maxSize 最大上传大小，单位MB
     * @param params.onRemove 客户端移除文件事件
     * @param params.onError 出错时回调
     * @param params.maxFiles 最大文件数量
     * @param params.onProcess 上传时事件
     * @param params.onComplete 上传完成时事件
     * @constructor
     */
    function Uploader(params) {
        /* jquery 的dropZone实例 */
        this.jDropInstance = $(params.dom).dropzone({
            url: params.url || defaultUrl,
            paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
            dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
            maxFilesize: params.maxSize || 2, // MB      //最大文件大小，单位是 MB
            maxFiles: params.maxFiles||10,               //限制最多文件数量
            maxThumbnailFilesize: 10,
            addRemoveLinks: true,
            thumbnailWidth: "150",      //设置缩略图的缩略比
            thumbnailHeight: "150",     //设置缩略图的缩略比
            acceptedFiles: params.allowTypes || ".gif,.png,.jpg",
            uploadMultiple: false,
            dictInvalidFileType: "文件格式错误:建议文件格式:" + params.allowTypes
                || "gif, png, jpg",//文件类型被拒绝时的提示文本。
            dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
            dictRemoveFile: "删除",                                        //移除文件链接的文本
            dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
            dictResponseError: "服务器暂无响应,请稍后再试!",
            dictCancelUpload: "取消上传",
            dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
            dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
            //autoProcessQueue: false,
            init: function () {

                // var imgDropzone = this;
                //添加了文件的事件
                this.on("addedfile", function (file) {
                    if (file && file.dataImg && file.previewElement) { //是网络加载的数据
                        $(file.previewElement).attr("data-img", file.dataImg);
                        if (file.size === '' || file.length === 0) {
                            $(file.previewElement).find(".dz-details").hide();
                        }
                    }
                    if (params.onProcess) {
                        params.onProcess();
                    }
                    //imgDropzone.processQueue();
                });
                this.on("success", function (file, data) {
                    if (typeof(data) === 'object') {
                        switch (data.errorCode) {
                            case 200: {
                                if (typeof(data.data) === 'object') {
                                    if(data.data[0]) {
                                        const resData = data.data[0];
                                        const imgId = resData.id;
                                        if (file && file.previewElement) {
                                            $(file.previewElement).attr("data-img", imgId);
                                            if (params.onSuccess) {
                                                resData.name = file.name;
                                                resData.size = file.size;
                                                console.log(file);
                                                console.log(resData);
                                                params.onSuccess(resData);
                                            }
                                        }
                                    }else {
                                        if (file && file.previewElement) {
                                            if (params.onSuccess) {
                                                params.onSuccess(data.data);
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                            default: {
                                utils.tools.alert("文件上传失败,请重新选择!",
                                    {timer: 1200, type: 'warning'});
                                break;
                            }
                        }
                    } else {
                        if (data === -1) {
                            utils.tools.alert("文件上传失败,请重新选择!",
                                {timer: 1200, type: 'warning'});
                        }
                    }
                });

                this.on("error", function (file, message) {
                    if (params.onError) {
                        params.onError(message);
                    } else {
                        $(".dz-error-message").html(message);
                    }
                });

                // 上传完成事件
                this.on("complete", function () {
                    if (params.onComplete) {
                        params.onComplete();
                    }
                });

                this.on("thumbnail", function (file) {
                    if (file && file.previewTemplate) {
                        file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                        file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                    }
                    const $dzImage = $('.dz-image');
                    $dzImage.css("height", "150px;");
                    $dzImage.css("width", "150px;");
                });

                this.on('removedfile', function (file) {
                    if (params.onRemove) {
                        params.onRemove(file);
                    }
                });

            }
        });

        /* 真实的dropZone实例 */
        this.dropZone = this.jDropInstance[0].dropzone;

        /**
         * 清空dropZone的上传文件
         */
        this.clear = function () {
            this.dropZone.removeAllFiles(true);
            return this;
        };

        /**
         * 添加图片预览到dropZone
         * @param imgUrl 图片地址
         * @param name 文件名
         * @param size 文件大小
         */
        this.addImage = function (imgUrl, name = '', size = '') {
            if (imgUrl) {
                const mockFile = {name: name, size: size, dataImg: imgUrl};
                this.dropZone.emit("addedfile", mockFile);
                this.dropZone.emit("thumbnail", mockFile, imgUrl);
                this.dropZone.emit("complete", mockFile);
                this.dropZone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
            }
            return this;
        };

        this.destroy = function () {
            this.dropZone.destroy();
        };

        this.disable = function () {
            this.dropZone.disable();
        };

        this.enable = function () {
            this.dropZone.enable();
        };

        this.disableClick = function () {
            $(".dz-hidden-input").prop("disabled", true);
        };

        this.enableClick = function () {
            $(".dz-hidden-input").prop("disabled", false);
        }
    }

    return {

        /**
         * 新建dropZone封装实例
         * @param params.onSuccess 成功后的回调函数，返回地址
         * @param params.dom drop的dom节点
         * @param params.url 上传文件地址，不指定则使用默认的
         * @param params.allowTypes 允许上传的文件类型
         * @param params.maxSize 最大上传大小，单位MB
         * @param params.onRemove 客户端移除文件事件
         * @param params.onError 出错时回调
         * @param params.onProcess 上传时事件
         * @param params.onComplete 上传完成时事件
         * @constructor
         */
        create: function (params) {
            return new Uploader(params);
        }
    }

});
/**
 * Created by jitre on 7/4/18.
 */
define('sync/dataQuery',['jquery', 'utils', 'ramda'], function ($, utils, R) {

    const SYNC_AMOUNT_URL = window.originalHost + "/third/supplier/sku/import";
    const SYNC_ROUTER_URL = window.originalHost
        + "/third/supplier/logistics/import";
    const SYNC_POINT_URL = window.host + "/point/balance/import";
    const SYNC_PRODUCT_URL = window.host + "/mall/import/product";
    const SYNC_WITHDRAW_URL = window.host + "/point/balance/import/withdraw";

    const urlCache = new Map();
    urlCache.set("router", SYNC_ROUTER_URL);
    urlCache.set("amount", SYNC_AMOUNT_URL);
    urlCache.set('point', SYNC_POINT_URL);
    urlCache.set('withdraw', SYNC_WITHDRAW_URL);
    urlCache.set('product',SYNC_PRODUCT_URL);

    function defaultDone(data) {
        $.unblockUI();
        if (data.errorCode === 200 || data.status === 200 || data.data) {
            if(data.moreInfo && data.moreInfo !== ''){
                utils.tools.alert(data.moreInfo,{type: 'success'});
            }else {
                utils.tools.alert('导入成功', {type: 'success'});
            }
        } else {
            if(data.moreInfo && data.moreInfo !== ''){
                utils.tools.alert(data.moreInfo);
            }else {
                utils.tools.alert(data.error);
            }
        }
    }

    function defaultFail() {
        $.unblockUI();
        utils.tools.error("同步数据出错!");
    }

    return {
        sync: function (file, type, jobId, month, platform) {
            utils.blockPage();
            $.get(urlCache.get(type), {
                url: file,
                jobId: jobId,
                month: month,
                platform: platform,
                usedType: $('#usedType option:selected').val()//选中的值
                // 如果silence则不处理
            }).done(defaultDone)
                .fail(defaultFail);
        }
    }

});
/**
 * Created by jitre on 7/4/18.
 */
define('sync/import',['jquery', 'utils', 'utils/fileUploader','sync/dataQuery', 'ramda', 'jquerySerializeObject'],
    function ($,utils, uploader,data, R) {

        // 临时处理, 只适用于表单上只有一个导入按钮的情况
        const fileUploadUrl = jobType => window.host + `/file/upload?type=DOC&jobType=${R.defaultTo('', jobType)}`;
        const dropZoneDom='#import';
        const form=$("#uploader-form");
        const modal=$("#modal-import-excel");
        let dropZoneInstance='';

        let eventInitialization = (function () {
            return {
                init: function () {
                    return this;
                },
                listenSyncButton:function(){
                    $("#sync").off("click").on("click",function(){
                        if(dropZoneInstance.dropZone.getUploadingFiles().length>0){
                            utils.tools.error("请等待上传完成!");
                            return ;
                        }
                        let obj = form.serializeObject();
                        if(!obj.key){
                            utils.tools.error("请先上传!");
                            return ;
                        }
                        data.sync(obj.key,obj.type,obj.jobId);
                        modal.modal("hide");
                    });
                    return this;
                },
                listenImportButton:function(){
                    $(".import").off("click").on("click",function(){
                        let type=$(this).attr("data-type");
                        $("input[name=type]").prop("value",type);
                        modal.modal("show");
                    });
                    return this;
                },
                resetForm: function () {
                    modal.on('hidden.bs.modal', function (e) {
                        form[0].reset();
                        dropZoneInstance.clear();
                    });
                    return this;
                },
                initDropZone:function(){
                    const params = {
                        dom: dropZoneDom,
                        url: fileUploadUrl($('.import').attr('job-type')),
                        allowTypes: '.xls,.xlsx',
                        maxSize: 10,
                        maxFiles: 1,
                        onSuccess: (file) => {
                        $("input[name=key]").prop("value",file.url);
                    $("input[name=jobId]").prop("value", file.jobId);
                    console.log(file);
                },
                    onRemove: (file) => {
                        console.log(file);
                    }
                };
                    dropZoneInstance = uploader.create(params);
                    return this;
                },
            }
        })();
        //init
        eventInitialization.init().resetForm().initDropZone().listenImportButton().listenSyncButton();
    });

/**
 * Created by quguangming on 16/5/24.
 */
require(['all']);

//module
require(['jquery','uniform','placeholder','product/list','product/edit',
    'sync/import'],function(){


});
define("product", function(){});
/**
 * 配送区域
 */
localStorage.setItem(
    'address',
    JSON.stringify({
        address:'全国'
    })
)
$('.local').click(function(){
    var hasSelectAll = false
    $('#selected-area').append()
    // $('.cityList').innerHTML='';
    const selectModal = $('#modal-local-excel');
    selectModal.modal("show");

    $('#select-all').click(function(){
        var xz = $(this).prop("checked")
        var ck = $(".select-area").prop("checked",xz);
        hasSelectAll = true
    })
    $(".select-area").click((ev)=> {
        // console.log(ev.target.checked)
        $('#select-all').prop("checked",false);
})
    function queren(){
        $('#selected-area').innerHTML='';
        var select = document.getElementsByClassName('select-area');
        var address = '';
        var address1 = '';
        for ( let ele of select){
            if(ele.checked){
                address+= ele.name + ',';
                address1 += ele.getAttribute('name1')+ ',';
            }
        }
        $('#select-all').prop("checked") ? hasSelectAll = true : hasSelectAll = false
        if(hasSelectAll == false ) {
            address1.length > 1 ?
                localStorage.setItem(
                    'address',
                    JSON.stringify({
                        address:address1
                    })
                )
                : localStorage.setItem(
                'address',
                JSON.stringify({
                    address:'全国'
                })
                )
        }else {
            document.querySelector('#country').click()
            const selectModal = $('#modal-local-excel');
            selectModal.modal("hide");

            return ''
        }

        const selectModal = $('#modal-local-excel');
        selectModal.modal("hide");
        let html1 = `<span>${address}</span>`
        $('#selected-area').empty().append(html1)
    }
    $('.btnQueren').click(()=>{
        $('#selected-area').innerHTML='';
    var select = document.getElementsByClassName('select-area');
    var address = '';
    var address1 = '';
    var selectArr = [0];
    let selectItem = 0;
    fn()
    function fn(){
        selectItem++
        selectArr[selectItem] = selectItem
        if(selectItem < select.length - 1){
            setTimeout(()=> {
                fn()
            },0)
        }else{
            selectArr.map(item => {
                if(select[item].checked){
                address+= select[item].name + ',';
                address1 += select[item].getAttribute('name1')+ ',';
            }
        })
            $('#select-all').prop("checked") ? hasSelectAll = true : hasSelectAll = false
            if(hasSelectAll == false ) {
                address1.length > 1 ?
                    localStorage.setItem(
                        'address',
                        JSON.stringify({
                            address:address1
                        })
                    )
                    : localStorage.setItem(
                    'address',
                    JSON.stringify({
                        address:'全国'
                    })
                    )
            }else {
                document.querySelector('#country').click()
                const selectModal = $('#modal-local-excel');
                selectModal.modal("hide");

                return ''
            }

            const selectModal = $('#modal-local-excel');
            selectModal.modal("hide");
            let html1 = `<span>${address}</span>`
            $('#selected-area').empty().append(html1)
            return false
        }
    }
})

    $('#country').click(function(){
        hasSelectAll = true
        $('#selected-area').empty();
        $('#select-all').prop("checked",false);
        $(".select-area").prop("checked",false);
        localStorage.setItem(
            'address',
            JSON.stringify({
                address:'全国'
            })
        )
    })
});