/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('mall/logistics',['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2'],
    function ($, utils, datatabels, blockui, bootbox, select2) {

        var $proId = utils.tools.request('pId');  //商品Id

        //参数三种情况【weight按重量，number按件数，volume按体积】
        function shipment(thead) {
            //重量表头
            this.thead = {
                "weight": '<tr class=""><th style="width:200px">可配送至</th><th>首重(g)</th><th>运费(元)</th><th>续重(g)</th><th>续费(元)</th><th style="width: 36px">管理</th></tr>',
                "number": '<tr class=""><th style="width:200px">可配送至</th><th>首件(个)</th><th>运费(元)</th><th>续件(个)</th><th>续费(元)</th><th style="width: 36px">管理</th></tr>',
                "volume": '<tr class=""><th style="width:200px">可配送至</th><th>首件(立方米)</th><th>运费(元)</th><th>续件(个)</th><th>续件(立方米)</th><th style="width: 36px">管理</th></tr>'
            };

            var html = '<div class="form-group">'
                + '<label class="col-sm-2 control-label text-right">配送区域设置</label>'
                + '<div class="col-sm-8">'
                + '<div class="postage-area" id="postage-area">'
                + '<table class="table table-bordered table-condensed table-middle">'
                + '<thead>';
            switch (thead) {
                case 'weight':
                    html += this.thead.weight;
                    break;
                case 'number':
                    html += this.thead.number;
                    break;
                case 'volume':
                    html += this.thead.volume;
                    break;
                default:
                    html += this.thead.weight;
            }

            html += '</thead>'
                + '<tbody>'
                + '<tr>'
                + '<td class="area-text">默认运费</td>'
                + '<td class="text-center"><input type="number" name="first" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="text" name="firstFee" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="number" name="second" class="w90 form-control"></td>'
                + '<td class="text-center"><input type="text" name="secondFee" class="w90 form-control"></td>'
                + '<td></td>'
                + '</tr>'
                + '</tbody>'
                + '</table>'
                + '<div class="mt5"><a class="btn btn-sm btn-primary" id="add_shipment" data-toggle="modal" data-target="#modal_region"><i class="icon-plus3"></i> 新增配送区域</a></div>'
                + '</div>'
                + '</div>'
                + '</div>';
            return html;
        }

        //按重量
        $("#woman").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("weight");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.weight;
                    };
                    var theadWeight = new shipment("weight");
                    $("#postage-area thead").html(theadWeight.theadTem());
                }
            }
        });

        //按件数
        $("#goodsQuantity").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("number");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.number;
                    };
                    var theadNumber = new shipment("number");
                    $("#postage-area thead").html(theadNumber.theadTem());
                }
            }
        });

        //按件数
        $("#sex").change(function () {
            var _this = $(this);
            if (_this.prop("checked")) {
                var html = shipment("volume");
                if ($("#postage-area").length <= 0) {
                    _this.parents(".form-group").after(html);
                } else {
                    shipment.prototype.theadTem = function (first_argument) {
                        return this.thead.volume;
                    };
                    var theadVolume = new shipment("volume");
                    $("#postage-area thead").html(theadVolume.theadTem());
                }
            }
        });

        //新增配送区域
        $(document.body).on("click", "#add_shipment", function () {
            initRegion(this);
        });

        function initRegion(dom) {
            var thisDom = $($(dom).attr("data-target"));
            var html = '<ul class="areas-body clearfix">';

            var editOperation = false;
            //编辑操作
            if ($(dom).hasClass('editProvince')) {
                editOperation = true;
                var checkedProvs = $(dom).closest("tr").children(".area-text").text().trim();
                $(thisDom.find('.modal-footer button.btn-ok')[0]).attr('provRow', checkedProvs);
            }

            $.each(province, function (i, v) {
                html += '<li class="area-item">';
                html += '<div class=""><label><input type="checkbox" zone-id="' + province[i].id + '" name="province" class="styled" ' + (editOperation && checkedProvs.indexOf(province[i].name) >= 0 ? 'checked=true' : '') + '/>' + province[i].name + '</label></div>';
                html += '</div></li>';
            });
            html += '</ul>';
            thisDom.find('.modal-body').html(html);
        }

        //编辑当前行配送区域
        $(document.body).on("click", "#postage-area .editProvince", function () {
            initRegion(this);
        });

        //确定按钮
        $(document.body).on("click", ".modal-footer button.btn-ok", function () {
            var provinceStr = '';
            var zoneIds = [];
            //遍历每个checkbox，把选中的提取出来
            $.each($($("#add_shipment").attr("data-target")).find(".area-item input[type=checkbox]"), function (index, value) {
                var _this = $(this);
                if (_this.is(":checked")) {
                    zoneIds.push(_this.attr('zone-id'));
                    provinceStr += (provinceStr == '' ?  '' : ',') +  _this.parent().text().trim();
                }
            });
            console.log(provinceStr + ':' + zoneIds);
            var a = 'this';
            var modalBtnOk = this;
            var row = '<td class="area-text">' + provinceStr + '</td><td class="text-center"><input type="number" name="first" class="w90 form-control"></td><td class="text-center"><input type="text" name="firstFee" class="w90 form-control"></td><td class="text-center"><input type="number" name="second" class="w90 form-control"></td><td class="text-center"><input type="text" name="secondFee" class="w90 form-control"></td><td><a class="pull-right editProvince" zone-ids="' + zoneIds + '" zone-names="' + provinceStr + '" data-toggle="modal" data-target="#modal_region"><i class="icon-pencil7"></i> 编辑</a></td>';
            var editOperation = false;
            var editrow = null;
            $("#postage-area tbody").find(".area-text").each(function() {
                if ($(this).text().trim() == $(modalBtnOk).attr('provRow')) {
                    editOperation = true;
                    editrow = this;
                }
            });

            if (editrow != null) {
                $(editrow).parent().html(row);
            } else {
                $("#postage-area tbody").append('<tr>' + row + '</tr>');
            }
        });


        //保存店铺邮费设置
        $("#savePostAgeSet").on('click', function() {
            //compose json request
            var customizedPostage = [];
            var flag = false;

            var name = $("#name").val();
            if(name == ''){
                alert('请输入运费模板名称');
                return;
            }

            $("#postage-area tbody").find(".editProvince").each(function(i,item) {
                var _this = $(this);
                var ids = $(this).attr('zone-ids').split(',');
                var names = $(this).attr('zone-names').split(',');

                var areas = [];
                $.each(ids, function(index, zoneId) {
                    areas.push({"id":zoneId, "name":names[index]});
                });

                var first = $(_this.closest("tr")).find("input[name='first']")[0].value;
                var firstFee = $(_this.closest("tr")).find("input[name='firstFee']")[0].value;
                var second = $(_this.closest("tr")).find("input[name='second']")[0].value;
                var secondFee = $(_this.closest("tr")).find("input[name='secondFee']")[0].value;
                if(first == '' || first == '0'){
                    alert('请输入首件数量');
                    flag = true;
                    return;
                }
                if(firstFee == ''){
                    alert('请输入首件运费');
                    flag = true;
                    return;
                }
                if(second == '' || second == '0'){
                    alert('请输入续件数量');
                    flag = true;
                    return;
                }
                if(secondFee == ''){
                    alert('请输入续件运费');
                    flag = true;
                    return;
                }

                customizedPostage.push({
                    "first": parseFloat(first),
                    "firstFee": parseFloat(firstFee),
                    "second": parseFloat(second),
                    "secondFee": parseFloat(secondFee),
                    "postage": parseFloat($(_this.closest("tr")).find("input[name='firstFee']")[0].value),
                    "areas": areas
                });
            });

            if(flag) return;

            var data = {
                "id": $proId,
                "name": $("#name").val(),
                "shopId": $("#shopId").val(),
                "type": $("input[name='valuation_type']:checked").val(),
                "freeShippingPrice" : parseFloat($("#freeShippingPrice").val()),
                "freeShippingFirstPay": $('#freeShippingFirstPay').is(':checked'),
                "postageStatus": true,
                "postage": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='firstFee']")[0].value),
                "defaultFirst": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='first']")[0].value),
                "defaultFirstFee": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='firstFee']")[0].value),
                "defaultSecond": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='second']")[0].value),
                "defaultSecondFee": parseFloat($($("#postage-area tbody").first("tr")).find("input[name='secondFee']")[0].value),
                "customizedPostage": customizedPostage
            };

            console.log(data);

            var config = {
                "json":true
            };

            utils.postAjaxWithBlock($(document), window.host + '/shop/updatePostageSet', JSON.stringify(data), function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("邮费设置保存成功", {timer: 1200, type: 'success'});
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("邮费设置保存失败,请稍后再试", {timer: 1200});
                }
            }, config);
        });

        function fillSet(postSet) {
            //设置最低包邮价格
            if (postSet.freeShippingPrice != null) {
                $("#freeShippingPrice").val(postSet.freeShippingPrice);
            }
            if (postSet.freeShippingFirstPay != null) {
                $('#freeShippingFirstPay').prop('checked', postSet.freeShippingFirstPay);
            }
            if (postSet.name != null && postSet.name != '') {
                $("#name").val(postSet.name);
            }

            if (postSet.type != null && postSet.type != '') {
                $(":radio[name='valuation_type'][value='" + postSet.type + "']").prop("checked", "checked");
            }

            if (postSet.type == '1') {
                $("#woman").attr("checked", "checked").trigger('change');
            }

            var html = '';
            //设置默认运费

            var defaultRow = '<tr>'
            + '<td class="area-text">默认运费</td>'
            + '<td class="text-center"><input type="number" name="first" class="w90 form-control" value="' + postSet.defaultFirst + '"></td>'
            + '<td class="text-center"><input type="text" name="firstFee" class="w90 form-control" value="' + (postSet.defaultFirstFee == null ? '0' : postSet.defaultFirstFee) + '"></td>'
            + '<td class="text-center"><input type="number" name="second" class="w90 form-control" value="' + postSet.defaultSecond + '"></td>'
            + '<td class="text-center"><input type="text" name="secondFee" class="w90 form-control" value="' + (postSet.defaultSecondFee == null ? '0' : postSet.defaultSecondFee) + '"></td>'
            + '<td></td>'
            + '</tr>';

            html += defaultRow;

            //设置特定区域运费
            $.each(postSet.customizedPostage, function(index, item) {
                var zoneIds = [];
                var provinceStr = '';
                $.each(item.areas, function(j, area) {
                    zoneIds.push(area.id);
                    provinceStr += (provinceStr == '' ?  '' : ',') +  area.name.trim();
                });

                var row = '<tr><td class="area-text">' + provinceStr + '</td><td class="text-center">' +
                    '<input type="number" name="first" class="w90 form-control" value="' + item.first+ '"></td><td class="text-center">' +
                    '<input type="text" name="firstFee" class="w90 form-control" value="' + item.firstFee + '"></td><td class="text-center">' +
                    '<input type="number" name="second" class="w90 form-control" value="' + item.second + '"></td><td class="text-center">' +
                    '<input type="text" name="secondFee" class="w90 form-control" value="' + item.secondFee + '"></td><td>' +
                    '<a class="pull-right editProvince" zone-ids="' + zoneIds + '" zone-names="' + provinceStr + '" data-toggle="modal" data-target="#modal_region"><i class="icon-pencil7"></i> 编辑</a></td></tr>';
                html += row;
            });

            $("#postage-area tbody").html(html);
        }

        var province = [];
        function init() {

            $("#woman").attr("checked", "checked").trigger('change');
            utils.postAjaxWithBlock($(document), window.host+'/zone/1/children', [] , function(res) {
                if (typeof(res) === 'object') {
                    $.each(res.data, function(index, item) {
                        province.push(item);
                    });
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("无法获取区域信息,请稍后再试", {timer: 1200});
                }
            });

            utils.postAjaxWithBlock($(document), window.host + '/shop/getPostageSet?id=' + $proId, [] , function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            fillSet(res.data);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("获取当前配送方式失败", {timer: 1200});
                }
            });
        }

        init();
    });



//微信账号绑定
require(['all']);


require(['mall/logistics']);

define("mallLogistics", function(){});

