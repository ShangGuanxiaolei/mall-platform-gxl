/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/24.
 */
define('comment/list',['jquery','utils','datatables','blockui','bootbox', 'select2', 'daterangepicker'], function($,utils,datatabels,blockui,select2,daterangepicker) {

    var $listUrl = window.host + "/comment/view/pc";

    var $orders = ['time'];

    //var $shopId = null;

    var $order = '';

    var $commentType = '';

    var $isBlocked = '';

    //replysdatables初始化的时候,如果传递一个空值,则会转向一个错误接口 Comment/{id}/reply ->Comment/reply ,所以这里给定一个默认值
    var $commentId='initID';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 初始化日期控件 **/
    var options = {
        timePicker: true,
        dateLimit: { days: 60000 },
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY-MM-DD h:mm a',
            separator: ' - ',
            applyLabel: '确定',
            startLabel: '开始日期:',
            endLabel: '结束日期:',
            cancelLabel: '取消',
            weekLabel: 'W',
            customRangeLabel: '日期范围',
            daysOfWeek: [ "一", "二", "三", "四", "五", "六", "日" ],
            monthNames: [ "1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月" ],
            firstDay: 6
        },
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
    };

    $('.daterange-time').daterangepicker(options);

    /** 回调 **/
    $('.daterange-time').on('apply.daterangepicker', function(ev, picker) {
        console.log(picker.startDate.format('YYYY-MM-DD'));
        console.log(picker.endDate.format('YYYY-MM-DD'));
        options.startDate = picker.startDate;
        options.endDate   = picker.endDate;
        $("#groupon_valid_from").val(picker.startDate.format('YYYY-MM-DD HH:mm'));
        $("#groupon_valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    // 全选
    $("#checkAllGoods").on('click', function() {
        $("input[name='checkGoods']").prop("checked", $(this).prop("checked"));
    });


    $(".btn-batchDel").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });

        var url = window.host + "/comment/batchDelete/" + ids;
        utils.postAjax(url,{},function(res){
                if(typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("删除成功!", {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                    }
                }
        });

    });

    $(".btn-batchBlock").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });
        //batchBlock(ids);
        var url = window.host + "/comment/batchBlock/" + ids;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    utils.tools.alert("屏蔽成功!", {timer: 1200, type: 'warning'});
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });

    });

    $(".btn-batchUnBlock").on('click', function() {
        var updateds = getTableContent();
        if(updateds.length == 0 ){
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return ;
        }
        var ids = '';
        $.each(updateds, function(index, row){
            if(ids != '') {
                ids += ',';
            }
            ids += row.id;
        });
        //batchUnblock(ids);

        var url = window.host + "/comment/batchUnBlock/" + ids;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    utils.tools.alert("撤销屏蔽成功!", {timer: 1200, type: 'warning'});
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    });

    // 得到当前选中的行数据
    function getTableContent(){
        var selectRows = new Array();
        for(var i = 0; i < $("input[name='checkGoods']:checked").length; i++){
            var value = {};
            var checkvalue = $("input[name='checkGoods']:checked")[i];
            value.id = $(checkvalue).attr("rowid");
            selectRows.push(value);
        }
        return selectRows;
    }



    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });


    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                // keyword: data.search.value,
                // pageable: true,
                order: function () {
                    if($order != ''){
                        return $order;
                    }else{
                        var _index = data.order[0].column;
                        if ( _index < 1){
                            return '';
                        } else {
                            return $orders[_index - 1];
                        }
                    }
                },
                direction: data.order ? data.order[0].dir :'asc',
                type : $commentType,
                blocked:$isBlocked,
            }, function(res) {
                if (!res.data) {
                    res.data = [];
                }
                callback({
                   data:res.data.list,
                    recordsTotal: res.data.count,
                    recordsFiltered: res.data.count,
                    data: res.data.list,
                    iTotalRecords:res.data.count,
                    iTotalDisplayRecords:res.data.count
                });


            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'+row.id+'" value="'+row.id+'"></label>';
                }
            },
            {
                width: "30px",
                orderable: false,
                render: function(data, type, row){
                    return '<img class="goods-image" src="'+row.avatar+'" />';
                }
            },
            {
                data: "loginname",
                width: "30px",
                orderable: false,
                name:"loginname"
            },

            {
                data: "objId",
                width: "30px",
                orderable: false,
                name:"objId"
            },

            {
                data: "content",
                width: "120px",
                orderable: false,
                name:"content"
            },
            {
                data: "likeCount",
                width: "30px",
                orderable: false,
                name:"likeCount"
            },
            {
                orderable: true,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },
            {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    if(row.blocked == true){
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="downpopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>撤销屏蔽</a>';
                    }else{
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="uppopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>屏蔽</a>';
                    }
                    html += '<a href="javascript:void(0);" class="replys" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7"></i>查看回复</a>';
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_product"><i class="icon-trash"></i>删除</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $replysdatatables = $('#xquark_list_replys_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get(window.host + "/comment/"+$commentId+"/reply", {
                size: data.length,
                page: (data.start / data.length),
                // keyword: data.search.value,
                pageable: true,
            }, function(res) {
                if (!res.data) {
                    res.data = [];
                }
                callback({
                    recordsTotal: res.data.count,
                    recordsFiltered: res.data.count,
                    data: res.data.list,
                    iTotalRecords:res.data.count,
                    iTotalDisplayRecords:res.data.count
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "loginname",
                width: "120px",
                orderable: false,
                name:"title"
            }, {
                data: "content",
                width: "50px",
                orderable: false,
                name:"price"
            },
            {
                orderable: true,
                width: "100px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    if(row.blocked == true){
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="replyDownpopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>撤销屏蔽</a>';
                    }else{
                        html += '<a href="javascript:void(0);" class="up role_check_table" style="margin-left: 10px;" data-toggle="replyUppopover" rowId="'+row.id+'" fid="up_shelves"><i class="icon-pencil7"></i>屏蔽</a>';
                    }
                    html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="replyPopover" rowId="'+row.id+'" fid="delete_product"><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        drawCallback: function () {  //数据加载完成
            initReplyEvent();
        }
    });


    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');



        // 回复管理
        $(".replys").on('click', function() {
            var rowId =  $(this).attr("rowId");
            $commentId = rowId;
            $replysdatatables.search('').draw();
            $("#modal_replys").modal("show");
        });



        /** 点击屏蔽弹出框 **/
        $("[data-toggle='uppopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="uppopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="uppopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                blockComment(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover('hide');
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "uppopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="uppopover"]').popover('hide');
            } else if (target.data("toggle") == "uppopover") {
                target.popover("show");
            }
        });

        /** 点击撤销屏蔽弹出框 **/
        $("[data-toggle='downpopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认撤销屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="downpopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="downpopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                unblockComment(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "downpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="downpopover"]').popover('hide');
            } else if (target.data("toggle") == "downpopover") {
                target.popover("show");
            }
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteComment(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("show");
            }
        });


    }

    function initReplyEvent() {

        /** 点击屏蔽弹出框 **/
        $("[data-toggle='replyUppopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyUppopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyUppopover"]').popover('hide');
            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                blockReply(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover('hide');
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyUppopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyUppopover"]').popover('hide');
            } else if (target.data("toggle") == "replyUppopover") {
                target.popover("show");
            }
        });

        /** 点击撤销屏蔽弹出框 **/
        $("[data-toggle='replyDownpopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation: true,
            content: function () {
                var rowId =  $(this).attr("rowId");
                return '<span>确认撤销屏蔽吗？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyDownpopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyDownpopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click", function () {
                var id = $(this).attr("mId");
                unblockReply(id);
            });
            $('.popover-btn-cancel').on("click", function () {
                $(that).popover("hide");
            });
        });

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyDownpopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyDownpopover"]').popover('hide');
            } else if (target.data("toggle") == "replyDownpopover") {
                target.popover("show");
            }
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='replyPopover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="replyPopover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="replyPopover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteReply(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_list_replys_tables');

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "replyPopover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="replyPopover"]').popover('hide');
            } else if(target.data("toggle") == "replyPopover"){
                target.popover("show");
            }
        });

    }



    /*删除评论*/
    function  deleteComment(cId){
        var url = window.host + "/comment/delete/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }


    /*删除回复*/
    function  deleteReply(rId){
        var url = window.host + "/comment/reply/delete/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }




    /*屏蔽一条评论*/
    function  blockComment(cId){
        var url = window.host + "/comment/block/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*撤销对评论的屏蔽*/
    function  unblockComment(cId){
        var url = window.host + "/comment/unblock/"+cId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $datatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*屏蔽一条回复*/
    function  blockReply(rId){
        var url = window.host + "/comment/reply/block/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }

    /*撤销对回复的屏蔽*/
    function  unblockReply(rId){
        var url = window.host + "/comment/reply/unblock/"+rId;
        utils.postAjax(url,{},function(res){
            if(typeof(res) === 'object') {
                if (res.data) {
                    $replysdatatables.search('').draw();
                } else {
                    utils.tools.alert("撤销屏蔽失败!", {timer: 1200, type: 'warning'});
                }
            }
        });
    }



    $('body').on('change','select[name="commentType"]', function(event) {
        $commentType = $(this).val();
        $listUrl = window.host + "/comment/view/pc";
        $datatables.search('').draw();
    });


    $('body').on('change','select[name="isBlocked"]', function(event) {
        $isBlocked = $(this).val();
        $listUrl = window.host + "/comment/view/pc";
        $datatables.search('').draw();
    });



});
/**
 * Created by quguangming on 16/5/24.
 */
require(['all']);

//module
require(['jquery','uniform','placeholder','comment/list'],function(){


});
define("comment", function(){});

