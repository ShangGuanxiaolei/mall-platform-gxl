/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * 封装日期控件
 */
define('datePicker',['jquery', 'daterangepicker', 'moment'], function ($) {

  /** 初始化日期控件 **/
  const options = {
    timePicker: true,
    dateLimit: {days: 60000},
    startDate: moment().subtract(0, 'month').startOf('month'),
    endDate: moment().subtract(0, 'month').endOf('month'),
    autoApply: false,
    timePickerIncrement: 1,
    locale: {
      format: 'YYYY/MM/DD',
      separator: ' - ',
      applyLabel: '确定',
      fromLabel: '开始日期:',
      toLabel: '结束日期:',
      cancelLabel: '清空',
      weekLabel: 'W',
      customRangeLabel: '日期范围',
      daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月",
        "11月", "12月"],
      firstDay: 6
    },
    ranges: {
      '今天': [moment(), moment().endOf('day')],
      '一星期': [moment(), moment().add(6, 'days')],
      '一个月': [moment(), moment().add(1, 'months')],
      '一年': [moment(), moment().add(1, 'year')]
    },
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small btn-default'

  };

  /* 单时间控件 */
  const singlePickerOptions = {
    singleDatePicker: true,
    showDropdowns: true
  };

  /**
   * 构造函数，创建一个dateRangePicker
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function DatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(options, function (start, end) {
      if (start._isValid && end._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm') + ' - '
            + end.format(
                'YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      console.log(picker.endDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      options.endDate = picker.endDate;
      params.onApply(picker.startDate.format('YYYY-MM-DD HH:mm'),
          picker.endDate.format('YYYY-MM-DD HH:mm'));
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
      //do something, like clearing an input
      $(this).val('');
      params.onEmpty();
    });

  }

  /**
   * 构造函数， 构造单时间控件
   * @param params.dom 日期控件dom节点
   * @param params.onApply 成功时的回调函数
   * @param params.onEmpty 清空时的回调函数
   * @constructor
   */
  function SingleDatePicker(params) {
    this.$dateRangeBasic = $(params.dom);

    this.$dateRangeBasic.daterangepicker(singlePickerOptions, function (start) {
      if (start._isValid) {
        $(this).val(start.format('YYYY-MM-DD HH:mm'));
      } else {
        $(this).val('');
      }
    });

    /** 回调 **/
    this.$dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      options.startDate = picker.startDate;
      var dateStr = picker.startDate.format('YYYY-MM-DD HH:mm');
      if (params.onApply) {
        params.onApply(dateStr);
      } else {
        $(this).val(dateStr)
      }
    });

    /**
     * 清空按钮清空选框
     */
    this.$dateRangeBasic.on('cancel.daterangepicker', function () {
      //do something, like clearing an input
      $(this).val('');
      if (params.onEmpty) {
        params.onEmpty();
      }
    });
  }

  /**
   * 原型，所有类型日期控件共有
   * @type {{resetStr: resetStr}}
   */
  const commonPrototype = {
    /**
     * 重置日期字符串
     * @param str
     */
    resetStr: function (str) {
      this.$dateRangeBasic.val(str);
    }
  };

  /**
   * 继承commonPrototype
   * @type {{resetStr: resetStr}}
   */
  DatePicker.prototype = SingleDatePicker.prototype = commonPrototype;

  return {

    /**
     * 初始化日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createPicker: function (params) {
      return new DatePicker(params);
    },

    /**
     * 初始化单日期控件
     * @param params.dom 日期控件dom节点
     * @param params.onApply 成功时的回调函数
     * @param params.onEmpty 清空时的回调函数
     */
    createSinglePicker: function (params) {
      return new SingleDatePicker(params);
    }

  }

});

define('productModalSingle',['jquery', 'datatables'], function ($) {

  var $orders = ['price', 'amount', 'sales', 'onsale'];
  var $order = '';
  var $productListUrl = window.host + "/product/list";

  var $shopId = null;
  var $category = '';
  var $pDataTable;

  const $modal = $('#choose-product-modal');

  //初始化商品分类信息
  $.ajax({
    url: window.host + '/shop/category/list',
    type: 'POST',
    dataType: 'json',
    data: {},
    success: function (data) {
      if (data.errorCode === 200) {
        var dataLength = data.data.length;
        for (var i = 0; i < dataLength; i++) {
          $("#categoryType").append('<option value=' + data.data[i].id + '>'
              + data.data[i].name + '</option>');
        }
        ;
      } else {
        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
      }
    },
    error: function (state) {
      if (state.status === 401) {
      } else {
        utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
      }
    }
  });

  /**
   * 标签搜索
   */
  $('body').on('change', 'select[name="categoryType"]', function () {
    $productListUrl = window.host + "/product/list";
    $category = $(this).val();
    $pDataTable.search('').draw();
  });

  /**
   * 关键字搜索
   */
  $(".btn-search-products").on('click', function () {
    var keyword = $.trim($("#select_products_sKeyword").val());
    if (keyword !== '' && keyword.length > 0 && shopId !== null) {
      $productListUrl = window.host + '/product/searchbyPc/' + shopId + '/'
          + keyword;
      $pDataTable.search(keyword).draw();
    } else if (keyword === '' || keyword.length === 0) {
      $productListUrl = window.host + "/product/list";
      $pDataTable.search('').draw();
    }

  });

  function initTable(params) {
    $pDataTable = $('#xquark_select_products_tables').DataTable({
      paging: true, //是否分页
      filter: false, //是否显示过滤
      lengthChange: false,
      processing: true,
      serverSide: true,
      deferRender: true,
      searching: true,
      ajax: function (data, callback, settings) {
        $.get($productListUrl, {
          size: data.length,
          page: (data.start / data.length),
          keyword: data.search.value,
          pageable: true,
          order: function () {
            if ($order !== '') {
              return $order;
            } else {
              var _index = data.order[0].column;
              if (_index < 4) {
                return '';
              } else {
                return $orders[_index - 4];
              }
            }
          },
          direction: data.order ? data.order[0].dir : 'asc',
          category: $category,
          isGroupon: ''
        }, function (res) {
          if (!res.data.list) {
            res.data.list = [];
          } else {
            if (res.data.shopId) {
              $shopId = res.data.shopId;
            }
          }
          callback({
            recordsTotal: res.data.categoryTotal,
            recordsFiltered: res.data.categoryTotal,
            data: res.data.list,
            iTotalRecords: res.data.categoryTotal,
            iTotalDisplayRecords: res.data.categoryTotal
          });
        });
      },
      rowId: "id",
      columns: [
        {
          width: "30px",
          orderable: false,
          render: function (data, type, row) {
            return '<a href="' + row.productUrl
                + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
          }
        },
        {
          data: "name",
          width: "120px",
          orderable: false,
          name: "name"
        },
        {
          data: "code",
          width: "40px",
          orderable: false,
          name: "code"
        },
        {
          width: "40px",
          orderable: false,
          render: function (data, type, row) {
            var status = '';
            switch (row.status) {
              case 'INSTOCK':
                status = '下架';
                break;
              case 'ONSALE':
                status = '在售';
                break;
              case 'FORSALE':
                status = '待上架发布';
                break;
              case 'DRAFT':
                status = '未发布';
                break;
              default:
                break;
            }
            return status;
          },
        }, {
          data: "price",
          width: "50px",
          orderable: true,
          name: "price"
        }, {
          data: "amount",
          orderable: true,
          width: "50px",
          name: "amount"
        },
        {
          data: "sales",
          orderable: true,
          width: "50px",
          name: "sales"
        }, {
          orderable: false,
          width: "180px",
          render: function (data, type, row) {
            if(row.createAt == null){
              return "";
            }
            var cDate = parseInt(row.createAt);
            var d = new Date(cDate);
            return d.format('yyyy-MM-dd hh:mm:ss');
          },
          name: "onsaleAt"
        }, {
          sClass: "right",
          width: "120px",
          orderable: false,
          render: function (data, type, row) {
            var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                + row.id + '" productName="' + row.name + '"productPrice="' + row.price
                + '" ><i class="icon-pencil7" ></i>选择</a>';
            return html;
          }
        }
      ],
      drawCallback: function () {  //数据加载完成
        initSelectProductEvent(params);
      }
    });
  }

  function initSelectProductEvent(params) {
    $(".selectproduct").on("click", function () {
      var id = $(this).attr("rowId");
      var name = $(this).attr("productName");
      var price = $(this).attr("productName");
      //回调外部函数
      if (params && params.onSelect) {
        params.onSelect(id, name, price);
      }
      $modal.modal("hide");
    });
  }

  return {
    /**
     * 创建商品表格
     * @param params
     */
    create: function (params) {
      initTable(params);

      /* 选择活动商品 */
      $(params.dom).on('focus', function () {
        $modal.modal("show");
      });
    },
    refresh: function () {
      $pDataTable.search('').draw();
    }
  }
});
define('productSaleZoneModal',['jquery', 'datatables'], function ($) {

  var $orders = ['price', 'amount', 'sales', 'onsale'];
  var $order = '';
  var $productListUrl = window.host + "/product/list";

  var $shopId = null;
  var $category = '';
  var $pDataTable;

  const $modal = $('#choose-product-modal');

  //初始化商品分类信息
  $.ajax({
    url: window.host + '/shop/category/list',
    type: 'POST',
    dataType: 'json',
    data: {},
    success: function (data) {
      if (data.errorCode === 200) {
        var dataLength = data.data.length;
        for (var i = 0; i < dataLength; i++) {
          $("#categoryType").append('<option value=' + data.data[i].id + '>'
              + data.data[i].name + '</option>');
        }
        ;
      } else {
        utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
      }
    },
    error: function (state) {
      if (state.status === 401) {
      } else {
        utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
      }
    }
  });

  /**
   * 标签搜索
   */
  $('body').on('change', 'select[name="categoryType"]', function () {
    $productListUrl = window.host + "/product/list";
    $category = $(this).val();
    $pDataTable.search('').draw();
  });

  /**
   * 关键字搜索
   */
  $(".btn-search-products").on('click', function () {
    var keyword = $.trim($("#select_products_sKeyword").val());
    if (keyword !== '' && keyword.length > 0 && shopId !== null) {
      $productListUrl = window.host + '/product/searchbyPc/' + shopId + '/'
          + keyword;
      $pDataTable.search(keyword).draw();
    } else if (keyword === '' || keyword.length === 0) {
      $productListUrl = window.host + "/product/list";
      $pDataTable.search('').draw();
    }

  });

  function initTable(params) {
    $pDataTable = $('#select_products_tables_sale_zone').DataTable({
      paging: true, //是否分页
      filter: false, //是否显示过滤
      lengthChange: false,
      processing: true,
      serverSide: true,
      deferRender: true,
      searching: true,
      ajax: function (data, callback, settings) {
        $.get($productListUrl, {
          size: data.length,
          page: (data.start / data.length),
          keyword: data.search.value,
          pageable: true,
          order: function () {
            if ($order !== '') {
              return $order;
            } else {
              var _index = data.order[0].column;
              if (_index < 4) {
                return '';
              } else {
                return $orders[_index - 4];
              }
            }
          },
          direction: data.order ? data.order[0].dir : 'asc',
          category: $category,
          isGroupon: ''
        }, function (res) {
          if (!res.data.list) {
            res.data.list = [];
          } else {
            if (res.data.shopId) {
              $shopId = res.data.shopId;
            }
          }
          callback({
            recordsTotal: res.data.categoryTotal,
            recordsFiltered: res.data.categoryTotal,
            data: res.data.list,
            iTotalRecords: res.data.categoryTotal,
            iTotalDisplayRecords: res.data.categoryTotal
          });
        });
      },
      rowId: "id",
      columns: [
        {
          width: "30px",
          orderable: false,
          render: function (data, type, row) {
            return '<a href="' + row.productUrl
                + '"><img class="goods-image" src="' + row.imgUrl + '" /></a>';
          }
        },
        {
          data: "name",
          width: "120px",
          orderable: false,
          name: "name"
        },
        {
          data: "code",
          width: "40px",
          orderable: false,
          name: "code"
        },
        {
          width: "40px",
          orderable: false,
          render: function (data, type, row) {
            var status = '';
            switch (row.status) {
              case 'INSTOCK':
                status = '下架';
                break;
              case 'ONSALE':
                status = '在售';
                break;
              case 'FORSALE':
                status = '待上架发布';
                break;
              case 'DRAFT':
                status = '未发布';
                break;
              default:
                break;
            }
            return status;
          },
        }, {
          data: "price",
          width: "50px",
          orderable: true,
          name: "price"
        }, {
          data: "amount",
          orderable: true,
          width: "50px",
          name: "amount"
        },
        {
          data: "sales",
          orderable: true,
          width: "50px",
          name: "sales"
        }, {
          orderable: false,
          width: "180px",
          render: function (data, type, row) {
            if(row.createAt == null){
              return "";
            }
            var cDate = parseInt(row.createAt);
            var d = new Date(cDate);
            return d.format('yyyy-MM-dd hh:mm:ss');
          },
          name: "onsaleAt"
        }, {
          sClass: "right",
          width: "120px",
          orderable: false,
          render: function (data, type, row) {
            var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                + row.id + '" productName="' + row.name + '"productPrice="' + row.price
                + '" ><i class="icon-pencil7" ></i>选择</a>';
            return html;
          }
        }
      ],
      drawCallback: function () {  //数据加载完成
        initSelectProductEvent(params);
      }
    });
  }

  function initSelectProductEvent(params) {
    $(".selectproduct").on("click", function () {
      var id = $(this).attr("rowId");
      var name = $(this).attr("productName");
      var price = $(this).attr("productName");
      //回调外部函数
      if (params && params.onSelect) {
        params.onSelect(id, name, price);
      }
      $modal.modal("hide");
    });
  }

  return {
    /**
     * 创建商品表格
     * @param params
     */
    create: function (params) {
      initTable(params);

      /* 选择活动商品 */
      $(params.dom).on('focus', function () {
        $modal.modal("show");
      });
    },
    refresh: function () {
      $pDataTable.search('').draw();
    }
  }
});
define('fileUploader',['jquery', 'dropzone'], function ($) {

  const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

  /**
   * 新建dropZone封装实例
   * @param params.onSuccess 成功后的回调函数，返回地址
   * @param params.dom drop的dom节点
   * @param params.url 上传文件地址，不指定则使用默认的
   * @constructor
   */
  function Uploader(params) {
    /* jquery 的dropZone实例 */
    this.jDropInstance = $(params.dom).dropzone({
      url: defaultUrl,
      paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
      dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
      maxFilesize: 10, // MB      //最大文件大小，单位是 MB
      maxFiles: 10,               //限制最多文件数量
      maxThumbnailFilesize: 10,
      addRemoveLinks: true,
      thumbnailWidth: "150",      //设置缩略图的缩略比
      thumbnailHeight: "150",     //设置缩略图的缩略比
      acceptedFiles: ".gif,.png,.jpg",
      uploadMultiple: false,
      dictInvalidFileType: "文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
      dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
      dictRemoveFile: "删除",                                        //移除文件链接的文本
      dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
      dictResponseError: "服务器暂无响应,请稍后再试!",
      dictCancelUpload: "取消上传",
      dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
      dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
      autoDiscover: false,
      //autoProcessQueue: false,
      init: function () {

        console.log(this);

        // var imgDropzone = this;
        //添加了文件的事件
        this.on("addedfile", function (file) {
          $(".btn-submit").addClass("disabled");
          if (file && file.dataImg && file.previewElement) { //是网络加载的数据
            $(file.previewElement).attr("data-img", file.dataImg);
            if (file.size === '' || file.length === 0) {
              $(file.previewElement).find(".dz-details").hide();
            }
          }
          //imgDropzone.processQueue();
        });
        this.on("success", function (file, data) {
          if (typeof(data) === 'object') {
            switch (data.errorCode) {
              case 200: {
                if (typeof(data.data) === 'object') {
                  var imgId = data.data[0].id;
                  if (file && file.previewElement) {
                    $(file.previewElement).attr("data-img", imgId);
                    if (params.onSuccess) {
                      params.onSuccess(imgId);
                    }
                  }
                }
                break;
              }
              default: {
                utils.tools.alert("图像上传失败,请重新选择!",
                    {timer: 1200, type: 'warning'});
                break;
              }
            }
          } else {
            if (data === -1) {
              utils.tools.alert("图像上传失败,请重新选择!",
                  {timer: 1200, type: 'warning'});
            }
          }
          $(".btn-submit").removeClass("disabled");
        });

        this.on("error", function () {
          utils.tools.alert("文件上传失败!", {timer: 1200, type: 'warning'});
          $(".dz-error-message").html("文件上传失败!");
          $(".btn-submit").removeClass("disabled");
        });

        this.on("complete", function () {   //上传完成,在success之后执行
          $(".btn-submit").removeClass("disabled");
        });

        this.on("thumbnail", function (file) {
          if (file && file.previewTemplate) {
            file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
            file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
          }
          const $dzImage = $('.dz-image');
          $dzImage.css("height", "150px;");
          $dzImage.css("width", "150px;");
        });

      }
    });

    /* 真实的dropZone实例 */
    this.dropZone = this.jDropInstance[0].dropzone;

    console.log(this.dropZone);

    /**
     * 清空dropZone的上传文件
     */
    this.clear = function () {
      this.dropZone.removeAllFiles(true);
      return this;
    };

    /**
     * 添加图片预览到dropZone
     * @param imgUrl 图片地址
     */
    this.addImage =  function(imgUrl) {
      if (imgUrl) {
        var mockFile = {name: "", size: "", dataImg: imgUrl};
        console.log(this);
        this.dropZone.emit("addedfile", mockFile);
        this.dropZone.emit("thumbnail", mockFile, imgUrl);
        this.dropZone.emit("complete", mockFile);
        this.dropZone.files.push( mockFile ); // 此处必须手动添加才可以用removeAllFiles移除
      }
      return this;
    }
  }

  return {
    /**
     * 新建一个文件上传实例并返回
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @returns {Uploader}
     */
    createUploader: function (params) {
      return new Uploader(params);
    }
  }

});

define('validator',['utils'], function (utils) {

  /**
   * 参数校验
   * @param rules 校验规则
   * @param data 参数对象
   * @param messageHandler 处理错误消息的函数
   * @returns {boolean}
   */
  function validateData(rules, data, messageHandler) {
    for (var k in data) {
      if (data.hasOwnProperty(k)) {
        if (rules.hasOwnProperty(k)) {
          var value = data[k];
          // 循环参数校验
          for (var i in rules[k]) {
            if (rules[k].hasOwnProperty(i)) {
              var checkObj = rules[k][i];
              var checker = checkObj.checker;
              if (!checker.call(null, value)) {
                if (!messageHandler) {
                  messageHandler = function (message) {
                    utils.tools.alert(message, { timer: 1200, type: 'warning' });
                  }
                }
                messageHandler(checkObj.message);
                return false;
              }
            }
          }
        }
      }
    }
    return true
  }

  return {
    validate: validateData
  }

});

define('flashSale/list',['jquery', 'utils', 'datePicker', 'productModalSingle', 'productSaleZoneModal', 'fileUploader',
      'validator',
      'jquerySerializeObject',
      'datatables', 'blockui', 'select2'],
    function ($, utils, datePicker, productModal, productSaleZoneModal, uploader, validator) {

      const prefix = window.host + '/flashSale';
      const listUrl = prefix + '/listPc';
      const saveUrl = prefix + '/saveProduct';
      const saveAreaUrl = prefix + '/saveAreaProduct';
      const closeUrl = prefix + '/close';
      const viewUrl = prefix + '/view';
      const checkInPromotionUrl = prefix + '/checkInPromotion';
      const saveSaleZoneTitleUrl = prefix + '/saveSaleZoneTitle?saleZoneTitle=';
      const findSaleZoneTitleUrl = prefix + '/findSaleZoneTitle?promotionType=';
      const findDateByBatchNoUrl = prefix + '/findDateByBatchNo?batchNo=';

      var $dataTable;

      var globalDatePicker;

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

      const manager = (function () {

        const $addFlashSaleBtn = $('.flash-sale-add');
        const $addAreaGoodsBtn = $('.area-goods-add');
        const $saveFlashSaleBtn = $('.saveFlashSaleBtn');
        const $saveAreaGoodsBtn = $('.saveAreaGoodsBtn');
        const $modalFlashSale = $('#modal_flash_sale');
        const $modalAreaGoods = $('#modal_area_goods');

        const $validFrom = $('#valid_from');
        const $validTo = $('#valid_to');
        const $areaValidFrom = $('#area_valid_from');
        const $areaValidTo = $('#area_valid_to');

        const $form = $('#flashSaleForm');
        const $areaFrom = $('#areaGoodsForm');

        const $img = $('#img');
        const $areaImg = $('#area_img');

        let imgUploader = uploader.createUploader({
          dom: '#product_image_dropzone',
          onSuccess: function (img) {
            $img.val(img);
          }
        });

        let imgUploader2 = uploader.createUploader({
          dom: '#area_product_image_dropzone',
          onSuccess: function (img) {
            $areaImg.val(img);
          }
        });

        const notNull = function (field) {
          return field && field !== '';
        };

        const betweenOneToTen = function (field) {
          return field >= 1 && field <= 10;
        };

        const notNegative = function (field) {
          return field && parseFloat(field) > 0;
        };

        const validationRules = {
          title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          discount: [
            {
              checker: notNull,
              message: '请输入抢购价格'
            },
            {
              checker: betweenOneToTen,
              message: '抢购折扣必须在1到10折之间'
            }
          ],
          amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ],
          limitAmount: [
            {
              checker: notNull,
              message: '请输入限购库存'
            },
            {
              checker: notNegative,
              message: '限购库存不能小于0'
            }
          ],
          area_title: [
            {
              checker: notNull,
              message: '请填写活动标题'
            }
          ],
          batchNo: [
            {
              checker: notNull,
              message: '请输入活动批次'
            },
            {
              checker: notNegative,
              message: '活动批次不能小于0'
            }
          ],
          area_img: [
            {
              checker: notNull,
              message: '请选择活动图片'
            }
          ],
          area_validTo: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          area_validFrom: [
            {
              checker: notNull,
              message: '请选择活动时间'
            }
          ],
          area_productId: [
            {
              checker: notNull,
              message: '请选择商品'
            }
          ],
          price: [
            {
              checker: notNull,
              message: '请输入活动价格'
            },
            {
              checker: notNegative,
              message: '活动价格不能小于0'
            }
          ],
          point: [
            {
              checker: notNegative,
              message: '德分不能小于0'
            }
          ],
          area_amount: [
            {
              checker: notNull,
              message: '请输入活动库存'
            },
            {
              checker: notNegative,
              message: '活动库存不能小于0'
            }
          ],
          area_limitAmount: [
            {
              checker: notNull,
              message: '请输入限购数量'
            },
            {
              checker: notNegative,
              message: '限购数量不能小于0'
            }
          ]
        };

        const globalInstance = {

          bindEvent: function () {

            /* 点击显示增加预购商品弹窗 */
            $addFlashSaleBtn.on('click', function () {
              utils.tools.syncForm($form);
              // FIXME reset没有生效, 先手动设置
              $('#promotionId').val('');
              $('#id').val('');
              $('#productId').val('');
              $('#img').val('');
              $('#valid_from').val('');
              $('#valid_to').val('');
              imgUploader.clear();
              $modalFlashSale.modal('show');
            });

            $addAreaGoodsBtn.on('click', function () {
              utils.tools.syncForm($areaFrom);
              // FIXME reset没有生效, 先手动设置
              $('#area_promotionId').val('');
              $('#area_id').val('');
              $('#area_productId').val('');
              $('#area_img').val('');
              $('#area_valid_from').val('');
              $('#area_valid_to').val('');
              $('#promotionBatch').val('');
              document.getElementById("usedPoint").style.display="none";//隐藏
              $("#promotionDate").removeAttr("disabled");
              $('#logisticsFee').val('');
              document.getElementById('price').style['width'] = '';
              imgUploader2.clear();
              $modalAreaGoods.modal('show');
            });

            /* 保存预购商品事件 */
            $saveFlashSaleBtn.on('click', function (e) {
              e.preventDefault();
              const params = $form.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(
                  params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              utils.postAjax(saveUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $('#modal_flash_sale').modal('hide');
                    $datatables.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });

            /* 保存专区商品事件 */
            $saveAreaGoodsBtn.on('click', function (e) {
              e.preventDefault();
              const params = $areaFrom.serializeObject();
              if (!validator.validate(validationRules, params)) {
                return;
              }
              let logisticsFee = $('#logisticsFee').val();
              if(logisticsFee != null && logisticsFee < 0){
                utils.tools.alert("活动运费不能小于0",
                    {timer: 1200, type: 'warning'});
                return;
              }
              if (parseFloat(params.discount) <= parseFloat(
                  params.preOrderPrice)) {
                utils.tools.alert('商品购买价格不能低于预购价格',
                    {timer: 1200, type: 'warning'});
                return;
              }
              utils.postAjax(saveAreaUrl, params, function (res) {
                if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试",
                      {timer: 1200, type: 'warning'});
                }
                if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                    $('#modal_area_goods').modal('hide');
                    $datatables.search('').draw();
                  } else {
                    if (res.moreInfo) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    } else {
                      utils.tools.alert('服务器错误',
                          {timer: 1200, type: 'warning'});
                    }
                  }
                }
              });
            });

            return this;
          },
          makeDatePicker: function () {
            /* 初始话日期控件 */
            globalDatePicker = datePicker.createPicker({
              dom: '.daterange-time',
              onApply: function (start, end) {
                $validFrom.val(start);
                $validTo.val(end);
                $areaValidFrom.val(start);
                $areaValidTo.val(end);
              },
              onEmpty: function () {
                $validFrom.val('');
                $validTo.val('');
                $areaValidFrom.val('');
                $areaValidTo.val('');
              }
            });
            return this;
          },
          makeProductModal: function () {
            /**
             * 初始化商品选择表格
             */
            productModal.create({
              dom: '#productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'),
                    checkInPromotionUrl, {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#productId").val(id);
                            $("#productName").val(name);
                            $("#area_productId").val(id);
                            $("#area_productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }
            });

            productSaleZoneModal.create({
              dom: '#area_productName',
              onSelect: function (id, name) {
                utils.postAjaxWithBlock($('.modal-content'),
                    checkInPromotionUrl, {productId: id}, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if (res.data === false) {
                            $("#area_productId").val(id);
                            $("#area_productName").val(name);
                          } else {
                            utils.tools.alert('该商品已参与其他活动，无法添加',
                                {timer: 1200, type: 'warning'});
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
              }});
          }
        };

        return {
          initGlobal: function () {
            globalInstance.bindEvent()
            .makeDatePicker()
            .makeProductModal();
            return this;
          },
          initTable: function () {
            if (!$dataTable) {

              $datatables = utils.createDataTable('#xquark_flash_sale_list', {

                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: true,
                ajax: function (data, callback) {
                  $.get(listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    batchNo: $('input[name="saleZoneBatchNo"]').val(),
                    promotionType: $('select[name="type"]').val(),
                    promotionStatus: $('select[name="status"]').val(),
                    pageable: true
                  }, function (res) {
                    if (!res.data) {
                      utils.tools.alert(res.moreInfo,
                          {timer: 1200, type: 'warning'});
                    }
                    callback({
                      recordsTotal: res.data.total,
                      recordsFiltered: res.data.total,
                      data: res.data.list,
                      iTotalRecords: res.data.total,
                      iTotalDisplayRecords: res.data.total
                    });
                  })
                },
                rowId: 'id',
                columns: [
                  {
                    title: '批次号',
                    data: "promotion.batchNo",
                    width: "40px",
                    orderable: false,
                    name: "promotionBatchNo"
                  },
                  {
                    title: '活动图片',
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                      return '<img class="goods-image" src="'
                          + row.promotion.img + '" />';
                    }
                  },
                  {
                    title: '活动名称',
                    data: "promotion.title",
                    width: "80px",
                    orderable: false,
                    name: "promotionName"
                  },
                  {
                    title: '抢购商品名',
                    data: "product.name",
                    width: "80px",
                    orderable: false,
                    name: "productName"
                  },
                  {
                    title: '活动类型',
                    width: "80px",
                    orderable: false,
                    name: "promotionType",
                    render: function (data, type, row) {
                          var promotionType = '';
                          switch(row.promotion.type)
                          {
                              case 'SALEZONE':
                                  promotionType = '专区活动';
                                  break;
                              case 'FLASHSALE':
                                  promotionType = '抢购活动';
                                  break;
                              default:
                                  break;
                          }
                      return promotionType;
                    }
                  },
                  {
                    title: '状态',
                    orderable: false,
                    width: "80px",
                    render: function (data, type, row) {
                      let isClosed = row.promotion.closed;
                      let validTo = row.promotion.validTo;
                      let validFrom = row.promotion.validFrom;
                      let now = new Date().getTime();
                      if(validFrom > now){
                        return '未开始';
                      } else if(validTo < now || isClosed){
                        return '已结束';
                      }
                      return '进行中';
                    }
                  },
                  {
                    title: '开始日期',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validFrom);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validFrom"
                  },
                  {
                    title: '结束时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.validTo);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "validTo"
                  },
                  {
                    title: '发布时间',
                    orderable: false,
                    width: "100px",
                    render: function (data, type, row) {
                      var cDate = parseInt(row.createdAt);
                      var d = new Date(cDate);
                      return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                  },
                  {
                    title: '管理',
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                      var html = '';
                      html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-pencil7" ></i>编辑</a>';
                      html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" rowId="'
                          + row.id
                          + '" ><i class="icon-trash"></i>结束</a>';
                      return html;
                    }
                  }
                ],
                select: {
                  style: 'multi'
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                  $('.del').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm('确认结束该活动吗', function () {
                      utils.postAjax(closeUrl, {id: id}, function (res) {
                        if (res === -1) {
                          utils.tools.alert("网络问题，请稍后再试",
                              {timer: 1200, type: 'warning'});
                        }
                        if (typeof res === 'object') {
                          if (res.errorCode === 200) {
                            utils.tools.alert('操作成功',
                                {timer: 1200, type: 'success'});
                            $datatables.search('').draw();
                          } else {
                            if (res.moreInfo) {
                              utils.tools.alert(res.moreInfo,
                                  {timer: 1200, type: 'warning'});
                            } else {
                              utils.tools.alert('服务器错误',
                                  {timer: 1200, type: 'warning'});
                            }
                          }
                        }
                      });
                    }, function () {
                    });
                  });

                  $('.edit').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.postAjax(viewUrl + '/' + id, null, function (res) {
                      if (res === -1) {
                        utils.tools.alert("网络问题，请稍后再试",
                            {timer: 1200, type: 'warning'});
                      }
                      if (typeof res === 'object') {
                        if (res.errorCode === 200) {
                          if(res.data.promotionType == "SALEZONE"){
                            $('#area_product_image_dropzone').val('');
                            const data = res.data;
                            const validFrom = new Date(
                                parseInt(data.validFrom)).format(
                                'yyyy-MM-dd hh:mm');
                            const validTo = new Date(
                                parseInt(data.validTo)).format(
                                'yyyy-MM-dd hh:mm');
                            const img = data.product.img;
                            imgUploader2.clear()
                                .addImage(img);
                            globalDatePicker.resetStr(validFrom + ' - '
                                + validTo);

                            if (data.promotion.isPointUsed) {
                              $("input[name=isPointUsedForArea][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isPointUsedForArea][value=false]").prop("checked", true);
                            }
                            if (data.promotion.isCountEarning) {
                              $("input[name=isCalculateEarnings][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isCalculateEarnings][value=false]").prop("checked", true);
                            }
                            if (data.promotion.isShowTime) {
                              $("input[name=isShowTime][value=true]").prop("checked", true);
                            } else {
                              $("input[name=isShowTime][value=false]").prop("checked", true);
                            }
                            $.uniform.update();

                            let formData = {
                              area_id: id,
                              area_productId: data.product.id,
                              area_promotionId: data.promotion.id,
                              area_title: data.promotion.title,
                              area_productName: data.product.name,
                              area_amount: data.amount,
                              area_limitAmount: data.limitAmount,
                              area_validFrom: validFrom,
                              area_validTo: validTo,
                              area_img: data.promotion.img,
                              price: data.promotion.price,
                              promotion_price: data.promotion.conversionPrice,
                              deduction_point: data.promotion.point,
                              batchNo: data.promotion.batchNo,
                              logisticsFee: data.promotion.logisticsFee
                            };
                            if(data.promotion.logisticsFee <= 0){
                              $('#logisticsFee').val('0');
                            }
                            utils.tools.syncForm($areaFrom, formData);
                            if(data.promotion.isPointUsed){
                              $('#usedPoint').show();
                            }else{
                              $('#usedPoint').hide();
                            }
                            $modalAreaGoods.modal('show');
                          }else{
                            $('#product_image_dropzone').val('');
                            const data = res.data;
                            const validFrom = new Date(
                                parseInt(data.validFrom)).format(
                                'yyyy-MM-dd hh:mm');
                            const validTo = new Date(
                                parseInt(data.validTo)).format(
                                'yyyy-MM-dd hh:mm');
                            const img = data.product.img;
                            imgUploader.clear()
                                .addImage(img);
                            globalDatePicker.resetStr(validFrom + ' - '
                                + validTo);

                            if (data.promotion.isPointUsed) {
                              $("input[name=isPointUsed][value=true]").prop("checked",
                                  true);
                            } else {
                              $("input[name=isPointUsed][value=false]").prop("checked",
                                  true);
                            }
                            $.uniform.update();

                            var formData = {
                              id: id,
                              productId: data.product.id,
                              promotionId: data.promotion.id,
                              title: data.promotion.title,
                              productName: data.product.name,
                              discount: data.discount,
                              amount: data.amount,
                              limitAmount: data.limitAmount,
                              validFrom: validFrom,
                              validTo: validTo,
                              img: data.promotion.img
                            };
                            utils.tools.syncForm($form, formData);
                            $modalFlashSale.modal('show');
                          }
                        } else {
                          if (res.moreInfo) {
                            utils.tools.alert(res.moreInfo,
                                {timer: 1200, type: 'warning'});
                          } else {
                            utils.tools.alert('服务器错误',
                                {timer: 1200, type: 'warning'});
                          }
                        }
                      }
                    });
                  })
                }

              });
            }
            return this;
          }
        }
      })();

      $(":radio[name='isPointUsedForArea']").click(function(){
        let index = $(":radio[name='isPointUsedForArea']").index($(this));
        if(index === 1) { //选中第1个时，div显示
          document.getElementById('price').style['width'] = '50%';
          $('#usedPoint').show();
        }
        else { //当被选中的不是第2个时，div隐藏
          $('#usedPoint').hide();
          document.getElementById('price').style['width'] = '';
        }
      });

      let domName = "";
      $("#area_productName").click(function () {
        domName = "#area_productName";
      });
      $("#productName").click(function () {
        domName = "#productName";
      });

      $('.btn-saveAreaTitle').on('click', function () {
        $.ajax({
          type: "POST",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: findSaleZoneTitleUrl + "SALEZONE",
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                $('#saleZoneTitle').val(data.data.promotionTitle);
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
        $('#modal_area_title').modal('show');
      });

      $("#saveSaleZoneTitleBtn").on('click', function () {
        let saleZoneTitle = $('#saleZoneTitle').val();
        $.ajax({
          type: "POST",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: saveSaleZoneTitleUrl + saleZoneTitle,
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                $('#modal_area_title').modal('hide');
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
      });

      /**
       * 获取系统时间（年月日）
       * @returns {number}
       */
      function getNowFormatDate() {
        let date = new Date();
        let month = date.getMonth() + 1;
        let strDate = date.getDate();
        if (month >= 1 && month <= 9) {
          month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
          strDate = "0" + strDate;
        }
        return date.getFullYear() + month + strDate;
      }

      $(".btn-search").off("click").on('click', function () {
        $datatables.search('').draw();
      });

      /**
       * 光标脱离事件
       */
      $("#batchNo").blur(function(){
        let batchNo = $('#batchNo').val();
        $.ajax({
          type: "GET",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: findDateByBatchNoUrl + batchNo,
          success: function (data) {
            if (data === -1) {
              utils.tools.alert("网络问题，请稍后再试",
                  {timer: 1200, type: 'warning'});
            }
            if (typeof data === 'object') {
              if (data.errorCode === 200) {
                let from = dateFormat(data.data.promotion.validFrom);
                $('#area_valid_from').val(from);
                let to = dateFormat(data.data.promotion.validTo);
                $('#area_valid_to').val(to);
                $('#promotionDate').val(from + " - " + to);
              } else {
                if (data.moreInfo) {
                  utils.tools.alert(data.moreInfo,
                      {timer: 1200, type: 'warning'});
                } else {
                  utils.tools.alert('服务器错误',
                      {timer: 1200, type: 'warning'});
                }
              }
            }
          }
        });
      });

      /**
       * 日期转换封装
       * @param date
       * @returns {*}
       */
      function dateFormat(date){
        let cDate = parseInt(date);
        let d = new Date(cDate);
        return d.format('yyyy-MM-dd hh:mm');
      }

      /* 初始化页面 */
      manager.initGlobal()
      .initTable();

    });

require(['all']);

//module
require(['jquery', 'uniform', 'placeholder', 'flashSale/list'], function () {

});
define("flashSale", function(){});

