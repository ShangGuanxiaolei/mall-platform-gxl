/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by chh on 16/12/09.
 */
define('utils', ['jquery', 'sweetAlert', 'blockui'], function () {


    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }


    var utils = {
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(/\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            confirm: function (sMsg, fnConfirm, fnCancel) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0)
                                str_cut = str_cut.concat(more);
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            }
        }
    };
    return utils;

});
/**
 * Created by chh on 16/12/09.
 */

//公共组件定义
require(['utils']);

//登出
define('all', ['jquery', 'utils'], function (jquery, utils) {
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            utils.logout();
        });
    });
});
define('agent/member', ['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'tree', 'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform, daterangepicker, moment, tree) {

        var userId = '';
        var parentName = '';
        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: true,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            dateLimit: {days: 600},
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoApply: true,
            opens: 'left',
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        $('.daterange-basic').daterangepicker(options);
        $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调**/
        $('.daterange-basic').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;
            $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
            $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
        });

        // 时间区间默认为空
        options.startDate = '';
        options.endDate = '';
        $("input[name='startDate']").val('');
        $("input[name='endDate']").val('');
        $('.daterange-basic').val('');

        $(".exportBtn").on('click', function () {
            $('#exportForm').submit();
        });

        buttonRoleCheck('.hideClass');

        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无待审核下级"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        // function ()
        var twitterTreeManager = {
            /**jstree*/
            $container: $('#twitterTreeManagerContainer'),
            // currentNode: null,
            root: {},
            eachPageSize: 15,
            createIcon: function (child) {
                var role = child['role'].toLowerCase();
                return '<div class="custom">' +
                    '<img src="' + child['avatar'] + '" alt="" class="user-icon">' +
                    '<em class="' + role + '">' + child['name'] + '</em>' +
                    '&nbsp;&nbsp;<em style="color: red">' + child['count'] + '</em>' +
                    '</div>'
            },
            config: {
                "core": {
                    "animation": 200,
                    "check_callback": true,
                    "themes": {"stripes": false, "dots": false, "responsive": true, "icons": false},
                    'strings': {
                        'Loading ...': '正在加载...'
                    },
                    'data': {
                        'url': function (node) {
                            return window.host + '/userAgent/getDirectChildren'
                        },
                        'type': 'GET',
                        'data': function (node) {
                            //TODO 设置Root节点
                            console.log(node)
                            console.log(node.id)
                            console.log($.jstree.root)
                            var id = node.id === $.jstree.root ? twitterTreeManager.root.id : node.id;
                            node.page = node.page ? node.page : 0;
                            return {
                                userId: id,
                                page: node.page,
                                size: twitterTreeManager.eachPageSize
                            }
                        },
                        'dataFilter': function (data) {
                            var dataInfo = JSON.parse(data).data;
                            console.log(JSON.parse(data))
                            if (JSON.parse(data).data) {
                                var total = JSON.parse(data).data.total,
                                    parent = JSON.parse(data).data.parent,
                                    list = JSON.parse(data).data.list;
                            } else alert('数据格式有误')
                            console.log(total);
                            console.log(parent);
                            console.log(list);
                            function renderChildData() {
                                var tmp = [];
                                list.forEach(function (value) {
                                    tmp.push({
                                        'id': value.id,
                                        'text': twitterTreeManager.createIcon(value), // 用户姓名
                                        'state': {'opened': false, 'selected': false},
                                        'children': !value.isLeaf
                                    })
                                });
                                if (total > 15) {
                                    var node = twitterTreeManager.tree.get_node(parent.id);
                                    var maxPage = parseInt(total / 15);
                                    node.maxPage = maxPage;
                                    if (node && node.page) {
                                        var page = node.page + 1;
                                        node.maxPage = maxPage;
                                    } else {
                                        page = 1;
                                        twitterTreeManager.root.maxPage = maxPage;
                                    }
                                    console.log(twitterTreeManager.tree.get_node(parent.id))
                                    tmp.push({
                                        'id': parent.id + "pagingBtn",
                                        'text': '<div class="flip-page">' +
                                        '<i class="previousPage">上页</i>' +
                                        '<em class="nextPage">下页</em>' +
                                        '<span>第' + (node.page + 1) +
                                        '页</span>' +
                                        '</div>',
                                        'state': {'opened': false, 'selected': false},
                                        'children': false
                                    })
                                }
                                return tmp
                            }

                            if (parent && parent.id) {
                                if (parent.id === twitterTreeManager.root.id && !twitterTreeManager.tree.get_node(parent.id)) {
                                    //根节点
                                    console.log('根节点')
                                    return JSON.stringify([{
                                        'id': parent.id,
                                        'text': twitterTreeManager.createIcon(parent), // 用户姓名
                                        'state': {'opened': false, 'selected': false},
                                        'children': !!(list.length)
                                    }])
                                } else {
                                    if (list.length != 0) {
                                        console.log('子节点')
                                        return JSON.stringify(renderChildData())
                                    }
                                }
                            }
                        }
                    }
                },
                "types": {
                    "#": {
                        "max_children": 1,
                        "max_depth": 6,
                    }
                },
                "plugins": [
                    "contextmenu", "search",
                    "state", "types", "sort", "Wholerow", "json_data"
                ]
            },

            init: function (userId) {
                this.root.id = userId;
                this.root.page = 0;
                this.$container
                //初始化时绑定需要处理的事件
                    .on('click.jstree', function (e) {
                        var className = e.target.getAttribute('class');
                        var tree = twitterTreeManager.tree;
                        if (className && className.match('previousPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if (parentNode.page > 0) {
                                parentNode.page = parentNode.page - 1;
                                tree.load_node(parentNode)
                            }
                        } else if (className && className.match('nextPage')) {
                            var parentNode = tree.get_node(tree.get_parent(e.target));
                            parentNode.page = typeof parentNode.page === 'number' ? parentNode.page : 0;
                            if (parentNode.page < parentNode.maxPage) {
                                parentNode.page = parentNode.page + 1;
                                tree.load_node(parentNode);
                                console.log(parentNode);
                            }
                        }

                        // 点击左侧缩放小箭头时不需要查看详情
                        var eventNodeName = e.target.nodeName;
                        if (eventNodeName == 'IMG' || eventNodeName == 'EM') {
                            var id = $(e.target).parents('li').attr('id');
                            if (id.indexOf('pagingBtn') == -1) {
                                showDetail(id);
                            }
                        }
                    })
                    //执行初始化
                    .jstree(this.config);
                this.tree = this.$container.jstree(true);
            },

            clear: function () {
                this.$container.jstree('destroy');
                delete twitterTreeManager.tree;
                // delete twitterTreeManager
            }
        };

        function showDetail(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/userId/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            $("#name").html(res.data.name);
                            $("#weixin").html(res.data.weixin);
                            $("#idcard").html(res.data.idcard == '' ? '无' : res.data.idcard);
                            $("#certNum").html(res.data.certNumView);
                            $("#startDate").html(res.data.startDate);
                            $("#endDate").html(res.data.endDate);
                            $("#agentTypeName").html(res.data.agentTypeName);
                            $("#kfname").html(res.data.name);
                            $("#kfphone").html('19911111111');
                            $("#modal_cert_manager").modal('show');
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("查看详情失败", {timer: 1200});
                }
            });
        }

        function showApplyingChildren(name, callBack) {
            $.get(window.host + "/userAgent/apply/list", {
                status: 'APPLYING',
                parentName: name
            }, function (res) {
                var list = res.data.list;
                callBack(list);
            }).fail(function () {
                utils.tools.alert("查找未审核下级失败", {timer: 1200});
            });
        }

        /* 生成未申请用户的节点 */
        function generateApplyingDOM(listResult) {
            var listGroup = $('#listGroup');
            // 清除用户列表
            listGroup.empty();
            if (!listResult || listResult.length === 0) {
                return;
            }
            var out = listResult.map(function (result) {
                return '<li class="media-header"></li><li class="media">' + result.name + '<a href="javascript:void(0);" aid="' + result.id + '" class="deleteBtn pull-right deleteApplying" fid="delete_applying">删除申请</a>' + '</li>'
            }).join('');
            listGroup.append('<div class="panel-heading"><h6 class="panel-title">待审核下级代理</h6></div>');
            listGroup.append('<div style="margin: 35px 35px"><ul class="media-list media-list-bordered">' + out + '</ul></div>');

            $('.deleteApplying').on('click', function () {
                var id = $(this).attr('aid');
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                // 删除后重新请求数据并生成
                                showApplyingChildren(parentName, generateApplyingDOM);
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            })
        }

        // 当前登陆用户的角色，如果为客户管理员角色，则需要隐藏某些功能
        var roles = $("#roles").val();

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering: false,
            ajax: function (data, callback, settings) {
                $.get(window.host + "/userAgent/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#status").val(),
                    type: $("#type").val(),
                    startDate: options.startDate != '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: options.endDate != '' ? options.endDate.format('YYYY-MM-DD') : '',
                    phone: $("#phone").val(),
                    name: $("#agentName").val(),
                    parentName: $("#parentName").val(),
                    pageable: true,
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            columns: [
                {
                    width: '75px',
                    title: '代理姓名',
                    data: 'name',
                    name: 'name',
                    sortable: false
                }, {
                    width: 200,
                    sClass: 'styled text-center sorting_disabled',
                    title: "代理信息",
                    sortable: false,
                    render: function (data, type, row) {
                        if ('CUSTOMER' == roles) {
                            return row.phone;
                        } else {
                            return '<a href="#" user-id="' + row.id + '" class="twitterChildrenBtn" data-toggle="modal" >' + row.phone + ' <i class="icon-search4"></i></a>'
                                + '&nbsp;<a href="#" user-name="' + row.name + '" user-id="' + row.userId + '" class="twitterChildrenTreeManager" data-toggle="modal" ><i class="icon-tree6"></i></a>';
                        }
                    }
                }, {
                    width: 75,
                    data: 'type',
                    name: 'type',
                    title: '代理级别',
                    sortable: false,
                    render: function (data, type, row) {
                        var value = row.type;
                        if (value == 'FOUNDER') {
                            return "联合创始人";
                        }else if (value == 'DIRECTOR') {
                            return "董事";
                        } else if (value == 'GENERAL') {
                            return "总顾问";
                        } else if (value == 'FIRST') {
                            return "一星顾问";
                        } else if (value == 'SECOND') {
                            return "二星顾问";
                        } else if (value == 'SPECIAL') {
                            return "特约";
                        }
                    }
                }, {
                    width: '100px',
                    title: '身份证号',
                    data: 'idcard',
                    name: 'idcard',
                    sortable: false
                }, {
                    width: '75px',
                    title: '上级名称',
                    data: 'parentVo.name',
                    name: 'parentVo.name',
                    sortable: false
                },
                {
                    width: 90,
                    data: 'unWithdrawFee',
                    name: 'unWithdrawFee',
                    sortable: false,
                    title: '未结算佣金'
                }, {
                    width: 90,
                    data: 'withdrawFee',
                    name: 'withdrawFee',
                    sortable: false,
                    title: '已结算佣金'
                },
                {
                    width: 75,
                    title: '加入时间',
                    sortable: false,
                    render: function (data, type, row) {
                        if (row.createdAt == null) {
                            return '';
                        }
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    }
                }, {
                    width: 75,
                    data: 'status',
                    name: 'status',
                    sortable: false,
                    title: '状态',
                    render: function (data, type, row) {
                        var value = row.status;
                        if (value == 'ACTIVE') {
                            return "已审核";
                        } else if (value == 'FROZEN') {
                            return "已冻结";
                        }
                    }
                }, {
                    width: 150,
                    sClass: 'styled text-center sorting_disabled',
                    align: 'right',
                    sortable: false,
                    title: '操作',
                    render: function (data, type, row) {
                        return '<a href="javascript:void(0);" class="disableBtn" data-toggle="disablepopover" rowId="' + row.id + '" fid="freeze_agent">冻结</a>  ' +
                            '<a href="javascript:void(0);" class="enableBtn" data-toggle="enablepopover" rowId="' + row.id + '" fid="enable_agent">启用</a>  ' +
                            '<a href="javascript:void(0);"    class="changeTreeBtn" userId="' + row.userId + '" rowId="' + row.id + '" fid="edit_agent_superior">改变上级</a>  ' +
                            '<a href="javascript:void(0);"    class="upgradeBtn"  rowId="' + row.id + '" name="' + row.name + '" phone="' + row.phone + '" type="' + row.type + '" fid="rise_agent"><i class="icon-users4"></i>升级</a>' +
                            '<a href="javascript:void(0);" class="deleteBtn" data-toggle="deletepopover" style="margin-left: 3px" rowId="' + row.id + '" fid="delete_agent">删除</a>';
                    }
                }],

            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        // 选择角色表单
        var $applyingTables = $('#xquark_applying_tables').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback) {
                $.get(window.host + "/userAgent/apply/list", {
                    parentName: parentName,
                    status: 'APPLYING',
                    size: data.length,
                    page: data.start / data.length,
                    pageable: true
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else {
                        // if (res.data.shopId) {
                        //     $shopId = res.data.shopId;
                        // }
                    }
                    callback({
                        recordsTotal: res.data.applyTotal,
                        recordsFiltered: res.data.applyTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.applytotal,
                        iTotalDisplayRecords: res.data.applyTotal
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name: "name"
                }, {
                    data: "phone",
                    width: "120px",
                    orderable: false,
                    name: "phone"
                }
                , {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" rowId="'
                            + row.id
                            + '" fid="deleteApplying" class="deleteApplying" aid="' + row.id + '" data-toggle="popover"><i class="icon-trash" ></i>删除</a>';
                        return html;
                    }
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                $('.deleteApplying').on('click', function () {
                    var id = $(this).attr('aid');
                    utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                        if (typeof(res) === 'object') {
                            switch (res.errorCode) {
                                case 200: {
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    // 删除后重新请求数据并生成
                                    // showApplyingChildren(parentName, generateApplyingDOM);
                                    $applyingTables.search('').draw();
                                    break;
                                }
                                default: {
                                    utils.tools.alert(res.moreInfo, {timer: 1200});
                                    break;
                                }
                            }
                        } else if (res == 0) {
                        } else if (res == -1) {
                            utils.tools.alert("删除失败", {timer: 1200});
                        }
                    });
                })
            }
        });

        // 如果是客户管理员角色，隐藏操作列
        if ('CUSTOMER' == roles) {
            var oTable = $('#guiderUserTable').dataTable();
            oTable.fnSetColumnVis(9, false);
        }

        /**var userId =  null;
         //查看推客下级
         var $twitterChildrenTables = $('#twitterChildrenInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/twitterMember/children", {
                    size: data.length,
                    page: (data.start / data.length),
                    userId : userId,
                    pageable: true,
                }, function(res) {

                    if (!res.data.list) {
                        res.data.list = [];
                    }

                    parentShopName = res.data.parentShopName;
                    //document.getElementById("parentShopName").innerText = parentShopName ;
                    $("#parentShopName").html(parentShopName );
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            columns: [{
                data: 'name',
                name: 'name',
                width: 310,
                title: "推客店铺"

            },{
                data: 'archive',
                name: 'archive',
                width: 150,
                title: "店铺状态",
                render: function (data, type, row) {
                    var value = row.archive;
                    if(value =='0'){
                        return "正常";
                    }else{
                        return "禁用";
                    }
                }
            }]
        });**/

        // 查看详情
        $(document).on('click', '.twitterChildrenBtn', function () {

            var id = $(this).attr("user-id");
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            $("#view_id").val(res.data.id);
                            $("#view_name").val(res.data.name);
                            $("#view_weixin").val(res.data.weixin);
                            $("#view_phone").val(res.data.phone);
                            //$("#view_email").val(res.data.email);
                            $("#view_idcard").val(res.data.idcard);
                            $("#view_address").val(res.data.address);
                            $("#view_type").val(res.data.type);

                            // $("#life_img").attr('src', res.data.lifeImg);
                            // $("#idcard_img").attr('src', res.data.idcardImg);
                            initFileInput('lifeCard', res.data.id, res.data.lifeImg);
                            initFileInput('idCard', res.data.id, res.data.idcardImg);

                            var addressId = res.data.address;
                            $.ajax({
                                url: 'http://' + window.location.host + '/v2/address/api/' + addressId,
                                method: 'POST',
                                success: function (data) {
                                    console.log(data);
                                    renderingData(data);
                                },
                                error: function (err) {
                                    console.log('error ')
                                }
                            });

                            //$("#view_idcard_img").attr("src",res.data.idcardImgUrl);
                            //$("#view_life_img").attr("src",res.data.lifeImgUrl);
                            $("#modal_viewDetail").modal("show");
                            /** 初始化选择框控件 **/
                            $('.select').select2({
                                minimumResultsForSearch: Infinity,
                            });
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("查看详情失败", {timer: 1200});
                }
            });

            //$('#modal_twitterChildrenInfo').modal('show');
            //$twitterChildrenTables.search(userId).draw();
        });

        // 修改保存用户代理信息
        $(document).on('click', '.saveAgentBtn', function () {

            var id = $("#view_id").val();
            var name = $("#view_name").val();
            var weixin = $("#view_weixin").val();
            var phone = $("#view_phone").val();
            var idcard = $("#view_idcard").val();
            var address = $("#view_address").val();
            var type = $("#view_type").val();
            var district = $("#district").val();
            var addressDetail = $("#view_detail").val();

            if (district == 0) {
                utils.tools.alert("请选择地区!", {timer: 2000, type: 'warning'});
                return;
            } else if (addressDetail == '') {
                utils.tools.alert("请输入街道门牌信息!", {timer: 2000, type: 'warning'});
                return;
            }

            var param = {
                id: id,
                name: name,
                weixin: weixin,
                phone: phone,
                idcard: idcard,
                address: address,
                type: type,
                district: district,
                addressDetail: addressDetail
            };

            utils.postAjax(window.host + '/userAgent/checkPhone', param, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.confirm('此代理的手机号已经存在于其他用户，修改后手机号对应的之前用户的相关信息将被删除,是否确认修改？', function () {
                            utils.postAjax(window.host + '/userAgent/save?updatePhone=1', param, function (res) {
                                if (typeof(res) === 'object') {
                                    if (res.data) {
                                        utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                        $datatables.search('').draw();
                                    } else {
                                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                    }
                                }
                                $("#modal_viewDetail").modal("hide");
                            });
                        }, function () {

                        });
                    } else {
                        utils.postAjax(window.host + '/userAgent/save', param, function (res) {
                            if (typeof(res) === 'object') {
                                if (res.data) {
                                    utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                    $datatables.search('').draw();
                                } else {
                                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                }
                            }
                            $("#modal_viewDetail").modal("hide");
                        });
                    }
                }
            });

        });

        // 代理升级保存
        $(document).on('click', '.upgradeAgentBtn', function () {
            var id = $("#upgrade_id").val();
            var type = $("#upgrade_type").val();
            var param = {
                id: id,
                type: type
            };
            utils.postAjax(window.host + '/userAgent/changeType', param, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.alert("升级成功!", {timer: 1200, type: 'warning'});
                        $datatables.search('').draw();
                    } else {
                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                    }
                }
                $("#modal_upgrade").modal("hide");
            });
        });

        $(document).on('click', '.twitterChildrenTreeManager', function () {
            userId = $(this).attr("user-id");
            parentName = $(this).attr('user-name');
            twitterTreeManager.init(userId);//初始化树
            // 刷新未审核代理表格
            $applyingTables.search('').draw();
            // 显示审核未通过下级代理
            // showApplyingChildren(parentName, generateApplyingDOM);
            $('#modal_tree_manager').modal('show');
        });

        $("#modal_tree_manager").on('hidden.bs.modal', function () {
            twitterTreeManager.clear();
        });

        $('.exportTwitterChildrenBtn').on('click', function () {
            if (userId != null) {
                $('#exportTwitterChildrenForm input[name="userId"]').val(userId);
                $('#exportTwitterChildrenForm').submit();
            }
        });

        function initEvent() {
            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            // 启用
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认启用吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "enablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="enablepopover"]').popover('hide');
                } else if (target.data("toggle") == "enablepopover") {
                    target.popover("toggle");
                }
            });

            //删除
            $("[data-toggle='deletepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="deletepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="deletepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    deleteUserAgent(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "deletepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="deletepopover"]').popover('hide');
                } else if (target.data("toggle") == "deletepopover") {
                    target.popover("toggle");
                }
            });


            // 禁用
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认冻结吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    disableShop(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "disablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="disablepopover"]').popover('hide');
                } else if (target.data("toggle") == "disablepopover") {
                    target.popover("toggle");
                }
            });

            // 提升为合伙人
            $("[data-toggle='setPartnerpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认提升为大区吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="setPartnerpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="setPartnerpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    setPartner(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "setPartnerpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="setPartnerpopover"]').popover('hide');
                } else if (target.data("toggle") == "setPartnerpopover") {
                    target.popover("toggle");
                }
            });


            $(".changeTreeBtn").on('click', function () {
                $("#modal_update_pred").modal('show');
                changeAgentId = $(this).attr('rowId');
                changeAgentUserId = $(this).attr('userId');

                //var testData = [{"id":"71l0vl7g","createdAt":1469598337000,"updatedAt":null,"phone":"13774307010","cmFee":0.0000,"unCmFee":0.0000,"name":"董董的科学发展观的小店","status":"ACTIVE"}];
                if ($guiderUserTable) {
                    $guiderUserTable.search('').draw();
                } else {
                    $guiderUserTable = $('#guiderLeaderTable').DataTable({
                        paging: true, //是否分页
                        filter: false, //是否显示过滤
                        lengthChange: false,
                        processing: true,
                        serverSide: true,
                        deferRender: true,
                        searching: false,
                        ajax: function (data, callback, settings) {
                            $.get(window.host + "/userAgent/list", {
                                size: data.length,
                                page: (data.start / data.length),
                                name: twitterName4ChangeTree,
                                phone: twitterPhone4ChangeTree,
                                notUserId: changeAgentUserId,
                                pageable: true
                            }, function (res) {
                                if (!res.data.list) {
                                    res.data.list = [];
                                }
                                callback({
                                    recordsTotal: res.data.total,
                                    recordsFiltered: res.data.total,
                                    data: res.data.list,
                                    iTotalRecords: res.data.total,
                                    iTotalDisplayRecords: res.data.total
                                });
                            });
                        },
                        columns: [
                            {
                                width: '75px',
                                title: '代理姓名',
                                data: 'name',
                                name: 'name',
                                sortable: false
                            }, {
                                width: '75px',
                                sClass: 'styled text-center sorting_disabled',
                                title: "手机",
                                render: function (data, type, row) {
                                    return '<a href="#" user-id="' + row.id + '" >' + row.phone + '</a>';
                                }
                            }, {
                                width: '75px',
                                data: 'type',
                                name: 'type',
                                title: '代理级别',
                                sortable: false,
                                render: function (data, type, row) {
                                    var value = row.type;
                                    if (value == 'FOUNDER') {
                                        return "联合创始人";
                                    }else if (value == 'DIRECTOR') {
                                        return "董事";
                                    } else if (value == 'GENERAL') {
                                        return "总顾问";
                                    } else if (value == 'FIRST') {
                                        return "一星顾问";
                                    } else if (value == 'SECOND') {
                                        return "二星顾问";
                                    } else if (value == 'SPECIAL') {
                                        return "特约";
                                    }
                                }
                            }, {
                                width: '75px',
                                title: '身份证号',
                                data: 'idcard',
                                name: 'idcard',
                                sortable: false
                            }, {
                                width: '75px',
                                title: '上级名称',
                                data: 'parentVo.name',
                                name: 'parentVo.name',
                                sortable: false
                            }, {
                                width: '75px',
                                sClass: 'sorting',
                                title: '加入时间',
                                sortable: false,
                                render: function (data, type, row) {
                                    if (row.createdAt == null) {
                                        return '';
                                    }
                                    var cDate = parseInt(row.createdAt);
                                    var d = new Date(cDate);
                                    return d.format('yyyy-MM-dd hh:mm:ss');
                                }
                            }, {
                                width: '75px',
                                data: 'status',
                                name: 'status',
                                title: '状态',
                                sortable: false,
                                render: function (data, type, row) {
                                    var value = row.status;
                                    if (value == 'ACTIVE') {
                                        return "已审核";
                                    } else if (value == 'FROZEN') {
                                        return "已冻结";
                                    }
                                }
                            }, {
                                width: '75px',
                                sClass: 'styled text-center sorting_disabled',
                                align: 'right',
                                title: '操作',
                                sortable: false,
                                render: function (data, type, row) {
                                    return '<a href="javascript:void(0);"    class="changeTreeConfirmBtn" data-toggle="popover" userId="' + row.userId + '" rowId="' + row.id + '">变更</a>';

                                }
                            }],
                        drawCallback: function () {  //数据加载完成
                            initEventForChangeTreeModal();
                        }
                    });
                }

            });

            // 代理升级
            $('.upgradeBtn').on('click', function () {
                var id = $(this).attr("rowId");
                var name = $(this).attr("name");
                var phone = $(this).attr("phone");
                var type = $(this).attr("type");
                $("#upgrade_id").val(id);
                $("#upgrade_name").val(name);
                $("#upgrade_phone").val(phone);

                // html select的option不支持hide，所以进行了如下处理
                $("#upgrade_type").html('');
                if (type == 'SPECIAL') {
                    $("#upgrade_type").html(' <option id="DIRECTOR" value="FOUNDER">联合创始人</option> ' +' <option id="DIRECTOR" value="DIRECTOR">董事</option> ' +
                        '<option id="GENERAL" value="GENERAL">总顾问</option>' +
                        '<option id="FIRST" value="FIRST">一星顾问</option>' + '<option id="SECOND" value="SECOND">二星顾问</option>');
                }else if (type == 'SECOND') {
                    $("#upgrade_type").html(' <option id="DIRECTOR" value="FOUNDER">联合创始人</option> ' +' <option id="DIRECTOR" value="DIRECTOR">董事</option> ' +
                        '<option id="GENERAL" value="GENERAL">总顾问</option>' +
                        '<option id="FIRST" value="FIRST">一星顾问</option>');
                } else if (type == 'FIRST') {
                    $("#upgrade_type").html(' <option id="DIRECTOR" value="FOUNDER">联合创始人</option> ' +' <option id="DIRECTOR" value="DIRECTOR">董事</option> ' +
                        '<option id="GENERAL" value="GENERAL">总顾问</option>');
                } else if (type == 'GENERAL') {
                    $("#upgrade_type").html(' <option id="DIRECTOR" value="FOUNDER">联合创始人</option> ' +' <option id="DIRECTOR" value="DIRECTOR">董事</option> ');
                } else if (type == 'DIRECTOR') {
                    $("#upgrade_type").html(' <option id="FOUNDER" value="FOUNDER">联合创始人</option> ');
                }

                $("#modal_upgrade").modal("show");
                /** 初始化选择框控件 **/
                $('.select').select2({
                    minimumResultsForSearch: Infinity,
                });
            });

            tableRoleCheck('#guiderUserTable');

            /*商品删除会判断 userId 否则删除失败*/
            function setPartner(id) {
                // 判断该推客是否已经是合伙人
                var url = window.host + "/userAgent/changeType?id=" + id + "&type=DIRECTOR";
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function disableShop(id) {
                var url = window.host + '/userAgent/frozen?id=' + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function enableShop(id) {
                var url = window.host + '/userAgent/unFrozen?id=' + id;
                utils.postAjax(url, {}, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.data) {
                            $datatables.search('').draw();
                        } else {
                            utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }

            function deleteUserAgent(id) {
                var url = window.host + '/userAgent/deleteLogically/' + id;
                utils.postAjax(url, {}, function (res) {
                    if (res === -1) {
                        utils.tools.alert('网络错误， 请稍后再试', {timer: 1200, type: 'warning'});
                        return;
                    }
                    if (typeof(res) === 'object') {
                        if (res.data && res.data === true) {
                            $datatables.search('').draw();
                            utils.tools.alert("操作成功!", {timer: 1200, type: 'warning'});
                        } else {
                            var info = res.moreInfo ? res.moreInfo : '操作失败';
                            utils.tools.alert(info, {timer: 1200, type: 'warning'});
                        }
                    }
                });
            }
        }

        $(".btn-search").on('click', function () {
            $datatables.search(status).draw();
        });

        $('#searchTwitter4ChangeTree').on('click', function () {
            twitterName4ChangeTree = $('#twitterName4ChangeTree').val();
            twitterPhone4ChangeTree = $('#twitterPhone4ChangeTree').val();
            //alert('把查询的必要条件去掉,否则查不到数据' + '条件:' + twitterName4ChangeTree + ',' + twitterPhone4ChangeTree);
            $guiderUserTable.search('').draw();
        });

        $('#quickChooseRootShop').on('click', function () {
            alert('写提交的代码');
        });

        // 初始化上传图片控件
        function initFileInput(name, id, img) {
            var $fileInput = $('#' + name + '-input');
            $fileInput.fileinput('destroy');
            var op = {
                language: 'zh',
                uploadUrl: window.host + '/userAgent/updateMultiPartImg',
                uploadExtraData: {"id": id, "type": name},
                showUpload: false,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                browseIcon: '<i class="icon-file-plus"></i>',
                uploadIcon: '<i class="icon-file-upload2"></i>',
                removeIcon: '<i class="icon-cross3"></i>',
                browseClass: 'btn btn-primary',
                uploadClass: 'btn btn-default',
                removeClass: 'btn btn-danger',
                initialCaption: '',
                maxFilesNum: 1,
                indicatorSuccessTitle: '上传成功',
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"],
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    footer: ''
                },

            };
            //如果img有值，则显示之前上传的图片
            if (img && img.toString().indexOf('http') != -1) {
                op = $.extend({
                    showPreview: true,
                    initialPreview: [ // 预览图片的设置
                        "<img src= '" + img + "' class='file-preview-image'>"]
                }, op);
            }

            $fileInput.fileinput(op);
            $fileInput.on('fileselect', function (event, numFiles, label) {
                $fileInput.fileinput('upload');
            });
            // $fileInput.fileinput('enable');
        }

        function initEventForChangeTreeModal() {
            $('.changeTreeConfirmBtn').on('click', function () {
                var rowId = $(this).attr('rowId');
                var parentUserId = $(this).attr('userId');
                var data = {
                    parentUserId: parentUserId,
                    id: changeAgentId,
                    userId: changeAgentUserId
                };
                var url = window.host + '/userAgent/changeParent';

                utils.postAjaxWithBlock($(document), url, data, function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                alert("上级变更成功");
                                location.href = '/sellerpc/agent/member';
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });
            });
        }

        function renderingData(data) {
            if (data.errorCode === 200 && data.data) {
                var zones = data.data.zones;

                $('#view_detail').val(data.data.street);

                $('#province').html('<option value="' + zones[1].id + '">' + zones[1].name + '</option>')
                $('#city').html('<option value="' + zones[2].id + '">' + zones[2].name + '</option>')
                $('#district').html('<option value="' + zones[3].id + '">' + zones[3].name + '</option>')
                initEditZones(zones[1].id, zones[2].id, zones[3].id);

            } else console.log('数据格式错误');
        }

        function initZones() {
            buildChildrenZone(1, document.getElementById('province'), '选择省份');
            $('#province').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('city'), '选择市');
            });
            $('#city').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
            });
        }

        function initEditZones(provinceId, cityId, districtId) {
            buildChildrenZone(1, document.getElementById('province'), '选择省份', function () {
                document.getElementById('province').value = provinceId;
                $('#province').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('city'), '选择市');
                });
            });
            buildChildrenZone(provinceId, document.getElementById('city'), '选择市', function () {
                document.getElementById('city').value = cityId;
                $('#city').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
                });
            });
            buildChildrenZone(cityId, document.getElementById('district'), '选择县区', function () {
                document.getElementById('district').value = districtId;
            });
        }

        function buildChildrenZone(zoneId, insertDom, firstOption, callback) {
            if (zoneId == '0') {
                var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                insertDom.innerHTML = frag;
                if (callback) {
                    callback();
                }
            } else {
                $.ajax({
                    url: 'http://' + window.location.host + '/v2/zone/' + zoneId + '/children',
                    method: 'POST',
                    success: function (data) {
                        if (data.data && data.errorCode) {
                            var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                            data.data.forEach(function (value) {
                                frag = frag + '<option value="' + value.id + '">' + value.name + '</option>';
                            });
                            insertDom.innerHTML = frag;
                            if (callback) {
                                callback();
                            }
                        } else console.log(data.error);
                    },
                    error: function (err) {
                        console.log('error ')
                    }
                })
            }
            if (firstOption == '选择市') {
                var frag = '<option value="' + 0 + '">选择县区</option>';
                document.getElementById('district').innerHTML = frag;
            }

        }

        initZones();

        var twitterName4ChangeTree;
        var twitterPhone4ChangeTree;
        var changeAgentId;
        var changeAgentUserId;
        var $guiderUserTable;
        var parentShopName;
    });
//微信账号绑定
require(['all']);

require(['agent/member']);

define("agentMember", [],function () {
});


//微信账号绑定
require(['all']);

require(['agent/member']);

define("agentMember", function(){});

