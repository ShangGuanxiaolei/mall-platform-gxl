/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('agent/apply',['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2', 'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform, daterangepicker, moment) {

        /** 初始化日期控件 **/
        var options = {
            autoUpdateInput: true,
            startDate: moment().subtract(0, 'month').startOf('month'),
            endDate: moment().subtract(0, 'month').endOf('month'),
            dateLimit: {days: 600},
            locale: {
                format: 'YYYY/MM/DD',
                separator: ' - ',
                applyLabel: '确定',
                startLabel: '开始日期:',
                endLabel: '结束日期:',
                cancelLabel: '取消',
                weekLabel: 'W',
                customRangeLabel: '日期范围',
                daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                firstDay: 6
            },
            ranges: {
                '今天': [moment(), moment()],
                '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '最近7天': [moment().subtract(6, 'days'), moment()],
                '最近15天': [moment().subtract(15, 'days'), moment()],
                '本月': [moment().startOf('month'), moment().endOf('month')],
                '上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            autoApply: true,
            opens: 'left',
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small btn-default'
        };

        $('.daterange-basic').daterangepicker(options);

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        /** 回调 **/
        $('.daterange-basic').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            options.startDate = picker.startDate;
            options.endDate = picker.endDate;
        });

        // 时间区间默认为空
        options.startDate = '';
        options.endDate = '';
        $('.daterange-basic').val('');

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无相关数据"
            }
        });

        $.fn.dataTable.ext.errMode = 'none';

        var $datatables = $('#guiderUserTable').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ordering: false,
            sortable: false,
            ajax: function (data, callback, settings) {
                $.get(window.host + "/userAgent/apply/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    status: $("#status").val(),
                    startDate: options.startDate != '' ? options.startDate.format('YYYY-MM-DD') : '',
                    endDate: options.endDate != '' ? options.endDate.format('YYYY-MM-DD') : '',
                    parentName: $("#parentName").val(),
                    name: $("#name").val(),
                    type: $("#type").val(),
                    phone: $("#phone").val(),
                    pageable: true,
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.applyTotal,
                        recordsFiltered: res.data.applyTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.applyTotal,
                        iTotalDisplayRecords: res.data.applyTotal
                    });
                });
            },
            columns: [{
                width: "120px",
                title: '姓名',
                data: 'name',
                name: 'name'
            }, {
                width: "120px",
                title: '微信号',
                data: 'weixin',
                name: 'weixin'
            }, {
                width: "120px",
                title: '电话',
                data: 'phone',
                name: 'phone'

            }, {
                width: 75,
                data: 'type',
                name: 'type',
                title: '申请级别',
                sortable: false,
                render: function (data, type, row) {
                    var value = row.type;
                    if (value == 'FOUNDER') {
                        return "联合创始人";
                    }else if (value == 'DIRECTOR') {
                        return "董事";
                    } else if (value == 'GENERAL') {
                        return "总顾问";
                    } else if (value == 'FIRST') {
                        return "一星顾问";
                    } else if (value == 'SECOND') {
                        return "二星顾问";
                    } else if (value == 'SPECIAL') {
                        return "特约";
                    }
                }
            }, {
                width: '75px',
                title: '上级名称',
                data: 'parentVo.name',
                name: 'parentVo.name',
                sortable: false
            }, {
                width: "120px",
                title: '身份证号',
                data: 'idcard',
                name: 'idcard'

            }, {
                width: "180px",
                title: '详细地址',
                data: 'addressStreet',
                name: 'addressStreet'

            }, {
                width: "160px",
                title: '申请时间',
                render: function (data, type, row) {
                    if (row.createdAt == null) {
                        return '';
                    }
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                }
            }, {
                width: "120px",
                title: '状态',
                render: function (data, type, row) {
                    var value = row.status;
                    if (value == 'ACTIVE') {
                        return "已审核通过";
                    } else if (value == 'FROZEN') {
                        return "已冻结";
                    }
                    {
                        return "待审核";
                    }
                }
            }, {
                width: "180px",
                sClass: 'styled text-center',
                align: 'right',
                title: '操作',
                render: function (data, type, row) {
                    var value = row.status;
                    var result = '<a href="javascript:void(0);" class="btn-sm btn btn-primary viewDetail role_check_table" rowId="' + row.id + '" fid="account_detail">查看详情</a> ';
                    if (value == 'APPLYING') {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table" data-toggle="enablepopover" rowId="' + row.id + '" fid="pass_agent">审核通过</a> '
                            + '<a href="javascript:void(0);" class="btn-sm btn btn-primary" data-toggle="disablepopover" rowId="' + row.id + '" fid="delete_agent_apply">删除申请</a> ';
                    } else if (value == 'ACTIVE') {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table"  data-toggle="frozenpopover" rowId="' + row.id + '" fid="account_freeze">冻结账户</a> ';
                    } else {
                        result = result + '<a href="javascript:void(0);" class="btn-sm btn btn-primary role_check_table" data-toggle="unfrozenpopover" rowId="' + row.id + '" fid="account_unfreeze">解冻账户</a> ';
                    }
                    return result;
                }
            }],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });

        function initEvent() {

            // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
            $('body').unbind('click');

            // 查看详情
            $(".viewDetail").on('click', function () {
                var id = $(this).attr("rowId");
                utils.postAjaxWithBlock($(document), window.host + '/userAgent/' + id, [], function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#view_name").val(res.data.name);
                                $("#view_weixin").val(res.data.weixin);
                                $("#view_phone").val(res.data.phone);
                                //$("#view_email").val(res.data.email);
                                $("#view_idcard").val(res.data.idcard);
                                $("#view_address").val(res.data.address);
                                $("#view_type").val(res.data.type);
                                $("#view_id").val(res.data.id);

                                // $("#life_img").attr('src', res.data.lifeImg);
                                // $("#idcard_img").attr('src', res.data.idcardImg);
                                initFileInput('lifeCard', res.data.id, res.data.lifeImg);
                                initFileInput('idCard', res.data.id, res.data.idcardImg);
                                var addressId = res.data.address;
                                $.ajax({
                                    url: 'http://' + window.location.host + '/v2/address/api/' + addressId,
                                    method: 'POST',
                                    success: function (data) {
                                        console.log(data);
                                        renderingData(data);
                                    },
                                    error: function (err) {
                                        console.log('error ')
                                    }
                                });

                                //$("#view_idcard_img").attr("src",res.data.idcardImgUrl);
                                //$("#view_life_img").attr("src",res.data.lifeImgUrl);
                                $("#modal_viewDetail").modal("show");
                                /** 初始化选择框控件 **/
                                $('.select').select2({
                                    minimumResultsForSearch: Infinity,
                                });
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });

            });

            // 审核通过
            $("[data-toggle='enablepopover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认审核通过吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="enablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="enablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    enableUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "enablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="enablepopover"]').popover('hide');
                } else if (target.data("toggle") == "enablepopover") {
                    target.popover("toggle");
                }
            });


            // 删除申请
            $("[data-toggle='disablepopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除申请吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="disablepopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="disablepopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    deleteUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "disablepopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="disablepopover"]').popover('hide');
                } else if (target.data("toggle") == "disablepopover") {
                    target.popover("toggle");
                }
            });

            // 冻结账户
            $("[data-toggle='frozenpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认冻结账户吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="frozenpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="frozenpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    frozenUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });
            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "frozenpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="frozenpopover"]').popover('hide');
                } else if (target.data("toggle") == "frozenpopover") {
                    target.popover("toggle");
                }
            });

            // 解冻账户
            $("[data-toggle='unfrozenpopover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认解冻账户吗？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" mId="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="unfrozenpopover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="unfrozenpopover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("mId");
                    unfrozenUser(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#guiderUserTable');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "unfrozenpopover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="unfrozenpopover"]').popover('hide');
                } else if (target.data("toggle") == "unfrozenpopover") {
                    target.popover("toggle");
                }
            });

        }

        /**
         * 审核通过
         * @param id
         */
        function enableUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/audit?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("审核失败", {timer: 1200});
                }
            });
        }

        /**
         * 删除申请
         * @param id
         */
        function deleteUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/delete/' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("删除失败", {timer: 1200});
                }
            });
        }

        /**
         * 冻结账户
         * @param id
         */
        function frozenUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/reject?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("冻结账户失败", {timer: 1200});
                }
            });
        }

        /**
         * 解冻账户
         * @param id
         */
        function unfrozenUser(id) {
            utils.postAjaxWithBlock($(document), window.host + '/userAgent/unFrozen?id=' + id, [], function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            location.reload();
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {
                } else if (res == -1) {
                    utils.tools.alert("解冻账户失败", {timer: 1200});
                }
            });
        }

        function renderingData(data) {
            if (data.errorCode === 200 && data.data) {
                var zones = data.data.zones;

                $('#view_detail').val(data.data.street);

                $('#province').html('<option value="' + zones[1].id + '">' + zones[1].name + '</option>')
                $('#city').html('<option value="' + zones[2].id + '">' + zones[2].name + '</option>')
                $('#district').html('<option value="' + zones[3].id + '">' + zones[3].name + '</option>')
                initEditZones(zones[1].id, zones[2].id, zones[3].id);

            } else console.log('数据格式错误');
        }

        function initZones() {
            buildChildrenZone(1, document.getElementById('province'), '选择省份');
            $('#province').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('city'), '选择市');
            });
            $('#city').on('change', function () {
                buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
            });
        }

        function initEditZones(provinceId, cityId, districtId) {
            buildChildrenZone(1, document.getElementById('province'), '选择省份', function () {
                document.getElementById('province').value = provinceId;
                $('#province').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('city'), '选择市');
                });
            });
            buildChildrenZone(provinceId, document.getElementById('city'), '选择市', function () {
                document.getElementById('city').value = cityId;
                $('#city').on('change', function () {
                    buildChildrenZone(this.value, document.getElementById('district'), '选择县区');
                });
            });
            buildChildrenZone(cityId, document.getElementById('district'), '选择县区', function () {
                document.getElementById('district').value = districtId;
            });
        }

        // 初始化上传图片控件
        function initFileInput(name, id, img) {
            var $fileInput = $('#' + name + '-input');
            $fileInput.fileinput('destroy');
            var op = {
                language: 'zh',
                uploadUrl: window.host + '/userAgent/updateMultiPartImg',
                uploadExtraData: {"id": id, "type": name},
                showUpload: false,
                browseLabel: '选择图片',
                removeLabel: '删除',
                uploadLabel: '确认',
                browseIcon: '<i class="icon-file-plus"></i>',
                uploadIcon: '<i class="icon-file-upload2"></i>',
                removeIcon: '<i class="icon-cross3"></i>',
                browseClass: 'btn btn-primary',
                uploadClass: 'btn btn-default',
                removeClass: 'btn btn-danger',
                initialCaption: '',
                maxFilesNum: 1,
                enctype: 'multipart/form-data',
                allowedFileExtensions: ["jpg", "png", "gif"],
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    footer: ''
                }

            };
            //如果img有值，则显示之前上传的图片
            if (img && img.toString().indexOf('http') != -1) {
                op = $.extend({
                    showPreview: true,
                    initialPreview: [ // 预览图片的设置
                        "<img src= '" + img + "' class='file-preview-image'>"]
                }, op);
            }

            $fileInput.fileinput(op);
            $fileInput.on('fileselect', function(event, numFiles, label) {
                $fileInput.fileinput('upload');
            });
            // $fileInput.fileinput('enable');
        }

        function buildChildrenZone(zoneId, insertDom, firstOption, callback) {
            if(zoneId == '0'){
                var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                insertDom.innerHTML = frag;
                if (callback) {
                    callback();
                }
            }else{
                $.ajax({
                    url: 'http://' + window.location.host + '/v2/zone/' + zoneId + '/children',
                    method: 'POST',
                    success: function (data) {
                        if (data.data && data.errorCode) {
                            var frag = '<option value="' + 0 + '">' + firstOption + '</option>';
                            data.data.forEach(function (value) {
                                frag = frag + '<option value="' + value.id + '">' + value.name + '</option>';
                            });
                            insertDom.innerHTML = frag;
                            if (callback) {
                                callback();
                            }
                        } else console.log(data.error);
                    },
                    error: function (err) {
                        console.log('error ')
                    }
                })
            }
            if(firstOption == '选择市'){
                var frag = '<option value="' + 0 + '">选择县区</option>';
                document.getElementById('district').innerHTML = frag;
            }
        }


        // 修改保存用户代理信息
        $(document).on('click', '.saveAgentBtn', function () {

            var id = $("#view_id").val();
            var name = $("#view_name").val();
            var weixin = $("#view_weixin").val();
            var phone = $("#view_phone").val();
            var idcard = $("#view_idcard").val();
            var address = $("#view_address").val();

            var district = $("#district").val();
            var addressDetail = $("#view_detail").val();

            var type = $("#view_type").val();

            if(district == 0){
                utils.tools.alert("请选择地区!", {timer: 2000, type: 'warning'});
                return;
            }else if(addressDetail == ''){
                utils.tools.alert("请输入街道门牌信息!", {timer: 2000, type: 'warning'});
                return;
            }

            var param = {
                id: id,
                name: name,
                weixin: weixin,
                phone: phone,
                idcard: idcard,
                address: address,
                type: type,
                district: district,
                addressDetail: addressDetail
            };

            utils.postAjax(window.host + '/userAgent/checkPhone', param, function (res) {
                if (typeof(res) === 'object') {
                    if (res.data) {
                        utils.tools.confirm('此代理的手机号已经存在于其他用户，修改后手机号对应的之前用户的相关信息将被删除,是否确认修改？', function () {
                            utils.postAjax(window.host + '/userAgent/save?updatePhone=1', param, function (res) {
                                if (typeof(res) === 'object') {
                                    if (res.data) {
                                        utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                        $datatables.search('').draw();
                                    } else {
                                        utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                    }
                                }
                                $("#modal_viewDetail").modal("hide");
                            });
                        }, function () {

                        });
                    } else {
                        utils.postAjax(window.host + '/userAgent/save', param, function (res) {
                            if (typeof(res) === 'object') {
                                if (res.data) {
                                    utils.tools.alert("修改成功!", {timer: 1200, type: 'warning'});
                                    $datatables.search('').draw();
                                } else {
                                    utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                                }
                            }
                            $("#modal_viewDetail").modal("hide");
                        });
                    }
                }
            });

        });

        $(".btn-search").on('click', function () {
            $datatables.search('').draw();
        });

        initZones();

    });
//微信账号绑定
require(['all']);

require(['agent/apply']);

define("agentApply", function(){});

