/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('message/send',['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui',
  'select2', 'duallistbox'], function ($, utils) {

  const prefix = window.host + '/message';

  const listUrl = prefix + '/listByAdmin';
  const sendSmsUrl = prefix + '/sendNotification';
  const deleteUrl = prefix + '/delete';
  const editUrl = 'messageEdit';

  const $dataTable = $('#xquark_message_list_tables');
  const $agentModal = $("#modal_update_pred");
  const $body = $('body');

  const columns = ['name', 'type', 'created_at'];
  const smsTypeMapper = {
    'DELIVERY': '发货模板',
    'COMMON': '通用模板'
  };

  var $agentDualListBox;
  var agentList = [];
  var templateId = '';

  // 初始化短信类型为发货类型
  var notificationType = 'NOTIFICATION';

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "",
      emptyTable: "暂无相关数据"
    }
  });

  const manager = (function () {

    const $addBtn = $('#add_button');
    const $sendSmsBtn = $('#send_sms_btn');
    const $agentDualListSelect = $('#agentDualListBox');

    const globalInstance = {
      bindEvent: function () {
        $addBtn.on('click', function () {
          window.location = 'messageEdit';
        });
        $sendSmsBtn.on('click', function () {
          var agentIds = $agentDualListSelect.val();
          utils.tools.confirm('将发送 ' + agentIds.length + '条消息，确认发送吗?',
              function () {
                utils.postAjaxWithBlock($(document), sendSmsUrl, {
                  msgId: templateId,
                  userIds: agentIds.join(',')
                }, function (res) {
                  if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                      case 200: {
                        utils.tools.alert('消息已发送',
                            {timer: 1200, type: 'success'});
                        $agentModal.modal('hide');
                        break;
                      }
                      default: {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                      }
                    }
                  } else if (res === 0) {

                  } else if (res === -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                  }
                });
              }, function () {
              });
        });
        return this;
      }
    };

    return {
      initGlobal: function () {
        globalInstance.bindEvent();
        return this;
      },
      initDualListBox: function () {
        utils.postAjaxWithBlock($(document), window.host + "/user/list",
            {showAll: true}, function (res) {
              if (typeof(res) === 'object') {
                switch (res.errorCode) {
                  case 200: {
                    agentList = res.data.list;
                    agentList.forEach(function (agent) {
                      var id = agent.id;
                      var name = agent.name;
                      var phone = agent.phone;
                      var $option = $('<option value="' + id + '">' + name
                          + ' : ' + phone + '</option>');
                      $agentDualListSelect.append($option);
                    });
                    $agentDualListBox = $(
                        'select[name="agentDualListBox"]').bootstrapDualListbox(
                        {
                          infoText: '未选中',
                          infoTextEmpty: '已选中',
                          filterPlaceHolder: '输入姓名或手机号',
                          infoTextFiltered: '4'
                        });
                    break;
                  }
                  default: {
                    utils.tools.alert(res.moreInfo, {timer: 1200});
                    break;
                  }
                }
              } else if (res === 0) {

              } else if (res === -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
              }
            });
        return this;
      },
      refreshDualListBox: function (data) {
        if ($agentDualListBox) {
          $agentDualListSelect.empty();
          if (data) {
            data.forEach(function (item) {
              var id = item.id;
              var name = item.name;
              var phone = item.phone;
              var $option = $('<option value="' + id + '">' + name + ' : '
                  + phone + '</option>');
              $agentDualListSelect.append($option);
            })
          }
          $agentDualListBox.bootstrapDualListbox('refresh', true);
        }
      },
      initTable: function () {

        $body.unbind('click');

        $('.edit_template').on('click', function () {
          var id = $(this).attr('rowId');
          window.location = editUrl + '?id=' + id;
        });

        $('.send_sms').on('click', function () {
          // templateId
          templateId = $(this).attr('rowId');
          $agentModal.modal('show');
          if (!$agentDualListBox) {
            manager.initDualListBox();
          }
        });
        /** 点击删除部门弹出框 **/
        $("[data-toggle='popover']").popover({
          trigger: 'manual',
          placement: 'left',
          html: 'true',
          animation: true,
          content: function () {
            var rowId = $(this).attr("rowId");
            return '<span>确认删除？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="'
                + rowId + '">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
          }
        });

        $('[data-toggle="popover"]').popover() //弹窗
        .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
          $(this).parent().parent().siblings().find(
              '[data-toggle="popover"]').popover('hide');
        }).on('shown.bs.popover', function () {
          var that = this;
          $('.popover-btn-ok').on("click", function () {
            var id = $(this).attr("id");
            utils.postAjax(deleteUrl, {id: id}, function (res) {
              if (res === -1) {
                utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
              }
              if (typeof res === 'object') {
                if (res.errorCode === 200) {
                  utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                  $smsDataTables.search('').draw();
                } else {
                  if (res.moreInfo) {
                    utils.tools.alert(res.moreInfo,
                        {timer: 1200, type: 'warning'});
                  } else {
                    utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                  }
                }
              }
            });
          });
          $('.popover-btn-cancel').on("click", function () {
            $(that).popover("hide");
          });
        });

        $body.on('click', function (event) {
          var target = $(event.target);
          if (!target.hasClass('popover') //弹窗内部点击不关闭
              && target.parent('.popover-content').length === 0
              && target.parent('.popover-title').length === 0
              && target.parent('.popover').length === 0
              && target.data("toggle") !== "popover") {
            //弹窗触发列不关闭，否则显示后隐藏
            $('[data-toggle="popover"]').popover('hide');
          } else if (target.data("toggle") === "popover") {
            target.popover("toggle");
          }
        });
      }
    }
  })();

  manager.initGlobal();

  /** 初始化表格数据 **/
  const $smsDataTables = $dataTable.DataTable({
    paging: true, //是否分页
    filter: false, //是否显示过滤
    lengthChange: false,
    processing: true,
    serverSide: true,
    deferRender: true,
    searching: false,
    ajax: function (data, callback) {
      $.get(listUrl, {
        size: data.length,
        page: data.start / data.length,
        pageable: true,
        type: notificationType,
        order: columns[data.order[0].column],
        direction: data.order[0].dir
      }, function (res) {
        if (!res.data) {
          utils.tools.alert(res.moreInfo);
        }
        callback({
          recordsTotal: res.data.total,
          recordsFiltered: res.data.total,
          data: res.data.list,
          iTotalRecords: res.data.total,
          iTotalDisplayRecords: res.data.total
        });
      })
    },
    rowId: 'id',
    columns: [
      {
        data: 'title',
        width: '50px',
        orderable: true,
        name: 'title'
      },
      {
        width: '100',
        orderable: true,
        render: function (data, type, row) {
          if (row.createdAt === null) {
            return '';
          }
          return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
        }
      },
      {
        sClass: "right",
        width: "100px",
        orderable: false,
        render: function (data, type, row) {
          var html = '';
          html += '<a href="javascript:void(0);" class="edit_template" rowId="'
              + row.id
              + '" fid="edit_template"><i class="icon-pencil7"></i>编辑</a>';
          html += '<a href="javascript:void(0);" class="send_sms" style="margin-left: 10px" rowId="'
              + row.id + '" fid="send_sms"><i class="icon-pencil7"></i>发送</a>';
          html += '<a href="javascript:void(0);" class="del_template" style="margin-left: 10px;" data-toggle="popover" rowId="'
              + row.id
              + '" fid="del_template"><i class="icon-trash"></i>删除</a>';
          return html;
        }
      }

    ],
    select: {
      style: 'multi'
    },
    drawCallback: function () {  //数据加载完成后初始化事件
      manager.initTable();
    }
  });

});

define('message/edit',['jquery', 'utils', 'jquerySerializeObject', 'select2',
  'ckEditor', 'blockui'], function ($, utils, selector) {

  const prefix = window.host + '/message';

  const saveUrl = prefix + '/save';
  const viewUrl = prefix + '/view';

  const id = utils.tools.request('id');

  const manager = (function () {

    // 保存按钮
    const $btnSave = $('.btn-submit');
    // 保存并发送
    const $btnSaveAndPush = $('.btn-push');

    const $btnSaveUserList = $('.btn-user-list');

    const $sendScope = $('input[name=scope]');

    const $modalSelectUser = $('#select_user_modal');

    const $form = $('#message_info_form');

    /**
     * 返回一个函数，通过用户数组生成 option
     * @param userList user数据
     * @returns {Function} 包含userList的生成option函数
     */
    const optionSupplier = function (userList) {
      return function () {
        return userList.map(function (user) {
          var id = user.id;
          var name = user.name;
          var phone = user.phone;
          return $('<option value="' + id + '">' + name + ' : ' + phone
              + '</option>');
        });
      };
    };

    return {
      initGlobal: function () {
        if (id && id !== '') {
          utils.postAjaxWithBlock($(document), viewUrl, { id : id }, function(res) {
              if (typeof(res) === 'object') {
                  switch (res.errorCode) {
                      case 200:
                      {
                          const data = res.data;
                          utils.tools.syncForm($form, data);
                          break;
                      }
                      default:
                      {
                          utils.tools.alert(res.moreInfo, {timer: 1200});
                          break;
                      }
                  }
              } else if (res === 0) {
              } else if (res === -1) {
                  utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
              }
          });
        }

        $btnSave.on('click', function (e) {
          e.preventDefault();
          var data = $form.serializeObject();
          data.id = id;
          utils.postAjax(saveUrl, data , function (res) {
              if (res === -1) {
                  utils.tools.alert("网络问题，请稍后再试", {timer: 1200, type: 'warning'});
              }
              if (typeof res === 'object') {
                  if (res.errorCode === 200) {
                    utils.tools.alert('保存成功', { timer: 1200, type: 'success' });
                  } else {
                      if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                      } else {
                          utils.tools.alert('服务器错误', {timer: 1200, type: 'warning'});
                      }
                  }
              }
          });
        });
        return this;
      },
      initEditor: function () {
        $("#content").ckeditor();
        return this;
      }
    }
  })();

  manager.initGlobal()
  .initEditor();

});
require(['all']);

require(['message/send', 'message/edit']);

define("message", function(){});

