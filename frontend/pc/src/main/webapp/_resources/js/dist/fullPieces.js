/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('fullPieces/edit',['jquery', 'utils', 'moment', 'daterangepicker', 'jquerySerializeObject',
  'blockui', 'select2', 'datatables'], function ($, utils, moment) {

  // TODO wangxinhua 跨级删除时level显示不正确
  const prefix = window.host + '/promotionFullPieces';
  var listProducts = window.host + "/product/list";
  const listGiftProducts = window.host + '/product/listGift';
  const saveUrl = prefix + '/savePromotion';
  const preSaveUrl = prefix + '/preSave';
  const listProductsUrl = prefix + '/listProduct';
  const deleteProductUrl = prefix + '/deleteProduct';
  const addProductUrl = prefix + '/addProduct';

  const delDiscountUrl = prefix + '/deleteDiscount';

  const $addProduct = $('.btn-addProduct');
  const $applyTimeBtn = $('.apply-btn');
  const $productModal = $('#modal_result_products');
  const $chosenProductModal = $('#modal_products');
  const $chooseModalClose = $('.close_product_choose');
  const $productTitle = $('#product-title');

  var $productDataTable;
  var $selectProductDataTables;
  var preSaved = false;

  var category = '';
  var order = '';
  var gift = false;
  var $currentGiftDiv = null;
  var promotionId = $('#promotionId').val();
  const commaAppender = createStrAppender(',');

  /** 页面表格默认配置 **/
  $.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
      lengthMenu: '<span>显示:</span> _MENU_',
      info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
      paginate: {
        'first': '首页',
        'last': '末页',
        'next': '&rarr;',
        'previous': '&larr;'
      },
      infoEmpty: "暂无相关数据",
      emptyTable: "暂无相关数据"
    }
  });

  const validate = (function () {
    const obj = {
      title: function (title) {
        if (!method.nonCheck(title, '请输入活动名称')) {
          return false;
        }
      },
      validFrom: function (validFrom) {
        if (!method.nonCheck(validFrom, '请选择日期')) {
          return false;
        }
      },
      validTo: function (validTo) {
        if (!method.nonCheck(validTo, '请选择日期')) {
          return false;
        }
      },
      discounts: function (discounts) {
        // checkBox 没选默认设置为false
        for (var index in discounts) {
          if (!method.nonCheck(discounts[index].minAmount, '请输入优惠门槛')) {
            return false;
          }
        }
      }
    };
    const method = {
      nonCheck: function (value, message) {
        if (!value || (typeof value) === 'undefined' || value === '') {
          utils.tools.alert(message, {timer: 1200, type: 'warning'});
          return false;
        }
        return this;
      }
    };
    return {
      do: function (data) {
        for (var key in obj) {
          if (obj.hasOwnProperty(key) && data.hasOwnProperty(key)) {
            if (obj[key](data[key]) === false) {
              return false;
            }
          }
        }
        return true;
      }
    }
  })();

  const options = {
    timePicker: true,
    dateLimit: {days: 60000},
    startDate: moment().subtract(0, 'month').startOf('month'),
    endDate: moment().subtract(0, 'month').endOf('month'),
    autoApply: false,
    locale: {
      format: 'YYYY/MM/DD',
      separator: ' - ',
      applyLabel: '确定',
      fromLabel: '开始日期:',
      toLabel: '结束日期:',
      cancelLabel: '清空',
      weekLabel: 'W',
      customRangeLabel: '日期范围',
      daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
      monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月",
        "11月", "12月"],
      firstDay: 6
    },
    ranges: {
      '今天': [moment(), moment().endOf('day')],
      '一星期': [moment(), moment().add(6, 'days')],
      '一个月': [moment(), moment().add(1, 'months')],
      '一年': [moment(), moment().add(1, 'year')]
    },
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small btn-default'
  };

  const manager = (function () {
    const $dateRangePicker = $('.daterange-time');
    const $addLevel = $('#addLevel');
    const $submitBtn = $('.btn-submit');
    const $scopeInputs = $('input[name=scope]');

    return {
      initGlobal: function () {
        return this;
      },
      initDateRangePricker: function () {
        $dateRangePicker.daterangepicker(options, function (start, end) {
          if (start._isValid && end._isValid) {
            $dateRangePicker.val(start.format('YYYY-MM-DD HH:mm') + ' - '
                + end.format('YYYY-MM-DD HH:mm'));
          } else {
            $dateRangePicker.val('');
          }
        });
        return this;
      },
      bindEvent: function () {

        $addProduct.on('click', function () {
          $productModal.modal('show');
          $chosenProductModal.modal('hide');
        });

        $chooseModalClose.on('click', function () {
          $productModal.modal('hide');
          if (!gift) {
            $chosenProductModal.modal('show');
          }
        });

        $dateRangePicker.on('apply.daterangepicker', function (ev, picker) {
          options.startDate = picker.startDate;
          options.endDate = picker.endDate;
          $("#valid_from").val(picker.startDate.format('YYYY-MM-DD HH:mm'));
          $("#valid_to").val(picker.endDate.format('YYYY-MM-DD HH:mm'));
        });

        /**
         * 清空按钮清空选框
         */
        $dateRangePicker.on('cancel.daterangepicker', function () {
          //do something, like clearing an input
          $dateRangePicker.val('');
          $("#valid_from").val('');
          $("#valid_to").val('');
        });

        $scopeInputs.on('click', function () {
          var value = $scopeInputs.filter(':checked').val();
          scopeCallBack(value)();
        });

        /* 点击赠品checkbox */
        $(document).on('click', '.toggleGift', function () {
          $currentGiftDiv = $(this).parent();
          if ($(this).is(':checked')) {
            gift = true;
            $selectProductDataTables.search('').draw();
            $productTitle.text('选择赠品');
            $productModal.modal('show');
          } else {
            $(this).siblings('input[type=hidden]').val('');
            $(this).siblings('span[class=productNames]').text('');
          }
        });

        /* 点击删除折扣 */
        $(document).on('click', '.delDiscount', function () {
          var discountId = $(this).attr('id');
          var self = this;
          if (discountId) {
            utils.postAjax(delDiscountUrl,
                {promotionId: promotionId, id: discountId}, function (res) {
                  if (res === -1) {
                    utils.tools.alert("网络问题，请稍后再试",
                        {timer: 1200, type: 'warning'});
                  }
                  if (typeof res === 'object') {
                    if (res.errorCode === 200) {
                      utils.tools.alert('删除成功', {timer: 1200, type: 'success'});
                      $(self).closest('tr').remove();
                    } else {
                      if (res.moreInfo) {
                        utils.tools.alert(res.moreInfo,
                            {timer: 1200, type: 'warning'});
                      } else {
                        utils.tools.alert('服务器错误',
                            {timer: 1200, type: 'warning'});
                      }
                    }
                  }
                });
          } else {
            $(this).closest('tr').remove();
          }
        });

        /**
         * 优惠方式显示、隐藏 (仅限两个元素时有效)
         */
        $(document).on('toggleVisible', '.toggleDiscount', function () {
          $(this).siblings().each(function (index, item) {
            var $item = $(item);
            if ($item.is(':visible')) {
              $item.hide().addClass('hide');
            } else {
              $item.show().removeClass('hide');
            }
          });
        });

        $(document).on('click', '.toggleDiscount', function () {
          $(this).trigger('toggleVisible');
          var $parentTd = $(this).parents('td');
          // 清空优惠数据
          $parentTd.find('input[type=number]').val('');
          $parentTd.find('.toggleDiscount').not(this).filter(
              function (index, item) {
                return $(item).is(':checked');
              }).prop('checked', false).trigger('toggleVisible');
        });

        /* 将模板中的 $index$ 替换为数组相应的索引，并将模板插入到table中 */
        $addLevel.on('click', function () {
          // 最大层级为5级
          var currLevel = getMaxLevel();
          if (currLevel >= 5) {
            return;
          }
          var html = $('#table-template').html()
          .replace(/\$index\$/g, currLevel)
          .replace(/\$level\$/g, currLevel + 1);
          $('tbody').append(html);
        });

        $submitBtn.on('click', function (e) {
          e.preventDefault();
          var data = $('#form').serializeObject();
          if (validate.do(data) === false) {
            return;
          }
          data.validFrom = new Date(data.validFrom);
          data.validTo = new Date(data.validTo);
          $.ajax({
            url: saveUrl,
            type: 'POST',
            async: false,
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (res) {
              if (res.errorCode === 200 && res.data) {
                promotionId = res.data;
                $('#promotionId').val(promotionId);
                utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
                location.href = '/sellerpc/mall/fullPiecesList';
              } else {
                utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
              }
            },
            error: function (res) {
              utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
            }
          });
        });

        $(".btn-search-products").on('click', function () {
          var keyword = $.trim($("#select_products_sKeyword").val());
          if (keyword != '' && keyword.length > 0) {
            listProducts = window.host + '/product/searchbyPc/' + shopId + '/'
                + keyword;
            $selectProductDataTables.search(keyword).draw();
          } else if (keyword == '' || keyword.length == 0) {
            listProducts = window.host + "/product/list";
            $selectProductDataTables.search('').draw();
          }
        });
        return this;
      },
      initProductDataTable: function () {
        if (!$productDataTable) {
          $productDataTable = $('#xquark_list_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function (data, callback, settings) {
              $.get(listProductsUrl, {
                size: data.length,
                page: (data.start / data.length),
                pageable: true,
                id: promotionId,
                gift: gift
              }, function (res) {
                if (!res.data && !res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.total,
                  recordsFiltered: res.data.total,
                  data: res.data.list,
                  iTotalRecords: res.data.total,
                  iTotalDisplayRecords: res.data.total
                });
              });
            },
            rowId: "id",
            columns: [
              {
                title: '商品',
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
              }, {
                title: '状态',
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                  var status = '';
                  switch (row.status) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '在售';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '未发布';
                      break;
                    default:
                      break;
                  }
                  return status;
                },
              }, {
                title: '价格',
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
              }, {
                title: '库存',
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
              },
              {
                title: '销量',
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
              }, {
                title: '发布时间',
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.onsaleAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
              }, {
                title: '管理',
                orderable: false,
                width: '50px',
                render: function (data, type, row) {
                  var html = '';
                  html += '<a href="javascript:void(0);" class="del" rowId="'
                      + row.id
                      + '" fid="delete_module"><i class="icon-trash"></i>删除</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              $('.del').on('click', function () {
                var productId = $(this).attr('rowId');
                var data = {
                  productId: productId,
                  promotionId: promotionId
                };
                utils.tools.confirm('确认删除吗', function () {
                  utils.postAjax(deleteProductUrl, data, function (res) {
                    if (res === -1) {
                      utils.tools.alert("网络问题，请稍后再试",
                          {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                      if (res.errorCode === 200) {
                        utils.tools.alert('删除成功',
                            {timer: 1200, type: 'success'});
                        $productDataTable.search('').draw();
                      } else {
                        if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo,
                              {timer: 1200, type: 'warning'});
                        } else {
                          utils.tools.alert('服务器错误',
                              {timer: 1200, type: 'warning'});
                        }
                      }
                    }
                  });
                }, function () {

                });
              });
            }
          });
        }
        return this;
      }, initSelectProductDataTables: function () {
        if (!$selectProductDataTables) {
          $selectProductDataTables = $(
              '#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
              $.get(getProductUrl(), {
                size: data.length,
                page: (data.start / data.length),
                keyword: data.search.value,
                pageable: true,
                order: function () {
                  if (order !== '') {
                    return order;
                  } else {
                    var _index = data.order[0].column;
                    if (_index < 4) {
                      return '';
                    } else {
                      return $orders[_index - 4];
                    }
                  }
                },
                direction: data.order ? data.order[0].dir : 'asc',
                category: category,
                isGroupon: '',
                fromType: 'twitterCommission'
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else {
                  if (res.data.shopId) {
                    shopId = res.data.shopId;
                  }
                }
                callback({
                  recordsTotal: res.data.categoryTotal,
                  recordsFiltered: res.data.categoryTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.categoryTotal,
                  iTotalDisplayRecords: res.data.categoryTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="' + row.productUrl
                      + '"><img class="goods-image" src="' + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                data: "name",
                width: "120px",
                orderable: false,
                name: "name"
              }, {
                width: "40px",
                orderable: false,
                render: function (data, type, row) {
                  var status = '';
                  switch (row.status) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '在售';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '未发布';
                      break;
                    default:
                      break;
                  }
                  return status;
                },
              }, {
                data: "price",
                width: "50px",
                orderable: true,
                name: "price"
              }, {
                data: "amount",
                orderable: true,
                width: "50px",
                name: "amount"
              },
              {
                data: "sales",
                orderable: true,
                width: "50px",
                name: "sales"
              }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                  var cDate = parseInt(row.onsaleAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
              }, {
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                  var html = '<a href="javascript:void(0);" class="selectproduct" style="margin-left: 10px;" rowId="'
                      + row.id + '" productImg="' + row.imgUrl
                      + '" productPrice="' + row.price + '" productName="'
                      + row.name + '" ><i class="icon-pencil7" ></i>选择</a>';
                  return html;
                }
              }
            ],
            drawCallback: function () {  //数据加载完成
              $(".selectproduct").on("click", function () {
                var productId = $(this).attr('rowId');
                var productName = $(this).attr('productname');
                // 选择赠品则暂存赠品id
                if (gift) {
                  // 赠品id
                  var $currGiftIdInput = $currentGiftDiv.find(
                      'input[type=hidden]');
                  // 赠品名称
                  var $currGiftNameSpan = $currentGiftDiv.find(
                      'span[class=productNames]');

                  var currGiftIds = $currGiftIdInput.val();
                  var currGiftNames = $currGiftNameSpan.text();
                  if (currGiftIds.indexOf(productId) !== -1) {
                    utils.tools.alert('该商品已设置为赠品',
                        {timer: 1200, type: 'warning'});
                    return;
                  }
                  $currGiftIdInput.val(commaAppender(currGiftIds, productId));
                  $currGiftNameSpan.text(
                      commaAppender(currGiftNames, productName));
                  utils.tools.alert('设置成功', {timer: 1200, type: 'success'});
                } else {
                  utils.postAjax(addProductUrl, {
                    promotionId: promotionId,
                    productId: productId
                  }, function (res) {
                    if (res === -1) {
                      utils.tools.alert("网络问题，请稍后再试",
                          {timer: 1200, type: 'warning'});
                    }
                    if (typeof res === 'object') {
                      if (res.errorCode === 200) {
                        $productDataTable.search('').draw();
                        utils.tools.alert('添加成功',
                            {timer: 1200, type: 'success'});
                      } else {
                        if (res.moreInfo) {
                          utils.tools.alert(res.moreInfo,
                              {timer: 1200, type: 'warning'});
                        } else {
                          utils.tools.alert('服务器错误',
                              {timer: 1200, type: 'warning'});
                        }
                      }
                    }
                  });
                }
              });
            }
          });
        }
        return this;
      }
    }
  })();

  manager.initGlobal() // 全局初始化
  .initDateRangePricker() // 时间控件初始化
  .bindEvent() // 全局时间绑定
  .initProductDataTable() // 初始化以选择商品表格
  .initSelectProductDataTables(); // 初始化全部商品表格

  function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  function scopeCallBack(scope) {
    var scopeMapper = {
      'PRODUCT': function () {
        gift = false;
        preSaveForm(function () {
          if ($productDataTable) {
            $productDataTable.search('').draw();
            $selectProductDataTables.search('').draw();
          }
          $productTitle.text('选择商品');
          $chosenProductModal.modal('show');
        });
      },
      'ALL': function () {
        $chosenProductModal.modal('hide');
      }
    };
    return scopeMapper[scope];
  }

  /**
   * 获取当前的最大层级
   */
  function getMaxLevel() {
    var maxLevel = 0;
    $('#form').find('.level').each(function () {
      var level = parseInt($(this).text());
      if (level > maxLevel) {
        maxLevel = level;
      }
    });
    return maxLevel;
  }

  function preSaveForm(callback) {
    var data = $('#form').serializeObject();
    data.validFrom = new Date(data.validFrom);
    data.validTo = new Date(data.validTo);
    delete data['discounts'];
    $.ajax({
      url: preSaveUrl,
      type: 'POST',
      async: false,
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(data),
      success: function (res) {
        if (res.errorCode === 200 && res.data) {
          var promotion = res.data;
          promotionId = promotion.id;
          $('#promotionId').val(promotionId);
          // utils.tools.alert('保存成功', {timer: 1200, type: 'success'});
          console.log(promotionId);
          preSaved = true;
          if (callback) {
            callback();
          }
        } else {
          utils.tools.alert('保存失败', {timer: 1200, type: 'warning'});
        }
      },
      error: function (res) {
        utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
      }
    });
  }

  function getProductUrl() {
    if (gift) {
      return listGiftProducts;
    } else {
      return listProducts;
    }
  }

  function createStrAppender(split) {
    return function (str1, str2) {
      return appendStr(str1, str2, split);
    }
  }

  function appendStr(str1, str2, split) {
    return str1 === '' ? str1 + str2 : str1 + (split ? split : '') + str2;
  }

});

define('fullPieces/list',['jquery', 'utils', 'jquerySerializeObject', 'datatables', 'blockui', 'select2'], function ($, utils) {

    const prefix = window.host + '/promotionFullPieces';
    const listUrl = prefix + '/listTable';
    const closeUrl = prefix + '/close';
    const listDetailUrl = prefix + '/orderDetail';

    const $dataTable = $('#xquark_full_cut_tables');
    const $detailTable = $('#xquark_order_detail_table');

    const typeMapper = {
        'FULLCUT': '满减优惠',
        'FULLPIECES': '满件优惠'
    };

    var keyWord = '';

    /** 页面表格默认配置 **/
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
            infoEmpty: "",
            emptyTable: "暂无相关数据"
        }
    });

    const manager = (function () {

        const $addPromotionBtn = $('.btn-release');

        const globalInstance = {};

        return {
            initGlobal: function () {
                return this;
            },
            initEvent: function () {
                $addPromotionBtn.on('click', function () {
                    location.href = '/sellerpc/mall/fullPiecesEdit';
                });
                return this;
            },
            initTable: function () {
                $('.edit').on('click', function () {
                    var id = $(this).attr('rowId');
                    location.href = '/sellerpc/mall/fullPiecesEdit?id=' + id;
                });

                $('.del').on('click', function () {
                    var id = $(this).attr('rowId');
                    utils.tools.confirm("确认结束活动吗", function () {
                        utils.postAjaxWithBlock($(document), closeUrl, {id: id}, function (res) {
                            if (typeof(res) === 'object') {
                                switch (res.errorCode) {
                                    case 200: {
                                        utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
                                        $fullCutDataTables.search('').draw();
                                        break;
                                    }
                                    default: {
                                        utils.tools.alert(res.moreInfo, {timer: 1200});
                                        break;
                                    }
                                }
                            } else if (res == 0) {

                            } else if (res == -1) {
                                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                            }
                        })
                    }, function () {

                    });
                });
            }
        }
    })();

    manager.initGlobal().initEvent();

    /** 初始化表格数据 **/
    const $fullCutDataTables = $dataTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback) {
            $.get(listUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true,
                keyWord: keyWord
                // order: $columns[data.order[0].column],
                // direction: data.order[0].dir
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                title: '',
                width: "10px",
                orderable: false,
                render: function (data, type, row) {
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="' + row.id + '"></label>';
                }
            },
            {
                title: '标题',
                data: "title",
                width: "120px",
                orderable: false,
                name: "title"
            }, {
                title: '开始时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validFrom"
            }, {
                title: '结束时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validTo"
            }, {
                title: '状态',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.archive) {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '发布时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            },
            {
                title: '管理',
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="' + row.id + '" ><i class="icon-pencil7" ></i>编辑</a>';
                    html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" ><i class="icon-trash"></i>结束</a>';
                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            manager.initTable();
        }
    });

    const $detailDataTable = $detailTable.DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: false,
        ajax: function (data, callback) {
            $.get(listDetailUrl, {
                size: data.length,
                page: data.start / data.length,
                pageable: true
            }, function (res) {
                if (!res.data) {
                    utils.tools.alert(res.moreInfo, {timer: 1200, type: 'warning'});
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                });
            })
        },
        rowId: 'id',
        columns: [
            {
                title: '活动名称',
                data: "title",
                width: "50px",
                orderable: false,
                name: "title"
            },
            {
                title: '优惠类型',
                width: "80px",
                orderable: false,
                name: "type",
                render: function (data, type, row) {
                    var type = row.type;
                    return typeMapper[type];
                }
            },
            {
                title: '买家',
                width: "100px",
                orderable: false,
                name: "buyer",
                render: function (data, type, row) {
                    var name = row.buyerName;
                    var phone = row.buyerPhone;
                    return (name ? name : '') + (phone ? '</br>' + phone : '');
                }
            },
            {
                title: '卖家',
                width: "100px",
                orderable: false,
                name: "seller",
                render: function (data, type, row) {
                    var name = row.sellerName;
                    var phone = row.sellerPhone;
                    return (name ? name : '') + (phone ? '</br>' + phone : '');
                }
            },
            {
                title: '订单号',
                data: "orderNo",
                width: "100px",
                orderable: false,
                name: "orderNo"
            },
            {
                title: '订单原价',
                data: "goodsFee",
                width: "50px",
                orderable: false,
                name: "goodsFee"
            },
            {
                title: '购买价',
                data: "totalFee",
                width: "50px",
                orderable: false,
                name: "totalFee"
            },
            {
                title: '优惠价',
                data: "discountFee",
                width: "50px",
                orderable: false,
                name: "discountFee"
            },
            {
                title: '开始时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validFrom);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validFrom"
            }, {
                title: '结束时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.validTo);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "validTo"
            }, {
                title: '状态',
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch (row.archive) {
                        case false:
                            status = '进行中';
                            break;
                        case true:
                            status = '已结束';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            }, {
                title: '发布时间',
                orderable: false,
                width: "120px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name: "onsaleAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            // manager.initTable();
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});


require(['all']);

//module
require(['fullPieces/edit', 'fullPieces/list'],function(){
});

define("fullPieces", function(){});

