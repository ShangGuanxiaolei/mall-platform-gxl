/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by chh on 16/10/19.
 */
define('tweet/list',['jquery','utils','datatables','blockui','bootbox', 'select2'], function($,utils,datatabels,blockui,select2) {

    var $listUrl = window.host + "/tweet/list";

    var $shopId = null;

    var $rowId = '';

    var $keyword = '';

    /** 初始化选择框控件 **/
    $('.select').select2({
        minimumResultsForSearch: Infinity,
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    // 新增发现记录
    $(".btn-release").on('click', function() {
        window.location.href = '/sellerpc/tweet/edit';
    });

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length),
                keyword: $keyword,
                keyword: data.search.value,
                pageable: true
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "10px",
                orderable: false,
                render: function(data, type, row){
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" value="'+row.id+'"></label>';
                }
            },
            {
                data: "title",
                width: "120px",
                orderable: false,
                name:"title"
            },{
                data: "description",
                width: "120px",
                orderable: false,
                name:"description"
            },{
                width: "60px",
                orderable: false,
                render: function (data, type, row) {
                    var status = '';
                    switch(row.targetType)
                    {
                        case 'PRODUCT':
                            status = '商品';
                            break;
                        case 'GROUPON':
                            status = '团购';
                            break;
                        case 'ACTIVITY':
                            status = '活动';
                            break;
                        case 'CATEGORY':
                            status = '类别';
                            break;
                        case 'CUSTOMIZED':
                            status = '自定义';
                            break;
                        default:
                            break;
                    }
                    return status;
                },
            },{
                data: "targetName",
                width: "120px",
                orderable: false,
                name:"targetName"
            }, {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            },{
                sClass: "right",
                width: "120px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                        html += '<a href="javascript:void(0);" class="edit" style="margin-left: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';

                    return html;
                }
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){
        // 先去掉body的所有click监听，否则后面再加入click监听会让界面popover产生异常
        $('body').unbind('click');

        $(".edit").on("click",function(){
            var pId =  $(this).attr("rowId");
            window.location.href = '/sellerpc/tweet/edit?pId='+pId;
        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'manual',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" pId="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var pId = $(this).attr("pId");
                deleteTweet(pId);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });


    }


    /*将发现数据手动删除*/
    function  deleteTweet(pId){
          var url = window.host + "/tweet/delete/"+pId;
          utils.postAjax(url,{},function(res){
              if(typeof(res) === 'object') {
                  if (res.data) {
                      alert('操作成功');
                      $datatables.search('').draw();
                  } else {
                      utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                  }
              }
          });
    }

    // 保存组件
    $(".saveBtn").on('click', function() {
        var url = window.host + '/viewPage/save';
        var id = $("#id").val();
        var title = $("#title").val();
        var description = $("#description").val();
        var pageType = $("#pageType").val();
        var layout = $("#layout").val();
        var backgroundImg = $("#backgroundImg").val();
        var customizedUrl = $("#customizedUrl").val();

        if(!title || title == '' ){
            utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
            return;
        }else if(!layout || layout == ''){
            utils.tools.alert("请输入页面布局!", {timer: 1200, type: 'warning'});
            return;
        }

        var data = {
            id: id,
            title: title,
            description: description,
            pageType: pageType,
            layout: layout,
            backgroundImg: backgroundImg,
            customizedUrl: customizedUrl
        };
        utils.postAjaxWithBlock($(document), url, data, function(res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200:
                    {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/view/viewpage';
                        break;
                    }
                    default:
                    {
                        utils.tools.alert(res.moreInfo, {timer: 1200});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });

    });


    $(".btn-search").on('click', function() {
        var keyword = $.trim($("#sKeyword").val());
        $keyword = keyword;
        $datatables.search( keyword ).draw();
    });


});
/**
 * Created by quguangming on 16/5/24.
 */
define('product/upload',['jquery','utils','dropzone'], function($,utils) {
    Dropzone.autoDiscover = false;

    var imgDropzone =  $("#product_image_dropzone").dropzone({
        url: window.host + "/_f/u?belong=PRODUCT",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 100, // MB      //最大文件大小，单位是 MB ,不限制
        maxFiles: 10,               //限制最多文件数量
        maxThumbnailFilesize: 10,
        addRemoveLinks: true,
        thumbnailWidth:"150",      //设置缩略图的缩略比
        thumbnailHeight:"150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: false,
        dictInvalidFileType:"文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                        //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation:"你确定要取消上传吗？",              //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function() {

            // var imgDropzone = this;
            //添加了文件的事件
            this.on("addedfile", function (file) {
                $(".btn-submit").addClass("disabled");
                if (file && file.dataImg && file.previewElement){ //是网络加载的数据
                    $(file.previewElement).attr("data-img",file.dataImg);
                    if(file.size=='' || file.length ==0){
                        $(file.previewElement).find(".dz-details").hide();
                    }
                }
                //imgDropzone.processQueue();
            });
            this.on("success", function(file,data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200:{
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                if (file && file.previewElement){
                                    $(file.previewElement).attr("data-img",imgId);
                                }
                            }
                            break;
                        }
                        default:{
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else{
                    if (data == -1){
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function(file, error) {
                utils.tools.alert(error, {timer: 1200, type: 'warning'});
                $(".dz-error-message").html(error);
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function(file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("thumbnail", function(file) {
                if (file && file.previewTemplate){
                    file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
                    file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
                }
                $(".dz-image").css("height","150px;");
                $(".dz-image").css("width","150px;");

            });

        }
    });

    return imgDropzone;
});
/**
 * Created by chh on 16/10/19.
 */
define('tweet/edit',['jquery', 'utils', 'product/upload', 'select2'],
    function ($, utils, uploader, select2) {

    var $category = '';

    var $orders = ['price','amount','sales','onsale'];

    var $order = '';

    var $productlistUrl = window.host + "/product/list";

    var $viewkeyword = '';

    var $grouponkeyword = '';

    var $pageId = utils.tools.request('pId');  //商品Id
    $(".btn-submit").on('click', function() {
        save();
    });

    $('body').on('change','select[name="categoryType"]', function(event) {
            $productlistUrl = window.host + "/product/list";
            $category = $(this).val();
            $selectproductdatatables.search('').draw();
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
    });

    //初始化商品分类信息
    $.ajax({
            url: window.host + '/shop/category/list',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.errorCode == 200) {
                    var dataLength = data.data.length;
                    for (var i = 0; i < dataLength; i++) {
                        $("#categoryType").append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
                    };
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                }
            },
            error: function (state) {
                if (state.status == 401) {
                } else {
                    utils.tools.alert('获取店铺分类信息失败！', {timer: 1200, type: 'warning'});
                }
            }
    });

    // 跳转类型改变后，清空之前选择的跳转目标
    $("#targetType").on('change', function() {
        $("#target").val('');
        $("#targetName").val('');
    });

    // 选择跳转目标
    $("#targetName").on('focus', function() {
        var targetType = $("#targetType").val();
        // 跳转目标为商品，则弹出商品选择界面
        if(targetType == 'PRODUCT'){
            $selectproductdatatables.search('').draw();
            $("#modal_select_products").modal("show");
        }
        // 自定义，则弹出自定义选择界面
        else if(targetType == 'CUSTOMIZED'){
            $selectviewdatatables.search('').draw();
            $("#modal_select_views").modal("show");
        }
        // 团购，则弹出团购选择界面
        else if(targetType == 'GROUPON'){
            $selectgroupondatatables.search('').draw();
            $("#modal_select_groupon").modal("show");
        }

    });

    //发现信息修改
    if ($pageId) {
        $("#pageId").val($pageId);

        $.ajax({
            url: window.host + '/tweet/' + $pageId,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.errorCode == 200) {

                    var tweet = data.data;
                    //商品图片
                    if (tweet.imgs) {
                        $.each(tweet.imgs, function (i, img) {
                            var key = img.img,
                                url = img.imgUrl,
                                img = {
                                    imgUrl: url,
                                    id: key
                                };
                            addProductImage(img);
                        });
                    }
                    $("#title").val(tweet.title);
                    $("#description").val(tweet.description);
                    $("#target").val(tweet.target);
                    $("#targetName").val(tweet.targetName);
                    $("#targetType").val(tweet.targetType);
                    /** 初始化选择框控件 **/
                    $('.select').select2({
                        minimumResultsForSearch: Infinity,
                    });

                } else {
                    alert(data.moreInfo);
                }
            },
            error: function (state) {
                if (state.status == 401) {
                    utils.tool.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });

    } else { //发布商品操作

    }

    var $selectproductdatatables = $('#xquark_select_products_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get($productlistUrl, {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    order: function () {
                        if($order != ''){
                            return $order;
                        }else{
                            var _index = data.order[0].column;
                            if ( _index < 3){
                                return '';
                            } else {
                                return $orders[_index - 3];
                            }
                        }
                    },
                    direction: data.order ? data.order[0].dir :'asc',
                    category : $category,
                    isGroupon : ''
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.categoryTotal,
                        recordsFiltered: res.data.categoryTotal,
                        data: res.data.list,
                        iTotalRecords:res.data.categoryTotal,
                        iTotalDisplayRecords:res.data.categoryTotal
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<a href="'+row.productUrl+'"><img class="goods-image" src="'+row.imgUrl+'" /></a>';
                    }
                },
                {
                    data: "name",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "40px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.status)
                        {
                            case 'INSTOCK':
                                status = '下架';
                                break;
                            case 'ONSALE':
                                status = '在售';
                                break;
                            case 'FORSALE':
                                status = '待上架发布';
                                break;
                            case 'DRAFT':
                                status = '未发布';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "price",
                    width: "50px",
                    orderable: true,
                    name:"price"
                }, {
                    data: "amount",
                    orderable: true,
                    width: "50px",
                    name:"amount"
                },
                {
                    data: "sales",
                    orderable: true,
                    width: "50px",
                    name:"sales"
                },{
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.onsaleAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"onsaleAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.name+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    var $selectviewdatatables = $('#xquark_select_views_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/viewPage/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: $viewkeyword,
                    keyword: data.search.value,
                    pageable: true
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    data: "title",
                    width: "120px",
                    orderable: false,
                    name:"title"
                },{
                    data: "description",
                    width: "120px",
                    orderable: false,
                    name:"description"
                },{
                    width: "60px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.pageType)
                        {
                            case 'TWEET':
                                status = '发现';
                                break;
                            case 'GROUPON':
                                status = '拼团';
                                break;
                            case 'FLASHSALE':
                                status = '特卖';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.title+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    var $selectgroupondatatables = $('#xquark_select_groupon_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function(data, callback, settings) {
                $.get(window.host + "/promotionGroupon/list", {
                    size: data.length,
                    page: (data.start / data.length),
                    keyword: data.search.value,
                    pageable: true,
                    status: '',
                    keywords : $grouponkeyword
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    } else{
                        if (res.data.shopId) {
                            $shopId = res.data.shopId;
                        }
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords:res.data.total,
                        iTotalDisplayRecords:res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
               {
                    width: "30px",
                    orderable: false,
                    render: function(data, type, row){
                        return '<img class="goods-image" src="'+row.productImg+'" />';
                    }
                },
                {
                    data: "productName",
                    width: "120px",
                    orderable: false,
                    name:"name"
                }, {
                    width: "80px",
                    orderable: false,
                    render: function (data, type, row) {
                        var status = '';
                        switch(row.archive)
                        {
                            case false:
                                status = '进行中';
                                break;
                            case true:
                                status = '已结束';
                                break;
                            default:
                                break;
                        }
                        return status;
                    },
                }, {
                    data: "productPrice",
                    width: "50px",
                    orderable: false,
                    name:"price"
                }, {
                    data: "discount",
                    width: "80px",
                    orderable: false,
                    name:"discount"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validFrom);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"validFrom"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.validTo);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"validTo"
                },{
                    orderable: false,
                    width: "140px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                },{
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '<a href="javascript:void(0);" class="selectTarget" style="margin-left: 10px;" rowId="'+row.id+'" name="'+row.productName+'" ><i class="icon-pencil7" ></i>选择</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initSelectTargetEvent();
            }
    });

    function initSelectTargetEvent(){
            $(".selectTarget").on("click",function(){
                var target =  $(this).attr("rowId");
                var targetName =  $(this).attr("name");
                $("#target").val(target);
                $("#targetName").val(targetName);
                $("#modal_select_products").modal("hide");
                $("#modal_select_views").modal("hide");
                $("#modal_select_groupon").modal("hide");
            });
    }

    //添加图片
    function addProductImage(img) {
        if (img) {
            var mockFile = {name: "", size: "", dataImg: img.id};
            uploader[0].dropzone.emit("addedfile", mockFile);
            uploader[0].dropzone.emit("thumbnail", mockFile, img.imgUrl);
            uploader[0].dropzone.emit("complete", mockFile);
        }
    }

    //保存
    function save(){
            var imgArr = [];
            $('#product_image_dropzone > .dz-complete').each(function(i, item) {
                imgArr.push($(item).attr('data-img'));
            });

            var pId = $("#pageId").val();
            var title = $("#title").val();
            var description = $("#description").val();
            var target = $("#target").val();
            var targetType = $("#targetType").val();

            if(!title || title == '' ){
                utils.tools.alert("请输入标题!", {timer: 1200, type: 'warning'});
                return;
            }else if(!targetType || targetType == ''){
                utils.tools.alert("请选择跳转类型!", {timer: 1200, type: 'warning'});
                return;
            }else if(!target || target == ''){
                utils.tools.alert("请选择跳转目标!", {timer: 1200, type: 'warning'});
                return;
            }else if(imgArr.length == 0){
                utils.tools.alert("请选择图片!", {timer: 1200, type: 'warning'});
                return;
            }

            var param = {
                id:pId,
                imgs: imgArr.join(','),
                title: title,
                description: description,
                target: target,
                targetType: targetType
            };

            $.ajax({
                url: host + '/tweet/save',
                type: 'POST',
                data: param,
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        alert("操作成功");
                        window.location.href = window.originalHost + '/tweet/list';
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });

        }

    $(".btn-search-products").on('click', function() {

            var keyword = $.trim($("#select_products_sKeyword").val());
            if (keyword != '' && keyword.length > 0 && shopId != null){
                $productlistUrl = window.host + '/product/searchbyPc/' + shopId + '/' + keyword;
                $selectproductdatatables.search( keyword ).draw();
            }else if (keyword == '' || keyword.length == 0 ){
                $productlistUrl = window.host + "/product/list";
                $selectproductdatatables.search('').draw();
            }


    });

    $(".btn-search-views").on('click', function() {
        var keyword = $.trim($("#sviewKeyword").val());
        $viewkeyword = keyword;
        $selectviewdatatables.search( keyword ).draw();
    });

    $(".btn-search-groupon").on('click', function() {
        var keyword = $.trim($("#select_groupon_sKeyword").val());
        $grouponkeyword = keyword;
        $selectgroupondatatables.search( keyword ).draw();
    });

});
/**
 * Created by chh on 16/10/19.
 */
require(['all']);

//module
require(['jquery','uniform','placeholder','tweet/list','tweet/edit'],function(){


});
define("tweet", function(){});

