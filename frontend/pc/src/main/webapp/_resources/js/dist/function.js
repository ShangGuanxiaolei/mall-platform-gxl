/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('function/manage',['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree',
        'fileinput_zh', 'fileinput'],
    function ($, utils, datatabels, blockui, select2, tree, fileinput_zh,
              fileinput) {

        var $listUrl = window.host + '/function/listTable';

        var $columns = ['name', 'module_url', 'visRolesDesc',
            'visRolesDesc', 'created_at'];

        var $functionId = '';

        var $settings = '';

        var $isReload = '';

        // 角色表格是否为第一次加载
        var $isFirstLoad = true;

        /* 当前选中节点 */
        var $selected = {
            'id': null,
            'parent': null
        };

        /* 全局事件绑定 */
        eventBind();

        var $roleFunctionSelect = function () {
            var checked = [];
            var unchecked = [];

            return {
                addChecked: function (roleId) {
                    if (checked.indexOf(roleId) === -1) {
                        checked.push(roleId);
                    }
                },
                removeChecked: function (roleId) {
                    checked.splice(checked.indexOf(roleId), 1);
                },
                isChecked: function (roleId) {
                    return checked.indexOf(roleId) !== -1;
                },
                addUnckecked: function (roleId) {
                    if (unchecked.indexOf(roleId) === -1) {
                        unchecked.push(roleId);
                    }
                },
                removeUnchecked: function (roleId) {
                    unchecked.splice(checked.indexOf(roleId), 1);
                },
                isUnchecked: function (roleId) {
                    return unchecked.indexOf(roleId) !== -1;
                },
                getChecked: function () {
                    return checked.join(',');
                },
                getUnchecked: function () {
                    return unchecked.join(',');
                },
                clear: function () {
                    checked = [];
                    unchecked = [];
                }
            }
        }();

        var merchantId = '';

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {
                    'first': '首页',
                    'last': '末页',
                    'next': '&rarr;',
                    'previous': '&larr;'
                },
                infoEmpty: "",
                emptyTable: "菜单下暂无按钮"
            }
        });

        /** 初始化表格数据 **/
        var $datatables = $('#xquark_function_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            pageable: true,
            ajax: function (data, callback, settings) {
                $.get($listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    moduleId: $selected.id,
                    pageable: true,
                    order: $columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (res.errorCode != '200') {
                        utils.tools.alert('数据加载失败');
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    data: 'name',
                    width: '60px',
                    orderable: true,
                    name: 'name'
                },
                {
                    data: 'htmlId',
                    width: '60px',
                    orderable: true,
                    name: 'htmlId'
                },
                {
                    width: '60px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.moduleName;
                    }
                },
                {
                    width: '120px',
                    orderable: false,
                    render: function (data, type, row) {
                        var visRoles = row.visRolesDesc;
                        if (visRoles) {
                            return visRoles.split(',').join(', \n');
                        }
                        return '默认 (全部可见)';
                    }
                },
                {
                    width: '120px',
                    orderable: false,
                    render: function (data, type, row) {
                        var avaRoles = row.avaRolesDesc;
                        if (avaRoles) {
                            return avaRoles.split(',').join(', \n');
                        }
                        return '默认 (全部可用)';
                    }
                },
                {
                    width: '80px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt === null) {
                            return '';
                        }
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit_setting_vis role_check_table" rowId="'
                            + row.id + '" fid="set_visible"><i class="icon-pencil7" ></i>可见角色</a>';
                        html += '<a href="javascript:void(0);" class="edit_setting_ava role_check_table" style="margin-left: 10px;" rowId="'
                            + row.id + '" fid="set_enable"><i class="icon-pencil7" ></i>可用角色</a>';
                        html += '<a href="javascript:void(0);" class="delete_function_role role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'
                            + row.id + '" functionId="' + row.id
                            + '" fid="delete_button"><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });

        var $roledatatables = $('#xquark_select_roles_tables').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ajax: function (data, callback, settings) {
                $.get(window.host + '/function/listRoleTable', {
                    functionId: $functionId,
                    pageable: true,
                    settings: $settings
                }, function (res) {
                    if (res.errorCode != 200) {
                        res.list = [];
                        utils.tools.alert('选择角色表格数据加载失败');
                        return;
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId: "id",
            columns: [
                {
                    width: "15px",
                    orderable: false,
                    render: function (data, type, row) {
                        var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled" ';
                        if (row.isSelect) {
                            str = str + ' checked="checked" ';
                        }
                        str = str + ' value="' + row.id + '"></label>';
                        return str;
                    }
                },
                {
                    data: "roleName",
                    width: "120px",
                    orderable: false,
                    name: "roleName"
                }, {
                    data: "roleDesc",
                    width: "120px",
                    orderable: false,
                    name: "roleDesc"
                }
                , {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name: "createdAt"
                }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                roleInitEvent();
            }
        });

        var jstree = $('#function_tree');
        jstree.jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {"stripes": true},
                'data': {
                    'url': window.host + '/module/listTree',
                    'data': function (node) {
                        if (node === null) {
                            return {'id': '0'};
                        }
                        return {'id': node.id};
                    }
                }
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 9,
                    "valid_children": ["root"]
                },
                "root": {
                    "icon": "/static/3.3.2/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "plugins": [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow"
            ],
            "contextmenu": {
                "items": {
                    "create": false,
                    "rename": false,
                    "remove": false
                }
            }
        }).on('loaded.jstree', function () {
            // 只展开第一级菜单
            jstree.jstree("select_node", "ul > li:first");
            var selectedNode = jstree.jstree("get_selected");
            jstree.jstree('open_node', selectedNode, false, true);
        }).on('delete_node.jstree', function (event, data) {
            // deleteModule(data.node.id);
        }).on('select_node.jstree', function (event, data) {
            onSelect(event, data);
        });

        /**
         * jsTree选中节点是刷新表格
         * @param event
         * @param data
         */
        function onSelect(event, data) {
            $selected = data.node;
            // console.log("当前节点: " + $selected.id + " : " + $selected.parent);
            $datatables.search('').draw();
        }

        /**
         * 全局事件绑定
         */
        function eventBind() {

            /** 添加按钮 **/
            $('#add_function').on('click', function () {
                if ($selected.id === '0' || $selected.parent === null
                    || $selected.parent === '0') {
                    utils.tools.alert('请选择子菜单');
                    return;
                }
                $('#id').val('');
                $('#htmlId').val('');
                $('#functionName').val('');
                $('#modal_function_save').modal('show');
            });

            $('#refresh_cache').on('click', function () {
                var url = window.host + '/cache/function';
                utils.postAjax(url, null, function (res) {
                    if (typeof(res) === 'object') {
                        if (res.errorCode == 200) {
                            // 全局变量复位
                            utils.tools.alert('操作成功，配置已生效');
                        } else {
                            utils.tools.alert(res.msg,
                                {title: '刷新失败', timer: 1200, type: 'warning'});
                        }
                    }
                });
            });

            // 按钮角色绑定确认角色确认
            $(".changeRoleBtn").on('click', function () {
                modify();
            });

            /* 保存按钮配置 */
            $('#saveBtn_function').on('click', function () {
                var url_post = window.host + '/function/save';
                var moduleId = $selected.id;
                var htmlId = $('#htmlId').val();
                var name = $('#functionName').val();
                if (!name || name === null) {
                    utils.tools.alert("请输入按钮名称!", {timer: 1200, type: 'warning'});
                    return;
                }
                if (!htmlId || htmlId === null) {
                    utils.tools.alert('请输入按钮ID', {timer: 1200, type: 'warning'});
                    return;
                }
                var data = {
                    name: name,
                    htmlId: htmlId,
                    moduleId: moduleId
                };
                utils.postAjaxWithBlock($(document), url_post, data, function (res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $datatables.search('').draw();
                                $('#modal_function_save').modal('hide');
                                utils.tools.alert('操作成功');
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                })
            });

            buttonRoleCheck('.hideClass');
        }

        // 角色表格初始化事件
        function roleInitEvent() {

            $(".styled").change(function () {
                var roleId = $(this).val();
                if (this.checked == true) {
                    if ($roleFunctionSelect.isUnchecked(roleId)) {
                        $roleFunctionSelect.removeUnchecked(roleId);
                        // console.log('删除未选中');
                    }
                    else {
                        $roleFunctionSelect.addChecked(roleId);
                        // console.log('添加选中');
                    }
                } else {
                    if ($roleFunctionSelect.isChecked(roleId)) {
                        // console.log('删除选中');
                        $roleFunctionSelect.removeChecked(roleId);
                    }
                    else {
                        // console.log('添加未选中');
                        $roleFunctionSelect.addUnckecked(roleId);
                    }
                }
            });

            if ($isReload) {
                $.unblockUI();
                $("#modal_select_roles").modal("show");
            }
        }

        function initEvent() {

            $('body').unbind('click');

            $(".edit_setting_vis").on("click", function () {
                // 清除已选中
                $roleFunctionSelect.clear();
                $settings = 'show';
                $functionId = $(this).attr("rowId");
                $isReload = true;
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });
                $roledatatables.search('').draw();

            });

            $(".edit_setting_ava").on("click", function () {
                // 清除以选中
                $roleFunctionSelect.clear();
                $settings = 'enable';
                $functionId = $(this).attr("rowId");
                $isReload = true;
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    timeout: 3000, //unblock after 5 seconds
                    overlayCSS: {
                        backgroundColor: '#1b2024',
                        opacity: 0.8,
                        zIndex: 1200,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        color: '#fff',
                        padding: 0,
                        zIndex: 1201,
                        backgroundColor: 'transparent'
                    }
                });

                $roledatatables.search('').draw();

            });


            /** 点击删除merchant弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'click',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var id = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'
                        + id + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find(
                        '[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var param = $(this).attr("param");
                    deleteFunction(param);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_function_tables');

            //给Body加一个Click监听事件
            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });
        }

        function modify() {
            var checked = $roleFunctionSelect.getChecked();
            var unChecked = $roleFunctionSelect.getUnchecked();
            var url = window.host + '/function/modifyRole';
            // console.log('functionId: ' + $functionId);
            // console.log("settings: " + $settings);
            // console.log('checkedRoles: \n');
            // console.log(checked);
            // console.log('uncheckedRoles: \n');
            // console.log(unChecked);
            var param = {
                'functionId': $functionId,
                'idsToSave': checked,
                'idsToRemove': unChecked,
                'settings': $settings
            };
            // 全局变量复位
            $settings = '';
            $roleFunctionSelect.clear();
            utils.postAjaxWithBlock($(document), url, param, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            if (res.data) {
                                $datatables.search('').draw();
                                utils.tools.alert("操作成功", {timer: 1200, type: 'success'});
                            } else {
                                utils.tools.alert("操作操作失败", {timer: 1200, type: 'success'});
                            }
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })


        }

        function deleteFunction(id) {
            var url = window.host + '/function/delete/' + id;

            $functionId = '';
            utils.postAjax(url, null, function (res) {
                if (typeof(res) === 'object') {
                    if (res.errorCode == 200) {
                        $datatables.search('').draw();
                        utils.tools.alert('操作成功');
                    } else {
                        utils.tools.alert(res.msg, {timer: 1200, type: 'warning'});
                    }
                }
            });
        }
    }
);


require(['all']);

require(['function/manage']);

define("function", function(){});

