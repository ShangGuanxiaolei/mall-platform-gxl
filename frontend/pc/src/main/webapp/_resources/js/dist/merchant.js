/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/24.
 */
define('merchant/list',['jquery','utils','datatables','blockui','bootbox'], function($,utils,datatabels,blockui) {


    $(".btn-admin-add").on('click', function() {
        location.href = '/sellerpc/merchant/admin/add';
    });

    /** 页面表格默认配置 **/
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
            infoEmpty: "",
            emptyTable:"暂无相关数据"
        }
    });

    var _userId = null;

    var merchantId = '';

    var $datatables = $('#xquark_list_tables').DataTable({
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            // make a regular ajax request using data.start and data.length
            $.get('/sellerpc/merchant/admin/list', {
                size: data.length,
                page: (data.start / data.length) +1,
                keyword: data.search.value,
                orderIndex: data.order[0].column,
                isAsc: data.order ? data.order[0].dir == 'asc':false
            }, function(res) {
                // map your server's response to the DataTables format and pass it to
                // DataTables' callback
                if (!res.data.rows) {
                    res.data.rows = [];
                }
                _userId = res.data.userId;
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.rows,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                data: "loginname",
                width: "180px",
                orderable: true,
                name:"loginname"
            }, {
                data: "name",
                width: "200px",
                orderable: true,
                name:"name"
            }, {
                data: "phone",
                width: "180px",
                orderable: true,
                name:"phone"
            }
            /**, {
                data: "wechat",
                orderable: true,
                width: "180px",
                name:"wechat"
            }**/
             , {
                orderable: true,
                width: "320px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"created_at"
            },
            /**{
                data: "rolesDesc",
                orderable: true,
                width: "180px",
                name:"rolesDesc"
            },**/
            {
                width: "220px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '<a href="javascript:void(0);" class="edit" style="margin-right: 10px;" rowId="'+row.id+'" ><i class="icon-pencil7" ></i>编辑</a>';
                    if ( _userId != row.id ) {
                        /**html += '<a hhref="javascript:void(0);" class="settings"  style="margin-right: 10px;" rowId="'+row.id+'"  ><i class="icon-checkbox-checked"></i>角色设置</a>';**/
                        html += '<a href="javascript:void(0);" class="del" data-toggle="popover" rowId="'+row.id+'" ><i class="icon-trash"></i>删除</a>';
                    }

                    return html;
                }
            }
        ],
        select: {
            //style: 'os',
            //selector: 'td:first-child'
            style: 'multi'
        },
        order: [[0, 'asc']],
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    var $roledatatables = $('#xquark_select_roles_tables').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get("/sellerpc/merchantRole/allList", {
                keyword: '',
                keyword: data.search.value,
                merchantId: merchantId,
                pageable: false
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    if (res.data.shopId) {
                        $shopId = res.data.shopId;
                    }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "15px",
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.roleName+'"></label>';
                    return str;
                }
            },
            {
                data: "roleName",
                width: "120px",
                orderable: false,
                name:"roleName"
            },{
                data: "roleDesc",
                width: "120px",
                orderable: false,
                name:"roleDesc"
            }
            , {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
            initEvent();
        }
    });

    function initEvent(){

        $(".edit").on("click",function(){
           var id =  $(this).attr("rowId")
            window.location.href="/sellerpc/merchant/admin/edit?merchantId="+id;
        });

        $(".settings").on("click",function(){
            merchantId =  $(this).attr("rowId");
            $roledatatables.search('').draw();
            $("#modal_select_roles").modal("show");

            // 全选
            $("#checkAllRoles").on('click', function() {
                $("[name=checkRoles]:checkbox").prop('checked', $(this).prop('checked'));
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'+rowId+'">确认</button>' +
                '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

         $('[data-toggle="popover"]').popover() //弹窗
             .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                 $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

             }).on('shown.bs.popover', function () {
                     var that = this;
                     $('.popover-btn-ok').on("click",function(){
                         var param = $(this).attr("param");
                         deleteMerchant(param);
                     });
                     $('.popover-btn-cancel').on("click",function(){
                         $(that).popover("hide");
                     });
            });
        //给Body加一个Click监听事件
        $('body').on('click', function(event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if(target.data("toggle") == "popover"){
                target.popover("toggle");
            }
        });

    }

    $(".btn-admin-search").on('click', function() {

        var keyword = $.trim($("#sKeyword").val());

        $datatables.search( keyword ).draw();

    });

    // 管理员选择角色确认
    $(".changeRoleBtn").on('click', function() {

        var roles = "";
        $("[name=checkRoles]:checkbox").each(function(){ //遍历table里的全部checkbox
            //如果被选中
            if($(this).prop("checked")){
                if(roles == ''){
                    roles = $(this).val();
                }else{
                    roles = roles + ',' + $(this).val();
                }
            }
        });


        var url = "/sellerpc/merchantRole/update";

        var data = {
            merchantId: merchantId,
            roles: roles
        }
        utils.postAjax(url,data,function(res){
            if(typeof(res) === 'object'){
                $("#modal_select_roles").modal("hide");
                if (res.rc == 1){
                    utils.tools.alert('设置成功',{timer: 1200, type: 'success'});
                    $datatables.search('').draw();
                } else{
                    utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                }
            }
        });
    });



    function  deleteMerchant(paramId){

          var url = "/sellerpc/merchant/admin/delete";

          var data = {
              merchantId: paramId
          }
          utils.postAjax(url,data,function(res){
              if(typeof(res) === 'object'){
                  if (res.rc == 1){
                      $datatables.search('').draw();
                  } else{
                      utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                  }
              }
          });
    }


});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
/**
 * Created by quguangming on 16/5/25.
 */
define('merchant/edit',['form/validate','utils','md5'], function(validate,utils,md5) {

    var $form = $(".merchant-edit-form");

    var $loginName = $("#loginName");

    var $id = $("#id").val();

    var checkNameUrl = "/sellerpc/pc/registered";

    var saveUrl = "/sellerpc/merchant/admin/save";

    var checkPhoneUrl = "/sellerpc/merchant/checkPhone";

    function saveMerchant(form){

        var data = $(form).serializeObject();

        /**if (data.orgId == null || data.orgId == 74) {
            utils.tools.alert('请选择部门');
            return;
        }
        console.log(data.orgId);**/

        // TODO wangxh 暂时不设置角色
        // var strRoles = "";
        //for(var i = 0; i < data.roles .length; i++){
        //   strRoles += data.roles [i] + ",";
        //}
        // data.roles = strRoles;
        if (data.password != '' && data.password.length > 0) {
            data.password = CryptoJS.MD5(data.password).toString()
        }

        utils.getJson(saveUrl, data, function (result) {
            console.log($id);
            if(!$id) {
                utils.tools.alert("创建成功!", {timer: 1200, type:'success'});
            } else{
                utils.tools.alert("修改成功!", {timer: 1200, type:'success'});
            }
          window.location.href = "/sellerpc/org/edit";
        });
    }

    var createForm = {

        rules:{
            loginname: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{1,16}$/,
                remote: {
                    url: checkNameUrl,
                    type: "Post",
                    data: {
                        loginName: function () { return $loginName.val(); }
                    }
                }
            },
            password: {
                required: true,
                pattern: /^[a-zA-Z0-9_]{6,16}$/
            },
            rePassword:{
                required: true,
                equalTo: "#password"
            },
            phone:{
                required: true,
                pattern: /^1\d{10}$/,
                remote: {
                    url: checkPhoneUrl,
                    type: "Post",
                    data: {
                        phone: function () {
                            var $phone = $("#phone");
                            return $phone.val();
                        }
                    }
                }
            },
            //"roles[]":{
            //    required: true
            //},
            email:{
                required: true,
                email: true
            }
        },
        messages:{
          loginname:{
                required:"账号不能为空.",
                pattern: "必须是 1~16位字母、数字和下划线的组合.",
                remote:  "该账号已注册,请重新输入."
            },
            password:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            rePassword:{
                required:"确认密码不能为空.",
                equalTo:  "密码不一致."
            },
            //"roles[]": {
            //    required: "至少选择一个角色."
            // },
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone:{
                required: "手机号不能为空.",
                pattern: "手机格式不正确.",
                remote: "该手机号已注册,请重新输入."
            }
        },
        focusCleanup: true,
        submitCallBack: function(form) {

            saveMerchant(form);
        }
    };


    var editForm = {

        rules:{
            password: {
                pattern: /^[a-zA-Z0-9_]{6,16}$/
            },
            rePassword:{
                equalTo: "#password"
            },
            phone:{
                required: true,
                pattern: /^1\d{10}$/,
            },
            //"roles[]":{
            //    required: true
            //},
            email:{
                required: true,
                email: true
            }
        },
        messages:{
            password:{
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            rePassword:{
                equalTo:  "密码不一致."
            },
            // "roles[]": {
            //    required: "至少选择一个角色."
            //},
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone:{
                required: "手机号不能为空.",
                pattern: "手机格式不正确."
            }
        },
        focusCleanup: true,
        submitCallBack: function(form) {
            saveMerchant(form);
        }
    }

    if ($id){
        validate($form,editForm);
    } else{
        validate($form,createForm);
    }

});
/**
 * Created by quguangming on 16/5/25.
 */
define('merchant/changepwd',['form/validate','utils','md5','jquerySerializeObject'], function(validate,utils,md5) {

    var $form = $(".change-pwd-form");

    var url = "/sellerpc/profile/changepwd"

    function submitForm(form){

        var data = $(form).serializeObject();

        data.oldpwd = CryptoJS.MD5(data.oldpwd).toString()
        data.newpwd = CryptoJS.MD5(data.newpwd).toString()

        //初始化商品分类信息
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data) {
                    utils.logout();
                    alert("密码修改成功,请重新登录!");
                    location.href = '/sellerpc/pc/login.html';
                } else {
                    utils.tools.alert("密码修改失败.请重新填写!");
                }
            },
            error: function (state) {
                utils.tools.alert("密码修改失败.请重新填写!");
            }
        });
    }

    var pwdForm = {
        rules:{
                oldpwd: {
                    required: true,
                    pattern: /^[a-zA-Z0-9_]{6,16}$/
                },
                newpwd: {
                    required: true,
                    pattern: /^[a-zA-Z0-9_]{6,16}$/
                },
                repwd:{
                    required: true,
                    equalTo: "#newpwd"
                }
        },
        messages:{
            oldpwd:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            newpwd:{
                required: "密码不能为空.",
                pattern:  "必须是 6~16位字母、数字和下划线的组合."
            },
            repwd:{
                required:"确认密码不能为空.",
                equalTo:  "密码不一致."
            },
        },
        focusCleanup: true,
        submitCallBack: function(form) {
            submitForm(form);
        }
    }
    validate($form,pwdForm);
});
/**
 * Created by quguangming on 16/5/25.
 */
define('merchant/profile',['form/validate', 'utils', 'dropzone', 'jquerySerializeObject'], function (validate, utils, upload) {

    var $form = $(".profile-form");

    var oldPhone = $("#phone").val();

    var saveUrl = "/sellerpc/profile/update";

    //图片上传
    // Defaults
    Dropzone.autoDiscover = false;

    var url = window.host + "/_f/u";

    // File limitations
    $(".avatar_dropzone").dropzone({
        url: url + "?belong=SHOP",      //指明了文件提交到哪个页面
        paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
        dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
        maxFilesize: 2, // MB      //最大文件大小，单位是 MB
        maxFiles: 1,               //限制最多文件数量
        maxThumbnailFilesize: 1,
        addRemoveLinks: true,
        thumbnailWidth: "150",      //设置缩略图的缩略比
        thumbnailHeight: "150",     //设置缩略图的缩略比
        acceptedFiles: ".gif,.png,.jpg",
        uploadMultiple: "false",
        dictInvalidFileType: "文件格式错误:建议文件格式: gif, png, jpg",//文件类型被拒绝时的提示文本。
        dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
        dictRemoveFile: "删除",                                     //移除文件链接的文本
        dictFallbackMessage: "您浏览器暂不支持该上传功能!",            //Fallback 情况下的提示文本
        dictResponseError: "服务器暂无响应,请稍后再试!",
        dictCancelUpload: "取消上传",
        dictCancelUploadConfirmation: "你确定要取消上传吗？",        //取消上传确认信息的文本
        dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",              //超过最大文件数量的提示文本。
        //autoProcessQueue: false,
        init: function () {

            var imgDropzone = this;
            this.on("success", function (file, data) {

                if (typeof(data) === 'object') {
                    switch (data.errorCode) {
                        case 200: {
                            if (typeof(data.data) === 'object') {
                                var imgId = data.data[0].id;
                                $("#avatar").val(imgId);
                            }
                            break;
                        }
                        default: {
                            utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                            break;
                        }
                    }
                } else {
                    if (data == -1) {
                        utils.tools.alert("图像上传失败,请重新选择!", {timer: 1200, type: 'warning'});
                    }
                }
                $(".btn-submit").removeClass("disabled");
            });
            this.on("error", function (file) {
                utils.tools.alert("文件上传失败!", {timer: 1200, type: 'warning'});
                $(".dz-error-message").html("文件上传失败!");
                $(".btn-submit").removeClass("disabled");
            });

            this.on("complete", function (file) {   //上传完成,在success之后执行
                $(".btn-submit").removeClass("disabled");
            });

            this.on("removedfile", function (file) {
                $("#avatar").val("");
            });


            this.on("thumbnail", function (file) {

            });


            //显示默认图片
            var imgId = $("#avatar").val();
            if (imgId != null && imgId.length > 0) {
                var imageUrl = $("#avatar").attr("src");
                var imgSize = $("#avatar").attr("img-size");
                var mockFile = {name: "", size: imgSize};
                imgDropzone.emit("addedfile", mockFile);
                imgDropzone.emit("thumbnail", mockFile, imageUrl);

            }

        }
    });


    function submitForm(form) {

        if ($(".btn-submit").hasClass("disabled")) return;

        var data = $(form).serializeObject();
        if (data.orgId == null || data.orgId == 74) {
            utils.tools.alert('请选择部门');
            return;
        }

        var imgId = $("#avatar").val();

        data.avatar = imgId;

        utils.getJson(saveUrl, data, function (result) {
            if (result) {
                utils.tools.alert("修改成功!", {timer: 1200, type: 'success'});
                //utils.logout();
            } else {
                utils.tools.alert("修改失败!", {timer: 1200, type: 'warning'});
            }
        });
    }

    var profileForm = {

        rules: {
            phone: {
                required: true,
                pattern: /^1\d{10}$/,
                remote: {
                    url: "/sellerpc/merchant/checkPhone",
                    type: "Post",
                    data: {
                        phone: function () {
                            var $phone = $("#phone");
                            return $phone.val() != oldPhone ? $phone.val() : 'phone';
                        }
                    }
                }
            },
            "roles[]": {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            "roles[]": {
                required: "至少选择一个角色."
            },
            email: {
                required: "邮件不能为空.",
                email: "邮件格式不正确."
            },
            phone: {
                required: "手机号不能为空.",
                pattern: "手机格式不正确.",
                remote: "该手机号已注册,请重新输入."
            }
        },
        focusCleanup: true,
        submitCallBack: function (form) {
            submitForm(form);
        }
    };
    //initInfo();

    function initInfo() {
        initOrgList();
        // initRolesList();
    }

    /** 初始化部门信息 **/
    function initOrgList() {
        var url = window.host + '/org/list';
        utils.postAjaxWithBlock($(document), url, null, function (res) {
            if (typeof(res) === 'object') {
                switch (res.errorCode) {
                    case 200: {
                        var orgTotal = res.data.orgTotal;
                        var select = $('#orgNames');
                        var selectedOption = select.find('option:selected');
                        if (orgTotal !== null && orgTotal !== 0) {
                            for (var i = 0; i < orgTotal; i++) {
                                select.append('<option value=' + res.data.list[i].id + '>' + res.data.list[i].name + '</option>');
                            }
                            //修改默认选中
                            var orgId = selectedOption.attr('value');
                            select.find("> option[value=" + orgId + "]").prop('selected', true)
                        } else {
                            selectedOption.text('请先创建部门');
                            $(".btn-submit").addClass("disabled");
                        }
                        break;
                    }
                    default: {
                        utils.tools.alert('获取部门信息失败！', {timer: 1200, type: 'warning'});
                        break;
                    }
                }
            } else if (res == 0) {

            } else if (res == -1) {
                utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
            }
        });
    }

    // function initRolesList() {
    //     var url = window.host + '/merchantRole/list';
    //     utils.postAjaxWithBlock($(document), url, {'keyword' : ''}, function (res) {
    //         if (typeof(res) === 'object') {
    //             switch (res.errorCode) {
    //                 case 200: {
    //                     var total = res.data.total;
    //                     var selectedOption = $('#roles').find('option:selected');
    //                     if (total !== null && total != 0) {
    //                         var list = res.data.list;
    //                         for (var i = 0; i < total; i++) {
    //                             $("#roles").append('<option value=' + list[i].roleName + '>' + list[i].roleDesc + '</option>');
    //                         }
    //                     }
    //                     break;
    //                 }
    //                 default: {
    //                     break;
    //                 }
    //             }
    //         } else if (res == 0) {
    //
    //         } else if (res == -1) {
    //             utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
    //         }
    //     });
    // }

    validate($form, profileForm);
});

/**
 * Created by quguangming on 16/5/24.
 */
require(['all']);

//module
require(['jquery','uniform','placeholder','merchant/list','merchant/edit','merchant/changepwd','merchant/profile'],function(){


});
define("merchant", function(){});

