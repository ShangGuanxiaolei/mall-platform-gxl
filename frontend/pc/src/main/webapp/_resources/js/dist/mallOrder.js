/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('mall/order',['jquery', 'utils', 'datatables', 'blockui', 'bootbox', 'select2',
      'uniform', 'daterangepicker', 'moment', 'fileinput_zh', 'fileinput',
      'jquerySerializeObject'],
    function ($, utils, datatabels, blockui, bootbox, select2, uniform,
              daterangepicker, moment, fileinput_zh, fileinput) {

      const NOT_FOUND = "/_resources/images/404.png";

      let orderId = "";//定义全局变量订单编号，供换货使用

      let orderIdForRefund = '';

      let orderNoForChange = '';//换货订单编号

      let updateStatusUrl = (id) => `${window.host}/order/refund/status/${id}`;//更新退款审核状态
      let updateWmsStatusUrl = (id) => `${window.host}/order/refund/warehouse/status/${id}`;//更新入库审核状态
      let updateCapableToRefundUrl = (id, capable) => `${window.host}/order/${id}/refund/capability?capableToRefund=${capable}`;
      let wmsForm = $("#wms-check-form");
      let wmsCheckModal = $("#modal_wms_check");

      //判断字符是否为空的方法
      function isEmpty(obj) {
        if (typeof obj == "undefined" || obj == null || obj == "") {
          return true;
        } else {
          return false;
        }
      }

      //下换货订单
      $("#btn-preOrder").on('click', function () {
        $.ajax({
          type: "POST",
          data: "", //JSON.stringify({"":""});
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: `${window.host}/order/change/confirm?orderId=` + orderId,
          success: function (data) {
            if (data != null) {
              utils.tools.alert('下单成功', {timer: 1200, type: 'success'});
              window.location.reload();//换货成功，刷新界面
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
            }
          }
        });
      });

      //获取供应商信息并建立缓存
      let sourceMap = new Map();
      $.ajax({
            url: `${window.host}/supplier/list?type=override&pageable=false`,
            async: false,
          }
      ).done(
          data => {
            let results = [];
            data.data.list.forEach(e => {
              sourceMap.set(e.id, e.name);
              let supplier = {
                id: e.id,
                text: e.name,
              };
              results.push(supplier);
              //初始化select2
              $("#supplierType").select2({
                minimumResultsForSearch: Infinity,
                data: results
              });
            });
          }
      ).fail(
        data => {}
      );

      /** 初始化日期控件 **/
      var options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'year').startOf('year'),
        endDate: moment().subtract(0, 'year').endOf('year'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var refund_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var change_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var reissue_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var success_options = {
        autoUpdateInput: false,
        startDate: moment().subtract(0, 'month').startOf('month'),
        endDate: moment().subtract(0, 'month').endOf('month'),
        timePicker: false,
        autoApply: false,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: '确定',
          fromLabel: '开始日期:',
          toLabel: '结束日期:',
          cancelLabel: '清空',
          weekLabel: 'W',
          customRangeLabel: '日期范围',
          daysOfWeek: ["一", "二", "三", "四", "五", "六", "日"],
          monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月",
            "10月", "11月", "12月"],
          firstDay: 6
        },
        ranges: {
          '今天': [moment(), moment()],
          '昨天': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          '最近7天': [moment().subtract(6, 'days'), moment()],
          '最近15天': [moment().subtract(15, 'days'), moment()],
          '本月': [moment().startOf('month'), moment().endOf('month')],
          '上月': [moment().subtract(1, 'month').startOf('month'),
            moment().subtract(1, 'month').endOf('month')],
        },
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small btn-default'
      };

      var $dateRangeBasic = $('.daterange-basic');
      $dateRangeBasic.daterangepicker(options, function (start, end) {
        if (start._isValid && end._isValid) {
          $dateRangeBasic.val(start.format('YYYY-MM-DD') + ' - ' + end.format(
              'YYYY-MM-DD'));
        } else {
          $dateRangeBasic.val('');
        }
      });

      var $refundDateRangeBasic = $('.refund_daterange-basic');
      $refundDateRangeBasic.daterangepicker(refund_options, function (start, end) {
        if (start._isValid && end._isValid) {
          $refundDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
              + end.format('YYYY-MM-DD'));
        } else {
          $refundDateRangeBasic.val('');
        }
      });

      var $changeDateRangeBasic = $('.change_daterange-basic');
      $changeDateRangeBasic.daterangepicker(change_options, function (start, end) {
        if (start._isValid && end._isValid) {
          $changeDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
              + end.format('YYYY-MM-DD'));
        } else {
          $changeDateRangeBasic.val('');
        }
      });

      var $reissueDateRangeBasic = $('.reissue_daterange-basic');
      $reissueDateRangeBasic.daterangepicker(reissue_options, function (start, end) {
        if (start._isValid && end._isValid) {
          $reissueDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
              + end.format('YYYY-MM-DD'));
        } else {
          $reissueDateRangeBasic.val('');
        }
      });

      var $successDateRangeBasic = $('.success_daterange-basic');
      $successDateRangeBasic.daterangepicker(success_options, function (start, end) {
        if (start._isValid && end._isValid) {
          $successDateRangeBasic.val(start.format('YYYY-MM-DD') + ' - '
              + end.format('YYYY-MM-DD'));
        } else {
          $successDateRangeBasic.val('');
        }
      });

      $("input[name='startDate']").val(options.startDate.format('YYYY-MM-DD'));
      $("input[name='endDate']").val(options.endDate.format('YYYY-MM-DD'));

      $("input[name='refund_startDate']").val(refund_options.startDate.format('YYYY-MM-DD'));
      $("input[name='refund_endDate']").val(refund_options.endDate.format('YYYY-MM-DD'));

      $("input[name='change_startDate']").val(change_options.startDate.format('YYYY-MM-DD'));
      $("input[name='change_endDate']").val(change_options.endDate.format('YYYY-MM-DD'));

      $("input[name='reissue_startDate']").val(reissue_options.startDate.format('YYYY-MM-DD'));
      $("input[name='reissue_endDate']").val(reissue_options.endDate.format('YYYY-MM-DD'));

      $("input[name='success_startDate']").val(success_options.startDate.format('YYYY-MM-DD'));
      $("input[name='success_endDate']").val(success_options.endDate.format('YYYY-MM-DD'));

      $dateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        options.startDate = picker.startDate;
        options.endDate = picker.endDate;
        $("input[name='startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      $refundDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        refund_options.startDate = picker.startDate;
        refund_options.endDate = picker.endDate;
        $("input[name='refund_startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='refund_endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      $changeDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        change_options.startDate = picker.startDate;
        change_options.endDate = picker.endDate;
        $("input[name='change_startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='change_endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      $reissueDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        reissue_options.startDate = picker.startDate;
        reissue_options.endDate = picker.endDate;
        $("input[name='reissue_startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='reissue_endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      $successDateRangeBasic.on('apply.daterangepicker', function (ev, picker) {
        success_options.startDate = picker.startDate;
        success_options.endDate = picker.endDate;
        $("input[name='success_startDate']").val(picker.startDate.format('YYYY-MM-DD'));
        $("input[name='success_endDate']").val(picker.endDate.format('YYYY-MM-DD'));
      });

      /**
       * 清空按钮清空选框
       */
      $dateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        $dateRangeBasic.val('');
      });

      /**
       * 清空按钮清空选框
       */
      $refundDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        $refundDateRangeBasic.val('');
      });

      /**
       * 清空按钮清空选框
       */
      $changeDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        $changeDateRangeBasic.val('');
      });

      /**
       * 清空按钮清空选框
       */
      $reissueDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        $reissueDateRangeBasic.val('');
      });

      /**
       * 清空按钮清空选框
       */
      $successDateRangeBasic.on('cancel.daterangepicker', function (ev, picker) {
        $successDateRangeBasic.val('');
      });

      $('#myTab a').click(function (e) {
        e.preventDefault();//阻止a链接的跳转行为
        $(this).tab('show');//显示当前选中的链接及关联的content
      });

      /** 初始化选择框控件 **/
      $('.select').select2({
        minimumResultsForSearch: Infinity,
      });

      // 所有订单
      var $datatables = utils.createDataTable('#xquark_list_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: false,
        searching: true,
        ordering: false,
        sortable: false,
        ajax: function (data, callback, settings) {
          $.get(window.host + "/merchant/order/list", {
            size: data.length,
            page: (data.start / data.length),
            orderNo: $('input[name="orderNo"]').val(),
            paymentMode: $('select[name="paymentMode"]').val(),
            status: $('select[name="orderStatus"]').val(),
            orderType: $('select[name="orderType"]').val(),
            grouponStatus: $('select[name="grouponStatus"]').val(),
            completeStartDate: $dateRangeBasic.val() !== '' && options.startDate
            !== '' ? options.startDate.format('YYYY-MM-DD') : '',
            completeEndDate: $dateRangeBasic.val() !== '' && options.endDate
            !== '' ? options.endDate.format('YYYY-MM-DD') : '',
            buyerName: $('input[name="buyerName"]').val(),
            sellerName: $('input[name="sellerName"]').val(),
            agentType: $('select[name="agentType"]').val(),
            pageable: true,
          }, function (res) {
            if (!res.data.list) {
              res.data.list = [];
            }
            callback({
              recordsTotal: res.data.orderTotal,
              recordsFiltered: res.data.orderTotal,
              data: res.data.list,
              iTotalRecords: res.data.orderTotal,
              iTotalDisplayRecords: res.data.orderTotal
            });
          });
        },
        rowId: "id",
        columns: [
          {
            width: "30px",
            orderable: false,
            render: function (data, type, row) {
              return '<a href="#"><img class="goods-image" src="' + row.imgUrl
                  + '" /></a>';
            }
          },
          {
            title: "订单",
            orderable: false,
            render: function (data, type, row) {
              return '订单编号: ' + row.orderNo + '<br/>'
                  + '支付单号: ' + row.payNo + '<br/>'
                  + '交易单号: ' + row.outTradeNo + '<br/>'
                  + row.orderItems[0].productName + '等';
            }
          },
          {
            title: "买家",
            orderable: false,
            render: function (data, type, row) {
              var userInfo = row.buyerName + '<br/>' + ((row.buyerPhone != null
                  && row.buyerPhone !== 'null')
                  ? row.buyerPhone : "");
              return userInfo;
            }
          }, {
            title: "下单时间",
            orderable: false,
            render: function (data, type, row) {
              var cDate = parseInt(row.createdAt);
              var d = new Date(cDate);
              return d.format('yyyy-MM-dd hh:mm:ss');
            },
          }
          , {
            title: "状态",
            orderable: false,
            render: function (data, type, row) {
              var status = "";
              var button = "";
              var grouponstatus = "";
              // 团购订单状态需要同时显示团购活动状态
              if (row.orderType == 'GROUPON') {
                switch (row.grouponStatus) {
                  case 'CREATED':
                    grouponstatus = '进行中';
                    break;
                  case 'REFUNDING':
                    grouponstatus = '退款申请中';
                    break;
                  case 'CHANGING':
                    grouponstatus = '换货申请中';
                    break;
                  case 'REISSUING':
                    grouponstatus = '补货申请中';
                    break;
                  case 'PAID':
                    grouponstatus = '待发货';
                    break;
                  case 'PAIDNOSTOCK':
                    grouponstatus = '未成团订单';
                    break;
                  case 'DELIVERY':
                    grouponstatus = '正在发货';
                    break;
                  case 'SHIPPED':
                    grouponstatus = '已发货';
                    break;
                  case 'SUCCESS':
                    grouponstatus = '已完成';
                    break;
                  case 'CLOSED':
                    grouponstatus = '未成团';
                    break;
                  default:
                    break;
                }
              }
              // 如果是退款申请状态，则订单状态显示退款申请前状态
              if (row.status === 'REFUNDING' || row.status === 'CHANGING' || row.status === 'REISSUING') {
                switch (row.refundStatus) {
                  case 'SUBMITTED':
                    status = '退款中';
                    break;
                  case 'SUCCESS':
                    status = '退款成功';
                    break;
                  case 'CANCELLED':
                    status = '退款已取消';
                    break;
                  case 'APPLY_FOR_REFUND':
                    status = "退款申请中";
                    break;
                  case 'APPLY_FOR_CHANGE':
                    status = "换货申请中";
                    break;
                  case 'APPLY_FOR_REISSUE':
                    status = "补货申请中";
                    break;
                  case 'ACCEPT_REFUND':
                    status = "退款申请通过";
                    break;
                  case 'ACCEPT_REISSUE':
                    status = "补货申请通过";
                    break;
                  case 'ACCEPT_CHANGE':
                    status = "换货申请通过";
                    break;
                  default:
                    status = "退款中";
                    break;
                }
              } else {
                switch (row.status) {
                  case 'SUBMITTED':
                    if (row.orderType === 'PURCHASE') {
                      status = '待审核';
                    } else {
                      status = '待付款';
                    }
                    break;
                  case 'CANCELLED':
                    status = '已取消';
                    break;
                  case 'PAID':
                    status = '待发货';
                    if ((row.orderType === 'GROUPON' && row.grouponStatus
                        === 'PAID') || row.orderType !== 'GROUPON') {
                      button = '</br><a class="btn btn-primary btn-sm shipBtn" isPickup="'
                          + row.isPickup + '" order-id="' + row.id
                          + '" fid="delivery_order"> 发货</a>';
                    }
                    break;
                  case 'SHIPPED':
                    status = '已发货';
                    if (row.isPickup !== 1) {
                      button = `</br> 物流公司:${row.logisticsCompany
                          ? row.logisticsCompany
                          : ''}</br> 物流单号:${row.logisticsOrderNo
                          ? row.logisticsOrderNo : ''}`;
                    }
                    break;
                  case 'SUCCESS':
                    status = '已完成';
                    break;
                  case 'REFUNDING':
                    status = '退款中';
                    break;
                  case 'CLOSED':
                    status = '已关闭';
                    break;
                  default:
                    break;
                }
              }
              var html = status;
              if (grouponstatus !== '') {
                html = html + '-' + grouponstatus;
              }
              html = html + button;
              return html;
            }
          }, {
            title: "数量",
            data: "amount",
            name: "amount",
            orderable: false,
          }, {
            title: "价格",
            data: "price",
            name: "price",
            orderable: false,
          }, {
            title: "金额",
            data: "totalFee",
            name: "totalFee",
            orderable: false,
          }, {
            title: "收货人姓名",
            orderable: false,
            render: function (data, type, row) {
              var userInfo = "";
              if (row.isPickup !== 1) {
                userInfo = row.orderAddress.consignee;
              }
              return userInfo;
            }
          }, {
            title: "收货人电话",
            orderable: false,
            render: function (data, type, row) {
              var userInfo = "";
              if (row.isPickup !== 1) {
                userInfo = row.orderAddress.phone;
              }
              return userInfo;
            }
          }
          , {
            title: "类别",
            orderable: false,
            render: function (data, type, row) {
              const orderType = row.orderType;
              switch (orderType) {
                case 'INSIDE_BUY':
                  return '内购订单';
                case 'COUPON':
                  return '优惠券';
                case 'FULLCUT':
                  return '会员活动';
                case 'EXCHANGE':
                  return '换货订单';
                case 'PIECE':
                  return '拼团订单';
                case 'VIP_RIGHT':
                  return '特权订单';
                case 'FLASHSALE':
                  return '秒杀订单';
                default:
                  return '普通订单';
              }
            }
          }
          , {
            orderable: false,
            width: "100px",
            title: "管理",
            render: function (data, type, row) {
              var html = '';
              html = html
                  + '<a class="viewDetailBtn role_check_table" href="#" style="margin-right: 10px;" order-no="'
                  + row.orderNo + '" > 查看订单详情</a>';
              if (row.status === 'SUBMITTED') {
                html = html
                    + '<a class="cancelBtn role_check_table" href="javascript:void(0)" style="margin-right: 10px;" order-id="'
                    + row.id + '" >取消订单</a>';
              }
              if ((row.status === 'SHIPPED' || row.status === 'SUCCESS')
                  && !row.capableToRefund && -1 === row.dest.indexOf(
                      "NOT_REFUNDABLE")) {
                html = html
                    + '<a class="openRefundEntryBtn role_check_table" href="javascript:void(0)" style="margin-right: 10px;" order-id="'
                    + row.id + '" >开启售后</a>';
              }
              if ((row.status === 'SHIPPED' || row.status === 'SUCCESS')) {
                html = html
                    + '<br/><a class="viewApplyBtn role_check_table" id="applyAfterSale" href="javascript:void(0)" data-toggle="modal" data-target="#myModal" style="margin-right: 10px; color: red" order-no="'
                    + row.orderNo + '" order-id="'+ row.id +'" >申请售后</a>';
              }
              return html;
            }
          }
        ],
        select: {
          style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
          initEvent();
        }
      });

      //后台退货
      $("#refundGoodsBtn").off("click").on("click", function () {
        var orderId = orderIdForRefund;//获取orderId
        var selected = document.getElementById("refundReason");//获取select对象
        var index = selected.selectedIndex;//获取选中项的索引
        var refundReason = selected.options[index].value;//获取选中项的value
        $.ajax({
          type: "POST",
          data: JSON.stringify({
            "orderId": orderId,
            "refundReason": refundReason,
            "refundMemo": "客服帮用户退货订单"
          }),
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: `${window.host}/order/refund/apply`,
          success: function (data) {
            if (data != null) {
              utils.tools.alert('操作成功', {timer: 1200, type: 'success'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
            }
          }
        });
      });

      //后台换货
      $("#changeBtnForAdmin").off("click").on("click", function () {
        var $changeGoodsTables;
        var orderNo = orderNoForChange;
        if ($changeGoodsTables !== undefined) {
          $changeGoodsTables.search('').draw();
          return;
        }
        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
          autoWidth: false,
          dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
          language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {
              'first': '首页',
              'last': '末页',
              'next': '&rarr;',
              'previous': '&larr;'
            },
            infoEmpty: "",
            emptyTable: "暂无相关数据"
          }
        });
        $changeGoodsTables = $('#changeGoodsInfo1').DataTable({
          paging: true, //是否分页
          filter: false, //是否显示过滤
          lengthChange: false,
          processing: true,
          serverSide: true,
          deferRender: true,
          searching: false,
          destroy: true,
          ordering: false,
          ajax: function (data, callback, settings) {
            $.get(window.host + "/twitter/order/orderItem", {
              size: data.length,
              page: (data.start / data.length),
              orderNo: orderNo,
              pageable: true,
            }, function (res) {
              if (res.errorCode != '200') {
                return;
              }
              if (!res.data.list) {
                res.data.list = [];
              }
              callback({
                recordsTotal: res.data.itemTotal,
                recordsFiltered: res.data.itemTotal,
                data: res.data.list,
                iTotalRecords: res.data.itemTotal,
                iTotalDisplayRecords: res.data.itemTotal
              });
            });
          },
          columns: [
            {
              data: 'product.id',
              title: '',
              width: "10px",
              orderable: false,
              render: function (data) {
                return '<label class="checkbox"><input id="checkGoods" name="checkGoods" type="checkbox" class="styled" rowid="'
                    + data + '" value="' + data + '"></label>';
              }
            },
            {
              data: 'product.imgUrl',
              name: 'product.imgUrl',
              orderable: false,
              title: "商品图片",
              render: function (data) {
                return data
                    ? `<img class="goods-image" src="${data}" />`
                    : `<img class="goods-image" src="${NOT_FOUND}" />`;
              }
            },
            {
              data: 'product.name',
              name: 'product.name',
              orderable: false,
              title: "商品名称"
            },
            {
              title: "数量",
              data: "amount",
              name: "amount",
              orderable: false,
              render: function (data) {
                return '<div class="input-group"><input type="text" class="form-control pull-left" name="netWorth"\n' +
                    'id="changeGoodsNum" placeholder="请输入数量"/>' +
                    '<span class="input-group-addon product-length-unit">数量</span>' +
                    '<input type="hidden" name="changeGoodsAmount1" value="' + data + '"/></div>';
              }
            },
            {
              data: 'product.status',
              name: 'product.status',
              orderable: false,
              title: "商品状态",
              render: function (data) {
                var status = '';
                switch (data) {
                  case 'INSTOCK':
                    status = '下架';
                    break;
                  case 'ONSALE':
                    status = '已上架';
                    break;
                  case 'FORSALE':
                    status = '待上架发布';
                    break;
                  case 'DRAFT':
                    status = '草稿';
                    break;
                  default:
                    break;
                }
                return status;
              }
            },
            {
              data: 'product.supplierId',
              name: 'product.supplierId',
              orderable: false,
              title: "供应商",
              render: function (data) {
                const id = data;
                if (id && id !== '') {
                  let name;
                  if (name = sourceMap.get(id)) {
                    return name;
                  }
                }
                return '无';
              }
            },
            {
              data: 'product.marketPrice',
              name: 'product.marketPrice',
              orderable: false,
              title: "原价"
            },
            {
              data: 'product.price',
              name: 'product.price',
              orderable: false,
              title: "优惠价"
            },
            {
              data: 'product.amount',
              name: 'product.amount',
              orderable: false,
              title: "库存"
            },
            {
              data: 'orderId',
              orderable: false,
              title: '',
              render: function (data) {
                return '<input id="changeOrderId" name="changeOrderId" type="hidden" rowid="'
                    + data + '" value="' + data + '"/>';
              }
            }
          ],
          drawCallback: function () {  //数据加载完成
          }
        });
      });

      //后台申请换货
      $("#applyChangeForAdmin").off("click").on("click", function () {
        let $changeGoodsTables;
        let orderNo = orderNoForChange;
        if ($changeGoodsTables !== undefined) {
          $changeGoodsTables.search('').draw();
          return;
        }
        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
          autoWidth: false,
          dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
          language: {
            search: '<span>筛选:</span> _INPUT_',
            lengthMenu: '<span>显示:</span> _MENU_',
            info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
            paginate: {
              'first': '首页',
              'last': '末页',
              'next': '&rarr;',
              'previous': '&larr;'
            },
            infoEmpty: "",
            emptyTable: "暂无相关数据"
          }
        });
        $changeGoodsTables = $('#changeGoodsInfo2').DataTable({
          paging: true, //是否分页
          filter: false, //是否显示过滤
          lengthChange: false,
          processing: true,
          serverSide: true,
          deferRender: true,
          searching: false,
          destroy: true,
          ordering: false,
          ajax: function (data, callback, settings) {
            $.get(window.host + "/twitter/order/orderItem", {
              size: data.length,
              page: (data.start / data.length),
              orderNo: orderNo,
              pageable: true,
            }, function (res) {
              if (res.errorCode != '200') {
                return;
              }
              if (!res.data.list) {
                res.data.list = [];
              }
              callback({
                recordsTotal: res.data.itemTotal,
                recordsFiltered: res.data.itemTotal,
                data: res.data.list,
                iTotalRecords: res.data.itemTotal,
                iTotalDisplayRecords: res.data.itemTotal
              });
            });
          },
          columns: [
            {
              data: 'id',
              title: '',
              width: "10px",
              orderable: false,
              render: function (data) {
                return '<label class="checkbox"><input id="checkGoods2" name="checkGoods2" type="checkbox" class="styled" rowid="'
                    + data + '" value="' + data + '"></label>';
              }
            },
            {
              data: 'product.imgUrl',
              name: 'product.imgUrl',
              orderable: false,
              title: "商品图片",
              render: function (data) {
                return data
                    ? `<img class="goods-image" src="${data}" />`
                    : `<img class="goods-image" src="${NOT_FOUND}" />`;
              }
            },
            {
              data: 'product.name',
              name: 'product.name',
              orderable: false,
              title: "商品名称"
            },
            {
              title: "数量",
              data: "amount",
              name: "amount",
              orderable: false,
              render: function (data) {
                return '<div class="input-group"><input type="text" class="form-control pull-left" name="netWorth2"\n' +
                    'id="changeGoodsNum2" placeholder="请输入数量"/>' +
                    '<span class="input-group-addon product-length-unit">数量</span>' +
                    '<input type="hidden" name="changeGoodsAmount" value="' + data + '"/></div>';
              }
            },
            {
              data: 'product.status',
              name: 'product.status',
              orderable: false,
              title: "商品状态",
              render: function (data) {
                var status = '';
                switch (data) {
                  case 'INSTOCK':
                    status = '下架';
                    break;
                  case 'ONSALE':
                    status = '已上架';
                    break;
                  case 'FORSALE':
                    status = '待上架发布';
                    break;
                  case 'DRAFT':
                    status = '草稿';
                    break;
                  default:
                    break;
                }
                return status;
              }
            },
            {
              data: 'product.supplierId',
              name: 'product.supplierId',
              orderable: false,
              title: "供应商",
              render: function (data) {
                const id = data;
                if (id && id !== '') {
                  let name;
                  if (name = sourceMap.get(id)) {
                    return name;
                  }
                }
                return '无';
              }
            },
            {
              data: 'product.marketPrice',
              name: 'product.marketPrice',
              orderable: false,
              title: "原价"
            },
            {
              data: 'product.price',
              name: 'product.price',
              orderable: false,
              title: "优惠价"
            },
            {
              data: 'product.amount',
              name: 'product.amount',
              orderable: false,
              title: "库存"
            },
            {
              data: 'orderId',
              orderable: false,
              title: '',
              render: function (data) {
                return '<input id="changeOrderId" name="changeOrderId" type="hidden" rowid="'
                    + data + '" value="' + data + '"/>';
              }
            }
          ],
          drawCallback: function () {  //数据加载完成
          }
        });
      });

      function showProductId(){
        obj = document.getElementsByName("checkGoods");
        amounts = document.getElementsByName("netWorth");
        let changeGoodsAmount1 = document.getElementsByName("changeGoodsAmount1");
        if(isEmpty(amounts)){
          alert("请输入换货商品数量！");
          return;
        }
        check_val = [];
        for(k in obj){
          if(obj[k].checked) {
            check_val.push(obj[k].value + ',' + amounts[k].value);
            if(isEmpty(amounts[k].value)){
              alert("请输入选择换货商品的数量！");
              return;
            }
            if(amounts[k].value > changeGoodsAmount1[k].value){
              alert("不能大于订单商品数量！");
              return;
            }
            if(amounts[k].value <= 0){
              alert("输入的商品数量不能小于等于0");
              return;
            }
          }
        }
        return check_val;
      }

      //提交换货订单
      $("#submitChangeGoods").off("click").on("click", function () {
        var productIds = showProductId();
        var orderId = $("#changeOrderId").val();
        if(isEmpty(productIds)){
          alert("请选择换货商品！");
          return;
        }
        $.ajax({
          type: "POST",
          data: "", //JSON.stringify({"":""});
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: `${window.host}/order/change/submit?orderId=` + orderId + '&productIdAndAmount=' + productIds,
          success: function (data) {
            if (data != null) {
              utils.tools.alert('下单成功', {timer: 1200, type: 'success'});
              window.location.reload();//换货成功，刷新界面
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
            }
          }
        });
      });

      function showProductId2(){
        obj = document.getElementsByName("checkGoods2");
        amounts = document.getElementsByName("netWorth2");
        let changeGoodsAmount = document.getElementsByName("changeGoodsAmount");
        if(isEmpty(amounts)){
          alert("请输入换货商品数量！");
          return;
        }
        check_val = [];
        for(k in obj){
          if(obj[k].checked) {
            check_val.push(obj[k].value + ',' + amounts[k].value);
            if(isEmpty(amounts[k].value)){
              alert("请输入选择换货商品的数量！");
              return;
            }
            if(amounts[k].value > changeGoodsAmount[k].value){
              alert("不能大于订单商品数量！");
              return;
            }
            if(amounts[k].value <= 0){
              alert("输入的商品数量不能小于等于0");
              return;
            }
          }
        }
        return check_val;
      }

      //后台提交换货申请
      $("#applyChangeBtn").off("click").on("click", function () {
        let orderItems = showProductId2();
        let orderId = $("#changeOrderId").val();
        let changeReason = $("#changeReason").val();
        if(isEmpty(orderItems)){
          alert("请选择换货商品！");
          return;
        }
        $.ajax({
          type: "POST",
          data: "",
          dataType: "JSON",
          async: false,
          contentType: "application/json",
          url: `${window.host}/order/change/adminApply?orderId=` + orderId + '&changeReason=' + changeReason
              + '&orderItems=' + orderItems,
          success: function (data) {
            if (data != null) {
              utils.tools.alert('申请换货成功', {timer: 1200, type: 'success'});
              window.location.reload();//换货成功，刷新界面
            } else {
              utils.tools.alert('操作失败', {timer: 1200, type: 'warning'});
            }
          }
        });
      });

      var $refund_datatables;

      // 申请退款订单
      $("#refundOrderTab").on("click", function () {
        if ($refund_datatables != undefined) {
          return;
        }
        $refund_datatables = utils.createDataTable(
          '#xquark_refund_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            destroy: true,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/merchant/order/list", {
                size: data.length,
                page: (data.start / data.length),
                productName: $('input[name="refund_productName"]').val(), //输入的商品名称
                orderNo: $('input[name="refund_orderNo"]').val(), //输入的订单编号
                paymentMode: $('select[name="refund_paymentMode"]').val(), //支付方式
                status: 'REFUNDING', //订单状态
                buyerRequire: '1', //买家请求（1代表仅退款，2代表退款退货，3表示换货）
                orderType: $('select[name="refund_orderType"]').val(), //订单类型
                completeStartDate: $refundDateRangeBasic.val() !== ''
                && refund_options.startDate !== ''
                    ? refund_options.startDate.format('YYYY-MM-DD') : '', //下单开始时间
                completeEndDate: $refundDateRangeBasic.val() !== ''
                && refund_options.endDate !== ''
                    ? refund_options.endDate.format('YYYY-MM-DD') : '', //下单结束时间
                pageable: true
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else if (res.data.orderTotal > 0) {
                  $("#refundOrderCountDiv").css("display", "inline-block");
                  $("#refundOrderCount").html(res.data.orderTotal);
                } else if (res.data.orderTotal == 0) {
                  $("#refundOrderCountDiv").hide();
                }
                callback({
                  recordsTotal: res.data.orderTotal,
                  recordsFiltered: res.data.orderTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.orderTotal,
                  iTotalDisplayRecords: res.data.orderTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="#"><img class="goods-image" src="'
                      + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                title: "订单",
                orderable: false,
                render: function (data, type, row) {
                  return '订单编号: ' + row.orderNo + '<br/>'
                      + '支付单号: ' + row.payNo + '<br/>'
                      + '交易单号: ' + row.outTradeNo + '<br/>'
                      + row.orderItems[0].productName + '等';
                }
              },
              {
                title: "买家",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup == 1) {
                    userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                    != null ? row.buyerPhone : "");
                  } else {
                    userInfo = row.orderAddress.consignee + '<br/>'
                        + (row.buyerPhone != null ? row.buyerPhone : "");
                  }
                  return userInfo;
                }
              }, {
                title: "下单时间",
                orderable: false,
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
              }, {
                title: "订单类型",
                orderable: false,
                render: function (data, type, row) {
                  var status = "";
                  // 订单类型
                  switch (row.orderType) {
                    case 'NORMAL':
                      status = '普通';
                      break;
                    case 'INSIDE_BUY':
                      status = '内购';
                      break;
                    case 'COUPON':
                      status = '优惠卷';
                      break;
                    case 'GROUPON':
                      status = '团购';
                      break;
                    case 'BARGAIN':
                      status = '砍价';
                      break;
                    case 'YUNDOU':
                      status = '积分';
                      break;
                    case 'FLASHSALE':
                      status = '限量秒杀';
                      break;
                    default:
                      break;
                  }
                  if (row.isPickup == 1) {
                    status = status + "【自提】";
                  }
                  var html = status;
                  return html;
                }
              }, {
                title: "订单状态",
                width: "80px",
                orderable: false,
                render: function (data, type, row) {
                  let status;
                  switch (row.refundStatus) {
                    case 'SUBMITTED':
                      status = '物流信息已提交</br>等待审核';
                      break;
                    case 'SUCCESS':
                      status = '退款成功';
                      break;
                    case 'CANCELLED':
                      status = '退款已取消';
                      break;
                    case 'APPLY_FOR_REFUND':
                      status = "退款申请中";
                      break;
                    case 'REJECT_REFUND':
                      status = "退款申请被拒绝</br>原因：";
                      break;
                    case 'ACCEPT_REFUND':
                      status = '退款申请通过</br>等待提交物流信息';
                      break;
                    case 'WMS_CHECKED':
                      status = 'WMS审核结束';
                      break;
                    default:
                      status = '';
                  }
                  return status;
                }
              }, {
                title: "实付金额",
                data: "totalFee",
                name: "totalFee",
                orderable: false,
              }, {
                title: "运费",
                data: "logisticsFee",
                name: "logisticsFee",
                orderable: false,
              }, {
                title: "退款原因",
                data: "refundReason",
                name: "refundReason",
                orderable: false,
              }, {
                title: "退款说明",
                data: "refundMemo",
                name: "refundMemo",
                orderable: false,
              },
              {
                title: "退款凭证",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  for (var i = 0; i < row.refundImgList.length; i++) {
                    html = html + '<a href="' + row.refundImgList[i].imageUrl
                        + '" target="_blank"><img class="goods-image" src="'
                        + row.refundImgList[i].imageUrl + '" /></a>'
                  }
                  return html;
                }
              },
              {
                title: "物流信息",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                      === 'WMS_CHECKED') {
                    let c = row.refundLogisticsCompany || '';
                    let no = row.refundLogisticsNo || '';
                    let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                    return logistics;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "WMS审核状态",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED') {
                    return "等待WMS审核";
                  }
                  if (row.refundStatus === 'WMS_CHECKED') {
                    let s = row.wmsCheckStatus || '';
                    let mark = row.wmsCheckRemark || '';
                    let time = row.wmsCheckTime || '';
                    let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常' : '入库异常'}
                                   </br> 备注: ${mark}
                                   </br>审核时间:${new Date(time).toLocaleString()}`;
                    return content;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "操作",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                  let html = '';
                  if (row.refundStatus === 'APPLY_FOR_REFUND') {
                    html += `<a class="passRefundApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >通过退款申请</a>`;
                    html += `<a class="failRefundApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >拒绝退款申请</a>`;
                  }
                  if (row.refundStatus === 'SUBMITTED') {
                    html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a>`;
                  }
                  if (row.refundStatus === 'WMS_CHECKED') {
                    html += '<a class="refundBtn" href="javascript:void(0)"  style="margin-right: 10px;" orderStatus="'
                        + row.orderStatus + '" totalFee="' + row.totalFee
                        + '" logisticsFee="' + row.logisticsFee + '" rowId="'
                        + row.id + '"> 退款</a>';
                  }
                  html = html
                      + '<a class="cancelRefundBtn role_check_table" href="javascript:void(0)" style="margin-right: 10px;" order-id="'
                      + row.id + '" > 取消退款</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              initEvent();
            }
          });
      });

      var $change_datatables; //定义换货订单列表tab

      // 申请换货订单
      $("#changeOrderTab").on("click", function () {
        if ($change_datatables != undefined) {
          return;
        }
        $change_datatables = utils.createDataTable(
          '#xquark_change_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            destroy: true,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/merchant/order/list", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $('input[name="change_orderNo"]').val(),
                paymentMode: $('select[name="change_paymentMode"]').val(),
                refundStatus: $('select[name="orderRefundStatus"]').val(),
                status: 'CHANGING',//状态为换货中
                buyerRequire: '3', //查询出换货的订单
                orderType: $('select[name="change_orderType"]').val(),
                completeStartDate: $changeDateRangeBasic.val() !== ''
                && change_options.startDate !== ''
                    ? change_options.startDate.format('YYYY-MM-DD') : '',
                completeEndDate: $changeDateRangeBasic.val() !== ''
                && change_options.endDate !== ''
                    ? change_options.endDate.format('YYYY-MM-DD') : '',
                pageable: true
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else if (res.data.orderTotal > 0) {
                  $("#changeOrderCountDiv").css("display", "inline-block");
                  $("#changeOrderCount").html(res.data.orderTotal);
                } else if (res.data.orderTotal == 0) {
                  $("#changeOrderCountDiv").hide();
                }
                callback({
                  recordsTotal: res.data.orderTotal,
                  recordsFiltered: res.data.orderTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.orderTotal,
                  iTotalDisplayRecords: res.data.orderTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="#"><img class="goods-image" src="'
                      + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                title: "订单",
                orderable: false,
                render: function (data, type, row) {
                  return '订单编号: ' + row.orderNo + '<br/>'
                      + '支付单号: ' + row.payNo + '<br/>'
                      + '交易单号: ' + row.outTradeNo + '<br/>'
                      + row.orderItems[0].productName + '等';
                }
              },
              {
                title: "买家",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup == 1) {
                    userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                    != null ? row.buyerPhone : "");
                  } else {
                    userInfo = row.orderAddress.consignee + '<br/>'
                        + (row.buyerPhone != null ? row.buyerPhone : "");
                  }
                  return userInfo;
                }
              },
              {
                title: "下单时间",
                orderable: false,
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                }
              },
              {
                title: "订单类型",
                orderable: false,
                render: function (data, type, row) {
                  var status = "";
                  // 订单类型
                  switch (row.orderType) {
                    case 'NORMAL':
                      status = '普通';
                      break;
                    case 'INSIDE_BUY':
                      status = '内购';
                      break;
                    case 'COUPON':
                      status = '优惠券';
                      break;
                    case 'GROUPON':
                      status = '团购';
                      break;
                    case 'BARGAIN':
                      status = '砍价';
                      break;
                    case 'YUNDOU':
                      status = '积分';
                      break;
                    case 'FLASHSALE':
                      status = '限量秒杀';
                      break;
                    default:
                      break;
                  }
                  if (row.isPickup == 1) {
                    status = status + "【自提】";
                  }
                  var html = status;
                  return html;
                }
              },
              {
                title: "订单状态",
                width: "80px",
                orderable: false,
                render: function (data, type, row) {
                  let status;
                  switch (row.refundStatus) {
                    case 'SUBMITTED':
                      status = '物流信息已提交</br>等待审核';
                      break;
                    case 'SUCCESS':
                      status = '换货成功';
                      break;
                    case 'CANCELLED':
                      status = '换货已取消';
                      break;
                    case 'APPLY_FOR_CHANGE':
                      status = "换货申请中";
                      break;
                    case 'REJECT_CHANGE':
                      status = "换货申请被拒绝</br>原因：";
                      break;
                    case 'ACCEPT_CHANGE':
                      status = '换货申请通过</br>等待提交物流信息';
                      break;
                    case 'WMS_CHECKED':
                      status = 'WMS审核结束';
                      break;
                    default:
                      status = '';
                  }
                  return status;
                }
              }, {
                title: "实付金额",
                data: "totalFee",
                name: "totalFee",
                orderable: false,
              }, {
                title: "运费",
                data: "logisticsFee",
                name: "logisticsFee",
                orderable: false,
              }, {
                title: "换货原因",
                data: "refundReason",
                name: "refundReason",
                orderable: false,
              }, {
                title: "换货说明",
                data: "refundMemo",
                name: "refundMemo",
                orderable: false,
              },
              {
                title: "换货凭证",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  if(row.refundImgList != null){
                    for (var i = 0; i < row.refundImgList.length; i++) {
                      html = html + '<a href="' + row.refundImgList[i].imageUrl
                          + '" target="_blank"><img class="goods-image" src="'
                          + row.refundImgList[i].imageUrl + '" /></a>'
                    }
                  }
                  return html;
                }
              },
              {
                title: "物流信息",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                      === 'WMS_CHECKED') {
                    let c = row.refundLogisticsCompany || '';
                    let no = row.refundLogisticsNo || '';
                    let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                    return logistics;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "WMS审核状态",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED') {
                    return "等待WMS审核";
                  }
                  if (row.refundStatus === 'WMS_CHECKED') {
                    let s = row.wmsCheckStatus || '';
                    let mark = row.wmsCheckRemark || '';
                    let time = row.wmsCheckTime || '';
                    let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常' : '入库异常'}
                                   </br> 备注: ${mark}
                                   </br>审核时间:${new Date(time).toLocaleString()} `;
                    return content;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "操作",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                  let html = '';
                  if (row.refundStatus === 'APPLY_FOR_CHANGE') {
                    html += `<a class="passChangeApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >通过换货申请</a>`;
                    html += `<a class="failChangeApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >拒绝换货申请</a><br/>`;
                  }
                  if (row.refundStatus === 'SUBMITTED') {
                    html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a><br/>`;
                  }
                  if (row.refundStatus === 'WMS_CHECKED' || row.wmsCheckStatus
                      === 'CHECKED_PASS') {
                    html += '<a class="changeBtn" href="javascript:void(0)"  style="margin-right: 10px;" orderStatus="'
                        + row.orderStatus + '" order-no="' + row.orderNo
                        + '" order-id="' + row.id + '"> 换货出仓</a><br/>';
                  }
                  html = html
                      + '<a class="cancelChangeBtn role_check_table" href="javascript:void(0)" style="margin-right: 10px;" order-id="'
                      + row.id + '" > 取消换货</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                      + '<a class="viewDetailBtnChange role_check_table" href="#" style="margin-right: 10px;" order-id="' + row.id + '" order-no="'
                      + row.orderNo + '" > 查看换货商品详情</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              initEvent();
            }
          });
      });

      var $reissue_datatables;//定义补货tab

      // 申请补货订单
      $("#reissueOrderTab").on("click", function () {
        if ($reissue_datatables != undefined) {
          return;
        }
        $reissue_datatables = utils.createDataTable(
          '#xquark_reissue_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            destroy: true,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/merchant/order/list", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $('input[name="reissue_orderNo"]').val(),
                paymentMode: $('select[name="reissue_paymentMode"]').val(),
                refundStatus: $('select[name="orderRefundStatus"]').val(),
                status: 'REISSUING',//状态为补货中
                buyerRequire: '4', //查询出补货的订单
                orderType: $('select[name="reissue_orderType"]').val(),
                completeStartDate: $reissueDateRangeBasic.val() !== ''
                && reissue_options.startDate !== ''
                    ? reissue_options.startDate.format('YYYY-MM-DD') : '',
                completeEndDate: $reissueDateRangeBasic.val() !== ''
                && reissue_options.endDate !== ''
                    ? reissue_options.endDate.format('YYYY-MM-DD') : '',
                pageable: true
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else if (res.data.orderTotal > 0) {
                  $("#reissueOrderCountDiv").css("display", "inline-block");
                  $("#reissueOrderCount").html(res.data.orderTotal);
                } else if (res.data.orderTotal == 0) {
                  $("#reissueOrderCountDiv").hide();
                }
                callback({
                  recordsTotal: res.data.orderTotal,
                  recordsFiltered: res.data.orderTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.orderTotal,
                  iTotalDisplayRecords: res.data.orderTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="#"><img class="goods-image" src="'
                      + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                title: "订单",
                orderable: false,
                render: function (data, type, row) {
                  return '订单编号: ' + row.orderNo + '<br/>'
                      + '支付单号: ' + row.payNo + '<br/>'
                      + '交易单号: ' + row.outTradeNo + '<br/>'
                      + row.orderItems[0].productName + '等';
                }
              },
              {
                title: "买家",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup == 1) {
                    userInfo = row.buyerName + '<br/>' + (row.buyerPhone
                    != null ? row.buyerPhone : "");
                  } else {
                    userInfo = row.orderAddress.consignee + '<br/>'
                        + (row.buyerPhone != null ? row.buyerPhone : "");
                  }
                  return userInfo;
                }
              },
              {
                title: "下单时间",
                orderable: false,
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                }
              },
              {
                title: "订单类型",
                orderable: false,
                render: function (data, type, row) {
                  var status = "";
                  // 订单类型
                  switch (row.orderType) {
                    case 'NORMAL':
                      status = '普通';
                      break;
                    case 'INSIDE_BUY':
                      status = '内购';
                      break;
                    case 'COUPON':
                      status = '优惠券';
                      break;
                    case 'GROUPON':
                      status = '团购';
                      break;
                    case 'BARGAIN':
                      status = '砍价';
                      break;
                    case 'YUNDOU':
                      status = '积分';
                      break;
                    case 'FLASHSALE':
                      status = '限量秒杀';
                      break;
                    default:
                      break;
                  }
                  if (row.isPickup == 1) {
                    status = status + "【自提】";
                  }
                  var html = status;
                  return html;
                }
              },
              {
                title: "订单状态",
                width: "80px",
                orderable: false,
                render: function (data, type, row) {
                  let status;
                  switch (row.refundStatus) {
                    case 'SUBMITTED':
                      status = '物流信息已提交</br>等待审核';
                      break;
                    case 'SUCCESS':
                      status = '补货成功';
                      break;
                    case 'CANCELLED':
                      status = '补货已取消';
                      break;
                    case 'APPLY_FOR_REISSUE':
                      status = "补货申请中";
                      break;
                    case 'REJECT_REISSUE':
                      status = "补货申请被拒绝</br>原因：";
                      break;
                    case 'ACCEPT_REISSUE':
                      status = '补货申请通过';
                      break;
                    default:
                      status = '';
                  }
                  return status;
                }
              }, {
                title: "实付金额",
                data: "totalFee",
                name: "totalFee",
                orderable: false,
              }, {
                title: "运费",
                data: "logisticsFee",
                name: "logisticsFee",
                orderable: false,
              }, {
                title: "补货原因",
                data: "refundReason",
                name: "refundReason",
                orderable: false,
              }, {
                title: "补货说明",
                data: "refundMemo",
                name: "refundMemo",
                orderable: false,
              },
              {
                title: "补货凭证",
                orderable: false,
                render: function (data, type, row) {
                  var html = '';
                  if(row.refundImgList != null){
                    for (var i = 0; i < row.refundImgList.length; i++) {
                      html = html + '<a href="' + row.refundImgList[i].imageUrl
                          + '" target="_blank"><img class="goods-image" src="'
                          + row.refundImgList[i].imageUrl + '" /></a>'
                    }
                  }
                  return html;
                }
              },
              {
                title: "物流信息",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED' || row.refundStatus
                      === 'WMS_CHECKED') {
                    let c = row.refundLogisticsCompany || '';
                    let no = row.refundLogisticsNo || '';
                    let logistics = `</br> 物流公司: ${c} </br> 物流单号: ${no} `;
                    return logistics;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "WMS审核状态",
                orderable: false,
                render: function (data, type, row) {
                  if (row.refundStatus === 'SUBMITTED') {
                    return "等待WMS审核";
                  }
                  if (row.refundStatus === 'WMS_CHECKED') {
                    let s = row.wmsCheckStatus || '';
                    let mark = row.wmsCheckRemark || '';
                    let time = row.wmsCheckTime || '';
                    let content = `</br> 审核结果: ${s === 'CHECKED_PASS' ? '入库正常' : '入库异常'}
                                   </br> 备注: ${mark}
                                   </br>审核时间:${new Date(time).toLocaleString()} `;
                    return content;
                  }
                  return "物流信息未提交";
                }
              },
              {
                title: "操作",
                width: "100px",
                orderable: false,
                render: function (data, type, row) {
                  let html = '';
                  if (row.refundStatus === 'APPLY_FOR_REISSUE') {
                    html += `<a class="passReissueApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >通过补货申请</a>`;
                    html += `<a class="failReissueApplyBtn role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >拒绝补货申请</a><br/>`;
                  }
                  if (row.refundStatus === 'SUBMITTED') {
                    html += `<a class="wmsCheck role_check_table" rowId=${row.id} href="#" style="margin-right: 10px;" order-no=${row.orderNo } >入库审核</a><br/>`;
                  }
                  if (row.refundStatus === 'WMS_CHECKED' || row.wmsCheckStatus
                      === 'CHECKED_PASS') {
                    html += '<a class="reissueBtn" href="javascript:void(0)"  style="margin-right: 10px;" orderStatus="'
                        + row.orderStatus + '" order-no="' + row.orderNo
                        + '" order-id="' + row.id + '"> 补货出仓</a><br/>';
                  }
                  html = html
                      + '<a class="cancelReissueBtn role_check_table" href="javascript:void(0)" style="margin-right: 10px;" order-id="'
                      + row.id + '" > 取消补货</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                      + '<a class="viewDetailBtnChange role_check_table" id="reissueGoodsDetail" href="#" style="margin-right: 10px;" order-id="' + row.id + '" order-no="'
                      + row.orderNo + '" > 查看补货商品详情</a>';
                  return html;
                }
              }
            ],
            select: {
              style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
              initEvent();
            }
          });
      });

      var $refundSuccess_datatables;

      // 售后完成订单
      $("#refundSuccessOrderTab").on("click", function () {
        if ($refundSuccess_datatables != undefined) {
            return;
        }
        $refundSuccess_datatables = utils.createDataTable(
          '#xquark_refundSuccess_list_tables', {
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            destroy: true,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/merchant/order/list", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: $('input[name="refundSuccess_orderNo"]').val(),
                paymentMode: $('select[name="refundSuccess_paymentMode"]').val(),
                refundStatus: 'SUCCESS',//售后状态
                buyerRequire: $('select[name="refundSuccess_refundType"]').val() == '' ? '5' : $('select[name="refundSuccess_refundType"]').val(),
                orderType: $('select[name="refundSuccess_orderType"]').val(),
                completeStartDate: $successDateRangeBasic.val() !== ''
                && success_options.startDate !== ''
                    ? success_options.startDate.format('YYYY-MM-DD') : '',
                completeEndDate: $successDateRangeBasic.val() !== ''
                && success_options.endDate !== ''
                    ? success_options.endDate.format('YYYY-MM-DD') : '',
                pageable: true
              }, function (res) {
                if (!res.data.list) {
                  res.data.list = [];
                } else if (res.data.orderTotal > 0) {
                  $("#refundSuccessOrderCountDiv").css("display", "inline-block");
                  $("#refundSuccessOrderCount").html(res.data.orderTotal);
                } else if (res.data.orderTotal == 0) {
                  $("#refundSuccessOrderCountDiv").hide();
                }
                callback({
                    recordsTotal: res.data.orderTotal,
                    recordsFiltered: res.data.orderTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.orderTotal,
                    iTotalDisplayRecords: res.data.orderTotal
                });
              });
            },
            rowId: "id",
            columns: [
              {
                width: "30px",
                orderable: false,
                render: function (data, type, row) {
                  return '<a href="#"><img class="goods-image" src="' + row.imgUrl
                      + '" /></a>';
                }
              },
              {
                title: "订单",
                orderable: false,
                render: function (data, type, row) {
                  return '订单编号: ' + row.orderNo + '<br/>'
                      + '支付单号: ' + row.payNo + '<br/>'
                      + '交易单号: ' + row.outTradeNo + '<br/>'
                      + row.orderItems[0].productName + '等';
                }
              },
              {
                title: "买家",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = row.buyerName + '<br/>' + ((row.buyerPhone != null
                      && row.buyerPhone !== 'null')
                      ? row.buyerPhone : "");
                  return userInfo;
                }
              }, {
                title: "下单时间",
                orderable: false,
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
              },{
                title: "售后类型",
                orderable: false,
                render: function (data, type, row) {
                  let refundType;
                  switch (row.buyerRequire) {
                    case '1':
                      refundType = '申请退款订单';
                      break;
                    case '2':
                      refundType = '申请退货订单';
                      break;
                    case '3':
                      refundType = '申请换货订单';
                      break;
                    case '4':
                      refundType = '申请补货订单';
                      break;
                    default:
                      refundType = '';
                      break;
                  }
                  return refundType;
                }
              }, {
                title: "数量",
                data: "amount",
                name: "amount",
                orderable: false,
              }, {
                title: "价格",
                data: "price",
                name: "price",
                orderable: false,
              }, {
                title: "金额",
                data: "totalFee",
                name: "totalFee",
                orderable: false,
              }, {
                title: "收货人姓名",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup !== 1) {
                    userInfo = row.orderAddress.consignee;
                  }
                  return userInfo;
                }
              }, {
                title: "收货人电话",
                orderable: false,
                render: function (data, type, row) {
                  var userInfo = "";
                  if (row.isPickup !== 1) {
                    userInfo = row.orderAddress.phone;
                  }
                  return userInfo;
                }
              }
              , {
                title: "类别",
                orderable: false,
                render: function (data, type, row) {
                  const orderType = row.orderType;
                  switch (orderType) {
                    case 'INSIDE_BUY':
                      return '内购订单';
                    case 'COUPON':
                      return '优惠券';
                    case 'FULLCUT':
                      return '会员活动';
                    case 'EXCHANGE':
                      return '换货订单';
                    default:
                      return '普通订单';
                  }
                }
              }
              , {
                orderable: false,
                width: "100px",
                title: "管理",
                render: function (data, type, row) {
                  var html = '';
                  html = html
                      + '<a class="viewDetailBtn role_check_table" href="#" style="margin-right: 10px;" order-no="'
                      + row.orderNo + '" > 查看订单详情</a>';
                  return html;
                }
              }
            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });
      });

      //给Body加一个Click监听事件
      $('body').on('click', function (event) {
        var target = $(event.target);
        if (!target.hasClass('popover') //弹窗内部点击不关闭
            && target.parent('.popover-content').length === 0
            && target.parent('.popover-title').length === 0
            && target.parent('.popover').length === 0
            && target.data("toggle") !== "popover") {
          //弹窗触发列不关闭，否则显示后隐藏
          $('[data-toggle="popover"]').popover('hide');
        } else if (target.data("toggle") == "popover") {
          target.popover("toggle");
        }
      });

      //给Body加一个Click监听事件
      $('body').on('click', function (event) {
        var target = $(event.target);
        if (!target.hasClass('popover') //弹窗内部点击不关闭
            && target.parent('.popover-content').length === 0
            && target.parent('.popover-title').length === 0
            && target.parent('.popover').length === 0
            && target.data("toggle") !== "refundpopover") {
          //弹窗触发列不关闭，否则显示后隐藏
          $('[data-toggle="refundpopover"]').popover('hide');
        } else if (target.data("toggle") == "refundpopover") {
          target.popover("toggle");
        }
      });

      var shippingOrderId;
      var isPickup;
      var auditOrderId;

      function initEvent() {
        //申请售后获取orderNo
        $(".viewApplyBtn").off("click").on("click", function () {
          orderIdForRefund = $(this).attr("order-id");
          orderNoForChange = $(this).attr("order-no");
        });

        $(".cancelRefundBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          utils.tools.confirmWihoutText('确定取消退款？', function () {
            cancelRefund(orderId);
          }, function () {
          })
        });

        //取消换货（调用的仍然是取消退款的逻辑，存在问题，不影响使用，暂时不做修改）
        $(".cancelChangeBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          utils.tools.confirmWihoutText('确定取消换货？', function () {
            cancelRefund(orderId);
          }, function () {
          })
        });

        //取消补货（调用的仍然是取消退款的逻辑，存在问题，不影响使用，暂时不做修改）
        $(".cancelReissueBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          utils.tools.confirmWihoutText('确定取消补货？', function () {
            cancelRefund(orderId);
          }, function () {
          })
        });

        $(".wmsCheck").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          wmsForm.find("input[name=id]").prop("value", id);
          wmsCheckModal.modal("show");
        });

        $(".cancelBtn").off("click").on("click", function () {
          let orderId = $(this).attr("order-id");
          cancelOrder(orderId);
        });

        $(".openRefundEntryBtn").off("click").on("click", function () {
          let id = $(this).attr("order-id");
          utils.tools.confirmWihoutText('确定开启售后？', function () {
            setTimeout(() => {
              $.get(updateCapableToRefundUrl(id, true)
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          });
        });

        $(".passRefundApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定通过退款审核？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "ACCEPT_REFUND"}
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $refund_datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          })
        });

        //换货审核
        $(".passChangeApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          $("#modal_change_wms_check").modal('show');
          $("#confirmChangeWmsCheck").off("click").on("click", function(){

            let result = $('input[name="changeWmsCheck"]:checked').val();
            if(result === 'NOT_NEED_CHECKED'){
              utils.tools.confirmWihoutText('确定通过换货审核？', function () {
                setTimeout(() => {
                  $.get(updateStatusUrl(id), {status: "WMS_CHECKED"} //调用控制器的方法更新订单状态
                  ).done(
                      data => {
                        utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                        $change_datatables.search('').draw();
                      }
                  ).fail(
                      data => {
                        utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                      });
                }, 500);
              }, function () {
              })
            }else{
              utils.tools.confirmWihoutText('确定通过换货审核？', function () {
                setTimeout(() => {
                  $.get(updateStatusUrl(id), {status: "ACCEPT_CHANGE"} //调用控制器的方法更新订单状态
                  ).done(
                      data => {
                        utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                        $change_datatables.search('').draw();
                      }
                  ).fail(
                      data => {
                        utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                      });
                }, 500);
              }, function () {
              })
            }
          });
        });


        //补货审核
        $(".passReissueApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定通过补货审核？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "ACCEPT_REISSUE"} //调用控制器的方法更新订单状态
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $reissue_datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          })
        });

        $(".failRefundApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定拒绝退款申请？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "REJECT_REFUND"}
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $refund_datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          })
        });

        //拒绝换货申请
        $(".failChangeApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定拒绝换货申请？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "REJECT_CHANGE"}
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $change_datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          })
        });

        //拒绝补货申请
        $(".failReissueApplyBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          utils.tools.confirmWihoutText('确定拒绝补货申请？', function () {
            setTimeout(() => {
              $.get(updateStatusUrl(id), {status: "REJECT_REISSUE"}
              ).done(
                  data => {
                    utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                    $reissue_datatables.search('').draw();
                  }
              ).fail(
                  data => {
                    utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
                  });
            }, 500);
          }, function () {
          })
        });

        $("select[name=orderStatus]").off("change").on("change", function () {
          $datatables.search('').draw();
        });

        $(".shipBtn").off("click").on("click", function () {
          shippingOrderId = $(this).attr('order-id');
          isPickup = $(this).attr('isPickup');
          if (isPickup == 1) {
            $("#logisticsTitle").html("确认发货");
            $("#logisticsCompany").hide();
            $("#logisticsOrderNo").hide();
            $("#logisticsDesc").show();
          }
          $("#modal_shipping").modal('show');
        });

        $(".auditBtn").off("click").on("click", function () {
          auditOrderId = $(this).attr('order-id');
          $("#modal_audit").modal('show');
        });

        // 修改订单数量
        $(".updateQtyBtn").off("click").on("click", function () {
          var orderNo = $(this).attr('order-no');
          var orderAmount = $(this).attr('order-amount');
          $("#update_orderNo").val(orderNo);
          $("#update_orderAmount").val(orderAmount);
          $("#modal_updateAmount").modal('show');
        });

        // 删除订单
        $(".delBtn").off("click").on("click", function () {
          var orderId = $(this).attr('order-id');
          var a = confirm("确定删除该订单吗？");
          if (a == true) {
            var data = {
              'orderId': orderId
            };
            utils.postAjax(window.host + '/order/delete', data, function (res) {
              if (typeof(res) === 'object') {
                if (res.data) {
                  utils.tools.alert("删除成功", {timer: 1200});
                  $datatables.search('').draw();
                } else {
                  utils.tools.alert("删除订单失败!",
                      {timer: 1200, type: 'warning'});
                }
              }
            });
          }
        });

        // 确认订单
        $(".confirmBtn").off("click").on("click", function () {
          var orderId = $(this).attr('order-id');
          var a = confirm("确定确认该订单吗？");
          if (a == true) {
            var data = {
              'orderId': orderId
            };
            utils.postAjax(window.host + '/order/confirmOrder', data, function (res) {
              if (typeof(res) === 'object') {
                if (res.data) {
                  utils.tools.alert("确认成功", {timer: 1200});
                  $datatables.search('').draw();
                } else {
                  utils.tools.alert("确认订单失败!",
                      {timer: 1200, type: 'warning'});
                }
              }
            });
          }
        });

        tableRoleCheck('#xquark_list_tables');

        //查看订单详情
        $(".viewDetailBtn").off("click").on("click", function () {

          var orderNo = $(this).attr("order-no");
          //查看订单详情
          if ($orderTables !== undefined) {
            $orderTables.search('').draw();
            return;
          }
          /** 页面表格默认配置 **/
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
              search: '<span>筛选:</span> _INPUT_',
              lengthMenu: '<span>显示:</span> _MENU_',
              info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
              paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
              },
              infoEmpty: "",
              emptyTable: "暂无相关数据"
            }
          });

          var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            destroy: true,
            ordering: false,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/twitter/order/orderItem", {
                size: data.length,
                page: (data.start / data.length),
                orderNo: orderNo,
                pageable: true,
              }, function (res) {
                if (res.errorCode != '200') {
                  return;
                }

                if (!res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.itemTotal,
                  recordsFiltered: res.data.itemTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.itemTotal,
                  iTotalDisplayRecords: res.data.itemTotal
                });
              });
            },
            columns: [{
              data: 'productName',
              name: 'productName',
              orderable: false,
              title: "商品"
            },
              {
                data: 'marketPrice',
                name: 'marketPrice',
                orderable: false,
                title: "原价"
              },
              {
                data: 'discount',
                name: 'discount',
                orderable: false,
                title: "折扣"
              },
              {
                data: 'discountPrice',
                name: 'discountPrice',
                orderable: false,
                title: "优惠金额"
              },
              {
                data: 'price',
                name: 'price',
                orderable: false,
                title: "订单金额"
              }, {
                data: 'amount',
                name: 'amount',
                orderable: false,
                title: "数量"
              }, {
                data: 'addressDetails',
                name: 'addressDetails',
                orderable: false,
                title: "收货地址"
              },
              {
                orderable: false,
                title: "下单时间",
                render: function (data, type, row) {
                  var cDate = parseInt(row.createdAt);
                  var d = new Date(cDate);
                  return d.format('yyyy-MM-dd hh:mm:ss');
                },
              },
              {
                data: 'logisticsOrderNo',
                name: 'logisticsOrderNo',
                orderable: false,
                title: "物流单号"
              }, {
                width: "150px",
                orderable: false,
                title: "收货时间",
                render: function (data, type, row) {
                  if (row.succeedAt && row.succeedAt != '') {
                    var cDate = parseInt(row.succeedAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  } else {
                    return "";
                  }
                },
              }],
            drawCallback: function () {  //数据加载完成
            }
          });
          $('#modal_orderInfo').modal('show');
        });

        //查看换货订单详情
        $(".viewDetailBtnChange").off("click").on("click", function () {

          var orderId = $(this).attr("order-id");
          //查看订单详情
          if ($orderTables !== undefined) {
            $orderTables.search('').draw();
            return;
          }
          /** 页面表格默认配置 **/
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
              search: '<span>筛选:</span> _INPUT_',
              lengthMenu: '<span>显示:</span> _MENU_',
              info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
              paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
              },
              infoEmpty: "",
              emptyTable: "暂无相关数据"
            }
          });
          var $orderTables = $('#orderInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            destroy: true,
            ordering: false,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/twitter/order/changeOrderItem", {
                size: data.length,
                page: (data.start / data.length),
                orderId: orderId,
                pageable: true,
              }, function (res) {
                if (res.errorCode != '200') {
                  return;
                }
                if (!res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.itemTotal,
                  recordsFiltered: res.data.itemTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.itemTotal,
                  iTotalDisplayRecords: res.data.itemTotal
                });
              });
            },
            columns: [
              {
                data: 'product.imgUrl',
                name: 'product.imgUrl',
                orderable: false,
                title: "商品图片",
                render: function (data) {
                  return data
                      ? `<img class="goods-image" src="${data}" />`
                      : `<img class="goods-image" src="${NOT_FOUND}" />`;
                }
              },
              {
                data: 'product.name',
                name: 'product.name',
                orderable: false,
                title: "商品名称"
              },
              {
                title: "数量",
                data: "amount",
                name: "amount",
                orderable: false,
              },
              {
                title: "类别",
                orderable: false,
                render: function (data, type, row) {
                  const orderType = row.orderType;
                  switch (orderType) {
                    case 'INSIDE_BUY':
                      return '内购订单';
                    case 'COUPON':
                      return '优惠券';
                    case 'FULLCUT':
                      return '会员活动';
                    default:
                      return '普通订单';
                  }
                }
              },
              {
                data: 'product.status',
                name: 'product.status',
                orderable: false,
                title: "商品状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '已上架';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '草稿';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.reviewStatus',
                name: 'product.reviewStatus',
                orderable: false,
                title: "审核状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'NO_NEED_TO_CHECK':
                      status = '未提交审核';
                      break;
                    case 'WAIT_CHECK':
                      status = '待审核';
                      break;
                    case 'CHECK_FAIL':
                      status = '审核未通过';
                      break;
                    case 'CHECK_PASS':
                      status = '审核通过';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.supplierId',
                name: 'product.supplierId',
                orderable: false,
                title: "供应商",
                render: function (data) {
                  const id = data;
                  if (id && id !== '') {
                    let name;
                    if (name = sourceMap.get(id)) {
                      return name;
                    }
                  }
                  return '无';
                }
              },
              {
                data: 'product.marketPrice',
                name: 'product.marketPrice',
                orderable: false,
                title: "原价"
              },
              {
                data: 'product.price',
                name: 'product.price',
                orderable: false,
                title: "优惠价"
              },
              {
                data: 'product.amount',
                name: 'product.amount',
                orderable: false,
                title: "库存"
              },
              {
                data: 'product.sales',
                name: 'product.sales',
                orderable: false,
                title: "销量"
              },
              {
                data: 'product.description',
                name: 'product.description',
                orderable: false,
                title: "商品描述"
              },
            ],
            drawCallback: function () {  //数据加载完成
            }
          });
          $('#modal_orderInfo').modal('show');
        });

        // 退款
        $(".refundBtn").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          let totalFee = $(this).attr("totalFee");
          $("#refund_order_id").val(id);
          $("#refund_fee").val(totalFee);
          $("#total_fee").val(totalFee);

          $("#refund_info_body").children().remove();
          var data = $('#xquark_refund_list_tables').DataTable().row(
              this.parentElement.parentElement).data()//fnGetNodes获取表格所有行，nTrs[i]表示第i行tr对象
          for (var i = 0; i < data.orderItems.length; i++) {

            $("#refund_info_body").append(
                `<br/><div>${data.orderItems[i].productName}  实际支付金额：${data.paidFee} </div>`);
            $("#refund_info_body").append(
                `<br/><div>退款原因：${data.refundReason} </div>`);
            $("#refund_info_body").append(
                `<br/><div>退款描述：${data.refundMemo} </div>`);
          }
          $('#modal_refund_logistic_info').modal('show');
        });

        // 换货出仓
        $(".changeBtn").off("click").on("click", function () {
          var $goodsTables;
          // var orderNo = $(this).attr("order-no");
          orderId = $(this).attr("order-id");
          if ($goodsTables !== undefined) {
            $goodsTables.search('').draw();
            return;
          }
          /** 页面表格默认配置 **/
          $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
              search: '<span>筛选:</span> _INPUT_',
              lengthMenu: '<span>显示:</span> _MENU_',
              info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
              paginate: {
                'first': '首页',
                'last': '末页',
                'next': '&rarr;',
                'previous': '&larr;'
              },
              infoEmpty: "",
              emptyTable: "暂无相关数据"
            }
          });
          $goodsTables = $('#changeGoodsInfo').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            destroy: true,
            ordering: false,
            ajax: function (data, callback, settings) {
              $.get(window.host + "/twitter/order/changeOrderItem", {
                size: data.length,
                page: (data.start / data.length),
                orderId: orderId,
                pageable: true,
              }, function (res) {
                if (res.errorCode != '200') {
                  return;
                }
                if (!res.data.list) {
                  res.data.list = [];
                }
                callback({
                  recordsTotal: res.data.itemTotal,
                  recordsFiltered: res.data.itemTotal,
                  data: res.data.list,
                  iTotalRecords: res.data.itemTotal,
                  iTotalDisplayRecords: res.data.itemTotal
                });
              });
            },
            columns: [
              {
                data: 'product.imgUrl',
                name: 'product.imgUrl',
                orderable: false,
                title: "商品图片",
                render: function (data) {
                  return data
                      ? `<img class="goods-image" src="${data}" />`
                      : `<img class="goods-image" src="${NOT_FOUND}" />`;
                }
              },
              {
                data: 'product.name',
                name: 'product.name',
                orderable: false,
                title: "商品名称"
              },
              {
                title: "数量",
                data: "amount",
                name: "amount",
                orderable: false,
              },
              {
                title: "类别",
                orderable: false,
                render: function (data, type, row) {
                  const orderType = row.orderType;
                  switch (orderType) {
                    case 'INSIDE_BUY':
                      return '内购订单';
                    case 'COUPON':
                      return '优惠券';
                    case 'FULLCUT':
                      return '会员活动';
                    default:
                      return '普通订单';
                  }
                }
              },
              {
                data: 'product.status',
                name: 'product.status',
                orderable: false,
                title: "商品状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'INSTOCK':
                      status = '下架';
                      break;
                    case 'ONSALE':
                      status = '已上架';
                      break;
                    case 'FORSALE':
                      status = '待上架发布';
                      break;
                    case 'DRAFT':
                      status = '草稿';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.reviewStatus',
                name: 'product.reviewStatus',
                orderable: false,
                title: "审核状态",
                render: function (data) {
                  var status = '';
                  switch (data) {
                    case 'NO_NEED_TO_CHECK':
                      status = '未提交审核';
                      break;
                    case 'WAIT_CHECK':
                      status = '待审核';
                      break;
                    case 'CHECK_FAIL':
                      status = '审核未通过';
                      break;
                    case 'CHECK_PASS':
                      status = '审核通过';
                      break;
                    default:
                      break;
                  }
                  return status;
                }
              },
              {
                data: 'product.supplierId',
                name: 'product.supplierId',
                orderable: false,
                title: "供应商",
                render: function (data) {
                  const id = data;
                  if (id && id !== '') {
                    let name;
                    if (name = sourceMap.get(id)) {
                      return name;
                    }
                  }
                  return '无';
                }
              },
              {
                data: 'product.marketPrice',
                name: 'product.marketPrice',
                orderable: false,
                title: "原价"
              },
              {
                data: 'product.price',
                name: 'product.price',
                orderable: false,
                title: "优惠价"
              },
              {
                data: 'product.amount',
                name: 'product.amount',
                orderable: false,
                title: "库存"
              },
              {
                data: 'product.sales',
                name: 'product.sales',
                orderable: false,
                title: "销量"
              },
              {
                data: 'product.description',
                name: 'product.description',
                orderable: false,
                title: "商品描述"
              },
            ],
            drawCallback: function () {  //数据加载完成
            }
          });
          $('#modal_change').modal('show');//显示弹出窗口
        });

        //补货
        $(".reissueBtn").off("click").on("click", function () {
          var orderNo = $(this).attr("order-no");
          var orderId = $(this).attr("order-id");
          var a = confirm("确认补货出仓吗？");
          if(a == true){
            var data = {
              orderId: orderId,
              orderNo: orderNo,
            };
            var url = window.host + "/update/order/status";
            utils.postAjax(url, data, function (res) {
              if (typeof(res) === 'object') {
                if (res.data === true) {
                  utils.tools.alert("操作成功!", {timer: 1200, type: 'success'});
                  $reissue_datatables.search('').draw();
                } else {
                  utils.tools.alert("操作失败!", {timer: 1200, type: 'warning'});
                }
              }
            });
          }
        });
      }

      $(".btn-search").off("click").on('click', function () {
        $listUrl = window.host + "/product/list";
        var keyword = $.trim($("#sKeyword").val());
        if (keyword != '' && keyword.length > 0 && shopId != null) {
          $listUrl = window.host + '/product/searchbyPc/' + shopId + '/'
              + keyword;
        }
        $datatables.search(keyword).draw();
      });

      $(".refund_btn-search").off("click").on('click', function () {
        $refund_datatables.search('').draw();
      });

      $(".change_btn-search").off("click").on('click', function () {
        $change_datatables.search('').draw();
      });

      $(".reissue_btn-search").off("click").on('click', function () {
        $reissue_datatables.search('').draw();
      });

      $(".refundSuccess_btn-search").off("click").on('click', function () {
        $refundSuccess_datatables.search('').draw();
      });

      $(".updateAmountBtn").off("click").on('click', function () {
        var orderAmount = $('input[name="update_orderAmount"]').val();
        var orderNo = $('input[name="update_orderNo"]').val();
        if (orderAmount == null || orderAmount == '') {
          utils.tools.alert('请填写订单数量');
        }
        var data = {
          'orderNo': orderNo,
          'orderAmount': orderAmount
        };
        utils.postAjax(window.host + '/order/updateAmount', data,
            function (res) {
              if (typeof(res) === 'object') {
                if (res.data) {
                  utils.tools.alert("修改成功", {timer: 1200});
                  $("#modal_updateAmount").modal('hide');
                  $datatables.search('').draw();
                } else {
                  utils.tools.alert("更新订单数量失败!",
                      {timer: 1200, type: 'warning'});
                }
              }
            });
      });

      $(".shippedBtn").off("click").on('click', function () {
        if (shippingOrderId != null) {
          var logisticsCompany = $('input[name="logisticsCompany"]').val();
          var logisticsOrderNo = $('input[name="logisticsOrderNo"]').val();
          if (isPickup != 1) {
            if(isEmpty(logisticsCompany) || isEmpty(logisticsOrderNo)){
              utils.tools.alert('请填写物流信息');
              return;
            }
          }
          var data = {
            'logisticsCompany': logisticsCompany,
            'logisticsOrderNo': logisticsOrderNo,
            'orderId': shippingOrderId
          };

          utils.postAjax(window.originalHost + '/order/shipped', data,
              function (res) {
                if (res == true) {
                  $datatables.search('').draw();//重绘界面，显示开启售后的按钮，后期取消此功能
                } else if (res == 0) {
                  utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                  $("input[name='logisticsCompany']").val("").focus();//清空输入框的内容
                  $("input[name='logisticsOrderNo']").val("").focus();
                } else if (res == -1) {
                  utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                } else {
                  utils.tools.alert("更新物流信息失败!",
                      {timer: 1200, type: 'warning'});
                }
              });
        }
      });

      $(".auditedBtn").off("click").on('click', function () {
        if (auditOrderId != null) {
          var logisticsCompany = $(
              'input[name="audit_logisticsCompany"]').val();
          var logisticsOrderNo = $(
              'input[name="audit_logisticsOrderNo"]').val();
          var data = {
            'logisticsCompany': logisticsCompany,
            'logisticsOrderNo': logisticsOrderNo,
            'orderId': auditOrderId
          };

          $.ajax({
            url: window.host + '/order/auditAndShip',
            data: data,
            type: "POST",
            success: function (data) {
              var _res = data;
              if (_res.rc == '1') {
                utils.tools.alert(_res.msg, {timer: 1200});
                $("#modal_audit").modal('hide');
                $datatables.search('').draw();
              } else if (_res.rc == '0') {
                utils.tools.alert(_res.msg, {timer: 4000});
              } else {
                alert("审核失败!");
              }
            },
            error: function (err) {
              alert("服务器暂时没有响应，请稍后重试。");
            }
          });

        }
      });

      /*商品删除会判断 userId 否则删除失败*/
      function deleteProduct(pId) {
        var url = window.host + "/product/delete/" + pId;
        utils.postAjax(url, {}, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              $datatables.search('').draw();
            } else {
              utils.tools.alert("删除失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      // 订单退款
      // 根据退款前订单状态判断是否退全款或者扣除运费，默认算出一个退款金额，用户也可手动更改
      function refundOrder(pId, refundFee, reason, logisticsFee) {
        var data = {
          orderId: pId,
          refundment: refundFee,
          refundReason: reason,
          logisticsFee: logisticsFee
        };
        var url = window.host + "/order/refund";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("退款成功!", {timer: 1200, type: 'success'});
              $datatables.search('').draw('page');
              $refund_datatables.search('').draw('page');
            } else {
              utils.tools.alert("退款失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      //取消退款
      function cancelRefund(oId) {
        var data = {
          orderId: oId,
        };
        var url = window.host + "/order/refund/cancel";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data === true) {
              utils.tools.alert("取消成功!", {timer: 1200, type: 'success'});
              // $refund_datatables.search('').draw();
              window.location.reload();//暂时刷新整个页面
            } else {
              utils.tools.alert("取消失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      // 取消订单
      function cancelOrder(pId) {
        var data = {
          orderId: pId,
        };
        var url = window.host + "/api/order/cancel";
        utils.postAjax(url, data, function (res) {
          if (typeof(res) === 'object') {
            if (res.data) {
              utils.tools.alert("取消成功!", {timer: 1200, type: 'warning'});
              $datatables.search('').draw();
            } else {
              utils.tools.alert("取消失败!", {timer: 1200, type: 'warning'});
            }
          }
        });
      }

      $('.file-input').fileinput({
        language: 'zh',
        uploadUrl: window.originalHost + '/order/import',
        previewFileType: 'text',
        browseLabel: '选择文件',
        removeLabel: '删除',
        uploadLabel: '上传',
        browseIcon: '<i class="icon-file-plus"></i>',
        uploadIcon: '<i class="icon-file-upload2"></i>',
        removeIcon: '<i class="icon-cross3"></i>',
        browseClass: 'btn btn-primary',
        uploadClass: 'btn btn-default',
        removeClass: 'btn btn-danger',
        initialCaption: '',
        maxFilesNum: 5,
        allowedFileExtensions: ["xls"],
        layoutTemplates: {
          icon: '<i class="icon-file-check"></i>',
          footer: '',
        },
      });

      $('#modal_import_logistic_info').on('hidden.bs.modal', function () {
        $('#file-input').fileinput('clear');
      });

      $('#modal_import_logistic_info').on('fileuploaded',
          function (event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                response = data.response, reader = data.reader;
            if (response == '200') {
              utils.tools.alert('文件上传成功');
              $('#modal_import_logistic_info').modal('hide');
            } else if (response == '207') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('发货信息上传全部失败,请核对上传格式是否正确,或数据是否填写正确');
            } else if (response == '206') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('部分订单上传失败,请核对上传格式是否正确,或数据是否填写正确');
            } else if (response == '500') {
              $('#file-input').fileinput('enable');
              utils.tools.alert('网络错误,请稍后再试,或者联系管理员');
            }
          });
      $(".exportOrderBtn").on('click', function () {
        $('#exportOrderForm').submit();
      });

      /**
       * 监听物流审核窗口的变化
       */
      wmsCheckModal.on('hidden.bs.modal', function (e) {
        wmsForm[0].reset();
      });

      /**
       * 监听物流审核创口的提交按钮点击
       */
      $("#submit-wms-check").on("click", function () {
        let obj = wmsForm.serializeObject();
        $.get(updateWmsStatusUrl(obj.id), {
              status: "WMS_CHECKED",
              checkStatus: obj.wmsCheckStatus,
              remark: obj.wmsCheckRemark,
            }
        ).done(
            data => {
              if (data && data.data === true) {
                utils.tools.alert('操作成功!', {timer: 1200, type: 'success'});
                wmsCheckModal.modal("hide");
                var refundClick = document.getElementById("refundBtnSearch");
                refundClick.click();
                $reissue_datatables.search('').draw();
              } else {
                utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
              }
            }
        ).fail(
            data => {
              utils.tools.alert("操作失败!", {timer: 2000, type: 'warning'});
            });
        $change_datatables.search('').draw();//新增的换货审核通过，重新加载界面
      });

      $("#refund_ok").off("click").on('click', function () {
        var orderid = $("#refund_order_id").val();
        var refund_fee = $("#refund_fee").val();
        var total_fee = $("#total_fee").val();
        var reason = $("input[type='radio'][name='reason']:checked").val();
        let logisticsFee = $("#logisticsFee").val();
        let result = isRealNum(logisticsFee);
        if(result !== 0 && result){
          alert("请输入正确的数值！");
          return;
        }
        if(logisticsFee > 100){
          alert('输入的值超过最大限制！');
          return;
        }
        if(logisticsFee < 0){
          alert('输入的值不能为负数');
          return;
        }
        if(result === 0){
          logisticsFee = 0;
        }
        if (Number(refund_fee) > Number(total_fee)) {
          utils.tools.alert('退款金额不能大于订单总金额');
          return;
        }
        $('#modal_refund_logistic_info').modal('hide');
        refundOrder(orderid, refund_fee, reason, logisticsFee);
        $('#logisticsFee').val("0");
      });

      function isRealNum(val){
        // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
        if(val === "" || val == null){
          return 0;
        }
        return isNaN(val);//不是数字返回true
      }

      buttonRoleCheck('.hideClass');

    }
);

//微信账号绑定
require(['all']);


require(['mall/order']);

define("mallOrder", function(){});

