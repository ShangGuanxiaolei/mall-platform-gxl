/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('utils/fileUploader',['jquery', 'utils', 'dropzone'], function ($, utils) {

  Dropzone.autoDiscover = false;
  const defaultUrl = window.host + '/_f/u?belong=PRODUCT';

  /**
   * 新建dropZone封装实例
   * @param params.onSuccess 成功后的回调函数，返回地址
   * @param params.dom drop的dom节点
   * @param params.url 上传文件地址，不指定则使用默认的
   * @param params.allowTypes 允许上传的文件类型
   * @param params.maxSize 最大上传大小，单位MB
   * @param params.onRemove 客户端移除文件事件
   * @param params.onError 出错时回调
   * @param params.maxFiles 最大文件数量
   * @param params.onProcess 上传时事件
   * @param params.onComplete 上传完成时事件
   * @constructor
   */
  function Uploader(params) {
    /* jquery 的dropZone实例 */
    this.jDropInstance = $(params.dom).dropzone({
      url: params.url || defaultUrl,
      paramName: "file", // The name that will be used to transfer the file 相当于<input>元素的name属性
      dictDefaultMessage: '<span ><i ></i> 拖动文件至该处</span> \ <span >(或点击此处)添加图片</span> <br /> \ <i ></i>',  //没有任何文件被添加的时候的提示文本。
      maxFilesize: params.maxSize || 2, // MB      //最大文件大小，单位是 MB
      maxFiles: params.maxFiles||10,               //限制最多文件数量
      maxThumbnailFilesize: 10,
      addRemoveLinks: true,
      thumbnailWidth: "150",      //设置缩略图的缩略比
      thumbnailHeight: "150",     //设置缩略图的缩略比
      acceptedFiles: params.allowTypes || ".gif,.png,.jpg",
      uploadMultiple: false,
      dictInvalidFileType: "文件格式错误:建议文件格式:" + params.allowTypes
      || "gif, png, jpg",//文件类型被拒绝时的提示文本。
      dictFileTooBig: "文件过大({{filesize}}MB). 上传文件最大支持: {{maxFilesize}}MB.",   //文件大小过大时的提示文本
      dictRemoveFile: "删除",                                        //移除文件链接的文本
      dictFallbackMessage: "您浏览器暂不支持该上传功能!",               //Fallback 情况下的提示文本
      dictResponseError: "服务器暂无响应,请稍后再试!",
      dictCancelUpload: "取消上传",
      dictCancelUploadConfirmation: "你确定要取消上传吗？",              //取消上传确认信息的文本
      dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",      //超过最大文件数量的提示文本。
      //autoProcessQueue: false,
      init: function () {

        // var imgDropzone = this;
        //添加了文件的事件
        this.on("addedfile", function (file) {
          if (file && file.dataImg && file.previewElement) { //是网络加载的数据
            $(file.previewElement).attr("data-img", file.dataImg);
            if (file.size === '' || file.length === 0) {
              $(file.previewElement).find(".dz-details").hide();
            }
          }
          if (params.onProcess) {
            params.onProcess();
          }
          //imgDropzone.processQueue();
        });
        this.on("success", function (file, data) {
          if (typeof(data) === 'object') {
            switch (data.errorCode) {
              case 200: {
                if (typeof(data.data) === 'object') {
                  if(data.data[0]) {
                    const resData = data.data[0];
                    const imgId = resData.id;
                    if (file && file.previewElement) {
                      $(file.previewElement).attr("data-img", imgId);
                      if (params.onSuccess) {
                        resData.name = file.name;
                        resData.size = file.size;
                        console.log(file);
                        console.log(resData);
                        params.onSuccess(resData);
                      }
                    }
                  }else {
                      if (file && file.previewElement) {
                        if (params.onSuccess) {
                          params.onSuccess(data.data);
                        }
                      }
                    }
                  }
                break;
              }
              default: {
                utils.tools.alert("文件上传失败,请重新选择!",
                    {timer: 1200, type: 'warning'});
                break;
              }
            }
          } else {
            if (data === -1) {
              utils.tools.alert("文件上传失败,请重新选择!",
                  {timer: 1200, type: 'warning'});
            }
          }
        });

        this.on("error", function (file, message) {
          if (params.onError) {
            params.onError(message);
          } else {
            $(".dz-error-message").html(message);
          }
        });

        // 上传完成事件
        this.on("complete", function () {
          if (params.onComplete) {
            params.onComplete();
          }
        });

        this.on("thumbnail", function (file) {
          if (file && file.previewTemplate) {
            file.previewTemplate.firstElementChild.childNodes[0].clientHeight = 150;
            file.previewTemplate.firstElementChild.childNodes[0].clientWidth = 150;
          }
          const $dzImage = $('.dz-image');
          $dzImage.css("height", "150px;");
          $dzImage.css("width", "150px;");
        });

        this.on('removedfile', function (file) {
          if (params.onRemove) {
            params.onRemove(file);
          }
        });

      }
    });

    /* 真实的dropZone实例 */
    this.dropZone = this.jDropInstance[0].dropzone;

    /**
     * 清空dropZone的上传文件
     */
    this.clear = function () {
      this.dropZone.removeAllFiles(true);
      return this;
    };

    /**
     * 添加图片预览到dropZone
     * @param imgUrl 图片地址
     * @param name 文件名
     * @param size 文件大小
     */
    this.addImage = function (imgUrl, name = '', size = '') {
      if (imgUrl) {
        const mockFile = {name: name, size: size, dataImg: imgUrl};
        this.dropZone.emit("addedfile", mockFile);
        this.dropZone.emit("thumbnail", mockFile, imgUrl);
        this.dropZone.emit("complete", mockFile);
        this.dropZone.files.push(mockFile); // 此处必须手动添加才可以用removeAllFiles移除
      }
      return this;
    };

    this.destroy = function () {
      this.dropZone.destroy();
    };

    this.disable = function () {
      this.dropZone.disable();
    };

    this.enable = function () {
      this.dropZone.enable();
    };

    this.disableClick = function () {
      $(".dz-hidden-input").prop("disabled", true);
    };

    this.enableClick = function () {
      $(".dz-hidden-input").prop("disabled", false);
    }
  }

  return {

    /**
     * 新建dropZone封装实例
     * @param params.onSuccess 成功后的回调函数，返回地址
     * @param params.dom drop的dom节点
     * @param params.url 上传文件地址，不指定则使用默认的
     * @param params.allowTypes 允许上传的文件类型
     * @param params.maxSize 最大上传大小，单位MB
     * @param params.onRemove 客户端移除文件事件
     * @param params.onError 出错时回调
     * @param params.onProcess 上传时事件
     * @param params.onComplete 上传完成时事件
     * @constructor
     */
    create: function (params) {
      return new Uploader(params);
    }
  }

});
define('brand/add',['jquery', 'validate', 'utils', 'utils/fileUploader',
      'jquerySerializeObject', 'select2', 'uniform'],
    function ($, validate, utils, uploader) {
      const moduleName = 'brand';
      const detailUrl = id => window.host + `/${moduleName}/${id}`;
      const saveUrl = window.host + `/${moduleName}/edit`;

      const addBrandButton = $("#addBrand");
      const brandForm = $('#brand-form');
      const modal = $("#modal_add_brand");
      const imageInput = $("#logo");
      const submitButton = $("#submit-brand");
      const isShowInput = $("#isShow");
      const dropZoneDom = "#brand-logo";
      let dropZoneInstance = '';
      let updateTable = '';
      let isSbmiting = false;

      const bindEvent = function () {
        $(".styled, .multiselect-container input").uniform();

        /**
         * 点击新增商品
         */
        addBrandButton.on("click", function () {
          modal.modal("show");
        });

      };

      const submitBrand = function (brand) {
        if (isSbmiting === true) {
          return;
        }
        if (!brand.id) {
          delete brand.id;
        }
        $.post(saveUrl, brand).done(date => {
          modal.modal("hide");
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(data => {
          modal.modal("hide");
          utils.tools.error("操作出错!");
        });
        isSbmiting = false;
      };

      const editBrand = function (id) {
        $.get(detailUrl(id)).done(date => {
          let brand = date.data;
          utils.tools.syncForm(brandForm, brand);
          brand.isShow === true ? isShowInput.prop("checked", true)
              : isShowInput.prop("checked", false);
          $.uniform.update();
          imageInput.prop("value", brand.logo);
          dropZoneInstance.addImage(brand.logoUrl, '', '', brand.logo);
          modal.modal("show");
        }).fail(data => {
          utils.tools.error("获取数据出错!");
        });
      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          resetForm: function () {
            modal.on('hidden.bs.modal', function (e) {
              brandForm[0].reset();
              brandForm.validate().resetForm();
              dropZoneInstance.clear();
            });
            return this;
          },
          initSubmit: function () {
            submitButton.on("click", () => {
              let result = brandForm.valid();
              if (result) {
                console.log("success valid");
                let brand = brandForm.serializeObject();
                brand.isShow = !!isShowInput.prop("checked");
                brand.source = "CLIENT";
                submitBrand(brand);
              }
            });
            return this;
          },
          initDropZone: function () {
            const params = {
              dom: dropZoneDom,
              maxFiles: 1,
              onSuccess: (data) => {
                imageInput.attr("value", data.id);
              },
              onRemove: () => {
                imageInput.attr("value", '');
              }
            };
            dropZoneInstance = uploader.create(params);
            return this;
          },
          initValidate: function () {

            brandForm.validate({
              rules: {
                logo: {
                  required: true,
                },
                name: {
                  required: true,
                },
                sortOrder: {
                  required: true,
                  number: true,
                },
                description: {
                  required: false,
                },
                siteHose: {
                  required: false,
                },
              },
              messages: {
                logo: {
                  required: "请上传商品logo"
                },
                name: {
                  required: "请输入商品名称",
                },
                sortOrder: {
                  required: "请输入排序号",
                },
              },
              submitHandler: function () {
              },
              invalidHandler: function (event, validator) {
                console.log("validate error");
                let errorList = validator.errorList;
                for (let i = 0; i < errorList.length; i++) {
                  utils.tools.alert(errorList[i].message);
                }
              }
            });
            return this;
          },
        }
      })();

      eventInitialization.init().initDropZone().initValidate().initSubmit().resetForm();

      return {
        edit: editBrand,
        setUpdateFunc: func => {
          updateTable = func
        }
      }

    });

/**
 * Created by jitre on 18/2/8.
 */
define('brand/list',['jquery', 'utils', 'datatables', 'select2', 'daterangepicker',
      'brand/add'],
    function ($, utils, datatables, select2, daterangepicker, add) {
      const NOT_FOUND = "/_resources/images/404.png";
      const moduleName = 'brand';
      const listUrl = window.host + `/${moduleName}/list`;
      const batchDeleteUrl = ids => window.host
          + `/${moduleName}/batchDelete?ids=${ids}`;
      const deleteUrl = id => window.host + `/${moduleName}/delete/${id}`;
      const addUrl = window.host + `/${moduleName}/edit`;

      const checkAll = '#checkAllGoods';
      const searchButton = $('#search');
      const publishButton = $("#publishCase");
      const modal = $("#modal_add_brand");
      const batchDeleteButton = $("#batchDel");
      const tableSource = '#guiderUserTable';
      var $datatables = '';

      /** 页面表格默认配置 **/
      $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
          search: '<span>筛选:</span> _INPUT_',
          lengthMenu: '<span>显示:</span> _MENU_',
          info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
          paginate: {
            'first': '首页',
            'last': '末页',
            'next': '&rarr;',
            'previous': '&larr;'
          },
          infoEmpty: "",
          emptyTable: "暂无相关数据"
        }
      });

      /**
       *删除
       */
      const deleteComponent = function (pId) {
        $.get(deleteUrl(pId)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       * 批量删除
       * @param ids
       */
      const batchDelete = function (ids) {
        $.get(batchDeleteUrl(ids)).done(date => {
          utils.tools.success("操作成功!");
          updateTable();
        }).fail(date => {
          utils.tools.error("操作出错!");
        });
      };

      /**
       *得到当前选中的行数据
       */
      const getTableContent = function () {
        let selectRows = new Array();
        for (let i = 0; i < $("input[name='checkGoods']:checked").length; i++) {
          let value = {};
          let checkvalue = $("input[name='checkGoods']:checked")[i];
          value.id = $(checkvalue).attr("rowid");
          selectRows.push(value);
        }
        return selectRows;
      };

      /**
       * 更新表格
       */
      const updateTable = function () {
        let keyword = $.trim($("#keyWord").val());
        $datatables.search(keyword).draw();
      };

      const bindEvent = function () {
        /**
         *监听批删除
         */
        batchDeleteButton.on('click', function () {
          let updateds = getTableContent();
          if (updateds.length == 0) {
            utils.tools.alert('请选择要操作的记录!', {timer: 1200, type: 'warning'});
            return;
          }
          let ids = '';
          $.each(updateds, function (index, row) {
            if (ids != '') {
              ids += ',';
            }
            ids += row.id;
          });
          batchDelete(ids);
        });

        /**
         * 监听搜索
         */
        searchButton.on("click", function () {
          updateTable();
        });

        /**
         * 监听发布按钮的点击
         */
        publishButton.on("click", function () {
          window.location.href = addUrl;
        });

      };

      const bindDataTableEvent = function () {
        /**
         * 监听删除按钮的点击
         */
        $(".del").off("click").on('click', function () {
          let rowId = $(this).attr("rowId");
          utils.tools.confirm('确认删除吗？', function () {
            deleteComponent(rowId);
          }, function () {

          });
        });

        /**
         * 监听全选
         */
        $(checkAll).on('click', function () {
          $("input[name='checkGoods']").prop("checked",
              $(this).prop("checked"));
        });

        /**
         * 监听编辑按钮的点积
         */
        $(".edit").off("click").on("click", function () {
          let id = $(this).attr("rowId");
          add.edit(id);
        });

      };

      let eventInitialization = (function () {
        return {
          init: function () {
            bindEvent();
            return this;
          },
          initDataTable() {
            $datatables = utils.createDataTable(tableSource, {
              paging: true, //是否分页
              filter: false, //是否显示过滤
              lengthChange: false,
              processing: true,
              serverSide: true,
              deferRender: true,
              searching: true,
              ajax: function (data, callback, settings) {
                $.get(listUrl, {
                  size: data.length,
                  page: (data.start / data.length),
                  keyWord: data.search.value,
                  pageable: true,
                }, function (res) {
                  if (!res.data.list) {
                    res.data.list = [];
                  }
                  callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords: res.data.total,
                    iTotalDisplayRecords: res.data.total
                  });
                });
              },
              columns: [
                {
                  title: '<label class="checkbox"><input id="checkAllGoods" name="checkAllGoods" type="checkbox" class="styled"></label>',
                  orderable: false,
                  render: function (data, type, row) {
                    return '<label class="checkbox"><input name="checkGoods" type="checkbox" class="styled" rowid="'
                        + row.id + '" value="' + row.id + '"></label>';
                  }
                },
                {
                  orderable: false,
                  render: function (data, type, row) {
                    return row.logoUrl
                        ? `<img class="goods-image" src="${row.logoUrl}" />`
                        : `<img class="goods-image" src="${NOT_FOUND}" />`;
                  }
                },
                {
                  title: '名称',
                  data: 'name',
                  name: 'name',
                  sortable: false
                }, {
                  title: '排序',
                  render: function (data, type, row) {
                    let order = row.sortOrder;
                    return order;
                  }
                }, {
                  title: '是否展示',
                  orderable: false,
                  render: function (data, type, row) {
                    let display = row.isShow;
                    return display === true ? '展示' : '不展示';
                  }
                }, {
                  title: '最后更新时间',
                  orderable: false,
                  render: function (data, type, row) {
                    let cDate = parseInt(row.updatedAt);
                    let d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                  },
                  name: "updatedAt"
                }, {
                  sortable: false,
                  title: '操作',
                  render: function (data, type, row) {
                    let html = '';
                    html += `<a href="javascript:void(0);" class="edit role_check_table" rowId=${row.id} fid="edit_audit_order"><i class="icon-pencil7"></i>编辑</a>`;
                    html += `<a href="javascript:void(0);" class="del role_check_table" rowId=${row.id} fid="delete_audit_order"><i class="icon-trash"></i>删除</a>`;
                    return html;
                  }
                }],
              drawCallback: bindDataTableEvent,
            });
            return this;
          },
          initCallBack: function () {
            add.setUpdateFunc(updateTable);
            return this;
          }
        }
      })();

      eventInitialization.init().initDataTable().initCallBack();

    });
//微信账号绑定
require(['all']);


require(['brand/list','brand/add']);

define("brand", function(){});

