/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, // 月份
            "d+": this.getDate(), // 日
            "h+": this.getHours(), // 小时
            "m+": this.getMinutes(), // 分
            "s+": this.getSeconds(), // 秒
            "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
            "S": this.getMilliseconds()
            // 毫秒
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var utils = {
        post: function (url, success, data) {

            const _this = this;

            function successHandler(res) {
                if (res.errorCode === 200) {
                    const data = res.data;
                    console.log("url: ", url, ' post success: \n', data);
                    success(data);
                } else {
                    _this.tools.error(res.moreInfo);
                }
            }

            function errorHandler(err) {
                console.log(err);
                _this.tools.error('服务器错误, 请稍候再试');
            }

            if (!success || !$.isFunction(success)) {
                throw 'success function can not be null';
            }
            // 如果指定了params则约定以JSON格式提交, 默认表单格式
            const params = data && data.params ? data.params : undefined;
            if (params) {
                const stringifyJSON = JSON.stringify(params);
                console.log('posting data: ', params, " to server...");
                $.ajax({
                    url: url,
                    data: stringifyJSON,
                    type: 'POST',
                    dataType: 'JSON',
                    contentType: "application/json; charset=utf-8",
                    success: successHandler,
                    error: errorHandler
                });
                return;
            }
            $.post(url, data, function () {
                if (data) {
                    console.log('posting data: ', data, " to server...");
                }
            })
                .done(successHandler)
                .fail(errorHandler);
        },
       /**
       * 获取options html
       * @param url 服务器地址
       * @param valueMapper 函数, 获取对象中的值作为option的value
       * @param contentMapper 函数, 获取对象中的值作为option的content
       * @param dataObtainer 可选参数, 从接口返回中获取需要的集合
       * @returns {Promise<any>} promise, 可取出结果
       */
        genOptions: function(url, valueMapper, contentMapper, dataObtainer) {
            const _this = this;
            return new Promise((resolve, reject) => {
                console.log(this);
                _this.post(url, data => {
                    if (dataObtainer && $.isFunction(dataObtainer)) {
                      data = dataObtainer(data) || [];
                    }
                    try {
                        resolve(data.map(x => `<option value="${valueMapper(x)}">${contentMapper(x)}</option>`))
                    } catch (e) {
                        console.log(e);
                        reject(e)
                    }
                })
            })
        },
        postAjax: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        createDataTable: function (id, options, drawBackCallBack) {
            /** 页面表格默认配置 **/
            $.extend($.fn.dataTable.defaults, {
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>筛选:</span> _INPUT_',
                    lengthMenu: '<span>显示:</span> _MENU_',
                    info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                    paginate: {
                        'first': '首页',
                        'last': '末页',
                        'next': '&rarr;',
                        'previous': '&larr;'
                    },
                    infoEmpty: "",
                    emptyTable: "暂无相关数据",
                    zeroRecords: "暂无相关数据"
                }
            });
            var basicOption = {
                paging: true, //是否分页
                filter: false, //是否显示过滤
                lengthChange: false,
                processing: true,
                serverSide: true,
                deferRender: true,
                searching: false,
                scrollX: true,
                scrollCollapse: true,
                ordering: false,
                sortable: false,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 1
                },
                rowId: 'id',
                select: {
                    style: 'multi'
                },
                columns: [],
                ajax: function () {
                },
                drawCallback: function () {  //数据加载完成后初始化事件
                    if (drawBackCallBack) {
                        drawBackCallBack();
                    }
                }
            };
            var ajax = options.ajax;
            if (!ajax) {
                var post = options.post;
                if (!post) {
                    throw new Error('缺少表格配置参数');
                }
                ajax = function (data, callback) {
                    var param = {
                        size: data.length,
                        page: (data.start / data.length),
                        keyword: '',
                        pageable: true
                    };
                    if (options.params) {
                        param = $.extend({}, param, options.params);
                    }
                    $.get(post.url, param, function (res) {
                        if (!res.data) {
                            utils.tools.alert('数据加载失败',
                                {timer: 1200, type: 'warning'});
                            return;
                        }
                        if (!res.data.list) {
                            res.data.list = [];
                        }
                        callback({
                            recordsTotal: res.data.total,
                            recordsFiltered: res.data.total,
                            data: res.data.list,
                            iTotalRecords: res.data.total,
                            iTotalDisplayRecords: res.data.total
                        });
                    });
                };
                options.ajax = ajax;
                delete options.post;
            }
            options = options ? $.extend({}, basicOption, options)
                : basicOption;
            return $(id).DataTable(options);
        },
        postAjaxJson: function (url, data, callback) {
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'JSON',
                contentType: "application/json; charset=utf-8",
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback(-1);
                },
                complete: function () {
                    callback(0);
                }
            });
        },
        postAjaxSync: function (url, data, callback) {
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                async: false,
                success:
                    function (res) {
                        callback(res);
                    }
                ,
                error: function () {
                    callback(-1);
                }
                ,
                complete: function () {
                    callback(0);
                }
            })
            ;
        },
        getJson: function (url, data, callback) {
            $.getJSON(url, data, callback);
        },
        postAjaxWithBlock: function (element, url, data, callback, config) {

            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                timeout: 3000, //unblock after 5 seconds
                overlayCSS: {
                    backgroundColor: '#1b2024',
                    opacity: 0.8,
                    zIndex: 1200,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    color: '#fff',
                    padding: 0,
                    zIndex: 1201,
                    backgroundColor: 'transparent'
                }
            });

            var wrappedCallBack = function (res) {
                if (0 == res) { //completed
                    $.unblockUI();
                }
                callback.call(this, res);
            };
            if (config != null && config.json == true) {
                $.ajax({
                    url: url,
                    data: data,
                    contentType: "application/json",
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback(-1);
                    },
                    complete: function () {
                        callback(0);
                    }
                });
            } else {
                this.postAjax(url, data, wrappedCallBack);
            }
        },
        logout: function (success, fail) {
            var that = this;
            $.ajax({
                url: host + '/logout',
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        $(window).off('beforeunload.pro');
                        utils.tools.goLogin(1);
                    } else {
                        fail && fail(data.moreInfo);
                    }
                },
                error: function (state) {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            });
        },
        blockPage:function(){
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            timeout: 2000, //unblock after 2 seconds
            overlayCSS: {
              backgroundColor: '#1b2024',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              color: '#fff',
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        tools: {
            /**
             * [request 获取url参数]
             * @param  {[string]} param [参数名称]
             * @return {[string]}       [返回参数值]
             * @example 调用：utils.tool.request(参数名称);
             * @author apis
             */
            request: function (param) {
                var url = location.href;
                var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
                    /\&|\#/g);
                var paraObj = {}
                for (i = 0; j = paraString[i]; i++) {
                    paraObj[j.substring(0,
                        j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
                        j.length);
                }
                var returnValue = paraObj[param.toLowerCase()];
                if (typeof(returnValue) == "undefined") {
                    return "";
                } else {
                    return returnValue;
                }
            },
            goLogin: function (noMsg) {
                if (noMsg) {
                    utils.tools.alert('退出成功～');
                } else {
                    utils.tools.alert('由于您长时间没有操作，请重新登录～');
                }
                setTimeout(function () {
                    location.href = '/sellerpc/pc/login.html';
                }, 1000);
            },
            alert: function (msg, config) {
                var warning = {
                    title: msg,
                    type: "warning",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    warning.timer = config.timer;
                }

                var success = {
                    title: msg,
                    type: "success",
                    confirmButtonColor: "#2196F3",
                };

                if (config != null && config.timer != null) {
                    success.timer = config.timer;
                }

                if (config == null || config.type == null) {
                    swal(warning);
                } else if (config.type == "warning") {
                    swal(warning);
                } else if (config.type == "success") {
                    swal(success);
                }
            },
            success: function (msg) {
                this.alert(msg, {timer: 1200, type: 'success'});
            },
            error: function (msg) {
                console.log(this);
                this.alert(msg, {timer: 1200, type: 'warning'});
            },
            confirm: function (sMsg, fnConfirm = () => {
            }, fnCancel = () => {
            }) {
                swal({
                        title: "确认操作",
                        text: sMsg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "是",
                        cancelButtonText: "否"
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            fnConfirm();
                        }
                        else {
                            fnCancel();
                        }
                    });
            },
            /**
             * [获得字符串的字节长度，超出一定长度在后面加符号]
             * @param  {[String]} str  [待查字符串]
             * @param  {[Number]} len  [指定长度]
             * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
             * @param  {[String]} more [替换超出字符的符号]
             */
            getStringLength: function (str, type, len, more) {
                var str_length = 0;
                var str_len = 0;
                str_cut = new String();
                str_len = str.length;
                if (type = 1) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                    }
                    return str_length;
                }
                ;
                if (type = 2) {
                    for (var i = 0; i < str_len; i++) {
                        a = str.charAt(i);
                        str_length++;
                        if (escape(a).length > 4) {
                            str_length++;
                        }
                        str_cut = str_cut.concat(a);
                        if (str_length >= len) {
                            if (more && more.length > 0) {
                                str_cut = str_cut.concat(more);
                            }
                            return str_cut;
                        }
                    }
                    if (str_length < len) {
                        return str;
                    }
                }
            },
          confirmWihoutText: function (sMsg, fnConfirm, fnCancel) {
            swal({
                  title: sMsg,
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#FF7043",
                  confirmButtonText: "是",
                  cancelButtonText: "否"
                },
                function (isConfirm) {
                  if (isConfirm) {
                    fnConfirm();
                  }
                  else {
                    fnCancel();
                  }
                });
          },
            /**
             * 同步数据到form
             * 要求form中input的name属性跟data中key的值对应
             * @param $form 需要同步的表单jquery对象
             * @param data 同步的json数据, 可选参数，不传则清空表单
             */
            syncForm: function ($form, data) {
                if (data) {
                    $.each($form.find(':input'), function (index, item) {
                        var $item = $(item);
                        var name = $item.attr('name');
                        var value = data[name];
                        var $this = $(this);
                        if (value) {
                            if ($this.is(':radio')) {
                                if ($this.val() === value) {
                                    $this.prop('checked', true);
                                }
                            } else {
                                $item.val(value);
                            }
                        }
                    });
                } else {
                    $form[0].reset();
                }
            }
        }
    };
    return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
define('org/list',['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, datatabels, blockui, select2, tree, fileinput_zh, fileinput) {

        var $listUrl = window.host + '/org/list';
        var $columns = ['name', 'sort_no', 'is_leaf', 'created_at', 'remark'];
        var selected = {
            'id': '0',
            'parent': null
        };

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "暂无子部门"
            }
        });

        /** 添加部门 **/
        $('#add').on('click', function () {
            $('#id').val(selected.id);
            $('#modal_component_save').modal('show');
        });

        $('#update').on('click', function () {
            var id = selected.id;
            if (id === '0') {
                utils.tools.alert('请在左侧选择部门');
                return;
            }
            updateOrg(id);
        });

        $('#delete').on('click', function () {
            var id = selected.id;
            var tree = $('#tree_list').jstree(true);
            if (id === '0') {
                utils.tools.alert('请在左侧选择部门');
                return;
            }
            utils.tools.confirm("删除该部门，将同时删除该部门所有下级部门，确定删除吗?", function () {
                tree.delete_node(selected);
            }, function () {

            });
        });

        $('#saveBtn').on('click', function () {
            var url = window.host + '/org/save';
            var id = $('#id').val();
            var parent_id = $('#parent_id').val();
            var name = $('#name').val();
            var remark = $('#remark').val();
            if (!name || name == '') {
                utils.tools.alert("请输入部门名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                parentId: id,
                name: name,
                remark: remark
            };
            utils.postAjaxWithBlock($(document), url, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            utils.tools.alert('操作成功');
                            window.location.href = window.originalHost + '/org/list';
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

        $('#updateBtn').on('click', function () {
            var url = window.host + '/org/update';
            var id = $('#id_update').val();
            var parent_id = $('#parent_id_update').val();
            var name = $('#name_update').val();
            var remark = $('#remark_update').val();
            if (!name || name == '') {
                utils.tools.alert("请输入部门名称!", {timer: 1200, type: 'warning'});
                return;
            }
            var data = {
                id: id,
                parentId: parent_id,
                name: name,
                remark: remark
            };
            utils.postAjaxWithBlock($(document), url, data, function (res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200: {
                            alert("操作成功");
                            window.location.href = window.originalHost + '/org/list';
                            break;
                        }
                        default: {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            })
        });

        buttonRoleCheck('.hideClass');

        /** 初始化表格数据 **/
        var $datatables = $('#xquark_list_tables').DataTable({
            paging: true, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: false,
            ajax: function (data, callback, settings) {
                $.get($listUrl, {
                    size: data.length,
                    page: data.start / data.length,
                    parentId: selected.id,
                    pageable: true,
                    order: $columns[data.order[0].column],
                    direction: data.order[0].dir
                }, function (res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.orgTotal,
                        recordsFiltered: res.data.orgTotal,
                        data: res.data.list,
                        iTotalRecords: res.data.orgTotal,
                        iTotalDisplayRecords: res.data.orgTotal
                    });
                })
            },
            rowId: 'id',
            columns: [
                {
                    data: 'name',
                    width: '120px',
                    orderable: true,
                    name: 'name'
                },
                {
                    width: '40px',
                    orderable: true,
                    render: function (data, type, row) {
                        return row.sort_no;
                    }
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.is_leaf ? '是' : '否';
                    }
                },
                {
                    width: '60px',
                    orderable: true,
                    render: function (data, type, row) {
                        if (row.createdAt == null) return '';
                        return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                        ;
                    }
                },
                {
                    width: '80px',
                    orderable: false,
                    render: function (data, type, row) {
                        return row.remark ? row.remark : '无';
                    }
                },
                {
                    sClass: "right",
                    width: "120px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-left: 10px;" rowId="' + row.id + '" parent_id="' + row.parent_id + '" fid="edit_org"><i class="icon-pencil7" ></i>编辑</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="' + row.id + '" parent_id="' + row.parent_id + '" fid="delete_org"><i class="icon-trash"></i>删除</a>';
                        return html;
                    }
                }

            ],
            select: {
                style: 'multi'
            },
            drawCallback: function () {  //数据加载完成后初始化事件
                initEvent();
            }
        });


        function initEvent() {
            $('body').unbind('click');

            /** 表格的编辑事件 **/
            $('.edit').on('click', function () {
                var id = $(this).attr("rowId");
                updateOrg(id);
            });

            /** 点击删除部门弹出框 **/
            $("[data-toggle='popover']").popover({
                trigger: 'manual',
                placement: 'left',
                html: 'true',
                animation: true,
                content: function () {
                    var rowId = $(this).attr("rowId");
                    return '<span>确认删除？</span>' +
                        '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" id="' + rowId + '">确认</button>' +
                        '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
                }
            });

            $('[data-toggle="popover"]').popover() //弹窗
                .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                    $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

                }).on('shown.bs.popover', function () {
                var that = this;
                $('.popover-btn-ok').on("click", function () {
                    var id = $(this).attr("id");
                    var tree = $('#tree_list').jstree(true);
                    tree.delete_node(id);
                });
                $('.popover-btn-cancel').on("click", function () {
                    $(that).popover("hide");
                });
            });

            tableRoleCheck('#xquark_list_tables');

            $('body').on('click', function (event) {
                var target = $(event.target);
                if (!target.hasClass('popover') //弹窗内部点击不关闭
                    && target.parent('.popover-content').length === 0
                    && target.parent('.popover-title').length === 0
                    && target.parent('.popover').length === 0
                    && target.data("toggle") !== "popover") {
                    //弹窗触发列不关闭，否则显示后隐藏
                    $('[data-toggle="popover"]').popover('hide');
                } else if (target.data("toggle") == "popover") {
                    target.popover("toggle");
                }
            });

        }


        var jstree = $('#tree_list');
        jstree.jstree({
            "core": {
                "animation": 0,
                "check_callback": true,
                "themes": {"stripes": true},
                'data': {
                    'url': window.host + '/org/listTree',
                    'data': function (node) {
                        if (node == null) return {'id': '0'};
                        return {'id': node.id};
                    }
                }
            },
            "types": {
                "#": {
                    "max_children": 1,
                    "max_depth": 9,
                    "valid_children": ["root"]
                },
                "root": {
                    "icon": "/static/3.3.2/assets/images/tree_icon.png",
                    "valid_children": ["default"]
                },
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "plugins": [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow"
            ],
            "contextmenu": {
                "items": {
                    "create": false,
                    "rename": false,
                    "remove": false
                }
            }
        }).on('loaded.jstree', function () {
            // 只展开第一级菜单
            jstree.jstree("select_node", "ul > li:first");
            var selectedNode = jstree.jstree("get_selected");
            jstree.jstree('open_node', selectedNode, false, true);
        }).on('create_node.jstree', function (event, data) {
            // 更新节点在rename事件中执行
        }).on('rename_node.jstree', function (event, data) {
            renameOrg(event, data);
        }).on('delete_node.jstree', function (event, data) {
            deleteOrgNode(event, data);
        }).on('select_node.jstree', function (event, data) {
            onSelect(event, data);
        });

        /**
         * 新增或修改节点
         * 根据 old 是否为New node判断
         * @param event
         * @param data
         */
        function renameOrg(event, data) {
            var text = data.text;
            var old = data.old;
            var parent = data.node.parent;
            var id = data.node.id;
            var mode = old === 'New node' ? 'create' : 'update';
            switch (mode) {
                case 'create':
                    createOrg(parent, text, function (result) {
                        // 后台判断已有重复节点时返回空
                        if (result.data == null) {
                            alert(result.moreInfo);
                            $('#' + id + '_anchor').remove();
                            return;
                        }
                        var orgId = result.data.id;
                        data.instance.set_id(data.node, orgId);
                    });
                    break;
                case 'update':
                    if (id === '0') {
                        utils.tools.alert("不能修改根节点");
                        data.instance.set_text(data.node, '所有部门');
                        return;
                    }
                    updateOrgName(id, parent, text, function (result) {
                        if (result.data == null) {
                            alert(result.moreInfo);
                            data.instance.set_text(data.node, data.old);
                        }
                    });
                    break;
                default:
                    throw 'Unsupported';
            }
        }

        /**
         * 新建节点步骤:
         * 1. create_node.jstree 事件, 新建一个 New node
         * 2. rename_node.jstree 事件, 重命名新建的节点
         * @param parentId 父节点id
         * @param newName 新节点名称
         * @param callBack 创建成功后的回调函数
         */
        function createOrg(parentId, newName, callBack) {
            var params = {'name': newName, 'parentId': parentId};
            $.ajax({
                'url': window.host + '/org/save',
                'type': 'post',
                'dataType': 'json',
                'cache': false,
                'data': params,
                'timeout': 1000 * 10
            }).done(function (json) {
                callBack(json);
            }).fail(function (e) {
                Metronic.unblockUI();
                alert("亲出错了，请稍后再试");
            });
        }

        /**
         * 更新部门名称
         * @param id 部门id
         * @param name 部门名称
         * @param parentId 父节点id
         * @param callBack 更新成功后的回调函数
         */
        function updateOrgName(id, parentId, name, callBack) {
            var params = {'id': id, 'parentId': parentId, 'name': name};
            $.ajax({
                'url': window.host + '/org/update',
                'type': 'post',
                'dataType': 'json',
                'cache': false,
                'data': params,
                'timeout': 1000 * 10
            }).done(function (json) {
                callBack(json);
            }).fail(function (e) {
                alert("亲出错了，请稍后再试");
            })
        }

        /**
         * 删除节点
         * @param event
         * @param data
         */
        function deleteOrgNode(event, data) {
            var id = data.node.id;
            console.log(id);
            var params = {'id': id};
            var url = window.host + "/org/delete";
            utils.postAjax(url, params, function (res) {
                utils.tools.alert('操作成功');
                $datatables.search('').draw();
            });
        }

        /*手动删除*/
        function deleteOrg(id) {
            var params = {'id': id};
            var url = window.host + "/org/delete";
            utils.postAjax(url, params, function (res) {
                utils.tools.alert('操作成功');
                $datatables.search('').draw();
            });
        }

        /**手动更新**/
        function updateOrg(id) {
            $.ajax({
                url: window.host + '/org/' + id,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.errorCode == 200) {
                        var org = data.data;
                        $("#id_update").val(org.id);
                        $("#parent_id_update").val(org.parent_id);
                        $("#name_update").val(org.name);
                        $("#remark_update").val(org.remark);
                        $("#modal_component_update").modal("show");
                    } else {
                        alert(data.moreInfo);
                    }
                },
                error: function (state) {
                    if (state.status == 401) {
                        utils.tools.goLogin();
                    } else {
                        utils.tools.alert('服务器暂时没有响应，请稍后重试...');
                    }
                }
            });
        }

        /**
         * jsTree选中节点是刷新表格
         * @param event
         * @param data
         */
        function onSelect(event, data) {
            selected = data.node;
            console.log(selected);
            $datatables.search('').draw();
        }


    }
);

/**
 * Created by wangxinhua on 17-3-22.
 */

define('org/orgMerchant',['jquery', 'utils', 'datatables', 'blockui', 'select2', 'tree', 'fileinput_zh', 'fileinput'], function ($, utils, datatabels, blockui, select2, tree, fileinput_zh, fileinput) {

    /** 表格列名数组,根据索引确定排序 **/
    /** 第一个位置存放默认值 **/
    var $columns = ['name', 'loginname', 'name', 'phone', 'orgName', 'createdAt', 'rolesDesc'];

    var $listUrl = '/sellerpc/org/listMerchant';

    var merchantId = '';

    var $userId = null;

    /** jsTree当前选中的节点 **/
    var $selected = {
        'id': null,
        'parent': '#'
    };

    initPage();

    /** 初始化表格数据 **/
    var $merchant_table = utils.createDataTable('#xquark_merchant_tables', {
        paging: true, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function (data, callback, settings) {
            $.get($listUrl, {
                size: data.length,
                page: (data.start / data.length) +1,
                orgId: $selected.id,
                pageable: true,
                keyword: data.search.value,
            }, function (res) {
                if (res.data) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    if (res.data.orgName) $selected.orgName = res.data.orgName;
                    $userId = res.data.userId;
                } else {
                    utils.tools.alert('数据加载失败');
                }
                callback({
                    recordsTotal: res.data.merchantTotal,
                    recordsFiltered: res.data.merchantTotal,
                    data: res.data.list,
                    iTotalRecords: res.data.merchantTotal,
                    iTotalDisplayRecords: res.data.merchantTotal,
                });
            })
        },
        rowId: 'id',
        columns: [
            {
              title: "账号",
                orderable: false,
                render: function (data, type, row) {
                    return row.loginname;
                }
            },
            {
              title: "昵称",
                data: 'name',
                orderable: true,
                name: 'name'
            },
            {
              title: "手机号",
                orderable: false,
                render: function (data, type, row) {
                    return row.phone;
                }
            },
            {
              title: "创建时间",
                orderable: true,
                render: function (data, type, row) {
                    if (row.createdAt == null) return '';
                    return new Date(row.createdAt).format('yyyy-MM-dd hh:mm:ss');
                }
            },
            {
              title: "角色",
                orderable: false,
                render: function (data, type, row) {
                    return row.rolesDesc && row.rolesDesc != '' ? row.rolesDesc : '无';
                }
            },
            {
                sClass: "right",
                width: "140px",
                orderable: false,
                render: function (data, type, row) {
                    var html = '';
                    html += '<a href="javascript:void(0);" class="edit role_check_table" style="margin-right: 10px;" rowId="'+row.id+'" fid="edit_merchant"><i class="icon-pencil7" ></i>编辑</a>';
                  if ($userId && $userId != row.id && !row.admin) {
                        html += '<a href="javascript:void(0);" class="settings role_check_table"  style="margin-left: 10px;" rowId="'+row.id+'" fid="edit_merchant_role"><i class="icon-checkbox-checked"></i>角色</a>';
                        html += '<a href="javascript:void(0);" class="del role_check_table" style="margin-left: 10px;" data-toggle="popover" rowId="'+row.id+'" fid="delete_merchant"><i class="icon-trash" ></i>删除</a>';
                    }
                    return html;
                }
            }

        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成后初始化事件
            initEvent();
        }
    });

    // 选择角色表单
    var $roledatatables = $('#xquark_select_roles_tables').DataTable({
        paging: false, //是否分页
        filter: false, //是否显示过滤
        lengthChange: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        ajax: function(data, callback, settings) {
            $.get("/sellerpc/merchantRole/allList", {
                keyword: '',
                keyword: data.search.value,
                merchantId: merchantId,
                pageable: false
            }, function(res) {
                if (!res.data.list) {
                    res.data.list = [];
                } else{
                    // if (res.data.shopId) {
                    //     $shopId = res.data.shopId;
                    // }
                }
                callback({
                    recordsTotal: res.data.total,
                    recordsFiltered: res.data.total,
                    data: res.data.list,
                    iTotalRecords:res.data.total,
                    iTotalDisplayRecords:res.data.total
                });
            });
        },
        rowId:"id",
        columns: [
            {
                width: "15px",
                orderable: false,
                render: function(data, type, row){
                    var str = '<label class="checkbox"><input name="checkRoles" type="checkbox" class="styled"';
                    if(row.isSelect){
                        str = str + ' checked="checked" ';
                    }
                    str = str + ' value="'+row.roleName+'"></label>';
                    return str;
                }
            },
            {
                data: "roleName",
                width: "120px",
                orderable: false,
                name:"roleName"
            },{
                data: "roleDesc",
                width: "120px",
                orderable: false,
                name:"roleDesc"
            }
            , {
                orderable: false,
                width: "180px",
                render: function (data, type, row) {
                    var cDate = parseInt(row.createdAt);
                    var d = new Date(cDate);
                    return d.format('yyyy-MM-dd hh:mm:ss');
                },
                name:"createdAt"
            }
        ],
        select: {
            style: 'multi'
        },
        drawCallback: function () {  //数据加载完成
        }
    });

    function initEvent() {
        $('body').unbind('click');

        $('.move').on('click', function () {
            var id = $(this).attr('rowId');
            var orgId = $(this).attr('orgId');
            var orgName = $(this).attr('orgName');
            $('#currentOrg').val(orgName != null && orgName != 'null' ? orgName : '无');
            $('#merchantId').val(id);
            $('#oldOrgId').val(orgId);
            $('#modal_merchant_update').modal('show');
        });

        $(".edit").on("click",function(){
            var id =  $(this).attr("rowId");
            window.location.href="/sellerpc/merchant/admin/edit?merchantId="+id;
        });

        $(".settings").on("click",function(){
            merchantId =  $(this).attr("rowId");
            $roledatatables.search('').draw();
            $("#modal_select_roles").modal("show");
            // 全选
            $("#checkAllRoles").on('click', function() {
                $("[name=checkRoles]:checkbox").prop('checked', $(this).prop('checked'));
            });

        });

        /** 点击删除merchant弹出框 **/
        $("[data-toggle='popover']").popover({
            trigger: 'click',
            placement: 'left',
            html: 'true',
            animation:true,
            content: function() {
                var rowId =  $(this).attr("rowId");
                return '<span>确认删除？</span>' +
                    '<button type="button" class="btn btn-sm btn-primary ml10 popover-btn-ok" param="'+rowId+'">确认</button>' +
                    '<button type="button" class="btn btn-sm btn-default ml5 popover-btn-cancel">取消</button>';
            }
        });

        $('[data-toggle="popover"]').popover() //弹窗
            .on('show.bs.popover', function () { //展示时,关闭非当前所有弹窗
                $(this).parent().parent().siblings().find('[data-toggle="popover"]').popover('hide');

            }).on('shown.bs.popover', function () {
            var that = this;
            $('.popover-btn-ok').on("click",function(){
                var param = $(this).attr("param");
                deleteMerchant(param);
            });
            $('.popover-btn-cancel').on("click",function(){
                $(that).popover("hide");
            });
        });

        tableRoleCheck('#xquark_merchant_tables');

        $('body').on('click', function (event) {
            var target = $(event.target);
            if (!target.hasClass('popover') //弹窗内部点击不关闭
                && target.parent('.popover-content').length === 0
                && target.parent('.popover-title').length === 0
                && target.parent('.popover').length === 0
                && target.data("toggle") !== "popover") {
                //弹窗触发列不关闭，否则显示后隐藏
                $('[data-toggle="popover"]').popover('hide');
            } else if (target.data("toggle") == "popover") {
                target.popover("toggle");
            }
        });
    }

    var jstree = $('#tree_merchant');
    jstree.jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            'data': {
                'url': window.host + '/org/listTree',
                'data': function (node) {
                    if (node == null) return {'id': '0'};
                    return {'id': node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 9,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.2/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow"
        ],
        "contextmenu": {
            "items": {
                "create": false,
                "rename": false,
                "remove": false
            }
        }
    }).on('loaded.jstree', function () {
        // 只展开第一级菜单
        jstree.jstree("select_node", "ul > li:first");
        var selectedNode = jstree.jstree("get_selected");
        jstree.jstree('open_node', selectedNode, false, true);
    }).on('select_node.jstree', function (event, data) {
        onSelect(event, data);
    });

    /**
     * jsTree选中节点是刷新表格
     * @param event
     * @param data
     */
    function onSelect(event, data) {
        $selected = data.node;
        $merchant_table.search('').draw();
    }

    /**
     * ajax更新部门信息
     * @param url
     * @param id
     * @param org_id
     */
    function updateMerchantOrg(id, orgId, callback) {
        var url = window.host + '/org/updateMerchantOrg';
        var params = {
            'merchantId': id,
            'orgId': orgId ? orgId : '0'
        };
        $.ajax(url, {
            'type': 'post',
            'dataType': 'json',
            'cache': false,
            'data': params,
            'timeout': 1000 * 10
        }).done(function (result) {
            if (result.data == null) {
                utils.tools.alert(result.moreInfo);
                return;
            }
            if (callback) callback(result);
        }).fail(function (e) {
            Metronic.unblockUI();
            utils.tools.alert("亲出错了，请稍后再试");
        });
    }

    /** 初始化页面信息 **/
    function initPage() {

        $(".btn-admin-search").on('click', function() {
            var keyword = $.trim($("#sKeyword").val());
            $merchant_table.search( keyword ).draw();
        });

        $(".btn-admin-add").on('click', function() {
            location.href = '/sellerpc/merchant/admin/add';
        });

        buttonRoleCheck('.hideClass');

        // 初始化部门选择框
        $.ajax({
            url: window.host + '/org/list',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.errorCode == 200) {
                    var dataLength = data.data.orgTotal;
                    var orgNames = $('#orgNames');
                    orgNames.empty();
                    orgNames.append('<option selected="selected">请选择新部门</option>');
                    for (var i = 0; i < dataLength; i++) {
                        orgNames.append('<option value=' + data.data.list[i].id + '>' + data.data.list[i].name + '</option>');
                    }
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200, type: 'warning'});
                }
            },
            error: function (state) {
                if (state.status == 401) {
                } else {
                    utils.tools.alert('获取部门信息失败！', {timer: 1200, type: 'warning'});
                }
            }
        });

        /** 页面表格默认配置 **/
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: {'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;'},
                infoEmpty: "",
                emptyTable: "部门下暂无用户"
            }
        });

        $('#update_merchant_btn').on('click', function () {
            var merchantId = $('#merchantId').val();
            var oldOrgId = $('#oldOrgId').val();
            var newOrgId = $('#orgNames').val();
            if (newOrgId == '请选择新部门') {
                utils.tools.alert('请先选择部门');
                return;
            }
            if (newOrgId == oldOrgId) {
                utils.tools.alert('用户已经在该部门中');
                return;
            }
            updateMerchantOrg(merchantId, newOrgId, function (data) {
                if (data.errorCode == '200') {
                    utils.tools.alert('操作成功');
                    $merchant_table.search('').draw();
                    $('#modal_merchant_update').modal('hide');
                } else {
                    utils.tools.alert(data.moreInfo, {timer: 1200});
                }
            })
        });

        // 管理员选择角色确认
        $(".changeRoleBtn").on('click', function() {
            var roles = "";
            $("[name=checkRoles]:checkbox").each(function(){ //遍历table里的全部checkbox
                //如果被选中
                if($(this).prop("checked")){
                    if(roles == ''){
                        roles = $(this).val();
                    }else{
                        roles = roles + ',' + $(this).val();
                    }
                }
            });
            var url = "/sellerpc/merchantRole/update";
            var data = {
                merchantId: merchantId,
                roles: roles
            };
            utils.postAjax(url,data,function(res){
                if(typeof(res) === 'object'){
                    $("#modal_select_roles").modal("hide");
                    if (res.rc == 1){
                        utils.tools.alert('设置成功',{timer: 1200, type: 'success'});
                        $merchant_table.search('').draw();
                    } else{
                        utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                    }
                }
            });
        });
    }

    function  deleteMerchant(paramId){
        var url = "/sellerpc/merchant/admin/delete";
        var data = {
            merchantId: paramId
        };
        utils.postAjax(url,data,function(res){
            if(typeof(res) === 'object'){
                if (res.rc == 1){
                    $merchant_table.search('').draw();
                    utils.tools.alert('删除成功');
                } else{
                    utils.tools.alert(res.msg,{timer: 1200, type: 'warning'});
                }
            }
        });
    }

});

require(['all']);

require(['org/list', 'org/orgMerchant']);

define("org", function(){});

