/**
 * Created by quguangming on 16/5/19.
 */
define('utils',['jquery', 'sweetAlert', 'blockui'], function () {

  Date.prototype.format = function (fmt) {
    var o = {
      "M+": this.getMonth() + 1, // 月份
      "d+": this.getDate(), // 日
      "h+": this.getHours(), // 小时
      "m+": this.getMinutes(), // 分
      "s+": this.getSeconds(), // 秒
      "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
      "S": this.getMilliseconds()
      // 毫秒
    };
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp("(" + k + ")").test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
            : (("00" + o[k]).substr(("" + o[k]).length)));
      }
    }
    return fmt;
  }

  var utils = {
    post: function (url, success, data) {
      if (!success || !$.isFunction(success)) {
        throw 'success function can not be null';
      }
      $.post(url, data, function () {
        if (data) {
          console.log('posting data: ' + JSON.stringify(data) + " to server...");
        }
      })
      .done((res) => {
        if (res.errorCode === 200) {
          var data = res.data;
          console.log("url: ", url, ' post success: \n', data);
          success(data);
        } else {
          this.tools.error(res.moreInfo);
        }
      })
      .fail((err) => {
        console.log(err);
        this.tools.error('服务器错误, 请稍候再试');
      });
    },
    postAjax: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxJson: function (url, data, callback) {
      $.ajax({
        url: url,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'JSON',
        contentType: "application/json; charset=utf-8",
        success: function (res) {
          callback(res);
        },
        error: function () {
          callback(-1);
        },
        complete: function () {
          callback(0);
        }
      });
    },
    postAjaxSync: function (url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: 'POST',
        dataType: 'JSON',
        async: false,
        success:
            function (res) {
              callback(res);
            }
        ,
        error: function () {
          callback(-1);
        }
        ,
        complete: function () {
          callback(0);
        }
      })
      ;
    },
    getJson: function (url, data, callback) {
      $.getJSON(url, data, callback);
    },
    postAjaxWithBlock: function (element, url, data, callback, config) {

      $.blockUI({
        message: '<i class="icon-spinner4 spinner"></i>',
        timeout: 3000, //unblock after 5 seconds
        overlayCSS: {
          backgroundColor: '#1b2024',
          opacity: 0.8,
          zIndex: 1200,
          cursor: 'wait'
        },
        css: {
          border: 0,
          color: '#fff',
          padding: 0,
          zIndex: 1201,
          backgroundColor: 'transparent'
        }
      });

      var wrappedCallBack = function (res) {
        if (0 == res) { //completed
          $.unblockUI();
        }
        callback.call(this, res);
      };
      if (config != null && config.json == true) {
        $.ajax({
          url: url,
          data: data,
          contentType: "application/json",
          type: 'POST',
          dataType: 'JSON',
          success: function (res) {
            callback(res);
          },
          error: function () {
            callback(-1);
          },
          complete: function () {
            callback(0);
          }
        });
      } else {
        this.postAjax(url, data, wrappedCallBack);
      }
    },
    logout: function (success, fail) {
      var that = this;
      $.ajax({
        url: host + '/logout',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
          if (data.errorCode == 200) {
            $(window).off('beforeunload.pro');
            utils.tools.goLogin(1);
          } else {
            fail && fail(data.moreInfo);
          }
        },
        error: function (state) {
          fail && fail('服务器暂时没有响应，请稍后重试...');
        }
      });
    },
    tools: {
      /**
       * [request 获取url参数]
       * @param  {[string]} param [参数名称]
       * @return {[string]}       [返回参数值]
       * @example 调用：utils.tool.request(参数名称);
       * @author apis
       */
      request: function (param) {
        var url = location.href;
        var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
            /\&|\#/g);
        var paraObj = {}
        for (i = 0; j = paraString[i]; i++) {
          paraObj[j.substring(0,
              j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
              j.length);
        }
        var returnValue = paraObj[param.toLowerCase()];
        if (typeof(returnValue) == "undefined") {
          return "";
        } else {
          return returnValue;
        }
      },
      goLogin: function (noMsg) {
        if (noMsg) {
          utils.tools.alert('退出成功～');
        } else {
          utils.tools.alert('由于您长时间没有操作，请重新登录～');
        }
        setTimeout(function () {
          location.href = '/sellerpc/pc/login.html';
        }, 1000);
      },
      alert: function (msg, config) {
        var warning = {
          title: msg,
          type: "warning",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          warning.timer = config.timer;
        }

        var success = {
          title: msg,
          type: "success",
          confirmButtonColor: "#2196F3",
        };

        if (config != null && config.timer != null) {
          success.timer = config.timer;
        }

        if (config == null || config.type == null) {
          swal(warning);
        } else if (config.type == "warning") {
          swal(warning);
        } else if (config.type == "success") {
          swal(success);
        }
      },
      success: function (msg) {
        this.alert(msg, {timer: 1200, type: 'success'});
      },
      error: function (msg) {
        console.log(this);
        this.alert(msg, {timer:1200, type: 'warning'});
      },
      confirm: function (sMsg, fnConfirm, fnCancel) {
        swal({
              title: "确认操作",
              text: sMsg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#FF7043",
              confirmButtonText: "是",
              cancelButtonText: "否"
            },
            function (isConfirm) {
              if (isConfirm) {
                fnConfirm();
              }
              else {
                fnCancel();
              }
            });
      },
      /**
       * [获得字符串的字节长度，超出一定长度在后面加符号]
       * @param  {[String]} str  [待查字符串]
       * @param  {[Number]} len  [指定长度]
       * @param  {[Number]} type [1为返回长度,2为截取后的字符串]
       * @param  {[String]} more [替换超出字符的符号]
       */
      getStringLength: function (str, type, len, more) {
        var str_length = 0;
        var str_len = 0;
        str_cut = new String();
        str_len = str.length;
        if (type = 1) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
          }
          return str_length;
        }
        ;
        if (type = 2) {
          for (var i = 0; i < str_len; i++) {
            a = str.charAt(i);
            str_length++;
            if (escape(a).length > 4) {
              str_length++;
            }
            str_cut = str_cut.concat(a);
            if (str_length >= len) {
              if (more && more.length > 0) {
                str_cut = str_cut.concat(more);
              }
              return str_cut;
            }
          }
          if (str_length < len) {
            return str;
          }
        }
      },
      /**
       * 同步数据到form
       * 要求form中input的name属性跟data中key的值对应
       * @param $form 需要同步的表单jquery对象
       * @param data 同步的json数据, 可选参数，不传则清空表单
       */
      syncForm: function ($form, data) {
        if (data) {
          $.each($form.find(':input'), function (index, item) {
            var $item = $(item);
            var name = $item.attr('name');
            var value = data[name];
            if (value) {
              $item.val(value);
            }
          });
        } else {
          $form[0].reset();
        }
      }
    }
  };
  return utils;

});
/**
 * Created by quguangming on 16/5/20.
 */

//公共组件定义
require(['utils']);

define('all',['jquery', 'utils'], function (jquery, utils) {

    //登出
    $('.j_logout').on('click', function () {
        utils.tools.confirm('确认退出当前账户？', function () {
            window.location.href = '/sellerpc/pc/login.html';
        }, function () {

        });
    });

});
/**
 * Created by quguangming on 16/5/18.
 */

define('form/validate',["jquery","validate"],function($, validate){

    $.extend($.validator.messages, {
        required: "必须填写",
        remote: "请修正此栏位",
        email: "请输入有效的电子邮件",
        url: "请输入有效的网址",
        date: "请输入有效的日期",
        dateISO: "请输入有效的日期 (YYYY-MM-DD)",
        number: "请输入正确的数字",
        digits: "只可输入数字",
        creditcard: "请输入有效的信用卡号码",
        equalTo: "你的输入不相同",
        extension: "请输入有效的后缀",
        maxlength: $.validator.format("最多 {0} 个字"),
        minlength: $.validator.format("最少 {0} 个字"),
        rangelength: $.validator.format("请输入长度为 {0} 至 {1} 之間的字串"),
        range: $.validator.format("请输入 {0} 至 {1} 之间的数值"),
        max: $.validator.format("请输入不大于 {0} 的数值"),
        min: $.validator.format("请输入不小于 {0} 的数值")
    });


    $.validator.addMethod( "pattern", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }
        if ( typeof param === "string" ) {
            param = new RegExp( "^(?:" + param + ")$" );
        }
        return param.test( value );
    }, "Invalid format." );


     return function(formObj,config) {

            formObj.validate({
                errorClass: config && config.errorClass && config.errorClass.length > 0  ? config.errorClass :'validation-error-label',
                successClass: config && config.successClass && config.successClass.length > 0  ? config.successClass : 'validation-valid-label',
                highlight: function (element, errorClass, validClass) {
                    //$(errorLabel).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    //$(errorLabel).removeClass(errorClass);
                },
                // Different components require proper error label placement
                errorPlacement: function (error, element) {

                    // Styled checkboxes, radios, bootstrap switch
                    if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                        if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                            error.appendTo(element.parent().parent().parent().parent());
                        }
                        else {
                            error.appendTo(element.parent().parent().parent().parent().parent());
                        }
                    }

                    // Unstyled checkboxes, radios
                    else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                        error.appendTo(element.parent().parent().parent());
                    }

                    // Input with icons and Select2
                    else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                        error.appendTo(element.parent());
                    }

                    // Inline checkboxes, radios
                    else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo(element.parent().parent());
                    }

                    // Input group, styled file input
                    else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                        error.appendTo(element.parent().parent());
                    }

                    else {
                        error.insertAfter(element);
                    }
                },
                validClass: config && config.successClass && config.successClass.length > 0 ? config.successClass : "validation-valid-label",
                success: function (label) {
                    $(label).addClass(this.validClass);
                    if ( !(config && config.successClass && config.successClass.length > 0)) {
                        $(label).css("display", "block");
                    } else{
                        if (config.setSuccessText){
                            config.setSuccessText(label);
                        } else{
                            $(label).text("ok");
                        }
                    }
                },
                showErrors: function (errorMap, errorList) {
                    this.defaultShowErrors();
                    $.each(errorList, function (i, error) {
                        $(error).css("display", "block");
                    });
                },
                focusCleanup: false,
                rules: config.rules,
                messages: config.messages,
                submitHandler: config && config.submitCallBack ? config.submitCallBack: function (form) {},
                invalidHandler: config && config.invalidCallBack ? config.invalidCallBack :  function(form, validator) {}
            });
    }

});
define('wechat/autoReply',['jquery','utils','datatables','bootbox','select2','blockui', 'form/validate', 'switch'],
    function($, utils, validate, bswitch) {

        $('#myTab a').click(function (e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
        })

        /** 页面表格默认配置 **/
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>筛选:</span> _INPUT_',
                lengthMenu: '<span>显示:</span> _MENU_',
                info: "当前 _START_-_END_ 条 共 _TOTAL_ 条",
                paginate: { 'first': '首页', 'last': '末页', 'next': '&rarr;', 'previous': '&larr;' },
                infoEmpty: "",
                emptyTable:"暂无相关数据"
            }
        });

        /** 初始化选择框控件 **/
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });

        getAutoBack();
        getAddBack();

        // 获取微信自动回复
        function getAutoBack() {
            $.get(window.originalHost + '/wechat/getAutoBack', {}, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            $("#autoReplyContent").val(res.data.replyContent);
                            $("#autoAutoReplyId").val(res.data.id);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        }

        // 获取被添加自动回复
        function getAddBack() {
            $.get(window.originalHost + '/wechat/getAddBack', {}, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            $("#addReplyContent").val(res.data.replyContent);
                            $("#addAutoReplyId").val(res.data.id);
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        }




        // 所有自动回复
        var $datatables = $('#guiderUserTable').DataTable({
            paging: false, //是否分页
            filter: false, //是否显示过滤
            lengthChange: false,
            processing: true,
            serverSide: true,
            deferRender: true,
            searching: true,
            ordering: false,
            sortable: false,
            ajax: function(data, callback, settings) {
                $.get(window.originalHost + '/wechat/listWechatAutoReply', {
                    size: data.length,
                    page: (data.start / data.length),
                    pageable: true,
                }, function(res) {
                    if (!res.data.list) {
                        res.data.list = [];
                    }
                    callback({
                        recordsTotal: res.data.total,
                        recordsFiltered: res.data.total,
                        data: res.data.list,
                        iTotalRecords: res.data.total,
                        iTotalDisplayRecords: res.data.total
                    });
                });
            },
            rowId:"id",
            columns: [
                {
                    data: "name",
                    name: "name",
                    width: "100px",
                    orderable: false,
                },{
                    orderable: false,
                    width:'75px',
                    data: 'matchType',
                    name: 'matchType',
                    render: function (data, type, row) {
                        var value = row.matchType;
                        if (value == 'LITERAL') {
                            return "精确匹配";
                        }else if (value == 'LIKE') {
                            return "模糊匹配";
                        } else{
                            return "";
                        }
                    }
                }, {
                    data: "condition",
                    name: "condition",
                    width: "100px",
                    orderable: false,
                }, {
                    data: "replyContent",
                    name: "replyContent",
                    width: "200px",
                    orderable: false,
                }, {
                    orderable: false,
                    width: "180px",
                    render: function (data, type, row) {
                        var cDate = parseInt(row.createdAt);
                        var d = new Date(cDate);
                        return d.format('yyyy-MM-dd hh:mm:ss');
                    },
                    name:"createdAt"
                }
                , {
                    width: "180px",
                    orderable: false,
                    render: function (data, type, row) {
                        var html = '';
                        html = html + '<a class="editBtn role_check_table" href="#"  style="margin-right: 10px;" rowId="' + row.id + '" fid="delete_order"> 修改</a>';
                        html = html + '<a class="delBtn role_check_table" href="#" style="margin-right: 10px;" rowId="' + row.id + '" fid="delete_order"> 删除</a>';
                        return html;
                    }
                }
            ],
            drawCallback: function () {  //数据加载完成
                initEvent();
            }
        });
        
        function initEvent() {
            $(".editBtn").on('click', function () {
                var id = $(this).attr("rowId");
                $.get(window.originalHost + '/wechat/viewDetail', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                $("#autoReplyName").val(res.data.name);
                                $("#keyword").val(res.data.condition);
                                $("#matchType").val(res.data.matchType);
                                $("#autoReplyId").val(res.data.id);
                                $("#replyContent").val(res.data.replyContent);
                                /** 初始化选择框控件 **/
                                $('.select').select2({
                                    minimumResultsForSearch: Infinity,
                                });
                                $("#modal_newInfo").modal('show');
                                break;
                            }
                            default: {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("查看详情失败", {timer: 1200});
                    }
                });
            });

            $(".delBtn").on('click', function () {
                var id = $(this).attr("rowId");
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $datatables.search('').draw();
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            });

        }

        $(".btn-search").on('click', function() {
            $datatables.search('').draw();
        });

        $(".btn-release").on('click', function() {
            $("#autoReplyName").val('');
            $("#keyword").val('');
            $("#autoReplyId").val('');
            $("#replyContent").val('');
            $("#modal_newInfo").modal("show");
        });

        // 保存被添加自动回复内容
        $("#saveWechatAddReplyBtn").on('click', function() {
            if($("#addReplyContent").val() == ''){
                utils.tools.alert("请输入回复内容", {timer: 1200, type: 'success'});
                return;
            }
            var data = {
                id: $("#addAutoReplyId").val(),
                name: 'ADD',
                condition: 'ADD',
                conditionType: $("#addConditionType").val(),
                matchType: 'ADD',
                replyContent: $("#addReplyContent").val(),
            };

            $.post(window.originalHost + '/wechat/saveWechatAutoReply', data, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            getAddBack();
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        });

        // 删除被添加自动回复内容
        $("#delWechatAddReplyBtn").on('click', function() {
            var id = $("#addAutoReplyId").val();
            if(id != ''){
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $("#addAutoReplyId").val('')
                                    $("#addReplyContent").val('')
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            }

        });

        // 保存自动回复内容
        $("#saveWechatAutoReplyBtn").on('click', function() {
            if($("#autoReplyContent").val() == ''){
                utils.tools.alert("请输入回复内容", {timer: 1200, type: 'success'});
                return;
            }
            var data = {
                id: $("#autoAutoReplyId").val(),
                name: 'AUTO',
                condition: 'AUTO',
                conditionType: $("#autoConditionType").val(),
                matchType: 'AUTO',
                replyContent: $("#autoReplyContent").val(),
            };

            $.post(window.originalHost + '/wechat/saveWechatAutoReply', data, function(res) {
                if (typeof(res) === 'object') {
                    switch (res.errorCode) {
                        case 200:
                        {
                            utils.tools.alert("保存成功", {timer: 1200, type: 'success'});
                            getAutoBack();
                            break;
                        }
                        default:
                        {
                            utils.tools.alert(res.moreInfo, {timer: 1200});
                            break;
                        }
                    }
                } else if (res == 0) {

                } else if (res == -1) {
                    utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                }
            });
        });

        // 删除自动回复内容
        $("#delWechatAutoReplyBtn").on('click', function() {
            var id = $("#autoAutoReplyId").val();
            if(id != ''){
                $.get(window.originalHost + '/wechat/deleteAutoReply', {"id" : id}, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200: {
                                if(res.data){
                                    utils.tools.alert('删除成功', {timer: 1200});
                                    $("#autoAutoReplyId").val('');
                                    $("#autoReplyContent").val('');
                                }else{
                                    utils.tools.alert('删除失败', {timer: 1200});
                                }
                            }
                            default: {
                                utils.tools.alert('删除成功', {timer: 1200});
                                break;
                            }
                        }
                    } else if (res == 0) {
                    } else if (res == -1) {
                        utils.tools.alert("删除失败", {timer: 1200});
                    }
                });
            }

        });
        
        // ------------------------------
        // 模板消息保存
        // ------------------------------
        var autoReplyForm = {
            form: $('#autoReplyForm'),

            autoReplyName: $("#autoReplyForm input[name=autoReplyName]"),
            keyword: $("#autoReplyForm input[name=keyword]"),
            conditionType: $("#autoReplyForm input[name=conditionType]"),
            matchType: $("#autoReplyForm select[name=matchType]"),
            replyContent: $("#autoReplyForm input[name=replyContent]"),
            autoReplyId: $("#autoReplyForm input[name=autoReplyId]"),

            saveWechatKeywordReplyBtn: $('#saveWechatKeywordReplyBtn'),
            url: window.originalHost + '/wechat/saveWechatAutoReply',

            rules: {
                autoReplyName: {
                    required: true,
                    minlength: 1,
                    maxlength: 32,
                },
                keyword: {
                    required: true,
                    minlength: 1,
                    maxlength: 32,
                },
                matchType: {
                    required: true,
                },
                replyContent: {
                    required: true,
                    minlength: 1,
                    maxlength: 512,
                }
            },

            messages: {
                autoReplyName: {
                    required: '请输入公众号名称',
                    minlength: '请至少输入一个字符',
                    maxlength:'公众号名称长度错误',
                },
                keyword: {
                    required: '请输入关键字',
                    minlength: '请至少输入一个字符',
                    maxlength:'关键字长度错误',
                },
                matchType: {
                    required: '请选择匹配类型',
                },
                replyContent: {
                    required: '请输入返回内容',
                    minlength: '请至少输入一个字符',
                    maxlength:'返回内容长度错误',
                },
            },

            save: function() {

                if(this.autoReplyName.val() == ''){
                    utils.tools.alert("请输入规则名称", {timer: 1200, type: 'success'});
                    return;
                }
                if(this.keyword.val() == ''){
                    utils.tools.alert("请输入规则关键字", {timer: 1200, type: 'success'});
                    return;
                }
                if(this.replyContent.val() == ''){
                    utils.tools.alert("请输入返回内容", {timer: 1200, type: 'success'});
                    return;
                }

                var data = {
                    id: this.autoReplyId.val(),
                    name: this.autoReplyName.val(),
                    condition: this.keyword.val(),
                    conditionType: this.conditionType.val(),
                    matchType: this.matchType.val(),
                    replyContent: this.replyContent.val(),
                };

                $.post(this.url, data, function(res) {
                    if (typeof(res) === 'object') {
                        switch (res.errorCode) {
                            case 200:
                            {
                                utils.tools.alert("自动回复规则保存成功", {timer: 1200, type: 'success'});
                                $datatables.search('').draw();
                                //location.reload();
                                break;
                            }
                            default:
                            {
                                utils.tools.alert(res.moreInfo, {timer: 1200});
                                $datatables.search('').draw();
                                break;
                            }
                        }
                    } else if (res == 0) {

                    } else if (res == -1) {
                        utils.tools.alert("网络问题,请稍后再试", {timer: 1200});
                    }
                });

            },

            init: function() {

                $(this.saveWechatKeywordReplyBtn).on('click',function() {
                    autoReplyForm.save();
                });
                var validateConfig = {
                    rules: this.rules,
                    messages: this.messages,
                    submitCallBack: function (form) {
                        autoReplyForm.save();
                    }
                };
                validate(this.form, validateConfig);
            }
        };

        //初始化
        autoReplyForm.init();
    });
//微信账号绑定
require(['all']);


require(['wechat/autoReply']);

define("wechatAutoReply", function(){});

