/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' },
        { name: 'about' }
    ];


    config.image_previewText =' ';
  config.height = 500;

    // 默认语言
    config.defaultLanguage = 'zh-cn';

    // 设置图片上传回调地址
    config.filebrowserUploadUrl = window.host + '/file/editor/upload/qiniuVideo';
    config.filebrowserImageUploadUrl= window.host + '/file/editor/upload/image';
    // config.filebrowserVideoBrowseUrl = window.host+'/appNotification/updateImg';
    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    //config.removeDialogTabs = 'image:advanced;link:advanced';

    //插件
    config.extraPlugins = 'html5video,widget,lineutils,clipboard,widgetselection,filebrowser,image2,imageresize';

};
