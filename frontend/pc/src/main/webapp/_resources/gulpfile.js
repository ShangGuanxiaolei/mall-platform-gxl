/* ------------------------------------------------------------------------------
 *
 *  # Gulp file
 *
 *  Basic Gulp tasks for Limitless template
 *
 *  Version: 1.0
 *  Latest update: Dec 2, 2015
 *
 * ---------------------------------------------------------------------------- */

// Include gulp
var gulp = require('gulp');
var plugins = require('gulp-load-plugins');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefix = new LessPluginAutoPrefix({
  browsers: ["last 5 versions"],
  cascade: true
});

/* 简单处理参数传递 传递方式 --a xxx --b xxx */
const args = (function (argList) {
  var curArg;
  return argList.reduce(function (result, item) {
    // 参数通过=分割传递
    var thisArg = item.replace(/^-+/, '');
    if (thisArg !== item) {
      curArg = thisArg;
    } else if (curArg) {
      result[curArg] = thisArg;
    }
    return result;
  }, {})
})(process.argv);

const $ = plugins();

const tomcatDest = args.tomcatDest || process.env.TOMCAT_HOME
    + '/webapps/sellerpc/_resources/';
const tomcatHtml = args.tomcatHtml || process.env.TOMCAT_HOME
    + '/webapps/sellerpc/WEB-INF';

console.log('using tomcatDesc: ' + tomcatDest);
console.log('useing tomcatHtml: ' + tomcatHtml);

const path = {
  // 源数据路径
  assets: {
    less: 'assets/less/_main',
    js: 'assets/js/core/',
    css: 'assets/css',
    image: 'assets/images',
    fonts: 'assets/css/icons/icomoon'
  },
  //目标数据路径
  local: {
    html: '../WEB-INF/',
    js: 'js',
    css: 'css',
    image: 'images',
    less: 'less',
    distJs: 'js/dist',
    tomcatDest: tomcatDest,
    tomcatHtml: tomcatHtml
  }
};

var shellEnd = [
  'paths.jquery=empty: ',
  'paths.jqueryui=empty: ',
  'paths.app=empty:',
  'paths.moment=empty: ',
  'paths.bootstrap=empty: ',
  'paths.text=empty: ',
  'paths.formSelects=empty: ',
  'paths.echartsNoAMD=empty: ',
  'paths.echartsTheme=empty: ',
  'paths.validate=empty: ',
  'paths.cookies=empty: ',
  'paths.uniform=empty: ',
  'paths.md5=empty: ',
  'paths.placeholder=empty: ',
  'paths.mustache=empty: ',
  'paths.blockui=empty: ',
  'paths.switch=empty: ',
  'paths.sweetAlert=empty: ',
  'paths.dropzone=empty: ',
  'paths.fileinput=empty: ',
  'paths.fileinput_zh=empty: ',
  'paths.jquerySerializeObject=empty: ',
  'paths.datatables=empty: ',
  'paths.bootstrapTable=empty: ',
  'paths.bootstrapTableEditable=empty: ',
  'paths.bootbox=empty: ',
  'paths.select2=empty: ',
  'paths.steps=empty: ',
  'paths.daterangepicker=empty: ',
  'paths.daterangepicker_v2=empty: ',
  'paths.datepicker=empty: ',
  'paths.moment=empty: ',
  'paths.tree=empty: ',
  'paths.d3=empty: ',
  'paths.d3tooltip=empty: ',
  'paths.multiselect=empty: ',
  'paths.bootstrapSwitch=empty: ',
  'paths.spectrum=empty: ',
  'paths.ckEditor=empty: ',
  'paths.tokenfield=empty: ',
  'paths.duallistbox=empty: ',
  'paths.ramda=empty: ',
  'paths.xlsx=empty: ',
  'paths.base=page'
].join(' ');

var requireOptimizeShell = [
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='login' out=js/dist/login.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='all' out=js/dist/all.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerOrder' out=js/dist/partnerOrder.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerMember' out=js/dist/partnerMember.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerSetting' out=js/dist/partnerSetting.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerSummary' out=js/dist/partnerSummary.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerRecord' out=js/dist/partnerRecord.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallOrder' out=js/dist/mallOrder.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallLogistics' out=js/dist/mallLogistics.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallSummary' out=js/dist/mallSummary.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='merchant' out=js/dist/merchant.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='product' out=js/dist/product.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='comment' out=js/dist/comment.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='member' out=js/dist/member.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='category' out=js/dist/category.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='groupon' out=js/dist/groupon.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='flashSale' out=js/dist/flashSale.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='bargain' out=js/dist/bargain.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='viewcomponent' out=js/dist/viewcomponent.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='viewpage' out=js/dist/viewpage.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='datacenterTrade' out=js/dist/datacenterTrade.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='datacenterFlow' out=js/dist/datacenterFlow.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='datacenterGuest' out=js/dist/datacenterGuest.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='partnerApply' out=js/dist/partnerApply.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='homeItem' out=js/dist/homeItem.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='agentSummary' out=js/dist/agentSummary.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='role' out=js/dist/role.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='rolePrice' out=js/dist/rolePrice.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='agentTree' out=js/dist/agentTree.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='agentCommission' out=js/dist/agentCommission.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='rule' out=js/dist/rule.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='ruleProduct' out=js/dist/ruleProduct.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='merchantRole' out=js/dist/merchantRole.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='auditRule' out=js/dist/auditRule.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='feedbackContact' out=js/dist/feedbackContact.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='auditRuleAgent' out=js/dist/auditRuleAgent.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='versionList' out=js/dist/versionList.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouOperation' out=js/dist/yundouOperation.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouRule' out=js/dist/yundouRule.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='memberCard' out=js/dist/memberCard.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='agentWhiteList' out=js/dist/agentWhiteList.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='agentFounder' out=js/dist/agentFounder.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='bonusMonth' out=js/dist/bonusMonth.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='bonusYear' out=js/dist/bonusYear.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='antifakeList' out=js/dist/antifakeList.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='antifakeList' out=js/dist/antifakeList.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='tradeRecord' out=js/dist/tradeRecord.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='tradeSummary' out=js/dist/tradeSummary.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouSetting' out=js/dist/yundouSetting.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouRecord' out=js/dist/yundouRecord.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouSummary' out=js/dist/yundouSummary.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='yundouProduct' out=js/dist/yundouProduct.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='productTop' out=js/dist/productTop.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='carousel' out=js/dist/carousel.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='storeMember' out=js/dist/storeMember.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='teamApply' out=js/dist/teamApply.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='couponManager' out=js/dist/couponManager.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallLogisticsList' out=js/dist/mallLogisticsList.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='productCollection' out=js/dist/productCollection.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='fullCut' out=js/dist/fullCut.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='fullPieces' out=js/dist/fullPieces.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='skuAttribute' out=js/dist/skuAttribute.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='fullPieces' out=js/dist/fullPieces.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='productTag' out=js/dist/productTag.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='signRule' out=js/dist/signRule.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='message' out=js/dist/message.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='helper' out=js/dist/helper.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='promotionAudit' out=js/dist/promotionAudit.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='privilegeProduct' out=js/dist/privilegeProduct.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='memberPromotion' out=js/dist/memberPromotion.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallCoupon' out=js/dist/mallCoupon.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='merchantSupplier' out=js/dist/merchantSupplier.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='sync' out=js/dist/sync.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='menu' out=js/dist/menu.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='org' out=js/dist/org.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='brand' out=js/dist/brand.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='package' out=js/dist/package.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='warehouse' out=js/dist/warehouse.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='supplier' out=js/dist/supplier.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='mallOrderRefund' out=js/dist/mallOrderRefund.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='pointBalance' out=js/dist/pointBalance.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='hotSearch' out=js/dist/hotSearch.js "
  + shellEnd,
  "node js/third/r.js  -o baseUrl='js/page' optimize='none' name='hotAdd' out=js/dist/hotAdd.js "
  + shellEnd
];

// Compile our less files
gulp.task('less', function (event) {
  console.log('less start : ' + event.path);
  return gulp
  .src([path.assets.less + '/*.less', path.local.less + '/**/*.less'])              // locate /less/ folder root to grab 4 main files
  //.pipe($.plumber())
  .pipe($.less({
    plugins: [autoprefix]
  }))                              // compile
  .pipe($.minifyCss({                             // minify CSS
    keepSpecialComments: 0                    // remove all comments
  }))
  .pipe(gulp.dest('css'));               // destination path for minified CSS
});

gulp.task('fonts', function () {
  return gulp
  .src(path.assets.fonts + '/**/*')              // locate /less/ folder root to grab 4 main files
  .pipe(gulp.dest('css'));               // destination path for minified CSS
});

//gulp.task('concatenate', function() {
// return gulp
//         .src('./assets/js/*.js')                        // path to js files you want to concat
//         .pipe($.concat('all.js'))                       // output file name
//         .pipe(gulp.dest('js/dist'))                 // destination path for normal JS
//         .pipe($.rename({                                // rename file
//         suffix: ".min"                            // add *.min suffix
//         }))
//         .pipe($.uglify())                               // compress JS
//         .pipe(gulp.dest('js/dist'));                // destination path for minified JS
// });

gulp.task('watch', function () {
  gulp.watch([path.assets.less + '/*.less', path.local.less + '/**/*.less'],
      ['less']);
  gulp.watch(path.assets.fonts + '/**/*', ['fonts']);
  gulp.watch(path.local.js + '/page/**/*.js', ['lint', 'bundle']);
  var webWatcher = gulp.watch(path.local.html + 'web.xml');
  webWatcher.on("change", function (event) {
    gulp.src(path.local.html + 'web.xml')
    .pipe(gulp.dest(path.local.tomcatHtml));
  });
});

gulp.task('bundle', $.shell.task(requireOptimizeShell));

gulp.task('lint', function () {
  gulp.src(path.local.js + '/page/**/*.js')
  .pipe($.jshint())
  .pipe($.jshint.reporter('default'));
});

gulp.task('build', ['fonts', 'less', 'bundle', 'watch'], function () {
  return gulp.src(['js/**/*', 'images/**/*', "css/**/*"]).pipe(
      $.size({title: 'build', gzip: true}));
});

gulp.task('default', [], function () {
  gulp.start('build');

  gulp.src('css/**/*', {base: 'css'})
  .pipe($.watch('css/**/*', {base: 'css', interval: 500}))
  .pipe($.debug({title: 'copy css:'}))
  .pipe(gulp.dest(path.local.tomcatDest + 'css'));
  gulp.src('js/**/*', {base: 'js'})
  .pipe($.watch('js/**/*', {base: 'js', interval: 500}))
  .pipe($.debug({title: 'copy js:'}))
  .pipe(gulp.dest(path.local.tomcatDest + 'js'));
  gulp.src('images/**/*', {base: 'images'})
  .pipe($.watch('images/**/*', {base: 'images', interval: 500}))
  .pipe($.debug({title: 'copy images:'}))
  .pipe(gulp.dest(path.local.tomcatDest + 'images'));

  //sync html views to tomcat webapp
  gulp.src([path.local.html + '/**/*', '!' + path.local.html + 'web.xml'],
      {base: path.local.html})
  .pipe($.watch(path.local.html, {base: path.local.html, interval: 500}))
  .pipe($.debug({title: 'copy html:'}))
  .pipe(gulp.dest(path.local.tomcatHtml));
});
