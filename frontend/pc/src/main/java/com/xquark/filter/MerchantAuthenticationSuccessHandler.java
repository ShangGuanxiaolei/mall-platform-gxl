package com.xquark.filter;

import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.MerchantSigninLog;
import com.xquark.service.merchant.MerchantSigninLogService;
import com.xquark.service.merchant.impl.MerchantSigninLogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by quguangming on 16/6/2.
 */
@Service("merchantAuthenticationSuccessHandler")
public class MerchantAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  @Autowired
  private MerchantSigninLogService merchantSigninLogService;

  private String defaultTargetUrl;

  protected static final String COOKIE_NAME_KDAUTHTOKEN = "kdAuthToken";

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
      HttpServletResponse response, Authentication authentication)
      throws IOException, ServletException {
    //记录用户登录环境Log
    Merchant merchant = (Merchant) authentication.getPrincipal();
    MerchantSigninLog log = MerchantSigninLogFactory.createUserSigninLog(request, merchant);
    merchantSigninLogService.insert(log);

    // pc登陆后，将rememberme token种到主域名下，这样优惠券功能在bos下不至于30分钟后就失效需要重新登陆了
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(COOKIE_NAME_KDAUTHTOKEN)) {
          String kdAuthToken = cookie.getValue();
          Cookie c = new Cookie(COOKIE_NAME_KDAUTHTOKEN, kdAuthToken);
          c.setDomain(".51shop.mobi");
          c.setPath("/");
          response.addCookie(c);
        }
      }
    }

    //使用了authentication-success-handler-ref，
    //原有的always-use-default-target="true" default-target-url="/signined"不生效
    //现在使用bean注入默认的跳转配置
    response.sendRedirect(request.getContextPath() + defaultTargetUrl);
  }

  public String getDefaultTargetUrl() {
    return defaultTargetUrl;
  }

  public void setDefaultTargetUrl(String defaultTargetUrl) {
    this.defaultTargetUrl = defaultTargetUrl;
  }
}
