package com.xquark.filter;

import org.slf4j.MDC;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

public class MerchantServletFilter implements Filter {

  private final String USER_KEY = "loginname";

  private final String CLIENT_ID = "pc_mid";

  public void destroy() {
  }

  public void doFilter(ServletRequest request, ServletResponse response,
      FilterChain chain) throws IOException, ServletException {

    boolean successfulRegistration = false;

    // Please note that we could have also used a cookie to
    // retrieve the user name
    HttpServletRequest req = (HttpServletRequest) request;

    HttpServletResponse resp = (HttpServletResponse) response;

    Principal principal = req.getUserPrincipal();
    if (principal != null) {
      String username = principal.getName();
      successfulRegistration = registerUsername(username);
    }
    try {
      chain.doFilter(request, response);
    } finally {
      if (successfulRegistration) {
        MDC.remove(USER_KEY);
      }
    }
  }

  public void init(FilterConfig arg0) throws ServletException {
  }

  /**
   * Register the user in the MDC under USER_KEY.
   *
   * @return true id the user can be successfully registered
   */
  private boolean registerUsername(String username) {
    if (username != null && username.trim().length() > 0) {
      MDC.put(USER_KEY, username);
      return true;
    }
    return false;
  }
}
