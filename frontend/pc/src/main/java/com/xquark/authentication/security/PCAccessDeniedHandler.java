package com.xquark.authentication.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by wangxinhua on 17-4-28. DESC:
 */
public class PCAccessDeniedHandler implements AccessDeniedHandler {

  private static Logger logger = LoggerFactory.getLogger(PCAccessDeniedHandler.class);

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response,
      AccessDeniedException e) throws IOException, ServletException {
    Authentication auth
        = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      logger.warn("User: " + auth.getName()
          + " attempted to access the protected URL: "
          + request.getRequestURI());
    }

    response.sendRedirect(request.getContextPath() + "/pc/accessDenied");
  }

}
