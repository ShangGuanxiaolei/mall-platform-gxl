package com.xquark.authentication.security;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * 作者: wangxh 创建日期: 17-4-5 简介:
 */
@Component
public class MerchantAccessDecisionManager implements AccessDecisionManager {

  @Override
  public void decide(Authentication authentication, Object o,
      Collection<ConfigAttribute> configAttributes)
      throws AccessDeniedException, InsufficientAuthenticationException {
    // 如果拥有权限则return
    // 若没有权限则抛出AccessDenied
    if (configAttributes == null) {
      return;
    }
    for (ConfigAttribute configAttribute : configAttributes) {
      String role = configAttribute.getAttribute();
      for (GrantedAuthority authority : authentication.getAuthorities()) {
        // TODO wangxh 暂时设置ROOT用户全部访问
        if (authority.getAuthority().equals("ROLE_ROOT") || authority.getAuthority().trim()
            .equals(role.trim())) {
          return;
        }
      }
    }
    throw new AccessDeniedException("");
  }

  @Override
  public boolean supports(ConfigAttribute configAttribute) {
    return true;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return true;
  }
}
