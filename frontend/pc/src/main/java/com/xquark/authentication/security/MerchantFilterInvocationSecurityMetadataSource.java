package com.xquark.authentication.security;

import com.xquark.cache.UrlRoleCache;
import com.xquark.service.module.ModuleService;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

/**
 * 作者: wangxh 创建日期: 17-4-5 简介:
 */
@Component
public class MerchantFilterInvocationSecurityMetadataSource implements
    FilterInvocationSecurityMetadataSource {

  private static Logger logger = LoggerFactory
      .getLogger(MerchantFilterInvocationSecurityMetadataSource.class);

  @Autowired
  private ModuleService moduleService;

//    private Map<String, List<String>> urlRoleCache;

  @Autowired
  private UrlRoleCache urlRoleCache;

  @Override
  public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
    FilterInvocation fi = (FilterInvocation) o;
    String url = fi.getRequestUrl();

    if (!urlRoleCache.isCached()) {
      urlRoleCache.refresh();
    }
    // 没有定义规则就默认放行
    if (!urlRoleCache.contains(url)) {
      return null;
    }
    ArrayList<String> roleList = (ArrayList<String>) urlRoleCache.load(url);
    String[] roleResult = roleList.toArray(new String[roleList.size()]);
    logger.debug("URL {} Associated Roles ==> {}", url, roleResult);
    return SecurityConfig.createList(roleResult);
  }

  @Override
  public Collection<ConfigAttribute> getAllConfigAttributes() {
    return null;
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return true;
  }

}
