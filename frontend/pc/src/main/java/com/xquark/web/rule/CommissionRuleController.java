package com.xquark.web.rule;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 16/12/21.
 */
@Controller
public class CommissionRuleController extends BaseController {

  @RequestMapping(value = "/rule/list")
  public String list(Model model) {
    return "rule/ruleList";
  }

  @RequestMapping(value = "/ruleProduct/list")
  public String ruleProductList(Model model) {
    return "rule/ruleProductList";
  }

}
