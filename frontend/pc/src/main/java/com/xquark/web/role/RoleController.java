package com.xquark.web.role;

import com.xquark.web.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 16/12/13.
 */
@Controller
public class RoleController extends BaseController {

  @RequestMapping(value = "/role/list")
  public String list(Model model) {
    return "role/roleList";
  }

}
