package com.xquark.web.merchant;

import com.xquark.dal.IUser;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.vo.MerchantVo;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantService;
import com.xquark.service.merchant.MerchantSigninLogService;
import com.xquark.service.msg.MessageService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by quguangming on 16/6/2.
 */
@RestController
public class MerchantAuthController extends BaseController {

  @Autowired
  private MerchantSigninLogService merchantSigninLogService;

  @Autowired
  MerchantService merchantService;


  @Autowired
  RememberMeServices rememberMeServices;

  @Value("${site.web.host.name}")
  private String domainName;


  /**
   * 未登录用户会跳到本页面；请使用 /signin_check?u=xx&p=yy 登录
   *
   * @return 带http status code, 因为访问任何未授权页面，都会跳到这里。
   */
  @RequestMapping(value = "/pc/signin")
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ResponseObject<?> signinPage(HttpServletRequest req) {
    return new ResponseObject<Object>(
        new RequestContext(req).getMessage("signin.failed"),
        GlobalErrorCode.UNAUTHORIZED);
  }

  @RequestMapping(value = "/ps/signin_fail")
  public ResponseObject<?> signinFailedPage(HttpServletRequest req) {
    return new ResponseObject<Object>(
        new RequestContext(req).getMessage("signin.failed"),
        GlobalErrorCode.UNAUTHORIZED);
  }

  /**
   * @return current merchant
   */
  @RequestMapping(value = "/pc/signined")
  public ResponseObject<IUser> signined(HttpServletRequest req) {
    try {
      updateSigninedData(req);  //更新登录用户的数据
      return new ResponseObject<IUser>(merchantService.getCurrentMerchant());
    } catch (BizException e) {
      // 未登录状态
      return new ResponseObject<IUser>();
    }
  }

  /**
   * 更新登录后用户的相关数据
   */
  private void updateSigninedData(HttpServletRequest req) {

    Merchant merchant = merchantService.getCurrentMerchant();
    if (merchant != null) {
      merchant.setLastLoginAt(merchant.getLoginAt());
      merchant.setLoginAt(new Date());
      merchant.setCount(merchant.getCount() + 1);

      merchantService.update(merchant);
    }

    //获取角色，放到list里面
    List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
    getRoles(merchant, list);

    if (merchant != null) {
      Authentication auth = new UsernamePasswordAuthenticationToken(merchant, null, list);
      SecurityContextHolder.clearContext();
      SecurityContextHolder.getContext().setAuthentication(auth);
    }
  }

  /**
   * 获取所属角色
   */
  private void getRoles(Merchant user, List<GrantedAuthority> list) {
    for (String role : user.getRoles().split(",")) {
      //权限如果前缀是ROLE_，security就会认为这是个角色信息，而不是权限，例如ROLE_MENBER就是MENBER角色，CAN_SEND就是CAN_SEND权限
      list.add(new SimpleGrantedAuthority("ROLE_" + role));
    }
  }

  /**
   * 检查用户是否已注册
   */
  @ResponseBody
  @RequestMapping(value = "/pc/registered")
  public Boolean registered(@RequestParam String loginName, Model model) {

    if (loginName == null || StringUtils.isBlank(loginName)) {
      return false;
    }

    return merchantService.isRegistered(loginName);
  }

}
