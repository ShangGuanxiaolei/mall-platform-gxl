package com.xquark.web.antifake;

import com.xquark.service.merchant.MerchantService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/6/13.
 */
@Controller
public class AntifakeController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @RequestMapping(value = "/antifake/list")
  public String month(Model model) {
    return "/antifake/list";
  }


}
