package com.xquark.web.wechat;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.IUser;
import com.xquark.dal.model.User;
import com.xquark.dal.model.wechat.*;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shop.ShopService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.service.wechat.WechatAutoReplyService;
import com.xquark.service.wechat.WechatCustomizedMenuService;
import com.xquark.service.wechat.WechatTemplateMessageService;
import com.xquark.utils.EncryptUtil;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信模块控制器
 */
@Controller
public class WechatController extends BaseController {

  @Autowired
  private WechatCustomizedMenuService wechatCustomizedMenuService;

  @Autowired
  private WechatTemplateMessageService wechatTemplateMessageService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private WechatAutoReplyService wechatAutoReplyService;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @RequestMapping(value = "/wechat/config")
  public String wechatConfig(Model model) {
    WechatAppConfig wechatAppConfig = wechatAppConfigService.load(getCurrentUser().getShopId());

    if (wechatAppConfig != null) {
      model.addAttribute("wechatAppConfig", wechatAppConfig);
    }

    return "wechat/wechatConfig";
  }

  @ResponseBody
  @RequestMapping("/wechat/saveWechatConfig")
  public ResponseObject<Boolean> saveWechatConfig(@Valid WechatConfigForm configForm) {
    WechatAppConfig appConfig = wechatAppConfigService.load(getCurrentUser().getShopId());
    if (appConfig == null) {
      appConfig = new WechatAppConfig();
    }
    String formpassword = configForm.getCert_password();
    String oldpassword = appConfig.getCert_password();

    String appformpassword = configForm.getApp_cert_password();
    String appoldpassword = appConfig.getApp_cert_password();
    try {
      copy(configForm, appConfig);
      // 保存时对证书密码进行加密
      // 判断密码是否有修改，如果有修改，则需要重新对密码进行加密后保存
      if (StringUtils.isNotEmpty(formpassword) && !formpassword.equals(oldpassword)) {
        EncryptUtil des1 = new EncryptUtil();
        appConfig.setCert_password(des1.encrypt(appConfig.getCert_password()));
      }
      if (StringUtils.isNotEmpty(appformpassword) && !appformpassword.equals(appoldpassword)) {
        EncryptUtil des1 = new EncryptUtil();
        appConfig.setApp_cert_password(des1.encrypt(appConfig.getApp_cert_password()));
      }
      wechatAppConfigService.save(appConfig);
    } catch (Exception be) {
      log.error("save wechat config error.", be);
      return new ResponseObject(be);
    }

    return new ResponseObject<Boolean>(true);
  }


  /**
   * 保存微信配置证书
   */
  @ResponseBody
  @RequestMapping("/wechat/saveWechatCert")
  public String saveWechatCert(@RequestParam("cert_file") MultipartFile file) {
    if (!file.isEmpty()) {
      InputStream fileInputStream = null;
      try {
        fileInputStream = file.getInputStream();
        byte[] b = FileCopyUtils.copyToByteArray(fileInputStream);

        // 保存时对证书进行加密
        EncryptUtil des1 = new EncryptUtil();
        b = des1.encrypt(b);

        WechatAppConfig wechatAppConfig = wechatAppConfigService.load(getCurrentUser().getShopId());
        HashMap map = new HashMap();
        if (wechatAppConfig == null) {
          map.put("cert_file", b);
          wechatAppConfigService.insertCert(map);
        } else {
          map.put("id", wechatAppConfig.getId());
          map.put("cert_file", b);
          wechatAppConfigService.updateCert(map);
        }
        return "200";
      } catch (Exception e) {
        log.error(e.getMessage());
        return "500";
      } finally {
        if (fileInputStream != null) {
          try {
            fileInputStream.close();
          } catch (Exception e) {
            log.error(e.getMessage());
          }
        }
      }
    } else {
      return "500";
    }
  }

  /**
   * 保存微信配置证书
   */
  @ResponseBody
  @RequestMapping("/wechat/saveWechatAppCert")
  public String saveWechatAppCert(@RequestParam("app_cert_file") MultipartFile file) {
    if (!file.isEmpty()) {
      InputStream fileInputStream = null;
      try {
        fileInputStream = file.getInputStream();
        byte[] b = FileCopyUtils.copyToByteArray(fileInputStream);

        // 保存时对证书进行加密
        EncryptUtil des1 = new EncryptUtil();
        b = des1.encrypt(b);

        WechatAppConfig wechatAppConfig = wechatAppConfigService.load(getCurrentUser().getShopId());
        HashMap map = new HashMap();
        if (wechatAppConfig == null) {
          map.put("app_cert_file", b);
          wechatAppConfigService.insertAppCert(map);
        } else {
          map.put("id", wechatAppConfig.getId());
          map.put("app_cert_file", b);
          wechatAppConfigService.updateAppCert(map);
        }
        return "200";
      } catch (Exception e) {
        log.error(e.getMessage());
        return "500";
      } finally {
        if (fileInputStream != null) {
          try {
            fileInputStream.close();
          } catch (Exception e) {
            log.error(e.getMessage());
          }
        }
      }
    } else {
      return "500";
    }
  }


  private void copy(WechatConfigForm configForm, WechatAppConfig appConfig) {
    appConfig.setId(configForm.getId());
    appConfig.setAppId(configForm.getAppId());
    appConfig.setAppSecret(configForm.getAppSecret());
    appConfig.setAppName(configForm.getAppName());
    appConfig.setMchId(configForm.getMchId());
    appConfig.setMchKey(configForm.getMchKey());
    appConfig.setCert_password(configForm.getCert_password());
    appConfig.setAppAppId(configForm.getAppAppId());
    appConfig.setAppAppSecret(configForm.getAppAppSecret());
    appConfig.setAppMchId(configForm.getAppMchId());
    appConfig.setAppMchKey(configForm.getAppMchKey());
    appConfig.setApp_cert_password(configForm.getApp_cert_password());
  }

  @RequestMapping(value = "/wechat/autoReply")
  public String wechatAutoReply(@RequestParam(required = false) String id, Model model) {
    if (StringUtils.isEmpty(id)) {
      return "wechat/wechatAutoReply";
    }

    AutoReply autoReply = wechatAutoReplyService.load(id);
    if (autoReply == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "规则id不存在");
    }

    model.addAttribute("autoReply", autoReply);
    return "wechat/wechatAutoReply";
  }

  /**
   * 获取微信自动回复列表
   */
  @ResponseBody
  @RequestMapping("/wechat/viewDetail")
  public ResponseObject<AutoReply> viewDetail(@RequestParam(required = false) String id,
      HttpServletRequest request) {
    AutoReply autoReply = wechatAutoReplyService.load(id);
    if (autoReply == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "规则id不存在");
    }
    return new ResponseObject<AutoReply>(autoReply);
  }

  /**
   * 删除微信自动回复
   */
  @ResponseBody
  @RequestMapping("/wechat/deleteAutoReply")
  public ResponseObject<Boolean> deleteAutoReply(@RequestParam(required = false) String id,
      HttpServletRequest request) {
    Boolean flag = wechatAutoReplyService.delete(id) > 0;
    return new ResponseObject<Boolean>(flag);
  }


  /**
   * 获取微信自动回复列表
   */
  @ResponseBody
  @RequestMapping("/wechat/listWechatAutoReply")
  public ResponseObject<Map<String, Object>> list(HttpServletRequest request) {
    List<AutoReply> viewPages = null;
    Map<String, Object> params = new HashMap<String, Object>();
    viewPages = wechatAutoReplyService.list(this.getCurrentUser().getShopId());
    Map<String, Object> aRetMap = new HashMap<String, Object>();
    aRetMap.put("total", wechatAutoReplyService.selectCnt(this.getCurrentUser().getShopId()));
    aRetMap.put("list", viewPages);
    return new ResponseObject<Map<String, Object>>(aRetMap);
  }

  @ResponseBody
  @RequestMapping("/wechat/saveWechatAutoReply")
  public ResponseObject<Boolean> saveWechatAutoReply(@Valid AutoReplyForm autoReplyForm) {
    AutoReply autoReply = new AutoReply();
    try {
      autoReply.setId(autoReplyForm.getId());
      autoReply.setName(autoReplyForm.getName());
      autoReply.setCondition(autoReplyForm.getCondition());
      autoReply.setConditionType(autoReplyForm.getConditionType());
      autoReply.setMatchType(autoReplyForm.getMatchType());
      autoReply.setReplyType(ReplyType.TEXT);
      autoReply.setReplyContent(autoReplyForm.getReplyContent());
      autoReply.setShopId(getCurrentUser().getShopId());

      if (StringUtils.isEmpty(autoReplyForm.getId())) {
        wechatAutoReplyService.insert(autoReply);
      } else {
        wechatAutoReplyService.update(autoReply);
      }
    } catch (BizException be) {
      log.error("save wechat config error.", be);
      return new ResponseObject(be);
    }

    return new ResponseObject<Boolean>(true);
  }

  @RequestMapping(value = "/wechat/customizedMenus")
  public String wechatCustomizedMenus(Model model) {
    User user = getCurrentUser();
    CustomizedMenu customizedMenu = wechatCustomizedMenuService.menuList(user.getShopId());
    model.addAttribute("customizedMenu", customizedMenu);
    log.info("wechatCustomizedMenus query menu: " + JSON.toJSONString(customizedMenu));
    return "wechat/wechatCustomizedMenus";
  }

  @ResponseBody
  @RequestMapping("/wechat/saveCustomizedMenuItem")
  public ResponseObject<String> saveMenus(@Valid CustomizedMenuItemForm itemForm) {
    String ret = "";

    if (StringUtils.isEmpty(itemForm.getId())) {
      ret = wechatCustomizedMenuService
          .addButton(itemForm.getParentId(), itemForm.getIndex(), itemForm.getName(),
              itemForm.getType(), itemForm.getContentOrMediaId(), itemForm.getUrl());
    } else {
      wechatCustomizedMenuService.modifyButtonInfo(itemForm.getId(), itemForm.getName(),
          itemForm.getType(), itemForm.getContentOrMediaId(), itemForm.getUrl());
    }

    return new ResponseObject<String>(ret);
  }

  @ResponseBody
  @RequestMapping("/wechat/modifyMenusOrder")
  public ResponseObject<Integer> modifyMenusOrder(@RequestParam(value = "ids[]") String[] ids,
      @RequestParam(value = "newIndexes[]") Integer[] newIndexes) {

    ResponseObject<Integer> ret;

    try {
      int updateCount = wechatCustomizedMenuService.modifyMenusOrder(ids, newIndexes);
      ret = new ResponseObject<Integer>(updateCount);
    } catch (BizException be) {
      ret = new ResponseObject<Integer>(be);
    } catch (Exception e) {
      ret = new ResponseObject<Integer>("系统内部错误", GlobalErrorCode.INTERNAL_ERROR);
      log.error("更新菜单顺序错误", e);
    }

    return ret;
  }

  @RequestMapping(value = "/wechat/summary")
  public String wechatSummary(Model model) {
    return "wechat/wechatSummary";
  }

  @RequestMapping(value = "/wechat/templateMessage")
  public String wechatTemplateMessage(Model model) {
    List<TemplateMessage> templateMessages = wechatTemplateMessageService
        .list(getCurrentUser().getShopId());
    Map<String, TemplateMessage> templateMessageMap = new HashMap<String, TemplateMessage>(16);
    for (TemplateMessage templateMessage : templateMessages) {
      TemplateMessageType type = TemplateMessageType.find(templateMessage.getTemplateNo());
      templateMessageMap.put(type.toString(), templateMessage);
    }

    model.addAttribute("templateMessageMap", templateMessageMap);
    return "wechat/wechatTemplateMessage";
  }

  @ResponseBody
  @RequestMapping("/wechat/saveTemplateMessage")
  public ResponseObject<Boolean> saveTemplateMessageId(
      @Valid TemplateMessageIdsForm templateMessageIdsForm) {
    TemplateMessageType[] templateMessageNames = {
        TemplateMessageType.financeEvent,
        TemplateMessageType.pointEvent,
        TemplateMessageType.orderEvent,
        TemplateMessageType.orderDeliveryEvent,
        TemplateMessageType.refundEvent,
        TemplateMessageType.distributorApplicationEvent,
        TemplateMessageType.commissionEvent,
        TemplateMessageType.productDetailEvent,
        TemplateMessageType.newPartnerEvent,
        TemplateMessageType.partnerObtainCommissionEvent,
    };

    String[] ids = {
        templateMessageIdsForm.getFinanceEvent(),
        templateMessageIdsForm.getPointEvent(),
        templateMessageIdsForm.getOrderEvent(),
        templateMessageIdsForm.getOrderDeliveryEvent(),
        templateMessageIdsForm.getRefundEvent(),
        templateMessageIdsForm.getDistributorApplicationEvent(),
        templateMessageIdsForm.getCommissionEvent(),
        templateMessageIdsForm.getProductDetailEvent(),
        templateMessageIdsForm.getNewPartnerEvent(),
        templateMessageIdsForm.getPartnerObtainCommissionEvent(),
    };

    wechatTemplateMessageService.saveTemplateId(templateMessageNames, ids);

    return new ResponseObject<Boolean>(true);
  }


  @RequestMapping(value = "/wechat/materials")
  public String wechatMaterials(Model model) {

    return "wechat/wechatMaterials";
  }

  /**
   * 获取微信自动回复
   */
  @ResponseBody
  @RequestMapping("/wechat/getAutoBack")
  public ResponseObject<AutoReply> getAutoBack(HttpServletRequest request) {
    AutoReply autoReply = null;
    autoReply = wechatAutoReplyService.getAutoBack();
    return new ResponseObject<AutoReply>(autoReply);
  }

  /**
   * 获取微信被添加自动回复
   */
  @ResponseBody
  @RequestMapping("/wechat/getAddBack")
  public ResponseObject<AutoReply> getAddBack(HttpServletRequest request) {
    AutoReply autoReply = null;
    autoReply = wechatAutoReplyService.getAddBack();
    return new ResponseObject<AutoReply>(autoReply);
  }

}
