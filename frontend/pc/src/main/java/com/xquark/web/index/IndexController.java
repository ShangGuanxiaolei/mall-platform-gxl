package com.xquark.web.index;

import com.xquark.dal.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.dal.model.User;
import com.xquark.service.error.BizException;
import com.xquark.service.user.UserService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;

@Controller
public class IndexController extends BaseController {

  @Autowired
  private UserService userService;

  @ResponseBody
  @RequestMapping(value = "/signined")
  public ResponseObject<User> signined() {
    try {
      User user = (User) userService.getCurrentUser();
      user.setPassword(null);
      return new ResponseObject<User>(user);
    } catch (BizException e) {
      // 未登录状态
      return new ResponseObject<User>();
    }
  }

  @RequestMapping(value = "/pc/login")
  public String login() {
    return "/login";
  }

}
