package com.xquark.web.error;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.xquark.monitor.WarnEntity;
import com.xquark.monitor.WarnEnv;
import com.xquark.monitor.WarnSystemName;
import com.xquark.monitor.drive.MonitorWarn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.XqProductBuyException;

/**
 * General error handler for the application.
 */
@ControllerAdvice
class ExceptionHandler {

  Logger log = LoggerFactory.getLogger(getClass());


  @Value("${profiles.active}")
  WarnEnv currentEnv;
  final WarnEnv[] warnEnv = new WarnEnv[] {WarnEnv.prod,WarnEnv.stage};
  /**
   * Handle exceptions thrown by handlers.
   */
  @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
  public ModelAndView exception(Exception e, HttpServletRequest req) {
    log.error("access '" + req.getRequestURI() + "' occurs error [" + e.getMessage()
        + "] on: " + req.getContextPath() + ", "
        + req.getParameterMap(), e);

    /*Exception 预警*/
    Set<WarnEnv> warnEnvSet = new HashSet<>(Arrays.asList(warnEnv));
    if(warnEnvSet.contains(currentEnv)) {
      WarnEntity warnEntity = new WarnEntity(currentEnv,WarnSystemName.REST_API,e,req);

      MonitorWarn monitorWarn = MonitorWarn.getInstance();
      monitorWarn.sendMonitorMsg(warnEntity);
    }
    /*Exception 预警 end*/


    ModelAndView mav = new ModelAndView("error/general");

    // 非业务异常，保存到数据库
    if (!(e instanceof BizException)) {
      //FIXME 记录错误到统计工具中
    }

    BizException be = getBizException(e);
    GlobalErrorCode ec;
    String moreInfo;
    List<Object> args = new ArrayList<Object>();

    RequestContext reqCtx = new RequestContext(req);
    if (e instanceof MissingServletRequestParameterException) {
      ec = GlobalErrorCode.INVALID_ARGUMENT;
      Object[] params = new Object[1];
      params[0] = ((MissingServletRequestParameterException) e).getParameterName();
      moreInfo = reqCtx.getMessage("valid.notBlank.param", params);
    } else if (be == null) {
      ec = GlobalErrorCode.NOT_FOUND;
      moreInfo = reqCtx.getMessage("page.error.404.title"); //对外隐藏具体的错误
    } else {
      ec = be.getErrorCode();
      moreInfo = reqCtx.getMessage("page.error.404.title"); //对外隐藏具体的错误
    }

    mav.addObject("title", reqCtx.getMessage("page.error." + ec.getErrorCode() + ".title"));
    args.add(moreInfo);

    return mav;
  }

  private BizException getBizException(Throwable e1) {
    Throwable e2 = e1;
    do {
      if (e2 instanceof BizException) {
        return (BizException) e2;
      }
      e1 = e2;
      e2 = e1.getCause();
    } while (e2 != null && e2 != e1);

    return null;
  }
}