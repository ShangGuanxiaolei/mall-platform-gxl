package com.xquark.web.account;


import com.xquark.dal.model.CustomerProfile;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 账号管理, 清除用户账号接口
 */
@RestController
@RequestMapping("/account")
public class AccountManageController extends BaseController {

    @Value("${profiles.active}")
    private String activeProfile;

    @Autowired
    private CustomerProfileService customerProfileService;


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ResponseObject<List<CustomerProfile>> getUserInfoBySearchKey(@RequestBody Map map) {
        String cpId = (String) map.get("cpId");
        ResponseObject<List<CustomerProfile>> result = new ResponseObject<>();
        if ("prod".equals(activeProfile)) {
            return new ResponseObject<>("该接口只能UAT调用", GlobalErrorCode.UNAUTHORIZED);
        }
        if (StringUtils.isEmpty(cpId)) {
            return new ResponseObject<>("cpId 不能为空", GlobalErrorCode.INVALID_ARGUMENT);
        }
        List notClearCpIds = getNotClearCpIds();
        if (notClearCpIds.contains(cpId)) {
            return new ResponseObject<>("系统用户, 不可清除", GlobalErrorCode.INVALID_ARGUMENT);
        }
        List<CustomerProfile> customerProfiles = customerProfileService.getCustomerProfilesByCpId(Long.valueOf(cpId));
        if (CollectionUtils.isEmpty(customerProfiles)) {
            return new ResponseObject<>("未找到匹配的用户", GlobalErrorCode.USER_NOT_EXIST);
        }
        result.setData(customerProfiles);
        return result;
    }

    @RequestMapping("/clear/{cpId}")
    public ResponseObject<Boolean> deleteCustomerProfile(@PathVariable("cpId") String cpId) {
        ResponseObject<Boolean> result = new ResponseObject<>();
        if ("prod".equals(activeProfile)) {
            return new ResponseObject<>("该接口只能UAT调用", GlobalErrorCode.UNAUTHORIZED);
        }
        if (StringUtils.isEmpty(cpId)) {
            return new ResponseObject<>("清除用户的cpId不能为空", GlobalErrorCode.INVALID_ARGUMENT);
        }
        List notClearCpIds = getNotClearCpIds();
        if (notClearCpIds.contains(cpId)) {
            return new ResponseObject<>("该用户不能清除", GlobalErrorCode.INVALID_ARGUMENT);
        }
        boolean b = customerProfileService.clearCustomer(Long.valueOf(cpId));
        result.setData(b);
        return result;
    }

    private static List getNotClearCpIds() {
        /**
         * 以下账号cpid不可删除：
         *  1000001，1000002，2000001，2000002
         */
        List<String> notClearCpIds = new ArrayList<>(4);
        notClearCpIds.add("1000001");
        notClearCpIds.add("1000002");
        notClearCpIds.add("2000001");
        notClearCpIds.add("2000002");
        return notClearCpIds;
    }
}
