package com.xquark.web.feedback;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/2/10.
 */
@Controller
public class FeedbackController extends BaseController {

  @RequestMapping(value = "/feedback/contact")
  public String list(Model model) {
    return "feedback/contact";
  }

}
