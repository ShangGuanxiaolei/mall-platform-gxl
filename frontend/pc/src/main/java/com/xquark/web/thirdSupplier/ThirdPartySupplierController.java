package com.xquark.web.thirdSupplier;

import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.ThirdPartySupplierMapper;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.SkuCodeResourcesType;
import com.xquark.dal.type.SkuType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.SupplierLogisticsExport;
import com.xquark.dal.vo.SupplierSkuExportVO;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.merchantSupplier.MerchantSupplierService;
import com.xquark.service.order.ThirdPartySupplierService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.shop.SupplierLogisticsExportSpec;
import com.xquark.web.shop.SupplierOrderExportSpec;
import com.xquark.web.shop.SupplierSkuExportSpec;
import com.xquark.web.shop.ThirdPartySupplierOrderExportVO;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @auther liuwei
 * @date 2018/6/26 19:09
 */
@RestController
@RequestMapping("/third/supplier")
public class ThirdPartySupplierController extends BaseController {

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ThirdPartySupplierService thirdPartySupplierService;

  @Autowired
  private ThirdPartySupplierMapper thirdPartySupplierMapper;

  @Autowired
  private MerchantSupplierService merchantSupplierService;

  @Autowired
  private SystemRegionService systemRegionService;//新的地址

  @Autowired
  private ExcelService excelService;

  /**
   * 订单用excel导出
   */
  @RequestMapping("/order/exportExcel")
  public ResponseObject<Void> export2Excel(HttpServletRequest request, HttpServletResponse resp) {
    log.info("===================================开始导出订单========================================");
    try{
      //获取商家信息
      Merchant merchant = getCurrentMerchant();
      //获取供应商品code
      String supplierCode = merchantSupplierService.getByMerchantId(
          merchant.getId()).getSupplierCode();
      log.info("获取供应商CODE: {}" + supplierCode);
      if(StringUtils.isNotBlank(supplierCode)) {
        //通过供应商品code获取订单列表
        List<OrderVO> orders =
            thirdPartySupplierService.selectOrders4WxExport(getDBSupplierCode(supplierCode));
        log.info("获取DB供应商CODE: {}" + getDBSupplierCode(supplierCode));
        log.info("获取供应商品生成的订单列表: {}" + orders);
        List<ThirdPartySupplierOrderExportVO> ordersExport = new ArrayList<ThirdPartySupplierOrderExportVO>();

        for (OrderVO vo : orders) {
          String orderId = vo.getId();
          //获取订单的Item
          List<OrderItem> items = orderItemMapper.selectByOrderId(orderId);
          OrderAddress orderAddress = vo.getOrderAddress();
          List<SystemRegion> systemRegionList = null;
          String street = "";
          String consignee = vo.getBuyerName();
          if(orderAddress != null){
            String id = vo.getOrderAddress().getZoneId();
            systemRegionList = systemRegionService.listParents(id);
            street = orderAddress.getStreet();
            consignee = orderAddress.getConsignee();
          }
          //订单详情
          for (OrderItem item : items) {
            OrderVO ordervo = new OrderVO();
            BeanUtils.copyProperties(vo, ordervo);
            //订单一小时之内可取消
              ordervo.setSkuCode(thirdPartySupplierMapper.selectSkuCodeBySkuId(item.getSkuId()));
              ordervo.setBarCode(thirdPartySupplierMapper.selectBarCodeBySkuId(item.getSkuId()));
              ordervo.setCreatedAt(item.getCreatedAt());
              ordervo.setItemProductName(item.getTitle());
              ordervo.setItemAmount(item.getAmount());
              ordervo.setItemPrice(item.getPrice());
              ordervo.setSkuStr(item.getSkuStr());
              ordervo.setStatus(OrderStatus.PAID);
            ThirdPartySupplierOrderExportVO exportVO = new ThirdPartySupplierOrderExportVO(
                ordervo, systemRegionList, street, consignee);

            ordersExport.add(exportVO);
          }
        }
        //excel的工作簿名称和文件前缀
        String sheetStr = "订单详情";
        String filePrefix = "orderList";

        excelService.export(filePrefix, ordersExport, ThirdPartySupplierOrderExportVO.class, sheetStr,
            null, SupplierOrderExportSpec.KEY_SET,
            SupplierOrderExportSpec.VALUE_SET, resp, false);
        return new ResponseObject<Void>(GlobalErrorCode.SUCESS);
      }else {
        return new ResponseObject<Void>("该供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
      }
    }catch (Exception e){
      log.error("拉取订单excel异常",e);
      return new ResponseObject<Void>("供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
    }
  }

  /**
   * sku用excel导出
   * @param request
   * @param resp
   */
  @RequestMapping("/sku/exportExcel")
  public ResponseObject<Void> exportSkuExcel(HttpServletRequest request, HttpServletResponse resp) {
    log.info("===================================开始导出sku库存模板========================================");
    try{
      Merchant merchant = getCurrentMerchant();
      String supplierCode = merchantSupplierService.getByMerchantId(
          merchant.getId()).getSupplierCode();
      if (StringUtils.isNotBlank(supplierCode)) {
        List<SupplierSkuExportVO> skus =
            thirdPartySupplierMapper.selectSkus4Export(getDBSupplierCode(supplierCode));

        //excel的工作簿名称和文件前缀
        String sheetStr = "商品规格模板";
        String filePrefix = "skuList";

        excelService.export(filePrefix, skus, SupplierSkuExportVO.class, sheetStr,
            null, SupplierSkuExportSpec.KEY_SET,
            SupplierSkuExportSpec.VALUE_SET, resp, false);
        return new ResponseObject<Void>(GlobalErrorCode.SUCESS);
      }else {
        return new ResponseObject<Void>("该供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
      }
    }catch (Exception e){
      log.error("拉取sku模板excel异常",e);
      return new ResponseObject<Void>("该供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
    }
  }

  /**
   * 快递信息用excel导出
   * @param request
   * @param resp
   */
  @RequestMapping("/logistics/exportExcel")
  public ResponseObject<Void> exportLogisticsExcel(HttpServletRequest request, HttpServletResponse resp) {
    log.info("===================================开始导出物流信息模板========================================");
    try{
      Merchant merchant = getCurrentMerchant();
      String supplierCode = merchantSupplierService.getByMerchantId(
          merchant.getId()).getSupplierCode();
      if(StringUtils.isNotBlank(supplierCode)) {
        //excel的工作簿名称和文件前缀
        String sheetStr = "物流信息模板";
        String filePrefix = "logisticsList";
        List<OrderVO> orders =
            thirdPartySupplierService.selectOrders4WxExport(getDBSupplierCode(supplierCode));
        List<SupplierLogisticsExport> logistics = new ArrayList<SupplierLogisticsExport>();

        for (OrderVO vo : orders) {
            SupplierLogisticsExport sle = thirdPartySupplierMapper.selectLogistics(vo.getOrderNo());
            logistics.add(sle);
        }
        /**
         * 导出订单excel（文件名前缀，详情列表（表格内容），详情实体类（用于调用里面的get方法），
         * 工作簿名称，第一标题，表格列名，表格列名对应的get,响应，是否有标题（关联第五个参数））
         */
        excelService.export(filePrefix, logistics, SupplierLogisticsExport.class, sheetStr,
            null, SupplierLogisticsExportSpec.KEY_SET,
            SupplierLogisticsExportSpec.VALUE_SET, resp, false);
        return new ResponseObject<Void>(GlobalErrorCode.SUCESS);
      }else {
        return new ResponseObject<Void>("该供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
      }
    }catch (Exception e){
      log.error("拉取物流模板excel异常",e);
      return new ResponseObject<Void>("该供应商不存在", GlobalErrorCode.USER_NOT_EXIST);
    }
  }

  private String getDBSupplierCode(String supplierCode){
    return SkuCodeResourcesType.getSkuCodeResourceBySupplierCode(supplierCode)
            .orElse(new SkuType()).getName();
  }

  //=======================================以下是表格导入===========================================

  @RequestMapping("/sku/import")
  public ResponseObject<Void> skuImport(@RequestParam("url") String url){
    log.info("================================开始验证用户是否存在================================");
    try{
      if(getDBSupplierCode(merchantSupplierService.getByMerchantId(
          getCurrentMerchant().getId()).getSupplierCode()) != null){
        log.info("===================================开始导入sku库存========================================");
          URL neturl = new URL(url);
          log.info("===================================开始把数据导入数据库========================================");
          thirdPartySupplierService.importExcelToInsert(neturl.openStream());
          log.info("===================================数据导入数据库完毕========================================");
        }
      }catch (Exception e){
      log.error("导入sku库存excel异常",e);
      return new ResponseObject<Void>(e.getMessage(), GlobalErrorCode.SCRM_PRODUCT_NOT_EXISTS);
    }
    return new ResponseObject<Void>(GlobalErrorCode.SUCESS);
  }

  @RequestMapping("/logistics/import")
  public ResponseObject<Void> logisticsImport(@RequestParam("url") String url){
    log.info("================================开始验证用户是否存在================================");
    try {
      if(getDBSupplierCode(merchantSupplierService.getByMerchantId(
          getCurrentMerchant().getId()).getSupplierCode()) != null) {
        log.info("===================================开始导入物流信息========================================");
          URL neturl = new URL(url);
          thirdPartySupplierService.importExcelToUpdate(neturl.openStream());
        }
      }catch (Exception e) {
      log.error("导入物流excel异常",e);
      return new ResponseObject<Void>(e.getMessage(), GlobalErrorCode.SCRM_PRODUCT_NOT_EXISTS);
    }
    return new ResponseObject<Void>(GlobalErrorCode.SUCESS);
  }
}

