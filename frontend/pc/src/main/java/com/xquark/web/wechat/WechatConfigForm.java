package com.xquark.web.wechat;

/**
 * Created by dongsongjie on 16/5/25.
 */
public class WechatConfigForm {

  private String id;

  private String appName;

  private String appId;

  private String appSecret;

  private String mchId;

  private String mchKey;

  private String cert_password;

  /**
   * 信开放平台审核通过的应用APPID
   */
  private String appAppId;

  /**
   * 微信开放平台审核通过的应用APPsecret
   */
  private String appAppSecret;

  /**
   * 微信app支付分配的商户号
   */
  private String appMchId;

  /**
   * 微信app支付分配的商户密钥
   */
  private String appMchKey;

  private String app_cert_password;

  public String getApp_cert_password() {
    return app_cert_password;
  }

  public void setApp_cert_password(String app_cert_password) {
    this.app_cert_password = app_cert_password;
  }

  public String getAppAppId() {
    return appAppId;
  }

  public void setAppAppId(String appAppId) {
    this.appAppId = appAppId;
  }

  public String getAppAppSecret() {
    return appAppSecret;
  }

  public void setAppAppSecret(String appAppSecret) {
    this.appAppSecret = appAppSecret;
  }

  public String getAppMchId() {
    return appMchId;
  }

  public void setAppMchId(String appMchId) {
    this.appMchId = appMchId;
  }

  public String getAppMchKey() {
    return appMchKey;
  }

  public void setAppMchKey(String appMchKey) {
    this.appMchKey = appMchKey;
  }

  public String getCert_password() {
    return cert_password;
  }

  public void setCert_password(String cert_password) {
    this.cert_password = cert_password;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getAppSecret() {
    return appSecret;
  }

  public void setAppSecret(String appSecret) {
    this.appSecret = appSecret;
  }

  public String getMchId() {
    return mchId;
  }

  public void setMchId(String mchId) {
    this.mchId = mchId;
  }

  public String getMchKey() {
    return mchKey;
  }

  public void setMchKey(String mchKey) {
    this.mchKey = mchKey;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
