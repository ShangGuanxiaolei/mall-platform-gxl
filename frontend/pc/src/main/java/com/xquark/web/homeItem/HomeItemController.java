package com.xquark.web.homeItem;

import com.xquark.web.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 16/12/06.
 */
@Controller
public class HomeItemController extends BaseController {

  @RequestMapping(value = "/homeItem/list")
  public String list(Model model) {
    return "homeItem/homeItemList";
  }

  @RequestMapping(value = "/homeItem/edit")
  public String edit(Model model, @RequestParam(required = false) String pId) {
    if (StringUtils.isNotBlank(pId)) {
      model.addAttribute("homeItemId", pId);
    }
    return "homeItem/homeItemEdit";
  }

}
