package com.xquark.web.yundou;

import com.xquark.dal.model.YundouSetting;
import com.xquark.service.yundou.YundouSettingService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by wangxinhua on 17-5-4. DESC:
 */
@Controller
public class YundouController extends BaseController {

  @Autowired
  private YundouSettingService yundouSettingService;

  @RequestMapping("/yundou/rule")
  public String yundouRule() {
    return "yundou/rule";
  }

  @RequestMapping("/yundou/operation")
  public String yundouOperation() {
    return "yundou/operation";
  }

  @RequestMapping(value = "/yundou/setting")
  public String twitterSetting(Model model) {

    //获得shopId
    String shopId = getCurrentUser().getShopId();

    //加载配置信息
    YundouSetting setting = yundouSettingService.selectByShopId(shopId);
    model.addAttribute("setting", setting);

    return "/yundou/yundouSetting";
  }

  @RequestMapping("/yundou/sign/rule")
  public String signInRule() {
    return "yundou/signRule";
  }

  @RequestMapping("/yundou/record")
  public String yundouRecord() {
    return "yundou/yundouRecord";
  }

  @RequestMapping("/yundou/summary")
  public String yundouSummary() {
    return "yundou/yundouSummary";
  }

  @RequestMapping("/yundou/product")
  public String yundouProduct() {
    return "yundou/yundouProduct";
  }

}
