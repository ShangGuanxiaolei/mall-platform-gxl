package com.xquark.web.org;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 作者: wangxh 创建日期: 17-3-14 简介:
 */
@Controller
public class OrgController extends BaseController {

  @RequestMapping(value = "/org/list")
  public String list(Model model) {
    return "org/orgList";
  }

  @RequestMapping(value = "/org/edit")
  public String edit(Model model) {
    return "org/orgMerchant";
  }

}
