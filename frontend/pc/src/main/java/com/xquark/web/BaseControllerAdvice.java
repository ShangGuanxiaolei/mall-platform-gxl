package com.xquark.web;

import com.xquark.cache.ButtonCache;
import com.xquark.cache.ModuleCache;
import com.xquark.dal.IUser;
import com.xquark.dal.model.Shop;
import com.xquark.dal.vo.ModuleVO;
import com.xquark.dal.vo.RoleFunctionVO;
import com.xquark.service.shop.ShopService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * sellerpc 控制器拦截
 */
@ControllerAdvice
public class BaseControllerAdvice {

  protected Logger log = LoggerFactory.getLogger(BaseControllerAdvice.class);

  @Value("${profiles.active}")
  String profile;

  @Value("${site.web.host.name}")
  String siteHost;

  @Autowired
  private ModuleCache moduleCache;

  @Autowired
  private ButtonCache buttonCache;

  @Autowired
  private ShopService shopService;

  /*************** Model 默认参数 ***********************/

  @ModelAttribute("user")
  public IUser currentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        return (IUser) principal;
      }

      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    return null;
  }

  @ModelAttribute("modules")
  public List<ModuleVO> currentModule() {
    IUser user = currentUser();
    if (user == null) {
      return null;
    }
    if (!moduleCache.isCached()) {
      moduleCache.refreshByUser(user);
    }
    String loginName = user.getUsername();
    List<ModuleVO> ret = (ArrayList) moduleCache.load(loginName);
    return ret;
  }

  @ModelAttribute("shop")
  public Shop currentShop() {
    IUser user = currentUser();
    if (user == null) {
      return null;
    }

    Shop shop = shopService.load(user.getShopId());
    return shop;
  }

  @ModelAttribute("siteHost")
  public String getSiteHost() {
    return siteHost;
  }

  /*************** Model 默认参数 ***********************/

}

