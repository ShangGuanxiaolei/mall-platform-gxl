package com.xquark.web.banner;

import com.xquark.dal.model.IndexBanner;

import java.io.Serializable;

/**
 * @ClassName: IndexBannerVO
 * @Description: TODO
 * @Author: Kwonghom
 * @CreateDate: 2019/5/15 9:51
 * @Version: 1.0
 **/
public class IndexBannerVO extends IndexBanner implements Serializable {

    private static final long serialVersionUID = -1310595646822122930L;

    private String categoryStr;
    private String productId;
    private String productName;

    public String getCategoryStr() {
        return categoryStr;
    }

    public void setCategoryStr(String categoryStr) {
        this.categoryStr = categoryStr;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
