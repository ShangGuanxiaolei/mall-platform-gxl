package com.xquark.web.shop;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class SupplierOrderExportSpec {

  public static final String SHEET_STR = "订单详情";

  private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

  public static String[] KEY_SET;
  public static String[] VALUE_SET;

  static {
    EXPORT_FORMAT.put("订单号", "getOrderNo");
    EXPORT_FORMAT.put("买家昵称", "getBuyerName");
    EXPORT_FORMAT.put("收货人名称", "getConsignee");
    EXPORT_FORMAT.put("手机号码", "getBuyerPhone");
    EXPORT_FORMAT.put("联系电话", "getBuyerPhone");
    EXPORT_FORMAT.put("邮政编码", "getZipCode");//adCode
    EXPORT_FORMAT.put("省", "getProvince");
    EXPORT_FORMAT.put("市", "getCity");
    EXPORT_FORMAT.put("区", "getArea");
    EXPORT_FORMAT.put("收货地址", "getStreet");
    EXPORT_FORMAT.put("备注", "getRemark");
    EXPORT_FORMAT.put("货品名称", "getItemProductName");
    EXPORT_FORMAT.put("SKU编码", "getSkuCode");
    EXPORT_FORMAT.put("商品规格", "getSkuStr");
    EXPORT_FORMAT.put("条形码", "getBarCode");
    EXPORT_FORMAT.put("货品数量", "getItemAmount");
    EXPORT_FORMAT.put("卖家备注", "getRemark");
    EXPORT_FORMAT.put("订单状态", "getStatusStr");
    EXPORT_FORMAT.put("日期", "getCreatedAtStr");
    KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
    VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
  }

  private static String[][] getArrayFromHash(Map<String, String> data) {
    String[][] str;
    {
      Object[] keys = data.keySet().toArray();
      Object[] values = data.values().toArray();
      str = new String[keys.length][values.length];
      for (int i = 0; i < keys.length; i++) {
        str[0][i] = (String) keys[i];
        str[1][i] = (String) values[i];
      }
    }
    return str;
  }
}
