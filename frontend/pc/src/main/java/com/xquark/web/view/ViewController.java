package com.xquark.web.view;

import com.xquark.dal.model.User;
import com.xquark.web.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 16/10/18.
 */
@Controller
public class ViewController extends BaseController {

  @RequestMapping(value = "/view/viewcomponent")
  public String viewComponent(Model model) {
    return "view/viewComponentList";
  }

  @RequestMapping(value = "/view/viewpage")
  public String viewPage(Model model) {
    return "view/viewPageList";
  }

}
