package com.xquark.web.banner;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.PromotionProductVO;
import com.xquark.service.product.vo.ProductVO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public class ProductVOEx extends ProductVO {

  private static final long serialVersionUID = 1L;

  private PromotionType canUsePromotionType;

  private PromotionProductVO canUsePromotion;

  private String productUrl;

  private String activityName;

  private String b2bProductUrl;

  private boolean inBlackList;

  private boolean hadGift;

  private Date effectFrom;

  private Date effectTo;

  private BigDecimal promotionPrice;

  private JSONObject crmProductInfo;

  private Map<String, Long> commentTotalMap;

  private boolean isSoldOut;

  private String soldOutTitle;

  private String giftType;

  private int giftCount;
  //是否需要邀请码
  private boolean isCode;
  //是否是大会活动商品
  private boolean isMeetingProduct;
  //是否为首单 新人专区模块使用 by tanggb 2019/03/05
  private boolean isFirstOrder;
  //新人专区限购数
  private Integer freshmanPurchase;
  //是否为新人专区商品
  private boolean isFreshmanProduct;
  /**
   * 是否展示倒计时
   */
  private Boolean isShowTime;

  public Boolean getIsShowTime() {
    return isShowTime;
  }

  public void setIsShowTime(Boolean showTime) {
    isShowTime = showTime;
  }

  public boolean getIsCode() {
    return isCode;
  }

  public void setIsCode(boolean isCode) {
    this.isCode = isCode;
  }

  public boolean getIsMeetingProduct() {
    return isMeetingProduct;
  }

  public void setIsMeetingProduct(boolean isMeetingProduct) {
    this.isMeetingProduct = isMeetingProduct;
  }

  public int getGiftCount() {
    return giftCount;
  }

  public void setGiftCount(int giftCount) {
    this.giftCount = giftCount;
  }
  public String getGiftType() {
    return giftType;
  }

  public void setGiftType(String giftType) {
    this.giftType = giftType;
  }

  public boolean isInBlackList() {
    return inBlackList;
  }

  public void setInBlackList(boolean inBlackList) {
    this.inBlackList = inBlackList;
  }

  public String getB2bProductUrl() {
    return b2bProductUrl;
  }

  public void setB2bProductUrl(String b2bProductUrl) {
    this.b2bProductUrl = b2bProductUrl;
  }

  public ProductVOEx(ProductVO product, String productUrl, String imgUrl, String activityName) {
    super(product);
    this.setProductUrl(productUrl);
    this.setImgUrl(imgUrl);
    this.setActivityName(activityName);
  }

  public void setCanUsePromotion(PromotionProductVO canUsePromotion) {
    this.canUsePromotion = canUsePromotion;
  }

  public String getProductUrl() {
    return productUrl;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public String getActivityName() {
    return activityName;
  }

  public void setActivityName(String activityName) {
    this.activityName = activityName;
  }

  public JSONObject getCrmProductInfo() {
    return crmProductInfo;
  }

  public void setCrmProductInfo(JSONObject crmProductInfo) {
    this.crmProductInfo = crmProductInfo;
  }

  public Map<String, Long> getCommentTotalMap() {
    return commentTotalMap;
  }

  public void setCommentTotalMap(Map<String, Long> commentTotalMap) {
    this.commentTotalMap = commentTotalMap;
  }

  public void setCanUsePromotionType(PromotionType canUsePromotionType) {
    this.canUsePromotionType = canUsePromotionType;
  }

  public String getPromotionTag() {
    if (canUsePromotionType == null) {
      return null;
    }
    return canUsePromotionType.getcName();
  }

  public PromotionType getCanUsePromotionType() {
    return canUsePromotionType;
  }

  public PromotionProductVO getCanUsePromotion() {
    return canUsePromotion;
  }

  public BigDecimal getPromotionPrice() {
    return promotionPrice;
  }

  public void setPromotionPrice(BigDecimal promotionPrice) {
    this.promotionPrice = promotionPrice;
  }

  public boolean isHadGift() {
    return hadGift;
  }

  public void setHadGift(boolean hadGift) {
    this.hadGift = hadGift;
  }

  public Date getEffectFrom() {
    return effectFrom;
  }

  public void setEffectFrom(Date effectFrom) {
    this.effectFrom = effectFrom;
  }

  public Date getEffectTo() {
    return effectTo;
  }

  public void setEffectTo(Date effectTo) {
    this.effectTo = effectTo;
  }

  public boolean getIsSoldOut() {
    return isSoldOut;
  }

  public void setIsSoldOut(boolean isSoldOut) {
    this.isSoldOut = isSoldOut;
  }

  public boolean isSoldOut() {
    return isSoldOut;
  }

  public void setSoldOut(boolean soldOut) {
    isSoldOut = soldOut;
  }

  public String getSoldOutTitle() {
    return soldOutTitle;
  }

  public void setSoldOutTitle(String soldOutTitle) {
    this.soldOutTitle = soldOutTitle;
  }

  public boolean isFirstOrder() {
    return isFirstOrder;
  }

  public void setFirstOrder(boolean firstOrder) {
    isFirstOrder = firstOrder;
  }

  public Integer getFreshmanPurchase() {
    return freshmanPurchase;
  }

  public void setFreshmanPurchase(Integer freshmanPurchase) {
    this.freshmanPurchase = freshmanPurchase;
  }

  public boolean isFreshmanProduct() {
    return isFreshmanProduct;
  }

  public void setFreshmanProduct(boolean freshmanProduct) {
    isFreshmanProduct = freshmanProduct;
  }
}
