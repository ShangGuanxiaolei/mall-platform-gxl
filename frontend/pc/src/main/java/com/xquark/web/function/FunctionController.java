package com.xquark.web.function;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 作者: wangxh 创建日期: 17-4-6 简介:
 */
@Controller
public class FunctionController {

  @RequestMapping("/function/manage")
  public String manage() {
    return "function/functionManage";
  }
}
