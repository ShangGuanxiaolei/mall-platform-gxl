package com.xquark.web.bonus;

import com.xquark.service.merchant.MerchantService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/6/7.
 */
@Controller
public class BonusController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @RequestMapping(value = "/bonus/month")
  public String month(Model model) {
    return "/bonus/monthList";
  }

  @RequestMapping(value = "/bonus/year")
  public String year(Model model) {

    return "/bonus/yearList";
  }


}
