package com.xquark.web.profile;

import com.xquark.dal.model.Image;
import com.xquark.dal.model.Merchant;
import com.xquark.service.file.ImageService;
import com.xquark.service.merchant.MerchantService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by quguangming on 16/5/4.
 */
@Controller
public class ProfileController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @Autowired
  private ImageService imageService;

  @RequestMapping(value = "profile")
  public String merchantInfo(Model model) {

    Merchant merchant = merchantService.getCurrentMerchant();

    Image avatar = null;
    if (merchant != null && StringUtils.isNotBlank(merchant.getAvatar())) {
      avatar = imageService.loadByImgKey(merchant.getAvatar());
    } else {
      avatar = new Image();
    }
    model.addAttribute("avatar", avatar);
    model.addAttribute("user", merchant);

    return "profile/userProfile";
  }


  @RequestMapping(value = "profile/password")
  public String merchantAdmin(Model model) {
    return "profile/profilePassword";
  }

  @ResponseBody
  @RequestMapping(value = "/profile/changepwd")
  public Boolean changePwd(@RequestParam(value = "oldpwd", required = false) String oldPwd,
      @RequestParam("newpwd") String newPwd, HttpServletRequest req) {
    if (StringUtils.isBlank(newPwd)) {
      return false;
    }
    return merchantService.changePwd(oldPwd, newPwd);
  }

  @ResponseBody
  @RequestMapping(value = "/profile/update")
  public Boolean update(Merchant merchant) {

    Merchant currentMerchant = merchantService.getCurrentMerchant();

    if (currentMerchant != null && merchant != null) {

      if (StringUtils.equals(currentMerchant.getId(), merchant.getId())) {

        currentMerchant.setAvatar(merchant.getAvatar());
        currentMerchant.setEmail(merchant.getEmail());
        currentMerchant.setPhone(merchant.getPhone());
        currentMerchant.setSex(merchant.getSex());
        currentMerchant.setName(merchant.getName());
        currentMerchant.setOrgId(merchant.getOrgId());

        int result = merchantService.update(currentMerchant);
        if (result > 0) {
          return true;
        }
      }
    }

    return false;
  }


}
