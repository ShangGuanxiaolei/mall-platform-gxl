package com.xquark.web.coupon;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.Coupon;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.shop.ShopService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.vo.Json;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by quguangming on 16/4/8.
 */
@Controller
public class CouponController extends BaseController {

  @Autowired
  private PromotionCouponService promotionCouponService;
  @Autowired
  private ShopService shopService;

  @Value("${site.web.host.name}")
  String siteHost;


  // 我的优惠券
  @RequestMapping(value = "/coupons/myCoupon")
  public String myCoupon(Model model) {
    return "/coupons/myCoupon";
  }

  /**
   * 获取优惠券列表
   */
  @ResponseBody
  @RequestMapping(value = "/coupons/list")
  public ResponseObject<Map<String, Object>> products(CouponSearchForm form, Pageable pageable) {

    String shopId = this.getCurrentUser().getShopId();

    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(form.getDirection())) {
      params.put("direction", form.getDirection());
    }
    if (StringUtils.isNotBlank(form.getDistributedAt())) {
      params.put("distributedAt", form.getDistributedAt());
    }
    if (StringUtils.isNotBlank(form.getOrder())) {
      params.put("order", form.getOrder());
    }
    if (StringUtils.isNotBlank(form.getStatus())) {
      params.put("status", form.getStatus());
    } else {
      params.put("status", CouponStatus.VALID.toString());
    }
    if (StringUtils.isNotBlank(form.getUsedNumber())) {
      params.put("flag", form.getFlag());
      params.put("useNumber", form.getUsedNumber());
    }
    if (StringUtils.isNotBlank(form.getValidFrom())) {
      params.put("validFrom", form.getValidFrom());
    }
    if (StringUtils.isNotBlank(form.getValidTo())) {
      params.put("validTo", form.getValidTo() + " 23:59:59");
    }
    if (StringUtils.isNotBlank(shopId)) {
      params.put("shopId", shopId);
    }

    List<PromotionCouponVo> promotionCouponVoes = null;

    Long total = promotionCouponService.countPromotionCouponByShopId(params);
    if (total.longValue() > 0) {
      promotionCouponVoes = promotionCouponService.listPromotionCouponByShopId(params, pageable);
    } else {
      promotionCouponVoes = new ArrayList<PromotionCouponVo>();
    }
    Map<String, Object> data = new HashMap<String, Object>();

    data.put("total", total);
    data.put("rows", promotionCouponVoes);

    return new ResponseObject<Map<String, Object>>(data);
  }

  @ResponseBody
  @RequestMapping(value = "/coupons/load")
  public ResponseObject<Map<String, Object>> load(@RequestParam(required = false) String couponId) {

    Map<String, Object> data = new HashMap<String, Object>();

    if (StringUtils.isEmpty(couponId)) {  //新建优惠券
      //发行时间
      Date date = new Date();
      SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
      String distributedDate = format.format(date);
      data.put("distributedDate", distributedDate);

      //批次号
      String batchNo = nextBatchNo();
      data.put("batchNo", batchNo);

    } else {   //修改优惠券
      Coupon coupon = promotionCouponService.load(couponId);
      data.put("coupon", coupon);
    }
    return new ResponseObject<Map<String, Object>>(data);
  }

  @ResponseBody
  @RequestMapping(value = "/coupons/update")
  public Json update(CouponCreateForm form) {

    String couponId = "";
    if (form != null) {
      couponId = form.getCouponId();
    }
    Json json = new Json();

//        String[] notInStatusArr = new String[]{CouponStatus.OVERDUE.toString(),CouponStatus.CLOSED.toString(),CouponStatus.USED.toString()};
//        long coupons = userCouponService.countPromotionCoupons(notInStatusArr,couponId);//修改优惠券状态为关闭或者过期时需验证是否用用户卷在使用
//        if( coupons > 0){
//          json.setRc(Json.RC_FAILURE);
//          json.setMsg("修改失败（该优惠券存在未使用的用户卷，不允许擅自更改状态！）");
//          return json;
//        }

    Map<String, Object> params = parsePromotionCouponCreateForm(form);
    try {
      int ret = 0;
      ret = promotionCouponService.modifyPromotionCoupon(params, couponId);
      if (ret > 0) {
        json.setRc(Json.RC_SUCCESS);
        json.setMsg("优惠券修改成功！");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("优惠券修改失败!");
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      json.setRc(Json.RC_FAILURE);
      json.setMsg("修改失败;" + e.getMessage());
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "/coupons/create")
  public String create(CouponCreateForm form) {

    if (form != null && form.getStatus() == null) {
      form.setStatus(CouponStatus.VALID);
    }
    Map<String, Object> params = parsePromotionCouponCreateForm(form);
    String respResult = null;
    try {
      int ret = promotionCouponService.create(params);
      if (ret > 0) {
        respResult = "200";  //优惠券创建成功
      } else {
        respResult = "206";  //优惠券创建失败
      }
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
      respResult = "500";
    }

    return respResult;
  }

  private Map<String, Object> parsePromotionCouponCreateForm(CouponCreateForm form) {

    Map<String, Object> params = new HashMap<String, Object>();
    if (null != form) {
      params.put("couponId", form.getCouponId());
      params.put("distributedAt", form.getDistributed_at());
      params.put("acquireLimit", form.getAcquire_limit());
      params.put("amount", form.getAmount());
      params.put("applyAbove", form.getApply_above());
      params.put("batchNo", form.getBatch_no());
      params.put("createAt", form.getCreate_at());
      params.put("couponName", form.getCouponName());
      params.put("discount", form.getDiscount());
      params.put("shopId", form.getShop_select());
      params.put("totalDiscount", form.getTotal_discount());
      params.put("validFrom", form.getValid_from() + " 00:00:00");
      params.put("validTo", form.getValid_to() + " 23:59:59");
      params.put("applyLimit", form.getApply_limit());
      params.put("updateAt", DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
      params.put("status", form.getStatus());
      params.put("archive", 0);
    }
    return params;
  }

  /**
   * 批次号生成规则：时间戳（精确到秒）+当前用户ID+序列号
   *
   * @return 24位批次号
   */
  private String nextBatchNo() {

    String version = "01"; //批次号生成规则 版本号

    String rdm = UUID.randomUUID().toString();
    String prefixRdm = "0001";
    String suffixRdm = "0001";
    if (rdm != null && rdm.length() >= 8) { //取UUID后8位数
      prefixRdm = rdm.substring(rdm.length() - 8, rdm.length() - 4);
      suffixRdm = rdm.substring(rdm.length() - 4, rdm.length());
    }

    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(System.currentTimeMillis());
    //生成规则：     月+秒 + 随机四位数+  小时 +版本号 +年 +日 +分钟 + 随机四位数
    return String
        .format("%1$tm%1$tS%2$s%1$tk%4$s%1$tY%1$tM%1$td%3$s", cal, prefixRdm, suffixRdm, version);
  }
}

