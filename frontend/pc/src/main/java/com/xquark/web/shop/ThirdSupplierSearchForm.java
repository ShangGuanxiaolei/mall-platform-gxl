package com.xquark.web.shop;

import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.type.PaymentMode;

/**
 * 订单导出查询sql
 */
public class ThirdSupplierSearchForm {

  private String orderNo;//订单号

  private PaymentMode paymentMode;//支付方式

  private OrderStatus status;//订单状态

  private String completeStartDate;//完整的开始时间

  private String completeEndDate;//完整的结束时间

  private OrderSortType orderType;//订单类型

  private String buyerName;//订货经销商

  private String sellerName;//接单经销商

  private AgentType agentType;//买家级别

  private String startDate;//开始时间

  private String endDate;//结束时间

  private OrderStatus orderStatus;//订单状态

  private ActivityGrouponStatus grouponStatus;//团购状态

  private String supplierCode;//供应商编码

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public AgentType getAgentType() {
    return agentType;
  }

  public void setAgentType(AgentType agentType) {
    this.agentType = agentType;
  }

  public ActivityGrouponStatus getGrouponStatus() {
    return grouponStatus;
  }

  public void setGrouponStatus(ActivityGrouponStatus grouponStatus) {
    this.grouponStatus = grouponStatus;
  }

  public OrderSortType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderSortType orderType) {
    this.orderType = orderType;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public PaymentMode getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(PaymentMode paymentMode) {
    this.paymentMode = paymentMode;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public String getCompleteStartDate() {
    return completeStartDate;
  }

  public void setCompleteStartDate(String completeStartDate) {
    this.completeStartDate = completeStartDate;
  }

  public String getCompleteEndDate() {
    return completeEndDate;
  }

  public void setCompleteEndDate(String completeEndDate) {
    this.completeEndDate = completeEndDate;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  @Override
  public String toString() {
    return "ThirdSupplierSearchForm{" +
        "orderNo='" + orderNo + '\'' +
        ", paymentMode=" + paymentMode +
        ", status=" + status +
        ", completeStartDate='" + completeStartDate + '\'' +
        ", completeEndDate='" + completeEndDate + '\'' +
        ", orderType=" + orderType +
        ", buyerName='" + buyerName + '\'' +
        ", sellerName='" + sellerName + '\'' +
        ", agentType=" + agentType +
        ", startDate='" + startDate + '\'' +
        ", endDate='" + endDate + '\'' +
        ", orderStatus=" + orderStatus +
        ", grouponStatus=" + grouponStatus +
        ", supplierCode='" + supplierCode + '\'' +
        '}';
  }
}
