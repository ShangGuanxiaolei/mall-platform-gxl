

package com.xquark.web.shop;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.OrderMessage;
import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.dal.vo.OrderFeeVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.voex.OrderVOEx;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.union.UnionService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.ExcelUtils;
import com.xquark.web.BaseController;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.RequestContext;

@Controller
public class OrderController extends BaseController {

  @Autowired
  private OrderService orderService;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private OrderRefundService orderRefundService;
  @Autowired
  private ZoneService zoneService;
  @Autowired
  private CashierService cashierService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private UnionService unionService;

  @Autowired
  private ExcelService excelService;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private ProductMapper productMapper;

  private final String ORDERNOTES = "您的交易金额大于%s，需要支付%s手续费";
  @Value("${tech.serviceFee.standard}")
  private String serviceFeethreshold;

  @Value("${order.delaysign.date}")
  private int defDelayDate;

  @Value("${export.excel.tempDir}")
  private String exportTemp;

  // 登录
  @RequestMapping(value = "/pc/login.html")
  public String pcLogin() {
    return "/login";
  }

  // accessDenied
  @RequestMapping(value = "/pc/accessDenied")
  public String accessDenied() {
    return "/accessDenied";
  }

  // 我的商品
  // 我的商品
  @RequestMapping(value = "/products/myproduct")
  public String myProduct(Model model) {
    return "/products/myproduct";
  }

  //商品分类
  @RequestMapping(value = "/products/category")
  public String proCate(Model model) {
    return "/products/category";
  }

  // 发布商品
  @RequestMapping(value = "/products/product")
  public String product(Model model) {
    return "/products/product";
  }

  // 订单列表
  @RequestMapping(value = "/orders/list")
  public String ordersList(Model model, OrderStatus status, Integer size,
      Integer page, Boolean isDesc, String orderName, String key) {
    // 初始化分页信息 page
    Pageable pageable = initPage(page, size, orderName, isDesc);
    model.addAttribute("page", pageable);

    // 模糊查询匹配
    List<OrderVO> orders = null;
    Long total = 0L;
    if (!StringUtils.isBlank(key)) {
      key = key.trim();
      // 买家姓名 微信号 电话
      orders = orderService.listByStatus4SellerWithLike(status, pageable,
          key, null, "");
      total = orderService.countSellerOrdersByStatusWithLike(status, key);

    } else {
      orders = orderService.listByStatus4Seller(status, pageable);
      total = orderService.countSellerOrdersByStatus(status);
    }
    model.addAttribute("total", total);

    // 回填图片和地区
    setImgAndZones(orders.toArray(new OrderVO[orders.size()]));

    // 查询的状态是已付款的，则获取成功的订单总数目
    if (OrderStatus.PAID.equals(status) && orders.size() > 0) {
      OrderVO vo = orders.get(0);
      vo.setSeq(orderService.selectOrderSeqByShopId(vo.getShopId()));
    }

    model.addAttribute("orders", orders);

    model.addAttribute("status", status);
    return "orders/order-list";
  }

  // 订单列表
  @RequestMapping(value = "/orders/twitterlist")
  public String ordersTwitterList(Model model, OrderStatus status, Integer size,
      Integer page, Boolean isDesc, String orderName, String key) {
    // 初始化分页信息 page
    Pageable pageable = initPage(page, size, orderName, isDesc);
    model.addAttribute("page", pageable);

    // 模糊查询匹配
    List<OrderVO> orders = null;
    Long total = 0L;
    if (!StringUtils.isBlank(key)) {
      key = key.trim();
      // 买家姓名 微信号 电话
      orders = orderService.listByStatus4SellerWithLike(status, pageable,
          key, null, "");
      total = orderService.countSellerOrdersByStatusWithLike(status, key);

    } else {
      orders = orderService.listByStatus4Seller(status, pageable);
      total = orderService.countSellerOrdersByStatus(status);
    }
    model.addAttribute("total", total);

    // 回填图片和地区
    setImgAndZones(orders.toArray(new OrderVO[orders.size()]));

    // 查询的状态是已付款的，则获取成功的订单总数目
    if (OrderStatus.PAID.equals(status) && orders.size() > 0) {
      OrderVO vo = orders.get(0);
      vo.setSeq(orderService.selectOrderSeqByShopId(vo.getShopId()));
    }

    model.addAttribute("orders", orders);

    model.addAttribute("status", status);
    return "orders/ordertwitter-list";
  }

  // 订单详情
  @RequestMapping(value = "/orders/details")
  public String ordersDetails(@RequestParam String orderId, Model model,
      HttpServletRequest req) {
    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("order.not.found"));
    }
    setLogisticsInfo(order);

    Shop shop = shopService.load(order.getShopId());
    if (shop != null) {
      order.setShopName(shop.getName());
    }
    // 设置图片和地区
    setImgAndZones(order);
    // 分佣
    setCommissionInfo(order);

    List<CouponInfoVO> orderCoupons = cashierService
        .loadCouponInfoByOrderNo(order.getOrderNo());
    order.setOrderCoupons(orderCoupons);

    List<OrderFeeVO> list = orderService.findOrderFees(order);
    OrderVOEx orderEx = new OrderVOEx(order, list);

    BigDecimal discountFee = orderEx.getDiscountFee();
    if (discountFee == null) {
      discountFee = BigDecimal.ZERO;
    }

    orderEx.setDiscountFee(discountFee.add(order.getHongbaoAmount()));
    orderEx.setDefDelayDate(defDelayDate);
    orderEx.setRefundableFee(orderEx.getTotalFee().subtract(
        orderEx.getLogisticsFee()));
    orderEx.setShowRefundBtn(false);

    // 快店IOS审核失败，暂时关闭买家端退款入口
    if (orderEx.getStatus() != OrderStatus.SUBMITTED
        && orderEx.getStatus() != OrderStatus.SUCCESS
        && orderEx.getStatus() != OrderStatus.CANCELLED) {
      if (orderEx.getStatus() == OrderStatus.CLOSED) {
        List<OrderRefund> refunds = orderRefundService
            .listByOrderId(order.getId());
        if (refunds.size() > 0) {
          orderEx.setShowRefundBtn(true);
        }
      } else {
        orderEx.setShowRefundBtn(true);
      }
    }

    model.addAttribute("order", orderEx);
    List<OrderMessage> messages = orderService
        .viewMessages(orderEx.getId());
    model.addAttribute("messages", messages);
    return "orders/order-detail";
  }

  /**
   * 取消订单
   */
  @ResponseBody
  @RequestMapping("/order/cancel")
  public Boolean cancelOrder(@RequestParam String orderId) {
    return orderService.cancel(orderId, getCurrentUser().getId());
  }

  /**
   * 发货
   */
  @ResponseBody
  @RequestMapping("/order/shipped")
  public Boolean shipped(@RequestParam String logisticsCompany,
      @RequestParam String logisticsOrderNo, @RequestParam String orderId) {
    orderService.ship(orderId, logisticsCompany, logisticsOrderNo);
    return true;
  }

  /**
   * 导入订单,批量发货
   */
  @ResponseBody
  @RequestMapping("/order/import")
  public String shipped(@RequestParam("excelFile") MultipartFile file)
      throws IOException {
    if (!file.isEmpty()) {
      InputStream fileInputStream = file.getInputStream();
      try {
        String[][] excelData = ExcelUtils.excelImport(fileInputStream);
        if (excelData == null || excelData.length == 0) {
          log.error("excelData is null");
          return "500";
        }
        int succCount = 1;
        int count = excelData.length;
        for (int i = 1; i < excelData.length; i++) {
          String orderNo = excelData[i][0].replace("\"", "");
          if (StringUtils.isBlank(orderNo)) {
            count--;
            continue;
          }
          String logisticsCompany = excelData[i][1];
          String logisticsOrderNo = excelData[i][2];
          try {
            OrderVO order = orderService.loadByOrderNo(orderNo);
            if (order != null && orderService
                .ship(order.getId(), logisticsCompany, logisticsOrderNo)) {
              succCount++;
            } else {
              succCount--;
            }
          } catch (Exception e) {
            succCount--;
          }
        }
        if (succCount == 0 || count == 1) {
          return "207";
        }
        if (succCount != count) {
          return "206";
        } else {
          return "200";
        }
      } catch (Exception e) {
        log.error(e.getMessage());
        return "500";
      } finally {
        if (fileInputStream != null) {
          try {
            fileInputStream.close();
          } catch (Exception e) {
            log.error(e.getMessage());
          }
        }
      }
    } else {
      return "500";
    }
  }

  /**
   * 订单导出
   */
  @ResponseBody
  @RequestMapping("/order/exportExcel")
  public void export2Excel(OrderSearchForm form, HttpServletResponse resp) {
    Map<String, Object> params = transForm2Map(form);
    List<OrderVO> orders = orderService.selectOrders4WxExport(params);
    List<OrderExportVO> ordersExport = new ArrayList<OrderExportVO>();

    for (OrderVO vo : orders) {
      String orderId = vo.getId();
      List<OrderItem> items = orderItemMapper.selectByOrderId(orderId);
      OrderAddress orderAddress = vo.getOrderAddress();
      List<Zone> zoneList = null;
      String street = "";
      String consignee = vo.getBuyerName();
      if (orderAddress != null) {
        String zoneId = vo.getOrderAddress().getZoneId();
        zoneList = zoneService.listParents(zoneId);
        street = orderAddress.getStreet();
        consignee = orderAddress.getConsignee();
      }
      for (OrderItem item : items) {
        OrderVO ordervo = new OrderVO();
        BeanUtils.copyProperties(vo, ordervo);
        ordervo.setItemProductName(item.getProductName());
        ordervo.setItemAmount(item.getAmount());
        ordervo.setItemPrice(item.getPrice());

        String encode = productMapper.selectEncodeById(item.getProductId());
        String u8Encode = productMapper.selectU8EncodeById(item.getProductId());
        OrderExportVO exportVO = new OrderExportVO(ordervo, zoneList, street,
            encode, u8Encode, consignee);
        ordersExport.add(exportVO);
      }
    }

    String sheetStr = "订单详情";
    String filePrefix = "orderList";
//		String[] secondTitle = new String[] {
//				"订单编号",
//				"物流公司",
//				"快递单号",
//				//"物流费用",
////				"卖家",
//				"买家",
//				//"店铺名称",
//				"订单金额",
//				//"付款时间",
//				"收件地址","联系电话","收件人",
//				//"交易类型","支付类型",
//				 "订单状态",
//				// "付款状态",
//				"创建时间",
//				"商品名称","商品数量","商品单价"};
//
//		String[] strBody = new String[] {
//				"getOrderNo",
//				"getLogisticsCompany",
//				"getLogisticsOrderNo",
//				//"getLogisticsFee",
////				"getSellerName",
//				"getBuyerName",
//				//"getShopName",
//				"getTotalFee",
//				//"getPaidAt",
//				"getAddressDetails", "getBuyerPhone", "getOrderAddress.getConsignee",
//				//"getTypeStr", "getPayTypeStr",
//				"getStatusStr",
//				//"getPaidStatus",
//				"getCreatedAtStr",
//				"getItemProductName","getItemAmount","getItemPrice"};

    excelService.export(filePrefix, ordersExport, OrderExportVO.class, sheetStr,
        transParams2Title(params), OrderExportSpec.KEY_SET,
        OrderExportSpec.VALUE_SET, resp, false);
  }

  private Map<String, Object> transForm2Map(OrderSearchForm orderSearchForm) {
    Map<String, Object> params = new LinkedHashMap<String, Object>();
    //params.put("rootShopId", getCurrentUser().getShopId());

    if (StringUtils.isNotBlank(orderSearchForm.getStartDate())) {
      params.put("startDate", orderSearchForm.getStartDate());
    }

    if (StringUtils.isNotBlank(orderSearchForm.getEndDate())) {
      try {
        Date date = DateUtils
            .addDays(DateUtils.parseDate(orderSearchForm.getEndDate(), "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (StringUtils.isNotEmpty(orderSearchForm.getOrderNo())) {
      params.put("orderNo", "%" + orderSearchForm.getOrderNo() + "%");
    }

    if (orderSearchForm.getOrderStatus() != null) {
      params.put("status", new OrderStatus[]{orderSearchForm.getOrderStatus()});
    }
    if (orderSearchForm.getPaymentMode() != null) {
      params.put("payType", new PaymentMode[]{orderSearchForm.getPaymentMode()});
    }

    if (orderSearchForm.getOrderType() != null) {
      params.put("orderType", orderSearchForm.getOrderType());
    }
    if (orderSearchForm.getGrouponStatus() != null) {
      params.put("grouponStatus", orderSearchForm.getGrouponStatus());
    }

    if (orderSearchForm.getAgentType() != null) {
      params.put("agentType", orderSearchForm.getAgentType());
    }
    if (StringUtils.isNotEmpty(orderSearchForm.getBuyerName())) {
      params.put("buyerName", "%" + orderSearchForm.getBuyerName() + "%");
    }
    if (StringUtils.isNotEmpty(orderSearchForm.getSellerName())) {
      params.put("sellerName", "%" + orderSearchForm.getSellerName() + "%");
    }

    return params;
  }

  private String transParams2Title(Map<String, Object> params) {

    Map keyCn = new HashMap();
    keyCn.put("startDate", "下单开始时间");
    keyCn.put("endDate", "下单结束时间");
    keyCn.put("status", "订单状态");
    keyCn.put("orderNo", "订单号");
    keyCn.put("agentType", "买家级别");
    keyCn.put("buyerName", "订货经销商");
    keyCn.put("sellerName", "接单经销商");

    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          // 结束时间需要特别处理，查询条件里加了一天，这里展示需要减去一天
          if ("endDate".equals(key)) {
            Date endDate = (Date) value;
            Date date = DateUtils.addDays(endDate, -1);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            value = sdf.format(date);
          }
          result += "{" + keyCn.get(key) + "=" + value.toString().replaceAll("%", "") + "}";
          i++;
        }
      }
    }
    return "订单查询条件：" + result;

  }

  /**
   * 改价
   */
  @RequestMapping("/order/update-price")
  @ResponseBody
  public Order updateOrderPrice(@RequestParam String orderId,
      String goodsFee, String logisticsFee) {
    if (StringUtils.isBlank(goodsFee) && StringUtils.isBlank(logisticsFee)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "邮费和商品费用不能都为0");
    }

    BigDecimal bGoodsFee = NumberUtils.createBigDecimal(goodsFee).setScale(
        2, BigDecimal.ROUND_DOWN);
    logisticsFee = StringUtils.defaultIfBlank(logisticsFee, "0");
    BigDecimal bLogisticsFee = NumberUtils.createBigDecimal(logisticsFee);
    // 商品价格不能改为0
    orderService.updatePrice(orderId, bGoodsFee, bLogisticsFee,
        getCurrentUser().getId());
    return orderService.load(orderId);
  }

  /**
   * 修改邮费
   */
  @RequestMapping(value = "/postAge")
  public String index(Principal principal, Model model) {
    return "settings/set-postage";
  }

  /*
   * @RequestMapping(value = "/desc") public String desc() { return
   * "settings/set-desc"; }
   */

  @RequestMapping(value = "/setShop")
  public String setShop(Model model) {
    return "settings/set-shop";
  }

  @RequestMapping(value = "/taobao-move")
  public String taobaoMove(Model model) {
    return "taobao-move";
  }

  // ===============================私有方法====================
  private void setImgAndZones(OrderVO... orders) {
    for (OrderVO order : orders) {
      // 订单图片
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        if (StringUtils.isBlank(imgUrl)) {
          continue;
        }

        imgUrl = resourceFacade.resolveUrl(imgUrl);
        // 解析图片
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      // 收货地址
      List<Zone> zoneList = zoneService.listParents(order
          .getOrderAddress().getZoneId());
      String addressDetails = "";
      for (Zone zone : zoneList) {
        addressDetails += zone.getName();
      }
      addressDetails += order.getOrderAddress().getStreet();
      order.setAddressDetails(addressDetails);
    }
  }

  // 回填分佣金信息
  private void setCommissionInfo(OrderVO order) {
    List<Commission> commissions = unionService
        .listByOrderId(order.getId());
    BigDecimal cmFee = BigDecimal.ZERO;
    for (Commission cm : commissions) {
      cmFee = cmFee.add(cm.getFee());
    }
    order.setCommissionFee(cmFee);
    if (order.getDiscountFee() == null) {
      order.setDiscountFee(BigDecimal.ZERO);
    }
    BigDecimal handingFee = orderService.loadTechServiceFee(order);
    if (!handingFee.equals(BigDecimal.ZERO)) {
      Formatter fmt = new Formatter();
      try {
        fmt.format(ORDERNOTES, serviceFeethreshold,
            handingFee.setScale(2, BigDecimal.ROUND_HALF_UP));
        order.setNotes(fmt.toString());
      } finally {
        fmt.close();
      }
    }

    order.setHongbaoAmount(handingFee);

  }

  /**
   * 回填物流信息
   */
  private void setLogisticsInfo(OrderVO order) {
    // 设置物流公司官网
    for (LogisticsCompany logisticsCompany : LogisticsCompany.values()) {
      if (logisticsCompany.getName().equals(order.getLogisticsCompany())) {
        order.setLogisticsOfficial(logisticsCompany.getUrl());
      }
    }

    // TODO 兼容老版本处理
    if ("顺丰".equals(order.getLogisticsCompany())
        || "顺丰快递".equals(order.getLogisticsCompany())
        || "SF_EXPRESS".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sf-express.com/");
    } else if ("圆通".equals(order.getLogisticsCompany())
        || "圆通快递".equals(order.getLogisticsCompany())
        || "YTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yto.net.cn/");
    } else if ("申通".equals(order.getLogisticsCompany())
        || "STO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sto.cn/");
    } else if ("中通".equals(order.getLogisticsCompany())
        || "ZTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.zto.cn/");
    } else if ("百世汇通".equals(order.getLogisticsCompany())
        || "BESTEX".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.htky365.com/");
    } else if ("韵达".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yundaex.com/");
    } else if ("天天".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ttkdex.com/");
    } else if ("全峰".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.qfkd.com.cn/");
    } else if ("邮政EMS".equals(order.getLogisticsCompany())
        || "中国邮政".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ems.com.cn/");
    } else {
      order.setLogisticsOfficial("");
    }
  }
}

