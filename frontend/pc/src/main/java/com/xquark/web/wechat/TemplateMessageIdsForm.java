package com.xquark.web.wechat;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by dongsongjie on 16/5/25.
 */
public class TemplateMessageIdsForm {

  @NotEmpty
  private String financeEvent; // 资金变更通知

  @NotEmpty
  private String pointEvent; // 积分变动通知

  @NotEmpty
  private String orderEvent; // 订单通知

  @NotEmpty
  private String orderDeliveryEvent; // 订单发货通知

  @NotEmpty
  private String refundEvent; // 退款通知

  @NotEmpty
  private String distributorApplicationEvent; // 分销商申请通知

  @NotEmpty
  private String commissionEvent; // 佣金通知

  @NotEmpty
  private String productDetailEvent; // 商详通知

  @NotEmpty
  private String newPartnerEvent; // 新合伙人通知

  @NotEmpty
  private String partnerObtainCommissionEvent; // 合伙人获得佣金通知

  public String getFinanceEvent() {
    return financeEvent;
  }

  public void setFinanceEvent(String financeEvent) {
    this.financeEvent = financeEvent;
  }

  public String getPointEvent() {
    return pointEvent;
  }

  public void setPointEvent(String pointEvent) {
    this.pointEvent = pointEvent;
  }

  public String getOrderEvent() {
    return orderEvent;
  }

  public void setOrderEvent(String orderEvent) {
    this.orderEvent = orderEvent;
  }

  public String getOrderDeliveryEvent() {
    return orderDeliveryEvent;
  }

  public void setOrderDeliveryEvent(String orderDeliveryEvent) {
    this.orderDeliveryEvent = orderDeliveryEvent;
  }

  public String getRefundEvent() {
    return refundEvent;
  }

  public void setRefundEvent(String refundEvent) {
    this.refundEvent = refundEvent;
  }

  public String getDistributorApplicationEvent() {
    return distributorApplicationEvent;
  }

  public void setDistributorApplicationEvent(String distributorApplicationEvent) {
    this.distributorApplicationEvent = distributorApplicationEvent;
  }

  public String getCommissionEvent() {
    return commissionEvent;
  }

  public void setCommissionEvent(String commissionEvent) {
    this.commissionEvent = commissionEvent;
  }

  public String getProductDetailEvent() {
    return productDetailEvent;
  }

  public void setProductDetailEvent(String productDetailEvent) {
    this.productDetailEvent = productDetailEvent;
  }

  public String getNewPartnerEvent() {
    return newPartnerEvent;
  }

  public void setNewPartnerEvent(String newPartnerEvent) {
    this.newPartnerEvent = newPartnerEvent;
  }

  public String getPartnerObtainCommissionEvent() {
    return partnerObtainCommissionEvent;
  }

  public void setPartnerObtainCommissionEvent(String partnerObtainCommissionEvent) {
    this.partnerObtainCommissionEvent = partnerObtainCommissionEvent;
  }
}
