package com.xquark.web.sku;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/11/22.
 */
@Controller
public class SkuAttributeController extends BaseController {

  @RequestMapping(value = "/skuAttribute/list")
  public String list(Model model) {
    return "skuAttribute/list";
  }

}
