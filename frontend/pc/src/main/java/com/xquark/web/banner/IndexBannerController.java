package com.xquark.web.banner;

import com.alibaba.fastjson.JSON;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.CategoryMapper;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.IndexBanner;
import com.xquark.dal.model.Product;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.Taxonomy;
import com.xquark.service.banner.IndexBannerService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @ClassName: IndexBannerController
 * @Description: 首页优化 Banner Api
 * @Author: Kwonghom
 * @CreateDate: 2019/4/2 11:36
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/index")
public class IndexBannerController extends BaseController {

    private Logger log = LoggerFactory.getLogger(getClass());

    private static final String PRODUCT_URI="hanwei://product/";
    private static final String CATEGORY_URI="hanwei://category/";

    @Value("${site.web.host.name}")
    private String siteHost;

    @Autowired
    private UrlHelper urlHelper;

    @Autowired
    private IndexBannerService indexBannerService;

    @Autowired
    private ShopService shopService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    protected ProductService productService;

    public @RequestMapping(value = "/banner/save", method = RequestMethod.POST)
    ResponseObject<Boolean> saveIndexBanner(@RequestBody Map<String, Object> map) {
        IndexBanner indexBanner = JSON.parseObject(JSON.toJSONString(map), IndexBanner.class);
        if (null == indexBanner) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数异常, 保存失败");
        }
        if (indexBannerService.insertIndexBanner(indexBanner) != 1) {
            return new ResponseObject<>(false);
        }
        return new ResponseObject<>(true);
    }

    public @RequestMapping(value = "/banner/search", method = {RequestMethod.POST, RequestMethod.GET})
    ResponseObject<Map<String, Object>> searchIndexBanner(@RequestBody Map<String, Object> map,@PageableDefault(size = 12) Pageable pageable) {
        Map<String, Object> resultMap = new HashMap<>();
        List<IndexBanner> bannerList = indexBannerService.searchIndexBanner(map, pageable);
        long totalCount = indexBannerService.countIndexBannerBySearch(map);
        resultMap.put("bannerList", bannerList);
        resultMap.put("totalCount", totalCount);
        return new ResponseObject<>(resultMap);
    }

    public @RequestMapping(value = "/banner/update", method = RequestMethod.POST)
    ResponseObject<Boolean> updateIndexBanner(@RequestBody Map<String, Object> map) {
        IndexBanner indexBanner = JSON.parseObject(JSON.toJSONString(map), IndexBanner.class);
        if (StringUtils.isNotEmpty(indexBanner.getId())) {
            long id = IdTypeHandler.decode(indexBanner.getId());
            log.info("当前操作的 banner id = {}", id);
            boolean b = indexBannerService.selectExists(indexBanner.getId());
            if (!b) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "更新失败, banner 不存在");
            }
            if (indexBannerService.updateByPrimaryKey(indexBanner) == 1) {
                return new ResponseObject<>(true);
            } else {
                return new ResponseObject<>(false);
            }
        } else {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "更新失败, id 不能为空");
        }
    }

    public @RequestMapping("/banner/edit")
    ResponseObject<Map<String, Object>> selectByPrimaryKey(@RequestParam("id") String id) {
        Map<String, Object> resultMap = new HashMap<>();
        boolean b = indexBannerService.selectExists(id);
        if (!b) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "编辑失败, banner 不存在");
        }
        IndexBanner indexBanner = indexBannerService.selectByPrimaryKey(id);
        IndexBannerVO indexBannerVO = new IndexBannerVO();
        BeanUtils.copyProperties(indexBanner, indexBannerVO);
        switch (indexBanner.getJumpType()) {
            case PRODUCT:
                String productId = indexBanner.getJumpTypeLink().substring(PRODUCT_URI.length());
                ProductVO productVO = productService.load(productId);
                if (productVO != null) {
                    indexBannerVO.setProductId(productId);
                    indexBannerVO.setProductName(productVO.getName());
                }
                break;
            case SECOND_CATEGORY:
                String secondCategoryId = indexBanner.getJumpTypeLink().substring(CATEGORY_URI.length());
                Category category = categoryService.load(secondCategoryId);
                if (category != null) {
                    String treePath = category.getTreePath() + secondCategoryId;
                    indexBannerVO.setCategoryStr(treePath);
                }
                break;
            default:
                break;
        }
        resultMap.put("indexBanner", indexBannerVO);
        return new ResponseObject<>(resultMap);
    }

    public @RequestMapping(value = "/banner/change/sequence", method = RequestMethod.POST)
    ResponseObject<Map<String, Object>> changeBannerSequence(HttpServletRequest req, HttpServletResponse rep,
                                @RequestBody Map<String, Object> map, @PageableDefault(size = 12) Pageable pageable) {
        if (null == map.get("direction") || "".equals(map.get("direction"))) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不正确, 顺序调整失败!");
        }
        if (null == map.get("id") || "".equals(map.get("id"))) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "id 不可为空");
        }
        boolean b = indexBannerService.selectExists((String) map.get("id"));
        if (!b) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "banner 不存在");
        }
        boolean b1 = indexBannerService.changeBannerSequence(map, pageable);

        // 跨域不支持 Session 共享, 禁用重定向
/*        if (b1)
            try {
                req.setAttribute("pageable", Boolean.TRUE);
                req.setAttribute("page", pageable.getOffset());
                rep.sendRedirect("/sellerpc/index/banner/list");
            } catch (IOException e) {
                log.error("重定向异常, 异常信息: {}", e.getMessage());
                e.printStackTrace();
            }
        return "";*/

        if (b1) {
            return selectIndexBanners(pageable);
        }
        throw new BizException(GlobalErrorCode.ERROR_PARAM, "调整失败...");
    }

    public @RequestMapping("/banner/list")
    ResponseObject<Map<String, Object>> getIndexBanners(@PageableDefault(size = 12) Pageable pageable) {
        return selectIndexBanners(pageable);
    }

    public @RequestMapping(value = "/banner/end", method = RequestMethod.GET)
    ResponseObject<Boolean> endIndexBanner(@RequestParam String id) {
        log.info("当前操作的 banner id = {}", id);
        boolean b = indexBannerService.selectExists(id);
        if (!b) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "手动结束失败, banner 不存在");
        }
        IndexBanner indexBanner = indexBannerService.selectByPrimaryKey(id);
        final Date curDate = new Date();
        if (indexBanner.getStartAt().before(curDate) && indexBanner.getEndAt().after(curDate)) {
            indexBanner.setEndAt(indexBanner.getStartAt());
            indexBannerService.updateByPrimaryKey(indexBanner);
            return new ResponseObject<>(true);
        } else {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "只有正在进行的banner才能结束...");
        }
    }

    public @RequestMapping(value = "/banner/deleted", method = RequestMethod.GET)
    ResponseObject<Boolean> deletedIndexBanner(@RequestParam String id) {
        log.info("当前操作的 banner id = {}", id);
        boolean b = indexBannerService.selectExists(id);
        if (!b) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "逻辑删除失败, banner 不存在");
        }
        int i = indexBannerService.deletedByPrimaryKey(id);
        return new ResponseObject<>(true);
    }

    public @RequestMapping(value = "/banner/category/list", method = RequestMethod.POST)
    ResponseObject<List<CategoryVO>> getAllFirstCategory(@RequestBody Map<String, Object> map
            /*@RequestParam("level") Integer level,
            @RequestParam(required = false) String shopId,
            @RequestParam(required = false) String parentCategoryId*/) {

        final Integer level = (Integer) map.get("level");
        final String shopId = (String) map.get("shopId");
        final String parentCategoryId = (String) map.get("parentCategoryId");


        List<CategoryVO> list = new ArrayList<>();
        List<Category> categories;
        String rootShopId = shopService.loadRootShop().getId();
        categories = categoryService.loadCategoriesByShopId(rootShopId, level, parentCategoryId,
                Taxonomy.GOODS);

        for (Category cat : categories) {
            CategoryVO vo = new CategoryVO();
            BeanUtils.copyProperties(cat, vo);
            vo.setCategoryImgUrl(cat.getImg());
            vo.setDisplayName(vo.getName());
//            long productCount = categoryService.countUserGoodsProducts(cat.getId());
//            vo.setProductCount(productCount);
            vo.setProductCount(0);
//            long sellerCount = categoryService.countUserCategorySellers(shopId, cat.getId());
//            vo.setSellerCount(sellerCount);
            vo.setSellerCount(0);
//            Category categoryOnsale = categoryMapper.loadBySourceIdAndShopId(cat.getId(), shopId);
//            if (categoryOnsale != null && categoryOnsale.getStatus().equals(CategoryStatus.ONSALE)) {
//                vo.setOnSaleForSeller(true);
//            } else {
//                vo.setOnSaleForSeller(false);
//            }
            vo.setOnSaleForSeller(false);

            //分类下所有商品列表
            //http://51shop.mobi/shop/brand/cabvysu7/6689?root=true&currentSellerShopId=cabvysu7
            vo.setCategoryH5Url(
                    siteHost + "/shop/brand/" + shopId + "/" + cat.getId() + "?root=true&currentSellerShopId="
                            + shopId);
            list.add(vo);
        }
        return new ResponseObject<>(list);
    }

    public @RequestMapping(value = "/banner/product/searchByPc", method = RequestMethod.POST)
    ResponseObject<Map<String, Object>> selectProductFromSearch(@RequestBody Map<String, Object> params, ProductType type,
            /*@RequestParam(required = false) String key,
            @RequestParam(required = false) String productId,
            @RequestParam(required = false) String categoryId,*/ Pageable page) {
        final String key = (String) params.get("key");
        final String productId = (String) params.get("productId");
        final String categoryId = (String) params.get("categoryId");

        Map<String, Object> map = new HashMap<>();
        String rootShopId = shopService.loadRootShop().getId();
        List<Product> list = productService.searchWithNoStatus(rootShopId, key, page, type, productId, categoryId);
        List<ProductVOEx> voList = new ArrayList<>();
        for (Product product : list) {
            ProductVO vo = new ProductVO(product);
            ProductVOEx voEX = new ProductVOEx(vo, urlHelper.genProductUrl(product.getId()),
                    product.getImg(), null);
            vo.setImgUrl(product.getImg());
            voList.add(voEX);

        }

        map.put("list", voList);
        Long count = productService.CountTotalByNameWithNoStatus(rootShopId, key, productId, categoryId);
        map.put("categoryTotal", count);
        return new ResponseObject<>(map);
    }

    public @RequestMapping("/app/banners")
    ResponseObject<List<IndexBanner>> getAppIndexBanners() {
        return new ResponseObject<>(indexBannerService.getAppIndexBanners());
    }


    private ResponseObject<Map<String, Object>> selectIndexBanners(@PageableDefault(size = 12) Pageable pageable) {
        Map<String, Object> resultMap = new HashMap<>();
        List<IndexBanner> bannerList = indexBannerService.getIndexBanners(pageable);
        long totalCount = indexBannerService.countIndexBannerList();
        resultMap.put("bannerList", bannerList);
        resultMap.put("totalCount", totalCount);
        return new ResponseObject<>(resultMap);
    }
}
