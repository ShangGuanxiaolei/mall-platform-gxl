package com.xquark.web.signin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class SigninController {

  private static Logger log = LoggerFactory.getLogger(SigninController.class);

  @RequestMapping(value = "signin")
  public String signin() {
    return "/pc/login";
  }


  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String index(Principal principal, @RequestParam(required = false) String accessMode,
      Model model) {

    if (principal != null && accessMode != null && "XQuark".equals(accessMode)) {
      model.addAttribute("accessMode", "Developer");
    }

    return principal != null ? "redirect:/mall/summary" : "/pc/login";
  }
}
