package com.xquark.web.shop;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class OrderExportSpec {

  public static final String SHOP_NAME = "微信商城";

  public static final String SHEET_STR = "订单详情";

  private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

  public static String[] KEY_SET;
  public static String[] VALUE_SET;

  static {
    EXPORT_FORMAT.put("外部订单号", "getOrderNo");
    EXPORT_FORMAT.put("店铺名称", "getShopName");
    EXPORT_FORMAT.put("买家昵称", "getBuyerName");
    EXPORT_FORMAT.put("收货人名称", "getConsignee");
    EXPORT_FORMAT.put("手机号码", "getBuyerPhone");
    EXPORT_FORMAT.put("联系电话", "getBuyerPhone");
    EXPORT_FORMAT.put("是否开票", "getNeedInvoiceStr");
    EXPORT_FORMAT.put("开票抬头", "getInvoiceSpec");
    EXPORT_FORMAT.put("邮政编码", "getZipCode");
    EXPORT_FORMAT.put("省", "getProvince");
    EXPORT_FORMAT.put("市", "getCity");
    EXPORT_FORMAT.put("区", "getArea");
    EXPORT_FORMAT.put("收货地址", "getStreet");
    EXPORT_FORMAT.put("备注", "getRemark");
    EXPORT_FORMAT.put("送货方式", "getDelivery"); // 暂无
    EXPORT_FORMAT.put("运费", "getLogisticsFee");
    EXPORT_FORMAT.put("物流公司", "getLogisticsCompany");
    EXPORT_FORMAT.put("U9编码", "getProductCode");
    EXPORT_FORMAT.put("U8编码", "getU8Encode");
    EXPORT_FORMAT.put("货品名称", "getItemProductName");
    EXPORT_FORMAT.put("货品数量", "getItemAmount");
    EXPORT_FORMAT.put("货品单价", "getItemPrice");
    EXPORT_FORMAT.put("货品开票单价", "getItemPrice");
    EXPORT_FORMAT.put("卖家备注", "getRemark");
    EXPORT_FORMAT.put("安装省", "getProvince");
    EXPORT_FORMAT.put("安装市", "getCity");
    EXPORT_FORMAT.put("安装区", "getArea");
    EXPORT_FORMAT.put("安装地址", "getStreet");
    EXPORT_FORMAT.put("订单类型：1 经销、2 一件代发", "getOrderDeliveryType");
    KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
    VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
  }

  private static String[][] getArrayFromHash(Map<String, String> data) {
    String[][] str;
    {
      Object[] keys = data.keySet().toArray();
      Object[] values = data.values().toArray();
      str = new String[keys.length][values.length];
      for (int i = 0; i < keys.length; i++) {
        str[0][i] = (String) keys[i];
        str[1][i] = (String) values[i];
      }
    }
    return str;
  }
}
