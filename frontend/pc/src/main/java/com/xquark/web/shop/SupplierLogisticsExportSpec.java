package com.xquark.web.shop;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class SupplierLogisticsExportSpec {

  public static final String SHEET_STR = "物流信息模板";

  private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

  public static String[] KEY_SET;
  public static String[] VALUE_SET;

  static {
    EXPORT_FORMAT.put("订单编号", "getOrderNo");
    EXPORT_FORMAT.put("下单时间","getCreatedAt");
    EXPORT_FORMAT.put("收件人姓名","getBuyerName");
    EXPORT_FORMAT.put("收件人电话","getBuyerPhone");
    EXPORT_FORMAT.put("快递公司名称", "getLogisticsCompany");
    EXPORT_FORMAT.put("快递单号", "getLogisticsOrderNo");
    EXPORT_FORMAT.put("快递单号2", "getLogisticsOrderNo2");
    EXPORT_FORMAT.put("快递单号3", "getLogisticsOrderNo3");
    EXPORT_FORMAT.put("快递单号4", "getLogisticsOrderNo4");
    EXPORT_FORMAT.put("快递单号5", "getLogisticsOrderNo5");

    KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
    VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
  }

  private static String[][] getArrayFromHash(Map<String, String> data) {
    String[][] str;
    {
      Object[] keys = data.keySet().toArray();
      Object[] values = data.values().toArray();
      str = new String[keys.length][values.length];
      for (int i = 0; i < keys.length; i++) {
        str[0][i] = (String) keys[i];
        str[1][i] = (String) values[i];
      }
    }
    return str;
  }
}
