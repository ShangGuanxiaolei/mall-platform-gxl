package com.xquark.web.wechat;

import com.xquark.dal.model.wechat.ConditionType;
import com.xquark.dal.model.wechat.MatchType;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by dongsongjie on 16/5/25.
 */
public class AutoReplyForm {

  private String id;

  @NotEmpty
  private String name;

  @NotEmpty
  private String condition;

  @NotNull
  private ConditionType conditionType;

  @NotNull
  private MatchType matchType;

  @NotEmpty
  private String replyContent;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCondition() {
    return condition;
  }

  public void setCondition(String condition) {
    this.condition = condition;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public void setMatchType(MatchType matchType) {
    this.matchType = matchType;
  }

  public String getReplyContent() {
    return replyContent;
  }

  public void setReplyContent(String replyContent) {
    this.replyContent = replyContent;
  }

  public ConditionType getConditionType() {
    return conditionType;
  }

  public void setConditionType(ConditionType conditionType) {
    this.conditionType = conditionType;
  }
}
