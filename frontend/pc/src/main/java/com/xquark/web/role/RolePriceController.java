package com.xquark.web.role;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 16/12/13.
 */
@Controller
public class RolePriceController extends BaseController {

  @RequestMapping(value = "/rolePrice/list")
  public String list(Model model) {
    return "rolePrice/rolePriceList";
  }

}
