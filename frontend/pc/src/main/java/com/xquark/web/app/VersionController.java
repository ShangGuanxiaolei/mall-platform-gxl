package com.xquark.web.app;

import com.xquark.web.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 17/4/1.
 */
@Controller
public class VersionController extends BaseController {

  @RequestMapping(value = "/version/list")
  public String list(Model model) {
    return "app/version";
  }

}
