package com.xquark.web.partner;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.OrderMessage;
import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.model.PartnerShopCommission;
import com.xquark.dal.model.PartnerType;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.CommissionType;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.dal.vo.OrderFeeVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.voex.OrderVOEx;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.partner.PartnerSettingService;
import com.xquark.service.partner.PartnerShopComService;
import com.xquark.service.partner.PartnerTypeService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
public class PartnerController extends BaseController {

  private final String ORDERNOTES = "您的交易金额大于%s，需要支付%s手续费";
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private OrderService orderService;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private ZoneService zoneService;
  @Autowired
  private CashierService cashierService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private OrderRefundService orderRefundService;
  @Autowired
  private UnionService unionService;
  @Autowired
  private ExcelService excelService;
  @Autowired
  private PartnerSettingService partnerSettingService;
  @Autowired
  private UserService userService;
  @Autowired
  private CommissionService commissionService;
  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private PartnerShopComService partnerShopComService;

  @Autowired
  private PartnerTypeService partnerTypeService;

  @Value("${order.delaysign.date}")
  private int defDelayDate;

  @Value("${tech.serviceFee.standard}")
  private String serviceFeethreshold;


  @RequestMapping(value = "/partner/summary")
  public String partnerSummary(Model model) {
    return "/partner/partnerSummary";
  }

  @ResponseBody
  @RequestMapping("/partner/exportPartnerChildrenExcel")
  public void twitterChildren(@RequestParam String userId, HttpServletResponse resp)
      throws IOException {
    //TODO liangfan
    Map<String, Object> params = new HashedMap();
    String shopId = shopService.findByUser(userId).getId();
    String userName = userService.load(userId).getName();
    String shopName = shopService.findByUser(userId).getName();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<ShopTree> result = shopTreeService.listChildren(rootShopId, shopId);
    List<User> userList = new ArrayList<User>();
    for (ShopTree shopTree : result) {
      String userShopId = shopTree.getDescendantShopId();
      User user = userService.load(shopService.load(userShopId).getOwnerId());
      userList.add(user);
    }

    String sheetStr = "合伙人下级列表";
    String filePrefix = "下级列表";
    //TODO
    String[] secondTitle = new String[]{
        "推客名字",
        "推客手机号"};

    String[] strBody = new String[]{
        "getName",
        "getPhone"};
    //TODO
    excelService.export(filePrefix,
        userList, User.class,
        sheetStr,
        "推客:" + userName + "的店铺: " + shopName + " 所有下级",
        secondTitle, strBody, resp, true);
  }

  @RequestMapping(value = "/partner/setting")
  public String partnerSetting(Model model) {
    //获得shopId
    String shopId = getCurrentUser().getShopId();
    PartnerShopCommission platform = null;    // 平台合伙人佣金
    PartnerShopCommission team = null;          //团队合伙人佣金
    PartnerShopCommission shareholder = null;  //股东合伙人佣金
    //加载合伙人默认配置的佣金信息
    List<PartnerShopCommission> partnerShopCommissions = partnerShopComService
        .selectByShopId(shopId);
    for (PartnerShopCommission partnerShopCommission : partnerShopCommissions) {
      if (partnerShopCommission.getType() == CommissionType.PLATFORM) {
        platform = partnerShopCommission;
      }
      if (partnerShopCommission.getType() == CommissionType.TEAM) {
        team = partnerShopCommission;
      }
      if (partnerShopCommission.getType() == CommissionType.SHAREHOLDER) {
        shareholder = partnerShopCommission;
      }
    }
    model.addAttribute("platform", platform);
    model.addAttribute("team", team);
    model.addAttribute("shareholder", shareholder);
    return "/partner/partnerSetting";
  }

  @RequestMapping(value = "/partner/member")
  public String partnerMember(Model model) {
    //获得shopId
    String shopId = getCurrentUser().getShopId();
    // 获取设置的团队，股东，平台合伙人类型
    List<PartnerType> types = partnerTypeService.listByShopId(shopId);
    ArrayList<PartnerType> teamTypes = new ArrayList<PartnerType>();
    ArrayList<PartnerType> platformTypes = new ArrayList<PartnerType>();
    ArrayList<PartnerType> shareholderTypes = new ArrayList<PartnerType>();
    for (PartnerType type : types) {
      if (type.getType() == CommissionType.TEAM) {
        teamTypes.add(type);
      } else if (type.getType() == CommissionType.PLATFORM) {
        platformTypes.add(type);
      } else if (type.getType() == CommissionType.SHAREHOLDER) {
        shareholderTypes.add(type);
      }
    }
    model.addAttribute("platformTypes", platformTypes);
    model.addAttribute("teamTypes", teamTypes);
    model.addAttribute("shareholderTypes", shareholderTypes);
    return "/partner/partnerMember";
  }

  @RequestMapping(value = "/partner/apply")
  public String partnerApply(Model model) {
    return "/partner/partnerApply";
  }

  @RequestMapping(value = "/partner/record")
  public String partnerRecord(Model model) {
    return "/partner/partnerRecord";
  }

  @RequestMapping(value = "/partner/order")
  public String partnerOrder(Model model, OrderStatus status, Integer size,
      Integer page, Boolean isDesc, String orderName, String key) {

    return "/partner/partnerOrder";
  }


  // 订单详情
  @RequestMapping(value = "/partner/order/details")
  public String ordersDetails(@RequestParam String orderId, Model model,
      HttpServletRequest req) {
    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("order.not.found"));
    }
    setLogisticsInfo(order);

    Shop shop = shopService.load(order.getShopId());
    if (shop != null) {
      order.setShopName(shop.getName());
    }
    // 设置图片和地区
    setImgAndZones(order);

    List<CouponInfoVO> orderCoupons = cashierService
        .loadCouponInfoByOrderNo(order.getOrderNo());
    order.setOrderCoupons(orderCoupons);

    List<OrderFeeVO> list = orderService.findOrderFees(order);
    OrderVOEx orderEx = new OrderVOEx(order, list);

    BigDecimal discountFee = orderEx.getDiscountFee();
    if (discountFee == null) {
      discountFee = BigDecimal.ZERO;
    }

    orderEx.setDiscountFee(discountFee.add(order.getHongbaoAmount()));
    orderEx.setDefDelayDate(defDelayDate);
    orderEx.setRefundableFee(orderEx.getTotalFee().subtract(
        orderEx.getLogisticsFee()));
    orderEx.setShowRefundBtn(false);

    // 快店IOS审核失败，暂时关闭买家端退款入口
    if (orderEx.getStatus() != OrderStatus.SUBMITTED
        && orderEx.getStatus() != OrderStatus.SUCCESS
        && orderEx.getStatus() != OrderStatus.CANCELLED) {
      if (orderEx.getStatus() == OrderStatus.CLOSED) {
        List<OrderRefund> refunds = orderRefundService
            .listByOrderId(order.getId());
        if (refunds.size() > 0) {
          orderEx.setShowRefundBtn(true);
        }
      } else {
        orderEx.setShowRefundBtn(true);
      }
    }

    model.addAttribute("order", orderEx);
    List<OrderMessage> messages = orderService.viewMessages(orderEx.getId());
    model.addAttribute("messages", messages);
    return "/partner/partnerOrderDetail";
  }

  private void setImgAndZones(OrderVO... orders) {
    for (OrderVO order : orders) {
      // 订单图片
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        if (StringUtils.isBlank(imgUrl)) {
          continue;
        }

        imgUrl = resourceFacade.resolveUrl(imgUrl);
        // 解析图片
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      // 收货地址
      List<Zone> zoneList = zoneService.listParents(order
          .getOrderAddress().getZoneId());
      String addressDetails = "";
      for (Zone zone : zoneList) {
        addressDetails += zone.getName();
      }
      addressDetails += order.getOrderAddress().getStreet();
      order.setAddressDetails(addressDetails);
    }
  }

  /**
   * 回填物流信息
   */
  private void setLogisticsInfo(OrderVO order) {
    // 设置物流公司官网
    for (LogisticsCompany logisticsCompany : LogisticsCompany.values()) {
      if (logisticsCompany.getName().equals(order.getLogisticsCompany())) {
        order.setLogisticsOfficial(logisticsCompany.getUrl());
      }
    }

    // TODO 兼容老版本处理
    if ("顺丰".equals(order.getLogisticsCompany())
        || "顺丰快递".equals(order.getLogisticsCompany())
        || "SF_EXPRESS".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sf-express.com/");
    } else if ("圆通".equals(order.getLogisticsCompany())
        || "圆通快递".equals(order.getLogisticsCompany())
        || "YTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yto.net.cn/");
    } else if ("申通".equals(order.getLogisticsCompany())
        || "STO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sto.cn/");
    } else if ("中通".equals(order.getLogisticsCompany())
        || "ZTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.zto.cn/");
    } else if ("百世汇通".equals(order.getLogisticsCompany())
        || "BESTEX".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.htky365.com/");
    } else if ("韵达".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yundaex.com/");
    } else if ("天天".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ttkdex.com/");
    } else if ("全峰".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.qfkd.com.cn/");
    } else if ("邮政EMS".equals(order.getLogisticsCompany())
        || "中国邮政".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ems.com.cn/");
    } else {
      order.setLogisticsOfficial("");
    }
  }

  @ResponseBody
  @RequestMapping("/partner/setting/save")
  public ResponseObject<Boolean> saveFamilyCard(HttpServletRequest req) {
    //获得shopId
    String shopId = getCurrentUser().getShopId();
    String teamId = req.getParameter("teamId");
    String teamName = req.getParameter("teamName");
    String teamRate = req.getParameter("teamRate");
    String shareholderId = req.getParameter("shareholderId");
    String shareholderName = req.getParameter("shareholderName");
    String shareholderRate = req.getParameter("shareholderRate");
    String platformId = req.getParameter("platformId");
    String platformName = req.getParameter("platformName");
    String platformRate = req.getParameter("platformRate");

    // 团队合伙人处理
    PartnerShopCommission commission = new PartnerShopCommission();
    commission.setName(teamName);
    if (StringUtils.isNotEmpty(teamRate)) {
      commission.setRate(new BigDecimal(teamRate));
    }
    if (StringUtils.isNotEmpty(teamId)) {
      commission.setId(teamId);
      partnerShopComService.updateByPrimaryKeySelective(commission);
    } else {
      commission.setArchive(false);
      commission.setType(CommissionType.TEAM);
      commission.setOwnShopId(shopId);
      partnerShopComService.insert(commission);
    }

    // 股东合伙人处理
    commission = new PartnerShopCommission();
    commission.setName(shareholderName);
    if (StringUtils.isNotEmpty(shareholderRate)) {
      commission.setRate(new BigDecimal(shareholderRate));
    }
    if (StringUtils.isNotEmpty(shareholderId)) {
      commission.setId(shareholderId);
      partnerShopComService.updateByPrimaryKeySelective(commission);
    } else {
      commission.setArchive(false);
      commission.setType(CommissionType.SHAREHOLDER);
      commission.setOwnShopId(shopId);
      partnerShopComService.insert(commission);
    }

    // 平台合伙人处理
    commission = new PartnerShopCommission();
    commission.setName(platformName);
    if (StringUtils.isNotEmpty(platformRate)) {
      commission.setRate(new BigDecimal(platformRate));
    }
    if (StringUtils.isNotEmpty(platformId)) {
      commission.setId(platformId);
      partnerShopComService.updateByPrimaryKeySelective(commission);
    } else {
      commission.setArchive(false);
      commission.setType(CommissionType.PLATFORM);
      commission.setOwnShopId(shopId);
      partnerShopComService.insert(commission);
    }

    return new ResponseObject<Boolean>(true);
  }

  @ResponseBody
  @RequestMapping("/partner/order/list/exportExcel")
  public void export2Excel(PartnerOrderSearchForm form, HttpServletResponse resp) {
    Map<String, Object> params = transForm2Map(form);
    //TODO 沿用listByPartner 最多导出5000条数据
    List<OrderVO> result = orderService.listByPartner(params, null);
    for (OrderVO order : result) {
      String sellerName = userService.load(order.getSellerId()).getLoginname();
      String buyerName = userService.load(order.getBuyerId()).getName();
      order.setSellerName(sellerName);
      order.setBuyerName(buyerName);
      List<Commission> list = commissionService
          .listByOrderIdAndType(order.getId(), CommissionType.PARTNER);
      BigDecimal sumCommission = new BigDecimal(0);
      for (Commission commission : list) {
        sumCommission = sumCommission.add(commission.getFee());
      }
      order.setCommissionFee(sumCommission);

    }

    String sheetStr = "合伙人佣金";
    String filePrefix = "partnerOrderList";
    String[] secondTitle = new String[]{"订单id", "订单号", "店铺", "买家", "卖家", "佣金"};
    String[] strBody = new String[]{"getIdLong", "getOrderNo", "getShopName", "getBuyerPhone",
        "getSellerPhone", "getCommissionFee"};
    excelService
        .export(filePrefix, result, OrderVO.class, sheetStr, transParams2Title(params), secondTitle,
            strBody, resp,
            true);
  }

  private Map<String, Object> transForm2Map(PartnerOrderSearchForm form) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("rootShopId", getCurrentUser().getShopId());

    if (StringUtils.isNotBlank(form.getStartDate())) {
      params.put("startDate", form.getStartDate());
    }

    if (StringUtils.isNotBlank(form.getEndDate())) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(form.getEndDate(), "yyyy-MM-dd"), 1);
        params.put("endDate", date);
      } catch (ParseException e) {
      }
    }

    if (StringUtils.isNotBlank(form.getOrderNo())) {
      params.put("orderNo", form.getOrderNo());
    }
//        if(StringUtils.isNotBlank(form.gets).equals("") && !status.equals("ALL")){
//            List<String> statusList = new ArrayList<String>();
//            statusList.add(status);
//            params.put("status", statusList);
//        }
    if (StringUtils.isNotBlank(form.getSellerPhone())) {
      params.put("sellerPhone", form.getSellerPhone());
    }

    if (StringUtils.isNotBlank(form.getPartnerPhone())) {
      params.put("partnerPhone", form.getPartnerPhone());
    }

    return params;
  }

  private String transParams2Title(Map<String, Object> params) {
    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          result += "{" + key + "=" + value.toString() + "}";
          i++;
        }
      }
    }
    return result;
  }
}
