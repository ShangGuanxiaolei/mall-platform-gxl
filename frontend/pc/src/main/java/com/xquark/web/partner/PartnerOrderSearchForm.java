package com.xquark.web.partner;


import org.apache.commons.lang3.StringUtils;

public class PartnerOrderSearchForm {

  private String orderNo;

  private String sellerPhone;

  private String paidAt;

  private String startDate;

  private String endDate;

  private String partnerPhone;

  public String getPaidAt() {
    return paidAt;
  }

  public void setPaidAt(String paidAt) {
    this.paidAt = paidAt;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getSellerPhone() {
    return sellerPhone;
  }

  public void setSellerPhone(String sellerPhone) {
    this.sellerPhone = sellerPhone;
  }

  public String getStartDate() {
    if (StringUtils.isNotBlank(getPaidAt())) {
      if (getPaidAt().indexOf("-") != -1) {
        startDate = getPaidAt().substring(0, getPaidAt().indexOf("-")).trim();
      }
    }
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    if (StringUtils.isNotBlank(getPaidAt())) {
      if (getPaidAt().indexOf("-") != -1) {
        endDate = getPaidAt().substring(getPaidAt().indexOf("-") + 1).trim();
      }
    }
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getPartnerPhone() {
    return partnerPhone;
  }

  public void setPartnerPhone(String partnerPhone) {
    this.partnerPhone = partnerPhone;
  }
}
