package com.xquark.web.member;

import com.xquark.dal.model.User;
import com.xquark.dal.vo.MemberCountsVO;
import com.xquark.dal.vo.UserMemberVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.member.MemberService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by quguangming on 16/5/3.
 */
@Controller
public class MemberController extends BaseController {

  @Autowired
  private MemberService memberService;

  @Autowired
  private ExcelService excelService;

  /**
   * 会员概况
   */
  @RequestMapping(value = "member/info")
  public String memberInfo(Model model) {
    return "member/memberInfo";
  }

  /**
   * 会员管理
   */
  @RequestMapping(value = "member/list")
  public String memberList(Model model) {
    return "member/memberList";
  }

  @ResponseBody
  @RequestMapping(value = "member/listAllData")
  public ResponseObject<Map<String, Object>> getAllListData(Model model, Pageable pageable,
      Boolean isAsc, MemberSearchForm searchForm) {
    String[] columnsName = {"name", "consumption_points", "balance"};
    String orderName = columnsName[0];

    Map<String, Object> params = new HashMap<String, Object>();
    if (searchForm != null) {
      if (StringUtils.isNotBlank(searchForm.getAccount())) {
        params.put("name", "%" + searchForm.getAccount() + "%");
      }

      if (StringUtils.isNotBlank(searchForm.getMemberLevel())) {
        params.put("cardId", searchForm.getMemberLevel());
      }

      if (StringUtils.isNotBlank(searchForm.getPhone())) {
        params.put("phone", "%" + searchForm.getPhone() + "%");
      }

      if (StringUtils.isNoneBlank(searchForm.getEmployeeId())) {
        params.put("employeeId", searchForm.getEmployeeId());
      }

      if (StringUtils.isNoneBlank(searchForm.getScope())) {
        params.put("scope", searchForm.getScope());
      }

    }

    pageable = initPage(pageable, isAsc, orderName);
    model.addAttribute("page", pageable);
    Long total = memberService.countMember(params);
    List<UserMemberVO> members = null;
    if (total > 0) {
      members = memberService.getMemberList(params, pageable);
    }
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", total);
    data.put("rows", members);
    data.put("userId", getCurrentUser().getId());

    return new ResponseObject<Map<String, Object>>(data);
  }

  /**
   * 会员数据
   *
   * @param loginOnly 是否只导出登录过得用户
   */
  @ResponseBody
  @RequestMapping("/member/export")
  public void exportMember(HttpServletRequest request, HttpServletResponse resp,
      boolean loginOnly) {

    List<UserMemberVO> viewPages = null;
    Map<String, Object> params = new HashMap<String, Object>();
    String name = request.getParameter("account");
    String phone = request.getParameter("phone");

    if (StringUtils.isNotBlank(name)) {
      params.put("name", "%" + name + "%");
    }

    if (StringUtils.isNotBlank(phone)) {
      params.put("phone", "%" + phone + "%");
    }

    params.put("loginOnly", loginOnly);

    viewPages = memberService.getMemberList(params, null);

    String sheetStr = "会员";
    String filePrefix = "memberList";
    String[] secondTitle = new String[]{
        "昵称",
        "手机",
//                "身份证号",
//                "积分",
        "加入时间"};

    String[] strBody = new String[]{
        "getName",
        "getPhone",
//                "getIdCardNum",
//                "getYundou",
        "getCreatedAtStr"
    };

    excelService.export(filePrefix, viewPages, UserMemberVO.class, sheetStr,
        transParams2Title(params), secondTitle, strBody, resp, false);
  }

  private String transParams2Title(Map<String, Object> params) {
    LinkedHashMap keyCn = new LinkedHashMap();
    keyCn.put("name", "会员昵称 ");
    keyCn.put("phone", "手机号码");

    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          // 结束时间需要特别处理，查询条件里加了一天，这里展示需要减去一天
          if ("endDate".equals(key)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date endDate = (Date) value;
            Date date = DateUtils.addDays(endDate, -1);
            value = sdf.format(date);
          }
          result += "{" + keyCn.get(key) + "=" + value.toString().replaceAll("%", "") + "}";
          i++;
        }
      }
    }
    return "会员查询条件：" + result;

  }

  @ResponseBody
  @RequestMapping(value = "member/listData")
  public ResponseObject<Map<String, Object>> getListData(Model model, Pageable pageable,
      Integer orderIndex, Boolean isAsc, MemberSearchForm searchForm) {

    //初始化排序列数据
    String[] columnsName = {"name", "consumption_points", "balance"};
    String orderName = columnsName[0];
//        if (orderIndex != null){
//            orderName = columnsName[orderIndex.intValue()];
//        }

    User user = getCurrentUser();
    String userId = user.getId();
    String shopId = user.getShopId();

    Map<String, Object> params = new HashMap<String, Object>();
    if (searchForm != null) {
      if (StringUtils.isNotBlank(searchForm.getAccount())) {
        params.put("name", "%" + searchForm.getAccount() + "%");
      }

      if (StringUtils.isNotBlank(searchForm.getMemberLevel())) {
        params.put("memberLevel", searchForm.getMemberLevel());
      }

      if (StringUtils.isNotBlank(searchForm.getPhone())) {
        params.put("phone", "%" + searchForm.getPhone() + "%");
      }
    }

    if (StringUtils.isNotBlank(shopId)) {

      params.put("shopId", shopId);
    }
    pageable = initPage(pageable, isAsc, orderName);
    model.addAttribute("page", pageable);

    Long total = memberService.countWechatMember(params);

    List<User> members = null;
    if (total.longValue() > 0) {
      members = memberService.getWechatMemberList(params, pageable);
    }

    Map<String, Object> data = new HashMap<String, Object>();

    data.put("total", total);
    data.put("rows", members);
    data.put("userId", userId);

    return new ResponseObject<Map<String, Object>>(data);
  }

  /**
   * 会员设置
   */
  @RequestMapping(value = "member/setting")
  public String memberSettings() {
    return "member/memberSetting";
  }

  /**
   * 查询数量
   */
  @ResponseBody
  @RequestMapping("/member/count")
  public ResponseObject<Map<String, Object>> countMembers() {
    long memberTotal, memberNew, dealMemberTotal, dealMemberNew;
    try {
      memberTotal = memberService.countMembers(null);
      memberNew = memberService.countMembersYesterday();
      dealMemberNew = memberService.countDealMembers(1);
      dealMemberTotal = memberService.countDealMembers(null);
    } catch (Exception e) {
      String msg = "查询会员数量错误";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    MemberCountsVO members = new MemberCountsVO(memberTotal, memberNew);
    MemberCountsVO dealMembers = new MemberCountsVO(dealMemberTotal, dealMemberNew);
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("members", members);
    result.put("dealMembers", dealMembers);
    return new ResponseObject<Map<String, Object>>(result);
  }

  @ResponseBody
  @RequestMapping("/member/chart")
  public ResponseObject<Map<String, Object>> chart() {
    Map<String, Object> result;
    try {
      result = memberService.lastWeekMemberChart();
    } catch (Exception e) {
      String msg = "加载新增用户失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<Map<String, Object>>(result);
  }

}
