package com.xquark.web.twitter;

import com.xquark.dal.model.FamilyCardSetting;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by liangfan on 16/7/7.
 */
public class TwitterCardListForm {

  private List<FamilyCardSetting> familyCardSettings;


  public List<FamilyCardSetting> getFamilyCardSettings() {
    return familyCardSettings;
  }

  public void setFamilyCardSettings(List<FamilyCardSetting> familyCardSettings) {
    this.familyCardSettings = familyCardSettings;
  }
}
