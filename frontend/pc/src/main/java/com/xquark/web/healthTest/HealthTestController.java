package com.xquark.web.healthTest;

import com.xquark.dal.model.HealthTestModule;
import com.xquark.dal.model.HealthTestPhysique;
import com.xquark.dal.model.HealthTestQuestion;
import com.xquark.dal.model.HealthTestResult;
import com.xquark.service.healthTest.HealthTestModuleService;
import com.xquark.service.healthTest.HealthTestPhysiqueService;
import com.xquark.service.healthTest.HealthTestQuestionService;
import com.xquark.service.healthTest.HealthTestResultService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by wangxinhua on 17-7-4. DESC: 健康自测Controller
 */
@Controller
@RequestMapping("/healthTest")
public class HealthTestController {

  private HealthTestQuestionService questionService;
  private HealthTestModuleService moduleService;
  private HealthTestPhysiqueService physiqueService;
  private HealthTestResultService resultService;

  @RequestMapping("/module")
  public String config() {
    return "healthTest/module";
  }

  @RequestMapping("/module/result")
  public String moduleResult() {
    return "healthTest/result_list";
  }

  @RequestMapping("/module/resultEdit")
  public String moduleResultEdit(
      @RequestParam("moduleId") String moduleId,
      @RequestParam(value = "resultId", required = false) String resultId,
      Model model) {
    String title;
    if (StringUtils.isBlank(resultId)) {
      title = "新增测试结果";
    } else {
      title = "修改测试结果";
      HealthTestResult result = resultService.loadResult(resultId);
      if (result != null) {
        model.addAttribute("result", result);
        String physiqueId = result.getPhysiqueId();
        if (StringUtils.isNotBlank(physiqueId) && StringUtils.isBlank(result.getDescription())) {
          HealthTestPhysique physique = physiqueService.load(physiqueId);
          if (physique != null) {
            result.setDescription(physique.getDescription());
          }
        }
      }
    }
    HealthTestModule module = moduleService.load(moduleId);
    if (module != null) {
      model.addAttribute("module", module);
    }
    model.addAttribute("title", title);
    return "healthTest/result_edit";
  }

  @RequestMapping("/question")
  public String question() {
    return "healthTest/question";
  }

  @RequestMapping("/question/edit")
  public String questionEdit(
      @RequestParam(value = "id", required = false) String id,
      @RequestParam("moduleId") String moduleId,
      Model model) {

    HealthTestModule module = moduleService.load(moduleId);
    String title;
    if (module != null) {
      model.addAttribute("moduleId", module.getId());
      model.addAttribute("moduleName", module.getName());
      List<HealthTestQuestion> questionVOS = questionService.list(moduleId);
      model.addAttribute("questions", questionVOS);
    }
    if (StringUtils.isNotBlank(id)) {
      title = "编辑问题";
      HealthTestQuestion question = questionService.load(id);
      if (question != null) {
        model.addAttribute("question", question);
      }
    } else {
      title = "新增问题";
    }
    model.addAttribute("title", title);
    return "healthTest/question_edit";
  }

  @RequestMapping("/physiqueEdit")
  public String physiqueEdit(String physiqueId, Model model) {
    if (StringUtils.isNotBlank(physiqueId)) {
      HealthTestPhysique physique = physiqueService.load(physiqueId);
      if (physique != null) {
        model.addAttribute("model", physique);
      }
    }
    List<HealthTestPhysique> physiques = physiqueService.list();
    if (physiques != null && physiques.size() > 0) {
      model.addAttribute("physiques", physiques);
    }
    return "healthTest/physique_edit";
  }

  @Autowired
  public void setQuestionService(HealthTestQuestionService questionService) {
    this.questionService = questionService;
  }

  @Autowired
  public void setModuleService(HealthTestModuleService moduleService) {
    this.moduleService = moduleService;
  }

  @Autowired
  public void setPhysiqueService(HealthTestPhysiqueService physiqueService) {
    this.physiqueService = physiqueService;
  }

  @Autowired
  public void setResultService(HealthTestResultService resultService) {
    this.resultService = resultService;
  }
}
