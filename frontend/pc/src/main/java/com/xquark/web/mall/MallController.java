package com.xquark.web.mall;

import com.google.common.base.Optional;
import com.xquark.dal.mapper.PostAgeSetMapper;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Module;
import com.xquark.dal.model.PostAgeSet;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.vo.PromotionFullCutVO;
import com.xquark.dal.vo.PromotionFullPiecesDiscountVO;
import com.xquark.dal.vo.PromotionFullPiecesVO;
import com.xquark.service.fullReduce.fullCut.PromotionFullCutService;
import com.xquark.service.fullReduce.fullPieces.PromotionFullPiecesService;
import com.xquark.service.merchant.MerchantService;
import com.xquark.service.module.ModuleService;
import com.xquark.service.product.ProductService;
import com.xquark.web.BaseController;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by dongsongjie on 16/6/6.
 */
@Controller
public class MallController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @Autowired
  private PromotionFullCutService promotionFullCutService;

  @Autowired
  private PromotionFullPiecesService promotionFullPiecesService;

  @Autowired
  private PostAgeSetMapper postAgeSetMapper;

  @Autowired
  private ProductService productService;

  private ModuleService moduleService;

  @Autowired
  public void setModuleService(ModuleService moduleService) {
    this.moduleService = moduleService;
  }


  @RequestMapping(value = "/mall/summary")
  public String mallSummary(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    return "mall/mallSummary";
  }

  @RequestMapping(value = "/mall/route")
  public String route(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    if (merchant.getAdmin()) {
      //如果是管理员，则直接跳转到商城首页
      return "redirect:/mall/summary";
    }
    String roles = merchant.getRoles();
    if (StringUtils.isNotBlank(roles)) {
      //其他情况下，则取该用户的第一个角色的所能访问的第一个菜单
      final String[] rs = roles.split(",");
      if (rs.length > 0) {
        String fr = rs[0];
        List<Module> modules = moduleService.listByRoleName(fr);
        if (CollectionUtils.isNotEmpty(modules)) {
          Module m = modules.get(0);
          String url = m.getUrl();
          if (StringUtils.isNotBlank(url)) {
            return "redirect:" + url;
          }
        }
      }
    }
    //没有角色的情况下，则退回到登录页面
    return "redirect:/pc/login.html";
  }

  @RequestMapping("/mall/comment")
  public String mallComments() {
    return "mall/mallComment";
  }

  @RequestMapping(value = "/mall/decoration")
  public String mallDecoration(Model model) {
    return "mall/mallDecoration";
  }

  @RequestMapping(value = "/mall/product")
  public String mallProduct(Model model) {
    model.addAttribute("name", ProductType.NORMAL.getName());
    model.addAttribute("type", ProductType.NORMAL.name());
    return "mall/mallProductList";
  }

  @RequestMapping(value = "/mall/filter")
  public String mallFilter(Model model) {
    fillMOdelByProductType(model, ProductType.FILTER);
    return "mall/mallProductList";
  }

  @RequestMapping(value = "/mall/combine")
  public String mallCombine(Model model) {
    fillMOdelByProductType(model, ProductType.COMBINE);
    return "mall/mallProductList";
  }

  private void fillMOdelByProductType(Model model, ProductType type) {
    model.addAttribute("name", type.getName());
    model.addAttribute("type", type.name());
  }

  @RequestMapping(value = "/mall/productTop")
  public String productTop(Model model) {
    return "products/productTop";
  }

  @RequestMapping(value = "/mall/memberPromotion")
  public String memberPromotion(Model model) {
    return "mall/memberPromotion";
  }

  @RequestMapping(value = "/mall/privilegeProduct")
  public String privilegeProduct(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    return "products/privilegeProduct";
  }

  @RequestMapping(value = "/mall/groupon")
  public String grouponProduct(Model model) {
    return "mall/grouponProductList";
  }

  @RequestMapping(value = "/mall/flashsale")
  public String flashsaleProduct(Model model) {
    return "mall/flashSaleProductList";
  }

  @RequestMapping(value = "/mall/commonsale")
  public String commonsaleProduct(Model model) {
    return "mall/commonSaleProductList";
  }

  @RequestMapping(value = "/mall/vipRights")
  public String vipRightProduct(Model model) {
    return "mall/vipRightProductList";
  }

  @RequestMapping(value = "/mall/preorder")
  public String preOrderProduct() {
    return "mall/preOrderProductList";
  }

  @RequestMapping(value = "/mall/bargain")
  public String bargainProduct() {
    return "mall/bargainProductList";
  }

  @RequestMapping(value = "/mall/fullCutEdit")
  public String fullCutEdit(String id, Model model) {
    if (StringUtils.isNotBlank(id)) {
      PromotionFullCutVO promotionFullCut = promotionFullCutService.load(id);
      if (promotionFullCut != null) {
        model.addAttribute("fullCut", promotionFullCut);
      }
    }
    return "mall/fullCutProductEditNew";
  }

  @RequestMapping(value = "/mall/fullPiecesEdit")
  public String fullPiecesEdit(String id, Model model) {
    if (StringUtils.isNotBlank(id)) {
      PromotionFullPiecesVO promotionFullPieces = promotionFullPiecesService.load(id);
      if (promotionFullPieces != null) {
        List<PromotionFullPiecesDiscountVO> discountVOS = promotionFullPieces.getDiscounts();
        if (CollectionUtils.isNotEmpty(discountVOS)) {
          for (PromotionFullPiecesDiscountVO discountVO : discountVOS) {
            String giftIds = discountVO.getGiftIds();
            if (StringUtils.isNotBlank(giftIds)) {
              String productNames = productService
                  .selectConcatName(StringUtils.split(giftIds, ","), true);
              discountVO.setProductNames(productNames);
            }
          }
        }
        model.addAttribute("fullPieces", promotionFullPieces);
      }
    }
    return "mall/fullPiecesProductEdit";
  }

  @RequestMapping(value = "/mall/fullCutList")
  public String fullCutList() {
    return "mall/fullCutProductList";
  }

  @RequestMapping(value = "/mall/fullPiecesList")
  public String fullPiecesList() {
    return "mall/fullPiecesProductList";
  }

  @RequestMapping(value = "/mall/diamond")
  public String diamondProduct(Model model) {
    return "mall/diamondProductList";
  }

  @RequestMapping(value = "/mall/product/edit")
  public String editProduct(Model model, @RequestParam(required = false) String pId,
      ProductType type) {

    User user = getCurrentUser();

    if (StringUtils.isNotBlank(pId)) {
      model.addAttribute("productId", pId);
      List<Sku> skus = productService.listSkus(pId);
      if (CollectionUtils.isNotEmpty(skus)) {
        model.addAttribute("skuId", skus.get(0).getId());
      }
    }

    type = Optional.fromNullable(type).or(ProductType.NORMAL);

    fillMOdelByProductType(model, type);
    model.addAttribute("shopId", user.getShopId());
    model.addAttribute("userId", user.getId());
    // 运费模版
    List<PostAgeSet> postAgeSets = postAgeSetMapper.selectByShopId(user.getShopId());
    model.addAttribute("postAgeSets", postAgeSets);
    return "mall/mallProductEdit";
  }

  @RequestMapping(value = "/mall/order")
  public String mallOrder(Model model) {
    return "mall/mallOrder";
  }

  @RequestMapping(value = "/mall/promotion")
  public String mallPromotion(Model model) {
    return "mall/mallPromotion";
  }

  @RequestMapping(value = "/mall/logistics")
  public String mallLogistics(Model model, @RequestParam(required = false) String pId) {
    return "mall/mallLogistics";
  }

  @RequestMapping(value = "/mall/logisticsList")
  public String logisticsList(Model model) {
    return "mall/mallLogisticsList";
  }

  @RequestMapping(value = "/mall/application")
  public String mallApplication(Model model) {
    return "mall/mallApplication";
  }

  @RequestMapping(value = "/mall/settings")
  public String mallSettings(Model model) {
    return "mall/mallSettings";
  }

  @RequestMapping(value = "/mall/roletest")
  public String roletest(Model model) {
    return "mall/roletest";
  }

  @RequestMapping(value = "/mall/carousel")
  public String carousel(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    return "mall/carouselList";
  }

  @RequestMapping(value = "/mall/coupon")
  public String coupon(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    return "mall/mallCoupon";
  }

  @RequestMapping(value = "/mall/productCollection")
  public String productCollection(Model model) {
    return "mall/productCollection";
  }

  @RequestMapping(value = "/mall/productTag")
  public String productTag() {
    return "mall/productTag";
  }

  @RequestMapping(value = "/mall/message")
  public String message() {
    return "message/send";
  }


  @RequestMapping(value = "/mall/brand")
  public String brand() {
    return "brand/brand";
  }

  @RequestMapping(value = "/mall/package")
  public String mallPackage() {
    return "package/package";
  }


  @RequestMapping(value = "/mall/order/refund/warehouse")
  public String mallOrderRefundWarehouse() {
    return "mall/refund";
  }

  public enum PAGE_ENUM {
    MODULE("moduleName"),
    DISPLAY("displayName"),
    PAGE("page");

    private String value;

    PAGE_ENUM(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }
  }


  @RequestMapping(value = "/mall/warehouse")
  public String mallWareHouse(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "warehouse");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "仓库");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "product");
    return "warehouse/warehouse";
  }

  @RequestMapping(value = "/mall/supplier")
  public String mallSupplier(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "supplier");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "供应商");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "product");
    model.addAttribute("showSearch", true);
    return "fragments/common";
  }

  @RequestMapping(value = "/mall/merchant/supplier")
  public String mallMerchantSupplier(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "merchantSupplier");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "供应商账户关联");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "merchant");
    model.addAttribute("showSearch", false);
    return "fragments/common";
  }

  @RequestMapping(value = "/mall/sync")
  public String dataSync(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "sync");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "合作伙伴");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "mall");
    return "mall/sync";
  }

  @RequestMapping(value = "mall/exportByOrder")
  public String exportByOrder(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "exportByOrder");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "按订单导出");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "mall");
    return "mall/exportByOrder";
  }

  @RequestMapping(value = "mall/exportByCustomer")
  public String exportByCustomer(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "exportByCustomer");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "按客户导出");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "mall");
    return "mall/exportByCustomer";
  }

  @RequestMapping(value = "mall/exportWithdraw")
  public String exportWithDraw() {
    return "mall/exportWithdraw";
  }

  @RequestMapping(value = "mall/exportWithdrawDetail")
  public String exportWithDrawDeatil() {
    return "mall/exportWithdrawDetail";
  }

  @RequestMapping(value = "/mall/exportLaborExpenses")
  public String exportLaborExpenses() { return "mall/exportLaborExpenses"; }

  @RequestMapping(value = "mall/pointImport")
  public String pointImport(Model model) {
    return "mall/pointImport";
  }

  @RequestMapping(value = "mall/withdrawImport")
  public String withdrawImport() {
    return "mall/withdrawImport";
  }

  @RequestMapping(value = "/mall/messageEdit")
  public String messageEdit() {
    return "message/edit";
  }

  @RequestMapping(value = "/mall/promotionAudit")
  public String promotionAudit() {
    return "mall/promotionAudit";
  }

  @RequestMapping("/mall/hotSearch")
  public String hot(Model model) {
    model.addAttribute(PAGE_ENUM.MODULE.getValue(), "hotSearch");
    model.addAttribute(PAGE_ENUM.DISPLAY.getValue(), "词条");
    model.addAttribute(PAGE_ENUM.PAGE.getValue(), "mall");
    model.addAttribute("showSearch", true);
    return "mall/hotSearch";
  }

  @RequestMapping("/mall/addHot")
  public String hotAdd(String id, Integer type, Model model) {
    model.addAttribute("hotId", id);
    model.addAttribute("type", type);
    return "mall/addHot";
  }
}
