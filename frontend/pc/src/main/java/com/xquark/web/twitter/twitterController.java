package com.xquark.web.twitter;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Commission;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.OrderMessage;
import com.xquark.dal.model.OrderRefund;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.TeamShopCommission;
import com.xquark.dal.model.TwitterLevel;
import com.xquark.dal.model.TwitterShopCommission;
import com.xquark.dal.model.User;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.TwitterLevelType;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.vo.CouponInfoVO;
import com.xquark.dal.vo.OrderFeeVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.voex.OrderVOEx;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.team.TeamShopComService;
import com.xquark.service.twitter.TwitterLevelService;
import com.xquark.service.twitter.impl.TwitterShopComServiceImpl;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

/**
 * Created by liangfan on 16/5/5.
 */
@Controller
public class twitterController extends BaseController {

  @Autowired
  private OrderService orderService;
  @Autowired
  private ResourceFacade resourceFacade;
  @Autowired
  private ZoneService zoneService;
  @Autowired
  private CashierService cashierService;
  @Autowired
  private ShopService shopService;
  @Autowired
  private OrderRefundService orderRefundService;
  @Autowired
  private UnionService unionService;
  @Autowired
  private ExcelService excelService;

  @Autowired
  private FamilyCardSettingService familyCardSettingService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private UserService userService;

  @Autowired
  private TwitterShopComServiceImpl twitterShopComServiceImpl;

  @Autowired
  private TwitterLevelService twitterLevelService;

  @Autowired
  private TeamShopComService teamShopComService;

  private final String ORDERNOTES = "您的交易金额大于%s，需要支付%s手续费";
  @Value("${order.delaysign.date}")
  private int defDelayDate;
  @Value("${tech.serviceFee.standard}")
  private String serviceFeethreshold;

  @RequestMapping(value = "/twitter/summary")
  public String twitterSummary(Model model) {
    return "/twitter/twitterSummary";
  }

  @RequestMapping(value = "/twitter/record")
  public String twitterRecord(Model model) {
    return "/twitter/twitterRecord";
  }

  @RequestMapping(value = "/twitter/apply")
  public String twitterApply(Model model) {
    return "/twitter/twitterApply";
  }

  @RequestMapping(value = "/twitter/order")
  public String twitterOrder(Model model, OrderStatus status, Integer size,
      Integer page, Boolean isDesc, String orderName, String key, String mobile) {

    // 初始化分页信息 page
    Pageable pageable = initPage(page, size, orderName, isDesc);
    model.addAttribute("page", pageable);

    // 模糊查询匹配
    List<OrderVO> orders = null;
    Long total = 0L;
    if (!StringUtils.isBlank(key)) {
      key = key.trim();
      // 买家姓名 微信号 电话
      orders = orderService.listByStatus4SellerWithLike(status, pageable,
          key, null, "");
      total = orderService.countSellerOrdersByStatusWithLike(status, key);

    } else {
      orders = orderService.listByStatus4Seller(status, pageable);
      total = orderService.countSellerOrdersByStatus(status);
    }
    model.addAttribute("total", total);

    // 回填图片和地区
    setImgAndZones(orders.toArray(new OrderVO[orders.size()]));

    // 查询的状态是已付款的，则获取成功的订单总数目
    if (OrderStatus.PAID.equals(status) && orders.size() > 0) {
      OrderVO vo = orders.get(0);
      vo.setSeq(orderService.selectOrderSeqByShopId(vo.getShopId()));
    }

    model.addAttribute("orders", orders);

    model.addAttribute("status", status);
    //model.addAttribute("orderNo", orderNo);
    return "/twitter/twitterOrder";
  }


  // 订单详情
  @RequestMapping(value = "/twitter/order/details")
  public String ordersDetails(@RequestParam String orderId, Model model,
      HttpServletRequest req) {
    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("order.not.found"));
    }
    setLogisticsInfo(order);

    Shop shop = shopService.load(order.getShopId());
    if (shop != null) {
      order.setShopName(shop.getName());
    }
    // 设置图片和地区
    setImgAndZones(order);
    // 分佣
    setCommissionInfo(order);

    List<CouponInfoVO> orderCoupons = cashierService
        .loadCouponInfoByOrderNo(order.getOrderNo());
    order.setOrderCoupons(orderCoupons);

    List<OrderFeeVO> list = orderService.findOrderFees(order);
    OrderVOEx orderEx = new OrderVOEx(order, list);

    BigDecimal discountFee = orderEx.getDiscountFee();
    if (discountFee == null) {
      discountFee = BigDecimal.ZERO;
    }

    orderEx.setDiscountFee(discountFee.add(order.getHongbaoAmount()));
    orderEx.setDefDelayDate(defDelayDate);
    orderEx.setRefundableFee(orderEx.getTotalFee().subtract(
        orderEx.getLogisticsFee()));
    orderEx.setShowRefundBtn(false);

    // 快店IOS审核失败，暂时关闭买家端退款入口
    if (orderEx.getStatus() != OrderStatus.SUBMITTED
        && orderEx.getStatus() != OrderStatus.SUCCESS
        && orderEx.getStatus() != OrderStatus.CANCELLED) {
      if (orderEx.getStatus() == OrderStatus.CLOSED) {
        List<OrderRefund> refunds = orderRefundService
            .listByOrderId(order.getId());
        if (refunds.size() > 0) {
          orderEx.setShowRefundBtn(true);
        }
      } else {
        orderEx.setShowRefundBtn(true);
      }
    }

    model.addAttribute("order", orderEx);
    List<OrderMessage> messages = orderService.viewMessages(orderEx.getId());
    model.addAttribute("messages", messages);
    return "/twitter/twitterOrderDetail";
  }

  @RequestMapping(value = "twitter/member/addLevel")
  public String twitterAddLevel(Model model) {
    return "/twitter/member/addLevel";
  }

  @RequestMapping(value = "/twitter/member")
  public String twitterMember(Model model) {
    String shopId = getCurrentUser().getShopId();
    // 获取所有自定义类型的推客等级，供页面选择
    List<TwitterLevel> levels = twitterLevelService
        .getByType(shopId, TwitterLevelType.CUSTOM.toString());
    model.addAttribute("levels", levels);
    // 获取所有战队分组，供页面选择
    List<TeamShopCommission> groups = teamShopComService.selectAllByshopId(shopId);
    model.addAttribute("groups", groups);
    return "/twitter/twitterMember";
  }

  @RequestMapping(value = "/twitter/withdraw")
  public String twitterWithdraw(Model model) {
    return "/twitter/twitterWithdraw";
  }

  @ResponseBody
  @RequestMapping("/twitter/exportTwitterChildrenExcel")
  public void twitterChildren(@RequestParam String userId, HttpServletResponse resp)
      throws IOException {
    //TODO liangfan
    Map<String, Object> params = new HashedMap();
    String shopId = shopService.findByUser(userId).getId();
    String userName = userService.load(userId).getName();
    String shopName = shopService.findByUser(userId).getName();
    String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
    List<ShopTree> result = shopTreeService.listDirectChildren(rootShopId, shopId);
    List<User> userList = new ArrayList<User>();
    for (ShopTree shopTree : result) {
      String userShopId = shopTree.getDescendantShopId();
      User user = userService.load(shopService.load(userShopId).getOwnerId());
      userList.add(user);
    }

    String sheetStr = "推客下级列表";
    String filePrefix = "下级推客";
    //TODO
    String[] secondTitle = new String[]{
        "推客名字",
        "推客手机号"};

    String[] strBody = new String[]{
        "getName",
        "getPhone"};
    //TODO
    excelService.export(filePrefix,
        userList, User.class,
        sheetStr,
        "推客:" + userName + "的店铺: " + shopName + " 所有下级推客",
        secondTitle, strBody, resp, true);
  }

  @RequestMapping(value = "/twitter/setting")
  public String twitterSetting(Model model) {

    //获得shopId
    String shopId = getCurrentUser().getShopId();

    //加载配置信息
    TwitterShopCommission twitterShopCommission = twitterShopComServiceImpl
        .selectDefaultByshopId(shopId);
    model.addAttribute("setting", twitterShopCommission);

    return "/twitter/twitterSetting";
  }

  private void setImgAndZones(OrderVO... orders) {
    for (OrderVO order : orders) {
      // 订单图片
      String imgUrl = "";
      for (OrderItem item : order.getOrderItems()) {
        imgUrl = item.getProductImg();
        if (StringUtils.isBlank(imgUrl)) {
          continue;
        }

        imgUrl = resourceFacade.resolveUrl(imgUrl);
        // 解析图片
        item.setProductImgUrl(imgUrl);
      }
      order.setImgUrl(imgUrl);

      // 收货地址
      List<Zone> zoneList = zoneService.listParents(order
          .getOrderAddress().getZoneId());
      String addressDetails = "";
      for (Zone zone : zoneList) {
        addressDetails += zone.getName();
      }
      addressDetails += order.getOrderAddress().getStreet();
      order.setAddressDetails(addressDetails);
    }
  }

  /**
   * 回填物流信息
   */
  private void setLogisticsInfo(OrderVO order) {
    // 设置物流公司官网
    for (LogisticsCompany logisticsCompany : LogisticsCompany.values()) {
      if (logisticsCompany.getName().equals(order.getLogisticsCompany())) {
        order.setLogisticsOfficial(logisticsCompany.getUrl());
      }
    }

    // TODO 兼容老版本处理
    if ("顺丰".equals(order.getLogisticsCompany())
        || "顺丰快递".equals(order.getLogisticsCompany())
        || "SF_EXPRESS".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sf-express.com/");
    } else if ("圆通".equals(order.getLogisticsCompany())
        || "圆通快递".equals(order.getLogisticsCompany())
        || "YTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yto.net.cn/");
    } else if ("申通".equals(order.getLogisticsCompany())
        || "STO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.sto.cn/");
    } else if ("中通".equals(order.getLogisticsCompany())
        || "ZTO".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.zto.cn/");
    } else if ("百世汇通".equals(order.getLogisticsCompany())
        || "BESTEX".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.htky365.com/");
    } else if ("韵达".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.yundaex.com/");
    } else if ("天天".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ttkdex.com/");
    } else if ("全峰".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.qfkd.com.cn/");
    } else if ("邮政EMS".equals(order.getLogisticsCompany())
        || "中国邮政".equals(order.getLogisticsCompany())) {
      order.setLogisticsOfficial("http://www.ems.com.cn/");
    } else {
      order.setLogisticsOfficial("");
    }
  }

  // 回填分佣金信息
  private void setCommissionInfo(OrderVO order) {
    List<Commission> commissions = unionService
        .listByOrderId(order.getId());
    BigDecimal cmFee = BigDecimal.ZERO;
    for (Commission cm : commissions) {
      cmFee = cmFee.add(cm.getFee());
    }
    order.setCommissionFee(cmFee);
    if (order.getDiscountFee() == null) {
      order.setDiscountFee(BigDecimal.ZERO);
    }
    BigDecimal handingFee = orderService.loadTechServiceFee(order);
    if (!handingFee.equals(BigDecimal.ZERO)) {
      Formatter fmt = new Formatter();
      try {
        fmt.format(ORDERNOTES, serviceFeethreshold,
            handingFee.setScale(2, BigDecimal.ROUND_HALF_UP));
        order.setNotes(fmt.toString());
      } finally {
        fmt.close();
      }
    }

    order.setHongbaoAmount(handingFee);
  }

  @ResponseBody
  @RequestMapping("/twitter/familyCard/save")
  public ResponseObject<Boolean> saveFamilyCard(
      @ModelAttribute("twitterSettingForm") TwitterCardListForm form) {
    //WechatAppConfig appConfig = new WechatAppConfig();
    for (FamilyCardSetting familyCardSetting : form.getFamilyCardSettings()) {
      familyCardSettingService.update(familyCardSetting);
    }

    return new ResponseObject<Boolean>(true);
  }

}
