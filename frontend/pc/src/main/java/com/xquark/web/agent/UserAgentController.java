package com.xquark.web.agent;

import com.xquark.dal.model.Merchant;
import com.xquark.service.merchant.MerchantService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 16/12/09.
 */
@Controller
public class UserAgentController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @RequestMapping(value = "/agent/summary")
  public String agentSummary(Model model) {
    return "/agent/agentSummary";
  }

  @RequestMapping(value = "/agent/apply")
  public String agentApply(Model model) {
    return "/agent/agentApply";
  }

  @RequestMapping(value = "/agent/auditRule")
  public String auditRule(Model model) {
    return "/auditRule/auditRuleAgentList";
  }

  @RequestMapping(value = "/agent/member")
  public String agentMember(Model model) {
    Merchant merchant = merchantService.getCurrentMerchant();
    model.addAttribute("roles", merchant.getRoles());
    return "/agent/agentMember";
  }

  @RequestMapping(value = "/agent/tree")
  public String agentTree(Model model) {
    return "/agent/agentTree";
  }

  @RequestMapping(value = "/agent/commission")
  public String agentCommission(Model model) {
    return "/agent/agentCommission";
  }

  @RequestMapping(value = "/agent/generalQrcode")
  public String agentGeneralQrcode(Model model) {
    return "/agent/generalQrcode";
  }

  @RequestMapping(value = "/agent/generalDirectorQrcode")
  public String generalDirectorQrcode(Model model) {
    return "/agent/generalDirectorQrcode";
  }

  @RequestMapping(value = "/agent/generalSecondQrcode")
  public String generalSecondQrcode(Model model) {
    return "/agent/generalSecondQrcode";
  }

  @RequestMapping(value = "/agent/generalFirstQrcode")
  public String generalFirstQrcode(Model model) {
    return "/agent/generalFirstQrcode";
  }

  @RequestMapping(value = "/agent/generalFounderQrcode")
  public String generalFounderQrcode(Model model) {
    return "/agent/generalFounderQrcode";
  }


  @RequestMapping(value = "/agent/generalSpecialQrcode")
  public String generalSpecialQrcode(Model model) {
    return "/agent/generalSpecialQrcode";
  }

  @RequestMapping(value = "/agent/whiteList")
  public String agentWhiteList(Model model) {
    return "/agent/whiteList";
  }

  @RequestMapping(value = "/agent/founder")
  public String agentFounder(Model model) {
    return "/agent/agentFounder";
  }

}
