package com.xquark.web.templeateController;

import com.xquark.dal.model.User;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.spring4.SpringTemplateEngine;

/**
 * User: huangjie
 * Date: 2018/5/23.
 * Time: 下午7:35
 * 获取渲染之后的raw html
 */
@Controller
public class TemplateController extends BaseController {

    private SpringTemplateEngine springTemplateEngine;

    @Autowired
    public void setSpringTemplateEngine(SpringTemplateEngine springTemplateEngine) {
        this.springTemplateEngine = springTemplateEngine;
    }
    @RequestMapping(value = "/template/top")
    public String main(Model model) {
        User currentUser = getCurrentUser();
        model.addAttribute("user",currentUser);
        return  "layout/newMainPage";
    }

    @RequestMapping(value = "/template/main")
    public String mainTemplate(Model model) {
        User currentUser = getCurrentUser();
        model.addAttribute("user",currentUser);
        return "layout/mainTemplate";
    }

}
