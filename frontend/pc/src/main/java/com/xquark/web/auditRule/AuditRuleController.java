package com.xquark.web.auditRule;

import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/1/20.
 */
@Controller
public class AuditRuleController extends BaseController {

  @RequestMapping(value = "/auditRule/list")
  public String list(Model model) {
    return "auditRule/auditRuleList";
  }

}
