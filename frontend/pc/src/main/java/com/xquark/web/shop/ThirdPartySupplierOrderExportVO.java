package com.xquark.web.shop;

import com.xquark.dal.model.SystemRegion;
import com.xquark.dal.model.Zone;
import com.xquark.dal.vo.OrderVO;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class ThirdPartySupplierOrderExportVO extends OrderVO{

  private static final int PROVINCE_INDEX = 0;
  private static final int CITY_INDEX = 1;
  private static final int AREA_INDEX = 2;

  private List<Zone> zoneList;

  private List<SystemRegion> systemRegionList;

  private String street;

  private final String consignee;

  private String skuStr;

  private String barCode;


//  public ThirdPartySupplierOrderExportVO(OrderVO orderVO, List<Zone> zoneList, String street,
//      String consignee) {
//    BeanUtils.copyProperties(orderVO, this);
//    this.zoneList = zoneList;
//    this.street = street;
//    this.consignee = consignee;
//  }

  public ThirdPartySupplierOrderExportVO(OrderVO orderVO, List<SystemRegion> systemRegionList,
      String street,
      String consignee) {
    BeanUtils.copyProperties(orderVO, this);
    this.systemRegionList = systemRegionList;
    this.street = street;
    this.consignee = consignee;
  }


  /**
   * 返回商城名称
   */
  public String getShopName() {
    return OrderExportSpec.SHOP_NAME;
  }
  /**
   * 街道
   */
  public String getStreet() {
    return street;
  }

  /**
   * 省
   */
  public String getProvince() {
    return getZoneNameByIndex(PROVINCE_INDEX);
  }

  /**
   * 市
   */
  public String getCity() {
    return getZoneNameByIndex(CITY_INDEX);
  }

  /**
   * 区
   */
  public String getArea() {
    return getZoneNameByIndex(AREA_INDEX);
  }

  /**
   * 邮政编码
   */
//  public String getZipCode() {
//    Zone zone = getZoneByIndex(AREA_INDEX);
//    if (zone == null) {
//      return "";
//    }
//    return zone.getZipCode();
//  }

  /**
   * 收货人
   */
  public String getConsignee() {
    return consignee;
  }

  /**
   * 根据地区索引值获取地区对象
   */
//  private Zone getZoneByIndex(int index) {
//    if (CollectionUtils.isEmpty(zoneList)
//        || index < 0
//        || index >= zoneList.size()) {
//      return null;
//    }
//    return zoneList.get(index);
//  }
//
//  /**
//   * 获取地区索引值, 如果传入意料之外的值则返回空
//   */
//  private String getZoneNameByIndex(int index) {
//    Zone zone = getZoneByIndex(index);
//    if (zone == null) {
//      return "";
//    }
//    return zone.getName();
//  }
  public Integer getZipCode() {
    SystemRegion zone = getZoneByIndex(AREA_INDEX);
    if (zone == null) {
      return null;
    }
    return zone.getAdCode();
  }

  /**
   * 根据地区索引值获取地区对象
   */
  private SystemRegion getZoneByIndex(int index) {
    if (CollectionUtils.isEmpty(systemRegionList)
        || index < 0
        || index >= systemRegionList.size()) {
      return null;
    }
    return systemRegionList.get(index);
  }

  /**
   * 获取地区索引值, 如果传入意料之外的值则返回空
   */
  private String getZoneNameByIndex(int index) {
    SystemRegion zone = getZoneByIndex(index);
    if (zone == null) {
      return "";
    }
    return zone.getName();
  }

  public String getSkuStr() {
    return skuStr;
  }

  public void setSkuStr(String skuStr) {
    this.skuStr = skuStr;
  }
}
