package com.xquark.web.shop;

import com.xquark.dal.status.ActivityGrouponStatus;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PayStatus;
import com.xquark.dal.type.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单导出查询sql
 */
public class OrderSearchForm {

  private String orderNo;

  private PaymentMode paymentMode;

  private OrderStatus status;

  private String completeStartDate;

  private String completeEndDate;

  private OrderSortType orderType;

  private String buyerName;

  private String sellerName;

  private AgentType agentType;

  private String startDate;

  private String endDate;

  private OrderStatus orderStatus;

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public AgentType getAgentType() {
    return agentType;
  }

  public void setAgentType(AgentType agentType) {
    this.agentType = agentType;
  }

  private ActivityGrouponStatus grouponStatus;

  public ActivityGrouponStatus getGrouponStatus() {
    return grouponStatus;
  }

  public void setGrouponStatus(ActivityGrouponStatus grouponStatus) {
    this.grouponStatus = grouponStatus;
  }

  public OrderSortType getOrderType() {
    return orderType;
  }

  public void setOrderType(OrderSortType orderType) {
    this.orderType = orderType;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public PaymentMode getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(PaymentMode paymentMode) {
    this.paymentMode = paymentMode;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public String getCompleteStartDate() {
    return completeStartDate;
  }

  public void setCompleteStartDate(String completeStartDate) {
    this.completeStartDate = completeStartDate;
  }

  public String getCompleteEndDate() {
    return completeEndDate;
  }

  public void setCompleteEndDate(String completeEndDate) {
    this.completeEndDate = completeEndDate;
  }
}
