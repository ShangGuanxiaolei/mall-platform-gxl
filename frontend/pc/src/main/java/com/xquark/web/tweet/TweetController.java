package com.xquark.web.tweet;

import com.xquark.web.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 16/10/19.
 */
@Controller
public class TweetController extends BaseController {

  @RequestMapping(value = "/tweet/list")
  public String list(Model model) {
    return "tweet/tweetList";
  }

  @RequestMapping(value = "/tweet/edit")
  public String edit(Model model, @RequestParam(required = false) String pId) {
    if (StringUtils.isNotBlank(pId)) {
      model.addAttribute("tweettId", pId);
    }
    return "tweet/tweetEdit";
  }

}
