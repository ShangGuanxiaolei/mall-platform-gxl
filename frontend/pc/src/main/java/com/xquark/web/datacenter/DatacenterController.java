package com.xquark.web.datacenter;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.partner.PartnerOrderSearchForm;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

@Controller
public class DatacenterController extends BaseController {

  @RequestMapping(value = "/datacenter/trade")
  public String trade(Model model) {
    return "/datacenter/trade";
  }

  @RequestMapping(value = "/datacenter/flow")
  public String flow(Model model) {
    return "/datacenter/flow";
  }

  @RequestMapping(value = "/datacenter/guest")
  public String guest(Model model) {
    return "/datacenter/guest";
  }

}
