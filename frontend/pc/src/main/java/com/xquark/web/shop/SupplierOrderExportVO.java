package com.xquark.web.shop;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.Zone;
import com.xquark.dal.vo.OrderVO;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class SupplierOrderExportVO extends OrderVO {

  private static final int COUNTRY_INDEX = 0;
  private static final int PROVINCE_INDEX = 1;
  private static final int CITY_INDEX = 2;
  private static final int AREA_INDEX = 3;

  private List<Zone> zoneList;

  private String street;

  private final String productCode;

  private final String consignee;

  public SupplierOrderExportVO(List<Zone> zoneList, String street, String productCode,
      String consignee) {
    this.zoneList = zoneList;
    this.street = street;
    this.productCode = productCode;
    this.consignee = consignee;
  }

  public SupplierOrderExportVO(Order order,
      List<Zone> zoneList, String street, String productCode, String consignee) {
    super(order);
    this.zoneList = zoneList;
    this.street = street;
    this.productCode = productCode;
    this.consignee = consignee;
  }

  /**
   * 返回商城名称
   */
  public String getShopName() {
    return OrderExportSpec.SHOP_NAME;
  }

  /**
   * 是否开发票
   */
  public String getNeedInvoiceStr() {
    if (getNeedInvoice()) {
      return "是";
    }
    return "否";
  }

  /**
   * 开票抬头, 保留
   */
  public String getInvoiceSpec() {
    return "";
  }

  /**
   * 街道
   */
  public String getStreet() {
    return street;
  }

  /**
   * 省
   */
  public String getProvince() {
    return getZoneNameByIndex(PROVINCE_INDEX);
  }

  /**
   * 市
   */
  public String getCity() {
    return getZoneNameByIndex(CITY_INDEX);
  }

  /**
   * 区
   */
  public String getArea() {
    return getZoneNameByIndex(AREA_INDEX);
  }

  /**
   * 邮政编码
   */
  public String getZipCode() {
    Zone zone = getZoneByIndex(AREA_INDEX);
    if (zone == null) {
      return "";
    }
    return zone.getZipCode();
  }

  /**
   * 送货方式, 待定
   */
  public String getDevlivery() {
    return "";
  }

  public String getOrderDeliveryType() {
    // 2表示一件代发
    return "2";
  }

  public String getProductCode() {
    return this.productCode;
  }

  /**
   * 收货人
   */
  public String getConsignee() {
    return consignee;
  }

  /**
   * 根据地区索引值获取地区对象
   */
  private Zone getZoneByIndex(int index) {
    if (CollectionUtils.isEmpty(zoneList)
        || index < 0
        || index >= zoneList.size()) {
      return null;
    }
    return zoneList.get(index);
  }

  /**
   * 获取地区索引值, 如果传入意料之外的值则返回空
   */
  private String getZoneNameByIndex(int index) {
    Zone zone = getZoneByIndex(index);
    if (zone == null) {
      return "";
    }
    return zone.getName();
  }
}
