package com.xquark.web.merchant;

import com.xquark.dal.IUser;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Org;
import com.xquark.dal.vo.MerchantOrgVO;
import com.xquark.dal.vo.MerchantRoleVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.merchant.MerchantRoleService;
import com.xquark.service.merchant.MerchantService;
import com.xquark.service.org.OrgService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.vo.Json;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by quguangming on 16/5/4.
 */
@Controller
public class MerchantController extends BaseController {

  @Autowired
  private MerchantService merchantService;

  @Autowired
  private MerchantRoleService merchantRoleService;

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Autowired
  private OrgService orgService;

  @RequestMapping(value = "merchant/info")
  public String merchantInfo(Model model) {

    return "merchant/merchantInfo";
  }

  @RequestMapping(value = "merchant/admin")
  public String merchantAdmin(Model model) {

    return "merchant/merchantAdmin";
  }

  @RequestMapping(value = "merchant/admin/add")
  public String merchantAdminAdd(Model model) {
    model.addAttribute("title", "新增管理员");
    return "merchant/merchantEdit";

  }

  @ResponseBody
  @RequestMapping(value = "merchant/admin/delete")
  public Json delete(Model model, String merchantId) {

    IUser user = merchantService.getCurrentMerchant();
    String userId = user.getId();

    Json json = new Json();
    if (merchantId != null) {

      Merchant merchant = merchantService.load(merchantId);
      //验证操作用户是否具有该merchant的操作权限
      if (merchant != null && (StringUtils.equals(userId, merchant.getAdminId()) || StringUtils
          .equals(userId, merchant.getCreaterId()))) {

        int result = merchantService.delete(merchantId);
        if (result > 0) {
          json.setRc(Json.RC_SUCCESS);
          json.setMsg("删除成功!");
        }
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("删除失败,您无该操作权限,请与管理员联系!");
      }

    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("删除失败!");
    }
    return json;
  }

  @RequestMapping(value = "merchant/admin/edit")
  public String load(Model model, String merchantId) {

    model.addAttribute("title", "修改管理员");

    Merchant merchant = null;
    if (merchantId != null) {

      merchant = merchantService.load(merchantId);

    }
    model.addAttribute("merchant", merchant);
    return "merchant/merchantEdit";
  }


  @ResponseBody
  @RequestMapping(value = "merchant/admin/list")
  public ResponseObject<Map<String, Object>> merchantList(Model model, Pageable pageable,
      Integer orderIndex, Boolean isAsc, String keyword) {

    IUser user = merchantService.getCurrentMerchant();
    String userId = user.getId();

    //初始化排序列数据
    String[] columnsName = {"loginname", "name", "phone", "wechat", "created_at", "roles"};
    String orderName = columnsName[0];
    if (orderIndex != null) {
      orderName = columnsName[orderIndex.intValue()];
    }

    pageable = initPage(pageable, isAsc, orderName);
    model.addAttribute("page", pageable);

    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(keyword)) {
      params.put("keyword", "%" + keyword + "%");
      model.addAttribute("keyword", keyword);
    }

    if (StringUtils.isNotBlank(userId)) {

      params.put("merchantId", userId);
    }

    Long total = merchantService.countMarchants(params);

    List<Merchant> merchants = null;
    if (total.longValue() > 0) {
      merchants = merchantService.list(params, pageable);
    }

    for (Merchant merchant : merchants) {
      String roles = merchant.getRoles();
      if (StringUtils.isNotEmpty(roles)) {
        ArrayList list = new ArrayList();
        for (String role : roles.split(",")) {
          list.add(role);
        }
        String rolesDesc = merchantRoleService.getRoleDesc(list);
        merchant.setRolesDesc(rolesDesc);
      }
    }

    Map<String, Object> data = new HashMap<String, Object>();

    data.put("total", total);
    data.put("rows", merchants);
    data.put("userId", userId);

    return new ResponseObject<Map<String, Object>>(data);
  }

  @ResponseBody
  @RequestMapping(value = "merchant/admin/save")
  public Boolean save(Merchant merchant) {

    if (merchant == null) {
      return false;
    }

    long ret = 0;

    Merchant user = merchantService.getCurrentMerchant();

    if (merchant.getId() == null) {  //创建操作
      merchant.setCreaterId(user.getId());
      if (user.getAdmin()) { //当前操作者是marchant 管理员
        merchant.setAdminId(user.getId());
      } else {
        merchant.setAdminId(user.getAdminId());
      }
      merchant.setShopId(user.getShopId());
      merchant.setAdmin(false);
      merchant.setRoles("BASIC");

      ret = merchantService.create(merchant);  //新增操作
    } else {
      Merchant editer = merchantService.load(merchant.getId());

      if (editer != null && (StringUtils.equals(user.getId(), editer.getAdminId()) ||
          StringUtils.equals(user.getId(), editer.getCreaterId())) ||
          StringUtils.equals(user.getId(), editer.getId())) {
        editer.setName(merchant.getName());
        editer.setPhone(merchant.getPhone());
        editer.setEmail(merchant.getEmail());
        editer.setRoles(merchant.getRoles());
        editer.setOrgId(merchant.getOrgId());
        if (StringUtils.isNotBlank(merchant.getPassword())) {
          editer.setPassword(pwdEncoder.encode(merchant.getPassword()));
        }
        ret = merchantService.update(editer);
      }
    }

    if (ret > 0) {
      return true;
    }
    return false;
  }


  /**
   * 检查手机号是否已注册
   */
  @ResponseBody
  @RequestMapping(value = "merchant/checkPhone")
  public Boolean registered(@RequestParam String phone, Model model) {

    if (phone == null || StringUtils.isBlank(phone)) {
      return false;
    }

    return merchantService.checkPhone(phone);
  }

  @RequestMapping(value = "merchant/role")
  public String merchantRole(Model model) {

    return "merchant/merchantRole";
  }

  /**
   * 择管理员角色查询，不需要分页,需要将此管理员已经有的角色设置isselect为true
   */
  @ResponseBody
  @RequestMapping("merchantRole/allList")
  public ResponseObject<Map<String, Object>> allList(Pageable pageable,
      @RequestParam String keyword, @RequestParam(required = false) String merchantId) {
    List<MerchantRoleVO> roles = null;
    roles = merchantRoleService.list(null, keyword);

    // 如果merchantId不为空，则为选
    if (StringUtils.isNotEmpty(merchantId)) {
      Merchant merchant = merchantService.load(merchantId);
      String mroles = merchant.getRoles();
      for (MerchantRoleVO vo : roles) {
        if (mroles.indexOf(vo.getRoleName()) != -1) {
          vo.setIsSelect(true);
        } else {
          vo.setIsSelect(false);
        }
      }
    }

    Map<String, Object> aRetMap = new HashMap<String, Object>();
    // 取非ROOT和非BASIC角色
    aRetMap.put("total", merchantRoleService.selectNormalCnt());
    aRetMap.put("list", roles);
    return new ResponseObject<Map<String, Object>>(aRetMap);
  }

  @ResponseBody
  @RequestMapping(value = "merchantRole/update")
  public Json updateRole(Model model, @RequestParam String merchantId, @RequestParam String roles) {

    IUser user = merchantService.getCurrentMerchant();
    String userId = user.getId();

    Json json = new Json();

    Merchant merchant = new Merchant();
    merchant.setId(merchantId);
    merchant.setRoles(roles);
    int x = merchantService.update(merchant);

    if (x > 0) {

      json.setRc(Json.RC_SUCCESS);
      json.setMsg("删除成功!");

    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("删除失败!");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping("org/listMerchant")
  public ResponseObject<Map> listMerchant(String orgId, Pageable pageable, String order,
      String direction, String keyword,Boolean ifPage) {
    ifPage= null == ifPage?true:ifPage;
    if ("".equals(orgId) || "0".equals(orgId)) {
      orgId = null;
    }
    if (StringUtils.isEmpty(keyword)) {
      keyword = null;
    }

    IUser user = merchantService.getCurrentMerchant();
    String userId = user.getId();

    if (orgId != null) {
      Org org = orgService.load(orgId);
      if (org == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "查询的部门不存在");
      }
    }
    if ("".equals(order) || "0".equals(order)) {
      order = null;
    }
    order = StringUtils.defaultIfEmpty(order, "name");
    direction = StringUtils.defaultIfEmpty(direction, "desc");
    List<MerchantOrgVO> merchantList = orgService
        .listMerchant(orgId, pageable, order, keyword, Sort.Direction.fromString(direction), ifPage);
    for (MerchantOrgVO merchantOrg : merchantList) {
      String roles = merchantOrg.getRoles();
      if (StringUtils.isNotEmpty(roles)) {
        List<String> list = new ArrayList<String>();
        Collections.addAll(list, roles.split(","));
        String rolesDesc = merchantRoleService.getRoleDesc(list);
        merchantOrg.setRolesDesc(rolesDesc);
      }
    }
    Map<String, Object> merchantMap = new HashMap<String, Object>();
    if (!StringUtils.isEmpty(userId)) {
      merchantMap.put("userId", userId);
    }
    Integer merchantTotal = orgService.countByOrgId(orgId);
    merchantMap.put("merchantTotal", merchantTotal);
    merchantMap.put("list", merchantList);
    return new ResponseObject<Map>(merchantMap);
  }

}
