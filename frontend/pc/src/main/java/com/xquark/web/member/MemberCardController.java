package com.xquark.web.member;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.model.MemberCard;
import com.xquark.dal.model.User;
import com.xquark.dal.type.FileBelong;
import com.xquark.dal.vo.MemberCardVO;
import com.xquark.dal.vo.ProductCardsVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.memberCard.MemberCardService;
import com.xquark.service.product.ProductService;
import com.xquark.service.user.UserService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-6-27. DESC:
 */
@Controller
public class MemberCardController extends BaseController {

  private MemberCardService memberCardService;

  private ResourceFacade resourceFacade;

  private UserService userService;

  private ProductService productService;

  /**
   * 会员卡管理
   */
  @RequestMapping(value = "/member/card")
  public String memberCard() {
    return "member/listCard";
  }

  /**
   * 会员卡管理 - 新增会员卡
   */
  @RequestMapping(value = "/member/editCard")
  public String memberCardEdit(@RequestParam(value = "id", required = false) String id,
      Model model) {
    if (StringUtils.isNotBlank(id)) {
      MemberCard card = memberCardService.load(id);
      if (card != null) {
        model.addAttribute("card", card);
      }
    }
    return "member/editCard";
  }

  /**
   * 会员卡接口
   */
  @ResponseBody
  @RequestMapping("/member/card/list")
  public ResponseObject<Map<String, Object>> listMemberCards(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    if (StringUtils.isBlank(order)) {
      order = null;
    }
    List<MemberCard> memberCardList;
    Long total;
    try {
      memberCardList = memberCardService
          .list(order, Sort.Direction.fromString(direction), pageable);
      total = memberCardService.count();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "数据加载出错");
    }
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("list", memberCardList);
    result.put("total", total);
    return new ResponseObject<Map<String, Object>>(result);
  }

  @ResponseBody
  @RequestMapping("/member/card/listVO")
  public ResponseObject<Map<String, Object>> listMemberCardsVO(
      @RequestParam String productId, @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    if (StringUtils.isBlank(order)) {
      order = null;
    }
    List<MemberCardVO> memberCardList;
    Long total;
    try {
      memberCardList = memberCardService
          .list(productId, order, Sort.Direction.fromString(direction), pageable);
      total = memberCardService.count();
    } catch (BizException e) {
      throw e;
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "数据加载出错");
    }
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("list", memberCardList);
    result.put("total", total);
    return new ResponseObject<Map<String, Object>>(result);
  }

  /**
   * 会员等级分布 查询会员卡以及拥有该会员卡的用户数 前端封装成Echarts JSON格式
   */
  @ResponseBody
  @RequestMapping("/member/card/listWithMemberCounts")
  public ResponseObject<Map<String, Object>> listWithMemberCounts(
      @RequestParam(defaultValue = "created_at") String order,
      @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
    List<MemberCardVO> cardVOList;
    long total;
    try {
      cardVOList = memberCardService
          .listWithMemberCounts(order, Sort.Direction.fromString(direction), pageable);
      total = memberCardService.count();
    } catch (Exception e) {
      String msg = "获取会员卡信息失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("list", cardVOList);
    result.put("total", total);
    return new ResponseObject<Map<String, Object>>(result);
  }

  @ResponseBody
  @RequestMapping("/member/card/save")
  public ResponseObject<Boolean> save(MemberCard memberCard) {
    if (memberCard == null) {
      return new ResponseObject<Boolean>(Boolean.FALSE);
    }
    boolean result;
    String id = memberCard.getId();
    if (StringUtils.isBlank(id)) {
      memberCard.setId(null);
      // 创建
      try {
        result = memberCardService.save(memberCard);
      } catch (BizException e) {
        throw e;
      } catch (Exception e) {
        String msg = "保存失败";
        log.error(msg, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
      }
    } else {
      // 更新
      try {
        result = memberCardService.update(memberCard);
      } catch (BizException e) {
        throw e;
      } catch (Exception e) {
        String msg = "修改会员卡信息失败";
        log.error(msg, e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
      }
    }
    return new ResponseObject<Boolean>(result);
  }

  @ResponseBody
  @RequestMapping("/member/card/upload")
  public Map<String, Object> uploadBackGround(
      @RequestParam("imgFile") MultipartFile file) {
    String imgUrl = "";
    if (!file.isEmpty()) {
      InputStream fileInputStream;
      try {
        fileInputStream = file.getInputStream();
        UpLoadFileVO vo = resourceFacade.uploadFileStream(fileInputStream, FileBelong.SHOP);
        String img = vo.getId();
        imgUrl = resourceFacade.resolveUrl(img);
      } catch (Exception e) {
        log.error("文件上传失败", e);
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, e);
      }
    }
    Map<String, Object> result = new HashMap<String, Object>();
    // bootstrap fileupload 会把error字段显示在页面上，因此不使用ResponseObject
    result.put("img", imgUrl);
    result.put("errorCode", 200);
    return result;
  }

  @ResponseBody
  @RequestMapping("/member/card/bind")
  public ResponseObject<Boolean> setLevel(
      @RequestParam("userId") String userId,
      @RequestParam("cardId") String cardId) {
    User user = userService.load(userId);
    if (user == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "升级的用户不存在");
    }
    MemberCard card = memberCardService.load(cardId);
    if (card == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "升级的会员卡不存在");
    }
    boolean result;
    try {
      result = memberCardService.bindUser(userId, cardId);
    } catch (Exception e) {
      String msg = "设置等级出错";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    return new ResponseObject<Boolean>(result);
  }

  @ResponseBody
  @RequestMapping("/member/yundou/edit")
  public ResponseObject<Boolean> editYundou(
      @RequestParam("userId") String userId,
      @RequestParam("yundou") String yundou) {
    int result = userService.updateYundouAdd(userId, yundou);
    return new ResponseObject<Boolean>(result > 0);
  }

  /**
   * 绑定商品与会员卡信息 cardId支持逗号分隔
   */
  @ResponseBody
  @RequestMapping("/member/card/bindProduct")
  public ResponseObject<Boolean> bindProduct(
      @RequestParam("productId") String productId,
      @RequestParam("cardId") String cardId) {

    if (StringUtils.isBlank(productId) || StringUtils.isBlank(cardId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不正确");
    }
    String[] cardIds = StringUtils.split(cardId, ",");
    if (cardIds == null) {
      return new ResponseObject<Boolean>(Boolean.FALSE);
    }
    boolean result = memberCardService.bindProduct(productId, cardIds);
    return new ResponseObject<Boolean>(result);
  }

  @ResponseBody
  @RequestMapping("/member/card/unbindProduct")
  public ResponseObject<Boolean> unbindProduct(
      @RequestParam("productId") String productId,
      @RequestParam("cardId") String cardId) {
    if (StringUtils.isBlank(productId) || StringUtils.isBlank(cardId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "参数不正确");
    }
    boolean result = memberCardService.unbindProduct(productId, cardId);
    return new ResponseObject<Boolean>(result);
  }

  @ResponseBody
  @RequestMapping("/member/card/delete")
  public ResponseObject<Boolean> deleteProduct(
      @RequestParam("productId") String productId) {
    boolean result = memberCardService.deleteProduct(productId);
    return new ResponseObject<Boolean>(result);
  }

  /**
   * 绑定了优惠信息的商品列表
   */
  @ResponseBody
  @RequestMapping("/member/card/productList")
  public ResponseObject<Map<String, Object>> productList(String order,
      @RequestParam(defaultValue = "ASC") String direction, Pageable pageable) {
    List<ProductCardsVO> productCardsVOS;
    long total;
    try {
      productCardsVOS = productService
          .listProductWithCards(order, Sort.Direction.fromString(direction), pageable);
      total = productService.countProductWithCards();
    } catch (Exception e) {
      String msg = "获取优惠商品列表失败";
      log.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("list", productCardsVOS);
    result.put("total", total);
    return new ResponseObject<Map<String, Object>>(result);
  }

  @Autowired
  public void setMemberCardService(MemberCardService memberCardService) {
    this.memberCardService = memberCardService;
  }

  @Autowired
  public void setResourceFacade(ResourceFacade resourceFacade) {
    this.resourceFacade = resourceFacade;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setProductService(ProductService productService) {
    this.productService = productService;
  }
}
