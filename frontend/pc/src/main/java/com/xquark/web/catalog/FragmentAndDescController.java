package com.xquark.web.catalog;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Image;
import com.xquark.dal.model.ProductDesc;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Shop;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.service.file.ImageService;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.product.ProductDescService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.web.BaseController;

@Controller
public class FragmentAndDescController extends BaseController {

  @Autowired
  protected ProductDescService productDescService;
  @Autowired
  protected ProductService productService;
  @Autowired
  protected ShopService shopService;
  @Autowired
  protected ImageService imageService;
  @Autowired
  protected ResourceFacade resourceFacade;
  @Autowired
  protected FragmentService fragmentService;
  @Autowired
  protected FragmentImageService fragmentImageService;

  protected void setFragmentAndDescInfo(ProductVO product) {
    if (product.getEnableDesc() != null && product.getEnableDesc()) {
      // 获取富文本信息
      ProductDesc productDesc = productDescService.load(product.getId());
      if (productDesc != null) {
        product.setProductDesc(productDesc);
      }
      return;
    }

    // 当店铺片段功能开启时，读取商品的片段信息
    Shop shop = shopService.load(product.getShopId());
    if (shop.getFragmentStatus()) {
      List<FragmentVO> fragments = getProductFragmentList(product.getId());
      product.setFragments(fragments);
    }
  }

  /**
   * 回填片段信息的图片信息
   */
  protected void setFragmentImage(FragmentVO... fragments) {
    for (FragmentVO vo : fragments) {
      List<FragmentImageVO> imgs = fragmentImageService.selectByFragmentId(vo.getId());
      List<FragmentImageVO> showImgs = new ArrayList<FragmentImageVO>();
      for (FragmentImageVO imgVo : imgs) {
        // 获取图片信息 小于一定大小的图片不再显示
        Image image = imageService.loadByImgKey(imgVo.getImg());
        if (image == null) {
          continue;
        }

        if (image.getWidth() <= 0 || image.getHeight() <= 0) {
          continue;
        }

        imgVo.setImgUrl(resourceFacade.resolveUrl(imgVo.getImg()));
        imgVo.setWidth(image.getWidth());
        imgVo.setHeight(image.getHeight());
        showImgs.add(imgVo);
      }

      vo.setImgs(showImgs);
    }
  }

  /**
   * 通过商品的id获取商品的片段信息
   */
  protected List<FragmentVO> getProductFragmentList(String productId) {
    List<FragmentVO> fragments = fragmentService.selectByProductId(productId);
    setFragmentImage(fragments.toArray(new FragmentVO[fragments.size()]));
    return fragments;
  }


  /**
   * 通过店铺获取片段信息
   */
  protected List<FragmentVO> getFragmentByShop(String shopId) {
    List<FragmentVO> list = fragmentService.selectByShopId(shopId);
    setFragmentImage(list.toArray(new FragmentVO[list.size()]));
    return list;
  }

  /**
   * 获取主图和sku图和详情图
   */
  protected void setMainImgs(ProductVO product) {
    List<String> imgsList = new ArrayList<String>();
    List<ProductImage> imgs = productService
        .requestImgs(String.valueOf(IdTypeHandler.decode(product.getId())), "");
    for (ProductImage img : imgs) {
      imgsList.add(resourceFacade.resolveUrl(img.getImg() + "|" + ResourceFacade.IMAGE_S1));
    }

    product.setMainImgs(imgsList);
  }
}
