package com.xquark.web.trade;

import com.xquark.service.order.OrderService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by chh on 17/06/26.
 */
@Controller
public class TradeController extends BaseController {

  @Autowired
  private OrderService orderService;


  @RequestMapping(value = "/trade/summary")
  public String twitterSummary(Model model) {
    return "/trade/tradeSummary";
  }

  @RequestMapping(value = "/trade/record")
  public String tradeRecord(Model model) {
    return "/trade/tradeRecord";
  }

}
