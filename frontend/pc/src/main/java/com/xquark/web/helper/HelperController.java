package com.xquark.web.helper;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by wangxinhua on 17-12-7. DESC:
 */
@Controller
@RequestMapping("/helper")
public class HelperController {

  @RequestMapping("/list")
  public String list() {
    return "helper/list";
  }

  @RequestMapping("edit")
  public String edit() {
    return "helper/edit";
  }

}
