

package com.xquark.web;

import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

/**
 * web工程的基类Controller
 *
 * @author odin
 */
public class BaseController {

  protected static final String COOKIE_NAME_KDSESSID = "KDSESSID";
  protected static final String COOKIE_NAME_KDAUTHTOKEN = "kdAuthToken";
  protected static final String COOKIE_NAME_FROMPAGE = "FromPage";
  protected static final String COOKIE_NAME_FROMCHANNEL = "FromChannel";
  public static final String COOKIE_NAME_INTERNALSHOP = "ISVToken";

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Value("${alipay.domain}")
  String alipayDomain;

  @Value("${profiles.active}")
  String profile;

  @Autowired
  private ShopService shopService;


  @Autowired
  private UserService userService;

  /**
   * 获取当前用户信息 如果是未登录的匿名用户，系统根据匿名用户唯一码自动创建一个用户 具体逻辑查看：UniqueNoFilter
   */
  public User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      } else if (principal instanceof Merchant) {
        Shop shop = shopService.load(((Merchant) principal).getShopId());
        return userService.load(shop.getOwnerId());
      }
      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

  public Merchant getCurrentMerchant() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof Merchant) {
        return (Merchant) principal;
      }
      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }


  /*
   * 封装page对象 sort结构示例 Iterator<org.springframework.data.domain.Sort.Order>
   * orders = page.getSort().iterator(); if(orders.hasNext()){
   * orders.next().getDirection(); orders.next().getProperty() }
   */
  protected Pageable initPage(Integer page, Integer size, String orderName,
      Boolean isDesc) {
    if (size == null) {
      size = 10;
    }

    if (page == null) {
      page = 0;
    }
    if (isDesc != null || !StringUtils.isBlank(orderName)) {
      Sort.Direction orderType = Sort.Direction.DESC;
      if (isDesc != null && !isDesc) {
        orderType = Sort.Direction.ASC;
      }

      return new PageRequest(page, size, orderType, orderName);
    }
    return new PageRequest(page, size);
  }


  /*
   * 封装page对象 sort结构示例 Iterator<org.springframework.data.domain.Sort.Order>
   * orders = page.getSort().iterator(); if(orders.hasNext()){
   * orders.next().getDirection(); orders.next().getProperty() }
   */
  protected Pageable initPage(Pageable pageable, Boolean isAsc, String orderName) {

    Integer size = null;
    Integer page = null;

    if (pageable != null) {
      size = pageable.getPageSize();
      page = pageable.getPageNumber();
    }
    if (size == null) {
      size = 10;
    }

    if (page == null) {
      page = 0;
    }

    if (isAsc != null || !StringUtils.isBlank(orderName)) {
      Sort.Direction orderType = Sort.Direction.ASC;
      if (isAsc != null && !isAsc) {
        orderType = Sort.Direction.DESC;
      }

      return new PageRequest(page, size, orderType, orderName);
    }
    return new PageRequest(page, size);
  }

}

