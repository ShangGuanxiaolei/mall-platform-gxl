package com.xquark.web.category;

import com.xquark.dal.type.Taxonomy;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by chh on 16/09/19.
 */
@Controller
public class CategoryController extends BaseController {

  /**
   * 类别管理
   */
  @RequestMapping(value = "category/list")
  public String fansList(@RequestParam("type") Taxonomy taxonomy) {
    if (taxonomy == Taxonomy.GOODS) {
      return "category/categoryList";
    }
    if (taxonomy == Taxonomy.FILTER) {
      return "category/filterCategoryList";
    }
    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品分类类型不正确");
  }

}