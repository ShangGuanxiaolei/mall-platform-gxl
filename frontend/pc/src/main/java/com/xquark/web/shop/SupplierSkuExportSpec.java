package com.xquark.web.shop;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wangxinhua on 18-3-19. DESC:
 */
public class SupplierSkuExportSpec {

  public static final String SHEET_STR = "商品规格模板";

  private static final Map<String, String> EXPORT_FORMAT = new LinkedHashMap<String, String>();

  public static String[] KEY_SET;
  public static String[] VALUE_SET;

  static {
    EXPORT_FORMAT.put("SKU编码", "getSkuCode");
    EXPORT_FORMAT.put("条形码", "getBarCode");
    EXPORT_FORMAT.put("SKU名称", "getProductName");
    EXPORT_FORMAT.put("库存", "getAmount");
    KEY_SET = getArrayFromHash(EXPORT_FORMAT)[0];
    VALUE_SET = getArrayFromHash(EXPORT_FORMAT)[1];
  }

  private static String[][] getArrayFromHash(Map<String, String> data) {
    String[][] str;
    {
      Object[] keys = data.keySet().toArray();
      Object[] values = data.values().toArray();
      str = new String[keys.length][values.length];
      for (int i = 0; i < keys.length; i++) {
        str[0][i] = (String) keys[i];
        str[1][i] = (String) values[i];
      }
    }
    return str;
  }
}
