package com.xquark.web.team;

import com.xquark.dal.model.TeamShopCommission;
import com.xquark.service.team.TeamShopComDeService;
import com.xquark.service.team.TeamShopComService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * 战队管理 Created by chh on 17/7/4.
 */
@Controller
public class TeamController extends BaseController {

  @Autowired
  private TeamShopComDeService teamShopComDeService;

  @Autowired
  private TeamShopComService teamShopComService;

  @RequestMapping(value = "/team/setting")
  public String teamSetting(Model model) {

    //获得shopId
    String shopId = getCurrentUser().getShopId();
    //找到默认配置信息
    TeamShopCommission teamShopCommission = teamShopComService.selectDefaultByshopId(shopId);
    model.addAttribute("teamSetting", teamShopCommission);

    Map map = teamShopComService.getWeeksAndMonths(teamShopCommission, shopId);

    model.addAttribute("weeks", map.get("weeks"));
    model.addAttribute("months", map.get("months"));

    return "/team/teamSetting";
  }

  @RequestMapping(value = "/team/member")
  public String teamMember(Model model) {
    String shopId = getCurrentUser().getShopId();
    // 获取所有战队分组，供页面选择
    List<TeamShopCommission> groups = teamShopComService.selectAllByshopId(shopId);
    model.addAttribute("groups", groups);
    return "/team/teamMember";
  }

  @RequestMapping(value = "/team/record")
  public String teamRecord(Model model) {
    return "/team/teamRecord";
  }

  @RequestMapping(value = "/team/order")
  public String teamOrder(Model model) {
    return "/team/teamOrder";
  }

  @RequestMapping(value = "/team/summary")
  public String teamSummary(Model model) {
    return "/team/teamSummary";
  }

  @RequestMapping(value = "/store/member")
  public String storeMember(Model model) {
    return "/store/storeMember";
  }

  @RequestMapping(value = "/team/apply")
  public String applyList(Model model) {
    return "/team/teamApply";
  }

}
