package com.xquark.web.wechat;

import com.xquark.dal.model.wechat.ButtonType;

/**
 * Created by dongsongjie on 16/5/25.
 */
public class CustomizedMenuItemForm {

  private String id;

  private String name;

  private ButtonType type;

  private String parentId;

  private Integer index;

  private String url;

  private String contentOrMediaId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ButtonType getType() {
    return type;
  }

  public void setType(ButtonType type) {
    this.type = type;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getContentOrMediaId() {
    return contentOrMediaId;
  }

  public void setContentOrMediaId(String contentOrMediaId) {
    this.contentOrMediaId = contentOrMediaId;
  }
}
