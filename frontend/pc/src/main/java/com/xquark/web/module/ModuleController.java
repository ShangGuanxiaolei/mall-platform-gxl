package com.xquark.web.module;

import com.xquark.dal.model.Module;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 作者: wangxh 创建日期: 17-3-27 简介:
 */
@Controller
public class ModuleController {

  @RequestMapping("/module/manage")
  public String manage(Module module) {
    return "module/moduleManage";
  }

}
