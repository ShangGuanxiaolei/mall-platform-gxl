package com.xquark.web.coupon;

/**
 * Created by  on 15-11-5.
 */
public class CouponSearchForm {

  private String distributedAt;
  private String usedNumber;
  private String validFrom;
  private String validTo;
  private String status;
  private String flag;
  private String shopId;
  private String direction; //排序方式  desc asc
  private String order;   //排序字段


  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  public String getDistributedAt() {
    return distributedAt;
  }

  public void setDistributedAt(String distributedAt) {
    this.distributedAt = distributedAt;
  }

  public String getUsedNumber() {
    return usedNumber;
  }

  public void setUsedNumber(String usedNumber) {
    this.usedNumber = usedNumber;
  }

  public String getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(String validFrom) {
    this.validFrom = validFrom;
  }

  public String getValidTo() {
    return validTo;
  }

  public void setValidTo(String validTo) {
    this.validTo = validTo;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }
}
