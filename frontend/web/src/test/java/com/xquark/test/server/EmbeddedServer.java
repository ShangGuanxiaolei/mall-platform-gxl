package com.xquark.test.server;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.Context;
import org.apache.catalina.loader.VirtualWebappLoader;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.lang3.StringUtils;
import org.apache.naming.resources.VirtualDirContext;

import com.radiadesign.catalina.session.RedisSessionHandlerValve;
import com.radiadesign.catalina.session.RedisSessionManager;

import javax.servlet.ServletException;

public class EmbeddedServer {

  /**
   * 启动时请将basedir设置为整个项目的根目录，便于启动多个module
   */
  public static void main(String[] args) throws Exception {

    final Tomcat tomcat = new Tomcat();
    tomcat.setPort(80);
    tomcat.setBaseDir("target/tomcat");
    tomcat.getConnector().setURIEncoding("UTF-8");

    addModule(tomcat, "/", "frontend/web/", "target/xquark-web-0.1.0-SNAPSHOT/WEB-INF/lib");
    addModule(tomcat, "/bos", "frontend/bos/", "target/xquark-bos-0.1.0-SNAPSHOT/WEB-INF/lib");

    tomcat.start();
    tomcat.getServer().await();
  }

  private static void addModule(Tomcat tomcat, String contextPath, String modulePath,
      String libPath) throws ServletException, MalformedURLException {
    final Context ctx = tomcat
        .addWebapp(contextPath, new File(modulePath + "src/main/webapp").getAbsolutePath());
    final RedisSessionManager manager = new RedisSessionManager();
    manager.setDatabase(0);
    manager.setHost("127.0.0.1");
    manager.setPort(6379);
    manager.setMaxInactiveInterval(60);
    ctx.setManager(manager);
    ctx.getPipeline().addValve(new RedisSessionHandlerValve());

    //设置按双亲委派查找class
    final File lib = new File(modulePath + libPath);

    // declare an alternate location for your "WEB-INF/classes" and "WEB-INF/lib" dir:
    final File war = new File(modulePath + "target/classes");
    final VirtualDirContext resources = new VirtualDirContext();
    resources.setExtraResourcePaths("/WEB-INF/classes=" + war + ","
        + "/WEB-INF/lib=" + lib);
    ctx.setResources(resources);
    //设置为动态加载/WEB-INF/classes，/WEB-INF/lib下面的class和jar
    ctx.setReloadable(true);
  }
}
