define("kdweb/0.0.1/js/page/confirm-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  require("kdweb/0.0.1/js/module/num-debug");
  //require('../module/select.js');
  require("kdweb/0.0.1/js/module/addressEditor-debug");
  require("kdweb/0.0.1/js/plugins/tab-debug");
  require("kdweb/0.0.1/js/module/cardBank-debug");
  $(document).ready(function() {
    var $$ = {
      ADDRESSSHOW: $('.address-show'),
      MONICHECKBOX: $('.moni-checkbox'),
      ADDRESSEDITOR: $(".addrss-editor"),
      CARDTABITEM: $('.card-tab-item a'),
      PAYCARD: $('#pay-card'),
      PAYZHIFUBAO: $('#pay-zhifubao'),
      CARTCARD: $(".cart-card"),
      CARTVIPLIST: $('.cart-vip-list'),
      ADDRESSMODIFY: $(".address-modify"),
      TABLEABLE: $('.table-able'),
      TOTALFEEID: $("#totalFee"),
      TOTALFEE: $("#cart-total-price").find('span'),
      ADDRESSBTN: $('#addressBtn'),
      ADDRESSID: $("#addressId"),
      ORDERBTN: $('#order-btn'),
      PAYTYPE: $('#payType'),
      ORDERPAYFORM: $('#id-cart'),
      CONFIRMTR: $('.confirm-tr'),
      cartcheckout: $('#vip2choose'),
      cartVIPBTN: $('#cart-vip-btn'),
      cartcheckoutVIP3LIST: $('.cart-vip3-list'),
      CARTYOUHUIMANAME: $("#cart-youhuima-name"),
      CARTVIPINPUT: $(".cart-vip-input"),
      SKUS: $("#skus"),
      CARTVIPSELECT: $(".cart-vip-select").find(".moni-checkbox"),
      VIP1: $("#vip1"),
      VIP2: $("#vip2"),
      VIPYOUHUI: $("#vip-youhui"),
      AGREEMENTID: $("#payAgreementId"),
      CARTINPUT: $(".cart-input")
    };
    /*   $$.MONICHECKBOX.bind('click',function(){
           $$.CARTVIPLIST.toggle();
       })*/
    $$.ADDRESSEDITOR.bind('click', function() {
      $$.ADDRESSMODIFY.show();
      $$.ADDRESSSHOW.hide();
      $$.ADDRESSID.val('');
    })
    $$.CARDTABITEM.click(function(e) {
        e.preventDefault();
        $(this).tab('show');
      })
      //字数限制
    function number(obj, numb) {
        obj.each(function(i, item) {
          if ($(item).text().length > numb) {
            $(item).text($(item).text().substring(0, numb) + "...");
          }
        })
      }
      //产品标题2行限制
    number($('.J-number'), 20);
    //sku2行限制
    number($('.confirm-sku'), 24);
    //qty参数修改
    if (!request('qty')) {
      $('#qty').val('0');
    } else {
      var amount = parseInt(request('qty'));
      $('#qty').val(amount);
    }
    /* 获取URL参数 */
    function request(paras) {
      var url = location.href;
      var paraString = url.substring(url.indexOf("?") + 1, url.length).split(/\&|\#/g);
      var paraObj = {}
      for (i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
      }
      var returnValue = paraObj[paras.toLowerCase()];
      if (typeof(returnValue) == "undefined") {
        return "";
      } else {
        return returnValue;
      }
    }

    //华为渠道红包（pc端华为渠道红包不显示）
    $('.cart-vip-input[type="checkbox"]').each(function(){
      var _this = $(this);
      var couponType = _this.attr('data-coupon-type');
      if (couponType=='XQ.HUAWEI1507') {
        if (_this.is(':checked')) {
          _this.click();
        }
        _this.parents('li.cart-vip-item').hide();
      };
    });

    function getShopPriceJson(param) {
        $.ajax({
          url: '/cart/pricing-groupby-shop',
          data: param,
          traditional: true,
          success: function(data) {
            if (data.errorCode == 200) {
              $$.TABLEABLE.each(function(i, item) {
                var shopid = $(item).attr('data-shop-id');
                $(item).next().find(".totalprice").text(Number(data.data.pricesMap[shopid].totalFee).toFixed(2));
                $(item).next().find(".footer-youfei-price").text(Number(data.data.pricesMap[shopid].logisticsFee).toFixed(2));
              })
              $$.TOTALFEE.text(Number(data.data.totalFee).toFixed(2));
              $$.TOTALFEEID.html('￥' + Number(data.data.totalFee).toFixed(2));
              vipPrice();
            } else {
              alert(data.moreInfo);
            }
          }
        });
      }
      //初始优惠码状态，选择框是否显示
    function vipHongbao() {
        if ($$.cartcheckout.is(':checked')) {
          $$.cartcheckoutVIP3LIST.show();
          vipPrice(); //计算优惠总金额
        } else {
          $$.cartcheckoutVIP3LIST.hide();
          if ($$.VIP1.is(':checked')) {
            $$.TOTALFEEID.text("￥" + Math.max(((Number($$.TOTALFEE.text().replace(/\￥|\,/g, '') * 100) - Number($$.VIP1.attr('data-numb') * 100)) / 100).toFixed(2), 0.00));
            //$$.VIPYOUHUI.text(Math.min(Number($$.VIP1.attr('data-numb')).toFixed(2)),Number($$.TOTALFEE.text().replace('￥','').replace(',','')));
            $$.VIPYOUHUI.text(Math.min(Number($$.VIP1.attr('data-numb')).toFixed(2), Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2)));
          } else {
            $$.TOTALFEEID.text("￥" + Math.max(Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2), 0.00).toFixed(2));
            vipPrice();
          }
        }
      }
      //VIP2
    $$.cartcheckout.bind('click', function() {
      vipHongbao();
    });
    $$.cartVIPBTN.bind('click', function() {
      var self = $(this);
      var param = {
        code: self.prev().val()
      };
      if (!self.prev().val()) {
        alert('优惠券号码不能为空！');
        self.prev().focus();
        return false;
      }
      $.ajax({
        url: '/coupon/detail',
        data: param,
        traditional: true,
        success: function(data) {
          if (data.errorCode == 200) {
            $$.CARTINPUT.name = "preferential";
            $$.VIP2.attr("data-extCouponId", data.data.extCouponId);
            $$.VIP2.attr("data-id", data.data.id);
            $$.CARTYOUHUIMANAME.text("获得   " + data.data.discount + '  元红包');
            $$.CARTYOUHUIMANAME.prev().attr("data-numb", data.data.discount);
            checkboxPrice();
            vipPrice();
            if ($$.CARTYOUHUIMANAME.hasClass("cart-error-red")) {
              $$.CARTYOUHUIMANAME.removeClass("cart-error-red");
            }
          } else {
            $$.CARTYOUHUIMANAME.text(data.moreInfo);
            $$.VIP2.attr("data-numb", "");
            checkboxPrice();
            vipPrice();
            if (!$$.CARTYOUHUIMANAME.hasClass("cart-error-red")) {
              $$.CARTYOUHUIMANAME.addClass("cart-error-red");
            }
          }
        }
      });
    });
    //已选择的优惠价格和
    function checkboxPrice() {
        var n = 0;
        $(".cart-vip-input:checked").each(function(i, item) {
          n = n + Number($(item).attr('data-numb'));
        })
        return n;
      }
      //计算优惠金额
    function vipPrice() {
      if ($('.moni-checkbox').hasClass('moni-checkbox-active')) {
        $$.VIPYOUHUI.text(Math.min(Number(checkboxPrice()).toFixed(2), Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2)).toFixed(2));
      }
      if ((Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')) - Number($$.VIPYOUHUI.text())).toFixed(2) > 0) {
        $$.TOTALFEEID.text("￥" + Math.max(((Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')) * 100 - Number($$.VIPYOUHUI.text()) * 100) / 100).toFixed(2), 0.00).toFixed(2));
      } else {
        $$.TOTALFEEID.text("￥" + 0.00);
      }
    }
    $$.CARTVIPSELECT.bind('click', function() {
        var moniCheckbox = $(".moni-checkbox");
        if ($$.CARTVIPSELECT.hasClass('moni-checkbox-active')) {
          moniCheckbox.removeClass("moni-checkbox-active");
          $$.CARTVIPLIST.hide();
          $$.VIPYOUHUI.text("0.00");
          $$.TOTALFEEID.text("￥" + Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2));
        } else {
          moniCheckbox.addClass("moni-checkbox-active");
          $$.VIPYOUHUI.text(Math.min(Number(checkboxPrice()).toFixed(2), Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2)).toFixed(2));
          $$.TOTALFEEID.text("￥" + Math.max(Number($$.TOTALFEE.text().replace(/\￥|\,/g, '')).toFixed(2), 0.00));
          $$.CARTVIPLIST.show();
          vipPrice(); //计算优惠总金额
          vipHongbao(); //初始优惠码状态，选择框是否显示
        }
      })
      //优惠券是否勾选
    var lenChecked = $('input.cart-vip-input:checked').length;
    if (lenChecked) {
      $$.CARTVIPSELECT.click();
    }
    $$.CARTVIPINPUT.bind('click', function() {
        vipPrice();
      })
      //地址变更
    var t;
    clearTimeout(t);
    $$.ADDRESSBTN.bind('click', function() {
      t = setTimeout(function() {
        var Id = $$.ADDRESSID.val();
        var skuid = [];
        var param;
        $$.CONFIRMTR.each(function(i, item) {
          skuid.push($(item).attr('data-sku-id'));
          var zoneid = $("#zoneId").val();
          param = {
            zoneId: zoneid,
            skuIds: skuid,
            qty: $('#qty').val()
          };
        })
        getShopPriceJson(param);
      }, 100)
    })

    function youhuiquanChoose() {
      var j = 0;
      $$.CARTVIPINPUT.each(function(i, item) {
        if ($(item).is(':checked')) {
          var couponType = "<input class='preferential-hiddle' type='hidden' name='coupon" + "[" + j + "]." + "activityId' " + " value='" + $(item).attr('data-coupon-type') + "'>";
          var extCouponId = "<input class='preferential-hiddle' type='hidden' name='coupon" + "[" + j + "]." + "extCouponId' " + " value='" + $(item).attr('data-extcouponid') + "'>";
          var id = "<input class='preferential-hiddle' type='hidden' name='coupon" + "[" + j + "]." + "id' " + " value='" + $(item).attr('data-id') + "'>";
          var code = "<input class='preferential-hiddle youhuima-hiddle' type='hidden' name='coupon" + "[" + j + "]." + "code' " + " value='" + $('#youhuimaInput').val() + "'>";
          $$.AGREEMENTID.after(couponType);
          $$.AGREEMENTID.after(extCouponId);
          $$.AGREEMENTID.after(id);
          // if ($(item).hasClass("cart-vip2-input") && !$$.VIP2.attr("data-numb") == "") {
          //   /*    $$.AGREEMENTID.after(couponType);
          //       $$.AGREEMENTID.after(extCouponId);*/
          //   $$.AGREEMENTID.after(id);
          //   if (!$(".preferential-hiddle").hasClass("youhuima-hiddle")) {
          //     $$.AGREEMENTID.after(code);
          //   }
          // } else {
          //   /*  $$.AGREEMENTID.after(couponType);
          //     $$.AGREEMENTID.after(extCouponId);*/
          //   if ($(item).attr("data-coupon-type") == "XQ.FIRST") {
          //     $$.AGREEMENTID.after(id);
          //   }
          // }
          j++;
        }
      })
    }
    $$.ORDERBTN.bind('click', function(e) {
      e.preventDefault();
      var T, checkSubmitFlg = false;
      clearTimeout(T);
      //传入danbao addressId payType payAgreementId （协议支付id） cardType bankCode
      if ($$.ADDRESSID.length == 0 || $$.ADDRESSID.val() == '') {
        alert('亲，地址不能为空，请添加地址！');
        $("html,body").animate({
          scrollTop: $(".cart-address").offset().top
        }, 100);
        $("#province").focus();
        return false;
      }
      if ($$.PAYTYPE.val() == '') {
        //formFouc();
        alert("请选择一种付款方式");
        return false;
      }
      if ($$.CARTVIPSELECT.hasClass("moni-checkbox-active") && $$.cartcheckout.is(':checked')) {
        if ($$.VIP2.is(':checked') && $$.CARTINPUT.val() == '') {
          alert("请填写优惠码,或是把优惠码前面的勾勾去掉哦~");
          return false;
        }
        if ($$.VIP2.is(':checked') && $$.CARTYOUHUIMANAME.hasClass("cart-error-red")) {
          alert("请填写正确优惠码,或是把优惠码前面的勾勾去掉哦~");
          return false;
        }
        if ($$.VIP2.is(':checked') && $$.VIP2.attr('data-numb') == '') {
          alert("优惠码要兑换才能使用哦,请先点击兑换按钮，或是把优惠码前面的勾勾去掉哦~");
          return false;
        }
      }
      if ($$.PAYCARD.is(":checked") && $("#bankCode").val() == '') {
        alert("请选择银行卡~");
        $(".cart-card").addClass("cart-card-lh");
        T = setTimeout(function() {
          $(".cart-card").removeClass("cart-card-lh");
        }, 3000);
        return false;
      }
      if ($$.CARTVIPSELECT.hasClass("moni-checkbox-active")) {
        youhuiquanChoose();
      }
      if (checkSubmitFlg == true) {
        return false; //当表单被提交过一次后checkSubmitFlg将变为true,根据判断将无法进行提交。
      }
      checkSubmitFlg == true;
      //$("#id-cart").attr("action","/xiangqu/web/order/submit");
      $$.ORDERPAYFORM.submit();
    });

    function payShow() {
      if ($$.PAYCARD.is(':checked')) {
        $$.CARTCARD.show();
      } else {
        $$.CARTCARD.hide();
      }
    }
    payShow();
    $(".cart-pay-item").bind('click', function() {
      payShow();
      $$.PAYTYPE.val($(this).attr('data-pay-type'));
      $$.AGREEMENTID.val($(this).attr('data-agreementid'));
    })

    function hbIsShow() {
      var hongbaoList = $(".vart-hongbao-list");
      if (hongbaoList.attr("data-coupon-valid") == 'false') {
        hongbaoList.addClass('disabled');
        hongbaoList.children('input').attr('disabled', true);
        hongbaoList.children('.cart-msg').removeClass('fn-hiddle');
      }
    }
    hbIsShow();
  });
  //});
});
define("kdweb/0.0.1/js/module/num-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //var $=require('jquery');
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  var numed = $(".numed");
  //数量输入框限定只能输入数字
  numed.on('change', function() {
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);
    var inventory = $(this).closest('tr').attr('data-inventory');
    if (!re.test($(this).val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
    } else if (Number($(this).val()) < 1) {
      numed.val('1').text('1').trigger('change');
    } else if (Number($(this).val()) > Number($(this).closest('tr').attr('data-inventory'))) {
      //alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      $(this).val(inventory).text(inventory).trigger('change');
    }
  });
  $('.numMinus').on('click', function() {
    var self = $(this),
      numedInput = self.next('.numed'),
      edge = self.attr('edge'),
      oldVal = parseInt(numedInput.val()),
      oldVal = isNaN(oldVal) ? 0 : oldVal,
      edge = self.attr('edge'),
      newVal = oldVal - 1;
    if (Number(newVal) < 1) {
      //alert('亲，所选数量不能小于1件哦~');
      numedInput.val(edge).text(edge).trigger('change');
    } else {
      numedInput.val(oldVal - 1).text(oldVal - 1).trigger('change');
    }
    return false;
  });
  $('.numPlus').on('click', function() {
    var self = $(this),
      numedInput = self.prev('.numed'),
      edge = self.attr('edge'),
      oldVal = parseInt(numedInput.val()),
      oldVal = isNaN(oldVal) ? 0 : oldVal,
      newVal = oldVal + 1;
    var inventory = $(this).closest('tr').attr('data-inventory');
    if (Number(newVal) > Number(inventory)) {
      //alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      numedInput.val(inventory).text(inventory).trigger('change');
    } else {
      numedInput.val(newVal).text(newVal).trigger('change');
    }
    return false;
  });
  /*  function calculate(obj){
        var self = $(obj), numed = self.parent().children('.numed'),
            delta = self.hasClass('numInc') ? 1 : -1, edge = self.attr('edge'),
            newVal = oldVal + delta;

        */
  /*  if (!!edge && (delta > 0 && newVal > edge || delta < 0 && newVal < edge)) {
                newVal = edge;
            }*/
  /*
      }*/
  //});
});
define("kdweb/0.0.1/js/module/addressEditor-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  var Address = (function() {
    return function() {
      this.initProvinces = function() {
        var provinceEle = $('#province');
        $.ajax({
          url: '/zone/1/children',
          success: function(data) {
            $.each(data, function(i, item) {
              provinceEle.append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            });
          }
        });
      }
      this.initCities = function(provinceId) {
        var cityEle = $('#city');
        cityEle.find('option').each(function() {
          if ($(this).val() > 0) {
            $(this).remove();
          }
        });
        $.ajax({
          url: '/zone/' + provinceId + '/children',
          success: function(data) {
            $.each(data, function(index, val) {
              cityEle.append('<option value="' + val.id + '">' + val.name + '</option>');
            });
          }
        });
      }
      this.initDistricts = function(cityId) {
        var districtEle = $('#district');
        districtEle.find('option').each(function() {
          if ($(this).val() > 0) {
            $(this).remove();
          }
        });
        $.ajax({
          url: '/zone/' + cityId + '/children',
          success: function(data) {
            $.each(data, function(index, val) {
              districtEle.append('<option value="' + val.id + '">' + val.name + '</option>');
            });
          }
        });
      }
    }
  })();
  var address = new Address();
  if ($('#id').length == 0) {
    address.initProvinces();
  }
  $('#province').on('change', function() {
    var addressShowDistrict = $(".address-show-district");
    //判断是否是市，去掉地区选择
    if ($(this).val() == "2" || $(this).val() == "20" || $(this).val() == "2465" || $(this).val() == "861") {
      $("#district").hide();
      if (!$(".address-show-district").length == 0) {
        addressShowDistrict && addressShowDistrict.remove('.address-show-district');
      }
    } else {
      $("#district").show();
      if ($(".address-show-district").length == 0) {
        $('.address-show-city').after('<span class="address-show-district"></span>');
      }
    }
    $('#district').find('option').each(function() {
      if ($(this).val() > 0) {
        $(this).remove();
      }
    });
    address.initCities($(this).val());
    //        address.updatePrice();
  });
  $('#city').on('change', function() {
    address.initDistricts($(this).val());
  });
  $("#phone").bind('blur', function() {
    var regu = /^[1][3-8][0-9]{9}$/;
    var re = new RegExp(regu);
    var phoneobj = $("#phone").val();
    clearTimeout(t);
    $(".phone-msg").css("display", "none");
    if (re.test(phoneobj)) {
      $(".phone-msg").css("display", "none");
    } else {
      $(".phone-msg").css("display", "block");
      var t = setTimeout(function() {
        $(".phone-msg").css("display", "none");
      }, 5000);
    }
  });
  $("#phone").bind('focus', function() {
    $(".phone-msg").css("display", "none");
  });
  $('#addressBtn').bind('click', function() {
    var regu = /^[1][3-8][0-9]{9}$/;
    var re = new RegExp(regu);
    if ($('#consignee').val() == '') {
      alert('收货人姓名不能为空');
      return false;
    }
    if (!re.test($("#phone").val())) {
      alert('请输入正确的手机号码!');
      return false;
    }
    if ($('#phone').val() == '') {
      alert('手机号码不能为空');
      return false;
    }
    if ($('#province').val() == '0') {
      alert("省份不能为空");
      return false;
    }
    if ($('#city').find('option').length > 1 && $('#city').val() == '0') {
      alert("市不能为空");
      return false;
    }
    if ($('#district').find('option').length > 1 && $('#district').val() == '0') {
      alert("地区不能为空");
      return false;
    }
    if ($('#street').val() == '') {
      alert("街道地址不能为空");
      return false;
    }
    $('#zoneId').val($('#district').val() != '0' ? $('#district').val() : ($('#city').val() != '0' ? $('#city').val() : $('#province').val()));
    var T;
    clearTimeout(T);
    var consignee = $('#consignee').val();
    var phone = $('#phone').val();
    var zoneId = $('#zoneId').val();
    var street = $('#street').val();
    var params = {
      consignee: consignee,
      phone: phone,
      zoneId: zoneId,
      street: street
    };
    if ($('#addressId').length > 0) {
      params.id = $("#addId").val();
    }
    var url = $('#post_form').attr('action');
    /* $.post(url, params, function() {

     });*/
    $.ajax({
      type: "POST",
      dataType: 'json',
      url: url,
      data: params,
      success: function(data) {
        console.log(data);
        $(".address-show-name").text(data.data.consignee);
        $(".address-show-tel").text(data.data.phone);
        $(".address-show-address").text(data.data.details);
        $("#addressId").val(data.data.id);
        $("#zoneId").val(data.data.zoneId);
        $(".address-modify").hide();
        $(".address-show").show();
        //location.reload();
      }
    });
  });
  //});
});
define("kdweb/0.0.1/js/plugins/tab-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  'use strict';
  // TAB CLASS DEFINITION
  // ====================
  var Tab = function(element) {
    this.element = $(element)
  }
  Tab.VERSION = '3.3.0'
  Tab.TRANSITION_DURATION = 150
  Tab.prototype.show = function() {
    var $this = this.element
    var $ul = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')
    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }
    if ($this.parent('li').hasClass('active')) return
    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })
    $previous.trigger(hideEvent)
    $this.trigger(showEvent)
    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return
    var $target = $(selector)
    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function() {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }
  Tab.prototype.activate = function(element, container, callback) {
      var $active = container.find('> .active')
      var transition = callback && $.support.transition && (($active.length && $active.hasClass('fade')) || !!container.find('> .fade').length)

      function next() {
        $active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded', false)
        element.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded', true)
        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }
        if (element.parent('.dropdown-menu')) {
          element.closest('li.dropdown').addClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded', true)
        }
        callback && callback()
      }
      $active.length && transition ? $active.one('bsTransitionEnd', next).emulateTransitionEnd(Tab.TRANSITION_DURATION) : next()
      $active.removeClass('in')
    }
    // TAB PLUGIN DEFINITION
    // =====================
  function Plugin(option) {
    return this.each(function() {
      var $this = $(this)
      var data = $this.data('bs.tab')
      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }
  var old = $.fn.tab
  $.fn.tab = Plugin
  $.fn.tab.Constructor = Tab
    // TAB NO CONFLICT
    // ===============
  $.fn.tab.noConflict = function() {
      $.fn.tab = old
      return this
    }
    // TAB DATA-API
    // ============
  var clickHandler = function(e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }
  $(document).on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler).on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)
    //});
});
define("kdweb/0.0.1/js/module/cardBank-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  $(".card-bank-item").bind('click', function() {
    $(this).siblings().removeClass("card-bank-active");
    $(this).addClass("card-bank-active");
    $("#payType").val($(this).attr('data-pay-type'));
    $("#cardType").val($(this).attr('data-card-type'));
    $("#bankCode").val($(this).attr('data-bank-code'));
    $("#paymentChannel").val($(this).attr('data-payment-channel'));
    if ($(this).attr("data-agreement") == "true") {
      $("#payAgreementId").val($(this).attr('data-agreement'));
    }
  });
  var showAll = function() {
      $("#creditCardHotTab").css('display', 'none');
      $("#creditCardAllTab").css('display', 'block');
      $("#showAllBtn").css('display', 'none');
    }
    //});
});