define("kdweb/0.0.1/js/page/cart-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  var $ = require("jquery/1.11.1/jquery-debug");
  require("kdweb/0.0.1/js/module/num-debug");
  require("kdweb/0.0.1/js/module/allSelect-debug");
  $(document).ready(function() {
    $('#order-btn').bind('click', function() {
      return false;
    });
    $('input.numed').on('change', function() {
      var obj = this;
      var oldNUmb = $(obj).parent().parent().parent().attr("data-old-numb");
      var newNumb = $(obj).val();
      var skuId = $(obj).parents('tr').attr('data-sku-id');
      var isTrue = false;
      var regu = /^([0-9]+)$/;
      var re = new RegExp(regu);
      if (re.test(newNumb) && Number($(obj).val()) <= ($(obj).closest('tr').attr('data-inventory'))) {
        var params = {
          skuId: skuId,
          amount: newNumb
        };
        $.ajax({
          url: "/cart/update",
          data: params,
          traditional: true,
          success: function(data) {
            isTrue = $(obj).parents('tr').find('.td0').find('.moni-checkbox').hasClass('moni-checkbox-active');
            if (data.errorCode == 200) {
              $(obj).parent().parent().parent().attr("data-old-numb", newNumb);
              numb();
              if (isTrue) {
                updatePrice();
              }
            } else {
              $(obj).val(oldNUmb);
              alert(data.moreInfo);
              return false;
            }
          }
        });
      } else {
        $(obj).val(oldNUmb);
        return false;
      }
    });
    //删除函数
    function del(obj) {
        var parent = $(obj).parent();
        var head = parent.parent();
        //店铺多属商品是否都删除，店铺头信息删除
        $(obj).remove();
        if (parent.children().length == 0) {
          head.remove();
        }
        if ($('.table').length == 0) {
          $('#id-cart').hide();
          $(".cart_no").css('display', 'block');
        }
      }
      //提交删除宝贝itemid
    function getDelJson(obj, itemid) {
        var Id;
        var isTrue = false;
        if (typeof(itemid) == "string") {
          Id = itemid;
          getJson();
        } else {
          for (i in itemid) {
            Id = itemid[i];
            getJson();
          }
        }

        function getJson() {
          var params = {
            itemId: Id
          };
          var time;
          clearTimeout(time);
          $.ajax({
            url: '/cart/delete',
            data: params,
            traditional: true,
            success: function(data) {
              isTrue = $(obj).find('.td0').find('.moni-checkbox').hasClass('moni-checkbox-active');
              if (data.errorCode == 200) {
                del(obj); //删除
                numb(); //计算总数量
                //选中获取价格
                time = setTimeout(function() {
                  if (isTrue) {
                    updatePrice();
                  }
                }, 100);
                disable();
              } else {
                alert(data.moreInfo);
              }
            }
          });
        }
      }
      //单个删除
    $('td.td6').bind('click', function() {
      var itemid = $(this).attr('data-item-id');
      if (confirm('确认要从购物车中删除该商品吗?')) {
        getDelJson($(this).parent(), itemid);
      }
    });
    //批量删除
    $('.del').bind('click', function() {
        numb();
        // var itemid = [];
        var obj;
        if (confirm('确认要从购物车中删除选中商品吗?')) {
          $('.moni-checkbox-active', '.td0').each(function(k, item) { //是否要在这判断是否是head
            /* if($(item).parent().hasClass('.td0')){
                 obj = $(item).parents('thead');
             }else{
                 itemid.push($(item).parents('tr').find('.td6').attr('data-item-id'));
                 obj = $(item).parent().parent();
                 getDelJson(obj, itemid);
             }*/
            // itemid.push($(item).parents('tr').find('.td6').attr('data-item-id'));
            var itemId = $(item).parents('tr').find('.td6').attr('data-item-id');
            obj = $(item).parent().parent();
            getDelJson(obj, itemId);
          })
        }
        //$('.moni-checkbox-active').parents('tr').remove();
      })
      //checkbox
    $('.moni-checkbox').bind('click', function() {
        numb();
        updatePrice();
      })
      //计算总数
    function numb() {
      var total = 0;
      $('.moni-checkbox-active', '.td0').each(function(i, item) {
        total += parseInt($(item).parents('tr').find('.td4').find('.amount').val());
      })
      $("#J-numb").text(total);
    }

    function updatePrice() {
      var skuid = [];
      if ($('.moni-checkbox-active', '.td0').length == 0) {
        $("#J-price").text("0.00");
      } else {
        $('.moni-checkbox-active', '.td0').each(function(i, item) {
          skuid.push($(item).parent().parent().attr('data-sku-id'));
        })
        var params = {
          skuIds: skuid
        };
        $.ajax({
          url: "/cart/pricing",
          data: params,
          traditional: true,
          success: function(data) {
            if (data.errorCode == 200) {
              //totalprice += parseFloat(data.data.totalFee,2);
              $('#J-price').text(Number(data.data.totalFee * 1).toFixed(2));
            } else {
              alert(data.moreInfo);
            }
          }
        });
      }
    }
    $('#order-btn').bind('click', function() {
      var skuid = [];
      var obj = $('.moni-checkbox-active', '.td0');
      var l = obj.length;
      obj.each(function(i, item) {
        sku = "skuId=" + $(item).parent().parent().attr('data-sku-id');
        if (i === l - 1) {
          skuid += sku;
        } else {
          skuid += (sku + "&");
        }
      })
      if ($('input.amount').val() == "") {
        $(".numed").val('1').text('1').trigger('change');
        return;
      }
      if (!skuid.length == 0) {
        location.href = "/xiangqu/web/cart/next" + "?" + skuid;
      } else {
        alert("请选择商品进行结算~");
      }
      return false;
    })

    function disable() {
      if (!$(".table-disable").find('tr').length == 0) {
        //$('.table-disable').find('.moni-checkbox').unmask();
        $('.table-disable').find('.moni-checkbox').each(function(v, obj) {
          $(obj).unbind('click');
        })
        $(".shelves-select").each(function(i, obj) {
          $(obj).find('.amount').attr("disabled", true);
        });
      }
    }
    disable();
  });
});
define("kdweb/0.0.1/js/module/num-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //var $=require('jquery');
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  var numed = $(".numed");
  //数量输入框限定只能输入数字
  numed.on('change', function() {
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);
    var inventory = $(this).closest('tr').attr('data-inventory');
    if (!re.test($(this).val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
    } else if (Number($(this).val()) < 1) {
      numed.val('1').text('1').trigger('change');
    } else if (Number($(this).val()) > Number($(this).closest('tr').attr('data-inventory'))) {
      //alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      $(this).val(inventory).text(inventory).trigger('change');
    }
  });
  $('.numMinus').on('click', function() {
    var self = $(this),
      numedInput = self.next('.numed'),
      edge = self.attr('edge'),
      oldVal = parseInt(numedInput.val()),
      oldVal = isNaN(oldVal) ? 0 : oldVal,
      edge = self.attr('edge'),
      newVal = oldVal - 1;
    if (Number(newVal) < 1) {
      //alert('亲，所选数量不能小于1件哦~');
      numedInput.val(edge).text(edge).trigger('change');
    } else {
      numedInput.val(oldVal - 1).text(oldVal - 1).trigger('change');
    }
    return false;
  });
  $('.numPlus').on('click', function() {
    var self = $(this),
      numedInput = self.prev('.numed'),
      edge = self.attr('edge'),
      oldVal = parseInt(numedInput.val()),
      oldVal = isNaN(oldVal) ? 0 : oldVal,
      newVal = oldVal + 1;
    var inventory = $(this).closest('tr').attr('data-inventory');
    if (Number(newVal) > Number(inventory)) {
      //alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      numedInput.val(inventory).text(inventory).trigger('change');
    } else {
      numedInput.val(newVal).text(newVal).trigger('change');
    }
    return false;
  });
  /*  function calculate(obj){
        var self = $(obj), numed = self.parent().children('.numed'),
            delta = self.hasClass('numInc') ? 1 : -1, edge = self.attr('edge'),
            newVal = oldVal + delta;

        */
  /*  if (!!edge && (delta > 0 && newVal > edge || delta < 0 && newVal < edge)) {
                newVal = edge;
            }*/
  /*
      }*/
  //});
});
define("kdweb/0.0.1/js/module/allSelect-debug", ["jquery/1.11.1/jquery-debug"], function(require, exports, module) {
  //var $=require('jquery');
  //define(function(require) {
  var $ = require("jquery/1.11.1/jquery-debug");
  //var $ = require('jquery/1.11.1/jquery.js');
  var moniCheckbox = $(".moni-checkbox");
  var allSelect = $(".allSelect");
  allSelect.unbind('click');
  moniCheckbox.unbind('click');
  //模拟多选框
  function moniSelect(obj, theadSel) {
      if ($(obj).hasClass("moni-checkbox-active")) {
        $(obj).removeClass("moni-checkbox-active");
        //店铺内产品列表非全选中情况，列表头部设置非选
        $(theadSel).removeClass("moni-checkbox-active");
        $(".allSelect").find(".moni-checkbox").removeClass("moni-checkbox-active");
      } else {
        $(obj).addClass("moni-checkbox-active");
      }
    }
    //店铺产品列表是否都选择，初始店铺多选框
  function shopAll(obj, tbodySel) {
    var activeSelect = $(obj).parents('.table-able').find('tbody').find(".moni-checkbox-active");
    if (tbodySel.length === activeSelect.length) {
      return true;
    } else {
      return false;
    }
  }
  moniCheckbox.bind('click', function() {
      var tbodySel = $(this).parents('.table-able').find('tbody').find(".moni-checkbox");
      var theadSel = $(this).parents('.table-able').find('thead').find('.moni-checkbox');
      //全选
      if ($(this).parent().hasClass("allSelect")) {
        if ($(this).hasClass("moni-checkbox-active")) {
          $(moniCheckbox).removeClass("moni-checkbox-active");
        } else {
          $(moniCheckbox).addClass("moni-checkbox-active");
        } //店铺列表选择
      } else if ($(this).parent().hasClass("shopSelect")) {
        if ($(this).hasClass("moni-checkbox-active")) {
          moniSelect(this, theadSel);
          tbodySel.removeClass("moni-checkbox-active");
        } else {
          moniSelect(this, theadSel);
          tbodySel.addClass("moni-checkbox-active");
        }
      } else { //一般单个选择
        moniSelect(this, theadSel);
      }
      //店铺内产品列表全选中情况，列表头部也选中
      if (shopAll(this, tbodySel)) {
        theadSel.addClass("moni-checkbox-active");
      }
      // 店铺列表都没选的情况，列表头部设置非选
      if (!tbodySel.hasClass("moni-checkbox-active")) {
        theadSel.removeClass("moni-checkbox-active");
      }
    })
    //});
});