//请求地址
// const host = "https://uathwapi.handeson.com/";
// const host = "https://stghwapi.handeson.com/";
const host = "https://devhwsc.handeson.com/";
// const host = "https://hwapi.handeson.com/";

//普通的请求方法 
function normalAjax(api, comeback) {
	if (data == "") {
		data = {}
	}
	$.ajax({
		type: "get",
		dataType: "json",
		url: host + api,
		success: function(response) {
			if (response.success == true) {
				comeback(response)
			} else {
				alert(response.message)
			}
		}
	});
}

//获取URL参数
function UrlSearch() {
	var name, value;
	var str = location.href; //取得整个地址栏
	var num = str.indexOf("?")
	str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]

	var arr = str.split("&"); //各个参数放到数组里
	for (var i = 0; i < arr.length; i++) {
		num = arr[i].indexOf("=");
		if (num > 0) {
			name = arr[i].substring(0, num);
			value = arr[i].substr(num + 1);
			this[name] = value;
		}
	}
}
var Request = new UrlSearch(); //实例化

//文字无缝滚动
var speed = 60;
var slide = document.getElementById("slide");
var slide2 = document.getElementById("slide2");
var slide1 = document.getElementById("slide1");
slide2.innerHTML = slide1.innerHTML;

function Marquee() {
	if (slide2.offsetTop - slide.scrollTop <= 0)
		slide.scrollTop -= slide1.offsetHeight
	else {
		slide.scrollTop++
	}
}

var MyMar = setInterval(Marquee, speed);

//倒计时  
setTimer('1548950400000');
var countDown = {};

function setTimer(end) {
	var totalSecond = end / 1000 - Date.parse(new Date()) / 1000;

	clearInterval(setTime);

	var setTime = setInterval(function() {
		if (totalSecond <= 0) {
			clearInterval(setTime);
		}
		// 秒数
		var second = totalSecond;

		// 天数位
		var day = Math.floor(second / 3600 / 24);
		var dayStr = day.toString();
		if (dayStr.length == 1) dayStr = "0" + dayStr;

		// 小时位
		var hr = Math.floor((second - day * 3600 * 24) / 3600);
		var hrStr = hr.toString();
		if (hrStr.length == 1) hrStr = "0" + hrStr;

		// 分钟位
		var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
		var minStr = min.toString();
		if (minStr.length == 1) minStr = "0" + minStr;

		// 秒位
		var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
		var secStr = sec.toString();
		if (secStr.length == 1) secStr = "0" + secStr;

		// this.countDown.day = dayStr;
		var hours = (day * 24 + hr).toString();
		if (hours.length == 1) hours = "0" + hours;

		countDown.hour0 = hours[0]; //hrStr;
		countDown.hour1 = hours[1]; //hrStr;

		// this.countDown.hour = (day * 24 + hr).toString(); //hrStr;
		this.countDown.minute0 = minStr[0];
		this.countDown.minute1 = minStr[1];
		this.countDown.second0 = secStr[0];
		this.countDown.second1 = secStr[1];
		var str = '<span>' + this.countDown.hour0 + '</span><span>' + this.countDown.hour1 + '</span>时<span>' + this.countDown
			.minute0 + '</span><span>' + this.countDown.minute1 + '</span>分<span>' + this.countDown.second0 + '</span><span>' +
			this.countDown.second1 + '</span>秒';
		//红包雨倒计时
		$('.timer_box').html(str);

		totalSecond--;
	}, 1000);

}

//获取相差天数
//模拟时间2.1-2.12  当前2.3
differdays('1548950400000', '1549900800000'); //开始到结束时间
function differdays(startdate, enddate, nowData) {
	var nowData = '1549123200000'; //当前时间 
	var differdays = (enddate - startdate) / 86400000;
	var differdays1 = (nowData - startdate) / 86400000;
	var lineWidth = $('.timing').width();
	var lineWidth1 = (12 / lineWidth) * 100; //背景图偏移量	
	var days = parseFloat((lineWidth / differdays * differdays1) / lineWidth * 100 - lineWidth1).toFixed(2);
	console.log(days, 'days');
	$('.line_bg').css('width', days + '%');

}

//时间戳转换
function formatTime(date, format) {
	var formateArr = ['yyyy', 'MM', 'dd', 'hh', 'mm', 'ss'];
	var returnArr = [];

	var date = new Date(date);
	returnArr.push(date.getFullYear());
	returnArr.push(Number(date.getMonth() + 1));
	returnArr.push(Number(date.getDate()));

	returnArr.push(Number(date.getHours()));
	returnArr.push(Number(date.getMinutes()));
	returnArr.push(Number(date.getSeconds()));

	for (var i in returnArr) {
		format = format.replace(formateArr[i], returnArr[i]);
	}
	return format;
}

//带Cookie 的请求 
var request = parameter => {
	//url必填项
	if (!parameter || parameter == {} || !parameter.url) {
		console.log('Data request can not be executed without URL.');
		return false;
	} else {
		var murl = parameter.url;
		var headerCookie = wx.getStorageSync('cookie');
		//判断是否有独自cookie请求
		var selfCookie = parameter.selfCookie;
		selfCookie && (headerCookie += selfCookie);
		wx.request({
			url: murl,
			data: parameter.data || {},
			header: {
				// 'Content-Type': 'application/x-www-form-urlencoded',
				'Cookie': headerCookie
			},
			method: parameter.method || 'POST',
			success: function(res) {
				parameter.success && parameter.success(res);

			},
			fail: function(e) {
				parameter.fail && parameter.fail(e);
				// console.log(e.errMsg);
				wx.showToast({
					title: '网络信号较差',
					icon: 'loading',
					duration: 3000
				});
			},
			complete: function() {
				parameter.complete && parameter.complete();
			}
		});
	}

}

//检测是否是微信小程序环境
function isMiniProgram() {
	var ua = navigator.userAgent.toLowerCase();
	var envType = '#';

	if (ua.match(/MicroMessenger/i) == 'micromessenger') { //微信环境
		wx.miniProgram.getEnv(function(res) {
			if (res.miniprogram) { // 小程序环境下逻辑
				envType = true;
			} else { //非小程序环境下逻辑
				envType = false;
			}
		})
	} else { //非微信环境逻辑
		envType = false;
	}
	return envType
}