const $cardBox = $('.id_card_shadow');
const $cardInputBox = $('#card_input_box');
const $cardSuccessBox = $('#card_success_box');
const $saveCardBtn = $('.saveBtn');
const $certificateId = $('#certificateId');

const regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

var cartCheckbox = $(".cart-checkbox");
var cartAllSelect = $(".allSelect");
cartCheckbox.unbind('click');
cartAllSelect.unbind('click');

function logisticsClick(target) {
  if (target == 'logistics') {
    $('#logistics').attr('class', 'icon-circle')
    $('#ispickup').attr('class', 'icon-circle-blank')
    $("#addressInfo").show();
  } else if (target == 'ispickup') {
    $('#ispickup').attr('class', 'icon-circle')
    $('#logistics').attr('class', 'icon-circle-blank')
    $("#addressInfo").hide();
  }
}

//模拟多选框
function checkSelect(obj, shopSel) {
  if ($(obj).hasClass("cart-checkbox-checked")) {
    $(obj).removeClass("cart-checkbox-checked");
    //店铺内产品列表非全选中情况，列表头部设置非选
    $(shopSel).removeClass("cart-checkbox-checked");
    $(".allSelect").find(".cart-checkbox").removeClass("cart-checkbox-checked");
  } else {
    $(obj).addClass("cart-checkbox-checked");
  }
}

//店铺产品列表是否都选择，初始店铺多选框
function shopAll(obj, itemSelVec) {
  var activeSelect = $(obj).parents('.order_list').find(
    '.order-bar-title').find(".cart-checkbox-checked");
  if (itemSelVec.length === activeSelect.length) {
    return true;
  } else {
    return false;
  }
}

/* 关闭身份证弹框 */
$('.card_circle').on('click', function () {
  $cardBox.hide();
});

/* 保存身份证信息 */
$saveCardBtn.on('click', function () {
  var inputCard = $('#inputCard').val();
  if (!regIdCard.test(inputCard)) {
    alert('请填写正确的身份证号码');
    return;
  }
  $certificateId.val(inputCard);
  var addressId = $("#addressId").val();
  if (addressId) {
    // 更新该收货地址对应的身份证号
    $.ajax({
      async: false,
      data: {certificateId: inputCard},
      type: "POST",
      url: '/v2/address/' + addressId + '/update',
      dataType: 'json',
      success: function (data) {
        var $cert = $('#certificateId_view');
        $cert.text('身份证：' + inputCard);
        $cert.removeClass('hidden');
        $(this).hide();
        $cardSuccessBox.show();
      }
    });
  }

});

//模拟多选框
cartCheckbox.bind('click', function () {
  //全选
  if ($(this).parent().hasClass("allSelect")) {   // 购物车全选
    if ($(this).hasClass("cart-checkbox-checked")) {
      $(cartCheckbox).removeClass("cart-checkbox-checked");
    } else {
      $(cartCheckbox).addClass("cart-checkbox-checked");
    }//店铺列表选择
  } else {
    var itemSelVec = $(this).parents('.order_list').find(
      '.order-bar-title').find(".cart-checkbox");
    var shopCheckId = '#shop-' + $(this).attr('shopId');
    var shopSel = $(shopCheckId);

    if ($(this).attr('shopId') != null) {       // 一般单个选择
      checkSelect(this, shopSel);

      //店铺内产品列表全选中情况，列表头部也选中
      if (shopAll(this, itemSelVec)) {
        shopSel.addClass("cart-checkbox-checked");
      }
      // 店铺列表都没选的情况，列表头部设置非选
      if (!itemSelVec.hasClass("cart-checkbox-checked")) {
        shopSel.removeClass("cart-checkbox-checked");
      }
    } else {                                    // 店铺选择
      // @this is shop selector,shopSel is empty
      if ($(this).hasClass("cart-checkbox-checked")) {
        checkSelect(this, shopSel);
        itemSelVec.removeClass("cart-checkbox-checked");
      } else {
        checkSelect(this, shopSel);
        itemSelVec.addClass("cart-checkbox-checked");
      }
    }
  }
})

$(document).ready(function () {
  var buyHref = "";
  var cart = new Cart();
  // cart.initProvinces();

  //商品数量修改+页面数据 初始化
  pageRequestSet();

  function pageRequestSet() {
    if (!request('qty')) {
      $('#qty').val('0');
    } else {
      $('#qty').val(request('qty'));
    }
  }

  //用户选择地址列表
  $('.js-change-address').on('click', function () {
    $(".addrls-help-info").css("display", "none");
    $("#addressListModal").modal('show');
  });
  $('.add-address').on('click', function () {
    $(".addrls-help-info").css("display", "none");
    $("#addressListModal").modal('show');
  });

  $('input.amount').on('change', function () {
    var obj = this;
    var oldNUmb = $(obj).parents('.cart-product-info').attr("data-old-numb");
    var newNumb = $(obj).val();
    var skuId = $(obj).parents('.cart-product-info').attr('data-sku-id');
    var isTrue = false;
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);

    if (re.test(newNumb) && Number($(obj).val()) <= ($(obj).closest(
      '.cart-product-info').attr('data-inventory'))) {
      var params = {
        skuId: skuId,
        amount: newNumb
      };

      af.ajax({
        url: "/cart/update",
        data: params,
        traditional: true,
        success: function (data) {
          ajaxobj = eval("(" + data + ")");
          if (ajaxobj.errorCode == 200) {
            $(obj).parents('.cart-product-info').attr("data-old-numb", newNumb);

            var itemSelId = '#itemsel-' + $(obj).attr('itemId');
            var itemSel = $(itemSelId);
            if ($(itemSel).hasClass("cart-checkbox-checked")) {
              isTrue = true;
            }

            numb();

            if (isTrue) {
              updatePrice(function () {
                popInfoTip('修改成功', 200)
              });
            } else {
              popInfoTip('修改成功', 200)
            }
          } else {
            $(obj).val(oldNUmb);
            alert(ajaxobj.moreInfo);
            return false;
          }
        }
      });
    } else {
      $(obj).val(oldNUmb);
      return false;
    }

  });

  //删除函数
  function del(obj) {
    var parent = $(obj).parent();
    var head = parent;
    //店铺多属商品是否都删除，店铺头信息删除
    $(obj).remove();
    if (parent.children().length == 1) {
      head.remove();
    }
    if ($('.order_list').length == 0) {
      $('#id-cart').hide();
      $('.bottom-bar').hide();
      $(".cart_no").css('display', 'block');
    }
  }

  //提交删除宝贝itemid
  function getDelJson(obj, itemid, callback) {
    var Id;
    var len = 0, num = 0;
    var isTrue = false;
    if (typeof(itemid) == "string") {
      Id = itemid;
      getJson();
    } else {
      for (i in itemid) {
        len++;
        Id = itemid[i];
        getJson();
      }
    }

    function getJson() {
      var params = {
        itemId: Id
      };
      af.getJSON('/cart/delete', params, function (data) {
        if (data.errorCode == 200) {
          num++;
          var itemRawId = $(obj).attr('id').substr(5);
          var itemSelId = '#itemsel-' + itemRawId;
          var itemSel = $(itemSelId);
          if ($(itemSel).hasClass("cart-checkbox-checked")) {
            isTrue = true;
          }

          del(obj);//删除
          numb();//计算总数量

          if (isTrue && len === num) {
            updatePrice();
          }

          disable();
          if (typeof callback === 'function') {
            callback();
          }
        } else {
          alert(data.moreInfo);
        }
      });
    }
  }

  function popInfoTip(title, t) {
    var options = {
      'animation': true,
      'container': 'body',
      'template': '<div class="popover" role="tooltip"><div class="popover-content"></div></div>',
      'content': function () {
        return '<div class="self-pop-wrap" style="width:' + innerWidth
          + 'px;height:' + innerHeight + 'px">' +
          '<div class="self-pop-info">' + (title ? title : '') + '</div></div>'
      },
      'html': true,
      'viewport': {selector: 'body', padding: 0}
    }
    $('body').popover(options);
    $('body').popover('show');
    setTimeout(function () {
      $('body').popover('destroy')
    }, t);
  }

  //单个删除
  $('.item-del-btn').bind('click', function () {
    var itemId = $(this).attr('data-item-id');
    if (confirm('确认要从购物车中删除该商品吗?')) {
      var itemLineId = '#item-' + itemId;
      var itemLine = $(itemLineId);
      getDelJson($(itemLine), itemId, function () {
        popInfoTip('删除成功', 800);
      });
    }
  });

  //批量删除
  $('.del').bind('click', function () {
    numb();
    var obj;
    if (confirm('确认要从购物车中删除所有选中商品吗?')) {
      var cartProdInfoItemVec = $('.cart-product-info');//.attr('data-sku-id');
      cartProdInfoItemVec.each(function (i, item) {
        var cartItem = $(item).parent().parent();
        var itemRawId = cartItem.attr('id').substr(5);
        var itemSelId = '#itemsel-' + itemRawId;
        var itemSel = $(itemSelId);
        if ($(itemSel).hasClass("cart-checkbox-checked")) {
          var itemLineId = '#item-' + itemRawId;
          var itemLine = $(itemLineId);

          getDelJson($(itemLine), itemRawId, function () {
            popInfoTip('删除成功', 800);
          });
        }
      })
    }
  })

  //checkbox
  $('.cart-checkbox').bind('click', function () {
    numb();
    updatePrice(function () {
      popInfoTip('添加成功', 200)
    });
  })

  // coupon select
  $('.shop-coupon-select').bind('change', function () {

    numb();
    updatePrice(function () {
      popInfoTip('修改成功', 200)
    });

  })

  // coupon select
  $('.shop-coupon-confirm-select').bind('change', function () {
    var shopCouponSelItemId = $(this).attr('id');
    var shopId = shopCouponSelItemId.substr(20);
    var shopCouponInputId = "shop-coupon-" + shopId;
    var shopCouponInput = $("#" + shopCouponInputId);
    shopCouponInput[0].value = $(this).val();
    updateConfirmPrice();
  })

  $('.cart-checkbox').bind('click', function () {

    numb();
    updatePrice(function () {
      popInfoTip('修改成功', 200)
    });
  })

  //计算选中的商品总数
  function numb() {
    var total = 0;
    var tdNumVec = $('.numed.amount');
    tdNumVec.each(function (i, item) {
      var itemSelId = '#itemsel-' + $(item).attr('itemId');
      var itemSel = $(itemSelId);
      if ($(itemSel).hasClass("cart-checkbox-checked")) {
        total += parseInt($(item).val());
      }
    })
    $("#J-numb").text(total);
    $(".quantitySum").text(total);
  }

  // 清空价格汇总显示
  function clearPriceView() {
    $('#cartGoodsFee').text('  0');
    // $('#cartLogisticsFee').text('  运费:0');
    $('#cartDiscountFee').text('  折扣:0');
    $('#cartTotalFee').text('  应付:0');
  }

  // 清空所有店铺优惠券选项
  function clearShopCouponOptions() {
    var shopCouponSelVec = $('.shop-coupon-select');
    $(shopCouponSelVec).empty();

    var shopCouponSelDivVec = $('.shop-coupon-select-div');
    shopCouponSelDivVec.each(function (i, item) {
      item.style.visibility = "hidden";
    })
  }

  function showShopCouponSelect(shopId, show) {
    var shopCouponSelDivId = '#shop-coupon-div-' + shopId;
    var shopCouponSelDiv = $(shopCouponSelDivId);
    shopCouponSelDiv.each(function (i, item) {
      if (show) {
        item.style.visibility = "visible";
      } else {
        item.style.visibility = "hidden";
      }
    })
  }

  function updatePrice(callback) {

    var skuid = [];

    var itemSelVecVec = $('.order-bar-title');

    if ($('.cart-checkbox-checked', itemSelVecVec).length == 0) {
      clearPriceView();
      clearShopCouponOptions();
    } else {
      var cartProdInfoItemVec = $('.cart-product-info');//.attr('data-sku-id');
      cartProdInfoItemVec.each(function (i, item) {
        var cartItem = $(item).parent().parent();
        var itemRawId = cartItem.attr('id').substr(5);
        var itemSelId = '#itemsel-' + itemRawId;
        var itemSel = $(itemSelId);
        if ($(itemSel).hasClass("cart-checkbox-checked")) {
          skuid.push($(item).attr('data-sku-id'));
        }
      });

      var shopCouponMap = new Object();
      var shopCouponSelVec = $(".shop-coupon-select");
      shopCouponSelVec.each(function (i, item) {
        var shopCouponSelItemId = $(item).attr('id');
        var shopId = shopCouponSelItemId.substr(12);
        if (item.selectedIndex != -1) {
          var selShopCouponId = $(item).val();
          // alert(selShopCouponId);
          shopCouponMap[shopId] = selShopCouponId;
        }
      });
      var qty = $('#qty').val();
      var params = {
        skuIds: skuid,
        shopCoupons: shopCouponMap,
        qty: qty
      };

      var url = "/cart/pricing",
        pdata = params;
      af.post(url, pdata, function (data) {
        if (data.errorCode == 200) {
          // 购物车价格汇总
          var goodsFee = data.data.totalPricingResult.goodsFee;
          var logisticsFee = data.data.totalPricingResult.logisticsFee;
          var discountFee = data.data.totalPricingResult.discountFee;
          var totalFee = data.data.totalPricingResult.totalFee;

          //totalprice += parseFloat(data.data.totalFee,2);
          $('#cartGoodsFee').text(Number(goodsFee * 1).toFixed(2));
          // $('#cartLogisticsFee').text('  运费:' + Number(logisticsFee * 1).toFixed(2));
          $('#cartDiscountFee').text('  折扣:' + Number(discountFee * 1).toFixed(
            2));
          $('#cartTotalFee').text('  应付:' + Number(totalFee * 1).toFixed(2));

          // 店铺折扣

          // 店铺优惠
          clearShopCouponOptions();

          var shopCoupons = data.data.shopCoupons;
          for (var key in shopCoupons) {
            var shopId = key;
            var couponVec = shopCoupons[key];
            var shopCouponSelId = '#shop-coupon-' + shopId;
            var shopCouponSel = $(shopCouponSelId);
            if ($(shopCouponSel).hasClass("shop-coupon-select")) {
              if (couponVec.length <= 1) {
                showShopCouponSelect(shopId, false);
              } else {
                showShopCouponSelect(shopId, true);
                for (var i = 0; i < couponVec.length; i++) {
                  var couponItem = couponVec[i];
                  var op = document.createElement("option");
                  op.value = couponItem.id;
                  if (couponItem.discount > 0) {
                    op.text = "省" + couponItem.discount + "元:店铺优惠券>";
                  } else {
                    op.text = "不使用优惠券";
                  }
                  $(shopCouponSel).append(op);
                  // $(shopCouponSel).append("<option value='" + couponItem.couponId +  "'>" + couponItem.discount + "</option>");
                }
              }
            }
          }

          // 单品折扣

          // 单品优惠

          // 店铺价格汇总

          if (typeof callback === 'function') {
            callback()
          }
        } else {
          alert(data.moreInfo);
        }
      }, 'json');
    }
  }

  // for cart next page
  function updateConfirmPrice() {
    var inputs = document.getElementsByTagName('input');
    var skuIds = new Array();
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].name == 'skuIds') {
        skuIds.push(inputs[i].value);
      }
    }

    var shopCouponMap = new Object();
    var shopCouponSelVec = $(".shop-coupon-confirm-select");
    shopCouponSelVec.each(function (i, item) {
      var shopCouponSelItemId = $(item).attr('id');
      var shopId = shopCouponSelItemId.substr(20);
      if (item.selectedIndex != -1) {
        var selShopCouponId = $(item).val();
        shopCouponMap[shopId] = selShopCouponId;
      }
    })
    var addressId = $('#addressId').val();
    var qty = $('#qty').val();
    var params = {
      skuIds: skuIds,
      shopCoupons: shopCouponMap,
      qty: qty,
      addressId: addressId
    };

    var url = "/cart/pricing",
      pdata = params;
    af.post(url, pdata, function (data) {
      if (data.errorCode == 200) {
        // 购物车价格汇总
        var goodsFee = data.data.totalPricingResult.goodsFee;
        var logisticsFee = data.data.totalPricingResult.logisticsFee;
        var discountFee = data.data.totalPricingResult.discountFee;
        var totalFee = data.data.totalPricingResult.totalFee;

        //totalprice += parseFloat(data.data.totalFee,2);
        $('#goodsFee').text(Number(goodsFee * 1).toFixed(2));
        $('#logisticsFee').text(Number(logisticsFee * 1).toFixed(2));
        $('#discountFee').text(Number(discountFee * 1).toFixed(2));
        $('#totalFee').text(Number(totalFee * 1).toFixed(2));

        // 店铺折扣

        // 店铺优惠
        clearShopCouponOptions();

        var shopCoupons = data.data.shopCoupons;
        for (var key in shopCoupons) {
          var shopId = key;
          var couponVec = shopCoupons[key];
          var shopCouponSelId = '#shop-coupon-' + shopId;
          var shopCouponSel = $(shopCouponSelId);
          if ($(shopCouponSel).hasClass("shop-coupon-select")) {
            if (couponVec.length <= 1) {
              showShopCouponSelect(shopId, false);
            } else {
              showShopCouponSelect(shopId, true);
              for (var i = 0; i < couponVec.length; i++) {
                var couponItem = couponVec[i];
                var op = document.createElement("option");
                op.value = couponItem.id;
                if (couponItem.discount > 0) {
                  op.text = "优惠券-" + couponItem.id + ":" + couponItem.discount;
                } else {
                  op.text = "不使用优惠券";
                }
                $(shopCouponSel).append(op);
                // $(shopCouponSel).append("<option value='" + couponItem.couponId +  "'>" + couponItem.discount + "</option>");
              }
            }
          }
        }

        // 单品折扣

        // 单品优惠

        // 店铺价格汇总
        var shopPricingResult = data.data.shopPricingResult;
        for (var key in shopCoupons) {
          var shopId = key;
          var shopPricingResultItem = shopPricingResult[key];

          var shopDiscountFeeId = '#shop-discountfee-' + shopId;
          var shopDiscountFee = $(shopDiscountFeeId);
          var shopDiscountFeeEm = $(shopDiscountFee).find(
            '.footer-youfei-price')[0];
          if (shopDiscountFeeEm) {
            shopDiscountFeeEm.innerText = "￥"
              + Number(shopPricingResultItem.discountFee * 1).toFixed(2);
          }

          var shopLogisticsFeeId = '#shop-logisticsfee-' + shopId;
          var shopLogisticsFee = $(shopLogisticsFeeId);
          var shopLogisticsFeeEm = $(shopLogisticsFee).find(
            '.footer-youfei-price')[0];
          if (shopLogisticsFeeEm) {
            shopLogisticsFeeEm.innerText = "￥"
              + Number(shopPricingResultItem.logisticsFee * 1).toFixed(2);
          }

          var shopGoodsFeeId = '#shop-goodsfee-' + shopId;
          var shopGoodsFee = $(shopGoodsFeeId);
          var shopGoodsFeeEm = $(shopGoodsFee).find('.footer-youfei-price')[0];
          if (shopGoodsFeeEm) {
            shopGoodsFeeEm.innerText = "￥"
              + Number(shopPricingResultItem.goodsFee * 1).toFixed(2);
          }

          var shopTotalFeeId = '#shop-totalfee-' + shopId;
          var shopTotalFee = $(shopTotalFeeId);
          var shopTotalFeeEm = $(shopTotalFee).find('.footer-youfei-price')[0];
          if (shopTotalFeeEm) {
            shopTotalFeeEm.innerText = "￥"
              + Number(shopPricingResultItem.totalFee * 1).toFixed(2);
          }

          var orderPrice = $(".order-price");
          if (orderPrice) {
            orderPrice.html("￥" + Number(shopPricingResultItem.totalFee
              * 1).toFixed(2));
          }

        }

      } else {
        alert(data.moreInfo);
      }
    }, 'json');
  }

  $('#order-btn').bind('click', function () {
    /*
            //var $form = $('#id-cart');
            var form    = $('#id-cart'),
                url     = "/cart/pricing",//form.attr('action'),
                pdata   = form.serialize();
            af.post(url, pdata, function(r) {
                showResponse(r);
            }, 'json');
    */

    var nextUrl = $(this).attr('nextUrl');
    //MemberSignin.login.checkSignined(nextUrl,function(){

    var shopId = $(this).attr('shopId');
    var skuid = [];

    var cartProdInfoItem = $('.cart-product-info');//.attr('data-sku-id');
    cartProdInfoItem.each(function (i, item) {
      var cartItem = $(item).parent().parent();
      var itemRawId = cartItem.attr('id').substr(5);
      var itemSelId = '#itemsel-' + itemRawId;
      var itemSel = $(itemSelId);
      if ($(itemSel).hasClass("cart-checkbox-checked")) {
        skuid.push($(item).attr('data-sku-id'));
      }
    })

    if ($('input.amount').val() == "") {
      $(".numed").val('1').text('1').trigger('change');
      return;
    }
    if (!skuid.length == 0) {
      var shopCouponMap = new Object();
      var shopCouponSelVec = $(".shop-coupon-select");
      shopCouponSelVec.each(function (i, item) {
        var shopCouponSelItemId = $(item).attr('id');
        var shopId = shopCouponSelItemId.substr(12);
        if (item.selectedIndex != -1) {
          var selShopCouponId = $(item).val();
          shopCouponMap[shopId] = selShopCouponId;
        }
      })

      var params = {
        skuIds: skuid,
        shopCoupons: shopCouponMap
      };

      var form1 = document.createElement("form");
      form1.id = "form1";
      form1.name = "form1";

      // 添加到 body 中
      document.body.appendChild(form1);

      // 创建一个输入
      var input1 = document.createElement("input");
      // 设置相应参数
      input1.type = "hidden";
      input1.name = "skuId";
      input1.value = skuid;

      // 将该输入框插入到 form 中
      form1.appendChild(input1);

      /* shopId is contain in form action
       // 创建一个输入
       var input2 = document.createElement("input");
       // 设置相应参数
       input2.type = "text";
       input2.name = "shopId";
       input2.value = shopId;

       // 将该输入框插入到 form 中
       form1.appendChild(input2);
       */

      for (var key in shopCouponMap) {
        var shopId = key;
        var shopCouponId = shopCouponMap[key];

        //<input name="users['x'].firstName" value="aaa" />
        // 创建一个输入
        var input3 = document.createElement("input");
        // 设置相应参数
        input3.type = "text";
        input3.name = "shopCoupons[" + shopId + "]";
        input3.value = shopCouponId;

        // 将该输入框插入到 form 中
        form1.appendChild(input3);
      }

      // form 的提交方式
      form1.method = "POST";
      // form 提交路径
      form1.action = nextUrl;

      // 对该 form 执行提交
      form1.submit();
      // 删除该 form
      // document.body.removeChild(form1);
    } else {
      alert("请选择商品进行结算~");
    }

    //});
    return false;
  })

  $('#edit-address-btn').bind('click', function () {
    var addressId = $(this).attr('addressId');
    $('#order_submit_form')[0].method = "POST";
    // form 提交路径
    $('#order_submit_form')[0].action = "/address/" + addressId + "/edit";

    // 对该 form 执行提交
    $('#order_submit_form')[0].submit();
  })

  $('#add-address-btn').bind('click', function () {
    $('#order_submit_form')[0].method = "POST";
    // form 提交路径
    $('#order_submit_form')[0].action = "/address/add";

    // 对该 form 执行提交
    $('#order_submit_form')[0].submit();
  })

  function disable() {
    if (!$(".table-disable").find('tr').length == 0) {
      //$('.table-disable').find('.cart-checkbox').unmask();
      $('.table-disable').find('.cart-checkbox').each(function (v, obj) {
        $(obj).unbind('click');
      })
      $(".shelves-select").each(function (i, obj) {
        $(obj).find('.amount').attr("disabled", true);
      });
    }
  }

  //没找到ID为checkout-btn，TODO 要删除?
  $('#checkout-btn').bind('click', function () {
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);
    var numed = $(".numed");
    var edge = $(".numPlus").attr("edge");
    if ($('input.amount').val() === "") {
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (!re.test(numed.val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) < 1) {
      alert('请输入大于1的商品数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) > edge) {
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      numed.val(edge).text(edge).trigger('change');
      return false;
    }
    var params = {
      skuId: $('#skuId').val(),
      shopId: $('#shopId').val()
    };
    af.getJSON('/cart/validate', params, function (data) {
      if (data.data) {
        location.href = $('#checkout-btn').attr('href');
      } else {
        alert(data.moreInfo);
      }
    });
    return false;
  });

  var paysubmit = function () {
    //$('#isdanbao').val($('#danbao').hasClass('pay-selected'));
    if (!$('#ispickup').prop('checked') && ($('#addressId').length == 0 || $(
      '#addressId').val() == '')) {
      alert('亲，地址不能为空，请添加地址！');
      return false;
    }
    var rc = new RegExp(regIdCard);
    if ($("#isCrossBorder").val() === '1') {
      var certificateId = $certificateId.val() || '';
      if (!rc.test(certificateId)) {
        $('#inputCard').val('');
        $cardInputBox.show();
        return;
      }
    }
    var skuId = $('#skuId').val();
    var qty = $('#qty').val();
    var productId = $('#productId').val();
    var addressId = $('#addressId').val();
    var promotionFrom = $('#promotionFrom').val();
    var nextUrl = '/order/submit?skuId=' + skuId + '&productId=' + productId
      + '&addressId=' + addressId + '&qty=' + qty;
    if (promotionFrom && promotionFrom !== '') {
      nextUrl += '&promotionFrom=' + promotionFrom
    }
    location.href = nextUrl;
  };

  $('#submitBtn').bind('click', function () {
    // alert($('#certificateId').val() || '无');
    var totalFee = $("#priceTotalFee").val();
    var edge = Number($(".numPlus").attr("edge"));
    var amount = $(".amount");
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);

    if ($('#pay-danbao').length > 0 && !$('#danbao').hasClass('pay-selected')) {
      $('#js-layer').css('display', 'block');
      $('#js-body').css('display', 'block');
      return false;
    }
    if (Number(amount.val()) > edge) {
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      amount.val(edge).text(edge).trigger('change');
      return false;
    } else if (!re.test(amount.val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
      return false;
    }
    paysubmit();
  });

  $('.l-next').click(function () {  //担保交易  继续按钮
    $('#js-layer').css('display', 'none');
    $('#js-body').css('display', 'none');
    paysubmit();
  });

  var ckeckallBtn = cartAllSelect.find('.cart-checkbox');
  ckeckallBtn.trigger('click');
});

Cart.prototype = {
  addToCart: function () {
    var sellerId = $('input[name=sellerId]').val();
    var productId = $('input[name=productId]').val();
    var skuId = $('input[name=skuId]').val();
    var amount = $('input[name=amount]').val();
    var params = {
      'sellerId': sellerId,
      'productId': productId,
      'skuId': skuId,
      'amount': amount
    };
    var url = '/cart/add';
    af.post(url, params, function (data) {
//            console.log(log);
    });
  },
  // TODO 此方法address.js里面有重复，要重构
  updatePrice: function () {
    //var zoneId = $('#district').val() != '0' ? $('#district').val() : $('#city').val();
    var zoneId = $('#district').val() != '0' ? $('#district').val() : ($(
      '#city').val() != '0' ? $('#city').val() : $('#province').val());
    var promotionId = $('[name=promotionId]:checked').length > 0 ? $(
      '[name=promotionId]:checked').val() : '';
    var couponId = $('[name=couponId]:checked').length > 0 ? $(
      '[name=couponId]:checked').val() : '';
    var activityGrouponId = $('#activityGrouponId').val();
    if (!activityGrouponId || typeof activityGrouponId !== 'undefined') {
      activityGrouponId = '';
    }
    var params = {
      skuIds: $('#skuId').val(),
      shopId: $('#shopId').val(),
      zoneId: zoneId,
      promotionId: promotionId,
      activityGrouponId: activityGrouponId,
      couponId: couponId
    };
    af.getJSON('/cart/pricing', params, function (data) {
      if (data.data) {
        var prices = data.data;
        $("#goodsFee").text(
          $("#goodsFee").text().replace(/\d+[\.\d,]*/g, prices.goodsFee));
        $("#logisticsFee").text(
          $("#logisticsFee").text().replace(/\d+[\.\d,]*/g,
            prices.logisticsFee));
        $("#discountFee").text(
          $("#discountFee").text().replace(/\d+[\.\d,]*/g, prices.discountFee));
        $("#totalFee").text(
          $("#totalFee").text().replace(/\d+[\.\d,]*/g, prices.totalFee));
      } else {
        alert(data.moreInfo);
      }
    });
  }
};

function Cart() {
}

/* 获取URL参数 */
function request(paras) {
  var url = location.href;
  var paraString = url.substring(url.indexOf("?") + 1, url.length).split(
    /\&|\#/g);
  var paraObj = {}
  for (i = 0; j = paraString[i]; i++) {
    paraObj[j.substring(0,
      j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1,
      j.length);
  }
  var returnValue = paraObj[paras.toLowerCase()];
  if (typeof(returnValue) == "undefined") {
    return "";
  } else {
    return returnValue;
  }
}
