$(document).ready(function() {
  var shopId = $("#shopId").val();
  var parentCategoryId = $("#parentCategoryId").val();
  var shopIndex = {
    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadBrand();
      this.loadProduct();

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          shopIndex.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function() {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/shop/product/list';
        var data = {
          'size': this.page.size,
          'page': this.page.index,
          'shopId': shopId
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#productData");
                var html = '';
                $.each(result.data.list, function(index, product) {
                  html = html +
                    '<li productId="' + product.id + '" onclick="productDetail(this)"><img src="' + product.imgUrl + '" alt="商品图片" class="home-commodity-pic"/>' +
                    '<p class="commodity-name">' + product.name + '</p>' +
                    '<p class="commodity-desc">' + product.description + '</p>' +
                    '<p class="now-price">￥' + product.price + '</p>' +
                    '<div class="purchase-button">立即抢购</div>' +
                    '</li>';
                });
                productData.append(html);
                shopIndex.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            shopIndex.loading = false;
          }
        });
      }
    },

    loadBrand: function() {
        var url = 'http://' + window.location.host + '/v2/shop/category/listOnsale';
        var data = {
          'shopId': shopId
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var brandData = $("#brandData");
                var html = '';
                $.each(result.data, function(index, brand) {
                  html = html +
                    '<div class="brand-commodity-show" shopId="' + shopId + '" categoryId="' + brand.id + '" onclick="click2brand(this)">' +
                    '<img src="' + brand.categoryImgUrl + '" alt="" class="brand-show-pic">' +
                    '</div>';
                });
                brandData.append(html);
                shopIndex.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            shopIndex.loading = false;
          }
        });
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  shopIndex.init();

});

// 点击跳转到品牌团明细页面
function click2brand(target) {
  var shopId = $(target).attr("shopId");
  var categoryId = $(target).attr("categoryId");
  window.location.href = '/shop/brand/' + shopId +'/' + categoryId;
}



