var notice_height;

var shopID = $("#shopID").html();
//var startPos = $('.shop-tab').offset().top;
var startPos = startposF();
var isLoading = false;
var pageSize = 20;

var prodItemWidth = 200;
var prodItemHeight = 200;

var initzx = function(){
    //初始化布局 start
    //初始化小图状态（后端返回目前状态）
    var proInfor = $('.pro-infor');
    if( !proInfor || $(proInfor).length == 0 )return;
    var proOuter = $('.img-outer');
    var imgObj= $(".js-img-size");
    var contain = $("#pro-all");
    var marginL;
   // if($('body').hasClass('smallimg')){
        proInfor.css({
            'display': 'inline-block',
            'width': prodItemWidth+"px",
            'float': 'left',
            'position':'static'
        });
        proOuter.css({
            'width':prodItemWidth + 'px',
            'height': prodItemHeight + "px"
        });
        $(".pro-item-text .widthhuanh").css('width',prodItemWidth+'px');

        var mL = function () {
            var l =proInfor.length;//产品个数
            var screenW = $(window).width();
            var marginTotal = screenW % prodItemWidth;//总间距
            var objLength = parseInt(screenW/prodItemWidth);//一屏能容纳的个数

            pageSize = objLength * 3 + 1 ; //每页显示个数
            if(objLength>l){

                marginL = Math.round((screenW-prodItemWidth*l)/(l+1));
                return marginL;
            }else{
                marginL = Math.round(marginTotal/(objLength+1));
                return marginL;

            }
        }
        proInfor.css({"margin-left":mL(),"margin-bottom":"10px","margin-top":"0","margin-right":"0"});
        imgcenter(imgObj,prodItemWidth);
    //}else if($('body').hasClass('waterfall')) {
    //    proInfor.css({
    //        "display":"block",
    //        'width': prodItemWidth + 'px',
    //        'float':'none',
    //        'margin':'0'
    //    })
    //    proOuter.css({
    //        'width': prodItemWidth + 'px',
    //        'height': 'auto'
    //    });
    //
    //    imgObj.each(function(j,obj){
    //        $(obj).css("width",prodItemWidth + "px");
    //    })
    //    $(".pro-item-text .widthhuanh").css('width',(prodItemWidth -5) + 'px');
    //    $(proInfor).wookmark({
    //        autoResize: true, // This will auto-update the layout when the browser window is resized.
    //        container: contain, // Optional, used for some extra CSS styling
    //        imgobj: imgObj,
    //        proItemText:'pro-item-text',
    //        offset:10,
    //        outerOffset: 10, // Optional, the distance between grid items
    //        itemWidth: prodItemWidth // Optional, the width of a grid item
    //    });
    //
    //
    //}else if($('body').hasClass('bigimg')){
    //    proInfor.css({
    //        "display":"block",
    //        'width':'95%',
    //        'float':'none',
    //        'margin':"0 auto 10px",
    //        'position':'static'
    //    });
    //    proOuter.css({
    //        'width':'100%',
    //        'height':'auto'
    //    });
    //    imgObj.css({
    //        'width':'100%',
    //        'height':'auto',
    //        'margin':'0'
    //    });
    //    //proInfor.css({"margin-left":parseInt((window.screen.availWidth-(window.screen.availWidth*0.95))/2),"margin-bottom":"10px","margin-top":"0","margin-right":"0"});
    //}
    //初始化布局 end
};

function startposF(){
    return 0;
}
function isscroll(){
    startPos =startposF();
    var scrolltop = getScrollTop();

    if(parseInt(scrolltop) > parseInt(startPos)){
        $('.shop-tab').css({'position':'fixed','top':0});
    }else{
        $('.shop-tab').css('position','static');
    }
    isTop();
}

function getScrollTop() {
    var scrollPos;
    if (window.pageYOffset) {
        scrollPos = window.pageYOffset; }
    else if (document.compatMode && document.compatMode != 'BackCompat')
    { scrollPos = document.documentElement.scrollTop; }
    else if (document.body) { scrollPos = document.body.scrollTop; }
    return scrollPos;
}

$(document).ready(function() {
    //oAjax("01",0);
    initzx();
    initClick();
    initSelPos();
    notice_height = $('.shop-notice').height();
    isTop();


});
function after(){
    var  inner = document.getElementsByClassName('inner')
    for(var i =0 ; i<inner.length;i++){
        var elem = inner[i]
        var div = document.createElement('div')
        div.innerHTML = 'aaaa'
        elem.parentNode.insertBefore(div,elem.nextSibling)
    }
}
function oAjax(index,cateID){
    var proAll = document.getElementById("allGoods");
    var ajaxURL;
    var dataNum;//总个数
    var offSet =1;
    var T;
    clearTimeout(T);

    isLoading = true;

    if(index == "01"){
        ajaxURL = "/shop/" + shopID + "/productByPage";
        dataNum = $(".all").attr("data-numb");
        hideNotice();
    }else if(index == 0){//单个类目
        ajaxURL = "/shop/" + shopID + "/category/"+cateID;
        $(".category-list").each(function(i,item){
            if($(item).attr("data-cateid")==cateID){
                dataNum = $(item).attr("data-numb");
            }
        });
    }
    var param={
        type : "post",
        size :  pageSize,
        page : 0
    }
    $.ajax({
        url: ajaxURL,
        data:param,
        success : function(data) {
            $("#pro-all").html(data);
            initzx();
            isLoading = false;
        }
    });
    function addCls(){
        if (Number(dataNum)>pageSize && !$(".shop_all_btn").length>0) {
            var obj = document.createElement("div");//新建一个div元素节点
            obj.innerHTML = '<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>'
            $(obj).addClass("loadEffect shop_all_btn");
            proAll.appendChild(obj);
        }
    }

    function removeCls(){
        if(offSet== Math.ceil(dataNum/pageSize)-1){
            $(".shop_all_btn").remove();
        }
    }

    $(".shop_all_btn").remove();
    addCls();
    //T=setTimeout(function(){
    $(".shop_all_btn").bind("click",function () {
        if (offSet < Math.ceil(dataNum/pageSize)) {
            var param = {
                type: "post",
                size: pageSize,
                page: offSet
            }
            $.ajax({
                url: ajaxURL,
                data: param,
                success: function (data) {
                    $("#pro-all").append(data);
                    offSet++;
                    initzx();
                    if (Number(dataNum) == $('.pro-infor').length) {
                        $(".shop_all_btn").remove();
                    }
                    isLoading = false;
                }
            });
        }
    });
    //},1000)
  $("#userCenter").bind("click",function () {
      var nextUrl = $(this).attr("nexturl");
      window.location.href=nextUrl;
  });

  $(".search-inner").bind("click",function () {
      var nextUrl = $(this).attr("nexturl");
      window.location.href=nextUrl;
  });

  $('.js-shop-cart').each(function () {
     var cart = $(this);
     cart.click(function () {
        var prodId = cart.attr("productid");
        alert(prodId);
    });
  });
}

function tabheightlight(obj){
    $('.tab-wrap span').each(function () {
        $(this).removeClass('category-item-selected');
    });
    $(obj).addClass('category-item-selected');
}
function tabitem() {
    $('.tab-wrap span').click(function () {
        // tab条是否居顶
        var elm = $('.shop-tab');

        $('.tab-wrap span').each(function () {
            $(this).removeClass('sel');
        });
        $(this).addClass('sel');

        var index = $(this).attr("index");
        if(index==null)
        {
            var dc=$(this).attr("data-cateid");
            oAjax(0,dc);
        }
        else
        {
            oAjax(index,0);
        }
        startPos =startposF();
        //类目list是否显示
        if ($('#category').css("display") == "block") {
            $('.shop-tab').css({"position": "fixed", "top": '0'});
            hideCategory();
        }
        isscroll();
        tabheightlight(this);
    })
}

$(window).resize(function() {
    tabheightlight('.sel');
});
function initClick() {
    $('.btn-notice').click(function() {
        if ($('.shop-notice').height() > 0) {
            $('.shop-notice').css3Animate({
                time : '200ms',
                opacity : 0,
                success : function() {
                }
            })
            $('.shop-notice').css("height", "0px");
            $('.shop-notice').css("padding-top", "0px");
            $('.shop-notice').css("padding-bottom", "0px");
        } else {
            $('.shop-notice').css3Animate({
                time : '200ms',
                opacity : 1,
                success : function() {
                }
            })
            $('.shop-notice').css("height", notice_height + "px");
            $('.shop-notice').css("padding-top", "15px");
            $('.shop-notice').css("padding-bottom", "15px");

        }

    })

    $('.category-list').children().each(function (i, item) {
        $(item).click(function (e) {
            //请求类目数据
            var cateID = $(item).attr("data-cateid");
            oAjax(0,cateID);
            $(".tab-wrap").hide();
            $(".tab-category").html($(e.target).text()).show();
            hideCategory();
        })
    })

    tabitem();
    var show_category_flag = false;

    $('#btn-category').click(function(){

        if(!show_category_flag){
            showCategory();
        }else{
            hideCategory();
        }

    })

    $("#btn-category-close").click(function(){
        hideCategory();
    })
    if($(".wechat-id").text()==""){
        $(".btn-wechat").hide();
    }
    $(".btn-wechat").click(function(){
        if(!show_wechat_flag){
            showWechatTip();
        }else{
            hideWechatTip();
        }
    })

    window.onscroll=function(){
        if(!isLoading) {
            var sHeight=document.documentElement.scrollTop||document.body.scrollTop;//滚动高度
            var wHeight=document.documentElement.clientHeight;//window
            var dHeight=document.documentElement.offsetHeight;//整个文档高度
            if(dHeight - (sHeight + wHeight) < 100 && $(".shop_all_btn").length > 0)
            {
                isLoading = true;
                $(".shop_all_btn").click();
            }
        }
    };
}

function initSelPos() {
    $(".all").click();
    $('.tab-wrap span').each(function() {
        if ($(this).hasClass('sel')) {
            $('.tab-wrap span').each(function () {
                $(this).removeClass('category-item-selected');
            });
            $(this).addClass('category-item-selected');
        }
    })

}


function isTop(){
    if($("#category").css("display")=='none'){
        $(window).bind("scroll", listenTopEvent);
        $('body').bind('touchmove',listenTopEvent);
    }

}

function listenTopEvent(){
     // var elm = $('.shop-tab');
     // var p = document.body.scrollTop || document.documentElement.scrollTop;
     // $(elm).css('position',(parseInt(p) > parseInt(startPos)) ? 'fixed' : 'static');
     // $(elm).css('top',(parseInt(p) > parseInt(startPos)) ? '0px' : '');
}

var show_wechat_flag = false;
function showCategory(){
    $('.info-section').css3Animate({
        time:"300ms"
    });
    $('body').unbind('touchmove',listenTopEvent);
    $(window).unbind("scroll",listenTopEvent);
    $('.shop-tab').css({"position":"fixed","top":"0"});

    $("#category").show();
    $("#category").css3Animate({
        time:"300ms"
    })
    var _cateHeight =  $(window).height() - $(".shop-tab")[0].offsetHeight - $(".footer-bottom")[0].offsetHeight-$(".category-title")[0].offsetHeight;
    $('.category-list').css("height",_cateHeight);
    $(".category-home").bind('click', function () {
        $(".tab-category").hide();
        $(".tab-wrap").show();
        hideCategory();
        tabitem();
    })
    /*  var canvas_height = $(window).height() - 50;
     $("#category").css("min-height",canvas_height +"px");*/
    $("#category").css("top","40px");
    show_category_flag = true;
}
function hideCategory(){

    $('.info-section').css3Animate({
        time:"300ms",
        success:function(){
        }
    })
    $('.info-section').css("margin-top","0px");
    $('.shop-tab').css("position","static");
    $("#category").css3Animate({
        time:"300ms"
    })
    $("#category").css("min-height","100px");
    $("#category").css("top","200px");
    $("#category").hide();
    isscroll();
    show_category_flag = false;
}



function showWechatTip(){
    $(".wechat-tip").css3Animate({
        time:"200ms",
        opacity:1
    })
    $(".wechat-tip").css("height","30px");
    show_wechat_flag = true;
}

function hideWechatTip(){
    $(".wechat-tip").css3Animate({
        time:"200ms",
        opacity:0
    })
    $(".wechat-tip").css("height","0px");
    show_wechat_flag = false;
}
function hideNotice(){
    $('.shop-notice').css3Animate({
        time : '200ms',
        opacity : 0,
        success : function() {
        }
    })
    $('.shop-notice').css("height", "0px");
    $('.shop-notice').css("padding-top", "0px");
    $('.shop-notice').css("padding-bottom", "0px");
}

function showNotice(){
    $('.shop-notice').css3Animate({
        time : '200ms',
        opacity : 1,
        success : function() {
        }
    })
    //$('.shop-notice').css("height", notice_height + "px");
    $('.shop-notice').css("padding-top", "15px");
    $('.shop-notice').css("padding-bottom", "15px");
}
