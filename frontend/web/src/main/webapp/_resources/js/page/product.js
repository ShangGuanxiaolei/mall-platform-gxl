window.riseTool = function (obj) {
  // selector
  var $ = function (x) {
    return document.querySelector(x);
  };

  var rise = {
    node: null,
    stillNeed: null,
    lines: [],
    button: "",

    addText: function () {
      var self = this;
      var output = "<p class='tips'>";
      this.lines.forEach(function (line) {
        output += String(line);
        output += "<br>";
      });
      output += "</p>";
      return output;
    },
    addDom: function () {
      var addHTML =
        "<div class='riseTool'>" +
        "<article class='msgBox'>" +
        this.addText() +
        "<button>" + this.button + "</button>" +
        "<img src='/_resources/images/arrow.png' alt='arrow'>" +
        "</article>" +
        "</div>";
      this.node.insertAdjacentHTML('afterend', addHTML);
    },

    hideRise: function () {
      document.querySelector('.riseTool').style.display = "none";
      $("html").style.overflow = "";
      document.querySelector('.riseTool').innerHTML = "";
    },

    getParam: function () {
      this.node = (obj.node) ? document.querySelector(obj.node) : null;
      this.stillNeed = (obj.stillNeed) ? obj.stillNeed : 5;
      this.lines = (obj.lines) ? obj.lines : "";
      this.button = (obj.button) ? obj.button : "";
    },

    allEvent: function () {
      var self = this;
      $('.riseTool').onclick = function () {
        self.hideRise();
      };
    },

    init: function () {
      this.getParam();
      this.addDom();
      this.allEvent();
      document.querySelector('.riseTool').style.display = "block";
      $("html").style.overflow = "hidden";
    }
  };
  rise.init();
};

function toAllProduct() {
  location.href = '/shop#/productList';
}

$(document).ready(function () {

  var confirmType = $('input[name=confirmType]');

  $('#addToCartBtn').bind('click', function () {
    confirmType.val('addToCart');
  });
  $('#buyBtn').bind('click', function () {
    confirmType.val('buy');
    togglePrice.changeToOriginalPrice();
  });

  // 想卖，判断当前用户如果不是推客，则跳到推客申请页面，否则判断如果是app，则调出app页面，如果是微信，则弹出模态窗口
  $('#sellerBtn').bind('click', function () {
    var shopId = $(this).attr("shopId");
    if (!isTwitter || isTwitter == null) {
      location.href = '/twitter/apply/' + shopId;
    } else {
      if (isFromApp) {
        try {
          shareWechat(shareTitle, shareUrl, shareImgLink);
        } catch (e) {
        }
        try {
          window.purchase.shareWechat(shareTitle, shareUrl, shareImgLink)
        } catch (e) {
        }
      } else {
        var riseObject = {
          node: "#rise",
          lines: [
            "点击右上角分享给好友"
          ],
          button: "我知道了"
        };
        riseTool(riseObject);
      }
    }
  });

  // 邀请好友帮忙砍价
  $('#cut_share-btn').bind('click', function () {

      if (isFromApp) {
        try {
          shareWechat(shareTitle, shareUrl, shareImgLink);
        } catch (e) {
        }
        try {
          window.purchase.shareWechat(shareTitle, shareUrl, shareImgLink)
        } catch (e) {
        }
      } else {
        var id = $('#bargainId').val();
        if (id) {
          var url = '/v2/promotionBargain/start?id=' + id;
          $.post(url, null, function (res) {
            if (res) {
              if (res.data) {
                /**if(res.data.hasDone){
                alertMsg('已经发起过砍价，赶快分享给好友吧。');
              }else{
                alertMsg('发起砍价成功。');
              }**/
                var bargainDetailId = res.data.id;
                var productId = res.data.productId;
                var promotionId = res.data.promotionId;
                shareUrl = siteHost + "/p/promotion/share/" + productId
                  + "?promotionId=" + promotionId + "&activityId="
                  + bargainDetailId;
                shareLink = shareUrl;
                wx.onMenuShareAppMessage({
                  title: shareTitle,
                  desc: '',
                  link: shareLink,
                  imgUrl: shareImgLink,
                  trigger: function (res) {
                  },
                  success: function () {
                  },
                  cancel: function () {
                  }
                });
                wx.onMenuShareTimeline({
                  title: shareTitle,
                  link: shareLink, // 分享链接
                  imgUrl: shareImgLink, // 分享图标
                  success: function () {
                  },
                  cancel: function () {
                  }
                });
                var riseObject = {
                  node: "#rise",
                  lines: [
                    "点击右上角分享给好友",
                    "邀请好友帮忙砍价吧"
                  ],
                  button: "我知道了"
                };
                riseTool(riseObject);
              } else {
                alert(res.moreInfo);
              }
            } else {
              alert('网络问题，请稍后再试');
            }
          });
        }
      }
    }
  );

  // 邀请好友帮忙拼团
  $('#groupon_share-btn').bind('click', function () {
    var needNumbers = $("#needNumbers").val();
    if (isFromApp) {
      try {
        shareWechat(shareTitle, shareUrl, shareImgLink);
      } catch (e) {
      }
      try {
        window.purchase.shareWechat(shareTitle, shareUrl, shareImgLink)
      } catch (e) {
      }
    } else {
      var riseObject = {
        node: "#rise",
        lines: [
          "还差<strong>" + needNumbers + "</strong>人即可成团",
          "点击右上角分享给好友",
          "邀请好友帮忙砍价吧"
        ],
        button: "我知道了"
      }
      riseTool(riseObject);
    }
  });

  $('#buyOriginalBtn').bind('click', function () {
    confirmType.val('buyOriginal');
    togglePrice.changeToDiscountPrice();
  });
  $('#confirmed').bind('click', function () {
    var product = new Product();
    if (confirmType.val() == 'buy') {
      product.buy();
    } else if (confirmType.val() == 'buyOriginal') {
      product.buyOriginal();
    } else if (confirmType.val() == 'addToCart') {
      product.addToCart();
    }
  });
  $('#province').bind('click', function () {
    var product = new Product();
    product.updateZoneSelector(this, 1);
  });
  $('#city').bind('click', function () {
    var product = new Product();
    product.updateZoneSelector(this, $('#province').val());
  });
  $('#district').bind('click', function () {
    var product = new Product();
    product.updateZoneSelector(this, $('#city').val());
  });
  //$('#identitySeller').bind('click', function(){
  //	var product = new Product();
  //	product.addToMyShop($("#product-id").val(), $("#shop-id").val());
  //});
  $('#userCenter').bind('click', function () {
    var nextUrl = $('#userCenter').attr("nexturl");
    MemberSignin.login.checkSignined(nextUrl, function () {
      window.location.href = nextUrl;
    });
  });
  $('#addToCartForm').bind('submit', function () {
    var skuId = $('#skuId').val();
    if (!skuId) {
      alert('请选择规格');
      return false;
    }
    return true;
  });

  //绑定加入购物车,立即购买按钮到模态窗口事件
  $("#addToCartBtn").click(function () {
    //$("#confirmed").html('确认');
    //$(".popup-sku").show();
    //$(".popup-overlay").addClass("visible");
    var product = new Product();
    product.addToCart();
  });
  $("#buyBtn").click(function () {
    $("#confirmed").html('确认');
    $(".popup-sku").show();
    $(".popup-overlay").addClass("visible");
  });
  $('#buyPromotionBtn').click(function () {
    var product = new Product();
    product.buy();
  });
  $('#startBargain').click(function () {
    $("#cut_share-btn").trigger("click");
  });
  $('#helpBargain').click(function () {
    var id = $('#helpBargain').val();
    if (id) {
      var url = '/v2/promotionBargain/helpBargain?id=' + id;
      $.post(url, null, function (res) {
        if (res) {
          if (res.data && res.data === true) {
            alertMsg('帮忙砍价成功');
            location.reload();
          } else {
            alert(res.moreInfo);
          }
        } else {
          alert('网络问题，请稍后再试');
        }
      });
    }
  });
  $("#buyOriginalBtn").click(function () {
    // $("#confirmed").html('确认');
    // $(".popup-sku").show();
    // $(".popup-overlay").addClass("visible");
    var product = new Product();
    product.buyOriginal();
  });

  $(".viewsku").click(function () {
    confirmType.val('buy');
    $("#confirmed").html('购买');
    $(".popup-sku").show();
    $(".popup-overlay").addClass("visible");
  });

  $(".close-popup").click(function () {
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
  });

  //选择sku
  if ($(".pro-sku-val").length == 0) {
    $("#skuId").val($(".js-sku").attr("data-sku-id"));
  }
  var selectItem = [];

  function selsectsku() {
    //获取已选择的sku单项
    selectItem = [];
    $('.pro-sku-val').find('.selected').each(function () {
      var item = {
        key: $(this).parent().attr('mapping_key'), //如spec1
        value: $(this).text() //如红色
      };
      selectItem.push(item);
    });
    return selectItem;
  }

  $('.prodOption').bind('click', function () {
    var self = $(this);
    var currentText = "<div class='currentText'>" + $(self).text() + "</div>";
    //同一维度sku之能选择一个
    var skuData = [];
    if (self.hasClass('sku_disabled')) {
      return;
    }

    if (self.hasClass('selected')) {
      self.removeClass('selected');
      $("#skuId").val('');
    } else {
      self.parent().find('.selected').removeClass('selected');
      self.addClass('selected');
      if ($('.selected').length == $(".pro-sku-val").length) {
        skuid(self);//sku都选时获取skuid
      } else {
        $("#skuId").val('');
      }
    }
    shows(self);
  });

  //判断书否可选
  /*if($('.prodOption').hasClass("no-seleted")){
   $('.prodOption').unbind('click');
   }*/

  /**
   * 修改购买时的价格
   * @type {{changeToOriginalPrice, changeToDiscountPrice}}
   */
  var togglePrice = function () {
    var originalPrice = $('#originalPrice').val();
    var discountPrice = $('#discountPrice').val();
    var $priceBefore = $('#priceCurrencyBefore');
    var $priceAfter = $('#priceCurrencyAfter');

    var fillPrice = function (price) {
      if (price) {
        var newPrice = price.split('.');
        $priceBefore.text(newPrice[0] + '.');
        $priceAfter.text(newPrice[1]);
      }
    };

    return {
      changeToOriginalPrice: function () {
        fillPrice(discountPrice);
      },
      changeToDiscountPrice: function () {
        fillPrice(originalPrice);
      }
    }
  }();

  $("#price").attr("data-price", $("#price").text());
  $("#J-amount").attr("data-amount", $("#J-amount").find('span').text());
  var skulist = [];

  function skuid(self) {
    skulist = [];
    var selectitem = selsectsku();
    var parent = $(self).parent();
    var specKey = parent.attr("mapping_key");//获取当前key即维度
    $("sku").each(function (k, item) {//几条数据代表几个纬度，与所需更新数据的坑一致，通过key，obj对应
      if ($(item).attr("data-" + specKey) == self.text()) {//筛选当前所选sku对应的项
        skulist.push(item);
      }
    });

    //sku多维度都选定，重置库存、价格、赋值skuid
    $(skulist).each(function (k, item) {
      if ($(".pro-sku-val").length == 1 && $(item).attr("data-" + $(
          selectitem).get(0).key) == $(selectitem).get(0).value ||
        $(".pro-sku-val").length == 2 && $(item).attr("data-" + $(
          selectitem).get(0).key) == $(selectitem).get(0).value && $(
          item).attr("data-" + $(selectitem).get(1).key) == $(selectitem).get(
          1).value) {
        $("#skuId").val($(item).attr("data-sku-id"));
        $("#inventory_amount").text($(item).attr("data-amount"));
        $("#price").text("￥" + $(item).attr("data-price"));
        $("#price").attr("data-price", $("#price").text());
        $("#market_price").text("￥" + $(item).attr("data-marketprice"));
      }
    });
  }

  //加入购物或直接购买，每个维度必须全选择
  function shows() {
    var selectitem = selsectsku();
    $('.pro-sku-val').find(':not(.selected)').addClass('sku_disabled');
    //理论上所有的sku
    $('.pro-sku-val').find('.prodOption').each(function (i, allItem) {
      //已选中的不做处理
      if ($(allItem).hasClass('selected')) {
        return true;
      }

      //未选中的
      var specKey = $(allItem).parent().attr('mapping_key'); //spec1
      var specValue = $(allItem).text();    //10kg
      var bInclude = false;

      //本商品的sku
      $('.js-sku').each(function (j, existItem) {
        if ($(existItem).attr('data-' + specKey) == specValue) { //找到有10kg的sku
          //已选择spec1 10kg
          $.each(selectitem, function (m, item) {
            if ($(existItem).attr('data-' + item.key) == item.value) {
              bInclude = true;
            }
            //sku多维度都选定，重置库存、价格、赋值skuid
            /* if($('.selected').length==$(".pro-sku-val").length){
             if($(selectitem).get(1)!==undefined && $(item).attr("data-"+$(selectitem).get(0).key)==$(selectitem).get(0).value && $(item).attr("data-"+$(selectitem).get(1).key)==$(selectitem).get(1).value){
             $("#skuId").val($(item).attr("data-sku-id"));
             $("#inventory_amount").text($(item).attr("data-amount"));
             $("#price").text("￥" + $(item).attr("data-price"));
             }
             }else{
             $("#skuId").val('');
             }*/

          });
        }
      });

      //sku 未选
      if (selectitem.length == 0) {
        $("#price").text($("#price").attr("data-price"));
        $("#J-amount").find("span").text($("#J-amount").attr("data-amount"));
        bInclude = true;
      }

      //选中一个
      if (selectitem.length == 1) {
        if (selectitem[0].key == 'spec1' && specKey == 'spec1') {
          bInclude = true;
        } else if (selectitem[0].key == 'spec2' && specKey == 'spec2') {
          bInclude = true;
        }
      }
      //console.log($(allItem));
      if (bInclude) {
        $(allItem).removeClass('sku_disabled');
      }
    });
  }
});

function alertMsg(msg) {
  var options = {
    'animation': true,
    'container': 'body',
    'content': '123',
    'placement': 'top',
    'template': '<div class="modal toast modal-out" style="display: block; margin-top: -11px; margin-left: -94px;" role="tooltip">'
    + msg + '</div>'
  };
  $('.bottom-bar').popover(options);
  $('.bottom-bar').popover('show');
  setTimeout(function () {
    $('.bottom-bar').popover('destroy')
  }, 800);
}

// 用户收藏或取消收藏商品
function collectionDo() {
  var productId = $("#product-id").val();
  if ($("#collectionBtn").hasClass("footer_star_active")) {
    var url = '/v2/productCollection/delete?productId=' + productId;
    $.post(url, {}, function (data) {
      var r = data;
      if (r.data) {
        $("#collectionBtn").removeClass("footer_star_active");
        $("#collectionBtn").addClass("footer_star-btn");
        alertMsg('已取消商品收藏');
        $("#collectionName").html('收藏');
      } else {
        alert(r.moreInfo);
      }
    });
  } else {
    var url = '/v2/productCollection/save?productId=' + productId;
    $.post(url, {}, function (data) {
      var r = data;
      if (r.data) {
        $("#collectionBtn").removeClass("footer_star-btn");
        $("#collectionBtn").addClass("footer_star_active");
        alertMsg('商品收藏成功');
        $("#collectionName").html('已收藏');
      } else {
        alert(r.moreInfo);
      }
    });
  }
}

Product.prototype = {

  addToCart: function () {
    var skuId = $('#skuId').val();
    var amount = $('input[name=amount]').val();
    var productId = $('#productId').val();
    var shopOwnerId = $('#shopOwnerId').val();
    if (!skuId) {
      alert('请选择规格');
      return;
    }
    var params = {
      'skuId': skuId,
      'amount': amount,
      'shopOwnerId': shopOwnerId,
      'productId': productId
    };
    var url = '/cart/add';
    $.post(url, params, function (data) {
      $(".popup-sku").hide();
      $(".popup-overlay").removeClass("visible");
      var r = data;
      if (r.data) {
        var options = {
          'animation': true,
          'container': 'body',
          'content': '123',
          'placement': 'top',
          'template': '<div class="modal toast modal-out" style="display: block; margin-top: -11px; margin-left: -94px;" role="tooltip">添加成功，在购物车等亲～</div>'
        };
        $('.bottom-bar').popover(options);
        $('.bottom-bar').popover('show');
        setTimeout(function () {
          $('.bottom-bar').popover('destroy')
        }, 800);

        // 更新购物车数量图标
        url = 'http://' + window.location.host + '/v2/cart/countByShopId';
        var shopId = $("#shop-id").val();
        params = {
          'shopId': shopId
        };
        $.post(url, params, function (data) {
          var r = data;
          if (r.data) {
            $(".cart-added-num").html(r.data);
          }
        });

      } else {
        alert(r.moreInfo);
      }
    });
  },
  buy: function () {
    var shopId = $("#shop-id").val();
    var skuId = $('#skuId').val();
    var productId = $('#productId').val();
    var qty = $('input[name=amount]').val();
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);
    var numed = $(".numed");
    // 订单类型，b2b进货订单等
    var orderType = $('#orderType').val();
    var promotionId = $('#promotion-id').val();
    var promotionFrom = $('#promotion-from').val();
    var activityGrouponId = $('#activityGroupon-id').val();
    var yundouProductId = $('#yundou-product-id').val();
    var inventory = parseInt($("#inventory_amount").text().replace(',', ''));
    if (!skuId) {
      alert('请选择规格');
      return;
    }
    if (!re.test(numed.val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) < 1) {
      alert('请输入大于1的商品数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) > inventory) {
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      return false;
    }

    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
    //下单前验证用户是否已登录
    var upline = get('upline');
    var nextUrl = '/cart/next?skuId=' + skuId + '&qty=' + qty + '&productId='
      + productId + '&shopId=' + shopId + '&orderType=' + orderType;
    if (promotionId && (typeof promotionId) !== 'undefined') {
      nextUrl += '&promotionId=' + promotionId;
    }
    if (promotionFrom && promotionFrom !== '') {
      nextUrl += '&promotionFrom=' + promotionFrom;
    }
    if (activityGrouponId && (typeof activityGrouponId !== 'undefined')) {
      nextUrl += '&activityGrouponId=' + activityGrouponId;
    }
    if (yundouProductId && (typeof yundouProductId !== 'undefined')) {
      nextUrl += '&yundouProductId=' + yundouProductId;
    }

    window.location.href = nextUrl;

    //js_checkSignin(nextUrl);

    /*var params = {
        'skuId': skuId,
        'amount': amount
    };
    $.post("/cart/update", params, function(r) {
        var data;
        try{
            data = $.parseJSON(r);
        }catch(e){
            data = eval(r);
        }
        if (data.data) {
            //location.href = "/cart?skuId=" + skuId;
            location.href = "/cart/next?skuId=" + skuId;
        } else {
            alert(data.moreInfo);
        }
    });*/
  },
  buyOriginal: function () {
    var shopId = $("#shop-id").val();
    var skuId = $('#skuId').val();
    var productId = $('#productId').val();
    var qty = 1;
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);
    var numed = $(".numed");
    // 订单类型，b2b进货订单等
    var orderType = $('#orderType').val();
    var inventory = parseInt($("#skuAmount").text().replace(',', ''));
    if (!skuId) {
      alert('请选择规格');
      return;
    }
    if (!re.test(numed.val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) < 1) {
      alert('请输入大于1的商品数字');
      numed.val('1').text('1').trigger('change');
      return false;
    } else if (Number(numed.val()) > inventory) {
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      return false;
    }

    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
    //下单前验证用户是否已登录
    var nextUrl = '/cart/next?skuId=' + skuId + '&qty=' + qty + '&productId='
      + productId + '&shopId=' + shopId + '&orderType=' + orderType;
    window.location.href = nextUrl;

    //js_checkSignin(nextUrl);

    /*var params = {
        'skuId': skuId,
        'amount': amount
    };
    $.post("/cart/update", params, function(r) {
        var data;
        try{
            data = $.parseJSON(r);
        }catch(e){
            data = eval(r);
        }
        if (data.data) {
            //location.href = "/cart?skuId=" + skuId;
            location.href = "/cart/next?skuId=" + skuId;
        } else {
            alert(data.moreInfo);
        }
    });*/
  },
  //selectProvince: function() {
  //    var url = '/zone/1/children';
  //    $.getJSON(url, function(data) {
  //        var i=0;
  //        var dataL=data.length;
  //        for (i; i < dataL; i++) {
  //            //console.log(data[i]);
  //            var option = '<option value="' + data[i].id + '">' + data[i].name + '</option>';
  //            $('#province').append(option);
  //        }
  //    });
  //},
  updateZoneSelector: function (zoneEle, parentZoneId) {
    var url = '/zone/' + parentZoneId + '/children';
    $.getJSON(url, function (data) {
      var i = 0;
      var dataL = data.length;
      for (i; i < dataL; i++) {
        var option = '<option value="' + data[i].id + '">' + data[i].name
          + '</option>';
        $(zoneEle).append(option);
      }
    });
  },
  //addToMyShop: function(productId, shopId){
  //	var params = {
  //        'productId': productId,
  //        'shopId': shopId
  //    };
  //    var url = '/v2/product/addDistributionProduct';
  //    $.post(url, params, function(data) {
  //        var r = data;
  //        if (r.data) {
  //            alert('添加成功');
  //        } else {
  //            alert(r.moreInfo);
  //        }
  //    });
  //},
  submitForm: function () {
  }
};

function Product() {
}

function get(name) {
  if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(
    location.search)) {
    return decodeURIComponent(name[1]);
  }
}
