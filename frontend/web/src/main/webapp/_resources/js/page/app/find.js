$(document).ready(function() {
  var tweet = {
    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadProduct();

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          tweet.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function() {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/tweet/listApp';
        var data = {
          'size': this.page.size,
          'page': this.page.index
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#tweetData");
                var html = '';
                $.each(result.data.list, function(index, product) {
                  html = html +
                    '<div class="app-more-cate">' +
                    '<div class="more-cate-head">' +
                    '<img src="/_resources/image/app_more_head.jpg" alt=""/>' +
                    '<span>' + product.title + '</span>' +
                    '<i>' + product.dayDesc + '</i>' +
                    '</div>';

                  // 发现的图片处理，一张图片和多张图片是不同的展现方式
                  var imgs = product.imgs;
                  if(imgs.length == 1){
                    html = html + '<div class="more-wraper-big">' +
                    '<img src="' + imgs[0].imgUrl + '" alt="" class="more-pic-big"/>' +
                    '</div>';
                  }else if(imgs.length > 1){
                    var count = 0;
                    html = html + '<div class="more-pic-row">';
                    $.each(imgs, function(index, img) {
                      count ++;
                      html = html + '<div class="more-wraper-small">' +
                        '<img src="' + img.imgUrl + '" alt="" class="more-pic-small"/>' +
                      '</div>';
                      // 每三个图片换一行
                      if(count%3 == 0){
                        html = html + '</div><div class="more-pic-row">';
                      }
                    });
                    // 补齐一行空白的图片div
                    while(count%3 !=0){
                      html = html + '<div class="more-wraper-small"></div>';
                      count ++;
                    }
                    html = html + '</div>';
                  }
                  html = html +'<div class="more-cate-des" targetType="' + product.targetType + '" target="' + product.target + '" onclick="tweetDetail(this)">' +
                    '<i>' + product.description + '</i>' +
                    '</div>' +
                    '<div class="flex-space-around save-copy-share">' +
                    '<span>保存图片</span>' +
                    '<em>复制文字</em>' +
                    '<i>分享</i>' +
                    '</div>' +
                    '</div>';
                });
                productData.append(html);
                tweet.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            tweet.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  tweet.init();

});


// 发现点击后判断类型，商品则跳到商品详情，团购则跳到团购详情
function tweetDetail(target){
  var targetType = $(target).attr("targetType");
  var target = $(target).attr("target");
  if(targetType == 'PRODUCT'){
    window.location.href = '/p/' + target;
  }else if(targetType == 'GROUPON'){
    window.location.href = '/groupon/' + target;
  }

}
