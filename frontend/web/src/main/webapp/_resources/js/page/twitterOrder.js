$(document).ready(function() {

  var twitterOrder = {

    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadOrder();

      //binding
      window.onscroll = function() {
        if(!this.loading) {
          if (  document.documentElement.clientHeight +
            $(document).scrollTop() >= document.body.offsetHeight ) {
            console.log("You're at the bottom of the page.");
            twitterOrder.loadOrder();
          }
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadOrder: function() {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/twitterCenter/order/list';
        var status = $("input[name=pageStatus]").val();
        var data = {
          'size': this.page.size,
          'page': this.page.index,
          'status': status,
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var OrderData = $(".orderListParent");
                var html = '';

                $.each(result.data, function(index, order) {

                  var items = order.orderItems;

                  html = html +
                    '<section class="order-goods active">' +
                    '<div class="show-goods">' +
                    '<div class="block-title">' +
                    '<label>' + order.orderNo + '</label>' +
                    '<a href="#" class="pull-right">' + order.statusStr + '</a>' +
                    '</div>' +
                    '<div class="">' +
                    '<ul class="cart-list js-cart-item">';

                  $.each(items, function(index, item){
                    html = html +
                      '<li>' +
                      '<div class="item-info cart-item-0">' +
                      '<a href="/p/' + item.productId + '" class="item-image">' +
                      '<img src="' + item.productImgUrl + '" />' +
                      '</a>' +
                      '<div class="cart-normal-info">' +
                      '<a href="/p/' + item.productId + '" class="item-title">' + item.productName + '</a>' +
                      '<span class="item-price">' + item.price + '</span>' +
                      '<div class="item-sku-quantity">' +
                      '<span class="quantity">× <span class="quantity-0">' + item.amount + '</span></span>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</li>';
                  });

                  var commissionFee = order.commissionFee == null ? 0 : order.commissionFee;
                  var totalFee = order.totalFee == null ? 0 : order.totalFee;
                  html = html +
                    '</ul>' +
                    '<div class="order-total">' +
                    '<label>总价:</label><span class="cl-price">' + totalFee + '</span>' +
                    '<span>   </span><label>佣金:</label><span class="cl-price">' + commissionFee  + '</span>' +
                    '<div class="pull-right peerpay-btn">' +
                    '<button id="orderDetailBtn" order-id="' + order.mainOrderId + '" class="pull-right btn btn-info-o">详情</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</section>';
                });
                OrderData.append(html);

                twitterOrder.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            twitterOrder.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "JSON",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  twitterOrder.init();

});
