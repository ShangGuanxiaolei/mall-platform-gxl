$(document).ready(function(){
  //数量输入框限定只能输入数字
  $("body").on("change",".numed",function(){
    var self = $(this);
    var edge = self.attr('edge');
    var regu = /^([0-9]+)$/;
    var re = new RegExp(regu);

    if (!re.test($(this).val())) {
      alert('请输入数字');
      numed.val('1').text('1').trigger('change');
      return false;
    }else if(Number($(this).val())<1){
      alert('请输入大于1的商品数字');
      numed.val('1').text('1').trigger('change');
      return false;
    }else if(Number($(this).val())>edge){
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      $(this).val(edge).text(edge).trigger('change');
      return false;
    }
  });

  $("body").on("click",".numInc,.numDec",function(){
    var self = $(this), numed = self.parent().children('.numed'),
      delta = self.hasClass('numInc') ? 1 : -1,  oldVal = parseInt(numed.val());
    oldVal = isNaN(oldVal) ? 0 : oldVal;
    edge = self.attr('edge');
    var newVal = oldVal + delta;
    if(newVal<1){
      alert('商品数量只能大于等于1哦~');
      numed.val('1').text('1').trigger('change');
    }else if(newVal>edge){
      alert('亲，手下留情哦，仓库都被你搬回家拉，所选数量大于库存数量了哦~');
      numed.val(edge).text(edge).trigger('change');
    }else{
      numed.val(newVal).text(newVal).trigger('change');
    }
    return false;
  });

  $('#backButton').click(function() {
    history.back();
    return false;
  });

});


//获取窗口可视范围的高度
function getClientHeight(){
  var clientHeight=0;
  if(document.body.clientHeight&&document.documentElement.clientHeight){
    clientHeight=(document.body.clientHeight<document.documentElement.clientHeight)?document.body.clientHeight:document.documentElement.clientHeight;
  }else{
    clientHeight=(document.body.clientHeight>document.documentElement.clientHeight)?document.body.clientHeight:document.documentElement.clientHeight;
  }
  return clientHeight;
}


function getScrollTop(){
  var scrollTop=0;
  scrollTop=(document.body.scrollTop>document.documentElement.scrollTop)?document.body.scrollTop:document.documentElement.scrollTop;
  return scrollTop;
}

// 点击跳转到商品明细页面
function productDetail(target){
  var productId = $(target).attr("productId");
  window.location.href = '/p/' + productId;
}

// 购物车图标点击后model展示界面
function cartShow(productId){
  var popupsku = $(".popup-sku");
  popupsku.html('');
  var href = "/v2/product/" + productId;
  $.ajax({
    url: href,
    type: 'POST',
    dataType: 'json',
    success: function (data) {
      if (data.errorCode == 200) {
        if (typeof(data) === 'object') {
          if (data.errorCode == 200) {
            var product = data.data;
            var html = "";
            html = html + '<div class="goods-summary">' +
              '<div class="thumb">' +
              '<img src=" ' + product.imgUrl + ' "/>' +
              '</div>' +
              '<div class="detail">' +
              '<p class="title" >' + product.name + '</p>' +
              '<div class="price-scope cart-price-num">' +
              '<i class="price-currency">￥</i>' +
              '<span class="price-integer">&nbsp;' + product.price + '</span>' +
              '</div>' +
              '</div>' +
              '<div data-popup=".popup-sku" class="close-popup if if-close"></div>' +
              '</div>' +
              '<div class="popup-sku-content" style="max-height: 185px;">';

            var skus = product.skus;
            $.each(skus, function (i, sku) {
              html = html + '<sku class="js-sku"  data-amount="'+ sku.amount +'" data-marketPrice="'+ sku.marketPrice +'" data-price="'+ sku.price +'" data-sku-id="'+ sku.id +'" data-spec1="'+ sku.spec1 +'" data-spec2="'+ sku.spec2 +'" data-spec3="'+ sku.spec3 +'" data-spec4="'+ sku.spec4 +'"  data-spec5="'+ sku.spec5 +'" >';
              if(i == 0){
                html = html + '<input type="hidden" name="skuId" value="' + sku.id + '"/>';
              }
              html = html + '</sku>';
            });

            var skuMappings = product.skuMappings;
            $.each(skuMappings, function (i, mapping) {
              html = html + '<div class="box pt10b5 font-dark sku-control cart-sku-control">' +
                  '<div class="w100 fz26 ml30">' + mapping.specName + '</div>' +
                  '<div class="flex1 mr30 pro-sku-val" mapping_id="' + mapping.id + '" mapping_key="' + mapping.specKey + '">';
              var mappingValues = mapping.mappingValues;
              $.each(mappingValues, function (i, value) {
                html = html + '<div class="button fz26 prodOption mb10 ff-numb';
                if(i == 0){
                  html = html + ' selected';
                }
                html = html + '" numb="' + mappingValues.length + '" data-th-text="${value}">' + value;
              });

              html = html + '</div></div></div>';
            });

            html = html + '<div class="quantity-control clearfix">' +
              '<label>数量</label>' +
              '<div class="quantity"><div class="numEditor">' +
              '<button type="button" class="numDec va-m numMinus" edge="' + product.amount + '" style="padding:0;"></button>' +
              '<input class="numed amount va-m" name="amount" edge="' + product.amount + '" value="1" />' +
              '<button type="button" class="numInc va-m numPlus" edge="' + product.amount + '" style="padding:0;"></button></div>' +
              '</div><div>' +
              '</div>' +
              '<div class="stock">库存：剩余<span class="cart-stock-num" id="J-amount" data-amount="" >' + product.amount + '件</span></div>' +
              '</div>' +
              '<div class="popup-options">' +
              '<div id="confirmed" onclick="addToCart(\'' + productId + '\');" type="button" class="btn btn-buy btn-block">确认</div>' +
              '</div>' +
              '</div>' +
              '</div>';

            popupsku.html(html);
          }
        }
      } else {
        alert('服务器暂时没有响应，请稍后重试...');
      }
    },
    error: function (state) {
      if (state.status == 401) {
        utils.tool.goLogin();
      } else {
        alert('服务器暂时没有响应，请稍后重试...');
      }
    }
  });
  popupsku.show();
  $(".popup-overlay").addClass("visible");
}

// 购物车界面点击确定加入购物车
function addToCart(productId) {
  var skuId = $('input[name=skuId]').val();
  var amount = $('input[name=amount]').val();
  var shopOwnerId = $('#shopOwnerId').val();
  if (!skuId) {
    alert('请选择规格');
    return;
  }
  var params = {
    'skuId': skuId,
    'amount':amount,
    'shopOwnerId':shopOwnerId,
    'productId': productId
  };
  var url = '/cart/add';
  $.post(url, params, function(data) {
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
    alert('添加成功，在购物车等亲～');
    /**var options = {
        'animation': true,
        'container': 'body',
        'content': '123',
        'placement': 'top',
        'template': '<div class="modal toast modal-out" style="display: block; margin-top: -11px; margin-left: -94px;" role="tooltip">添加成功，在购物车等亲～</div>'
    }
    $('.bottom-bar').popover(options);
    $('.bottom-bar').popover('show');
    setTimeout(function () {
        $('.bottom-bar').popover('hide');
    }, 800);**/
  });
}

