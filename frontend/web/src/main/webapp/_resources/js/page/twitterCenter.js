$(document).ready(function() {

  var twitterMember = {

    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadTeamMember();

      //binding
      window.onscroll = function() {
        if(!this.loading) {
          if (  document.documentElement.clientHeight +
            $(document).scrollTop() >= document.body.offsetHeight ) {
            console.log("You're at the bottom of the page.");
            twitterMember.loadTeamMember();
          }
        }
      };

    },

    loadTeamMember: function() {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/twitterCenter/teamMember';
        var status = $("input[name=pageStatus]").val();
        var data = {
          'size': this.page.size,
          'page': this.page.index,
          'status': status,
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var teamMemberData = $("#teamMemberData");
                var html = '';
                $.each(result.data, function(index, user) {
                  html = html +
                    '<li>' +
                    '<div class="item-info">' +
                    '<a href="/shop/' + user.shopId + '" class="item-image">' +
                    '<img src="' + user.avatar + '" />' +
                    '</a>' +
                    '<div class="account">' +
                    '<a href="/shop/' + user.shopId + '">' +
                    '<span class="user-name">' + user.name + '</span>' +
                    '</a>' +
                    '<span class="user-account"><i class="if if-user"></i>' + user.phone + '</span>' +
                    '</div>' +
                    '</div>' +
                    '</li>';
                });

                teamMemberData.append(html);




                twitterMember.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            twitterMember.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      var that = this;
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "JSON",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    },
  };

  twitterMember.init();

});
