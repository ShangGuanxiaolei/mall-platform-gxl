$(document).ready(function() {
  var timer;
  var sale = {
    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      $('body').append('<div id="loading" style="position:absolute;top:0;left:0;z-index:99;width:' + 
                        innerWidth + 
                        'px;height:' + 
                        innerHeight + 
                        'px;background:url(\'../_resources/images/b2b/loading.gif\') center center no-repeat"></div>'
      );
      this.loadProduct(function(){
        $('#loading').remove();
      });

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          sale.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function(callback) {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/promotionFlashsale/user/list';
        var data = {
          'size': this.page.size,
          'page': this.page.index
        };

        this.postMethod(url, data, function (result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#flashsaleData");
                var html = '';
                $.each(result.data.list, function (index, product) {
                  html = html +
                    '<div class="sale-panic-items" productId="' + product.productId + '" onclick="productDetail(this)">' +
                    '<img src="' + product.productImg + '" alt="" class="sale-commodity-pic"/>' +
                    '<p class="sale-commodity-des">' + product.title + product.productName + ' </p>' +
                    '<p class="sale-now-price">￥' + product.discount + '</p>' +
                    '<p class="sale-original-price">' +
                    '<span>￥' + product.productPrice + '</span>' +
                    '<span class="saled-customer">' + product.sales + '已抢购</span>' +
                    '</p>' +
                    '<p class="sale-rest-time" validTo="' + product.validTo + '" validFrom="' + product.validFrom + '">1天1小时12分后结束</p>' +
                    '<div class="sale-panic-btn">' +
                    '立即抢购' +
                    '</div>' +
                    '</div>';
                });
                if(typeof callback === 'function') {
                  callback();
                }
                if(result.data.list && result.data.list.length>0){
                  productData.append(html);
                  sale.page.index += 1;
                  //异步得到数据后，需要重新调用计时器，对限时时间进行倒数
                  if (timer) {
                    clearInterval(timer);
                  }
                  doTimer();
                }
                break;
              case -1:
                break;
              default:
                break;
            }
            sale.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  sale.init();

});

// 计时器，刷新页面上的限时时间倒数
function doTimer()
{
  $(".sale-rest-time").each(function(){
    var validFrom = $(this).attr("validFrom");
    var validTo = $(this).attr("validTo");
    var now = new Date().getTime();
    var ts;
    if(validFrom > now){
      ts = validFrom - now;//活动还未开始
    }else if(now > validFrom && validTo >now){
      ts = validTo - now;//活动进行中
    }else{
      ts = now - validTo;//活动已经结束
    }
    var dd = parseInt(ts / 1000 / 60 / 60 / 24, 10);//计算剩余的天数
    var hh = parseInt(ts / 1000 / 60 / 60 % 24, 10);//计算剩余的小时数
    var mm = parseInt(ts / 1000 / 60 % 60, 10);//计算剩余的分钟数
    var ss = parseInt(ts / 1000 % 60, 10);//计算剩余的秒数
    dd = checkTime(dd);
    hh = checkTime(hh);
    mm = checkTime(mm);
    $(this).html(getDateStr(now, validFrom, dd, hh, mm));
    // 根据开始结束时间，判断按钮显示
    if(now > validTo){
      $(this).hide();
      $(this).next("div").addClass("sale-panic-btn-end");
      $(this).next("div").html('活动结束');
    }else if(now > validFrom){
      $(this).next("div").addClass("sale-panic-btn");
      $(this).next("div").html('立即抢购');
    }else if(now < validFrom){
      $(this).next("div").addClass("sale-panic-btn-begin");
      $(this).next("div").html('即将开始');
    }

  });
  timer = setInterval("doTimer()",60000);

}
function checkTime(i)
{
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}
function getDateStr(now, validFrom, dd, hh, mm){
  var str = "";
  if(dd == '00'){
    if(hh == '00'){
      str = mm + "分后";
    }else{
      str = hh + "时" + mm + "分后";
    }
  }else{
    str = dd + "天" + hh + "时" + mm + "分后";
  }
  if(now > validFrom){
    str = str + "结束";
  }else{
    str = str + "开始";
  }
  return str;
}
