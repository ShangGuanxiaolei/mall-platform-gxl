$(document).ready(function() {

  var twitterWithdraw = {

    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadWithdraw();

      //binding
      window.onscroll = function() {
        if(!this.loading) {
          if (  document.documentElement.clientHeight +
            $(document).scrollTop() >= document.body.offsetHeight ) {
            console.log("You're at the bottom of the page.");
            twitterWithdraw.loadWithdraw();
          }
        }
      };
    },

    loadWithdraw: function() {
      if (this.loading == false) {
        this.loading = true;
        var url = 'http://' + window.location.host + '/v2/withdraw/applylist';
        var data = {
          'size': this.page.size,
          'page': this.page.index
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var OrderData = $(".withdrawListParent");
                var html = '';
                if(result.data){
                  $.each(result.data.list, function(index, order) {
                    html = html +
                      '<tr><td>' + order.createdAtStr + '</td><td>' + order.applyMoney + '</td><td>' + order.payAtStr + '</td><td>' + order.statusStr + '</td></tr>';
                  });

                  OrderData.append(html);

                  twitterWithdraw.page.index += 1;
                  break;
                }
              case -1:
                break;
              default:
                break;
            }
            twitterWithdraw.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "JSON",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  twitterWithdraw.init();

});
