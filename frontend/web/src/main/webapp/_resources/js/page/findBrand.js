$(document).ready(function() {

  $(".popup-sku").on('click','.close-popup',function(){
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
  });

  $("#keywords").on('focus',function () {
    $("#searchBtn").show();
  });

  $("#searchBtn").on('click',function () {
    var keywords = $("#keywords").val();
    window.location.href = 'http://' + window.location.host + '/catalog/allProducts?shopId=' + shopId + '&categoryId=0&keywords=' + keywords;
  });

  var shopId = $("#shopId").val();
  var categoryId = $("#categoryId").val();
  var rootShopId = $("#rootShopId").val();
  var keyword = $("#keyword").val();
  if(keyword && keyword != ''){
    $("#keywords").val(keyword);
    $("#searchBtn").show();
  }

  var findBrand = {
    page: {
      size: 5,
      index: 0
    },

    loading: false,

    init: function() {
      this.loadCategory();
      this.loadProduct();

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          findBrand.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function() {
      if (this.loading == false) {
        this.loading = true;
        var url;
        var data;
        if(keyword != ''){
          url = 'http://' + window.location.host + '/v2/product/searchbyPc/' + rootShopId + '/' + keyword;
          data = {
            'size': this.page.size,
            'page': this.page.index
          };
        }else{
          url = 'http://' + window.location.host + '/v2/product/list/forbrand';
          data = {
            'size': this.page.size,
            'page': this.page.index,
            'categoryId': categoryId,
            'shopId': shopId
          };
        }

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#productData");
                var html = '';
                $.each(result.data, function(index, category) {
                  html = html +
                    '<div class="brand-commodity-show" shopId="' + shopId + '" categoryId="' + category.categoryId + '" onclick="click2brand(this)">' +
                    '<img src="' + category.img + '" alt="" class="brand-show-pic">' +
                    '</div>';
                  if(category.list){
                    $.each(category.list, function(index, product) {
                        html = html +
                          '<div class="wrap-list" > ' +
                          '<div class="commodity-wrap">' +
                          '<img src="' + product.imgUrl + '" alt="" class="more-commodity-pic" productId="' + product.id + '" onclick="productDetail(this)"/>' +
                          '<div  class="more-commodity-des">' +
                          '<p>' + product.name + '</p>' +
                          '</div>' +
                          '<p class="more-commodity-price">￥' + product.price + '</p>' +
                          '<img src="/_resources/image/icon_carta_nml.png" alt="" class="add-cat-icon" onclick="cartShow(\'' + product.id +'\')"/>' +
                          '</div>' +
                          '</div>';
                    });
                  }
                });
                productData.append(html);
                findBrand.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            findBrand.loading = false;
          }
        });
      }
    },

    loadCategory: function() {
        var url = 'http://' + window.location.host + '/v2/category/list/1';
        var data = {
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var categoryData = $("#categoryData");
                var html = '';
                $.each(result.data, function(index, category) {
                  html = html +
                    '<a href="/catalog/allProducts?shopId=' + shopId + '&categoryId=' + category.id + '" class="catagory-item';
                  if(category.id == categoryId){
                    html = html + ' catagory-item-active';
                  }
                  html = html +  '"><span>' + category.name + '</span></a>';
                });
                html = html + '<a href="#" class="search-area"></a>';
                categoryData.append(html);
                findBrand.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            findBrand.loading = false;
          }
        });
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  findBrand.init();

});

// 点击跳转到品牌团明细页面
function click2brand(target) {
  var shopId = $(target).attr("shopId");
  var categoryId = $(target).attr("categoryId");
  window.location.href = '/shop/brand/' + shopId +'/' + categoryId + '?root=true';
}
