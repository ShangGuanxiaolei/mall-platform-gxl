$(document).ready(function () {

  var isUpdateAddress = false;
  var infoT = null;

  $("#js-address-list").on('click', ".js-address-item", function () {
    $(".address-checkbox-checked").each(function () {
      $(this).removeClass("address-checkbox-checked");
    });
    $(this).find(".address-checkbox").addClass("address-checkbox-checked");

    var id = $(this).find(".address-checkbox").attr("id");
    var consignee = $(this).find(".modal-addr-consignee").text();
    var phone = $(this).find(".modal-addr-phone").text();
    var addrDetails = $(this).find(".modal-addr-details").text();
    var certificateId = $(this).find(".address-certificateId").val();

    //修改用户默认地址
    var params = {
      'addressId': id
    };
    var url = '/user/changeDefaultAddress';
    $.getJSON(url, params, function (data) {
      var r = data;
      var $certificateIdView = $('#certificateId_view');
      if (r.rc == 1) {
        $("#addressId").val(id);
        $(".addr-consignee").html(consignee);
        $(".addr-phone").html(phone);
        $(".addr-details").html(addrDetails);
        // $("#certificateId").val(certificateId);
        // $certificateIdView.text('身份证：' + certificateId);
        // if (certificateId && certificateId !== '') {
        //   $certificateIdView.removeClass('hidden');
        // } else {
        //   $certificateIdView.addClass('hidden');
        // }
        $("#addressListModal").modal("hide");
      } else {
        $(".addrls-help-info").css("display", "block");
        infoT = setTimeout(function () {
          $(".addrls-help-info").css("display", "none");
        }, 5000);
      }
    });
    var skuId = $('#skuId').val();
    var qty = $('#qty').val();
    rePricing(skuId, id, qty);
  });

  //新增
  $('body').on('click', '.btn-address-create', function () {
    $('#editAddressModal').find("#id").val("");
    $("#addressListModal").modal("hide");
    $("#editAddressModal").modal("show");
  });
  //编辑
  $('body').on('click', '.btn-address-edit', function () {
    var addrId = $(this).attr("id");
    $('#editAddressModal').find("#id").val(addrId);
    $("#addressListModal").modal("hide");
    $("#editAddressModal").modal("show");
  });

  //初始化地址数据
  $('#editAddressModal').on('show.bs.modal', function () {

    var addrId = $('#editAddressModal').find("#id").val();

    $.getJSON("/user/editAddress?addressId=" + addrId, function (result) {

      var address = result.address;
      var province = result.province;
      var provinceList = result.provinceList;

      for (var i = 0; i < provinceList.length; i++) {
        var selected = "";
        if (province != null && provinceList[i].id == province.id) {
          selected = "selected";
        }
        $('#province').append("<option value='" + provinceList[i].id + "' "
          + selected + ">" + provinceList[i].name + "</option>");
      }

      if (addrId.length > 0) {  //编辑状态
        if (address != null) {
          $('#editAddressModal').find("#zoneId").val(address.zoneId);
          $('#editAddressModal').find("#consignee").val(address.consignee);
          $('#editAddressModal').find("#phone").val(address.phone);
          $('#editAddressModal').find("#street").val(address.street);

          var city = result.city;
          var cityList = result.cityList;

          for (var i = 0; i < cityList.length; i++) {
            var selected = "";
            if (city != null && cityList[i].id == city.id) {
              selected = "selected";
            }
            $('#city').append("<option value='" + cityList[i].id + "' "
              + selected + ">" + cityList[i].name + "</option>");
          }

          var district = result.district;
          var districtList = result.districtList;
          for (var i = 0; i < districtList.length; i++) {
            var selected = "";
            if (district != null && districtList[i].id == district.id) {
              selected = "selected";
            }
            $('#district').append("<option value='" + districtList[i].id + "' "
              + selected + ">" + districtList[i].name + "</option>");
          }
        }
      }
    });
  });

  //初始化地址列表数据
  $('#addressListModal').on('show.bs.modal', function () {
    if (isUpdateAddress) {

      $.getJSON("/user/listAddress", function (result) {
        if (result.errorCode == "200") {

          if (result != null && result.data.length > 0) {
            $("#js-address-list").empty();
            for (var i = 0; i < result.data.length; i++) {
              var id = result.data[i].id;
              var consignee = result.data[i].consignee;
              var phone = result.data[i].phone;
              var details = result.data[i].details;
              var certificateId = result.data[i].certificateId;
              var isDefault = result.data[i].isDefault;
              $("#js-address-list").append('<ul class="fz24 font-dark plr30 addressbox js-address-item">'
                +
                '<li class="bg-white cart-address-line font-dark clearfix t-lf" style="position: relative;" id="'
                + id + '">' +
                '<span data-th-fragment="select" id="' + id
                + '"  class="address-checkbox fl-l ' + (isDefault
                  ? "address-checkbox-checked" : "") + '"></span>' +
                '<span style="margin-left: 8px;">收货人:</span><span class="modal-addr-consignee" >'
                + consignee + '</span>' +
                '<span style="margin-left: 5px;margin-right: 5px;">,</span><span class="ff-numb modal-addr-phone" >'
                + phone + '</span>' +
                '<span class="icon-edit btn-address-edit" style="float:right;margin-right: 5%;font-size: 20px;color:#f36825;" id="'
                + id + '"></span>' +
                '</li><li class="bg-white cart-address-narrow-line clearfix t-lf"><span style="margin-left: 8px;">收货地址:</span>'
                +
                '<span class="modal-addr-details" >' + details + '</span></li>'
                +
                + '</ul>');
            }
            isUpdateAddress = false;
          }
        }
      });
    }
  });

  //数据清理
  $('#editAddressModal').on('hide.bs.modal', function () {

    $('#editAddressModal').find("#id").val("");
    $('#editAddressModal').find("#zoneId").val("");
    $('#editAddressModal').find("#consignee").val("");
    $('#editAddressModal').find("#phone").val("");
    $('#editAddressModal').find("#street").val("");

    $("#addrcr-help-content").css("display", "none");

    clearTimeout(infoT);

    $('#province').empty();
    $('#province').prepend("<option value='0'>--省份--</option>");

    $('#city').empty();
    $('#city').prepend("<option value='0'>--城市--</option>");

    $('#district').empty();
    $('#district').prepend("<option value='0'>--地区--</option>");

  });

  var address = new Address();

  $('#province').on('change', function () {
    $('#district').find('option').each(function () {
      if ($(this).val() > 0) {
        $(this).remove();
      }
    });
    address.initCities($(this).val());
  });

  $('#city').on('change', function () {
    address.initDistricts($(this).val());
  });

  $("#phone").on('blur', function () {
    var regu = /^[1][3-8][0-9]{9}$/;
    var re = new RegExp(regu);
    var phoneobj = $("#phone").val();
    clearTimeout(infoT);
    $("#addrcr-help-content").css("display", "none");
    if (re.test(phoneobj)) {
      $("#addrcr-help-content").css("display", "none");
    } else {
      $("#addrcr-help-content").css("display", "block");
      $("#addrcr-help-content").html("请输入正确的手机号码!");
      infoT = setTimeout(function () {
        $("#addrcr-help-content").css("display", "none");
      }, 5000);
    }
  });

  $("#phone").on('focus', function () {
    $("#addrcr-help-content").css("display", "none");
  });

  function alertHelpInfo(helpInfo) {
    clearTimeout(infoT);
    $("#addrcr-help-content").css("display", "block");
    $("#addrcr-help-content").html(helpInfo);
    infoT = setTimeout(function () {
      $("#addrcr-help-content").css("display", "none");
    }, 5000);

  }

  $('#addrSubmitBtn').on('click', function () {
    var regu = /^[1][3-8][0-9]{9}$/;
    var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    var re = new RegExp(regu);
    var rc = new RegExp(regIdCard);
    if ($('#consignee').val() == '') {
      alertHelpInfo('收货人姓名不能为空');
      return false;
    }
    if (!re.test($("#phone").val())) {
      alertHelpInfo('请输入正确的手机号码!');
      return false;
    }
    if ($('#phone').val() == '') {
      alertHelpInfo('手机号码不能为空');
      return false;
    }
    if ($('#province').val() == '0') {
      alertHelpInfo("省份不能为空");
      return false;
    }
    if ($('#city').find('option').length > 1 && $('#city').val() == '0') {
      alertHelpInfo("市不能为空");
      return false;
    }
    if ($('#district').find('option').length > 1 && $('#district').val()
      == '0') {
      alertHelpInfo("地区不能为空");
      return false;
    }
    if ($('#street').val() == '') {
      alertHelpInfo("街道地址不能为空");
      return false;
    }

    $('#zoneId').val(
      $('#district').val() != '0' ? $('#district').val() : ($('#city').val()
      != '0' ? $('#city').val() : $('#province').val()));

    var consignee = $('#consignee').val();
    var phone = $('#phone').val();
    var zoneId = $('#zoneId').val();
    var provinceId = $('#province').val();
    var cityId = $('#city').val();
    var street = $('#street').val();
    var isDefault = $('#isDefault').val();
    var params = {
      consignee: consignee,
      phone: phone,
      zoneId: zoneId,
      provinceId: provinceId,
      cityId: cityId,
      street: street,
      isDefault: isDefault
    };

    if ($('#id').length > 0) {
      params.id = $('#id').val();
    }
    ;

    $certificateId.val('');
    var url = "/v2/address/save";
    $.getJSON(url, params, function (data) {
      var address = data.data;
      $("#addressId").val(address.id);
      $("#editAddressModal").modal("hide");
      isUpdateAddress = true;

      $("#j-address").find(".addressbox").empty();
      $("#j-address").find(".addressbox").append(
        '<div class="address-info address">' +
        '<div class="clearfix">' +
        '<span class="address-name">收货人：<span class="addr-consignee">'
        + address.consignee + '</span></span>' +
        '<span class="address-phone addr-phone" >' + address.phone + '</span>' +
        '</div>' +
        '<div class="address-detail addr-details" >' + address.details
        + '</div>' +
        '</div>'
      );

      var skuId = $('#skuId').val();
      var qty = $('#qty').val();
      rePricing(skuId, address.id, qty);

      $(".add-address").hide();
    });
    //
    //if (!$('#submitBtn').is('.disabled')) {
    //    $('#submitBtn').removeClass('red').addClass('disabled').attr('disabled', 'disabled');
    //}
  });
});

Address.prototype = {
  initProvinces: function () {
    var url = '/v2/systemRegion/1/children';
    var provinceEle = $('#province');
    $.getJSON(url, function (data) {
      $.each(data.data, function (i, item) {
        provinceEle.append('<option value="' + data[i].id + '">' + data[i].name
          + '</option>');
      });
    });
  },

  initCities: function (provinceId) {
    var url = '/v2/systemRegion/' + provinceId + '/children';
    var cityEle = $('#city');
    cityEle.find('option').each(function () {
      if ($(this).val() > 0) {
        $(this).remove();
      }
    });
    $.getJSON(url, function (data) {
      $.each(data.data, function (index, val) {
        cityEle.append('<option value="' + val.id + '">' + val.name
          + '</option>');
      });
    });
  },

  initDistricts: function (cityId) {
    var url = '/v2/systemRegion/' + cityId + '/children';
    var districtEle = $('#district');
    districtEle.find('option').each(function () {
      if ($(this).val() > 0) {
        $(this).remove();
      }
    });
    $.getJSON(url, function (data) {
      $.each(data.data, function (index, val) {
        districtEle.append('<option value="' + val.id + '">' + val.name
          + '</option>');
      });
    });
  }
};

function Address() {
}

function rePricing(skuId, addressId, qty) {
  var params = {
    skuId: skuId,
    addressId: addressId,
    qty: qty,
    promotionFrom: $('#promotionFrom').val()
  };
  af.getJSON('/v2/order/confirm', params, function (data) {
    if (data.errorCode !== 200) {
      alert('网络错误, 请稍后再试');
      console.log(data.moreInfo);
      return;
    }
    var pricing = data.data.prices;
    if (pricing) {
      $('#logisticsFee').text('￥' + pricing.logisticsFee);
      $('#logisticsDiscount').text('￥' + pricing.logisticsDiscount);
      $('.order-price').text('￥' + pricing.totalFee);
    }
  });
}

function calPostageRet(addressId, skuId, qty) {
  var url = '/v2/address/calPostage';
  $.getJSON(url + '?addressId=' + addressId + '&skuId=' + skuId + '&qty=' + qty,
    function (data) {
      if (data.errorCode !== 200) {
        alert('获取配送费信息错误, 请稍后再试');
        console.log(data.moreInfo);
        return;
      }
      var postage = data.data;
      if (postage) {
        $('#logisticsFee').text('￥' + postage.logisticsFee);
        $('#logisticsDiscount').text('￥' + postage.logisticsDiscount);
      }
    });
}
