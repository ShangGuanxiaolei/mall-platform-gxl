/**
 * Created by quguangming on 16/4/6.
 */

$(document).ready(function() {

  pushHistory();

  // 我的订单返回后直接到我的首页
  window.addEventListener("popstate", function(e) {  //回调函数中实现需要的功能
    location.href='/shop#/me';  //在这里指定其返回的地址
  }, false);

  var oid = '';

  $('.orderListParent').on('click', "#orderDetailBtn", function(event) {
    var orderId = $(this).attr("order-id");
    window.location.href = "/order/"+orderId;
  });

  $('.orderListParent').on('click', "#payBtn", function(event) {
    var orderId = $(this).attr("order-id");
    window.location.href = "/order/"+orderId + "/pay";
  });

  $('.orderListParent').on('click', "#doCancel", function(event) {
    var orderId = $(this).attr("order-id");
    $.getJSON("/user/order/"+orderId+"/cancel", function (result) {
      if (result.errorCode == "200") {
        location.reload();
      }
    });
  });

  <!--点击加入购物车  弹框显示  点击关闭 弹框 不显示-->
  $(".close-popup").click(function(){
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
    return false;
  });

  // 申请退款退货
  $('.orderListParent').on('click', "#refundBtn", function(event) {
    var _this = this;
    var orderId = $(this).attr("order-id");
    var orderStatus = $(this).attr("order-status");
    var msg = '';
    //if(orderStatus == 'SHIPPED'){
    //  msg = '已发货的订单退款将扣除运费,';
    //}
    /**msg = msg + '确定要提交退款申请吗?';
    if (!confirm(msg))
      return;
    oid = orderId;
    $(".popup-sku").show();
    $(".popup-overlay").addClass("visible");**/
    window.location.href = "/user/refundService?orderId=" + orderId;
  });

  $('body').on('click', "#confirmed", function(event) {
      var _this = this;
      var params = {
        refundMemo: $('#refundMemo').val(),
        //refundFee: $('#refundFee').val(),
        //refundReason: $('#refundReason').val(),
        orderId: oid
      };
      $.getJSON('/user/order/requestrefund', params, function(json) {
        alert(json.msg);
        if (json.rc == '1') {
          location.reload();
        } else
          $(_this).removeClass('disable');
      });

  });



  // 取消退款申请
  $('.orderListParent').on('click', "#cancelrefundBtn", function(event) {
    var _this = this;
    var orderId = $(this).attr("order-id");

    if (!confirm("确定要取消退款申请吗?"))
      return;

    if (!$(_this).hasClass('disable')) {
      $(_this).addClass('disable');
      var params = {
      };
      $.getJSON('/user/order/' + orderId +'/cancelrefund', params, function(json) {
        alert(json.msg);
        if (json.rc == '1') {
          location.reload();
        } else
          $(_this).removeClass('disable');
      });

    }

  });

  // 确认收货
  $('.orderListParent').on('click', "#confirmBtn", function(event) {
    var _this = this;
    var orderId = $(this).attr("order-id");

    if (!confirm("确定货物已经收到了吗?"))
      return;

    if (!$(_this).hasClass('disable')) {
      $(_this).addClass('disable');
      var params = {
        'orderId': orderId
      };
      $.getJSON('/v2/api/order/confirmShipped', params, function(result) {
        if (result.errorCode == "200") {
          alert('确认收货成功');
          location.reload();
        }
      });

    }

  });


});

function pushHistory() {
  var state = {
    title: "title",
    url: "/shop#/me"
  };
  window.history.pushState(state, state.title, state.url);
}
