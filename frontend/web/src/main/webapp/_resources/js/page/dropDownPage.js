!function($){
  document.addEventListener('DOMContentLoaded' , function(){
    var requestData ={
      shopId:location.search.match(/shopId=.*/g)[0].split('=')[1],
      size : 5 ,
      page : 0 ,
    }
    var setLisen = function(ele , eventName , callback, pram){
      ele.addEventListener(eventName , function(){
        return callback(pram);
      });
    }
    var reload = function(callback){
      reload = false;
      $.ajax({
          url: '../v2/catalog/allProducts',
          data: requestData,
          type: "GET",
          success: callback,
          error: function() {
            console.log('error');
            reload = reference;
          },
        })
    };
    var reference = reload;
    var dropLoading = function(){
      if(reload && (document.body.offsetHeight - document.body.scrollTop - innerHeight) < 50){
        reload(function(response){
          var data = JSON.parse(response).data.categoryProducts;
          if(data.length){
            appendFrg(data,[
              '<a href="../p/{{href}}">',
              '<div class="goods-image">',
              '<img style="display: inline;" class="lazy" data-original="" src="{{picURL}}">',
              '</div>',
              '<div class="goods-info">',
              '<p class="goods-title">{{title}}</p>',
              '<p class="goods-price">￥{{price}}.00</p>',
              '</div>',
              '</a>'
            ].join(''));
            reload = reference;
            requestData.page++;
          }else{
            console.log('没有更多了');
          }
        });
      }
    };
    var appendFrg = function(data,template){
        var productList = document.getElementsByClassName('goods-list')[0];
        var documentFrag = document.createDocumentFragment();
        for(var i = 0,j = data.length; i<j; i++){
          var li = document.createElement('li');
          li.innerHTML = template.replace(/{{href}}/ , data[i].id).replace(/{{picURL}}/ , data[i].imgUrl).replace(/{{title}}/ , data[i].description).replace(/{{price}}/ , data[i].price);
          documentFrag.appendChild(li);
        }
        productList.appendChild(documentFrag);      
    };

    setLisen(window , 'scroll' ,dropLoading );  
  })

}(jQuery)