$(document).ready(function() {

  $(".popup-sku").on('click','.close-popup',function(){
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
  });

  var shopId = $("#shopId").val();
  var categoryId = $("#categoryId").val();
  var root = $("#root").val();
  if(categoryId == '0'){
    categoryId = '';
  }
  var brandList = {
    page: {
      size: 10,
      index: 0
    },
    loading: false,
    init: function() {
      this.loadProduct();

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          brandList.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function() {
      if (this.loading == false) {
        this.loading = true;
        var url;
        // 如果是总店的品牌团查看明细走一条分支，子店铺首页品牌团走另一条分支
        if(root == 'true'){
          url = 'http://' + window.location.host + '/v2/product/list';
        }else{
          url = 'http://' + window.location.host + '/v2/shop/product/list';
        }
        var data = {
          'size': this.page.size,
          'page': this.page.index,
          'category': categoryId,
          'shopId': shopId
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#productData");
                var html = '';
                $.each(result.data.list, function(index, product) {
                  html = html +
                    '<div class="wrap-list" > ' +
                    '<div class="commodity-wrap">' +
                    '<img src="' + product.imgUrl + '" alt="" class="more-commodity-pic" productId="' + product.id + '" onclick="productDetail(this)"/>' +
                    '<div  class="more-commodity-des">' +
                    '<p>' + product.name + '</p>' +
                    '</div>' +
                    '<p class="more-commodity-price">￥' + product.price + '</p>' +
                    '<img src="/_resources/image/icon_carta_nml.png" alt="" class="add-cat-icon" onclick="cartShow(\'' + product.id +'\')"/>' +
                    '</div>' +
                    '</div>';
                });
                productData.append(html);
                brandList.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            brandList.loading = false;
          }
        });
      }
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  brandList.init();

});
