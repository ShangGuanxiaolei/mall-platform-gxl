/**
 * 初始化数据
 */
window.host = 'http://51shop.mobi/v2';
var  _contents = {
             "phone":'<div class="modal-header" style="padding: 9px 15px;"><a class="close" data-dismiss="modal">×</a>'+
                       '<h3 id="userLoginTitle" style="line-height: 27px;font-size: 18px;margin: 0;color:#f36825;font-weight: bold;">请填写您的手机号码</h3></div>'+
                       '<div class="modal-body"><div class="row"><div class="col-md-12"><div class="widget wgreen"><div class="widget-content"><div class="padd">'+
                       '<form class="form-horizontal" role="form"><div class="form-item"><label for="memberPhone">手机号码</label><input type="tel" class="form-control" '+
                       'id="memberPhone" name="memberPhone" autocomplete="off" maxlength="11" placeholder="手机号码"/>'+
                       '</div><div class="error-info js-help-info">请填写11位手机号码</div></form></div></div></div></div></div></div><div class="modal-footer"><a href="#" class="btn btn-warning btn-lg btn-block" id="checkPhone">确认手机号码</a> </div>',
          "register":  '<div class="modal-header" style="padding: 9px 15px;"><a class="close" data-dismiss="modal">×</a>'+
                       '<h3 id="userLoginTitle" style="line-height: 27px;font-size: 18px;margin: 0;color:#f36825;font-weight: bold;">注册新账号</h3></div>'+
                       '<div class="modal-body"><div class="row"><div class="col-md-12"><div class="widget wgreen"><div class="widget-content"><div class="padd">'+
                       '<form class="form-horizontal" role="form"><div class="form-item"><label for="memberPhone">手机号码</label><input type="tel" class="form-control" '+
                       'id="memberPhone" name="j_username" autocomplete="off" maxlength="11" placeholder="手机号码" disabled="disabled" value=""/>'+
                       '<div class="form-item js-member-captcha" id="js-member-captcha" name="js-member-captcha"><label for="captcha">验证码</label> <input type="text" class="form-control" style="width: 200px;" '+
                       'id="captcha" name="captcha" autocomplete="off" maxlength="6" placeholder="请输入短信验证码"  value=""/><button type="button" data-text="获取验证码" class="btn btn-warning btn-captcha">获取验证码</button></div>'+
                       '<div class="form-item js-member-pwd" id="js-member-pwd" name="js-member-pwd"><label for="memberPwd">密码</label> <input type="password" class="form-control" '+
                       'id="memberPwd" name="j_password"  autofocus="autofocus" autocomplete="off" placeholder="请输入8-20位数字和字母组合密码" value=""/>'+
                       '</div></div><div class="error-info js-help-info">请填写您的登录密码</div></form></div></div></div></div></div></div><div class="modal-footer"><a href="#" class="btn btn-warning btn-lg btn-block" id="btnRegister">确认</a> '+
                       '<div class="bottom-tips js-bottom-cg" id="js-bottom-cg" name="js-bottom-cg"><span class="s-orange">如果您忘了密码，请</span><a href="javascript:;" '+
                       'class="a-blue js-change-pwd">点我找回密码</a> <a href="javascript:;" class="a-blue js-change-phone" style="margin-left: 30px;">更换手机号</a></div></div>',
              "login":  '<div class="modal-header" style="padding: 9px 15px;"><a class="close" data-dismiss="modal">×</a>'+
                        '<h3 id="userLoginTitle" style="line-height: 27px;font-size: 18px;margin: 0;color:#f36825;font-weight: bold;">该号码已注册过,请直接登录</h3></div>'+
                        '<div class="modal-body"><div class="row"><div class="col-md-12"><div class="widget wgreen"><div class="widget-content"><div class="padd">'+
                        '<form class="form-horizontal" role="form"><div class="form-item"><label for="memberPhone">手机号码</label><input type="tel" class="form-control" '+
                        'id="memberPhone" name="j_username" autocomplete="off" maxlength="11" placeholder="手机号码" disabled="disabled" value=""/>'+
                        '<div class="form-item js-member-pwd" id="js-member-pwd" name="js-member-pwd"><label for="memberPwd">密码</label> <input type="password" class="form-control" '+
                        'id="memberPwd" name="j_password"  autofocus="autofocus" autocomplete="off" placeholder="请输入登录密码" />'+
                        '</div></div><div class="error-info js-help-info"></div></form></div></div></div></div></div></div><div class="modal-footer"><a href="#" class="btn btn-warning btn-lg btn-block" id="btnLogin">确认</a> '+
                        '<div class="bottom-tips js-bottom-cg" id="js-bottom-cg" name="js-bottom-cg"><span class="s-orange">如果您忘了密码，请</span><a href="javascript:;" '+
                        'class="a-blue js-change-pwd">点我找回密码</a> <a href="javascript:;" class="a-blue js-change-phone" style="margin-left: 30px;">更换手机号</a></div></div>',
            "changePwd":'<div class="modal-header" style="padding: 9px 15px;"><a class="close" data-dismiss="modal">×</a>'+
                        '<h3 id="userLoginTitle" style="line-height: 27px;font-size: 18px;margin: 0;color:#f36825;font-weight: bold;">找回账号密码</h3></div>'+
                        '<div class="modal-body"><div class="row"><div class="col-md-12"><div class="widget wgreen"><div class="widget-content"><div class="padd">'+
                        '<form class="form-horizontal" role="form"><div class="form-item"><label for="memberPhone">手机号码</label><input type="tel" class="form-control" '+
                        'id="memberPhone" name="j_username" autocomplete="off" maxlength="11" placeholder="手机号码" disabled="disabled" value=""/>'+
                        '<div class="form-item js-member-captcha" id="js-member-captcha" name="js-member-captcha"><label for="captcha">验证码</label> <input type="text" class="form-control" style="width: 200px;" '+
                        'id="captcha" name="captcha" autocomplete="off" maxlength="6" placeholder="请输入短信验证码"  value=""/><button type="button" data-text="获取验证码" class="btn btn-warning btn-captcha">获取验证码</button></div>'+
                        '<div class="form-item js-member-pwd" id="js-member-pwd" name="js-member-pwd"><label for="memberPwd">密码</label> <input type="password" class="form-control" '+
                        'id="memberPwd" name="j_password"  autofocus="autofocus" autocomplete="off" placeholder="请输入8-20位数字和字母组合密码" value=""/>'+
                        '</div></div><div class="error-info js-help-info"></div></form></div></div></div></div></div></div><div class="modal-footer"><a href="#" class="btn btn-warning btn-lg btn-block" id="btnRePwd">确认</a> '+
                        '<div class="bottom-tips js-bottom-lg" id="js-bottom-lg" name="js-bottom-lg"><a href="javascript:;" class="a-blue inline-item js-login">已有账号登录</a>'+
                        '<a href="javascript:;" class="a-blue js-register" style="margin-left: 12px;">注册新账号</a></div></div>'

               };

var UserSignin = UserSignin || {};
var infoTime = null;
UserSignin.login = {
    init: function() {

    },
    verifyPhone: function(val) {
        return val = "" + val, /^1\d{10}$/.test(val);
    },
    verifyCaptcha: function(val) {
        return val = "" + val, /^\d{6}$/.test(val);
    },
    verifyPwd:function(val) {
        var w = /[a-zA-Z]/g
          , d = /[0-9]/g
          , l = /^.{6,50}$/
          , i = 0
          , wo = w.test(val)
          , ds = d.test(val)
          , ls = l.test(val);
        return ls && (wo || ds ) && (i = 1),i;
    },
    verifyForm: function(){
        var that = this;
        var phoneObj = $("#memberPhone");
        var pwdObj = $("#memberPwd");
        var captchaObj = $("#captcha");
        if (phoneObj) {
          if (!that.verifyPhone(phoneObj.val())) {
            that.alertInfo("请填写11位手机号码。");
            return false;
          }
        }
        if (captchaObj) {
          if (!that.verifyCaptcha(captchaObj.val())) {
            that.alertInfo("请输入格式正确的验证码。");
            return false;
          }
        }
        if (pwdObj) {
          if (!that.verifyPwd(pwdObj.val())) {
            that.alertInfo("请输入至少6位的密码。");
            return false;
          }
        }
        return true;
    },
    trimSpace: function(val) {
        return val.replace(/\s/g, "")
    },
    checkSignined: function(nextUrl,successCallBack) {   //检查用户是否已登录
      var url = '/user/checkSignin';
      var that = this;
      //用户登录验证
      $.getJSON(url, function(data) {
          if (data.rc == 1){ //登录验证成功
               //需求修改，不在发送请求，直接拼写url跳转到next页面
              successCallBack();
          } else{
              that.showLoginEvent(nextUrl);
          }
      });
    },
    showLoginEvent: function(nextUrl){
        $("#modal-content").html(_contents.phone);
        $("#userLogin").attr("next-url",nextUrl);
        $("#userLogin").modal("show");
        this.checkPhoneEvent();
    },
    checkPhoneEvent: function() {
      var that = this;
      $('#checkPhone').bind('click', function () {
          var phone = $('#memberPhone').val();
          if (!that.verifyPhone(phone)) {
              that.alertInfo("请填写11位手机号码。");
              return false;
          }
          var params = {
            'memberPhone': phone
          };
          var url = '/user/checkRegister';
          $.post(url, params, function (data) {
              var r = data;
              if (r.rc == 1) {  //注册用户
                that.loginView(phone);
              } else {
                that.regView(phone);
              }
          });
      });
    },
    loginView: function(phone){
        var that = this;
        $("#modal-content").html(_contents.login);
        $("#modal-content #memberPhone").val(phone);
        $('#btnLogin').bind('click', function () {
          that.login();
        });
        that.phoneView(phone);
        that.passwordView(phone);
    },
    regView: function(phone){
        var that = this;
        $("#modal-content").html(_contents.register);
        $("#modal-content #memberPhone").val(phone);
        $("#modal-content #captcha").val('');
        $("#modal-content #memberPwd").val('');
        $('#btnRegister').bind('click', function () {
            that.register();
        });
        that.phoneView(phone);
        that.passwordView(phone);
        that.sendCaptchaEvent(phone);
    },
    phoneView: function(phone){
        var that = this;
        $('.js-change-phone').bind('click', function(){
          $("#modal-content").html(_contents.phone);
          $("#modal-content #memberPhone").val(phone);
            that.checkPhoneEvent();
        });
    },
    passwordView:function(phone){
      var that = this;
      $('.js-change-pwd').bind('click', function(){
          $("#modal-content").html(_contents.changePwd);
          $("#modal-content #memberPhone").val(phone);
          $('#btnRePwd').bind('click', function(){
               that.changePwd();
          });
          $('.js-login').bind('click', function(){
              $("#modal-content").html(_contents.phone);
              $("#modal-content #memberPhone").val(phone);
               that.checkPhoneEvent();
          });

          $('.js-register').bind('click', function(){
              $("#modal-content").html(_contents.phone);
              $("#modal-content #memberPhone").val(phone);
              that.checkPhoneEvent();
          });
          that.sendCaptchaEvent(phone);
      });
    },
    validateCaptchaEvent: function(){
          var that = this;
          $('#captcha').on('blur', function() {
                var code = $('#captcha').val();
                var phone = $('#memberPhone').val();

                if (code.length < 1) {
                  return false;
                }

                if ((that.verifyCaptcha(code)) && (that.verifyPhone(phone))) {
                  var url = window.host;
                  var data = {
                      mobile: phone,
                      smsCode: code
                  };
                  var btnReg = $('#btnRegister')[0];
                  var url = window.host;
                  if (btnReg) {
                      url = url + '/register/verify';
                  } else {
                      url = url + '/pwdModify/verify';
                  }

                  that.postMethod(url, data, function(result) {
                    if (typeof(result) === 'object') {
                        switch (result.errorCode) {
                            case 200: break;
                            case -1:
                              that.alertInfo('验证码不正确！');
                              break;
                            default:
                              that.alertInfo(result.moreInfo);
                        }
                    }
                  });
                } else {
                  that.alertInfo('验证码格式不正确！');
                }
          });
    },
    sendCaptchaEvent: function(phone){   //验证码
       var that = this;
       that.validateCaptchaEvent();
       $('.btn-captcha').bind('click', function(){
             //确定注册和登录
             var btnReg = $('#btnRegister')[0];
             var url = window.host;
             if (btnReg){
                 url = url + '/send-sms-code';
             } else{
                 url = url + '/forget-pwd';
             }
             var data = {
               mobile: phone
             };
             var waitTime = 60;
             var btnCaptcha = $(this);
             function countdownTimer() {
                 if (waitTime === 0) {
                     btnCaptcha.removeAttr("disabled");
                     btnCaptcha.text('发送验证码');
                     waitTime = 60;
                 } else {
                     btnCaptcha.attr("disabled","disabled");
                     btnCaptcha.text('剩余' + waitTime + 's...');
                     waitTime--;
                     setTimeout(function() {
                       countdownTimer();
                     }, 1000);
                 }
             }
             countdownTimer();

             that.postMethod(url, data, function(result) {
                 if (typeof(result) === 'object') {
                   switch (result.errorCode) {
                     case 200:
                       that.alertInfo('验证码发送成功，请查收短信。');
                       break;
                     default:
                       that.alertInfo('验证码发送失败！');
                       break;
                   }
                 }
              });
       });
    },
    register: function(){
        var that = this;
        if( that.verifyForm()){
             var phone = $("#memberPhone").val();
             var pwd = $("#memberPwd").val();
             var code = $("#captcha").val();
             var url = window.host + '/register/create';
             var data = {
                mobile: phone,
                smsCode: code,
                pwd: CryptoJS.MD5(pwd).toString()
             };
            that.postMethod(url, data, function(result) {
                if (typeof(result) === 'object') {
                    switch (result.errorCode) {
                      case 200:
                        that.loginView(phone);
                        that.alertInfo('注册成功,请登录!');
                        break;
                      case -1:
                        that.alertInfo(result.moreInfo);
                        break;
                      default:
                        that.alertInfo(result.moreInfo);
                    }
                }
            });
        }
    },
    changePwd: function(){
        var that = this;
        if( that.verifyForm()){
            var phone = $("#memberPhone").val();
            var pwd = $("#memberPwd").val();
            var code = $("#captcha").val();

            var url = window.host + '/validate-forget-pwd';
            var data = {
              mobile: phone,
              smsCode: code,
              pwd: CryptoJS.MD5(pwd).toString()
            };
            that.postMethod(url, data, function(result) {
              if (typeof(result) === 'object') {
                  switch (result.errorCode) {
                      case 200:
                        that.loginView(phone);
                        that.alertInfo('修改密码成功,请登录!');
                        break;
                      case -1:
                        that.alertInfo(result.moreInfo);
                        break;
                      default:
                        that.alertInfo(result.moreInfo);
                        break;
                  }
              }
            });

        }
    },
    login: function() {
           var that = this;
           var userName = $("#memberPhone").val();
           var pwd = $("#memberPwd").val();
           if (!that.verifyPhone(userName)){
                that.alertInfo("请填写11位手机号码。");
               return false;
           }
           if (!that.verifyPwd(pwd)){
               that.alertInfo("请输入至少6位的密码。");
              return false;
           }
           var url = window.host + "/signin_check";
           var params = {
                    u: userName,
                    p: CryptoJS.MD5(pwd).toString()
           };
           return this.postMethod(url, params, function(c) {
              if ("object" == typeof c)
                 switch (c.errorCode) {
                    case 200:
                       var nextURL= $("#userLogin").attr("next-url");
                       window.location.href = nextURL;
                       break;
                    default:
                       that.alertInfo("登录失败，手机号码或密码错误!");
                       break;
                 }
           });
      },
    hideInfo: function(){
           clearTimeout(infoTime);
           $(".js-help-info").css("display","none");
    },
    alertInfo: function(msg){       //窗口提示信息显示
            $('.js-help-info').html(msg);
            $('.js-help-info').css('display', 'block');
            infoTime = setTimeout(function() {
              $(".js-help-info").css("display","none");
            },5000);
    },
    postMethod: function(url, data, callback) {
            var that = this;
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                dataType: "JSON",
                success: function(a) {
                  callback(a)
                },
                error: function() {
                  that.alertInfo('服务器暂时没有响应，请稍后重试。');
                  callback(-1)
                },
                complete: function() {
                  callback(0)
                }
            })
    }
};


$(function() {
  UserSignin.login.init();
});





