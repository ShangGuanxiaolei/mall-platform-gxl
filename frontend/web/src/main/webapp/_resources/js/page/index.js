$(document).ready(function() {
  // 根据浏览器地址判断点击的是哪个页签，点亮点击的页签
  var href = window.location.href;
  if(href.indexOf('/shop/') > -1){
    $(".nav-index-dark").addClass("nav-index-light").removeClass("nav-index-dark");
  }else if(href.indexOf('/flashsale/') > -1){
    $(".nav-sale-dark").addClass("nav-sale-light").removeClass("nav-sale-dark");
  }else if(href.indexOf('/catalog/') > -1){
    $(".nav-more-dark").addClass("nav-more-light").removeClass("nav-more-dark");
  }else if(href.indexOf('/cart/') > -1){
    $(".nav-car-dark").addClass("nav-car-light").removeClass("nav-car-dark");
  }else if(href.indexOf('/user/') > -1){
    $(".nav-my-dark").addClass("nav-my-light").removeClass("nav-my-dark");
  }

});
