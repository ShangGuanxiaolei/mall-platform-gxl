$(document).ready(function() {

  $(".popup-sku").on('click','.close-popup',function(){
    $(".popup-sku").hide();
    $(".popup-overlay").removeClass("visible");
  });

  $("#keywords").on('focus',function () {
    $("#searchBtn").show();
  });

  $("#searchBtn").on('click',function () {
    var keywords = $("#keywords").val();
    window.location.href = 'http://' + window.location.host + '/catalog/allProducts?shopId=' + shopId + '&categoryId=' + categoryId+ '&keywords=' + keywords;
  });

  var shopId = $("#shopId").val();
  var rootShopId = $("#rootShopId").val();
  var categoryId = $("#categoryId").val();
  var keyword = $("#keyword").val();
  if(keyword && keyword != ''){
    $("#keywords").val(keyword);
    $("#searchBtn").show();
  }
  if(categoryId == '0'){
    categoryId = '';
  }
  var findMore = {
    page: {
      size: 10,
      index: 0
    },

    loading: false,

    init: function() {
      $('body').append('<div id="loading" style="position:absolute;top:0;left:0;z-index:99;width:' + 
                        innerWidth + 
                        'px;height:' + 
                        innerHeight + 
                        'px;background:url(\'../_resources/images/b2b/loading.gif\') center center no-repeat"></div>'
      );
      this.loadCategory();
      this.loadProduct(function(){
        $('#loading').remove();
      });

      //binding
      window.onscroll = function() {
        var dch = getClientHeight();
        var scrollTop = getScrollTop();
        var scrollBottom = document.body.scrollHeight - scrollTop;
        if(scrollBottom >= dch && scrollBottom <= (dch+10)){
          console.log("You're at the bottom of the page.");
          findMore.loadProduct();
        }
      };

      $(".orderListParent").on('click', '#orderDetailBtn' ,function(event){
        var orderId = $(event.toElement).attr("order-id");
        window.location.href = "/order/"+orderId;
      });
    },

    loadProduct: function(callback) {
      if (this.loading == false) {
        this.loading = true;
        var url;
        var data;
        if(keyword != ''){
          url = 'http://' + window.location.host + '/v2/product/searchbyPc/' + rootShopId + '/' + keyword;
          data = {
            'size': this.page.size,
            'page': this.page.index
          };
        }else{
          url = 'http://' + window.location.host + '/v2/product/list';
          data = {
            'size': this.page.size,
            'page': this.page.index,
            'category': categoryId,
            'shopId': rootShopId
          };
        }


        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var productData = $("#productData");
                var html = '';
                $.each(result.data.list, function(index, product) {
                  html = html +
                    '<div class="wrap-list" >' +
                    '<div class="commodity-wrap">' +
                    '<img src="' + product.imgUrl + '" alt="" class="more-commodity-pic" productId="' + product.id + '" onclick="productDetail(this)"/>' +
                    '<div  class="more-commodity-des">' +
                    '<p>' + product.name + '</p>' +
                    '</div>' +
                    '<p class="more-commodity-price">￥' + product.price + '</p>' +
                    '<div class="add-cat-icon" onclick="cartShow(\'' + product.id +'\')"></div>' +
                    '</div>' +
                    '</div>';
                });
                productData.append(html);
                findMore.page.index += 1;
                if(typeof callback === 'function') {
                  callback();
                }
                break;
              case -1:
                break;
              default:
                break;
            }
            findMore.loading = false;
          }
        });
      }
    },

    loadCategory: function() {
        var url = 'http://' + window.location.host + '/v2/category/list/1';
        var data = {
        };

        this.postMethod(url, data, function(result) {
          if (typeof(result) === 'object') {
            switch (result.errorCode) {
              case 200:
                var categoryData = $("#categoryData");
                var html = '';
                $.each(result.data, function(index, category) {
                  var tempCategoryId = category.id;
                  if(tempCategoryId == '0'){
                    tempCategoryId = '';
                  }
                  html = html +
                    '<a href="/catalog/allProducts?shopId=' + shopId + '&categoryId=' + category.id + '" class="catagory-item';
                  if(tempCategoryId == categoryId){
                    html = html + ' catagory-item-active';
                  }
                  html = html + '">' + category.name + '</a>';
                });
                categoryData.append(html);
                findMore.page.index += 1;
                break;
              case -1:
                break;
              default:
                break;
            }
            findMore.loading = false;
          }
        });
    },

    postMethod: function(url, data, callback) {
      $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json",
        success: function(a) {
          callback(a)
        },
        error: function() {
          callback(-1)
        },
        complete: function() {
          callback(0)
        }
      })
    }
  };

  findMore.init();

});
