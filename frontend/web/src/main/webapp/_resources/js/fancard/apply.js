$(document).ready(function () {

  const fanCode = $('#fanCode').text();
  if (fanCode && fanCode !== '') {
    showCode(fanCode);
  }

  $(".shadow-close").click(function () {
    location.href = '/fancard/center';
  });

  /* 表单提交点击时间 */
  $('#submit').on('click', function (e) {
    var formData = $('form.table-form').serializeObject();
    if (validateData(formData, errorMsg)) {
      $.post('/v2/fanCard/apply', formData)
      .done(function (res) {
        if (res.errorCode === 200) {
          if (res.data) {
            $.post('/v2/fanCard/viewCode')
            .done(function (res) {
              if (res.errorCode === 200) {
                var code = res.data;
                if (code && code !== '') {
                  successMsg('申请成功');
                  setTimeout(function () {
                    showCode(code);
                  }, 2000);
                } else {
                  errorMsg('申请失败, 请稍后再试', '抱歉')
                }
              } else {
                errorMsg(res.moreInfo);
              }
            })
          }
        } else {
          errorMsg(res.moreInfo);
        }
      });
    }
  });

});

const checker = {
  name: function (name) {
    return name && name !== '';
  },
  gender: function (gender) {
    return gender && gender !== '' && ['M', 'FM'].indexOf(gender) !== -1;
  },
  age: function (age) {
    return age && age !== '';
  },
  ageRange: function (age) {
    var intAge = parseInt(age);
    return intAge > 0 && intAge <= 150;
  },
  phone: function (phone) {
    return phone
      && /(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/.test(
        phone);
  },
  idCard: function (idCard) {
    return idCard && idCard !== '';
  },
  idCardRegex: function (idCard) {
    return idCard
      && /^[1-9]\d{13,16}[a-zA-Z0-9]$/.test(idCard);
  },
  email: function (email) {
    return email && /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/.test(email);
  },
  wechatNo: function (wechatNo) {
    return wechatNo && wechatNo !== '';
  },
  address: function (address) {
    return address && address !== '';
  }
};

const validationRules = {
  name: [
    {
      checker: checker.name,
      message: '请填写姓名'
    }
  ],
  gender: [
    {
      checker: checker.gender,
      message: '性别请填写M, FM'
    }
  ],
  age: [
    {
      checker: checker.age,
      message: '请填写年龄'
    },
    {
      checker: checker.ageRange,
      message: '请填写正确的年龄'
    }
  ],
  phone: [
    {
      checker: checker.phone,
      message: '请输入正确的电话号码'
    }
  ],
  idCard: [
    {
      checker: checker.idCard,
      message: '请输入身份证号码'
    },
    {
      checker: checker.idCardRegex,
      message: '请输入15到18位的身份证号码'
    }
  ],
  email: [
    {
      checker: checker.email,
      message: '请输入正确的邮箱'
    }
  ],
  wechatNo: [
    {
      checker: checker.wechatNo,
      message: '请填写微信号'
    }
  ],
  address: [
    {
      checker: checker.address,
      message: '请填写地址'
    }
  ]
};

/**
 * 参数校验
 * @param data 参数对象
 * @param messageHandler 处理错误消息的函数
 * @returns {boolean}
 */
function validateData(data, messageHandler) {
  for (var k in data) {
    if (data.hasOwnProperty(k)) {
      if (validationRules.hasOwnProperty(k)) {
        var value = data[k];
        // 循环参数校验
        for (var i in validationRules[k]) {
          if (validationRules[k].hasOwnProperty(i)) {
            var checkObj = validationRules[k][i];
            var checker = checkObj.checker;
            if (!checker.call(null, value)) {
              messageHandler(checkObj.message);
              return false;
            }
          }
        }
      }
    }
  }
  return true
}

