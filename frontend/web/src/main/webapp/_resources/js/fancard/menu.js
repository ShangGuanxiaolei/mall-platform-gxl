$(document).ready(function () {

  $("div.shadow").click(function () {
    $(".shadow").css({"display": "none"});
    forbidScroll = false;
  });

  $('#show-code').on('click', function (e) {
    e.preventDefault();
    $.post('/v2/fanCard/viewCode')
    .done(function (res) {
      if (res.errorCode === 200) {
        var code = res.data;
        if (code && code !== '') {
          showCode(code);
        } else {
          message('您还没有申请过范卡，正在为您跳转到申请页面...');
          setTimeout(function () {
            location.href = '/fancard/apply';
          }, 2000);
        }
      } else {
        alert(res.moreInfo);
      }
    })
  });

});

function showCode(code) {
  $('p.shadow-code').text(code);
  $('div.shadow').show();
}

