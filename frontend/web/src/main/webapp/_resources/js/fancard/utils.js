var forbidScroll = false;

window.addEventListener("touchmove", function (e) {
  if (forbidScroll) {
    event.preventDefault();
    event.stopPropagation();
  }
}, {passive: false});

window.addEventListener('mousewheel', function (event) {
  if (forbidScroll) {
    event.preventDefault();
    event.stopPropagation();
  }
});

function message(message) {
  swal({
    text: message,
    timer: 2000,
    button: false
  });
}

function errorMsg(message, title) {
  swal({
    title: title || '提示',
    text: message,
    timer: 2000,
    button: false,
    icon: 'error'
  });
}

function successMsg(title, message) {
  swal({
    title: title,
    text: message,
    timer: 2000,
    button: false,
    icon: 'success',
    className: 'swal-title'
  });
}

function showCode(code) {
  $('p.shadow-code').text(code);
  $('div.shadow').show();
  forbidScroll = true;
}


