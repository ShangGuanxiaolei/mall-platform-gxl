var sessionId = Request.sessionId; //获取sessionId/
// var sessionId = '539024769748639744';
// var goHost = 'http://139.224.130.139:1699';
//点击开始红包雨
var packetStart = function() {
	$(".box1,.shade_bg,.bottom_img,.packet_box").show();
	setTimeout(function() {
		$(".box1").hide();
		packetRain();
	}, 2000)
}
//开启红包雨动画
var packetRain = function() {
	//返回红包队列
	$.ajax({
		type: 'GET',
		url: host + 'v1/point/packet/rain/generate',
		contentType: "application/json",
		beforeSend: function(XMLHttpRequest) {
			XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
		},
		success: function(data) {
			if (!data.data) {
				var errMsg = data.error;
				// console.log(errMsg);
				$(".box1,.shade_bg,.bottom_img,.packet_box").hide();
				$('.hint_packet').text(errMsg).show();
				var msgTimer = setTimeout(function() {
					$('.hint_packet').text(errMsg).hide();
				}, 3000)
				return false;
			} else {
				$(".box2").show();
				var win = (parseInt($(".couten").css("width")));
				$(".mo").css("height", $(document).height());
				$(".couten").css("height", $(document).height());
				// $(".backward").css("height", $(document).height()); 
				$("li").css({});
				var point = 0; //单个红包德分
				var pointNum = 0; //红包个数
				var packetPoint = 0; //返回的红包总数
				var dataArray = []; //返回的红包队列

				var del = function() {
					nums++;
					//					console.info(nums);
					//					console.log($(".li" + nums).css("left"));
					$(".li" + nums).remove();
					setTimeout(del, 500)
				}

				var Obj = data;
				var arrayPoint = Obj.data.allQueue;

				// var arrayPoint = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
				var getArray = arrayPoint;
				var arraylength = arrayPoint.length;
				var ss = 100 - arraylength;
				console.log(getArray, 'getArray');

				for (var i = 0; i < ss; i++) {
					var point = parseInt(Math.random() * arrayPoint.length);
					arrayPoint.splice(point, 0, 0);
				}
				console.log(arrayPoint, 'arrayPoint');

				var add = function() {
					var point = parseInt(Math.random() * (10 - 0) + 0);
					var hb = parseInt(Math.random() * (3 - 1) + 1);
					var Wh = parseInt(Math.random() * (70 - 30) + 30);
					var Left = parseInt(Math.random() * (win - 0) + 0);
					var rot = (parseInt(Math.random() * (45 - (-45)) - 45)) + "deg";
					//				console.log(rot)
					num++;
					$(".couten").append("<li class='li" + num + "' ><a href='javascript:;'><img src='img/hb_" + hb +
						".png'></a><div class='point'>+<span>" + point + "</span></div></li>");
					$(".li" + num).css({
						"left": Left,
					});
					$(".li" + num + " a img").css({
						"width": Wh,
						"transform": "rotate(" + rot + ")",
						"-webkit-transform": "rotate(" + rot + ")",
						"-ms-transform": "rotate(" + rot + ")",
						/* Internet Explorer */
						"-moz-transform": "rotate(" + rot + ")",
						/* Firefox */
						"-webkit-transform": "rotate(" + rot + ")",
						/* Safari 和 Chrome */
						"-o-transform": "rotate(" + rot + ")" /* Opera */
					});
					$(".li" + num).animate({
						'top': $(window).height() + 20
					}, 5000, function() {
						//删掉已经显示的红包
						this.remove()
					});
					//点击红包的时显示德分,为0消失
					$(".li" + num).click(function() {
						packetPoint += point;
						pointNum = pointNum + 1;
						var len = getArray.length;
						for (var i = 0; i < len; i++) {
							if (getArray[i] == point && point > 0) { //删除红包队列中领取过的红包
								getArray.splice(i, 1);
							}
						}
						// getArray.push(point);
						console.log(getArray, '=getArray');
						$('#addPoint').text(packetPoint);

						if (point == 0) {
							$(this).remove();

						} else {
							$(this).find('.point').show();

						}
					});
					// console.log(num,'==num');
					setTimeout(add, 200)
				}


				//增加红包
				var num = 0;
				var packetTimer = setTimeout(add, 4000);

				$('.isOK').on('click', function() {
					$('.ok_box,.shade_bg,.bottom_img').hide();
					//显示返回按钮
					$('.go_back').show();
					// clearInterval(packetTimer);
					// window.location.reload();
					//移除红包队列中的0
					for (var i = 0; i < getArray.length; i++) {
						if (getArray[i] > 0) {
							dataArray.push(getArray[i]);
						}
					}

					var res = {
						sumPoint: packetPoint,
						surplusQueue: dataArray
					}

					$.ajax({
						type: 'POST',
						data: JSON.stringify(res),
						url: host + 'v1/point/packet/rain/settlement',
						contentType: "application/json",
						beforeSend: function(XMLHttpRequest) {
							XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
						},
						success: function(data) {
							if (data.data == true) {
								console.log('提交成功');
								window.location.reload();
							} else {
								console.log('提交失败');
								window.location.reload();
							}
						},
						error: function(e) {
							console.log(e);
							wx.miniProgram.navigateTo({url: '/pages/authorize'});
						}
					})
				})

				//开始倒数计时
				var backward = function() {
					numz--;
					if (numz < 0) {
						return false;
					} else if (numz > 0) {
						$(".box2 p").html(numz);
					} else {
						$(".box2 p").html('GO');
						setTimeout(function() {
							$(".box2").remove();
							$(".box3,.box3 h4,.getPoint").show();
						}, 1000)

					}
					setTimeout(backward, 1000)

				}

				var numz = 4;
				backward();

				//倒计时15s
				var backward1 = function() {
					numz1--;
					if (numz1 < 0) {
						return false;
					} else if (numz1 > 0) {
						$(".box3 p").html(numz1);
					} else {
						$(".box3,.couten").remove();
						$(".packet_box").hide();
						$('#getNum').text(pointNum);
						$('#getPoint').text(packetPoint);
						console.log(pointNum, packetPoint);
						$('.ok_box').show();
						// 			window.location.reload()
						// 			clearInterval(packetTimer);
					}
					setTimeout(backward1, 1000)
					// console.log(numz1,'==numz1');
				}

				var numz1 = 20;
				backward1();
			}
		},
		error: function(e) {
			console.log(e);
			wx.miniProgram.navigateTo({url: '/pages/authorize'});
		}
	})

}

//抽奖
var luck = {
	index: -1, //当前转动到哪个位置，起点位置
	count: 0, //总共有多少个位置
	timer: 0, //setTimeout的ID
	speed: 20, //初始转动速度
	times: 0, //转动次数
	cycle: 50, //转动基本次数：即至少需要转动多少次再进入抽奖环节
	prize: -1, //中奖位置
	init: function(id) {
		if ($("#" + id).find(".luck-unit").length > 0) {
			$luck = $("#" + id);
			$units = $luck.find(".luck-unit");
			this.obj = $luck;
			this.count = $units.length;
			$luck.find(".luck-unit-" + this.index).addClass("active");
		};
	},


	roll: function() {
		var index = this.index;
		var count = this.count;
		var luck = this.obj;
		$(luck).find(".luck-unit-" + index).removeClass("active");
		index += 1;
		if (index > count - 1) {
			index = 0;
		};
		$(luck).find(".luck-unit-" + index).addClass("active");
		this.index = index;
		return false;
	},
	stop: function(index) {
		this.prize = index;
		$(luck).find(".luck-unit").removeClass("active");
		$(luck).find(".luck-unit-" + index).addClass("active");
		//显示返回按钮
		$('.go_back').show();
		console.log(this.prize, '==this.prize');
		return false;
	},
	lucyData: function() {
		var sessionId = '539024769748639744';
		//抽奖返回数据
		$.ajax({
			type: 'post',
			url: host + 'v1/point/packet/lottery',
			contentType: "application/json",
			timeout : 1000, //超时时间设置
			beforeSend: function(XMLHttpRequest) {
				XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
			},
			success: function(data) {
				if (data.data) {
					var times = data.data.times; //剩余次数
					$('#btn p').html('（' + times + '次）');
					if (data.data.type == 1) {
						var stopIndex = data.data.prize.point;
						console.log(stopIndex, '===stopIndex');
						$('.lucky_hint_box p').html('运气不错，获得<em>' + stopIndex + '</em>德分');
						$('.lucky_img').hide();
						$('.packet_img').attr('src', '../img/' + stopIndex + 'defen@2x.png').show();
						$('.lucky_name').text(stopIndex + '德分');
						$('.hint_btn').text('再玩一次');

						switch (stopIndex) {
							case 2:
								luck.stop(0);
								clearTimeout(luck.timer);
								break;
							case 6:
								luck.stop(2);
								clearTimeout(luck.timer);
								break;
							case 8:
								luck.stop(4);
								clearTimeout(luck.timer);
								break;
							case 10:
								luck.stop(6);
								clearTimeout(luck.timer);
								break;
							default:
								break;
						}

					} else if (data.data.type == 2) {
						var stopIndex2 = data.data.prize.rank;
						$(luck).find(".luck-unit").removeClass("active");
						$(luck).find(".luck-unit-" + stopIndex2).addClass("active");
						var luckyName = $(luck).find(".luck-unit-" + stopIndex2).find('p').text();
						$('.lucky_hint_box p').html('运气爆棚！获得' + stopIndex2 + '等奖');
						$('.packet_img').hide();
						$('.lucky_img').attr('src', '../img/' + stopIndex2 + 'dengjiang@2x').show();
						$('.lucky_name').text(luckyName);
						$('.hint_btn').text('立即查看');
						switch (stopIndex2) {
							case 1:
								luck.stop(7);
								clearTimeout(luck.timer);
								break;
							case 2:
								luck.stop(3);
								clearTimeout(luck.timer);
								break;
							case 3:
								luck.stop(5);
								clearTimeout(luck.timer);
								break;
							default:
								break;
						}
					} else {
						clearTimeout(luck.timer);
						$('.hint_packet').text('服务器繁忙，稍后请重试～').show();
						var msgTimer = setTimeout(function() {
							$('.hint_packet').text('服务器繁忙，稍后请重试～').hide();
						}, 3000)
					}
				} else {
					var errMsg = data.error;
					luck.stop(-1);
					luck.prize = -1;
					luck.times = 0;
					clearTimeout(luck.timer);
					$('.hint_packet').text(errMsg).show();
					var msgTimer = setTimeout(function() {
						$('.hint_packet').text(errMsg).hide();
					}, 3000)
				}
			},
			complete : function(XMLHttpRequest,status){
		　　　　if(status=='timeout'){//超时
		 　　　　　 	ajaxTimeoutTest.abort();
		　　　　　  	clearTimeout(luck.timer);
					$('.hint_packet').text('服务器繁忙，稍后请重试～').show();
					var msgTimer = setTimeout(function() {
						$('.hint_packet').text('服务器繁忙，稍后请重试～').hide();
					}, 3000)
		　　　　}
		　　 },
			error: function(e) {
				console.log(e);
				wx.miniProgram.navigateTo({url: '/pages/authorize'});
			}
		})
	}
};


function roll() {
	luck.times += 1;
	luck.roll();
	if (luck.times > luck.cycle + 10 && luck.prize == luck.index) {
		//抽奖停止，显示弹出层
		$('.lucky_hint_box').show();
		luck.lucyData();
		clearTimeout(luck.timer);
		luck.prize = -1;
		luck.times = 0;
		click = false;
	} else {
		if (luck.times < luck.cycle) {
			luck.speed -= 10;
		} else if (luck.times == luck.cycle) {
			var index = Math.random() * (luck.count) | 0;
			luck.prize = index;
			console.log(index, '===index');
			//抽奖停止
			luck.lucyData();
			clearTimeout(luck.timer);
			luck.prize = -1;
			luck.times = 0;
			click = false;

		} else {
			console.log(122332);
			if (luck.times > luck.cycle + 10 && ((luck.prize == 0 && luck.index == 7) || luck.prize == luck.index + 1)) {
				luck.speed += 110;
			} else {
				luck.speed += 20;
				console.log(20);
			}
		}
		if (luck.speed < 40) {
			luck.speed = 40;
		};

		luck.timer = setTimeout(roll, luck.speed);
	}

	return false;
}

//中奖名单滚动
var speedText = 60;

function Marquee() {
	if (slide2.offsetTop - slide.scrollTop <= 0)
		slide.scrollTop -= slide1.offsetHeight
	else {
		slide.scrollTop++
	}
}

var click = false;
$(function() {
	//初始化抽奖
	luck.init('luck');

	//点击抽奖切换按钮
	$('.lottery_btn').on('click', function() {
		if ($('.btn_box').hasClass('lucky_bg')) {
			return false
		} else {
			//隐藏返回按钮
			$('.go_back').hide();

			//闪灯效果
			var num = 0;
			$(".shanDeng").attr("class", function() {
				var shanDengTimer = setInterval(function() {
					num++;
					if (num % 2 == 0) {
						$('#deng').addClass("shanDeng2");
					} else {
						$('#deng').addClass("shanDeng");
						$('#deng').removeClass('shanDeng2');
					}
				}, 500)
			})

			var MyMar = setInterval(Marquee, speedText);

			$(this).addClass('active').siblings().removeClass('active');
			$('.btn_box').addClass('lucky_bg').removeClass('packet_bg');
			$('.packet_center,.packet_order,.packet_rule').hide();
			$('.lucky_box,.luck_order,.rule_box,.hint').show();

			//抽奖返回数据
			$.ajax({
				type: 'GET',
				url: host + 'v1/point/packet/lottery/show',
				contentType: "application/json",
				beforeSend: function(XMLHttpRequest) {
					XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
				},
				success: function(data) {
					if (data.errorCode == 501) {
						var errMsg = data.error;
						$('.hint_packet').text(errMsg).show();
						var msgTimer = setTimeout(function() {
							$('.hint_packet').text(errMsg).hide();
						}, 3000)
						$('#btn').attr('disabled', "true");
						$('#btn').click(function(e) {
							e.stopPropagation(); //阻止事件冒泡
						})
						console.log(11111111);
						return false;
					} else {
						$('#btn p').html('（' + data.data.times + '次）');
						$('#btn').removeAttr("disabled");
					}
				},
				error: function(e) {
					console.log(e);
					wx.miniProgram.navigateTo({url: '/pages/authorize'});
				}
			})
		}

	})

	//点击红包处，消失提示框
	$(document).click(function() {
		$('.lucky_hint_box').hide();
	})
	$('.lucky_hint_box').click(function(e) {
		e.stopPropagation(); //阻止事件冒泡
	})

	//点击抽奖或者再来一次按钮
	$("#btn,.one_more").on('click',function() {
		//隐藏提示框
		$('.lucky_hint_box').hide();

		//按下弹起效果
		$("#btn").addClass("cjBtnDom");
		setTimeout(function() {
			$("#btn").removeClass("cjBtnDom");
		}, 200);
		console.log(click, '==click');
		if (click) {
			return false;
		} else {
			//点击抽奖扣除德分
			var consumePoint = 10;
			$.ajax({
				type: 'POST',
				url: host + '/v2/point/packet/pointrain/consume?consumePoint=' + consumePoint,
				contentType: "application/json",
				beforeSend: function(XMLHttpRequest) {
					XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
				},
				success: function(data) {
					if (data.errorCode == 200) {
						luck.speed = 100;
						roll();
						click = false;
						return false;
					} else {
						var errMsg = data.moreInfo;
						$('.hint_packet').text(errMsg).show();
						var msgTimer = setTimeout(function() {
							$('.hint_packet').text(errMsg).hide();
						}, 3000)
						click = true;
						return false;
					}
				},
				error: function(e) {
					console.log(e);
					wx.miniProgram.navigateTo({url: '/pages/authorize'});
				}
			})

		}

	});

	//中奖名单
	// var sessionId = '539024769748639744';
	$.ajax({
		type: 'GET',
		url: host + '/v2/winning/getWinningUserList',
		contentType: "application/json",
		beforeSend: function(XMLHttpRequest) {
			XMLHttpRequest.setRequestHeader("thirdSessionId", sessionId)
		},
		success: function(data) {
			if (!data) {
				return false;
			} else {
				var str = '';
				for (var i = 0; i < data.data.length; i++) {
					str += "<p>" + data.data[i].userName + "<span>" + data.data[i].lucky + "</span></p>"

				}
				$('#slide1').html(str);
				//文字无缝滚动
				var slide = document.getElementById("slide");
				var slide2 = document.getElementById("slide2");
				var slide1 = document.getElementById("slide1");
				slide2.innerHTML = slide1.innerHTML;
				var MyMar = setInterval(Marquee, speedText);

			}
		}
	})
});
