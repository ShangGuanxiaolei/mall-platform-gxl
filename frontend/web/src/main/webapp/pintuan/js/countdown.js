function countDown(count, processing, finish) {
    function callback() {
        if (countDown > 0) {
            countDown--;
            processing(countDown);
        } else {
            clearInterval(timeId);
            finish()
        }
    }
    var countDown = count;
    var timeId = setInterval(callback, 1000)
};
var countDownDomList = document.querySelectorAll('.js-count-down-item')
countDownDomList.forEach(function (item) {
    var start = item.querySelector('.js-countdown-start').innerHTML
    var end = item.querySelector('.js-countdown-end').innerHTML

    var current = item.querySelector('.js-current-time').innerHTML

    if(current < start){
        item.querySelector('.js-countdown').innerHTML = '未开始'
        return
    }else if(current>end){
        item.querySelector('.js-countdown').innerHTML = '已结束'
        return
    }

    var count = parseInt((end - current) / 1000)

    countDown(count, function (time) {
        var dd=Math.floor(time/(60*60*24))>0 ? Math.floor(time/(60*60*24)) + '天' + '&nbsp' : ''   //天
        var hh=Math.floor((time%(60*60*24))/(60*60))<0 ? '0' + Math.floor((time%(60*60*24))/(60*60)) : Math.floor((time%(60*60*24))/(60*60)) //小时
        // var hh=Math.floor(time/3600)<10 ? '0'+ Math.floor(time/3600):Math.floor(time/3600) //小时
        var min=parseInt((time % (60 * 60))/60)<10 ? '0' + parseInt((time % (60 * 60))/60):parseInt((time % (60 * 60))/60) //分钟
        var ss=Math.ceil(time%60%60)<10 ? '0'+ Math.ceil(time%60%60) : Math.ceil(time%60%60) //秒
        item.querySelector('.js-countdown').innerHTML =dd + hh + ':' + min + ':' + ss

    }, function () {
        item.querySelector('.js-countdown').innerHTML = '结束'
    })
})