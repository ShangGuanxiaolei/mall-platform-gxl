/*global module:false*/

module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        // Metadata.
        // Task configuration.  

        pkg: grunt.file.readJSON('package.json'),

        less: {
            zt: {
                files: {
                    'css/index.css': 'less/index.less',
                    'css/login.css': 'less/login.less'
                },
                options: {
                    'yuicompress': true
                }
            }
        },
        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'js_src',
                    src: '**/*.js',
                    dest: 'js/'
                }]
            },
            third :{
                files: [{
                    expand: true,
                    cwd: 'js/third',
                    src: '*.js',
                    dest: 'js/third/'
                }]
            }
        },
        watch: {
            css: {
                files: 'less/*.less',
                tasks: ['less'],
                options: {
                    nospawn: false
                }
            },
            js: {
                files: 'js_src/*.js',
                tasks: ['uglify:my_target'],
                options: {
                    nospawn: false
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task.
    grunt.registerTask('default', ['less', 'uglify', 'watch']);

};