//商品详情弹窗
var KD = KD || {};
KD.prodetail = {
    setProDetail: function() {
        //轮播
        var nowPos, imgLength, imgSign, waitTemp;
        var imgCarousel = {
            init: function() {
                clearInterval(imgRepeat);
                clearTimeout(waitTemp);
                var proId = $("#prodetailbox").parents("div.dialog:first").attr("data-proid");
                $.ajax({
                    url: host + '/product/' + proId,
                    type: 'POST',
                    error: function(state) {
                        if (state.status == 401) {
                            KD.API.goLogin();
                        } else {

                        }
                    },
                    success: function(data) {
                        if (data.errorCode === 200) {
                            $(".proinfo h1").html(data.data.name);
                            $(".proinfo h2 span").html(data.data.price);
                            imgCarousel.renderCode(data.data);
                            imgCarousel.load(data.data);

                            //console.log(data.data);
                            //微博分享参数配置
                            var summary = "商品名称："+data.data.name;
                            var proUrl = getabsoluteurl('/p/'+data.data.id);
                            jiathis_config = {
                                summary: summary,
                                url: proUrl,
                                pic:data.data.imgs[0].imgUrl || ''
                            }
                        } else {
                            console.log(data);
                        }
                    }
                })
            },
            load: function(data) {
                //初始化
                //从data里面加载商品图片
                //console.log(data.imgs)
                var loadLength = data.imgs.length;
                $(".prodetailbox ul").empty();
                for (var i = 0; i < loadLength; i++) {
                    var htm = '<li><img src="' + data.imgs[i].imgUrl + '" alt="" title="" /></li>';
                    $("#prodetailbox ul").append(htm);
                }
                imgLength = $("#prodetailbox ul li").length;
                if (loadLength === 1) {
                    //一张图不轮播
                    $("#prodetailbox ul").stop().css({'width':'390px','left':0});
                    return false;
                }
                $(".prodetailbox ul").append($(".prodetailbox ul li:first").clone());
                imgLength += 1; //图片个数
                $(".prodetailbox ul").css('width', 390 * imgLength + 100);
                imgSign = 1; //按钮索引
                $(".prodetailbox ul li").each(function() {
                    $(this).attr("data-imgsign", imgSign);
                    if (imgSign == imgLength) {
                        return false;
                    };
                    $(".proimgitem").append("<i data-imgsign='" + imgSign + "'></i>");
                    imgSign += 1;
                });
                nowPos = 1;
                $(".proimgitem i:first").addClass("active");
                clearInterval(imgRepeat);
                imgCarousel.imgAuto();
            },
            imgAnimate: function() {
                $("#prodetailbox .proimglist ul").stop().animate({
                    "left": -nowPos * 390
                },
                function() {
                    $(".proimgitem i").removeClass("active");
                    $("[data-imgsign='" + (nowPos + 1) + "']").addClass("active");
                    if (nowPos == (imgLength - 1)) {
                        $(".prodetailbox ul").css("left", 0);
                        nowPos = 1;
                        $(".proimgitem i").removeClass("active");
                        $("[data-imgsign='1']").addClass("active");
                        return false;
                    }
                    nowPos += 1;
                });
            },
            imgAuto: function() {
                waitTemp = setTimeout(function() {
                    imgRepeat = setInterval(function() {
                        var imgLength = $("#prodetailbox ul li").length;
                        if (imgLength === 1) {
                            //一张图不轮播
                            clearInterval(imgRepeat);
                            $("#prodetailbox ul").stop().css({'width':'390px','left':0});
                            return false;
                        }
                        imgCarousel.imgAnimate();
                    },
                    2000);
                },
                2000);
            },
            imgClick: function() {
                clearInterval(imgRepeat);
                clearTimeout(waitTemp);
                $(".proimgitem i").removeClass("active");
                nowPos = parseInt($(this).attr("data-imgsign"), 10);
                $(this).addClass("active");
                $("#prodetailbox .proimglist ul").stop().animate({
                    "left": -(nowPos - 1) * 390
                },
                function() {
                    imgCarousel.imgAuto();
                })
            },
            renderCode: function(data) {
                var iRender = '';
                try {
                    document.createElement('canvas').getContext('2d');
                    var addDiv = document.createElement('div');
                    iRender = 'canvas';
                } catch(e) {
                    iRender = 'table';
                };
                var proAddress = data.productUrl;

                $('.procode').qrcode({
                    render: iRender,
                    text: proAddress,
                    width: 146,
                    height: 146
                });
            }
        };
        imgCarousel.init();
        //绑定事件
        function openDetail() {
            $("#prodetailbox").show();
            imgCarousel.init();
            return false;
        };
        $("body").off("click", ".proimgitem i", imgCarousel.imgClick);
        $("body").on("click", ".proimgitem i", imgCarousel.imgClick);
    }
}
KD.prodetail.setProDetail();



function getabsoluteurl(url) {
    var img = new Image();
    img.src = url; // 设置相对路径给image, 此时会发送出请求
    url = img.src; // 此时相对路径已经变成绝对路径
    img.src = null; // 取消请求
    return url;
}
