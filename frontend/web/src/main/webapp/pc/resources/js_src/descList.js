var KD = KD || {};
KD.desclist = {
    init: function() {
        this.getData();
        this.submit();
    },
    getData: function() {
        var addTpl = $('.tpl-fragment').html();
        var doTtmpl = doT.template(addTpl);
        var box = $('#describeList .list tbody');
        KD.API.getFragment(function(data) {
            box.empty();
            if (!data.data.length) return;
            $.each(data.data, function(i, el) {
                var imgs = [],
                    keys = [],
                    src;
                $.each(el.imgs, function(index, img) {
                    imgs.push(img.imgUrl);
                    keys.push(img.img);
                });
                if (el.imgs[0]) {
                    src = el.imgs[0].imgUrl;
                }
                var data = {
                    id: el.id,
                    name: el.name,
                    description: el.description,
                    imgs: imgs.toString(),
                    keys: keys.toString(),
                    src: imgs[0],
                    showModel: el.showModel
                }
                box.append(doTtmpl(data));
            });
            initUI(box);
        });
    },
    submit: function() {
        var addTpl = $('.tpl-desc').html();
        var doTtmpl = doT.template(addTpl);
        //buttonSave
        $('#buttonSave').on('click', function() {
            var list = [];
            $('#describeList [name="productId"]:checked').each(function(i, el) {
                var $pare = $(el).closest('tr');
                var data_id = $pare.attr('data-id');
                if (!$('.descList .pro-desibe[data-id="' + data_id + '"]').length) {
                    var data = {
                        id: data_id,
                        name: $pare.find('.pro-text').text(),
                        description: $pare.find('.pro-desc').text(),
                        imgs: $pare.find('.pro-img64').attr('srcs'),
                        keys: $pare.find('.pro-img64').attr('keys'),
                        src: $pare.find('.pro-img64').attr('src'),
                        showModel: $pare.attr('data-showmodel')
                    }
                    $('.descList').append(doTtmpl(data));
                }
            });
            KD.product.initUi();
            $('#describeList .close').trigger('click');
        });
    }
}
KD.desclist.init();