//
var KD = KD || {};
var mmt = moment();
KD.myproduct = {
    init: function() {
        this.curr = 1;
        //打开设置邮费弹窗
        this.openTransPop();
        //下架
        this.downs();
        //上架
        this.ups();

        //获取link里面status值
        this.setStatus();
        //获取商品列表并渲染
        this.getProduct();

        //combox改变值时改变form表单值的状态
        this.changeStatus();

        //排序
        this.orders();

        //搜索
        this.search();

    },
    //设置状态 使列表跳到对应状态
    setStatus : function(){ 
        var that = this;
        var link = $('.navTab-tab li[tabId="myproduct"]').attr('url');
        if (link.indexOf('?') < 0) {
            return;
        }
        var search = link.split('?')[1];
        var status = KD.main.query('status', search);
        var kv = {
            "ONSALE":"onsale",
            "DRAFT":"draft",
            "FORSALE":"delay"
        }
        if( status ){
            $('select[name="productStatus"]').find('option[value="'+ kv[status] +'"]').attr('selected','selected');
            that.initParams(0,0,0,0,0,kv[status]);
        }
    },
    //搜索
    search: function() {
        //搜索操作，回车和搜索icon都绑定搜索事件，当搜索框的内容为空的时候，默认展现当前combox状态下的全部商品，搜索框有内容的时候，按照“search”状态走搜索流程
        var that = this;
        var ipt = $('.searchWrap .searchTxt');

        function searchFunc(){
            var v = $('select[name="productStatus"]').val();
            var orderHandel = $('.proList .thead em');
            var reg = /^\s*$/;
            if (reg.test(ipt.val())) {
                ipt.val('');
            };
            if (ipt.val()==='') {
                //当前商品状态的商品列表组装，仿照select的change
                that.initParams(0, 0, 0, 0, 0, v);
                if (v == 'onsale') {
                    orderHandel.show();
                    $('.j-downs').css('display','inline-block');
                    $('.j-ups').hide();
                } else if(v == 'offsale'){
                    orderHandel.hide();
                    $('.j-downs').hide();
                    $('.j-ups').css('display','inline-block');
                }else{
                    orderHandel.hide();
                    $('.j-downs').hide();
                    $('.j-ups').hide();
                }
                $('[name="pageNum"]').val(1);
                that.getProduct();
            }else{
                if (ipt.val().length>100) {
                    $('.searchWrap .searchTxt').val(ipt.val().slice(0,100));
                };
                that.initParams(1, 0, 0, 0, 0, 'search');
                $('[name="pageNum"]').val(1);
                that.getProduct();
            }

            //搜索的时候，select的展示改为“所有商品”
            var slt = $('select[name="productStatus"]');
            var strAdd = '';
            var strOpt = '';
            if (slt.find('option[name="all"]').length) {
                strAdd = '';
            }else{
                strAdd = '<option value="all">全部商品</option>';
            }
            strOpt = slt.html()+strAdd;
            slt.html(strOpt).val('all');
            slt.siblings('a').html('所有商品').attr('value','all');
            var Pid = slt.parent('.select').attr('id');
            $('#op_'+Pid+' li:not(".li-all") a').removeClass('selected');
            if (!$('#op_'+Pid+' li.li-all').length) {
                $('#op_'+Pid).append('<li class="li-all"><a class="selected" href="#" value="all">所有商品</a></li>');
            };
        }
        ipt.on('keydown', function(ev) {
            var ev = ev || event;
            if (ev.keyCode == 13) {
                searchFunc();
            }
        });
        $('.pro .searchBar .searchWrap i').on('click',function(){
            searchFunc();
        });
    },
    //点击分页的回调 重新获取列表
    submitForm: function() {
        this.getProduct();
        return false;
    },
    //设置邮费弹出
    openTransPop: function() {
        $('.j-setTrans').showPop({
            target: $('.j-pop'),
            close: $('.j-pop-close'),
            modal: true,
            beforeCheck: function() {
                //return $('tbody .icheck').vals() === null
            },
            beforeCallBack: function() {
                alertMsg.error('请选择要设置邮费的商品！');
            }
        });
    },
    //单个商品下架回调
    down : function(res,state){
        if( res.data ){
            alertMsg.correct('下架成功！');
            KD.myproduct.getProduct();
        }else{
            alertMsg.error('商品由于参加活动未能下架成功！');
        }
    },
    //批量下架
    downs: function() {
        var that = this;
        $('.j-downs').off('click');
        $('.j-downs').on('click', function() {
            if ($('tbody .icheck').vals() === null) {
                alertMsg.error('请选择要下架的商品！');
                return;
            }
            alertMsg.confirm("确认下架这些商品？", {
                okCall: function() {
                    KD.API.batchInstock($('tbody .icheck').vals(), function(data) {
                        // that.getProduct();
                        if (data.data.failList.length) {
                            alertMsg.correct('部分商品由于参加活动未能下架成功！');
                        } else {
                            alertMsg.correct('下架成功！',setTimeout(function(){
                                if (!$('table.list tobdy tr').length) {
                                    //console.log($('.j-next').prev('.j-num a'))
                                    // $('.j-next').prev('.selected.j-num').find('a').click();
                                    $('.proList [name=pageNum]').val(  $('.proList [name=pageNum]').val() > 1 ? Number($('.proList [name=pageNum]').val()) - 1 : 1 );
                                    that.getProduct();
                                };
                            },1500));
                        }
                    });
                }
            });
        });
    },
    //单个商品上架回调
    up : function(res,state){
        if( res.data ){
            alertMsg.correct('上架成功！');
            KD.myproduct.getProduct();
        }else{
            alertMsg.error('上架失败！');
        }
    },
    //批量上架商品
    ups: function() {
        var that = this;
        $('.j-ups').on('click', function() {
            if ($('tbody .icheck').vals() === null) {
                alertMsg.error('请选择要上架的商品！');
                return;
            }
            alertMsg.confirm("确认上架这些商品？", {
                okCall: function() {
                    KD.API.batchOnsale($('tbody .icheck').vals(), function(data) {
                        // that.getProduct();
                        if (data.data.failList.length) {
                            alertMsg.correct('部分商品由于参加活动未能上架成功！');
                        } else {
                            alertMsg.correct('上架成功！',setTimeout(function(){
                                if (!$('table.list tobdy tr').length) {
                                    //console.log($('.j-next').prev('.j-num a'))
                                    // $('.j-next').prev('.j-num').find('a').click();
                                    $('.proList [name=pageNum]').val(  $('.proList [name=pageNum]').val() > 1 ? Number($('.proList [name=pageNum]').val()) - 1 : 1 );
                                    that.getProduct();
                                };
                            },1500));
                        }
                    });
                }
            });
        });
    },
    //获取商品列表
    getProduct: function() {
        var that = this;
        var tmpl = $('.tpl-proList').html();
        var size = $('[name="pageSize"]').val();
        var pageNum = $('[name="pageNum"]').val();
        var pageOffset = (pageNum - 1) * size;
        var orderField = $('[name="orderField"]').val();
        var direction = $('[name="direction"]').val();
        var keyword = $('.searchWrap .searchTxt').val();
        var status = $('[name="status"]').val();
        that.curr = pageNum;
        var options = {};
        options.order = orderField;

        if (status == 'delay') {
            $('.proList .col2 i').html('计划发布时间');
        } else {
            $('.proList .col2 i').html('销量');
        }

        if (orderField) {
            options.direction = direction;
        }
        if (keyword) {
            options.keyword = keyword;
        }
        options.page = pageNum - 1;
        options.size = size;

        KD.API.getProductList(status, options, function(data) {
            var doTtmpl = doT.template(tmpl);
            $('.proList .list tbody').html(doTtmpl(data));
            $('.pagination').attr('totalCount', data.data.categoryTotal).attr('numPerPage', size).attr('currentPage', that.curr);
            initUI($('.pro .proList'));
            //分页数据请求加载之后重置全选
            $('.proList .list tfoot input.checkAll').iCheck('uncheck');


        }, function(code) {

        });
    },
    //更改列表类型
    changeStatus: function() {
        var that = this;
        var orderHandel = $('.proList .thead em');
        $('select[name="productStatus"]').on('change', function() {
            var v = $(this).val();
            var _this = $(this);
            
            //下拉菜单中的“所有商品”显示与隐藏操作
            var Pid = _this.parent('.select').attr('id');
            _this.find('option[value="all"]').remove();
            $('#op_'+Pid+' li.li-all').remove();

            $(this).find('option[name="search"]').hide();
            that.initParams(0, 0, 0, 0, 0, v);
            if (v == 'onsale') {
                orderHandel.show();
                $('.j-downs').css('display','inline-block');
                $('.j-ups').hide();
            } else if(v == 'offsale'){
                orderHandel.hide();
                $('.j-downs').hide();
                $('.j-ups').css('display','inline-block');
            }else{
                orderHandel.hide();
                $('.j-downs').hide();
                $('.j-ups').hide();
            }
            $('[name="pageNum"]').val(1);
            that.getProduct();
        });
    },
    //更改查询条件
    initParams: function(search, offset, orderSales, orderCount, orderPrice, status) {
        
        if (!search) {
            $('.searchWrap .searchTxt').val('');
        } else {
            $('[name="keyword"]').val(search);
            $('.proList .thead em').hide();
        }
        $('[name="pageOffset"]').val(offset);

        if (orderSales) {
            $('[name="orderField"]').val('sales');
            $('[name="direction"]').val(orderSales);
        } else if (orderCount) {
            $('[name="orderField"]').val('amount');
            $('[name="direction"]').val(orderCount);
        } else if (orderPrice) {
            $('[name="orderField"]').val('price');
            $('[name="direction"]').val(orderPrice);
        } else {
            $('[name="orderField"]').val('');
            $('[name="direction"]').val('');
        }

        if (status) {
            $('[name="status"]').val(status);
        }
    },
    //排序
    orders: function() {
        var that = this;
        //解决噁心的dwz问题 防止多次触发
        $('body').off('click', '.j-order');
        $('body').off('click', '.j-order-sales');
        $('body').off('click', '.j-order-amount');
        $('body').off('click', '.j-order-price');

        $('body').on('click', '.j-order', function() {
            if ($(this).hasClass('asc')) {
                //现在是非升序 改成升序
                $(this).removeClass('asc').addClass('desc').attr('data-dir', 'desc');
                $(this).html('&#xe612;');
            } else {
                $(this).removeClass('desc').addClass('asc').attr('data-dir', 'asc');
                $(this).html('&#xe613;');
            }
            $('[name="pageNum"]').val(1);
        });
        $('body').on('click', '.j-order-sales', function() {
            that.initParams(0, 0, $(this).attr('data-dir'), 0, 0);
            that.getProduct();
        });
        $('body').on('click', '.j-order-amount', function() {
            that.initParams(0, 0, 0, $(this).attr('data-dir'), 0);
            that.getProduct();
        });
        $('body').on('click', '.j-order-price', function() {
            that.initParams(0, 0, 0, 0, $(this).attr('data-dir'));
            that.getProduct();
        });
    }
}

KD.myproduct.init();