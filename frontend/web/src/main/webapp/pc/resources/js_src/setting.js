var KD = KD || {};
KD.setting = { 
	init: function(){
		function isIE() { //ie?  
			if (!!window.ActiveXObject || "ActiveXObject" in window)  
				return true;  
			else{
				return false;
			}
		};
		var thisIsIE = isIE();
		var danbao = 1;
		$("body").on("click",".checkbtn",function(){
			if(danbao){
				$(this).removeClass("need");
				$(this).addClass("notneed");
				if(thisIsIE){
					$(this).css("backgroundPosition","-60px 0");
				};
				$(".danbaocheck").val(false);
				danbao = 0;
			}else{
				$(this).removeClass("notneed");
				$(this).addClass("need");
				if(thisIsIE){
					$(this).css("backgroundPosition","0 0");
				};
				$(".danbaocheck").val(true);
				danbao = 1;
			}
		});
		$("body").on("click",".removemap",function(){
			var that = $(this);
			var thatUl = $(this).parents("ul:first");
			var thatLi = $(this).parents("li.custompost:first");
			$(this).parents("li:first").hide("fast",function(){
				that.parents("li:first").remove();
				if(!thatLi){
					return false;
				}
				if(thatUl.find("li").length == 0){
					thatLi.prev().remove();
					thatLi.remove();
				}
			})
		});
		
		//表单提交
		$('body').on('click','#J_setSubmit',function(e){
			e.preventDefault();
			var danbaoCheck = $('#J_shopSet .danbaocheck').val();
			console.log(danbaoCheck)
			if (danbaoCheck=='false') {
				alertMsg.error('请阅读并同意担保交易！');
				return false;
			}else{
				var param = {
					name : $('#J_shopSet input[name="name"]').val(),
					wechat : $('#J_shopSet input[name="wechat"]').val(),
					description : $('#J_shopSet textarea[name="description"]').val(),
					provinceId : $('#J_shopSet a[name="provinceId"]').attr('value'),
					cityId : $('#J_shopSet a[name="cityId"]').attr('value'),
					bulletin : $('#J_shopSet textarea[name="bulletin"]').val(),
					banner : $('#J_shopSet input[name="banner"]').val(),
					danbao : danbaoCheck
				};
				$.ajax({
					
                    url: host + '/shop/update',
                    type: 'POST',
                    data: param,
                    dataType: 'json',
                    success: function(data) {
                        if (data.errorCode == 200) {
                            $('#alertBackground').show();
                            $('.loginInfo-name,.userInfo .nick').text(data.data.name);
							alertMsg.correct('您的数据提交成功！',setTimeout(function(){
								$('.siderLink a[rel="setting"]').click();
								$('#alertBackground').hide();
							},1500));
                        } else {
                            alertMsg.error(data.moreInfo);
                        }
                    }
                });
			}
		});



		$("body").on("click",".addcustompost span",function(){
			var listNum = new Date().getTime();
			if(KDZoneList){
			}else{
				KD.zone.getList();
			}
			var htm =	'<div class="horizon"></div>'+
						'<li class="custompost">'+
							'<ul>'+
								'<li class="h5li">'+
									'<h5>自定义邮费区域设置</h5>'+
								'</li>'+
								'<li>'+
									'<label>邮费：</label>'+
									'<input class="required setinput cuspos" name="totalpost" type="text" size="30" />'+
									'<span class="yuan">元</span>'+
								'</li>'+
								'<li class="postlist">'+
									'<label for="freemap">区域：</label>'+
									'<ul>'+
									'</ul>'+
								'</li>'+
								'<li>'+
									'<label>&nbsp;</label>'+
									'<select class="combox" name="province" ref="combox_city_'+listNum+'" refUrl="'+ host +'/zone/{value}/children">'+
										'<option value="1">所有省市</option>'+
										KDZoneList+
									'</select>'+
									'<select class="combox" name="city" id="combox_city_'+listNum+'" ref="combox_area_'+listNum+'" refUrl="'+ host +'/zone/{value}/children">'+
										'<option value="1">所有城市</option>'+
									'</select>'+
									'<button class="addfreemap" type="button">添加</button>'+
								'</li>'+
							'</ul>'+
						'</li>';
			$(this).parents("li.h5li:first").prev().before(htm);
			$("select.combox",'.setting').combox();
		});
		function addfreepost(){
			var id = $(this).prev().find("a").attr("value");
			var name = $(this).prev().find("a").html();
			var canAddSign = 1;
			if(id === 'all' || id === 'null' || !canAddSign){
				var id = $(this).prev().prev().find("a").attr("value");
				var name = $(this).prev().prev().find("a").html();
			}
			$(this).parents("ul:first").find(".postlist ul li").each(function(){
				if($(this).find("span").attr("val") === id){
					canAddSign = 0;
				}
			});
			if(id === 'all' || id === 'null' || !canAddSign){
				return false;
			}
			var temp_htm =	'<li>'+
								'<span class="freeaddress" val="'+id+'">'+name+'</span>'+
									'<em class="removemap">&#xe602;</em>'+
							'</li>';
			$(this).parents("li:first").prev().find("ul").append(temp_htm);
		};
		$("body").off("click",".addfreemap",addfreepost);
		$("body").on("click",".addfreemap",addfreepost);
	}
}
$(function(){
	KD.setting.init();
});

