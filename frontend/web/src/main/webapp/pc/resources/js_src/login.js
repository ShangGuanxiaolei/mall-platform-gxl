var KD = KD || {};
KD.login = {
    init: function() {
        this.renderCode();
        this.callCode();
        this.loginEvent();
        $('.ipt').placeholder();
        this.initForm();
    },
    initForm: function () {
        //cookie
        $(".username").val($.cookie("username"));
        // $(".password").val($.cookie("password"));
    },
    save: function () {
        var username = $(".username").val();
        var password = $(".password").val();
        $.cookie("username", username, { expires: 7 });
        // $.cookie("password", password, { expires: 7 });
    },

    loginEvent: function() {
        var that=this;
        var btn = $('.login_btn');
        var un = $('.username'); 
        var pwd = $('.password');
        var username, password;
        var loginUrl = '/v2/signin_check';
        var doing = false;
        pwd.on('keydown', function(ev) {
            var ev = ev || event;
            if (ev.keyCode == 13) {
                btn.trigger('click');
            }
        })
        btn.on('click', function() {
            if (doing) return;
            doing = true;
            username = un.val();
            password = pwd.val();
            if (username == '') {
                alert('用户名不能为空~');
                un.focus();
                doing = false;
                return;
            }
            if (password == '') {
                alert('密码不能为空~');
                pwd.focus();
                doing = false;
                return;
            }
            $('#pwd').val(CryptoJS.MD5(password));
            var data = {
                u: username,
                p: $('#pwd').val()
            }
            $.ajax({
                url: loginUrl,
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function(res) {
                    if( res.errorCode == 200 ){
                        that.save();
                        //success
                        location.href = 'index.html';
                    }else if( res.errorCode == 401 ){
                        alert('用户名或密码错误!');
                        pwd.focus();
                        doing = false;
                        return;
                    }else{

                    }
                    console.log(res);
                },
                error: function() {

                },
                complete: function() {
                    doing = false;
                }
            })
        });
    },
    callCode: function() {
        var caller = $('.codeCaller');
        var target = $('.codeBox');
        caller.on('mouseenter', function() {
            target.fadeIn();
        });

        target.add(caller).on('click', function(ev) {
            ev.stopPropagation();
        });
        $(document).on('click', function() {
            target.fadeOut();
        });
    },
    renderCode: function() {
        var iRender = '';
        try {
            document.createElement('canvas').getContext('2d');
            var addDiv = document.createElement('div');
            iRender = 'canvas';
        } catch (e) {
            iRender = 'table';
        };
        var iosAddress = 'https://itunes.apple.com/us/app/yi-heng-cai-xiao-ping-tai/id1033434019',
            androidAddress = 'http://openbox.mobilem.360.cn/index/d/sid/3097613';

        $('.codeBox .ios dt').qrcode({
            render: iRender,
            text: iosAddress,
            width: 104,
            height: 104
        });
        $('.codeBox .android dt').qrcode({
            render: iRender,
            text: androidAddress,
            width: 104,
            height: 104
        });
    }
}
$(function() {
    KD.login.init();
});