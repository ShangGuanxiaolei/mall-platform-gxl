var KD = KD || {};
var imgRepeat;
var kdmovefirst = 1;
var domain = '';
var host = domain + '/v2';

if(!window.console){
    window.console = {};
    window.console.log = function(){};
}

KD.main = {
    init: function() {
        var that = this;
        //dwz初始化
        that.initDWZ();
        //侧导航变色
        that.navLink();
        //商品导入
        that.leadingIn();

        that.getShopInfo();
    },
    initDWZ: function() {
        DWZ.init("dwz.frag.xml", {
            loginUrl: "login.html",
            loginTitle: "登录", // 弹出登录对话框
            statusCode: {
                ok: 200,
                error: 300,
                timeout: 401
            }, //【可选】
            debug: false, // 调试模式 【true|false】
            ui: {
                hideMode: 'display'
            },
            callback: function() {
                initEnv();
                setTimeout(function() {
                        $('.siderLink a:first').addClass('active');
                        navTab.openTab('myproduct', $('.siderLink a:first').attr('data-href'), {
                            title: "我的商品",
                            fresh: true,
                            data: {}
                        });
                    })
                    // themeBase 相对于index页面的主题base路径
            }
        });
    },
    getShopInfo: function() {
        KD.API.getShopInfo(function(data) {
            $('.loginInfo-name').text(data.data.name).attr('data-id', data.data.id);
            $('.siderNav .avatar img').attr('src', data.data.imgUrl);
            $('.siderNav .nick').text(data.data.name);
        });
    },
    navLink: function() {
        var links = $('.siderLink a');
        links.on('click', function() {
            $(this).addClass('active').siblings().removeClass('active');
        })
    },
    logout: function() {
        alertMsg.confirm("确认退出？", {
            okCall: function() {
                KD.API.logOut(function() {
                    location.href = './login.html';
                });
            }
        });
    },
    leadingIn: function() { 
        /*
        $("#j-lead-upload").uploadify({
            'auto': true,
            'buttonText': '选择文件...',
            'width': '280',
            'height': '40',
            'swf': 'resources/uploadify/scripts/uploadify.swf',
            'uploader': 'uploadify.php',
            'onSWFReady': function() {
                console.log('flash is okay!')
            }
        });
        $('.j-leadingin').showPop({
            target: $('.j-lead'),
            close: $('.j-lead-close'),
            modal: true
        });
        */
    },
    query: function(name,strs) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var str = strs ? strs : window.location.search.substr(1);
        var r = str.match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
}


KD.API = {
    getProduct : function(id, success, fail){
        var that = this;
        $.ajax({
            url: host + '/product/' + id,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    getShopInfo: function(success, fail) {
        var that = this;
        $.ajax({
            url: host + '/shop/mine',
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (!data.data) {
                    that.goLogin();
                    return;
                }
                // console.log(data);
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    getProductList: function(type, options, success, fail) {
        var that = this;
        var link = '';
        switch (type) {
            //在售
            case "onsale":
                link = '/product/list';
                break;
                //草稿 
            case "draft":
                link = '/product/list';
                options.order = 'statusDraft';
                break;
                //计划
            case "delay":
                link = '/product/list/forsalebyPC';
                break;
                // 下架
            case "offsale":
                link = '/product/list';
                options.order = 'soldout';
                break;
            case "search":
                link = '/product/searchbyPc/' + $('.loginInfo-name').attr('data-id') + '/' + options.keyword;
                delete options.keyword;
                break;
            default:
                link = '/product/list';
        }

        var settings = {
            order: '',
            direction: '',
            pageable: true,
            page: 0,
            size: 8
        }
        $.extend(settings, options);
        $.ajax({
            url: host + link,
            type: 'POST',
            data: settings,
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                if (data.errorCode == 200) {
                    data.type = type;
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    instock: function(id, success, fail) {
        var that = this;
        $.ajax({
            url: host + '/product/instock/' + id,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    batchInstock: function(ids, success, fail) {
        var that = this;
        var data = {
            "ids": ids.join(',')
        };
        $.ajax({
            url: host + '/product/batch-instock',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    batchOnsale: function(ids, success, fail) {
        var that = this;
        var data = {
            "ids": ids.join(',')
        };
        $.ajax({
            url: host + '/product/batch-onsale',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    logOut: function(success, fail) {
        var that = this;
        $.ajax({
            url: host + '/v2/logout',
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                fail && fail('服务器暂时没有响应，请稍后重试...');
            }
        });
    },
    goLogin: function() {
        var that = this;
        location.href = "login.html";
    },
    //保存段落描述
    saveDesc: function(data, success, fail) {
        var that = this;
        $.ajax({
            url: host + '/fragment/save',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    saveproDesc: function(data, success, fail) {
        var that = this;
        $.ajax({
            url: host + '/productFragment/save',
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    },
    //获取段落描述列表
    getFragment : function(success,fail){
        var that = this;
        $.ajax({
            url: host + '/fragment/list',
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.errorCode == 200) {
                    success && success(data);
                } else {
                    fail && fail(data.errorCode);
                }
            },
            error: function(state) {
                if (state.status == 401) {
                    that.goLogin();
                } else {
                    fail && fail('服务器暂时没有响应，请稍后重试...');
                }
            }
        });
    }
}
var KDZoneList, tempId;
KD.zone = {
    getList: function() {
        $.ajax({
            type: "GET",
            url: host + "/zone/1/children",
            async: false,
            error: function(data) {
                console.log(data.msg);
            },
            success: function(res) {
                var data = res.data;
                var stateLength = data.length;
                var htm = '';
                var html = '';
                for (var i = 0; i < stateLength; i++) {
                    htm += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    html += '<li><a href="javascript:;" value="' + data[i].id + '">' + data[i].name + '</a></li>';
                };
                KDZoneList = htm;
            }
        });
    }
}
$(function() {
    KD.main.init();
});