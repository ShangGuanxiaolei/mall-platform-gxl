var KD = KD || {};
KD.product = {
    init: function() {
        var that = this;

        //specName detele

        that.skuDetele();
        //推荐按钮模拟
        that.seeting();
        //监听事件
        that.eventistening();
        //添加类型
        that.typeAdd();
        //    添加分类
        that.skuNext();

        that.skuEnd();
        that.sku();
        that.specDetele();
        that.verify();
        //编辑段落描述
        that.modifyDesc();
        //关闭段落描述弹出
        that.closeDescPop();
        //保存描述
        that.submitDesc();
        //删除段落描述中的图片
        that.delImg();
        that.delProImg();

        //    编辑器
        that.editor();
        that.descrip();
        //如果是编辑商品需要填充数据
        that.fillForm();
        //点击sku 改变价格
        that.selectSku();
        that.skusArr = new Array;
        that.editTable = false;
        that.skuInfoBack = null;
        that.skuInfoBase = null;
        that.skuInfo = null;
    },
    combine: function(arr) {
        var r = [];
        (function f(t, a, n) {
            if (n == 0) return r.push(t);
            for (var i = 0; i < a[n - 1].length; i++) {
                f(t.concat(a[n - 1][i]), a, n - 1);
            }
        })([], arr, arr.length);
        return r;
    },
    selectSku: function() {
        //检测价格和数量
        var that = this;
        $('body').off('click', '.sku-infor .spec-name');
        $('body').on('click', '.sku-infor .spec-name', function() {
            var self = this;
            var result = [], //skus副本
                resultYes = [], //可用的skus
                resultAmount0 = []; //库存为0的sku
            $.each(that.skuInfo.skus, function(i, el) {
                result.push(el); //存储一份副本 ，不在原来的上操作
                if (el.amount == 0) {
                    resultAmount0.push(el);
                }
            });

            $(this).toggleClass('spec-active').siblings().removeClass('spec-active');

            var selected = [];
            $('.sku-infor .sku-item.trueItem').each(function(i, el) {
                var v = $(el).find('.spec-active').text();
                selected.push(v);
            });
            //匹配数据 看匹配的总数 和 最低价格,同时排除没有的
            $.each(result, function(i, el) {
                var bYes = true;
                for (var j = 0; j < selected.length; j++) {
                    if (!(selected[j] === '' || selected[j] === el.sku[j])) {
                        //如果没匹配
                        bYes = false;
                        break;
                    }
                }
                if (bYes) {
                    resultYes.push(el);
                }
            });
            var items = that.getSkuData(resultYes);
            $('.sku-item-price').html(items.price);
            $('.sku-item-amount').html(items.amount);
        });
    },
    getSkuData: function(skus) {
        var skuArr = []; //副本
        $.each(skus, function(i, el) {
            //计算库存不为0的数据
            if (el.amount && el.amount > 0) {
                skuArr.push(el);
            }
        });
        //用来获取一组skus里面最小的价格 和 总的库存
        if (!skuArr.length) {
            return {
                "price": 0,
                "amount": 0
            };
        }
        skuArr.sort(function(a, b) {
            return Number(a.price) > Number(b.price);
        });
        var total = 0;
        $.each(skuArr, function(i, el) {
            total += Number(el.amount);
        });
        return {
            "price": skuArr[0].price,
            "amount": total
        }
    },
    verify: function() {
        $("#skuLast").delegate(".input-numb", 'blur', function(ev) {
            var regu = /^\-?[0-9]*\.[0-9]*$|^([0-9]+)$/;
            var re = new RegExp(regu);
            if (!re.test($(ev.target).val())) {
                alertMsg.error('请输入数字');
                $(ev.target).val('').text('').trigger('change');
            }

            if ($(this).hasClass('sku-amount')) {
                if ($(ev.target).val() < 1) {
                    alertMsg.error('请输入大于0的商品数字');
                    $(ev.target).val('').text('').trigger('change');
                }
            } else {
                if ($(ev.target).val() <= 0) {
                    alertMsg.error('请输入大于0的商品数字');
                    $(ev.target).val('').text('').trigger('change');
                }
            }
        })
    },
    initSku: function(skuMappding, skuArrs) {
        Array.min = function(array) {
            return Math.min.apply(Math, array);
        };
        var priceArr = [],
            amountArr = 0;
        $(skuArrs).each(function(i, item) {
            priceArr.push($(item)[0].price);
            amountArr = amountArr + Number($(item)[0].amount);
        });
        var specLi = "<li class='sku-item'></li>";
        var oSpecKey = "<span class='spec-key'></span>";
        var oSpecName = "<span class='spec-name'></span>";
        $(skuMappding).each(function(j, obj) {
            $(specLi).appendTo($(".sku-infor"));
            var k = j + 1
            var specI = "spec" + k;
            $(oSpecKey).appendTo($(".sku-item")[k]).text(obj[specI]);
            //$(oSpecName).appendTo($(".sku-item")[k]).text(obj.specI);
        });

        $(".sku-item-price").text(Array.min(priceArr)); //价格
        $(".sku-item-amount").text(amountArr); //数量


    },
    skuGo: function(step) {
        var that = this;
        //编辑sku时go 第几步
        var $step1 = $('#skuEdit'),
            $step2 = $('#skuNext'),
            $step3 = $('#skuLast');
        switch (step) {
            case 1:
                $step1.show();
                $step2.hide();
                $step3.hide();
                break;
            case 2:
                $step2.show();
                $step1.hide();
                $step3.hide();
                that.getSkuInfoBack();
                break;
            case 3:
                $step3.show();
                $step2.hide();
                $step1.hide();
                break;
            default:
                $step1.hide();
                $step2.hide();
                $step3.hide();
                $('#dialogMask').remove();
                $(".sku-infor").show();
        }
    },
    upload: function() {
        $('#uploadify').uploadify({
            // Some options
            'onUploadSuccess': function(file, data, response) {
                alert('The file was saved to: ' + data);
            }
        });
    },
    seeting: function() {
        if ($(".danbanbtn").hasClass('need')) {
            $("#danbaoRecommend").val('1');
        } else if ($(".danbanbtn").hasClass('notneed')) {
            $("#danbaoRecommend").val('0');
        }
        $(".danbanbtn").click(function() {
            if ($(".danbanbtn").hasClass('need')) {
                $("#danbaoRecommend").val('0');
            } else if ($(".danbanbtn").hasClass('notneed')) {
                $("#danbaoRecommend").val('1');
            }
        });
        $(".sku-infor").hide();

    },
    getSkuInfoBack: function() {
        var that = this;
        if (that.skuInfoBack) {
            $.each(that.skuInfoBack.skus, function(i, el) {
                el.amount = 0;
                el.price = 0;
            });
        }
        var skuTable = $('.skusuccess .pro-table table tbody');
        //保存sku 要判断一下是不是对应的 因为可能会删除一些sku
        for (var i = 0; i < skuTable.find('tr').size(); i++) {
            for (var j = 0; j < that.skuInfoBack.skus.length; j++) {
                var isThis = true;
                //遍历skus 取key组  得到匹配的项
                $.each(that.skuInfoBack.skus[j].sku, function(k, e) {
                    //判断每一个key是不是和表格的值对应
                    if (e != $(skuTable.find('tr')[i]).find('.w20 div').eq(k).text().replace(/^\s*|\s*$/g, '')) {
                        isThis = false;
                    }
                });
                if (isThis) {
                    that.skuInfoBack.skus[j].amount = $(skuTable.find('tr')[i]).find('[data-name="amount"]').val();
                    that.skuInfoBack.skus[j].price = $(skuTable.find('tr')[i]).find('[data-name="price"]').val();
                }
            }
        }
    },
    sku: function() {
        /*
         #skudd : 编辑型号的按钮
         #skuEdit : 创建类型 step1
         #skuNext : 添加型号 step2
         #skusuccess : 编辑型号 step3
         */
        var that = this;
        $(".describeAdds").hide();
        $("#skudd").bind("click", function() {
            that.skuGo(1);
            $('#skuEdit').after('<div id="dialogMask"></div>');
        });
        $(".J-sku-next").bind("click", function() {
            if ($(".sku-checkbox .checked").length > 5) {
                alert("所选类型不能超过5个~~");
                return false;
            } else {
                that.skuGo(2);
            }
        });
        $(".J-sku-success").bind("click", function() {
            that.skuGo(3);
            //setData();
        });
        $("#J-sku-success").bind('click', function() {
            var skuTable = $('.skusuccess .pro-table table tbody');
            //保存sku
            function isTrue() {
                var isOk = true;
                $(".input-numb").each(function(i, item) {
                    if ($(item).val() == '') {
                        isOk = false;
                        alert("请填写完整价格和库存~~");
                        return false;
                    }
                });
                return isOk;
            }

            if (isTrue() == true) {
                /*
                 var param = {},
                 mappingArrs = new Array(),
                 skuArrs = new Array();
                 var oValue;
                 var L = $('.pro-table').find('tbody').find('tr').length;
                 var skuObj;
                 */

                that.getSkuInfoBack();

                that.skuInfo = that.skuInfoBack;

                if (!$('.pro-table tr .tr-detele').size()) {
                    that.skuInfo = {
                        "mappings": [],
                        "skus": []
                    }
                }

                that.skuGo();
                that.initSkuUI();



                /*
                 function arrItem(j) {
                 var skuItem = new Object();
                 var tdItem = $('.pro-table').find('tbody').find('tr').eq(j).find('td');
                 var i = 0;
                 l = $('.pro-table').find('tbody').find('tr').eq(0).find('td').length - 1;
                 for (; i < l; i++) {
                 if (i < l - 2) {
                 var oName = tdItem.eq(i).find('div').attr('data-name');
                 var oValue = tdItem.eq(i).find('div').text();
                 } else {
                 var oName = tdItem.eq(i).find('input').attr('data-name');
                 var oValue = tdItem.eq(i).find('input').val();
                 }

                 skuItem[oName] = oValue;
                 }
                 return skuItem;

                 }
                 */
                /*  $('.pro-table').find('tbody').find('tr').each(function(j,item){
                 arrItem(j);
                 that.config.skuArrs.push(arrItem(j));
                 });*/
                /*

                 $("#dataSkus").data('data-skuArrs', skuArrs); //sku
                 var thItem = $('.pro-table').find('thead').find('th');
                 var thItemLength = thItem.length;
                 var mappingItem = new Object();
                 var k = 0;
                 for (; k < thItemLength - 3; k++) {
                 var thName = thItem.eq(k).find('div').attr('data-name');
                 var thValue = thItem.eq(k).find('div').text();
                 mappingItem['specKey'] = thName;
                 mappingItem['specName'] = thValue;
                 mappingItem['mappingValues'] = thValue;
                 }
                 $("#skumapping").data('data-skuMappding', mappingItem); //skumapping
                 that.skuGo();
                 */
            }
            /*
             that.initSku($("#skumapping").data('data-skuMappding'), $("#dataSkus").data('data-skuArrs'));
             */

        })
        $(".J-sku-add").bind("click", function() {
            that.skuGo(1);
        });
        $("#skuddPrev").bind("click", function() {
            that.skuGo(1);
        });

    },
    initSkuUI: function() {
        var that = this;
        var skuInfoTPL = $('.tpl-skuUI').html();

        var types = [];
        $('#dialogMask').fadeOut().remove();

        $.each(that.skuInfo.mappings, function(i, el) {
            //遍历每一项sku 取 第i项  判断 是否已经存在
            types[i] = types[i] || [];
            $.each(that.skuInfo.skus, function(j, sku) {
                if (_.indexOf(types[i], sku.sku[i]) < 0) {
                    types[i].push(sku.sku[i]);
                }
            });
        });
        $.extend(that.skuInfo, {
            price: that.getSkuData(that.skuInfo.skus).price,
            amount: that.getSkuData(that.skuInfo.skus).amount,
            types: types
        });
        $('#proPrice,#proNumb').removeClass('error');
        $('#proNumb').parents('li').find('span.error').remove();

        var doTtmpl = doT.template(skuInfoTPL);
        $('.sku-infor').html(doTtmpl(that.skuInfo)).show();
        if (!$('.sku-infor .trueItem').length) {
            $('.sku-infor').html('').hide();
            $('#proPrice').parent('.fl-l').parent('li').show();
        } else {
            $('#proPrice').parent('.fl-l').parent('li').hide();
        }
    },
    eventistening: function() {
        var that = this;
        $(document).off('click').on("click", function(ev) {
            var obj, clsNam, checkSubmitFlg = false;
            /*if(ev.target.className=="J-specName-detele"){
             var eParent = $(ev.target).parent();
             if(eParent.length==1){
             eParent.addClass("fn-hide");
             }else{
             eParent.remove();
             }
             }*/
            if (ev.target.id == "sku-add") {
                obj = $(".pro-sku-add");
                clsNam = 'pro-sku-add';
                add(obj, clsNam);
            }
            if (ev.target.id == "timePublish") {
                if ($(this).attr("checked", true)) {
                    $("#data-choose").css("display", 'block');
                    $(".inputDateButton").css("display", 'block');
                }
            }

            function add(obj, clsNam) {
                obj.before(obj.clone().removeClass("fn-hide").removeClass(clsNam));
                var len = $(".pro-sku").not(".pro-sku-add").length + 1;
                obj.find(".skus").attr('name', 'spec' + len);

            }


            if (ev.target.className == "timePublish") {
                if (!$(this).val()) {
                    $(this).attr("checked", true);
                }
            }
            if (ev.target.id == "J-comfirm") {
                if (!$(".productImgs .imgBox img").size()) {
                    alert("商品图片不能为空哦~");
                    return false;
                }

                if (!$("#proName").val()) {
                    alert("商品名称不能为空哦~");
                    return false;
                }
                if (!$("#description").val()) {
                    alert("商品描述不能为空哦~");
                    return false;
                }
                if (!$('.sku-item-price').text()) {
                    if (!Number($("#proPrice").val())>0 || !Number($("#proNumb").val())>0) {
                        alert("商品价格或商品数量不能为0~");
                        return false;
                    }
                }
                if ($("#proPrice").val() === '' && $('.sku-item-price').text() === '') {
                    alert("商品价格不能为空哦~");
                    return false;
                }
                if ($("#proNumb").val() === '' && $('.sku-item-amount').text() ==='') {
                    alert("商品数量不能为空哦~");
                    return false;
                }

                if ($(".danbaobtn").hasClass('notneed')) {
                    $("#danbaoRecommend").val('false');
                } else if ($(".danbaobtn").hasClass('need')) {
                    $("#danbaoRecommend").val('true');
                }
                if ($('.radio-status[name="status"]:checked').val() == 'FORSALE') {
                    var timePlan = $('#data-choose input[name="forsaleDate"]').val();
                    timePlan = new Date(timePlan);
                    if (timePlan) {
                        var timeNow = new Date();
                        if (timePlan.getTime() <= timeNow.getTime()) {
                            alert('计划发布时间必须大于当前时间~');
                            return false;
                        };
                    } else {
                        alert('计划发布时间不能为空~');
                        return false;
                    }
                };
                if ($(".error").css("display") == "block") {
                    alert("请确认下信息是否填正确哦~");
                    return false;
                }


                var imgArr = [];
                $(".productImgs .imgBox img").each(function(i, item) {
                    imgArr.push($(item).attr('key'));
                });
                if ($('.radio-status[name="status"]:checked').val() == 'FORSALE') {
                    var timePlan = $('#data-choose input[name="forsaleDate"]').val();
                    if (timePlan) {
                        timePlan = new Date(timePlan);
                        var timeNow = new Date();
                        if (timePlan.getTime() <= timeNow.getTime()) {
                            alert('计划发布时间必须大于当前时间~');
                            return false;
                        };
                    } else {
                        alert('计划发布时间不能为空~');
                        return false;
                    }
                };


                var date = $(".radio-data").val();
                var daledate = date ? new Date((date).replace(new RegExp("-", "gm"), "/")).getTime() : '';

                var skuData = [],
                    skuMappingData = [],
                    skuPA = []; //price amount
                if (that.skuInfo) {
                    $.each(that.skuInfo.skus, function(i, el) {
                        skuData[i] = {};
                        skuPA[i] = {};
                        $.each(el.sku, function(j, s) {
                            skuData[i]['spec' + (j + 1)] = s;
                        });
                        skuPA[i].price = el.price;
                        skuPA[i].amount = el.amount;
                    });

                    $.each(that.skuInfo.mappings, function(i, el) {
                        skuMappingData[i] = {};
                        skuMappingData[i].specKey = 'spec' + (i + 1);
                        skuMappingData[i].specName = el;
                    });
                }

                var skuFormData = {};

                var index = 0;
                var validArr = [];
                $.each(skuPA, function(i, el) {
                    if (el.price && el.price > 0 && el.amount && el.amount > 0) {
                        validArr.push(i);
                        skuFormData['skus[' + index + '].price'] = el.price;
                        skuFormData['skus[' + index + '].amount'] = el.amount;
                        index++;
                    }

                });

                var indexJ = 0;
                $.each(skuData, function(i, el) {
                    if (_.indexOf(validArr, i) >= 0) {
                        for (var attr in el) {
                            skuFormData['skus[' + indexJ + '].' + attr] = el[attr];
                        }
                        indexJ++;
                    }
                });


                $.each(skuMappingData, function(i, el) {
                    for (var attr in el) {
                        skuFormData['skuMappings[' + i + '].' + attr] = el[attr];
                    }
                });



                //价格和库存取值判断
                var price = 0;
                var amount = 0;
                if ($('.sku-item-price').text()) {
                    price = $('.sku-item-price').text();
                    amount = $('.sku-item-amount').text();
                } else {
                    price = $("#proPrice").val();
                    amount = $("#proNumb").val();
                }

                param = {
                    'imgs': imgArr.join(','),
                    'name': $("#proName").val(),
                    'description': $("#description").val(),
                    'recommend': $("#danbaoRecommend").val(),
                    // 'status': $("#danbaoRecommend").val(),
                    'status': $('.pageFormContent [name=status]:checked').val(),
                    'forsaleDate': daledate
                }

                $.extend(param, skuFormData);

                if (!$('.sku-infor .spec-name').size()) {
                    $.extend(param, {
                        'skus[0].price': price,
                        'skus[0].amount': amount,
                        'skus[0].spec1': '无'
                    })
                }

                if (that.productId) {
                    $.extend(param, {
                        id: that.productId
                    });
                }

                $.ajax({
                    url: host + '/product/save',
                    type: 'POST',
                    data: param,
                    //traditional: true,
                    dataType: 'json',
                    success: function(data) {
                        if (data.errorCode == 200) {
                            var proId = data.data.id;
                            var fraId = [];

                            $('#alertBackground').show();
                            alertMsg.correct('操作成功！', setTimeout(function() {
                                $('#alertBackground').hide();
                            }, 3000));

                            //使用dwz自己的方式打开
                            navTab.openTab('myproduct', 'myproduct.html?status=' + param.status, {
                                title: "我的商品",
                                fresh: true,
                                data: {}
                            });


                            $(".pro-desibe").each(function(i, item) {
                                var id = $(item).attr('data-id');
                                fraId.push(id);
                            });
                            $.ajax({
                                url: host + '/productFragment/save',
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    fragmentIds: fraId.join(','),
                                    productId: proId
                                },
                                success: function(res) {

                                }
                            });
                        } else {
                            alertMsg.error(data.moreInfo);
                        }
                    }
                });
            }
            //  获取
            $(".skus").change(function() {
                $(this).parents('table').find(".specName").attr("name", $(this).val());
            });
        });
    },

    skusEditBuild: function(data) {
        var that = this;
        //渲染自定义类型
        var skuA = [];
        var diyTPL = $('.tpl-diy').html();
        var diyHTML = doT.template(diyTPL);
        $('#commonTypeList .sku-checkbox').each(function(i, el) {
            skuA.push($.trim($(el).text()));
        });

        $.each(data.data.skuMappings, function(i, sk) {
            if (_.indexOf(skuA, sk.specName) < 0) {
                //如果有
                $('.sku-form-type .sku-checkboxs').append(diyHTML({
                    name: sk.specName
                }));
                $('.sku-form-type .sku-checkboxs input').iCheck();
                $('.sku-form-type .sku-checkboxs').show();
            }
        });
        $('.sku-form-type .sku-checkboxs').find('.sku-checkbox-item').eq(0).hide().find('input').iCheck('uncheck');;
        initUI($('.sku-form-type .sku-checkboxs'));


        for (index in data.data.skuMappings) {
            $(".sku-edit").find(".pageFormContent-item").find(".sku-checkbox").each(function(i, item) {
                if ($.trim($(item).text()) == data.data.skuMappings[index].specName) {
                    $(item).find("input").iCheck('check');
                }
            })
        }
        $(".sku-edit").delegate("#typeNext", 'click', function() {
            var specArr = new Array;
            var p = 1;
            var L = data.data.skuMappings.length;
            var l = L - 1;
            for (; l >= 0; l--) {
                var k = data.data.skuMappings[l].mappingValues.length;
                var specTPL = $('.tpl-spec-item').html();
                var doTtmpl = doT.template(specTPL);
                //for(var j=0;j<k;j++){
                $(".J-sku-spec").children().each(function(index, item) {
                    if (data.data.skuMappings[l].specName == $(item).find(".sku-speckey").text().replace(/^\s*|\s*$/g, '')) {
                        specArr = data.data.skuMappings[l].mappingValues;
                        $(".sku-form-sku" + index).html(doTtmpl(specArr));
                    }
                })
            }
            /*for(;l>=0;l--){
             var i=L-l-1;
             var specTPL = $('.tpl-spec-item').html();
             var doTtmpl = doT.template(specTPL);
             specArr = data.data.skuMappings[l].mappingValues;
             $(".sku-form-sku"+i).html(doTtmpl(specArr));
             }*/

        });
        $("#skuNext").delegate("#skuSuccess", 'click', function() {
            that.editTable = true;
            that.creatTable();
        });


    },
    skuDetele: function() {
        var that = this;

        function skuD(partentEvent, eventObj) {
            // var that = this;
            partentEvent.delegate(eventObj, 'click', function(ev) {
                var objParent;
                /*  if ($(ev.target).hasClass("detele-parent")) {
                 objParent = $(this).parent().parent();
                 $(".sku-form-common").children().each(function(i, item) {
                 if ($(item).text().replace(/^\s*|\s*$/g, '') == partentEvent.parent().prev().text().replace(/^\s*|\s*$/g, '')) {
                 $(item).find(".icheck").iCheck('uncheck');
                 }
                 })
                 $(".pro-table").find('th').each(function(i, item) {
                 if ($(item).find('div').text() == partentEvent.parent().prev().text()) {
                 $(item).remove();
                 $("tbody").find('tr').children().eq(i).remove();
                 }
                 });
                 } else {*/
                objParent = $(this).parent();
                //}
                if ($(this).parent().parent().parent().children().length == 1) {
                    objParent.addClass('fn-hide');
                } else {
                    objParent.remove();
                }

                that.getTypeList();
                that.creatTable();

            });
        }
        var partentSpecName = $(".sku-form-sku");
        var specNameEvent = ".J-specName";
        skuD(partentSpecName, specNameEvent);
        //spec detele
        /*  var skuDeteleEvent = ".sku-detele";
         var partentEvent = $(".skunext");
         skuD(partentEvent, skuDeteleEvent);*/

        //    specName
        var specEvent = ".sku-delete";
        var partentSpecEvent = $(".skusuccess");
        skuD(partentSpecEvent, specEvent);
    },
    specDetele: function() {
        $(".skunext").delegate(".detele-parent", 'click', function(ev) {
            var objParent = $(this).parent().parent();
            //第二步删除整个类型
            $(".sku-form-common").children().each(function(i, item) {
                if ($(item).text().replace(/^\s*|\s*$/g, '') == objParent.find('label').text().replace(/^\s*|\s*$/g, '')) {
                    $(item).find(".icheckbox").removeClass('checked');
                }
            })
            $(".pro-table").find('th').find('div').each(function(i, item) {
                if ($(item).text() == objParent.find('label').text()) {
                    $(item).parent().remove();
                    $('tr').each(function(j, obj) {
                        $(obj).find('td').eq(i).remove();
                    })
                }

            })
            if ($(this).parent().parent().parent().children().length == 1) {
                objParent.addClass('fn-hide');
            } else {
                objParent.remove();
            }

        });
    },
    typeAdd: function() {
        var diyTPL = $('.tpl-diy').html();
        var diyHTML = doT.template(diyTPL);
        $("#type-btn").click(function() {
            $(".diy-type").removeClass("fn-hide");
            $(this).addClass("fn-hide");
        });
        $(".sku-name-comfirm").click(function() {
            $('.sku-checkbox-item').eq(0).show();
            if ($("#diyType").val()) {
                if ($(".sku-checkboxs").hasClass("fn-hide")) {
                    $(".sku-checkboxs").removeClass("fn-hide");
                    $(".type-item").text($("#diyType").val());
                    $("#diyType").val('');

                } else {
                    var isrepeat = false;

                    function isRepeat() {
                        $(".type-item").each(function(i, item) {
                            if ($("#diyType").val() == $(item).text()) {
                                isrepeat = true;
                            }
                        })
                        return isrepeat;
                    }
                    if (!isRepeat()) {
                        // var str = $('.tplSKU li').clone();
                        // str.find('.type-item').text($("#diyType").val());
                        // // str.find('input').iCheck('check');
                        // $('.sku-form-type .sku-checkboxs').append(str).show();
                        // $('.sku-form-type .sku-checkboxs input').iCheck();


                        $('.sku-form-type .sku-checkboxs').append(diyHTML({
                            name: $("#diyType").val()
                        }));
                        $('.sku-form-type .sku-checkboxs input').iCheck();
                        // $(".sku-checkbox-item").last().clone().find(".type-item").text($("#diyType").val()).parent().parent().appendTo($(".sku-checkboxs"));
                        $("#diyType").val('');
                    } else {
                        alert("该类型名称上去已有了哦~");
                    }
                }

            } else {
                alert("请填先写自定义类型名称")
            }

        });
        $('body').off('click', '#deteleTypeItem');
        $('body').on('click', '#deteleTypeItem', function() {
            $(".diy-type").addClass("fn-hide");
            $("#type-btn").removeClass("fn-hide");
        });

        $('body').off('click', '.comfirm-type');
        $('body').on('click', '.comfirm-type', function() {
            if ($(".sku-checkboxs").children().length > 1) {
                $(this).parent().remove();
            } else {
                $(this).parent().addClass("fn-hide");
                $(this).parent().parent().addClass("fn-hide");
            }
        });
        $(".sku-add-close").click(function() {
            $(".prodetailbox").hide();
            $('#dialogMask').fadeOut().remove();
        });

        /*   $(".sku-detele").delegate('click',function(){

         });
         */
        $(".J-sku-spec").delegate(".add-spec-name", 'click', function(ev) {
            var oPartent = $(ev.target).parent().prev();
            var classIndex = 0;
            if (oPartent.find(".sku-form-skus-item").hasClass("fn-hide")) {
                oPartent.children().removeClass('fn-hide');
            } else {
                oPartent.find("li:last").clone().appendTo(oPartent);
                oPartent.find("li:last").find(".textInput").val(" ");
            }

        });
        //存储specName input的值
        $(".sku-form-skus-item").find('.textInput').bind('change', function(ev) {
            $(ev.target).parent().attr('data-attr', $(ev.target).val());
        });



        $(".skunext").delegate('.J-specName-detele', 'click', function(ev) {
            var eObj = $(ev.target).parent();
            var eParent = eObj.parent().children();
            if (eParent.length == 1) {
                eObj.addClass("fn-hide");
            } else {
                eObj.remove();
            }
        })
    },
    getTypeList: function() {
        //获取skuMapping列表缓存数据
        var that = this;
        that.typeList = {};
        $('.J-specKey').not('.fn-hide').each(function(i, el) {
            var key = $(el).find('.sku-form-item.sku-speckey').text();
            that.typeList[key] = [];
            $(el).find('.sku-spec-name .sku-form-skus-item input').each(function(j, e) {
                that.typeList[key].push($(e).val());
            });
        });
    },
    skuNext: function() {
        var specTPL = $('.tpl-spec').html();
        var doTtmpl = doT.template(specTPL);
        var that = this;
        that.typeList = {};

        $('body').off('change', '.sku-form-skus-item input');
        $('body').on('change', '.sku-form-skus-item input', function() {
            that.getTypeList();
        });
        $("#typeNext").click(function() {
            that.typeList = that.typeList || {};
            var keyArry = [];
            $(".sku-form").find(".icheckbox").each(function(i, item) {
                if ($(item).hasClass("checked")) {
                    keyArry.push($(item).parent().text().replace(/^\s*|\s*$/g, ''));
                }
            });

            for (var attr in that.typeList) {
                if (_.indexOf(keyArry, attr) < 0) {
                    //如果不存在 删掉
                    delete that.typeList[attr];
                }
            }

            var i = 0,
                j = keyArry.length;

            var data = {
                keyArr: keyArry,
                typeList: that.typeList
            }

            $(".J-sku-spec").html(doTtmpl(data));
            // for (; i < j; i++) {
            //     $(".J-spec-key").clone().appendTo($(".J-sku-spec"));
            //     $(".J-sku-spec").children().each(function (i, item) {
            //         $(item).removeClass("J-spec-key").removeClass('fn-hide').addClass("J-specKey").find('label').text(keyArry[i]).end().find(".sku-form-skus-item").addClass("form-skus-item" + i);
            //     });
            // }
            //dataAttr['keyArray']=keyArry;
            //$(".J-sku-spec").attr("data-spec",keyArry);
            /* var arrText = doT.template($(".tpl-speckey").html());
             $(".J-sku-spec").html(arrText(dataAttr));
             console.log(arrText);*/
        })

    },
    skuEnd: function() {
        var that = this;
        $(".pro-table").delegate(".tr-detele", 'click', function(ev) {
            //删除sku
            $(ev.target).parent().parent().remove();

        });
        $(".spec-name").click(function(ev) {
            $(ev.target).addClass('spec')
        });
        $(".sku-next").delegate("#skuSuccess", 'click', function() {
            $(".pro-table").html('');

            $(".speckey").each(function(j, obj) {
                $(obj).remove();
            });

            $(".J-specKey").each(function(k, item) {
                $(".skusuccess").find(".pro-table").find("tr").prepend("<td class='speckey'></td>");
            });
            $("#proTable").find(".speckey").each(function(i, item) {
                $(item).text($(".J-sku-spec").find(".sku-speckey").eq(i).text());
            });

            //创建table
            that.creatTable();

        })
    },
    creatTable: function() {
        var that = this;
        //获取specname二维数组
        var arr = new Array;

        function skuItem(i) {
            arr[i] = new Array;
            $(".sku-form-sku" + i).children().each(function(j, obj) {
                arr[i].push($(obj).find('input').val());
            });
        }
        for (var i = 0; i < $(".sku-form-sku").length; i++) {
            skuItem(i);
        }
        $("#mappingValues").data("mappingValues", arr);
        //    创建table
        var res = that.combine(arr);

        var tableTpl = $('.tpl-table').html();
        var doTtmpl = doT.template(tableTpl);

        if (that.skuInfoBack) {
            // that.skuInfoBase = that.skuInfoBack;
            that.skuInfoBase = {};
            $.extend(that.skuInfoBase, that.skuInfoBack);
        }

        // console.log(that.skuInfoBack);

        that.skuInfoBack = {
            mappings: [],
            skus: []
        }

        $.each($(".J-specKey"), function(i, el) {
            that.skuInfoBack.mappings.unshift($(el).find('label').text().replace(/^\s*|\s*$/g, ''));
        });


        $.each(res, function(i, el) {
            that.skuInfoBack.skus.push({
                "sku": res[i],
                "price": 0,
                "amount": 0
            });
        });
        if (that.skuInfoBase) {
            $.each(that.skuInfoBack.skus, function(i, el) {
                //遍历所有的skus 如果 back 中的每一项sku 和 base中的一样  替换价格和数量
                $.each(that.skuInfoBase.skus, function(j, e) {
                    //全部相等再替换
                    var isAll = true;
                    for (var k = 0; k < e.sku.length; k++) {
                        if (!(el.sku[k] && e.sku[k] == el.sku[k])) {
                            isAll = false;
                            break;
                        }
                    }
                    if (isAll) {
                        el.price = e.price;
                        el.amount = e.amount;
                    }
                });
            });
        }
        $(".pro-table").html(doTtmpl(that.skuInfoBack));
        if (that.editTable) {
            that.skuInfoBack.skus = [];
            /*  $.each(res, function(i, el) {
             that.skuInfoBack.skus.push({
             "sku": res[i],
             "price": 0,
             "amount": 0
             });
             });*/
            var i = 0;
            var L = res.length;
            var l = res[0].length;

            function aa(i) {
                var o = 0;
                var j = 0;
                for (; j < l; j++) {
                    if (that.skuInfo.skus[i] && $(".pro-table").find('tbody').find("tr").eq(i).find('.w20').eq(j).text().replace(/^\s*|\s*$/g, '') == that.skuInfo.skus[i].sku[j]) {
                        o++;
                    }
                    if (o == l) { //spec的值都匹配的话获取当前的价格和数量
                        $(".pro-table").find('tbody').find("tr").eq(i).find('.sku-amount').val(that.skuInfo.skus[i]["amount"]);
                        $(".pro-table").find('tbody').find("tr").eq(i).find('.sku-price').val(that.skuInfo.skus[i]["price"]);
                    }
                    //that.skuInfoBack.skus[i].sku.push($(".pro-table").find('tbody').find("tr").eq(i).find('.w20').eq(j).text().replace(/^\s*|\s*$/g, ''))
                }
            }
            for (; i < L; i++) {
                aa(i);
                that.skuInfoBack.skus.push({
                    "sku": res[i],
                    "price": $(".pro-table").find('tbody').find("tr").eq(i).find('.sku-price').val(),
                    "amount": $(".pro-table").find('tbody').find("tr").eq(i).find('.sku-amount').val()
                });
            }
            /*  $.each(res, function(i, el) {
             that.skuInfoBack.skus.push({
             "sku": res[i]
             */
            /* "price": $(".pro-table").find('tbody').find("tr").find('.sku-price').val(),
                         "amount": $(".pro-table").find('tbody').find("tr").find('.sku-amount').val()*/
            /*
                         });
                         */
            /* if(that.skuInfoBack.skus[i]==){

                         }*/
            /*
                         });*/
            console.log(that.skuInfoBack.skus);
            /* for (; i < res.length; i++) {
             for (; j < res[0].length; j++) {
             var k = Number(j) + 1;
             if (res[i][0] == data.data.skus[i]['spec' + 1] && res[i][1] == data.data.skus[i]['spec' + 2] && res[i][2] == data.data.skus[i]['spec' + 3] && res[i][3] == data.data.skus[i]['spec' + 4] && res[i][4] == data.data.skus[i]['spec' + 5]) {
             console.log(data.data.skus[i]["price"]);
             console.log(data.data.skus[i]["amount"]);
             }
             }
             }*/

        }
        that.skuInfoBase = null;

    },
    descrip: function() {
        var that = this;
        $(".edit-choose").delegate('.detele', 'click', function() {
            var _this = this;
            alertMsg.confirm("确认删除此段落描述？", {
                okCall: function() {
                    if ($(".descrip").length == 1) {
                        $(_this).parent().parent().addClass("fn-hide");
                    } else {
                        $(_this).parent().parent().remove();
                    }
                }
            });
        });
        $(".formBar").delegate("#buttonSave", 'click', function() {
            var proDesibe = $(".pro-desibe");
            $(".pro-desibe").eq(proDesibe.length - 1).clone().appendTo(".sortDrag");
            $(".pro-name").text($("#proName").val());
            $(".pro-text").text($("#describeText").val());
        })
        $(".newClassify").delegate("#new-describe", 'click', function() {
            that.fillDesc();
            $('#describeAdd').after('<div id="dialogMask"></div>');
            $("#describeAdd").show();
        });
        /* $("#describeAdd").delegate(".formBar-detele",function(){
         $("#describeAdd").css("display",'none');
         });*/
    },
    editor: function() {
        $(".edit-type-list").delegate('.input-edit', 'click', function() {
            $(".pageEditor").hide();
            $(".sortDrag").hide();
            if ($("#editWeb").attr("checked")) {
                $(".sortDrag").hide();
                $(".pageEditor").show();
            } else if ($("#editApp").attr("checked")) {
                $(".pageEditor").hide();
                $(".sortDrag").show();
            }
        })
    },
    //填充段落描述弹出层
    fillDesc: function(data) {
        $('#describeAdd .imgBox').empty();
        if (data) {
            //编辑状态
            $('#describeName').val(data.name);
            $('#describeText').val(data.description);
            if (data.imgs[0]) {
                for (var i = 0; i < data.imgs.length; i++) {
                    $('#describeAdd .imgBox').append('<span><img src="' + data.imgs[i] + '" key="' + data.keys[i] + '" /><i>&#xe602;</i></span>');
                }
            }
            $('.pageFormContent [name="paraPlace"]').filter('[value=' + data.showModel + ']').attr('checked', true);
            $("#describeAdd").attr('state', 'modify').attr('_from', data.index);
        } else {
            //新增状态
            $('#describeName').val('');
            $('#describeText').val('');
            $('.pageFormContent [name="paraPlace"]:eq(0)').attr('checked', "checked");
            $("#describeAdd").attr('state', '').attr('_from', '');
            $('#describeAdd .imgBox').empty();
        }
    },
    modifyDesc: function() {
        var that = this;
        $('body').off('click', '.pro-desibe .edit')
        $('body').on('click', '.pro-desibe .edit', function() {
            $('#skuEdit').after('<div id="dialogMask"></div>');
            var $this = $(this).closest('.pro-desibe');
            var data = {
                name: $this.find('.pro-name span').text(),
                description: $this.find('.pro-text').text(),
                imgs: $this.find('.pro-img img').attr('srcs').split(','),
                keys: $this.find('.pro-img img').attr('keys').split(','),
                showModel: $this.attr('data-showModel'),
                index: $this.attr('data-id')
            }
            that.fillDesc(data);
            $("#describeAdd").show();
        });

    },
    closeDescPop: function() {
        var that = this;
        $('body').on('off', '.formBar-detele');
        $('body').on('click', '.formBar-detele', function() {
            that.fillDesc();
            $("#describeAdd").hide();
            $('#dialogMask').fadeOut().remove();
        });
    },
    submitDesc: function() {
        var that = this;
        var addTpl = $('.tpl-desc').html();
        var doTtmpl = doT.template(addTpl);
        $('body').off('click', '#describeAdd button[type="submit"]');
        $('body').on('click', '#describeAdd button[type="submit"]', function() {
            var _this = this;
            var pop = $("#describeAdd");
            for (var i = 0; i < pop.find('.required').length; i++) {
                if (pop.find('.required').eq(i).val() == '') {
                    alertMsg.error('标有*号的项不能为空~');
                    return;
                }
            }
            var imgs = [];
            var keys = [];
            var descId = '';
            pop.find('.imgBox img').each(function(i, el) {
                imgs.push($(el).attr('src'));
                keys.push($(el).attr('key'));
            });
            if (!imgs.length) {
                alertMsg.error('段落描述图片为必选项~');
                return;
            }
            var data = {
                id: '',
                name: pop.find('#describeName').val(),
                description: pop.find('#describeText').val(),
                imgs: imgs.join(','),
                keys: keys.join(','),
                src: imgs[0],
                showModel: pop.find('[name="paraPlace"]:checked').val()
            }
            var ajaxData = {
                    id: data.id,
                    name: data.name,
                    description: data.description,
                    showModel: data.showModel,
                    imgs: data.keys
                }
                //保存或新建在这里处理
            if (pop.attr('state') == 'modify') {
                //如果是修改
                ajaxData.id = data.id = pop.attr('_from');
                var str = doTtmpl(data);
                $('.pro-desibe[data-id="' + data.id + '"]').replaceWith(str);
            } else {
                //如果是新建
                delete ajaxData.id;
                delete data.id;
                $('.descList').append(doTtmpl(data));
            }
            //发送ajax请求
            KD.API.saveDesc(ajaxData, function(res) {
                if (res.moreInfo) {
                    alertMsg.error(res.moreInfo);
                    return;
                }
                if (!ajaxData.id) {
                    $('.pro-desibe:last').attr('data-id', res.data.id);
                }
                $('#dialogMask').fadeOut().remove();
                //如果是编辑商品级别的还要请求一次,具体代码编辑商品时修改
                if (that.productId) {
                    var productId = that.productId;
                    KD.API.saveproDesc({
                        productId: productId,
                        fragmentId: res.data.id
                    }, function() {

                    }, function() {
                        console.log('error');
                    });
                }

            });
            //防止出现绑定多次事件
            that.initUi();
            $("#describeAdd").hide();
        });
    },
    initUi: function() {
        $('.sortDrag').find('.moveico').off('mousedown');
        $('.sortDrag').sortDrag();
    },

    delImg: function() {
        var that = this;
        $('body').off('click', '#describeAdd .imgBox i');
        $('body').on('click', '#describeAdd .imgBox i', function() {
            var _this = this;
            alertMsg.confirm("确认删除此段落描述图片？", {
                okCall: function() {
                    $(_this).parent('span').remove();
                }
            });
        });
    },
    delProImg: function() {
        var that = this;
        $('body').off('click', '.productImgs .imgBox i');
        $('body').on('click', '.productImgs .imgBox i', function() {
            var _this = this;
            alertMsg.confirm("确认删除此商品图片？", {
                okCall: function() {
                    $(_this).parent('span').remove();
                }
            });
        });
    },
    addImg: function(src, key) {
        var that = this;
        $('#describeAdd .imgBox').append('<span><img src="' + src + '" key="' + key + '" /><i>&#xe602;</i></span>');
    },
    addProductImg: function(src, key) {
        var that = this;
        $('.productImgs .imgBox').append('<span><img src="' + src + '" key="' + key + '" /><i>&#xe602;</i></span>');
    },
    fillForm: function() {
        var that = this;
        var addTpl = $('.tpl-desc').html();
        var doTtmpl = doT.template(addTpl);
        var link = $('.navTab-tab li[tabId="addProduct"]').attr('url');
        if (link.indexOf('?') < 0) {
            that.productId = '';
            return;
        }

        var search = link.split('?')[1];
        that.productId = KD.main.query('id', search);
        KD.API.getProduct(that.productId, function(res) {
            console.log(res)
            that.skusEditBuild(res);
            $('#proName').val(res.data.name);
            $('#description').val(res.data.description);
            $('#proPrice').val(res.data.marketPrice);
            $('#proNumb').val(res.data.amount);
            $('#productAdd .danbanbtn').removeClass('notneed need');
            $('.pageFormContent [name=status]').filter('[value="' + res.data.status + '"]').attr('checked', 'checked');
            if (res.data.recommend) {
                $('#productAdd .danbanbtn').addClass('need');
            } else {
                $('#productAdd .danbanbtn').addClass('notneed');
            }

            //计划发布时间填充显示
            var timePlan = timeFormat(res.data.forsaleAt, 'yyyy-MM-dd HH:mm:ss');
            if ($('.radio-status[name="status"]:checked').val() == 'FORSALE') {
                $('#data-choose').show();
                $('#data-choose input[name="forsaleDate"]').val(timePlan);
            };

            //商品图片
            $.each(res.data.imgs, function(i, el) {
                that.addProductImg(el.imgUrl, el.img);
            });

            //整合sku数据
            that.skuInfo = {
                mappings: [],
                skus: [],
            }


            var skuList = [];

            $.each(res.data.skuMappings, function(i, el) {
                that.skuInfo.mappings.push(el.specName);
                skuList.unshift(el.mappingValues);
            });

            $.each(that.combine(skuList), function(i, el) {
                that.skuInfo.skus.push({
                    "sku": el,
                    "price": 0,
                    "amount": 0
                });
            });

            //修改对应sku的价格和数量
            for (var i = 0; i < res.data.skus.length; i++) {
                //对比数据
                for (var j = 0; j < that.skuInfo.skus.length; j++) {
                    var isThis = true;
                    $.each(that.skuInfo.skus[j].sku, function(index, el) {
                        if (el != res.data.skus[i]['spec' + (index + 1)]) {
                            isThis = false;
                        }
                    });
                    if (isThis) {
                        that.skuInfo.skus[j].price = res.data.skus[i].price;
                        that.skuInfo.skus[j].amount = res.data.skus[i].amount;
                    }
                }
            }


            that.initSkuUI();
            //显示sku部分结束

            //如果当前商品存在型号，则隐藏价格可库存的单独输入框
            var lenType = $('.sku-item.trueItem').length;
            if (res.data.name && lenType) {
                $('#proPrice').parent('.fl-l').parent('li').hide();
            } else {
                $('.sku-item').hide();
            }

            //notneed no
            //need yes
            //显示片段
            if (res.data.fragments && res.data.fragments.length) {
                $('.descList').empty();
                $.each(res.data.fragments, function(i, el) {
                    var imgs = [],
                        keys = [],
                        src;
                    $.each(el.imgs, function(index, img) {
                        imgs.push(img.imgUrl);
                        keys.push(img.img);
                    });
                    if (el.imgs[0]) {
                        src = el.imgs[0].imgUrl;
                    }
                    var data = {
                        id: el.id,
                        name: el.name,
                        description: el.description,
                        imgs: imgs.toString(),
                        keys: keys.toString(),
                        src: imgs[0],
                        showModel: el.showModel
                    }
                    $('.descList').append(doTtmpl(data));
                });
                that.initUi();
            }

        });
    }
}
KD.product.init();

function timeFormat(time, format) {
    var t = new Date(time);
    var tf = function(i) {
        return (i < 10 ? '0' : '') + i
    };
    return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function(a) {
        switch (a) {
            case 'yyyy':
                return tf(t.getFullYear());
                break;
            case 'MM':
                return tf(t.getMonth() + 1);
                break;
            case 'mm':
                return tf(t.getMinutes());
                break;
            case 'dd':
                return tf(t.getDate());
                break;
            case 'HH':
                return tf(t.getHours());
                break;
            case 'ss':
                return tf(t.getSeconds());
                break;
        }
    })
}