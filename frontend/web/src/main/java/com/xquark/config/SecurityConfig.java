package com.xquark.config;

import com.xquark.service.shop.ShopService;
import javax.sql.DataSource;

import com.xquark.authentication.TokenAuthenticationFilter;
import com.xquark.authentication.WebTokenBasedRememberMeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.xquark.biz.authentication.DBLoginStrategyBuilder;
import com.xquark.biz.authentication.DomainBasedLoginUrlEntryPoint;
import com.xquark.biz.authentication.LoginConfigurationException;
import com.xquark.biz.authentication.LoginStrategyBuilder;
import com.xquark.biz.authentication.LoginUrlEntryPoint;
import com.vdlm.common.bus.BusSignalManager;
import com.xquark.service.user.UserService;

@Configuration
@EnableWebMvcSecurity
@ImportResource("classpath:META-INF/spring-security-context.xml")
class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  DataSource dataSource;

  @Autowired
  UserService userService;

  @Autowired
  ShopService shopService;

  @Value("${rememberMeServices.key}")
  String remMeKey;

  @Value("${rememberMeServices.token.calidity.seconds}")
  int validSeconds;

  @Value("${rememberMeServices.token.concurrency.safe.seconds}")
  int concurrencySafeSeconds;

  @Value("${site.webapi.host.name}")
  private String siteHost;

  @Bean
  public PersistentTokenRepository persistentTokenRepository() {
    final JdbcTokenRepositoryImpl repo = new JdbcTokenRepositoryImpl();
    repo.setDataSource(dataSource);
    return repo;
  }

  @Bean
  public RememberMeServices rememberMeServices() {

    WebTokenBasedRememberMeServices svc = new WebTokenBasedRememberMeServices(remMeKey,
        userService, shopService);
    svc.setUserService(userService);
    svc.setAlwaysRemember(true);
    svc.setTokenValiditySeconds(validSeconds);
    svc.setCookieName("AppAuthToken");
    // svc.setConcurrencySafeSeconds(concurrencySafeSeconds);
    if (this.siteHost.startsWith("http://")) {
      svc.setUseSecureCookie(false);
    } else if (this.siteHost.startsWith("https://")) {
      svc.setUseSecureCookie(true);
    }
    return svc;
  }

  @Autowired
  @Bean
  public LoginStrategyBuilder loginStrategyBuilder(BusSignalManager bsm) {
    return new DBLoginStrategyBuilder(bsm, "xquark-web");
  }

  @Bean
  @Autowired
  public TokenAuthenticationFilter tokenAuthenticationFilter(@Qualifier("jsonRedisTemplate") RedisTemplate<String, Object> redisTemplate) {
    return new TokenAuthenticationFilter(redisTemplate);
  }

  @Autowired
  @Bean(name = "loginEntryPoint")
  public LoginUrlEntryPoint loginEntryPoint(LoginStrategyBuilder loginStrategyBuilder)
      throws LoginConfigurationException {
    return new DomainBasedLoginUrlEntryPoint("/signin", loginStrategyBuilder);
  }
}