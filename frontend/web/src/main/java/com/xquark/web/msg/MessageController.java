package com.xquark.web.msg;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.dal.model.Message;
import com.xquark.service.msg.MessageService;
import com.xquark.service.push.PushService;
import com.xquark.utils.HtmlUtils;
import com.xquark.web.ResponseObject;

@Controller
public class MessageController {

  @Autowired
  private MessageService messageService;

  @Autowired
  private PushService pushService;

  @RequestMapping(value = "/message/{id}")
  public String product(@PathVariable("id") String id, Model model, HttpServletRequest req) {
    Message message = messageService.loadMessage(id);
    message.setContent(HtmlUtils.strToHtml(message.getContent()));
    model.addAttribute("message", message);
    return "msg/view";
  }

  @ResponseBody
  @RequestMapping(value = "/smsTest")
  public ResponseObject<Boolean> smsTest() {
    String mobile = "15972020280";
    String msg = "测试消息";
    boolean result = pushService.sendSmsEngine(mobile, msg);
    return new ResponseObject<Boolean>(result);
  }


}
