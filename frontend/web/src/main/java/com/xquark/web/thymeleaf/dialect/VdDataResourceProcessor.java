package com.xquark.web.thymeleaf.dialect;

import com.xquark.biz.res.ResourceFacade;

public class VdDataResourceProcessor extends VdResourceProcessor {

  public static final String ATTR_NAME = "data-original";

  public VdDataResourceProcessor(ResourceFacade resourceFacade) {
    super(ATTR_NAME, resourceFacade);
  }
}
