package com.xquark.web.groupon;

import com.alibaba.fastjson.JSON;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.file.ImageService;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PromotionGrouponController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private PromotionGrouponService promotionGrouponService;


  @RequestMapping(value = "/app/grouponList")
  public String view(@RequestParam(value = "status", required = false) String status, Model model,
      HttpServletRequest req) {
    if (status == null) {
      status = "all";
    }
    model.addAttribute("status", status);
    String groupDesc = "";
    if ("all".equals(status)) {
      groupDesc = "全部的";
    } else if ("todoing".equals(status)) {
      groupDesc = "进行中的";
    } else if ("unstart".equals(status)) {
      groupDesc = "未开始的";
    }
    model.addAttribute("status", status);
    model.addAttribute("groupDesc", groupDesc);
    return "b2b/app/groupon/groupon";
  }

}
