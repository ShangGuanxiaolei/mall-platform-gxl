package com.xquark.web.catalog;

import com.alibaba.fastjson.JSON;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.ProductImageMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.SkuMappingMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.PromotionStatus;
import com.xquark.dal.type.ProductSource;
import com.xquark.dal.type.ProductType;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.*;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.CartService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.pricing.PromotionCheckService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.product.ProductCollectionService;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterLevelService;
import com.xquark.service.twitter.TwitterProductComService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.RolePriceService;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.YundouProductService;
import com.xquark.service.yundou.YundouSettingService;
import com.xquark.utils.ResourceResolver;
import com.xquark.web.ResponseObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ProductController extends FragmentAndDescController {

  @Autowired
  UserService userService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private SkuMappingMapper skuMappingMapper;

  @Autowired
  private ProductImageMapper productImageMapper;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private PromotionGrouponService promotionGrouponService;

  @Autowired
  private CartService cartService;

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private RolePriceService rolePriceService;

  @Autowired
  private TwitterLevelService twitterLevelService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private PromotionCheckService promotionCheckService;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private YundouProductService yundouProductService;

  @Autowired
  private YundouSettingService yundouSettingService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private ProductCollectionService productCollectionService;

  @Autowired
  private TwitterProductComService twitterProductComService;

  @Autowired
  private TwitterShopComService twitterShopComService;

  @Autowired
  private FlashSalePromotionProductService flashSalePromotionProductService;

  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/im/detail")
  public String imDetail() {
    return "im/detail";
  }

  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/im/list")
  public String imList() {
    return "im/list";
  }

  //更新用户当前访问的卖家店铺id并且会刷新session
  public final String CURRENT_SELLER_SHOP_ID_PARAM_NAME = "currentSellerShopId";

  @Value("${site.web.host.name}")
  String siteHost;

  /**
   * html5 页面 地址==/p/{id} 查看商品详情
   */
  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/{id}")
  public String view(@PathVariable("id") String productId, Model model, HttpServletRequest req,
      HttpServletResponse resp,
      @RequestParam(value = "union_id", defaultValue = "") String unionId,
      @RequestParam(value = "identity", defaultValue = "") String identity
  ) {

    //普通商品若参与了活动则跳转到活动页
/*
    Promotion promotion = promotionService.loadByProductId(productId);
    if (promotion != null) {
      return "forward:" + UrlHelper.PRODUCT_URL_PREFIX + "/promotion/" + productId
          + "?promotionId=" + promotion.getId();
    }*/
    // 默认检查是否参与限时抢购
    getProductInfo(productId, model, req);

    // 获取当前用户
    if (!StringUtils.isEmpty(unionId)) {
      User user = userService.load(unionId);
      if (user != null) {
        Cookie c = new Cookie("union_id", unionId);
        c.setMaxAge(86400 * 15);
        c.setPath("/");
        resp.addCookie(c);
      }
    }

    // 区分进入商品详情页的是商家还是客户
    if (!StringUtils.isEmpty(identity)) {
      model.addAttribute("identity", identity);
    }

    // 购物车内商品总数
    String shopId = shopService.loadRootShop().getId();
    Integer cartCount;
    String userId = this.getCurrentUser().getId();
    cartCount = cartService.countByShopId(userId, shopId);
    model.addAttribute("cartCount", cartCount);

    // 微信分享链接地址
//    String shareUrl = siteHost + "/p/" + productId + "?1=1";
//    // 判断当前用户，如果是推客，则分享商品时应该带上推客的店铺，否则分享商品带上当前卖家的店铺
//    List<UserTwitter> twitters = userTwitterService.selectByUserId(userId);
//    if (twitters != null) {
//      for (UserTwitter twitter : twitters) {
//        if (twitter != null && twitter.getStatus() == TwitterStatus.ACTIVE) {
//          shareUrl = shareUrl + "&currentSellerShopId=" + shopService.findByUser(userId).getId();
//        }
//      }
//    }
//    model.addAttribute("shareUrl", shareUrl);

    return "catalog/product";
  }

  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/yundou/{id}")
  public String viewYundou(@PathVariable("id") String id, Model model, HttpServletRequest req,
      HttpServletResponse resp,
      @RequestParam(value = "union_id", defaultValue = "") String unionId,
      @RequestParam(value = "identity", defaultValue = "") String identity) {

    YundouProductVO yundouProduct = yundouProductService.load(id);
    if (yundouProduct == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "积分商品不存在");
    }
    String productId = yundouProduct.getProductId();
    ProductVO productVO = productService.load(productId);

    // 计算折扣价
    BigDecimal originalPrice = productVO.getPrice();
    BigDecimal discountPrice = yundouProductService.getDiscountPrice(originalPrice, id);
    productVO.setDiscountPrice(discountPrice);
    YundouSetting setting = yundouSettingService.loadUnique();
    yundouProduct.setSetting(setting.getAmount());

    productVO.setYundouVO(yundouProduct);
    productVO.setIsYundou(true);
    getBasicProductInfo(productVO, model, req);
    return "catalog/product_yundou";
  }

  /**
   * html5 页面 地址==/p/promotion/{id} 查看活动商品
   *
   * @param productId 商品id
   */
  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/promotion/{id}")
  public String viewPromotion(@PathVariable("id") String productId, Model model,
      HttpServletRequest req, HttpServletResponse resp,
      @RequestParam(value = "union_id", defaultValue = "") String unionId,
      @RequestParam(value = "identity", defaultValue = "") String identity,
      @RequestParam(value = "promotionId") String promotionId,
      @RequestParam(required = false) String activityGrouponId,
      @RequestParam(required = false) String from
  ) {
    Promotion promotion = promotionService.load(promotionId);
    if (promotion == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在");
    }
    Date validTo = promotion.getValidTo();
    if (!"view".equals(from) && new Date().after(validTo)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已结束");
    }
    FlashSalePromotionProductVO promotionProduct =
        (FlashSalePromotionProductVO) flashSalePromotionProductService
            .loadPromotionProductByPId(productId);
    if (promotionProduct == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动商品不存在");
    }
    PromotionType type = promotion.getPromotionType();

    getProductInfo(productId, model, req, promotion, activityGrouponId, from);

    // 获取当前用户
    if (!StringUtils.isEmpty(unionId)) {
      User user = userService.load(unionId);
      if (user != null) {
        Cookie c = new Cookie("union_id", unionId);
        c.setMaxAge(86400 * 15);
        c.setPath("/");
        resp.addCookie(c);
      }
    }

    // 区分进入商品详情页的是商家还是客户
    if (!StringUtils.isEmpty(identity)) {
      model.addAttribute("identity", identity);
    }

    // 购物车内商品总数
    User user = getCurrentUser();
    String shopId = user.getCurrentSellerShopId();
    Integer cartCount;
    String userId = user.getId();
    Long cpId = user.getCpId();
    cartCount = cartService.countByShopId(userId, shopId);
    model.addAttribute("cartCount", cartCount);
    model.addAttribute("promotionId", promotionId);
    model.addAttribute("promotionProduct", promotionProduct);
    model.addAttribute("promotion", promotion);

    // 微信分享链接地址
    String shareUrl = siteHost + "/p/promotion/" + productId + "?promotionId=" + promotionId
        + "?cpId=" + cpId;

    // 用户从首页进入团购活动详情时，如果用户参与过此团购活动，getProductInfo会返回此团购活动的activityGrouponId modify by chh 2017-10-23
//    if (StringUtils.isEmpty(activityGrouponId)) {
//      Map modelMap = model.asMap();
//      if (modelMap.containsKey("activityGrouponId")) {
//        activityGrouponId = (String) modelMap.get("activityGrouponId");
//      }
//    }
    // 砍价活动进入前，判断用户是否参加过此砍价活动，如果是，则分享链接加上activityid
//    if (type != null && type == PromotionType.BARGAIN && StringUtils.isEmpty(activityGrouponId)) {
//      PromotionBargainDetail bargainDetail = bargainDetailService
//          .selectByUserAndPromotionId(promotionId, productId, userId);
//      if (bargainDetail != null) {
//        activityGrouponId = bargainDetail.getId();
//      }
//    }

//    if (StringUtils.isNotEmpty(activityGrouponId)) {
//      shareUrl = siteHost + "/p/promotion/share/" + productId + "?promotionId=" + promotionId
//          + "&activityId=" + activityGrouponId;
//    }
    // 判断当前用户，如果是推客，则分享商品时应该带上推客的店铺，否则分享商品带上当前卖家的店铺
//    List<UserTwitter> twitters = userTwitterService.selectByUserId(userId);
//    if (twitters != null) {
//      for (UserTwitter twitter : twitters) {
//        if (twitter != null && twitter.getStatus() == TwitterStatus.ACTIVE) {
//          shareUrl = shareUrl + "&currentSellerShopId=" + shopService.findByUser(userId).getId();
//        }
//      }
//    }
    model.addAttribute("shareUrl", shareUrl);

    return type.getUrl();
  }

  @RequestMapping(UrlHelper.PRODUCT_URL_PREFIX + "/promotion/share/{id}")
  public String viewPromotionShare(@PathVariable("id") String productId,
      @RequestParam String promotionId, @RequestParam String activityId, Model model) {
    Promotion promotion = promotionService.load(promotionId);
    if (promotion == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在");
    }
    Date validTo = promotion.getValidTo();
    if (new Date().after(validTo)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已结束");
    }
    ProductVO productVO = productService.load(productId);
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String imgUrl = resourceFacade.resolveUrl(productVO.getImg());
    productVO.setImgUrl(imgUrl);

    promotionCheckService.checkFlashSaleProduct(productVO, promotion, activityId, model);
    PromotionType type = promotion.getPromotionType();

    String joinUrl = siteHost + "/p/promotion/" + productId + "?promotionId=" + promotionId;
    String grouponUrp = joinUrl + "&activityGrouponId=" + activityId;
    model.addAttribute("product", productVO);
    model.addAttribute("joinUrl", joinUrl);
    model.addAttribute("grouponUrl", grouponUrp);

    // 微信分享链接地址
    String shareUrl = siteHost + "/p/promotion/share/" + productId + "?promotionId=" + promotionId
        + "&activityId=" + activityId;

    model.addAttribute("shareUrl", shareUrl);

    return type.getUrl() + "_share";
  }

  /**
   * b2b进货功能的html5 页面 地址==/p/purchase/{id} 查看商品详情
   */
  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/purchase/{id}")
  public String viewPurchase(@PathVariable("id") String productId, Model model,
      HttpServletRequest req, HttpServletResponse resp,
      @RequestParam(value = "union_id", defaultValue = "") String unionId,
      @RequestParam(value = "identity", defaultValue = "") String identity
  ) {

    String userId = getCurrentUser().getId();
    String currentSellerShopId = req.getParameter(CURRENT_SELLER_SHOP_ID_PARAM_NAME);
    if (StringUtils.isNotEmpty(currentSellerShopId)) {
      getCurrentUser().setCurrentSellerShopId(currentSellerShopId);
    }

    // 联合创始人或董事类型的经销商，上级默认为总部
    UserAgentVO userAgentVO = userAgentService.selectByUserId(userId);
    AgentType agentType = userAgentVO.getType();
    if (agentType == AgentType.DIRECTOR || agentType == AgentType.FOUNDER) {
      getCurrentUser().setCurrentSellerShopId(userAgentService.getRootShopId());
    }
    getProductInfo(productId, model, req);

    // 获取当前用户
    if (!StringUtils.isEmpty(unionId)) {
      User user = userService.load(unionId);
      if (user != null) {
        Cookie c = new Cookie("union_id", unionId);
        c.setMaxAge(86400 * 15);
        c.setPath("/");
        resp.addCookie(c);
      }
    }

    // 区分进入商品详情页的是商家还是客户
    if (!StringUtils.isEmpty(identity)) {
      model.addAttribute("identity", identity);
    }

    // 购物车内商品总数
    String shopId = getCurrentUser().getCurrentSellerShopId();
    Integer cartCount = 0;
    cartCount = cartService.countByShopId(userId, shopId);
    model.addAttribute("cartCount", cartCount);
    return "agent/product";
  }

  /**
   * 获取基本商品信息存放到model, pro
   *
   * @param productVO 商品信息
   * @param model model
   * @param req request
   * @return seller
   */
  private void getBasicProductInfo(ProductVO productVO, Model model, HttpServletRequest req) {

    String productId = productVO.getId();

    // FIXME:out-sync case should not handle in here
    // need better control for distribution product info sync on writting but not reading
    // check source product
    Product prod = productService.findProductById(productId);
    log.info("sync dist prod" + JSON.toJSONString(prod));
    log.info("source prod id" + prod.getSourceProductId());
    if (!prod.getSourceProductId().isEmpty()) {
      // sync source product status and amount
      Product srcProduct = productService.findProductById(prod.getSourceProductId());
      if (srcProduct != null) {
        productVO.setStatus(srcProduct.getStatus());
        productVO.setAmount(srcProduct.getAmount());
        log.info("sync status" + srcProduct.getStatus() + " and amount:" + srcProduct.getAmount());
      }
    }

    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String imgUrl = resourceFacade.resolveUrl(productVO.getImg());
    productVO.setImgUrl(imgUrl);
    List<Sku> skus = skuMapper.selectByProductId(productId);
    if(CollectionUtils.isNotEmpty(skus)){
      Sku sku = skus.get(0);
      productVO.setDeductionDPoint(sku.getDeductionDPoint());
      productVO.setPoint(sku.getPoint());
      productVO.setPrice(sku.getPrice());
      productVO.setAmount(sku.getAmount());
    }
    model.addAttribute("product", productVO);

    // 获取店铺内商品总数
//    long productCount = productService.countProductsBySales(rootShopId);
//    model.addAttribute("productCount", productCount);

    // 判断用户是否收藏过该商品
    ProductCollection productCollection = productCollectionService
        .selectByProductId(getCurrentUser().getId(), productId);
    if (productCollection != null) {
      model.addAttribute("isCollection", true);
    } else {
      model.addAttribute("isCollection", false);
    }

    model.addAttribute("siteHost", siteHost);
  }

  /**
   * 获取活动商品，存放到model中
   *
   * @param productVO 商品信息
   * @param model model
   * @param req request
   * @param promotion 活动
   * @param activityId 活动商品id，groupon为activityGrouponId, bargain为detailId
   * @param from 目前值只有view 表示入口为 "我的" ，隐藏部分按钮
   */
  private void getPromotionProductInfo(ProductVO productVO, Model model, HttpServletRequest req,
      Promotion promotion, String activityId, String from) {
    // 判断商品是否在活动中， 如果活动为空则判断是否在限时抢购中
    PromotionStatus status = promotionCheckService
        .checkFlashSaleProduct(productVO, promotion, activityId, model);

    // 如果from为view则不需要检查时间、库存，忽略状态异常
    boolean fromView = StringUtils.equals("view", from);
    boolean promotionSellout = false;
    if (status == PromotionStatus.NOT_IN_PROMOTION) {
      // 满减、满件活动标识
//      Map<String, List<Promotion>> fullReducePromotions = promotionService
//          .listReduceMapPromotionByProductId(productVO.getId(), new Date(), null);
//      if (MapUtils.isNotEmpty(fullReducePromotions)) {
//        model.addAttribute("fullReduces", fullReducePromotions);
//      }
    } else if (status == PromotionStatus.SELL_OUT) {
      promotionSellout = true;
    } else if (status != PromotionStatus.NORMAL && !fromView) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, status.getMessage());
    }
    model.addAttribute("promotionSellout", promotionSellout);

    model.addAttribute("fromView", fromView);

    // TODO wangxinhua 临时处理团购按钮隐藏
    String currentUserId = getCurrentUser().getId();
//    boolean showGrouponBtn = true;
//    if (promotion != null && promotion.getPromotionType() == PromotionType.GROUPON) {
//      List<ActivityGrouponDetailVO> detailVOS = productVO.getGrouponVO().getUsers();
//      if (detailVOS != null && detailVOS.size() > 0) {
//        for (ActivityGrouponDetailVO detailVO : detailVOS) {
//          String userId = detailVO.getUserId();
//          if (StringUtils.equals(userId, currentUserId)) {
//            // 当前用户在detail中说明参与过团购
//            showGrouponBtn = false;
//            break;
//          }
//        }
//      }
//    }
    boolean isStart = true;
    if (StringUtils.isNotBlank(activityId)) {
      isStart = false;
    }
    // 活动是否已存在
    model.addAttribute("isStart", isStart);
    // 是否显示团购按钮
//    model.addAttribute("showGrouponBtn", showGrouponBtn);
  }

  private User getProductInfo(String productId, Model model, HttpServletRequest req) {
    return this.getProductInfo(productId, model, req, null, null, null);
  }

  /**
   * 获取商品与活动商品的信息，放到model中
   *
   * @param productId 商品id
   * @param model model
   * @param req request
   * @param promotion 活动
   * @param activityGrouponId 活动商品id
   * @param from view
   * @return 卖家
   */
  private User getProductInfo(String productId, Model model, HttpServletRequest req,
      Promotion promotion, String activityGrouponId, String from) {
    // 获取商品Vo
    User user = getCurrentUser();
    String realShopId = shopService.loadRootShop().getId();
    ProductVO productVO = productService.load(productId, Boolean.TRUE);
    if (productVO == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("proudct.not.found"));
    }

    List<Sku> skus = productVO.getSkus();
    assert skus != null && skus.size() == 1;
    Integer skuAmount = productVO.getAmount();
    // 是否有原始商品库存
    boolean sellOut = skuAmount == null || skuAmount <= 0;
    model.addAttribute("sellout", sellOut);

    productVO.setShopId(realShopId);

    // 获取基本商品信息
    getBasicProductInfo(productVO, model, req);
    // 获取活动商品信息
    getPromotionProductInfo(productVO, model, req, promotion, activityGrouponId, from);

    // 获取基本商品信息
//    return getBasicProductInfo(productVO, model, req);
    return user;
  }

  /**
   * 商品详情
   */
  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/apidesc/{id}")
  public String apiDes(@PathVariable("id") String productId, Model model, HttpServletRequest req) {
    getProductInfo(productId, model, req);
    return "catalog/apidesc";
  }

  private List<String> imgsList(String productId) {
    List<String> imgsList = new ArrayList<String>();
    List<ProductImage> imgs = productService
        .requestImgs(String.valueOf(IdTypeHandler.decode(productId)), "");
    for (ProductImage img : imgs) {
      imgsList.add(resourceFacade.resolveUrl(img.getImg() + "|" + ResourceFacade.IMAGE_S1));
    }

    return imgsList;
  }

  private List<FragmentImageVO> imgsListSize(String productId) {
    List<FragmentImageVO> imgsList = new ArrayList<FragmentImageVO>();
    List<ProductImage> imgs = productService
        .requestImgs(String.valueOf(IdTypeHandler.decode(productId)), "");
    for (ProductImage img : imgs) {
      FragmentImageVO vo = new FragmentImageVO();
      Image image = imageService.loadByImgKey(img.getImg());
      String url = resourceFacade.resolveUrl(img.getImg() + "|" + ResourceFacade.IMAGE_S1);
      if (image != null) {
        Integer width = image.getWidth();
        Integer height = image.getHeight();
        vo.setImgUrl(url);
        if (width != null) {
          vo.setHeight(height);
        }
        if (height != null) {
          vo.setWidth(width);
        }
      }
      imgsList.add(vo);
    }

    return imgsList;
  }

  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/raw/{id}")
  public String redirectToView(@PathVariable("id") String id) {
    String productId = IdTypeHandler.encode(Long.parseLong(id));
    return "redirect:/p/" + productId;
  }

  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX + "/related/{shopId}/{prodId}")
  public String productsRelated(@PathVariable("shopId") String shopId,
      @PathVariable("prodId") String productId,
      Model model, @PageableDefault Pageable page, HttpServletRequest req) {
    // TODO 分页
    List<Product> related = productService
        .listProductsByRelated(shopId, productId, new PageRequest(0, 100));
    model.addAttribute("relatedProds", related);

    return "catalog/productsRelated";
  }

  /**
   * 商品查询
   */
  @RequestMapping("/catalog/search")
  public void searchView(@RequestParam("shopId") String shopId,
      @RequestParam(value = "key", required = false) String key, Model model) {
    if (StringUtils.isNotBlank(key)) {
      List<Product> prods = productService.search(shopId, key.trim(), ProductType.NORMAL);
      model.addAttribute("prods", prods);
      if (prods == null || prods.size() == 0) {
        model.addAttribute("keyWord", key);
      }
    }
  }

  /**
   * 全部商品页
   */
  @RequestMapping("/catalog/allProducts")
  public String allProducts(@RequestParam(value = "shopId", required = true) String shopId,
      @RequestParam(value = "categoryId", required = false) String categoryId,
      @RequestParam(value = "keywords", required = false) String keywords, Model model,
      HttpServletRequest req) {
    User user = getCurrentUser();
    Shop shop = shopService.load(shopId);
    if (shop == null || BooleanUtils.isTrue(shop.getArchive())) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }
    String rootShopId = shopTreeService.selectRootShopByShopId(shop.getId()).getRootShopId();
    model.addAttribute("rootShopId", rootShopId);
    model.addAttribute("shop", shop);
    model.addAttribute("categoryId", categoryId);
    model.addAttribute("keywords", keywords);

    // 购物车内商品总数
    Integer cartCount = 0;
    String userId = this.getCurrentUser().getId();
    cartCount = cartService.countByShopId(userId, shopId);
    model.addAttribute("cartCount", cartCount);

    Category category = categoryService.load(categoryId);
    // 品牌团分类点击后跳转到专属的一个页面，其他类别共用一个页面
    if (category != null && "品牌团".equals(category.getName())) {
      return "b2b/findMore/findBrand";
    } else {
      return "b2b/findMore/findMore";
    }
  }

  /**
   * 商品保存，新增和修改共用一个 api，接口
   */
  @ResponseBody
  @RequestMapping(value = UrlHelper.PRODUCT_URL_PREFIX
      + "/addDistributionProduct")
  public ResponseObject<String> addDistributionProduct(
      @RequestParam("productId") String productId,
      @RequestParam("shopId") String shopId) {
    Product productSrc = productService.findProductById(productId);
    Product product = new Product();
    BeanUtils.copyProperties(productSrc, product);

    product.setSourceProductId(productSrc.getId());
    product.setSourceShopId(productSrc.getShopId());
    product.setIsDistribution(true);

    product.setUserId(userService.getCurrentUser().getId());
    product.setStatus(productSrc.getStatus());
    Shop shop = shopService.mine();
    product.setShopId(shop.getId());

    List<Sku> skus = skuMapper.selectByProductId(productId);
    List<SkuMapping> skuMapping = skuMappingMapper
        .selectByProductId(productId);
    List<ProductImage> imgs = productImageMapper
        .selectByProductId(productId);

    // List<Tag> tags = initTag( form);
    List<Tag> tags = new ArrayList<Tag>();
    product.setImg(null != imgs && imgs.size() > 0 ? imgs.get(0).getImg()
        : null);

    if (productSrc.getEnableDesc() == null) {
      product.setEnableDesc(false);
    }
    product.setAmount(productSrc.getAmount());
    product.setArchive(productSrc.getArchive());
    product.setCreatedAt(productSrc.getCreatedAt());
    product.setDelayed(productSrc.getDelayed());
    product.setDelayAt(productSrc.getDelayAt());

    int rc = 0;

    product.setSource(ProductSource.CLIENT);

    rc = productService.create(product, skus, tags, null, imgs, skuMapping);

    if (rc == 0) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "product");
    }

    ProductVO vo = productService.load(product.getId(), Boolean.TRUE);
    convertUrl(vo); // 生成商品的图片URL
    formatSynchronousVal(vo);
    return new ResponseObject<String>(String.valueOf(rc));

  }

  /**
   * 生成商品的图片URL
   */
  private void convertUrl(ProductVO vo) {
    if (null != vo.getImgs() && vo.getImgs().size() > 0) {
      for (ProductImageVO img : vo.getImgs()) {
        img.setImgUrl(img.getImg());
      }
    }

    vo.setCategory(categoryService.loadCategoryByProductId(vo.getId()));
  }

  // 格式化商品来源渠道
  private void formatSynchronousVal(Product product) {
    String synchronousVal = "";
    String synchronous = product.getSynchronousFlag();
    if (synchronous != null && synchronous.length() > 1) {
      if (synchronous.substring(9, 10).equals("1")) {
        synchronousVal += "XIANGQU" + ",";
      }
    }
    product.setSynchronousFlag(synchronousVal);
  }

  /**
   * 查看拼团详情
   */
  @RequestMapping("/groupon/{id}")
  public String view(@PathVariable("id") String grouponId, Model model, HttpServletRequest req,
      HttpServletResponse resp) {
    PromotionGrouponVO promotionGrouponVO = promotionGrouponService.selectByPrimaryKey(grouponId);
    String productId = promotionGrouponVO.getProductId();
    getProductInfo(productId, model, req);
    model.addAttribute("groupon", promotionGrouponVO);
    return "groupon/groupon";
  }

}
