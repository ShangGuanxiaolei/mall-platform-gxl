

package com.xquark.web;

import com.xquark.dal.IUser;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.User;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.service.wechat.WechatService;
import com.xquark.service.wechat.WechatUtils;
import com.xquark.utils.UserAgentUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * sellerpc 控制器拦截
 */
@ControllerAdvice
public class BaseControllerAdvice {

  protected Logger log = LoggerFactory.getLogger(BaseControllerAdvice.class);

  private static final String[] CALL_JSAPI_MODULE = {"/p/", "/twitter/", "/shop",
      "/twitterCenter/promotion"};

  @Value("${profiles.active}")
  String profile;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @Autowired
  private WechatService wechatService;

  @Autowired
  private ShopService shopService;

  /**
   * 所有用于view层的环境变量
   */
  @ModelAttribute
  public void env(Model model, HttpServletRequest request) {
    Map<String, Object> applicationEnv = new HashMap<String, Object>();
    applicationEnv.put("profile", profile);
    applicationEnv.put("isFromWechat", WechatUtils.isFromWechatApp(request));
    applicationEnv.put("isFromApp", UserAgentUtils.isFromApp(request));
    model.addAttribute("applicationEnv", applicationEnv);
  }

  @ModelAttribute("user")
  public IUser currentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        return (IUser) principal;
      }

      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    return null;
  }

  @ModelAttribute
  public void jsApiConfig(Model model, HttpServletRequest request) {
    if (!WechatUtils.isFromWechatApp(request)) {
      return;
    }

    boolean callJsApi = false;
    for (int i = 0; i < CALL_JSAPI_MODULE.length; i++) {
      if (StringUtils.startsWith(request.getRequestURI(), CALL_JSAPI_MODULE[i])) {
        callJsApi = true;
        break;
      }
    }

    if (!callJsApi) {
      return;
    }

    String shopId = shopService.loadRootShop().getId();
    WechatAppConfig appConfig = wechatAppConfigService.load(shopId);
    String timestamp = Long.toString(new Date().getTime() / 1000);
    String noncestr = CommonUtil.CreateNoncestr();

    //sign
    String jsApiTicket = wechatService.obtainJsapiTicket(appConfig);
    if (jsApiTicket == null) {
      log.error("can not obtain jsapi ticket.");
      return;
    }
    String reqQueryStr = request.getQueryString();
    StringBuffer reqURL = request.getRequestURL();
    String url = reqURL + (StringUtils.isEmpty(reqQueryStr) ? "" : "?" + reqQueryStr);
    String signValue =
        "jsapi_ticket=" + jsApiTicket + "&noncestr=" + noncestr + "&timestamp=" + timestamp
            + "&url=" + url;
    String signature = SHA1Util.Sha1((signValue));

    model.addAttribute("appId", appConfig.getAppId());
    model.addAttribute("timeStamp", timestamp);
    model.addAttribute("nonceStr", noncestr);
    model.addAttribute("signature", signature);
    model.addAttribute("wxJsapiCall", "wxJsapiCall");
  }

}

