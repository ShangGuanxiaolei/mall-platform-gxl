package com.xquark.web.wechat;

import com.xquark.service.wechat.WechatService;
import com.xquark.web.BaseController;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.oauth.OAuthException;
import com.xquark.wechat.oauth.OAuthManager;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenRequest;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenResponse;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoRequest;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 微信接口请求的入口
 */
@Controller
public class WebEntryController extends BaseController {

  @Autowired
  private WechatService wechatService;

  @RequestMapping(value = "/wechat/entry")
  @ResponseBody
  public String entry(HttpServletRequest request, HttpServletResponse response) {
    String ret = "";
    ret = wechatService.onEventDispatch(request);
    /**
     * // 微信加密签名
     String signature = request.getParameter("signature");
     // 随机字符串
     String echostr = request.getParameter("echostr");
     // 时间戳
     String timestamp = request.getParameter("timestamp");
     // 随机数
     String nonce = request.getParameter("nonce");

     String[] str = { "xquark2016", timestamp, nonce };
     Arrays.sort(str); // 字典序排序
     String bigStr = str[0] + str[1] + str[2];
     // SHA1加密
     // String digest = new SHA1().getDigestOfString(bigStr.getBytes()).toLowerCase();

     // 确认请求来至微信
     try{
     //if (digest.equals(signature)) {
     response.getWriter().print(echostr);
     //}

     }catch (Exception e){

     }**/
    return ret;
  }

  @RequestMapping(value = "/wechat/getOauthRedirectUrl")
  @ResponseBody
  public String getOauthRedirectUrl() {
    String url = "";

    //TODO 根据当前用户访问的店铺拿到config
    Config config = new Config();
    url = OAuthManager.generateRedirectURI(config,
        "http://7nodmtvdlm.proxy.qqbrowser.cc/wechat/oauthCallback",
        OAuthManager.SCOPE_USERINFO,
        "带回来的参数"); //TODO 用户授权后应该访问的url

    return url;
  }

  @RequestMapping(value = "oauthCallback")
  public String oauthCallback(@RequestParam String code, @RequestParam String state) {
    if (StringUtils.isEmpty(code)) {
      return ""; //TODO 用户拒绝授权, 跳转url
    }

    try {
      //获取用户信息
      Config config = new Config(); //TODO 根据当前用户访问的店铺拿到config

      GetAccessTokenRequest getAccessTokenRequest = new GetAccessTokenRequest(config, code);
      GetAccessTokenResponse response = OAuthManager.getAccessToken(getAccessTokenRequest);
      String oauthAccessToken = response.getAccess_token();

      GetUserinfoRequest getUserinfoRequest = new GetUserinfoRequest(config, oauthAccessToken,
          response.getOpenid());
      GetUserinfoResponse getUserinfoResponse = OAuthManager.getUserinfo(getUserinfoRequest);
      getUserinfoResponse.getNickName();
      getUserinfoResponse.getAvatar();
      getUserinfoResponse.getOpenId();
      getUserinfoResponse.getUnionId();

      //TODO 根据返回结果保存用户信息

    } catch (OAuthException e) {
      log.error("获取用户信息失败" + e.getErrmsg(), e);
    } finally {
      state = ""; //出错页面
    }

    return state; // 跳转到用户授权后应该访问的url
  }

  @RequestMapping(value = "restoreWechatSession")
  public void restoreSession(@RequestParam String code, @RequestParam String state) {
    //示例代码
    try {
      //获取用户信息
      Config config = new Config(); //TODO 根据当前用户访问的店铺拿到config

      GetAccessTokenRequest getAccessTokenRequest = new GetAccessTokenRequest(config, code);
      GetAccessTokenResponse response = OAuthManager.getAccessToken(getAccessTokenRequest);

      //TODO 根据OPENID和当前公众号对应的店铺自动登录
      String openid = response.getOpenid();

    } catch (OAuthException e) {
      log.error("获取用户信息失败" + e.getErrmsg(), e);
    } finally {
      state = ""; //出错页面
    }
  }
}
