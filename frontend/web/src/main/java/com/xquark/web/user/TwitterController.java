package com.xquark.web.user;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.xquark.biz.filter.UserServletFilter;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.mapper.WeixinJSTicketMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.commission.CommissionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pay.PayRequestService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.team.TeamService;
import com.xquark.service.team.UserTeamService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.twitter.impl.TwitterShopComServiceImpl;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.service.wechat.WechatService;
import com.xquark.utils.EmojiFilter;
import com.xquark.utils.ImgUtils;
import com.xquark.utils.ResourceResolver;
import com.xquark.web.BaseController;
import com.xquark.web.vo.Json;
import com.xquark.wechat.user.LanguageType;
import com.xquark.wechat.user.UserManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
public class TwitterController extends BaseController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private WechatService wechatService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private TinyUrlService tinyUrlService;

  @Autowired
  private CommissionService commissionService;

  @Autowired
  WithdrawApplyService withdrawApplyService;

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Autowired
  private OutPayService outPayService;

  @Autowired
  PayRequestService payRequestService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private TeamService teamService;

  @Autowired
  private UserTeamService userTeamService;

  @Value("${site.web.host.name}")
  String siteHost;

  @Autowired
  private WeixinJSTicketMapper weixinJSTicketMapper;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @Autowired
  private TwitterShopComServiceImpl twitterShopComServiceImpl;

  @RequestMapping(value = "/twitterCenter/center")
  public String twitterCenter(Model model, HttpServletRequest req) {

    //TODO toliangfan 增加团队人数
    String currentShopId = getCurrentUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(currentShopId).getRootShopId();
    String userId = shopService.load(currentShopId).getOwnerId();
    SubAccount avalidSubAccount = accountApiService
        .findSubAccountByUserId(getCurrentUser().getId(), AccountType.AVAILABLE);
    SubAccount commissionSubAccount = accountApiService
        .findSubAccountByUserId(getCurrentUser().getId(), AccountType.COMMISSION);

    model.addAttribute("commissionCount",
        avalidSubAccount.getBalance().add(commissionSubAccount.getBalance()).setScale(2));
    model.addAttribute("commissionCanWithdraw", avalidSubAccount.getBalance().setScale(2));

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("userPhone", StringUtils.defaultIfBlank(userService.load(userId).getPhone(), null));
    //params.put("status", form.getCommission_status_kwd());
    Long orderCount = commissionService.countCommissionsByAdmin(params);
    //Long orderCount = orderService.countSellerOrdersByStatus(null);
    model.addAttribute("orderCount", orderCount);

    // 提现记录条数
    int withdrawCount = withdrawApplyService.countWithdrawApply(userId);
    model.addAttribute("withdrawCount", withdrawCount);

    if (StringUtils.isNotBlank(rootShopId) && StringUtils.isNotBlank(currentShopId)) {
      Long myTwitterCount = shopTreeService.countDirectChildren(rootShopId, currentShopId);
      model.addAttribute("myTwitterCount", myTwitterCount);
    }

    return "twitter/twitterCenter";
  }

  @RequestMapping(value = "/twitterCenter/order")
  public String twitterOrder(@RequestParam("status") String status, Model model,
      HttpServletRequest req) {
    model.addAttribute("status", status);
    return "twitter/twitterOrder";
  }

  @RequestMapping(value = "/twitterCenter/promotion")
  public String twitterPromotion(Model model, HttpServletRequest req) {
    String shareUrl = "";
    String userId = getCurrentUser().getId();
    User user = userService.load(userId);
    String shopId = user.getShopId();
    Shop shop = shopService.load(shopId);
    Team team = teamService.selectByUserId(userId);
    if (team != null) {
      shareUrl = getTeamShareUrl(req);
    } else {
      shareUrl = getShareUrl(req);
    }

    model.addAttribute("shareTitle", "招募合伙人,带你赚钱带你飞");
    model.addAttribute("shareUrl", shareUrl);
    model.addAttribute("shopId", shopId);
    model.addAttribute("shopName", shop.getName());
    model.addAttribute("avatar", user.getAvatar());
    model.addAttribute("qrcodeUrl", "/twitterCenter/promotion/qrcode/" + shopId);

    // 加载微信认证数据
    // 在BaseControllerAdvice里会统一处理微信信息
    // loadWeixinShareTicket(req,model,shopId);

    return "twitter/twitterPromotion";
  }

  private String getShareUrl(HttpServletRequest req) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/shop/" + getCurrentUser().getShopId()
            + "?currentSellerShopId=" + getCurrentUser().getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 战队中的推客分享的地址链接
   */
  private String getTeamShareUrl(HttpServletRequest req) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/twitter/teamPromotion/" + getCurrentUser()
            .getShopId());
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  @RequestMapping(value = "/twitterCenter/promotion/qrcode/{shopId}", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] generateQrcode(@PathVariable("shopId") String shopId, HttpServletRequest req) {

    //生成一张制定规格的白色图片
    //将百盛u+logo覆盖在白色图片的指定位置
    //将用户logo覆盖在白色图片的指定位置
    //写入店铺：店铺名
    //将店铺二维码覆盖到指定位置
    int width = 468;
    int height = 624;
    Shop shop = shopService.load(shopId);
    User user = userService.load(shop.getOwnerId());
    String shopName = shop.getName();
    String imgUrl = user.getAvatar();
    String bannerUrl = "http://images.handeson.com/Ftihf_rxLYPV95uKM--IZ4xzrxE0";
    String img = shop.getImg();
    String banner = shop.getBanner();
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    //String imgUrl = resourceFacade.resolveUrl(img);
    //String bannerUrl = resourceFacade.resolveUrl(banner);
    int imgW = 100;
    int imgH = 100;
    int bannerW = 468;
    int bannerH = 624;
    BufferedImage imageBanner = ImgUtils.getImageFromNetByUrl(bannerUrl);
    BufferedImage imageBannerResize = ImgUtils.resizeImage(imageBanner, bannerW, bannerH);
    BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
    BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, imgW, imgH);
    BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);
    BufferedImage ImageQrcode = null;
    try {
      HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);

      BitMatrix byteMatrix;

      String shareUrl = "";
      String twitterUserId = shop.getOwnerId();
      Team team = teamService.selectByUserId(twitterUserId);
      if (team != null) {
        shareUrl = getTeamShareUrl(req);
      } else {
        shareUrl = getShareUrl(req);
      }

      byteMatrix = new MultiFormatWriter().encode(shareUrl, BarcodeFormat.QR_CODE, 240, 240, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (WriterException e) {
      log.error("generateQrcode error", e);
      throw new RuntimeException(e);
    }

    Font font = new Font("宋体", Font.PLAIN, 27);
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setBackground(Color.WHITE);
    g2.clearRect(0, 0, width, height);
    g2.setPaint(Color.black);
    g2.setFont(font);

    // 百盛u+logo
    g2.drawImage(imageBannerResize, 0, 0, null);
    // 用户logo
    g2.drawImage(imageImgRect, 185, 115, null);
    // 店铺名
    g2.drawString(shopName, 172, 270);
    // 店铺二维码
    g2.drawImage(ImageQrcode, 110, 300, null);
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      log.error("generate png error", e);
      throw new RuntimeException(e);
    }

    return os.toByteArray();//从流中获取数据数组。


  }

  public static void main(String args[]) {

    //生成一张制定规格的白色图片
    //将百盛u+logo覆盖在白色图片的指定位置
    //将用户logo覆盖在白色图片的指定位置
    //写入店铺：店铺名
    //将店铺二维码覆盖到指定位置
    int width = 468;
    int height = 624;
    String shopName = "二维码";
    String bannerUrl = "http://images.handeson.com/Ftihf_rxLYPV95uKM--IZ4xzrxE0";
    String imgUrl = "http://wx.qlogo.cn/mmhead/L3Qib0nCc28kcwlcOk6b8hqEnicdq2Vu4SF23XRDJEiaR7DItlRQn4H9A/0";
    //String imgUrl = resourceFacade.resolveUrl(img);
    //String bannerUrl = resourceFacade.resolveUrl(banner);
    int bannerW = 468;
    int bannerH = 624;
    int imgW = 100;
    int imgH = 100;
    BufferedImage imageBanner = ImgUtils.getImageFromNetByUrl(bannerUrl);
    BufferedImage imageImg = ImgUtils.getImageFromNetByUrl(imgUrl);
    BufferedImage imageBannerResize = ImgUtils.resizeImage(imageBanner, bannerW, bannerH);
    BufferedImage imageImgResize = ImgUtils.resizeImage(imageImg, imgW, imgH);
    BufferedImage imageImgRect = ImgUtils.drawRect(imageImgResize);
    BufferedImage ImageQrcode = null;
    try {
      HashMap<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);

      BitMatrix byteMatrix;

      String shareUrl = "http://nffclub.com/twitter/teamPromotion/70wvwujg";

      byteMatrix = new MultiFormatWriter().encode(shareUrl, BarcodeFormat.QR_CODE, 240, 240, hints);
      ImageQrcode = MatrixToImageWriter.toBufferedImage(byteMatrix);
    } catch (WriterException e) {
      throw new RuntimeException(e);
    }

    Font font = new Font("宋体", Font.PLAIN, 26);
    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = (Graphics2D) bi.getGraphics();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
    g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING,
        RenderingHints.VALUE_COLOR_RENDER_QUALITY);
    g2.setBackground(Color.WHITE);
    g2.clearRect(0, 0, width, height);
    g2.setPaint(Color.black);
    g2.setFont(font);

    //
    g2.drawImage(imageBannerResize, 0, 0, null);
    g2.drawImage(imageImgRect, 185, 115, null);
    g2.drawString(shopName, 182, 270);
    g2.drawImage(ImageQrcode, 110, 300, null);
    ByteArrayOutputStream os = new ByteArrayOutputStream();//新建流。
    try {
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    try {
      ImageIO.write(bi, "png", os);//利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。

      File file = new File("/home/caohonghui/1.png");

      ImageIO.write(bi, "png", file);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @RequestMapping(value = "/twitterCenter/team")
  public String twitterTeam(@RequestParam(value = "status", required = false) String status,
      Model model, HttpServletRequest req) {
    if ("all".equals(status)) {

    } else if ("twitter".equals(status)) {

    } else if ("leader".equals(status)) {

    }
    model.addAttribute("status", status);
    return "twitter/twitterTeam";
  }

  @RequestMapping(value = "/twitter/apply/{shopId}")
  public String view(@PathVariable("shopId") String shopId, Model model, HttpServletRequest req,
      HttpServletResponse resp) {

    shopId = this.getShopId(shopId);

    Shop shop = shopService.load(shopId);
    ShopTree shopTree = shopTreeService.selectRootShopByShopId(shopId);
    if (shop == null || shop.getArchive() == Boolean.TRUE || shopTree == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }

    Shop rootShop = shopService.load(shopTree.getRootShopId());
    if (rootShop == null || rootShop.getArchive() == Boolean.TRUE) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }

    //用户当前申请的是总店下的推客,检查用户是否存在访问过推客小店的cookie,如果有则改为申请最后一次访问的推客小店的shop
    if (shop.getId().equals(rootShop.getId())) {
      String recommendShopId = shop.getId();
      Cookie[] cookies = req.getCookies();
      if (cookies != null) {
        for (Cookie cookie : cookies) {
          if (cookie.getName().equals("lastTwitterApplyShopId") && cookie.getValue() != null) {
            recommendShopId = cookie.getValue();
            break;
          }
        }
      }
      shop = shopService.load(recommendShopId);
      if (shop == null) {
        shop = rootShop;
      }
    } else { //用户申请的是推客小店下的推客,此时记录用户的访问shopId信息到cookie中
      Cookie lastTwitterApplyShopId = new Cookie("lastTwitterApplyShopId", shopId);
      lastTwitterApplyShopId.setMaxAge(Integer.MAX_VALUE);
      lastTwitterApplyShopId.setPath("/");
      resp.addCookie(lastTwitterApplyShopId);
    }

    WechatAppConfig appConfig = wechatAppConfigService.load(rootShop.getId());
    com.xquark.wechat.user.User wechatUser = UserManager
        .getUserInfo(getCurrentUser().getLoginname(), wechatService.obtainAccessToken(appConfig),
            LanguageType.zh_CN);

    List<UserTwitter> userTwitterApplies = userTwitterService
        .selectByUserId(getCurrentUser().getId());
    boolean applying = false;
    boolean approved = false;
    if (CollectionUtils.isNotEmpty(userTwitterApplies)) {
      for (UserTwitter userTwitterApply : userTwitterApplies) {
        if (TwitterStatus.APPLYING.equals(userTwitterApply.getStatus()) || TwitterStatus.FROZEN
            .equals(userTwitterApply.getStatus())) {
          applying = true;
          approved = false;
          break;
        } else if (TwitterStatus.ACTIVE.equals(userTwitterApply.getStatus())) {
          applying = false;
          approved = true;
          break;
        }
      }
    }

    if (applying) {
      model.addAttribute("applying", applying);
    }

    //进入申请页,此时申请已经通过,只是session未刷新,跳转到该用户自己的小店并且reload session
    if (approved) {
      String twitterShopId = userService.load(getCurrentUser().getId()).getShopId();
      return "redirect:/shop?" + UserServletFilter.CURRENT_SELLER_SHOP_ID_PARAM_NAME + "="
          + twitterShopId;
    }

    model.addAttribute("subscribed", wechatUser != null ? wechatUser.getSubscribe() : 0);
    model.addAttribute("shop", shop);
    model.addAttribute("wechatUser", wechatUser);
    model.addAttribute("rootShop", rootShop);

    // 推客别名显示
    String twitterAlias = twitterShopComServiceImpl.getTwitterAlias();
    model.addAttribute("twitterAlias", twitterAlias);

    return "twitter/apply";
  }

  @RequestMapping(value = "/twitter/applying")
  public String apply(@RequestParam("shopId") String shopId, @RequestParam("phone") String phone,
      @RequestParam(value = "realName", required = false) String realName,
      @RequestParam(value = "weixinAccount", required = false) String weixinAccount, Model model,
      HttpServletRequest req) {
    //FIXME duplicate phone in the same root shop by liangfan

    String nickname = EmojiFilter.filterEmoji(realName);
    // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
    if (nickname == null || "".equals(nickname)) {
      nickname = "普通用户";
    }

    // 更新用户对应的手机号和名称
    User user = getCurrentUser();
    User updateUser = new User();
    updateUser.setId(user.getId());
    updateUser.setPhone(phone);
    updateUser.setName(nickname);
    userService.updateUserInfo(updateUser);

    // 更新用户店铺对应的名称
    String currentShopId = user.getShopId();
    if (StringUtils.isNotEmpty(currentShopId)) {
      Shop currentShop = shopService.load(currentShopId);
      if (currentShop != null) {
        Shop updateShop = new Shop();
        updateShop.setId(currentShopId);
        updateShop.setName(nickname);
        shopMapper.updateByPrimaryKeySelective(updateShop);
      }
    }

    // 新增推客申请信息
    UserTwitter userTwitter = new UserTwitter();
    userTwitter.setStatus(TwitterStatus.APPLYING);
    userTwitter.setUserId(user.getId());
    userTwitter.setOwnerShopId(shopTreeService.selectRootShopByShopId(shopId).getRootShopId());
    userTwitter.setParentUserId(shopService.load(shopId).getOwnerId());
    userTwitterService.create(userTwitter);

    return "redirect:/shop/" + shopId +
        "?" + UserServletFilter.CURRENT_SELLER_SHOP_ID_PARAM_NAME + "=" + shopId;
  }

  @RequestMapping(value = "/partnerCenter/center")
  public String partnerCenter(Model model, HttpServletRequest req) {

    String currentShopId = getCurrentUser().getShopId();
    String rootShopId = shopTreeService.selectRootShopByShopId(currentShopId).getRootShopId();
    String userId = getCurrentUser().getId();
//        SubAccount avalidSubAccount = accountApiService.findSubAccountByUserId(getCurrentUser().getId(), AccountType.AVAILABLE);
//        SubAccount commissionSubAccount = accountApiService.findSubAccountByUserId(getCurrentUser().getId(), AccountType.COMMISSION);
    //todo liangfan  合伙人手动提现无法统计未结算
    model.addAttribute("commissionCount", 0.00);
    model.addAttribute("commissionCanWithdraw", 0.00);

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("userPhone", StringUtils.defaultIfBlank(userService.load(userId).getPhone(), null));
    //params.put("status", form.getCommission_status_kwd());
    //Long orderCount = commissionService.countCommissionsByAdmin(params);
    Long orderCount = new Long(0);
    //Long orderCount = orderService.countSellerOrdersByStatus(null);
    model.addAttribute("orderCount", orderCount);

    if (StringUtils.isNotBlank(rootShopId) && StringUtils.isNotBlank(currentShopId)) {
      Long myChildrenCount = shopTreeService.countChildren(rootShopId, currentShopId);
      model.addAttribute("myChildrenCount", myChildrenCount);
    }

    return "twitter/partnerCenter";
  }

  @RequestMapping(value = "/partnerCenter/team")
  public String partnerTeam(Model model, HttpServletRequest req) {

    return "twitter/partnerTeam";
  }

  @RequestMapping(value = "/partnerCenter/order")
  public String partnerOrder(Model model, HttpServletRequest req) {

    return "twitter/partnerOrder";
  }

  /**
   * 推客提现申请
   */
  @ResponseBody
  @RequestMapping(value = "/twitterCenter/withdrawapplay")
  public Json withdrawApplay(@RequestParam String commissionCount, HttpServletRequest req) {
    Json json = new Json();
    User currentUser = getCurrentUser();
    WithdrawApply withdrawApply = new WithdrawApply();
    withdrawApply.setType(currentUser.getWithdrawType());
    //提现方式为微信
    User user = userService.load(currentUser.getId());
    if (!StringUtils.isNoneBlank(user.getWeixinCode())) {
      log.warn("userId [" + user.getId() + "] 未绑定微信！");
      json.setRc(Json.RC_FAILURE);
      json.setMsg("未绑定微信,操作失败");
      return json;
    }

    ShopTree rootShop = shopTreeService.selectRootShopByShopId(user.getShopId());
    withdrawApply.setAccountName(user.getName());
    withdrawApply.setAccountNumber("" + IdTypeHandler.decode(rootShop.getRootShopId()));
    withdrawApply.setBankId(user.getId());
    withdrawApply.setOpeningBank("微信");
    withdrawApply.setApplyNo(withdrawApplyService.generateApplyNo());
    withdrawApply.setApplyMoney(new BigDecimal(commissionCount));
    withdrawApply.setUserId(currentUser.getId());
    withdrawApply.setStatus(WithdrawApplyStatus.NEW);
    int result = withdrawApplyService.insert(withdrawApply);
    if (result > 0) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("提现申请成功，待审核通过后钱会自动转帐到您的微信账户");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("操作失败");
    }
    return json;
  }

  @RequestMapping(value = "/twitterCenter/withdraw")
  public String twitterWithdraw(Model model, HttpServletRequest req) {
    return "twitter/twitterWithdraw";
  }


  /**
   * 战队界面，有申请推客，提交意向，直接购买选项
   */
  @RequestMapping(value = "/twitter/teamPromotion/{shopId}")
  public String teamPromotion(@PathVariable("shopId") String shopId, Model model,
      HttpServletRequest req, HttpServletResponse resp) {
    model.addAttribute("shopId", shopId);
    return "team/select";
  }

  /**
   * 战队提交推客意向界面
   */
  @RequestMapping(value = "/twitter/teamApply/{shopId}")
  public String teamApply(@PathVariable("shopId") String shopId, Model model,
      HttpServletRequest req, HttpServletResponse resp) {
    model.addAttribute("shopId", shopId);
    return "team/apply";
  }


}
