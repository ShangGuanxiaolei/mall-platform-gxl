package com.xquark.web;

import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * web工程的基类Controller
 *
 * @author odin
 */
public class BaseController {

  protected static final String COOKIE_NAME_KDSESSID = "KDSESSID";
  protected static final String COOKIE_NAME_KDAUTHTOKEN = "kdAuthToken";
  protected static final String COOKIE_NAME_FROMPAGE = "FromPage";
  protected static final String COOKIE_NAME_FROMCHANNEL = "FromChannel";
  public static final String COOKIE_NAME_INTERNALSHOP = "ISVToken";

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Value("${alipay.domain}")
  String alipayDomain;

  @Value("${profiles.active}")
  String profile;

  /**
   * 当界面传递过来的shopid为空时，默认取当前用户的sellershopid
   */
  public String getShopId(String shopId) {
    String shopId_ = "";
    if (StringUtils.isEmpty(shopId) || "undefined".equals(shopId)) {
      shopId_ = this.getCurrentUser().getCurrentSellerShopId();
    } else {
      shopId_ = shopId;
    }
    return shopId_;
  }

  /**
   * 获取当前用户信息 如果是未登录的匿名用户，系统根据匿名用户唯一码自动创建一个用户 具体逻辑查看：UniqueNoFilter
   */
  public User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      } else if (principal instanceof Merchant) {
        Shop shop = shopService.load(((Merchant) principal).getShopId());
        return userService.load(shop.getOwnerId());
      }

      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }
//
    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
//        User user = userService.loadByLoginname("18657562524");
//        user.setCurrentSellerShopId("8s1vtuat");
//        return user;
  }

  /**
   * 支付宝要求支付宝出现的页面不能在其他第三方域名下，只能在.kkkd.com下
   */
  @Deprecated
  protected String redirectDomain(HttpServletRequest request, HttpServletResponse response) {
    String redirect = "";
    if ("prod".equals(profile) // only redirect in prod env
        && !request.getServerName().toLowerCase().endsWith(alipayDomain)) {
      String requestURL = request.getRequestURI();
      if (request.getQueryString() != null) {
        requestURL += "?" + request.getQueryString();
      }

      try {
        requestURL = URLEncoder.encode(requestURL, "utf-8");
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
        log.error("requestURL encode error, requestURL:[" + requestURL + "]");
      }

      Cookie[] cookies = request.getCookies();
      String kdSessId = "";
      String kdAuthToken = "";
      String fromPage = "";
      String fromChannel = "";
      if (cookies != null) {
        for (Cookie cookie : cookies) {
          if (cookie.getName().equals(COOKIE_NAME_KDSESSID)) {
            kdSessId = cookie.getValue();
          }
          if (cookie.getName().equals(COOKIE_NAME_KDAUTHTOKEN)) {
            kdAuthToken = cookie.getValue();
          }
          // fix VD763 in jira -- set FromPage and FromChannel cookie to www.kkkd.com from xiangqu
          if (cookie.getName().equals(COOKIE_NAME_FROMPAGE)) {
            fromPage = cookie.getValue();
          }
          if (cookie.getName().equals(COOKIE_NAME_FROMCHANNEL)) {
            fromChannel = cookie.getValue();
          }
        }
      }
      // TODO will remove the hard code
      redirect = "redirect:http://www.kkkd.com/auth/redirect?kdSessId=" + kdSessId + "&kdAuthToken="
          + kdAuthToken
          + "&fromPage=" + fromPage + "&fromChannel=" + fromChannel + "&url=" + requestURL;
    }
    if (StringUtils.isNotEmpty(redirect)) {
      log.info("Url will redirect to: " + redirect);
    } else {
      log.info("Url will not redirect");
    }
    return redirect;
  }


}
