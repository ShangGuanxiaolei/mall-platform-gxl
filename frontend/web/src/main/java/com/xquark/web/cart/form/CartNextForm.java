package com.xquark.web.cart.form;

import com.xquark.dal.type.PromotionType;
import java.util.List;
import java.util.Map;

/**
 * @author tonghu
 */
public class CartNextForm {

  private PromotionType promotionFrom;

  private List<String> skuId;

  private Map<String, String> shopCoupons;

  // get from address return
  private Map<String, String> shopRemarksCache;

  private String shopId;

  private boolean fromCart;

  private int qty;//直接下单购买数量

  private String productId; // 直接下单购买代销商品的id

  private String promotionId;

  private String activityGrouponId;

  private String yundouProductId;

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public List<String> getSkuId() {
    return skuId;
  }

  public void setSkuId(List<String> skuId) {
    this.skuId = skuId;
  }

  public String getShopId() {
    return shopId;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Map<String, String> getShopRemarksCache() {
    return shopRemarksCache;
  }

  public void setShopRemarksCache(Map<String, String> shopRemarksCache) {
    this.shopRemarksCache = shopRemarksCache;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getActivityGrouponId() {
    return activityGrouponId;
  }

  public void setActivityGrouponId(String activityGrouponId) {
    this.activityGrouponId = activityGrouponId;
  }

  public String getYundouProductId() {
    return yundouProductId;
  }

  public void setYundouProductId(String yundouProductId) {
    this.yundouProductId = yundouProductId;
  }

  public boolean getFromCart() {
    return fromCart;
  }

  public void setFromCart(boolean fromCart) {
    this.fromCart = fromCart;
  }

  public PromotionType getPromotionFrom() {
    return promotionFrom;
  }

  public void setPromotionFrom(PromotionType promotionFrom) {
    this.promotionFrom = promotionFrom;
  }
}
