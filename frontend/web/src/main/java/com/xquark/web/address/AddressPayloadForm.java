package com.xquark.web.address;

import java.util.List;
import java.util.Map;

/**
 * Created by uzstudio on 15/12/1.
 */
public class AddressPayloadForm {

  private List<String> skuIds;
  private String skuId;

  private Map<String, String> shopCoupons;

  private Map<String, String> shopRemarks;

  private String shopId;

  private int qty;//直接下单购买数量

  private String productId; // 直接下单购买代销商品的id

  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public List<String> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<String> skuIds) {
    this.skuIds = skuIds;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getShopId() {
    return shopId;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Map<String, String> getShopRemarks() {
    return shopRemarks;
  }

  public void setShopRemarks(Map<String, String> shopRemarks) {
    this.shopRemarks = shopRemarks;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }
}
