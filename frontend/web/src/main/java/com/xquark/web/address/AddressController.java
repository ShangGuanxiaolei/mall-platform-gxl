package com.xquark.web.address;

import com.xquark.dal.model.Address;
import com.xquark.dal.model.Zone;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.zone.ZoneService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AddressController extends BaseController {

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @RequestMapping(value = "/addresses")
  @ResponseBody
  public ResponseObject<List<AddressVO>> list() {
    List<AddressVO> addresses = addressService.listUserAddressesVo();
    return new ResponseObject<List<AddressVO>>(addresses);
  }

  @RequestMapping("/address/add")
  public String add(@ModelAttribute AddressPayloadForm form, HttpServletRequest req, Model model,
      @RequestHeader(value = "Referer", required = false) String referer) {

    String skuId = req.getParameter("skuId");
    String shopId = req.getParameter("shopId");
    String isCrossBorder = req.getParameter("isCrossBorder");
    if (!StringUtils.isBlank(isCrossBorder)) {
      model.addAttribute("isCrossBorder", isCrossBorder);
    }
//        model.addAttribute("backUrl", backUrl);
    if (referer != null) {
      model.addAttribute("backUrl", referer);
    } else {
      String backUrl = "/cart/next?skuId=" + skuId;
      if (StringUtils.isBlank(skuId)) {
        backUrl = "/cart/next?shopId=" + shopId;
      }
      model.addAttribute("backUrl", backUrl);
    }

    model.addAttribute("productId", form.getProductId());
    model.addAttribute("qty", form.getQty());
    model.addAttribute("skuId", form.getSkuId());

    model.addAttribute("shopId", form.getShopId());
    model.addAttribute("skuIds", form.getSkuIds());
    model.addAttribute("shopCoupons", form.getShopCoupons());
    if (form.getShopRemarks() != null) {
      model.addAttribute("shopRemarksCache", form.getShopRemarks());
    }

    if ("xiangqu".equalsIgnoreCase(getCurrentUser().getPartner())) {
      return "xiangqu/address";
    } else {
      return "cart/address";
    }
  }

  @RequestMapping("/address/{id}/edit")
  public String edit(@PathVariable("id") String id, @ModelAttribute AddressPayloadForm form,
      HttpServletRequest req, Model model,
      @RequestHeader(value = "Referer", required = false) String referer) {
    Address address = addressService.load(id);
    model.addAttribute("address", address);
    String isCrossBorder = req.getParameter("isCrossBorder");
    if (!StringUtils.isBlank(isCrossBorder)) {
      model.addAttribute("isCrossBorder", isCrossBorder);
    }
    List<Zone> parents = zoneService.listParents(address.getZoneId());
    // parents.add(zoneService.load(address.getZoneId()));

    Zone province = null;
    List<Zone> provinceList = null;
    if (parents.size() > 1) {
      province = parents.get(1);
      provinceList = zoneService.listSiblings(province.getId());
    } else {
      provinceList = zoneService.listChildren("1");
    }
    model.addAttribute("province", province);
    model.addAttribute("provinceList", provinceList);

    Zone city = null;
    List<Zone> cityList = null;
    if (parents.size() > 2) {
      city = parents.get(2);
      cityList = zoneService.listSiblings(city.getId());
    }
    model.addAttribute("city", city);
    model.addAttribute("cityList", cityList);

    Zone district = null;
    List<Zone> districtList = null;
    if (parents.size() > 3) {
      district = parents.get(3);
      districtList = zoneService.listSiblings(district.getId());
    }
    model.addAttribute("district", district);
    model.addAttribute("districtList", districtList);

    String skuId = req.getParameter("skuId");
    String shopId = req.getParameter("shopId");

    model.addAttribute("productId", form.getProductId());
    model.addAttribute("qty", form.getQty());
    model.addAttribute("skuId", form.getSkuId());

    model.addAttribute("shopId", form.getShopId());
    model.addAttribute("skuIds", form.getSkuIds());
    model.addAttribute("shopCoupons", form.getShopCoupons());
    if (form.getShopRemarks() != null) {
      model.addAttribute("shopRemarksCache", form.getShopRemarks());
    }

    if (referer != null) {
      model.addAttribute("backUrl", referer);
    } else {
      String backUrl = "/cart/next?skuId=" + skuId;
      if (StringUtils.isBlank(skuId)) {
        backUrl = "/cart/next?shopId=" + shopId;
      }
      model.addAttribute("backUrl", backUrl);
    }

    if ("xiangqu".equalsIgnoreCase(getCurrentUser().getPartner())) {
      return "xiangqu/address";
    } else {
      return "cart/address";
    }
  }

  @RequestMapping("/address/save")
  @ResponseBody
  public ResponseObject<AddressVO> save(@Valid @ModelAttribute AddressForm form) {
    Address address = new Address();
    BeanUtils.copyProperties(form, address);
    address.setCommon(false);
    return new ResponseObject<AddressVO>(addressService.saveUserAddress(address, true));
  }
}
