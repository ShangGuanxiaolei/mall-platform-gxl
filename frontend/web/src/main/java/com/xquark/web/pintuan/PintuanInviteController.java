package com.xquark.web.pintuan;

import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import java.sql.Timestamp;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author liuyandong
 * @date 2018/9/27 9:33
 */
@Controller
public class PintuanInviteController {

  private final static Logger LOGGER = LoggerFactory.getLogger(PintuanInviteController.class);

  @Autowired
  private PromotionPgMemberInfoService promotionPgMemberInfoService;

  @RequestMapping("/p/promotionPgMember/kaituan")
  public String selectMemberInfoAndTime(@RequestParam("bizNo") String bizNo, Model model) {

    PromotionOrderGoodsInfoVO pov = promotionPgMemberInfoService.loadByOrderNO(bizNo);
    //根据bizNo获取tranCode
    String tranCode = pov.getPieceGroupTranCode();
    ////根据bizNo获取pCode
    String pCode = pov.getpCode();
    //拼团剩余人数
//        int leaveCount= this.selectLeaveCount(tranCode);
    //拼团总人数
    int count = promotionPgMemberInfoService.selectPgMemberCount(tranCode);
    if (count == 0) {
      LOGGER.error("拼团 {} 信息不正确", tranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
    }

//        //拼团成员信息
    //List<PromotionPgMemberInfoVO> memberInfo=promotionPgMemberInfoService.selectMemberInfo(tranCode);
    //查询成团有效时间
    int regimentTime = promotionPgMemberInfoService.selectRegimentTime(pCode);
    if (regimentTime == 0) {
      LOGGER.error("拼团 {} 信息不正确", tranCode);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "拼团信息不正确");
    }
    //查询开团时间
    Date groupOpenTime = promotionPgMemberInfoService.selectGroupOpenTime(tranCode);

    //结束时间
    long date = regimentTime * 1000 * (60 * 60);
    Timestamp timestamp = new Timestamp(date + groupOpenTime.getTime());
    long time = timestamp.getTime();
    //查询当前拼团的商品信息
    PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoService
        .selectGoodsInfo(tranCode);
    if (!promotionOrderGoodsInfoVO.getImg().startsWith("http")) {
      promotionOrderGoodsInfoVO.setImg(
          "http://images.handeson.com/" + promotionOrderGoodsInfoVO.getImg()
              .replace("qn|hs-resources|", "") + "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");
    }
    //当前拼团已有人数
    int beginCount = promotionPgMemberInfoService.selectSuccessPromotionMemberCount(tranCode);
    String tranStatus = promotionPgMemberInfoService.selectStatus(tranCode);
    model.addAttribute("regimentTime", time);
    model.addAttribute("groupOpenTime", groupOpenTime);
    model.addAttribute("MemberCount", count);
    model.addAttribute("OrderGoods", promotionOrderGoodsInfoVO);
    model.addAttribute("beginCount", beginCount);
    model.addAttribute("status", tranStatus);
    return "pintuan/html/afterPay_detail_2";

  }
}
