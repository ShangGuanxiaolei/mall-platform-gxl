package com.xquark.web.coupon;

import com.xquark.dal.mapper.WeixinJSTicketMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.UserService;
import com.xquark.web.BaseController;
import com.xquark.web.vo.Json;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;

/**
 * Created by jason on 15-11-19.
 */
@Controller
public class UserPromotionCouponController extends BaseController {

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private UserService userService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private ProductService productService;

  @Autowired
  private WeixinJSTicketMapper weixinJSTicketMapper;

  @Value("${site.web.host.name}")
  String siteHost;


  private final int ACQUIRE_NEED_PHONE = 2;

  @ResponseBody
  @RequestMapping(value = "coupon/userPromotionCoupon/acquire")
  public Json acquireShopCoupon(@RequestParam(required = false) String phone,
      @RequestParam("promotionCouponId") String promotionCouponId,
      @RequestParam(value = "shopId", required = false) String shopId, HttpServletRequest request,
      HttpServletResponse response) {
    Json json = new Json();
    if (StringUtils.isEmpty(shopId)) {
      ShopTree rootShopTree = shopTreeService.getRootShop();
      shopId = rootShopTree.getDescendantShopId();
    }

    //获取优惠券总数判断是否可以领取
    Long remainder = promotionCouponService.selectRemainder(promotionCouponId);
    if (remainder < 1) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("你来晚了,优惠券已被领完！");
      return json;
    }
    // 用户判断获取用户ID
    User user = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        user = (User) principal;
      }
    }

    if (user != null) {

      // 判断优惠券是否可使用
      PromotionCouponVo promotionCouponVo = promotionCouponService.load(promotionCouponId);
      if (promotionCouponVo.getStatus() != CouponStatus.VALID
          || promotionCouponVo.getValidTo().compareTo(new Date()) < 0) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("领取失败，该张券已经过期");
        return json;
      }

      // 判断是否达到领取上限
      int acquireLimit = promotionCouponVo.getAcquireLimit();
      Long acquireCount = userCouponService
          .countAcquirePromotionCoupons(user.getId(), promotionCouponId);
      if (acquireCount >= acquireLimit) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("领取失败，该张券的领取量已超限额");
        return json;
      }
      //添加记录到用户优惠券中
      UserCoupon userCoupon = new UserCoupon();
      userCoupon.setCouponId(promotionCouponId);
      userCoupon.setPhone(user.getPhone());
      userCoupon.setStatus(CouponStatus.VALID);
      userCoupon.setUserId(user.getId());
      String ua = request.getHeader("User-Agent");
      String ip = getIpAddr(request);
      userCoupon.setDeviceIp(ip);
      if (ua != null && ua.length() >= 512) {
        ua = ua.substring(0, 511);
      }
      userCoupon.setDeviceAgent(ua);
      userCoupon.setShopId(shopId);

      int result = userCouponService.acquireCoupon(userCoupon);
      if (result > 0) {
        json.setRc(Json.RC_SUCCESS);
        json.setMsg("优惠券领取成功！");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("领取失败！");
      }
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("领取失败！");
    }

    return json;
  }


  @RequestMapping(value = "coupon/userPromotionCoupon/{shopId}/{couponId}")
  public String intoShopPromotion(@PathVariable("shopId") String shopId,
      @PathVariable("couponId") String couponId, Model model, HttpServletRequest request) {
    //用户通过扫描进入店铺优惠

    Shop shop = shopService.load(shopId);
    List<Product> products = productService.listProducts(shop.getId());

    model.addAttribute("shop", shop);
    if (products != null && products.size() > 6) { //在页面只显示部分商品列表数据
      products = products.subList(0, 5);
    }
    model.addAttribute("products", products);

    PromotionCouponVo coupon = promotionCouponService.load(couponId);
    if (coupon == null || !CouponStatus.VALID.equals(coupon.getStatus())
        || coupon.getRemainder() <= 0) {
      model.addAttribute("coupon", null);
    } else {
      model.addAttribute("coupon", coupon);
    }
    model.addAttribute("couponId", couponId);
//
//        ShopStyle shopStyle = shopService.loadShopStyle(shopId);
//        List<String> bodyClasses = new ArrayList<String>();
//        bodyClasses.add(shopStyle.getAvatarStyle());
//        bodyClasses.add(shopStyle.getBackgroundColor());
//        bodyClasses.add(shopStyle.getFontColor());
//        bodyClasses.add(shopStyle.getFontFamily());
//        bodyClasses.add(StringUtils.isEmpty(shopStyle.getListView()) ? "smallimg" : shopStyle.getListView());
//        model.addAttribute("styles", StringUtils.join(bodyClasses, " "));
//        model.addAttribute("waterfall", "waterfall".equalsIgnoreCase(shopStyle.getListView()));

    //加载微信认证数据
    loadWeixinShareTicket(request, model, shopId, couponId);

    model.addAttribute("siteHost", siteHost);

    return "coupon/shopCoupon";
  }

  @RequestMapping(value = "coupon/userPromotionCoupon/myCoupon")
  public String myCouponList(@RequestParam(required = false) String phone,
      @RequestParam(required = false) String shopId
      , Model model, HttpServletRequest request, HttpServletResponse response) {
    //获取当前用户

    List<UserCouponVo> couponVos = null;

    if (phone != null) {
      couponVos = userCouponService.selectMyCouponList(null, phone, null);

    } else {

      User user = null;
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (auth != null) {
        Object principal = auth.getPrincipal();
        if (principal instanceof User) {
          user = (User) principal;
        }
      }

      if (user != null) {
        couponVos = userCouponService.selectMyCouponList(user.getId(), null, null);
      }
    }
    model.addAttribute("coupons", couponVos);

    return "coupon/myCoupon";
  }

  /**
   * 加载微信分享认证数据
   */
  private void loadWeixinShareTicket(HttpServletRequest request, Model model, String shopId,
      String couponId) {

    String ua = request.getHeader("User-Agent");
    final String micmsg = "micromessenger";

    if (ua != null && ua.toLowerCase().indexOf(micmsg) > 0) {

      WeixinJSTicket weixinJSTicket = weixinJSTicketMapper.selectByAppId(CommonUtil.ECMOHO_APP_ID);
      String ticket = null;

      if (weixinJSTicket != null) {

        ticket = weixinJSTicket.getJSTicket();

      }
      if (ticket != null) {
        String timestamp = Long.toString(new Date().getTime() / 1000);
        String url = siteHost + "/coupon/userPromotionCoupon/";
        StringBuffer sbUrl = new StringBuffer(url);
        sbUrl.append(shopId);
        sbUrl.append("/").append(couponId);
        url = sbUrl.toString();
        String noncestr = CommonUtil.CreateNoncestr();
        String signValue =
            "jsapi_ticket=" + ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url="
                + url;
        //这个签名.主要是给加载微信js使用.
        String signature = SHA1Util.Sha1((signValue));

        model.addAttribute("appId", "'" + CommonUtil.ECMOHO_APP_ID + "'");
        model.addAttribute("timestamp", timestamp);
        model.addAttribute("noncestr", "'" + noncestr + "'");
        model.addAttribute("signature", "'" + signature + "'");
        model.addAttribute("url", url);
      }
    }
  }

  public static String SendGET(String url, String param) {
    String result = "";//访问返回结果
    BufferedReader read = null;//读取访问结果

    try {
      //创建url
      URL realurl = new URL(url + "?" + param);
      //打开连接
      URLConnection connection = realurl.openConnection();
      // 设置通用的请求属性
      connection.setRequestProperty("accept", "*/*");
      connection.setRequestProperty("connection", "Keep-Alive");
      connection.setRequestProperty("user-agent",
          "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
      //建立连接
      connection.connect();
      // 定义 BufferedReader输入流来读取URL的响应
      read = new BufferedReader(new InputStreamReader(
          connection.getInputStream(), "UTF-8"));
      String line;//循环读取
      while ((line = read.readLine()) != null) {
        result += line;
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (read != null) {//关闭流
        try {
          read.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return result;
  }


  private String getIpAddr(HttpServletRequest request) {
    String ip = request.getHeader("x-forwarded-for");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return ip;
  }
}
