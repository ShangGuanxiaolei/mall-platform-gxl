package com.xquark.web.agent;

import com.xquark.dal.model.Shop;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.interceptor.Token;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.CartService;
import com.xquark.service.shop.ShopService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserAgentsController extends BaseController {

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private CartService cartService;

  @RequestMapping(value = "/userAgent/apply")
  @Token(save = true)
  public String apply(Model model, HttpServletRequest req) {
    String phone = this.getCurrentUser().getPhone();
    if (phone == null) {
      phone = "";
    }
    String type = req.getParameter("type");
    String parentId = req.getParameter("parentId");
    String typeName = "";
    String parentType = "";

    if (AgentType.FOUNDER.toString().equals(type)) {
      typeName = "联合创始人";
    } else if (AgentType.DIRECTOR.toString().equals(type)) {
      typeName = "董事";
    } else if (AgentType.GENERAL.toString().equals(type)) {
      typeName = "总顾问";
    } else if (AgentType.FIRST.toString().equals(type)) {
      typeName = "一星顾问";
    } else if (AgentType.SECOND.toString().equals(type)) {
      typeName = "二星顾问";
    } else if (AgentType.SPECIAL.toString().equals(type)) {
      typeName = "特约";
    }

    if (StringUtil.isNotEmpty(parentId)) {
      UserAgentVO parentVo = userAgentService.selectByUserId(parentId);
      parentType = parentVo.getType().toString();
    }

    model.addAttribute("parentType", parentType);
    model.addAttribute("typeName", typeName);
    model.addAttribute("type", type);
    model.addAttribute("phone", phone);
    model.addAttribute("parentId", parentId);
    return "agent/apply";
  }

  /**
   * 代理认证查询
   */
  @RequestMapping(value = "/userAgent/query")
  public String query(Model model, HttpServletRequest req) {
    return "agent/agentQuery";
  }

  /**
   * 代理认证查看
   */
  @RequestMapping(value = "/userAgent/view")
  public String view(Model model, HttpServletRequest req) {
    String id = req.getParameter("id");
    model.addAttribute("id", id);
    return "agent/agentView";
  }

  @RequestMapping(value = "/userAgent/apply4Admin")
  public String apply4Admin(Model model, HttpServletRequest req) {
    String phone = "";
    String type = req.getParameter("type");
    String parentId = req.getParameter("parentId");
    String typeName = "";
    String parentType = "";

    if (AgentType.FOUNDER.toString().equals(type)) {
      typeName = "联合创始人";
    } else if (AgentType.DIRECTOR.toString().equals(type)) {
      typeName = "董事";
    } else if (AgentType.GENERAL.toString().equals(type)) {
      typeName = "总顾问";
    } else if (AgentType.FIRST.toString().equals(type)) {
      typeName = "一星顾问";
    } else if (AgentType.SECOND.toString().equals(type)) {
      typeName = "二星顾问";
    } else if (AgentType.SPECIAL.toString().equals(type)) {
      typeName = "特约";
    }

    if (StringUtil.isNotEmpty(parentId)) {
      UserAgentVO parentVo = userAgentService.selectByUserId(parentId);
      parentType = parentVo.getType().toString();
    }

    model.addAttribute("parentType", parentType);
    model.addAttribute("typeName", typeName);
    model.addAttribute("type", type);
    model.addAttribute("phone", phone);
    model.addAttribute("parentId", parentId);
    return "agent/apply4Admin";
  }

}
