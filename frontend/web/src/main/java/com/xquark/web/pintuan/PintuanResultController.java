package com.xquark.web.pintuan;

import com.xquark.dal.vo.PromotionOrderGoodsInfoVO;
import com.xquark.service.pintuan.PromotionPgMemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author liuyandong
 * @date 2018/9/26 15:32
 */
@Controller
public class PintuanResultController {

    @Autowired
    private PromotionPgMemberInfoService promotionPgMemberInfoService;
    @RequestMapping("/p/promotionPgMember/selectMemberInfoAndTime/{cpId}/{bizNo}")
    public String selectMemberInfoAndTime(@PathVariable("bizNo") String bizNo, Model model) {

        PromotionOrderGoodsInfoVO pov = promotionPgMemberInfoService.loadByBizNo(bizNo);
        //根据bizNo获取tranCode
        String tranCode = pov.getPieceGroupTranCode();
        ////根据bizNo获取pCode
        String pCode = pov.getpCode();
        //拼团剩余人数
//        int leaveCount= this.selectLeaveCount(tranCode);
        //拼团总人数
        int count = promotionPgMemberInfoService.selectPgMemberCount(tranCode);

//        //拼团成员信息
        //List<PromotionPgMemberInfoVO> memberInfo=promotionPgMemberInfoService.selectMemberInfo(tranCode);
        //查询成团有效时间
        long regimentTime = promotionPgMemberInfoService.selectRegimentTime(pCode);
        //查询开团时间
        Date groupOpenTime = promotionPgMemberInfoService.selectGroupOpenTime(tranCode);
        Timestamp ts = new Timestamp(groupOpenTime.getTime());
        long tsTime = ts.getTime();

        long localtime = System.currentTimeMillis();
        Long l=regimentTime*1000;
        //long l = regimentTime + tsTime - localtime;
        // Integer.parseInt()
        //查询当前拼团的商品信息
        PromotionOrderGoodsInfoVO promotionOrderGoodsInfoVO = promotionPgMemberInfoService.selectGoodsInfo(tranCode);
        if(!promotionOrderGoodsInfoVO.getImg().startsWith("http")){
            promotionOrderGoodsInfoVO.setImg("http://images.handeson.com/" + promotionOrderGoodsInfoVO.getImg().replace("qn|hs-resources|", "") + "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");}
        //当前拼团已有人数
        int beginCount = promotionPgMemberInfoService.selectSuccessPromotionMemberCount(tranCode);
        String tranStatus=promotionPgMemberInfoService.selectStatus(tranCode);
        model.addAttribute("regimentTime", l);
        model.addAttribute("localTime", localtime);
        model.addAttribute("startTime", tsTime);
        model.addAttribute("groupOpenTime", groupOpenTime);
        model.addAttribute("MemberCount", count);
        model.addAttribute("OrderGoods", promotionOrderGoodsInfoVO);
        model.addAttribute("beginCount", beginCount);
        model.addAttribute("status",tranStatus);
        return "pintuan/html/afterPay_detail_1";

    }
}
