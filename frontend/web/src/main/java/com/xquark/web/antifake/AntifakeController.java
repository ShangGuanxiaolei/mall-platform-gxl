package com.xquark.web.antifake;

import com.xquark.dal.status.AgentType;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.cart.CartService;
import com.xquark.thirds.umpay.api.util.StringUtil;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AntifakeController extends BaseController {

  /**
   * 防伪码查询
   */
  @RequestMapping(value = "/antifake/query")
  public String query(Model model, HttpServletRequest req) {
    return "antifake/query";
  }

  /**
   * 追溯拓扑图展示
   */
  @RequestMapping(value = "/antifake/orgView")
  public String orgView(Model model, HttpServletRequest req) {
    return "antifake/orgView";
  }


}
