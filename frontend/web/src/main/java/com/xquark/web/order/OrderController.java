package com.xquark.web.order;

import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.model.BasePointCommTotal;
import com.hds.xquark.dal.model.CommissionTotal;
import com.hds.xquark.dal.model.PointTotal;
import com.hds.xquark.service.point.CommissionServiceApi;
import com.hds.xquark.service.point.PointServiceApi;
import com.vdlm.common.lang.CollectionUtil;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.biz.filter.UserServletFilter;
import com.xquark.dal.IUser;
import com.xquark.dal.consts.PayUrlConfig;
import com.xquark.dal.mapper.FirstOrderMapper;
import com.xquark.dal.mapper.PayNoLogMapper;
import com.xquark.dal.mapper.PromotionSkusMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.MainOrderStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.YundouOperationType;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionReduceDetailVO;
import com.xquark.dal.vo.PromotionTranInfoVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.freshman.FirstOrderService;
import com.xquark.service.freshman.FreshManProductService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.fullReduce.PromotionFullReduceService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderUserDeviceService;
import com.xquark.service.outpay.OutPayAgreementService;
import com.xquark.service.outpay.impl.PaymentFacade;
import com.xquark.service.ownerAdress.CustomerOwnerAdress;
import com.xquark.service.pintuan.PromotionTranInfoService;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionOrderDetailService;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.service.yundou.Result;
import com.xquark.service.yundou.YundouAmountService;
import com.xquark.utils.StringUtil;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.order.form.OrderPayAgainForm;
import com.xquark.web.order.form.OrderSumbitForm;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

/**
 * 订单相关逻辑
 *
 * @author odin
 * @author ahlon
 */
@Controller
public class OrderController extends BaseController {
  private static final String WHITELIST_CONFIRE_ALERT_CONTENT="该活动尚未开始，请耐心等待";
  @Autowired
  private OrderService orderService;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private OutPayAgreementService outPayAgreementService;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderUserDeviceService orderUserDeviceService;

  @Autowired
  private YundouAmountService yundouAmountService;

  @Value("${site.web.host.name}")
  private String hostName;

  @Value("${xiangqu.web.site}")
  private String xiangquWebSite;

  @Autowired
  private PromotionFullReduceService reduceService;

  @Autowired
  private PayUrlConfig payUrlConfig;

  @Value("${site.webapi.host.name}")
  private String siteHost;

  @Autowired
  private PromotionTranInfoService promotionTranInfoService;

  @Autowired
  private CustomerOwnerAdress customerOwnerAdress;

  @Autowired
  private FlashSalePromotionProductService flashSaleService;

  @Autowired
  private PayNoLogMapper payNoLogMapper;

  private PointServiceApi pointService;

  private CommissionServiceApi commissionService;

  @Autowired
  private FirstOrderService firstOrderService;

  @Autowired
  private FirstOrderMapper firstOrderMapper;

  private final static Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

  @Autowired
  public void setPointService(PointContextInitialize initialize) {
    this.pointService = initialize.getPointServiceApi();
  }

  @Autowired
  public void setCommissionService(PointContextInitialize initialize) {
    this.commissionService = initialize.getCommissionServiceApi();
  }

  @Autowired
  private FreshManProductService freshManProductService;

  @Autowired
  private FreshManService freshManService;

  @Autowired
  private PaymentFacade paymentFacade;

  /**
   * 订单列表页面
   */
  @RequestMapping(value = "/orders")
  public String index(@RequestParam(required = false) OrderStatus orderStatus, Model model,
      Pageable pager) {
    List<OrderVO> orders = orderService.listByStatus4Buyer(orderStatus, pager);
    model.addAttribute("orders", orders);
    return "order/index";
  }

  /**
   * 订单详情页面
   */
  @RequestMapping(value = {"/order/{id}", "/order/{id}/pay"})
  public String view(@PathVariable("id") String orderId, HttpServletRequest request,
      HttpServletResponse response, Model model) {

    // 订单类型，区分b2b进货订单等
    String orderType = request.getParameter("orderType");

//    String redirect = redirectDomain(request, response);
//    if (StringUtils.isNotEmpty(redirect)) {
//      return redirect;
//    }

    MainOrderVO mainOrderVO = mainOrderService.loadVO(orderId);
    if (mainOrderVO == null) {
      return "order/404";
    }

    model.addAttribute("mainOrder", mainOrderVO);
    BigDecimal goodsFee = new BigDecimal(0);
    BigDecimal logisticsFee = new BigDecimal(0);
    List<PromotionReduceDetailVO> reduceDetails = new ArrayList<PromotionReduceDetailVO>();
    boolean hasCoupon = true;
    for (OrderVO orderVO : mainOrderVO.getOrders()) {
      goodsFee = goodsFee.add(orderVO.getGoodsFee());
      logisticsFee = logisticsFee.add(orderVO.getLogisticsFee());
      String couponId = orderVO.getCouponId();
      String subOrderId = orderVO.getId();
      // 优惠券与满减活动只能二选一
      if (StringUtils.isBlank(couponId)) {
        // 获取订单对应的活动优惠
        hasCoupon = false;
        reduceDetails.addAll(reduceService.listDetailByOrderId(subOrderId));
      }
    }
    if (CollectionUtils.isNotEmpty(reduceDetails)) {
      model.addAttribute("reduceDetails", reduceDetails);
    }
    model.addAttribute("hasCoupon", hasCoupon);

    // 订单买家信息，当自提方式无法带出订单用户信息时，取订单买家信息
    if (mainOrderVO.getOrders() != null && mainOrderVO.getOrders().size() > 0) {
      String buyerId = mainOrderVO.getOrders().get(0).getBuyerId();
      User buyer = userService.load(buyerId);
      model.addAttribute("buyer", buyer);
    }
    model.addAttribute("goodsFee", goodsFee);
    model.addAttribute("logisticsFee", logisticsFee);
    model.addAttribute("orderType", orderType);

    User user = getCurrentUser();
    String orderPhone = "";
    if (mainOrderVO.getOrders().get(0).getOrderAddress() != null) {
      orderPhone = mainOrderVO.getOrders().get(0).getOrderAddress().getPhone();
    }

    if (StringUtils.equals(orderPhone, user.getLoginname())) {
      model.addAttribute("authed", true);
    }

    model.addAttribute("phoneParamKey", UserServletFilter.FORM_VD_PHONE_LGN);
    //放入协议支付的银行
    model.addAttribute("userAgreeBanks",
        outPayAgreementService.listBankByUserId(getCurrentUser().getId()));
    return "order/view";
  }

  /**
   * sub订单详情页面
   */
  @RequestMapping(value = "/suborder/{id}")
  public String viewSubOrder(@PathVariable("id") String orderId, HttpServletRequest request,
      HttpServletResponse response, Model model) {

//    String redirect = redirectDomain(request, response);
//    if (StringUtils.isNotEmpty(redirect)) {
//      return redirect;
//    }

    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      return "order/404";
    }
    Shop shop = shopService.load(order.getShopId());
    order.setShopName(shop.getName());
    model.addAttribute("order", order);

    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      e.printStackTrace();
    }

    /**
     * 说明：该问题由于点击链接后app没有用户登录，故无法获取用户id，因订单中买家id与此不能匹配，所以出现此问题.
     * TODO
     */
    //model.addAttribute("authed", user != null && order.getBuyerId().equals(user.getId()));
    model.addAttribute("authed", true);
    User sellerUser = userService.load(order.getSellerId());
    if (sellerUser != null) {
      if (user != null && user.getId().equals(sellerUser.getId())) {
        model.addAttribute("imUser", false);
      } else {
        model.addAttribute("imUser", sellerUser.getFunctionSts(KdFunctionSet.FUNC_IM.ordinal()));
      }
    } else {
      model.addAttribute("imUser", false);
    }

    if (order.getStatus() == OrderStatus.SUBMITTED) {
      //放入协议支付的银行
      model.addAttribute("userAgreeBanks",
          outPayAgreementService.listBankByUserId(getCurrentUser().getId()));
    }

    /**
     * 判断是否想去的订单
     * @author odin
     */
    User buyer = userService.load(order.getBuyerId());

    // buyer 也有返回为空的情况，这个对应的archive是1，返回的
    if ((buyer != null && buyer.getPartner() != null && "xiangqu"
        .equalsIgnoreCase(buyer.getPartner()))
        || UserPartnerType.XIANGQU.equals(order.getPartnerType())) {
      model.addAttribute("xiangquWebSite", xiangquWebSite);
      return "xiangqu/orderView";
    } else {
      return "suborder/view";
    }
  }


  @RequestMapping(value = "/order/{id}/verify")
  public String verify(@PathVariable("id") String orderId, String postfix, Model model,
      HttpServletRequest req, HttpServletResponse resp) {
    MainOrderVO mainOrder = mainOrderService.loadVO(orderId);
    String orderPhone = "";
    if (mainOrder.getOrders().get(0).getOrderAddress() != null) {
      orderPhone = mainOrder.getOrders().get(0).getOrderAddress().getPhone();
    } else {
      String buyerId = mainOrder.getOrders().get(0).getBuyerId();
      User buyer = userService.load(buyerId);
      orderPhone = buyer.getPhone();
    }

    if (StringUtils.isEmpty(postfix)) {
      model.addAttribute("order", mainOrder);
      model.addAttribute("phone", orderPhone);
      return "order/orderVerify";
    } else {
      boolean verified = StringUtils.equals(StringUtils.substring(orderPhone, 7), postfix);
      if (verified) {
        return "redirect:/order/" + mainOrder.getId() + "?" + UserServletFilter.FORM_VD_PHONE_LGN
            + "=" + orderPhone;
      } else {
        model.addAttribute("order", mainOrder);
        model.addAttribute("phone", orderPhone);
        model.addAttribute("error", "手机后四位号码不正确");
        return "order/orderVerify";
      }
    }
  }

  /**
   * 订单提交
   */
  @RequestMapping(value = "/order/submit")
//  @Token(remove = true)
  public String submit(@ModelAttribute OrderSumbitForm form,
      @CookieValue(value = "tu_id", defaultValue = "") String tuId,
      Errors errors, Device device, Model model, HttpServletRequest request,
      @RequestHeader(value = "referer", required = false) String referer) {
    //数据校验
    ControllerHelper.checkException(errors);

    // 订单类型，区分b2b进货订单等
    String orderType = request.getParameter("orderType");

    MainOrder mianOrder; //主订单
    Address address = null;
    // 自提方式不需要传入收货地址
    if (!"1".equals(form.getIspickup())) {
      address = addressService.load(form.getAddressId());
    }

    String realShopId = shopService.loadRootShop().getId();
    if (StringUtils.isNotBlank(form.getOrderId())) { // 多次提交
      mianOrder = mainOrderService.load(form.getOrderId());
    } else {
      UserSelectedProVO userSelectedProVO = null;
      Map<String, String> shopCoupons = form.getShopCoupons();
      if (MapUtils.isNotEmpty(shopCoupons)) {
        userSelectedProVO = new UserSelectedProVO();
        Map<Shop, String> shopConpons = new HashMap<Shop, String>();
        for (Map.Entry<String, String> entry : shopCoupons.entrySet()) {
          String shopId = entry.getKey();
          String couponId = entry.getValue();
          shopConpons.put(shopService.load(shopId), couponId);
        }
        userSelectedProVO.setSelShopCouponMap(shopConpons);
      }
      OrderAddress oa = getOrderAddress(form);
      if (form.isDirectBuy()) {
        //代销商品立即购买时保存productLiteralId FIXME dongsongjie 将参数放入form中传递
        String unionId = "";
        if (StringUtils.isNotEmpty(form.getProLiteralId())) {
          Product product = productService.findProductById(form.getProLiteralId());
          if (product != null) {
            Shop shop = shopService.load(product.getShopId());
            if (shop != null) {
              unionId = shop.getOwnerId();//分佣给店铺的owner
            }
          }
        }

        // 直接购买取留言map中的第一个即可
        String remark = "";
        if (MapUtils.isNotEmpty(form.getShopRemarks())) {
          for (java.util.Map.Entry<String, String> entry : form.getShopRemarks().entrySet()) {
            remark = entry.getValue();
          }
        }
        mianOrder = mainOrderService
//            .submitBySkuId(form.getSkuId(), oa, form.getRemark(), userSelectedProVO,
//                address,
//                true, form.getQty(), realShopId, null, form.isUseDeduction(),
//                promotionType, form.getIsPrivilege(), form.isNeedInvoice(),
//                form.getSelectPoint(), form.getSelectCommission());
            // TODO H5下单暂时不支持活动提交
            .submitBySkuId(form.getSkuId(), oa, remark, userSelectedProVO, address, form.isDanbao(),
                form.getQty(), realShopId, orderType,
                form.isUseDeduction(),
                null, false, null, null, null, null);
      } else if (form.isCartBuy()) {   // 购物车下单
        mianOrder = mainOrderService
            .submitBySkuIds(form.getSkuIds(), oa, form.getShopRemarks(), userSelectedProVO,
                null, address, form.isDanbao(), realShopId, orderType,
                form.isUseDeduction(), false, null, null, null);
      } else {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单提交参数不正确");
      }
    }

    OrderUserDevice orderUserDevice = buildOrderUserDevice(request);
    orderUserDevice.setOrderId(mianOrder.getId());
    orderUserDevice.setUserId(mianOrder.getBuyerId());
    orderUserDeviceService.save(orderUserDevice);

    return "redirect:" + payUrlConfig.hostName() + "/order/" + mianOrder.getId() + "/pay?orderType="
        + orderType;
  }


  /**
   * 获得当前订单的收货地址和联系方式
   */
  private OrderAddress getOrderAddress(OrderSumbitForm form) {
    OrderAddress oa = new OrderAddress();
    if ("1".equals(form.getIspickup())) {
      oa = null;
    } else {
      if (StringUtils.isNotEmpty(form.getAddressId())) {
        Address address = addressService.load(form.getAddressId());
        BeanUtils.copyProperties(address, oa);
      } else {
        form.setStreet(form.getStreet().trim());
        BeanUtils.copyProperties(form, oa);
      }
    }
    return oa;
  }

  private OrderUserDevice buildOrderUserDevice(HttpServletRequest request) {
    OrderUserDevice device = new OrderUserDevice();
    device.setUserAgent(request.getHeader("User-Agent"));
    device.setClientIp(request.getRemoteAddr());
    device.setServerIp(request.getServerName());
    return device;
  }

  /**
   * 买家取消订单
   */
  @RequestMapping(value = "/order/{id}/cancel")
  public String cancel(@PathVariable("id") String orderId, Model model) {
    orderService.cancel(orderId);
    return "redirect:" + payUrlConfig.hostName() + "/order/" + orderId;
  }

  /**
   * 订单支付请求
   */
  // @RequestMapping(value = "/order/{id}/pay")
  @Deprecated
  public String pay(@PathVariable("id") String orderId, Model model,
      RedirectAttributes redirectAttrs,
      @RequestHeader(value = "referer", required = false) String referer, Device device) {

    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      return "order/404";
    }
    model.addAttribute("order", order);

    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      e.printStackTrace();
    }

    model.addAttribute("authed", user != null && order.getBuyerId().equals(user.getId()));

    if (order.getStatus() == OrderStatus.SUBMITTED) {
      //放入协议支付的银行
      model.addAttribute("userAgreeBanks",
          outPayAgreementService.listBankByUserId(getCurrentUser().getId()));
    }

    model.addAttribute("shop", shopService.load(order.getShopId()));

    return "order/view";
  }

  @Autowired
  private PromotionSkusService promotionSkusService;
  @Autowired
  private SkuMapper skuMapper;
  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;
  @Autowired
  private PromotionOrderDetailService promotionOrderDetailService;
  @Autowired
  private PromotionConfigService promotionConfigService;



  /**
   * 订单支付前大会活动校验
   */
  @ResponseBody
  @RequestMapping(value = "/order/beforePromotionPay",method = RequestMethod.POST)
  public ResponseObject<String> beforePromotionPay(@RequestParam("orderId") String orderId) {
      User user = (User) getCurrentIUser();
      MainOrderVO mainOrderVO = mainOrderService.loadVO(orderId);
      if (mainOrderVO == null) {
        throw new BizException(GlobalErrorCode.NOT_FOUND, "订单[" + orderId + "]不存在");
      }

      if (mainOrderVO.getStatus() != MainOrderStatus.SUBMITTED) {
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
                "订单[" + mainOrderVO.getOrderNo() + "]状态错误，无法提交");
      }


    Integer feshManCount = 0;
    for (int i = 0; i <mainOrderVO.getOrders().size() ; i++) {
      String freshmanlimit = mainOrderVO.getOrders().get(i).getOrderType().name();
      if(freshmanlimit.equals("FRESHMAN")) {
        List<OrderItem> orderItems = mainOrderVO.getOrders().get(0).getOrderItems();
        for (OrderItem item : orderItems) {
          //新人专区商品限购数量校验
          List<FreshManProductVo> freshManProductVos = freshManProductService.selectByProductId(item.getProductId());
          if (CollectionUtil.isNotEmpty(freshManProductVos)) {
            feshManCount = feshManCount + item.getAmount();
          }
        }

        //新人商品限购
        freshManService.freshmanLimit(feshManCount, user);
      }
    }

      //拼团活动时间校验
      PromotionOrderDetail promotionOrderDetail = promotionOrderDetailService.loadDetailByMainOrderNo(mainOrderVO.getOrderNo(),
              "piece");
    if(promotionOrderDetail != null){
        if(promotionOrderDetail.getMember_id() == getCurrentUser().getCpId()){//开团者是当前用户则是开团

          PromotionBaseInfo promotionBaseInfo = promotionBaseInfoService.selectByPCode(promotionOrderDetail.getP_code());
          if(promotionBaseInfo != null && promotionBaseInfo.getEffectTo() != null){
            if(System.currentTimeMillis() > promotionBaseInfo.getEffectTo().getTime()){//活动时间校验
              ResponseObject result = new ResponseObject<String>("活动已结束",GlobalErrorCode.PIECE_ERROR_OVERTIME);
              result.setData("error");
              return result;
            }
          }

        }else{//开团者不是当前用户则为参团

          PromotionTranInfoVO promotionTranInfoVO = promotionTranInfoService.selectPromotionPgTranCode(promotionOrderDetail.getPiece_group_tran_code());
          if(promotionTranInfoVO != null && promotionTranInfoVO.getGroupOpenTime() != null){
            long endTime = promotionTranInfoVO.getGroupOpenTime().getTime() + promotionTranInfoVO.getPieceEffectTime();
            if(System.currentTimeMillis() > endTime){//成团时间校验
              ResponseObject result = new ResponseObject<String>("团已失效",GlobalErrorCode.PIECE_ERROR_OVERTIME);
              result.setData("error");
              return result;
            }
          }

        }
      }

      // 大会活动有效性判断
       String prmotionMessage = this.beforeValidatePromotion(mainOrderVO);
       if(StringUtils.isNotBlank(prmotionMessage)){
         ResponseObject result = new ResponseObject<String>(prmotionMessage,GlobalErrorCode.SUCESS);
         return result;
       }
       ResponseObject success = new ResponseObject();
       success.setData("success");
       return success;
  }

  private String  beforeValidatePromotion (MainOrderVO mainOrderVO) {
    //杭州大会判断，2018-10-07 by Chenpeng
    //杭州大会活动有效期判断
    //User user = (User) getCurrentIUser();
    User user = super.getCurrentUser();

    List<String> skuIds = new ArrayList<String>();
    List<OrderVO> orderList = mainOrderVO.getOrders();
    //促销折扣 待付款限购校验
      for (OrderVO orderVO : orderList) {
          List<OrderItem> orderItems = orderVO.getOrderItems();
          for (OrderItem item : orderItems) {
              List<Promotion4ProductSalesVO> promotion4ProductSalesVOS = promotionBaseInfoService.selectExsitSalesPromotionByProductId(item.getProductId());
              //促销折扣商品限购数量校验,购物车该商品的数量
              if(CollectionUtils.isNotEmpty(promotion4ProductSalesVOS)){

                  if (promotion4ProductSalesVOS.get(0).getEffectTo().before(new Date())) {
                      return "商品活动已结束";
                  }
                  if (promotion4ProductSalesVOS.get(0).getEffectFrom().after(new Date())) {
                      return "商品活动未开始";

                  }
                  Integer buyCountlimit   = promotionSkusMapper.getBuyCountLimitBySkuId(item.getSkuId());
                  if(null!=buyCountlimit){
                      Integer buyCount  = promotionSkusMapper.getBuyCountBuySkuId(user.getId(), item.getSkuId());
                      if(item.getAmount() >(buyCountlimit-(buyCount-item.getAmount()))){
                          return "促销限购" +buyCountlimit + "件哦";
                      }
                  }
              }
          }

      }


    //过滤掉非活动商品的订单行， 只留下活动商品的并且不是赠品的订单行
    List<OrderItem> promotionOrderItem = new ArrayList<OrderItem>();

    for (OrderVO orderVO : orderList) {
      List<OrderItem> orderItems = orderVO.getOrderItems();
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        if (null != orderItem.getPromotionType() &&
                orderItem.getPromotionType().equals(PromotionType.MEETING)) {
          skuIds.add(skuId);
          promotionOrderItem.add(orderItem);
        }
      }
    }

    List<String> promotionSkuIds = promotionSkusService.getPromotionSkuIds(skuIds);
    List<String> promotionSkuCodes = new ArrayList<String>();
    if (org.apache.commons.collections.CollectionUtils.isNotEmpty(skuIds)) {
      for (String skuId : skuIds) {
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        List ret = promotionSkusService.getPromotionSkusAllStatus(sku.getSkuCode());
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(ret)) {
          promotionSkuCodes.add(sku.getSkuCode());
        }
      }
    }
    if (org.apache.commons.collections.CollectionUtils.isNotEmpty(promotionSkuCodes)) {
      for (String promotionSkuCode : promotionSkuCodes) {
        //根据商品sku获取活动信息
        List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectEffectiveBySkuCode(promotionSkuCode);
        if(CollectionUtils.isEmpty(promotionBaseInfos)){
            return "活动已失效";
          }
        //  TODO
        if(!promotionBaseInfos.get(0).getpType().contains("meeting")){
          continue;
        }
        PromotionBaseInfo proBaseInfo = promotionBaseInfos.get(0);
        String pCode = proBaseInfo.getpCode();
        //查询活动配置信息
        Map configType = new HashMap();
        configType.put("configType", promotionBaseInfos.get(0).getpType());
        List<PromotionConfig> promotionConfigs = promotionConfigService.selectListByConfigType(configType);
        //活动金额上限默认不设限
        String top_value="10000000";

        if (null != promotionConfigs && promotionConfigs.size() > 0) {
          for (PromotionConfig promotionConfig : promotionConfigs) {
            if ("top_value".equals(promotionConfig.getConfigName())
                    && null != promotionConfig.getConfigValue()
                    && !"".equals(promotionConfig.getConfigValue())) {
              top_value = promotionConfig.getConfigValue();
            }

          }
        }

        Date effectFrom = proBaseInfo.getEffectFrom();
        if (effectFrom.after(new Date())) {
              return "活动未开始";
          }
        if (proBaseInfo.getEffectTo().before(new Date())) {
          return "活动已结束";
        }

        //杭州大会活动有效期判断
        //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
//        boolean hasEnoughStock = promotionSkusService.hasEnoughStock(promotionSkuIds);
//        if (!hasEnoughStock) {
//           return "活动商品已售罄";
//        }
        //杭州大会活动商品库存判断，2018-10-07 by Chenpeng
        //杭州大会总价值上线判断，2018-10-07 by Chenpeng
        boolean isOverUpValue ;
        if(CollectionUtils.isNotEmpty(promotionOrderItem)){
          isOverUpValue = promotionOrderDetailService.isOverUpValueByOrderItem(promotionOrderItem, user, Integer.valueOf(top_value), pCode);
          if (isOverUpValue) {
            return "购买总额已超过活动上限";
          }
        }
        //杭州大会总价值上线判断，2018-10-07 by Chenpeng
        //大会活动增加商品购买数量限制
        Map<String,String> map = new HashMap<String, String>();
        if(CollectionUtils.isNotEmpty(promotionOrderItem) && promotionOrderItem.size() > 1) {
          map = isOverRestriction(promotionSkuCodes,user,pCode);
        } else if(CollectionUtils.isNotEmpty(promotionOrderItem) && promotionOrderItem.size() == 1) {
          OrderItem orderItem = promotionOrderItem.get(0);
          map = isOverRestriction(orderItem.getSkuId(), user, orderItem.getAmount(), pCode);
        }

//        if("true".equals(map.get("stock"))){
//          return "商品库存不足";
//        }

        if("true".equals(map.get("restriction"))){
          return "商品购买数量超过活动规定数量";
        }
        //杭州大会判断，2018-10-07 by Chenpeng
      }
    }else{
      return "";
    }
      return "";
  }

  //判断是否超过限购数量(购物车)
  private Map<String,String> isOverRestriction(List<String> promotionSkuCodes, User user, String pCode) {
    Map<String,String> flag = promotionOrderDetailService.isOverRestriction(promotionSkuCodes,user,pCode);
    return flag;
  }

  //判断是否超过限购数量(单个商品)
  private Map<String,String> isOverRestriction(String promotionSkuId, User user, int qty, String pCode) {
    Map<String,String> flag = promotionOrderDetailService.isOverRestriction(promotionSkuId, user, qty, pCode);
    return flag;
  }

  /**
   * 校验德分积分
   */
  private String validatePoint(MainOrderVO mainOrderVO,
      PointTotal pointTotal, CommissionTotal commissionTotal) {
    if (mainOrderVO == null) {
      return null;
    }
    List<OrderVO> orders = mainOrderVO.getOrders();
    for (OrderVO orderVO : orders) {
      BigDecimal paidPoint = orderVO.getPaidPoint();
      BigDecimal realPoint = Optional.ofNullable(pointTotal)
          .map(BasePointCommTotal::getTotalUsable)
          .orElse(BigDecimal.ZERO);
      if (paidPoint != null && paidPoint.compareTo(realPoint) > 0) {
        return "德分不足，无法支付";
      }

      BigDecimal paidCommission = orderVO.getPaidCommission();
      BigDecimal realCommission = Optional.ofNullable(commissionTotal)
          .map(BasePointCommTotal::getTotalUsable)
          .orElse(BigDecimal.ZERO);
      if (paidCommission != null && paidCommission.compareTo(realCommission) > 0) {
        return "积分不足, 无法支付";
      }
    }
    return null;
  }

  @Autowired
  private PromotionSkusMapper promotionSkusMapper;

  private void validatePromotion(MainOrderVO mainOrderVO) {
    User user = super.getCurrentUser();

    //杭州大会判断，2018-10-07 by Chenpeng
    //杭州大会活动有效期判断
    List<String> skuIds = new ArrayList<String>();
    List<OrderVO> orderList = mainOrderVO.getOrders();

    //过滤掉非活动商品的订单行， 只留下活动商品的并且不是赠品的订单行
    List<OrderItem> promotionOrderItem = new ArrayList<OrderItem>();

    for (OrderVO orderVO : orderList) {
      List<OrderItem> orderItems = orderVO.getOrderItems();
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        if (null != orderItem.getPromotionType() &&
                !orderItem.getPromotionType().equals(PromotionType.MEETING_GIFT)) {

          skuIds.add(skuId);

          promotionOrderItem.add(orderItem);
        }
      }
    }
    List<String> promotionSkuIds = promotionSkusService.getPromotionSkuIds(skuIds);

    List<String> promotionSkuCodes = new ArrayList<String>();
    if (org.apache.commons.collections.CollectionUtils.isNotEmpty(skuIds)) {
      for (String skuId : skuIds) {
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        List ret = promotionSkusService.getPromotionSkus(sku.getSkuCode());
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(ret)) {
          promotionSkuCodes.add(sku.getSkuCode());
        }
      }
    }

    if (org.apache.commons.collections.CollectionUtils.isNotEmpty(promotionSkuCodes)) {
      for (String promotionSkuCode : promotionSkuCodes) {
        //根据商品sku获取活动信息
        List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectEffectiveBySkuCode(promotionSkuCode);
        if(CollectionUtils.isEmpty(promotionBaseInfos)){
            throw new BizException(GlobalErrorCode.UNKNOWN,
                    "活动已失效");
        }
        //只校验大会活动的商品
        if(!promotionBaseInfos.get(0).getpType().contains("meeting")){
          continue;
        }
        PromotionBaseInfo proBaseInfo = promotionBaseInfos.get(0);
        String pCode = proBaseInfo.getpCode();
        //查询活动配置信息
        Map configType = new HashMap();
        configType.put("configType", promotionBaseInfos.get(0).getpType());
        List<PromotionConfig> promotionConfigs = promotionConfigService.selectListByConfigType(configType);
        //活动金额上限默认不设限
        String top_value="10000000";

        if (null != promotionConfigs && promotionConfigs.size() > 0) {
          for (PromotionConfig promotionConfig : promotionConfigs) {
            if ("top_value".equals(promotionConfig.getConfigName())
                    && null != promotionConfig.getConfigValue()
                    && !"".equals(promotionConfig.getConfigValue())) {
              top_value = promotionConfig.getConfigValue();
            }

          }
        }

        Date effectFrom = proBaseInfo.getEffectFrom();
        if (effectFrom.after(new Date())) {
          throw new BizException(GlobalErrorCode.UNKNOWN,
                  "活动未开始");
        }
        if (proBaseInfo.getEffectTo().before(new Date())) {
          throw new BizException(GlobalErrorCode.UNKNOWN,
                  "活动已结束");
        }

        boolean isOverUpValue ;
        if(CollectionUtils.isNotEmpty(promotionOrderItem)){
          isOverUpValue = promotionOrderDetailService.isOverUpValueByOrderItem(promotionOrderItem, user, Integer.valueOf(top_value), pCode);
          if (isOverUpValue) {
            throw new BizException(GlobalErrorCode.UNKNOWN,
                    "购买总额已超过活动上限");
          }
        }
        //杭州大会总价值上线判断，2018-10-07 by Chenpeng
        Map<String,String> map = new HashMap<String, String>();
        boolean isOverRestriction = false;
        if(CollectionUtils.isNotEmpty(promotionOrderItem) && promotionOrderItem.size() > 1) {
          map = isOverRestriction(promotionSkuCodes,user,pCode);
        } else if(CollectionUtils.isNotEmpty(promotionOrderItem) && promotionOrderItem.size() == 1) {
          OrderItem orderItem = promotionOrderItem.get(0);
          map = isOverRestriction(orderItem.getSkuId(), user, orderItem.getAmount(), pCode);
        }
        if("true".equals(map.get("restriction"))){
          throw new BizException(GlobalErrorCode.UNKNOWN,
                  "商品购买数量超过活动规定数量");
        }
        //杭州大会判断，2018-10-07 by Chenpeng
      }
    }

  }

  /**
   * 订单支付请求
   */
  @RequestMapping(value = "/order/pay")
  public String pay(@ModelAttribute OrderPayAgainForm form, Model model,
      RedirectAttributes redirectAttrs, Device device) {
    MainOrderVO mainOrderVO = mainOrderService.loadVO(form.getOrderId());

    if (mainOrderVO == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单[" + form.getOrderId() + "]不存在");
    }

    //校验白名单
    Long cpId= getCurrentUser().getCpId();
//    this.checkIsWhiteOnPay(mainOrderVO, cpId);

    //临时方案防止用户重复支付，支付流程完善之后可拿掉
//    boolean b = this.checkMultiPay(mainOrderVO);
//    if(b){
//      throw new BizException(GlobalErrorCode.DUPLICATE_PAYMENT, "订单[" + form.getOrderId() + "]已支付");
//    }

    if (mainOrderVO.getStatus() != MainOrderStatus.SUBMITTED) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "订单[" + mainOrderVO.getOrderNo() + "]状态错误，无法提交");
    }

    if (checkSaleZoneJoinPolicyForPay(mainOrderVO)) {
      if (checkSaleZone()) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "同一用户只能参加一次专区活动哦");
      }
    }

    //拼团活动有效性判断
    checkPieceTime(mainOrderVO, cpId);

    // 大会活动有效性判断
    this.validatePromotion(mainOrderVO);

    PointTotal pointTotal = pointService.loadTotal(cpId).orNull();
    CommissionTotal commissionTotal = commissionService.loadTotal(cpId).orNull();
    String msg = this.validatePoint(mainOrderVO, pointTotal, commissionTotal);
    if (msg != null) {
      LOGGER.warn("用户 {} 付款是积分或德分不足, msg: {}", cpId, msg);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }

    // 是否全积分兑换等因素导致支付0元
    boolean isFreePay = mainOrderVO.getTotalFee().signum() == 0;

    model.addAttribute("order", mainOrderVO);
    String payType = form.getPayType().toString();
    String mainOrderNo = StringUtils.defaultString(mainOrderVO.getOrderNo());
    String pNo = "";
    boolean isUseEBS2Pay = paymentFacade.epsStatus();
    PaymentChannel paymentChannel = PaymentChannel.PLATFORM;
    if(isUseEBS2Pay)
    	paymentChannel = PaymentChannel.EBS;
    CashierItem item = cashierService.loadByPayTypeAndOrderNo(paymentChannel,payType,mainOrderNo);
    boolean isUseNewPayNo = false;
    if(item != null) { 
    	//如果cashiter存在数据则用之前的payno
    	pNo = item.getBizNo();
    	log.info("{} 使用CashierItem中的payNo {}", mainOrderNo, pNo);
    }else if (StringUtils.isNotBlank(mainOrderVO.getPayNo()) && mainOrderVO.getPayType().equals(payType)) {
    	//如果主订单已存在payno且支付方式不切换，使用之前的payno
    	pNo = mainOrderVO.getPayNo();
    	log.info("{} 使用mainOrderVO中的payNo {}", mainOrderNo, pNo);
    }
    // 没有的话会生成空字符串，表示第一次生成
    if(StringUtils.isBlank(pNo)){ 
    	//切换支付方式或者老的为空重新生成pNo
    	UniqueNoType payNoType = isFreePay ? UniqueNoType.NPO : UniqueNoType.P;
    	pNo = UniqueNoUtils.next2(payNoType, orderService.nextval2());
    	isUseNewPayNo = true;
    }
    
    String oldPayNo = StringUtils.defaultString(mainOrderVO.getPayNo());
	String oldPayType = StringUtils.defaultString(mainOrderVO.getPayType()+"");
	if( !oldPayType.equalsIgnoreCase(payType) || isUseNewPayNo)
		log.info("{} 新生成payNo {} -> {},切换支付方式:{} -> {}", mainOrderNo, oldPayNo, pNo,oldPayType,payType);	
	if(isUseNewPayNo)
		payNoLogMapper.insert(new PayNoLog(mainOrderNo, oldPayNo, pNo,oldPayType,payType));
	
    if (mainOrderVO.getId() != null) {
      mainOrderVO.setPayType(form.getPayType());
      MainOrder mainOrder = new MainOrder();
      mainOrder.setId(mainOrderVO.getId());
      mainOrder.setPayType(form.getPayType());
      mainOrder.setPayNo(pNo);
      mainOrderService.updatePayNo(new MainOrderVO(mainOrder,
              mainOrderVO.getOrders()), pNo, form.getPayType());
      log.info("订单[{}] 新生成payNo[{}]需要更新主单和子单支付方式和支付号", mainOrderNo, oldPayNo, pNo);
    }

    String productId = mainOrderVO.getOrders().get(0).getOrderItems().get(0).getProductId();
    String productName = mainOrderVO.getOrders().get(0).getOrderItems().get(0).getProductName();
    UserPartnerType partnerType = UserPartnerType.KKKD;
    BigDecimal totalFee = mainOrderVO.getTotalFee();
    String userId = mainOrderVO.getBuyerId();

    // 全积分兑换直接走付款逻辑
    if (isFreePay) {
      mainOrderService.pay(mainOrderVO.getOrderNo(), null, pNo);
      List<OrderVO> orders = mainOrderVO.getOrders();
      for (OrderVO order : orders) {
        orderService.pay(order.getOrderNo(), null, pNo);
      }
      log.info("订单[{}]全积分兑换直接走付款逻辑", mainOrderNo);
      return "redirect:" + payUrlConfig.hostName() + "/pay/freepayApp?tradeNo=" + pNo;
    }
	if(isUseNewPayNo) {
	    //加入支付方式的收银台
	    List<CashierItem> items = new ArrayList<>(createCashierChannel(form.getPayAgreementId(), pNo, form.getPayType(),
	        totalFee, form.getCardType(), form.getBankCode(), userId, productId, productName,
	        partnerType.toString(), mainOrderVO.getOrderNo(), paymentChannel));
	    log.info("订单[{}]创建收银台信息", mainOrderNo);
	    cashierService.save(items, pNo, null);
	}
    return "redirect:" + payUrlConfig.hostName() + "/cashier/" + pNo
            + "?bizType=order&batchBizNos="
            + mainOrderVO.getOrderNo() + "&currentUrl=" + form.getCurrentUrl();
  }

  private void checkPieceTime(MainOrderVO mainOrderVO, Long cpId) {
    List<OrderVO> orders = mainOrderVO.getOrders();
    if (orders == null || orders.size() == 0 || orders.get(0).getOrderType() == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "订单信息有误");
    }
    if (!OrderSortType.PIECE.equals(orders.get(0).getOrderType())) {
      return;
    }
    PromotionOrderDetail promotionOrderDetail =
        promotionOrderDetailService.loadDetailByMainOrderNo(mainOrderVO.getOrderNo(), "piece");
    if (promotionOrderDetail == null) {
      throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "获取活动订单失败");
    }

    String tranCode = promotionOrderDetail.getPiece_group_tran_code();

    PromotionTranInfoVO tranInfoVO = promotionTranInfoService.selectPromotionPgTranCode(tranCode);
    if (tranInfoVO == null) {
      throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "获取团信息失败");
    }

    long currentTimeMillis = System.currentTimeMillis();

    if (tranInfoVO.getGroupOpenMemberId().equals(cpId.toString())) {//开团者是当前用户则为开团

      PromotionBaseInfo promotionBaseInfo = promotionBaseInfoService.selectByPCode(promotionOrderDetail.getP_code());
      if (promotionBaseInfo == null || promotionBaseInfo.getEffectTo() == null) {
        throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "获取活动信息失败");
      }

      if (currentTimeMillis > promotionBaseInfo.getEffectTo().getTime()) {//活动时间校验
        throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "拼团活动已结束");
      }

    } else {//开团者不是当前用户则为参团

      if (tranInfoVO.getGroupOpenTime() == null) {
        throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "开团时间为空");
      }
      tranInfoVO.getTranEndTime().map(Date::getTime)
          .ifPresent(endTime -> {
            if (currentTimeMillis > endTime) {
              log.info("团 {} 已失效", tranCode);
              throw new BizException(GlobalErrorCode.PIECE_ERROR_OVERTIME, "团已失效");
            }
          });
    }
  }

  /**
   * 临时方案已经支付过
   * @param m
   * @return
   */
  private boolean checkMultiPay(MainOrderVO m) {
    if (Objects.isNull(m))
      return false;
    if(StringUtils.isNotBlank(m.getPayNo()))
      return true;
    return false;
  }

  /**
   * 支付校验白名单
   */
  private boolean checkIsWhiteOnPay(MainOrderVO mainOrderVO, Long cpId) {
    if( mainOrderVO == null){
      return false;
    }
    List<String> skuIds = new ArrayList<>();
    List<OrderVO> orders = mainOrderVO.getOrders();
    //获取sku
    for (OrderVO oVO : orders){
      if(oVO == null){
        continue;
      }
      List<OrderItem> orderItems = oVO.getOrderItems();
      for (OrderItem oItem : orderItems){
        if(oItem == null){
          continue;
        }
        String skuId = oItem.getSkuId();
        if(StringUtils.isNotBlank(skuId)){
          skuIds.add(skuId);
        }
      }
    }
    if(skuIds.isEmpty()){
      return false;
    }
    String[] skuIdArr = skuIds.toArray(new String[0]);
    //用sku去校验
    List<Sku> skus = skuMapper.selectByIds(skuIdArr);
    if(skus == null || skus.isEmpty()){
      return false;
    }
    Set<Boolean> b = new HashSet<>();
    for (Sku sku : skus){
      if(sku != null){
          boolean isWhite = promotionBaseInfoService.checkIsWhiteBySkuId(sku.getSkuCode(), cpId);
          b.add(isWhite);
        }
    }
    if(!b.isEmpty() && b.size() == 1 && b.contains(false)){
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, WHITELIST_CONFIRE_ALERT_CONTENT);
    }

    return true;
  }

  private List<CashierItem> createCashierChannel(String payAgreementId, String pNo,
      PaymentMode mode,
      BigDecimal totalFee, PaymentChannel bankName, String bankCode, String userId,
      String productId,
      String productName, String partner, String bizNos, PaymentChannel paymentChannel) {
    List<CashierItem> items = new ArrayList<CashierItem>();

    if (totalFee.compareTo(BigDecimal.ZERO) == 1) {
      //记录支付
      CashierItem item = new CashierItem();
      item.setBizNo(pNo);
      item.setBankCode(bankCode);
      item.setAmount(totalFee);
      item.setUserId(userId);
      item.setProductId(productId);
      item.setProductName(productName);
      item.setPartner(partner);
      item.setAgreementId(StringUtils.isBlank(payAgreementId) ? null : payAgreementId);
      item.setPaymentMode(mode);
      item.setBatchBizNos(bizNos);
      item.setStatus(PaymentStatus.PENDING);
      item.setBankName(bankName == null ? "" : bankName.toString());
      item.setPaymentChannel(paymentChannel);
      items.add(item);
    }

    return items;
  }

  /**
   * 订单确认请求
   */
  @RequestMapping(value = "/order/confirmShipped")
  public String confirmShipped(@ModelAttribute OrderPayAgainForm form,
      Model model, RedirectAttributes redirectAttrs, Device device) {
    OrderVO order = orderService.loadVO(form.getOrderId());
    if (order == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "订单["
          + form.getOrderId() + "]不存在");
    }

    if (order.getTotalFee().add(order.getDiscountFee())
        .compareTo(BigDecimal.ZERO) != 1) {
      log.error("订单总额不能为0  orderNo=[" + order.getOrderNo() + "]");
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "订单总额不能为0");
    }

    if (order.getId() != null) {
      orderService.executeBySystem(order.getId(), OrderActionType.SIGN, null);
      model.addAttribute("success", "确认收货成功");
      log.info("订单  orderNo=[" + order.getOrderNo() + "]" + "以签收");
    }

    // 加入确认收货加积分
    List<Result> modifyResult = yundouAmountService
        .modifyAmount(YundouOperationType.CONFIRM_RECEIPT, order.getId());
    log.info("订单 orderNo=[{}] 确认收货后修改相应积分，明细: {}", order.getOrderNo(), modifyResult);

    return "order/orderSuccess";
  }

  //一元专区一个批次只能参加一次 add by hs
  private Boolean checkSaleZoneJoinPolicyForPay(MainOrderVO mainOrderVO) {

    List<String> skuIds = new ArrayList<String>();
    List<OrderVO> orderList = mainOrderVO.getOrders();
    for (OrderVO orderVO : orderList) {
      List<OrderItem> orderItems = orderVO.getOrderItems();
      for (OrderItem orderItem : orderItems) {
        String skuId = orderItem.getSkuId();
        if (null != orderItem.getPromotionType() &&
                orderItem.getPromotionType().equals(PromotionType.SALEZONE)) {
          skuIds.add(skuId);
        }
      }
    }
    return skuIds.size() > 0;
  }

  private Boolean checkSaleZone() {
    User user = getCurrentUser();
    return orderService.checkSaleZone(user.getId());
  }

  public IUser getCurrentIUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        IUser user = (IUser) principal;
        if (!user.isAnonymous()) {
          return user;
        }
      }

      if (!auth.getClass().getSimpleName().contains("Anonymous")) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }


}
