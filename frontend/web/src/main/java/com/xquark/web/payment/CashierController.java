package com.xquark.web.payment;

import com.xquark.dal.consts.PayUrlConfig;
import com.xquark.dal.mapper.MainOrderMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.BankItemsMap;
import com.xquark.dal.model.CashierItem;
import com.xquark.dal.model.PayBankWay;
import com.xquark.dal.model.User;
import com.xquark.dal.type.*;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.event.MainOrderActionEvent;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.payBank.PayBankService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.product.ProductService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.web.BaseController;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class CashierController extends BaseController {

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private PayBankService payBankService;

  @Autowired
  private MainOrderMapper mainOrderMapper;


  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private PayUrlConfig payUrlConfig;


  @Value("${site.web.host.name}")
  private String hostName;

  @Value("${xiangqu.web.site}")
  private String xqHostName;


  /**
   * 想去支付
   */
  private String xiqngqu(CashierItem item, RedirectAttributes redirectAttrs) {
    User user = super.getCurrentUser();
    if (!"xiangqu".equals(user.getPartner())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户类型错误 userId=" + user.getId());
    }
    redirectAttrs.addAttribute("cashierItemId", item.getId());
    redirectAttrs.addAttribute("couponId", item.getCouponId());
    redirectAttrs.addAttribute("outUserId", user.getExtUserId());
    redirectAttrs.addAttribute("productId", item.getProductId());
    redirectAttrs.addAttribute("tradeNo", item.getBizNo());
    // redirectAttrs.addAttribute("subject", orderItem.getProductName());
    redirectAttrs.addAttribute("subject", item.getBizNo());
    redirectAttrs.addAttribute("totalFee", item.getAmount());
    return "redirect:/pay/xiangqu";
  }

  /**
   * 支付宝支付
   */
  private String alipay(CashierItem item, RedirectAttributes redirectAttrs, Device device) {
    redirectAttrs.addAttribute("cashierItemId", item.getId());
    redirectAttrs.addAttribute("tradeNo", item.getBizNo());
    redirectAttrs.addAttribute("subject", item.getProductName());
    redirectAttrs.addAttribute("tradeUrl",
        hostName + "/order/" + cashierService.findDefaultBizId(item.getBizNo()));
    redirectAttrs.addAttribute("totalFee", item.getAmount());
    redirectAttrs.addAttribute("tradeType", TradeType.ALIPAY);
    redirectAttrs.addAttribute("payType", "ALIPAY");
    if (item.getPaymentMode() == PaymentMode.ALIPAY) {
      return "redirect:/pay/alipay";
    } else {
      return "redirect:/pay/alipayPc";
    }
  }

  private String alipayPc(CashierItem item, RedirectAttributes redirectAttrs) {
    redirectAttrs.addAttribute("cashierItemId", item.getId());
    redirectAttrs.addAttribute("tradeNo", item.getBizNo());
    redirectAttrs.addAttribute("subject", item.getBizNo());
    redirectAttrs.addAttribute("tradeUrl",
        hostName + "/order/" + cashierService.findDefaultBizId(item.getBizNo()));
    redirectAttrs.addAttribute("totalFee", item.getAmount());
    return "redirect:/pay/alipayPc";
  }

  /**
   * 小程序支付
   */
  private String tenpayMiniProgram(CashierItem item, RedirectAttributes redirectAttrs,
      String currentUrl) {
    String realTenPayURL =
          payUrlConfig.hostName() + "/pay/tenpayMiniProgram?cashierItemId=" + item.getId()
            + "&tradeNo=" + item.getBizNo()
            + "&subject=" + item.getBizNo()
            + "&tradeUrl=" + hostName + ("/order/" + cashierService.findDefaultBizId(item.getBizNo()))
            + "&totalFee=" + item.getAmount()
            + "&currentUrl=" + currentUrl
            + "&payType=WEIXIN&tradeType=MINIPROGRAM";
    return "redirect:" + realTenPayURL;
  }

  /**
   * 微信服务号支付
   */
  private String tenpay(CashierItem item, RedirectAttributes redirectAttrs, String currentUrl) {
    String realTenPayURL = payUrlConfig.hostName() + "/pay/tenpay?cashierItemId=" + item.getId()
        + "&tradeNo=" + item.getBizNo()
        + "&subject=" + item.getBizNo()
        + "&tradeUrl=" + hostName + ("/order/" + cashierService.findDefaultBizId(item.getBizNo()))
        + "&totalFee=" + item.getAmount()
        + "&currentUrl=" + currentUrl;
    //FIXME 第二版优化
    String oauthUrl = "";
    try {
      String productId = item.getProductId();
      String shopId = productService.load(productId).getShopId();
      String rootShopId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();
      String appId = wechatAppConfigService.load(rootShopId).getAppId();
      //String appId = "wxac9fc36ed2f86209";
      oauthUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId
          + "&redirect_uri=" + URLEncoder.encode(realTenPayURL, "utf-8")
          + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect"
          + "&payType=WEIXIN&tradeType=H5";
    } catch (final UnsupportedEncodingException e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "生成支付认证地址失败，", e);
    }

    //https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx9ecb8bd557cdc1f9&redirect_uri=http%3a%2f%2f51shop.mobi%2fp%2fo0ibxq0&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
    return "redirect:" + oauthUrl;
  }

  /**
   * APP微信支付
   */
  private String tenpayApp(CashierItem item, RedirectAttributes redirectAttrs) {
    String realTenPayURL = "/pay/tenpayApp?cashierItemId=" + item.getId()
        + "&tradeNo=" + item.getBizNo()
        + "&subject=" + item.getBizNo()
        + "&tradeUrl=" + hostName + ("/order/" + cashierService.findDefaultBizId(item.getBizNo()))
        + "&totalFee=" + item.getAmount()
        + "&payType=WEIXIN&tradeType=APP";
    return "redirect:" + realTenPayURL;
  }

  private String union(CashierItem item, RedirectAttributes redirectAttrs) {
    redirectAttrs.addAttribute("cashierItemId", item.getId());
    redirectAttrs.addAttribute("subject", item.getBizNo());
    redirectAttrs.addAttribute("tradeNo", item.getBizNo());
    redirectAttrs.addAttribute("totalFee", item.getAmount());

    return "redirect:/pay/umpay";
  }

  private String nextUrl(CashierItem item, String bizNo, String bizType, Device device, Model model,
      RedirectAttributes redirectAttrs, String currentUrl) {
    if ("order".equals(bizType)) {
      //TODO应该调交易系统
      if (item == null) {
        List<CashierItem> items = cashierService.listByBizNo(bizNo);
        String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
        String orderNo = batchBizNos[0];
        MainOrderVO order = mainOrderService.loadByOrderNo(orderNo);
        model.addAttribute("order", order);
        return "/pay/call_back";
      }

      // 全积分兑换跳过支付
      // TODO 待测试
      if (item.getAmount() == null || item.getAmount().signum() == 0) {
        return null;
      }

      switch (item.getPaymentMode()) {
        case XIANGQU:
          return xiqngqu(item, redirectAttrs);
        case ALIPAY:
          return alipay(item, redirectAttrs, device);
        case ALIPAY_PC:
          return alipayPc(item, redirectAttrs);
        case UNION:
        case UMPAY:
          return union(item, redirectAttrs);
        case WEIXIN:
          return tenpay(item, redirectAttrs, currentUrl);
        case WEIXIN_MINIPROGRAM:
          return tenpayMiniProgram(item, redirectAttrs, currentUrl);
        case WEIXIN_APP:
          return tenpayApp(item, redirectAttrs);
        default:
          return null;
      }
    }
    return null;
  }

  @RequestMapping(value = "/cashier/callback2/")
  public String payCallback2(HttpServletRequest req){
    String bizType="order";
    String cashierItemId="9587";
    String paidStatus="success";
    PaymentMode paymentMode = PaymentMode.WEIXIN_MINIPROGRAM;

    return payCallback(req,"ES180922233342054006",bizType,paymentMode,paidStatus,cashierItemId,null,null,null);
  }

  @RequestMapping(value = "/cashier/callback/{bizNo}")
  public String payCallback(HttpServletRequest req, @PathVariable("bizNo") String bizNo,
      String bizType,
      PaymentMode paymentMode, String paidStatus, String cashierItemId,
      Device device, Model model, RedirectAttributes redirectAttrs) {

    if ("success".equals(paidStatus)) {
      CashierItem nextCashierItem = cashierService.paid(bizNo, bizType,
          paymentMode, cashierItemId);
      List<CashierItem> items = cashierService.listByBizNo(bizNo);
      String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
      String orderNo = batchBizNos[0];
      // 订单支付短信通知
      log.info("开始向买家卖家发送支付成功短信，订单号：orderNO=" + orderNo);
//			applicationContext.publishEvent(new OrderActionEvent(
//					OrderActionType.PAY, orderMapper.selectByOrderNo(orderNo)));
      applicationContext.publishEvent(new MainOrderActionEvent(
          MainOrderActionType.PAY, mainOrderMapper.selectByOrderNo(orderNo)));
      return nextUrl(nextCashierItem, bizNo, bizType, device, model,
          redirectAttrs, "");
    } else {
      List<CashierItem> items = cashierService.listByBizNo(bizNo);
      String[] batchBizNos = items.get(0).getBatchBizNos().split(",");
      String orderNo = batchBizNos[0];
      OrderVO order = orderService.loadByOrderNo(orderNo);

      if (UserPartnerType.XIANGQU == order.getPartnerType()) {
        return "redirect:" + xqHostName + "/order/pay/" + order.getId()
                + ".html?status=0";
      } else {

        // 跳转到失败页面
        log.error("cashier failed bizNo=[" + bizNo + "] paymentMode=["
            + paymentMode + "]");
        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "支付失败");
      }
    }
  }

  @RequestMapping(value = "/cashier/notify/{bizNo}")
  public void payNotify(@PathVariable("bizNo") String bizNo, String bizType,
      PaymentMode paymentMode, RedirectAttributes redirectAttrs) {
    cashierService.paid(bizNo, bizType, paymentMode);
  }

  /**
   * 单个订单支付
   */
  @RequestMapping(value = "/cashier/{bizNo}")
  public String pay(@PathVariable("bizNo") String bizNo, String bizType, String batchBizNos,
      Device device, Model model, RedirectAttributes redirectAttrs, String currentUrl) {
    CashierItem item = cashierService.nextCashierItem(bizNo);
    return nextUrl(item, bizNo, bizType, device, model, redirectAttrs, currentUrl);
  }

  /**
   * 批量支付(多订单合并支付)
   */
  @RequestMapping(value = "/cashier")
  public String batchPay(String bizNos, String bizType, Device device, Model model,
      RedirectAttributes redirectAttrs) {

    CashierItem item = cashierService.nextCashierItem(bizNos);
    return nextUrl(item, bizNos, bizType, device, model, redirectAttrs, "");
  }

  /**
   * 选择银行卡后回调
   */
  @RequestMapping(value = "/cashier/{bizNo}/{cashierItemId}/{cardType}/{bankCode}/{paymentMode}")
  public String bank(@PathVariable("bizNo") String bizNo,
      @PathVariable("cashierItemId") String cashierItemId,
      @PathVariable("cardType") PaymentChannel cardType, @PathVariable("bankCode") String bankCode,
      @PathVariable("paymentMode") PaymentMode paymentMode, Model model,
      RedirectAttributes redirectAttrs) {
    CashierItem item = new CashierItem();
    item.setId(cashierItemId);
//		item.setPaymentChannel(cardType);
    item.setBankName(cardType.toString());
    item.setBankCode(bankCode);
    item.setPaymentMode(paymentMode);
    cashierService.update(item);
    return "redirect:/cashier/" + bizNo + "?bizType=order";
  }

  @RequestMapping(value = "/pay/unionBankList")
  public String queryPayBankForShow(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model mode) {
    String tradeNo = request.getTradeNo();
    String subject = request.getSubject();
    String tradeUrl = request.getTradeUrl();
    String cashierItemId = req.getParameter("cashierItemId");
    CashierItem cashierItem = cashierService.load(cashierItemId);
    BigDecimal totalFee = request.getTotalFee();
//		OrderVO order = orderService.loadByOrderNo(tradeNo);
    List<PayBankWay> hotBanksCreditCard = payBankService.queryHotPayBanksCreditCard();
    List<PayBankWay> allBanksCreditCard = payBankService.queryAllPayBanksCreditCard();
    List<BankItemsMap> creditCardBanks = null;
    creditCardBanks = trans4Show(hotBanksCreditCard, allBanksCreditCard);

    List<PayBankWay> hotBanksDebitCard = payBankService.queryHotPayBanksDebitCard();
    List<PayBankWay> allBanksDebitCard = payBankService.queryAllPayBanksDebitCard();
    List<BankItemsMap> debitCardBanks = null;
    debitCardBanks = trans4Show(hotBanksDebitCard, allBanksDebitCard);

    mode.addAttribute("tradeNo", tradeNo);
    mode.addAttribute("tradeUrl", tradeUrl);
    mode.addAttribute("subject", subject);
    mode.addAttribute("totalFee", totalFee);
    mode.addAttribute("cashierItemId", req.getParameter("cashierItemId"));
    mode.addAttribute("creditCardBanks", creditCardBanks);
    mode.addAttribute("debitCardBanks", debitCardBanks);
    if (StringUtils.equalsIgnoreCase(cashierItem.getPartner(), "xiangqu")) {
      return "xiangqu/unionBankList";
    } else {
      return "pay/unionBankList";
    }
  }


  private List<BankItemsMap> trans4Show(List<PayBankWay> hotBanks, List<PayBankWay> allBanks) {
    List<BankItemsMap> result = new ArrayList<BankItemsMap>();
    if (hotBanks != null && hotBanks.size() > 0) {
      BankItemsMap map = new BankItemsMap();
      map.setKeyName("hot");
      map.setValueList(hotBanks);
      result.add(map);
    }

    List<String> keys = new ArrayList<String>();
    Map<String, List<PayBankWay>> temp = new HashMap<String, List<PayBankWay>>();
    if (allBanks != null && allBanks.size() > 0) {
      for (int i = 0; i < allBanks.size(); i++) {
        PayBankWay bean = allBanks.get(i);
        String key = bean.getStartWith();
        if (!keys.contains(key)) {
          keys.add(key);
          List<PayBankWay> list = new ArrayList<PayBankWay>();
          list.add(bean);
          temp.put(key, list);
        } else {
          temp.get(key).add(bean);
        }
      }
    }
    for (int i = 0; i < keys.size(); i++) {
      String key = keys.get(i);
      List<PayBankWay> list = temp.get(key);
      BankItemsMap map = new BankItemsMap();
      map.setKeyName(key);
      map.setValueList(list);
      result.add(map);
    }
    return result;
  }


  public static void main(String[] args) {
  }


}
