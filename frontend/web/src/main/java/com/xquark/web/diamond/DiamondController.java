package com.xquark.web.diamond;

import com.xquark.dal.model.Category;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.PromotionDiamondProductVO;
import com.xquark.service.category.CategoryService;
import com.xquark.service.category.TermTaxonomyService;
import com.xquark.service.diamond.PromotionDiamondService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.web.BaseController;
import com.xquark.web.diamond.vo.DiamondAppVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DiamondController extends BaseController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private TermTaxonomyService termTaxonomyService;

  @Autowired
  private PromotionDiamondService promotionDiamondService;

  @Autowired
  private ShopTreeService shopTreeService;

  private final Logger logger = LoggerFactory.getLogger(getClass());

  /**
   * app端访问钻石专享页面
   */
  @RequestMapping(value = "/app/diamondList")
  public String view(Model model, HttpServletRequest req) {
    ArrayList<DiamondAppVO> vos = new ArrayList<DiamondAppVO>();
    String shopId = this.getCurrentUser().getShopId();
    ShopTree ShopTree = shopTreeService.selectRootShopByShopId(shopId);
    String rootShopId = ShopTree.getRootShopId();
    Shop shop = shopService.load(shopId);
    // 先找到所有一级分类
    List<Category> categories = termTaxonomyService.listRootCategories(Taxonomy.GOODS);
    for (Category category : categories) {
      // 取出此大类下的六个商品，如果没有商品，则大类也不显示
      List<PromotionDiamondProductVO> products = promotionDiamondService
          .listDiamondApp(rootShopId, category.getId(), 6);
      if (products != null && products.size() > 0) {
        DiamondAppVO vo = new DiamondAppVO();
        vo.setCategory(category);
        vo.setProducts(products);
        vos.add(vo);
      }
    }
    model.addAttribute("vos", vos);
    model.addAttribute("shop", shop);
    return "b2b/app/vip/vip";
  }

  /**
   * app钻石专享点击分类的更多到分类商品界面
   */
  @RequestMapping(value = "/app/diamondMore")
  public String diamondMore(@RequestParam(value = "categoryId", required = false) String categoryId,
      Model model, HttpServletRequest req) {
    Category category = categoryService.load(categoryId);
    String shopId = this.getCurrentUser().getShopId();
    ShopTree ShopTree = shopTreeService.selectRootShopByShopId(shopId);
    String rootShopId = ShopTree.getRootShopId();
    Shop shop = shopService.load(shopId);
    model.addAttribute("categoryId", category.getId());
    model.addAttribute("categoryName", category.getName());
    model.addAttribute("shop", shop);
    return "b2b/app/vip/vipMore";
  }


}
