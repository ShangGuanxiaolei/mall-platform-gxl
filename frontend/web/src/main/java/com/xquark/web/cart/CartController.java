package com.xquark.web.cart;

import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.mapper.ShopAccessMapper;
import com.xquark.dal.mapper.TermRelationshipMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.ProductStatus;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.outpay.OutPayAgreementService;
import com.xquark.service.payBank.PayBankService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.impl.CartFrom;
import com.xquark.service.pricing.impl.pricing.PricingMode;
import com.xquark.service.pricing.vo.CartPricingResultVO;
import com.xquark.service.pricing.vo.CartPromotionResultVO;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.pricing.vo.UserSelectedProVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.cart.form.AddToCartForm;
import com.xquark.web.cart.form.CartForm;
import com.xquark.web.cart.form.CartNextForm;
import com.xquark.web.cart.form.CartPricingForm;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;

/**
 * 购物车相关逻辑
 *
 * @author odin
 * @author tonghu
 */
@Controller
public abstract class CartController extends BaseController {

//  private final String MSG_KEY_SKU_NOT_EXSIT = "product.sku.not_exist";
//  private final String MSG_KEY_SKU_OUT_OF_STOCK = "product.sku.out.of.stock";
//  private final String MSG_KEY_SKU_SHORT_OF_STOCK = "product.sku.short.of.stock";
//  private final String MSG_KEY_CART_ITEM_EXCEED_OF_STOCK = "product.item.exceed.of.stock";

  @Autowired
  private CartService cartService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private PricingService pricingService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  protected CouponService couponService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private OutPayAgreementService outPayAgreementService;

  @Autowired
  protected PayBankService payBankService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ShopAccessMapper shopAccessMapper;

  @Autowired
  private TermRelationshipMapper termRelationshipMapper;

  @Autowired
  private FlashSalePromotionProductService flashSalePromotionProductService;

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Value("${xiangqu.web.site}")
  private String xiangquWebSite;

  @Value("${xiangqu.web.head.url}")
  private String xiangquWebHeadUrl;

  @Value("${xiangqu.web.foot.url}")
  private String xiangquWebFootUrl;

  /**
   * 商品加入购物车
   */
  public ResponseObject<Integer> add(@Valid @ModelAttribute AddToCartForm form,
      HttpServletRequest req, Errors errors, String domain, String source) {
    try {
      ControllerHelper.checkException(errors);
      User user = getCurrentUser();
      String productId = form.getProductId();
      ProductVO productVO = productService.load(productId);
      String realShopId = user.getCurrentSellerShopId();
      cartService.addToCart(user.getId(), form.getSkuId(), form.getAmount(), form.getShopOwnerId(),
          form.getProductId(), realShopId);

      Integer count = 0;
      List<CartItemVO> cartItems = cartService.checkout();
      for (CartItemVO cartItemVO : cartItems) {
        if (cartItemVO.getAmount() > 0) {
          count++;
        }
      }
      return new ResponseObject<Integer>(count);
    } catch (BizException e) {
      return new ResponseObject<Integer>(e);
    }
  }

  /**
   * 商品直接购买
   */
  public ResponseObject<Boolean> buy(@ModelAttribute AddToCartForm form, HttpServletRequest req,
      Model model) {
    int amount =
        form.getAmount() == null || form.getAmount().intValue() == 0 ? 1 : form.getAmount();
    cartService.saveOrUpdateCartItemAmount(form.getSkuId(), amount);
    return new ResponseObject<Boolean>(true);
  }

  /**
   * 想去检查skus，在cart/next之前
   */
  public String checkout(String skuId, @RequestParam(defaultValue = "1") Integer amount,
      HttpServletRequest request, HttpServletResponse response, Device device, Model model) {
    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
    }

    String partner = request.getParameter("partner");
    if ("xiangqu".equals(partner) && (user == null || !"xiangqu".equals(user.getPartner()) || user
        .getLoginname().startsWith("CID"))) {
//            Cookie[] cookies = request.getCookies();
//            if (cookies != null) {
//                for(Cookie cookie : cookies) {
//                    if (cookie.getName().equals("KDSESSID") && request.getSession().getId().equals(cookie.getValue())) { //
//                        log.info("KDSESSID:" + cookie.getValue());
//                        // 删除子域的domain
//                        cookie.setPath("/");
//                        cookie.setMaxAge(0);
//                        response.addCookie(cookie);
//                    }
//                }
//            }

      String requestURL = request.getRequestURL().toString();
      if (request.getQueryString() != null) {
        requestURL += "?" + request.getQueryString();
      }
      String xiangquGetTokenUrl = "/";
      try {
        xiangquGetTokenUrl =
            xiangquWebSite + "/XQuark/getToken?backUrl=" + URLEncoder.encode(requestURL, "utf-8");
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
      return "redirect:" + xiangquGetTokenUrl;
    }

    if (skuId != null) {
      Sku sku = productService.loadSku(skuId);
      if (sku != null) {
        ProductVO aVo = productService.load(sku.getProductId());

        if (aVo == null || !aVo.getStatus().equals(ProductStatus.ONSALE)) {
          if (user.getPartner() != null) {
            //model.addAttribute("prodId", aVo.getId());
            return user.getPartner() + "/merchandiseOffShelf";
          } else {
            return "cart/merchandiseOffShelf";
          }
        }
      }
    }

    //if (amount > 0) {
    //cartService.saveOrUpdateCartItemAmount(skuId, amount);
    //}
    return "redirect:/cart/next?skuId=" + skuId + "&qty=" + amount;
  }

  /**
   * 购物车页面
   */
  public String cart(@ModelAttribute CartForm form, HttpServletRequest req, Device device,
      Model model) {
    // 获取购物车参数
    String shopId = form.getShopId();
    shopId = this.getShopId(shopId);
    Integer cartStatus = 0;

    if (shopId == null) {
      List<String> recentShopRawIds = shopAccessMapper.selectRecentVisitedShop(5);

      Map<String, Shop> recentShops = new HashMap<String, Shop>();
      for (String recentShopRawId : recentShopRawIds) {
        String recentShopId = IdTypeHandler.encode(Long.parseLong(recentShopRawId));
        Shop recentShop = shopService.load(recentShopId);
        recentShops.put(recentShopId, recentShop);
      }
      model.addAttribute("recentShops", recentShops);
      cartStatus = 2;
    } else {
      Shop shop = shopService.load(shopId);
      model.addAttribute("shop", shop);
    }

    // 去掉参数中重复的skuId
    List<String> skuIds = null;
    if (form.getSkuIds() != null) {
      skuIds = new ArrayList<String>(new HashSet<String>(form.getSkuIds()));
    } else {
      skuIds = new ArrayList<String>();
    }

    Integer amount = form.getAmount();

    List<String> errors = new ArrayList<String>();
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    String nextUrl = "";

    if (skuIds != null && skuIds.size() > 0) {
      // 过滤相同的sku
      Set<String> skuIdSet = new LinkedHashSet<String>(skuIds);
      if (skuIds.size() == 1 && amount != null) {
        String sid = (String) skuIds.toArray()[0];
        cartService.saveOrUpdateCartItemAmount(sid, amount);
      }
      try {
        cartItems = cartService.checkout(skuIdSet);
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems(skuIdSet);
      }
      nextUrl = "/cart/next?skuId=" + skuIdSet;
    } else if (StringUtils.isNotEmpty(shopId)) {
      try {
            /* ----
            * update by liubei 2015-11-16
            * disable cart list shop filter for new cart function
            * also disable amount and status check to avoid cart item implicit drop
                cartItems = cartService.checkout(shopId);
            ---- */
        cartItems = cartService.listCartItems(shopId);
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems(shopId);
      }
      nextUrl = "/cart/next?shopId=" + shopId;
    } else {
      try {
        cartItems = cartService.checkout();
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems();
      }
      nextUrl = "/cart/next";
    }

    // 可在购物车中实现修改购买规格
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    Map<Shop, List<CartItemVO>> cartItemMap = new LinkedHashMap<Shop, List<CartItemVO>>();
    for (CartItemVO item : cartItems) {
      skuMap.put(item.getSkuId(), item.getAmount());

      Shop srcShop = item.getShop();
      List<CartItemVO> list = cartItemMap.get(srcShop);
      if (list == null) {
        list = new ArrayList<CartItemVO>();
        cartItemMap.put(srcShop, list);
      }
      list.add(item);
    }

    if (cartStatus == 2) {
      cartItemMap.clear();
    } else if (cartItemMap.size() > 0) {
      cartStatus = 1;
    }

    //计算购物车费用
    //PricingResultVO prices = pricingService.calculate(skuMap, null, null);
    CartPromotionResultVO cartPromotionRet = promotionService
        .calculate4Cart(cartItemMap, getCurrentUser(), null,
            false);

    //以cartItem的k，v来get
    model.addAttribute("prices", cartPromotionRet.getTotalPricingResult());
    model.addAttribute("shopPromotions", cartPromotionRet.getShopPromotions());
    model.addAttribute("shopCoupons", cartPromotionRet.getShopCoupons());
    model.addAttribute("cartItemCoupons", cartPromotionRet.getCartItemCoupons());
    model.addAttribute("cartItemPromotions", cartPromotionRet.getCartItemPromotions());
    model.addAttribute("shopPricingResult", cartPromotionRet.getShopPricingResult());
    model.addAttribute("cartStatus", cartStatus);

    // FIXME:to delete
    model.addAttribute("cartItems", cartItems);
    model.addAttribute("cartItemMap", cartItemMap);
    model.addAttribute("nextUrl", nextUrl);
    model.addAttribute("skuIds", skuIds);
    model.addAttribute("shopId", shopId);
    model.addAttribute("errors", errors);

    return "cart/cart";
  }

  public ResponseObject<Integer> count() {
    Integer count = cartService.count();
//        Integer count = 0;
//        List<CartItemVO> cartItems = cartService.checkout();
//        for (CartItemVO cartItemVO : cartItems) {
//            if (cartItemVO.getAmount() > 0) {
//                count++;
//            }
//        }
    return new ResponseObject<Integer>(count);
  }

  public ResponseObject<Integer> count(String userId) {
    Integer count = cartService.count(userId);
    return new ResponseObject<Integer>(count);
  }

  public ResponseObject<Integer> count(String partner, String thirdId) {
    User user = userService.loadByLoginname(thirdId + "@" + partner);
    Integer count = 0;
    if (user != null) {
      count = cartService.count(user.getId());
    } else {
      count = cartService.count();
    }
    return new ResponseObject<Integer>(count);
  }

  /**
   * 更新购物车中商品的数量
   */
  public ResponseObject<Boolean> update(@RequestParam String skuId, @RequestParam int amount,
      HttpServletRequest req) {
    try {
      // TODO 参数校验
      cartService.saveOrUpdateCartItemAmount(skuId, amount);
      return new ResponseObject<Boolean>(true);
    } catch (BizException e) {
      return new ResponseObject<Boolean>(e);
    }
  }

  public ResponseObject<Boolean> delete(String itemId) {
    // TODO 参数校验
    return new ResponseObject<Boolean>(cartService.remove(itemId));
  }

  /**
   * ajax调用 检查商品规格
   */
  public ResponseObject<Boolean> validate(HttpServletRequest req) {
    String skuId = req.getParameter("skuId");
    String shopId = req.getParameter("shopId");

    if (StringUtils.isNotEmpty(skuId)) {
      List<String> skuIds = Arrays.asList(skuId.split("-"));
      try {
        cartService.validate(skuIds);
      } catch (BizException ex) {
        return new ResponseObject<Boolean>(ex);
      }
    } else if (StringUtils.isNotEmpty(shopId)) {
      try {
        cartService.validate(shopId);
      } catch (BizException ex) {
        return new ResponseObject<Boolean>(ex);
      }
    } else {
      return new ResponseObject<Boolean>("参数错误，skuId, shopId其中一个不能为空",
          GlobalErrorCode.INVALID_ARGUMENT);
    }
    return new ResponseObject<Boolean>(true);
  }

  private AddressVO fillAddress(Model model) {
    List<AddressVO> addresses = addressService.listUserAddressesVo();
    AddressVO address = null;

    if (!CollectionUtils.isEmpty(addresses)) {  //获取常用的默认地址
      for (AddressVO addressVo : addresses) {
        if (addressVo.getIsDefault()) {
          address = addressVo;
          addresses.remove(address);
          addresses.add(0, address);
          break;
        }
      }
      if (address == null) {  //没有常用地址 默认选择第一个
        address = addresses.get(0);
      }
      model.addAttribute("addresses", addresses);
    }

    if (address != null) {
      List<Zone> parents = zoneService.listParents(address.getZoneId());
      Zone province = null;
      List<Zone> provinceList = null;
      if (parents.size() > 1) {
        province = parents.get(1);
        provinceList = zoneService.listSiblings(province.getId());
      } else {
        provinceList = zoneService.listChildren("1");
      }
      model.addAttribute("province", province);
      model.addAttribute("provinceList", provinceList);

      Zone city = null;
      List<Zone> cityList = null;
      if (parents.size() > 2) {
        city = parents.get(2);
        cityList = zoneService.listSiblings(city.getId());
      }
      model.addAttribute("city", city);
      model.addAttribute("cityList", cityList);

      Zone district = null;
      List<Zone> districtList = null;
      if (parents.size() > 3) {
        district = parents.get(3);
        districtList = zoneService.listSiblings(district.getId());
      }
      model.addAttribute("district", district);
      model.addAttribute("districtList", districtList);
    }
    model.addAttribute("address", address);
    return address;
  }

  /**
   * 提交订单页面，选择收件地址，优惠，支付方式
   */
  public String next(@ModelAttribute CartNextForm form, HttpServletRequest req, Device device,
      Model model, HttpServletResponse response) {
    String promotionId = form.getPromotionId();

    String yundouProductId = form.getYundouProductId();
    String activityId = form.getActivityGrouponId();
    // 默认不参加活动除非有活动id
    Promotion promotion;
    if (StringUtils.isNotBlank(promotionId)) {
      promotion = promotionService.load(promotionId);
      // TODO 临时只支持抢购
      if (promotion != null && promotion.isValid()) {
        model.addAttribute("promotionFrom", promotion.getPromotionType());
      }
    }
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    String realShopId = shopService.loadRootShop().getId();
    if (form.getQty() > 0) {
      // 直接下单,不走购物车流程
      Set<String> skuIds = new LinkedHashSet<String>(form.getSkuId());
      cartItems = cartService.checkout(skuIds, form.getQty(), realShopId, null);
    } else if (form.getSkuId() != null && form.getSkuId().size() > 0) {
      // 以sku结算
      Set<String> skuIds = new LinkedHashSet<String>(form.getSkuId());
      cartItems = cartService.checkout(skuIds, realShopId, null);
    } else if (StringUtils.isNotEmpty(form.getShopId())) {
      // 以店铺结算
      cartItems = cartService.checkout(form.getShopId(), null);
    } else {
      // do nothing
    }

    if (CollectionUtils.isEmpty(cartItems)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "请选择要结算的商品");
    }

    // 购物车中所有商品id
    List<String> productIds = new ArrayList<String>();
    //
    String isCrossBorder = "0";
    // 购物车商品按照店铺分组，注意shop做key，需要实现shop的equals方法
    Map<Shop, List<CartItemVO>> cartItemMap = new HashMap<Shop, List<CartItemVO>>();
    for (CartItemVO item : cartItems) {
      List<CartItemVO> list = cartItemMap.get(item.getShop());
      if (list == null) {
        list = new ArrayList<CartItemVO>();
        cartItemMap.put(item.getShop(), list);
        logger.debug("put shop[" + item.getShop().getId() + "]");
      }
      productIds.add(item.getProductId());
      list.add(item);

      // 判断是否是跨境购商品
      List<String> categoryIds = termRelationshipMapper.getCategoryByProduct(item.getProductId());
      if (categoryIds.contains("1")) {
        isCrossBorder = "1";
      }
    }
    if (CollectionUtils.isNotEmpty(productIds)) {
      for (String pId : productIds) {
        // 校验抢购活动是否开始
        flashSalePromotionProductService.checkPromotionStart(pId);
      }
    }
    model.addAttribute("isCrossBorder", isCrossBorder);

    AddressVO address = fillAddress(model);

    Shop shop = (Shop) cartItemMap.keySet().toArray()[0];

    CartPromotionResultVO cartPromotionRet;
    UserSelectedProVO userSelectedPro = new UserSelectedProVO();
    if (form.getShopCoupons() != null) {

      Map<Shop, String> selShopCouponMap = new LinkedHashMap<Shop, String>();
      Map<String, String> shopCouponParam = form.getShopCoupons();
      Iterator<Entry<String, String>> iterator = shopCouponParam.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry<String, String> entry = iterator.next();
        String shopId = entry.getKey();
        String shopCouponId = entry.getValue();
        Shop eachShop = shopService.load(shopId);
        selShopCouponMap.put(eachShop, shopCouponId);
      }
      userSelectedPro.setSelShopCouponMap(selShopCouponMap);

      Map<Shop, String> selShopPromotionMap = new LinkedHashMap<Shop, String>();
      userSelectedPro.setSelShopPromotionMap(selShopPromotionMap);

      Map<CartItemVO, String> selCartItemPromotionMap = new LinkedHashMap<CartItemVO, String>();
      userSelectedPro.setSelCartItemPromotionMap(selCartItemPromotionMap);

      Map<CartItemVO, String> selCartItemCouponMap = new LinkedHashMap<CartItemVO, String>();
      userSelectedPro.setSelCartItemCouponMap(selCartItemCouponMap);
    }

    cartPromotionRet = promotionService
        .calculate4Cart(cartItemMap, getCurrentUser(), userSelectedPro, address,
            false, true, null, PricingMode.CONFIRM);

    //以cartItem的k，v来get
    if (StringUtils.isNotBlank(activityId)) {
      model.addAttribute("activityGrouponId", activityId);
    }
    if (StringUtils.isNotBlank(yundouProductId)) {
      model.addAttribute("yundouProductId", yundouProductId);
    }
    model.addAttribute("canUseCoupon", cartPromotionRet.getCanUseCoupon());
    model.addAttribute("prices", cartPromotionRet.getTotalPricingResult());
    model.addAttribute("shopPromotions", cartPromotionRet.getShopPromotions());
    model.addAttribute("shopCoupons", cartPromotionRet.getShopCoupons());
    model.addAttribute("cartItemCoupons", cartPromotionRet.getCartItemCoupons());
    model.addAttribute("cartItemPromotions", cartPromotionRet.getCartItemPromotions());
    model.addAttribute("shopPricingResult", cartPromotionRet.getShopPricingResult());
    model.addAttribute("skuIds", form.getSkuId());
    if (form.getShopRemarksCache() != null) {
      model.addAttribute("shopRemarksCache", form.getShopRemarksCache());
    }
    model.addAttribute("productId", form.getProductId());
    model.addAttribute("qty", form.getQty());

    // FIXME:to delete
    model.addAttribute("skuId", form.getSkuId() != null ? form.getSkuId().get(0) : "");
    model.addAttribute("shopId", form.getShopId());
    model.addAttribute("shop", shop);
    model.addAttribute("cartItems", cartItems);
    model.addAttribute("cartItem", cartItems.get(0));
    model.addAttribute("cartItemMap", cartItemMap);
    model.addAttribute("xiangquWebSite", xiangquWebSite);

    //放入协议支付的银行
    model.addAttribute("userAgreeBanks",
        outPayAgreementService.listBankByUserId(getCurrentUser().getId()));

    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      log.error(e.getMessage());
    }

    // 订单类型，区分b2b进货订单等
    // 目前此字段主要作用是进货功能进入cart/next页时，微信支付按钮名称变成确认
    String orderType = req.getParameter("orderType");
    model.addAttribute("orderType", orderType);
    model.addAttribute("promotionId", promotionId);

    return "cart/next";
  }


  public ResponseObject<CartPromotionResultVO> pricing(@Valid @ModelAttribute CartPricingForm form,
      HttpServletRequest req, Model model) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    int qty = form.getQty() == null ? 1 : form.getQty();
    Set<String> skuIds = new LinkedHashSet<String>(form.getSkuIds());

    List<String> errors = new ArrayList<String>();

    User user = getCurrentUser();
    String realShopId = user.getCurrentSellerShopId();
    CartFrom from = form.getFromCart() ? CartFrom.PRICING : CartFrom.NORMAL;

    if (skuIds != null && skuIds.size() > 0) {
      // 过滤相同的sku
      Set<String> skuIdSet = new LinkedHashSet<String>(skuIds);
      try {
        cartItems = cartService.checkout(skuIdSet, realShopId, null);
      } catch (BizException e) {
        errors.add(e.getMessage());
        cartItems = cartService.listCartItems(skuIdSet);
      }
    }

    // 可在购物车中实现修改购买规格
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    Map<Shop, List<CartItemVO>> cartItemMap = new LinkedHashMap<Shop, List<CartItemVO>>();
    for (CartItemVO item : cartItems) {
      skuMap.put(item.getSkuId(), item.getAmount());

      // 如果qty不为0,说明是订单直接购买，cartitem中的amount设置为qty的值，否则amount会默认为1
      if (qty > 1) {
        item.setAmount(qty);
      }

      Shop srcShop = item.getShop();
      List<CartItemVO> list = cartItemMap.get(srcShop);
      if (list == null) {
        list = new ArrayList<CartItemVO>();
        cartItemMap.put(srcShop, list);
      }
      list.add(item);
    }

    AddressVO addressVO = null;
    if (StringUtils.isNotEmpty(form.getAddressId())) {
      addressVO = addressService.loadUserAddress(form.getAddressId());
    }

    if (form.getShopCoupons() != null) {
      UserSelectedProVO userSelectedPro = new UserSelectedProVO();

      Map<Shop, String> selShopCouponMap = new LinkedHashMap<Shop, String>();
      Map<String, String> shopCouponParam = form.getShopCoupons();
      Iterator<Entry<String, String>> iterator = shopCouponParam.entrySet().iterator();
      while (iterator.hasNext()) {
        Map.Entry<String, String> entry = iterator.next();
        String shopId = entry.getKey();
        String shopCouponId = entry.getValue();
        Shop eachShop = shopService.load(shopId);
        selShopCouponMap.put(eachShop, shopCouponId);
      }
      userSelectedPro.setSelShopCouponMap(selShopCouponMap);

      Map<Shop, String> selShopPromotionMap = new LinkedHashMap<Shop, String>();
      userSelectedPro.setSelShopPromotionMap(selShopPromotionMap);

      Map<CartItemVO, String> selCartItemPromotionMap = new LinkedHashMap<CartItemVO, String>();
      userSelectedPro.setSelCartItemPromotionMap(selCartItemPromotionMap);

      Map<CartItemVO, String> selCartItemCouponMap = new LinkedHashMap<CartItemVO, String>();
      userSelectedPro.setSelCartItemCouponMap(selCartItemCouponMap);

      CartPromotionResultVO cartPromotionRetFull = promotionService
          .calculate4Cart(cartItemMap, getCurrentUser(), userSelectedPro, addressVO,
              false, true, null, PricingMode.CONFIRM);

      return new ResponseObject<CartPromotionResultVO>(cartPromotionRetFull);
    }

    //计算购物车费用
    CartPromotionResultVO cartPromotionRet = promotionService
        .calculate4Cart(cartItemMap, getCurrentUser(), addressVO,
            false);

    return new ResponseObject<CartPromotionResultVO>(cartPromotionRet);
  }

  public ResponseObject<CartPricingResultVO> pricingGroupByShop(
      @Valid @ModelAttribute CartPricingForm form, HttpServletRequest req, Model model) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();

    Set<String> skuIds = new LinkedHashSet<String>(form.getSkuIds());
    cartItems = cartService.checkout(skuIds);

    Map<String, Map<String, Integer>> groupByShopMap = new HashMap<String, Map<String, Integer>>();
    for (CartItemVO item : cartItems) {
      Map<String, Integer> map = groupByShopMap.get(item.getShopId());
      if (map == null) {
        map = new HashMap<String, Integer>();
        groupByShopMap.put(item.getShopId(), map);
      }
      //qty>0表示该请求是直接下单并且商品数量大于1
      if (form.getQty() > 0) {
        item.setAmount(form.getQty());
      }
      map.put(item.getSkuId(), item.getAmount());
    }

    CartPricingResultVO result = new CartPricingResultVO();
    for (Entry<String, Map<String, Integer>> entry : groupByShopMap.entrySet()) {
      PricingResultVO prices = pricingService.calculate(entry.getValue(), form.getZoneId(), null);
      result.putPrices(entry.getKey(), prices);
    }
    return new ResponseObject<CartPricingResultVO>(result);
  }

  protected List<BankItemsMap> trans4Show(List<PayBankWay> hotBanks, List<PayBankWay> allBanks) {
    List<BankItemsMap> result = new ArrayList<BankItemsMap>();
    if (hotBanks != null && hotBanks.size() > 0) {
      BankItemsMap map = new BankItemsMap();
      map.setKeyName("hot");
      map.setValueList(hotBanks);
      result.add(map);
    }

    List<String> keys = new ArrayList<String>();
    Map<String, List<PayBankWay>> temp = new HashMap<String, List<PayBankWay>>();
    if (allBanks != null && allBanks.size() > 0) {
      for (int i = 0; i < allBanks.size(); i++) {
        PayBankWay bean = allBanks.get(i);
        String key = bean.getStartWith();
        if (!keys.contains(key)) {
          keys.add(key);
          List<PayBankWay> list = new ArrayList<PayBankWay>();
          list.add(bean);
          temp.put(key, list);
        } else {
          temp.get(key).add(bean);
        }
      }
    }
    for (int i = 0; i < keys.size(); i++) {
      String key = keys.get(i);
      List<PayBankWay> list = temp.get(key);
      BankItemsMap map = new BankItemsMap();
      map.setKeyName(key);
      map.setValueList(list);
      result.add(map);
    }
    return result;
  }

}
