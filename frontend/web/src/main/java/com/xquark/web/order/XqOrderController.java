package com.xquark.web.order;

import com.alibaba.fastjson.JSONObject;
import com.xquark.biz.controller.ControllerHelper;
import com.xquark.dal.consts.PayUrlConfig;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.CashierItem;
import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderUserDevice;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentChannel;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.address.AddressService;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.XqProductBuyException;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.OrderUserDeviceService;
import com.xquark.service.outpay.OutPayAgreementService;
import com.xquark.service.pricing.CouponService;
import com.xquark.service.pricing.CouponVO;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.product.ProductSP;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.utils.MD5Util;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import com.xquark.web.BaseController;
import com.xquark.web.order.form.XqOrderSumbitForm;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestOperations;

@Controller
public abstract class XqOrderController extends BaseController {

  @Autowired
  private AddressService addressService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CouponService couponService;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderAddressService orderAddressService;

  @Autowired
  private OutPayAgreementService outPayAgreementService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ActivityService activityService;

  @Autowired
  private CartService cartService;

  @Autowired
  private UserService userService;

  @Autowired
  private PricingService pricingService;

  @Autowired
  private OrderUserDeviceService orderUserDeviceService;

  @Autowired
  private RestOperations restTemplate;

  @Autowired
  private PayUrlConfig payUrlConfig;

  @Value("${xiangqu.web.site}")
  private String xiangquWebSite;

  @Value("${site.web.host.name}")
  private String kkkdWebSite;

  public String submit(@ModelAttribute XqOrderSumbitForm form,
      @CookieValue(value = "union_id", defaultValue = "") String unionId,
      @CookieValue(value = "tu_id", defaultValue = "") String tuId,
      Errors errors, Device device, Model model, HttpServletRequest request,
      @RequestHeader(value = "referer", required = false) String referer) {
    // 数据校验
    ControllerHelper.checkException(errors);

    OrderAddress oa = new OrderAddress();
    Address address = addressService.load(form.getAddressId());
    BeanUtils.copyProperties(address, oa);

    User user = getCurrentUser();

    //判断是否有特价商品 及是否超过数量
    int prodSPSum = 0;  //本次特价商品购买数量 总和
    for (String skuId : form.getSkuIds()) {

      CartItem cartItem = null;
      String productId = null;

      if (form.getQty() > 0) {
        //直接下单，不走购物车流程
        Sku sku = productService.loadSku(skuId);
        productId = sku.getProductId();
      } else {
        //购物车下单
        cartItem = cartService.loadBySku(skuId);
        if (cartItem.getAmount() <= 0) {
          continue;
        }
        productId = cartItem.getProductId();
      }

      ProductSP productSP = activityService.checkProductSP(productId, user);
      if (productSP.getFirstActivity() == null) {
        continue;
      }

      if (form.getQty() > 0) {
        prodSPSum += form.getQty();
      } else {
        prodSPSum += cartItem.getAmount();
      }

      if (!productSP.onSale()) {
        throw new XqProductBuyException(GlobalErrorCode.INVALID_ARGUMENT, "哎呀，您来早啦，活动尚未开始");
      } else if (productSP.overLimit(prodSPSum)) {
        String errMsg = "嘿，一人只能购一份免费送商品，要把机会留给更多的朋友哦～";
        //errMsg += "buy=" + productSP.getBoughtAmount() + " new_buy=" + prodSPSum;

        throw new XqProductBuyException(GlobalErrorCode.INVALID_ARGUMENT, errMsg);
      } else if (user == null || user.getLoginname().startsWith("CID")) {
        throw new XqProductBuyException(GlobalErrorCode.INVALID_ARGUMENT, "商品用户登录后才能购买");
      }
    }

    // 下单的商品中包含有特殊商品
    if (prodSPSum > 0) {
      // 想去用户没有绑定
      if (isXqUserNotBindMobile(user.getExtUserId())) {
        // pc端设备
        if (device.isNormal()) {
          String backURL = xiangquWebSite + "/user/orders";
          return "redirect:" + xiangquWebSite + "/user/pc/bindPhone.html?callbackUrl=" + backURL;
        } else { // 移动端设备
          long t = System.currentTimeMillis();
          String signKey = "zlhzxqxl0604*@";
          signKey = MD5Util.MD5Encode(signKey, "UTF-8").toUpperCase();
          signKey = MD5Util.MD5Encode(signKey + t, "UTF-8").toUpperCase();
          String extUserId = user == null ? "" : user.getExtUserId();
          String backURL = referer == null ? kkkdWebSite + "/xiangqu/wap/cart" : referer;

          String szParam = "userId=" + extUserId + "&"
              + "t=" + t + "&"
              + "key=" + signKey + "&"
              + "callbackUrl=" + backURL;
          return "redirect:" + xiangquWebSite + "/user/wap/bindPhone.html?" + szParam;
        }
      }

//            else {
//                // pc端设备
//                if(device.isNormal()) {
//                    return "redirect:" + xiangquWebSite + "/app/download.html";
//                }
//            }
    }

    // FIXME 修改红包使用方案
    Map<String, Integer> map = new HashMap<String, Integer>();

    if (form.getQty() > 0) {
      //直接下单，不走购物车流程
      map.put(form.getSkuIds().get(0), form.getQty());
    } else {
      List<CartItemVO> cartItems = cartService.checkout(new HashSet<String>(form.getSkuIds()));
      for (CartItemVO item : cartItems) {
        map.put(item.getSkuId(), item.getAmount());
      }
    }
    PricingResultVO prices = pricingService.calculate(map, oa.getZoneId(), null);
    if (prices.getGoodsFee().doubleValue() <= 25) {
      List<Coupon> coupons = form.getCoupon();
      if (form.getCoupon() != null) {
        for (Coupon coupon : coupons) {
          if ("XQ.HONGBAO".equals(coupon.getActivityId())) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该红包只能在订单金额大于25元时使用");
          }
        }
      }
    }

    // TODO 将来重构第三方的时候，修改这里的逻辑
    // 想去单一接口下单，分账id代码里暂时固定写死
    if (StringUtils.isEmpty(unionId)) {
      User xqUser = userService.loadByLoginname("xiangqu");
      if (xqUser != null) {
        unionId = xqUser.getId();
      }
    }

    List<Order> orders = new ArrayList<Order>();
    if (form.getQty() > 0) {
      //直接下单，不走购物车流程
      List<String> list = new ArrayList<String>(form.getRemarks().values());
      orders.add(orderService
          .submitBySkuId(form.getSkuIds().get(0), null, oa, list.get(0), unionId, tuId, true,
              form.getQty()));
    } else {
      //购物车下单
      orders = orderService
          .submitBySkuIds(form.getSkuIds(), oa, form.getRemarks(), unionId, tuId, true);
    }

    String pNo = UniqueNoUtils.next(UniqueNoType.P);
    List<String> orderNos = new ArrayList<String>();
    BigDecimal totalFee = BigDecimal.ZERO;
    String productId = null, productName = null;
    UserPartnerType partnerType = null;
    String userId = null;
    for (Order order : orders) {
      orderNos.add(order.getOrderNo());
      totalFee = totalFee.add(order.getTotalFee());
      if (productId == null) {
        OrderVO vo = orderService.loadByOrderNo(order.getOrderNo());
        productId = vo.getOrderItems().get(0).getProductId();
        productName = vo.getOrderItems().get(0).getProductName();
        partnerType = vo.getPartnerType();
        userId = vo.getBuyerId();
      }

      Order updateOrder = new Order();
      updateOrder.setId(order.getId());
      updateOrder.setPayType(form.getPayType());
      if (orders.size() == 1) {
        updateOrder.setPayNo(pNo);
      }
      orderService.update(updateOrder);

      OrderUserDevice orderUserDevice = buildOrderUserDevice(request);
      orderUserDevice.setOrderId(order.getId());
      orderUserDevice.setUserId(user.getId());
      orderUserDeviceService.save(orderUserDevice);
    }
    String bizNos = StringUtils.join(orderNos, ",");

    List<CashierItem> items = null;
    //加入红包
    if (form.getCoupon() != null && form.getCoupon().size() > 0) {
      items = couponService.writeToCashier(form.getCoupon(), pNo, totalFee,
          userId, productId, productName, partnerType.toString(), bizNos);
      for (CashierItem item : items) {
        totalFee = totalFee.subtract(item.getAmount());
      }
    }

    if (items == null) {
      items = new ArrayList<CashierItem>();
    }

    //加入支付方式的收银台
    if (totalFee.compareTo(BigDecimal.ZERO) > 0) {
      items.addAll(createCashierChannel(form.getPayAgreementId(), pNo, form.getPayType(),
          totalFee, form.getCardType(), form.getBankCode(),
          userId, productId, productName, partnerType.toString(), bizNos));
    }

    cashierService.save(items, pNo, null);

    if (prodSPSum > 0) {
      // 想去用户绑定了手机号
      if (!isXqUserNotBindMobile(user.getExtUserId()) && device.isNormal()) {
        return "redirect:" + xiangquWebSite + "/app/download.html";
      }
    }

    String returnUrl =
        payUrlConfig.hostName() + "/cashier/" + pNo + "?bizType=order&batchBizNos=" + bizNos;
    return "redirect:" + returnUrl;
  }

  private OrderUserDevice buildOrderUserDevice(HttpServletRequest request) {
    OrderUserDevice device = new OrderUserDevice();
    device.setUserAgent(request.getHeader("User-Agent"));
    device.setClientIp(request.getRemoteAddr());
    device.setServerIp(request.getServerName());
    return device;
  }

  private boolean isXqUserNotBindMobile(String xqUserId) {
    String xqBindURL = xiangquWebSite + "/user/check/bindPhone?userId=" + xqUserId;
    String res = restTemplate.getForObject(xqBindURL, String.class, new Object[]{});
    JSONObject json = JSONObject.parseObject(res);
    return json.getIntValue("code") == 4001;
  }

  private List<CashierItem> createCashierChannel(String payAgreementId, String pNo,
      PaymentMode mode,
      BigDecimal totalFee, PaymentChannel paymentChannel, String bankCode, String userId,
      String productId,
      String productName, String partner, String bizNos) {
    List<CashierItem> items = new ArrayList<CashierItem>();

    if (totalFee.compareTo(BigDecimal.ZERO) == 1) {
      //记录支付
      CashierItem item = new CashierItem();
      item.setBizNo(pNo);
      item.setBankCode(bankCode);
      item.setAmount(totalFee);
      item.setUserId(userId);
      item.setProductId(productId);
      item.setProductName(productName);
      item.setPartner(partner);
      item.setBatchBizNos(bizNos);
      item.setAgreementId(StringUtils.isBlank(payAgreementId) ? null : payAgreementId);
      item.setPaymentMode(mode);
      item.setStatus(PaymentStatus.PENDING);
      item.setBankName(paymentChannel == null ? "" : paymentChannel.toString());

//			if(paymentChannel != null) {
//                item.setPaymentChannel(paymentChannel);
//            } else {
      item.setPaymentChannel(PaymentChannel.PLATFORM);
//            }
      items.add(item);
    }

    return items;
  }

  public String orderPay(@PathVariable("id") String orderId, Model model) {
    OrderVO order = orderService.loadVO(orderId);
    if (order == null) {
      return "order/404";
    }
    model.addAttribute("order", order);

    User user = null;
    try {
      user = getCurrentUser();
    } catch (Exception e) {
      e.printStackTrace();
    }

    model.addAttribute("authed", user != null && order.getBuyerId().equals(user.getId()));

    if (order.getStatus() == OrderStatus.SUBMITTED) {
      model.addAttribute("userAgreeBanks",
          outPayAgreementService.listBankByUserId(getCurrentUser().getId()));

      Map<String, List<CouponVO>> couponMap = couponService
          .listValidsWithExt(order.getTotalFee(), null, null);
      model.addAttribute("coupons", couponService.dealCoupons(couponMap));
    }

    model.addAttribute("address", orderAddressService.selectByOrderId(orderId));
    model.addAttribute("shop", shopService.load(order.getShopId()));
    model.addAttribute("xiangquWebSite", xiangquWebSite);

    return "pc/order-pay";
  }

}
