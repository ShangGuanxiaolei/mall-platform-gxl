package com.xquark.web.order.form;

import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.Order;
import com.xquark.dal.type.PromotionType;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 提交订单Form表单逻辑 扩展下单Form，新增支付方式
 *
 * @author odin
 */
public class OrderSumbitForm extends OrderForm {

  private static final String MOBILE_MESSAGE = "{valid.mobile.message}";

  /**
   * 订单id
   */
  private String orderId;

  /**
   * 订单类型，主订单，子订单
   */
  private String orderType;

  /**
   * 购买购物车中的一个或多个项目
   */
  @NotNull
  private List<String> skuIds;

  /**
   * 对店铺选用的优惠
   */
  private Map<String, String> shopCoupons;

  /**
   * 买家对卖家们的留言
   */
  private Map<String, String> shopRemarks;

  /**
   * 直接下单skuId
   */
  private String skuId;

  /**
   * 直接下单，购买数量
   */
  private int qty;

  /**
   * 直接下单的代销商品id
   */
  private String proLiteralId;

  /**
   * 收货人
   */
//    @NotBlank(message = "{consignee.notBlank.message}")
  private String consignee;

  /**
   * 手机号码
   */
  @Pattern(regexp = "(13\\d|14[57]|15[^4,\\D]|17[678]|18\\d)\\d{8}|170[059]\\d{7}", message = MOBILE_MESSAGE)
  private String phone;

  /**
   * 地区数字
   */
//    @NotBlank(message = "{zoneId.notBlank.message}")
  private String zoneId;

  /**
   * 详细地址
   */
//    @NotBlank(message = "{street.notBlank.message}")
  private String street;

  /**
   * 微信号
   */
  private String weixinId;

  /**
   * 地址id
   */
  private String addressId;

  /**
   * 店铺使用的优惠券
   */
  private Map<String, String> shopCouponIds;

  /**
   * 是否是担保交易
   */
  private Boolean danbao;

  private boolean useDeduction;

  private String hongbaoId = "";
  private String hongbaoAmount = "0";
  private String hongbaoName = "";

  private String activityGrouponId;
  private String yundouProductId;

  private String promotionId;

  private PromotionType promotionFrom;

  /**
   * 是否自提
   */
  private String ispickup;

  public String getIspickup() {
    return ispickup;
  }

  public void setIspickup(String ispickup) {
    this.ispickup = ispickup;
  }

  public String getActivityGrouponId() {
    return activityGrouponId;
  }

  public void setActivityGrouponId(String activityGrouponId) {
    this.activityGrouponId = activityGrouponId;
  }

  public String getYundouProductId() {
    return yundouProductId;
  }

  public void setYundouProductId(String yundouProductId) {
    this.yundouProductId = yundouProductId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getWeixinId() {
    return weixinId;
  }

  public void setWeixinId(String weixinId) {
    this.weixinId = weixinId;
  }

  public String getAddressId() {
    return addressId;
  }

  public void setAddressId(String addressId) {
    this.addressId = addressId;
  }

  public Boolean isDanbao() {
    return danbao;
  }

  public void setDanbao(Boolean danbao) {
    this.danbao = danbao;
  }

  public String getHongbaoId() {
    return hongbaoId;
  }

  public void setHongbaoId(String hongbaoId) {
    this.hongbaoId = hongbaoId;
  }

  public String getHongbaoAmount() {
    return hongbaoAmount;
  }

  public void setHongbaoAmount(String hongbaoAmount) {
    this.hongbaoAmount = hongbaoAmount;
  }

  public String getHongbaoName() {
    return hongbaoName;
  }

  public void setHongbaoName(String hongbaoName) {
    this.hongbaoName = hongbaoName;
  }

  public List<String> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<String> skuIds) {
    this.skuIds = skuIds;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  /**
   * 直接下单，购买数量
   */
  public int getQty() {
    return qty;
  }

  public void setQty(int qty) {
    this.qty = qty;
  }

  public String getProLiteralId() {
    return proLiteralId;
  }

  public void setProLiteralId(String proLiteralId) {
    this.proLiteralId = proLiteralId;
  }

  public Map<String, String> getShopRemarks() {
    return shopRemarks;
  }

  public void setShopRemarks(Map<String, String> shopRemarks) {
    this.shopRemarks = shopRemarks;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  /**
   * 判断当前提交的表单orderId是主订单还是子订单
   */
  public boolean isMainOrder() {
    if (StringUtils.isBlank(orderType) || MainOrder.MAIN_ORDER_TYPE.equals(orderType)) {
      return true;
    } else if (Order.SUB_ORDER_TYPE.equals(orderType)) {
      return false;
    } else {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "未知的订单类型：" + orderType);
    }
  }

  public boolean isDirectBuy() {
    return this.qty > 0 ? true : false;
  }

  public boolean isCartBuy() {
    return CollectionUtils.isNotEmpty(this.skuIds);
  }

  public Map<String, String> getShopCouponIds() {
    return shopCouponIds;
  }

  public void setShopCouponIds(Map<String, String> shopCouponIds) {
    this.shopCouponIds = shopCouponIds;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public boolean isUseDeduction() {
    return useDeduction;
  }

  public void setUseDeduction(boolean useDeduction) {
    this.useDeduction = useDeduction;
  }

  public PromotionType getPromotionFrom() {
    return promotionFrom;
  }

  public void setPromotionFrom(PromotionType promotionFrom) {
    this.promotionFrom = promotionFrom;
  }
}