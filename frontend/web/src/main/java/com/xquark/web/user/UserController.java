package com.xquark.web.user;

import com.xquark.dal.mapper.UserPartnerMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.OrderRefundStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderRefundActionType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionGrouponUserVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.member.MemberService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.msg.vo.UserMessageVO;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.systemRegion.SystemRegionService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.utils.StringUtil;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.address.AddressForm;
import com.xquark.web.order.OrderRefundRequestForm;
import com.xquark.web.vo.Json;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by quguangming on 16/3/2.
 */
@Controller
public class UserController extends BaseController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private MemberService memberService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private UserPartnerMapper userPartnerMapper;

  @Autowired
  private OrderRefundService orderRefundService;

  @Autowired
  private UserService userService;

  @Autowired
  private PromotionGrouponService promotionGrouponService;

  @Autowired
  private CartService cartService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private SystemRegionService systemRegionService;

  //去中心化状态
  private boolean isDecentralized = true;

  @RequestMapping(value = "/user/userCenter/{shopId}")
  public String view(@PathVariable("shopId") String shopId, Model model, HttpServletRequest req) {
    // 获取店铺
    Shop shop = shopService.load(shopId);
    if (shop == null || shop.getArchive() == Boolean.TRUE) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }
    String ownerShopId = shopTreeService.selectRootShopByShopId(shop.getId()).getRootShopId();
    UserPartner userPartner = userPartnerMapper
        .selectActiveUserPartnersByUserIdAndShopId(shop.getOwnerId(), ownerShopId);
    model.addAttribute("userPartner", userPartner);

    User user = getCurrentUser();
    model.addAttribute("shop", shop);
    model.addAttribute("user", user);

    // 购物车内商品总数
    Integer cartCount = 0;
    String userId = this.getCurrentUser().getId();
    cartCount = cartService.countByShopId(userId, shopId);
    model.addAttribute("cartCount", cartCount);

    return "b2b/personalCenter/personalCenter";
  }

  /**
   * 检测用户是否注册过
   */
  @ResponseBody
  @RequestMapping(value = "user/checkRegister")
  public Json checkRegister(@RequestParam("memberPhone") String memberPhone, Model model,
      HttpServletRequest req) {

    Json json = new Json();

    boolean isRegister = memberService.checkRegister(memberPhone);

    if (isRegister) {
      json.setRc(Json.RC_SUCCESS);
      json.setMsg(memberPhone);
    } else {
      json.setRc(Json.RC_FAILURE);
    }
    return json;
  }


  /**
   * 验证用户是否正常登录
   */
  @ResponseBody
  @RequestMapping(value = "user/checkSignin")
  public Json checkSignin(Model model, HttpServletRequest req) {

    Json json = new Json();
    boolean isLogin = memberService.checkSignature();

    if (isLogin) {
      json.setRc(Json.RC_SUCCESS);
    } else {
      json.setRc(Json.RC_FAILURE);
    }
    return json;
  }

  /**
   * 修改默认地址
   *
   * @param addressId 地址id
   */
  @ResponseBody
  @RequestMapping(value = "user/changeDefaultAddress")
  public Json changeDefaultAddress(@RequestParam("addressId") String addressId, Model model,
      HttpServletRequest req) {

    Json json = new Json();
    boolean isSuccess = addressService.asDefault(addressId);
    if (isSuccess) {
      json.setRc(Json.RC_SUCCESS);
    } else {
      json.setRc(Json.RC_FAILURE);
    }
    return json;
  }

  /**
   * 修改默认地址
   *
   * @param addressId 地址id
   */
  @ResponseBody
  @RequestMapping(value = "user/editAddress")
  public Map<String, Object> editAddress(@RequestParam("addressId") String addressId, Model model,
      HttpServletRequest req) {

    Map<String, Object> data = new HashMap<String, Object>();

    Address address = null;
    List<SystemRegion> parents = null;
    if (StringUtils.isNotEmpty(addressId)) {
      address = addressService.load(addressId);
      parents = systemRegionService.listParents(address.getZoneId());
    }
    data.put("address", address);

    List<SystemRegion> provinceList = null;
    if (parents != null && parents.size() > 1) {
      SystemRegion province = null;
      province = parents.get(1);
      provinceList = systemRegionService.listSiblings(
          String.valueOf(province.getId()));
      data.put("province", province);
    } else {
      provinceList = systemRegionService.listChildren("1");
    }
    data.put("provinceList", provinceList);

    if (parents != null && parents.size() > 2) {
      SystemRegion city = null;
      List<SystemRegion> cityList = null;
      city = parents.get(2);
      cityList = systemRegionService.listSiblings(String.valueOf(city.getId()));
      data.put("city", city);
      data.put("cityList", cityList);
    }

    if (parents != null && parents.size() > 3) {
      SystemRegion district = null;
      List<SystemRegion> districtList = null;
      district = parents.get(3);
      districtList = systemRegionService
          .listSiblings(String.valueOf(district.getId()));
      data.put("district", district);
      data.put("districtList", districtList);
    }

    return data;
  }

  /**
   * 获取地址列表
   */
  @ResponseBody
  @RequestMapping(value = "user/listAddress")
  public ResponseObject<List> listAddress(Model model, HttpServletRequest req) {

    List<AddressVO> addresses = addressService.listUserAddressesVo();
    //将默认地址排在第一个
    if (!CollectionUtils.isEmpty(addresses)) {  //获取常用的默认地址
      for (AddressVO addressVo : addresses) {
        if (addressVo.getIsDefault()) {
          addresses.remove(addressVo);
          addresses.add(0, addressVo);
          break;
        }
      }
    }
    return new ResponseObject<List>(addresses);
  }

  /**
   * 添加新地址
   */
  @ResponseBody
  @RequestMapping(value = "user/saveAddress")
  public ResponseObject<AddressVO> addAddress(@Valid @ModelAttribute AddressForm form) {
    Address address = new Address();
    BeanUtils.copyProperties(form, address);
    address.setCommon(false);
    return new ResponseObject<AddressVO>(addressService.saveUserAddress(address, true));
  }


  /**
   * 我的拼团
   *
   * @param status 全部 all 进行中 todoing  已结束 toend
   */
  @RequestMapping(value = "user/groupon/{shopId}")
  public String groupon(@PathVariable("shopId") String shopId,
      @RequestParam(value = "status", required = false) String status, Model model,
      HttpServletRequest req) {
    shopId = this.getShopId(shopId);
    // 获取店铺
    Shop shop = shopService.load(shopId);
    if (shop == null || shop.getArchive() == Boolean.TRUE) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }
    if (StringUtils.isEmpty(status)) {
      status = "all";
    }
    if (status != null) {
      User user = getCurrentUser();
      String userId = user.getId();

      String orderDesc = "全部";
      if ("tosuccess".equals(status)) {
        orderDesc = "已成团";
      }
      if ("toend".equals(status)) {
        orderDesc = "未成团";
      }
      List<PromotionGrouponUserVO> orders = promotionGrouponService
          .listUserPromotion(userId, status, null);

      for (PromotionGrouponUserVO userVO : orders) {
        Date paidDate = userVO.getCreatedAt();
        Date now = new Date();
        String dateString = dateDiff(now, paidDate);
        userVO.setDayDesc(dateString);
      }
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("orders", orders);
      model.addAttribute("shop", shop);
    }
    return "b2b/groupPurchase/groupPurchase";
  }

  public String dateDiff(Date startTime, Date endTime) {
    String dateString = "";
    try {
      //按照传入的格式生成一个simpledateformate对象
      long nd = 1000 * 24 * 60 * 60;//一天的毫秒数
      long nh = 1000 * 60 * 60;//一小时的毫秒数
      long nm = 1000 * 60;//一分钟的毫秒数
      long ns = 1000;//一秒钟的毫秒数long diff;try {
      //获得两个时间的毫秒时间差异
      long diff = startTime.getTime() - endTime.getTime();
      long day = diff / nd;//计算差多少天
      long hour = diff % nd / nh;//计算差多少小时
      long min = diff % nd % nh / nm;//计算差多少分钟
      long sec = diff % nd % nh % nm / ns;//计算差多少秒//输出结果
      dateString = day + "天" + hour + "小时前";
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "日期时间计算出错");
    }
    return dateString;
  }


  /**
   * 我的订单
   *
   * @param status 全部 all 待付款 topay  待发货 tosend  待收货 send  已完成 finish 退货 refund
   */
  @RequestMapping(value = "user/order/{shopId}")
  public String order(@RequestParam("status") String status, @PathVariable("shopId") String shopId,
      Model model, HttpServletRequest req, Pageable pager) {

    shopId = this.getShopId(shopId);
    Map<String, Object> params = new HashMap<String, Object>();

    if (status != null) {

      User user = getCurrentUser();
      String userId = user.getId();
      String spId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

      String orderDesc = "全部";
      String[] statusArr = null;
      if ("topay".equals(status)) {
        statusArr = new String[]{OrderStatus.SUBMITTED.name()};
        orderDesc = "待付款的";
      }

      if ("tosend".equals(status)) {
        statusArr = new String[]{OrderStatus.PAID.name()};
        orderDesc = "待发货的";
      }

      if ("send".equals(status)) {
        statusArr = new String[]{OrderStatus.SHIPPED.name()};
        orderDesc = "待收货的";
      }

      if ("finish".equals(status)) {
        statusArr = new String[]{OrderStatus.CANCELLED.name(), OrderStatus.SUCCESS.name(),
            OrderStatus.CLOSED.name()};
        orderDesc = "已完成的";
      }

      if ("refund".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        orderDesc = "待退款的";
      }

      List<OrderVO> orders = memberService
          .getOrderListByOrderStatus(statusArr, userId, spId, pager, params);
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("shopId", shopId);
      model.addAttribute("orders", orders);
    }

    return "user/order";
  }

  /**
   * 我的退款/售后订单
   */
  @RequestMapping(value = "user/orderRefund/{shopId}")
  public String orderRefund(@RequestParam("status") String status,
      @PathVariable("shopId") String shopId, Model model, HttpServletRequest req, Pageable pager) {

    shopId = this.getShopId(shopId);
    Map<String, Object> params = new HashMap<String, Object>();

    if (status != null) {

      User user = getCurrentUser();
      String userId = user.getId();
      String spId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

      String orderDesc = "全部";
      String[] statusArr = null;

      if ("all".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name(), OrderStatus.CLOSED.name()};
        orderDesc = "全部";
      }

      if ("toclose".equals(status)) {
        statusArr = new String[]{OrderStatus.CLOSED.name()};
        orderDesc = "已完成";
      }

      if ("toreturn".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        params.put("buyerRequire", "2");
        orderDesc = "待寄回退货";
      }

      if ("torefund".equals(status)) {
        params.put("buyerRequire", "1");
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        orderDesc = "待退款";
      }

      List<OrderVO> orders = memberService
          .getOrderListByOrderStatus(statusArr, userId, spId, pager, params);
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("shopId", shopId);
      model.addAttribute("orders", orders);
    }

    return "user/orderRefund";
  }

  /**
   * 我的待处理的订单
   *
   * @param status 全部 all 待付款 topay  待发货 tosend  待收货 send  已完成 finish 退货 refund
   */
  @RequestMapping(value = "user/orderTodo/{shopId}")
  public String orderTodo(@RequestParam("status") String status,
      @PathVariable("shopId") String shopId, Model model, HttpServletRequest req, Pageable pager) {
    shopId = this.getShopId(shopId);
    Map<String, Object> params = new HashMap<String, Object>();
    if (status != null) {

      User user = getCurrentUser();
      String userId = user.getId();
      String spId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

      String orderDesc = "全部";
      String[] statusArr = null;
      if ("topay".equals(status)) {
        statusArr = new String[]{OrderStatus.SUBMITTED.name()};
        orderDesc = "待付款的";
      }

      if ("tosend".equals(status)) {
        statusArr = new String[]{OrderStatus.PAID.name()};
        orderDesc = "待发货的";
      }

      if ("send".equals(status)) {
        statusArr = new String[]{OrderStatus.SHIPPED.name()};
        orderDesc = "待收货的";
      }

      if ("refund".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
      }

      List<OrderVO> orders = memberService
          .getOrderListByOrderStatus(statusArr, userId, spId, pager, params);
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("shopId", shopId);
      model.addAttribute("orders", orders);
    }

    return "user/orderTodo";
  }

  /**
   * 我的卖出的订单
   *
   * @param status 全部 all 待付款 topay  待发货 tosend  待收货 send  已完成 finish 退货 refund
   */
  @RequestMapping(value = "user/order/seller/{shopId}")
  public String sellerOrder(@RequestParam("status") String status,
      @PathVariable("shopId") String shopId, Model model, HttpServletRequest req, Pageable pager) {

    shopId = this.getShopId(shopId);
    Map<String, Object> params = new HashMap<String, Object>();
    if (status != null) {

      User user = getCurrentUser();
      String userId = user.getId();
      String sellerShopId = user.getShopId();
      String spId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

      String orderDesc = "全部";
      String[] statusArr = null;
      if ("topay".equals(status)) {
        statusArr = new String[]{OrderStatus.SUBMITTED.name()};
        orderDesc = "待付款的";
      }

      if ("tosend".equals(status)) {
        statusArr = new String[]{OrderStatus.PAID.name()};
        orderDesc = "待发货的";
      }

      if ("send".equals(status)) {
        statusArr = new String[]{OrderStatus.SHIPPED.name()};
        orderDesc = "待收货的";
      }

      if ("finish".equals(status)) {
        statusArr = new String[]{OrderStatus.CANCELLED.name(), OrderStatus.SUCCESS.name(),
            OrderStatus.CLOSED.name()};
        orderDesc = "已完成的";
      }

      if ("refund".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
      }

      List<OrderVO> orders = memberService
          .getSellerOrderListByOrderStatus(statusArr, sellerShopId, spId, pager, params);
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("shopId", shopId);
      model.addAttribute("orders", orders);
    }

    return "user/orderSeller";
  }

  /**
   * 我的卖出的售后订单
   */
  @RequestMapping(value = "user/order/seller/refund/{shopId}")
  public String sellerOrderRefund(@RequestParam("status") String status,
      @PathVariable("shopId") String shopId, Model model, HttpServletRequest req, Pageable pager) {

    shopId = this.getShopId(shopId);
    Map<String, Object> params = new HashMap<String, Object>();
    if (status != null) {

      User user = getCurrentUser();
      String userId = user.getId();
      String sellerShopId = user.getShopId();
      String spId = shopTreeService.selectRootShopByShopId(shopId).getRootShopId();

      String orderDesc = "全部";
      String[] statusArr = null;

      if ("all".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name(), OrderStatus.CLOSED.name()};
        orderDesc = "全部";
      }

      if ("toclose".equals(status)) {
        statusArr = new String[]{OrderStatus.CLOSED.name()};
        orderDesc = "已完成";
      }

      if ("toreturn".equals(status)) {
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        params.put("buyerRequire", "2");
        orderDesc = "待寄回退货";
      }

      if ("torefund".equals(status)) {
        params.put("buyerRequire", "1");
        statusArr = new String[]{OrderStatus.REFUNDING.name()};
        orderDesc = "待退款";
      }

      List<OrderVO> orders = memberService
          .getSellerOrderListByOrderStatus(statusArr, sellerShopId, spId, pager, params);
      model.addAttribute("orderDesc", orderDesc);
      model.addAttribute("status", status);
      model.addAttribute("shopId", shopId);
      model.addAttribute("orders", orders);
    }

    return "user/orderSellerRefund";
  }

  /**
   * 买家取消订单
   */
  @RequestMapping(value = "/user/order/{id}/cancel")
  @ResponseBody
  public ResponseObject<String> cancel(@PathVariable("id") String orderId, Model model) {
    orderService.cancel(orderId);
    return new ResponseObject<String>("cancel");
  }

  /**
   * 买家提交退款申请
   */
  @RequestMapping(value = "/user/order/requestrefund")
  @ResponseBody
  public Json requestrefund(OrderRefundRequestForm form, Model model) {
    Json json = new Json();
    json.setRc(Json.RC_SUCCESS);
    json.setMsg("操作成功");

    // 获取订单相关信息
    OrderVO order = orderService.loadVO(form.getOrderId());
    form.setRefundFee(order.getTotalFee().toString());

    if (form.getRefundMemo() != null && form.getRefundMemo().length() > 100) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("说明的字数不能大于100个字！");
      return json;
    }

    OrderRefund orderRefund = new OrderRefund();
    BeanUtils.copyProperties(form, orderRefund);

    if (StringUtil.isNotNull(orderRefund.getRefundImg())) {
      List<String> refundImgList = Arrays.asList(orderRefund.getRefundImg().split(","));
      List<OrderRefundImg> list = new ArrayList<OrderRefundImg>();
      for (String image : refundImgList) {
        OrderRefundImg orderRefundImg = new OrderRefundImg();
        orderRefundImg.setImage(image);
        list.add(orderRefundImg);
      }
      orderRefund.setRefundImgList(list);
    }

    try {
      BigDecimal refundFee = new BigDecimal(form.getRefundFee()).setScale(2, BigDecimal.ROUND_DOWN);
      orderRefund.setRefundFee(refundFee);
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("退款金额格式不正确！");
      return json;
    }

    if (StringUtils.isBlank(orderRefund.getId())) {
      if (orderRefundService.listByOrderId(orderRefund.getOrderId()).size() > 0) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("申请已提交，请勿重复提交");
        return json;
      }

      orderRefund.setApplyNo(UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.R));
      orderRefund.setOrderStatus(order.getStatus());
      orderRefund.setStatus(OrderRefundStatus.SUBMITTED);
      OrderItem orderItem = order.getOrderItems().get(0);
      orderRefund.setProductName(orderItem.getProductName());
      orderRefund.setImg(orderItem.getProductImg());
      orderRefundService.insert(orderRefund);
      orderService.requestRefund(order.getId());

      //TODO须移出
      //发送消息
      //orderMessageService.sendSellerSMSOrderRefundRequest(order);

      //pushMessage(PushMsgId.REFUND_REQUEST.getId(), order.getSellerId(), Long.toString(new Date().getTime()), PushMsgType.MSG_REFUND_REQUEST_S.getValue(), order.getId(), order.getOrderNo());

    }

    return json;
  }

  /**
   * 买家提交取消退款申请
   */
  @RequestMapping(value = "/user/order/{id}/cancelrefund")
  @ResponseBody
  public Json cancelrefund(@PathVariable("id") String orderId, Model model) {
    Json json = new Json();
    List<OrderRefund> orderRefunds = orderRefundService.listByOrderId(orderId);
    if (orderRefunds != null && orderRefunds.size() > 0) {
      OrderRefund orderRefund = orderRefunds.get(0);
      orderRefundService.execute(OrderRefundActionType.CANCEL, orderRefund, null);
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("操作成功");
    } else {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("操作失败");
    }
    return json;
  }

  /**
   * 我的收货地址
   */
  @RequestMapping(value = "user/myAddress")
  public String myAddress(Model model, HttpServletRequest req) {
    List<AddressVO> addresses = addressService.listUserAddressesVo();
    model.addAttribute("addresses", addresses);
    return "user/myAddress";
  }

  /**
   * 修改我的收货地址
   */
  @RequestMapping(value = "user/editMyAddress")
  public String editMyAddress(@RequestParam(value = "addressId", required = false) String addressId,
      Model model, HttpServletRequest req) {

    Address address = null;
    List<Zone> parents = null;
    if (StringUtils.isNotEmpty(addressId)) {
      address = addressService.load(addressId);
      parents = zoneService.listParents(address.getZoneId());
    }
    model.addAttribute("address", address);

    List<Zone> provinceList = null;
    if (parents != null && parents.size() > 1) {
      Zone province = null;
      province = parents.get(1);
      provinceList = zoneService.listSiblings(province.getId());
      model.addAttribute("province", province);
    } else {
      provinceList = zoneService.listChildren("1");
    }
    model.addAttribute("provinceList", provinceList);

    if (parents != null && parents.size() > 2) {
      Zone city = null;
      List<Zone> cityList = null;
      city = parents.get(2);
      cityList = zoneService.listSiblings(city.getId());
      model.addAttribute("city", city);
      model.addAttribute("cityList", cityList);
    }

    if (parents != null && parents.size() > 3) {
      Zone district = null;
      List<Zone> districtList = null;
      district = parents.get(3);
      districtList = zoneService.listSiblings(district.getId());
      model.addAttribute("district", district);
      model.addAttribute("districtList", districtList);
    }

    return "user/editAddress";
  }

  /**
   * 在用户中心添加新地址
   */
  @RequestMapping(value = "user/saveAddressInUserCenter")
  public String saveAddressInUserCenter(@Valid @ModelAttribute AddressForm form) {
    Address address = new Address();
    BeanUtils.copyProperties(form, address);
    address.setCommon(false);
    addressService.saveUserAddress(address, true);
    return "redirect:/user/myAddress";
  }

  /**
   * 用户个人信息
   */
  @ResponseBody
  @RequestMapping(value = "user/userInfo")
  public void userinfo(Model model, HttpServletRequest req) {
    User user = getCurrentUser();
    model.addAttribute("user", user);
  }

  /**
   * 退款退货选择界面
   */
  @RequestMapping("/user/refundService")
  public String refundService(HttpServletRequest req, HttpServletResponse resp, Model model,
      @RequestParam String orderId) {
    OrderVO vo = orderService.loadVO(orderId);
    model.addAttribute("orderVO", vo);
    return "user/refundService";
  }

  /**
   * 退款退货界面
   */
  @RequestMapping("/user/checkRefund/{orderId}")
  public String checkRefund(@PathVariable("orderId") String orderId, HttpServletRequest req,
      HttpServletResponse resp, Model model, @RequestParam String buyerRequire) {
    OrderVO vo = orderService.loadVO(orderId);
    model.addAttribute("orderVO", vo);
    if ("1".equals(buyerRequire)) {
      return "user/requestRefund";
    } else {
      return "user/requestReturn";
    }
  }

  /**
   * 我的消息
   */
  @RequestMapping("/user/myMessage")
  public String myMessage(HttpServletRequest req, HttpServletResponse resp, Model model) {
    String userId = getCurrentUser().getId();
    List<UserMessageVO> messageVOs = messageService.loadAllMessages();
    model.addAttribute("messageVOs", messageVOs);
    // 更新用户消息为已读
    messageService.updateUserAllRead(userId);
    return "user/message";
  }

  /**
   * 我的个人信息
   */
  @RequestMapping(value = "user/info")
  public String info(Model model, HttpServletRequest req) {
    String userId = getCurrentUser().getId();
    User user = userService.load(userId);
    model.addAttribute("user", user);
    return "user/userInfo";
  }

  /**
   * 更新昵称界面
   */
  @RequestMapping(value = "user/updateName")
  public String updateName(Model model, HttpServletRequest req) {
    String userId = getCurrentUser().getId();
    User user = userService.load(userId);
    model.addAttribute("user", user);
    return "user/userNameUpdate";
  }

  /**
   * 更新头像界面
   */
  @RequestMapping(value = "user/updateAvatar")
  public String updateAvatar(Model model, HttpServletRequest req) {
    String userId = getCurrentUser().getId();
    User user = userService.load(userId);
    model.addAttribute("user", user);
    return "user/userAvatarUpdate";
  }

}
