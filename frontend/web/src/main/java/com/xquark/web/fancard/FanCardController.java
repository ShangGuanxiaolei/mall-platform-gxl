package com.xquark.web.fancard;

import com.xquark.dal.IUser;
import com.xquark.dal.model.User;
import com.xquark.service.fancard.FanCardService;
import com.xquark.service.user.UserService;
import com.xquark.web.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by wangxinhua on 17-11-7. DESC:
 */
@Controller
@RequestMapping("/fancard")
public class FanCardController extends BaseController {

  private FanCardService fanCardService;

  private UserService userService;

  @Autowired
  public void setFanCardService(FanCardService fanCardService) {
    this.fanCardService = fanCardService;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @RequestMapping("/center")
  public String fanCenter() {
    return "fancard/center";
  }

  @RequestMapping("/menu")
  public String fanMenu(Model model) {
    String code = this.getCode();
    if (StringUtils.isNotBlank(code)) {
      model.addAttribute("code", code);
    }
    return "fancard/menu";
  }

  @RequestMapping("/apply")
  public String fanApply(Model model) {
    IUser user = getCurrentUser();
    String userId = user.getId();
    User dbUser = userService.load(userId);

    String code = dbUser.getFanCardNo();
    if (StringUtils.isNotBlank(code)) {
      model.addAttribute("code", code);
    }
    return "fancard/apply";
  }

  private String getCode() {
    IUser user = getCurrentUser();
    String userId = user.getId();
    User dbUser = userService.load(userId);
    return dbUser.getFanCardNo();
  }

}
