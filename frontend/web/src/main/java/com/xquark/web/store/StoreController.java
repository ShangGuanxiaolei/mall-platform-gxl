package com.xquark.web.store;

import com.xquark.service.shop.ShopService;
import com.xquark.service.store.StoreService;
import com.xquark.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StoreController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private StoreService storeService;

  /**
   * 线下门店二维码扫描进入申请页面
   */
  @RequestMapping(value = "/store/applyMember")
  public String view(Model model, HttpServletRequest req) {
    return "b2c/teamApply";
  }

}
