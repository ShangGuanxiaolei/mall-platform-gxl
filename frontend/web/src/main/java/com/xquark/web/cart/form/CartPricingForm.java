package com.xquark.web.cart.form;

import java.util.List;
import java.util.Map;

import com.xquark.dal.model.Shop;
import com.xquark.service.cart.vo.CartItemVO;
import org.hibernate.validator.constraints.NotEmpty;

public class CartPricingForm {

  // 商品skuId数组
  @NotEmpty
  private List<String> skuIds;

  private Map<String, String> shopCoupons;

  // 收件地址区域
  private String zoneId;
  // 使用的优惠券
  private String couponId;

  private Integer qty;

  private String addressId;

  private boolean fromCart;

  public String getAddressId() {
    return addressId;
  }

  public void setAddressId(String addressId) {
    this.addressId = addressId;
  }

  public String getZoneId() {
    return zoneId;
  }

  public void setZoneId(String zoneId) {
    this.zoneId = zoneId;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public List<String> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<String> skuIds) {
    this.skuIds = skuIds;
  }

  public Map<String, String> getShopCoupons() {
    return shopCoupons;
  }

  public void setShopCoupons(Map<String, String> shopCoupons) {
    this.shopCoupons = shopCoupons;
  }

  public Integer getQty() {
    return qty;
  }

  public void setQty(Integer qty) {
    this.qty = qty;
  }

  public boolean getFromCart() {
    return fromCart;
  }

  public void setFromCart(boolean fromCart) {
    this.fromCart = fromCart;
  }
}
