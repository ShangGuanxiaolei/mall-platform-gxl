package com.xquark.web.feedback;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.mapper.WeixinJSTicketMapper;
import com.xquark.dal.model.WeixinJSTicket;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.service.activity.ActivityService;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.product.ProductService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.web.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
public class FeedbackController extends BaseController {

  @Autowired
  private WeixinJSTicketMapper weixinJSTicketMapper;

  @Autowired
  private WechatAppConfigService wechatAppConfigService;

  @RequestMapping("/sales")
  public String sales(Model model, HttpServletRequest req) throws Exception {
    /**String openId = this.getCurrentUser().getLoginname();
     String userId = this.getCurrentUser().getId();
     model.addAttribute("openId", openId);
     model.addAttribute("userId", userId);**/
    String url = req.getRequestURL().toString();
    String param = req.getQueryString();
    if (StringUtils.isNotEmpty(param)) {
      url = url + "?" + param;
    }
    // 微信分享需要先获取相关验证
    WechatAppConfig appConfig = wechatAppConfigService.load("aj6wd1mm");
    String ticket = null;
    if (appConfig != null) {
      ticket = appConfig.getJsapiTicket();
    }
    if (ticket != null) {
      String timestamp = Long.toString(new Date().getTime() / 1000);
      String noncestr = CommonUtil.CreateNoncestr();
      String signValue =
          "jsapi_ticket=" + ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url="
              + url;
      //这个签名.主要是给加载微信js使用.
      String signature = SHA1Util.Sha1((signValue));

      model.addAttribute("appId", appConfig.getAppId());
      model.addAttribute("timestamp", timestamp);
      model.addAttribute("noncestr", noncestr);
      model.addAttribute("signature", signature);
      model.addAttribute("url", url);
    }

    return "sales";
  }

  @RequestMapping("/sales404")
  public String sales404(Model model) throws Exception {
    return "sales404";
  }
}
