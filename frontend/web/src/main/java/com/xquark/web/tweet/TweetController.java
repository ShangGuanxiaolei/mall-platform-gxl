package com.xquark.web.tweet;

import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shop.ShopStyleVO;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.web.ResponseObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TweetController {

  @Autowired
  private ShopService shopService;

  @RequestMapping(value = "/app/tweetList")
  public String apptweetList(Model model, HttpServletRequest req) {
    return "b2b/app/find/find";
  }


}
