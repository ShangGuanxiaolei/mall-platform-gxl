package com.xquark.web.diamond.vo;

import com.xquark.dal.model.Category;
import com.xquark.dal.vo.PromotionDiamondProductVO;

import java.util.List;

/**
 * Created by root on 16-10-26.
 */
public class DiamondAppVO {

  private Category category;
  private List<PromotionDiamondProductVO> products;

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public List<PromotionDiamondProductVO> getProducts() {
    return products;
  }

  public void setProducts(List<PromotionDiamondProductVO> products) {
    this.products = products;
  }
}
