package com.xquark.web.order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xquark.interceptor.Token;
import com.xquark.web.order.form.XqOrderSumbitForm;

@Controller
@RequestMapping("/xiangqu/wap")
public class XqWapOrderController extends XqOrderController {

  @RequestMapping(value = "/order/submit")
  @Token(remove = true)
  public String submit(@ModelAttribute XqOrderSumbitForm form,
      @CookieValue(value = "union_id", defaultValue = "") String unionId,
      @CookieValue(value = "tu_id", defaultValue = "") String tuId,
      Errors errors, Device device, Model model, HttpServletRequest request,
      @RequestHeader(value = "referer", required = false) String referer) {
    return super.submit(form, unionId, tuId, errors, device, model, request, referer);
  }

  @RequestMapping(value = "/order/{id}")
  public String orderPay(@PathVariable("id") String orderId, Model model,
      HttpServletRequest request, HttpServletResponse response) {
//    String redirect = redirectDomain(request, response);
//    if (StringUtils.isNotEmpty(redirect)) {
//      return redirect;
//    }

    model.addAttribute("message", "");
    return "catalog/rusherror";
  }

}
