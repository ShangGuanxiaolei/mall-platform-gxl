package com.xquark.web.pintuan;

/**
 * @author liuyandong
 * @date 2018/9/13 19:45
 */

import com.alibaba.fastjson.JSONObject;
import com.wordnik.swagger.annotations.ApiOperation;
import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.IUser;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.page.PageHelper;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.vo.*;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.pintuan.PromotionGoodsDetailService;
import com.xquark.service.pintuan.PromotionGoodsService;
import com.xquark.service.pintuan.vo.PgSkuDetailVo;
import com.xquark.service.product.ProductDescService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductImageVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.promotion.impl.FlashSalePromotionProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.sku.SkuAttributeService;
import com.xquark.service.user.UserService;
import com.xquark.web.catalog.FragmentAndDescController;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 拼团
 */
@Deprecated
@Controller
public class PintuanController extends FragmentAndDescController {
    @Autowired
    private UserService userService;
    @Autowired
    protected FragmentImageService fragmentImageService;
    @Autowired
    private PromotionGoodsDetailService promotionGoodsDetailService;
    @Autowired
    protected ProductService productService;
    @Autowired
    protected ProductDescService productDescService;
    @Autowired
    private SkuAttributeService skuAttributeService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    protected ShopService shopService;
    @Autowired
    private UrlHelper urlHelper;
    @Autowired
    private PromotionCouponService couponService;
    @Autowired
    private FlashSalePromotionProductService flashSaleService;
    @Autowired
    private PromotionGoodsService promotionGoodsService;

    @RequestMapping(value = "/p/promotionList/ongoing/{cpid}")
    public String getOngoingPromotionList(@PathVariable String cpid, @RequestParam(value = "page", defaultValue = "1") Integer page, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, Model model) {
        //查询拼团活动列表
        PageHelper<PromotionGoodsVO> promotions = promotionGoodsService.selectBeginningPromotionGoods(null, cpid, page, pageSize);
        List<PromotionGoodsExVO> goods = new ArrayList<PromotionGoodsExVO>(promotions.getResult().size());
        PromotionGoodsExVO good = null;
        for (PromotionGoodsVO a : promotions.getResult()) {
            good = new PromotionGoodsExVO();
            BeanUtils.copyProperties(a, good);
            good.setTimeFromBegin(a.getEffectFrom().getTime());
            good.setTimeFromEnd(a.getEffectTo().getTime());
            good.setCurrentTime(System.currentTimeMillis());
            if(!a.getImg().startsWith("http")){
            good.setImg("http://images.handeson.com/" + a.getImg().replace("qn|hs-resources|", "") + "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");}
            goods.add(good);
        }
        model.addAttribute("promotions", goods);
        model.addAttribute("userId",cpid);


        return "pintuan/html/list1";
    }


    @RequestMapping("/p/promotionList/aboutto/{userId}")
    public String getAboutToPromotionList(@PathVariable String userId, @RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value="pageSize" ,defaultValue = "10") Integer pageSize, Model model) {
        //即将开团
        PageHelper<PromotionGoodsVO> promotions = promotionGoodsService.selectNotBeginningPromotionGoods(null, userId,page,pageSize);
        List<PromotionGoodsExVO> goods = new ArrayList<PromotionGoodsExVO>(promotions.getResult().size());
        PromotionGoodsExVO good = null;
        for (PromotionGoodsVO a : promotions.getResult()) {
            good = new PromotionGoodsExVO();
            BeanUtils.copyProperties(a, good);
            good.setTimeFromBegin(a.getEffectFrom().getTime());
            good.setTimeFromEnd(a.getEffectTo().getTime());
            good.setCurrentTime(System.currentTimeMillis());
            if(!a.getImg().startsWith("http")){
            good.setImg("http://images.handeson.com/" + a.getImg().replace("qn|hs-resources|", "") + "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");}
            goods.add(good);
        }
        model.addAttribute("promotions", goods);
        model.addAttribute("userId",userId);
        return "pintuan/html/list_soon";
    }

    @RequestMapping("/p/promotionPgDetail/{id}/{cpId}")
    @ApiOperation(value = "查看某个具体的活动商品", notes = "查看某个具体的活动商品信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
    public String main(@PathVariable String id, HttpServletRequest req, Model model,
                       @RequestParam(value = "promotionProductId", required = false) String promotionProductId,
                       @PathVariable(value = "cpId") Long cpId,
                       @RequestParam(value = "promotionFrom", required = false) PromotionType promotionType) {
        //1. 取拼团活动详细信息
        List<PromotionGoodsDetailVO> detailVOList = this.selectPeopleAndDiscountPrice(id);
        PromotionGoodsDetailVO goodsDetailVO = promotionGoodsDetailService.selectPromotionTimeAndMaxDiscount(id, null, cpId);
        List<Sku> promotionSku=this.selectSku(id);

        PromotionGoodsDetailExVO vo = new PromotionGoodsDetailExVO();
        BeanUtils.copyProperties(goodsDetailVO, vo);
        vo.setTimeFromBegin(goodsDetailVO.getEffectFrom().getTime());
        vo.setTimeFromEnd(goodsDetailVO.getEffectTo().getTime());
        vo.setCurrentTime(System.currentTimeMillis());
        //2. 取到sku详情
        PgSkuDetailVo skuDetailVo = this.getSkuDetal(id, cpId, req);
        for (ProductImageVO piv : skuDetailVo.getImgs()) {
            if(!piv.getImg().startsWith("http")){
            piv.setImgUrl("http://images.handeson.com/" + piv.getImg().replace("qn|hs-resources|", "") + "?imageView2/2/w/640/q/100/@w/$w$@/@h/$h$@");}

        }

        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("detailVOList", detailVOList);
        objectMap.put("promotionSku",promotionSku);
        objectMap.put("goodsDetailVO", vo);
        objectMap.put("skuDetail", skuDetailVo);

//        String main = skuDetailVo.getImgs().get(0).getImgUrl();
//        model.addAttribute("mainUrl",main);
        model.addAttribute("detailVOList", detailVOList);
        model.addAttribute("goodsDetailVO", vo);
        model.addAttribute("skuDetail", skuDetailVo);
        model.addAttribute("product_json", JSONObject.toJSONString(objectMap));
//        model.addAttribute("promotions_json", JSONObject.toJSONString(skuDetailVo));
//        model.addAttribute("promotions_json1", JSONObject.toJSONString(skuDetailVo.getSkus().get(0)));

        return "pintuan/html/detail_1";
    }

    /**
     * 根据活动编码查询拼团活动的人数和对应优惠价格
     *
     * @param code
     * @return
     */
    @ResponseBody
    public List<PromotionGoodsDetailVO> selectPeopleAndDiscountPrice(@PathVariable String code) {
        return promotionGoodsDetailService.selectPeopleAndDiscountPrice(code, null);

    }
    @ResponseBody
    public List<Sku> selectSku(String code){
        return promotionGoodsDetailService.selectSku(code);
    }
//    /**
//     * 查询剩余时间和最高拼团价格
//     *
//     * @param code
//     * @return
//     */
//    @ResponseBody
//    public PromotionGoodsDetailVO selectPromotionTimeAndMaxDiscount(String code) {
//        User user = (User) getCurrentIUser();
//        return promotionGoodsDetailService.selectPromotionTimeAndMaxDiscount(code,user.getId());
//    }


    /**
     * 生成商品的图片URL
     */
    private void convertUrl(ProductVO vo) {
        if (null != vo.getImgs() && vo.getImgs().size() > 0) {
            for (ProductImageVO img : vo.getImgs()) {
                img.setImgUrl(img.getImg());
            }
        }
        //TODO
        vo.setCategory(categoryService.loadCategoryByProductId(vo.getId()));
    }

    private PgSkuDetailVo getSkuDetal(String id, Long cpId, HttpServletRequest req) {

        ProductVO product = productService.load(id);
        if (product == null) {
            RequestContext requestContext = new RequestContext(req);
            throw new BizException(GlobalErrorCode.NOT_FOUND,
                    requestContext.getMessage("product.not.found"));
        }

        //test
        convertUrl(product); // 生成商品的图片URL
        // 约定获取商品的图文信息的时候，从productService中获取，实现：先获取富文本信息，再获取片段信息
        setFragmentAndDescInfo(product);//----------------------------

        // 获取商品的多sku规格属性
        List<SkuAttributeVO> skuAttributeVOs = skuAttributeService.findAttributesByProductId(id);
        product.setSkuAttributes(skuAttributeVOs);

        User user = userService.selectUserIdByCpId(cpId == null ? null : String.valueOf(cpId));
        List<BasePromotionCouponVO> availableCoupons = couponService
                .listBasePromotionCouponByProductAndUserId(product.getId(), user.getId());
        product.setCouponList(availableCoupons);

        // 计算积分兑换比例
        // 只在前端调用时计算
        PgSkuDetailVo result = new PgSkuDetailVo(product, urlHelper.genProductUrl(
                product.getId() + "?upline=" + cpId),
                product.getImg(), null);

        PromotionProductVO promotionProduct = flashSaleService
                .loadPromotionProductByPId(product.getId());
        if (promotionProduct != null) {
            result.setCanUsePromotion(promotionProduct);
            result.setCanUsePromotionType(PromotionType.FLASHSALE);
        }

        // 放入评价统计信息
        Map<String, Long> commentTotalMap = commentService.commentTotalMap(product.getId());
        result.setCommentTotalMap(commentTotalMap);

        //用第一个sku的数据覆盖商品的规格数据
        if (CollectionUtils.isNotEmpty(result.getSkus())) {
            Sku sku = result.getSkus().get(0);
            result.setDeductionDPoint(sku.getDeductionDPoint());
            result.setNetWorth(sku.getNetWorth());
            result.setPoint(sku.getPoint());
            result.setPrice(sku.getPrice());
            result.setAmount(sku.getAmount());
        }

        return result;
    }

    public IUser getCurrentIUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal instanceof IUser) {
                IUser user = (IUser) principal;
                if (!user.isAnonymous()) {
                    return user;
                }
            }

            if (!auth.getClass().getSimpleName().contains("Anonymous")) {
                log.error("Unknown authentication encountered, ignore it. " + auth);
            }
        }

        throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
    }
    public class PromotionGoodsDetailExVO extends PromotionGoodsDetailVO {
        private Long currentTime;
        private Long timeFromBegin;
        private Long timeFromEnd;
        public Long getTimeFromBegin() {
            return timeFromBegin;
        }

        public Long getTimeFromEnd() {
            return timeFromEnd;
        }
        public Long getCurrentTime() {
            return currentTime;
        }

        public void setCurrentTime(Long currentTime) {
            this.currentTime = currentTime;
        }

        public void setTimeFromBegin(Long timeFromBegin) {
            this.timeFromBegin = timeFromBegin;
        }

        public void setTimeFromEnd(Long timeFromEnd) {
            this.timeFromEnd = timeFromEnd;
        }
    }

    public class PromotionGoodsExVO extends PromotionGoodsVO {
        public Long getTimeFromBegin() {
            return timeFromBegin;
        }

        public Long getTimeFromEnd() {
            return timeFromEnd;
        }


        public void setTimeFromBegin(Long timeFromBegin) {
            this.timeFromBegin = timeFromBegin;
        }

        public Long getCurrentTime() {
            return currentTime;
        }

        public void setCurrentTime(Long currentTime) {
            this.currentTime = currentTime;
        }

        public void setTimeFromEnd(Long timeFromEnd) {
            this.timeFromEnd = timeFromEnd;
        }
        private Long currentTime;
        private Long timeFromBegin;
        private Long timeFromEnd;
    }
}

