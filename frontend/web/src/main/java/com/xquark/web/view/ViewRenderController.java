package com.xquark.web.view;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.xquark.dal.model.ViewComponent;
import com.xquark.dal.model.ViewPage;
import com.xquark.dal.model.ViewPageComponent;
import com.xquark.service.view.ViewComponentService;
import com.xquark.service.view.ViewPageComponentService;
import com.xquark.service.view.ViewPageService;
import com.xquark.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * 自定义页面渲染controller Created by chh on 16-10-20.
 */
@Controller
public class ViewRenderController {

  @Autowired
  private ViewPageService viewPageService;

  @Autowired
  private ViewComponentService viewComponentService;

  @Autowired
  private ViewPageComponentService viewPageComponentService;

  /**
   * 根据pageid进行该自定义页面的组件渲染
   */
  @RequestMapping(value = "/viewRender/{id}")
  public String render(@PathVariable String id, Model model, HttpServletRequest req) {
    ViewPage page = viewPageService.selectByPrimaryKey(id);
    // 该自定义页面使用哪个模板页面
    String layout = page.getLayout();
    StringBuffer renderHtml = new StringBuffer();
    // 根据pageid找到对应的所有组件配置信息
    List<ViewPageComponent> pageComponents = viewPageComponentService.findByPageId(id);
    // 循环所有组件，解析组件html
    for (ViewPageComponent viewPageComponent : pageComponents) {
      String componentId = viewPageComponent.getComponentId();
      // 将各个组件根据配置的参数值解析得到html内容
      ViewComponent component = viewComponentService.selectByPrimaryKey(componentId);
      String sourceHtml = component.getContent();
      String paramValue = viewPageComponent.getComponentParams();
      // 将数据库存储的json字符串转成json对象
      JSONObject object = JSONObject.parseObject(paramValue);
      //使用Mustache解析将配置的参数值设置到组件的content内容中
      MustacheFactory mf = new DefaultMustacheFactory();
      Mustache mustache = mf.compile(new StringReader(sourceHtml), "example");
      StringWriter writer = new StringWriter();
      mustache.execute(writer, object);
      try {
        String html = writer.toString();
        renderHtml.append(html);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    model.addAttribute("renderHtml", renderHtml);
    return "view/" + layout;
  }

  public static void main(String[] args) {

    StringWriter writer = new StringWriter();
    HashMap map = new HashMap();
    map.put("name", "chh");
    MustacheFactory mf = new DefaultMustacheFactory();
    Mustache mustache = mf.compile(new StringReader("my name is {{name}}"), "example");
    mustache.execute(writer, map);
    try {
      String xx = writer.toString();
      System.out.println(" result = " + xx);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
