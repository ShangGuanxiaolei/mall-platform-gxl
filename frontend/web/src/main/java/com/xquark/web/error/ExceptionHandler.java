package com.xquark.web.error;

import com.alibaba.fastjson.JSONObject;
import com.xquark.dal.type.PayChannelConst;
import com.xquark.monitor.WarnEntity;
import com.xquark.monitor.WarnEnv;
import com.xquark.monitor.WarnSystemName;
import com.xquark.monitor.drive.MonitorWarn;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.PayNotifyException;
import com.xquark.service.outpay.OutPayLogs;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.HttpClientUtil;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.web.ResponseObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * General error handler for the application.
 */
@ControllerAdvice
class ExceptionHandler {

  Logger log = LoggerFactory.getLogger(getClass());
  private Logger thirdRefundLogger = OutPayLogs.thirdRefundLogger;

  @Value("${profiles.active}")
  WarnEnv currentEnv;
  final WarnEnv[] warnEnv = new WarnEnv[] {WarnEnv.prod, WarnEnv.stage, WarnEnv.uat, WarnEnv.uat2, WarnEnv.sit};

  /**
   * Handle exceptions thrown by handlers.
   */
  @ResponseBody
  @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
  public ResponseObject<?> exception(Exception e, HttpServletRequest req) {
    log.error("access '" + req.getRequestURI() + "' occurs error [" + e.getMessage()
        + "] on: " + req.getContextPath() + ", "
        + req.getParameterMap(), e);

    /*Exception 预警*/
    Set<WarnEnv> warnEnvSet = new HashSet<>(Arrays.asList(warnEnv));
    if(warnEnvSet.contains(currentEnv)) {
      WarnEntity warnEntity = new WarnEntity(currentEnv, WarnSystemName.WEB, e, req);

      MonitorWarn monitorWarn = MonitorWarn.getInstance();
      monitorWarn.sendMonitorMsg(warnEntity);
    }
    /*Exception 预警 end*/

    log.error(req.getRequestURI() + " general error", e);
    BizException be = getBizException(e);
    GlobalErrorCode ec;
    String moreInfo;
    if (e instanceof MissingServletRequestParameterException) {
      ec = GlobalErrorCode.INVALID_ARGUMENT;
      RequestContext requestContext = new RequestContext(req);
      Object[] params = new Object[1];
      params[0] = ((MissingServletRequestParameterException) e).getParameterName();
      moreInfo = requestContext.getMessage("valid.not_Blank.param", params);
    } else if (e instanceof SQLException) {
      ec = GlobalErrorCode.INTERNAL_ERROR;
      moreInfo = e.getClass().getName();
    } else if (be == null) {
      ec = GlobalErrorCode.UNKNOWN;
      moreInfo = e.getClass().getName();
    } else {
      ec = be.getErrorCode();
      moreInfo = be.getMessage();
    }
    return new ResponseObject<>(moreInfo, ec);
  }

  /**
   * 异常处理器:处理当请求的参数不符合规范时产生的异常(表单)
   *
   * @param e 非检查异常
   * @return 返回HTTP状态码400
   */
  @org.springframework.web.bind.annotation.ExceptionHandler(value = MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ResponseObject<String> violationException(MethodArgumentNotValidException e) {
    // 取出所有的校验错误并整理为一个字符串
    BindingResult bindingResult = e.getBindingResult();
    List<ObjectError> allErrors = bindingResult.getAllErrors();
    StringBuilder violationResult = new StringBuilder();
    for (ObjectError error : allErrors) {
      violationResult.append(error.getDefaultMessage()).append(" ");
    }
    // 记录日志
    log.info("ConstraintViolationException: {}", violationResult.toString());
    return new ResponseObject<>(violationResult.toString());
  }

  private BizException getBizException(Throwable e1) {
    Throwable e2 = e1;
    do {
      if (e2 instanceof BizException) {
        return (BizException) e2;
      }
      e1 = e2;
      e2 = e1.getCause();
    } while (e2 != null && e2 != e1);

    return null;
  }
  
  @Value("${pay.notify.third_refund_url:/refund/create}")
  private String thirdRefundUrl; 
  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;
  
  /**
   * @description 支付回调失败，统一拦截进行退款操作
   * @param e 拦截异常类型，需要发送退款的数据
   * @param HttpServletResponse resp 对应支付方式结果输出
   */
  @org.springframework.web.bind.annotation.ExceptionHandler(value = PayNotifyException.class)
  @ResponseBody
  public void payNotifyException(PayNotifyException e,HttpServletResponse resp) {
	  
		String respMsg = thirdPaymentQueryService.doThirdRefund(e);
		CommonUtil.responseBody(respMsg,resp);
  }
  
}