package com.xquark.web.payment;

import com.google.common.collect.ImmutableMap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.xquark.dal.mapper.PaymentSwitchConfigMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.pay.PaymentConfig;
import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.OrderSortType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.type.TradeType;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.dal.vo.PromotionGrouponVO;
import com.xquark.interceptor.Token;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.groupon.PromotionGrouponService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.PaymentResponseClientVO;
import com.xquark.service.outpay.ThirdPartyPayment;
import com.xquark.service.outpay.impl.*;
import com.xquark.service.outpay.impl.tenpay.CommonUtil;
import com.xquark.service.outpay.impl.tenpay.SHA1Util;
import com.xquark.service.pay.model.PaymentResponseVO;
import com.xquark.web.BaseController;
import com.xquark.web.ResponseObject;
import com.xquark.web.vo.Json;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.*;

import static com.xquark.wechat.util.WeChatUtil.createSign;

@Controller
public class PaymentController extends BaseController {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ThirdPartyPayment aliPayment;

  @Autowired
  private TenPaymentImpl tenPayment;

  @Autowired
  private ThirdPartyPayment umPayment;

  @Autowired
  private ThirdPartyPayment sumPayment;

  @Autowired
  private ThirdPartyPayment xqPayment;

  @Autowired
  private ThirdPartyPayment esunPayment;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private PromotionGrouponService promotionGrouponService;

  @Value("${site.web.host.name}")
  String siteHost;

  @Autowired
  private ThirdPartyPayment aliPaymentPc;

  @Autowired
  private PaymentFacade paymentFacade;

  @Autowired
  private PaymentSwitchConfigMapper paymentSwitchConfigMapper;

  @Value("${profiles.active}")
  String profile;

  private boolean epsStatus() {
    PaymentSwitchConfig paymentSwitchConfig = paymentSwitchConfigMapper.selectByCodeAndProfile("EPS", profile);
    return paymentSwitchConfig.getStatus();
  }

  @RequestMapping("/pay/alipayPc")
  public void alipayPc(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    log.info("alipay pc pay request bizNo=[" + request.getTradeNo() + "]");
    try {
      aliPaymentPc.payRequest(req, resp, request);
    } catch (Exception e) {
      log.error("alipayPc支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping(AliPaymentPCImpl.call_back_url)
  public String alipayPcCallback(HttpServletRequest req, HttpServletResponse resp,
      RedirectAttributes redirectAttrs) {
    log.info("alipay pc call back url is called bizNo=[" + req.getParameter("out_trade_no") + "]");
    PaymentResponseVO paymentVO = aliPaymentPc.payCallback(req, resp);
    if (paymentVO == null) {
      log.warn(
          "payment alipy pc response is null! bizNo=[" + req.getParameter("out_trade_no") + "]");
      redirectAttrs.addAttribute("paidStatus", "fail");
    } else {
      redirectAttrs.addAttribute("paidStatus", "success");
    }
    redirectAttrs.addAttribute("bizType", "order");
    redirectAttrs.addAttribute("bizNo", req.getParameter("out_trade_no"));
    redirectAttrs.addAttribute("paymentMode", PaymentMode.ALIPAY);

    return getCallbackRedirectUrl(req.getParameter(
        "out_trade_no"));// "redirect:/cashier/callback/" + req.getParameter("out_trade_no");
  }

  @RequestMapping(AliPaymentPCImpl.notify_url)
  public void alipayPcNotify(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info("alipay pc notify url is called bizNo=[" + req.getParameter("out_trade_no") + "]");
    PaymentResponseVO paymentVO = aliPaymentPc.payNotify(req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info("alipay notify url is failed bizNo=[" + req.getParameter("out_trade_no") + "]");
      return;
    }

    responseBody("success", resp);
  }

  /**
   * 协议支付，跳转到短信页面，类似umpay 协议支付 第一步
   */
  @RequestMapping("/pay/umpay/agreement")
  public String umpayAgreement(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {

    try {
      request.setUserId(super.getCurrentUser().getId());
      PaymentResponseVO paymentVO = umPayment.payRequest(req, resp, request);
      model.addAttribute("paymentVO", paymentVO);
    } catch (Exception e) {
      // log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo() + "," + e.getMessage(), e);
    }
    return "pay/sms";
  }

  /**
   * 协议支付再次发送短信
   */
  @ResponseBody
  @RequestMapping("/pay/umpay/sendSmsAgain")
  public Json sendSmsAgain(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request) {
    Json json = new Json();
    try {
      umPayment.sendSmsAgain(request);
      json.setMsg("发送成功");
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("再次发送失败;" + e.getMessage());
      log.error("支付请求再次发送短信失败，tradeNo=" + request.getTradeNo() + "," + e.getMessage(), e);
    }
    return json;
  }

  /**
   * 首次支付
   */
  @RequestMapping("/pay/umpay")
  public void umpay(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {

    try {
      request.setUserId(super.getCurrentUser().getId());
      umPayment.payRequest(req, resp, request);
    } catch (Exception e) {
      // log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  /**
   * 输入短信后的确认页面，类似callback操作 协议支付 第二步
   */
  @ResponseBody
  @RequestMapping("/pay/umpayConfirm")
  public Json umpayConfirm(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    log.info("umpay call back url is called " + request);
    Json json = new Json();
    PaymentResponseVO paymentVO = null;
    try {
      User user = getCurrentUser();
      request.setUserId(user.getId());
      paymentVO = umPayment.payConfirm(req, resp, request);
      json.setObj(paymentVO.getBillNo());
      json.setRc(Json.RC_SUCCESS);
      json.setMsg("支付成功");
      log.info("umpay call back url is success " + request);

    } catch (Exception e) {
      log.info("umpay call back url is failed " + request);
      json.setRc(Json.RC_FAILURE);
      json.setMsg(e.getMessage());
    }
    return json;
  }

  /**
   * 协议支付成功后跳转 协议支付  第三次
   */
  @RequestMapping("/pay/umpayConfirmDone")
  public String umpayConfirmDone(HttpServletRequest req, HttpServletResponse resp, String orderNo,
      RedirectAttributes redirectAttrs) {
    redirectAttrs.addAttribute("paidStatus", "success");
    redirectAttrs.addAttribute("bizType", "order");
    redirectAttrs.addAttribute("bizNo", orderNo);
    redirectAttrs.addAttribute("paymentMode", PaymentMode.UMPAY);
    //return "redirect:/cashier/callback/" + orderNo;
    return getCallbackRedirectUrl(orderNo);
  }

  @RequestMapping(UmPaymentImpl.call_back_url)
  public String umpayCallback(HttpServletRequest req, HttpServletResponse resp,
      RedirectAttributes redirectAttrs) {

    log.info("umpay call back url is called orderNo=[" + req.getParameter("order_id") + "]");
    PaymentResponseVO paymentVO = umPayment.payCallback(req, resp);
    if (paymentVO == null) {
      log.warn("payment response is null! orderNo=[" + req.getParameter("order_id") + "]");
      redirectAttrs.addAttribute("paidStatus", "fail");
    } else {
      redirectAttrs.addAttribute("paidStatus", "success");
    }
    redirectAttrs.addAttribute("bizType", "order");
    redirectAttrs.addAttribute("bizNo", req.getParameter("order_id"));
    redirectAttrs.addAttribute("paymentMode", PaymentMode.UMPAY);

    //return "redirect:/cashier/callback/" + req.getParameter("order_id");
    return getCallbackRedirectUrl(req.getParameter("order_id"));
  }

  /**
   * 首次支付和协议首次最后的notify
   */
  @RequestMapping(UmPaymentImpl.notify_url)
  public void umpayNotify(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    log.info("umpay notify url is called bizNo=[" + req.getParameter("order_id") + "]");
    PaymentResponseVO paymentVO = umPayment.payNotify(req, resp);
    if (paymentVO.getBillStatus() != PaymentStatus.SUCCESS) {
      log.error("umpay notify url is failed bizNo=[" + req.getParameter("order_id") + "]");
    } else {
      log.info("umpay notify url is success bizNo=[" + req.getParameter("order_id") + "]");
    }

    String html =
        "<html><head><META NAME=\"MobilePayPlatform\" CONTENT=\"" + paymentVO.getRespDate()
            + "\" /> <title>result</title></head></html>";
    responseBody(html, resp);
  }

  @RequestMapping("/pay/umRefund")
  public void umRefund(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      PaymentResponseVO paymentVO = umPayment.payRefund(req, resp, request);
      umPayment.savePayResult(paymentVO);
    } catch (Exception e) {
      // log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "退款请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping(UmPaymentImpl.refund_notify_url)
  public void umpayRefundNotify(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info("umpay refund notify url is called");
    PaymentResponseVO paymentVO = umPayment.payNotify(req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info("umpay refund notify url is failed");
      return;
    }
    log.info(paymentVO.toString());
    log.info("umpay notify url is fail");
  }

  @RequestMapping("/pay/sumpay")
  public void sumpay(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      sumPayment.payRequest(req, resp, request);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping(SumPaymentImpl.call_back_url)
  public String sumpayCallback(HttpServletRequest req, HttpServletResponse resp, Model model) {
    log.info("alipay call back url is called");
    PaymentResponseVO paymentVO = aliPayment.payCallback(req, resp);
    OrderVO order = (OrderVO) model.asMap().get("order");
    if (UserPartnerType.XIANGQU == order.getPartnerType()) {
      return "/xiangqu/pay_call_back";
    }
    return "/pay/call_back";
  }

  @RequestMapping(SumPaymentImpl.notify_url)
  public void sumpayNotify(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info("alipay notify url is called");
    PaymentResponseVO paymentVO = aliPayment.payNotify(req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info("alipay notify url is failed");
      return;
    }
    log.info(paymentVO.toString());
    log.info("alipay notify url is fail");
    responseBody("fail", resp);
  }

  /**
   * 支付宝客户端支付
   * @param req
   * @param resp
   * @param request
   * @param model
   */
  @ResponseBody
  @RequestMapping("/pay/alipay")
  public void alipay(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      List<CashierItem> items = cashierService.listByBizNo(request.getTradeNo());
      String[] orderNos = items.get(0).getBatchBizNos().split(",");
      BigDecimal realTotalFee = BigDecimal.ZERO;
      for (String orderNo : orderNos) {
        MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(orderNo);
        realTotalFee = realTotalFee.add(mainOrderVO.getTotalFee());
      }
      request.setRemoteHost(getIpAddr(req));
      // 校验金额
      checkPayFee(request, getCurrentUser(), orderNos, realTotalFee);
      if(epsStatus()){
        Map<String, String> pay = paymentFacade.pay(request, getCurrentUser());
        JSONObject json = new JSONObject(pay.toString());
        responseBody(json.toString(), resp);
      }else{
        aliPayment.payRequest(req, resp, request);
      }
    } catch (Exception e) {
      log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }


  @RequestMapping("/pay/esunpay")
  public void esunpay(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      esunPayment.payRequest(req, resp, request);
    } catch (Exception e) {
      log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping("/pay/esun/notify")
  public void esunpayNotify(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info("esunpay notify url is called bizNo=[" + req.getParameter("out_trade_no") + "]");
    for (Enumeration names = req.getParameterNames(); names
        .hasMoreElements(); ) {
      String name = (String) names.nextElement();
      String values = req.getParameter(name);
      System.out.println("name:" + name + "  values=" + values);
    }
    PaymentResponseVO paymentVO = aliPayment.payNotify(req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info("alipay notify url is failed bizNo=[" + req.getParameter("out_trade_no") + "]");
      return;
    }

    responseBody("success", resp);
  }


  protected String getCallbackRedirectUrl(String bizNo) {
    return "redirect:/cashier/callback/" + bizNo;
  }

  @RequestMapping(AliPaymentImpl.call_back_url)
  public String alipayCallback(HttpServletRequest req, HttpServletResponse resp,
      RedirectAttributes redirectAttrs) {
    log.info("alipay call back url is called orderNo=[" + req.getParameter("out_trade_no") + "]");
    PaymentResponseVO paymentVO = aliPayment.payCallback(req, resp);
    if (paymentVO == null) {
      log.warn("payment response is null! orderNo=[" + req.getParameter("out_trade_no") + "]");
      redirectAttrs.addAttribute("paidStatus", "fail");
    } else {
      redirectAttrs.addAttribute("paidStatus", "success");
    }
    redirectAttrs.addAttribute("bizType", "order");
    redirectAttrs.addAttribute("bizNo", req.getParameter("out_trade_no"));
    redirectAttrs.addAttribute("paymentMode", PaymentMode.ALIPAY);

    return getCallbackRedirectUrl(req.getParameter("out_trade_no"));
  }

  @RequestMapping(AliPaymentImpl.notify_url_app)
  public void alipayNotifyApp(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info(
        "alipay app notify url is called notify_data=[" + req.getParameter("notify_data") + "]");

    PaymentResponseVO paymentVO = aliPayment.payNotifyApp(
        req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info(
          "alipay app notify url is failed notify_data=[" + req.getParameter("notify_data") + "]");
      return;
    }

    responseBody("success", resp);
  }

  @RequestMapping(AliPaymentImpl.notify_url)
  @Token(save = true)
  public void alipayNotify(HttpServletRequest req, HttpServletResponse resp, Model model)
      throws IOException {
    log.info("alipay notify url is called notify_data=[" + req.getParameter("notify_data") + "]");

    PaymentResponseVO paymentVO = aliPayment.payNotify(req, resp);
    if (paymentVO == null) {
      responseBody("fail", resp);
      log.info("alipay notify url is failed notify_data=[" + req.getParameter("notify_data") + "]");
      return;
    }

    responseBody("success", resp);
  }

  @RequestMapping(AliPaymentImpl.merchant_url + "/{orderId}")
  public String alipayUndone(@PathVariable("orderId") String orderId, Model model) {
    // TODO 该方法须重写
    List<CashierItem> items = cashierService.listByBizNo(orderId);
    String batchBizNos = items.get(0).getBatchBizNos();
    String orderNo = batchBizNos.split(",")[0];
    OrderVO order = orderService.loadByOrderNo(orderNo);
    model.addAttribute("order", order);

    return "/pay/undone";
  }

  @RequestMapping("/pay/xiangqu")
  public String xiangqu(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, RedirectAttributes redirectAttrs) {
    log.info("xqpay payrequest url is called request=[" + request + "]");

    try {
      xqPayment.payRequest(req, resp, request);

    } catch (Exception e) {
      log.error("想去支付请求失败，request=" + request, e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "想去支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
    redirectAttrs.addAttribute("bizType", "order");
    redirectAttrs.addAttribute("cashierItemId", request.getCashierItemId());
    redirectAttrs.addAttribute("paymentMode", PaymentMode.XIANGQU);
    redirectAttrs.addAttribute("paidStatus", "success");//如果失败，则直接抛异常

    return getCallbackRedirectUrl(request.getTradeNo());
  }

  protected void responseBody(String html, HttpServletResponse resp) throws IOException {
    PrintWriter writer = resp.getWriter();
    try {
      writer.print(html);
      writer.flush();
    } finally {
      writer.close();
    }
  }

  /**
   * 财付通的特殊性，需要返回一个xml结构的订单信息
   */
  @RequestMapping("/pay/wxpay/notify")
  public void urlNotify(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    tenPayment.payCallback(req, resp);
  }

  public static String getIpAddr(HttpServletRequest request) {
    String xForwardedFor = request.getHeader( "x-forwarded-for");
    String ip;
    if (xForwardedFor != null) {
      ip = StringUtils. split(xForwardedFor, ",")[0];
      if ( isIpValid(ip)) {
        return ip;
      }
    }
    ip = request.getHeader("Proxy-Client-IP");
    if (isIpValid(ip )) {
      return ip;
    }
    ip = request.getHeader("WL-Proxy-Client-IP");
    if (isIpValid(ip )) {
      return ip;
    }
    ip = request.getRemoteAddr();
    if (isIpValid(ip )) {
      return ip;
    }
    return "127.0.0.1";
  }

  private static boolean isIpValid(String ip) {
    if (ip == null || ip.length() == 0 || "unkown".equalsIgnoreCase(ip ) || ip .split("\\." ).length != 4) {
      return false;
    }
    return true;
  }

  @ResponseBody
  @RequestMapping("/pay/freepayApp")
  public ResponseObject<Map<String, Object>> freePayApp(@ModelAttribute PaymentRequestVO request) {
    Map<String, Object> ret = ImmutableMap.<String, Object>of(
        "tradeNo", request.getTradeNo(),
        "isFreePay", true);
    return new ResponseObject<Map<String, Object>>(ret);
  }

  /**
   * 微信支付(处理app内微信支付逻辑)
   */
  @ResponseBody
  @RequestMapping("/pay/tenpayApp")
  public ResponseObject<Map<String, String>> tenpayApp(HttpServletRequest req, HttpServletResponse resp,
      @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      // 支付订单id
      List<CashierItem> items = cashierService.listByBizNo(request.getTradeNo());
      String[] orderNos = items.get(0).getBatchBizNos().split(",");
      BigDecimal realTotalFee = BigDecimal.ZERO;
      for (String orderNo : orderNos) {
        MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(orderNo);
        realTotalFee = realTotalFee.add(mainOrderVO.getTotalFee());
      }
      request.setRemoteHost(getIpAddr(req));
      // 校验金额
      checkPayFee(request, getCurrentUser(), orderNos, realTotalFee);
      Map<String, String> ret = paymentFacade.pay(request, getCurrentUser());
      return new ResponseObject<>(ret);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping("/pay/tenpay")
  public String tenpay(HttpServletRequest req, HttpServletResponse resp,
      @ModelAttribute PaymentRequestVO request, Model model) {
    try {
      // 支付订单id
      String orderId = "";
      request.setRemoteHost(getIpAddr(req));
      String openId = getCurrentUser().getLoginname();
      request.setOpenId(openId);
      List<CashierItem> items = cashierService.listByBizNo(request.getTradeNo());
      String[] orderNos = items.get(0).getBatchBizNos().split(",");
      List<MainOrder> orders = new ArrayList<MainOrder>();
      BigDecimal realTotalFee = BigDecimal.ZERO;
      for (String orderNo : orderNos) {
        MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(orderNo);
        orderId = mainOrderVO.getOrders().get(0).getId();
        MainOrder mainOrder = new MainOrder();
        BeanUtils.copyProperties(mainOrderVO, mainOrder);
        orders.add(mainOrder);
        realTotalFee = realTotalFee.add(mainOrder.getTotalFee());
      }
      // 校验金额
      checkPayFee(request, getCurrentUser(), orderNos, realTotalFee);
      PaymentResponseClientVO response = tenPayment.sign(request);

      String app_id = response.getPayInfo().get("app_id");
      String app_key = response.getPayInfo().get("app_key");
      String ticket = response.getPayInfo().get("ticket");
      String trade_url = response.getPayInfo().get("trade_url");

      String packageStr = "prepay_id=" + response.getPayInfo().get("prepay_id");

      String timestamp = Long.toString(new Date().getTime() / 1000);

      String noncestr = CommonUtil.CreateNoncestr();

      String prepay_id = response.getPayInfo().get("prepay_id");

      SortedMap<Object, Object> params = new TreeMap<Object, Object>();
      params.put("appId", app_id);
      params.put("timeStamp", timestamp);
      params.put("nonceStr", noncestr);
      params.put("package", "prepay_id=" + prepay_id);
      params.put("signType", "MD5");

      String paySign = createSign(app_key, "UTF-8", params);
      // 方便ajax传值到页面中package是js的关键字出错
      params.put("packageValue", "prepay_id=" + prepay_id);

      String reqQueryStr = req.getQueryString();
      StringBuffer reqURL = req.getRequestURL();
      String url = reqURL + "?" + reqQueryStr;
      String signValue =
          "jsapi_ticket=" + ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url="
              + url;
      //这个签名.主要是给加载微信js使用.别和上面的搞混了.
      String signature = SHA1Util.Sha1((signValue));
      /*--
      "appId" : "${appId}",    //公众号名称，由商户传入
              "timeStamp":"${timeStamp}",          //时间戳，自 1970 年以来的秒数
              "nonceStr" : "${nonceStr}", //随机串
              "package" : "${package}",
              "signType" : "${signType}",          //微信签名方式:
              "paySign" : "${paySign}" //微信签名memcache
      --*/
      model.addAttribute("appId", app_id);
      model.addAttribute("timeStamp", timestamp);
      model.addAttribute("nonceStr", noncestr);
      model.addAttribute("package", packageStr);
      model.addAttribute("signType", "MD5");
      model.addAttribute("paySign", paySign);
      model.addAttribute("signature", signature);
      model.addAttribute("trade_url", trade_url);

      model.addAttribute("payment", request);
      model.addAttribute("orders", orders);
      model.addAttribute("orderId", orderId);
      return "/pay/weixinQrcode";
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping("/pay/tenpayMiniProgram")
  @ResponseBody
  public ResponseObject<Map<String, String>> tenpayMiniProgram(HttpServletRequest req, HttpServletResponse resp,
      @ModelAttribute PaymentRequestVO request) {
    try {
      request.setRemoteHost(getIpAddr(req));
      User user = getCurrentUser();
      request.setOpenId(user.getLoginname());
      List<CashierItem> items = cashierService.listByBizNo(request.getTradeNo());
      String[] orderNos = items.get(0).getBatchBizNos().split(",");
      List<MainOrder> orders = new ArrayList<MainOrder>();
      BigDecimal realTotalFee = BigDecimal.ZERO;
      for (String orderNo : orderNos) {
        MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(orderNo);
        MainOrder mainOrder = new MainOrder();
        BeanUtils.copyProperties(mainOrderVO, mainOrder);
        orders.add(mainOrder);
        realTotalFee = realTotalFee.add(mainOrder.getTotalFee());
      }
      // 校验金额it
      checkPayFee(request, getCurrentUser(), orderNos, realTotalFee);
      Map<String, String> ret = paymentFacade.pay(request, getCurrentUser());
      return new ResponseObject<Map<String, String>>(ret);
    } catch (Exception e) {
      log.error("支付请求失败，tradeNo={}", request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "支付请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @RequestMapping(value = "/pay/tenpay/qrimage/{id}", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] getFile(@PathVariable("id") String id) {
    try {
      Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
      // 指定纠错等级
      hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
      hints.put(EncodeHintType.MARGIN, 1);
      // 指定编码格式

      String codeUrl = base64Decode(id);
      BitMatrix byteMatrix;
      byteMatrix = new MultiFormatWriter()
          .encode(new String(codeUrl.getBytes("UTF-8"), "iso-8859-1"), BarcodeFormat.QR_CODE, 220,
              220, hints);
      ByteArrayOutputStream bao = new ByteArrayOutputStream();
      MatrixToImageWriter.writeToStream(byteMatrix, "png", bao);

      return bao.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (WriterException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private String base64Encode(byte[] b) {
    BASE64Encoder encoder = new BASE64Encoder();
    String codeBase64;
    StringBuilder pictureBuffer = new StringBuilder();
    pictureBuffer.append(encoder.encode(b));
    System.out.println(pictureBuffer.toString());
    codeBase64 = pictureBuffer.toString();
    return codeBase64;
  }

  public static String base64Decode(String s) {
    if (s == null) {
      return null;
    }
    BASE64Decoder decoder = new BASE64Decoder();
    try {
      byte[] b = decoder.decodeBuffer(s);
      return new String(b);
    } catch (Exception e) {
      return null;
    }
  }

  @RequestMapping(TenPaymentImpl.notify_url)
  public void tenpayNotify(HttpServletRequest req, HttpServletResponse resp,
      @RequestBody String body) throws IOException {
    req.setAttribute("wxNotify", body);
    tenPayment.payNotify(req, resp);
    HashMap<String, String> xml = new HashMap<String, String>();
    xml.put("return_code", "SUCCESS");
    xml.put("return_msg", "OK");
    responseBody(CommonUtil.ArrayToXml(xml), resp);
  }

  @RequestMapping(TenPaymentImpl.notify_mini_url)
  public void tenpayMiniProgramNotify(HttpServletRequest req, HttpServletResponse resp,
      @RequestBody String body) throws IOException {
    log.info("微信小程序支付回調，" + body);
    String decode = URLDecoder.decode(body, "UTF-8");
    String xmlBody = decode.replace("=", "");
    req.setAttribute("wxNotify", xmlBody);
    tenPayment.payMiniNotify(req, resp);
    HashMap<String, String> xml = new HashMap<String, String>();
    xml.put("return_code", "SUCCESS");
    xml.put("return_msg", "OK");
    responseBody(CommonUtil.ArrayToXml(xml), resp);
  }

  @RequestMapping(TenPaymentImpl.notify_app_url)
  public void tenpayAppNotify(HttpServletRequest req, HttpServletResponse resp,
      @RequestBody String body) throws IOException {
    log.info("APP支付回調，" + body);
    String decode = URLDecoder.decode(body, "UTF-8");
    String xmlBody = decode.replace("=", "");
    req.setAttribute("wxNotify", xmlBody);
    //获取appid
    PaymentConfig paymentConfig = paymentFacade.loadPayment(profile, PaymentMode.EPS, TradeType.APP.toString());
    req.setAttribute("appid", paymentConfig.getAppId());

    tenPayment.payAppNotify(req, resp);
    HashMap<String, String> xml = new HashMap<String, String>();
    xml.put("return_code", "SUCCESS");
    xml.put("return_msg", "OK");
    responseBody(CommonUtil.ArrayToXml(xml), resp);
  }

  /**
   * 支付成功后跳转到支付完成页面
   */
  @RequestMapping("/pay/success")
  public String success(HttpServletRequest req, HttpServletResponse resp, Model model,
      @RequestParam String orderId) {
    OrderVO vo = orderService.loadVO(orderId);
    model.addAttribute("orderVO", vo);
    model.addAttribute("mainOrderId", vo.getMainOrderId());
    // 团购订单付款成功后，加上一个跳转到分享页面的链接
    if (vo.getOrderType() == OrderSortType.GROUPON) {
      ActivityGroupon activityGroupon = activityGrouponService
          .findActivityGrouponByOrderId(orderId);
      String grouponId = activityGroupon.getGrouponId();
      PromotionGrouponVO promotionGrouponVO = promotionGrouponService.selectByPrimaryKey(grouponId);
      String grouponTargetUrl =
          siteHost + "/p/promotion/" + vo.getOrderItems().get(0).getProductId() + "?promotionId="
              + promotionGrouponVO.getPromotionId() + "&activityGrouponId=" + activityGroupon
              .getId() + "&from=view";
      model.addAttribute("grouponTargetUrl", grouponTargetUrl);
    }
    return "pay/paySuccess";
  }

  @RequestMapping("/pay/aliRefund")
  public void payRefund(HttpServletRequest req, HttpServletResponse resp,
      @Validated @ModelAttribute PaymentRequestVO request, Model model) {
    log.info("alipay refund request bizNo=[" + request.getTradeNo() + "]");
    try {
      aliPayment.payRefund(req, resp, request);
    } catch (Exception e) {
      log.error("alipay退款请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          "退款请求失败，tradeNo=" + request.getTradeNo(), e);
    }
  }

  @Autowired
  private OrderQuery orderQuery;

  @RequestMapping("/test/findOrder/app")
  public ResponseObject<Map> test(String orderNo,String payNo){
    Order order = new Order();
    order.setOrderNo(orderNo);
    order.setPayNo(payNo);
    order.setPayType(PaymentMode.EPS);
    Map<String, String> map = orderQuery.queryByEps(order, TradeType.ALIPAY);
    System.out.println(map);
    return new ResponseObject<>(map);
  }


  /**
   * 将request中的支付金额改为真实金额
   */
  private void checkPayFee(PaymentRequestVO request, User user, String[] mainOrderIds,
      BigDecimal realTotalFee) {
    BigDecimal reqTotalFee = request.getTotalFee();
    // 请求的支付金额与真实金额不一致
    if (reqTotalFee.compareTo(realTotalFee) != 0) {
      log.error("订单id: {}, cpId {}, IP: {} 请求支付金额与实际金额不一致, 请求: {}, 实际: {}",
          mainOrderIds, user.getCpId(), request.getRemoteHost(), reqTotalFee, realTotalFee);
    }
    request.setTotalFee(realTotalFee);
  }
}
