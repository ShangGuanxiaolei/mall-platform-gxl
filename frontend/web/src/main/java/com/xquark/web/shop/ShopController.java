package com.xquark.web.shop;

import com.xquark.biz.url.UrlHelper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.Merchant;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopAccessLog;
import com.xquark.dal.model.ShopStyle;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserTwitter;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.service.address.AddressService;
import com.xquark.service.address.AddressVO;
import com.xquark.service.cart.CartService;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.pricing.PricingService;
import com.xquark.service.pricing.vo.PricingResultVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shop.ShopStyleVO;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.tinyurl.TinyUrlService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.twitter.impl.TwitterShopComServiceImpl;
import com.xquark.service.user.UserService;
import com.xquark.service.zone.ZoneService;
import com.xquark.web.ResponseObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

@Controller
public class ShopController {

  @Autowired
  private ShopService shopService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CartService cartService;

  @Autowired
  private PricingService pricingService;

  @Autowired
  private AddressService addressService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private ShopAccessLogService shopAccessLogService;

  @Autowired
  private TinyUrlService tinyUrlService;

  @Autowired
  private UserTwitterService userTwitterService;

  @Autowired
  private TwitterShopComServiceImpl twitterShopComServiceImpl;

  @RequestMapping("/packet/index")
  public String viewPacket() {
    return "packet/index";
  }

  @RequestMapping(value = "/shop")
  public String view(Model model, HttpServletRequest req) {
    /**String shopId = req.getParameter("currentSellerShopId");
     Shop shop = shopService.load(shopId);
     if (shop == null || shop.getArchive() == Boolean.TRUE) {
     throw new BizException(GlobalErrorCode.NOT_FOUND, new RequestContext(req).getMessage("shop.not.found"));
     }

     model.addAttribute("shop", shop);
     model.addAttribute("owner", userService.load(shop.getOwnerId()));

     // 购物车内商品总数
     Integer cartCount = 0;
     String userId = this.getCurrentUser().FgetId();
     cartCount = cartService.countByShopId(userId, shopId);
     model.addAttribute("cartCount", cartCount);

     // 找到品牌团的类别id
     /**Category brand = categoryService.loadByName("品牌团");
     String parentCategoryId = brand.getId();
     model.addAttribute("parentCategoryId", parentCategoryId);**/

    // 访问店铺首页时，记录用户访问记录，用于统计流量访问
    String shopId = this.getCurrentUser().getCurrentSellerShopId();
    if (StringUtils.isNotEmpty(shopId)) {
      String userId = this.getCurrentUser().getId();
      ShopAccessLog log = new ShopAccessLog();
      log.setArchive(false);
      log.setShopId(shopId);
      log.setUserId(userId);
      shopAccessLogService.insert(log);

      // 店铺分享信息
      // 判断如果当前用户已经是推客，则首页分享出去是他自己的店铺，如果仅仅是访客，则分享出去是访客的卖家店铺
      String shareShopId = "";
      List<UserTwitter> twitters = userTwitterService.selectByUserId(userId);
      UserTwitter twitter = null;
      for (UserTwitter item : twitters) {
        if (item.getStatus() == TwitterStatus.ACTIVE) {
          twitter = item;
        }
      }

      if (twitter != null) {
        shareShopId = getCurrentUser().getShopId();
      } else {
        shareShopId = getCurrentUser().getCurrentSellerShopId();
      }

      Shop currentShop = shopService.load(shareShopId);
      User currentUser = userService.load(currentShop.getOwnerId());
      model.addAttribute("avatar", currentUser.getAvatar());
      model.addAttribute("shareUrl", getShareUrl(req, shareShopId));
      model.addAttribute("shareTitle", currentUser.getName());

      Shop sellerShop = shopService.load(shopId);
      model.addAttribute("shopName", sellerShop.getName());
    }

    return "b2c/index";
  }

  private String getShareUrl(HttpServletRequest req, String shareShopId) {
    String key = tinyUrlService.insert(
        req.getScheme() + "://" + req.getServerName() + "/shop?currentSellerShopId=" + shareShopId);
    return req.getScheme() + "://" + req.getServerName() + "/t/" + key;
  }

  /**
   * 兼容之前店铺路径
   */
  @RequestMapping(value = "/shop/{id}")
  public String viewId(Model model, @PathVariable("id") String id, HttpServletRequest req) {
    String shopId = req.getParameter("currentSellerShopId");
    if (StringUtils.isEmpty(shopId) || "null".equals(shopId) || "undefind".equals(shopId)) {
      shopId = id;
    }
    return "redirect:/shop?currentSellerShopId=" + shopId;
  }

  /**
   * 查看店铺下某个品牌团的商品页面
   */
  @RequestMapping(value = "/shop/brand/{id}/{categoryId}")
  public String brand(@PathVariable("id") String shopId,
      @PathVariable("categoryId") String categoryId, Model model, HttpServletRequest req) {
    String pshopId = "";
    Shop shop = shopService.load(shopId);
    // 个人商铺与平台商铺的品牌团查询共用一个界面，需要传入不同的shopid
    String root = req.getParameter("root");
    if ("true".equals(root)) {
      ShopTree ShopTree = shopTreeService.selectRootShopByShopId(shopId);
      pshopId = ShopTree.getRootShopId();
    } else {
      pshopId = shop.getId();
    }

    User user = null;
    if (shop == null || shop.getArchive() == Boolean.TRUE) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }

    model.addAttribute("shop", shop);
    model.addAttribute("root", root);
    model.addAttribute("owner", userService.load(shop.getOwnerId()));
    model.addAttribute("shopId", pshopId);
    Category category = categoryService.load(categoryId);
    model.addAttribute("category", category);

    return "b2b/shopIndex/brandList";
  }

  private User getCurrentUser() {
    User user = null;
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        user = (User) principal;
      } else if (principal instanceof Merchant) {
        Shop shop = shopService.load(((Merchant) principal).getShopId());
        return userService.load(shop.getOwnerId());
      }
    }
    return user;
  }

  @RequestMapping(value = "/shop/{id}/products")
  public String productAll(@PathVariable("id") String shopId, Model model, HttpServletRequest req) {
    Shop shop = shopService.load(shopId);
    //List<Product> products = productService.listProducts(shop.getId());
    List<Product> products = productService.listRootProducts(shop.getId());
    model.addAttribute("shop", shop);
    model.addAttribute("products", products);

//		ShopStyle shopStyle = shopService.loadShopStyle(shopId);
//        List<String> bodyClasses = new ArrayList<String>();
//        bodyClasses.add(shopStyle.getAvatarStyle());
//        bodyClasses.add(shopStyle.getBackgroundColor());
//        bodyClasses.add(shopStyle.getFontColor());
//        bodyClasses.add(shopStyle.getFontFamily());
//        bodyClasses.add(StringUtils.isEmpty(shopStyle.getListView()) ? "smallimg" : shopStyle.getListView());
//        model.addAttribute("styles", StringUtils.join(bodyClasses, " "));
//        model.addAttribute("waterfall", "waterfall".equalsIgnoreCase(shopStyle.getListView()));

    return "shop/shopAll";
  }

  /**
   * 只获取店铺信息，不获取商品列表
   */
  @RequestMapping(value = "/shop/{id}/withoutProduct")
  public String viewWithoutProduct(@PathVariable("id") String shopId, Model model,
      HttpServletRequest req) {
    Shop shop = shopService.load(shopId);
    if (shop == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND,
          new RequestContext(req).getMessage("shop.not.found"));
    }

    model.addAttribute("shop", shop);
    model.addAttribute("owner", userService.load(shop.getOwnerId()));

    ShopStyle shopStyle = shopService.loadShopStyle(shopId);
    List<String> bodyClasses = new ArrayList<String>();
    bodyClasses.add(shopStyle.getAvatarStyle());
    bodyClasses.add(shopStyle.getBackgroundColor());
    bodyClasses.add(shopStyle.getFontColor());
    bodyClasses.add(shopStyle.getFontFamily());
    bodyClasses
        .add(StringUtils.isEmpty(shopStyle.getListView()) ? "smallimg" : shopStyle.getListView());
    model.addAttribute("styles", StringUtils.join(bodyClasses, " "));
    model.addAttribute("waterfall", "waterfall".equalsIgnoreCase(shopStyle.getListView()));

    List<Category> categories = categoryService.listRootCategoriesByShop(shop.getId(),
        Taxonomy.GOODS);
    List<CategoryVO> categoryVOs = new ArrayList<CategoryVO>();
    for (int i = 0; i < categories.size(); i++) {
      CategoryVO categoryVO = new CategoryVO();
      BeanUtils.copyProperties(categories.get(i), categoryVO);
      categoryVO
          .setProductCount(categoryService.countProductsUnderCategory((categories.get(i)).getId()));
      categoryVOs.add(categoryVO);
    }
    model.addAttribute("categories", categoryVOs);

    model.addAttribute("recommendCount", productService.countByRecommend(shopId));
    model.addAttribute("allCount", productService.countProductsBySales(shopId));

    return "shop/shopView";
  }

  @RequestMapping(value = "/shop/{id}/recommendProduct")
  public String listProductsByRecommend(@PathVariable("id") String shopId, Model model) {
    List<Product> recommends = productService
        .listProductsByRecommend(shopId, new PageRequest(0, 12));
    model.addAttribute("prods", recommends);
    return "fragments/shopProducts";
  }

  @RequestMapping(value = "/shop/{id}/hotSaleProduct")
  public String listProductsBySales(@PathVariable("id") String shopId, Model model) {
    List<Product> prodsHot = productService
        .listProductsBySales(shopId, "", "", new PageRequest(0, 12), Direction.DESC, "", null, "",
            "",
            null, null);
    model.addAttribute("prods", prodsHot);
    return "fragments/shopProducts";
  }

  @RequestMapping(value = "/shop/{id}/crossSaleProduct")
  public String listCrossProductsBySales(@PathVariable("id") String shopId, Model model) {
    List<ProductVO> prodsCross = productService
        .listCrossProductsBySales(shopId, new PageRequest(0, 12), Direction.DESC);
    model.addAttribute("prods", prodsCross);
    model.addAttribute("crossBorder", null != prodsCross && prodsCross.size() > 0 ? true : false);
    return "fragments/shopProducts";
  }

  @RequestMapping(value = "/shop/{id}/productByPage")
  public String productAll(@PathVariable("id") String shopId, Pageable page, Model model) {
    ShopTree ShopTree = shopTreeService.selectRootShopByShopId(shopId);
    String rootShopId = ShopTree.getRootShopId();
    List<Product> products = productService
        .listProductsBySales(rootShopId, "", "", page, Direction.DESC, "", null, "", "",
            null, null);
    Long cnt = productService.countProductsByStatus(rootShopId, ProductStatus.ONSALE);
    model.addAttribute("prods", products);
    model.addAttribute("total", cnt);
    return "fragments/shopProducts";
  }

  @RequestMapping(value = "/shop/{id}/category/{cid}")
  public String productAll(@PathVariable("id") String shopId,
      @PathVariable("cid") String categoryId, Pageable page, Model model) {

    List<Product> products = categoryService.listProductsInCategory(categoryId, page);
    model.addAttribute("prods", products);
    return "fragments/shopProducts";
  }

  @RequestMapping(value = "/shop/{id}/cart")
  public String cart(@PathVariable("id") String shopId, Model model, HttpServletRequest req) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    PricingResultVO prices = new PricingResultVO();

    // 购物车没有传递item参数，取用户购物车所有项目
    List<CartItemVO> items = cartService.listCartItems(shopId);
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (CartItem item : items) {
      CartItemVO itemVO = new CartItemVO();
      BeanUtils.copyProperties(item, itemVO);
      itemVO.setProduct(productService.load(item.getProductId()));
      Sku sku = productService.loadSku(item.getProductId(), item.getSkuId());
      if (sku == null) {
        continue;
      }
      itemVO.setSku(sku);
      cartItems.add(itemVO);
      skuMap.put(sku.getId(), item.getAmount());
    }
    prices = pricingService.calculate(skuMap, null, null);
    model.addAttribute("cartItems", cartItems);
    model.addAttribute("prices", prices);

    String nextUrl = "/shop/" + shopId + "/cart-next";
    model.addAttribute("nextUrl", nextUrl);
    return "cart/cart";
  }

  @RequestMapping(value = "/shop/{id}/cart-next")
  public String next(@PathVariable("id") String shopId, Model model, HttpServletRequest req) {
    List<CartItemVO> cartItems = new ArrayList<CartItemVO>();
    PricingResultVO prices = new PricingResultVO();

    List<CartItemVO> items = cartService.listCartItems(shopId);
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (CartItem item : items) {
      Sku sku = productService.loadSku(item.getProductId(), item.getSkuId());
      if (sku == null) {
        continue;
      }
      CartItemVO itemVO = new CartItemVO();
      BeanUtils.copyProperties(item, itemVO);
      itemVO.setProduct(productService.load(item.getProductId()));
      itemVO.setSku(sku);
      cartItems.add(itemVO);
      skuMap.put(sku.getId(), item.getAmount());
    }
    prices = pricingService.calculate(skuMap, null, null);

    if (cartItems.size() == 0) {
      return "redirect:/shop/" + shopId + "/cart";
    }
    model.addAttribute("cartItems", cartItems);
    model.addAttribute("prices", prices);
    model.addAttribute("shopId", shopId);

    List<AddressVO> addresses = addressService.listUserAddressesVo();
    if (addresses != null && addresses.size() > 0) {
      model.addAttribute("address", addresses.get(0));
    }
    return "cart/next";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step1")
  public String step1() {
    return "shop/step1";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step2")
  public String step2() {
    return "shop/step2";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step3")
  public String step3() {
    return "shop/step3";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step4")
  public String step4() {
    return "shop/step4";
  }

  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/step5")
  public String step5() {
    return "shop/step5";
  }

  @ResponseBody
  @RequestMapping(UrlHelper.SHOP_URL_PREFIX + "/{id}/styles")
  public ResponseObject<ShopStyleVO> shopStylesCss(@PathVariable String id) {
    ShopStyleVO ss = shopService.loadShopStyle(id);
    return new ResponseObject<ShopStyleVO>(ss);
  }
}
