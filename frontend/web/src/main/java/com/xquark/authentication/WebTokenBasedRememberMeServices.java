package com.xquark.authentication;

import com.xquark.dal.model.User;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.util.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Date;

/**
 * 基于token校验用户登陆状态
 * <li>签名改为sha256+salt</li>
 * <li>Token信息DES加密后再使用base64编码</li>
 *
 * FIXME 与 AppTokenBasedRememberMeServices 重复
 * Created by dongsongjie on 16/9/28.
 */
@SuppressWarnings("all")
public class WebTokenBasedRememberMeServices extends TokenBasedRememberMeServices {

  public static final String DELIMITER = ":";

  private static final String KEY_ALGORITHM = "DES";

  private static final String COOKIE_KEY = "2016xquarkb2b";

  private final ShopService shopService;

  private UserService userService;

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public WebTokenBasedRememberMeServices(String key, UserDetailsService userDetailsService,
      ShopService shopService) {
    super(key, userDetailsService);
    this.shopService = shopService;
  }

  @Override
  protected UserDetails processAutoLoginCookie(String[] cookieTokens, HttpServletRequest request,
      HttpServletResponse response) {

    if (cookieTokens.length != 4) {
      throw new InvalidCookieException("Cookie token did not contain 4" +
          " tokens, but contained '" + Arrays.asList(cookieTokens) + "'");
    }

    long tokenExpiryTime;

    try {
      tokenExpiryTime = new Long(cookieTokens[1]);
    } catch (NumberFormatException nfe) {
      throw new InvalidCookieException(
          "Cookie token[1] did not contain a valid number (contained '" +
              cookieTokens[1] + "')");
    }

    if (isTokenExpired(tokenExpiryTime)) {
      throw new InvalidCookieException("Cookie token[1] has expired (expired on '"
          + new Date(tokenExpiryTime) + "'; current time is '" + new Date() + "')");
    }

    // Check the user exists.
    // Defer lookup until after expiry time checked, to possibly avoid expensive database call.
    //UserDetails userDetails = getUserDetailsService().loadUserByUsername(cookieTokens[0]);

    Long cpId = Long.valueOf(cookieTokens[3]);
    // 根据cookie token恢复出user对象，同时需要把CurrentSellerShopId加进去
    User userDetails = userService.loadByCpId(cpId);
    if (userDetails == null) {
      throw new InvalidCookieException("Cookie error");
    }

    //在APP中的卖家,将当前的访问店铺自动设置为卖家自身的shopid
    userDetails.setCurrentSellerShopId(shopService.loadRootShop().getId());

    String expectedTokenSignature = makeTokenSignature(userDetails.getUsername(), tokenExpiryTime,
        userDetails.getPassword(), cpId);

    if (!equals(expectedTokenSignature, cookieTokens[2])) {
      throw new InvalidCookieException("Cookie token[2] contained signature '" + cookieTokens[2]
          + "' but expected '" + expectedTokenSignature + "'");
    }

    return userDetails;
  }


  @Override
  public void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication successfulAuthentication) {

    String username = retrieveUserName(successfulAuthentication);
    String password = retrievePassword(successfulAuthentication);
    Object principal = successfulAuthentication.getPrincipal();

    Long cpId;
    if (principal instanceof User) {
      User user = (User) principal;
      cpId = user.getCpId();
    } else {
      logger.error("用户登录类型不正确");
      return;
    }

    // If unable to find a username and password, just abort as TokenBasedRememberMeServices is
    // unable to construct a valid token in this case.
    if (!StringUtils.hasLength(username)) {
      logger.debug("Unable to retrieve username");
      return;
    }

    if (!StringUtils.hasLength(password)) {
//      UserDetails user = getUserDetailsService().loadUserByUsername(username);
//      password = user.getPassword();

//      if (!StringUtils.hasLength(password)) {
//        logger.debug("Unable to obtain password for user: " + username);
//      }
      // 默认密码为123456
//      else {
      password = "123456";
//      }
    }

    int tokenLifetime = calculateLoginLifetime(request, successfulAuthentication);
    long expiryTime = System.currentTimeMillis();
    // SEC-949
    expiryTime += 1000L * (tokenLifetime < 0 ? TWO_WEEKS_S : tokenLifetime);
    String signatureValue = makeTokenSignature(username, expiryTime, password, cpId);

    setCookie(new String[]{username, Long.toString(expiryTime), signatureValue,
            String.valueOf(cpId)}, tokenLifetime,
        request, response);

    if (logger.isDebugEnabled()) {
      logger.debug("Added remember-me cookie for user '" + username + "', expiry: '"
          + new Date(expiryTime) + "'");
    }
  }

  /**
   * 使用SHA256签名
   */
  protected String makeTokenSignature(String username, long tokenExpiryTime, String password,
      Long cpId) {
    String salt = getKey();

    if (StringUtils.isEmpty(password)) {
      password = "123456";
    }

    String data = username + DELIMITER + tokenExpiryTime + DELIMITER + password + DELIMITER
        + cpId + DELIMITER + salt;
    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new IllegalStateException("No SHA-256 algorithm available!");
    }

    return new String(Hex.encode(digest.digest(data.getBytes())));
  }

  /**
   * Decodes the cookie and splits it into a set of token strings using the ":" delimiter.
   *
   * @param cookieValue the value obtained from the submitted cookie
   * @return the array of tokens.
   * @throws InvalidCookieException if the cookie was not base64 encoded.
   */
  protected String[] decodeCookie(String cookieValue) throws InvalidCookieException {
    for (int j = 0; j < cookieValue.length() % 4; j++) {
      cookieValue = cookieValue + "=";
    }

    if (!Base64.isBase64(cookieValue.getBytes())) {
      throw new InvalidCookieException(
          "Cookie token was not Base64 encoded; value was '" + cookieValue + "'");
    }

    String[] tokens = StringUtils
        .delimitedListToStringArray(cipher(cookieValue, Cipher.DECRYPT_MODE), DELIMITER);

    return tokens;
  }

  /**
   * Inverse operation of decodeCookie. Encrypt with DES.
   *
   * @param cookieTokens the tokens to be encoded.
   * @return base64 encoding of the tokens concatenated with the ":" delimiter.
   */
  protected String encodeCookie(String[] cookieTokens) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < cookieTokens.length; i++) {
      sb.append(cookieTokens[i]);

      if (i < cookieTokens.length - 1) {
        sb.append(DELIMITER);
      }
    }

    String value = sb.toString();
    sb = new StringBuilder(cipher(value, Cipher.ENCRYPT_MODE));

    while (sb.charAt(sb.length() - 1) == '=') {
      sb.deleteCharAt(sb.length() - 1);
    }

    return sb.toString();
  }

  private String cipher(String cookie, int mode) {
    Cipher cipher = null;
    byte[] ret = null;

    try {

      cipher = Cipher.getInstance(KEY_ALGORITHM);
      DESKeySpec dks = new DESKeySpec(COOKIE_KEY.getBytes());
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(KEY_ALGORITHM);
      SecretKey secretKey = keyFactory.generateSecret(dks);
      cipher.init(mode, secretKey);

      if (mode == Cipher.ENCRYPT_MODE) {
        ret = cipher.doFinal(cookie.getBytes("UTF-8"));
        return new String(Base64.encode(ret), "UTF-8");
      } else if (mode == Cipher.DECRYPT_MODE) {
        ret = cipher.doFinal(Base64.decode(cookie.getBytes("UTF-8")));
        return new String(ret, "UTF-8");
      } else {
        throw new AuthenticationServiceException("unsupported cipher mode:" + mode);
      }

    } catch (NoSuchAlgorithmException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (NoSuchPaddingException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (InvalidKeyException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (InvalidKeySpecException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (IllegalBlockSizeException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (BadPaddingException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    } catch (UnsupportedEncodingException e) {
      throw new AuthenticationServiceException("encrypt or decrypt tokens failed.", e);
    }
  }

  /**
   * Constant time comparison to prevent against timing attacks.
   */
  private static boolean equals(String expected, String actual) {
    byte[] expectedBytes = bytesUtf8(expected);
    byte[] actualBytes = bytesUtf8(actual);
    if (expectedBytes.length != actualBytes.length) {
      return false;
    }

    int result = 0;
    for (int i = 0; i < expectedBytes.length; i++) {
      result |= expectedBytes[i] ^ actualBytes[i];
    }
    return result == 0;
  }

  private static byte[] bytesUtf8(String s) {
    if (s == null) {
      return null;
    }
    return Utf8.encode(s);
  }

}
