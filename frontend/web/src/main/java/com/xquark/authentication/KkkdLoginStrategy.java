package com.xquark.authentication;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xquark.biz.filter.UserServletFilter;
import com.xquark.dal.status.UserStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.xquark.biz.authentication.LoginStrategy;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserSigninLog;
import com.xquark.service.user.UserService;
import com.xquark.service.userAgent.UserSigninLogService;
import com.xquark.service.userAgent.impl.UserSigninLogFactory;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;

/**
 * @author: chenxi
 */

public class KkkdLoginStrategy implements LoginStrategy {

  private static final String CLIENT_ID = "vd_cid";

  private static Logger LOG = LoggerFactory.getLogger(KkkdLoginStrategy.class);

//    private List<String> whiteList;
//    private List<String> whitePrefixList;

  @Override
  public String buildLoginUrl(HttpServletRequest request,
      HttpServletResponse response, AuthenticationException exception) {
//        boolean inWhiteList = false;
//        for (String wlist : whiteList) {
//            if (requestURI.equals(wlist)) {
//                inWhiteList = true;
//            }
//        }
//        if (!inWhiteList) {
//            for (String wlist : whitePrefixList) {
//                if (requestURI.startsWith(wlist)) {
//                    inWhiteList = true;
//                }
//            }
//        }
//
//        if (!inWhiteList) {
//            // TODO
//            return null;
//        }

    Cookie[] cookies = request.getCookies();
    String cid = null;    //匿名用户CID
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(CLIENT_ID) && cookie.getValue() != null) {   //未输入的匿名用户
          cid = cookie.getValue();
          LOG.warn("user auto login by cookie " + cid);
        }
      }
    }
    setCurrentUser(cid, request, response);

    String requestURI = request.getRequestURI();
    String query = request.getQueryString();
    if (query != null) {
      return requestURI + "?" + query;
    }

    return requestURI;
  }

  private void setCurrentUser(String cid, HttpServletRequest request,
      HttpServletResponse response) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    String ua = request.getHeader("User-Agent");

    if ((auth == null || auth.getPrincipal() == null || !(auth.getPrincipal() instanceof User))
        && !StringUtils.startsWith(request.getRequestURI(), "/sms/") //sms是内部请求,无需创建匿名用户
        // 爬虫访问无需创建匿名用户
        && !StringUtils.containsIgnoreCase(ua, "Baiduspider") && !StringUtils
        .containsIgnoreCase(ua, "Alibaba.Security") && !StringUtils
        .containsIgnoreCase(ua, "bingbot")) {
      WebApplicationContext webApplicationContext = WebApplicationContextUtils
          .getWebApplicationContext(request.getServletContext());
      UserService userService = (UserService) webApplicationContext.getBean("userService");

      User user = null;
      if (cid != null) {
        user = userService.loadByMoreParam(null, null, null, cid);
      }
      if (user == null) {
        String newCid = UniqueNoUtils.next(UniqueNoType.CID);
        Cookie newCookie = new Cookie(CLIENT_ID, newCid);
        newCookie.setMaxAge(Integer.MAX_VALUE);
        newCookie.setPath("/");
        response.addCookie(newCookie);
        request.setAttribute(CLIENT_ID, newCid);
        user = userService.registerAnonymous(newCid, request.getParameter("partner"));
        user.setUserStatus(UserStatus.ANONYMOUS);  //确定匿名用户

        LOG.warn("create new user and login by cookie " + newCid);
      }

      String currentSellerShopId = request
          .getParameter(UserServletFilter.CURRENT_SELLER_SHOP_ID_PARAM_NAME);
      if (StringUtils.isNotEmpty(currentSellerShopId)) {
        user.setCurrentSellerShopId(currentSellerShopId);
      }

      Authentication auth1 = new UsernamePasswordAuthenticationToken(user, null,
          user.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(auth1);
      //记录用户登录环境Log
      UserSigninLog log = UserSigninLogFactory.createUserSigninLog(request, user);
      UserSigninLogService userSigninLogService = (UserSigninLogService) webApplicationContext
          .getBean("userSigninLogService");
      userSigninLogService.insert(log);
    }
  }

  public static void main(String[] args) {
    boolean flag = StringUtils.containsIgnoreCase(
        "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)", "bingbot1");
    System.out.println(flag);
  }

}
