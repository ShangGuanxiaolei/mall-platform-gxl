package com.xquark.authentication;

import com.xquark.dal.model.User;
import com.xquark.service.wechat.WechatUtils;
import java.io.IOException;
import java.util.Date;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

/** Created by wangxinhua on 17-10-26. DESC: */
public class TokenAuthenticationFilter extends GenericFilterBean {

  private RedisTemplate<String, Object> redisTemplate;

  private static Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

  public TokenAuthenticationFilter(RedisTemplate<String, Object> redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest servletRequest = (HttpServletRequest) request;
    createSessionForMiniProgram(servletRequest);
    chain.doFilter(request, response);
  }

  /**
   * @param request
   * @return
   */
  private boolean createSessionForMiniProgram(HttpServletRequest request) {
    String sessionId = WechatUtils.getSessionId(request);
    if (StringUtils.isBlank(sessionId)) {
      return false;
    }
    // json 走正常流程
    User user;
    try {
      user = (User) redisTemplate.opsForValue().get(sessionId);
      if (user == null) {
        return false;
      }
      renewAuth(user);
    } catch (Exception e) {
      logger.error("sessionId {} json 解析失败", sessionId, e);
      // 解析失败, value不合法则强制删除key重新登录
      redisTemplate.delete(sessionId);
    }
    return false;
  }

  private void renewAuth(User user) {
    if (user.getCreatedAt() == null) {
      user.setCreatedAt(new Date());
    }
    if (user.getUpdatedAt() == null) {
      user.setUpdatedAt(new Date());
    }
    Authentication auth =
        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    SecurityContextHolder.clearContext();
    SecurityContextHolder.getContext().setAuthentication(auth);
  }
}
