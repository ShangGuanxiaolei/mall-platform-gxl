package com.xquark.bos.couponManager;

import com.xquark.dal.status.CouponStatus;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Future;
import java.math.BigDecimal;

/**
 * Created by  on 15-11-5.
 */
public class PromotionCouponCreateForm {

  private String couponId;
  private String create_at;
  @Future(message = "优惠券发行日期不得早于当前时间")
  private String distributed_at;
  @NotBlank(message = "店铺不能为空")
  private String shop_select;
  @NotBlank(message = "批次号不能为空")
  private String batch_no;
  @Future(message = "优惠券开始时间不得早于当前时间")
  private String valid_from;
  @Future(message = "优惠券开始时间不得早于当前时间")
  private String valid_to;
  @NotEmpty(message = "折扣金额不能为空")
  private BigDecimal discount;
  @NotEmpty(message = "发布优惠券数量不能为空")
  private Integer amount;
  @NotEmpty(message = "发布总金额不能为空")
  private BigDecimal total_discount;
  @NotEmpty(message = "领取张数限制不能为空")
  private Integer acquire_limit;
  @NotEmpty(message = "使用张数限制不能为空")
  private Integer apply_limit;
  @NotEmpty(message = "满金额使用该优惠券不能为空")
  private BigDecimal apply_above;
  @NotEmpty(message = "优惠券状态不能为空")
  private CouponStatus status;

  private String couponName;

  private String name;

  private String product_select;

  private String category_select;

  public String getCategory_select() {
    return category_select;
  }

  public void setCategory_select(String category_select) {
    this.category_select = category_select;
  }

  public String getProduct_select() {
    return product_select;
  }

  public void setProduct_select(String product_select) {
    this.product_select = product_select;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCouponName() {
    return couponName;
  }

  public void setCouponName(String couponName) {
    this.couponName = couponName;
  }

  public CouponStatus getStatus() {
    return status;
  }

  public void setStatus(CouponStatus status) {
    this.status = status;
  }

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public Integer getAcquire_limit() {
    return acquire_limit;
  }

  public void setAcquire_limit(Integer acquire_limit) {
    this.acquire_limit = acquire_limit;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public BigDecimal getApply_above() {
    return apply_above;
  }

  public void setApply_above(BigDecimal apply_above) {
    this.apply_above = apply_above;
  }

  public Integer getApply_limit() {
    return apply_limit;
  }

  public void setApply_limit(Integer apply_limit) {
    this.apply_limit = apply_limit;
  }

  public String getBatch_no() {
    return batch_no;
  }

  public void setBatch_no(String batch_no) {
    this.batch_no = batch_no;
  }

  public String getCreate_at() {
    return create_at;
  }

  public void setCreate_at(String create_at) {
    this.create_at = create_at;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  public String getDistributed_at() {
    return distributed_at;
  }

  public void setDistributed_at(String distributed_at) {
    this.distributed_at = distributed_at;
  }

  public String getShop_select() {
    return shop_select;
  }

  public void setShop_select(String shop_select) {
    this.shop_select = shop_select;
  }

  public BigDecimal getTotal_discount() {
    return total_discount;
  }

  public void setTotal_discount(BigDecimal total_discount) {
    this.total_discount = total_discount;
  }

  public String getValid_from() {
    return valid_from;
  }

  public void setValid_from(String valid_from) {
    this.valid_from = valid_from;
  }

  public String getValid_to() {
    return valid_to;
  }

  public void setValid_to(String valid_to) {
    this.valid_to = valid_to;
  }
}
