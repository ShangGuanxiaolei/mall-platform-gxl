package com.xquark.bos.product;

import com.xquark.ResponseObject;
import java.math.BigDecimal;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.xquark.bos.coupon.CouponCreateForm;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CouponStatus;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.mapper.ActivityProductMapper;
import com.xquark.dal.mapper.ProductImageMapper;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.mapper.SkuMappingMapper;
import com.xquark.dal.mapper.TagMapper;
import com.xquark.dal.model.ActivityProduct;
import com.xquark.dal.model.Fragment;
import com.xquark.dal.model.FragmentImage;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductFragment;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuMapping;
import com.xquark.dal.model.Tag;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.vo.FragmentImageVO;
import com.xquark.dal.vo.FragmentVO;
import com.xquark.dal.vo.ProductAdmin;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.FragmentService;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.product.ProductService;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.user.UserService;

@Controller
public class ProductController extends BaseController {

  public static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100L);

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private CategoryMapper categoryMapper;

  @Autowired
  private CategoryActivityMapper categoryActivityMapper;

  @Autowired
  private ProductService productService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private SyncEventService syncEventService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private ActivityProductMapper activityProductMapper;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private SkuMapper skuMapper;

  @Autowired
  private SkuMappingMapper skuMappingMapper;

  @Autowired
  private ProductImageMapper productImageMapper;

  @Autowired
  private TagMapper tagMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private FragmentService fragmentService;

  @Autowired
  private ProductFragmentService productFragmentService;

  @Autowired
  private FragmentImageService fragmentImageService;

  @Value("${site.web.host.name}")
  String siteHost;

  @RequestMapping(value = "product")
  public String list(Model model, HttpServletRequest req) {
    return "product/products";
  }

  @RequestMapping(value = "distribution")
  public String distribution(Model model, HttpServletRequest req) {
    return "product/distriView";
  }

  @RequestMapping(value = "activity")
  public String activity(Model model, HttpServletRequest req) {
    return "product/activity";
  }

  @RequestMapping(value = "/product/syncUpdate")
  public void startFirstSync(String shopId, Boolean syncProd) {
    syncEventService.startUpdateSync(shopId, syncProd);
  }

  @ResponseBody
  @RequestMapping(value = "product/list")
  public Map<String, Object> list(ProductSearchForm form, String order, String direction,
      Pageable pageable) {
    order = StringUtils.defaultIfBlank(order, "onsaleAt");
    direction = StringUtils.defaultIfBlank(direction, "desc");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("archive", ObjectUtils.defaultIfNull(form.getArchive_kwd(), Boolean.FALSE));
    if (StringUtils.isNotBlank(form.getProduct_name_kwd())) {
      params.put("productName", "%" + form.getProduct_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getShop_name_kwd())) {
      params.put("shopName", "%" + form.getShop_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getCreated1_kwd())) {
      params.put("createdFrom", form.getCreated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCreated2_kwd())) {
      params.put("createdTo", form.getCreated2_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated1_kwd())) {
      params.put("updatedFrom", form.getUpdated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated2_kwd())) {
      params.put("updatedTo", form.getUpdated2_kwd());
    }
    params.put("productStatus", form.getProduct_status_kwd());
    params.put("containCloseShop", form.getContain_closed_shop_kwd());
    List<ProductAdmin> products = productService.listProductsByAdmin(params, pageable);
    generateImgUrl(products);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", productService.countProductsByAdmin(params));
    data.put("rows", products);

    return data;
  }

  @ResponseBody
  @RequestMapping(value = "product/categorybylist/{id}/{categoryid}")
  public Map<String, Object> categorybylist(ProductSearchForm form, String order, String direction,
      Pageable pageable, @PathVariable("id") String activityid,
      @PathVariable("categoryid") String categoryid) {

    Map<String, Object> data = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(form.getProduct_name_kwd())) {
      data.put("productName", "%" + form.getProduct_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getShop_name_kwd())) {
      data.put("shopName", "%" + form.getShop_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getCreated1_kwd())) {
      data.put("createdFrom", form.getCreated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCreated2_kwd())) {
      data.put("createdTo", form.getCreated2_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated1_kwd())) {
      data.put("updatedFrom", form.getUpdated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated2_kwd())) {
      data.put("updatedTo", form.getUpdated2_kwd());
    }
    List<ProductAdmin> products = productService
        .listProductsByCategory(data, pageable, activityid, categoryid);
    generateImgUrl(products);
    data.put("total", productService.countProductsByCategory(data, activityid, categoryid));
    data.put("rows", products);
    return data;
  }

  private void generateImgUrl(List<ProductAdmin> products) {
    for (Product product : products) {
      product.setImg(resourceFacade.resolveUrl(product.getImg() + "|" + ResourceFacade.IMAGE_S025));
    }
  }

  @ResponseBody
  @RequestMapping(value = "product/delete")
  public Boolean delete(String[] ids) {
    log.info(super.getCurrentUser().getPhone() + "删除商品productId in:" + ids);
    return productService.deleteByAdmin(ids) == ids.length;
  }

  @ResponseBody
  @RequestMapping(value = "product/undelete")
  public Boolean undelete(String[] ids) {
    log.info(super.getCurrentUser().getPhone() + "恢复商品productId in:" + ids);
    return productService.undeleteByAdmin(ids) == ids.length;
  }

  @RequestMapping(value = "product/view/{id}")
  public String view(@PathVariable("id") String productId, Model model) {
    model.addAttribute("product", productService.loadByAdmin(productId));
    return "product/view";
  }

  @RequestMapping(value = "product/categorydetailview/{id}")
  public String categorydetailview(@PathVariable("id") String activityid, Model model) {
    model.addAttribute("activityid", activityid);
    return "product/categoryManger";
  }

  @RequestMapping(value = "product/productlistview/{id}")
  public String productlistview(@PathVariable("id") String activityid, Model model) {
    List<CategoryActivity> list = categoryActivityMapper
        .listCategorybyActivity(null, null, activityid);
    model.addAttribute("activityid", activityid);
    model.addAttribute("categorylist", list);
    return "product/categoryProductManger";
  }

  @RequestMapping(value = "product/categoryproductlistview/{id}/{categoryid}")
  public String categoryproductlistview(@PathVariable("id") String activityid,
      @PathVariable("categoryid") String categoryid, Model model) {

    model.addAttribute("activityid", activityid);
    model.addAttribute("categoryid", categoryid);
    return "product/categoryRelationManger";
  }

  @RequestMapping(value = "product/batchCategorylistview/{id}")
  public String batchCategorylistview(@PathVariable("id") String activityid, Model model) {
    model.addAttribute("activityid", activityid);
    return "product/batchCategoryManger";
  }


  @RequestMapping(value = "product/shopdetailview/{id}")
  public String shopdetailview(@PathVariable("id") String activityid, Model model) {
    model.addAttribute("activityid", activityid);
    List<Shop> shoplist = shopMapper.listShopByActivity(activityid);
    if (shoplist.size() > 0) {
      model.addAttribute("list", shoplist);
    }
    return "product/shopRelationManger";
  }

  @RequestMapping(value = "/redirectProductView/{productId}")
  public String redirectUserShop(@PathVariable("productId") String productId) {
    return "redirect:" + siteHost + "/p/" + productId;
  }

  @ResponseBody
  @RequestMapping(value = "product/instock")
  public Json instock(String[] ids) {
    Json json = new Json();
    try {
      if (productService.instockByAdmin(ids)) {
        /**
         * 商品下架后，设置分销池商品下架
         */
        for (String id : ids) {
          List<ActivityProduct> aplist = activityProductMapper.selectByproductId(id);
          if (null != aplist && aplist.size() > 0) {
            for (ActivityProduct activityProduct : aplist) {
              // 设置分销池商品下架
              if (!activityProduct.getArchive()) {
                activityProduct.setArchive(true);
                activityProductMapper.update(activityProduct);
              }
              // 更新已分销商品状态(非原商品)--下架
              List<Product> prods = productMapper.listProductsBySourceId(id);
              if (null != prods && prods.size() > 0) {
                for (Product product : prods) {
                  productMapper.updateForStatusBySourceId(product.getSourceProductId(),
                      ProductStatus.INSTOCK + "");
                }
              }
            }
          }
        }

        json.setMsg("商品下架成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("商品下架失败");
      }

    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("下架失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "商品id=[" + ids + "] rc=[" + json.getRc() + "]");
    return json;
  }

  /**
   * 商品分销
   */
  @ResponseBody
  @RequestMapping(value = "product/distribution")
  public Json distribution(String[] ids, String[] commissionRate) {
    Json json = new Json();
    boolean flag = false;
    int updateRes = 0;
    int insertRes = 0;
    try {
      for (String id : ids) {
        Product pro = productMapper.selectByPrimaryKey(id);
        // 如果是上架商品并可用，则添加到分销池
        if (null != pro && ProductStatus.ONSALE.equals(pro.getStatus()) && !pro.getArchive()) {
          flag = false;
        } else {
          flag = true;
          break;
        }
      }
      // 如果有非上架商品则不允许添加到分销池
      if (flag) {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("请选择可用的上架商品");
        return json;
      }
      // 设置商品默认分佣比例

      BigDecimal rate = BigDecimal.ZERO;
      if (null != commissionRate && commissionRate.length > 0) {
        rate = NumberUtils.createBigDecimal(commissionRate[0]);
      }
      boolean updateCommisionRateRet = false;
      updateCommisionRateRet = productMapper.updateCommisionById(ids,
          rate.equals(BigDecimal.ZERO) ? BigDecimal.ZERO : rate.divide(ONE_HUNDRED, 4));

      List<ActivityProduct> list = new ArrayList<ActivityProduct>();
      for (String id : ids) {
        List<ActivityProduct> aplist = activityProductMapper.selectByproductId(id);
        if (null != aplist && aplist.size() > 0) {
          for (ActivityProduct activityProduct : aplist) {
            // 如果该商品已经下架过，则更新商品分销状态为上架
            if (activityProduct.getArchive()) {
              activityProduct.setArchive(false);
              updateRes = activityProductMapper.update(activityProduct);
            }
          }
        } else {
          ActivityProduct entity = new ActivityProduct();
          entity.setProductId(id);
          entity.setProductOrder(entity.getProductOrder());
          entity.setActivityId(entity.getActivityId());
          entity.setArchive(false);
          list.add(entity);
        }

        // 更新已分销商品状态(非原商品)--上架
        List<Product> prods = productMapper.listProductsBySourceId(id);
        if (null != prods && prods.size() > 0) {
          for (Product product : prods) {
            productMapper
                .updateForStatusBySourceId(product.getSourceProductId(), ProductStatus.ONSALE + "");
          }
        }
      }
      if (null != list && list.size() > 0) {
        insertRes = activityProductMapper.insertBatch(list);
      }

      if (updateCommisionRateRet && (updateRes > 0 || insertRes > 0 ||
          (updateRes == 0 && insertRes == 0))) {
        json.setMsg("商品分销成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("商品分销失败");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("分销失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "商品id=[" + ids + "] rc=[" + json.getRc() + "]");
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/shopActivityUpdate")
  public Json shopActivityUpdate(String[] ids, String[] selects, String activityid) {
    Json json = new Json();
    try {
      int ret = 0;
      for (int i = 0; i < ids.length; i++) {
        String shopid = ids[i];
        String isSelect = selects[i];
        if (StringUtils.equals(isSelect, "1")) {
          ret += shopMapper.activityUpdated(shopid, activityid);
        } else {
          ret += shopMapper.activityUpdated(shopid, IdTypeHandler.encode(0));
        }
      }
      if (ret > 0) {
        json.setMsg("操作成功!");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("操作失败!");
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }


  @ResponseBody
  @RequestMapping(value = "product/productCategoryActivityUpdate")
  public Json productCategoryActivityUpdate(String[] ids, String[] sIds, String activityid) {
    Json json = new Json();
    try {
      int ret = 0;
      for (int i = 0; i < sIds.length; i++) {
        String productId = sIds[i];
        for (int j = 0; j < ids.length; j++) {
          String categoryId = ids[j];
          if (categoryActivityMapper.existcategoryforproduct(activityid, productId, categoryId)
              .intValue() == 0) {
            ret += categoryActivityMapper
                .createCategoryforProduct(activityid, productId, categoryId);
          } else {
            ret += categoryActivityMapper
                .updateCategoryforProduct(activityid, productId, categoryId);
          }
        }
      }
      if (ret > 0) {
        json.setMsg("操作成功!");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("操作失败!");
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }


  @ResponseBody
  @RequestMapping(value = "product/deleteProductCategory")
  public Json deleteProductCategory(String[] sIds, String categoryid, String activityid) {
    Json json = new Json();
    try {
      int ret = 0;
      for (int i = 0; i < sIds.length; i++) {
        String productId = sIds[i];
        ret += categoryActivityMapper.deleteCategoryforProduct(activityid, productId, categoryid);
      }
      if (ret > 0) {
        json.setMsg("操作成功!");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("操作失败!");
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }


  @ResponseBody
  @RequestMapping(value = "product/activityList")
  public Map<String, Object> activtyList(ProductSearchForm form, String order, String direction,
      Pageable pageable) {
    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(form.getProduct_name_kwd())) {
      params.put("name", "%" + form.getProduct_name_kwd() + "%");
    }

    if (StringUtils.isNotBlank(form.getCreated1_kwd())) {
      params.put("startTime", form.getCreated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCreated2_kwd())) {
      params.put("endTime", form.getCreated2_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated1_kwd())) {
      params.put("applyStartTime", form.getUpdated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated2_kwd())) {
      params.put("apply_end_time", form.getUpdated2_kwd());
    }

    List<Activity> products = productService.listActivity(params, pageable);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", productService.countAllactivity(params));
    data.put("rows", products);
    return data;
  }

  @ResponseBody
  @RequestMapping(value = "product/categoryActivityList/{activityId}")
  public Map<String, Object> categoryActivityList(ProductSearchForm form, String order,
      String direction, Pageable pageable, @PathVariable("activityId") String activityId) {
    List<CategoryActivity> list = productService.listCategorybyActivity(null, pageable, activityId);
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", productService.countCategorybyActivity(activityId));
    data.put("rows", list);
    return data;
  }


  @ResponseBody
  @RequestMapping(value = "product/categoryAllList")
  public Map<String, Object> categoryAllList(ProductSearchForm form, String order, String direction,
      Pageable pageable) {
    List<Category> list = categoryMapper.listCategorybyAll(null, pageable);
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", categoryMapper.listCategoryCountbyAll());
    data.put("rows", list);
    return data;
  }

  @ResponseBody
  @RequestMapping(value = "product/createCategoryforActivity/{activityId}")
  public Json createCategoryforActivity(CategoryActivityForm form,
      @PathVariable("activityId") String activityId) {

    Map<String, String> params = new HashMap<String, String>();
    params.put("name", form.getName());
    params.put("activityId", activityId);

    Json json = new Json();
    try {
      int ret = 0;
      if (productService.existsNameforCategoryActivity(params).intValue() == 0) {
        ret = productService.createCategoryActivity(params);
      } else {
        ret = -1;
      }
      if (ret > 0) {
        json.setMsg("创建成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        if (ret == -1) {
          json.setMsg("类别名不能重复!");
        } else {
          json.setMsg("创建失败!");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/categoryactivityDelete")
  public Json categoryactivityDelete(String[] ids) {
    Json json = new Json();
    int updateRes = 0;
    try {
      for (String id : ids) {
        updateRes = productService.deleteCategoryActivity(id);
        updateRes = productService.deleteCategoryActivityRelation(id);
      }
      if (updateRes > 0) {
        json.setMsg("类别删除成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("类别删除失败");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("类别删除失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "类别id=[" + ids + "] rc=[" + json.getRc() + "]");
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/batchCategoryCreate")
  public Json batchCategoryCreate(String[] names, String activityId) {

    Json json = new Json();
    try {
      int ret = 0;
      for (String name : names) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("activityId", activityId);

        if (productService.existsNameforCategoryActivity(params).intValue() == 0) {
          ret += productService.createCategoryActivity(params);
        }
      }
      if (ret > 0) {
        json.setMsg("创建成功" + ret);
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("创建失败!");
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/createActivity")
  public Json createActivity(ActivityCreateForm form) {

    Map<String, String> params = new HashMap<String, String>();
    params.put("name", form.getAcName());
    params.put("validfrom", form.getValidFrom() + " 00:00:00");
    params.put("validto", form.getValidTo() + " 23:59:59");
    params.put("createdat", DateFormatUtils.format(Calendar.getInstance(),
        "yyyy-MM-dd HH:mm:ss"));
    Json json = new Json();
    try {
      int ret = 0;
      ret = productService.createActivity(params);
      if (ret > 0) {
        json.setMsg("创建成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        if (ret == -1) {
          json.setMsg("活动名重复,创建失败!");
        } else {
          json.setMsg("创建失败!");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("创建失败;" + e.getMessage());
    } finally {
      log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/distriList")
  public Map<String, Object> distriList(ProductSearchForm form, String order, String direction,
      Pageable pageable) {
    order = StringUtils.defaultIfBlank(order, "onsaleAt");
    direction = StringUtils.defaultIfBlank(direction, "desc");
    Map<String, Object> params = new HashMap<String, Object>();
    Boolean activityStatus = null;
    //params.put("archive", ObjectUtils.defaultIfNull(form.getArchive_kwd(), Boolean.FALSE));
    if (StringUtils.isNotBlank(form.getProduct_name_kwd())) {
      params.put("productName", "%" + form.getProduct_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getShop_name_kwd())) {
      params.put("shopName", "%" + form.getShop_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getCreated1_kwd())) {
      params.put("createdFrom", form.getCreated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCreated2_kwd())) {
      params.put("createdTo", form.getCreated2_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated1_kwd())) {
      params.put("updatedFrom", form.getUpdated1_kwd());
    }
    if (StringUtils.isNotBlank(form.getUpdated2_kwd())) {
      params.put("updatedTo", form.getUpdated2_kwd());
    }
    if (null != form.getProduct_status_kwd()) {
      if (ProductStatus.ONSALE.equals(form.getProduct_status_kwd())) {
        activityStatus = false;
      } else {
        activityStatus = true;
      }
    }
    params.put("productStatus", activityStatus);
    params.put("containCloseShop", form.getContain_closed_shop_kwd());
    List<ProductAdmin> products = productService.listProds2ActivityByAdmin(params, pageable);
    generateImgUrl(products);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", productService.countProds2ActivityByAdmin(params));
    data.put("rows", products);
    return data;
  }

  @ResponseBody
  @RequestMapping(value = "product/deleteActivity")
  public Boolean deleteActivity(String[] ids) {
    log.info(super.getCurrentUser().getPhone() + "删除分销池商品activityId in:" + ids);
    // 修改商品类别和佣金比例
    for (String id : ids) {
      ActivityProduct activityProduct = activityProductMapper.loadById(id);
      productMapper.updateCommisionAndRateById(activityProduct.getProductId(), null, null);
    }
    return activityProductMapper.deleteByIds(ids) == ids.length;
  }


  @ResponseBody
  @RequestMapping(value = "product/activityDelete")
  public Json activityDelete(String[] ids) {
    Json json = new Json();
    int updateRes = 0;
    try {
      for (String id : ids) {
        updateRes = productService.deleteActivity(id);
      }
      if (updateRes > 0) {
        json.setMsg("活动删除成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("活动删除失败");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("活动删除失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "活动id=[" + ids + "] rc=[" + json.getRc() + "]");
    return json;
  }


  @ResponseBody
  @RequestMapping(value = "product/instock2Activity")
  public Json instock2Activity(String[] ids) {
    Json json = new Json();
    int updateRes = 0;
    try {
      /**
       * 设置分销池商品下架
       */
      for (String id : ids) {
        ActivityProduct activityProduct = activityProductMapper.loadById(id);
        if (null != activityProduct) {
          // 设置分销池商品下架
          if (!activityProduct.getArchive()) {
            activityProduct.setArchive(true);
            updateRes = activityProductMapper.update(activityProduct);
            // 更新已分销商品状态(非原商品)--下架
            List<Product> prods = productMapper.listProductsBySourceId(id);
            if (null != prods && prods.size() > 0) {
              for (Product product : prods) {
                productMapper.updateForStatusBySourceId(product.getSourceProductId(),
                    ProductStatus.INSTOCK + "");
              }
            }
          }
        }
      }
      if (updateRes > 0) {
        json.setMsg("商品下架成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("商品下架失败");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("分销池商品下架失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "商品id=[" + ids + "] rc=[" + json.getRc() + "]");
    return json;
  }

  /**
   * 设置分销比例
   */
  @ResponseBody
  @RequestMapping(value = "product/setCommissionRate")
  public Json setCommissionRate(String[] id, String[] commissionRate) {
    Json json = new Json();
    int updateRes = 0;
    try {
      BigDecimal rate = BigDecimal.ZERO;
      if (null != commissionRate && commissionRate.length > 0) {
        rate = NumberUtils.createBigDecimal(commissionRate[0]);
        log.info("literal value:" + commissionRate[0] + "   |||  "
            + "commissionRate value:" + rate + "  |||  " + "divide ret:" + rate
            .divide(ONE_HUNDRED, 4));
      }
      updateRes = productMapper.updateCommisionRateById(id[0],
          rate.equals(BigDecimal.ZERO) ? BigDecimal.ZERO
              : rate.divide(ONE_HUNDRED, 4, BigDecimal.ROUND_DOWN));
      if (updateRes > 0) {
        json.setMsg("修改成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("修改失败");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("分销池商品分销比例修改失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "商品id=[" + id + "] rc=[" + json.getRc() + "]");
    return json;
  }

  /**
   * 批量复制商品
   */
  @ResponseBody
  @RequestMapping(value = "product/copy")
  public Json copy(String[] ids, String shopName) {
    Json json = new Json();
    int countSuccess = 0;
    int countFail = 0;
    int countDist = 0;
    StringBuffer message = new StringBuffer();
    Shop shop = shopMapper.selectByShopName(shopName);

    if (shop != null) {
      List<Product> list = productMapper.selectByIds(ids);
      int rc = 0;
      for (Product product : list) {
        if (product.getIsDistribution() == null || (product.getIsDistribution() != null && !product
            .getIsDistribution())) {
          Product productCopy = new Product();
          BeanUtils.copyProperties(product, productCopy);
          String productId = product.getId();
          productCopy.setId("");
          productCopy.setShopId(shop.getId());
          productCopy.setUserId(shop.getOwnerId());
          List<Sku> skus = skuMapper.selectByProductId(productId);
          List<SkuMapping> skuMapping = skuMappingMapper
              .selectByProductId(productId);
          List<ProductImage> imgs = productImageMapper
              .selectByProductId(productId);
          List<Tag> tags = tagMapper.selectByProductId(productId);

          rc = productService.create(productCopy, skus, tags, imgs,
              skuMapping);
          if (rc == 0) {
            log.info("product copy failture product id = " + productId);
            countFail = countFail + 1;
          } else {
            countSuccess = countSuccess + 1;
            List<ProductFragment> listProductFragment = productFragmentService
                .selectByProductId(productId);
            if (listProductFragment != null) {
              for (int i = 0; i < listProductFragment.size(); i++) {
                ProductFragment productFragmentCopy = new ProductFragment();
                ProductFragment productFragment = listProductFragment.get(i);
                BeanUtils.copyProperties(productFragment, productFragmentCopy);
                productFragmentCopy.setId("");
                productFragmentCopy.setProductId(productCopy.getId());
                productFragmentService.insert(productFragmentCopy);
                Fragment fragmentCopy = new Fragment();
                FragmentVO fragmentVO = fragmentService.selectById(productFragment.getFragmentId());
                BeanUtils.copyProperties(fragmentVO, fragmentCopy);
                fragmentCopy.setId("");
                fragmentCopy.setShopId(shop.getId());
                fragmentService.insert(fragmentCopy);
                List<FragmentImageVO> listFragmentImageVO = fragmentVO.getImgs();
                if (listFragmentImageVO != null) {
                  for (int j = 0; j < listFragmentImageVO.size(); j++) {
                    FragmentImageVO fragmentImageVO = listFragmentImageVO.get(i);
                    FragmentImage fragmentImageCopy = new FragmentImage();
                    BeanUtils.copyProperties(fragmentImageVO, fragmentImageCopy);
                    fragmentImageCopy.setId("");
                    fragmentImageService.insert(fragmentImageCopy);
                  }
                }

              }
            }
          }
        } else {
          countDist = countDist + 1;
        }
      }
    } else {
      log.info("product copy failture shopname = " + shopName
          + "could not find.");
      message.append("店铺名称不存在！ 店铺名：" + shopName);
      json.setRc(Json.RC_FAILURE);
      json.setMsg(message.toString());
      return json;
    }
    message.append("商品复制成功数量：" + countSuccess)
        .append(" 商品复制失败数量：" + countFail + "忽略分销商品数量：" + countDist);

    json.setMsg(message.toString());
    return json;

  }

  public static void main(String[] args) {

    List<String> updateIdsList = new ArrayList<String>();

    updateIdsList.add("123");
    updateIdsList.add("456");
    updateIdsList.add("789");


  }

  @ResponseBody
  @RequestMapping(value = "product/getSkulist/{productid}")
  public Json getSkulist(@PathVariable("productid") String productid) {
    Json json = new Json();
    List<Sku> list = skuMapper.selectByProductId(productid);
    json.setObj(list);
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "product/skuRelationupdate")
  public Json skuRelationupdate(String activityid, String productid, String skuid, String price,
      String rate) {
    Json json = new Json();
    int updateRes = 0;
    try {
      int count = skuMapper.countbySkuActivity(skuid, activityid, productid).intValue();
      if (count == 0) {
        updateRes = skuMapper.createSkuActivity(skuid, activityid, productid, price, rate);
      } else {
        updateRes = skuMapper.updateSkuActivity(skuid, activityid, productid, price, rate);
      }
      if (updateRes > 0) {
        json.setMsg("修改价格成功!");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("修改价格失败!");
      }
    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("sku价格关联;" + e.getMessage());
    }
    log.info(
        super.getCurrentUser().getId() + "sku价格关联id=[" + skuid + "] rc=[" + json.getRc() + "]");
    return json;
  }


}
