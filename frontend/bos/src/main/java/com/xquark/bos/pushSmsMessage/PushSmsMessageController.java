package com.xquark.bos.pushSmsMessage;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;


import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.push.PushService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.mapper.ProductMapper;
import com.xquark.service.shop.ShopService;

/**
 * Created by uzstudio on 2015/9/28.
 */
@Controller
public class PushSmsMessageController extends BaseController {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ShopService shopService;

  @Autowired
  private ProductMapper productMapper;

  @Autowired
  private PushService pushService;

  @RequestMapping(value = "pushSmsMessage")
  public String list(Model model, HttpServletRequest req) {
    return "pushSmsMessage/pushSmsMessage";
  }

  @ResponseBody
  @RequestMapping(value = "/pushsms/send")
  public Json sendSmsMessage(@RequestParam("msgReceiver") String msgReceiver,
      @RequestParam("msgContent") String msgContent) {

    // TODO:need discussion.MandaoSmsClient::mdSmsSend has hide Oudyan group sms message send ability
    String[] mobiles = StringUtils.split(msgReceiver, ",");

    String msg;
    try {
      msg = java.net.URLDecoder.decode(msgContent, "utf-8");
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "解析短信内容失败,Base64字符串:" + msgContent,
          e);
    }

    int sucessCount = 0;
    int failCount = 0;
    for (int i = 0; i < mobiles.length; i++) {
      boolean ret = sendChunkedSmsMessage(mobiles[i], msg);
      if (ret) {
        sucessCount++;
      } else {
        failCount++;
      }
    }
    Json json = new Json();
    if (sucessCount == 0) {
      json.setMsg("发送失败");
    } else if (failCount == 0) {
      json.setMsg("发送成功");
    } else {
      json.setMsg("发送成功:" + sucessCount + "条, 失败:" + failCount + "条");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "/pushsms/preview")
  public Json sendSmsMessagePreview(@RequestParam("msgReceiver") String msgReceiver,
      @RequestParam("msgContent") String msgContent) {
    String mobile = msgReceiver;

    String msg;
    try {
      msg = java.net.URLDecoder.decode(msgContent, "utf-8");
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "解析短信内容失败,Base64字符串:" + msgContent,
          e);
    }

    boolean ret = sendChunkedSmsMessage(mobile, msg);
    Json json = new Json();
    if (ret) {
      json.setMsg("发送预览短信成功");
    } else {
      json.setMsg("发送预览短信失败");
    }
    return json;
  }

  private boolean sendChunkedSmsMessage(String mobileNo, String msgContent) {
        /* disable auto split message chunk because chunk order mess up
        ArrayList msgContentChunks = com.xquark.utils.StringUtil.splitString(msgContent, 140);
        boolean ret = true;
        for (int i = 0; i < msgContentChunks.size(); i++) {
             ret &= pushService.sendSmsEngine(mobileNo, (String)msgContentChunks.get(i));
        }
        return ret;
        */
    return pushService.sendSmsEngine(mobileNo, msgContent);
  }

}
