package com.xquark.bos.couponManager;

import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.model.User;
import com.xquark.dal.model.UserCoupon;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.dal.vo.UserCouponVo;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.user.UserService;
import com.xquark.utils.ExcelUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by  on 15-11-5.
 */
@Controller
public class UserCouponController extends BaseController {

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private UserService userService;

  private final int RC_REMAINDER_NO = 2;

  @RequestMapping("userCoupon")
  public String list(Model model, HttpServletRequest req) {
    return "couponManager/userCoupon";
  }

  @ResponseBody
  @RequestMapping(value = "userCoupon/list")
  public Map<String, Object> list(UserCouponSearchForm form, Pageable pageable) {

    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(form.getStatus_kwd())) {
      params.put("status", form.getStatus_kwd());
    }
    if (StringUtils.isNotBlank(form.getPhone_kwd())) {
      params.put("phone", "%" + form.getPhone_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getSerialNo_kwd())) {
      params.put("SerialNo", "%" + form.getSerialNo_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getAcquired_at_kwd())) {
      params.put("acquiredAt", form.getAcquired_at_kwd());
    }
    if (StringUtils.isNotBlank(form.getApplied_at_kwd())) {
      params.put("appliedAt", form.getApplied_at_kwd());
    }

    List<UserCouponVo> userCoupons = null;
    Long total = userCouponService.countUserCoupons(params);
    if (total.longValue() > 0) {
      userCoupons = userCouponService.list(params, pageable);
    } else {
      userCoupons = new ArrayList<UserCouponVo>();
    }
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", total);
    data.put("rows", userCoupons);

    return data;
  }

  @ResponseBody
  @RequestMapping(value = "userCoupon/validShopCouponList")
  public Map<String, Object> validShopCouponList() {

    //获取可使用店铺优惠券
    List<PromotionCouponVo> shopCouponList = promotionCouponService
        .loadShopPromotionCoupon(null, CouponStatus.VALID.toString());
    Map<String, Object> data = new HashMap<String, Object>();
    Map<String, String> shopDate = new HashMap<String, String>();
    Map<String, List<PromotionCouponVo>> couponDate = new HashMap<String, List<PromotionCouponVo>>();
    for (PromotionCouponVo pcVo : shopCouponList) {
      if (!shopDate.containsKey(pcVo.getId())) {
        shopDate.put(pcVo.getShopId(), pcVo.getShopName());
      }
      List<PromotionCouponVo> relaCouponList = couponDate.get(pcVo.getShopId());
      if (relaCouponList == null) {
        relaCouponList = new ArrayList<PromotionCouponVo>();
        relaCouponList.add(pcVo);
        couponDate.put(pcVo.getShopId(), relaCouponList);
      } else {
        relaCouponList.add(pcVo);
      }
    }
    data.put("shopList", shopDate);
    data.put("couponList", couponDate);
    return data;
  }

  @ResponseBody
  @RequestMapping(value = "userCoupon/sendCoupon")
  public Json sendCoupon(@RequestParam("excelFile") MultipartFile file,
      @RequestParam(required = false) String shopId,
      @RequestParam(required = false) String couponId, HttpServletRequest request,
      HttpServletResponse response) {

    Json json = new Json();
    List<PromotionCouponVo> shopCouponList = null;
    if ((shopId == null || "".equals(shopId))) { //所有店铺的优惠券

      shopCouponList = promotionCouponService
          .loadShopPromotionCoupon(null, CouponStatus.VALID.toString());

    } else if (couponId == null || "".equals(couponId)) {//某店铺的优惠券

      shopCouponList = promotionCouponService
          .loadShopPromotionCoupon(shopId, CouponStatus.VALID.toString());

    } else {  //已选定优惠券
      shopCouponList = new ArrayList<PromotionCouponVo>();
      PromotionCouponVo pcVo = promotionCouponService.load(couponId);
      shopCouponList.add(pcVo);
    }

    if (shopCouponList == null || shopCouponList.size() == 0) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("暂无可使用的店铺优惠券！");
      return json;
    }

    if (!file.isEmpty()) {
      InputStream fileInputStream = null;
      try {
        fileInputStream = file.getInputStream();

        String[][] excelData = ExcelUtils.excelImport(fileInputStream);
        if (excelData == null || excelData.length == 0) {
          log.error("excelData is null");
          json.setRc(Json.RC_FAILURE);
          json.setMsg("Excel 上传数据是空的！");
          return json;
        }
        int count = excelData.length;
        if (count > 1) { //查看店铺剩余优惠券数量是否发放
          if (shopCouponList != null) {
            for (PromotionCouponVo pcVo : shopCouponList) {
              if (pcVo.getRemainder() < count - 1) {
                json.setRc(RC_REMAINDER_NO);
                json.setObj(pcVo);
                json.setMsg(String.valueOf(count - 1));
                return json;
              }
            }
          }
        }
        //查找用户 如果没有该用户则跳过
        int succCount = 0;
        for (int i = 1; i < excelData.length; i++) {

          String userPhone = excelData[i][1];
          boolean isPhone = Pattern.matches("^[1][3-8][0-9]{9}$", userPhone); //验证手机号码

          if (StringUtils.isBlank(userPhone) || !isPhone) {
            continue;
          }
          if (shopCouponList != null) {
            for (PromotionCouponVo pcVo : shopCouponList) {
              UserCoupon userCoupon = ceateUserCoupon(userPhone, pcVo);
              if (userCoupon != null) {
                int result = userCouponService.acquireCoupon(userCoupon);
                if (result > 0) {
                  succCount++;
                }
              }
            }
          }
        }
        int sendNum = (count - 1) * shopCouponList.size();
        if (succCount != sendNum) {
          json.setRc(Json.RC_FAILURE);
          json.setMsg("共有 " + sendNum + " 条上传数据，已成功定向发放 " + succCount + " 条数据！");
        } else {
          json.setRc(Json.RC_SUCCESS);
          json.setMsg("共有 " + sendNum + " 条上传数据，已成功定向发放 " + succCount + " 条数据！");
        }
        return json;
      } catch (Exception e) {
        log.error(e.getMessage(), e);
        json.setRc(Json.RC_FAILURE);
        json.setMsg("服务器数据处理失败，请稍后在试！");
        return json;
      } finally {
        IOUtils.closeQuietly(fileInputStream);
      }
    }
    json.setRc(Json.RC_FAILURE);
    json.setMsg("excel 导入数据定向发送失败，请稍后在试！");
    return null;
  }

  private UserCoupon ceateUserCoupon(String phone, PromotionCouponVo pcVo) {
    UserCoupon uc = null;
    //确定用户
    User user = userService.loadAnonymousByPhone(phone);
    if (user != null) {
      //创建UserCoupon
      uc = new UserCoupon();
      uc.setPhone(phone);
      uc.setStatus(CouponStatus.VALID);
      uc.setCouponId(pcVo.getId());
      uc.setShopId(pcVo.getShopId());
      uc.setUserId(user.getId());
      uc.setRecommendedBy("system sender");
    }
    return uc;
  }
}
