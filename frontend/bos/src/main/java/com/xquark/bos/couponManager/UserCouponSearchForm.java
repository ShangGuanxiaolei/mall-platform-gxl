package com.xquark.bos.couponManager;

import com.xquark.dal.status.CouponStatus;

import java.util.Date;

/**
 * Created by  on 15-11-5.
 */
public class UserCouponSearchForm {

  private String serialNo_kwd;

  private String phone_kwd;

  private String acquired_at_kwd;

  private String applied_at_kwd;

  private String status_kwd;

  public String getAcquired_at_kwd() {
    return acquired_at_kwd;
  }

  public void setAcquired_at_kwd(String acquired_at_kwd) {
    this.acquired_at_kwd = acquired_at_kwd;
  }

  public String getApplied_at_kwd() {
    return applied_at_kwd;
  }

  public void setApplied_at_kwd(String applied_at_kwd) {
    this.applied_at_kwd = applied_at_kwd;
  }

  public String getStatus_kwd() {
    return status_kwd;
  }

  public void setStatus_kwd(String status_kwd) {
    this.status_kwd = status_kwd;
  }

  public String getPhone_kwd() {
    return phone_kwd;
  }

  public void setPhone_kwd(String phone_kwd) {
    this.phone_kwd = phone_kwd;
  }

  public String getSerialNo_kwd() {
    return serialNo_kwd;
  }

  public void setSerialNo_kwd(String serialNo_kwd) {
    this.serialNo_kwd = serialNo_kwd;
  }

}
