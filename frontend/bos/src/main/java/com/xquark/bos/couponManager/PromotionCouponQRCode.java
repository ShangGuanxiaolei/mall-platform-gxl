package com.xquark.bos.couponManager;

/**
 * Created by jason on 15-11-11.
 */
public class PromotionCouponQRCode {

  private String couponId;

  private String distributedAt;

  private String shopName;

  public String getCouponId() {
    return couponId;
  }

  public void setCouponId(String couponId) {
    this.couponId = couponId;
  }

  public String getDistributedAt() {
    return distributedAt;
  }

  public void setDistributedAt(String distributedAt) {
    this.distributedAt = distributedAt;
  }

  public String getShopName() {
    return shopName;
  }

  public void setShopName(String shopName) {
    this.shopName = shopName;
  }
}
