package com.xquark.bos.couponManager;

/**
 * Created by  on 15-11-5.
 */
public class PromotionCouponSearchForm {

  private String batch_no_kwd;
  private String distributed_at_kwd;
  private String shop_name_kwd;
  private String flag_kwd;
  private String used_number_kwd;
  private String coupon_no_kwd;
  private String valid_from_kwd;
  private String valid_to_kwd;
  private String status_kwd;

  public String getStatus_kwd() {
    return status_kwd;
  }

  public void setStatus_kwd(String status_kwd) {
    this.status_kwd = status_kwd;
  }

  public String getBatch_no_kwd() {
    return batch_no_kwd;
  }

  public void setBatch_no_kwd(String batch_no_kwd) {
    this.batch_no_kwd = batch_no_kwd;
  }

  public String getCoupon_no_kwd() {
    return coupon_no_kwd;
  }

  public void setCoupon_no_kwd(String coupon_no_kwd) {
    this.coupon_no_kwd = coupon_no_kwd;
  }

  public String getDistributed_at_kwd() {
    return distributed_at_kwd;
  }

  public void setDistributed_at_kwd(String distributed_at_kwd) {
    this.distributed_at_kwd = distributed_at_kwd;
  }

  public String getFlag_kwd() {
    return flag_kwd;
  }

  public void setFlag_kwd(String flag_kwd) {
    this.flag_kwd = flag_kwd;
  }

  public String getShop_name_kwd() {
    return shop_name_kwd;
  }

  public void setShop_name_kwd(String shop_name_kwd) {
    this.shop_name_kwd = shop_name_kwd;
  }

  public String getUsed_number_kwd() {
    return used_number_kwd;
  }

  public void setUsed_number_kwd(String used_number_kwd) {
    this.used_number_kwd = used_number_kwd;
  }

  public String getValid_from_kwd() {
    return valid_from_kwd;
  }

  public void setValid_from_kwd(String valid_from_kwd) {
    this.valid_from_kwd = valid_from_kwd;
  }

  public String getValid_to_kwd() {
    return valid_to_kwd;
  }

  public void setValid_to_kwd(String valid_to_kwd) {
    this.valid_to_kwd = valid_to_kwd;
  }
}
