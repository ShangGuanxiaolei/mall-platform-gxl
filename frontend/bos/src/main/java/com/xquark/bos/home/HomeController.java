package com.xquark.bos.home;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.bos.BaseController;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.order.OrderService;
import com.xquark.service.pay.PayRequestService;

@Controller
public class HomeController extends BaseController {

  @Autowired
  private PayRequestService payRequestService;

  @Autowired
  private OrderService orderService;


  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String index(Principal principal, @RequestParam(required = false) String accessMode,
      Model model) {
    //OrderService orderService = (OrderService)SpringContextUtil.getBean("orderService");

    if (principal != null && accessMode != null && "XQuark".equals(accessMode)) {
      model.addAttribute("accessMode", "Developer");
    }

    return principal != null ? "home/workshop" : "home/homeNotSignedIn";
    //return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
  }

  @RequestMapping(value = "/workshop", method = RequestMethod.GET)
  public String workshop() {
    return "home/workshop";
  }

  @RequestMapping(value = "/layout/south", method = RequestMethod.GET)
  public String south() {
    return "layout/south";
  }

  @RequestMapping(value = "/layout/north", method = RequestMethod.GET)
  public String north() {
    return "layout/north";
  }

  @ResponseBody
  @RequestMapping(value = "/abcefgaaoo", method = RequestMethod.GET)
  public String initAbcdefg() {
    if (super.getCurrentUser().getPhone().equals("15988475631")) {
      orderService.pay("SO140808111626112351", PaymentMode.ALIPAY, "SO140808111626112351");
      return "success";
    } else {
      return "failed";
    }
  }
}
