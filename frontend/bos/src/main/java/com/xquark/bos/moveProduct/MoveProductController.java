package com.xquark.bos.moveProduct;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.shop.ShopService;

@Controller
public class MoveProductController extends BaseController {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ShopService shopService;

  @RequestMapping(value = "moveProduct")
  public String list(Model model, HttpServletRequest req) {
    return "moveProduct/moveProduct";
  }

  @ResponseBody
  @RequestMapping(value = "/moveProduct/byhand")
  public Json moveProduct(String shopId, String shopUrl) {
    Json json = new Json();
    // get shop owner
    Long userId = shopService.selectShopOwner(shopId);
    if (userId == null) {
      json.setMsg("xquark shop not exist");
    } else {
      log.info(super.getCurrentUser().getId() + " exec moveProduct.shop:"
          + shopId + ",url:" + shopUrl);
      Map<String, Object> result = new HashMap<String, Object>();
      User user = new User();
      user.setId(IdTypeHandler.encode(userId));
      user.setShopId(shopId);
      result = shopService.moveThirdShopProducts(user, shopUrl, shopId, 0, 1);
      json.setRc(Integer.parseInt(result.get("statusCode").toString()));
      switch (Integer.parseInt(result.get("statusCode").toString())) {
        case 501:
          json.setMsg("Not Implemented");
          break;
        case 502:
          json.setMsg("Bad Gateway");
          break;
        case 601:
          json.setMsg("Shop Not Exists");
          break;
        case 602:
          json.setMsg("Unknow Error");
          break;
        case 200:
          json.setMsg("Success");
          break;
        default:
          break;
      }
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "/moveItem/byhand")
  public Json moveItem(String shopId, String itemId) {
    Json json = new Json();
    // get shop owner
    Long userId = shopService.selectShopOwner(shopId);
    if (userId == null) {
      json.setMsg("xquark shop not exist");
    } else {
      log.info(super.getCurrentUser().getId() + " exec moveItem.shop:"
          + shopId + ",itemId:" + itemId);
      Map<String, Object> result = new HashMap<String, Object>();
      User user = new User();
      user.setId(IdTypeHandler.encode(userId));
      user.setShopId(shopId);
      result = shopService.moveThirdItem(user, itemId);
      json.setRc(Integer.parseInt(result.get("statusCode").toString()));
      switch (Integer.parseInt(result.get("statusCode").toString())) {
        case 501:
          json.setMsg("Not Implemented");
          break;
        case 502:
          json.setMsg("Bad Gateway");
          break;
        case 601:
          json.setMsg("Shop Not Exists");
          break;
        case 602:
          json.setMsg("Unknow Error");
          break;
        case 200:
          json.setMsg("Success");
          break;
        default:
          break;
      }
    }
    return json;
  }

}
