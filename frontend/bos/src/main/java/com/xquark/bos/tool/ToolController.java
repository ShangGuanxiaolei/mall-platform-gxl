package com.xquark.bos.tool;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.bos.vo.Json;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.zone.ZoneService;

@Controller
public class ToolController {

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private ResourceFacade resourceFacade;

  @RequestMapping("tool/idTool")
  public String idTool() {
    return "tool/idTool";
  }

  @ResponseBody
  @RequestMapping("tool/encode/{id}")
  public Json encode(@PathVariable String id) {
    Json json = new Json();
    json.setMsg(IdTypeHandler.encode(Long.parseLong(id)));
    return json;
  }

  @ResponseBody
  @RequestMapping("tool/decode/{code}")
  public Json decode(@PathVariable String code) {
    Json json = new Json();
    json.setMsg(String.valueOf(IdTypeHandler.decode(code)));
    return json;
  }

  @RequestMapping("tool/imgTool")
  public String imgTool() {
    return "tool/imgTool";
  }

  @ResponseBody
  @RequestMapping("tool/img/decode/{key}")
  public Json imgDecode(@PathVariable String key) {
    Json json = new Json();
    json.setMsg(resourceFacade.resolveUrl(key));
    return json;
  }

  @ResponseBody
  @RequestMapping("tool/zoneFullName/{zoneId}")
  public String zoneFullName(@PathVariable String zoneId, String street) {
    String addressDetails = "";
    if (StringUtils.isNoneBlank(zoneId)) {
      List<Zone> zoneList = zoneService.listParents(zoneId);
      for (Zone zone : zoneList) {
        addressDetails += zone.getName();
      }
      addressDetails += street;
    }
    return addressDetails;
  }

}
