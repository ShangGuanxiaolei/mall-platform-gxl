package com.xquark.bos.msg;

import com.xquark.dal.type.PushMessageDeviceType;


/**
 * 运营消息发送FORM
 *
 * @author odin
 */
public class MessageForm {

  private String title;

  private String content;

  private String url;

  private PushMessageDeviceType plantForm;

  private String pushType;

  private String appName;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getPushType() {
    return pushType;
  }

  public void setPushType(String pushType) {
    this.pushType = pushType;
  }

  public PushMessageDeviceType getPlantForm() {
    return plantForm;
  }

  public void setPlantForm(PushMessageDeviceType plantForm) {
    this.plantForm = plantForm;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

}
