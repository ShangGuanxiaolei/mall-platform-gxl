package com.xquark.bos.product;

/**
 * Created by freedom on 15/11/18.
 */
public class ActivityCreateForm {

  private String acName;
  private String validFrom;
  private String validTo;

  public String getAcName() {
    return acName;
  }

  public void setAcName(String acName) {
    this.acName = acName;
  }

  public String getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(String validFrom) {
    this.validFrom = validFrom;
  }

  public String getValidTo() {
    return validTo;
  }

  public void setValidTo(String validTo) {
    this.validTo = validTo;
  }
}
