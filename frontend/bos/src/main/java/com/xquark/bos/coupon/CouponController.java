package com.xquark.bos.coupon;

import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.model.Coupon;
import com.xquark.dal.model.CouponActivity;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.CouponStatus;
import com.xquark.service.excel.ExcelService;
import com.xquark.service.pricing.CouponService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CouponController extends BaseController {

  @Autowired
  private CouponService couponService;

  @Autowired
  private ExcelService excelService;

  @RequestMapping("coupon")
  public String list(Model model, HttpServletRequest req) {
    return "coupon/coupon";
  }

  @ResponseBody
  @RequestMapping(value = "coupon/list")
  public Map<String, Object> list(CouponSearchForm form, Pageable pageable) {
    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(form.getCode_kwd())) {
      params.put("code", "%" + form.getCode_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getActivity_title())) {
      params.put("activityid", form.getActivity_title());
    }
    if (StringUtils.isNotBlank(form.getValid1_kwd())) {
      params.put("validfrom", form.getValid1_kwd());
    }
    if (StringUtils.isNotBlank(form.getValid2_kwd())) {
      try {
        Date date = DateUtils.addDays(DateUtils.parseDate(form.getValid2_kwd(), "yyyy-MM-dd"), 1);
        params.put("validto", date);
      } catch (ParseException e) {

      }
    }
    if (StringUtils.isNotBlank(form.getStatus_kwd())) {
      params.put("status", form.getStatus_kwd());
    }
    if (StringUtils.isNotBlank(form.getCashieritem1_kwd())) {
      params.put("cashieritemfrom", form.getCashieritem1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCashieritem2_kwd())) {
      try {
        Date date = DateUtils
            .addDays(DateUtils.parseDate(form.getCashieritem2_kwd(), "yyyy-MM-dd"), 1);
        params.put("cashieritemto", date);
      } catch (ParseException e) {

      }
    }

    List<Coupon> coupons = couponService.listCouponsByAdmin(params,
        pageable);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", couponService.countCouponsByAdmin(params));
    data.put("rows", coupons);

    return data;
  }

  @ResponseBody
  @RequestMapping(value = "coupon/list/exportExcel")
  public void export2Excel(CouponSearchForm form, HttpServletResponse resp) {
    Map<String, Object> params = transForm2Map(form);
    List<Coupon> coupons = couponService.listByAdmin(params, null);
    String sheetStr = "优惠码";
    String filePrefix = "couponList";
    String[] secondTitle = new String[]{"优惠码", "优惠金额", "有效开始时间",
        "有效结束时间", "状态", "创建时间", "付款单号", "付款时间"};
    String[] strBody = new String[]{"getCode", "getDiscount",
        "getValidFromStr", "getValidToStr", "getStatusVal",
        "getCreatedAtStr", "getBizNo",
        "getCashieritemCreatedAtStr"};
    excelService.export(filePrefix, coupons, Coupon.class, sheetStr,
        transParams2Title(params), secondTitle, strBody, resp, true);
  }

  private Map<String, Object> transForm2Map(CouponSearchForm form) {
    Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotBlank(form.getCode_kwd())) {
      params.put("code", "%" + form.getCode_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getActivity_title())) {
      params.put("activityid", form.getActivity_title());
    }
    if (StringUtils.isNotBlank(form.getValid1_kwd())) {
      params.put("validfrom", form.getValid1_kwd());
    }
    if (StringUtils.isNotBlank(form.getValid2_kwd())) {
      params.put("validto", form.getValid2_kwd());
    }
    if (StringUtils.isNotBlank(form.getStatus_kwd())) {
      params.put("status", form.getStatus_kwd());
    }
    if (StringUtils.isNotBlank(form.getCashieritem1_kwd())) {
      params.put("cashieritemfrom", form.getCashieritem1_kwd());
    }
    if (StringUtils.isNotBlank(form.getCashieritem2_kwd())) {
      params.put("cashieritemto", form.getCashieritem2_kwd());
    }
    return params;
  }

  private String transParams2Title(Map<String, Object> params) {
    String result = "";
    if (params != null) {
      Iterator<String> it = params.keySet().iterator();
      int i = 0;
      while (it.hasNext()) {
        String key = it.next();
        Object value = params.get(key);
        if (value != null && !value.equals("")) {
          if (i > 0) {
            result += ";";
          }
          result += "{" + key + "=" + value.toString() + "}";
          i++;
        }
      }
    }
    return result;
  }

  @ResponseBody
  @RequestMapping(value = "coupon/create")
  public Json create(CouponCreateForm form, String count) {
    List<String> listCodes = new ArrayList<String>(Integer.parseInt(count));
    for (int i = 0; i < Integer.parseInt(count); i++) {
      // listCodes.add(UUID.randomUUID().toString()); //TODO 生成方式
      listCodes.add(change(IdTypeHandler.encode((System
          .currentTimeMillis() + 100 + i))));
    }
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("discount", form.getDiscount());
    params.put("activityid", couponService.loadByActCode("XQ.COUPONCODE")
        .getId());
    params.put("validfrom", form.getValidFrom() + " 00:00:00");
    params.put("validto", form.getValidTo() + " 23:59:59");
    params.put("status", CouponStatus.VALID);
    params.put("createdat", DateFormatUtils.format(Calendar.getInstance(),
        "yyyy-MM-dd HH:mm:ss"));
    Json json = new Json();
    try {
      int ret = 0;
      ret = couponService.create(listCodes, params);
      if (ret > 0) {
        json.setMsg("成功生成 " + ret + " 条优惠码");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("生成失败");
      }
    } catch (Exception e) {
      e.printStackTrace();
      json.setRc(Json.RC_FAILURE);
      json.setMsg("生成失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + " rc=[" + json.getRc() + "]");
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "coupon/delete")
  public Json delete(String[] ids) {
    Json json = new Json();
    try {
      if (couponService.delete(ids)) {
        json.setMsg("删除成功");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("删除失败");
      }

    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("删除失败;" + e.getMessage());
    }
    log.info(super.getCurrentUser().getId() + "优惠码id=[" + ids + "] rc=["
        + json.getRc() + "]");
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "coupon/activity")
  public Map<String, Object> listCouponActivity() {
    Map<String, Object> data = new HashMap<String, Object>();

    List<CouponActivity> list = new ArrayList<CouponActivity>();
    list = couponService.listCouponActivityByAdmin();
    data.put("rows", list);

    return data;
  }

  /*
   * 奇数字符随机替换成0-9数字/A-Z字符偶数字符替换成后2位的字符
   */
  public static String change(String str) {
    char[] chr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
        'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    char letters[] = new char[str.length()];
    Random random = new Random();
    for (int i = 0; i < str.length(); i++) {
      char letter = str.charAt(i);
      // 奇偶数替换
      if (i % 2 == 0) {
        if (i < str.length() - 2) {
          letter = str.charAt(i + 2);
        }
        // 替换大小写
        if (letter >= 'a' && letter <= 'z') {
          letter = (char) (letter - 32);
        } else if (letter >= 'A' && letter <= 'Z') {
          letter = (char) (letter + 32);
        }
      } else {
        letter = chr[random.nextInt(36)];
      }
      letters[i] = letter;
    }
    return new String(letters);
  }
}
