package com.xquark.bos.payBatch;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.xquark.bos.vo.Json;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.*;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.service.shop.ShopService;
import com.xquark.utils.EncryptUtil;
import com.xquark.wechat.common.PaySetting;
import com.xquark.wechat.pay.transfer.Transfers;
import com.xquark.wechat.pay.transfer.bean.TransferRequest;
import com.xquark.wechat.pay.transfer.bean.TransferResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.runtime.Runtime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PayRequestBizType;
import com.xquark.dal.type.PayRequestPayType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.alipay.UserAlipayService;
import com.xquark.service.bank.WithdrawApplyService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.ReceiverDetailVO;
import com.xquark.service.outpay.ThirdPartyPayment;
import com.xquark.service.outpay.impl.AliPaymentImpl;
import com.xquark.service.outpay.impl.alipay.AlipayNotify;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pay.PayRequestService;
import com.xquark.service.user.UserService;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PaymentBatchController {

  Logger log = LoggerFactory.getLogger(PaymentBatchController.class);

  @Autowired
  private UserAlipayService userAlipayService;

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private ShopService shopService;

  // 签名方式，选择项：0001(RSA)、MD5
  @Value("${payment.merchant.sign_type.alipay}")
  String sign_type;

  @Value("${payment.merchant.secret.alipay}")
  String secret;

  @Value("${payment.merchant.id.alipay}")
  String partner;
  @Value("${payment.merchant.mail.alipay}")
  String partnerMail;
  @Value("${payment.merchant.userName.alipay}")
  String partnerUserName;

  @Autowired
  private ThirdPartyPayment aliPayment;

  @Autowired
  private ThirdPartyPayment tenPayment;

  @Autowired
  private ThirdPartyPayment umPayment;

  @Autowired
  private WithdrawApplyService withdrawApplyService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private OutPayService outPayService;

  @Autowired
  PayRequestService payRequestService;

  @Autowired
  private OrderService orderService;

  private static List<String> decryptKey = new ArrayList<String>();

  static {
    decryptKey.add("notify_time");
    decryptKey.add("notify_type");
    decryptKey.add("notify_id");
    decryptKey.add("sign_type");
    decryptKey.add("sign");
    decryptKey.add("batch_no");
    decryptKey.add("pay_user_id");
    decryptKey.add("pay_user_name");
    decryptKey.add("pay_account_no");
    decryptKey.add("success_details");
    decryptKey.add("fail_details");
  }

  @RequestMapping("/refundBatch/alipay")
  public void alipayRefundBatch(HttpServletRequest req, HttpServletResponse resp, String orderId) {
    Order order = orderService.load(orderId);
    String orderNo = order.getOrderNo();
    String payNo = order.getPayNo();
    log.info("refundBatchRequest orderNo=[" + orderNo + "]");
    try {
      //194537
      OutPay oldOutPay = outPayService.findByOrderNo4Refund(payNo, "ALIPAY");

//			PayRequest payRequest = trans2PayRequest4Refund(oldOutPay, order);
//			
//			if(payRequestService.onCreate(payRequest)){
//				OutPay newOutPay = trans2OutPay4Refund(oldOutPay, payRequest);
//				outPayService.insert(newOutPay);
//				
      ReceiverDetailVO bean = new ReceiverDetailVO();
      bean.setSwiftNum(oldOutPay.getTradeNo());
      bean.setReceiverFee(order.getRefundFee());
      bean.setReceiverRemark("协商退款,orderNo=" + orderNo);
      bean.setOrderNo(orderNo);

      List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
      list.add(bean);
      aliPayment.refundBatchRequest(req, resp, list);
      log.info("refundBatchRequest success orderNo=[" + orderNo + "]");
//				payRequestService.onPay(payRequest);
//			}else{
//				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
//						"子账户余额不足，请求失败");
//			}
    } catch (Exception e) {
      log.info("refundBatchRequest failed orderNo=[" + orderNo + "]");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "即时到账批量退款，请求失败", e);
    }
  }


  @SuppressWarnings({"unchecked", "rawtypes"})
  @RequestMapping(AliPaymentImpl.refundBatch_notify_url + "/{orderNo}")
  public void alipayRefundBatchNotify(@PathVariable("orderNo") String orderNo,
      HttpServletRequest req,
      HttpServletResponse resp) {
//		log.info("alipay refund notify url is called " + request);
    try {
      Map<String, String> nitofyMsg = aliPayment.refundBatchNotify(req, resp);
      if (nitofyMsg == null) {
        log.error("alipayRefundBatchNotify 解析支付宝notify信息出错");
        return;
      }
      Map<String, Object> map = transDrawBackNotifyResult(nitofyMsg);
      //String transNo = map.get("batchNo").toString();
      //String successNum = map.get("successNum").toString();
      List<ReceiverDetailVO> list = (List) map.get("details");
      if (list != null && list.size() > 0) {
        for (int i = 0; i < list.size(); i++) {
          ReceiverDetailVO bean = list.get(i);
          String tradeNo = bean.getSwiftNum();
//					OutPay outpay = outPayService.findOutPayByOldTradeNo(tradeNo, "ALIPAY");
//					String orderNo = outpay.getBillNo();
//					PayRequest payRequest = payRequestService.queryRefundByOutPayId(outpay.getId());

          if (bean.getStatus().equals("SUCCESS")) {
            OutPay oldOutPay = outPayService.findOutPayByTradeNo(tradeNo, "ALIPAY");
            Order order = orderService.loadByOrderNo(orderNo);

            PayRequest payRequest = trans2PayRequest4Refund(oldOutPay, order);

            //新的outpay
            OutPay newOutPay = trans2OutPay4Refund(oldOutPay, payRequest);
            newOutPay.setRequestId("0");
            newOutPay.setOutStatus("SUCCESS");
            newOutPay.setStatus(PaymentStatus.SUCCESS);
            newOutPay.setTradeNo(bean.getSwiftNum());
            newOutPay.setDetail(nitofyMsg.toString().getBytes());
            //该操作无外部交易号
            outPayService.insert(newOutPay);
            orderService.updateOrderRefundByAdmin(order.getId());
//						}
          }
        }
      }
      resp.getWriter().print("success");
    } catch (Exception e) {
      log.error("支付宝回调信息处理失败", e);
    }
  }

  @RequestMapping("/paymentBatch/wechatpay")
  @ResponseBody
  public Json wechatpayBatch(HttpServletRequest req, HttpServletResponse resp,
      String withdrawApplyIds, String withdrawconfirmMoneys) {
    Json json = new Json();
    try {
      String[] ids = withdrawApplyIds.split(",");
      String[] confirmMoneys = withdrawconfirmMoneys.split(",");

      for (int i = 0; i < ids.length; i++) {
        WithdrawApply withdrawApply = withdrawApplyService.load(ids[i]);
        withdrawApply.setConfirmMoney(NumberUtils.createBigDecimal(confirmMoneys[i]));

        if (withdrawApply.getApplyMoney().compareTo(withdrawApply.getConfirmMoney()) < 0) {
          throw new BizException(GlobalErrorCode.UNKNOWN,
              withdrawApply.getAccountName() + " 核准金额=" + withdrawApply.getConfirmMoney()
                  + " 不能大于申请金额：" + withdrawApply.getApplyMoney());
        }

        User wechatToUser = userService.load(withdrawApply.getUserId());
        if (wechatToUser == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "当前打款用户无微信信息");
        }
        withdrawApplyService.withdrawPendingByApplyNo(withdrawApply.getApplyNo());

        //TODO 1 get the wechat enterprise pay config(according to userid to find paysetting)
        //     2 pay to  wechat
        //     3 update  withdraw
        PaySetting paySetting = new PaySetting();
        // 从数据库中读取配置好的该公众号证书等相关信息
        String rootShopId = getCurrentUser().getShopId();
        WechatAppConfig wechatAppConfig = wechatAppConfigMapper.selectByShopId(rootShopId);
        paySetting.setAppId(wechatAppConfig.getAppId());
        paySetting.setMchId(wechatAppConfig.getMchId());
        paySetting.setKey(wechatAppConfig.getMchKey());
        //证书存储在数据库中
        byte[] cert = wechatAppConfig.getCert_file();
        //私钥（在安装证书时设置）
        String password = wechatAppConfig.getCert_password();
        if (cert == null || StringUtils.isEmpty(password)) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "微信配置证书或密码为空");
        }
        // 证书,证书密码需要解密后使用
        EncryptUtil des1 = new EncryptUtil();
        password = des1.decrypt(password);
        cert = des1.decrypt(cert);
        paySetting.setCertPassword(password);
        paySetting.setCertFile(cert);
        PaySetting.setDefault(paySetting);
        TransferRequest transferRequest = new TransferRequest();
        BigDecimal amount = withdrawApply.getConfirmMoney().multiply(new BigDecimal(100));
        transferRequest.setAmount(amount.intValue());
        transferRequest.setCheckName("NO_CHECK");
        transferRequest.setClientIp("127.0.0.1");
        transferRequest.setDesc("分佣");
        transferRequest.setOpenId(wechatToUser.getLoginname());
        transferRequest.setPartnerTradeNo(withdrawApply.getApplyNo());
        // 调用微信付款接口
        TransferResponse transferResponse = Transfers.defaultTransfers().transfer(transferRequest);
        if (transferResponse.success()) {
          PayRequest request = trans2PayRequest4PayBatch(withdrawApply);
          request.setOutpayType(PaymentMode.WEIXIN.toString());
          request.setOutpayInfo(wechatToUser.getWeixinCode());
          if (payRequestService.onCreate(request)) {
            payRequestService.onPay(request);
            payRequestService.onSuccess(request);
            log.info("pay request success, withdraw applyNo:" + transferResponse.getPaymentNo());
            OutPay snapshot = trans2OutPay4PayBatch(request);
            snapshot.setOutStatus("SUCCESS");
            snapshot.setRequestId(request.getId());
            snapshot.setStatus(PaymentStatus.SUCCESS);
            snapshot.setTradeNo(transferResponse.getPaymentNo());
            snapshot.setDetail(transferResponse.getReturnMessage().getBytes());
            outPayService.insert(snapshot);
            log.info("outpay success, withdraw applyNo:" + transferResponse.getPaymentNo());
            withdrawApplyService.withdrawSuccessByNo(transferResponse.getPaymentNo(),
                new BigDecimal(transferRequest.getAmount() / 100));
            json.setRc(Json.RC_SUCCESS);
            json.setMsg("打款成功");
          } else {
            json.setRc(Json.RC_FAILURE);
            json.setMsg("打款生成支付凭证失败");
          }
        } else {
          json.setRc(Json.RC_SUCCESS);
          json.setMsg("微信打款失败：ApplyNO=" + withdrawApply.getApplyNo() + " msg:" + transferResponse
              .getErrorCodeDesc());
          withdrawApplyService.withdrawFailedByApplyNo(withdrawApply.getApplyNo());
        }

      }

    } catch (Exception e) {
      json.setRc(Json.RC_FAILURE);
      json.setMsg("操作失败");
      // log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "批量付款到微信用户，请求失败", e);
    }
    return json;
  }

  @RequestMapping("/paymentBatch/alipay")
  public void alipayBatch(HttpServletRequest req, HttpServletResponse resp,
      String withdrawApplyIds, String withdrawconfirmMoneys) {
    try {
      String[] ids = withdrawApplyIds.split(",");
      String[] confirmMoneys = withdrawconfirmMoneys.split(",");

      List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
      for (int i = 0; i < ids.length; i++) {
        WithdrawApply withdrawApply = withdrawApplyService.load(ids[i]);
        withdrawApply.setConfirmMoney(NumberUtils.createBigDecimal(confirmMoneys[i]));

        if (withdrawApply.getApplyMoney().compareTo(withdrawApply.getConfirmMoney()) < 0) {
          throw new BizException(GlobalErrorCode.UNKNOWN,
              withdrawApply.getAccountName() + " 核准金额=" + withdrawApply.getConfirmMoney()
                  + " 不能大于申请金额：" + withdrawApply.getApplyMoney());
        }

        ReceiverDetailVO bean = new ReceiverDetailVO();
        bean.setSwiftNum(withdrawApply.getApplyNo());
        UserAlipay userAlipay = userAlipayService.loadByUserId(withdrawApply.getUserId());
        if (userAlipay == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "当前打款用户无支付宝信息");
        }
        bean.setReceiverAccount(withdrawApply.getAccountNumber());
        bean.setReceiverName(withdrawApply.getAccountName());
        bean.setReceiverFee(withdrawApply.getConfirmMoney());
        bean.setReceiverRemark(ids[i]);
        withdrawApplyService.withdrawPendingByApplyNo(withdrawApply.getApplyNo());

        list.add(bean);
      }
      aliPayment.payBatchRequest(req, resp, list);
    } catch (Exception e) {
      // log.error("支付请求失败，tradeNo=" + request.getTradeNo(), e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "批量付款到支付宝用户，请求失败", e);
    }
  }

  @RequestMapping(AliPaymentImpl.paymentBatch_notify_url)
  public void alipayBatchNotify(HttpServletRequest req,
      HttpServletResponse resp) {
    try {
      Map<String, String> notifyMsg = aliPayment.payBatchNotify(req, resp);
      if (notifyMsg == null) {
        log.error("alipayBatchNotify 解析支付宝notify信息出错");
        return;
      }
      List<ReceiverDetailVO> list = transPayRequestNotifyResult(notifyMsg);
      if (list != null && list.size() > 0) {
        for (int i = 0; i < list.size(); i++) {
          ReceiverDetailVO bean = list.get(i);
          WithdrawApply withdrawApply = withdrawApplyService.loadByApplyNo(bean.getSwiftNum());
          if (withdrawApply.getStatus() == WithdrawApplyStatus.SUCCESS) {
            continue;
          }
          if (bean.getStatus().equals("S")) {
            PayRequest request = trans2PayRequest4PayBatch(withdrawApply);
            request.setOutpayType(PaymentMode.ALIPAY.toString());
            UserAlipay userAlipay = userAlipayService.loadByUserId(withdrawApply.getUserId());
            request.setOutpayInfo(userAlipay.getAccount());
            if (payRequestService.onCreate(request)) {
              payRequestService.onPay(request);
              payRequestService.onSuccess(request);
              log.info("pay request success, withdraw applyNo:" + bean.getSwiftNum());
              OutPay snapshot = trans2OutPay4PayBatch(request);
              snapshot.setOutStatus("SUCCESS");
              snapshot.setRequestId(request.getId());
              snapshot.setStatus(PaymentStatus.SUCCESS);
              snapshot.setTradeNo(bean.getAlipaySwiftNum());
              snapshot.setDetail(notifyMsg.toString().getBytes());
              outPayService.insert(snapshot);
              log.info("outpay success, withdraw applyNo:" + bean.getSwiftNum());
              withdrawApplyService.withdrawSuccessByNo(bean.getSwiftNum(), bean.getReceiverFee());
            }
          } else {
            log.error("支付宝打款失败：ApplyNO=" + withdrawApply.getApplyNo() + " msg:" + bean.getMsg());
            withdrawApplyService.withdrawFailedByApplyNo(withdrawApply.getApplyNo());
          }
        }
      }
      resp.getWriter().print("success");
    } catch (Exception e) {
      log.error("支付宝回调信息处理失败", e);
    }
  }

  private List<ReceiverDetailVO> transPayRequestNotifyResult(Map<String, String> nitofyMsg) {
    List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
    //根据回传的值，得到每一笔支付的处理结果
    try {
      if (sign_type.equals("0001")) {
        nitofyMsg = AlipayNotify.decryptByKey(nitofyMsg, secret, decryptKey);
      }
      String successDetails = nitofyMsg.get("success_details");
      String failDetails = nitofyMsg.get("fail_details");
      list.addAll(splitPayRequestBatchDetails(successDetails));
      list.addAll(splitPayRequestBatchDetails(failDetails));
    } catch (Exception e) {
      log.error("paymentBatch verify ok, but parse data failed.", e);
    }
    return list;
  }

  private Map<String, Object> transDrawBackNotifyResult(Map<String, String> nitofyMsg) {
    Map<String, Object> result = new HashMap<String, Object>();
    //根据回传的值，得到每一笔支付的处理结果
    try {
      if (sign_type.equals("0001")) {
        nitofyMsg = AlipayNotify.decryptByKey(nitofyMsg, secret, decryptKey);
      }
      result.put("batchNo", nitofyMsg.get("batch_no"));
      result.put("successNum", nitofyMsg.get("success_num"));
      String details = nitofyMsg.get("result_details");
      List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
      list.addAll(splitDrawBackBatchDetails(details));
      result.put("details", list);
    } catch (Exception e) {
      log.error("paymentBatch verify ok, but parse data failed.", e);
    }
    return result;
  }

  private List<ReceiverDetailVO> splitPayRequestBatchDetails(String detailStr) {
    List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
    if (detailStr != null && detailStr.length() > 0) {
      String[] details = detailStr.split("\\|");
      for (int i = 0; i < details.length; i++) {
        String[] temp = details[i].split("\\^");
        ReceiverDetailVO bean = new ReceiverDetailVO();
        bean.setSwiftNum(temp[0]);
        bean.setReceiverAccount(temp[1]);
        bean.setReceiverName(temp[2]);
        bean.setReceiverFee(new BigDecimal(temp[3]));
        bean.setStatus(temp[4]);
        bean.setMsg(temp[5]);
        bean.setAlipaySwiftNum(temp[6]);
        bean.setPayTime(temp[7]);
        list.add(bean);
      }
    }
    return list;
  }

  private List<ReceiverDetailVO> splitDrawBackBatchDetails(String detailStr) {
    List<ReceiverDetailVO> list = new ArrayList<ReceiverDetailVO>();
    if (detailStr != null && detailStr.length() > 0) {
      String[] details = detailStr.split("\\|");
      for (int i = 0; i < details.length; i++) {
        String[] temp = details[i].split("\\^");
        ReceiverDetailVO bean = new ReceiverDetailVO();
        bean.setSwiftNum(temp[0]);  //原交易号
        bean.setReceiverFee(new BigDecimal(temp[1]));  //退款金额
        bean.setStatus(temp[2]);//处理结果
        list.add(bean);
      }
    }
    return list;
  }

  private PayRequest trans2PayRequest4Refund(OutPay outpay, Order order) {
    String payNo = payRequestApiService.generatePayNo();
    SubAccount toAccount = accountApiService
        .findSubAccountByUserId(outpay.getUserId(), AccountType.AVAILABLE);
    SubAccount fromAccount = accountApiService
        .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
    PayRequest request = new PayRequest(payNo, outpay.getBillNo(),
        PayRequestBizType.REFUND, PayRequestPayType.REFUND,
        outpay.getAmount(), fromAccount.getId(),
        toAccount.getId(), null);
    return request;
  }

  private OutPay trans2OutPay4Refund(OutPay oldOutPay, PayRequest payRequest) {
    OutPay aOp = new OutPay();
    aOp.setUserId(oldOutPay.getUserId());
    aOp.setOutId(oldOutPay.getOutId()); // WEIXIN微信支付  UNION银联支付 TENPAY财付通支付 ALIPAY支付宝
    aOp.setpOutpayId("0");        // 父亲支付ID，用于批量打款和批量退款中的子支付记录
    aOp.setRequestId(payRequest.getId());          //
    aOp.setForOutpayId(oldOutPay.getId());        // 仅用于原路退回的操作，关联原始支付的OutPayId

    aOp.setOutAccountId(oldOutPay.getOutAccountId());        // 三方支付帐号编号
    aOp.setOutAccountName(oldOutPay.getOutAccountName());  // 支付帐号名称
    aOp.setOutStatus("SUBMITTED");                  //  支付请求的状态，这个状态不是必须的，对于支付宝就是一个字符串描述
    aOp.setOutstatusex("");                      //  外部交易扩展状态
    aOp.setDetail("".getBytes());   // byte[]);
    aOp.setBillNo(payRequest.getBizId());        //  商户订单号
    aOp.setTradeNo("");      // 第三方交易号

    aOp.setStatus(
        PaymentStatus.PENDING);      //系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setOutpayType(PayRequestBizType.REFUND.toString());
    aOp.setOutpayTypeEx(
        "ADMIN_REFUND"); // 支付类型，USER_PAY用户即时到账,ADMIN_REFUND运营退款，ADMIN_WITHDRAW运营打款，信用还款CREDIT_REPAYMENT，CREDIT_REFUND_AFTER_REPAYMENT还款后的退款
    aOp.setAmount(oldOutPay.getAmount());
    aOp.setUpdatedAt(new Date());
    aOp.setCreatedAt(new Date());
    return aOp;
  }

  private PayRequest trans2PayRequest4PayBatch(WithdrawApply withdrawApply) {
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
        .findSubAccountByUserId(withdrawApply.getUserId(), AccountType.WITHDRAW);
    SubAccount toAccount = accountApiService
        .findSubAccountByUserId(userService.loadKkkdUserId(), AccountType.WITHDRAW);
    PayRequest request = new PayRequest(payNo, withdrawApply.getApplyNo(),
        PayRequestBizType.WITHDRAW, PayRequestPayType.WITHDRAW,
        withdrawApply.getApplyMoney(), fromAccount.getId(),
        toAccount.getId(), null);
    return request;
  }

  private OutPay trans2OutPay4PayBatch(PayRequest payRequest) {
    OutPay aOp = new OutPay();
    aOp.setUserId(payRequest.getToSubAccountId());
    aOp.setOutId(payRequest.getOutpayType()); // WEIXIN微信支付  UNION银联支付 TENPAY财付通支付 ALIPAY支付宝
    aOp.setpOutpayId("0");        // 父亲支付ID，用于批量打款和批量退款中的子支付记录
    aOp.setRequestId(payRequest.getId());          //
    aOp.setForOutpayId("0");        // 仅用于原路退回的操作，关联原始支付的OutPayId

    aOp.setOutAccountId(payRequest.getFromSubAccountId());        // 三方支付帐号编号
    aOp.setOutAccountName(partnerMail);  // 支付帐号名称
    aOp.setOutStatus("SUBMITTED");                  //  支付请求的状态，这个状态不是必须的，对于支付宝就是一个字符串描述
    aOp.setOutstatusex("");                      //  外部交易扩展状态
    aOp.setDetail("".getBytes());   // byte[]);
    aOp.setBillNo("");        //  商户订单号
    aOp.setTradeNo("");      // 第三方交易号

    aOp.setStatus(
        PaymentStatus.PENDING);      //系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setOutpayType(PayRequestBizType.WITHDRAW.toString());
    aOp.setOutpayTypeEx(
        "ADMIN_WITHDRAW"); // 支付类型，USER_PAY用户即时到账,ADMIN_REFUND运营退款，ADMIN_WITHDRAW运营打款，信用还款CREDIT_REPAYMENT，CREDIT_REFUND_AFTER_REPAYMENT还款后的退款
    aOp.setAmount(payRequest.getAmount());
    aOp.setUpdatedAt(new Date());
    aOp.setCreatedAt(new Date());
    return aOp;
  }

  /**
   * 获取当前用户信息 如果是未登录的匿名用户，系统根据匿名用户唯一码自动创建一个用户 具体逻辑查看：UniqueNoFilter
   */
  public User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      } else if (principal instanceof Merchant) {
        Shop shop = shopService.load(((Merchant) principal).getShopId());
        return userService.load(shop.getOwnerId());
      }
      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

}
