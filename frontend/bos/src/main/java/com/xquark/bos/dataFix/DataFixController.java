package com.xquark.bos.dataFix;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xquark.bos.vo.Json;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.SpiderItem;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.test.TestService;

@Controller
public class DataFixController {

  @Autowired
  private TestService testService;

  @RequestMapping(value = "dataFix")
  public String list(Model model, HttpServletRequest req) {
    return "dataFix/dataFix";
  }

  @ResponseBody
  @RequestMapping(value = "/fixData/SpiderList")
  public Map<String, Object> list1(String shop_id, Pageable pageable) {
    Map<String, Object> params = new HashMap<String, Object>();
    if (shop_id == null) {
      params.put("shop_id", shop_id);
    } else {
      params.put("shop_id", IdTypeHandler.encode(Long.parseLong(shop_id)));
    }

    List<SpiderItem> items = testService.listItemsByAdmin(params, pageable);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", testService.countItemsByAdmin(params));
    data.put("rows", items);

    return data;
  }

  @ResponseBody
  @RequestMapping(value = "/fixData/xquarkList")
  public Map<String, Object> list2(String shop_id, Pageable pageable) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("shop_id", shop_id);

    List<Product> products = testService.listProductsByAdmin(params, pageable);

    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", testService.countProductsByAdmin(params));
    data.put("rows", products);

    return data;
  }

  @ResponseBody
  @RequestMapping(value = "/fixData/shop")
  public Json shopid(String from, String to) {
    Json json = new Json();
    int iFrom = Integer.parseInt(from);
    int iTo = Integer.parseInt(to);
    try {
      json.setMsg(testService.selectShopIdByAdmin(iFrom, iTo).toString());
    } catch (Exception e) {
      json.setMsg("-1");
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "/fixData/update")
  public Json update(String productid, String thirdid) {
    Json json = new Json();
    switch (testService.updateThirdId(productid, thirdid)) {
      case -1:
        json.setMsg("淘宝id有误,未存在spider");
        break;
      case -2:
        json.setMsg("淘宝id有误,已存在xquark");
        break;
      case 1:
        json.setMsg("保存成功");
        break;
      default:
        json.setMsg("保存失败");
        break;
    }
    return json;
  }
}
