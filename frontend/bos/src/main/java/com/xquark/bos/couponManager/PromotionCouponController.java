package com.xquark.bos.couponManager;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.xquark.bos.BaseController;
import com.xquark.bos.vo.Json;
import com.xquark.dal.model.Category;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopTree;
import com.xquark.dal.model.promotion.Coupon;
import com.xquark.dal.status.CouponStatus;
import com.xquark.dal.type.UserCouponType;
import com.xquark.dal.vo.PromotionCouponVo;
import com.xquark.service.category.CategoryService;
import com.xquark.service.coupon.PromotionCouponService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.product.ProductService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by  on 15-11-5.
 */
@Controller
public class PromotionCouponController extends BaseController {

  @Autowired
  private PromotionCouponService promotionCouponService;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Value("${site.web.host.name}")
  String siteHost;

  @RequestMapping("couponManager")
  public String list(Model model, HttpServletRequest req) {
    return "couponManager/couponManager";
  }

  @ResponseBody
  @RequestMapping(value = "couponManager/list")
  public Map<String, Object> list(PromotionCouponSearchForm form, Pageable pageable) {

    Map<String, Object> params = new HashMap<String, Object>();

    if (StringUtils.isNotBlank(form.getBatch_no_kwd())) {
      params.put("batchNo", "%" + form.getBatch_no_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getCoupon_no_kwd())) {
      params.put("code", "%" + form.getCoupon_no_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getShop_name_kwd())) {
      params.put("shopName", "%" + form.getShop_name_kwd() + "%");
    }
    if (StringUtils.isNotBlank(form.getUsed_number_kwd())) {
      params.put("flag", form.getFlag_kwd());
      params.put("useNumber", form.getUsed_number_kwd());
    }
    if (StringUtils.isNotBlank(form.getValid_from_kwd())) {
      params.put("validFrom", form.getValid_from_kwd());
    }
    if (StringUtils.isNotBlank(form.getValid_to_kwd())) {
      params.put("validTo", form.getValid_to_kwd() + " 23:59:59");
    }
    if (StringUtils.isNotBlank(form.getDistributed_at_kwd())) {
      params.put("distributedAt", form.getDistributed_at_kwd());
    }
    if (StringUtils.isNotBlank(form.getStatus_kwd())) {
      params.put("status", form.getStatus_kwd());
    }
    //扫描更新优惠券状态
    promotionCouponService.autoClosePromotionCoupon();

    List<PromotionCouponVo> promotionCouponVos = null;
    Long total = promotionCouponService.countPromotionCoupon(params);
    if (total.longValue() > 0) {
      promotionCouponVos = promotionCouponService.list(params, pageable);
    } else {
      promotionCouponVos = new ArrayList<PromotionCouponVo>();
    }
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("total", total);
    data.put("rows", promotionCouponVos);
    return data;
  }

  @ResponseBody
  @RequestMapping(value = "couponManager/update")
  public Json update(PromotionCouponCreateForm form) {

    String couponId = "";
    if (form != null) {
      couponId = form.getCouponId();
    }
    Json json = new Json();

//        String[] notInStatusArr = new String[]{CouponStatus.OVERDUE.toString(),CouponStatus.CLOSED.toString(),CouponStatus.USED.toString()};
//        long coupons = userCouponService.countPromotionCoupons(notInStatusArr,couponId);//修改优惠券状态为关闭或者过期时需验证是否用用户卷在使用
//        if( coupons > 0){
//          json.setRc(Json.RC_FAILURE);
//          json.setMsg("修改失败（该优惠券存在未使用的用户卷，不允许擅自更改状态！）");
//          return json;
//        }

    Map<String, Object> params = parsePromotionCouponCreateForm(form);
    try {
      int ret = 0;
      // 修改优惠券时，同时需要处理剩余数量字段，否则会导致实际能领券人数不对
      PromotionCouponVo old = promotionCouponService.load(couponId);
      int oldRemainder = old.getRemainder();
      int oldAmount = old.getAmount();

      int nowAmount = form.getAmount();
      int newRemainder = oldRemainder + (nowAmount - oldAmount);
      if (newRemainder < 0) {
        newRemainder = 0;
      }
      params.put("remainder", newRemainder);

      // 如果修改了优惠券的状态，由正常变更为过期或作废，则需要同步更新所有用户的优惠券信息
      // 同时需要将那些使用了此优惠券的提交未支付的订单，优惠减去
      if (old.getStatus() == CouponStatus.VALID && (form.getStatus() == CouponStatus.OVERDUE
          || form.getStatus() == CouponStatus.CLOSED)) {
        userCouponService.updateStatusByCouponId(couponId, form.getStatus().toString());
        promotionCouponService.updateMainOrderDiscount(couponId, old.getDiscount());
        promotionCouponService.updateOrderDiscount(couponId, old.getDiscount());
      }

      ret = promotionCouponService.modifyPromotionCoupon(params, couponId);
      if (ret > 0) {
        json.setRc(Json.RC_SUCCESS);
        json.setMsg("优惠券修改成功！");
      } else {
        json.setRc(Json.RC_FAILURE);
        json.setMsg("优惠券修改失败!");
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      json.setRc(Json.RC_FAILURE);
      json.setMsg("修改失败;" + e.getMessage());
    }
    return json;
  }

  @ResponseBody
  @RequestMapping(value = "couponManager/load")
  public Map<String, Object> load(@RequestParam(required = false) String couponId) {

    //店铺列表数据
    List<Shop> shops = shopService.listAllShops();
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("shops", shops);

    List<Product> products = productService.listAllProductsByOnsaleAt(null, null);
    data.put("products", products);

    ShopTree shopTree = shopTreeService.getRootShop();
    List<Category> categories = categoryService
        .loadCategoriesByShopIdNotAll(shopTree.getAncestorShopId(), 1, "");
    data.put("categorys", categories);

    if (couponId == null) {  //新建优惠券
      //发行时间
      Date date = new Date();
      SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
      String distributedDate = format.format(date);
      data.put("distributedDate", distributedDate);

      //批次号
      String batchNo = nextBatchNo();
      data.put("batchNo", batchNo);

    } else {   //修改优惠券
      Coupon coupon = promotionCouponService.load(couponId);
      data.put("coupon", coupon);
    }
    return data;
  }


  @ResponseBody
  @RequestMapping(value = "couponManager/create")
  public String create(PromotionCouponCreateForm form) {

    if (form != null && form.getStatus() == null) {
      form.setStatus(CouponStatus.VALID);
    }
    Map<String, Object> params = parsePromotionCouponCreateForm(form);
    String respResult = null;
    try {
      int ret = promotionCouponService.create(params);
      if (ret > 0) {
        respResult = "200";  //优惠券创建成功
      } else {
        respResult = "206";  //优惠券创建失败
      }
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
      respResult = "500";
    }

    return respResult;
  }

  private Map<String, Object> parsePromotionCouponCreateForm(PromotionCouponCreateForm form) {

    String productId = form.getProduct_select();
    String categoryId = form.getCategory_select();

    UserCouponType scope;
    if (StringUtils.isNotBlank(productId) && StringUtils.isNotBlank(categoryId)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "不能同时选择分类与商品");
    }
    if (StringUtils.isBlank(productId) && StringUtils.isBlank(categoryId)) {
      scope = UserCouponType.SHOP;
    } else if (StringUtils.isNotBlank(productId)) {
      scope = UserCouponType.PRODUCT;
    } else {
      scope = UserCouponType.CATEGORY;
    }

    Map<String, Object> params = new HashMap<String, Object>();
    if (null != form) {
      params.put("couponId", form.getCouponId());
      params.put("distributedAt", form.getDistributed_at());
      params.put("acquireLimit", form.getAcquire_limit());
      params.put("amount", form.getAmount());
      params.put("applyAbove", form.getApply_above());
      params.put("batchNo", form.getBatch_no());
      params.put("createAt", form.getCreate_at());
      params.put("couponName", form.getCouponName());
      params.put("name", form.getName());
      params.put("discount", form.getDiscount());
      params.put("shopId", form.getShop_select());
      params.put("totalDiscount", form.getTotal_discount());
      params.put("validFrom", form.getValid_from() + " 00:00:00");
      params.put("validTo", form.getValid_to() + " 23:59:59");
      params.put("applyLimit", form.getApply_limit());
      params.put("updateAt", DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
      params.put("status", form.getStatus());
      params.put("archive", 0);
      params.put("productId", productId);
      params.put("categoryId", categoryId);
      params.put("scope", scope);
    }
    return params;
  }


  @RequestMapping(value = "couponManager/shop/qrImage/{shopId}/{couponId}", method = RequestMethod.GET, produces = "image/png")
  public @ResponseBody
  byte[] getShopQRImage(HttpServletRequest request, HttpServletResponse response,
      @PathVariable("shopId") String shopId,
      @PathVariable("couponId") String couponId) {

    Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
    // 指定纠错等级
    hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
    hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); //编码
    hints.put(EncodeHintType.MARGIN, 1);

    String url = siteHost + "/coupon/userPromotionCoupon/";

    StringBuffer codeUrl = new StringBuffer(url);
    codeUrl.append(shopId);
    codeUrl.append("/").append(couponId);

    // 指定编码格式
    ByteArrayOutputStream byteArrayOutputStream = null;
    BitMatrix byteMatrix = null;
    try {
      byteMatrix = new MultiFormatWriter()
          .encode(codeUrl.toString(), BarcodeFormat.QR_CODE, 220, 220, hints);
      byteArrayOutputStream = new ByteArrayOutputStream();
      MatrixToImageWriter.writeToStream(byteMatrix, "png", byteArrayOutputStream);
      return byteArrayOutputStream.toByteArray();

    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    } catch (WriterException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    } finally {
      IOUtils.closeQuietly(byteArrayOutputStream);
    }
  }


  /**
   * 批次号生成规则：时间戳（精确到秒）+当前用户ID+序列号
   *
   * @return 24位批次号
   */
  private String nextBatchNo() {

    String version = "01"; //批次号生成规则 版本号

    String rdm = UUID.randomUUID().toString();
    String prefixRdm = "0001";
    String suffixRdm = "0001";
    if (rdm != null && rdm.length() >= 8) { //取UUID后8位数
      prefixRdm = rdm.substring(rdm.length() - 8, rdm.length() - 4);
      suffixRdm = rdm.substring(rdm.length() - 4, rdm.length());
    }

    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(System.currentTimeMillis());
    //生成规则：     月+秒 + 随机四位数+  小时 +版本号 +年 +日 +分钟 + 随机四位数
    return String
        .format("%1$tm%1$tS%2$s%1$tk%4$s%1$tY%1$tM%1$td%3$s", cal, prefixRdm, suffixRdm, version);
  }
}
