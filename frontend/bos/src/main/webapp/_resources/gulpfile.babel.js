// generated on 2016-01-15 using generator-gulp-webapp 1.1.1
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import {stream as wiredep} from 'wiredep';

var watch = require('gulp-watch');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const destination = 'dist';
const htmlViews = '../WEB-INF/';
//const tomcatDestination = '/Users/quguangming/Documents/code/Ecmoho/Dev/o2o/b2b/platform/branches/xquark-platform/mall-platform/frontend/target/tomcat/webapps/xquark-bos/opt/yiheng/tomcat/webapps/bos/_resources/dist';
//const tomcatHtmlViews = '/Users/quguangming/Documents/code/Ecmoho/Dev/o2o/b2b/platform/branches/xquark-platform/mall-platform/frontend/target/tomcat/webapps/xquark-bos/WEB-INF';

const tomcatDestination = '/Users/quguangming/Documents/tools/tomcat/webapps/bos/_resources/dist';
const tomcatHtmlViews = '/Users/quguangming/Documents/tools/tomcat/webapps/bos/WEB-INF';


//
//gulp.task('styles', () => {
//  return gulp.src('css/*.scss')
//    .pipe($.plumber())
//    .pipe($.sourcemaps.init())
//    .pipe($.sass.sync({
//      outputStyle: 'expanded',
//      precision: 10,
//      includePaths: ['.']
//    }).on('error', $.sass.logError))
//    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
//    .pipe($.sourcemaps.write())
//    .pipe(gulp.dest('.tmp/styles'))
//    .pipe(reload({stream: true}));
//});


function lint(files, options) {
  return () => {
    return gulp.src(files)
      .pipe(reload({stream: true, once: true}))
      .pipe($.eslint(options))
      .pipe($.eslint.format())
      .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
  };
}
const testLintOptions = {
  env: {
    mocha: true
  }
};

gulp.task('lint', lint('js/**/*.js'));
//gulp.task('lint:test', lint('test/spec/**/*.js', testLintOptions));

//gulp.task('html', ['styles'], () => {
//  return gulp.src('source/*.html')
//    .pipe($.useref({searchPath: ['.tmp', 'source', '.']}))
//    .pipe($.if('*.js', $.uglify()))
//    .pipe($.if('*.css', $.cssnano()))
//    .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
//    .pipe(gulp.dest('dist'));
//});

//gulp.task('images', () => {
//  return gulp.src('images/**/*')
//    .pipe($.if($.if.isFile, $.cache($.imagemin({
//      progressive: true,
//      interlaced: true,
//      // don't remove IDs from SVGs, they are often used
//      // as hooks for embedding and styling
//      svgoPlugins: [{cleanupIDs: false}]
//    }))
//    .on('error', function (err) {
//      console.log(err);
//      this.end();
//    })))
//    .pipe(gulp.dest('dist/images'));
//});

//gulp.task('fonts', () => {
//  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
//    .concat('source/fonts/**/*'))
//    .pipe(gulp.dest('.tmp/fonts'))
//    .pipe(gulp.dest('dist/fonts'));
//});

//gulp.task('extras', () => {
//  return gulp.src([
//    'source/*.*',
//    '!source/*.html'
//  ], {
//    dot: true
//  }).pipe(gulp.dest('dist'));
//});

//gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

//gulp.task('serve', ['styles', 'fonts'], () => {
//  browserSync({
//    notify: false,
//    port: 9000,
//    server: {
//      baseDir: ['.tmp', 'source'],
//      routes: {
//        '/bower_components': 'bower_components'
//      }
//    }
//  });
//
//  gulp.watch([
//    'source/*.html',
//    'source/scripts/**/*.js',
//    'source/images/**/*',
//    '.tmp/fonts/**/*'
//  ]).on('change', reload);
//
//  gulp.watch('source/styles/**/*.scss', ['styles']);
//  gulp.watch('source/fonts/**/*', ['fonts']);
//  gulp.watch('bower.json', ['wiredep', 'fonts']);
//});
//
//gulp.task('serve:dist', () => {
//  browserSync({
//    notify: false,
//    port: 9000,
//    server: {
//      baseDir: ['dist']
//    }
//  });
//});
//
//gulp.task('serve:test', () => {
//  browserSync({
//    notify: false,
//    port: 9000,
//    ui: false,
//    server: {
//      baseDir: 'test',
//      routes: {
//        '/scripts': 'source/scripts',
//        '/bower_components': 'bower_components'
//      }
//    }
//  });
//
//  gulp.watch('test/spec/**/*.js').on('change', reload);
//  gulp.watch('test/spec/**/*.js', ['lint:test']);
//});

// inject bower components
//gulp.task('wiredep', () => {
//  gulp.src('source/styles/*.scss')
//    .pipe(wiredep({
//      ignorePath: /^(\.\.\/)+/
//    }))
//    .pipe(gulp.dest('source/styles'));
//
//  gulp.src('source/*.html')
//    .pipe(wiredep({
//      exclude: ['bootstrap-sass'],
//      ignorePath: /^(\.\.\/)*\.\./
//    }))
//    .pipe(gulp.dest('source'));
//});

//gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
//  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
//});

gulp.task('default', [], () => {

  //watch update for build
  //gulp.watch('source/styles/**/*.scss', ['styles']);
  //gulp.watch('source/fonts/**/*', ['fonts']);
  //gulp.watch('bower.json', ['wiredep', 'fonts']);

  //sync dist dir to tomcat webapp
  gulp.src('css/**/*', {base: 'css'})
    .pipe(watch('./', {base: 'css'}))
    .pipe(gulp.dest(tomcatDestination));
  gulp.src('js/**/*', {base: 'js'})
    .pipe(watch('./', {base: 'js'}))
    .pipe(gulp.dest(tomcatDestination));
  gulp.src('images/**/*', {base: 'images'})
    .pipe(watch('./', {base: 'images'}))
    .pipe(gulp.dest(tomcatDestination));
  gulp.src('font/**/*', {base: 'font'})
    .pipe(watch('./', {base: 'font'}))
    .pipe(gulp.dest(tomcatDestination));

  //sync html views to tomcat webapp
  gulp.src(htmlViews + '/**/*', {base: htmlViews})
    .pipe(watch(htmlViews, {base: htmlViews}))
    .pipe(gulp.dest(tomcatHtmlViews));

});

