package com.xquark.service;

import com.xquark.dal.IUser;
import com.xquark.service.error.BizException;

public interface BaseService {

  /**
   * @throws BizException if not logined
   */
  IUser getCurrentUser() throws BizException;

}