package com.xquark.service.push.type;



import org.apache.commons.lang3.StringUtils;
/**
 * @author luqig
 * @since 2019-04-29
 */
public enum Platform{
  /**
   * android & IOS
   */
  ALL{
    @Override
    public String description() {
      return "all-push";
    }
  },

  /**
   * android
   */
  A {
    @Override
    public String description() {
      return "android-push";
    }
  },

  /**
   * IOS
   */
  I {
    @Override
    public String description() {
      return "ios-push";
    }
  };

  @Override
  public String toString() {
    return StringUtils.lowerCase(super.toString());
  }

  public abstract String description();
}