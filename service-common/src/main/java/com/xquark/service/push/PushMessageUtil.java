package com.xquark.service.push;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xquark.service.push.pojo.PushParam;
import com.xquark.service.push.type.Platform;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author luqing
 * @since 2019-04-26
 */
public class PushMessageUtil {
  private final static Logger LOGGER = LoggerFactory.getLogger(PushMessageUtil.class);

  private static String host;

  public static void setHost(String host) {
    PushMessageUtil.host = host;
  }

  public static Boolean pushMessage(PushParam.Params params, Platform platform, Integer priority, String url, String title, String text){
//    for (PushParam.Params params : paramsList){
    // 推送系统类型 all/a/i
    params.setPlatform(StringUtils.lowerCase(String.valueOf(platform)));
    // 通知 暂不支持
    params.setDisplayType("notification");
    // 落地页URL
    params.setCustom("{\"url\":\"" + url +"\"}");
    // 主标题
    params.setTitle(title);
    // 安卓副标题
    params.setTicker(title);
    // IOS副标题
    params.setSubtitle(title);
    // push 文案
    params.setText(text);
    // android专有，当displayType为notification时必填，为自定义点击"通知"的后续行为, 这里暂时只支持go_custom
    params.setAfterOpen("go_custom");
    // 描述
    params.setDescription(platform.description());

    return pushMessage(params);
  }

  private static Boolean pushMessage(PushParam.Params params){
    return pushMessage(new PushParam(Collections.singletonList(params)));
  }

  private static Boolean pushMessage(PushParam pushParam){
    HttpInvokeResult post = PoolingHttpClients.postJSON(new HttpHost(host).toURI(), pushParam);
    JSONObject response = JSON.parseObject(post.getContent());
    Boolean result = response.getBoolean("result");

    return result != null && result;
  }

  public static <T> List<List<T>> subList(List<T> source, Integer subLength){
    int size = source.size();
    int newSize = (size%subLength) ==0 ? size/subLength : size/subLength + 1;
    List<List<T>> target = new ArrayList<>(newSize);
    for (int index = 0;index < newSize ; index++) {
      if (index == (source.size()/subLength)){
        target.add(source.subList(index * subLength,source.size()));
      }else {
        target.add(source.subList(index * subLength ,(index + 1) * subLength));
      }
    }

    return target;
  }

}
