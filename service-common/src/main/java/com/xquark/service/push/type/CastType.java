package com.xquark.service.push.type;


import com.xquark.service.push.PushMessageUtil;
import com.xquark.service.push.pojo.PushParam;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.xquark.service.push.PushMessageUtil.subList;

/**
 * @author luqing
 * @since 2019-04-29
 * 推送类型
 */
public enum CastType{
  /**
   * 广播 : 全部设备一起发
   */
  BROADCAST{
    @Override
    public Boolean pushMassage(Platform platform, List<Long> cpIdList,Integer priority, String url, String title, String text) {
      LOGGER.info("push所有用户,落地页:{},简称:{},文案内容:{}",url,title,text);
      PushParam.Params params = new PushParam.Params();
      params.setCastType(StringUtils.lowerCase(BROADCAST.toString()));
      return PushMessageUtil.pushMessage(params, platform, priority,url, title, text);
    }
  },
  /**
   * 自定义播 : 指定aliasType和alias，把消息发送到指定用户群体
   */
  CUSTOMIZEDCAST{
    @Override
    public Boolean pushMassage(Platform platform,List<Long> cpIdList,Integer priority,String url,String title,String text) {
      LOGGER.info("push部分用户,用户列表:{},优先级:{},落地页:{},简称:{},文案内容:{}",cpIdList,priority,url,title,text);

      if (CollectionUtils.isEmpty(cpIdList)){
        return false;
      }

      PushParam.Params params = new PushParam.Params();
      params.setCastType(StringUtils.lowerCase(CUSTOMIZEDCAST.toString()));
      params.setAliasType("hanwei");

      // 一次最多推送200用户
      List<List<Long>> lists = subList(cpIdList, 200);
      for (List<Long> subCpIdList : lists){
        params.setAlias(subCpIdList);
        PushMessageUtil.pushMessage(params,platform,priority,url,title,text);
      }
      return true;
    }
  };
  private final static Logger LOGGER = LoggerFactory.getLogger(CastType.class);

  public abstract Boolean pushMassage(Platform platform, List<Long> cpIdList,Integer priority, String url, String title, String text);

  public static void main(String[] args) {
//    CUSTOMIZEDCAST.pushMassage(Platform.ALL, Collections.singletonList(3077575L),)
  }
}

