package com.xquark.service.push;

import com.xquark.dal.mapper.ActivityPushDetailMapper;
import com.xquark.dal.type.ActionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author luqing
 * @since 2019-05-02
 */
@Component
public class ActivityPushPostConstruct {

  @Autowired
  private ActivityPushDetailMapper pushDetailMapper;

  @Value("${push.host}")
  private String host;


  @PostConstruct
  private void setActivityMapper() {
    ActionType.setPushMapper(pushDetailMapper);
  }

  @PostConstruct
  private void setHost(){
    PushMessageUtil.setHost(host);
  }



}
