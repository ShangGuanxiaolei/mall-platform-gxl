package com.xquark.service.push.pojo;


import java.util.List;

/**
 * @author luqig
 * @since 2019-04-26
 */
public class PushParam {

  /**
   * "jsonrpc" : "2.0",
   * "method" : "Push.send",                        // 内容固定
   * "params" : [{
   *   "platform" : "i/a/all",                         // 必填 设备平台系统 i-ios, a-android
   *   "castType" : "broadcast/customizedcast",     // 必填 消息发送类型，broadcast-广播, customizedcast-自定义播
   *   "aliasType" : "hanwei",                     // 当castType=customizedcast时必填
   *   "alias" : ["10001", "10002", "10003"],      // 当type=customizedcast时必填, 以数组形式填写, 一次最多200个
   *   "priority" : 1,                             // 选填 消息发送的优先级 1-4 对应 低-中-高-极高, 默认1
   *   "displayType" : "notification/message",     // android必填, 消息类型: notification(通知)、message(消息)
   *   "ticker" : "",                              // android专有, 当displayType为notification时必填，通知栏提示文字
   *   "title" : "",                               // 1.android，当displayType为notification时必填，通知栏提示文字 2.ios必填
   *   "subtitle" : "",                            // ios必填
   *   "text" : "",                                // 1.android，当displayType为notification时必填，通知文字描述 2. ios必填
   *   "afterOpen" : "go_custom",                  // android专有，当displayType为notification时必填，为自定义点击"通知"的后续行为, 这里暂时只支持go_custom
   *   "custom" : "",                              // 必填 string
   *                                               // 1. 当(platform=a ,并且displayType=notification)或者(platform=i)时,填写"{\"url\":\"cn.bing.com\"}"格式的字符串(这里只需修改url的值)
   *                                               // 2. 当 platform=a ,并且displayType=message 时, 不能为空
   *   "description" : "",                         // 可选 发送消息描述
   *     }]
   */

  private String jsonrpc = "2.0";
  private String method = "Push.send";
  private List<Params> params;

  public PushParam(List<Params> params) {
    this.params = params;
  }

  public String getJsonrpc() {
    return jsonrpc;
  }

  public void setJsonrpc(String jsonrpc) {
    this.jsonrpc = jsonrpc;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public List<Params> getParams() {
    return params;
  }

  public void setParams(List<Params> params) {
    this.params = params;
  }

  public static class Params {
    private String platform;
    private String castType;
    private String aliasType;
    private Integer priority;
    private String displayType;
    private String ticker;
    private String title;
    private String subtitle;
    private String text;
    private String afterOpen;
    private String custom;
    private String description;
    private List<Long> alias;

    public Params(){
    }

    public String getPlatform() {
      return platform;
    }

    public void setPlatform(String platform) {
      this.platform = platform;
    }

    public String getCastType() {
      return castType;
    }

    public void setCastType(String castType) {
      this.castType = castType;
    }

    public String getAliasType() {
      return aliasType;
    }

    public void setAliasType(String aliasType) {
      this.aliasType = aliasType;
    }

    public Integer getPriority() {
      return priority;
    }

    public void setPriority(Integer priority) {
      this.priority = priority;
    }

    public String getDisplayType() {
      return displayType;
    }

    public void setDisplayType(String displayType) {
      this.displayType = displayType;
    }

    public String getTicker() {
      return ticker;
    }

    public void setTicker(String ticker) {
      this.ticker = ticker;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getSubtitle() {
      return subtitle;
    }

    public void setSubtitle(String subtitle) {
      this.subtitle = subtitle;
    }

    public String getText() {
      return text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public String getAfterOpen() {
      return afterOpen;
    }

    public void setAfterOpen(String afterOpen) {
      this.afterOpen = afterOpen;
    }

    public String getCustom() {
      return custom;
    }

    public void setCustom(String custom) {
      this.custom = custom;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public List<Long> getAlias() {
      return alias;
    }

    public void setAlias(List<Long> alias) {
      this.alias = alias;
    }
  }
}

