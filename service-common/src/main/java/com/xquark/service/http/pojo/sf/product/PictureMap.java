package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PictureMap{

	@JSONField(name="file")
	private String file;

	@JSONField(name="year")
	private int year;

	public void setFile(String file){
		this.file = file;
	}

	public String getFile(){
		return file;
	}

	public void setYear(int year){
		this.year = year;
	}

	public int getYear(){
		return year;
	}

	@Override
 	public String toString(){
		return 
			"PictureMap{" + 
			"file = '" + file + '\'' + 
			",year = '" + year + '\'' + 
			"}";
		}
}