package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 区域返回值
 *
 * @auther liuwei
 * @date 2018/6/6 16:54
 */
public class SFRegion {

  @JSONField(name = "showName")
  private String showName;//展示名称
  @JSONField(name = "regionId")
  private int regionId;//区域id
  @JSONField(name = "regionType")
  private boolean regionType;//区域类型
  @JSONField(name = "regionName")
  private String regionName;//区域名称
  @JSONField(name = "parentId")
  private int parentId;//父区域id

  public String getShowName() {
    return showName;
  }

  public void setShowName(String showName) {
    this.showName = showName;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  public boolean getRegionType() {
    return regionType;
  }

  public void setRegionType(boolean regionType) {
    this.regionType = regionType;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public int getParentId() {
    return parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }

  @Override
  public String toString() {
    return "SFRegion{" +
        "showName='" + showName + '\'' +
        ", regionId=" + regionId +
        ", regionType=" + regionType +
        ", regionName='" + regionName + '\'' +
        ", parentId=" + parentId +
        '}';
  }
}
