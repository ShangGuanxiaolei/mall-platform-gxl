package com.xquark.service.http;

import java.io.Serializable;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public abstract class ServerResponse<T> implements Serializable {

  protected T data;

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
