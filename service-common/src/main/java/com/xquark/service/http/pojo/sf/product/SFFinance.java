package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFFinance{

	@JSONField(name="productId")
	private int productId;

	@JSONField(name="baseTax")
	private int baseTax;

	@JSONField(name="tax")
	private int tax;

	@JSONField(name="id")
	private int id;

	@JSONField(name="isTaxFree")
	private int isTaxFree;

	@JSONField(name="inTax")
	private int inTax;

	@JSONField(name="outTax")
	private int outTax;

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setBaseTax(int baseTax){
		this.baseTax = baseTax;
	}

	public int getBaseTax(){
		return baseTax;
	}

	public void setTax(int tax){
		this.tax = tax;
	}

	public int getTax(){
		return tax;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIsTaxFree(int isTaxFree){
		this.isTaxFree = isTaxFree;
	}

	public int getIsTaxFree(){
		return isTaxFree;
	}

	public void setInTax(int inTax){
		this.inTax = inTax;
	}

	public int getInTax(){
		return inTax;
	}

	public void setOutTax(int outTax){
		this.outTax = outTax;
	}

	public int getOutTax(){
		return outTax;
	}

	@Override
 	public String toString(){
		return 
			"SFFinance{" + 
			"productId = '" + productId + '\'' + 
			",baseTax = '" + baseTax + '\'' + 
			",tax = '" + tax + '\'' + 
			",id = '" + id + '\'' + 
			",isTaxFree = '" + isTaxFree + '\'' + 
			",inTax = '" + inTax + '\'' + 
			",outTax = '" + outTax + '\'' + 
			"}";
		}
}