package com.xquark.service.http.pojo.sf.base;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午11:30 顺丰请求基类
 */
public abstract class SFBaseParam {

  public SFBaseParam() {
  }

  public SFBaseParam(String app_key, String access_token, String timestamp) {
    this.app_key = app_key;
    this.access_token = access_token;
    this.timestamp = timestamp;
  }

  private String app_key;

  private String access_token;

  private String timestamp;

  private String format = "json";

  private String version = "1.0";

  public String getApp_key() {
    return app_key;
  }

  public void setApp_key(String app_key) {
    this.app_key = app_key;
  }

  public String getAccess_token() {
    return access_token;
  }

  public void setAccess_token(String access_token) {
    this.access_token = access_token;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}

