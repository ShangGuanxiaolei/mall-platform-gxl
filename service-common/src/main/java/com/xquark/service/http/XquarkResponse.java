package com.xquark.service.http;

/**
 * Created by wangxinhua on 18-2-26. DESC:
 */
public class XquarkResponse<T> extends ServerResponse<T> {

  private String error;

  private Integer errorCode;

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public boolean isSuccess() {
    return this.errorCode == 200;
  }

  @Override
  public String toString() {
    return "XquarkResponse{" +
        "data=" + data +
        ", error='" + error + '\'' +
        ", errorCode=" + errorCode +
        '}';
  }
}
