package com.xquark.service.http.pojo.sf.product;

/**
 * 区域词典返回数据实体类 create by
 *
 * @auther liuwei
 * @date 2018/6/6 10:51
 */
public class SFRegionList {

  private String showName;//展现名称
  private int regionId;//区域id
  private int regionType;//区域类型
  private String regionName;//区域名称
  private int parentId;//父区域id

  public String getShowName() {
    return showName;
  }

  public void setShowName(String showName) {
    this.showName = showName;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  public int getRegionType() {
    return regionType;
  }

  public void setRegionType(int regionType) {
    this.regionType = regionType;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public int getParentId() {
    return parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }

  @Override
  public String toString() {
    return "SFRegionList{" +
        "showName='" + showName + '\'' +
        ", regionId=" + regionId +
        ", regionType=" + regionType +
        ", regionName='" + regionName + '\'' +
        ", parentId=" + parentId +
        '}';
  }
}
