package com.xquark.service.http.pojo.sf.base;

/**
 * created by
 *
 * @author wangxinhua at 18-6-4 下午4:00
 */
public class SFTokenParam {

  private final String grant_type;

  private final String client_id;

  private final String client_secret;

  public SFTokenParam(String grant_type, String client_id, String client_secret) {
    this.grant_type = grant_type;
    this.client_id = client_id;
    this.client_secret = client_secret;
  }

  public String getGrant_type() {
    return grant_type;
  }

  public String getClient_id() {
    return client_id;
  }

  public String getClient_secret() {
    return client_secret;
  }
}
