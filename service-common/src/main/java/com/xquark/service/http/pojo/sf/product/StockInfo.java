package com.xquark.service.http.pojo.sf.product;

/**
 * 库存返回值实体类
 *
 * @auther liuwei
 * @date 2018/6/6 11:01
 */
public class StockInfo {

  private int productId;//商品ID
  private int saleNumber;//可售数量
  private int shipTime;//时效
  private int stockStatus;//库存状态
  private int maxSaleNumber;//最大可售数量
  private int regionId;//区域ID

  public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public int getSaleNumber() {
    return saleNumber;
  }

  public void setSaleNumber(int saleNumber) {
    this.saleNumber = saleNumber;
  }

  public int getShipTime() {
    return shipTime;
  }

  public void setShipTime(int shipTime) {
    this.shipTime = shipTime;
  }

  public int getStockStatus() {
    return stockStatus;
  }

  public void setStockStatus(int stockStatus) {
    this.stockStatus = stockStatus;
  }

  public int getMaxSaleNumber() {
    return maxSaleNumber;
  }

  public void setMaxSaleNumber(int maxSaleNumber) {
    this.maxSaleNumber = maxSaleNumber;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  @Override
  public String toString() {
    return "stockInfo{" +
        "productId=" + productId +
        ", saleNumber=" + saleNumber +
        ", shipTime=" + shipTime +
        ", stockStatus=" + stockStatus +
        ", maxSaleNumber=" + maxSaleNumber +
        ", regionId=" + regionId +
        '}';
  }
}
