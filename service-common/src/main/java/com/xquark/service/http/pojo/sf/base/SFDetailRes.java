package com.xquark.service.http.pojo.sf.base;

import com.xquark.service.http.pojo.sf.product.SFProductDetailWrapper;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 下午7:29
 */
public class SFDetailRes {

  private Integer code;

  private String msg;

  private SFProductDetailWrapper data;

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public SFProductDetailWrapper getData() {
    return data;
  }

  public void setData(SFProductDetailWrapper data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "SFDetailRes{" +
        "code=" + code +
        ", msg='" + msg + '\'' +
        ", data=" + data +
        '}';
  }
}
