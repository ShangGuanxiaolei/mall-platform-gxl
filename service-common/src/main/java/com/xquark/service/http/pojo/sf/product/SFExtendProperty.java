package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFExtendProperty{

	@JSONField(name="confirmTime")
	private int confirmTime;

	public void setConfirmTime(int confirmTime){
		this.confirmTime = confirmTime;
	}

	public int getConfirmTime(){
		return confirmTime;
	}

	@Override
 	public String toString(){
		return 
			"SFExtendProperty{" + 
			"confirmTime = '" + confirmTime + '\'' + 
			"}";
		}
}