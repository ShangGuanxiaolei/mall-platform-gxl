package com.xquark.service.http;

/**
 * Created by wangxinhua on 18-3-1. DESC:
 */
public interface ResponseHandler<T> {

  /**
   * 什么都不处理的handler
   */
  ResponseHandler<Object> DISCARD_HANDLER = new ResponseHandler<Object>() {
    @Override
    public void handleRes(ServerResponse<Object> res) {
      // do nothing
    }
  };

  void handleRes(ServerResponse<T> res);

}
