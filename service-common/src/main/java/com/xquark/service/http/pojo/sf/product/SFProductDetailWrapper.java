package com.xquark.service.http.pojo.sf.product;

import java.util.List;

/**
 * created by
 *
 * @author wangxinhua at 18-6-5 下午4:27
 */
public class SFProductDetailWrapper {

  private List<SFProductDetail> productInfoDetails;

  public List<SFProductDetail> getProductInfoDetails() {
    return productInfoDetails;
  }

  public void setProductInfoDetails(
      List<SFProductDetail> productInfoDetails) {
    this.productInfoDetails = productInfoDetails;
  }
}
