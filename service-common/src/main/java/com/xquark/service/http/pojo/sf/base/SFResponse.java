package com.xquark.service.http.pojo.sf.base;

import com.xquark.service.http.ServerResponse;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午11:16
 */
public class SFResponse<T> extends ServerResponse<T> {

  private Integer code;

  private String msg;

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public T getData() {
    return super.getData();
  }

  @Override
  public void setData(T data) {
    super.setData(data);
  }

  @Override
  public String toString() {
    return "SFResponse{" +
        "code=" + code +
        ", msg='" + msg + '\'' +
        ", data=" + data +
        '}';
  }
}
