package com.xquark.service.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 18-2-26. DESC:
 */
@Component
public class XquarkHttpUtils {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final ExecutorService ec = ExecutorPool.HTTP_EXECUTOR_SERVICE;

  public Future<Optional<JSONObject>> getFuture(final String url) {
    return getFuture(url, JSONObject.class);
  }

  private <T> Future<Optional<T>> getFuture(final String url, final Class<T> clazz) {
    return ec.submit(new Callable<Optional<T>>() {
      @Override
      public Optional<T> call() {
        return get(url, clazz);
      }
    });
  }

  public Optional<JSONObject> get(String url) {
    return get(url, JSONObject.class);
  }

  @SuppressWarnings("unchecked, unused")
  public <T> Optional<T> get(String url, Class<T> calzz) {
    HttpInvokeResult res = PoolingHttpClients.get(url);
    String msg;
    if (!res.isOK()) {
      msg = String.format("请求 %s 失败, 错误码: %s", url, res.getStatusCode());
      logger.error(msg);
      return Optional.absent();
    }
    String content = res.getContent();
    XquarkResponse<T> xquarkRes;
    try {
      xquarkRes = JSON.parseObject(content, XquarkResponse.class);
    } catch (Exception e) {
      msg = "实例类型不正确";
      logger.error(msg);
      throw new RuntimeException(msg);
    }
    if (!xquarkRes.isSuccess()) {
      logger.error("服务器返回失败: {}", xquarkRes.getError());
      return Optional.absent();
    }
    return Optional.of(xquarkRes.getData());
  }

}
