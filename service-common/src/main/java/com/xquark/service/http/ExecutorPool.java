package com.xquark.service.http;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by wangxinhua on 18-3-8. DESC:
 * TODO 使用自定义线程池优化性能
 */
public class ExecutorPool {

  public static final ExecutorService HTTP_EXECUTOR_SERVICE = Executors.newFixedThreadPool(20);

  public static final ExecutorService PULL_PRODUCT_SERVICE = Executors.newFixedThreadPool(5);

}
