package com.xquark.service.http.pojo.sf.base;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午8:13
 */
public class SFProductEntity {

  public SFProductEntity(String number) {
    this(null, null);
    this.number = number;
  }

  public SFProductEntity(Integer page, Integer pageSize) {
    this.page = page;
    this.pageSize = pageSize;
  }

  public SFProductEntity(Integer page, Integer pageSize, Integer isShow) {
    this.page = page;
    this.pageSize = pageSize;
    this.isShow = isShow;
  }

  private String number;

  private Integer page;

  private Integer pageSize;

  private Integer isShow;

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Integer getIsShow() {
    return isShow;
  }

  public void setIsShow(Integer isShow) {
    this.isShow = isShow;
  }
}
