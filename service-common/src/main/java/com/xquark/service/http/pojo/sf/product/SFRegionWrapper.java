package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.List;

/**
 * @auther liuwei
 * @date 2018/6/6 21:46
 */
public class SFRegionWrapper {

  @JSONField(name = "regionList")
  private List<SFRegion> regionList;

  public List<SFRegion> getRegionList() {
    return regionList;
  }

  public void setRegionList(List<SFRegion> regionList) {
    this.regionList = regionList;
  }
}
