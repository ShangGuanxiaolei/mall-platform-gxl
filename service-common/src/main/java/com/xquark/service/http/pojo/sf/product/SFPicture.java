package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFPicture{

	@JSONField(name="sourceId")
	private int sourceId;

	@JSONField(name="isDefault")
	private boolean isDefault;

	@JSONField(name="filename")
	private String filename;

	@JSONField(name="pictureId")
	private int pictureId;

	@JSONField(name="productId")
	private int productId;

	@JSONField(name="year")
	private int year;

	@JSONField(name="sourceType")
	private int sourceType;

	@JSONField(name="pictureMap")
	private PictureMap pictureMap;

	@JSONField(name="type")
	private int type;

	@JSONField(name="delete")
	private boolean delete;

	@JSONField(name="originalFilename")
	private String originalFilename;

	@JSONField(name="order")
	private int order;

	public void setSourceId(int sourceId){
		this.sourceId = sourceId;
	}

	public int getSourceId(){
		return sourceId;
	}

	public void setIsDefault(boolean isDefault){
		this.isDefault = isDefault;
	}

	public boolean getIsDefault(){
		return isDefault;
	}

	public void setFilename(String filename){
		this.filename = filename;
	}

	public String getFilename(){
		return filename;
	}

	public void setPictureId(int pictureId){
		this.pictureId = pictureId;
	}

	public int getPictureId(){
		return pictureId;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setYear(int year){
		this.year = year;
	}

	public int getYear(){
		return year;
	}

	public void setSourceType(int sourceType){
		this.sourceType = sourceType;
	}

	public int getSourceType(){
		return sourceType;
	}

	public void setPictureMap(PictureMap pictureMap){
		this.pictureMap = pictureMap;
	}

	public PictureMap getPictureMap(){
		return pictureMap;
	}

	public void setType(int type){
		this.type = type;
	}

	public int getType(){
		return type;
	}

	public void setDelete(boolean delete){
		this.delete = delete;
	}

	public boolean isDelete(){
		return delete;
	}

	public void setOriginalFilename(String originalFilename){
		this.originalFilename = originalFilename;
	}

	public String getOriginalFilename(){
		return originalFilename;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"SFPicture{" + 
			"sourceId = '" + sourceId + '\'' + 
			",isDefault = '" + isDefault + '\'' + 
			",filename = '" + filename + '\'' + 
			",pictureId = '" + pictureId + '\'' + 
			",productId = '" + productId + '\'' + 
			",year = '" + year + '\'' + 
			",sourceType = '" + sourceType + '\'' + 
			",pictureMap = '" + pictureMap + '\'' + 
			",type = '" + type + '\'' + 
			",delete = '" + delete + '\'' + 
			",originalFilename = '" + originalFilename + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}