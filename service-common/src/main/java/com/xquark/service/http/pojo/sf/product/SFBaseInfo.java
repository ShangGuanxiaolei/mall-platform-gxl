package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFBaseInfo{

	@JSONField(name="country")
	private int country;

	@JSONField(name="isReturn")
	private Object isReturn;

	@JSONField(name="boxSpecification")
	private String boxSpecification;

	@JSONField(name="modifiedTime")
	private String modifiedTime;

	@JSONField(name="addTime")
	private int addTime;

	@JSONField(name="minPurchaseUnit")
	private Object minPurchaseUnit;

	@JSONField(name="qsProportion")
	private Object qsProportion;

	@JSONField(name="saleUnit")
	private String saleUnit;

	@JSONField(name="shelfLifeProportion")
	private Object shelfLifeProportion;

	@JSONField(name="number")
	private String number;

	@JSONField(name="isHave")
	private Object isHave;

	@JSONField(name="isBook")
	private Object isBook;

	@JSONField(name="isCod")
	private Object isCod;

	@JSONField(name="productArea")
	private int productArea;

	@JSONField(name="systemName")
	private String systemName;

	@JSONField(name="price")
	private int price;

	@JSONField(name="isPresale")
	private Object isPresale;

	@JSONField(name="model")
	private String model;

	@JSONField(name="operatorId")
	private int operatorId;

	@JSONField(name="height")
	private Double height;

	@JSONField(name="categoryOne")
	private int categoryOne;

	@JSONField(name="isUniqueNumber")
	private Object isUniqueNumber;

	@JSONField(name="newTag")
	private Object newTag;

	@JSONField(name="productId")
	private int productId;

	@JSONField(name="isBatchNumber")
	private Object isBatchNumber;

	@JSONField(name="weight")
	private Double weight;

	@JSONField(name="giveIntegral")
	private Object giveIntegral;

	@JSONField(name="packages")
	private Object packages;

	@JSONField(name="click")
	private Object click;

	@JSONField(name="barCode")
	private String barCode;

	@JSONField(name="isSell")
	private int isSell;

	@JSONField(name="isPromote")
	private Object isPromote;

	@JSONField(name="auditTime")
	private int auditTime;

	@JSONField(name="brandId")
	private int brandId;

	@JSONField(name="name")
	private String name;

	@JSONField(name="mwaveragePrice")
	private Object mwaveragePrice;

	@JSONField(name="purchaseName")
	private String purchaseName;

	@JSONField(name="favorite")
	private Object favorite;

	@JSONField(name="status")
	private int status;

	@JSONField(name="maxUnsalable")
	private Object maxUnsalable;

	@JSONField(name="englishName")
	private String englishName;

	@JSONField(name="supplierId")
	private Object supplierId;

	@JSONField(name="categoryTwo")
	private int categoryTwo;

	@JSONField(name="businessModel")
	private int businessModel;

	@JSONField(name="refuseNotes")
	private String refuseNotes;

	@JSONField(name="isMulticity")
	private Object isMulticity;

	@JSONField(name="remark")
	private Object remark;

	@JSONField(name="storage")
	private Object storage;

	@JSONField(name="inventory")
	private Object inventory;

	@JSONField(name="adventShelves")
	private Object adventShelves;

	@JSONField(name="categoryThree")
	private int categoryThree;

	@JSONField(name="isOutgoing")
	private Object isOutgoing;

	@JSONField(name="referInPrice")
	private Object referInPrice;

	@JSONField(name="proWarning")
	private Object proWarning;

	@JSONField(name="isGift")
	private Object isGift;

	@JSONField(name="isFragile")
	private Object isFragile;

	@JSONField(name="isSeasonal")
	private Object isSeasonal;

	@JSONField(name="shelfLife")
	private int shelfLife;

	@JSONField(name="productType")
	private int productType;

	@JSONField(name="inPrice")
	private Object inPrice;

	@JSONField(name="length")
	private Double length;

	@JSONField(name="priceType")
	private int priceType;

	@JSONField(name="returnPolicy")
	private Object returnPolicy;

	@JSONField(name="specification")
	private String specification;

	@JSONField(name="isPackage")
	private Object isPackage;

	@JSONField(name="parentId")
	private int parentId;

	@JSONField(name="storageConditions")
	private Object storageConditions;

	@JSONField(name="isShow")
	private int isShow;

	@JSONField(name="merchantNumber")
	private int merchantNumber;

	@JSONField(name="isOos")
	private Object isOos;

	@JSONField(name="parentNumber")
	private int parentNumber;

	@JSONField(name="shelveDate")
	private int shelveDate;

	@JSONField(name="width")
	private Object width;

	@JSONField(name="adWord")
	private Object adWord;

	@JSONField(name="supplyPrice")
	private int supplyPrice;

	public void setCountry(int country){
		this.country = country;
	}

	public int getCountry(){
		return country;
	}

	public void setIsReturn(Object isReturn){
		this.isReturn = isReturn;
	}

	public Object getIsReturn(){
		return isReturn;
	}

	public void setBoxSpecification(String boxSpecification){
		this.boxSpecification = boxSpecification;
	}

	public String getBoxSpecification(){
		return boxSpecification;
	}

	public void setModifiedTime(String modifiedTime){
		this.modifiedTime = modifiedTime;
	}

	public String getModifiedTime(){
		return modifiedTime;
	}

	public void setAddTime(int addTime){
		this.addTime = addTime;
	}

	public int getAddTime(){
		return addTime;
	}

	public void setMinPurchaseUnit(Object minPurchaseUnit){
		this.minPurchaseUnit = minPurchaseUnit;
	}

	public Object getMinPurchaseUnit(){
		return minPurchaseUnit;
	}

	public void setQsProportion(Object qsProportion){
		this.qsProportion = qsProportion;
	}

	public Object getQsProportion(){
		return qsProportion;
	}

	public void setSaleUnit(String saleUnit){
		this.saleUnit = saleUnit;
	}

	public String getSaleUnit(){
		return saleUnit;
	}

	public void setShelfLifeProportion(Object shelfLifeProportion){
		this.shelfLifeProportion = shelfLifeProportion;
	}

	public Object getShelfLifeProportion(){
		return shelfLifeProportion;
	}

	public void setNumber(String number){
		this.number = number;
	}

	public String getNumber(){
		return number;
	}

	public void setIsHave(Object isHave){
		this.isHave = isHave;
	}

	public Object getIsHave(){
		return isHave;
	}

	public void setIsBook(Object isBook){
		this.isBook = isBook;
	}

	public Object getIsBook(){
		return isBook;
	}

	public void setIsCod(Object isCod){
		this.isCod = isCod;
	}

	public Object getIsCod(){
		return isCod;
	}

	public void setProductArea(int productArea){
		this.productArea = productArea;
	}

	public int getProductArea(){
		return productArea;
	}

	public void setSystemName(String systemName){
		this.systemName = systemName;
	}

	public String getSystemName(){
		return systemName;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setIsPresale(Object isPresale){
		this.isPresale = isPresale;
	}

	public Object getIsPresale(){
		return isPresale;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setOperatorId(int operatorId){
		this.operatorId = operatorId;
	}

	public int getOperatorId(){
		return operatorId;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public void setCategoryOne(int categoryOne){
		this.categoryOne = categoryOne;
	}

	public int getCategoryOne(){
		return categoryOne;
	}

	public void setIsUniqueNumber(Object isUniqueNumber){
		this.isUniqueNumber = isUniqueNumber;
	}

	public Object getIsUniqueNumber(){
		return isUniqueNumber;
	}

	public void setNewTag(Object newTag){
		this.newTag = newTag;
	}

	public Object getNewTag(){
		return newTag;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setIsBatchNumber(Object isBatchNumber){
		this.isBatchNumber = isBatchNumber;
	}

	public Object getIsBatchNumber(){
		return isBatchNumber;
	}

	public void setWeight(double weight){
		this.weight = weight;
	}

	public Double getWeight() {
		return weight;
	}

	public void setGiveIntegral(Object giveIntegral){
		this.giveIntegral = giveIntegral;
	}

	public Object getGiveIntegral(){
		return giveIntegral;
	}

	public void setPackages(Object packages){
		this.packages = packages;
	}

	public Object getPackages(){
		return packages;
	}

	public void setClick(Object click){
		this.click = click;
	}

	public Object getClick(){
		return click;
	}

	public void setBarCode(String barCode){
		this.barCode = barCode;
	}

	public String getBarCode(){
		return barCode;
	}

	public void setIsSell(int isSell){
		this.isSell = isSell;
	}

	public int getIsSell(){
		return isSell;
	}

	public void setIsPromote(Object isPromote){
		this.isPromote = isPromote;
	}

	public Object getIsPromote(){
		return isPromote;
	}

	public void setAuditTime(int auditTime){
		this.auditTime = auditTime;
	}

	public int getAuditTime(){
		return auditTime;
	}

	public void setBrandId(int brandId){
		this.brandId = brandId;
	}

	public int getBrandId(){
		return brandId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setMwaveragePrice(Object mwaveragePrice){
		this.mwaveragePrice = mwaveragePrice;
	}

	public Object getMwaveragePrice(){
		return mwaveragePrice;
	}

	public void setPurchaseName(String purchaseName){
		this.purchaseName = purchaseName;
	}

	public String getPurchaseName(){
		return purchaseName;
	}

	public void setFavorite(Object favorite){
		this.favorite = favorite;
	}

	public Object getFavorite(){
		return favorite;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setMaxUnsalable(Object maxUnsalable){
		this.maxUnsalable = maxUnsalable;
	}

	public Object getMaxUnsalable(){
		return maxUnsalable;
	}

	public void setEnglishName(String englishName){
		this.englishName = englishName;
	}

	public String getEnglishName(){
		return englishName;
	}

	public void setSupplierId(Object supplierId){
		this.supplierId = supplierId;
	}

	public Object getSupplierId(){
		return supplierId;
	}

	public void setCategoryTwo(int categoryTwo){
		this.categoryTwo = categoryTwo;
	}

	public int getCategoryTwo(){
		return categoryTwo;
	}

	public void setBusinessModel(int businessModel){
		this.businessModel = businessModel;
	}

	public int getBusinessModel(){
		return businessModel;
	}

	public void setRefuseNotes(String refuseNotes){
		this.refuseNotes = refuseNotes;
	}

	public String getRefuseNotes(){
		return refuseNotes;
	}

	public void setIsMulticity(Object isMulticity){
		this.isMulticity = isMulticity;
	}

	public Object getIsMulticity(){
		return isMulticity;
	}

	public void setRemark(Object remark){
		this.remark = remark;
	}

	public Object getRemark(){
		return remark;
	}

	public void setStorage(Object storage){
		this.storage = storage;
	}

	public Object getStorage(){
		return storage;
	}

	public void setInventory(Object inventory){
		this.inventory = inventory;
	}

	public Object getInventory(){
		return inventory;
	}

	public void setAdventShelves(Object adventShelves){
		this.adventShelves = adventShelves;
	}

	public Object getAdventShelves(){
		return adventShelves;
	}

	public void setCategoryThree(int categoryThree){
		this.categoryThree = categoryThree;
	}

	public int getCategoryThree(){
		return categoryThree;
	}

	public void setIsOutgoing(Object isOutgoing){
		this.isOutgoing = isOutgoing;
	}

	public Object getIsOutgoing(){
		return isOutgoing;
	}

	public void setReferInPrice(Object referInPrice){
		this.referInPrice = referInPrice;
	}

	public Object getReferInPrice(){
		return referInPrice;
	}

	public void setProWarning(Object proWarning){
		this.proWarning = proWarning;
	}

	public Object getProWarning(){
		return proWarning;
	}

	public void setIsGift(Object isGift){
		this.isGift = isGift;
	}

	public Object getIsGift(){
		return isGift;
	}

	public void setIsFragile(Object isFragile){
		this.isFragile = isFragile;
	}

	public Object getIsFragile(){
		return isFragile;
	}

	public void setIsSeasonal(Object isSeasonal){
		this.isSeasonal = isSeasonal;
	}

	public Object getIsSeasonal(){
		return isSeasonal;
	}

	public void setShelfLife(int shelfLife){
		this.shelfLife = shelfLife;
	}

	public int getShelfLife(){
		return shelfLife;
	}

	public void setProductType(int productType){
		this.productType = productType;
	}

	public int getProductType(){
		return productType;
	}

	public void setInPrice(Object inPrice){
		this.inPrice = inPrice;
	}

	public Object getInPrice(){
		return inPrice;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public void setPriceType(int priceType){
		this.priceType = priceType;
	}

	public int getPriceType(){
		return priceType;
	}

	public void setReturnPolicy(Object returnPolicy){
		this.returnPolicy = returnPolicy;
	}

	public Object getReturnPolicy(){
		return returnPolicy;
	}

	public void setSpecification(String specification){
		this.specification = specification;
	}

	public String getSpecification(){
		return specification;
	}

	public void setIsPackage(Object isPackage){
		this.isPackage = isPackage;
	}

	public Object getIsPackage(){
		return isPackage;
	}

	public void setParentId(int parentId){
		this.parentId = parentId;
	}

	public int getParentId(){
		return parentId;
	}

	public void setStorageConditions(Object storageConditions){
		this.storageConditions = storageConditions;
	}

	public Object getStorageConditions(){
		return storageConditions;
	}

	public void setIsShow(int isShow){
		this.isShow = isShow;
	}

	public int getIsShow(){
		return isShow;
	}

	public void setMerchantNumber(int merchantNumber){
		this.merchantNumber = merchantNumber;
	}

	public int getMerchantNumber(){
		return merchantNumber;
	}

	public void setIsOos(Object isOos){
		this.isOos = isOos;
	}

	public Object getIsOos(){
		return isOos;
	}

	public void setParentNumber(int parentNumber){
		this.parentNumber = parentNumber;
	}

	public int getParentNumber(){
		return parentNumber;
	}

	public void setShelveDate(int shelveDate){
		this.shelveDate = shelveDate;
	}

	public int getShelveDate(){
		return shelveDate;
	}

	public void setWidth(Object width){
		this.width = width;
	}

	public Object getWidth(){
		return width;
	}

	public void setAdWord(Object adWord){
		this.adWord = adWord;
	}

	public Object getAdWord(){
		return adWord;
	}

	public int getSupplyPrice() {
		return supplyPrice;
	}

	public void setSupplyPrice(int supplyPrice) {
		this.supplyPrice = supplyPrice;
	}

	@Override
	public String toString() {
		return "SFBaseInfo{" +
				"country=" + country +
				", isReturn=" + isReturn +
				", boxSpecification='" + boxSpecification + '\'' +
				", modifiedTime='" + modifiedTime + '\'' +
				", addTime=" + addTime +
				", minPurchaseUnit=" + minPurchaseUnit +
				", qsProportion=" + qsProportion +
				", saleUnit='" + saleUnit + '\'' +
				", shelfLifeProportion=" + shelfLifeProportion +
				", number='" + number + '\'' +
				", isHave=" + isHave +
				", isBook=" + isBook +
				", isCod=" + isCod +
				", productArea=" + productArea +
				", systemName='" + systemName + '\'' +
				", price=" + price +
				", isPresale=" + isPresale +
				", model='" + model + '\'' +
				", operatorId=" + operatorId +
				", height=" + height +
				", categoryOne=" + categoryOne +
				", isUniqueNumber=" + isUniqueNumber +
				", newTag=" + newTag +
				", productId=" + productId +
				", isBatchNumber=" + isBatchNumber +
				", weight=" + weight +
				", giveIntegral=" + giveIntegral +
				", packages=" + packages +
				", click=" + click +
				", barCode='" + barCode + '\'' +
				", isSell=" + isSell +
				", isPromote=" + isPromote +
				", auditTime=" + auditTime +
				", brandId=" + brandId +
				", name='" + name + '\'' +
				", mwaveragePrice=" + mwaveragePrice +
				", purchaseName='" + purchaseName + '\'' +
				", favorite=" + favorite +
				", status=" + status +
				", maxUnsalable=" + maxUnsalable +
				", englishName='" + englishName + '\'' +
				", supplierId=" + supplierId +
				", categoryTwo=" + categoryTwo +
				", businessModel=" + businessModel +
				", refuseNotes='" + refuseNotes + '\'' +
				", isMulticity=" + isMulticity +
				", remark=" + remark +
				", storage=" + storage +
				", inventory=" + inventory +
				", adventShelves=" + adventShelves +
				", categoryThree=" + categoryThree +
				", isOutgoing=" + isOutgoing +
				", referInPrice=" + referInPrice +
				", proWarning=" + proWarning +
				", isGift=" + isGift +
				", isFragile=" + isFragile +
				", isSeasonal=" + isSeasonal +
				", shelfLife=" + shelfLife +
				", productType=" + productType +
				", inPrice=" + inPrice +
				", length=" + length +
				", priceType=" + priceType +
				", returnPolicy=" + returnPolicy +
				", specification='" + specification + '\'' +
				", isPackage=" + isPackage +
				", parentId=" + parentId +
				", storageConditions=" + storageConditions +
				", isShow=" + isShow +
				", merchantNumber=" + merchantNumber +
				", isOos=" + isOos +
				", parentNumber=" + parentNumber +
				", shelveDate=" + shelveDate +
				", width=" + width +
				", adWord=" + adWord +
				", supplyPrice=" + supplyPrice +
				'}';
	}
}