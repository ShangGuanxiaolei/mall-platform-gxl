package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFShipping{

	@JSONField(name="shippingMap")
	private ShippingMap shippingMap;

	@JSONField(name="sfairline")
	private String sfairline;

	@JSONField(name="productId")
	private int productId;

	@JSONField(name="id")
	private int id;

	@JSONField(name="sfshipping")
	private String sfshipping;

	public void setShippingMap(ShippingMap shippingMap){
		this.shippingMap = shippingMap;
	}

	public ShippingMap getShippingMap(){
		return shippingMap;
	}

	public void setSfairline(String sfairline){
		this.sfairline = sfairline;
	}

	public String getSfairline(){
		return sfairline;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSfshipping(String sfshipping){
		this.sfshipping = sfshipping;
	}

	public String getSfshipping(){
		return sfshipping;
	}

	@Override
 	public String toString(){
		return 
			"SFShipping{" + 
			"shippingMap = '" + shippingMap + '\'' + 
			",sfairline = '" + sfairline + '\'' + 
			",productId = '" + productId + '\'' + 
			",id = '" + id + '\'' + 
			",sfshipping = '" + sfshipping + '\'' + 
			"}";
		}
}