package com.xquark.service.http.pojo.sf.base;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午11:43
 */
public class SFProductParam extends SFBaseParam {

  public SFProductParam() {
  }

  public SFProductParam(String app_key, String access_token, String timestamp) {
    super(app_key, access_token, timestamp);
  }
}
