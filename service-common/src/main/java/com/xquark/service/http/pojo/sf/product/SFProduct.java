package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.List;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 上午10:54
 */
public class SFProduct {

  @JSONField(name = "picture")
  private List<SFPicture> picture;

  private SFBaseInfo baseInfo;

  private Integer costPrice;

  private SFCategory categoryOne;

  private SFCategory categoryTwo;

  private SFCategory categoryThree;

  private SFBrand brand;

  private SFFinance finance;

  private SFShipping sfshipping;

  public List<SFPicture> getPicture() {
    return picture;
  }

  public void setPicture(List<SFPicture> picture) {
    this.picture = picture;
  }

  public SFBaseInfo getBaseInfo() {
    return baseInfo;
  }

  public void setBaseInfo(SFBaseInfo baseInfo) {
    this.baseInfo = baseInfo;
  }

  public Integer getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(Integer costPrice) {
    this.costPrice = costPrice;
  }

  public SFCategory getCategoryOne() {
    return categoryOne;
  }

  public void setCategoryOne(SFCategory categoryOne) {
    this.categoryOne = categoryOne;
  }

  public SFCategory getCategoryTwo() {
    return categoryTwo;
  }

  public void setCategoryTwo(SFCategory categoryTwo) {
    this.categoryTwo = categoryTwo;
  }

  public SFCategory getCategoryThree() {
    return categoryThree;
  }

  public void setCategoryThree(SFCategory categoryThree) {
    this.categoryThree = categoryThree;
  }

  public SFBrand getBrand() {
    return brand;
  }

  public void setBrand(SFBrand brand) {
    this.brand = brand;
  }

  public SFFinance getFinance() {
    return finance;
  }

  public void setFinance(SFFinance finance) {
    this.finance = finance;
  }

  public SFShipping getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(SFShipping sfshipping) {
    this.sfshipping = sfshipping;
  }

  @Override
  public String toString() {
    return "SFProduct{" +
        "baseInfo=" + baseInfo +
        ", costPrice=" + costPrice +
        '}';
  }
}
