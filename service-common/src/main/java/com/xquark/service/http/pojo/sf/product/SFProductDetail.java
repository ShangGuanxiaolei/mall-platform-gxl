package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFProductDetail {

  @JSONField(name = "blockId")
  private int blockId;

  @JSONField(name = "productId")
  private int productId;

  @JSONField(name = "httpsContent")
  private String httpsContent;

  @JSONField(name = "imagePath")
  private String imagePath;

  @JSONField(name = "id")
  private int id;

  @JSONField(name = "sort")
  private int sort;

  @JSONField(name = "title")
  private String title;

  @JSONField(name = "content")
  private String content;

  public void setBlockId(int blockId) {
    this.blockId = blockId;
  }

  public int getBlockId() {
    return blockId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public int getProductId() {
    return productId;
  }

  public void setHttpsContent(String httpsContent) {
    this.httpsContent = httpsContent;
  }

  public String getHttpsContent() {
    return httpsContent;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setSort(int sort) {
    this.sort = sort;
  }

  public int getSort() {
    return sort;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  @Override
  public String toString() {
    return
        "SFProductDetail{" +
            "blockId = '" + blockId + '\'' +
            ",productId = '" + productId + '\'' +
            ",httpsContent = '" + httpsContent + '\'' +
            ",imagePath = '" + imagePath + '\'' +
            ",id = '" + id + '\'' +
            ",sort = '" + sort + '\'' +
            ",title = '" + title + '\'' +
            ",content = '" + content + '\'' +
            "}";
  }
}