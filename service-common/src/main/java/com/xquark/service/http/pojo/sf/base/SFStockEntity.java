package com.xquark.service.http.pojo.sf.base;

/**
 * 除公共参数的请求参数实体类
 *
 * @auther liuwei
 * @date 2018/6/6 11:39
 */
public class SFStockEntity {

  private int invokeSource;//调用来源
  private int productId;//商品Id
  private int productNum;//商品数量
  private int regionId;//区域Id
  private int sfairline;//空运温控条件
  private int sfshipping;//陆运温控条件
  private String bomSn;//礼包编码

  public int getInvokeSource() {
    return invokeSource;
  }

  public void setInvokeSource(int invokeSource) {
    this.invokeSource = invokeSource;
  }

  public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public int getProductNum() {
    return productNum;
  }

  public void setProductNum(int productNum) {
    this.productNum = productNum;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  public int getSfairline() {
    return sfairline;
  }

  public void setSfairline(int sfairline) {
    this.sfairline = sfairline;
  }

  public int getSfshipping() {
    return sfshipping;
  }

  public void setSfshipping(int sfshipping) {
    this.sfshipping = sfshipping;
  }

  public String getBomSn() {
    return bomSn;
  }

  public void setBomSn(String bomSn) {
    this.bomSn = bomSn;
  }

  public SFStockEntity() {
  }

  public SFStockEntity(int invokeSource, int productId, int productNum, int regionId, int sfairline,
      int sfshipping, String bomSn) {
    this.invokeSource = invokeSource;
    this.productId = productId;
    this.productNum = productNum;
    this.regionId = regionId;
    this.sfairline = sfairline;
    this.sfshipping = sfshipping;
    this.bomSn = bomSn;
  }
}
