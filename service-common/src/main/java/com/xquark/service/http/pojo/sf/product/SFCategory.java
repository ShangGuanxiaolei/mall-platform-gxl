package com.xquark.service.http.pojo.sf.product;

public class SFCategory {

  private String categoryCode;
  private String categoryName;
  private Long categoryId;

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  @Override
  public String toString() {
    return
        "SFCategory{" +
            "categoryCode = '" + categoryCode + '\'' +
            ",categoryName = '" + categoryName + '\'' +
            ",categoryId = '" + categoryId + '\'' +
            "}";
  }
}
