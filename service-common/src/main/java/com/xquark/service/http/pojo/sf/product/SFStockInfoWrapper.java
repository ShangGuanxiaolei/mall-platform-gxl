package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.List;

/**
 * @auther liuwei
 * @date 2018/6/12 11:42
 */
public class SFStockInfoWrapper {

  @JSONField(name = "stockInfoList")
  private List<StockInfo> stockInfoList;

  public List<StockInfo> getStockInfoList() {
    return stockInfoList;
  }

  public void setStockInfoList(
      List<StockInfo> stockInfoList) {
    this.stockInfoList = stockInfoList;
  }
}
