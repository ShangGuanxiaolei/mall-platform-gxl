package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SFBrand {

  @JSONField(name = "englishName")
  private String englishName;

  @JSONField(name = "brandName")
  private String brandName;

  @JSONField(name = "siteUrl")
  private String siteUrl;

  @JSONField(name = "brandDesc")
  private String brandDesc;

  @JSONField(name = "brandId")
  private int brandId;

  @JSONField(name = "sortOrder")
  private String sortOrder;

  @JSONField(name = "brandLogo")
  private String brandLogo;

  @JSONField(name = "isShow")
  private Integer isShow;

  public String getEnglishName() {
    return englishName;
  }

  public void setEnglishName(String englishName) {
    this.englishName = englishName;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getSiteUrl() {
    return siteUrl;
  }

  public void setSiteUrl(String siteUrl) {
    this.siteUrl = siteUrl;
  }

  public String getBrandDesc() {
    return brandDesc;
  }

  public void setBrandDesc(String brandDesc) {
    this.brandDesc = brandDesc;
  }

  public int getBrandId() {
    return brandId;
  }

  public void setBrandId(int brandId) {
    this.brandId = brandId;
  }

  public String getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(String sortOrder) {
    this.sortOrder = sortOrder;
  }

  public String getBrandLogo() {
    return brandLogo;
  }

  public void setBrandLogo(String brandLogo) {
    this.brandLogo = brandLogo;
  }

  public Integer getIsShow() {
    return isShow;
  }

  public void setIsShow(Integer isShow) {
    this.isShow = isShow;
  }

  @Override
  public String toString() {
    return
        "SFBrand{" +
            "englishName = '" + englishName + '\'' +
            ",brandName = '" + brandName + '\'' +
            ",siteUrl = '" + siteUrl + '\'' +
            ",brandDesc = '" + brandDesc + '\'' +
            ",brandId = '" + brandId + '\'' +
            ",sortOrder = '" + sortOrder + '\'' +
            ",brandLogo = '" + brandLogo + '\'' +
            ",isShow = '" + isShow + '\'' +
            "}";
  }
}