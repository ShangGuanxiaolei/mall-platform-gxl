package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ShippingMap{

	@JSONField(name="sfairline")
	private String sfairline;

	@JSONField(name="sfshipping")
	private String sfshipping;

	public void setSfairline(String sfairline){
		this.sfairline = sfairline;
	}

	public String getSfairline(){
		return sfairline;
	}

	public void setSfshipping(String sfshipping){
		this.sfshipping = sfshipping;
	}

	public String getSfshipping(){
		return sfshipping;
	}

	@Override
 	public String toString(){
		return 
			"ShippingMap{" + 
			"sfairline = '" + sfairline + '\'' + 
			",sfshipping = '" + sfshipping + '\'' + 
			"}";
		}
}