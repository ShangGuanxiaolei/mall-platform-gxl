package com.xquark.service.http.pojo.sf.product;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.List;

/**
 * created by
 *
 * @author wangxinhua at 18-6-1 下午1:16
 */
public class SFProductWrapper {

  @JSONField(name = "productList")
  private List<SFProduct> productList;

  @JSONField(name = "totalCount")
  private Integer totalCount;

  public List<SFProduct> getProductList() {
    return productList;
  }

  public void setProductList(List<SFProduct> productList) {
    this.productList = productList;
  }

  public Integer getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }
}
