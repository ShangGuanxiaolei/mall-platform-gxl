package com.xquark.service.http;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.xquark.dal.consts.SCamURLConstants.SYNC_ADDRESS;
import static com.xquark.dal.consts.SCamURLConstants.SYNC_ADDRESS_DEL;
import static com.xquark.dal.consts.SCamURLConstants.SYNC_ADDRESS_SET_DEFAULT;
import static com.xquark.dal.consts.SCamURLConstants.SYNC_USER;
import static com.xquark.service.http.ResponseHandler.DISCARD_HANDLER;
import static com.xquark.utils.http.HttpHelper.parseToMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.xquark.dal.consts.SCamURLConstants;
import com.xquark.dal.mapper.AddressMapper;
import com.xquark.dal.mapper.SCrmHttpLogMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.Address;
import com.xquark.dal.model.SCrmHttpLog;
import com.xquark.dal.model.Zone;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;
import com.xquark.utils.http.po.SCrmAddressIdParam;
import com.xquark.utils.http.po.SCrmAddressParam;
import com.xquark.utils.http.po.SCrmParam;
import com.xquark.utils.http.po.SCrmUserParam;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wangxinhua on 18-2-28. DESC: 请求SCRM接口工具类 TODO wangxinhua 与{@link XquarkHttpUtils}
 * 一起作为继承体系
 */
@Component
public class SCrmHttpUtils {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final ExecutorService ec = ExecutorPool.HTTP_EXECUTOR_SERVICE;

  private final SCrmHttpLogMapper logMapper;

  private AddressMapper addressMapper;

  private UserMapper userMapper;

  @Autowired
  public SCrmHttpUtils(SCrmHttpLogMapper logMapper) {
    this.logMapper = logMapper;
  }

  @Autowired
  public void setAddressMapper(AddressMapper addressMapper) {
    this.addressMapper = addressMapper;
  }

  @Autowired
  public void setUserMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
  }

  /**
   * post请求接口 如果请求失败则返回null
   *
   * @param url 接口地址
   * @param param 参数
   * @param <T> 数据类型
   * @return {@link SCRMResponse<T>} 封装的请求结果
   */
  public <T> SCRMResponse<T> post(String url, SCrmParam param) {
    String jsonStr = JSON.toJSONString(param);
    Map<String, Object> params = parseToMap(jsonStr, String.class, Object.class);
    HttpInvokeResult result = PoolingHttpClients
        .post(url, params);
    if (!result.isOK()) {
      log(SCrmHttpLog.httpErrorLog(url, param, result.getException().getMessage(),
          result.getStatusCode()));
      return null;
    }
    String content = result.getContent();
    return JSON.parseObject(content, new TypeReference<SCRMResponse<T>>() {
    });
  }

  /**
   * FIXME 使用更好的异步处理方式 异步请求SCRM接口
   *
   * @param url 接口地址
   * @param param 参数
   * @param handler 请求处理类
   * @param <T> 结果数据类型
   */
  private <T> void postAsync(final String url, final SCrmParam param,
      final ResponseHandler<T> handler) {
    ec.execute(new Runnable() {
      @Override
      public void run() {
        SCRMResponse<T> response;
        try {
          response = post(url, param);
          handler.handleRes(response);
        } catch (Throwable throwable) {
          log(SCrmHttpLog.exceptionLog(url, param, throwable));
          return;
        }
        if (response.getReturnCode() == 0) {
          log(SCrmHttpLog.bizLog(url, param, response.getReturnMsg()));
          return;
        }
        log(SCrmHttpLog.successLog(url, param, response));
      }
    });
  }

  /**
   * 请求SCRM用户同步接口
   *
   * @param mobile 用户手机号
   * @param unionId 用户unionId
   */
  public void postSyncUserAsync(String mobile, String unionId) {
    SCrmParam params = new SCrmUserParam(mobile, unionId);
    // 除日志外无后续操作
    this.postAsync(SYNC_USER, params, DISCARD_HANDLER);
  }

  /**
   * 请求SCRM接口地址同步接口
   *
   * @param addressId 本地地址表id, 若调用成功需要将对方id更新到本地地址id中
   * @param addressParam 地址对象
   */
  private void postSyncAddressAsync(final String addressId, SCrmAddressParam addressParam) {
    this.postAsync(SYNC_ADDRESS, addressParam, new ResponseHandler<Integer>() {
      // 该接口data为对方地址id
      @Override
      public void handleRes(ServerResponse<Integer> res) {
        Integer extId = res.getData();
        // 如果对方地址id为null则应该返回错误的errorCode
        if (extId != null) {
//          addressMapper.updateExtIdByPrimaryKey(addressId, extId);
        }
      }
    });
  }

  /**
   * 将本地数据封装为post参数请求
   *
   * @param addressId 本地地址id, 用于回调时更新
   * @param address 本地地址表对象
   * @param zoneList 包含省市区的地区集合
   */
  public void postSyncAddressAsync(final String addressId, Address address, List<Zone> zoneList) {
    checkNotNull(address, "地址信息错误");
    checkArgument(zoneList != null && zoneList.size() == 4, "地区信息错误");
    String userId = address.getUserId();
    String unionId = userMapper.selectUnionIdByPrimaryKey(userId);
    // 索引0为中国
    Zone province = zoneList.get(1);
    Zone city = zoneList.get(2);
    Zone area = zoneList.get(3);
    SCrmAddressParam param = new SCrmAddressParam();
    param.setId(address.getId());
    param.setaId(address.getExtId());
    param.setProvinceId(province.getId());
    param.setProvinceName(province.getName());
    param.setCityId(city.getId());
    param.setCityName(city.getName());
    param.setAreaId(area.getId());
    param.setAreaName(area.getName());
    param.setContacts(address.getConsignee());
    param.setContactsMobile(address.getPhone());
    param.setStreetName(address.getStreet());
    param.setUnionId(unionId);
    param.setIsDefault(address.getIsDefault());
    this.postSyncAddressAsync(addressId, param);
  }


  /**
   * 请求SCRM地址删除接口
   *
   * @param aId scrm系统id
   */
  public void postDeleteAddressAsync(Integer aId) {
    // 除日志外无后续操作
    if (aId != null) {
      this.postAsync(SYNC_ADDRESS_DEL, new SCrmAddressIdParam(aId), DISCARD_HANDLER);
      return;
    }
    logger.warn("同步删除地址时没有提交地址id");
  }

  /**
   * 请求SCRM设置默认地址接口
   *
   * @param aId scrm系统id
   */
  public void postSetDefaultAddressAsync(Integer aId) {
    if (aId != null) {
      this.postAsync(SYNC_ADDRESS_SET_DEFAULT, new SCrmAddressIdParam(aId), DISCARD_HANDLER);
      return;
    }
    logger.warn("同步设置默认地址时没有提交地址id");
  }

  /**
   * 将请求结果记录日志
   */
  private void log(SCrmHttpLog log) {
    if (!log.getIsSuccess()) {
      logger.warn("请求SCRM地址 {} 错误, 错误原因: {}", log.getUrl(), log.getError());
    }
    boolean logResult = false;
    try {
      logResult = this.logMapper.insert(log) > 0;
    } catch (Exception e) {
      logger.error("------------ SCRM日志记录失败 ----------");
    }
    if (!logResult) {
      logger.error("------------ SCRM日志记录失败 ----------");
    }
  }

  @SuppressWarnings("unused")
  public static void main(String[] args) {
    SCrmParam userParam = new SCrmUserParam("18600000001",
        "ozLnPweRZBREqK8gxOFFXFFIaS4E");
    SCrmAddressParam addressParam = new SCrmAddressParam();
//		addressParam.setaId(107);
    addressParam.setProvinceId("330000");
    addressParam.setProvinceName("浙江省");
    addressParam.setCityId("330600");
    addressParam.setCityName("绍兴市");
    addressParam.setAreaId("330621");
    addressParam.setAreaName("绍兴县");
    addressParam.setContacts("签收人2");
    addressParam.setContactsMobile("18600000001");
    addressParam.setStreetName("街道地址");
    addressParam.setUnionId("ozLnPweRZBREqK8gxOFFXFFIaS4E");
    new SCrmHttpUtils(null)
        .post(SCamURLConstants.SYNC_ADDRESS,
            addressParam);
  }

}
