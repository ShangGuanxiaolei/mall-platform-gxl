package com.xquark.service.common;

import com.xquark.dal.IUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.xquark.service.BaseService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

public abstract class BaseServiceImpl implements BaseService {

  protected Logger log = LoggerFactory.getLogger(getClass());

  /**
   * 获取当前用户信息 如果是未登录的匿名用户，系统根据匿名用户唯一码自动创建一个用户 具体逻辑查看：UniqueNoFilter
   */
  public IUser getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof IUser) {
        return (IUser) principal;
      }

      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }

    throw new BizException(GlobalErrorCode.UNAUTHORIZED, "need login first.");
  }

//	public User getCurrentUser(boolean persist) {
//	    
//	}
}