package com.xquark.service.common;

import java.util.List;

public interface GrandPromotionService<T> {

  List<T> getPromotion(Integer grandSaleId, String cpId, Integer page, Integer pageSize);

}
