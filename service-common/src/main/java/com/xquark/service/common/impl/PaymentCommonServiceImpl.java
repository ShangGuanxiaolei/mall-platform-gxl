package com.xquark.service.common.impl;

import java.util.List;

import com.xquark.service.common.PaymentCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xquark.dal.mapper.PaymentMapper;
import com.xquark.dal.model.Payment;
import com.xquark.service.common.BaseServiceImpl;


@Service("paymentCommonService")
public class PaymentCommonServiceImpl extends BaseServiceImpl implements PaymentCommonService {

  @Autowired
  private PaymentMapper paymentMapper;

  private List<Payment> payments;

  @Override
  public List<Payment> load() {
    if (payments == null) {
      payments = paymentMapper.list();
    }
    return payments;
  }

  @Override
  public List<Payment> reload() {
    payments = paymentMapper.list();
    return payments;
  }
}
