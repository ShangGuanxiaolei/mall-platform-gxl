package com.xquark.service.common;

import java.util.List;

import com.xquark.dal.model.Payment;

public interface PaymentCommonService {

  List<Payment> load();

  List<Payment> reload();

}
