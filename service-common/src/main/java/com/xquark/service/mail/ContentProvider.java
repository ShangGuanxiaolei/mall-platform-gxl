package com.xquark.service.mail;

import java.util.Map;

/**
 * @author: chenxi
 */

public interface ContentProvider {

  public String generate(String template, Map<String, Object> context);
}
