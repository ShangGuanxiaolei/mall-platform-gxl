package com.xquark.service.mail;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: chenxi
 */

public class MailHeader {

  private String from;
  private String[] to;
  private String[] cc;
  private String[] bcc;
  private String[] replyTo;
  private String subject;

  private Map<String, Object> mailAttrs = new HashMap<String, Object>();

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String[] getTo() {
    return to;
  }

  public void setTo(String[] to) {
    this.to = to;
  }

  public String[] getCc() {
    return cc;
  }

  public void setCc(String[] cc) {
    this.cc = cc;
  }

  public String[] getBcc() {
    return bcc;
  }

  public void setBcc(String[] bcc) {
    this.bcc = bcc;
  }

  public String[] getReplyTo() {
    return replyTo;
  }

  public void setReplyTo(String[] replyTo) {
    this.replyTo = replyTo;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Map<String, Object> getMailAttrs() {
    return mailAttrs;
  }

  public void setMailAttrs(Map<String, Object> mailAttrs) {
    this.mailAttrs = mailAttrs;
  }

}
