package com.xquark.service.mail;

import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * @author: chenxi
 */

public class VelocityContentProvider implements ContentProvider {

  // configured through spring
  private VelocityEngine velocityEngine;

  @Override
  public String generate(String template, Map<String, Object> context) {
    return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "UTF-8", template, context);
  }

}
