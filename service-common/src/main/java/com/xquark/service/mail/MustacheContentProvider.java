package com.xquark.service.mail;

import java.util.Map;

import com.samskivert.mustache.Mustache;

/**
 * @author: chenxi
 */

public class MustacheContentProvider implements ContentProvider {

  @Override
  public String generate(String template, Map<String, Object> context) {
    return Mustache.compiler().compile(template).execute(context);
  }

}
