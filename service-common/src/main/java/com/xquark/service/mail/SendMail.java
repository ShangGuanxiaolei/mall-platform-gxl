package com.xquark.service.mail;

import com.xquark.service.BaseEntityService;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

public class SendMail {

  private String host = ""; // smtp服务器
  private String from = ""; // 发件人地址
  private String to = ""; // 收件人地址
  private String affix = ""; // 附件地址
  private String affixName = ""; // 附件名称
  private String user = ""; // 用户名
  private String pwd = ""; // 密码
  private String subject = ""; // 邮件标题

  public void setAddress(String to, String subject) {
    this.from = from;
    this.to = to;
    this.subject = subject;
  }

  public void setAffix(String affix, String affixName) {
    this.affix = affix;
    this.affixName = affixName;
  }

  public void send(File attachment) {

    Properties properties = new Properties();
    InputStream inputStream = BaseEntityService.class.getClassLoader()
        .getResourceAsStream("env/config-dev-hds.properties");
    try {
      properties.load(inputStream);
      this.host = properties.getProperty("mailServer.host");
      this.user = properties.getProperty("mailServer.username");
      this.pwd = properties.getProperty("mailServer.pwd");
      this.from = properties.getProperty("mailServer.username");
    } catch (IOException e) {
      e.printStackTrace();
    }
    Properties props = new Properties();

    // 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
    props.put("mail.smtp.host", host);
    // 需要经过授权，也就是有户名和密码的校验，这样才能通过验证
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", 465);
    props.put("mail.smtp.ssl.enable", true);
    // 用刚刚设置好的props对象构建一个session
    Session session = Session.getDefaultInstance(props);

    // 有了这句便可以在发送邮件的过程中在console处显示过程信息，供调试使
    // 用（你可以在控制台（console)上看到发送邮件的过程）
    session.setDebug(true);

    // 用session为参数定义消息对象
    MimeMessage message = new MimeMessage(session);
    try {
      // 加载发件人地址
      message.setFrom(new InternetAddress(from));
      // 加载收件人地址
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(
          to));
      // 加载标题
      message.setSubject(subject);

      // 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
      Multipart multipart = new MimeMultipart();

      // 设置邮件的文本内容
      BodyPart contentPart = new MimeBodyPart();
      contentPart.setText("This is my page.laololp");
      multipart.addBodyPart(contentPart);
      // 添加附件
//      BodyPart messageBodyPart = new MimeBodyPart();
//      DataSource source = new FileDataSource(affix);
//      BodyPart messageBodyPart = new MimeBodyPart();
//      DataSource source = new FileDataSource(attachment);
      // 添加附件的内容
//      messageBodyPart.setDataHandler(new DataHandler(source));
//      // 添加附件的标题
//      // 这里很重要，通过下面的Base64编码的转换可以保证你的中文附件标题名在发送时不会变成乱码
//      sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
//      messageBodyPart.setFileName("=?GBK?B?"
//          + enc.encode(affixName.getBytes()) + "?=");
//      multipart.addBodyPart(messageBodyPart);

      if (attachment != null) {
        BodyPart attachmentBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(attachment);
        attachmentBodyPart.setDataHandler(new DataHandler(source));

        // 网上流传的解决文件名乱码的方法，其实用MimeUtility.encodeWord就可以很方便的搞定
        // 这里很重要，通过下面的Base64编码的转换可以保证中文附件你的标题名在发送时不会变成乱码
        //sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
        //messageBodyPart.setFileName("=?GBK?B?" + enc.encode(attachment.getName().getBytes()) + "?=");

        //MimeUtility.encodeWord可以避免文件名乱码
        attachmentBodyPart.setFileName(MimeUtility.encodeWord(attachment.getName()));
        multipart.addBodyPart(attachmentBodyPart);
      }

      // 将multipart对象放到message中
      message.setContent(multipart);
      // 保存邮件
      message.saveChanges();
      // 发送邮件
      Transport transport = session.getTransport("smtp");
      // 连接服务器的邮箱
      transport.connect(host, user, pwd);
      // 把邮件发送出去
      transport.sendMessage(message, message.getAllRecipients());
      transport.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

//  public static void main(String[] args) {
//
//    File file = new File("C:\\content.csv");
//    try {
//      OutputStream os = new FileOutputStream(file);
//      BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os,
//          "utf-8"));
//      bw.write("hello");
//      bw.close();
//      os.close();
//    } catch (Exception e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//    }
//
//
//    SendMail cn = new SendMail();
//    // 设置发件人地址、收件人地址和邮件标题
//    cn.setAddress("362428280@qq.com", "page");
//    // 设置要发送附件的位置和标题
//    cn.setAffix("C:\\content.csv", "content.csv");
//    // 设置smtp服务器以及邮箱的帐号和密码
//    cn.send();
//  }
}