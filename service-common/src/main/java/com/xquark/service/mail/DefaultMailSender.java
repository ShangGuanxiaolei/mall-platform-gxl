package com.xquark.service.mail;

import java.io.File;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * @author: chenxi
 */

@Component("mailSender")
public class DefaultMailSender implements MailSender {

  private static final Logger LOG = LoggerFactory.getLogger(DefaultMailSender.class);

  @Autowired
  @Qualifier("JavaMailSender")
  private JavaMailSender jms;

  @Autowired
  private ContentProvider contentProvider;

  @Value("${mailServer.host}")
  private String mailFrom;

  @Override
  public void sendMessage(MailHeader header, String template,
      Map<String, Object> context) {
    try {
      MimeMessageHelper mmmHelper = new MimeMessageHelper(
          jms.createMimeMessage(), "UTF-8");

      String content = contentProvider.generate(template, context);

      if (StringUtils.isEmpty(header.getFrom())) {
        mmmHelper.setFrom(mailFrom);
      } else {
        mmmHelper.setFrom(header.getFrom());
      }
      mmmHelper.setTo(header.getTo());
      mmmHelper.setSubject(header.getSubject());
//      mmmHelper.setCc(header.getCc());
      mmmHelper.setText(content, true);

      jms.send(mmmHelper.getMimeMessage());
    } catch (MessagingException e) {
      LOG.error("mail send fail:{}", e);
    }
  }

  @Override
  public void sendMessage(MailHeader header, String template,
      Map<String, Object> context, List<File> attachments) {
    try {
      MimeMessageHelper mmmHelper = new MimeMessageHelper(
          jms.createMimeMessage(), true, "UTF-8");

      String content = contentProvider.generate(template, context);

      if (StringUtils.isEmpty(header.getFrom())) {
        mmmHelper.setFrom(mailFrom);
      } else {
        mmmHelper.setFrom(header.getFrom());
      }
      mmmHelper.setTo(header.getTo());
      mmmHelper.setSubject(header.getSubject());
      mmmHelper.setCc(header.getCc());
      mmmHelper.setText(content, true);

      for (File file : attachments) {
        mmmHelper.addAttachment(file.getName(), file);
      }
      jms.send(mmmHelper.getMimeMessage());
    } catch (MessagingException e) {
      throw new RuntimeException(e);
    }
  }

}
