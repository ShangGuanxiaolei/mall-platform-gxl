package com.xquark.service.mail;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * @author: chenxi
 */

@Component("contentProvider")
public class ThymeleafContentProvider implements ContentProvider {

  // configured through spring
  @Autowired
  @Qualifier("engine")
  private TemplateEngine engine;

  @Override
  public String generate(String template, Map<String, Object> context) {
    Context ctx = new Context();
    ctx.setVariables(context);
    Writer writer = new StringWriter();
    engine.process(template, ctx, writer);
    return writer.toString();
  }

}
