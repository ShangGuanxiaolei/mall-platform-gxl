package com.xquark.service.mail;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author: chenxi
 */

public interface MailSender {

  public void sendMessage(MailHeader header, String template, Map<String, Object> context);

  public void sendMessage(MailHeader header, String template,
      Map<String, Object> context, List<File> attachments);
}
