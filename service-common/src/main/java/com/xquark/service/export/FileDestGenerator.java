package com.xquark.service.export;

/**
 * @author: chenxi
 */

public interface FileDestGenerator {

  public String getFileDest();
}
