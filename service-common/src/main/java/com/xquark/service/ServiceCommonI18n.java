package com.xquark.service;

import com.xquark.utils.resultcode.ResultCode;
import com.xquark.utils.resultcode.ResultCodeMessage;
import com.xquark.utils.resultcode.ResultCodeUtil;

/**
 * @author: chenxi
 */

public enum ServiceCommonI18n implements ResultCode {

  ILLEGAL_QUERY_REQUEST,
  COMMERCIAL_SIGNATURE_ERROR;

  private final ResultCodeUtil util = new ResultCodeUtil(this);

  @Override
  public String getName() {
    return util.getName();
  }

  @Override
  public ResultCodeMessage getMessage() {
    return util.getMessage();
  }

  @Override
  public int getCode() {
    return util.getCode();
  }

  public String toString() {
    return getMessage().getMessage();
  }

}
