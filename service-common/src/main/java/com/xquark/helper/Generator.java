package com.xquark.helper;

/**
 * Created by wangxinhua on 17-11-23. DESC: 生成器接口
 */
public interface Generator<T> {

  /**
   * 获取下一个生成值
   *
   * @return 下一个值
   */
  T next();

  /**
   * 获取接下来连续的几个生成值
   *
   * @param times 连续多少次
   * @return 连续 {@code times} 次的值
   */
  T[] limit(int times);

  /**
   * 清空到初始状态
   *
   * @return this 方便清空后连续调用
   */
  Generator<T> clear();

}
