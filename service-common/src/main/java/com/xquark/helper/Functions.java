package com.xquark.helper;

import com.google.common.base.Function;
import com.xquark.dal.BaseEntity;
import com.xquark.utils.functional.BeanToPropertyFunction;
import com.xquark.utils.functional.EntryToObjFunction;
import com.xquark.utils.functional.MapLike;
import java.util.Map.Entry;

/**
 * 函数工具类
 *
 * @author wangxinhua.
 * @date 2018/9/28
 */
public class Functions {

  private final static Function<BaseEntity, String> ID_PROPERTY_FUNCTION
      = new BeanToPropertyFunction<>("id");

  /**
   * 包装id转换函数
   *
   * @return function of baseEntity -> id
   */
  @SuppressWarnings("unchecked")
  public static <T extends BaseEntity> Function<T, String> newIdPropertyFunction() {
    return (Function<T, String>) ID_PROPERTY_FUNCTION;
  }

  /**
   * 包装map => object转换函数
   */
  public static <K, V, T extends MapLike<K, V>> Function<Entry<K, V>, T> newEntryToObjFunction(
      Class<T> clazz) {
    return new EntryToObjFunction<>(clazz);
  }

}
