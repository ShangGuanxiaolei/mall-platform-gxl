package com.xquark.config;

import com.xquark.service.scan.CommonScanned;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackageClasses = CommonScanned.class)
@EnableTransactionManagement
@ImportResource({"classpath:/META-INF/applicationContext-service-common.xml"})
public class ServiceCommonConfig {

  @Value("${mailServer.host}")
  private String MAIL_HOST;

  @Value("${mailServer.port}")
  private Integer MAIL_PORT;

  @Value("${mailServer.encoding}")
  private String MAIL_ENCODING;

  @Value("${mailServer.username}")
  private String MAIL_ACCOUNT;

  @Value("${mailServer.pwd}")
  private String MAIL_PWD;

  @Value("${mailServer.protocol}")
  private String MAIL_PROTOCOL;

  /**
   * 邮件发送器
   * @return
   */
  @Bean(name = {"JavaMailSender"})
  public JavaMailSender mailSender() {
    JavaMailSenderImpl mailSender =new JavaMailSenderImpl();
    mailSender.setPort(MAIL_PORT);
    mailSender.setHost(MAIL_HOST);
    mailSender.setDefaultEncoding(MAIL_ENCODING);
    mailSender.setUsername(MAIL_ACCOUNT);
    mailSender.setPassword(MAIL_PWD);
    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", MAIL_PROTOCOL);
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.ssl.enable", "true");
    props.put("mail.smtp.socketFactory.port", MAIL_PORT);
    props.put("mail.smtp.socketFactory.class",
        "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.timeout", "1000");
    return mailSender;
  }
}