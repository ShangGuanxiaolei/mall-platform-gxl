package com.xquark.adapter;

import com.google.common.base.Function;
import com.xquark.dal.model.Promotion;
import com.xquark.dal.model.PromotionBaseInfo;
import com.xquark.dal.type.PromotionScope;
import com.xquark.dal.type.PromotionType;
import com.xquark.dal.type.PromotionUserScope;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * @author wangxinhua
 * @since 18-11-1 活动适配器
 */
public abstract class PromotionAdapter<T extends Promotion> implements
    Function<PromotionBaseInfo, T> {

  private final static Logger LOGGER = LoggerFactory.getLogger(PromotionAdapter.class);

  private final Class<T> clazz;

  private final BigDecimal discount;

  /**
   * 折扣率, 普通商品不存在
   */
  public PromotionAdapter(Class<T> clazz, BigDecimal discount) {
    this.clazz = clazz;
    this.discount = discount;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  /**
   * @param baseInfo * 转换 {@link PromotionBaseInfo} 为 {@link Promotion}
   * @param baseInfo 新类型的活动数据
   * @return 活动对象
   */
  @Override
  public T apply(PromotionBaseInfo baseInfo) {
    if (baseInfo == null) {
      return null;
    }
    PromotionType pType = transType(baseInfo.getpType());
    if (pType == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动类型不兼容");
    }
    T ret;
    try {
      ret = clazz.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      String msg = String.format("类型 %s 无法实例化", clazz.getSimpleName());
      LOGGER.error(msg, e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
    }
    ret.setType(pType);
    ret.setTitle(baseInfo.getpName());
    ret.setDiscount(transDiscount());
    ret.setValidFrom(baseInfo.getEffectFrom());
    //将当前时间加上一天传入，满足拼团活动不根据活动时间校验而是根据成团时间校验
    ret.setValidTo(transValidTo(baseInfo.getEffectTo()));
    ret.setIsPointUsed(true);
    ret.setScope(PromotionScope.ALL);
    ret.setUserScope(PromotionUserScope.ALL);
    // isDeleted 逻辑相反
    ret.setArchive(baseInfo.getIsDeleted() == 0);
    ret.setIsCountEarning(true);
    ret.setIsFreeDelivery(Objects.equals(baseInfo.getIsDeliveryReduction(), 1));//是否免邮,1->免邮
    // 子类设置子类的属性
    setExtra(ret);
    return ret;
  }

  /**
   * 转换新的活动类型对源类型做兼容
   */
  protected abstract PromotionType transType(String newType);

  /**
   * 若新的适配活动折扣率与原来的不兼容, 则复写该方法
   *
   * @return 转换的折扣率
   */
  protected BigDecimal transDiscount() {
    return getDiscount();
  }

  /**
   * 若新的活动的有效时间与原来不兼容, 则复写该方法
   */
  protected Date transValidTo(Date orgiDate) {
    return orgiDate;
  }

  protected void setExtra(T promotion) {
    // do nothing, implement by sub class
  }

}
