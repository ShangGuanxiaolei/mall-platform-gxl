package com.xquark.adapter;

import com.xquark.dal.model.PiecePromotion;
import com.xquark.dal.type.PromotionType;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * @author wangxinhua
 * @since 18-11-1
 */
public class PgPromotionAdapter extends PromotionAdapter<PiecePromotion> {

  private final boolean isNew;

  /**
   * 折扣率, 普通商品不存在
   */
  public PgPromotionAdapter(BigDecimal discount, boolean isNew) {
    super(PiecePromotion.class, discount);
    this.isNew = isNew;
  }

  @Override
  protected PromotionType transType(String newType) {
    if (StringUtils.equals(newType, "piece")) {
      return PromotionType.PIECE_GROUP;
    }
    return null;
  }

  @Override
  protected BigDecimal transDiscount() {
    // 拼团配置折扣率比原来的活动配置折扣率大10备
    if (this.getDiscount() == null) {
      return null;
    }
    return this.getDiscount().divide(BigDecimal.valueOf(10), 5, BigDecimal.ROUND_HALF_EVEN);
  }

  @Override
  protected Date transValidTo(Date orgiDate) {
    Date time = new Date();
    long endTime = time.getTime() + 24 * 1000 * 3600;
    return new Date(endTime);
  }

  @Override
  protected void setExtra(PiecePromotion promotion) {
    promotion.setIsNew(isNew);
  }

}
