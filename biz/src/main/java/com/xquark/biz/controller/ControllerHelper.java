package com.xquark.biz.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.mysql.jdbc.StringUtils;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;

public class ControllerHelper {

  public static void checkException(Errors errors) {
    checkException(errors, null);
  }

  public static void checkException(Errors errors, String errorMsg) {
    if (errors != null && errors.hasErrors()) {
      FieldError fe = errors.getFieldError();
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          fe == null ? errors.getGlobalError().getDefaultMessage()
              : fe.getDefaultMessage() + " " + fe.getField());
    } else if (!StringUtils.isNullOrEmpty(errorMsg)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, errorMsg);
    }
  }
}
