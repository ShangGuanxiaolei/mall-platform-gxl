package com.xquark.biz.listener;

import com.xquark.biz.msg.PushMessageService;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Message;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PushMessage;
import com.xquark.dal.type.PushMessageDeviceType;
import com.xquark.dal.type.PushMessageType;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.type.PushRefreshType;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.service.msg.MessageService;
import com.xquark.service.order.OrderService;
import com.xquark.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;

import java.util.Date;
import java.util.List;

//@Component
public class MessageNotifyListener implements ApplicationListener<MessageNotifyEvent> {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Value("${site.web.host.name}")
  private String domainName;

  @Autowired
  private PushMessageService pushMessageService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private UserService userService;

  @Autowired
  private MessageService messageService;

  //private String xcxHost = "16cdeb81.ngrok.io";
  private String xcxHost = "127.0.0.1:9999";

  @Async
  @Override
  public void onApplicationEvent(final MessageNotifyEvent event) {
    PushMessage androidBean = createBeanAndroid(event);
    Message message = (Message) event.getSource();
    // 如果是帮砍价或拼团消息，需要特殊处理
    if (event.getType() == 9004 || event.getType() == 9003) {
      String fromUserId = message.getData();
      String content = message.getContent() == null ? "" : message.getContent();
      messageService.sendBargainMessage(fromUserId, event.getUserId(), message.getId(), content);
    } else {
      messageService.sendMessage(event.getUserId(), message.getId());
    }

    //PushMessage iosBean = createBeanIos(event);
    try {
      /**if (event.getPlantForm().equals(PushMessageDeviceType.ALL)) {
       pushMessageService.send(androidBean);
       pushMessageService.send(iosBean);
       } else if (event.getPlantForm().equals(PushMessageDeviceType.ANDROID)) {
       pushMessageService.send(androidBean);
       } else if (event.getPlantForm().equals(PushMessageDeviceType.IOS)) {
       pushMessageService.send(iosBean);
       } else {
       log.warn("unsupported plantform");
       }**/
      // 不区分平台，所有消息都是所有android，ios都进行推送
      pushMessageService.send(androidBean);
    } catch (Exception e) {
      log.warn("push exception:" + e);
    }

  }

  private void msg4Order(String orderId, PushMessage pushMessage) {
    List<OrderItem> ois = orderService.listOrderItems(orderId);
    if (ois != null && ois.size() > 0) {
      pushMessage
          .setImageUrl(resourceFacade.resolveUrl(ois.get(0).getProductImg())); // 默认取第一个商品图片???
    }
    pushMessage.setOrderId(orderId);
  }

  private PushMessage createBeanAndroid(MessageNotifyEvent event) {
    Message message = (Message) event.getSource();
    PushMessage pushMessage = new PushMessage();
    pushMessage.setApp("hotshop");
    pushMessage.setTitle(message.getTitle());
    pushMessage.setDesc(message.getContent());
    pushMessage.setBaiduTagName(event.getBaiduTag());
    pushMessage.setUserId(event.getUserId());
    pushMessage.setMsgType(event.getType());
    pushMessage.setDetailUrl(event.getUrl());
    pushMessage.setType(PushMessageType.MESSAGE);
    pushMessage.setDeviceType(PushMessageDeviceType.ANDROID);

    String ahs = new String();  // 用于消息合并
    if (PushMsgType.MSG_ORDER.getValue() < event.getType()
        && event.getType() <= PushMsgType.MSG_ORDER.getValue() + 999) {
      pushMessage.setRefreshType(PushRefreshType.ORDER);
      if (StringUtils.isNotEmpty(event.getRelatId())) {
        msg4Order(event.getRelatId(), pushMessage);
        ahs = event.getType().toString() + "_" + event.getRelatId();
      }
    } else if (PushMsgType.MSG_AGENT_APPLY.getValue() == event.getType()) {
      pushMessage.setRefreshType(PushRefreshType.AGENT);
    } else if (PushMsgType.MSG_AGENT_AUDIT.getValue() == event.getType()) {
      pushMessage.setRefreshType(PushRefreshType.USERTYPE);
    } else if (event.getType() == PushMsgType.MSG_LEAVEMSGS.getValue()) {
      pushMessage.setOrderId(event.getRelatId());
      Long time = new Date().getTime();
      ahs = event.getType().toString() + "_" + time.toString();
    }
    pushMessage.setMsgId(ahs.hashCode());
    if (StringUtils.isEmpty(pushMessage.getImageUrl())) {
      pushMessage.setImageUrl(resourceFacade.getDefLogo()); // 默认图片
    }

    if (event.getType().equals(PushMsgType.MSG_NORMAL.getValue()) && StringUtils
        .isEmpty(event.getUrl())) {
      pushMessage.setDetailUrl(domainName + "/message/" + message.getId());
    } else {
      pushMessage.setDetailUrl(event.getUrl());
    }
    return pushMessage;
  }

}
