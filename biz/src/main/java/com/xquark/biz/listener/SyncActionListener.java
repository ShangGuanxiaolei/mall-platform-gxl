/**
 *
 */
package com.xquark.biz.listener;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.mapper.ProductImageMapper;
import com.xquark.dal.model.ProductImage;
import com.xquark.event.SyncActionEvent;
import com.xquark.service.fragment.FragmentImageService;
import com.xquark.service.fragment.ProductFragmentService;
import com.xquark.service.syncevent.KDSyncData;
import com.xquark.service.syncevent.SyncEventService;
import com.xquark.service.syncevent.SyncEventType;

/**
 * @author stone
 */
@Component
public class SyncActionListener implements ApplicationListener<SyncActionEvent> {

  @Autowired
  private SyncEventService syncEventService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Autowired
  private ProductFragmentService ProductFragmentService;

  @Autowired
  private FragmentImageService FragmentImageService;

  @Autowired
  private ProductImageMapper productImageMapper;

  @Value("${qiniu.extfmt}")
  String qiniuExtFmt;

  private static final Integer IMAGELIMIT = 30; // 最大图片数量
  //private Logger log = LoggerFactory.getLogger(getClass());

  private String cutqiniuExtFmtStr(String imgUrl) {
    if (imgUrl == null || imgUrl.isEmpty()) {
      return null;
    }
    int idx = imgUrl.indexOf(qiniuExtFmt);
    if (0 < idx && idx < imgUrl.length()) {
      return imgUrl.substring(0, idx);
    }
    return imgUrl;
  }

  @Async
  @Override
  public void onApplicationEvent(SyncActionEvent event) {
    KDSyncData data = (KDSyncData) event.getSource();

    if (event.getType().equals(SyncEventType.SHOP_CREATE) ||  // 店铺
        event.getType().equals(SyncEventType.SHOP_UPDATE)) {
      if (data.getShop() != null && data.getShop().getImg() != null) {
        data.getShop()
            .setImg(cutqiniuExtFmtStr(resourceFacade.resolveUrl2Orig(data.getShop().getImg())));
      }
    } else { // 商品
      if (data.getProdList() != null && data.getProdList().size() != 0) {
        List<ProductImage> aList = productImageMapper
            .getImgs4Sync(data.getProdList().get(0).getId());
        String mainPic = cutqiniuExtFmtStr(
            resourceFacade.resolveUrl2Orig(data.getProdList().get(0).getImg()));
        int cnt = 0;
        StringBuilder imgUrl = new StringBuilder();
        if (mainPic != null) {
          imgUrl.append(mainPic);
          imgUrl.append(";");
          cnt++;
        }
        if (aList != null && aList.size() != 0) {
          for (ProductImage pi : aList) {
            String img = cutqiniuExtFmtStr(resourceFacade.resolveUrl2Orig(pi.getImg()));
            if (mainPic != null && mainPic.equals(imgUrl)) {
              continue;
            }
            imgUrl.append(img);
            imgUrl.append(";");
            cnt++;
            if (cnt >= IMAGELIMIT) {
              break;
            }
          }
          data.getProdList().get(0).setImg(imgUrl.toString());
        }
      }
    }
    syncEventService.pushMQ(data);
  }
}
