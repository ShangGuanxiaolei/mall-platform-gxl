package com.xquark.biz.listener;

import java.util.Date;
import java.util.Formatter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.xquark.biz.msg.PushMessageService;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.dal.model.Message;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.PushMessage;
import com.xquark.dal.type.PushMsgType;
import com.xquark.event.XQMessageNotifyEvent;
import com.xquark.service.order.OrderService;

@Component
public class XQMessageNotifyListener implements ApplicationListener<XQMessageNotifyEvent> {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private PushMessageService pushMessageService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ResourceFacade resourceFacade;

  @Async
  @Override
  public void onApplicationEvent(final XQMessageNotifyEvent event) {
    PushMessage bean = createBean(event);
    if (0 != pushMessageService.pushToThirdServ(bean)) {
      log.debug("pushToThirdServ err");
    }
  }

  private void msg4Order(String orderId, PushMessage pushMessage, Message message) {
    Formatter fmt = new Formatter();
    try {
      List<OrderItem> ois = orderService.listOrderItems(orderId);
      if (ois != null && ois.size() > 0) {
        fmt.format(message.getContent(), ois.get(0).getProductName());
        pushMessage
            .setImageUrl(resourceFacade.resolveUrl(ois.get(0).getProductImg())); // 默认取第一个商品图片???
      } else {
        fmt.format(message.getContent(), "");
      }
      pushMessage.setDesc(fmt.toString());
      pushMessage.setOrderId(orderId);
    } finally {
      fmt.close();
    }
  }

  private PushMessage createBean(XQMessageNotifyEvent event) {
    Message message = (Message) event.getSource();
    PushMessage pushMessage = new PushMessage();
    pushMessage.setApp("hotshop");
    pushMessage.setTitle(message.getTitle());
    pushMessage.setDesc(message.getContent());
    pushMessage.setBaiduTagName(event.getBaiduTag());
    pushMessage.setUserId(event.getBaiduTag());
    pushMessage.setMsgType(event.getType());
    pushMessage.setDetailUrl(event.getUrl());
    pushMessage.setMsgId(0);
    pushMessage.setExtData(event.getExtDate());

    String ahs = new String();  // 用于消息合并
    if (PushMsgType.MSG_ORDER.getValue() < event.getType()
        && event.getType() <= PushMsgType.MSG_ORDER.getValue() + 999) {
      if (StringUtils.isNotEmpty(event.getRelatId())) {
        msg4Order(event.getRelatId(), pushMessage, message);
        ahs = event.getType().toString() + "_" + event.getRelatId();
      }
    } else if (event.getType() == PushMsgType.MSG_LEAVEMSGS.getValue()) {
      pushMessage.setOrderId(event.getRelatId());
      Long time = new Date().getTime();
      ahs = event.getType().toString() + "_" + time.toString();
    }
    pushMessage.setMsgId(ahs.hashCode());
    if (StringUtils.isEmpty(pushMessage.getImageUrl())) {
      pushMessage.setImageUrl(resourceFacade.getDefLogo()); // 默认图片
    }

    return pushMessage;
  }
}
