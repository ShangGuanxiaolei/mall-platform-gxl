package com.xquark.biz.util;

import com.xquark.dal.model.*;
import com.xquark.dal.status.TwitterStatus;
import com.xquark.dal.status.ValidType;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by root on 17-8-15.
 */
public class UserUtil {

  public void setCurrentSellerShopId(User user, String currentSellerShopId,
      TwitterShopComService twitterShopComService, ShopAccessLogService shopAccessLogService,
      ShopTreeService shopTreeService,
      ShopService shopService, UserTwitterService twitterService) {
    if ("undefined".equals(currentSellerShopId) || "null".equals(currentSellerShopId)) {
      currentSellerShopId = "";
    }
    // 当前用户店铺信息需要根据推客设置的引导消费者有效期进行获取
    String sellerShopId = "";
    String rootShopId = shopService.loadRootShop().getId();

    user.setRootSellerShopId(rootShopId);
    // 如果买家是推客，则卖家默认是总店
//    List<UserTwitter> twitters = twitterService.selectByUserId(user.getId());
//    if (twitters != null) {
//      for (UserTwitter twitter : twitters) {
//        if (twitter != null && twitter.getStatus() == TwitterStatus.ACTIVE) {
//          user.setCurrentSellerShopId(rootShopId);
//          return;
//        }
//      }
//    }
//
//    TwitterShopCommission twitterShopCommission = twitterShopComService.selectDefault();
//    ValidType validType = twitterShopCommission.getValidType();
//    // 永久: 以用户最先访问的小店链接为准有效
//    if (validType == ValidType.ALWAYS) {
//      ShopAccessLog shopAccessLog = shopAccessLogService.getFirstShop(user.getId());
//      if (shopAccessLog != null) {
//        sellerShopId = shopAccessLog.getShopId();
//      }
//    }
//    // 其他情况均按照最近一次访问的店铺为准
//    else {
//      // 如果当前访问店铺非总店
//      if (StringUtils.isNotEmpty(currentSellerShopId) && !currentSellerShopId.equals(rootShopId)) {
//        sellerShopId = currentSellerShopId;
//      }
//      // 如果访问的是总店，则取最近一次访问的店铺为准
//      else {
//        ShopAccessLog shopAccessLog = shopAccessLogService.getLastShop(user.getId());
//        if (shopAccessLog != null) {
//          sellerShopId = shopAccessLog.getShopId();
//        }
//      }
//    }

    // 如果之前访问的店铺已经找不到了，则设置为总店店铺，且将店铺访问记录表中的该店铺信息都删掉
//    Shop sellerShop = null;
//    if (StringUtils.isNotEmpty(sellerShopId)) {
//      sellerShop = shopService.load(sellerShopId);
//      if (sellerShop == null) {
//        shopAccessLogService.deleteByShopId(sellerShopId);
//      }
//    }
//
//    if (StringUtils.isNotEmpty(sellerShopId) && sellerShop != null) {
//      user.setCurrentSellerShopId(sellerShopId);
//    } else {
//      // 卖家的店铺可能已经不存在，如果再次访问则默认访问总店
//      Shop currentSellerShop = null;
//      if (StringUtils.isNotEmpty(currentSellerShopId)) {
//        currentSellerShop = shopService.load(currentSellerShopId);
//      }
//      if (StringUtils.isNotEmpty(currentSellerShopId) && currentSellerShop != null) {
//        user.setCurrentSellerShopId(currentSellerShopId);
//        String userId = user.getId();
//        ShopAccessLog log = new ShopAccessLog();
//        log.setArchive(false);
//        log.setShopId(currentSellerShopId);
//        log.setUserId(userId);
//        shopAccessLogService.insert(log);
//      } else {
//        user.setCurrentSellerShopId(rootShopId);
//      }
//    }
  }

}
