package com.xquark.biz.csrf;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import com.xquark.service.outpay.impl.AliPaymentImpl;

@Component("csrfMatcher")
public class CsrfMatcher implements RequestMatcher {

  private Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");

  @Override
  public boolean matches(HttpServletRequest request) {
    if (allowedMethods.matcher(request.getMethod()).matches()) {
      return false;
    } else {
      String actionPath = request.getRequestURI().substring(request.getContextPath().length());
      if (actionPath.equals(AliPaymentImpl.paymentBatch_notify_url)) {
        return false;
      }
      if (actionPath.startsWith(AliPaymentImpl.refundBatch_notify_url)) {
        return false;
      }
    }
    return true;
  }

}
