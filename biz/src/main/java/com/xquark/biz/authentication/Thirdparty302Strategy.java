package com.xquark.biz.authentication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;

/**
 * Use this class as the default login strategy instead of {@link SimpleMappingStrategy} to support
 * common 302 redirection.
 */

public class Thirdparty302Strategy extends SimpleMappingStrategy {

  private static final String BACKURL_PARAM = "backUrl";

  private static Logger LOG = LoggerFactory.getLogger(Thirdparty302Strategy.class);

  @Override
  public String buildLoginUrl(HttpServletRequest request,
      HttpServletResponse response, AuthenticationException exception) {
    final String serverName = request.getServerName();
    String thirdpartyUrl = mappings.get(serverName);
    String requestURL = request.getRequestURL().toString();
    if (request.getQueryString() != null) {
      requestURL += "?" + request.getQueryString();
    }
    try {
      thirdpartyUrl += "?" + BACKURL_PARAM + "=" + URLEncoder.encode(requestURL, "utf-8");
    } catch (final UnsupportedEncodingException e) {
      LOG.error(e.getMessage());
    }
    return thirdpartyUrl;
  }

}
