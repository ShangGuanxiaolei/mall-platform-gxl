package com.xquark.biz.authentication;

/**
 *
 *
 */

public interface LoginStrategyBuilder {

  // element
  String ROOT_EL = "login-strategies";
  String DOMAINS_EL = "domains";
  String DOMAIN_EL = "domain";
  String DEFAULT_EL = "default";

  // attribute
  String NAME_ATTR = "name";
  String TYPE_ATTR = "type";
  String URL_ATTR = "url";
  String STRAGETY_ATTR = "strategy";

  // specified value
  String SIMPLE_TYPE = "simple";
  String CUSTOMIZED_CLASS_TYPE = "customized-class";
  String CUSTOMIZED_REF_TYPE = "customized-ref";

  public LoginStrategy build() throws LoginConfigurationException;
}
