package com.xquark.biz.authentication;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.vdlm.common.bus.BusSignalListener;
import com.vdlm.common.bus.BusSignalManager;
import com.xquark.dal.model.DomainLoginStrategy;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.service.authentication.LoginStrategyEvent;
import com.xquark.service.authentication.LoginStrategyService;

/**
 *
 *
 */

public class DBLoginStrategyBuilder implements BusSignalListener<LoginStrategyEvent>,
    LoginStrategyBuilder {

  private final static Logger LOG = LoggerFactory.getLogger(DBLoginStrategyBuilder.class);

  @Value("${profiles.active}")
  private String profile;
  private final String module;
  @Autowired
  private LoginStrategyService loginStrategyService;

  public DBLoginStrategyBuilder(BusSignalManager bsm, String module) {
    bsm.bind(LoginStrategyEvent.class, this);
    this.module = module;
  }

  @Override
  public LoginStrategy build() throws LoginConfigurationException {
    final LoginStrategyProxy strategy = new LoginStrategyProxy();
    final List<DomainLoginStrategy> dlsList = loginStrategyService
        .loadLoginStrategies(profile, module);
    for (final DomainLoginStrategy dls : dlsList) {
      buildDomain(strategy, dls);
    }
    return strategy;
  }

  private void buildDomain(LoginStrategyProxy proxy, DomainLoginStrategy dls)
      throws LoginConfigurationException {
    final String name = dls.getDomainName();
    final String type = dls.getLoginStrategyType();
    if (type == null) {
      throw new LoginConfigurationException("no type attribute specified!");
    }
    if (!(SIMPLE_TYPE.equals(type) || CUSTOMIZED_CLASS_TYPE.equals(type)
        || CUSTOMIZED_REF_TYPE.equals(type))) {
      throw new LoginConfigurationException(
          "type must be simple or customized-class or customized-ref!");
    }
    final String value = dls.getLoginStrategyValue();
    if (SIMPLE_TYPE.equals(type)) {
      if (value == null) {
        throw new LoginConfigurationException(
            "domain with simple type must specify the url attribute!");
      }
      proxy.registerSimple(name, value);
    }
    if (CUSTOMIZED_CLASS_TYPE.equals(type)) {
      if (value == null) {
        throw new LoginConfigurationException(
            "domain with customized type must specify the strategy attribute!");
      }
      proxy.registerCustomized(name, value);
    }
    if (CUSTOMIZED_REF_TYPE.equals(type)) {
      if (value == null) {
        throw new LoginConfigurationException(
            "domain with customized type must specify the strategy attribute!");
      }
      proxy.registerCustomized(name, (LoginStrategy) SpringContextUtil.getBean(value));
    }
  }

  @Override
  public void signalFired(LoginStrategyEvent signal) {
    // TODO Auto-generated method stub

  }
}
