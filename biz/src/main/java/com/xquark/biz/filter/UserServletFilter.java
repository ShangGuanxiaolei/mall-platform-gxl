package com.xquark.biz.filter;

import com.xquark.biz.util.UserUtil;
import com.xquark.config.PropertiesUtils;
import com.xquark.dal.model.User;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.dal.status.UserStatus;
import com.xquark.dal.vo.BaseCustomerVO;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.shop.ShopAccessLogService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.twitter.TwitterShopComService;
import com.xquark.service.twitter.UserTwitterService;
import com.xquark.service.user.UserService;
import com.xquark.service.user.impl.WechatThirdUserConverter;
import com.xquark.service.wechat.WechatUtils;
import com.xquark.utils.EmojiFilter;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.oauth.OAuthException;
import com.xquark.wechat.oauth.OAuthManager;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenRequest;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenResponse;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoRequest;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class UserServletFilter implements Filter {

  private final static WechatAppConfig STATIC_APP_CONFIG;

  private final static Config STATIC_CONFIG;

  private final static String UPLINE_KEY = "upline";

  private final static Comparator<BaseCustomerVO> PLATFORM_BASED_COMPARATOR = new Comparator<BaseCustomerVO>() {
    @Override
    public int compare(BaseCustomerVO o1, BaseCustomerVO o2) {
      if (o1 == null) {
        return -1;
      }
      if (o2 == null) {
        return 1;
      }
      // 按商城平台倒序, 汉薇(3)优先
      return -o1.getSource().compareTo(o2.getSource());
    }
  };

  static {
    String appId = "wxff737688dbc55e59";
    String secret = "693f78317aa773d30cd0761a86c04e9b";
    STATIC_APP_CONFIG = new WechatAppConfig();
    STATIC_APP_CONFIG.setAppName("gh_f89c609321d5");
    STATIC_APP_CONFIG.setAppId(appId);
    STATIC_APP_CONFIG.setAppSecret(secret);

    STATIC_CONFIG = new Config("", "", "", appId,
        secret, "", "", "", "");
  }

  public static final String FORM_VD_PHONE_LGN = "formVdPhoneLgn";

  private final String USER_KEY = "loginname";

  private final String CLIENT_ID = "vd_cid";

  //更新用户当前访问的卖家店铺id并且会刷新session
  public static final String CURRENT_SELLER_SHOP_ID_PARAM_NAME = "currentSellerShopId";

  private Logger log = LoggerFactory.getLogger(getClass());

  private static final String[] NEED_SIGNED_MODULE = {
      "/user/userCenter",
      "/shop",
      "/cart/next",
      "/order",
      "/twitter",
      "/twitterCenter",
      "/p",
  };

  public void destroy() {
  }

  public void doFilter(ServletRequest request, ServletResponse response,
      FilterChain chain) throws IOException, ServletException {

    // Please note that we could have also used a cookie to
    // retrieve the user name
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse resp = (HttpServletResponse) response;

    final String uri = req.getRequestURI().startsWith("/") ? req.getRequestURI().substring(1) : req.getRequestURI();
    //特殊情况 判断一下url是不是指定的url

    if(uri.matches("^v2/dna/.*$")
            || uri.matches("^v2/wx/.*$")
            || uri.matches("^v2/tx/.*$")
            || uri.matches("v2/sendSmsCode")
            || uri.matches("v2/register/verifyDna")
            || uri.matches("v2/register/bindPhoneDna")
            || uri.matches("v2/register/bindPhoneDna/verify")
            || uri.matches("v2/wechat/createQrCode")
            || uri.matches("v2/or/.*$")
    ) {
      chain.doFilter(request, response);
      return;
    }



    //微信用户创建及更新session
    try {
      CreateSessionForWechatApp(req, resp);
    } catch (BizException e) {
      log.error("CreateSessionForWechatApp", e);
    }

    //WAP,WEB用户创建及更新
    User user;
    String phone = req.getParameter(FORM_VD_PHONE_LGN);
    //已是手机号登录用户再次输入不同手机号，将会重新创建新的用户 并覆盖原有的Cookies 值
    //已是手机号登录用户再次输入相同手机号
    if (StringUtils.isNotEmpty(phone)) {  //提交手机号码

      WebApplicationContext webApplicationContext = WebApplicationContextUtils
          .getWebApplicationContext(request.getServletContext());
      UserService userService = (UserService) webApplicationContext.getBean("userService");

      user = userService.loadAnonymousByPhone(phone);//通过电话号码获取匿名用户 避免用户跨浏览器访问

      if (user == null) {
        String cid = null;
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
          for (Cookie cookie : cookies) {
            if (cookie.getName().equals(CLIENT_ID) && cookie.getValue() != null) {   //未输入的匿名用户
              cid = cookie.getValue();
              break;
            }
          }
        }
        if (cid != null) { //将匿名用户数据更新为手机登录用户
          user = userService.loadByMoreParam(null, null, null, cid); //从已注册用户中加载用户信息
          if (user == null) {
            user = userService.loadByLoginname(cid);
            if (user != null && user.getPhone() == null) {
              int result = userService.updateAnonymousPhone(cid, phone);
              if (result > 0) {
                user.setPhone(phone);
              }
            }
            if (user != null) {
              user.setUserStatus(UserStatus.ANONYMOUS);
            }
          } else {
            user.setUserStatus(UserStatus.DETACHED);
            user.setPhone(phone);
            userService.updateUserInfo(user);
          }
        }
        if (user == null) {
          String partner = request.getParameter("partner");
          String newCid = UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.CID);
          Cookie newCookie = new Cookie(CLIENT_ID, newCid);
          newCookie.setMaxAge(Integer.MAX_VALUE);
          newCookie.setPath("/");
          resp.addCookie(newCookie);
          request.setAttribute(CLIENT_ID, newCid);

          user = userService.createAnonymous(partner, phone, newCid);
          user.setUserStatus(UserStatus.ANONYMOUS);
        }
      } else {
        user.setUserStatus(UserStatus.DETACHED);
      }

      if (user != null) {
        setCurrentSellerShopId(req, null, user);
        Authentication auth = new UsernamePasswordAuthenticationToken(user, null,
            user.getAuthorities());
        SecurityContextHolder.clearContext();
        SecurityContextHolder.getContext().setAuthentication(auth);
      }
    }

    Principal principal = req.getUserPrincipal();
    if (principal != null) {
//      String username = principal.getName();
//      successfulRegistration = registerUsername(username);
    }
    try {
      //对所有的微信Oauth跳转请求,不再执行后续Filter
      if (resp.getStatus() == HttpServletResponse.SC_MOVED_TEMPORARILY
          && resp.getHeader("Location")
          .contains(OAuthManager.HTTPS_OPEN_WEIXIN_QQ_COM_CONNECT_OAUTH2_AUTHORIZE)) {
        return;
      } else {
        chain.doFilter(request, response);
      }
    } finally {
//      if (successfulRegistration) {
//        MDC.remove(USER_KEY);
//      }
    }
  }

  /**
   * 创建微信公众号访问商城的用户session
   */
  private boolean CreateSessionForWechatApp(HttpServletRequest request,
      HttpServletResponse response) throws BizException {
    String upline = request.getParameter(UPLINE_KEY);
    boolean isFromWechatApp = WechatUtils.isFromWechatApp(request);
    boolean ret = false;
    if (isFromWechatApp) {
      String code = request.getParameter("code");
      String state = request.getParameter("state");
      if (isNotEmpty(code) && isNotEmpty(state)) {
        //来自微信的请求,并且已通过Oauth授权,通常请求来自微信公众号菜单项(已默认先访问Oauth授权),或者经过处理的分享链接,或者未经过授权直接访问的链接被强制跳转授权
        WebApplicationContext webApplicationContext = WebApplicationContextUtils
            .getWebApplicationContext(request.getServletContext());
        UserService userService = (UserService) webApplicationContext.getBean("userService");
        ShopService shopService = (ShopService) webApplicationContext.getBean("shopService");
        CustomerProfileService customerProfileService = (CustomerProfileService) webApplicationContext
            .getBean("customerProfileServiceImpl");
        WechatThirdUserConverter wechatUserConverter = (WechatThirdUserConverter) webApplicationContext
            .getBean("wechatThirdUserConverter");

//        String shopId = wechatService.getShopIdFromState(state);
//        if (shopId == null) {
//          log.error("shopId can not be found in state parameter:" + state);
//          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "微信链接参数无效");
//        }
        try {
          GetAccessTokenResponse oauthRes = OAuthManager.getAccessToken(
              new GetAccessTokenRequest(WechatUtils.convertWechatAppConfig(STATIC_APP_CONFIG),
                  code));
          String openId = oauthRes.getOpenid();
          String unionId = oauthRes.getUnionid();
          User user;
          boolean isUnionIdExists = customerProfileService.isWechatUnionExists(unionId);
          if (!isUnionIdExists) {
            WechatUserBean wechatUser;
            try {
              GetUserinfoRequest userInfo = new GetUserinfoRequest(STATIC_CONFIG,
                  oauthRes.getAccess_token(),
                  openId);
              wechatUser = OAuthManager.getUserinfo(userInfo);
            } catch (Exception e) {
              throw new BizException(GlobalErrorCode.WECHAT_OAUTH_CODE_NOT_VAILD, "获取微信信息失败: ", e);
            }
            user = userService
                .createUserFromThirdUserIfNotExists(wechatUserConverter,
                    wechatUser, upline,"");
          } else {
            List<BaseCustomerVO> baseCustomerVOS = customerProfileService
                .listCustomerByUnionId(unionId);
            if (CollectionUtils.isNotEmpty(baseCustomerVOS)) {
              // 优先取汉薇用户
              Collections.sort(baseCustomerVOS, PLATFORM_BASED_COMPARATOR);
              user = userService.loadByCpId(baseCustomerVOS.get(0).getCpId());
            } else {
              throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "登录异常");
            }
          }
          if (user != null) {
            user.setLoginname(openId);
            user.setRootSellerShopId(shopService.loadRootShop().getId());
            if (StringUtils.isNoneBlank(upline)) {
              // 如果用户已经在登录态, 新的分享人也应该要刷新upline
              resetUpline(user, upline);
            }
            renewAuth(user);
          }
          ret = true;
        } catch (OAuthException e) {
          log.error("error:" + e.getMessage(), e);
        }
      } else {
        String url = request.getScheme() + "://" + request.getServerName();
        // 静态资源跟Go服务不走oAuth
        if (StringUtils.startsWith(request.getRequestURL().toString(), url + "/_resources")
            || (StringUtils.startsWith(request.getRequestURL().toString(), url + "/v1")
            || StringUtils.contains(request.getRequestURL().toString(), "favicon.ico"))) {
          return true;
        }

        WebApplicationContext webApplicationContext = WebApplicationContextUtils
            .getWebApplicationContext(request.getServletContext());
        String siteHost = webApplicationContext.getBean(PropertiesUtils.class)
            .getPropertiesValue("site.webapi.host.name");
        //来自微信的正常请求,不带code和state等oauth校验参数
        User currentUser = getCurrentUser();
        if (currentUser == null) {     // 尚未登陆
          setRedirect(request, response, getWechatAppConfig(request, url),
              OAuthManager.SCOPE_USERINFO, siteHost);
        } else {
          resetUpline(currentUser, upline);
          renewAuth(currentUser);
        }
      }
    }
    return ret;
  }

  private void renewAuth(User oldUser) {
    User newUser = new User();
    if (oldUser.getCreatedAt() == null) {
      oldUser.setCreatedAt(new Date());
    }
    if (oldUser.getUpdatedAt() == null) {
      oldUser.setUpdatedAt(new Date());
    }
    try {
      BeanUtils.copyProperties(newUser, oldUser);
    } catch (IllegalAccessException e) {
      log.error("renewAuth error", e);
    } catch (InvocationTargetException e) {
      log.error("renewAuth error", e);
    }
    Authentication auth = new UsernamePasswordAuthenticationToken(newUser, null,
        newUser.getAuthorities());
    SecurityContextHolder.clearContext();
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  private void getUserInfo(UserService userService, WechatAppConfig appConfig,
      GetAccessTokenResponse oauthRes, User user) throws OAuthException {
    GetUserinfoRequest getUserinfoRequest = new GetUserinfoRequest(
        WechatUtils.convertWechatAppConfig(appConfig),
        oauthRes.getAccess_token(),
        oauthRes.getOpenid()
    );
    GetUserinfoResponse userInfoRes = OAuthManager.getUserinfo(getUserinfoRequest);
    user.setAvatar(userInfoRes.getAvatar());
    String nickname = EmojiFilter.filterEmoji(userInfoRes.getNickName());
    // 防止微信昵称全部是emoji符号，导致返回的昵称为空，则默认赋值普通用户
    if (nickname == null || "".equals(nickname)) {
      nickname = "普通用户";
    }
    user.setName(nickname);
    user.setWechatName(nickname);
    userService.updateUserInfo(user);
  }

  private WechatAppConfig getWechatAppConfig(HttpServletRequest request, String url) {
//    WebApplicationContext webApplicationContext = WebApplicationContextUtils
//        .getWebApplicationContext(request.getServletContext());
//    WechatAppConfigService wechatAppConfigService = (WechatAppConfigService) webApplicationContext
//        .getBean("WechatAppConfigService");

//    List<WechatAppConfig> appConfigs = wechatAppConfigService.selectByUrlBegin(url);
//    if (CollectionUtils.isNotEmpty(appConfigs) && appConfigs.size() > 1) {
//      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "多个公众号使用该地址:" + url);
//    } else if (CollectionUtils.isEmpty(appConfigs)) {
//      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "找不到对应的公众号:" + url);
//    }

    return STATIC_APP_CONFIG;
  }

  private void setRedirect(HttpServletRequest request, HttpServletResponse response,
      WechatAppConfig appConfig, String scope, String siteHost) {
    String redirectUrl = OAuthManager.generateRedirectURI(
        WechatUtils.convertWechatAppConfig(appConfig),
        siteHost + request.getServletPath() + (request.getQueryString() == null ? ""
            : "?" + request.getQueryString()),
        scope,
        OAuthManager.STATE_FLAG);

    response.reset();
    response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
    response.setHeader("Location", redirectUrl);
  }

  /**
   * 从当前请求的上下文中获取用户当前访问的店铺 如果request包含参数currentSellerShopId则将User对象中的currentSellerShopId赋值
   * 否则如果信息不存在,则将appconfig中的总店赋值给User的currentSellerShopId
   */
  private void setCurrentSellerShopId(HttpServletRequest request, WechatAppConfig appConfig,
      User user) {
    String currentSellerShopId = request.getParameter(CURRENT_SELLER_SHOP_ID_PARAM_NAME);
    if ("undefined".equals(currentSellerShopId) || "null".equals(currentSellerShopId)) {
      currentSellerShopId = "";
    }
    WebApplicationContext webApplicationContext = WebApplicationContextUtils
        .getWebApplicationContext(request.getServletContext());
    TwitterShopComService twitterShopComService = (TwitterShopComService) webApplicationContext
        .getBean("twitterShopComService");
    ShopAccessLogService shopAccessLogService = (ShopAccessLogService) webApplicationContext
        .getBean("shopAccessLogService");
    ShopTreeService shopTreeService = (ShopTreeService) webApplicationContext
        .getBean("shopTreeService");
    ShopService shopService = (ShopService) webApplicationContext.getBean("shopService");
    UserTwitterService twitterService = (UserTwitterService) webApplicationContext
        .getBean("userTwitterService");
    // 获取当前用户的卖家信息
    UserUtil util = new UserUtil();
    util.setCurrentSellerShopId(user, currentSellerShopId, twitterShopComService,
        shopAccessLogService, shopTreeService, shopService, twitterService);
  }

  private User getCurrentUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      }

      if (auth.getClass().getSimpleName().indexOf("Anonymous") < 0) {
        log.error("Unknown authentication encountered, ignore it. " + auth);
      }
    }
    return null;
  }

  public void init(FilterConfig arg0) throws ServletException {
  }

  /**
   * Register the user in the MDC under USER_KEY.
   *
   * @return true id the user can be successfully registered
   */
  private boolean registerUsername(String username) {
    if (username != null && username.trim().length() > 0) {
      MDC.put(USER_KEY, username);
      return true;
    }
    return false;
  }

  private void resetUpline(User user, String upline) {
    Long uplineL;
    if (StringUtils.isNotBlank(upline)) {
      try {
        uplineL = Long.valueOf(upline);
        user.setUpline(uplineL);
      } catch (Exception e) {
        log.warn("请求参数upline类型不正确");
      }
    }
  }

}
