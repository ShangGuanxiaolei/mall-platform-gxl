package com.xquark.biz.verify;

import com.xquark.biz.verify.impl.VerificationFacadeImpl.SmsType;

import javax.servlet.http.HttpSession;

public interface VerificationFacade {

  boolean generateCodeForMiniProgram(String mobile, String thirdSessionId);

  /**
   * 发送手机验证码；控制生成节奏，验证码有效期1小时
   */
  boolean generateCode(String mobile, SmsType smsType);
  boolean generateCodeDNA(String mobile, SmsType smsType, HttpSession session);
  boolean verifyCodeForMiniProgram(String mobile, String code, String thirdSessionId);

  /**
   * 验证验证码
   *
   * @param smsType 消息类型 sign modify_bank
   */
  boolean verifyCode(String mobile, String code, SmsType smsType);

  boolean verifyCodeDNA(String cpid,String mobile, String code, SmsType smsType,HttpSession session);
}
