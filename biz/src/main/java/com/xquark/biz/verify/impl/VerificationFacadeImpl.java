package com.xquark.biz.verify.impl;

import com.xquark.biz.verify.VerificationFacade;
import com.xquark.dal.mapper.CustomerProfileMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.User;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.push.PushService;
import com.xquark.service.wxservice.config.WeChatConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

@Component
public class VerificationFacadeImpl implements VerificationFacade {

  @Autowired
  PushService pushService;
  //private final static String SESSIONKEY_SMS_CODE = "_SESSIONKEY_SMS_CODE";

  @Autowired
  UserMapper userMapper;
  @Autowired
  CustomerProfileMapper customerProfileMapper;

  @Autowired
  private RedisTemplate<String, Object> redisTemplate;

  private final Logger log = LoggerFactory.getLogger(getClass());

  public enum SmsType {
    SIGN("sms.signin.msg", "_SMS_SIGN_CODE", 1800),
    MODIFY_PHONE("sms.signin.msg", "_SMS_MODIFYPHONE_CODE", 1800),
    MODIFY_PWD("sms.modifyPwd.msg", "_SMS_MODIFYPWD_CODE", 1800),
    MODIFY_BANK("sms.modifyBank.msg", "_SMS_MODIFYBANK_CODE", 1800),
    INTERNAL_SHOP("sms.internalShop.msg", "_SMS_INTERNALSHOP_CODE", 1800);

    private String msgCode;  //短信内容
    private String sessionkeyCode; //验证码存放的session
    private int validSeconds = 1800;

    private SmsType(String msgCode, String sessionkeyCode, int seconds) {
      this.msgCode = msgCode;
      this.sessionkeyCode = sessionkeyCode;
      this.validSeconds = seconds;
    }

    public String getMsgCode() {
      return this.msgCode;
    }

    public String getSessionkeyCode() {
      return this.sessionkeyCode;
    }

    public int getValidSeconds() {
      return this.validSeconds;
    }
  }

  /**
   * 非线上环境，跳过短信验证
   */
  @Value("${sms.gateway.off}")
  private boolean verifySkip;

  @Override
  public boolean generateCodeForMiniProgram(String mobile, String thirdSessionId) {
    VeriCode veriCode = getVeriCodeFromRedis(mobile, thirdSessionId);
    WeChatConfig.smsCode=veriCode.smsCode;
    System.out.println("==================================================code"+veriCode.smsCode);
    if (!veriCode.canSend()) {
      return false;
    }
    boolean sent = verifySkip || pushService.sendSmsEngine(mobile, veriCode.smsCode);
    if (sent) {
      veriCode.sent();
    }
    return sent;
  }

  //chp 增
  /*
   * @Author chp
   * @Description 生成验证码随机数
   * @Date
   * @Param
   * @return
   **/
  public static int randomCode() {
    return (int) ((Math.random() * 9 + 1) * 100000);
  }


  @Override
  public boolean generateCodeDNA(String mobile, SmsType smsType,HttpSession session) {
    int i = randomCode();
    String codes  = String.valueOf(i);
//      session.setAttribute( mobile,codes);
    Boolean  aBoolean  = setRedis(mobile, codes);
    if (!aBoolean){throw  new BizException(GlobalErrorCode.INTERNAL_ERROR,"验证存入失败");
    }
    WeChatConfig.smsCode=codes;
//      HttpServletRequest request = getServletRequest();
//      RequestContext requestContext = new RequestContext(request);
//    String veriCodeMsg = requestContext.getMessage(smsType.msgCode, new Object[]{veriCode.smsCode});
//    log.debug("您要发送的短信内容为：" + veriCodeMsg);
    //boolean sent = verifySkip || pushService.sendSms(mobile, veriCodeMsg.replace("{veriCode}", veriCode.smsCode));

    boolean sent = verifySkip || pushService.sendSmsEngine(mobile, codes);
    WeChatConfig.smsCode=codes;
    return sent;
  }

  //结尾

  @Override
  public boolean generateCode(String mobile, SmsType smsType) {
    HttpSession s = getServletRequest().getSession();
    VeriCode veriCode = (VeriCode) s.getAttribute(smsType.sessionkeyCode);
    if (veriCode == null || !veriCode.valid(mobile)) {
      if (veriCode == null) {
        log.debug("generateCode : veriCode is null");
      } else if (!veriCode.valid(mobile)) {
        log.debug("generateCode : mobile " + mobile + " valid false");
      }
      veriCode = new VeriCode(mobile, smsType);
      s.setAttribute(smsType.sessionkeyCode, veriCode);
    }

    if (!veriCode.canSend()) {
      return false;
    }

    HttpServletRequest request = getServletRequest();
    RequestContext requestContext = new RequestContext(request);
//    String veriCodeMsg = requestContext.getMessage(smsType.msgCode, new Object[]{veriCode.smsCode});
//    log.debug("您要发送的短信内容为：" + veriCodeMsg);
    //boolean sent = verifySkip || pushService.sendSms(mobile, veriCodeMsg.replace("{veriCode}", veriCode.smsCode));

    boolean sent = verifySkip || pushService.sendSmsEngine(mobile, veriCode.smsCode);
    if (sent) {
      veriCode.sent();
    }
    return sent;
  }

  @Override
  public boolean verifyCodeForMiniProgram(String mobile, String code, String thirdSessionId) {
    long functionSet = 0;
    // 根据手机号查询用户FunctionSet字段，为99代表登陆不需要短信验证码
    User user = userMapper.loadAnonymousByPhone(mobile);
    if (user != null) {
      functionSet = userMapper.getFunctionSet(mobile);
    }
    // 18696153096为ios上架测试帐号，不需要短信验证码
    if (verifySkip || "18696153096".equals(mobile) || functionSet == 99) {
      return true;
    }

    VeriCode veriCode = getVeriCodeFromRedis(mobile, thirdSessionId);
    if (veriCode != null) {
      log.debug("verifyCode: veriCode is not null");
      if (veriCode.verify(mobile, code)) {
        log.debug("verifyCode: veriCode.verify is true");
      }
    }
    boolean success = veriCode != null && veriCode.verify(mobile, code);
    if (success) {
      redisTemplate.delete(generateVeriCodeKey(mobile, thirdSessionId));
    }
    return success;
  }

  /**
   * 小程序中session无效， 改用redis存放
   *
   * @deprecated {@link VerificationFacadeImpl#verifyCodeForMiniProgram(String, String, String)}
   */
  @Deprecated
  @Override
  public boolean verifyCode(String mobile, String code, SmsType smsType) {
    long functionSet = 0;
    // 根据手机号查询用户FunctionSet字段，为99代表登陆不需要短信验证码
    User user = userMapper.loadAnonymousByPhone(mobile);
    if (user != null) {
      functionSet = userMapper.getFunctionSet(mobile);
    }
    // 18696153096为ios上架测试帐号，不需要短信验证码
    if (verifySkip || "18696153096".equals(mobile) || functionSet == 99) {
      return true;
    }

    // 后门短信验证码
    if (StringUtils.equals(code, "123026")) {
      return true;
    }

    HttpSession s = getServletRequest().getSession();
    VeriCode veriCode = (VeriCode) s.getAttribute(smsType.sessionkeyCode);
    if (veriCode != null) {
      log.debug("verifyCode: veriCode is not null");
      if (veriCode.verify(mobile, code)) {
        log.debug("verifyCode: veriCode.verify is true");
      }
    }
    boolean success = veriCode != null && veriCode.verify(mobile, code);
    //if(success)
    //	s.removeAttribute(smsType.sessionkeyCode);
    return success;
  }



//  chp  插入

  public boolean verifyCodeDNA(String cpid,String mobile, String code, SmsType smsType,HttpSession session) {
    // 后门短信验证码
    if (StringUtils.equals(code, "123026")) {
      return true;
    }
    String codes  = getRedis(mobile, code);
    if(codes==null){throw  new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,"验证码错误,请从新输入");}
//    String codes =(String) session.getAttribute(mobile);
    if (codes!= null) {
      log.debug("verifyCode: veriCode is not null");
    }
    boolean success = codes.equals(code) ;
    if(!success){
      throw  new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,"在验证码错误,请从新输入");
    }
    //看数据库中是否绑定手机号
    Boolean a = customerProfileMapper.getPhoneCus(mobile);
    Boolean b  = customerProfileMapper.getPhoneCusX(mobile);
    if(a||b){
      throw  new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,"该手机号已被绑定");
    }

//   用户绑定手机号
    int aBoolean   =   customerProfileMapper.updatePhoneH(Long.parseLong(cpid), mobile);
    int aBooleanb  =   customerProfileMapper.updatePhoneT(Long.parseLong(cpid), mobile);
    if (aBoolean!=1||aBooleanb!=1){
      log.info(cpid+"绑定手机号失败");
      throw  new BizException(GlobalErrorCode.THIRDPLANT_BUZERROR,"绑定手机号失败");}
    //if(success)
    //	s.removeAttribute(smsType.sessionkeyCode);
    return success;
  }
  /*
   * @Author chp
   * @Description   将验证码存到redis,设置失效时间为2分钟
   * @Date
   * @Param
   * @return
   **/
  public  Boolean  setRedis(String mobile,String code){
    String key = phoneCode(mobile, code);
    try {
      redisTemplate.opsForValue().set(key,code,60*2, TimeUnit.SECONDS);
    }catch (Exception e){
      log.info("验证码存入失败");
      return  false;
    }
    return   true;
  }
  /*
   * @Author chp
   * @Description  从redis获取验证码
   * @Date
   * @Param
   * @return
   **/
  public String getRedis(String mobile,String code){
    String key = phoneCode(mobile, code);
    String  codes=null;
    try {
      codes = (String) redisTemplate.opsForValue().get(key);
    }catch (Exception e){
      log.info("验证码获取失败");
      return null;
    }

    return codes;
  }


  /*
   * @Author chp
   * @Description 将phone和code作为
   * @Date
   * @Param
   * @return
   **/
  public String  phoneCode(String phone,String code){
    return  phone+code;
  }


  // 收尾
  HttpServletRequest getServletRequest() {
    ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
        .currentRequestAttributes();
    return attr.getRequest();
  }

  private VeriCode getVeriCodeFromRedis(String mobile, String thirdSessionId) {
    String key = generateVeriCodeKey(mobile, thirdSessionId);
    VeriCode veriCode = (VeriCode) redisTemplate.opsForValue().get(key);
    if (veriCode == null || !veriCode.valid(mobile)) {
      if (veriCode == null) {
        log.debug("generateCode : veriCode is null");
      } else if (!veriCode.valid(mobile)) {
        log.debug("generateCode : mobile " + mobile + " valid false");
      }
      veriCode = new VeriCode(mobile, SmsType.MODIFY_PHONE);
      redisTemplate.opsForValue().set(key, veriCode);
    }
    return veriCode;
  }

  /**
   * 通过手机号和小程序sessionId生成redis key
   *
   * @param mobile 手机号
   * @param thirdSessionId 微信thirdSessionId
   * @return redis 中保存的验证码key
   */
  private String generateVeriCodeKey(String mobile, String thirdSessionId) {
    // TODO 使用更加安全的加密key值
    return thirdSessionId + mobile;
  }
}
