package com.xquark.biz.verify.impl;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.RandomUtils;

class VeriCode implements java.io.Serializable {

  private static final long serialVersionUID = 1L;
  final String smsCode;
  final Date validDate;
  final Calendar canSendDate;
  final String mobile;

  VeriCode(String mobile, VerificationFacadeImpl.SmsType smsType) {
    this.mobile = mobile;
    smsCode = "" + RandomUtils.nextInt(100000, 1000000);
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.SECOND, smsType.getValidSeconds());
    validDate = cal.getTime();
    canSendDate = Calendar.getInstance();
  }

  public boolean verify(String mobile2, String code) {
    return mobile2 != null && mobile2.equals(mobile) && code != null && code.equals(smsCode);
  }

  /**
   * 每隔2秒允许发一次
   */
  public boolean canSend() {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MILLISECOND, 1);// 防止第一次请求或者服务器重启后生成的时间一样
    return canSendDate.before(cal);
  }

  /**
   * 刚sms过，控制下节奏，控制为2秒
   */
  public void sent() {
    canSendDate.add(Calendar.SECOND, 2);
  }

  /**
   * 验证码有效期1小时
   */
  public boolean valid(String mobile2) {
    if (mobile2 == null || !mobile2.equals(mobile)) {
      return false;
    }
    return validDate.after(new Date());
  }
}