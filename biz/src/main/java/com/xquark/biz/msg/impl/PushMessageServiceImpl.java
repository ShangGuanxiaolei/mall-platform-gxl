package com.xquark.biz.msg.impl;

import java.util.List;

import com.xquark.utils.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.xquark.biz.msg.PushMessageService;
import com.xquark.dal.mapper.PushMessageMapper;
import com.xquark.dal.model.PushMessage;
import com.xquark.dal.status.PushMessageStatus;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.push.BaiduPushMessage;
import com.xquark.service.push.PushService;
import com.xquark.utils.http.HttpInvokeResult;
import com.xquark.utils.http.PoolingHttpClients;

@Service("pushMessageService")
public class PushMessageServiceImpl extends BaseServiceImpl implements PushMessageService {

  @Autowired
  PushMessageMapper pushMessageMapper;

  @Autowired
  private PushService pushService;

  @Value("${xiangqu.web.site}")
  private String xqUrl;

  @Override
  public int send(PushMessage message) {
    int retVal = 0;
    boolean ret = false;
    if (StringUtils.isEmpty(message.getDesc())) {
      message.setDesc(message.getTitle());
    }
    // 用户id不为空，则是针对单个用户的单播，否则则是所有用户的群播
    if (StringUtils.isNotEmpty(message.getUserId())) {
      ret = pushService.sendUnicastMessageByUmeng(message);
    } else {
      ret = pushService.sendBroadcastMessageByUmeng(message);
    }
    if (!ret) {
      message.setStatus(PushMessageStatus.FAIL);
      message.setCount(0);
      retVal = pushMessageMapper.insert(message);
    } else {
      retVal = 1;
    }
    return retVal;
  }

  @Override
  /**
   * 通过时间程序做延迟发送
   */
  public int sendDelay(PushMessage message) {
    message.setStatus(PushMessageStatus.FAIL);
    message.setCount(0);
    return pushMessageMapper.insert(message);
  }

  @Override
  public int reSend() {
    int retVal = 0;
    boolean ret = false;
    List<PushMessage> messages = pushMessageMapper.selectFail();
    for (PushMessage message : messages) {
      BaiduPushMessage baiduMessage = new BaiduPushMessage();
      BeanUtils.copyProperties(message, baiduMessage);
      if (baiduMessage.getBaiduChannelId() != null && baiduMessage.getBaiduUserId() != null) {
        ret = pushService.sendUnicastMessageByBaidu(baiduMessage);
      } else if (baiduMessage.getBaiduTagName() != null) {
        ret = pushService.sendTagMessageByBaidu(baiduMessage);
      } else {
        ret = pushService.sendBroadcastMessageByBaidu(baiduMessage);
      }
      if (message.getCount() >= 5) { // 重试9次太多
        ret = true;
      }
      if (ret) {
        PushMessage newMessage = new PushMessage();
        newMessage.setId(message.getId());
        newMessage.setStatus(PushMessageStatus.SUCCESS);
        pushMessageMapper.update(newMessage);
        retVal++;
      } else {
        PushMessage newMessage = new PushMessage();
        newMessage.setId(message.getId());
        newMessage.setStatus(PushMessageStatus.FAIL);
        pushMessageMapper.update(newMessage);
      }
    }
    return retVal;
  }

  @Override
  public int pushToThirdServ(PushMessage msg) {
    final HttpInvokeResult result = PoolingHttpClients
        .post(xqUrl + "/openapi/push/message", JSON.toJSONBytes(msg));
    log.debug(JSON.toJSONString(msg) + result.toString());

    if (!result.isOK()) {
      log.error("Error to pushMsg:" + result, result.getException());
      return -1;
    }

    return 0;

  }
}
