package com.xquark.biz.msg;

import com.xquark.dal.model.PushMessage;


public interface PushMessageService {

  int send(PushMessage message);

  int sendDelay(PushMessage message);

  int reSend();

  int pushToThirdServ(PushMessage pushMessage);
}
