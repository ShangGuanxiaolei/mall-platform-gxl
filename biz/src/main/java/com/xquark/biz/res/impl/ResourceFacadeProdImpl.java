package com.xquark.biz.res.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.xquark.config.ApplicationConfig;

/**
 * 线上环境的资源门面
 *
 * @author jamesp
 */
@Component("resourceFacade")
@Profile({ApplicationConfig.PROFILE_NAME_PROD, ApplicationConfig.PROFILE_NAME_PROD_SERVICE, ApplicationConfig.PROFILE_NAME_PREPROD})
public class ResourceFacadeProdImpl extends ResourceFacadeNonProdImpl {

  @Override
  public String resolveUrl(String resKey) {
    return super.resolveUrl(resKey);
  }

  @Override
  public String resolveUrl2Orig(String resKey) {
    return super.resolveUrl2Orig(resKey);
  }

}
