package com.xquark.biz.res;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.RequestDataValueProcessor;

/**
 * Created by dongsongjie on 16/9/14. 对于所有支持RequestDataValueProcessor的Template Engine
 * 扩展实现模板渲染过程中对于资源的处理
 */
@Component("requestDataValueProcessor")
public class XquarkRequestDataValueProcessor implements RequestDataValueProcessor {

  protected Logger log = LoggerFactory.getLogger(getClass());

  @Value("${site.web.res.host.name}")
  String resDomain;

  private Pattern DISABLE_CSRF_TOKEN_PATTERN = Pattern.compile("(?i)^(GET|HEAD|TRACE|OPTIONS)$");

  private String DISABLE_CSRF_TOKEN_ATTR = "DISABLE_CSRF_TOKEN_ATTR";

  public String processAction(HttpServletRequest request, String action, String method) {

    if (method != null && DISABLE_CSRF_TOKEN_PATTERN.matcher(method).matches()) {
      request.setAttribute(DISABLE_CSRF_TOKEN_ATTR, Boolean.TRUE);
    } else {
      request.removeAttribute(DISABLE_CSRF_TOKEN_ATTR);
    }

    return action;
  }

  public String processFormFieldValue(HttpServletRequest request,
      String name, String value, String type) {
    return value;
  }

  public Map<String, String> getExtraHiddenFields(HttpServletRequest request) {

    if (Boolean.TRUE.equals(request.getAttribute(DISABLE_CSRF_TOKEN_ATTR))) {
      request.removeAttribute(DISABLE_CSRF_TOKEN_ATTR);
      return Collections.emptyMap();
    }

    CsrfToken token = (CsrfToken) request.getAttribute(CsrfToken.class
        .getName());

    if (token == null) {
      return Collections.emptyMap();
    }

    Map<String, String> hiddenFields = new HashMap<String, String>(1);
    hiddenFields.put(token.getParameterName(), token.getToken());
    return hiddenFields;
  }

  /**
   * <li>将所有的css,js的url生成的域名改为res.{domain.com} 的二级域名形式</li>
   */
  @Override
  public String processUrl(HttpServletRequest request, String url) {
    String ret = url;
//        if (StringUtils.contains(url, "_resources/") &&
//                (StringUtils.contains(url, ".js") || StringUtils.contains(url, ".css"))) {
//            ret = resDomain + url;
//        } else {
//
//        }
    return ret;
  }
}


















