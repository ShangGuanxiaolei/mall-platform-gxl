package com.xquark.biz.res.impl;

import com.xquark.biz.qiniu.Qiniu;
import com.xquark.biz.res.ResourceFacade;
import com.xquark.biz.vo.UpLoadFileVO;
import com.xquark.dal.model.Image;
import com.xquark.dal.type.FileBelong;
import com.xquark.service.file.ImageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 非线上环境的资源门面
 *
 * @author jamesp
 */
@Component("resourceFacade")
@Profile({"dev", "sit", "test", "innertest", "uat", "uat2", "stage"})
public class ResourceFacadeNonProdImpl implements ResourceFacade {

  @Autowired
  private ImageService imageService;

  @Autowired
  private Qiniu qiniu;

  @Value("${site.web.host.name}")
  String siteHost;

  static final String RES_PREFIX_SYS = "/_resources";

  @Override
  public String resolveUrl2Orig(String resKey) {
    if (StringUtils.isBlank(resKey)) {
      return siteHost + RES_PREFIX_SYS + "/images/404.png";
    } else if (resKey.startsWith(RES_PREFIX_SYS)) {
      return siteHost + resKey;
    } else if (resKey.startsWith(EXT_RES_PREFIX)) {
      return resKey;
    }
    return qiniu.genQiniuOrig(resKey);// 返回七牛的URL，直接去七牛服务器上取
  }

  @Override
  public String resolveUrl(String resKey) {
    if (StringUtils.isBlank(resKey)) {
      resKey = RES_PREFIX_SYS + "/images/404.png";
      return resKey;
    } else if (resKey.startsWith(EXT_RES_PREFIX)) {
      return resKey;
    } else if (resKey.startsWith(RES_PREFIX_SYS)) {
      return siteHost + resKey;
    } else if (resKey.startsWith(FILE_STORE_KEY_V2)) {
      return qiniu.genQiniuFileUrl(resKey);// 返回七牛的URL，直接去七牛服务器上取
    }
    return siteHost + RES_PREFIX_UP_FILE + (resKey.startsWith("/") ? "" : "/") + resKey;
  }

  @Override
  @Transactional
  public List<UpLoadFileVO> uploadFile(List<MultipartFile> files, FileBelong belong)
      throws Exception {
    final List<InputStream> ins = new ArrayList<InputStream>();
    for (final MultipartFile file : files) {
      ins.add(file.getInputStream());
    }
    return uploadFileStream(ins, belong);
  }


  @Override
  @Transactional
  public UpLoadFileVO uploadFileStream(InputStream in, FileBelong belong) throws Exception {
    final List<InputStream> ins = new ArrayList<InputStream>();
    ins.add(in);
    return uploadFileStream(ins, belong).get(0);
  }

  @Override
  @Transactional
  public List<UpLoadFileVO> uploadFileStream(List<InputStream> ins, FileBelong belong)
      throws Exception {
    final List<UpLoadFileVO> vos = qiniu.uploadImgStream(ins, belong);
    for (final UpLoadFileVO vo : vos) {
      vo.setUrl(resolveUrl(vo.getKey()));
    }
    saveToLocal(vos, belong);
    return vos;
  }

  protected int saveToLocal(List<UpLoadFileVO> vos, FileBelong belong) {
    Image image = null;
    for (final UpLoadFileVO vo : vos) {
      if (imageService.loadByImgKey(vo.getKey()) != null) {
        continue;
      }
      image = new Image();
      BeanUtils.copyProperties(vo, image);
      image.setId(null);
      image.setBelong(belong);
      imageService.insert(image);
    }

    return 0;
  }

  @Override
  public String genUpTokenForClient(FileBelong belong) throws Exception {
    return qiniu.genUpTokenForClient(qiniu.genBucketName(belong));
  }

  @Override
  public String getDefLogo() {
    return siteHost + "/static/images/kkkdlogo.png";
  }

}
