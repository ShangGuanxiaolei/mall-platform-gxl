package com.xquark.biz;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.xquark.biz.msg.PushMessageService;
import com.xquark.dal.mapper.PushMessageMapper;
import com.xquark.dal.model.PushMessage;
import com.xquark.dal.status.PushMessageStatus;
import com.xquark.dal.type.PushMessageDeviceType;
import com.xquark.dal.type.PushMessageType;


public class PushMessageServiceTest {

  @Value("${site.web.host.name}")
  String siteHost;

  @Autowired
  private PushMessageService pushMessageService;

  @Autowired
  private PushMessageMapper pushMessageMapper;

  @Test
  public void testSend() {
    PushMessage pushMessage = new PushMessage();
    pushMessage.setTitle("测试标题");
    pushMessage.setDesc("测试内容描述2222");
    pushMessage.setDetailUrl(siteHost + "/message/" + "dfrd43");
    pushMessage.setType(PushMessageType.MESSAGE);
    pushMessage.setDeviceType(PushMessageDeviceType.ANDROID);
    int ret = pushMessageService.send(pushMessage);
    Assert.assertTrue(ret == 1);
  }

  @Test
  public void testInsert() {
    PushMessage pushMessage = new PushMessage();
    pushMessage.setBaiduChannelId(5531318068615389789L);
    pushMessage.setBaiduUserId("1056618266872566044");
    //TODO 发送消息时,确认发送到的app名字
    pushMessage.setApp("hotshop");
    pushMessage.setTitle("测试标题22222");
    pushMessage.setDesc("测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述测试内容描述");
    pushMessage.setDetailUrl("/message/18yg4");
    pushMessage.setDeviceType(PushMessageDeviceType.IOS);
    pushMessage.setType(PushMessageType.NOTIFICATION);
    pushMessage.setStatus(PushMessageStatus.FAIL);
    pushMessage.setCount(0);
    int ret = pushMessageMapper.insert(pushMessage);
    Assert.assertTrue(ret == 1);
  }

  @Test
  public void testReSend() {
    Assert.assertTrue(pushMessageService.reSend() >= 0);
  }

}
