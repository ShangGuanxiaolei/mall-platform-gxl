package com.xquark.service.account;

import org.springframework.beans.factory.annotation.Value;

public class ThirdPartyAccount {

  @Value("${account.id.alipay}")
  public static String ALIPAY;

  @Value("${account.id.tenpay}")
  public static String TENPAY;

  @Value("${account.id.weixin}")
  public static String WEIXIN;

  @Value("${account.id.union}")
  public static String UNION;
}

