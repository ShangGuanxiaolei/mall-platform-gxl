/*     */
package com.xquark.service.account.impl;
/*     */
/*     */

import com.xquark.dal.mapper.AccountMapper;
/*     */ import com.xquark.dal.mapper.DealLogMapper;
/*     */ import com.xquark.dal.mapper.SubAccountMapper;
/*     */ import com.xquark.dal.model.Account;
/*     */ import com.xquark.dal.model.DealLog;
/*     */
/*     */ import com.xquark.dal.type.PaymentMode;
/*     */ import com.xquark.dal.vo.DealHistoryVO;
/*     */ import com.xquark.service.account.AccountService;
/*     */ import com.xquark.service.common.BaseServiceImpl;
/*     */ import com.xquark.service.error.BizException;
/*     */ import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
/*     */ import java.math.BigDecimal;

/*     */ import java.util.List;
/*     */ import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.data.domain.Pageable;
/*     */ import org.springframework.stereotype.Service;

/*     */
/*     */
@Service("accountService")
/*     */ public class AccountServiceImpl extends BaseServiceImpl
    /*     */ implements AccountService
    /*     */ {

  /*  28 */   private Logger log = LoggerFactory.getLogger(getClass());
  /*     */
  /*     */
  @Autowired
  /*     */ private AccountMapper accountMapper;
  /*     */
  /*     */
  @Autowired
  /*     */ private SubAccountMapper subAccountMapper;
  /*     */
  /*     */
  @Autowired
  /*     */ private UserService userService;
  /*     */
  /*     */
  @Autowired
  /*     */ private DealLogMapper dealLogMapper;

  /*     */
  /*  44 */
  public String loadAccountIdByPaymentMode(PaymentMode paymentMode) {
    String userId = (String) this.userService.getThirdPartyUsers().get(paymentMode);
    /*  45 */
    return loadByUserId(userId).getId();
    /*     */
  }

  /*     */
  /*     */
  public Account load(String id)
  /*     */ {
    /*  50 */
    return this.accountMapper.selectByPrimaryKey(id);
    /*     */
  }

  /*     */
  /*     */
  public Account loadByUserId(String userId)
  /*     */ {
    /*  55 */
    this.log.debug("crater account userId=[" + userId + "]");
    /*  56 */
    Account account = this.accountMapper.selectByUserId(userId);
    /*  57 */
    if (account == null) {
      /*  58 */
      account = new Account();
      /*  59 */
      account.setUserId(userId);
      /*  60 */
      account.setBalance(BigDecimal.ZERO);
      /*  61 */
      account.setFreezeBalance(BigDecimal.ZERO);
      /*  62 */
      this.accountMapper.insert(account);
      /*     */
    }
    /*  64 */
    return account;
    /*     */
  }

  /*     */
  /*     */
  public List<DealHistoryVO> listDeal(Pageable page)
  /*     */ {
    /*  69 */
    Account account = loadByUserId(getCurrentUser().getId());
    /*  70 */
    return this.accountMapper.listDealByAccountId(account.getId(), page);
    /*     */
  }

  /*     */
  /*     */
  public boolean increaseBlance(String accountId, BigDecimal fee, String dealId)
  /*     */ {
    /*  82 */
    DealLog dealLog = new DealLog();
    /*  83 */
    dealLog.setDealId(dealId);
    /*  84 */
    dealLog.setTitle("");
    /*  85 */
    dealLog.setAccountId(accountId);
    /*  86 */
    dealLog.setFee(fee);
    /*  87 */
    this.dealLogMapper.insert(dealLog);
    /*     */
    /*  89 */
    return this.accountMapper.addBalance(accountId, fee) > 0;
    /*     */
  }

  /*     */
  /*     */
  public boolean decreaseBlance(String accountId, BigDecimal fee, String dealId)
  /*     */ {
    /*  97 */
    Account account = this.accountMapper.selectByPrimaryKey(accountId);
    /*  98 */
    if ((account == null) || (account.getBalance().doubleValue() < fee.doubleValue())) {
      /*  99 */
      throw new BizException(GlobalErrorCode.UNKNOWN, "user balance is not enough, need " +
          /* 100 */         fee.doubleValue() + ", actural " + (account == null ? ""
          : Double.valueOf(account.getBalance().doubleValue())));
      /*     */
    }
    /* 102 */
    DealLog dealLog = new DealLog();
    /* 103 */
    dealLog.setDealId(dealId);
    /* 104 */
    dealLog.setTitle("");
    /* 105 */
    dealLog.setAccountId(accountId);
    /* 106 */
    dealLog.setFee(new BigDecimal(-fee.doubleValue()));
    /* 107 */
    this.dealLogMapper.insert(dealLog);
    /*     */
    /* 109 */
    return this.accountMapper.subBalance(accountId, fee) > 0;
    /*     */
  }

  /*     */
  /*     */
  public boolean freezeBalance(String accountId, BigDecimal fee) {
    /* 113 */
    Account account = this.accountMapper.selectByPrimaryKey(accountId);
    /* 114 */
    if ((account == null) || (account.getBalance().doubleValue() < fee.doubleValue())) {
      /* 115 */
      throw new BizException(GlobalErrorCode.UNKNOWN,
          "user balance is not enough, need " + fee.doubleValue() + ", actural " + account
              .getBalance().doubleValue());
      /*     */
    }
    /* 117 */
    return this.accountMapper.freezeBalance(accountId, fee) == 1;
    /*     */
  }

  /*     */
  /*     */
  public boolean unFreezeBalance(String accountId, BigDecimal fee) {
    /* 121 */
    return this.accountMapper.unFreezeBalance(accountId, fee) == 1;
    /*     */
  }

  /*     */
  /*     */
  public List<Account> listByCanWithdraw()
  /*     */ {
    /* 126 */
    return this.accountMapper.listByCanWithdraw();
    /*     */
  }

  /*     */
  /*     */
  public int insert(Account e)
  /*     */ {
    /* 131 */
    return this.accountMapper.insert(e);
    /*     */
  }
  /*     */
}
