/*    */
package com.xquark.service.account.impl;
/*    */
/*    */

import com.xquark.dal.mapper.SubAccountLogMapper;
/*    */ import com.xquark.dal.model.SubAccount;
/*    */ import com.xquark.dal.model.SubAccountLog;
/*    */ import com.xquark.dal.model.SubAccountSnapshot;
/*    */ import com.xquark.dal.vo.ProductAdmin;
/*    */ import com.xquark.dal.vo.TradeDetailsVO;
/*    */ import com.xquark.service.account.SubAccountLogService;
/*    */ import com.xquark.service.account.SubAccountService;
/*    */ import com.xquark.service.account.SubAccountSnapshotService;
/*    */ import com.xquark.service.common.BaseServiceImpl;
/*    */ import java.math.BigDecimal;
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.data.domain.Pageable;
/*    */ import org.springframework.stereotype.Service;

/*    */
/*    */
@Service("subAccountLogService")
/*    */ public class SubAccountLogServiceImpl extends BaseServiceImpl
    /*    */ implements SubAccountLogService
    /*    */ {

  /*    */
  /*    */
  @Autowired
  /*    */ SubAccountLogMapper subAccountLogMapper;
  /*    */
  /*    */
  @Autowired
  /*    */ SubAccountSnapshotService subAccountSnapshotService;
  /*    */
  /*    */
  @Autowired
  /*    */ SubAccountService subAccountService;

  /*    */
  /*    */
  public SubAccountLog load(String id)
  /*    */ {
    /* 36 */
    return this.subAccountLogMapper.selectByPrimaryKey(id);
    /*    */
  }

  /*    */
  /*    */
  public int insert(SubAccountLog subAccountLog)
  /*    */ {
    /* 41 */
    int flag = this.subAccountLogMapper.insert(subAccountLog);
    /* 42 */
    if (flag == 0) {
      return 0;
    }
    /*    */
    /* 44 */
    SubAccountSnapshot subAccountSnapshot = new SubAccountSnapshot();
    /* 45 */
    subAccountSnapshot.setSubAccountLogId(subAccountLog.getId());
    /* 46 */
    SubAccount subAccount = this.subAccountService.load(subAccountLog.getSubAccountId());
    /* 47 */
    subAccountSnapshot.setAccountId(subAccount.getAccountId());
    /* 48 */
    List<SubAccount> subAccounts = this.subAccountService
        .listByAccountId(subAccount.getAccountId());
    /*    */
    /* 50 */
    StringBuffer snapshotJson = new StringBuffer();
    /* 51 */
    for (SubAccount sAccount : subAccounts) {
      /* 52 */
      if (snapshotJson.toString().length() > 0) {
        /* 53 */
        snapshotJson.append(",");
        /*    */
      }
      /* 55 */
      snapshotJson.append("{");
      /* 56 */
      snapshotJson.append("type:" + sAccount.getAccountType());
      /* 57 */
      snapshotJson.append(",balance:" + sAccount.getBalance());
      /* 58 */
      snapshotJson.append("}");
      /*    */
    }
    /*    */
    /* 61 */
    subAccountSnapshot.setSnapshot("{" + snapshotJson + "}");
    /* 62 */
    flag = this.subAccountSnapshotService.insert(subAccountSnapshot);
    /*    */
    /* 64 */
    return flag;
    /*    */
  }

  /*    */
  /*    */
  public List<ProductAdmin> listDetail(String userId, Map<String, Object> params, Pageable page)
  /*    */ {
    /* 71 */
    return null;
    /*    */
  }

  /*    */
  /*    */
  public List<TradeDetailsVO> queryTradeDetailsByUserId(Map<String, Object> params, Pageable page)
  /*    */ {
    /* 76 */
    return this.subAccountLogMapper.queryTradeDetailsByUserId(params, page);
    /*    */
  }

  /*    */
  /*    */
  public Map<String, Object> countTradeDetailsByUserId(Map<String, Object> params)
  /*    */ {
    /* 81 */
    return this.subAccountLogMapper.countTradeDetailsByUserId(params);
    /*    */
  }

  /*    */
  /*    */
  public BigDecimal totalAmountByParams(Map<String, Object> params)
  /*    */ {
    /* 86 */
    return this.subAccountLogMapper.totalAmountByParams(params);
    /*    */
  }
  /*    */
}

