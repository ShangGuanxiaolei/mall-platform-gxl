package com.xquark.service.account;

import com.xquark.dal.model.SubAccount;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.SubAccountVO;
import com.xquark.service.BaseEntityService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface SubAccountService extends BaseEntityService<SubAccount> {

  public Boolean isBalanceEnough(String paramString, BigDecimal paramBigDecimal);

  public int addBalance(String paramString, BigDecimal paramBigDecimal);

  public SubAccount loadByPayModeAndAccountType(PaymentMode paramPaymentMode,
      AccountType paramAccountType);

  public SubAccount loadByUserIdAndAccountType(String paramString, AccountType paramAccountType);

  public SubAccount load(String paramString);

  public int insert(SubAccount paramSubAccount);

  public List<SubAccount> listByAccountId(String paramString);

  public List<SubAccountVO> listByCanWithdraw();

  public Map<AccountType, BigDecimal> selectBalanceByUser(String paramString);

  public BigDecimal selectBalanceByUserAndType(String paramString, AccountType paramAccountType);

  public SubAccount loadByAccountIdAndType(String paramString, AccountType paramAccountType);
}

