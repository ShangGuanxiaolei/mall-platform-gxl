/*    */
package com.xquark.service.account.impl;
/*    */
/*    */

import com.xquark.dal.model.SubAccount;
/*    */ import com.xquark.dal.type.AccountType;
/*    */ import com.xquark.dal.type.PaymentMode;
/*    */ import com.xquark.service.account.AccountApiService;
/*    */ import com.xquark.service.account.SubAccountService;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;

/*    */
/*    */
@Service("accountApiService")
/*    */ public class AccountApiServiceImpl
    /*    */ implements AccountApiService
    /*    */ {

  /*    */
  /*    */
  @Autowired
  /*    */ SubAccountService subAccountService;

  /*    */
  /*    */
  public SubAccount findSubAccountByPayMode(PaymentMode payMode, AccountType accountType)
  /*    */ {
    /* 20 */
    return this.subAccountService.loadByPayModeAndAccountType(payMode, accountType);
    /*    */
  }

  /*    */
  /*    */
  public SubAccount findSubAccountByUserId(String userId, AccountType accountType)
  /*    */ {
    /* 25 */
    return this.subAccountService.loadByUserIdAndAccountType(userId, accountType);
    /*    */
  }

  /*    */
  /*    */
  public SubAccount findSubAccountByAccountId(String accountId, AccountType accountType)
  /*    */ {
    /* 30 */
    return this.subAccountService.loadByAccountIdAndType(accountId, accountType);
    /*    */
  }

  /*    */
  /*    */
  public SubAccount findSubAccountBySubAccountId(String subAccountId)
  /*    */ {
    /* 35 */
    return this.subAccountService.load(subAccountId);
    /*    */
  }
  /*    */
}

