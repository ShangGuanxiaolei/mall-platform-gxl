package com.xquark.service.account;

import com.xquark.dal.model.Account;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.DealHistoryVO;
import com.xquark.service.BaseEntityService;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.domain.Pageable;

public abstract interface AccountService extends BaseEntityService<Account> {

  public abstract String loadAccountIdByPaymentMode(PaymentMode paramPaymentMode);

  public abstract Account load(String paramString);

  public abstract List<DealHistoryVO> listDeal(Pageable paramPageable);

  public abstract boolean increaseBlance(String paramString1, BigDecimal paramBigDecimal,
      String paramString2);

  public abstract boolean decreaseBlance(String paramString1, BigDecimal paramBigDecimal,
      String paramString2);

  public abstract boolean freezeBalance(String paramString, BigDecimal paramBigDecimal);

  public abstract boolean unFreezeBalance(String paramString, BigDecimal paramBigDecimal);

  public abstract Account loadByUserId(String paramString);

  public abstract List<Account> listByCanWithdraw();
}

