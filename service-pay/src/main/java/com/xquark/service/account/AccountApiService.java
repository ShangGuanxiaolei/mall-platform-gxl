package com.xquark.service.account;

import com.xquark.dal.model.SubAccount;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PaymentMode;

public abstract interface AccountApiService {

  public abstract SubAccount findSubAccountByPayMode(PaymentMode paramPaymentMode,
      AccountType paramAccountType);

  public abstract SubAccount findSubAccountByUserId(String paramString,
      AccountType paramAccountType);

  public abstract SubAccount findSubAccountByAccountId(String paramString,
      AccountType paramAccountType);

  public abstract SubAccount findSubAccountBySubAccountId(String paramString);
}

