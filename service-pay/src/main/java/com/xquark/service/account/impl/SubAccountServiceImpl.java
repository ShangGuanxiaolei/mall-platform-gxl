/*     */
package com.xquark.service.account.impl;
/*     */
/*     */

import com.xquark.dal.mapper.SubAccountMapper;
/*     */ import com.xquark.dal.model.Account;
/*     */ import com.xquark.dal.model.SubAccount;
/*     */ import com.xquark.dal.type.AccountType;
/*     */ import com.xquark.dal.type.PaymentMode;
/*     */ import com.xquark.dal.vo.SubAccountVO;
/*     */ import com.xquark.service.account.AccountService;
/*     */ import com.xquark.service.account.SubAccountService;
/*     */ import com.xquark.service.common.BaseServiceImpl;
/*     */ import java.math.BigDecimal;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.stereotype.Service;

/*     */
/*     */
@Service("subAccountService")
/*     */ public class SubAccountServiceImpl extends BaseServiceImpl
    /*     */ implements SubAccountService
    /*     */ {

  /*     */
  /*     */
  @Autowired
  /*     */ SubAccountMapper subAccountMapper;
  /*     */
  /*     */
  @Autowired
  /*     */ AccountService accountService;

  /*     */
  /*     */
  public SubAccount loadByPayModeAndAccountType(PaymentMode payMode, AccountType accountType)
  /*     */ {
    /*  32 */
    String accountId = this.accountService.loadAccountIdByPaymentMode(payMode);
    /*  33 */
    return loadByAccountIdAndType(accountId, accountType);
    /*     */
  }

  /*     */
  /*     */
  public SubAccount loadByUserIdAndAccountType(String userId, AccountType accountType)
  /*     */ {
    /*  38 */
    Account account = this.accountService.loadByUserId(userId);
    /*  39 */
    return loadByAccountIdAndType(account.getId(), accountType);
    /*     */
  }

  /*     */
  /*     */
  public SubAccount loadByAccountIdAndType(String accountId, AccountType accountType)
  /*     */ {
    /*  44 */
    SubAccount subAccount = this.subAccountMapper.selectByAccountIdAndType(accountId, accountType);
    /*  45 */
    if (subAccount == null) {
      /*  46 */
      subAccount = new SubAccount();
      /*  47 */
      subAccount.setAccountId(accountId);
      /*  48 */
      subAccount.setAccountType(accountType);
      /*  49 */
      subAccount.setBalance(BigDecimal.ZERO);
      /*  50 */
      this.subAccountMapper.insert(subAccount);
      /*     */
    }
    /*  52 */
    return subAccount;
    /*     */
  }

  /*     */
  /*     */
  public SubAccount load(String id)
  /*     */ {
    /*  57 */
    return this.subAccountMapper.selectByPrimaryKey(id);
    /*     */
  }

  /*     */
  /*     */
  public int insert(SubAccount subAccount)
  /*     */ {
    /*  62 */
    return this.subAccountMapper.insert(subAccount);
    /*     */
  }

  /*     */
  /*     */
  public Boolean isBalanceEnough(String subAccountId, BigDecimal amount)
  /*     */ {
    /*  67 */
    SubAccount subAccount = load(subAccountId);
    /*  68 */
    amount = amount.setScale(2, 1);
    /*  69 */
    if (subAccount.getBalance().compareTo(amount) > -1) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
    /*     */
  }

  /*     */
  /*     */
  public int addBalance(String subAccountId, BigDecimal amount)
  /*     */ {
    /*  74 */
    return this.subAccountMapper.addBalance(subAccountId, amount);
    /*     */
  }

  /*     */
  /*     */
  public List<SubAccount> listByAccountId(String accountId)
  /*     */ {
    /*  79 */
    return this.subAccountMapper.listByAccountId(accountId);
    /*     */
  }

  /*     */
  /*     */
  public List<SubAccountVO> listByCanWithdraw()
  /*     */ {
    /*  84 */
    return this.subAccountMapper.listByCanWithdraw(AccountType.AVAILABLE);
    /*     */
  }

  /*     */
  /*     */
  public Map<AccountType, BigDecimal> selectBalanceByUser(String userId)
  /*     */ {
    /*  89 */
    Map map = new HashMap();
    /*  90 */
    List<SubAccount> list = this.subAccountMapper.selectBalanceByUser(userId);
    /*  91 */
    if ((list != null) && (list.size() > 0)) {
      /*  92 */
      for (SubAccount account : list) {
        /*  93 */
        map.put(account.getAccountType(), account.getBalance());
        /*     */
      }
      /*     */
    }
    /*  96 */
    return map;
    /*     */
  }

  /*     */
  /*     */
  public BigDecimal selectBalanceByUserAndType(String userId, AccountType accountType)
  /*     */ {
    /* 102 */
    return this.subAccountMapper.selectBalanceByUserAndType(userId, accountType);
    /*     */
  }
  /*     */
}

