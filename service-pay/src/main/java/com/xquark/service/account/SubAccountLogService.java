package com.xquark.service.account;

import com.xquark.dal.model.SubAccountLog;
import com.xquark.dal.vo.ProductAdmin;
import com.xquark.dal.vo.TradeDetailsVO;
import com.xquark.service.BaseEntityService;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Pageable;

public interface SubAccountLogService extends BaseEntityService<SubAccountLog> {

  public SubAccountLog load(String paramString);

  public int insert(SubAccountLog paramSubAccountLog);

  public List<ProductAdmin> listDetail(String paramString, Map<String, Object> paramMap,
      Pageable paramPageable);

  public Map<String, Object> countTradeDetailsByUserId(Map<String, Object> paramMap);

  public List<TradeDetailsVO> queryTradeDetailsByUserId(Map<String, Object> paramMap,
      Pageable paramPageable);

  public BigDecimal totalAmountByParams(Map<String, Object> paramMap);
}

