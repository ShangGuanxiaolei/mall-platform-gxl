/*    */
package com.xquark.service.account.impl;
/*    */
/*    */

import com.xquark.dal.mapper.SubAccountSnapshotMapper;
/*    */ import com.xquark.dal.model.SubAccountSnapshot;
/*    */ import com.xquark.service.account.SubAccountSnapshotService;
/*    */ import com.xquark.service.common.BaseServiceImpl;
/*    */ import java.util.List;
/*    */ import org.springframework.beans.factory.annotation.Autowired;
/*    */ import org.springframework.stereotype.Service;

/*    */
/*    */
@Service("subAccountSnapshotService")
/*    */ public class SubAccountSnapshotServiceImpl extends BaseServiceImpl
    /*    */ implements SubAccountSnapshotService
    /*    */ {

  /*    */
  /*    */
  @Autowired
  /*    */ SubAccountSnapshotMapper subAccountSnapshotMapper;

  /*    */
  /*    */
  public int insert(SubAccountSnapshot snapshot)
  /*    */ {
    /* 22 */
    return this.subAccountSnapshotMapper.insert(snapshot);
    /*    */
  }

  /*    */
  /*    */
  public List<SubAccountSnapshot> listByAccountId(String accountId) {
    /* 26 */
    return this.subAccountSnapshotMapper.selectByAccountId(accountId);
    /*    */
  }
  /*    */
}

