package com.xquark.service.account;

import com.xquark.dal.model.SubAccountSnapshot;
import com.xquark.service.BaseService;

public interface SubAccountSnapshotService extends BaseService {

  public int insert(SubAccountSnapshot paramSubAccountSnapshot);
}

