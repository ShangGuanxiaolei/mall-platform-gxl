package com.xquark.service.pay;

import com.xquark.dal.model.PayRequest;
import com.xquark.service.BaseService;
import java.math.BigDecimal;
import java.util.List;

public interface PayRequestApiService extends BaseService {

  public String generatePayNo();

  public Boolean payRequest(PayRequest paramPayRequest);

  public List<PayRequest> loadDanbaoRequest(PayRequest paramPayRequest);

  public List<PayRequest> loadConsumeReqeustByPayNo(String paramString);

  public String loadPayNoByBizId(String paramString);

  public List<PayRequest> listByPayNo(String paramString);

  public List<PayRequest> loadRefundRequest(PayRequest paramPayRequest);

  public BigDecimal findRefundedAmount(PayRequest paramPayRequest);

  public BigDecimal findMaxRefundAmount(PayRequest paramPayRequest);

  public PayRequest loadByRequestId(String paramString);
}
