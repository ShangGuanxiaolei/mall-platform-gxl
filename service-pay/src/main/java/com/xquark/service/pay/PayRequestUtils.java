package com.xquark.service.pay;

import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PayRequestPayType;

public class PayRequestUtils {

  /**
   * 根据来源账号类型获得payType
   */
  public static PayRequestPayType findPayTypeByFromAccountType(AccountType accountType) {
    PayRequestPayType payType = null;
    switch (accountType) {
      case AVAILABLE:
        payType = PayRequestPayType.AVAILABLE2CONSUME;
        break;
      case HONGBAO:
        payType = PayRequestPayType.HONGBAO2CONSUME;
        break;
      default:
        break;
    }
    return payType;
  }

  /**
   * 根据request的payType获得来源账号(退款用到)
   */
  public static AccountType findFromAccountTypeByPayType(PayRequestPayType payType) {
    AccountType accountType = null;
    switch (payType) {
      case AVAILABLE2CONSUME:
        accountType = AccountType.AVAILABLE;
        break;
      case HONGBAO2CONSUME:
        accountType = AccountType.HONGBAO;
        break;
      default:
        break;
    }
    return accountType;
  }
}


