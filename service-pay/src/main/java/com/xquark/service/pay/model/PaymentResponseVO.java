package com.xquark.service.pay.model;

import com.xquark.dal.status.PaymentStatus;
import com.xquark.dal.type.PaymentMode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class PaymentResponseVO implements Serializable {

  private static final long serialVersionUID = 1L;

  String billNo; //系统账单号

  String billNoPay; //支付方账单号

  PaymentStatus billStatus; //账单支付状态

  String buyerEmail; //第三方支付账号名

  String buyerId; //第三方支付账号id

  String totalFee; //交易金额

  PaymentMode paymentMode; //支付方式 ALIPAY,UMPAY

  String detail; //整个通知url结果

  String respDate; //返回给第三方支付的date，现用于U付

  String payAgreementId; //协议ID

  String paymentMerchantId; //

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getRespDate() {
    return respDate;
  }

  public void setRespDate(String respDate) {
    this.respDate = respDate;
  }

  public String getTotalFee() {
    return totalFee;
  }

  public void setTotalFee(String totalFee) {
    this.totalFee = totalFee;
  }

  public PaymentMode getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(PaymentMode paymentMode) {
    this.paymentMode = paymentMode;
  }

  public String getBuyerEmail() {
    return buyerEmail;
  }

  public void setBuyerEmail(String buyerEmail) {
    this.buyerEmail = buyerEmail;
  }

  public String getBuyerId() {
    return buyerId;
  }

  public void setBuyerId(String buyerId) {
    this.buyerId = buyerId;
  }

  /**
   * 系统内部支付方账单号
   */
  public String getBillNo() {
    return billNo;
  }

  /**
   * 系统内部支付方账单号
   */
  public void setBillNo(String billNo) {
    this.billNo = billNo;
  }

  /**
   * @return 支付方的账单号
   */
  public String getBillNoPay() {
    return billNoPay;
  }

  /**
   * @param billNoPay 支付方的账单号
   */
  public void setBillNoPay(String billNoPay) {
    this.billNoPay = billNoPay;
  }

  /**
   * 账单支付状态
   */
  public PaymentStatus getBillStatus() {
    return billStatus;
  }

  /**
   * 账单支付状态
   */
  public void setBillStatus(PaymentStatus billStatus) {
    this.billStatus = billStatus;
  }

  public String getPayAgreementId() {
    return payAgreementId;
  }

  public void setPayAgreementId(String payAgreementId) {
    this.payAgreementId = payAgreementId;
  }

  public String getPaymentMerchantId() {
    return paymentMerchantId;
  }

  public void setPaymentMerchantId(String paymentMerchantId) {
    this.paymentMerchantId = paymentMerchantId;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }

}
