/*     */
package com.xquark.service.pay.impl;
/*     */
/*     */

import com.xquark.dal.model.PayRequest;
/*     */ import com.xquark.dal.status.PayRequestStatus;
/*     */ import com.xquark.dal.type.PayRequestPayType;
/*     */ import com.xquark.service.account.AccountService;
/*     */ import com.xquark.service.common.BaseServiceImpl;
/*     */ import com.xquark.service.error.BizException;
/*     */ import com.xquark.service.error.GlobalErrorCode;
/*     */ import com.xquark.service.pay.PayRequestApiService;
/*     */ import com.xquark.service.pay.PayRequestService;
/*     */ import com.xquark.utils.UniqueNoUtils;
/*     */ import com.xquark.utils.UniqueNoUtils.UniqueNoType;
/*     */ import java.math.BigDecimal;
/*     */ import java.util.List;
/*     */ import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.springframework.transaction.annotation.Transactional;

/*     */
/*     */
@Service("payRequestApiService")
/*     */ public class PayRequestApiServiceImpl extends BaseServiceImpl
    /*     */ implements PayRequestApiService
    /*     */ {

  /*  27 */   private Logger log = LoggerFactory.getLogger(getClass());
  /*     */
  /*     */
  @Autowired
  /*     */ private PayRequestService payRequestService;
  /*     */
  /*     */
  @Autowired
  /*     */ private AccountService accountService;

  /*     */
  /*  37 */
  public String generatePayNo() {
    return UniqueNoUtils.next(UniqueNoUtils.UniqueNoType.P);
  }

  /*     */
  /*     */
  /*     */
  @Transactional
  public Boolean payRequest(PayRequest request) {
    this.log.info("payRequest start [ " + request + "]");
    Boolean bFlag;
    request.setStatus(PayRequestStatus.SUBMITTED);
    bFlag = this.payRequestService.onCreate(request);
    if (!bFlag) {
      return Boolean.FALSE;
    }
    bFlag = this.payRequestService.onPay(request);
    if (!bFlag) {
      this.log.warn("request onPay failed request[" + request + "] ");
      throw new BizException(GlobalErrorCode.UNKNOWN, " 支付请求支付时失败");
    }
    this.log.info("payRequest paid " + request);
    bFlag = this.payRequestService.onSuccess(request);
    if (!bFlag) {
      return Boolean.FALSE;
    }
    this.log.info("payRequest successed" + request);
    return Boolean.TRUE;
  }

  /*     */
  /*     */
  public BigDecimal findRefundedAmount(PayRequest request)
  /*     */ {
    /*  79 */
    BigDecimal refunded = BigDecimal.ZERO;
    /*     */
    /*  81 */
    List<PayRequest> refundedRequests = loadRefundRequest(request);
    /*  82 */
    for (PayRequest refundedRequest : refundedRequests) {
      /*  83 */
      if (refundedRequest.getPayType() == PayRequestPayType.DANBAO2CONSUME) {
        /*  84 */
        refunded = refunded.add(refundedRequest.getAmount());
        /*     */
      }
      /*     */
    }
    /*  87 */
    refunded = refunded.setScale(2, 1);
    /*     */
    /*  89 */
    return refunded;
    /*     */
  }

  /*     */
  /*     */
  public BigDecimal findMaxRefundAmount(PayRequest request)
  /*     */ {
    /*  95 */
    BigDecimal amount = BigDecimal.ZERO;
    /*     */
    /*  97 */
    List<PayRequest> danbaos = loadDanbaoRequest(request);
    /*  98 */
    for (PayRequest danbao : danbaos) {
      /*  99 */
      amount = danbao.getAmount();
      /*     */
    }
    /* 101 */
    amount = amount.setScale(2, 1);
    /*     */
    /* 104 */
    return amount.subtract(findRefundedAmount(request));
    /*     */
  }

  /*     */
  /*     */
  public List<PayRequest> listByPayNo(String payNo)
  /*     */ {
    /* 109 */
    return this.payRequestService.listByPayNo(payNo);
    /*     */
  }

  /*     */
  /*     */
  public List<PayRequest> loadDanbaoRequest(PayRequest request) {
    return this.payRequestService.loadDanbaoRequest(request);
  }

  /*     */
  /*     */
  public List<PayRequest> loadConsumeReqeustByPayNo(String payNo)
  /*     */ {
    /* 119 */
    return this.payRequestService.loadConsumeReqeustByPayNo(payNo);
    /*     */
  }

  /*     */
  /*     */
  public List<PayRequest> loadRefundRequest(PayRequest request)
  /*     */ {
    /* 124 */
    return this.payRequestService.loadRefundRequest(request);
    /*     */
  }

  /*     */
  /*     */
  public PayRequest loadByRequestId(String id)
  /*     */ {
    /* 129 */
    return this.payRequestService.load(id);
    /*     */
  }

  /*     */
  /*     */
  public String loadPayNoByBizId(String bizId)
  /*     */ {
    /* 135 */
    return null;
    /*     */
  }
  /*     */
}

