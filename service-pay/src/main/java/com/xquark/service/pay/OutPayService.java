package com.xquark.service.pay;

import com.xquark.dal.model.OutPay;
import com.xquark.service.BaseEntityService;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public abstract interface OutPayService extends BaseEntityService<OutPay> {

  public abstract Boolean outPayRequest(OutPay paramOutPay)
      throws Exception;

  public abstract OutPay outPayCallback(OutPay paramOutPay);

  public abstract OutPay load(String paramString);

  public abstract int insert(OutPay paramOutPay);

  public abstract int insertSelective(OutPay paramOutPay);

  public abstract List<OutPay> listByUserId(String paramString);

  public abstract List<OutPay> listByBillNo(String paramString);

  public abstract List<OutPay> listByTradeNo(String paramString);

  public abstract List<OutPay> listByAccountName(String paramString);

  public abstract void updateByPrimaryKey(OutPay paramOutPay);

  public abstract int finishOutPay(OutPay paramOutPay);

  public abstract OutPay findByOrderNo4Refund(String paramString1, String paramString2);

  public abstract OutPay findOutPayByTradeNo(String paramString1, String paramString2);

  public abstract OutPay findOutPayBybillNo(String paramString1, String paramString2);

  List<OutPay> selectRecords(Map<String, Object> params, Pageable pageable);

  Long countRecords(Map<String, Object> params);

    OutPay queryOutPayBybillNo(String bizNo);

  void savePayResult(PaymentResponseVO payVO);

}

/* Location:           /Users/liangfan/work/trunk/vdlm/SharedLibs/com/vdlm/vdlm-service-pay/0.1.0-SNAPSHOT/vdlm-service-pay-0.1.0-SNAPSHOT.jar
 * Qualified Name:     com.vdlm.service.pay.OutPayService
 * JD-Core Version:    0.6.2
 */