package com.xquark.service.pay.impl;

import com.xquark.dal.mapper.PaymentMapper;
import com.xquark.dal.type.DeviceType;
import com.xquark.dal.vo.PaymentVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pay.PaymentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("paymentService")
public class PaymentServiceImpl extends BaseServiceImpl
    implements PaymentService {

  @Autowired
  private PaymentMapper paymentMapper;

  public List<PaymentVO> list(DeviceType deviceType) {
    return this.paymentMapper.listByDeviceType(deviceType, new String[0]);
  }
}

