package com.xquark.service.pay;

import com.xquark.dal.model.PayRequest;
import com.xquark.service.BaseService;
import java.util.List;

public interface PayRequestService extends BaseService {

  public Boolean onCreate(PayRequest paramPayRequest);

  public Boolean onCancel(PayRequest paramPayRequest);

  public Boolean onPay(PayRequest paramPayRequest);

  public Boolean onSuccess(PayRequest paramPayRequest);

  public Boolean onFailed(PayRequest paramPayRequest);

  public List<PayRequest> listByPayNo(String paramString);

  public boolean update(PayRequest paramPayRequest);

  public PayRequest load(String paramString);

  public String loadPayNoByBizId(String paramString);

  public PayRequest queryPaidPayInfoByBizId(String paramString);

  public PayRequest queryRefundByOutPayId(String paramString);

  public List<PayRequest> loadDanbaoRequest(PayRequest paramPayRequest);

  public List<PayRequest> loadConsumeReqeustByPayNo(String paramString);

  public List<PayRequest> loadRefundRequest(PayRequest paramPayRequest);
}
