/*     */
package com.xquark.service.pay.impl;
/*     */
/*     */

import com.xquark.dal.mapper.AccountMapper;
import com.xquark.dal.mapper.PayRequestMapper;
import com.xquark.dal.model.PayRequest;
import com.xquark.dal.status.PayRequestStatus;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.account.SubAccountLogService;
import com.xquark.service.account.SubAccountService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pay.PayRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */

/*     */
/*     */
@Service("payRequestService")
/*     */ public class PayRequestServiceImpl extends BaseServiceImpl
    /*     */ implements PayRequestService
    /*     */ {

  /*  31 */   private Logger log = LoggerFactory.getLogger(getClass());
  /*     */
  /*     */
  @Autowired
  /*     */ private PayRequestMapper payRequestMapper;
  /*     */
  /*     */
  @Autowired
  /*     */ private SubAccountService subAccountService;
  /*     */
  /*     */
  @Autowired
  /*     */ private SubAccountLogService subAccountLogService;
  /*     */
  /*     */
  @Autowired
  /*     */ PayRequestApiService payRequestApiService;
  /*     */
  /*     */
  @Autowired
  /*     */ AccountApiService accountApiService;
  /*     */
  /*     */
  @Autowired
  /*     */ AccountMapper accountMapper;

  public Boolean onCreate(PayRequest request) {
    this.log.info("create payRequest " + request);
    request.setStatus(PayRequestStatus.SUBMITTED);
    request.setAmount(request.getAmount().setScale(2, 1));
    if (this.payRequestMapper.insert(request) == 1) {
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

  public Boolean onPay(PayRequest request) {
    if (this.payRequestMapper.updateByPay(request) == 1) {
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

  public Boolean onSuccess(PayRequest request) {
    if (this.payRequestMapper.updateBySuccess(request) == 1) {
      return Boolean.TRUE;
    }
    return Boolean.FALSE;
  }

  /*     */
  /*     */
  public List<PayRequest> listByPayNo(String payNo)
  /*     */ {
    /* 133 */
    return this.payRequestMapper.listByPayNo(payNo);
    /*     */
  }

  /*     */
  /*     */
  public PayRequest load(String id)
  /*     */ {
    /* 138 */
    return this.payRequestMapper.selectByPrimaryKey(id);
    /*     */
  }

  /*     */
  /*     */
  public Boolean onCancel(PayRequest request)
  /*     */ {
    /* 144 */
    if (this.payRequestMapper.updateByOnCancel(request) == 1) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
    /*     */
  }

  /*     */
  /*     */
  public boolean update(PayRequest request)
  /*     */ {
    /* 149 */
    return this.payRequestMapper.updateByPrimaryKeySelective(request) == 1;
    /*     */
  }

  /*     */
  /*     */
  public String loadPayNoByBizId(String bizId)
  /*     */ {
    /* 154 */
    return this.payRequestMapper.findPayNoByBizId(bizId);
    /*     */
  }

  /*     */
  /*     */
  public Boolean onFailed(PayRequest request)
  /*     */ {
    /* 159 */
    if (this.payRequestMapper.updateByFailed(request) == 1) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
    /*     */
  }

  /*     */
  /*     */
  public PayRequest queryPaidPayInfoByBizId(String bizId)
  /*     */ {
    /* 164 */
    return this.payRequestMapper.queryPaidPayInfoByBizId(bizId);
    /*     */
  }

  /*     */
  /*     */
  public PayRequest queryRefundByOutPayId(String tradeNo)
  /*     */ {
    /* 169 */
    return this.payRequestMapper.queryRefundByOutPayId(tradeNo);
    /*     */
  }

  /*     */
  /*     */
  public List<PayRequest> loadDanbaoRequest(PayRequest request) {
    return this.payRequestMapper.loadDanbaoRequest(request);
  }

  /*     */
  /*     */
  public List<PayRequest> loadConsumeReqeustByPayNo(String payNo)
  /*     */ {
    /* 179 */
    return this.payRequestMapper.loadConsumeReqeustByPayNo(payNo);
    /*     */
  }

  /*     */
  /*     */
  public List<PayRequest> loadRefundRequest(PayRequest request)
  /*     */ {
    /* 184 */
    return this.payRequestMapper.loadRefundRequest(request);
    /*     */
  }
  /*     */
}

