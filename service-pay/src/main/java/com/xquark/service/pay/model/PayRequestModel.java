/*    */
package com.xquark.service.pay.model;
/*    */
/*    */

import com.xquark.dal.type.AccountType;
/*    */ import com.xquark.dal.type.PayRequestPayType;
/*    */ import com.xquark.dal.type.PaymentMode;
/*    */ import java.math.BigDecimal;
/*    */ import java.util.List;

/*    */
/*    */ public class PayRequestModel
    /*    */ {

  /*    */   private PaymentMode payMode;
  /*    */   private PayRequestPayType payType;
  /*    */   private List<AccountTransform> accounts;

  /*    */
  /*    */
  public PayRequestModel(PaymentMode payMode, PayRequestPayType payType,
      List<AccountTransform> accounts)
  /*    */ {
    /* 29 */
    this.payMode = payMode;
    /* 30 */
    this.payType = payType;
    /* 31 */
    this.accounts = accounts;
    /*    */
  }

  /*    */
  /*    */
  public PaymentMode getPayMode() {
    /* 35 */
    return this.payMode;
    /*    */
  }

  /*    */
  /*    */
  public void setPayMode(PaymentMode payMode) {
    /* 39 */
    this.payMode = payMode;
    /*    */
  }

  /*    */
  /*    */
  public PayRequestPayType getPayType() {
    /* 43 */
    return this.payType;
    /*    */
  }

  /*    */
  /*    */
  public void setPayType(PayRequestPayType payType) {
    /* 47 */
    this.payType = payType;
    /*    */
  }

  /*    */
  /*    */
  public List<AccountTransform> getAccounts() {
    /* 51 */
    return this.accounts;
    /*    */
  }

  /*    */
  /*    */
  public void setAccounts(List<AccountTransform> accounts) {
    /* 55 */
    this.accounts = accounts;
    /*    */
  }

  /*    */
  /*    */   public class AccountTransform
      /*    */ {

    /*    */ AccountType accountType;
    /*    */ String fromAccount;
    /*    */ String toAccount;
    /*    */ BigDecimal amount;

    /*    */
    /*    */
    public AccountTransform()
    /*    */ {
      /*    */
    }
    /*    */
  }
  /*    */
}

/* Location:           /Users/liangfan/work/trunk/vdlm/SharedLibs/com/vdlm/vdlm-service-pay/0.1.0-SNAPSHOT/vdlm-service-pay-0.1.0-SNAPSHOT.jar
 * Qualified Name:     com.vdlm.service.pay.model.PayRequestModel
 * JD-Core Version:    0.6.2
 */