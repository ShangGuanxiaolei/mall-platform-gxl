/*    */
package com.xquark.service.pay.impl;
/*    */
/*    */

import com.xquark.dal.mapper.OutpayMapper;
import com.xquark.dal.model.OutPay;
import com.xquark.dal.type.PayRequestBizType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.pay.OutPayService;
import com.xquark.service.pay.model.PaymentResponseVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
@Service("outPayService")
/*    */ public class OutPayServiceImpl extends BaseServiceImpl
    /*    */ implements OutPayService
    /*    */ {

  /*    */
  /*    */
  @Autowired
  /*    */ private OutpayMapper outpayMapper;

  /*    */
  /*    */
  public OutPay load(String id)
  /*    */ {
    /* 21 */
    return this.outpayMapper.selectByPrimaryKey(id);
    /*    */
  }

  /*    */
  /*    */
  public int insert(OutPay snapshot)
  /*    */ {
    /* 26 */
    return this.outpayMapper.insert(snapshot);
    /*    */
  }

  /*    */
  /*    */
  public Boolean outPayRequest(OutPay request)
  /*    */     throws Exception
  /*    */ {
    /* 32 */
    return Boolean.FALSE;
    /*    */
  }

  /*    */
  /*    */
  public OutPay outPayCallback(OutPay request)
  /*    */ {
    /* 38 */
    return null;
    /*    */
  }

  /*    */
  /*    */
  public int insertSelective(OutPay snapshot)
  /*    */ {
    /* 43 */
    return this.outpayMapper.insertSelective(snapshot);
    /*    */
  }

  /*    */
  /*    */
  public List<OutPay> listByUserId(String userId)
  /*    */ {
    /* 48 */
    return this.outpayMapper.selectByUserId(userId);
    /*    */
  }

  /*    */
  /*    */
  public List<OutPay> listByBillNo(String billNo)
  /*    */ {
    /* 53 */
    return this.outpayMapper.selectByBillNO(billNo);
    /*    */
  }

  /*    */
  /*    */
  public List<OutPay> listByTradeNo(String tradeNo)
  /*    */ {
    /* 58 */
    return this.outpayMapper.selectByTradeNo(tradeNo);
    /*    */
  }

  /*    */
  /*    */
  public void updateByPrimaryKey(OutPay outpay)
  /*    */ {
    /* 63 */
    this.outpayMapper.updateByPrimaryKeySelective(outpay);
    /*    */
  }

  /*    */
  /*    */
  public List<OutPay> listByAccountName(String name)
  /*    */ {
    /* 68 */
    return this.outpayMapper.selectByAccountName(name);
    /*    */
  }

  /*    */
  /*    */
  public int finishOutPay(OutPay record)
  /*    */ {
    /* 73 */
    return this.outpayMapper.finishOutPay(record);
    /*    */
  }

  /*    */
  /*    */
  public OutPay findByOrderNo4Refund(String payNo, String payType)
  /*    */ {
    /* 78 */
    return this.outpayMapper.findByOrderNo4Refund(payNo, payType);
    /*    */
  }

  /*    */
  /*    */
  public OutPay findOutPayByTradeNo(String tradeNo, String payType)
  /*    */ {
    /* 83 */
    return this.outpayMapper.findOutPayByTradeNo(tradeNo, payType);
    /*    */
  }

  /*    */
  /*    */
  public OutPay findOutPayBybillNo(String billNo, String payType)
  /*    */ {
    /* 88 */
    return this.outpayMapper.findOutPayByBillNo(billNo, payType);
    /*    */
  }

  public List<OutPay> selectRecords(Map<String, Object> params, Pageable pageable) {
    return outpayMapper.selectRecords(params, pageable);
  }


  public Long countRecords(Map<String, Object> params) {
    return outpayMapper.countRecords(params);
  }

  @Override
  public OutPay queryOutPayBybillNo(String bizNo) {
    return outpayMapper.queryOutPayBybillNo(bizNo);
  }

  @Override
  public void savePayResult(PaymentResponseVO payVO) {
    OutPay aOp = new OutPay();
    aOp.setOutId(payVO.getPaymentMode().toString()); // WEIXIN微信支付  UNION银联支付 TENPAY财付通支付 ALIPAY支付宝
    aOp.setpOutpayId("0"); // 父亲支付ID，用于批量打款和批量退款中的子支付记录
    aOp.setRequestId("0"); //
    aOp.setForOutpayId("0"); // 仅用于原路退回的操作，关联原始支付的OutPayId
    aOp.setOutAccountId(payVO.getBuyerId()); // 三方支付帐号编号
    aOp.setOutAccountName(payVO.getBuyerEmail()); // 支付帐号名称
    aOp.setOutStatus("SUCCESS"); // 支付请求的状态，这个状态不是必须的，对于支付宝就是一个字符串描述
    aOp.setOutstatusex("");  // 外部交易扩展状态
    aOp.setBillNo(payVO.getBillNo()); // 商户订单号
    aOp.setTradeNo(payVO.getBillNoPay()); // 第三方交易号
    aOp.setPaymentMerchantId(StringUtils.defaultIfBlank(payVO.getPaymentMerchantId(), "0"));
    //aOp.setStatus(payVO.getBillStatus()); 			//系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setStatus(payVO.getBillStatus()); // 系统内部的支付状态，SUBMITTED提交  FAILED支付失败  SUCCESS支付完成 CANCEL取消
    aOp.setOutpayType(PayRequestBizType.ORDER.toString());
    aOp.setOutpayTypeEx(
        "USER_PAY"); // 支付类型，USER_PAY用户即时到账,ADMIN_REFUND运营退款，ADMIN_WITHDRAW运营打款，信用还款CREDIT_REPAYMENT，CREDIT_REFUND_AFTER_REPAYMENT还款后的退款
    BigDecimal amount =
        payVO.getTotalFee() != null ? NumberUtils.createBigDecimal(payVO.getTotalFee()) : null;
    aOp.setAmount(amount);
    byte[] detail = payVO.getDetail() != null ? payVO.getDetail().getBytes() : null;
    aOp.setDetail(detail);
    aOp.setUpdatedAt(new Date());

    List<OutPay> aOutPays = this.listByTradeNo(payVO.getBillNoPay());
    OutPay outPay = null;
    for (OutPay pay : aOutPays) {
      if (payVO.getPaymentMode().toString().equals(pay.getOutId())) {
        outPay = pay;
        aOp.setId(pay.getId());
        this.updateByPrimaryKey(aOp);
        break;
      }
    }
    if (outPay == null) {
      aOp.setCreatedAt(new Date());
      this.insertSelective(aOp);
    }
  }


  /*    */
}

