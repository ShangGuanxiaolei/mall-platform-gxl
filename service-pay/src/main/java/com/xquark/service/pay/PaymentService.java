package com.xquark.service.pay;

import com.xquark.dal.type.DeviceType;
import com.xquark.dal.vo.PaymentVO;
import java.util.List;

public abstract interface PaymentService {

  public abstract List<PaymentVO> list(DeviceType paramDeviceType);
}