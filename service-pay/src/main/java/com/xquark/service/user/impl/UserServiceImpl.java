package com.xquark.service.user.impl;

import com.xquark.dal.IUser;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.user.UserService;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

//@Service("userService")
//service已在applicationContext-service.xml中定义
public class UserServiceImpl extends BaseServiceImpl implements UserService {

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Autowired
  private ShopMapper shopMapper;

  //第三方支付的User
  private LinkedHashMap<PaymentMode, String> thirdPartyUsers;

  //自己平台的账号ID，目前用于分佣
  @Value("${user.id.kkkd}")
  private String userIdKkkd;

  @Override
  public String loadKkkdUserId() {
    return userIdKkkd;
  }

  @Override
  public User loadUserByUsername(String username) throws UsernameNotFoundException {
    return userMapper.loadByLoginname(username);
  }

  @Override
  @Transactional
  public User registerAnonymous(String cid, String partner) {
    // 用户的验证
    if (!cid.startsWith(UniqueNoType.CID.name())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "非法注册用户");
    }
    User u = userMapper.loadByLoginname(cid);
    if (u != null) {
      return u;
    }
    User user = new User();
    user.setLoginname(cid);
    user.setPartner(partner);
    this.insert(user);
    return user;
  }

  @Override
  public User checkUserInfo(String u, String p) {
    User u2 = userMapper.loadByLoginname(u);
    if (u2 == null) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "用户" + u + "不存在");
    }

    if (!pwdEncoder.matches(p, u2.getPassword())) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "密码错误");
    }

    return u2;
  }

  @Override
  @Transactional
  public User register(String phone, String password) {
    // 用户的验证应该放到这里来，否则有安全问题
    User u = userMapper.loadByLoginname(phone);
    if (u != null) {
      if (pwdEncoder.matches(password, u.getPassword())) {
        return u;
      }
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR,
          "用户[" + phone + "]已经注册，请直接登录或者使用其他的用户名注册");
    }
    User user = new User();
    user.setLoginname(phone);
    user.setPhone(phone);
    user.setPassword(pwdEncoder.encode(password));
    this.insert(user);
    return user;
  }

  @Override
  public boolean isPwdSet() {
    User user = userMapper.selectByPrimaryKey(getCurrentUser().getId());
    return user != null && user.getPassword() != null;
  }

  @Override
  public boolean changePwd(String oldPwd, String newPwd) {
    IUser sessionUser = getCurrentUser();
    User u = load(sessionUser.getId());

    if (org.apache.commons.lang3.StringUtils.isNotBlank(oldPwd) && !pwdEncoder
        .matches(oldPwd, u.getPassword())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "原密码不正确");
    }

    if (!StringUtils.hasLength(oldPwd) && !StringUtils.hasLength(u.getPassword()) || oldPwd != null
        && u.getPassword() != null && pwdEncoder.matches(oldPwd, u.getPassword())) {
      newPwd = pwdEncoder.encode(newPwd);
      boolean changed = userMapper.changePwd(u.getId(), newPwd) > 0;
      if (changed) {
        u.setPassword(newPwd);
      }
      return changed;
    }
    return false;
  }

  @Override
  public boolean updateUserInfo(User user) {
    if (!getCurrentUser().getId().equals(user.getId())) {
      return false;
    }
    return userMapper.updateByPrimaryKeySelective(user) > 0;
  }

  @Override
  public User load(String id) {
    return userMapper.selectByPrimaryKey(id);
  }

  @Override
  public boolean isRegistered(String loginname) {
    return userMapper.countRegistered(loginname) > 0;
  }

  @Override
  public int insert(User record) {
    int id = userMapper.insert(record);
    addCode(record.getId());
    return id;
  }

  @Override
  public int delete(String id) {
    return userMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userMapper.undeleteByPrimaryKey(id);
  }

  @Override
  public boolean emptyUserPassword(String mobile) {
    return userMapper.emptyUserPasswordByLoginname(mobile);
  }

  @Override
  public boolean emptyUserPassword(String mobile, String code) {
    // validate mobile and code
    User user = userMapper.loadByLoginname(mobile);
    if (user == null) {
      return false;
    }
    String userId = user.getId();
    return userMapper.emptyUserPassword(userId);
  }

  @Override
  public User loadByLoginname(String loginname) {
    return userMapper.loadByLoginname(loginname);
  }

  @Override
  public LinkedHashMap<PaymentMode, String> getThirdPartyUsers() {
    return thirdPartyUsers;
  }

  public void setThirdPartyUsers(LinkedHashMap<PaymentMode, String> thirdPartyUsers) {
    this.thirdPartyUsers = thirdPartyUsers;
  }

  @Override
  public int updateNameAndIdCardNumByPrimaryKey(String id, String name, String idCardNum) {
    return userMapper.updateNameAndIdCardNumByPrimaryKey(id, name, idCardNum);
  }

  @Override
  public User registerExtUser(String partner, String extUserId, String userName, String avatar) {
    String loginname = extUserId + "@" + partner;
    User u = userMapper.loadByLoginname(loginname);
    if (u != null) {
      return u;
    }
    User user = new User();
    user.setLoginname(loginname);
    user.setName(userName);
    user.setAvatar(avatar);
    user.setPartner(partner);
    user.setExtUserId(extUserId);
    this.insert(user);
    return user;
  }

  @Override
  public int saveWithDrawType(String id, int type) {
    return userMapper.saveWithDrawType(id, type);
  }

  @Override
  public User loadByAdmin(String id) {
    return userMapper.selectByPKAndAdmin(id);
  }

  @Override
  public List<User> getFeeSplitAcct() {
    return userMapper.loadByRoles(2L);
  }

  @Override
  public User loadExtUser(String domain, String extUid) {
    User user = userMapper.selectByDomainAndExtUid(domain, extUid);
    if (user == null) {
      String loginname = extUid + "@" + domain;
      User u = userMapper.loadByLoginname(loginname);
      if (u != null) {
        return u;
      }
      User newUser = new User();

      newUser.setLoginname(loginname);
      newUser.setName("name@" + domain);
      newUser.setAvatar("avatar@" + domain);
      newUser.setPartner(domain);
      newUser.setExtUserId(extUid);
      try { //并发时
        this.insert(newUser);
      } catch (Exception e) {
        newUser = userMapper.selectByDomainAndExtUid(domain, extUid);
      }
      return newUser;
    }
    return user;
  }

  @Override
  public List<User> listNoCodeUsers() {
    return userMapper.listNoCodeUsers();
  }

  @Override
  public void addCode(String... ids) {
    if (ids == null || ArrayUtils.isEmpty(ids)) {
      return;
    }

    for (String p : ids) {
      userMapper.addCode(p);
    }
  }

  @Override
  public SellerInfoVO getSellerInfoVO(String id) {
    User user = load(id);
    Shop shop = shopMapper.selectByUserId(user.getId());
    SellerInfoVO vo = new SellerInfoVO();
    BeanUtils.copyProperties(user, vo);
    if (shop != null) {
      if (shop.getName() != null) {
        vo.setUserShopName(shop.getName());
      }
      if (shop.getServicePhone() != null && !shop.getServicePhone().equals("")) {
        vo.setServicePhone(shop.getServicePhone());
      } else {
        vo.setServicePhone(user.getPhone());
      }
    }
    return vo;
  }

  @Override
  public User loadAnonymousByPhone(String phone) {
    return userMapper.loadAnonymousByPhone(phone);
  }

  @Override
  public int updateAnonymousPhone(String cid, String phone) {
    return userMapper.updateAnonymousPhone(cid, phone);
  }

  @Override
  public User createAnonymous(String loginName, String partner, String phone) {
    User user = new User();
    user.setLoginname(loginName);
    user.setPartner(partner);
    user.setPhone(phone);
    this.insert(user);
    return user;
  }
}
