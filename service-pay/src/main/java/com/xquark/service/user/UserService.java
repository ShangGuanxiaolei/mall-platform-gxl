package com.xquark.service.user;

import com.xquark.dal.model.User;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.vo.SellerInfoVO;
import com.xquark.service.ArchivableEntityService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author jamesp
 */
@Validated
public interface UserService extends UserDetailsService, ArchivableEntityService<User> {

  User registerAnonymous(String cid, String partner);

  /**
   * 手机号码注册，若已存在，直接返回该对象
   */
  User register(@NotNull String loginname, String password);

  User registerExtUser(String partner, String extUserId, String userName, String avatar);

  /**
   * 修改密码
   *
   * @param oldPwd can be null
   * @param newPwd not null
   * @return changed?
   */
  boolean changePwd(String oldPwd, String newPwd);

  /**
   * @return 当前用户是否已设置密码
   */
  boolean isPwdSet();

  /**
   * 更新用户姓名，身份证号码
   */
  boolean updateUserInfo(User user);

  /**
   * 用户是否已注册
   */
  boolean isRegistered(String loginname);

  /**
   * 用户是否已注册
   */
  User loadByLoginname(String loginname);

  /**
   * 用户是否已注册
   */
  User loadExtUser(String domain, String extUid);

  boolean emptyUserPassword(String mobile);

  boolean emptyUserPassword(String mobile, String code);

  String loadKkkdUserId();

  LinkedHashMap<PaymentMode, String> getThirdPartyUsers();

  int updateNameAndIdCardNumByPrimaryKey(String id, String name, String idCardNum);

  int saveWithDrawType(String id, int type);

  User loadByAdmin(String id);

  List<User> getFeeSplitAcct();

  User checkUserInfo(String u, String p);

  List<User> listNoCodeUsers();

  void addCode(String... ids);

  SellerInfoVO getSellerInfoVO(String id);

  User loadAnonymousByPhone(String phone);

  int updateAnonymousPhone(String cid, String phone);

  User createAnonymous(String loginName, String partner, String phone);
}