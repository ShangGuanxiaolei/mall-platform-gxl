package com.xquark.promotion;

/**
 * @author: chenxi
 */

public enum InterceptorWay {

  BEFORE,
  AFTER,
  AROUND
}
