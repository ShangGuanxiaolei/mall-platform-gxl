package com.xquark.promotion;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;

/**
 * specify the method name and types of parameters in {@link PromotionExecutor}
 *
 * @author: chenxi
 */

public enum PromotionOpType {

  SHOP("processShop", Shop.class, User.class),
  PRODUCT("processProduct", Product.class, User.class),
  ORDER("processOrder", Order.class, List.class, User.class);

  private final String method;
  private final Class<?>[] paramTypes;

  PromotionOpType(String method, Class<?>... paramTypes) {
    this.method = method;
    this.paramTypes = paramTypes;
  }

  public String method() {
    return method;
  }

  public Class<?>[] paramTypes() {
    return paramTypes;
  }

  public static PromotionOpType fromOrdinal(int ordinal) {
    for (final PromotionOpType type : PromotionOpType.values()) {
      if (type.ordinal() == ordinal) {
        return type;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return method();
  }
}
