package com.xquark.promotion.classic.action;

import java.util.Map;

import com.google.common.collect.Maps;
import com.xquark.dal.model.promotion.PromotionActionType;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.provider.FreeLogisticsFee4Order;
import com.xquark.promotion.classic.action.provider.OffPrice4Order;
import com.xquark.promotion.classic.action.provider.ReducePrice4Order;

/**
 * @author: chenxi
 */

public class SimplePromotionActionFactory implements PromotionActionFactory {

  private final Map<PromotionActionType, ShopPromotionAction> shopActions = Maps.newHashMap();
  private final Map<PromotionActionType, ProductPromotionAction> productActions = Maps.newHashMap();
  private final Map<PromotionActionType, OrderPromotionAction> orderActions = Maps.newHashMap();

  public SimplePromotionActionFactory() {
    initShop();
    initProduct();
    initOrder();
  }

  private void initShop() {
    // TODO
  }

  private void initProduct() {
    // TODO
  }

  private void initOrder() {
    orderActions.put(PromotionActionType.REDUCE, new ReducePrice4Order());
    orderActions.put(PromotionActionType.OFF, new OffPrice4Order());
    orderActions.put(PromotionActionType.FREE_LOGISTICS_FEE, new FreeLogisticsFee4Order());
    // TODO
  }

  @Override
  public ShopPromotionAction getShopAction(PromotionModel model) {
    return shopActions.get(model.getAction());
  }

  @Override
  public ProductPromotionAction getProductAction(PromotionModel model) {
    return productActions.get(model.getAction());
  }

  @Override
  public OrderPromotionAction getOrderAction(PromotionModel model) {
    return orderActions.get(model.getAction());
  }
}
