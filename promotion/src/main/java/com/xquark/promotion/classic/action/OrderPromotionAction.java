package com.xquark.promotion.classic.action;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface OrderPromotionAction {

  public void process(PromotionModel model, Order order, List<OrderItem> orderItems)
      throws Exception;
}
