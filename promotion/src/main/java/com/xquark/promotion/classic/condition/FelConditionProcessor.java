package com.xquark.promotion.classic.condition;

import com.greenpineyu.fel.FelEngine;
import com.greenpineyu.fel.common.FelBuilder;
import com.greenpineyu.fel.context.FelContext;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public class FelConditionProcessor implements RuntimeConditionProcessor {

  private final static String CONTEXT_NAME = "condition";

  @Override
  public boolean match(PromotionModel model, RuntimeConditionParameter parameter) {
    final FelEngine engine = FelBuilder.engine();
    final FelContext ctx = engine.getContext();
    ctx.set(CONTEXT_NAME, parameter);
    return (Boolean) engine.eval(model.getCondition());
  }

}
