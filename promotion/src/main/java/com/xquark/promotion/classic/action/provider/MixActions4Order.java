package com.xquark.promotion.classic.action.provider;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.data.MixActionsData;

/**
 * @author: chenxi
 */

public class MixActions4Order extends DatableOrderPromotionAction<MixActionsData> {

  @Override
  protected void process(PromotionModel model, Order order,
      List<OrderItem> orderItems, MixActionsData data) {
    if (data.isFreeLogisticsFee()) {
      // TODO
      // do free logistics fee
    }
    // TODO
    // do reduce price
    // do off price
    // do use coupon
    // do attach product
  }

  @Override
  protected MixActionsData convertJson(String json) throws Exception {
    return converter.fromString(json, MixActionsData.class);
  }

}
