package com.xquark.promotion.classic.action;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface ProductPromotionAction {

  public void process(PromotionModel model, Product product) throws Exception;
}
