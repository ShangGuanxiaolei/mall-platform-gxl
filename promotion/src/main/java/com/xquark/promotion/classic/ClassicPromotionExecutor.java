package com.xquark.promotion.classic;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.PromotionActionType;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.PromotionExecutor;
import com.xquark.promotion.classic.auth.PromotionAuthenticator;
import com.xquark.promotion.classic.condition.RuntimeConditionParameter;
import com.xquark.promotion.classic.condition.RuntimeParameterType;
import com.xquark.promotion.classic.rule.PromotionRuleProcessor;

/**
 * The classic promotion executor, it lists almost all possible rule types in common promotion
 * scenarios, refer to {@link PromotionActionType} for information. the whole process is as follows:
 * 1, locate the proper {@link PromotionModel}s from {@link PromotionModelLocator}, if nothing found
 * then return. 2, for every {@link PromotionModel} P, if P can go through the authentication the
 * {@link PromotionAuthenticator} then get the proper promotion rule from {@link
 * PromotionRuleFactory} and deliver P to the rule for further process, or continue to handle the
 * next one.
 *
 * Please refer to {@link PromotionModel} and all types of rules for more information like {@link
 * ShopPromotionRule}, {@link ProductPromotionRule}, {@link OrderPromotionRule} and so on.
 *
 * @author: chenxi
 */

public class ClassicPromotionExecutor implements PromotionExecutor {

  private final PromotionModelLocator modelLocator;
  private final PromotionAuthenticator authenticator;
  private final PromotionRuleProcessor ruleProcessor;

  public ClassicPromotionExecutor(PromotionModelLocator modelLocator,
      PromotionAuthenticator authenticator,
      PromotionRuleProcessor ruleProcessor) {
    this.modelLocator = modelLocator;
    this.authenticator = authenticator;
    this.ruleProcessor = ruleProcessor;
  }

  @Override
  public void processShop(Shop shop, User user) throws Exception {
    final List<PromotionModel> models = modelLocator.getPromotionModel(shop);
    if (models == null || models.size() == 0) {
      return;
    }
    boolean valid = false;
    for (final PromotionModel model : models) {
      valid = authenticator.authenticate(model, user);
      if (!valid) {
        continue;
      }

      ruleProcessor
          .process(model, new RuntimeConditionParameter(RuntimeParameterType.SHOP, user, shop));
      if (!model.isSuperposition() && !model.isPlatform()) {
        break;
      }
    }
  }

  @Override
  public void processProduct(Product product, User user) throws Exception {
    final List<PromotionModel> models = modelLocator.getPromotionModel(product);
    if (models == null || models.size() == 0) {
      return;
    }
    boolean valid = false;
    for (final PromotionModel model : models) {
      valid = authenticator.authenticate(model, user);
      if (!valid) {
        continue;
      }
      ruleProcessor.process(model,
          new RuntimeConditionParameter(RuntimeParameterType.PRODUCT, user, product));
      if (!model.isSuperposition() && !model.isPlatform()) {
        break;
      }
    }
  }

  @Override
  public void processOrder(Order order, List<OrderItem> orderItems, User user)
      throws Exception {
    final List<PromotionModel> models = modelLocator.getPromotionModel(order, orderItems);
    if (models == null || models.size() == 0) {
      return;
    }
    boolean valid = false;
    for (final PromotionModel model : models) {
      valid = authenticator.authenticate(model, user);
      if (!valid) {
        continue;
      }
      ruleProcessor.process(model,
          new RuntimeConditionParameter(RuntimeParameterType.ORDER, user, order, orderItems));
      if (!model.isSuperposition() && !model.isPlatform()) {
        break;
      }
    }
  }

}
