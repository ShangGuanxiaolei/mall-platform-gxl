package com.xquark.promotion.classic.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdlm.common.bus.BusSignalListener;
import com.vdlm.common.bus.BusSignalManager;
import com.vdlm.common.job.shared.Job;
import com.xquark.dal.mapper.PromotionModelMapper;

/**
 * @author: chenxi
 */

public class PromotionModelUpdater implements BusSignalListener<Job> {

  private static final Logger LOG = LoggerFactory.getLogger(PromotionModelUpdater.class);

  private final PromotionModelMapper promotionModelMapper;

  public PromotionModelUpdater(BusSignalManager bsm, PromotionModelMapper promotionModelMapper) {
    bsm.bind(Job.class, this);
    this.promotionModelMapper = promotionModelMapper;
  }

  @Override
  public void signalFired(Job signal) {
    final String promotionModelId = (String) signal.getArguments()[0];
    promotionModelMapper.updatePeriodicityDate(promotionModelId);
    LOG.info("updated promotion model start and end date for id " + promotionModelId);
  }

}
