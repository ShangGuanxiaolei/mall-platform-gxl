package com.xquark.promotion.classic.action.data;

/**
 * @author: chenxi
 */

public class SimpleDataStructures {

  public static class ReduceData {

    private double price;

    public double getPrice() {
      return price;
    }

    public void setPrice(double price) {
      this.price = price;
    }

  }

  public static class OffData {

    private double off;

    public double getOff() {
      return off;
    }

    public void setOff(double off) {
      this.off = off;
    }

  }

  public static class CouponData {

    private double coupon;

    public double getCoupon() {
      return coupon;
    }

    public void setCoupon(double coupon) {
      this.coupon = coupon;
    }

  }
}
