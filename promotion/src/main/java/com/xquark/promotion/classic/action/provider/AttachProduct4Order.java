package com.xquark.promotion.classic.action.provider;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.data.AttachProductData;

/**
 * @author: chenxi
 */

public class AttachProduct4Order extends DatableOrderPromotionAction<AttachProductData> {

  @Override
  protected void process(PromotionModel model, Order order,
      List<OrderItem> orderItems, AttachProductData data) {
    // TODO Auto-generated method stub

  }

  @Override
  protected AttachProductData convertJson(String json) throws Exception {
    return converter.fromString(json, AttachProductData.class);
  }

}
