package com.xquark.promotion.classic.condition;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;

/**
 * @author: chenxi
 */

public class RuntimeConditionParameter {

  private final RuntimeParameterType type;
  private User user;
  private Shop shop;
  private Product product;
  private Order order;
  private List<OrderItem> orderItems;

  public RuntimeConditionParameter(RuntimeParameterType type, User user) {
    this.type = type;
    this.user = user;
  }

  public RuntimeConditionParameter(RuntimeParameterType type, User user, Shop shop) {
    this(type, user);
    this.shop = shop;
  }

  public RuntimeConditionParameter(RuntimeParameterType type, User user, Product product) {
    this(type, user);
    this.product = product;
  }

  public RuntimeConditionParameter(RuntimeParameterType type, User user, Order order,
      List<OrderItem> orderItems) {
    this(type, user);
    this.order = order;
    this.orderItems = orderItems;
  }

  public RuntimeParameterType getType() {
    return type;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(List<OrderItem> orderItems) {
    this.orderItems = orderItems;
  }

  public boolean containItem(String productId) {
    for (final OrderItem item : orderItems) {
      if (item.getProductId().equals(productId)) {
        return true;
      }
    }
    return false;
  }

  public OrderItem getItem(String productId) {
    for (final OrderItem item : orderItems) {
      if (item.getProductId().equals(productId)) {
        return item;
      }
    }
    return null;
  }
}
