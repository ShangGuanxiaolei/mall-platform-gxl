package com.xquark.promotion.classic.rule;

import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.OrderPromotionAction;
import com.xquark.promotion.classic.action.ProductPromotionAction;
import com.xquark.promotion.classic.action.PromotionActionFactory;
import com.xquark.promotion.classic.action.ShopPromotionAction;
import com.xquark.promotion.classic.condition.RuntimeConditionParameter;
import com.xquark.promotion.classic.condition.RuntimeConditionProcessor;
import com.xquark.promotion.classic.condition.RuntimeParameterType;

/**
 * @author: chenxi
 */

public class ConditionActionRuleProcessor implements PromotionRuleProcessor {

  private final RuntimeConditionProcessor rcp;
  protected final PromotionActionFactory factory;

  public ConditionActionRuleProcessor(RuntimeConditionProcessor rcp,
      PromotionActionFactory factory) {
    this.rcp = rcp;
    this.factory = factory;
  }

  @Override
  public void process(PromotionModel model,
      RuntimeConditionParameter parameter) throws Exception {
    if (!parameter.getType().needCondition()) {
      promote(model, parameter);
    } else if (rcp.match(model, parameter)) {
      promote(model, parameter);
    }
  }

  protected void promote(PromotionModel model, RuntimeConditionParameter parameter)
      throws Exception {
    final RuntimeParameterType type = parameter.getType();
    if (RuntimeParameterType.SHOP.equals(type)) {
      final ShopPromotionAction action = factory.getShopAction(model);
      if (action != null) {
        action.process(model, parameter.getShop());
      }
      return;
    }
    if (RuntimeParameterType.PRODUCT.equals(type)) {
      final ProductPromotionAction action = factory.getProductAction(model);
      if (action != null) {
        action.process(model, parameter.getProduct());
      }
      return;
    }
    if (RuntimeParameterType.ORDER.equals(type)) {
      final OrderPromotionAction action = factory.getOrderAction(model);
      if (action != null) {
        action.process(model, parameter.getOrder(), parameter.getOrderItems());
      }
      return;
    }
  }
}
