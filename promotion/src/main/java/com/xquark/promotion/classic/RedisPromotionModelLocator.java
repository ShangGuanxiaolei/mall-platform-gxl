package com.xquark.promotion.classic;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation use Redis to locate {@link PromotionModel}s.
 *
 * @author: chenxi
 */

public class RedisPromotionModelLocator extends AbstractPromotionModelLocator {

  @Override
  public List<PromotionModel> getPromotionModel(Shop shop) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<PromotionModel> getPromotionModel(Product product) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<PromotionModel> getPromotionModel(Order order,
      List<OrderItem> orderItems) {
    // TODO Auto-generated method stub
    return null;
  }

}
