package com.xquark.promotion.classic.auth;

import java.util.List;

import com.google.common.collect.Lists;
import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * The default implementation use composite pattern to iterate all inner implementations and call
 * {@link #authenticate()}) methods on them, note the order of inner implementations is important.
 *
 * @author: chenxi
 */

public class DefaultAuthenticator implements PromotionAuthenticator {

  private final List<PromotionAuthenticator> authentications = Lists.newArrayList();

  public DefaultAuthenticator() {
    init();
  }

  private void init() {
    /*
     * we can determine the basic order according common business
     */
    authentications.add(new DateAuthenticator());
    authentications.add(new MaxPurchaseAuthenticator());
    authentications.add(new CouponAuthenticator());
  }

  @Override
  public boolean authenticate(PromotionModel model, User user) {
    for (final PromotionAuthenticator authentication : authentications) {
      if (!authentication.authenticate(model, user)) {
        return false;
      }
    }
    return true;
  }

}
