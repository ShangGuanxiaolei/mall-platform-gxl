package com.xquark.promotion.classic.action;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface ShopPromotionAction {

  public void process(PromotionModel model, Shop shop) throws Exception;
}
