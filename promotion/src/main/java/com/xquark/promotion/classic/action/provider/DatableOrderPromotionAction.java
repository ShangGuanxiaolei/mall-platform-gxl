package com.xquark.promotion.classic.action.provider;

import java.util.List;

import org.codehaus.jackson.map.PropertyNamingStrategy;

import com.vdlm.common.protocol.JsonObjectConverter;
import com.vdlm.common.protocol.ObjectConverter;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.OrderPromotionAction;

/**
 * @author: chenxi
 */

public abstract class DatableOrderPromotionAction<T> implements OrderPromotionAction {

  protected ObjectConverter converter = new JsonObjectConverter(
      PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);

  @Override
  public void process(PromotionModel model, Order order,
      List<OrderItem> orderItems) throws Exception {
    final T data = parseData(model);
    process(model, order, orderItems, data);
  }

  protected abstract void process(PromotionModel model,
      Order order,
      List<OrderItem> orderItems,
      T data);

  protected abstract T convertJson(String json) throws Exception;

  T parseData(PromotionModel model) throws Exception {
    final String extra = model.getActionData();
    if (extra == null) {
      return null;
    }

    return convertJson(extra);
  }
}
