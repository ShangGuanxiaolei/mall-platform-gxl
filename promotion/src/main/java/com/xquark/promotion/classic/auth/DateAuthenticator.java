package com.xquark.promotion.classic.auth;

import java.util.Date;

import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation of {@link PromotionAuthenticator} to validate the start date and end date of
 * the pass-in {@link PromotionModel}
 *
 * @author: chenxi
 */

public class DateAuthenticator implements PromotionAuthenticator {

  @Override
  public boolean authenticate(PromotionModel model, User user) {
    final Date now = new Date();
    if (model.getStart() != null && now.compareTo(model.getStart()) < 0) {
      return false;
    }
    if (model.getEnd() != null && now.compareTo(model.getEnd()) > 0) {
      return false;
    }
    return true;
  }

}
