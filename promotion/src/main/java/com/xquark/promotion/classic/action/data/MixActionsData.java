package com.xquark.promotion.classic.action.data;

import java.util.List;

/**
 * { 'free_logistics_fee': true/false, 'reduce': 10.5, 'off': 0.8, 'coupon': 20, 'attach':
 * [{'pid':'1111', 'amount':2},{'pid':'2222', 'amount':3}] // TODO }
 *
 * @author: chenxi
 */

public class MixActionsData {

  private boolean freeLogisticsFee = false;
  private double reduce = 0;
  private double off = 1;
  private double coupon = 0;
  private List<ProductIDAmount> attach = null;

  public boolean isFreeLogisticsFee() {
    return freeLogisticsFee;
  }

  public void setFreeLogisticsFee(boolean freeLogisticsFee) {
    this.freeLogisticsFee = freeLogisticsFee;
  }

  public double getReduce() {
    return reduce;
  }

  public void setReduce(double reduce) {
    this.reduce = reduce;
  }

  public double getOff() {
    return off;
  }

  public void setOff(double off) {
    this.off = off;
  }

  public double getCoupon() {
    return coupon;
  }

  public void setCoupon(double coupon) {
    this.coupon = coupon;
  }

  public List<ProductIDAmount> getAttach() {
    return attach;
  }

  public void setAttach(List<ProductIDAmount> attach) {
    this.attach = attach;
  }

  public static class ProductIDAmount {

    private String pid;
    private int amount;

    public String getPid() {
      return pid;
    }

    public void setPid(String pid) {
      this.pid = pid;
    }

    public int getAmount() {
      return amount;
    }

    public void setAmount(int amount) {
      this.amount = amount;
    }

    @Override
    public String toString() {
      return "ProductIDAmount [pid=" + pid + ", amount=" + amount + "]";
    }

  }
}
