package com.xquark.promotion.classic;

import java.util.List;

import com.google.common.collect.Lists;
import com.xquark.dal.model.ObjectRange;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.RangeIdPair;
import com.xquark.dal.model.Shop;

/**
 * An abstract and helper class to assemble a correct parameter for further locating.
 *
 * @author: chenxi
 */

public abstract class AbstractPromotionModelLocator implements PromotionModelLocator {

  protected List<RangeIdPair> getRuleRangeIdPairs(Shop shop) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    pairs.add(new RangeIdPair(ObjectRange.SHOP, shop.getId()));
    return pairs;
  }

  protected List<RangeIdPair> getRuleRangeIdPairs(Product product) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    pairs.add(new RangeIdPair(ObjectRange.PRODUCT, product.getId()));
    pairs.add(new RangeIdPair(ObjectRange.SHOP, product.getShopId()));
    return pairs;
  }

  protected List<RangeIdPair> getRuleRangeIdPairs(Order order, List<OrderItem> orderItems) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    for (final OrderItem orderItem : orderItems) {
      pairs.add(new RangeIdPair(ObjectRange.PRODUCT, orderItem.getProductId()));
    }
    pairs.add(new RangeIdPair(ObjectRange.SHOP, order.getShopId()));
    return pairs;
  }

}
