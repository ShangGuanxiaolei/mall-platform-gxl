package com.xquark.promotion.classic.action.provider;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.data.SimpleDataStructures;

/**
 * @author: chenxi
 */

public class ReducePrice4Order extends
    DatableOrderPromotionAction<SimpleDataStructures.ReduceData> {

  @Override
  protected void process(PromotionModel model, Order order,
      List<OrderItem> orderItems, SimpleDataStructures.ReduceData data) {
    // TODO Auto-generated method stub

  }

  @Override
  protected SimpleDataStructures.ReduceData convertJson(String json) throws Exception {
    return converter.fromString(json, SimpleDataStructures.ReduceData.class);
  }

}
