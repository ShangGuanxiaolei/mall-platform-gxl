package com.xquark.promotion.classic.action.data;

import java.util.List;

/**
 * {'list': [{'pid':'1111',  'list':[{'skuID':'dsd','amount':20}] },{'pid':'2222', 'list':[{
 * 'skuid':'ds','amount':28 }] }]} 1.当sku list 为空时，意为附送商品是由用户自己根据pid 进行选择sku,amout 可不固定 2.否则
 * 则系统强制赠送用户相应的sku，sku,amount 为固定值
 *
 * @author: chenxi
 */

public class AttachProductData {

  private List<SkuData> list;

  public static class SkuData {

    private String pid;

    private int amount;

    private List<SkuIDAmount> list;


    public String getPid() {
      return pid;
    }

    public void setPid(String pid) {
      this.pid = pid;
    }

    public List<SkuIDAmount> getList() {
      return list;
    }

    public void setList(List<SkuIDAmount> list) {
      this.list = list;
    }

    public int getAmount() {
      return amount;
    }

    public void setAmount(int amount) {
      this.amount = amount;
    }


  }

  public List<SkuData> getList() {
    return list;
  }


  public void setList(List<SkuData> list) {
    this.list = list;
  }


  public static class SkuIDAmount {

    private String skuID;
    private int amount;

    public String getSkuID() {
      return skuID;
    }

    public void setSkuID(String skuID) {
      this.skuID = skuID;
    }

    public int getAmount() {
      return amount;
    }

    public void setAmount(int amount) {
      this.amount = amount;
    }

    @Override
    public String toString() {
      return "ProductIDAmount [skuID=" + skuID + ", amount=" + amount + "]";
    }


  }
}
