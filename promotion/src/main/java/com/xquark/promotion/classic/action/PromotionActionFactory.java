package com.xquark.promotion.classic.action;

import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface PromotionActionFactory {

  public ShopPromotionAction getShopAction(PromotionModel model);

  public ProductPromotionAction getProductAction(PromotionModel model);

  public OrderPromotionAction getOrderAction(PromotionModel model);
}
