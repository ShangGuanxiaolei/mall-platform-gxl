package com.xquark.promotion.classic.action.provider;

import java.math.BigDecimal;
import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.OrderPromotionAction;

/**
 * @author: chenxi
 */

public class FreeLogisticsFee4Order implements OrderPromotionAction {

  @Override
  public void process(PromotionModel model, Order order,
      List<OrderItem> orderItems) throws Exception {
    final BigDecimal subtract = order.getTotalFee().subtract(order.getLogisticsFee());
    if (subtract.compareTo(new BigDecimal(0)) >= 0) {
      order.setTotalFee(subtract);
    } else {
      order.setTotalFee(new BigDecimal(0));
    }
    order.setLogisticsFee(new BigDecimal(0));
  }

}
