package com.xquark.promotion.classic.auth;

import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * This interface is responsible for doing authentication of the pass-in {@link PromotionModel} to
 * check if it is valid. for sample, the start date and end date of the promotion, the max number of
 * purchase, the coupon code and so on. the return value true indicates a successful authentication
 * and can continue further process, false indicates a failed authentication and stop at this step.
 *
 * @author: chenxi
 */

public interface PromotionAuthenticator {

  public boolean authenticate(PromotionModel model, User user);
}
