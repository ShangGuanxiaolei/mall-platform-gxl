package com.xquark.promotion.classic.condition;

import com.xquark.dal.model.promotion.PromotionActionType;

/**
 * @author: chenxi
 */

public enum RuntimeParameterType implements Conditional {

  SHOP(1, "shop", false),
  PRODUCT(2, "product", false),
  ORDER(3, "order", true);

  private final int code;
  private final String desc;
  private final boolean conditional;

  RuntimeParameterType(int code, String desc, boolean conditional) {
    this.code = code;
    this.desc = desc;
    this.conditional = conditional;
  }

  public int code() {
    return code;
  }

  public String desc() {
    return desc;
  }

  @Override
  public boolean needCondition() {
    return conditional;
  }

  public static PromotionActionType fromCode(int code) {
    for (final PromotionActionType type : PromotionActionType.values()) {
      if (type.code() == code) {
        return type;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return desc();
  }

}
