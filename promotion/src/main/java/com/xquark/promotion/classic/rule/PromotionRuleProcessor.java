package com.xquark.promotion.classic.rule;

import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.condition.RuntimeConditionParameter;

/**
 * @author: chenxi
 */

public interface PromotionRuleProcessor {

  public void process(PromotionModel model, RuntimeConditionParameter parameter) throws Exception;
}
