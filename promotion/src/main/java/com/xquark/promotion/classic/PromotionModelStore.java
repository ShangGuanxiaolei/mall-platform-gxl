package com.xquark.promotion.classic;

import java.util.List;

import com.xquark.dal.model.promotion.PromotionModel;

/**
 * The promotion model store to save proper {@link PromotionModel}s to somewhere like memory, RDBMS,
 * NoSql and so on.
 *
 * @author: chenxi
 */

public interface PromotionModelStore {

  public boolean savePromotionModel(PromotionModel model);

  public boolean savePromotionModels(List<PromotionModel> models);
}
