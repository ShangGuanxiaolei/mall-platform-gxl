package com.xquark.promotion.classic;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vdlm.common.job.client.JobSchedulerClient;
import com.vdlm.common.job.shared.Job;
import com.vdlm.common.job.shared.JobStatus;
import com.xquark.dal.mapper.PromotionModelMapper;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation use RDBMS to save {@link PromotionModel}s.
 *
 * @author: chenxi
 */

public class RDBMSPromotionModelStore implements PromotionModelStore {

  private static final Logger LOG = LoggerFactory.getLogger(RDBMSPromotionModelStore.class);

  private final PromotionModelMapper promotionModelMapper;
  private final JobSchedulerClient scheduler;

  public RDBMSPromotionModelStore(PromotionModelMapper promotionModelMapper,
      JobSchedulerClient scheduler) {
    this.promotionModelMapper = promotionModelMapper;
    this.scheduler = scheduler;
  }

  @Override
  public boolean savePromotionModel(PromotionModel model) {
    final int inserts = promotionModelMapper.insert(model);
    if (inserts != 1) {
      LOG.error("failed to insert promotion model to db: " + model);
      return false;
    }
    if (!model.isPeriodicity()) {
      return true;
    }
    final String id = model.getId();
    final Job job = new Job("promotion" + id, model.getSellerId())
        .withGroup(model.getPartner())
        .withArgument(new Object[]{id})
        .withCron(model.getCronExpression());
    final JobStatus status = scheduler.scheduleJob(job);
    if (JobStatus.StatusCode.SUCCEED.equals(status.getStatusCode())) {
      return true;
    }
    LOG.warn("inserted a periodical promotion model to db, but cannot create a job: " + model);
    return false;
  }

  @Override
  public boolean savePromotionModels(List<PromotionModel> models) {
    // TODO Auto-generated method stub
    return false;
  }

}
