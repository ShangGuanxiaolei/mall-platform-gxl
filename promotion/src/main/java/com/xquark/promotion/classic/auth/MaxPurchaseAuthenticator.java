package com.xquark.promotion.classic.auth;

import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation of {@link PromotionAuthenticator} to validate the max number of purchase of the
 * pass-in {@link PromotionModel}
 *
 * @author: chenxi
 */

public class MaxPurchaseAuthenticator implements PromotionAuthenticator {

  @Override
  public boolean authenticate(PromotionModel model, User user) {
    if (user == null) {
      return false;
    }
    // TODO validate max amount the buyer
    /*
     * if (user.alreadyBoughtCount > model.getMaxAmount()) {
     * 		return;
     * }
     */
    return false;
  }

}
