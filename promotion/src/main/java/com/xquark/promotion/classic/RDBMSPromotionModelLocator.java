package com.xquark.promotion.classic;

import java.util.List;

import com.xquark.dal.mapper.PromotionModelMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.RangeIdPair;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation use RDBMS to locate {@link PromotionModel}s.
 *
 * @author: chenxi
 */

public class RDBMSPromotionModelLocator extends AbstractPromotionModelLocator {

  private final PromotionModelMapper promotionModelMapper;

  public RDBMSPromotionModelLocator(PromotionModelMapper promotionModelMapper) {
    this.promotionModelMapper = promotionModelMapper;
  }

  @Override
  public List<PromotionModel> getPromotionModel(Shop shop) {
    final List<RangeIdPair> pairs = super.getRuleRangeIdPairs(shop);
    return promotionModelMapper.selectByRangesAndObjIds(pairs);
  }

  @Override
  public List<PromotionModel> getPromotionModel(Product product) {
    final List<RangeIdPair> pairs = super.getRuleRangeIdPairs(product);
    return promotionModelMapper.selectByRangesAndObjIds(pairs);
  }

  @Override
  public List<PromotionModel> getPromotionModel(Order order, List<OrderItem> orderItems) {
    final List<RangeIdPair> pairs = super.getRuleRangeIdPairs(order, orderItems);
    return promotionModelMapper.selectByRangesAndObjIds(pairs);
  }

}
