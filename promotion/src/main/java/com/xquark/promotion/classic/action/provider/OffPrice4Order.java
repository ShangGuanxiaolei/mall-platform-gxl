package com.xquark.promotion.classic.action.provider;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.data.SimpleDataStructures;

/**
 * @author: chenxi
 */

public class OffPrice4Order extends DatableOrderPromotionAction<SimpleDataStructures.OffData> {

  @Override
  protected void process(PromotionModel model, Order order,
      List<OrderItem> orderItems, SimpleDataStructures.OffData data) {
    // TODO Auto-generated method stub

  }

  @Override
  protected SimpleDataStructures.OffData convertJson(String json) throws Exception {
    return converter.fromString(json, SimpleDataStructures.OffData.class);
  }

}
