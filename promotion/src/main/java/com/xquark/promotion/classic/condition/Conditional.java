package com.xquark.promotion.classic.condition;

/**
 * @author: chenxi
 */

public interface Conditional {

  public boolean needCondition();
}
