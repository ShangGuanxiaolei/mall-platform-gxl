package com.xquark.promotion.classic.action.provider;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;
import com.xquark.promotion.classic.action.data.SimpleDataStructures;
import com.xquark.promotion.classic.action.data.SimpleDataStructures.CouponData;

/**
 * @author: chenxi
 */

public class UseCoupon4Order extends DatableOrderPromotionAction<SimpleDataStructures.CouponData> {

  @Override
  protected void process(PromotionModel model, Order order,
      List<OrderItem> orderItems, CouponData data) {
    // TODO Auto-generated method stub

  }

  @Override
  protected CouponData convertJson(String json) throws Exception {
    return converter.fromString(json, SimpleDataStructures.CouponData.class);
  }

}
