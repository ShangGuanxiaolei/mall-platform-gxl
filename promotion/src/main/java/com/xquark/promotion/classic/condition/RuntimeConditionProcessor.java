package com.xquark.promotion.classic.condition;

import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface RuntimeConditionProcessor {

  public boolean match(PromotionModel model, RuntimeConditionParameter parameter);
}
