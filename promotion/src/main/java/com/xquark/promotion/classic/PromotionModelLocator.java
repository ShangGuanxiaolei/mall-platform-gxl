package com.xquark.promotion.classic;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * The promotion model locator to fetch proper {@link PromotionModel}s from somewhere like memory,
 * RDBMS, NoSql and so on.
 *
 * @author: chenxi
 */

public interface PromotionModelLocator {

  public List<PromotionModel> getPromotionModel(Shop shop);

  public List<PromotionModel> getPromotionModel(Product product);

  public List<PromotionModel> getPromotionModel(Order order, List<OrderItem> orderItems);
}
