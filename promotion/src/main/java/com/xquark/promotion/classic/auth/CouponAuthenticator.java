package com.xquark.promotion.classic.auth;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.mapper.CouponCodeMapper;
import com.xquark.dal.model.User;
import com.xquark.dal.model.promotion.CouponCode;
import com.xquark.dal.model.promotion.CouponCodeStatus;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * An implementation of {@link PromotionAuthenticator} to validate coupon code of the pass-in {@link
 * PromotionModel}, like the start date and end date of the coupon, whether the coupon is already
 * used, the code value and so on.
 *
 * @author: chenxi
 */

public class CouponAuthenticator implements PromotionAuthenticator {

  private final static String ZERO_ENCODED_STR = "74";

  @Autowired
  private CouponCodeMapper couponCodeMapper;

  @Override
  public boolean authenticate(PromotionModel model, User user) {
    if (ZERO_ENCODED_STR.equals(model.getCouponCodeId())) {
      return true;
    }

    // validate coupon
    final CouponCode coupon = couponCodeMapper.selectByPrimaryKey(model.getCouponCodeId());
    if (coupon.isOnlyOnce() &&
        !CouponCodeStatus.UNUSED.equals(coupon.getStatus())) {
      return false;
    }
    final Date now = new Date();
    if (coupon.getStart() != null && now.compareTo(coupon.getStart()) < 0) {
      return false;
    }
    if (coupon.getEnd() != null && now.compareTo(coupon.getEnd()) > 0) {
      return false;
    }
    // TODO compare coupon value
    /*
     * String code = getCurrentUser().getCode();
     * if (coupon.getValue().equals(code)) {
     *     return true;
     * }
     */
    return false;
  }

}
