package com.xquark.promotion;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.codehaus.jackson.map.PropertyNamingStrategy;
import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.vdlm.common.bus.BusSignalManager;
import com.vdlm.common.bus.DefaultBusRegistry;
import com.vdlm.common.job.JobScheduler;
import com.vdlm.common.job.client.JobSchedulerClient;
import com.vdlm.common.jobserver.scheduler.QuartzJobScheduler;
import com.vdlm.common.jobserver.scheduler.QuartzQueuedJob;
import com.vdlm.common.jobserver.workflow.JVMBusEventLauncher;
import com.vdlm.common.jobserver.workflow.WorkflowLauncher;
import com.vdlm.common.protocol.JsonObjectConverter;
import com.vdlm.common.protocol.ObjectConverter;
import com.xquark.dal.mapper.PromotionModelMapper;
import com.xquark.promotion.classic.ClassicPromotionExecutor;
import com.xquark.promotion.classic.PromotionModelLocator;
import com.xquark.promotion.classic.PromotionModelStore;
import com.xquark.promotion.classic.RDBMSPromotionModelLocator;
import com.xquark.promotion.classic.RDBMSPromotionModelStore;
import com.xquark.promotion.classic.action.PromotionActionFactory;
import com.xquark.promotion.classic.action.SimplePromotionActionFactory;
import com.xquark.promotion.classic.auth.DefaultAuthenticator;
import com.xquark.promotion.classic.auth.PromotionAuthenticator;
import com.xquark.promotion.classic.condition.FelConditionProcessor;
import com.xquark.promotion.classic.condition.RuntimeConditionProcessor;
import com.xquark.promotion.classic.job.PromotionModelUpdater;
import com.xquark.promotion.classic.rule.ConditionActionRuleProcessor;
import com.xquark.promotion.classic.rule.PromotionRuleProcessor;

/**
 * @author: chenxi
 */

@Configuration
@ImportResource("classpath:applicationContext-promotion.xml")
public class PromotionConfig {

  private final static String QUARTZ_FILE = "quartz.properties";

//	@Autowired
//	@Bean
//	PromotionInterceptor promotionInterceptor(PromotionExecutor executor) {
//		return new PromotionInterceptor(executor);
//	}

  @Autowired
  @Bean
  PromotionExecutor promotionExecutor(PromotionModelLocator modelLocator,
      PromotionAuthenticator authenticator,
      PromotionRuleProcessor ruleProcessor) {
    return new ClassicPromotionExecutor(modelLocator, authenticator, ruleProcessor);
  }

  @Autowired
  @Bean
  PromotionRuleProcessor promotionRuleProcessor(RuntimeConditionProcessor rcp,
      PromotionActionFactory factory) {
    return new ConditionActionRuleProcessor(rcp, factory);
  }

  @Autowired
  @Bean
  PromotionModelLocator promotionModelLocator(PromotionModelMapper promotionModelMapper) {
    return new RDBMSPromotionModelLocator(promotionModelMapper);
  }

  @Bean
  RuntimeConditionProcessor runtimeConditionProcessor() {
    return new FelConditionProcessor();
  }

  @Bean
  PromotionActionFactory promotionActionFactory() {
    return new SimplePromotionActionFactory();
  }

  @Bean
  PromotionAuthenticator PromotionAuthentication() {
    return new DefaultAuthenticator();
  }

  @Autowired
  @Bean
  PromotionModelStore promotionModelStore(PromotionModelMapper promotionModelMapper,
      JobSchedulerClient scheduler) {
    return new RDBMSPromotionModelStore(promotionModelMapper, scheduler);
  }

  @Bean
  ObjectConverter objectConverter() {
    return new JsonObjectConverter(
        PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
  }

  @Bean
  BusSignalManager busSignalManager() {
    return new BusSignalManager(new DefaultBusRegistry());
  }

//	@Bean
//	JobFactory jobFactory() {
//		return new QuartzJobFactory();
//	}

  @Bean
  SchedulerFactory schedulerFactory() {
    final Properties properties = new Properties();
    final InputStream resource = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream(QUARTZ_FILE);
    try {
      properties.load(resource);
    } catch (final IOException e) {
      throw new RuntimeException("Failed to load properties file", e);
    }

    final StdSchedulerFactory factory = new StdSchedulerFactory();
    try {
      factory.initialize(properties);
    } catch (final SchedulerException e) {
      throw new RuntimeException("Failed to initialize scheduler factory", e);
    }
    return factory;
  }

  @Autowired
  @Bean
  Scheduler scheduler(SchedulerFactory schedulerFactory,
      JobFactory jobFactory) {
    try {
      final Scheduler scheduler = schedulerFactory.getScheduler();
      scheduler.setJobFactory(jobFactory);
      // TODO move to a startable place?
      scheduler.start();
      return scheduler;
    } catch (final SchedulerException e) {
      final String error = "[ERROR] initialize quartz scheduler: " + e.getMessage();
      throw new RuntimeException(error, e);
    }
  }

  @Autowired
  @Bean
  JobScheduler jobScheduler(Scheduler scheduler) {
    return new QuartzJobScheduler(scheduler);
  }

  @Autowired
  @Bean
  JobSchedulerClient jobSchedulerClient(JobScheduler scheduler) {
    return new JobSchedulerClient(scheduler);
  }

  @Autowired
  @Bean
  PromotionModelUpdater promotionModelUpdater(BusSignalManager bsm,
      PromotionModelMapper promotionModelMapper) {
    return new PromotionModelUpdater(bsm, promotionModelMapper);
  }

  @Autowired
  @Bean
  WorkflowLauncher workflowLauncher(BusSignalManager bsm) {
    return new JVMBusEventLauncher(bsm);
  }

  @Autowired
  @Bean
  Job job(WorkflowLauncher launcher) {
    return new QuartzQueuedJob(launcher);
  }

}
