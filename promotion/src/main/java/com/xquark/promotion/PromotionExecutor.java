package com.xquark.promotion;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;

/**
 * The core interface as the only entry to execute all types of parameters, which used by {@link
 * PromotionInterceptor} to invoke the whole promotion process.
 *
 * @author: chenxi
 */

public interface PromotionExecutor {

  public void processShop(Shop shop, User user) throws Exception;

  public void processProduct(Product product, User user) throws Exception;

  public void processOrder(Order order, List<OrderItem> orderItems, User user) throws Exception;
}
