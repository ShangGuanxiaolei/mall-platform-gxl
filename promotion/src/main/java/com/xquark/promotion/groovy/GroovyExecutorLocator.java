package com.xquark.promotion.groovy;

import com.xquark.promotion.PromotionExecutor;


/**
 * The groovy promotion executor which loads the correct {@link PromotionExecutor} through {@link
 * PromotionClassLoader}
 *
 * @author: chenxi
 */

public interface GroovyExecutorLocator {

  public PromotionExecutor loadPromotionExecutor(String customerId) throws Exception;

  public void clearPromotionExecutor(String customerId) throws Exception;
}
