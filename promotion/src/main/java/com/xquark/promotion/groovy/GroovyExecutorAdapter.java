package com.xquark.promotion.groovy;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.promotion.PromotionExecutor;

/**
 * An adapter implemenation of {@link PromotionExecutor} just for groovy class to extend.
 *
 * @author: chenxi
 */

public class GroovyExecutorAdapter implements PromotionExecutor {

  @Override
  public void processShop(Shop shop, User user) throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public void processProduct(Product product, User user) throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public void processOrder(Order order, List<OrderItem> orderItems, User user)
      throws Exception {
    // TODO Auto-generated method stub

  }

}
