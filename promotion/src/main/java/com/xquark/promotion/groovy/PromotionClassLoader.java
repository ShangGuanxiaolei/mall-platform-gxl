package com.xquark.promotion.groovy;

import groovy.lang.GroovyClassLoader;

/**
 * Adding a useful method to remove the source of specified file name from memory cache to support
 * running time modification for the groovy file.
 *
 * @author: chenxi
 */

public class PromotionClassLoader extends GroovyClassLoader {

  public PromotionClassLoader(ClassLoader loader) {
    super(loader);
  }

  public void removeSourceCacheEntry(String name) {
    synchronized (sourceCache) {
      sourceCache.remove(name);
    }
  }
}
