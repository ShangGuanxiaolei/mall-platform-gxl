package com.xquark.promotion.groovy;

import java.io.File;
import java.net.URL;

import com.xquark.promotion.PromotionExecutor;

/**
 * An abstract implementation to do the core operations for loading groovy file and clear specified
 * file from cache in {@link PromotionClassLoader}
 *
 * @author: chenxi
 */

public abstract class AbstractGrooyExecutorLocator implements GroovyExecutorLocator {

  private static final String MKT_EXE_EXT = ".groovy";

  private final PromotionClassLoader pcl;

  public AbstractGrooyExecutorLocator() {
    pcl = new PromotionClassLoader(getClass().getClassLoader());
  }

  @Override
  public PromotionExecutor loadPromotionExecutor(String customerId) throws Exception {
    final Class<?> groovyClass = pcl.parseClass(new File(getResource(customerId) + MKT_EXE_EXT));

    final PromotionExecutor mktExe = (PromotionExecutor) groovyClass.newInstance();
    // FIXME just for test
    clearPromotionExecutor(customerId);
    return mktExe;
  }

  @Override
  public void clearPromotionExecutor(String customerId) throws Exception {
    final File file = new File(getResource(customerId) + MKT_EXE_EXT);
    final URL url = file.toURI().toURL();
    pcl.removeSourceCacheEntry(url.toExternalForm());
  }

  protected abstract String getResource(String customerId);
}
