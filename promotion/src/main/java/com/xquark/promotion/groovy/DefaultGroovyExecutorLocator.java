package com.xquark.promotion.groovy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The simple implementation extending the core operations about groovy plus just specifying the
 * groovy file root path.
 *
 * @author: chenxi
 */

@Component("promotionExecutorLocator")
public class DefaultGroovyExecutorLocator extends AbstractGrooyExecutorLocator {

  @Value("${promotion.exe.rootpath}")
  private String exeRootpath;

  @Override
  protected String getResource(String customerId) {
    return exeRootpath + customerId;
  }

}
