package com.xquark.promotion.groovy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.promotion.PromotionExecutor;

/**
 * A proxy {@link PromotionExecutor} which responsible for extracting proper parameter, delegating
 * the process to target {@link PromotionExecutor} loaded by {@link GroovyExecutorLocator} through
 * the parameter above.
 *
 * @author: chenxi
 */

public class GroovyExecutorProxy implements PromotionExecutor {

  @Autowired
  private GroovyExecutorLocator locator;

  @Override
  public void processShop(Shop shop, User user) throws Exception {
    final String sellerId = shop.getOwnerId();
    final PromotionExecutor exe = locator.loadPromotionExecutor(sellerId);
    exe.processShop(shop, user);
  }

  @Override
  public void processProduct(Product product, User user) throws Exception {
    final String sellerId = product.getUserId();
    final PromotionExecutor exe = locator.loadPromotionExecutor(sellerId);
    exe.processProduct(product, user);
  }

  @Override
  public void processOrder(Order order, List<OrderItem> orderItems, User user) throws Exception {
    final String sellerId = order.getSellerId();
    final PromotionExecutor exe = locator.loadPromotionExecutor(sellerId);
    exe.processOrder(order, orderItems, user);
  }

}
