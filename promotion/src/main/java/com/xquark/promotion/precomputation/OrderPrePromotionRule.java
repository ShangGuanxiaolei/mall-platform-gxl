package com.xquark.promotion.precomputation;

import java.math.BigDecimal;
import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface OrderPrePromotionRule {

  public BigDecimal preprocess(PromotionModel model, Order order, List<OrderItem> orderItems)
      throws Exception;
}
