package com.xquark.promotion.precomputation;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public interface MultiPromotionModelLocator {

  public List<List<PromotionModel>> getPromotionModel(Shop shop);

  public List<List<PromotionModel>> getPromotionModel(Product product);

  public List<List<PromotionModel>> getPromotionModel(Order order, List<OrderItem> orderItems);
}
