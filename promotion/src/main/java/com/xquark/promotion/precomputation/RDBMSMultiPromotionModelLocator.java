package com.xquark.promotion.precomputation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.mapper.PromotionModelMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.RangeIdPair;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public class RDBMSMultiPromotionModelLocator extends AbstractMultiPromotionModelLocator implements
    MultiPromotionModelLocator {

  @Autowired
  private PromotionModelMapper promotionModelMapper;

  @Override
  public List<List<PromotionModel>> getPromotionModel(Shop shop) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<List<PromotionModel>> getPromotionModel(
      Product product) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<List<PromotionModel>> getPromotionModel(
      Order order, List<OrderItem> orderItems) {
    final List<RangeIdPair> pairs = super.getRuleRangeIdPairs(order, orderItems);
    final List<PromotionModel> models = promotionModelMapper.selectByRangesAndObjIds(pairs);
    return getPossiblePromotionModels(models);
  }

}
