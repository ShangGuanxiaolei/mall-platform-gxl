package com.xquark.promotion.precomputation;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * @author: chenxi
 */

public class PermutationCombination {

  private final List<Integer> sizes = Lists.newArrayList();
  private final List<Integer> currents = Lists.newArrayList();

  private int totalCombinations = 0;
  private int currentCombination = 0;

  // make sure pass-in size greater than 0
  void addSize(int size) {
    sizes.add(size);
  }

  void reset() {
    if (sizes.size() == 0) {
      return;
    }

    totalCombinations = 1;
    currentCombination = 1;
    for (int i = 0; i < sizes.size(); i++) {
      currents.add(0);
      totalCombinations *= sizes.get(i);
    }
  }

  boolean hasNext() {
    return currentCombination < totalCombinations;
  }

  List<Integer> next() {
    if (currentCombination >= totalCombinations) {
      throw new IllegalArgumentException(
          "has no more combination. total combinations is " + totalCombinations);
    }

    int size;
    int currentSize;
    for (int i = 0; i < sizes.size(); i++) {
      size = sizes.get(i);
      currentSize = currents.get(i);
      if (currentSize < size) {
        currents.set(i, ++currentSize);
        break;
      }
      continue;
    }
    currentCombination++;
    return Collections.unmodifiableList(currents);
  }

  int getTotalCombinations() {
    return totalCombinations;
  }

}
