package com.xquark.promotion.precomputation;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xquark.dal.model.ObjectRange;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.RangeIdPair;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.promotion.PromotionModel;

/**
 * @author: chenxi
 */

public abstract class AbstractMultiPromotionModelLocator implements MultiPromotionModelLocator {

  protected List<RangeIdPair> getRuleRangeIdPairs(Shop shop) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    pairs.add(new RangeIdPair(ObjectRange.SHOP, shop.getId()));
    return pairs;
  }

  protected List<RangeIdPair> getRuleRangeIdPairs(Product product) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    pairs.add(new RangeIdPair(ObjectRange.PRODUCT, product.getId()));
    pairs.add(new RangeIdPair(ObjectRange.SHOP, product.getShopId()));
    return pairs;
  }

  protected List<RangeIdPair> getRuleRangeIdPairs(Order order, List<OrderItem> orderItems) {
    final List<RangeIdPair> pairs = Lists.newArrayList();
    for (final OrderItem orderItem : orderItems) {
      pairs.add(new RangeIdPair(ObjectRange.PRODUCT, orderItem.getProductId()));
    }
    pairs.add(new RangeIdPair(ObjectRange.SHOP, order.getShopId()));
    return pairs;
  }

  /**
   * helper method to support multiply {@link PromotionModel} in same {@link ObjectRange}, use
   * {@link LinkedHashMap} to keep the same order of putting and getting.
   */
  protected List<List<PromotionModel>> getPossiblePromotionModels(List<PromotionModel> models) {
    final Map<ObjectRange, List<PromotionModel>> map = Maps.newLinkedHashMap();
    List<PromotionModel> possibles;
    ObjectRange range;
    for (final PromotionModel model : models) {
      range = model.getRange();
      possibles = map.get(range);
      if (possibles == null) {
        possibles = Lists.newArrayList();
        map.put(range, possibles);
      }
      possibles.add(model);
    }

    final PermutationCombination pc = new PermutationCombination();
    final List<List<PromotionModel>> allPossibles = Lists.newArrayList();
    possibles = Lists.newArrayList();
    final Iterator<ObjectRange> it = map.keySet().iterator();
    int index = 0;
    final Map<Integer, ObjectRange> indexRanges = Maps.newHashMap();
    while (it.hasNext()) {
      range = it.next();
      indexRanges.put(index, range);
      pc.addSize(map.get(range).size());
      index++;
    }
    pc.reset();

    List<Integer> combinations;
    int pos;
    PromotionModel model;
    while (pc.hasNext()) {
      combinations = pc.next();
      for (int i = 0; i < combinations.size(); i++) {
        pos = combinations.get(i);
        range = indexRanges.get(i);
        model = map.get(range).get(pos);
        possibles.add(model);
      }
      allPossibles.add(possibles);
    }

    return allPossibles;
  }

}
