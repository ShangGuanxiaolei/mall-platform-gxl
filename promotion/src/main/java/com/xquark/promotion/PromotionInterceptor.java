package com.xquark.promotion;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.xquark.dal.model.User;

/**
 * This interceptor should be configured as an aspect bean through spring configuration file or
 * spring annotation,
 *
 * @author: chenxi
 */

public class PromotionInterceptor {

  @Autowired
  private PromotionExecutor executor;

  public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
    Method method = joinPoint.getSignature().getDeclaringType().getMethods()[0];
    final Object[] arguments = joinPoint.getArgs();
    final PromotionMethod pm = method.getAnnotation(PromotionMethod.class);
    /*
     * just call original if no {@link PromotionMethod} annotation found
     */
    if (pm == null) {
      return joinPoint.proceed();
    }

    /*
     * prepare the method and parameters for the promotion executor
     */
    final PromotionOpType type = pm.type();
    final Class<?>[] paramTypes = type.paramTypes();
    final Object[] promArgs = getPromotionArgs(arguments, paramTypes);
    final String callMethod = type.method();
    method = PromotionExecutor.class.getMethod(callMethod, paramTypes);

    /*
     * do the special method on promotion executor
     */
    if (InterceptorWay.BEFORE.equals(pm.way()) || InterceptorWay.AROUND.equals(pm.way())) {
      method.invoke(executor, promArgs);
    }

    final Object result = joinPoint.proceed();

    if (InterceptorWay.AFTER.equals(pm.way()) || InterceptorWay.AROUND.equals(pm.way())) {
      method.invoke(executor, promArgs);
    }

    return result;
  }

  /*
   * extract the parameters for promotion operation through parameter types
   */
  private Object[] getPromotionArgs(Object[] arguments, Class<?>[] paramTypes) {
    // one more argument for current user
    final Object[] promArgs = new Object[paramTypes.length];
    Class<?> paramType;
    for (int i = 0; i < paramTypes.length; i++) {
      paramType = paramTypes[i];
      promArgs[i] = getPromotionArg(arguments, paramType);
    }
    promArgs[promArgs.length - 1] = getCurrentUser();
    return promArgs;
  }

  private Object getPromotionArg(Object[] arguments, Class<?> paramType) {
    for (final Object argument : arguments) {
      if (argument != null && (argument.getClass().equals(paramType)
          || paramType.isAssignableFrom(argument.getClass()))) {
        return argument;
      }
    }
    return null;
  }

  private User getCurrentUser() {
    final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

    if (auth != null) {
      final Object principal = auth.getPrincipal();
      if (principal instanceof User) {
        return (User) principal;
      }
    }

    return new User();
  }

}
