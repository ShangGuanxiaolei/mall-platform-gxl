package com.xquark.promotion;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * An annotation on the method level to activate the whole promotion operation, see {@link
 * PromotionInterceptor} for more information
 *
 * @author: chenxi
 */

@Documented
@Inherited
@Target(METHOD)
@Retention(RUNTIME)
public @interface PromotionMethod {

  public PromotionOpType type();

  public InterceptorWay way();
}
