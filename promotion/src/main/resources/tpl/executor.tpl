package com.xquark.promotion.custom.c@customerId@;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.promotion.PromotionExecutor;

class CustomPromotionExecutor implements PromotionExecutor {

	@Override
	public void processShop(Shop shop) throws Exception {
		// FIXME bulletin field?
		#shop#
		shop.bulletin = "全场8折";
	}

	@Override
	public void processProduct(Product product) throws Exception {
		// TODO Auto-generated method stub
		product.description = product.description + "本产品95折";
	}

	@Override
	public void processOrder(Order order, List<OrderItem> orderItems)
			throws Exception {
		if (order.totalFee > 100)
			order.totalFee -= 10;
	}
	
}