// 满X元包邮  free logistics fee
if (order.totalFee > @value) {
	order.totalFee -= order.logisticsFee;
	order.logisticsFee = 0;
}

// 支持全场满送 FIXME use observer pattern to send an event?
if (order.totalFee > @value) {
   OrderItem orderItem = new OrderItem();
   ProductService productService = SpringContextUtil.getBean("productService");
   ProductSkuVO product = productService.load(@pid, @sid); //productService.load(cartItem.getProductId(), cartItem.getSkuId());
   orderItem.setProductId(product.getId());
   orderItem.setProductName(product.getName());
   orderItem.setProductImg(product.getImg());
   orderItem.setPrice(0); // free
   orderItem.setSkuId(product.getSku().getId());
   orderItem.setSkuStr(product.getSku().getSpec());
   orderItem.setAmount(@num);
   orderItems.add(orderItem);
}

// 支持指定商品满送 FIXME use observer pattern to send an event?
BigDecimal price = 0;
for (OrderItem orderItem : orderItems) {
	if (orderItem.productId == @prodId) {
		price += orderItem.price;
	}
}
if (price > @value) {
   OrderItem orderItem = new OrderItem();
   ProductService productService = SpringContextUtil.getBean("productService");
   ProductSkuVO product = productService.load(@pid, @sid); //productService.load(cartItem.getProductId(), cartItem.getSkuId());
   orderItem.setProductId(product.getId());
   orderItem.setProductName(product.getName());
   orderItem.setProductImg(product.getImg());
   orderItem.setPrice(0); // free
   orderItem.setSkuId(product.getSku().getId());
   orderItem.setSkuStr(product.getSku().getSpec());
   orderItem.setAmount(@num);
   orderItems.add(orderItem);
}

// TODO 在支付时用户可选择是否需要赠送商品 FIXME use observer pattern to send an event?

// 支持全场满减
if (order.totalFee > @value) {
	order.discountFee += @money;
	order.totalFee -= @money;
}

// 支持指定商品满减
BigDecimal price = 0;
for (OrderItem orderItem : orderItems) {
	if (orderItem.productId == @prodId) {
		price += orderItem.price;
	}
}
if (price > @value) {
	order.discountFee += @money;
	order.totalFee -= @money;
}

// TODO 在支付时自动满减

// 支持全场满N件打折
int amount = 0;
for (OrderItem orderItem : orderItems) {
	amount += orderItem.amount;
	if (amount > @value)
		break;
}
if (amount > @value) {
	BigDecimal discount = order.totalFee * (1-@discountRatio);
	order.discountFee += discount;
	order.totalFee -= discount;
}

// 支持指定商品满N件打折
int amount = 0;
for (OrderItem orderItem : orderItems) {
	if (orderItem.productId == @prodId) {
		amount += orderItem.amount;
		if (amount > @value)
			break;
	}
}
if (amount > @value) {
	BigDecimal discount = order.totalFee * (1-@discountRatio);
	order.discountFee += discount;
	order.totalFee -= discount;
}

// TODO 在支付时显示折后价格

// 支持全场满N件减
int amount = 0;
for (OrderItem orderItem : orderItems) {
	amount += orderItem.amount;
	if (amount > @value)
		break;
}
if (amount > @value) {
	order.discountFee += @discount;
	order.totalFee -= @discount;
}

// 支持指定商品满N件减
int amount = 0;
for (OrderItem orderItem : orderItems) {
	if (orderItem.productId == @prodId) {
		amount += orderItem.amount;
		if (amount > @value)
			break;
	}
}
if (amount > @value) {
	order.discountFee += @discount;
	order.totalFee -= @discount;
}

// TODO 在支付时显示减后价格

// 按用户等级、新注册、首次购买、用户生日、好友邀请等条件定义用户可享受的优惠