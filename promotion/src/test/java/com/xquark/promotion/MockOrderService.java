package com.xquark.promotion;

import java.util.List;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;

/**
 * @author: chenxi
 */

public class MockOrderService {

  @PromotionMethod(type = PromotionOpType.ORDER, way = InterceptorWay.BEFORE)
  public void printOrder(Order order, List<OrderItem> orderItems) {
    System.out.println("order.totalFee: " + order.getTotalFee());
  }
}
