package com.xquark.promotion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.DalConfig;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;

/**
 * @author: chenxi
 */

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DalConfig.class, ApplicationConfig.class, PromotionConfig.class})
public class MockOrderServiceTest {

  @Autowired
  private MockOrderService mockOrderService;

  @Test
  public void testShop() {
    final Order order = new Order();
    order.setSellerId("1r5xn5m1");
    order.setShopId("f75d");
    order.setTotalFee(new BigDecimal(110));
    order.setLogisticsFee(new BigDecimal(10));
    final OrderItem item = new OrderItem();
    item.setPrice(new BigDecimal(100));
    item.setProductId("lv8p");
    final List<OrderItem> items = new ArrayList<OrderItem>();
    items.add(item);
    mockOrderService.printOrder(order, items);
  }

}
