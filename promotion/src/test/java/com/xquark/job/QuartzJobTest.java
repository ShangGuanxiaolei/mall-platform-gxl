package com.xquark.job;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.inject.Injector;
import com.vdlm.common.job.client.JobSchedulerClient;
import com.vdlm.common.job.shared.Job;
import com.vdlm.common.job.shared.JobStatus;
import com.vdlm.common.jobserver.daemon.JobSchedulerDaemon;
import com.xquark.config.ApplicationConfig;
import com.xquark.config.DalConfig;
import com.xquark.promotion.PromotionConfig;

/**
 * @author: chenxi
 */

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DalConfig.class, ApplicationConfig.class, PromotionConfig.class})
public class QuartzJobTest {

//	@Autowired
//	private JobSchedulerClient client;

  @Autowired
  private Injector injector;

  @Test
  public void testJob() {
    injector.getInstance(JobSchedulerDaemon.class).start();
    final JobSchedulerClient client = injector.getInstance(JobSchedulerClient.class);
    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, 2);
    Job job = new Job("testRemoteMethod2", "user1").withGroup("test1")
        .withArgument(new Object[]{"normal"})
        .withRepeatForever()
        .withRepeatInterval(60, TimeUnit.SECONDS).withEndTime(calendar.getTime());
    JobStatus status = client.scheduleJob(job);
    System.out.println(status);

    // pause the previous job
//        job = new Job("testRemoteMethod2", "user1").withGroup("test1");
//        status = service.pauseJob(job);
//        System.out.println(status);

    // reschedule the previous job
    job = new Job("testRemoteMethod2", "user1").withGroup("test1")
        .withArgument(new Object[]{"reschedule"})
        .withRepeatForever()
        .withRepeatInterval(120, TimeUnit.SECONDS);
    status = client.rescheduleJob(job);
    System.out.println(status);

    // resume the previous job
    job = new Job("testRemoteMethod2", "user1").withGroup("test1");
    status = client.resumeJob(job);
    System.out.println(status);

    // delete the previous job
    job = new Job("testRemoteMethod2", "user1").withGroup("test1");
    status = client.deleteJob(job);
    System.out.println(status);

    // schedule the another job
    job = new Job("testRemoteMethod3", "user1").withGroup("test2")
        .withArgument(new Object[]{"String Parameter"})
        .withRepeatForever()
        .withRepeatInterval(2, TimeUnit.MINUTES);
    status = client.scheduleJob(job);
    System.out.println(status);

    // delete the previous job
    job = new Job("testRemoteMethod3", "user1").withGroup("test2");
    status = client.deleteJob(job);
    System.out.println(status);
  }
}
