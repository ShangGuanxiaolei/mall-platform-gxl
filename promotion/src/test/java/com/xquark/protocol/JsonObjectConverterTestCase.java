package com.xquark.protocol;

import java.util.List;

import org.codehaus.jackson.map.PropertyNamingStrategy;

import com.google.common.collect.Lists;
import com.vdlm.common.protocol.JsonObjectConverter;
import com.vdlm.common.protocol.ObjectConverter;
import com.xquark.promotion.classic.action.data.AttachProductData;
import com.xquark.promotion.classic.action.data.AttachProductData.SkuData;

/**
 * @author: chenxi
 */

public class JsonObjectConverterTestCase {

  public static void main(String[] args) throws Exception {
    final ObjectConverter converter = new JsonObjectConverter(
        PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
    AttachProductData data = new AttachProductData();
    final List<SkuData> list = Lists.newArrayList();
    SkuData pia = new SkuData();
    pia.setPid("abcd");
    pia.setAmount(2);
    list.add(pia);
    pia = new SkuData();
    pia.setPid("xyz");
    pia.setAmount(5);
    list.add(pia);
    data.setList(list);
    final String dataStr = converter.toString(data);
    System.out.println(dataStr);
    data = converter.fromString(dataStr, AttachProductData.class);
    System.out.println(data);
  }

}
