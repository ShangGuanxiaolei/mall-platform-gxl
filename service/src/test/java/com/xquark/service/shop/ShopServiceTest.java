
package com.xquark.service.shop;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.ShopStatus;
import com.xquark.service.test.MockShopServiceTest;

public class ShopServiceTest extends MockShopServiceTest<Shop, ShopService> {

  @Override
  public void doInit() {
    super.doInit();
  }

  public ShopServiceTest() {
    super(Shop.class);
  }

  private String shopName = "Test Test Shop 1";

  @Autowired
  private ShopService shopService;

  @Autowired
  private ZoneMapper zoneMapper;

  @Test
  public void testCreateAndLoad() {
    Shop shop = mockShop(seller.getId(), shopName);
    shop = shopService.create(shop);

    Assert.assertNotNull(shop.getId());

    Shop target = shopService.load(shop.getId());
    Assert.assertEquals(ShopStatus.ACTIVE, target.getStatus());
    Assert.assertEquals(shopName, target.getName());
  }

  @Test
  public void testFindByUser() {
    Shop shop = mockShop(seller.getId(), shopName);

    this.setAuthUser(seller);
    shop = shopService.create(shop);

    Shop newShop = shopService.findByUser(shop.getOwnerId());
    Assert.assertNotNull(newShop);
    Assert.assertEquals(shopName, newShop.getName());
  }

  @Test
  public void testFindByName() {
    Shop shop = mockShop(seller.getId(), shopName);
    this.setAuthUser(seller);
    Shop chkShop = shopService.findByName(shop.getName());
    Assert.assertNull(chkShop);
    shop = shopService.create(shop);
    chkShop = shopService.findByName(shop.getName());
    Assert.assertNotNull(chkShop);
  }

  @Test
  public void testUpdate() {
    Shop shop = mockShop(seller.getId(), shopName);

    shop = shopService.create(shop);
    String bulletin = "Bulletin " + new Date().getTime();
    shop.setBulletin(bulletin);
    shopService.update(shop);
    Shop newShop = shopService.load(shop.getId());
    Assert.assertEquals(newShop.getBulletin(), bulletin);
  }

  @Test
  public void testOpenDanbao() {
    Shop shop = mockShop(seller.getId(), shopName);
    shop = shopService.create(shop);

    this.setAuthUser(seller);
    Assert.assertTrue(shopService.openDanbao());
  }

  @Test
  public void testCountByShop() {
    Shop shop = mockShop(seller.getId(), shopName);

    shopService.create(shop);

    Assert.assertTrue(shopService.countByShop() > 0);
  }

  @Test
  public void testListAll() {
    Shop shop = mockShop(seller.getId(), shopName);

    shopService.create(shop);
    Pageable page = new PageRequest(0, 20);
    Assert.assertTrue(shopService.listAll(page).size() > 0);
  }

  protected Shop newEntityInstance() {
    return mockShop(seller.getId(), shopName);
  }

  /**
   * 重写父类的方法，不存数据库返回
   */
  protected Shop mockShop(String userId, String shopName) {
    this.setAuthUser(seller);

    Shop shop = new Shop();
    shop.setOwnerId(userId);
    shop.setName(shopName);
    shop.setStatus(ShopStatus.ACTIVE);
    shop.setImg("");
    return shop;
  }

  @Test
  public void testGetRnd() {
    this.setAuthUser(seller);

    final long rnd = shopService.getRnd();

    System.out.println("rnd=" + rnd);

    Assert.assertTrue(rnd > 0);
  }

  @Test
  public void testLoadStatistics() {
    Map<String, Object> statisticses = shopService.loadStatistics();
    for (String statistics : statisticses.keySet()) {
      System.err.println(statistics + " = " + statisticses.get(statistics));
    }
  }


  private Zone getZone4Cal(String zoneId) {
    if (StringUtils.isBlank(zoneId)) {
      return null;
    }

    Zone zone = zoneMapper.selectByPrimaryKey(zoneId);
    if (zone.getId().equals("1")) {
      return null; // TODO: 不能直接选中国, 国际版怎么办?
    }

    if (zone != null && zone.getParentId().equals("1")) {  // 中国省一级
      return zone;
    } else if (zone != null && zone.getParentId().equals("0")) {  // 除中国以外的其它一级(国家)
      return zone;
    } else if (zone != null) { // 排除中国, (中国省一级以内各地区)
      Zone pZone = zoneMapper.findParent(zone.getId());
      if (pZone != null) {
        return getZone4Cal(pZone.getId());
      }
    }

    return null;
  }

  @Test
  public void testGetZone4Calc() {
    String[] zones = {"1", "4", "23", "36", "77", "87", "90", "111", "3512", "1499", "3294", "xxx",
        "3323", "3513", "3526", "3530", "3514", "3515"};

    System.out.println("------------------------------------------");
    for (String zoneId : zones) {
      Zone zone = null;
      try {
        zone = getZone4Cal(zoneId);
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (zone != null) {
        System.out.println(zone.getId() + "->" + zone.getName() + "->" + zone.getZoneTag());
        System.out.println("#####");
        //Assert.assertTrue(zone.getParentId().equals("1") && !zone.getId().equals("1"));
      } else {
        System.out.println("zoneId: " + zoneId + " is invalid for postage calc");
      }
    }
    System.out.println("------------------------------------------");

  }


  @Override
  public ShopService getService() {
    return this.shopService;
  }
}