package com.xquark.service.shop;

import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Shop;
import com.xquark.dal.model.ShopAccess;
import com.xquark.dal.status.ShopStatus;
import com.xquark.service.test.MockServiceTest;

public class ShopAccessServiceTest extends MockServiceTest {

  @Autowired
  private ShopAccessService shopAccessService;

  @Before
  public void init() {
    super.init();
  }

  @Test
  public void testInsert() {
    Shop shop = mockShop(seller.getId(), "Test Shop22");
    shop = shopService.create(shop);
    Assert.assertNotNull(shop.getId());
    Calendar cal = Calendar.getInstance();

    ShopAccess shopAccess = new ShopAccess();
    shopAccess.setPv(1);
    shopAccess.setShopId(shop.getId());
    shopAccess.setDate(cal.getTime());
    shopAccess.setHour(1);
    shopAccess.setUserId(seller.getId());
    int rc = shopAccessService.save(shopAccess);
    Assert.assertTrue(rc == 1);

    shopAccess = new ShopAccess();
    shopAccess.setPv(1);
    shopAccess.setShopId(shop.getId());
    shopAccess.setDate(cal.getTime());
    shopAccess.setHour(1);
    shopAccess.setUserId(seller.getId());
    rc = shopAccessService.save(shopAccess);
    Assert.assertTrue(rc == 1);

    List<ShopAccess> shopAccesses = shopAccessService
        .findShopAccessByShopId(shop.getId(), cal.getTime());
    Assert.assertEquals(shopAccesses.size(), 1);
    Assert.assertTrue(shopAccesses.get(0).getPv() == 2);
  }


  /**
   * 重写父类的方法，不存数据库返回
   */
  protected Shop mockShop(String userId, String shopName) {
    this.setAuthUser(seller);

    Shop shop = new Shop();
    shop.setOwnerId(userId);
    shop.setName(shopName);
    shop.setStatus(ShopStatus.ACTIVE);
    shop.setImg("");
    return shop;
  }
}