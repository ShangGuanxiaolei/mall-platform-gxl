package com.xquark.service.file;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Image;

public class ImageServiceTest {

  @Autowired
  private ImageService imageService;

  //@Transactional(propagation=Propagation.NOT_SUPPORTED)


  public void load() {
    Image image = imageService.load(7135 + "_80_80");
    Assert.assertNotNull(image);
  }

  public void delete() {
    int rc = imageService.delete("7258");
    Assert.assertTrue(rc > 0);
  }

  /**
   * 获得指定文件的byte数组
   */
  public static byte[] getBytes(File file) {
    byte[] buffer = null;
    try {

      FileInputStream fis = new FileInputStream(file);
      ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
      byte[] b = new byte[1000];
      int n;
      while ((n = fis.read(b)) != -1) {
        bos.write(b, 0, n);
      }
      fis.close();
      bos.close();
      buffer = bos.toByteArray();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return buffer;
  }

//	@Override
//	public void setService(ImageService svc) {
//		imageService = (ImageService) svc;
//		
//	}
//
//	@Override
//	public ImageService getService() {
//		return imageService;
//	}

  public Image newEntityInstance() {
    Image image = new Image();
    image.setName("test_image");
    return image;
  }
}
