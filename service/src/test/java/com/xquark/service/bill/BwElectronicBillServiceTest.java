package com.xquark.service.bill;

import com.xquark.dal.model.ElectronicBillResult;
import com.xquark.dal.model.PersonalBill;
import com.xquark.dal.type.BillType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.service.bill.impl.bw.BWElectronicBillService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import com.xquark.service.test.BaseServiceTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BwElectronicBillServiceTest extends BaseServiceTest<BWElectronicBillService> {

    private BWElectronicBillService billService;

    private OrderService orderService;

  private static String orderId = "1whe6";

    private static final Logger LOG= LoggerFactory.getLogger(BwElectronicBillServiceTest.class);

  private MainOrderService mainOrderService;

  @Autowired
  public void setMainOrderService(MainOrderService mainOrderService) {
    this.mainOrderService = mainOrderService;
  }


    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setBWElectronicBillService(BWElectronicBillService billService) {
        this.billService = billService;
    }

    @Test
    public void testPersonalBill() throws Exception {
        PersonalBill pb = new PersonalBill();
        pb.setType(BillType.ELECTRONIC);
        pb.setReceiveMail("jitrejake@163.com");
        pb.setReceivePhone("18358733695");
        pb.setOrderId(orderId);
      MainOrderVO vo = mainOrderService.loadVO(orderId);
        ElectronicBillResult result = billService.makePersonalBill(pb, vo);
        Assert.assertNotNull(result);
        LOG.info(result.toString());
    }


    @Override
    public BWElectronicBillService getService() {
        return this.billService;
    }
}
