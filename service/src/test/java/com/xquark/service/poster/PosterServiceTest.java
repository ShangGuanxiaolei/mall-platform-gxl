package com.xquark.service.poster;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Poster;
import com.xquark.service.test.MockServiceTest;

public class PosterServiceTest extends MockServiceTest {

  @Autowired
  private PosterService posterService;

  @Before
  public void init() {
    super.init();
    this.setAuthUser(seller);
  }

  @Test
  public void test() {
    String tag = "首页海报1";

    List<Poster> posters = posterService.listAll(null);
    for (Poster poster : posters) {
      posterService.savePosterTag(poster.getId(), tag);
    }

    List<Poster> posters2 = posterService.listByTag(tag);
    Assert.assertEquals(posters.size(), posters2.size());
  }

}
