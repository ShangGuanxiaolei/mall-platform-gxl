package com.xquark.service.test;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.DalConfig;
import com.xquark.config.ServiceTestConfig;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.User;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.address.AddressService;
import com.xquark.service.cart.CartService;
import com.xquark.service.order.OrderService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.utils.MD5Util;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DalConfig.class, ServiceTestConfig.class, ApplicationConfig.class})
public abstract class MockServiceTest {

  protected User seller;

  protected User buyer;

  protected User unioner;

  protected Shop shop;

  protected ProductVO product;

  @Autowired
  protected OrderService orderService;

  @Autowired
  protected UserService userService;

  @Autowired
  protected CartService cartService;

  @Autowired
  protected ShopService shopService;

  @Autowired
  protected ProductService productService;
  @Autowired
  protected AddressService addressService;


  @Before
  public void init() {
    this.seller = mockUser("136068062672");
    // this.seller = mockUser("15869112600");
    this.buyer = mockUser("188065209391");
    this.unioner = mockUser("159884756311");
  }

  protected User mockUser(String mobile) {
    return userService.register(mobile, MD5Util.MD5Encode("123456", null));
  }

  protected Shop mockShop(String userId, String shopName) {
    Shop s = shopService.findByUser(userId);
    if (s != null) {
      return s;
    } else {
      this.setAuthUser(seller);
      Shop shop = new Shop();
      shop.setOwnerId(userId);
      shop.setName(shopName);
      shop.setImg("");
      shop.setDanbao(false);
      shop.setCommisionRate(BigDecimal.valueOf(0.1));
      shop = shopService.create(shop);
      return shop;
    }
  }

  protected ProductVO mockProduct(String shopId, String sellerId) {
    this.setAuthUser(seller);

    Product product = new Product();
    product.setName("Test Product");
    product.setImg("md_6766");
    product.setUserId(sellerId);
    product.setShopId(shopId);
    product.setStatus(ProductStatus.ONSALE);
    product.setDelayed(0);
    product.setDescription("description");

    product.setDelayed(0);
    product.setDelayAt(0);

    List<Sku> skus = new ArrayList<Sku>();
    Sku sku = new Sku();
    sku.setAmount(10);
    sku.setPrice(BigDecimal.valueOf(12.34));
    sku.setSpec("test1");
    skus.add(sku);

    sku = new Sku();
    sku.setAmount(5);
    sku.setPrice(BigDecimal.valueOf(34.56));
    sku.setSpec("test2");
    skus.add(sku);

    productService.create(product, skus, null, null, null, null);

    return productService.load(product.getId());
  }

  protected Order mockDirectOrder() {
    return mockOrder(OrderType.DIRECT);
  }

  protected Order mockDirectUnionOrder() {
    return mockOrder(OrderType.DIRECT);
  }

  protected Order mockDanbaoOrder() {
    return mockOrder(OrderType.DANBAO);
  }

  private Order mockOrder(OrderType type) {
    this.setAuthUser(seller);
    if (type.equals(OrderType.DIRECT)) {
      shopService.closeDanbao();
    } else {
      shopService.openDanbao();
    }

    //Shop s = shopService.mine();

    this.setAuthUser(buyer);
    Order order = new Order();
    order.setBuyerId(buyer.getId());
    order.setShopId(shop.getId());
    order.setSellerId(seller.getId());
    order.setPayType(PaymentMode.ALIPAY); // can not be seems does not make sense
    order.setTotalFee(BigDecimal.valueOf(43.21));

    OrderAddress oa = mockOrderAddress();

//		List<OrderItem> orderItems = new ArrayList<OrderItem>();
//		OrderItem orderItem = new OrderItem();
//		orderItem.setProductId(product.getId());
//		orderItem.setSkuId(product.getSkus().get(0).getId());
//		orderItem.setProductName(product.getName());
//		orderItem.setSkuStr(product.getSkus().get(0).getSpec());
//		orderItem.setProductImg(product.getImg());
//		orderItem.setPrice(product.getSkus().get(0).getPrice());
//		orderItem.setAmount(1);
//		orderItems.add(orderItem);

    String skuId = product.getSkus().get(0).getId();

    cartService.addToCart(buyer.getId(), skuId, 1, "", "");

    // order = orderService.submitBySkuId(skuId, oa, PaymentMode.ALIPAY, "", unioner.getId(), null, null, false);
    order = orderService.submitBySkuId(skuId, oa, "", "", null, false);
    return order;
  }

  protected OrderAddress mockOrderAddress() {
    OrderAddress oa = new OrderAddress();
    oa.setConsignee("陈阿隆");
    oa.setPhone("13606806267");
    oa.setStreet("西湖区文一路");
    oa.setZoneId("2");
    return oa;
  }

  protected void setAuthUser(User user) {
    Authentication auth = new UsernamePasswordAuthenticationToken(user, null);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }


}
