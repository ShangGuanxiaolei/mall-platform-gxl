package com.xquark.service.test;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.CachingConfig;
import com.xquark.config.DalConfig;
import com.xquark.config.ServiceCommonConfig;
import com.xquark.config.ServiceTestConfig;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.BaseService;
import com.xquark.thirds.config.ThirdConfig;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DalConfig.class, ServiceTestConfig.class, ApplicationConfig.class,
    ThirdConfig.class,CachingConfig.class,ServiceCommonConfig.class})
public abstract class BaseServiceTest<S extends BaseService> {

  @Before
  public final void init() {
    User user = new User();
    user.setId(IdTypeHandler.encode(1L));
    user.setLoginname("18888888888");
    user.setShopId(IdTypeHandler.encode(1L));

    mockCurrentUser(user);
    // 支持其他service注入式mock
    MockitoAnnotations.initMocks(this);

    doInit();
  }

  protected void doInit() {
  }

  /**
   * mock当前登录用户
   */
  protected void mockCurrentUser(User user) {
    Authentication auth = new UsernamePasswordAuthenticationToken(user, null);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  public abstract S getService();
}
