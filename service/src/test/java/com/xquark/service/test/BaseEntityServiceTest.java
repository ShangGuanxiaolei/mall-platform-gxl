package com.xquark.service.test;

import org.junit.Assert;

import com.xquark.dal.Archivable;
import com.xquark.dal.BaseEntity;
import com.xquark.service.ArchivableEntityService;
import com.xquark.service.BaseEntityService;

public abstract class BaseEntityServiceTest<E extends BaseEntity, S extends BaseEntityService<E>> extends
    BaseServiceTest<S> {

  final Class<E> clz;

  protected BaseEntityServiceTest(Class<E> clz) {
    this.clz = clz;
  }

  /**
   * 部分业务没有暴露insert或者archive的接口，具体由对应的业务的servcie test中去实现
   */
//	@Test
  public void testLoadAndArchive() throws Exception {
    E e = newEntityInstance();

    // insert & load
    Assert.assertTrue(getService().insert(e) > 0);
    Assert.assertNotNull(e.getId());
    Assert.assertNotNull(getService().load(e.getId()));

    if (!(e instanceof Archivable)) {
      return;
    }

    Assert.assertTrue("Archivable实体的service，必须实现接口" + ArchivableEntityService.class.getSimpleName(),
        getService() instanceof ArchivableEntityService);

    @SuppressWarnings({"unchecked", "rawtypes"})
    ArchivableEntityService<Archivable> svc = (ArchivableEntityService) getService();

    // archive
    Assert.assertTrue(svc.delete(e.getId()) > 0);
    Assert.assertNull(svc.load(e.getId()));

    // 取消archive
    Assert.assertTrue(svc.undelete(e.getId()) > 0);
    Assert.assertNotNull(svc.load(e.getId()));
  }

  protected E newEntityInstance() throws Exception {
    if (clz != null) {
      return clz.newInstance();
    }
    return null;
  }
}
