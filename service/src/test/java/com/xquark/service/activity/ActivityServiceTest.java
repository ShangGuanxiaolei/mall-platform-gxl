package com.xquark.service.activity;

import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Activity;
import com.xquark.dal.model.CampaignProduct;
import com.xquark.dal.model.PreferentialType;
import com.xquark.dal.vo.ActivityEX;
import com.xquark.service.test.BaseEntityServiceTest;


public class ActivityServiceTest extends BaseEntityServiceTest<Activity, ActivityService> {

  @Autowired
  private ActivityService activityService;

  public ActivityServiceTest() {
    super(Activity.class);
  }

  protected ActivityEX newEntityInstance() {
    ActivityEX activity = new ActivityEX();
    activity.setName("双十一促销活动");

    Calendar c = Calendar.getInstance();
    c.set(2014, 10, 20);
    activity.setStartTime(c.getTime());
    c.set(2014, 10, 22);
    activity.setEndTime(c.getTime());
    activity.setPreferentialType(PreferentialType.ACTIVITY_PRODUCT_DISCOUNT);
    activity.setDiscount(new Float(20));
    return activity;
  }

  @Test
  public void testSave() {
    Activity activity = newEntityInstance();
    activity = activityService.saveActivity(activity);
    Assert.assertNotNull(activity.getId());
  }

  @Test
  public void testListCampaignProducts() {
    Activity activity = newEntityInstance();
    activity = activityService.saveActivity(activity);

    List<CampaignProduct> list = activityService.listCampaignProducts(activity.getId());
    Assert.assertTrue(list.size() > 0);
  }

  @Override
  public ActivityService getService() {
    return activityService;
  }

}
