package com.xquark.service.feedback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Feedback;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.FeedbackStatus;
import com.xquark.dal.vo.FeedbackVO;
import com.xquark.service.test.BaseServiceTest;

public class FeedbackServiceTest extends BaseServiceTest<FeedbackService> {

  @Autowired
  private FeedbackService feedbackService;

  @Test
  public void testInsert() {
    Feedback feedback = new Feedback();
    feedback.setContact("13888888888");
    feedback.setContent("测试的意见反馈");
    feedback.setStatus(FeedbackStatus.NEW);
    int count = feedbackService.insert(feedback);
    Assert.assertTrue(count == 1);
  }

  @Test
  public void testList() {
    Pageable page = new PageRequest(0, 10);
    Map<String, Object> params = new HashMap<String, Object>();
    List<FeedbackVO> feedbacks = feedbackService.listFeedbacksByAdmin(params, page);
    Long count = feedbackService.countFeedbacksByAdmin(params);
    Assert.assertTrue(feedbacks.size() > 0 && count.longValue() > 0);
  }

  @Test
  public void testClosed() {
    int count = feedbackService.updateForClosed(IdTypeHandler.encode(2830L), "回复的内容");
    Assert.assertTrue(count == 1);
  }

  @Override
  public FeedbackService getService() {
    return this.feedbackService;
  }
}
