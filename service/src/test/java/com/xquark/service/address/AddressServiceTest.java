package com.xquark.service.address;

import com.xquark.dal.model.Address;
import com.xquark.service.test.MockServiceTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AddressServiceTest extends MockServiceTest {

  @Autowired
  private AddressService addressService;

  @Before
  public void init() {
    super.init();
    this.setAuthUser(buyer);
  }

  @Test
  public void testSaveUserAddress() {
    Address address = mockAddress();
    address = addressService.saveUserAddress(address, true);
    Assert.assertNotNull(address.getId());
  }

//	@Test
//	public void testUpdateUserAddress(){
//		Address address = mockAddress();
//		address.setId("1");
//		String street = "浙江省杭州市天目山路145号 " + new Date().getTime();
//		address.setStreet(street);
//		address = addressService.updateUserAddress(address);
//		Assert.assertEquals(address.getStreet(), street);
//		
//	}

  @Test
  public void testListUserAddresses() {
    List<AddressVO> address = addressService.listUserAddressesVo();
    Assert.assertNotNull(address);
  }

  @Test
  public void testLoadUserAddress() {
    this.setAuthUser(buyer);
    Address address = mockAddress();
    address = addressService.saveUserAddress(address, true);

    this.setAuthUser(buyer);
    address = addressService.loadUserAddress(address.getId());
    Assert.assertEquals(address.getUserId(), buyer.getId());
  }

  @Test
  public void testArchiveAddress() {
    Address address = new Address();
    address.setId("1");
    int updates = addressService.archiveAddress(address.getId(), true);
    Assert.assertEquals(updates, 1);
  }

  protected Address mockAddress() {
    Address address = new Address();
    address.setUserId(buyer.getId());
    address.setConsignee("znu991");
    address.setPhone("1388888888");
    address.setStreet("浙江省杭州市天目山路145号");
    address.setZoneId("123");
    address.setCommon(false);
    return address;
  }

}
