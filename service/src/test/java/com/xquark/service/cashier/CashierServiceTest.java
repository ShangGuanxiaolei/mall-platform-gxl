package com.xquark.service.cashier;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.DalConfig;
import com.xquark.config.ServiceTestConfig;
import com.xquark.thirds.config.ThirdConfig;

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DalConfig.class, ServiceTestConfig.class, ApplicationConfig.class,
    ThirdConfig.class})
public class CashierServiceTest {

  @Autowired
  CashierService cashierService;

  @Test
  public void loadPaidFee() {
    String bizNo = "SO14092220395109900011";
    BigDecimal fee = cashierService.loadPaidFee(bizNo);
    System.out.println(fee);
  }

}
