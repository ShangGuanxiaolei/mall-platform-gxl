package com.xquark.service.outpay;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.type.PaymentMode;
import com.xquark.service.payBank.PayBankService;
import com.xquark.service.test.BaseServiceTest;

public class UmPaymentTest extends BaseServiceTest<ThirdPartyPayment> {

  @Autowired
  private ThirdPartyPayment umPayment;

  @Autowired
  private PayBankService payBankService;

  @Test
  public void testLoadBankList() {
    List<String> banks = umPayment.loadBankList();
    System.out.println(banks.toString());
    Assert.assertTrue(banks.size() > 0);
  }

  @Test
  public void testUmpayTask() {
    // TODO
    List<String> result = umPayment.loadBankList();
    if (result == null || result.size() == 0) {
      System.out.println("返回U付支持的银行列表异常");
      return;
    }
    int rec = payBankService.updateBankStatusFalse(result, PaymentMode.UMPAY);
    System.out.println("更新的不支持银行数:" + rec);
    List<String> noSupportBanks = payBankService.noSupportBank(result, PaymentMode.UMPAY);
    ;
    System.out.println("未支持的银行列表：" + noSupportBanks.toString());
  }

  @Test
  public void testPayRequest() throws Exception {
    // TODO
//		PaymentRequestVO request = new PaymentRequestVO();
//		request.setTradeNo(UniqueNoUtils.next(UniqueNoType.ESO));
//		request.setTotalFee(new BigDecimal(0.01));
//		umPayment.payRequest(null, null, request);
  }

  @Override
  public ThirdPartyPayment getService() {
    return this.umPayment;
  }
}
