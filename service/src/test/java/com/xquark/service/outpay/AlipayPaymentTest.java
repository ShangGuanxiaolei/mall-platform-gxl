package com.xquark.service.outpay;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.service.test.BaseServiceTest;

public class AlipayPaymentTest extends BaseServiceTest<ThirdPartyPayment> {

  @Autowired
  private ThirdPartyPayment aliPayment;

  @Test
  public void closeTrade() {
    String orderNo = "SO141226113336198002";
    aliPayment.closeTrade(orderNo);
  }

  @Override
  public ThirdPartyPayment getService() {
    return this.aliPayment;
  }
}
