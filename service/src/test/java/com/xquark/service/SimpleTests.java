package com.xquark.service;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.xquark.service.sync.SyncContent;
import com.xquark.service.sync.internal.SyncTaskBean;
import com.xquark.service.syncevent.ProdItem;
import com.xquark.service.syncevent.SyncEventType;


public class SimpleTests {

  ApplicationContext ctx;

  @Before
  public void before() {
//		 ctx = new ClassPathXmlApplicationContext("prod-service-sync.xml");
  }

  @SuppressWarnings("serial")
  @Test
  public void testMain() {
    final ProdItem item = new ProdItem();
    item.setEventCode((byte) 5);
    item.setImage(
        "http://img-prod.kkkd.com/Fom6ipEk66Xv9IOAE8-P7yNoAoeb?imageView2/2/w/480/q/100;");
    item.setProductId("3ig4slre");
    item.setShopId("3ibs2fay");
    item.setTimestamp(new Date(1414412470137l));
    item.setTitle("定制 宇航员 空气布一字领廓形大卫衣 | 番塔塔独立设计");
    item.setUrl("http://www.kkkd.com/p/3ig4slre?partner=xiangqu&union_id=1s47poz5");
    item.setPrice(BigDecimal.valueOf(173));

    final SyncTaskBean bean = new SyncTaskBean((byte) 2, SyncEventType.STATUS_ONSALE,
        new SyncContent() {
          @Override
          public byte[] toJSONBytes() {
            return JSONObject.toJSONBytes(item);
          }
        });

    byte[] data = bean.getData();

    SyncTaskBean stb = SyncTaskBean.parse(data);

    System.out.println(stb.getEvent().getGroup());
    System.out.println(stb.getEvent().getTopic());

    System.out.println(bean);
  }
}
