package com.xquark.service.deal;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import com.xquark.dal.model.Account;
import com.xquark.dal.model.Deal;
import com.xquark.dal.model.DealLog;
import com.xquark.dal.model.Shop;
import com.xquark.dal.status.DealStatus;
import com.xquark.dal.type.DealType;
import com.xquark.service.account.AccountService;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.test.MockServiceTest;

public class DealServiceTest extends MockServiceTest {

  @Autowired
  private DealService dealService;

  @Autowired
  private AccountService accountService;

  private Account accoutFrom;

  private Account accountTo;

  @Before
  public void init() {
    super.init();

    Shop shop = mockShop(seller.getId(), "Test Shop1");
    this.shop = shop;

    ProductVO product = mockProduct(shop.getId(), seller.getId());
    this.product = product;

    accoutFrom = accountService.loadByUserId(buyer.getId());
    accountTo = accountService.loadByUserId(seller.getId());
  }

  @Test
  public void testCreateDeal() {
    Deal deal = mockDeal();
    dealService.createDeal(deal);
    Assert.assertNotNull(deal.getId());
  }

  @Test
  public void testHalfDoDanbaoDeal() {
    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    Deal deal = mockDeal();
    deal.setDealType(DealType.DANBAO_PAY);
    dealService.createDeal(deal);

    dealService.halfDoDanbaoDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());
    Assert.assertEquals(DealStatus.IN_PROGRESS, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(logs.size(), 2);
  }

  @Test
  public void testDrawbackDanbaoDeal() {
    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    Deal deal = mockDeal();
    deal.setDealType(DealType.DANBAO_PAY);
    dealService.createDeal(deal);

    dealService.halfDoDanbaoDeal(deal.getId());
    dealService.drawbackDanbaoDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());

    Assert.assertEquals(DealStatus.CLOSED, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(logs.size(), 4);
  }

  @Test
  public void testFinishDanbaoDeal() {
    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    Deal deal = mockDeal();
    deal.setDealType(DealType.DANBAO_PAY);
    dealService.createDeal(deal);

    dealService.halfDoDanbaoDeal(deal.getId());
    dealService.finishDanbaoDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());

    Assert.assertEquals(DealStatus.SUCCESS, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(4, logs.size());

    Account account = accountService.load(accountTo.getId());

  }

  @Test
  public void testDepositDeal() {
    BigDecimal amount = new BigDecimal(100);
    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(amount);
    dealService.finishDeal(deposit.getId());

    Account account = accountService.load(accoutFrom.getId());

    Assert.assertTrue(amount.doubleValue() == account.getBalance().doubleValue());
  }

  @Test
  public void testFinishDeal() {
    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    Deal deal = mockDeal();
    dealService.createDeal(deal);

    dealService.finishDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());

    Assert.assertEquals(DealStatus.SUCCESS, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(logs.size(), 2);
  }

  @Test
  public void testCloseDeal() {
    Deal deal = mockDeal();
    dealService.createDeal(deal);

    dealService.closeDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());

    Assert.assertEquals(DealStatus.CLOSED, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(logs.size(), 0);
  }

  @Test
  public void testListDealByUserId() {

    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    Deal deal = mockDeal();
    dealService.createDeal(deal);

    dealService.finishDeal(deal.getId());
    Deal deal1 = dealService.loadDeal(deal.getId());

    Assert.assertEquals(DealStatus.SUCCESS, deal1.getStatus());

    List<DealLog> logs = dealService.listDealLogsByDeal(deal.getId());
    Assert.assertEquals(logs.size(), 2);

    dealService.listDealByUserId(accountTo.getUserId(), new PageRequest(0, 10));
  }

  protected Deal mockDeal() {
    Deal deal = new Deal();
    deal.setDealType(DealType.DIRECT_PAY);
    deal.setAccountFrom(accoutFrom.getId());
    deal.setAccountTo(accountTo.getId());
    deal.setFee(BigDecimal.valueOf(12.34));
    return deal;
  }
}
