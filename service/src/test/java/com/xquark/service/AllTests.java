package com.xquark.service;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.xquark.service.address.AddressServiceTest;
import com.xquark.service.app.AppVersionServiceTest;
import com.xquark.service.cart.CartServiceTest;
import com.xquark.service.deal.DealServiceTest;
import com.xquark.service.order.OrderAddressServiceTest;
import com.xquark.service.order.OrderServiceTest;
import com.xquark.service.product.ProductServiceTest;
import com.xquark.service.shop.ShopServiceTest;
import com.xquark.service.user.UserServiceTest;
import com.xquark.service.zone.ZoneServiceTest;

@RunWith(Suite.class)
@SuiteClasses({AppVersionServiceTest.class, ZoneServiceTest.class,
    UserServiceTest.class, AddressServiceTest.class, ShopServiceTest.class,
    ProductServiceTest.class,
    CartServiceTest.class, OrderServiceTest.class, OrderAddressServiceTest.class,
    DealServiceTest.class})
public class AllTests {

  public static Test suite() {
    TestSuite suite = new TestSuite("AllTests");
    // suite.addTest(AllTests.suite());
    return suite;
  }
}
