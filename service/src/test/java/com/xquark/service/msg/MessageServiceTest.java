package com.xquark.service.msg;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.UserMessage;
import com.xquark.dal.status.UserMessageStatus;
import com.xquark.service.msg.vo.UserMessageVO;
import com.xquark.service.test.MockServiceTest;

public class MessageServiceTest extends MockServiceTest {

  @Autowired
  private MessageService messageService;

  @Before
  public void init() {
    super.init();
  }

  @Test
  public void testSendSystemMessage() {
    String userId = seller.getId();
    String title = "测试标题";
    String content = "测试内容";
    UserMessage msg = messageService.sendSystemMessage(userId, title, content);
    Assert.assertNotNull(msg.getMsgId());
  }

  @Test
  public void testSendUserMessage() {
    String fromId = seller.getId();
    String toId = buyer.getId();
    String title = "测试标题";
    String content = "测试内容";

    UserMessage msg = messageService.sendUserMessage(fromId, toId, title, content, null);
    UserMessage msg1 = messageService.loadUserMessage(msg.getId());
    Assert.assertEquals(msg.getMsgId(), msg1.getMsgId());
  }

  @Test
  public void testRemoveUserMessage() {
    String fromId = seller.getId();
    String toId = buyer.getId();
    String title = "";
    String content = "";

    UserMessage msg = messageService.sendUserMessage(fromId, toId, title, content, null);
    messageService.removeUserMessage(msg.getId());
    UserMessage msg1 = messageService.loadUserMessage(msg.getId());
    Assert.assertEquals(msg.getMsgId(), msg1.getMsgId());
    Assert.assertEquals(UserMessageStatus.DELETED, msg1.getStatus());
  }

  @Test
  public void testLoadInboxMessages() {
    String fromId = seller.getId();
    String toId = buyer.getId();
    String title = "";
    String content = "";

    messageService.sendUserMessage(fromId, toId, title, content, null);

    this.setAuthUser(buyer);
    List<UserMessageVO> list = messageService.loadInboxMessages();
    Assert.assertEquals(1, list.size());
  }

  @Test
  public void testloadOutboxMessages() {
    String fromId = seller.getId();
    String toId = buyer.getId();
    String title = "";
    String content = "";

    messageService.sendUserMessage(fromId, toId, title, content, null);

    this.setAuthUser(seller);
    List<UserMessageVO> list = messageService.loadOutboxMessages();
    Assert.assertEquals(1, list.size());
  }
}
