package com.xquark.service.msg;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.SmsMessage;
import com.xquark.dal.status.SmsMessageStatus;
import com.xquark.dal.type.SmsMessageType;
import com.xquark.service.test.MockServiceTest;

public class SmsMessageServiceTest extends MockServiceTest {

  @Autowired
  private SmsMessageService smsMessageService;

  @Test
  public void testInsert() {
    SmsMessage message = new SmsMessage();
    message.setPhone("13662691715");
    message.setContent("测试内容");
    message.setStatus(SmsMessageStatus.FAIL);
    message.setType(SmsMessageType.PAID_BUYER);
    message.setCount(0);
    int ret = smsMessageService.insert(message);
    Assert.assertTrue(ret == 1);
  }

  @Test
  public void testReSend() {
    Assert.assertTrue(smsMessageService.reSend() >= 0);
  }


}
