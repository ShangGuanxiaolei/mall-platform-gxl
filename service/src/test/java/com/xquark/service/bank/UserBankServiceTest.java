package com.xquark.service.bank;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserBank;
import com.xquark.service.test.BaseEntityServiceTest;

public class UserBankServiceTest extends BaseEntityServiceTest<UserBank, UserBankService> {

  @Autowired
  private UserBankService userBankService;

  public UserBankServiceTest() {
    super(UserBank.class);
  }

  protected UserBank newEntityInstance() {
    UserBank userBank = new UserBank();
    userBank.setAccountName("aaaa");
    userBank.setAccountNumber("dddd");
    userBank.setOpeningBank("中国银行");
    userBank.setUserId(userBankService.getCurrentUser().getId());
    return userBank;
  }

  @Test
  public void testInsert() {
    UserBank bank = newEntityInstance();
    int rc = userBankService.insert(bank);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(bank.getId()));
  }

  @Test
  public void testMine() {
    UserBank bank = newEntityInstance();
    userBankService.insert(bank);

    List<UserBank> banks = userBankService.mine();
    Assert.assertTrue(banks.size() > 0);

  }


  @Test
  public void testDelete() {
    UserBank bank = newEntityInstance();
    userBankService.insert(bank);
    int rc = userBankService.delete(bank.getId());
    Assert.assertTrue(rc == 1);
    Assert.assertNull(userBankService.load(bank.getId()));
  }

  @Test
  public void testUnDelete() {
    UserBank bank = newEntityInstance();
    userBankService.insert(bank);
    userBankService.delete(bank.getId());
    Assert.assertNull(userBankService.load(bank.getId()));

    int rc = userBankService.undelete(bank.getId());
    Assert.assertTrue(rc == 1);
    bank = userBankService.load(bank.getId());
    Assert.assertFalse(bank.getArchive());
  }


  @Override
  public UserBankService getService() {
    return this.userBankService;
  }
}
