package com.xquark.service.bank;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Deal;
import com.xquark.dal.model.User;
import com.xquark.dal.model.WithdrawApply;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.DealStatus;
import com.xquark.dal.status.WithdrawApplyStatus;
import com.xquark.dal.type.DealType;
import com.xquark.service.account.AccountService;
import com.xquark.service.deal.DealService;
import com.xquark.service.test.BaseEntityServiceTest;
import com.xquark.utils.UniqueNoUtils;
import com.xquark.utils.UniqueNoUtils.UniqueNoType;

public class WithdrawApplyServiceTest extends
    BaseEntityServiceTest<WithdrawApply, WithdrawApplyService> {

  @Autowired
  private WithdrawApplyService withdrawApplyService;

  @Autowired
  private AccountService accountService;

  @Autowired
  private DealService dealService;

  public WithdrawApplyServiceTest() {
    super(WithdrawApply.class);
  }

  protected WithdrawApply newEntityInstance() {
    WithdrawApply apply = new WithdrawApply();
    apply.setApplyNo(withdrawApplyService.generateApplyNo());
    apply.setApplyMoney(new BigDecimal(1));
    apply.setStatus(WithdrawApplyStatus.NEW);
    apply.setAccountName("abc");
    apply.setBankId(IdTypeHandler.encode(739));
    apply.setAccountNumber("edf");
    apply.setOpeningBank("hij");
    apply.setType(1);
    apply.setUserId(withdrawApplyService.getCurrentUser().getId());
    return apply;
  }

  @Test
  public void testInsert() {
    WithdrawApply apply = newEntityInstance();
    int rc = withdrawApplyService.insert(apply);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(apply.getId()));
  }

  @Test
  public void testPay() {
    WithdrawApply apply = newEntityInstance();
    int rc = withdrawApplyService.insert(apply);
    Assert.assertTrue(rc == 1);
    apply.setConfirmMoney(apply.getApplyMoney());
    withdrawApplyService.pay(apply);
    //Assert.assertTrue(rc == 1);
  }

  @Test
  public void testListApplyByUserId() {
    WithdrawApply withdraw = newEntityInstance();
    Deal deal = new Deal();
    deal.setDealNo(UniqueNoUtils.next(UniqueNoType.TDN));
    deal.setDealType(DealType.DIRECT_PAY);
    deal.setStatus(DealStatus.NEW);
    deal.setFee(new BigDecimal(101));
    deal.setAccountTo(accountService.loadByUserId(withdraw.getUserId()).getId());
    dealService.createDeal(deal);
    dealService.finishDeal(deal.getId());
    deal = dealService.loadDeal(deal.getId());

    Pageable page = new PageRequest(0, 20);
    int rc = withdrawApplyService.insert(withdraw);
    Assert.assertTrue(rc == 1);

    List<WithdrawApply> withdraws = withdrawApplyService
        .listWithdrawApply(withdraw.getUserId(), WithdrawApplyStatus.NEW, page);
    Assert.assertTrue(withdraws.size() > 0);
  }

  @Test
  public void testTotalWithdrawApplyByStatus() {
    withdrawApplyService.totalWithdrawApplyByStatus(withdrawApplyService.getCurrentUser().getId(),
        WithdrawApplyStatus.SUCCESS);
  }

  @Test
  public void testAutoWithdrawByTask() {
    withdrawApplyService.autoWithdrawByTask();
  }

  @Override
  public WithdrawApplyService getService() {
    return this.withdrawApplyService;
  }
}
