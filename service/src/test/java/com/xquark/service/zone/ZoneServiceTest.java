package com.xquark.service.zone;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.config.ApplicationConfig;
import com.xquark.config.DalConfig;
import com.xquark.config.ServiceConfig;
import com.xquark.dal.model.Zone;
import com.xquark.dal.mybatis.IdTypeHandler;

@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DalConfig.class, ServiceConfig.class, ApplicationConfig.class})
public class ZoneServiceTest {

  @Autowired
  private ZoneService zoneService;

  @Test
  public void testLoad() {
    String id = "1";
    Zone zone = zoneService.load(id);
    Assert.assertNotNull(zone);
  }

  @Test
  public void testListRoots() {
    List<Zone> list = zoneService.listRoots();
    Assert.assertNotNull(list);
    Assert.assertTrue(list.size() > 0);
  }

  @Test
  public void testListChildren() {
    String zoneId = IdTypeHandler.encode(1L);
    List<Zone> list = zoneService.listChildren(zoneId);
    Assert.assertNotNull(list);
    Assert.assertTrue(list.size() > 0);
  }

  @Test
  public void testFindParent() {
    String zoneId = "2";
    Zone parent = zoneService.findParent(zoneId);
    Assert.assertNotNull(parent);
  }

}
