package com.xquark.service.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.model.Account;
import com.xquark.dal.model.Deal;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.DealStatus;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.DealType;
import com.xquark.dal.type.LogisticsCompany;
import com.xquark.dal.type.OrderType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.Customer;
import com.xquark.dal.vo.OrderFeeVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.listener.handler.transaction.DanbaoOrderTransactionHandler;
import com.xquark.service.account.AccountService;
import com.xquark.service.deal.DealService;
import com.xquark.service.order.vo.CustomerVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.test.MockServiceTest;
import com.xquark.service.union.UnionService;

public class OrderServiceTest extends MockServiceTest {

  @Autowired
  protected DealService dealService;

  @Autowired
  protected AccountService accountService;

  @Autowired
  protected UnionService unionService;

//	@Autowired
//	OrderActionHandler danbaoOrderActionHandler;

  @Before
  public void init() {
    super.init();

    Shop shop = mockShop(seller.getId(), "Best Shop Ever");
    this.shop = shop;

    ProductVO product = mockProduct(shop.getId(), seller.getId());
    this.product = product;
  }

  /**
   * 测试直接到帐交易全流程
   */
  @Test
  public void testDirectOrder() {
    // create order
    Order order = mockDirectOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    // ------------ order submit begin --------------------
    // verify order info
    Assert.assertNotNull(order);
    Assert.assertNotNull(order.getId());
    Assert.assertEquals(OrderStatus.SUBMITTED, order.getStatus());
    Assert.assertEquals(OrderType.DIRECT, order.getType());

    // verify deal info
//		Deal deal = dealService.loadDealByOrder(order.getId());
//		Assert.assertEquals(DealStatus.NEW, deal.getStatus());
//		Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP), 
//				deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));
//		Account buyerAccount = accountService.loadByUserId(buyer.getId());	
//		Account sellerAccount = accountService.loadByUserId(seller.getId());
//		Assert.assertEquals(buyerAccount.getId(), deal.getAccountFrom());
//		Assert.assertEquals(sellerAccount.getId(), deal.getAccountTo());

    // verify product status and stock
    List<OrderItem> items = orderService.listOrderItems(order.getId());
    for (OrderItem item : items) {
      Sku sku = productService.loadSku(item.getSkuId());
      Assert.assertEquals(sku.getAmount(),
          (Integer) (skuMap.get(item.getSkuId()) - item.getAmount()));
    }
    // ------------ order submit end --------------------

    // ------------ order pay begin --------------------
//		BigDecimal buyerBalance1 = accountService.loadByUserId(buyer.getId()).getBalance();
//		BigDecimal sellerBalance1 = accountService.loadByUserId(seller.getId()).getBalance();

    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    // 验证支付时间，支付状态
    Order order1 = orderService.load(order.getId());
    Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP),
        order1.getPaidFee().setScale(4, BigDecimal.ROUND_HALF_UP));
    Assert.assertEquals(OrderStatus.PAID, order1.getStatus());
    // Assert.assertEquals(PayStatus.SUCCESS, order1.getPaidStatus());
    Assert.assertNotNull(order1.getPaidAt());

    // 验证支付交易状态
//		deal = dealService.loadDealByOrder(order.getId());
//		Assert.assertNotNull(deal);
//		Assert.assertEquals(DealStatus.SUCCESS, deal.getStatus());
//		Assert.assertEquals(DealType.DIRECT_PAY, deal.getDealType());
//		Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP), deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));

//		BigDecimal buyerBalance2 = accountService.loadByUserId(buyer.getId()).getBalance();
//		BigDecimal sellerBalance2 = accountService.loadByUserId(seller.getId()).getBalance();
//		Assert.assertEquals(buyerBalance1.setScale(4, BigDecimal.ROUND_HALF_UP), buyerBalance2.setScale(4, BigDecimal.ROUND_HALF_UP));
//		Assert.assertEquals(sellerBalance1.add(deal.getFee()).setScale(4, BigDecimal.ROUND_HALF_UP), sellerBalance2.setScale(4, BigDecimal.ROUND_HALF_UP));
    // ------------ order pay end --------------------

    // ------------ order ship begin --------------------
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    order1 = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SUCCESS, order1.getStatus());
    Assert.assertEquals(LogisticsCompany.STO.toString(), order1.getLogisticsCompany());
    Assert.assertEquals("123456", order1.getLogisticsOrderNo());
    Assert.assertNotNull(order1.getShippedAt());
    // ------------ order ship end --------------------
  }

  @Test
  public void testDanbaoOrder() {
    // create order
    Order order = mockDanbaoOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    // ------------ order submit begin --------------------
    // 1. verify order info
    Assert.assertNotNull(order);
    Assert.assertNotNull(order.getId());
    Assert.assertEquals(OrderStatus.SUBMITTED, order.getStatus());
    Assert.assertEquals(OrderType.DANBAO, order.getType());

    // 2. verify deal info
//		Deal deal = dealService.loadDealByOrder(order.getId());
//		Assert.assertEquals(DealStatus.NEW, deal.getStatus());
//		Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP), 
//				deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));
//		Account sellerAccount = accountService.loadByUserId(seller.getId());
//		Account buyerAccount = accountService.loadByUserId(buyer.getId());	
//		Assert.assertEquals(buyerAccount.getId(), deal.getAccountFrom());
//		Assert.assertEquals(sellerAccount.getId(), deal.getAccountTo());

    // 3. verify product status and stock
    List<OrderItem> items = orderService.listOrderItems(order.getId());
    for (OrderItem item : items) {
      Sku sku = productService.loadSku(item.getSkuId());
      Assert.assertEquals(sku.getAmount(),
          (Integer) (skuMap.get(item.getSkuId()) - item.getAmount()));
    }

    // 4. verify union data
//		for (OrderItem item : items) {
//			Commission commission = unionService.loadByOrderItem(item.getId());
//			Assert.assertEquals(item.getSkuId(), commission.getSkuId());
//			Assert.assertEquals(item.getAmount(), commission.getAmount());
//			Assert.assertTrue(commission.getFee().doubleValue() > 0);
//		}

    // ------------ order submit end --------------------

    // ------------ order pay begin --------------------
    BigDecimal buyerBalance1 = accountService.loadByUserId(buyer.getId()).getBalance();
    BigDecimal sellerBalance1 = accountService.loadByUserId(seller.getId()).getBalance();

    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    // 验证支付时间，支付状态
    Order order1 = orderService.load(order.getId());
    Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP),
        order1.getPaidFee().setScale(4, BigDecimal.ROUND_HALF_UP));
    Assert.assertEquals(OrderStatus.PAID, order1.getStatus());
    // Assert.assertEquals(PayStatus.SUCCESS, order1.getPaidStatus());
    Assert.assertNotNull(order1.getPaidAt());

    // 验证支付交易状态
//		deal = dealService.loadDealByOrder(order.getId());
//		Assert.assertNotNull(deal);
//		Assert.assertEquals(DealStatus.IN_PROGRESS, deal.getStatus());
//		Assert.assertEquals(DealType.DANBAO_PAY, deal.getDealType());
//		Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP), deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));

    BigDecimal buyerBalance2 = accountService.loadByUserId(buyer.getId()).getBalance();
    BigDecimal sellerBalance2 = accountService.loadByUserId(seller.getId()).getBalance();
    Assert.assertEquals(buyerBalance2.setScale(4, BigDecimal.ROUND_HALF_UP),
        buyerBalance1.setScale(4, BigDecimal.ROUND_HALF_UP));
    Assert.assertEquals(sellerBalance2.setScale(4, BigDecimal.ROUND_HALF_UP),
        sellerBalance1.setScale(4, BigDecimal.ROUND_HALF_UP));
    // ------------ order pay end --------------------

    // ------------ order ship begin --------------------
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    order1 = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SHIPPED, order1.getStatus());
    Assert.assertEquals(LogisticsCompany.STO.toString(), order1.getLogisticsCompany());
    Assert.assertEquals("123456", order1.getLogisticsOrderNo());
    Assert.assertNotNull(order1.getShippedAt());
    // ------------ order ship end --------------------

    // ------------ order sign begin ------------------
    this.setAuthUser(buyer);
    orderService.sign(order.getId());

    order1 = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SUCCESS, order1.getStatus());

//		deal = dealService.loadDealByOrder(order.getId());
//		Assert.assertNotNull(deal);
//		Assert.assertEquals(DealStatus.SUCCESS, deal.getStatus());
    // ------------ order sign end ------------------
  }

  @Test
  public void testRefund() {
    Order order = mockDanbaoOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SHIPPED, order.getStatus());

    this.setAuthUser(seller);
    orderService.refund(order.getId());

    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.CLOSED, order.getStatus());
  }

  @Test
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void testRefundFee() {
    String orderNo = "SO150108175200199003";
    OrderVO order = orderService.loadByOrderNo(orderNo);
    BigDecimal bGoodsFee = new BigDecimal(7);
    bGoodsFee = bGoodsFee.setScale(2, BigDecimal.ROUND_DOWN);
    BigDecimal bLogisticsFee = new BigDecimal(0);
    orderService.refund(order.getId(), bGoodsFee, bLogisticsFee);
    order = orderService.loadByOrderNo(orderNo);
//		Assert.assertTrue(order.getRefundFee().compareTo(bGoodsFee) == 0);
//		Assert.assertTrue(order.getRefundPlatformFee().compareTo(new BigDecimal(5)) == 0);
  }

  @Test
  public void testRefuncd() {
    DanbaoOrderTransactionHandler danbaoOrderTransactionHandler = (DanbaoOrderTransactionHandler) SpringContextUtil
        .getBean("danbaoOrderTransactionHandler");
    Order order = orderService.loadByOrderNo("SO150105192133199001");
    BigDecimal fee = BigDecimal.valueOf(0.01);
    BigDecimal result = fee.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
    order.setRefundFee(result);
    danbaoOrderTransactionHandler.onRefund(order);
//		
//		order = orderService.loadByOrderNo("SO150105192133199001");
//		fee = BigDecimal.valueOf(0.01);
//		result = fee.divide(new BigDecimal(1), 2, BigDecimal.ROUND_HALF_UP);
//		order.setRefundFee(result);
//		danbaoOrderTransactionHandler.onRefund(order);
  }

  @Test
  public void testRefundAccept() {
    Order order = mockDanbaoOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(buyer);
    orderService.requestRefund(order.getId());

    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.REFUNDING, order.getStatus());

    this.setAuthUser(seller);
    orderService.acceptRefund(order.getId());
    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.CLOSED, order.getStatus());
  }

  @Test
  public void testRefundReject() {
    Order order = mockDanbaoOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(buyer);
    orderService.requestRefund(order.getId());
    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.REFUNDING, order.getStatus());

    this.setAuthUser(seller);
    orderService.rejectRefund(order.getId());
    order = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SHIPPED, order.getStatus());
  }

  /**
   * 测试订单取消
   */
  @Test
  public void testCancel() {
    Order order = mockDirectOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }
    order = orderService.cancel(order.getId());

    Assert.assertEquals(OrderStatus.CANCELLED, order.getStatus());
    // Assert.assertNotNull(order.getCancelledAt());

    // verify product status and stock
    List<OrderItem> items = orderService.listOrderItems(order.getId());

    // product stock has been restored
    for (OrderItem item : items) {
      Sku sku = productService.loadSku(item.getSkuId());
      Assert.assertEquals(sku.getAmount(), (Integer) (skuMap.get(item.getSkuId())));
    }
  }

  @Test
  public void testListByAdmin() {
    Pageable pageable = new PageRequest(0, 10);
    Map<String, Object> params = new HashMap<String, Object>();

    List<OrderVO> orders = null;
    Long total = orderService.countByAdmin(params);
    if (total.longValue() > 0) {
      orders = orderService.listByAdmin(params, pageable);
    } else {
      orders = new ArrayList<OrderVO>();
    }

    Assert.assertTrue(orders.size() > 0);
  }

  @AfterTransaction
  public void testAfterTransaction() {
    System.out.println("hello");
  }

  /**
   * 测试订单创建
   */
  public void testSubmit() {
    Order order = mockDirectOrder();
    Map<String, Integer> skuMap = new HashMap<String, Integer>();
    for (Sku sku : product.getSkus()) {
      skuMap.put(sku.getId(), sku.getAmount());
    }

    // verify order info
    Assert.assertNotNull(order);
    Assert.assertNotNull(order.getId());
    Assert.assertEquals(OrderStatus.SUBMITTED, order.getStatus());
    Assert.assertEquals(OrderType.DIRECT, order.getType());

    // verify deal info
    Deal deal = dealService.loadDealByOrder(order.getId());
    Assert.assertEquals(DealStatus.NEW, deal.getStatus());
    Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP),
        deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));
    Account sellerAccount = accountService.loadByUserId(seller.getId());
    Account buyerAccount = accountService.loadByUserId(buyer.getId());
    Assert.assertEquals(buyerAccount.getId(), deal.getAccountFrom());
    Assert.assertEquals(sellerAccount.getId(), deal.getAccountTo());

    // verify product status and stock
    List<OrderItem> items = orderService.listOrderItems(order.getId());
    for (OrderItem item : items) {
      Sku sku = productService.loadSku(item.getSkuId());
      Assert.assertEquals(sku.getAmount(),
          (Integer) (skuMap.get(item.getSkuId()) - item.getAmount()));
    }
  }

  /**
   * 测试即时到帐交易订单支付
   */
  public void testPay() {
    Order order = mockDirectOrder();
    accountService.loadByUserId(buyer.getId()).getBalance();
    BigDecimal sellerBalance1 = accountService.loadByUserId(seller.getId()).getBalance();

    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());

    // 验证支付时间，支付状态
    Order order1 = orderService.load(order.getId());
    Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP),
        order1.getPaidFee().setScale(4, BigDecimal.ROUND_HALF_UP));
    Assert.assertEquals(OrderStatus.PAID, order1.getStatus());
    // Assert.assertEquals(PayStatus.SUCCESS, order1.getPaidStatus());
    Assert.assertNotNull(order1.getPaidAt());

    // 验证支付交易状态
    Deal deal = dealService.loadDealByOrder(order.getId());
    Assert.assertNotNull(deal);
    Assert.assertEquals(DealStatus.SUCCESS, deal.getStatus());
    Assert.assertEquals(DealType.DIRECT_PAY, deal.getDealType());
    Assert.assertEquals(order.getTotalFee().setScale(4, BigDecimal.ROUND_HALF_UP),
        deal.getFee().setScale(4, BigDecimal.ROUND_HALF_UP));

    accountService.loadByUserId(buyer.getId()).getBalance();
    BigDecimal sellerBalance2 = accountService.loadByUserId(seller.getId()).getBalance();
    // Assert.assertEquals(buyerBalance2, buyerBalance1 - deal.getFee());
    Assert.assertEquals(sellerBalance2.setScale(4, BigDecimal.ROUND_HALF_UP),
        sellerBalance1.add(deal.getFee()).setScale(4, BigDecimal.ROUND_HALF_UP));
  }

  /**
   * 测试订单发货
   */
  public void testShip() {
    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");
    Order order1 = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SUCCESS, order1.getStatus());
    Assert.assertEquals(LogisticsCompany.STO.toString(), order1.getLogisticsCompany());
    Assert.assertEquals("123456", order1.getLogisticsOrderNo());
    Assert.assertNotNull(order1.getShippedAt());
  }

  /**
   * 测试订单签收
   */
  public void testSign() {
    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(seller);
    orderService.sign(order.getId());

    Order order1 = orderService.load(order.getId());
    Assert.assertEquals(OrderStatus.SUCCESS, order1.getStatus());

    Deal deal = dealService.loadDealByOrder(order.getId());
    Assert.assertNotNull(deal);
    Assert.assertEquals(DealStatus.SUCCESS, deal.getStatus());
  }

  /**
   * 测试订单加载
   */
  public void testLoad() {
    Order order = mockDirectOrder();
    Order order1 = orderService.load(order.getId());
    Assert.assertNotNull(order1);
  }

  /**
   * 测试即时到帐交易订单支付
   */
  public void testPay2() {
    orderService.pay("SO140612163457001030", PaymentMode.ALIPAY, "SO140612163457001030");
  }

  public void testListByStatus4Buyer() {
    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(seller);
    orderService.sign(order.getId());

    this.setAuthUser(buyer);
    List<OrderVO> orders = orderService.listByStatus4Buyer(OrderStatus.SUCCESS, null);
    Assert.assertTrue(orders.size() > 0);
    Assert.assertTrue(orders.get(0).getId().equals(order.getId()));
  }

  public void testListByStatus4Seller() {
    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(seller);
    orderService.sign(order.getId());

    this.setAuthUser(seller);
    Pageable pageable = new PageRequest(0, 10);
    List<OrderVO> orders = orderService.listByStatus4Seller(OrderStatus.SUCCESS, pageable);
    Assert.assertTrue(orders.size() > 0);
    Assert.assertTrue(orders.get(0).getId().equals(order.getId()));
  }

  public void testListCustomers() {
    accountService.loadByUserId(buyer.getId());

    this.setAuthUser(buyer);
    Deal deposit = dealService.createDepositDeal(new BigDecimal(100));
    dealService.finishDeal(deposit.getId());

    accountService.loadByUserId(buyer.getId());

    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(seller);
    orderService.sign(order.getId());

    List<Customer> customers = orderService.listCustomers(null);
    Assert.assertTrue(customers.size() > 0);
  }

  public void testListOrdersByCustomer() {
    Order order = mockDirectOrder();
    // current user
    this.setAuthUser(buyer);
    orderService.pay(order.getOrderNo(), PaymentMode.ALIPAY, order.getOrderNo());
    // current user
    this.setAuthUser(seller);
    orderService.ship(order.getId(), LogisticsCompany.STO.toString(), "123456");

    this.setAuthUser(seller);
    orderService.sign(order.getId());

    CustomerVO customer = orderService.listOrdersByCustomer(buyer.getId());
    Assert.assertTrue(customer.getOrders().size() > 0);
    Assert.assertTrue(customer.getOrders().get(0).getId().equals(order.getId()));
  }

  public void testAutoCancel() {
    Assert.assertNotNull(orderService.autoCancel());
  }

  @Test
  public void testFindOrderFees() {
    OrderVO order = orderService.loadByOrderNo("SO141124232037101010");
    List<OrderFeeVO> lsVO = orderService.findOrderFees(order);
    for (OrderFeeVO vo : lsVO) {
      System.out.println(
          vo.getCode() + " name=" + vo.getName() + " type=" + vo.getType() + " amount=" + vo
              .getAmount());
    }
  }

  @Test
  public void testUpdatePrice() {
    Order order = mockDirectOrder();
    this.setAuthUser(seller);

    BigDecimal goodsFee = BigDecimal.ONE;
    BigDecimal logisticsFee = BigDecimal.TEN;
    orderService.updatePrice(order.getId(), goodsFee, logisticsFee);
    order = orderService.load(order.getId());
    Assert.assertTrue(order.getLogisticsFee().compareTo(logisticsFee) == 0);
    Assert.assertTrue(order.getTotalFee().compareTo(goodsFee.add(logisticsFee)) == 0);
  }

  @Test
  public void testDelete() {

    int i = orderService.delete(IdTypeHandler.encode(5041));
    Assert.assertTrue(i == 1);
  }
}
