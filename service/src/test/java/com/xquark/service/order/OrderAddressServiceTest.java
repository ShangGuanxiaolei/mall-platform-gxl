package com.xquark.service.order;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.test.BaseEntityServiceTest;

public class OrderAddressServiceTest extends
    BaseEntityServiceTest<OrderAddress, OrderAddressService> {

  public OrderAddressServiceTest() {
    super(OrderAddress.class);
  }

  protected OrderAddress newEntityInstance() {
    OrderAddress address = new OrderAddress();
    address.setOrderId(IdTypeHandler.encode(123));
    address.setConsignee("znu991");
    address.setZoneId("123");
    address.setStreet("浙江省杭州市天目山路145号");
    address.setPhone("1388888888");
    return address;
  }


  @Autowired
  private OrderAddressService orderAddressService;

  @Test
  public void testInsert() {
    OrderAddress address = newEntityInstance();
    orderAddressService.insert(address);
    Assert.assertNotNull(address.getId());
  }

  @Test
  public void testLoad() {
    OrderAddress address = newEntityInstance();
    orderAddressService.insert(address);
    address = orderAddressService.load(address.getId());
    Assert.assertEquals("znu991", address.getConsignee());
  }

  @Test
  public void testSelectByOrderId() {
    OrderAddress address = newEntityInstance();
    orderAddressService.insert(address);

    OrderAddress address1 = orderAddressService.selectByOrderId(address.getOrderId());
    Assert.assertEquals(address.getConsignee(), address1.getConsignee());
  }

  @Override
  public OrderAddressService getService() {
    return orderAddressService;
  }
}
