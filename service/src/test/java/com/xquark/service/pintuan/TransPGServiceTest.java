package com.xquark.service.pintuan;

import com.xquark.config.*;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PGTranInfoVo;
import com.xquark.service.test.BaseServiceTest;
import com.xquark.thirds.config.ThirdConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author qiuchuyi
 * @date 2018/9/13 10:29
 */
@ContextConfiguration(classes = {DalConfig.class, ServiceTestConfig.class, ApplicationConfig.class,
        ThirdConfig.class, CachingConfig.class, ServiceCommonConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class TransPGServiceTest extends BaseServiceTest<TransPGService> {

    @Autowired
    private TransPGService transPGService;

    @Test
    public void transPGService() {
        PGMemberInfoVo pgMemberInfoVo = new PGMemberInfoVo();
        pgMemberInfoVo.setIsGroupHead(1);
//        pgMemberInfoVo.setWxNickname("linbaishui");
        //==================================================================
        PGTranInfoVo pgTranInfoVo = new PGTranInfoVo();
        pgTranInfoVo.setPieceGroupTranCode("qweasdzxc123");
        pgTranInfoVo.setGroupHeadId("asdqwezxc");
        String openId = "6669527";
//        transPGService.transPGService(pgMemberInfoVo,openId,pgTranInfoVo);
    }

    @Test
    public void test(){
        System.out.println("qwe");
    }

    @Override
    public TransPGService getService() {
        return null;
    }
}