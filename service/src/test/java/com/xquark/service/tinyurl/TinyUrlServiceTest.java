package com.xquark.service.tinyurl;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.TinyUrl;
import com.xquark.service.test.MockServiceTest;

public class TinyUrlServiceTest extends MockServiceTest {

  @Autowired
  private TinyUrlService tinyUrlService;

  protected TinyUrl newEntityInstance() {
    TinyUrl tinyUrl = new TinyUrl();
    tinyUrl.setUrl("http://www.taobao.com22233");
    return tinyUrl;
  }

  @Test
  public void testInsert() {
    TinyUrl tinyUrl = newEntityInstance();
    String key = tinyUrlService.insert(tinyUrl.getUrl());
    Assert.assertNotNull(key);

  }

  @Test
  public void testFindUrlByKey() {
    TinyUrl tinyUrl = newEntityInstance();
    String key = tinyUrlService.insert(tinyUrl.getUrl());
    String url = tinyUrlService.findUrlByKey(key);
    Assert.assertEquals(url, tinyUrl.getUrl());
  }

  @Test
  public void testFindUrlByUrl() {
    TinyUrl tinyUrl = newEntityInstance();
    String key = tinyUrlService.insert(tinyUrl.getUrl());
    String key2 = tinyUrlService.findKeyByUrl(tinyUrl.getUrl());
    Assert.assertEquals(key, key2);
  }
}
