package com.xquark.service.category;

import com.xquark.dal.model.Category;
import com.xquark.dal.type.Taxonomy;
import com.xquark.service.test.MockServiceTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class CategoryServiceTest extends MockServiceTest {

  @Autowired
  private CategoryService categoryService;

  @Before
  public void init() {
    super.init();
    this.setAuthUser(seller);
  }

  @Test
  public void test() {
    this.setAuthUser(seller);
    Category category1 = categoryService.saveUserGoods("男装1");
    Category category2 = categoryService.saveUserGoods("女装1");
    categoryService.saveUserGoods("保健1");
    categoryService.saveUserGoods("手表1");
    categoryService.saveUserGoods("鞋靴1");
    categoryService.saveUserGoods("包包1");

    List<Category> list = categoryService.listUserRootGoods(Taxonomy.GOODS);
    Assert.assertEquals(6, list.size());

    category1 = categoryService.updateUserGoodsName(category1.getId(), Taxonomy.GOODS, "男士1");
    Assert.assertEquals("男士1", category1.getName());

    categoryService.removeUesrGoods(category2.getId());
    list = categoryService.listUserRootGoods(Taxonomy.GOODS);
    Assert.assertEquals(5, list.size());
  }

}
