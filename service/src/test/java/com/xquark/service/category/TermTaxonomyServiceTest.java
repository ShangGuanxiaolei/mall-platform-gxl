package com.xquark.service.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Category;
import com.xquark.dal.type.Taxonomy;
import com.xquark.dal.vo.CategoryVO;
import com.xquark.dal.vo.ProductAdmin;
import com.xquark.service.poster.PosterService;
import com.xquark.service.product.ProductService;
import com.xquark.service.test.MockServiceTest;

public class TermTaxonomyServiceTest extends MockServiceTest {

  @Autowired
  private TermTaxonomyService termTaxonomyService;

  @Autowired
  private PosterService posterService;

  @Autowired
  private ProductService productService;

  @Autowired
  private CategoryService categoryService;

  @Before
  public void init() {
    super.init();
    this.setAuthUser(seller);
  }

  public void testSave() {
    Category cat1 = termTaxonomyService
        .save("电子产品", Taxonomy.CATALOG, null, seller.getId(), seller.getShopId());
    Assert.assertNotNull(cat1.getId());

    Category cat2 = termTaxonomyService
        .save("手机", Taxonomy.CATALOG, cat1.getId(), seller.getId(), seller.getShopId());
    Assert.assertNotNull(cat2.getId());
  }

  @Test
  public void testCategory() {
    Category cat1 = termTaxonomyService
        .save("保健品1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat2 = termTaxonomyService
        .save("手表1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat3 = termTaxonomyService
        .save("鞋1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat4 = termTaxonomyService
        .save("包1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat5 = termTaxonomyService
        .save("服装1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());

    Category cat6 = termTaxonomyService
        .save("男装1", Taxonomy.GOODS, cat5.getId(), seller.getId(), seller.getShopId());
    Category cat7 = termTaxonomyService
        .save("女装1", Taxonomy.GOODS, cat5.getId(), seller.getId(), seller.getShopId());

    Category cat8 = termTaxonomyService
        .save("裙子1", Taxonomy.GOODS, cat7.getId(), seller.getId(), seller.getShopId());
    Category cat9 = termTaxonomyService
        .save("内衣1", Taxonomy.GOODS, cat7.getId(), seller.getId(), seller.getShopId());

    Category cat10 = termTaxonomyService
        .save("连衣裙1", Taxonomy.GOODS, cat8.getId(), seller.getId(), seller.getShopId());

    Category cat11 = termTaxonomyService
        .save("工装连衣裙1", Taxonomy.GOODS, cat10.getId(), seller.getId(), seller.getShopId());

    List<Category> roots = termTaxonomyService.listRootCategories(Taxonomy.GOODS);
    // Assert.assertEquals(39, roots.size());

    Assert.assertEquals(1,
        termTaxonomyService.listSiblingCategories(cat8.getId()).size());

    Assert.assertEquals("女装1",
        termTaxonomyService.loadParentCategory(cat8.getId()).getName());
    Assert.assertEquals(2,
        termTaxonomyService.listAscendantCategories(cat8.getId()).size());

    Assert.assertEquals(1, termTaxonomyService.listSubCategories(cat8.getId())
        .size());
    Assert.assertEquals(2,
        termTaxonomyService.listDescendantCategories(cat8.getId()).size());
  }

  @Test
  public void testAddProductCategory() {
    Category cat1 = termTaxonomyService
        .save("保健品1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat2 = termTaxonomyService
        .save("手表1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat3 = termTaxonomyService
        .save("鞋1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat4 = termTaxonomyService
        .save("包1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());
    Category cat5 = termTaxonomyService
        .save("服装1", Taxonomy.GOODS, null, seller.getId(), seller.getShopId());

    Category cat6 = termTaxonomyService
        .save("男装1", Taxonomy.GOODS, cat5.getId(), seller.getId(), seller.getShopId());
    Category cat7 = termTaxonomyService
        .save("女装1", Taxonomy.GOODS, cat5.getId(), seller.getId(), seller.getShopId());

    Category cat8 = termTaxonomyService
        .save("裙子1", Taxonomy.GOODS, cat7.getId(), seller.getId(), seller.getShopId());
    Category cat9 = termTaxonomyService
        .save("内衣1", Taxonomy.GOODS, cat7.getId(), seller.getId(), seller.getShopId());

    Category cat10 = termTaxonomyService
        .save("连衣裙1", Taxonomy.GOODS, cat8.getId(), seller.getId(), seller.getShopId());

    Category cat11 = termTaxonomyService
        .save("工装连衣裙1", Taxonomy.GOODS, cat10.getId(), seller.getId(), seller.getShopId());

    List<Category> categories = termTaxonomyService.listAll(Taxonomy.GOODS);
    List<ProductAdmin> products = new ArrayList<ProductAdmin>();
    Map<String, Object> params = new HashMap<String, Object>();
    int page = 0;
    int idx = 0;
    int catSize = categories.size();
    do {
      Pageable pager = new PageRequest(page, 100);
      products = productService.listProductsByAdmin(params, pager);
      if (products != null && products.size() > 0) {
        for (ProductAdmin productAdmin : products) {
          categoryService.addProductCategory(productAdmin.getId(), categories.get(idx).getId());
          idx = (++idx) % catSize;
        }
      }
      page++;
    } while (products != null && products.size() == 100);

//		Pageable pageable = new PageRequest(0, 20);

//		List<Product> plist1 = categoryService.listProductsInCategory(cat8.getId(), pageable);
//		Assert.assertEquals(1, plist1.size());
//		
//		List<Product> plist2 = categoryService.listProductsUnderCategory(cat8.getId(), pageable);
//		Assert.assertEquals(2, plist2.size());
//
//		List<Product> plist3 = categoryService.listProductsInCategory(cat10.getId(), pageable);
//		Assert.assertEquals(0, plist3.size());
//		
//		List<Product> plist4 = categoryService.listProductsUnderCategory(cat10.getId(), pageable);
//		Assert.assertEquals(1, plist4.size());

  }

  @Test
  public void testLoadCategoryByProductId() {
    Pageable pager = new PageRequest(0, 100);
    Map<String, Object> params = new HashMap<String, Object>();
    List<ProductAdmin> products = productService.listProductsByAdmin(params, pager);
    for (ProductAdmin productAdmin : products) {
      CategoryVO vo = categoryService.loadCategoryByProductId(productAdmin.getId());
//    		Assert.assertNotNull(vo);
    }
  }
}
