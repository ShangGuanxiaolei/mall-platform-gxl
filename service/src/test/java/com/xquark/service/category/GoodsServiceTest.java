package com.xquark.service.category;

import com.xquark.dal.model.Category;
import com.xquark.dal.type.Taxonomy;
import com.xquark.service.test.MockServiceTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class GoodsServiceTest extends MockServiceTest {

  @Autowired
  private GoodsService goodsService;

  @Before
  public void init() {
    super.init();
    this.setAuthUser(seller);
  }

  @Test
  public void test() {
    this.setAuthUser(seller);
    Category category1 = goodsService.saveUserGoods("男装1");
    Category category2 = goodsService.saveUserGoods("女装1");
    goodsService.saveUserGoods("保健1");
    goodsService.saveUserGoods("手表1");
    goodsService.saveUserGoods("鞋靴1");
    goodsService.saveUserGoods("包包1");

    List<Category> list = goodsService.listUserRootGoods(Taxonomy.GOODS);
    Assert.assertEquals(6, list.size());

    category1 = goodsService.updateUserGoodsName(category1.getId(), Taxonomy.GOODS, "男士1");
    Assert.assertEquals("男士1", category1.getName());

    goodsService.removeUesrGoods(category2.getId());
    list = goodsService.listUserRootGoods(Taxonomy.GOODS);
    Assert.assertEquals(5, list.size());
  }

}
