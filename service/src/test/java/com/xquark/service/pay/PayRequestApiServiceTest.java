package com.xquark.service.pay;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Order;
import com.xquark.dal.model.PayRequest;
import com.xquark.dal.model.SubAccount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.type.PayRequestBizType;
import com.xquark.dal.type.PayRequestPayType;
import com.xquark.dal.type.PaymentMode;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.test.BaseServiceTest;

public class PayRequestApiServiceTest extends BaseServiceTest<PayRequestApiService> {

  @Autowired
  PayRequestApiService payRequestApiService;

  @Autowired
  PayRequestService payRequestService;

  @Autowired
  AccountApiService accountApiService;

  @Test
  //@Transactional(propagation=Propagation.NOT_SUPPORTED)
  public void testDirectPayRequest() {
    Order order = orderEntityInstance();
    // Mockito.when(null).thenReturn(value);

    // 先从支付宝充值
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
        .findSubAccountByPayMode(PaymentMode.ALIPAY, AccountType.AVAILABLE);
    SubAccount toAccount = accountApiService
        .findSubAccountByUserId(order.getBuyerId(), AccountType.AVAILABLE);
    PayRequest request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.RECHARGE, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        null);
    payRequestApiService.payRequest(request);
    // 充值支付成功

    // 从买家的可用账户转账到买家可消费账户
    fromAccount = accountApiService
        .findSubAccountByUserId(order.getBuyerId(), AccountType.AVAILABLE);
    toAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    payNo = payRequestApiService.generatePayNo();
    String forRequestId = request.getId();
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        forRequestId);
    payRequestApiService.payRequest(request);

    // 从买家的消费账户转账到卖家的可用账户
    fromAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    toAccount = accountApiService
        .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        forRequestId);
    payRequestApiService.payRequest(request);
  }

  /**
   * 担保交易
   */
  @Test
  public void testDanbaoPayRequest() {

    Order order = orderEntityInstance();
    // Mockito.when(null).thenReturn(value);

    // 先从支付宝充值
    String payNo = payRequestApiService.generatePayNo();
    SubAccount fromAccount = accountApiService
        .findSubAccountByPayMode(PaymentMode.ALIPAY, AccountType.AVAILABLE);
    SubAccount toAccount = accountApiService
        .findSubAccountByUserId(order.getBuyerId(), AccountType.AVAILABLE);
    PayRequest request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.RECHARGE, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        null);
    Assert.assertTrue(payRequestApiService.payRequest(request));
    // 充值支付成功

    // 从买家的可用账户转账到买家可消费账户
    fromAccount = accountApiService
        .findSubAccountByUserId(order.getBuyerId(), AccountType.AVAILABLE);
    toAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    payNo = payRequestApiService.generatePayNo();
    String forRequestId = request.getId();
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        forRequestId);
    Assert.assertTrue(payRequestApiService.payRequest(request));

    // 从买家的消费账户转账到卖家的担保账户
    fromAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    toAccount = accountApiService.findSubAccountByUserId(order.getSellerId(), AccountType.DANBAO);
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        forRequestId);
    Assert.assertTrue(payRequestApiService.payRequest(request));

    payNo = payRequestApiService.generatePayNo();
    // 从卖家的担保账户转账到卖家的可用账户
    fromAccount = accountApiService.findSubAccountByUserId(order.getSellerId(), AccountType.DANBAO);
    toAccount = accountApiService
        .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee(), fromAccount.getId(), toAccount.getId(),
        null);
    Assert.assertTrue(payRequestApiService.payRequest(request));
  }

  protected Order orderEntityInstance() {
    Order order = new Order();
    order.setOrderNo("SO23423412");
    order.setTotalFee(BigDecimal.TEN);
    order.setSellerId(IdTypeHandler.encode(123));
    order.setBuyerId(IdTypeHandler.encode(234));
    return order;
  }

  @Override
  public PayRequestApiService getService() {
    return this.payRequestApiService;
  }
}
