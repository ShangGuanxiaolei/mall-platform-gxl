package com.xquark.service.account;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.SubAccount;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.type.AccountType;
import com.xquark.dal.vo.SubAccountVO;
import com.xquark.service.test.BaseEntityServiceTest;

public class SubAccountServiceTest extends BaseEntityServiceTest<SubAccount, SubAccountService> {

  @Autowired
  private SubAccountService subAccountService;


  protected SubAccount newEntityInstance() {
    SubAccount subAccount = new SubAccount();
    subAccount.setAccountId("uzyi");
    subAccount.setAccountType(AccountType.AVAILABLE);
    subAccount.setBalance(BigDecimal.TEN);
    return subAccount;
  }

  @Test
  public void testInsert() {
    SubAccount subAccount = newEntityInstance();
    int rc = subAccountService.insert(subAccount);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(subAccount.getId()));
  }

  @Test
  public void testLoad() {
    SubAccount subAccount = newEntityInstance();
    subAccountService.insert(subAccount);

    subAccount = subAccountService.load(subAccount.getId());
    Assert.assertTrue(StringUtils.isNotBlank(subAccount.getId()));
    Assert.assertTrue(StringUtils.isNotBlank(subAccount.getAccountId()));
    Assert.assertTrue(subAccount.getCreatedAt() != null);
  }

  @Test
  public void testListByCanWithdraw() {
    List<SubAccountVO> list = subAccountService.listByCanWithdraw();
    Assert.assertTrue(list.size() > 0);
  }

  @Test
  public void testSelectBalanceByUser() {
    Map<AccountType, BigDecimal> map = subAccountService
        .selectBalanceByUser(IdTypeHandler.encode(697411));
    Assert.assertTrue(!map.isEmpty());
  }

  @Test
  public void testSelectBalanceByUserAndType() {
    BigDecimal balance = subAccountService
        .selectBalanceByUserAndType(IdTypeHandler.encode(697411), AccountType.WITHDRAW);
    Assert.assertNotNull(balance);
  }


  public SubAccountServiceTest() {
    super(SubAccount.class);
  }

  @Override
  public SubAccountService getService() {
    return this.subAccountService;
  }
}
