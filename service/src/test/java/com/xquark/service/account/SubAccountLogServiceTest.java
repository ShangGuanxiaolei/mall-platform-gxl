package com.xquark.service.account;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.SubAccountLog;
import com.xquark.dal.type.AccountOpType;
import com.xquark.service.test.BaseEntityServiceTest;

public class SubAccountLogServiceTest extends
    BaseEntityServiceTest<SubAccountLog, SubAccountLogService> {

  @Autowired
  private SubAccountLogService subAccountLogService;

  protected SubAccountLog newEntityInstance() {
    SubAccountLog subAccountLog = new SubAccountLog();
    subAccountLog.setAmount(BigDecimal.TEN);
    subAccountLog.setSubAccountId("uzyi");//默认kkkd账户 24970
    subAccountLog.setRequestId("1dfsdf");
    subAccountLog.setOpType(AccountOpType.RECHARGE);
    return subAccountLog;
  }

  @Test
  public void testInsert() {
    SubAccountLog subAccountLog = newEntityInstance();
    int rc = subAccountLogService.insert(subAccountLog);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(subAccountLog.getId()));
  }

  @Test
  public void testLoad() {
    SubAccountLog subAccountLog = newEntityInstance();
    subAccountLogService.insert(subAccountLog);

    subAccountLog = subAccountLogService.load(subAccountLog.getId());
    Assert.assertTrue(StringUtils.isNotBlank(subAccountLog.getId()));
    Assert.assertTrue(subAccountLog.getCreatedAt() != null);
  }

  public SubAccountLogServiceTest() {
    super(SubAccountLog.class);
  }

  @Override
  public SubAccountLogService getService() {
    return this.subAccountLogService;
  }
}
