package com.xquark.service.Comment.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.model.Comment;
import com.xquark.dal.model.CommentLike;
import com.xquark.dal.model.CommentReply;
import com.xquark.dal.type.CommentType;
import com.xquark.dal.vo.CommentVO;
import com.xquark.service.Comment.CommentService;
import com.xquark.service.test.BaseServiceTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jitre on 11/10/17.
 */
public class CommentServiceImplTest extends BaseServiceTest<CommentService> {

  @Autowired
  private CommentService commentService;

  @Test
  public void testComment() throws Exception {

    //添加一个评论
    Comment comment = new Comment();
    comment.setType(CommentType.PRODUCT);
    comment.setContent("content~");
    comment.setUserId(this.getService().getCurrentUser().getId());
    comment.setObjId("1s60089t");

    int ret = commentService.addComment(comment);

    Assert.assertNotNull(ret);

    //更新一个评论
    comment.setContent("updated content");

    commentService.updateCommentById(comment);

    Comment retComment = commentService.getCommentById(comment.getId());

    Assert.assertEquals(retComment.getContent(), "updated content");

    comment.setContent("updated content again");

    commentService.updateCommentById(comment);

    retComment = commentService.getCommentById(comment.getId());

    Assert.assertEquals(retComment.getContent(), "updated content again");

    //点赞
    CommentLike like = new CommentLike();
    like.setCommentId(retComment.getId());
    like.setUserId("u427561");
    commentService.likeComment(like);

    like.setUserId("s11761");
    like.setCommentId(retComment.getId());
    commentService.likeComment(like);

    retComment = commentService.getCommentById(comment.getId());
    Assert.assertEquals(retComment.getLikeCount().intValue(), 2);

    //重复点赞测试
    ret = commentService.likeComment(like);
    Assert.assertEquals(-1, ret);

    //屏蔽一个评论
    String commentId = comment.getId();
    commentService.blockComment(commentId);

    retComment = commentService.getCommentById(commentId);
    Assert.assertNull(retComment);

    //解除对一个评论的回复
    commentService.unblockComment(commentId);

    retComment = commentService.getCommentById(commentId);
    Assert.assertNotNull(retComment);

    //添加回复
    CommentReply reply = new CommentReply();
    reply.setCommentId(comment.getId());
    reply.setContent("my reply");
    reply.setFromUserId("s11761");
    reply.setToUserId(comment.getUserId());

    commentService.addCommentReply(reply);

    CommentReply retReply = commentService.getCommentReplyById(reply.getId());

    Assert.assertEquals("my reply", retReply.getContent());

    //修改回复
    reply.setContent("my reply updated");

    commentService.updateCommentReplyById(reply);

    retReply = commentService.getCommentReplyById(reply.getId());

    Assert.assertEquals("my reply updated", retReply.getContent());

    //屏蔽对回复的屏蔽
    String replyId = reply.getId();
    commentService.blockReply(replyId);

    retReply = commentService.getCommentReplyById(replyId);
    Assert.assertNull(retReply);

    //解除对一个回复的屏蔽
    commentService.unblockReply(replyId);

    retReply = commentService.getCommentReplyById(replyId);
    Assert.assertNotNull(retReply);

    //获取评论的VO数据
    final List<CommentVO> comments = commentService
        .getCommentsVO("1s60089t", this.getService().getCurrentUser().getId(), 0, 5);
    System.out.println(JSON.toJSONString(comments));

    //取消点赞
    commentService.cancelLikeByCommentIdAndUserId(like);

    retComment = commentService.getCommentById(comment.getId());

    Assert.assertEquals(retComment.getLikeCount().intValue(), 1);

    //取消点赞一个还未点赞的评论
    like.setUserId("p31081");

    ret = commentService.cancelLikeByCommentIdAndUserId(like);

    Assert.assertEquals(-1, ret);

    //删除回复
    commentService.deleteCommentReplyById(reply.getId());

    retReply = commentService.getCommentReplyById(reply.getId());

    Assert.assertNull(retReply);

    //删除评论
    commentService.deleteCommentById(comment.getId());

    retComment = commentService.getCommentById(comment.getId());

    Assert.assertNull(retComment);

  }


  /**
   * 测试返回到web端的数据
   */
  @Test
  public void testGetCommentsVO() {
    final List<CommentVO> comments = commentService.getCommentsVO("f75d", "1s60089t", 0, 5);
    System.out.println(JSON.toJSONString(comments));
  }

  /**
   * 测试返回到后台评的数据接口
   */
  @Test
  public void testManageComments() {
//        List<CommentManagment> comments = commentService.getCommentManagment(null, Boolean.TRUE, Boolean.TRUE, null, null);
//        System.out.println(JSON.toJSONString(comments,true));
  }

  @Override
  public CommentService getService() {
    return this.commentService;
  }
}