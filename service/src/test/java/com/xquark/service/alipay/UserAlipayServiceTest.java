package com.xquark.service.alipay;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.User;
import com.xquark.dal.model.UserAlipay;
import com.xquark.service.test.BaseEntityServiceTest;

@FixMethodOrder(MethodSorters.DEFAULT)
public class UserAlipayServiceTest extends
    BaseEntityServiceTest<UserAlipay, UserAlipayService> {

  @Autowired
  private UserAlipayService userAlipayService;

  private UserAlipay userAlipay;

  public UserAlipayServiceTest() {
    super(UserAlipay.class);
  }

  protected UserAlipay newEntityInstance() {
    UserAlipay bean = new UserAlipay();
    bean.setAccount("aaaa");
    bean.setName("dddd");
    bean.setUserId(userAlipayService.getCurrentUser().getId());
    return bean;
  }

  @Test
  public void testInsert() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));
  }

  @Test
  public void testLoad() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));

    UserAlipay bean = userAlipayService.load(userAlipay.getId());
    Assert.assertNotNull(bean);
  }

  @Test
  public void testLoadByUserId() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));

    UserAlipay bean = userAlipayService.loadByUserId(userAlipay.getUserId());
    Assert.assertNotNull(bean);
  }

  @Test
  public void testUpdate() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));

    rc = userAlipayService.update(userAlipay);
    Assert.assertTrue(rc == 1);
  }

  @Test
  public void testDelete() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));

    rc = userAlipayService.delete(userAlipay.getId());
    Assert.assertTrue(rc == 1);
    Assert.assertNull(userAlipayService.load(userAlipay.getId()));
  }

  @Test
  public void testUndelete() {
    userAlipay = newEntityInstance();
    int rc = userAlipayService.insert(userAlipay);
    Assert.assertTrue(rc == 1);
    Assert.assertTrue(StringUtils.isNotBlank(userAlipay.getId()));

    rc = userAlipayService.delete(userAlipay.getId());
    Assert.assertTrue(rc == 1);
    Assert.assertNull(userAlipayService.load(userAlipay.getId()));

    rc = userAlipayService.undelete(userAlipay.getId());
    Assert.assertTrue(rc == 1);
    Assert.assertNotNull(userAlipayService.load(userAlipay.getId()));
  }

  @Override
  public UserAlipayService getService() {
    return this.userAlipayService;
  }
}
