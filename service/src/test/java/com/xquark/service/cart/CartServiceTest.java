package com.xquark.service.cart;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.CartItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.service.cart.vo.CartItemVO;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.test.MockServiceTest;

public class CartServiceTest extends MockServiceTest {

  @Autowired
  private CartService cartService;

  @Before
  public void init() {
    super.init();
    Shop shop = mockShop(seller.getId(), "Best Shop Ever");
    this.shop = shop;

    ProductVO product = mockProduct(shop.getId(), seller.getId());
    this.product = product;
  }

  @Test
  public void testAddToCart() {
    this.setAuthUser(buyer);
    Sku sku = product.getSkus().get(0);
    CartItem cartItem = cartService.addToCart(buyer.getId(), sku.getId(), 2, "", "");

    Assert.assertEquals(sku.getId(), cartItem.getSkuId());
    Assert.assertEquals(2, cartItem.getAmount().intValue());
    Assert.assertEquals(buyer.getId(), cartItem.getUserId());

    cartItem = cartService.load(cartItem.getId());
    Assert.assertEquals(sku.getId(), cartItem.getSkuId());
    Assert.assertEquals(2, cartItem.getAmount().intValue());
    Assert.assertEquals(buyer.getId(), cartItem.getUserId());

    cartItem = cartService.loadBySku(sku.getId());
    Assert.assertEquals(sku.getId(), cartItem.getSkuId());
    Assert.assertEquals(2, cartItem.getAmount().intValue());
    Assert.assertEquals(buyer.getId(), cartItem.getUserId());

    List<CartItemVO> itemList = cartService.listCartItems();
    Assert.assertEquals(1, itemList.size());
    Assert.assertEquals(sku.getId(), itemList.get(0).getSkuId());
    Assert.assertEquals(2, itemList.get(0).getAmount().intValue());
    Assert.assertEquals(buyer.getId(), itemList.get(0).getUserId());

    itemList = cartService.listCartItems(shop.getId());
    Assert.assertEquals(1, itemList.size());
    Assert.assertEquals(sku.getId(), itemList.get(0).getSkuId());
    Assert.assertEquals(2, itemList.get(0).getAmount().intValue());
    Assert.assertEquals(buyer.getId(), itemList.get(0).getUserId());
  }

  public void testCheckout() {
    this.setAuthUser(buyer);
    Sku sku = product.getSkus().get(0);
    CartItem cartItem = cartService.addToCart(buyer.getId(), sku.getId(), 2, "", "");
    Assert.assertNotNull(cartItem);
    Set<String> skuIds = new HashSet<String>();
    skuIds.add(sku.getId());
    List<CartItemVO> list = cartService.checkout(skuIds);
    Assert.assertNotNull(list);
  }

  public void testFindAndAdjustCartItem() {
    String skuId = "1";
    int delta = 1;
    CartItem cartItem1 = cartService.loadBySku(skuId);
    int amount = cartItem1 == null ? 0 : cartItem1.getAmount().intValue();

//		cartService.adjustCartItem(skuId, delta);

    CartItem cartItem2 = cartService.loadBySku(skuId);
    Assert.assertNotNull(cartItem2);

    Assert.assertEquals(amount + delta, cartItem2.getAmount().intValue());
  }

  @Test
  public void testListCartItems() {
//		CartItem cartItem1 = newEntityInstance();
    // cartService.insert(cartItem1);

    List<CartItemVO> list = cartService.listCartItems();
    Assert.assertTrue(list.size() == 0);
  }

  public void testUpdateCartItem() {
//		CartItem cartItem1 = newEntityInstance();
    // cartService.insert(cartItem1);

//		int amount = 10;
//		cartService.updateCartItem(cartItem1.getSkuId(), amount);

//		CartItem cartItem2 = cartService.loadBySku(cartItem1.getSkuId());
//		Assert.assertEquals(amount, cartItem2.getAmount().intValue());
  }

  public void testClearCart() {
//		CartItem cartItem1 = newEntityInstance();
    // cartService.insert(cartItem1);

//		CartItem cartItem2 = cartService.load(cartItem1.getId());
//		Assert.assertNotNull(cartItem2);
//
//		cartService.clear();
//		CartItem cartItem3 = cartService.load(cartItem1.getId());
//		Assert.assertNull(cartItem3);
  }
}
