package com.xquark.service.app;


import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.AppVersion;
import com.xquark.dal.type.OSType;
import com.xquark.service.test.BaseEntityServiceTest;

public class AppVersionServiceTest extends BaseEntityServiceTest<AppVersion, AppVersionService> {

  public AppVersionServiceTest() {
    super(AppVersion.class);
  }

  @Autowired
  private AppVersionService appVersionService;

  @Test
  public void testFindCurrentVersion() {
    AppVersion appVersion = appVersionService.findCurrentVersion(1, OSType.IOS);
    Assert.assertEquals("1.0.12", appVersion.getName());
  }

  @Override
  public void testLoadAndArchive() {
  }

  @Override
  public AppVersionService getService() {
    return appVersionService;
  }
}
