package com.xquark.service.union;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.Commission;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.status.CommissionStatus;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.test.MockServiceTest;

public class UnionServiceTest extends MockServiceTest {

  @Autowired
  protected UnionService unionService;

  @Before
  public void init() {
    super.init();

    Shop shop = mockShop(seller.getId(), "Best Shop Ever");
    this.shop = shop;

    ProductVO product = mockProduct(shop.getId(), seller.getId());
    this.product = product;
  }

  @Test
  public void testCalc() {
//		User unionUser = mockUser("xiangqu");
//		Order order = mockDanbaoOrder();

    Order order = orderService.load("1r5fza75");
    Order record = new Order();
    record.setId(order.getId());
//		record.setUnionId(unionUser.getId());
    record.setTotalFee(BigDecimal.valueOf(20));
    orderService.update(record);

    unionService.calc(order.getId());
    if (!StringUtils.isEmpty(order.getUnionId())) {
      List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
      for (OrderItem orderItem : orderItems) {
        Commission comm = unionService.loadByOrderItem(orderItem.getId());
        Assert.assertEquals(orderItem.getSkuId(), comm.getSkuId());
        Assert.assertEquals(orderItem.getAmount(), comm.getAmount());
        Assert.assertEquals(order.getUnionId(), comm.getUserId());
        Assert.assertEquals(comm.getFee().setScale(4, BigDecimal.ROUND_HALF_UP),
            new BigDecimal(comm.getRate() * comm.getAmount()));
        Assert.assertEquals(CommissionStatus.NEW, comm.getStatus());
      }
    }
  }
}
