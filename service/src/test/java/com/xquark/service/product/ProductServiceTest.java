package com.xquark.service.product;

import com.xquark.dal.model.Product;
import com.xquark.dal.model.ProductImage;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.SkuMapping;
import com.xquark.dal.model.Tag;
import com.xquark.dal.model.User;
import com.xquark.dal.status.ProductStatus;
import com.xquark.dal.type.ProductType;
import com.xquark.service.product.vo.ProductVO;
import com.xquark.service.shop.ShopService;
import com.xquark.service.test.BaseEntityServiceTest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class ProductServiceTest extends BaseEntityServiceTest<Product, ProductService> {

  @Autowired
  private ProductService productService;

  @Autowired
  private ShopService shopService;

  public ProductServiceTest() {
    super(Product.class);
  }

  protected Product newEntityInstance() {
    User user = (User) productService.getCurrentUser();
    Shop shop = shopService.load(user.getShopId());//shopService.findByUser(user.getId());
    Product product = new Product();
    product.setRecommend(false);
    product.setName("test");
    product.setUserId(user.getId());
    product.setShopId(shop.getId());
    product.setStatus(ProductStatus.ONSALE);
    product.setDescription("znu991");
    product.setDelayed(1);
    product.setDelayAt(7);
    return product;
  }


  private List<Tag> newTags() {
    List<Tag> tags = new ArrayList<Tag>();
    Tag tag = new Tag();
    tag.setTag("女装");
    tags.add(tag);

    tag = new Tag();
    tag.setTag("男装");
    tags.add(tag);
		/*
		tag = new Tag();
		tag.setTag("中装");
		tags.add(tag);
		*/
    return tags;
  }

  private List<ProductImage> newImgs() {
    List<ProductImage> imgs = new ArrayList<ProductImage>();
    ProductImage img = new ProductImage();
    img.setImg("qn@xaya@Fova8f4fD6wwo_avjzg49BH2xHUx");
    imgs.add(img);

    img = new ProductImage();
    img.setImg("qn@xaya@FuVB6MlHhKwWJu-6uVxYSG95V5AJ");
    imgs.add(img);
    return imgs;
  }

  @Test
  public void testLoadCommissionRate() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    BigDecimal prod_rate = new BigDecimal(0);
    BigDecimal shop_rate = BigDecimal.valueOf(0.2);
    product.setCommissionRate(prod_rate);
    productService.create(product, skus, tags, null, imgs, skuMppings);
    Shop shop = shopService.load(product.getShopId());
    Shop editShop = new Shop();
    editShop.setId(shop.getId());
    editShop.setCommisionRate(shop_rate);
    shopService.update(editShop);

    BigDecimal cmRate = productService.loadCommissionRate(product.getId(), null);
//		System.out.println("cmRate=" + cmRate);"1s45q56p"
    Assert.assertTrue(shop_rate.setScale(4, BigDecimal.ROUND_HALF_DOWN).compareTo(cmRate) == 0);
  }

  @Test
  public void testLoad() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    //去重判断
    Tag tag = new Tag();
    tag.setTag("女装");
    tags.add(tag);

    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);
    ProductVO productvo = productService.load(product.getId());
    Assert.assertEquals(productvo.getName(), "test");
    Assert.assertEquals(productvo.getSkus().size(), 3);
    Assert.assertEquals(productvo.getTags().size(), 2);
  }

  @Test
//	@Transactional(propagation=Propagation.NOT_SUPPORTED)
  public void testCreate() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
//		List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    int rc = productService.create(product, skus, tags, null, imgs, null);
    Assert.assertTrue(rc == 1);
    Assert.assertNotNull(product.getId());
    for (Sku sku : skus) {
      Assert.assertNotNull(sku.getId());
    }

    for (ProductImage img : imgs) {
      Assert.assertNotNull(img.getId());
    }

    ProductVO productVO = productService.load(product.getId());
    Assert.assertTrue(productVO.getImgs().size() > 0);
    Assert.assertTrue(productVO.getDelayed() == 1);
    Assert.assertTrue(productVO.getDelayAt() == 7);
  }

  private List<Sku> newSkus() {
    List<Sku> skus = new ArrayList<Sku>();
    Sku sku = new Sku();
    sku.setAmount(0);
    sku.setPrice(BigDecimal.ZERO);
//		sku.setSpec("test1");
    sku.setSpec1("红色");
    sku.setSpec2("M");
    skus.add(sku);

    sku = new Sku();
    sku.setAmount(0);
    sku.setPrice(BigDecimal.ZERO);
//		sku.setSpec("test3");
    sku.setSpec1("红色");
    sku.setSpec2("L");
    skus.add(sku);

    sku = new Sku();
    sku.setAmount(0);
    sku.setPrice(BigDecimal.ZERO);
//		sku.setSpec("test3");
    sku.setSpec1("黄色");
    sku.setSpec2("M");
    skus.add(sku);
    return skus;
  }

  private List<SkuMapping> newSkuMappings() {
    List<SkuMapping> skuMappings = new ArrayList<SkuMapping>();
    SkuMapping skuMapping = new SkuMapping();
    skuMapping.setSpecKey("spec1");
    skuMapping.setSpecName("颜色分类");
    skuMappings.add(skuMapping);

    skuMapping = new SkuMapping();
    skuMapping.setSpecKey("spec2");
    skuMapping.setSpecName("尺寸");
    skuMappings.add(skuMapping);

    return skuMappings;
  }

  @Test
  public void testUpdate() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);
    ProductVO productVO = productService.load(product.getId());

    Assert.assertTrue(productVO.getSkus().size() > 0);
    Assert.assertTrue(productVO.getTags().size() == 2);
    Assert.assertTrue(productVO.getImgs().size() == 2);

    product.setName("new test");
    for (Sku sku : skus) {
      sku.setSpec("new spec");
    }

    imgs = new ArrayList<ProductImage>();
    ProductImage img = new ProductImage();
    img.setImg("qn@xaya@Fova8f4fD6wwo_avjzg49BH2xHUx");
    imgs.add(img);

    tags = new ArrayList<Tag>();
    Tag tag = new Tag();
    tag.setTag("女装");
    tags.add(tag);

    product.setDelayed(0);
    product.setDelayAt(0);

    int rc = productService.update(product, skus, tags, imgs, skuMppings);

    Assert.assertTrue(rc == 1);

    Assert.assertEquals(product.getName(), "new test");
    for (Sku sku : skus) {
      Assert.assertEquals(sku.getSpec(), "new spec");
    }

    productVO = productService.load(product.getId());
    Assert.assertTrue(productVO.getImgs().size() == 1);
    Assert.assertTrue(productVO.getTags().size() == 1);
    Assert.assertTrue(productVO.getDelayed() == 0);
    Assert.assertTrue(productVO.getDelayAt() == 0);
  }

  @Test
  public void testArchive() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);
    productService.delete(product.getId());
    product = productService.load(product.getId());
    Assert.assertNull(product);
  }

  @Test
  public void testUndelete() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);
    productService.undelete(product.getId());
    ProductVO productVO = productService.load(product.getId());
    Assert.assertEquals(productVO.getArchive(), false);
  }

//	@Test
//	public void testLoadAndArchive() {
//	}

//	@Test
//	public void testListProductsByOnsaleAt() {
//		// Product product = newEntityInstance();
//		// productService.insert(product);
//		// productService.insert(product);
//		// productService.insert(product);
//		// productService.insert(product);
//		PageHelper page = new PageHelper();
//		List<Product> products = productService.listProductsBySales(IdTypeHandler.encode(1), page);
//		Assert.assertTrue(products.size() > 0);
//	}
//
//	@Test
//	public void testListProductsBySales() {
//		Product product = newEntityInstance();
//		PageHelper page = new PageHelper(); 
//		List<Product> products = productService.listProductsBySales(product.getUserId(), page);
//		Assert.assertTrue(products.size() > 0);
//	}
//
//	@Test
//	public void testListProductsByAmount() {
//		Product product = newEntityInstance();
//		PageHelper page = new PageHelper(); 
//		List<Product> products = productService.listProductsByAmount(product.getUserId(), page);
//		Assert.assertTrue(products.size() > 0);
//	}
//
//	@Test
//	public void testListProductsBySoldout() {
//		Product product = newEntityInstance();
//		PageHelper page = new PageHelper(); 
//		List<Product> products = productService.listProductsBySoldout(product.getUserId(), page);
//		Assert.assertTrue(products.size() > 0);
//	}
//	
//	@Test
//	public void listProductsByRelated() {
//		Product product = newEntityInstance();
//		List<Sku> skus = newSkus();
//		List<Tag> tags = newTags();
//		List<ProductImage> imgs = newImgs();
//		if(imgs.size() > 0){
//			product.setImg(imgs.get(0).getImg());
//		}
//		productService.create(product, skus, tags, imgs);
//		
//		Product product1 = newEntityInstance();
//		List<Sku> skus1 = newSkus();
//		List<Tag> tags1 = new ArrayList<Tag>();
//		List<ProductImage> imgs1 = newImgs();
//		if(imgs1.size() > 0){
//			product1.setImg(imgs1.get(0).getImg());
//		}
//		Tag tag = new Tag();
//		tag.setTag("女装");
//		tags1.add(tag);
//		
//		tag = new Tag();
//		tag.setTag("男装new");
//		tags1.add(tag);
//		productService.create(product1, skus1, tags1, imgs1);
//		PageHelper page = new PageHelper(); 
//		List<Product> products = productService.listProductsByRelated(product.getShopId(), product.getId(), page);
//		Assert.assertTrue(products.size() > 0);
//		
//	}

  @Test
  public void testSearch() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);
    List<Product> products = productService.search(product.getShopId(), "t", ProductType.NORMAL);
    Assert.assertTrue(products.size() > 0);

    products = productService.search(product.getShopId(), "test1", ProductType.NORMAL);
    Assert.assertTrue(products.size() == 0);
  }

  @Test
  public void testCountProductsByStatus() {
    Product product = newEntityInstance();
    List<Sku> skus = newSkus();
    List<SkuMapping> skuMppings = newSkuMappings();
    List<Tag> tags = newTags();
    List<ProductImage> imgs = newImgs();
    if (imgs.size() > 0) {
      product.setImg(imgs.get(0).getImg());
    }
    productService.create(product, skus, tags, null, imgs, skuMppings);

    Long l = productService.countProductsByStatus(product.getShopId(), ProductStatus.ONSALE);
    Assert.assertTrue(l > 0);

  }

  @Test
  public void testDelayProductList() {
    testCreate();
    Product product = newEntityInstance();
    Pageable page = new PageRequest(0, 20);
    long total = productService.countDelayProduct(product.getShopId());
    List<Product> products = null;
    if (total > 0) {
      products = productService.listDelayProduct(product.getShopId(), page);
    } else {
      products = new ArrayList<Product>();
    }
    Assert.assertTrue(total == products.size());
    System.out
        .println("DelayProductList,total count:" + total + " productList size:" + products.size());
  }

  @Test
  public void testList() throws ParseException {
    /**Product product = newEntityInstance();
     product.setRecommend(true);
     //		List<Sku> skus = newSkus();
     //		List<Tag> tags = newTags();
     List<ProductImage> imgs = newImgs();
     if(imgs.size() > 0){
     product.setImg(imgs.get(0).getImg());
     }
     //productService.create(product, skus, tags, imgs);
     // Order order = new Order(Direction.DESC, "onsaleAt");
     // ort sort = new Sort(order);
     Pageable page = new PageRequest(0, 20);
     List<Product> products = productService.listProductsByRecommend(product.getShopId(), page);

     products = productService.listProductsByAmount(product.getShopId(), "", page, Direction.DESC);

     Map<String, Object> params = new HashMap<String, Object>();
     Calendar cal = Calendar.getInstance();
     params.put("onsaleAt1", cal.getTime());
     cal.add(Calendar.DAY_OF_MONTH, 1);
     params.put("onsaleAt2", cal.getTime());
     products = productService.listProductsByOnsaleAt(product.getShopId(), "", page, params);
     System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "", "default", new HashMap<String, Object>()) );

     products = productService.listProductsByRelated(product.getShopId(), product.getId(), page);
     //System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "default", new HashMap<String, Object>()) ); // 没实现

     products = productService.listProductsBySoldout(product.getShopId(), "", page, Direction.DESC);
     System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "", "soldout", new HashMap<String, Object>()) );

     products = productService.listProductsByStatusDraft(product.getShopId(), "", page);
     System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "", "statusDraft", new HashMap<String, Object>()) );

     products = productService.listProductsByOutOfStock(product.getShopId(), "", page);
     System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "", "outofstock", new HashMap<String, Object>()) );

     products = productService.listProductsByForSale(product.getShopId(), page);
     System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "", "forsale", new HashMap<String, Object>()) );

     productService.listProductByRecently(product.getShopId(), Calendar.getInstance().getTime(), 3);
     //System.out.println( "total count:" + productService.getLastTotalCnt(product.getShopId(), "default", new HashMap<String, Object>()) );  // 没实现

     Assert.assertTrue(products.size() >= 0);**/
  }

  //public List<ProductOnSaleVO> listProductsByOnsale(Date date, int days){

  @Override
  public ProductService getService() {
    return this.productService;
  }
}
