package com.xquark.service.push;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

public class PushServiceTest {

  @Autowired
  private PushService pushService;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  public void testPushSms() {
    Assert.assertFalse(pushService.sendSmsEngine("1561876710", "测试短信"));
    // 短信测试只能是绑定ip的，所以不测了
  }

//	@Override
//	public PushService getService() {
//		return pushService;
//	}
//
//	@Override
//	public void setService(PushService svc) {
//		pushService = svc;
//	}
}
