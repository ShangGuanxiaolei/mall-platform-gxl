package com.xquark.service.user;

import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.service.test.BaseEntityServiceTest;

public class UserServiceTest extends BaseEntityServiceTest<User, UserService> {

  public UserServiceTest() {
    super(User.class);
  }

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Autowired
  private UserService userService;

  private static final String testUserMobile = "13606801234";

  private static final String testUserPassword = "123456";

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Override
  protected void doInit() {
    super.doInit();
  }

  @Test
  public void testRegisterNullUser() {
    thrown.expect(ConstraintViolationException.class);
    userService.register(null, null);
  }

  @Test
  public void testRegister() {
    User user = userService.register(testUserMobile, testUserPassword);
    Assert.assertEquals(user.getLoginname(), testUserMobile);

    User userExists = userService.register(testUserMobile, testUserPassword);
    Assert.assertEquals("重复注册OK", user.getId(), userExists.getId());
  }

  @Test
  public void testLoadUserByUsernameNonExists() {
    thrown.expect(UsernameNotFoundException.class);
    userService.loadUserByUsername("non_exists");
  }

  @Test
  public void testLoadUserByUsername() {
    testRegister();
    UserDetails user = userService.loadUserByUsername(testUserMobile);
    Assert.assertNotNull("load existing user", user);
  }

  @Test
  public void testChangeUser() {
    User user = userService.register(testUserMobile, testUserPassword);
    Assert.assertEquals(user.getLoginname(), testUserMobile);

    mockCurrentUser(user);

    Assert.assertTrue(userService.isPwdSet());

    boolean changed = userService.changePwd(testUserPassword, "mima123");
    Assert.assertTrue(changed);

    changed = userService.changePwd("mima123", "mima456");
    Assert.assertTrue("重复更改密码", changed);

    Assert.assertTrue(userService.isPwdSet());

    User userUpdate = new User();
    userUpdate.setId(user.getId());
    userUpdate.setEmail("a@a.a");
    userUpdate.setName("name1");
    userService.updateUserInfo(userUpdate);
  }

  @Test
  public void testUpdateNameAndIdCardNumByPrimaryKey() {
    int i = userService
        .updateNameAndIdCardNumByPrimaryKey(IdTypeHandler.encode(20173), "名字", "111111");
    Assert.assertTrue(i == 1);
  }

  @Override
  public UserService getService() {
    return userService;
  }

  public User newEntityInstance() {
    User user = new User();
    user.setLoginname(testUserMobile);
    user.setPhone(testUserMobile);
    user.setPassword(pwdEncoder.encode(testUserPassword));
    return user;
  }

  @Test
  public void testIncomeChange() {
    User user = userService.register(testUserMobile, testUserPassword);
    Assert.assertEquals(user.getLoginname(), testUserMobile);

    user = userService.loadByLoginname(testUserMobile);

    int rc = userService.saveWithDrawType(user.getId(), 1);
    Assert.assertTrue(rc == 1);
    user = userService.load(user.getId());
    Assert.assertTrue(user.getWithdrawType() == 1);
    rc = userService.saveWithDrawType(user.getId(), 0);
    Assert.assertTrue(rc == 1);
    user = userService.load(user.getId());
    Assert.assertTrue(user.getWithdrawType() == 0);
  }

}
