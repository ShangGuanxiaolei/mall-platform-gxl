package com.xquark.config;

import com.vdlm.common.bus.BusSignalManager;
import com.vdlm.common.bus.DefaultBusRegistry;
import com.xquark.service.Scanned;
import com.xquark.service.promotion.PromotionOrderSubject;
import com.xquark.service.promotion.impl.FlashSaleOrderObserver;
import com.xquark.service.promotion.impl.PreOrderOrderObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackageClasses = Scanned.class)
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
@ImportResource({"classpath:/META-INF/applicationContext-service.xml",
    "classpath:/META-INF/applicationContext-sync.xml"})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ServiceTestConfig {

  @Bean
  @Autowired
  public PromotionOrderSubject promotionOrderSubject(PreOrderOrderObserver preOrderOrderObserver,
                                                     FlashSaleOrderObserver flashSaleOrderObserver) {
    PromotionOrderSubject subject = new PromotionOrderSubject();
    subject.add(preOrderOrderObserver);
    subject.add(flashSaleOrderObserver);
    return subject;
  }

  @Bean
  BusSignalManager busSignalManager() {
    return new BusSignalManager(new DefaultBusRegistry());
  }
}