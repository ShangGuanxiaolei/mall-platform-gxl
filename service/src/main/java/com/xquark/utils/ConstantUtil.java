package com.xquark.utils;

/**
 * 打印log格式
 * @author tanggb
 * @date 2019/05/24 14:04
 */
public class ConstantUtil {

    public static int success = 1;
    public static int failed = 0;
    public static int vfailed = -1;
    public static final String ENCRYPT_PWD = "A/S62x7'cr,FRK4vjO5s";//加密字符串，用于注册和登录
    public static final String IN_PARAMETER_FORMAT = "========>>>>{}.{}->in params:{}";
    public static final String OUT_PARAMETER_FORMAT = "========>>>>{}.{}->out params:{}";
    public static final String ERROR_FORMAT = "========>>>>{}.{}->error:{}";
    public static final int PAGESIZE = 10; // 同一管理分页数据
    public static final int CURRENTPAGE = 1;
}
