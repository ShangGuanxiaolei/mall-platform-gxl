package com.xquark.utils.Email.vo;

import java.util.List;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/30
 * Time:10:21
 * Des:发邮件实体类
 */
public class HvEmail {
    public HvEmail(){}
    public HvEmail(String host, String transportProtocol, String smtpAuth){
        this.host="smtp.qiye.aliyun.com";
        this.transportProtocol="smtp";
        this.smtpAuth="true";
        this.setFrom("hdsalarm@handeson.com");
        this.setUser("hdsalarm@handeson.com");
        this.setPassword("S229rQAy35");
    }
    private String host;//邮件服务器，默认用smtp.qiye.aliyun.com
    private String transportProtocol;//SMTP服务，默认smtp
    private String smtpAuth;//默认true
    private String user;//用户
    private String password;//密码
    private String from;//发送人
    private List<String> toList;//收件人

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    private List<String> ccList;//抄送人
    private List<String> bccList;//秘密抄送人
    private String subject; //主题
    private String content; //内容

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getTransportProtocol() {
        return transportProtocol;
    }

    public void setTransportProtocol(String transportProtocol) {
        this.transportProtocol = transportProtocol;
    }

    public String getSmtpAuth() {
        return smtpAuth;
    }

    public void setSmtpAuth(String smtpAuth) {
        this.smtpAuth = smtpAuth;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getToList() {
        return toList;
    }

    public void setToList(List<String> toList) {
        this.toList = toList;
    }

    public List<String> getCcList() {
        return ccList;
    }

    public void setCcList(List<String> ccList) {
        this.ccList = ccList;
    }

    public List<String> getBccList() {
        return bccList;
    }

    public void setBccList(List<String> bccList) {
        this.bccList = bccList;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}