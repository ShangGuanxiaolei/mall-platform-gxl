package com.xquark.utils.Email;

import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.utils.Email.vo.HvEmail;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/30
 * Time:10:19
 * Des:发送邮件
 */
public class HvSendEmailUtil {

    private static final String SINGLE_EMAIL_REGEX = "(?:(?:[A-Za-z0-9\\-_@!#$%&'*+/=?^`{|}~]|(?:\\\\[\\x00-\\xFF]?)|(?:\"[\\x00-\\xFF]*\"))+(?:\\.(?:(?:[A-Za-z0-9\\-_@!#$%&'*+/=?^`{|}~])|(?:\\\\[\\x00-\\xFF]?)|(?:\"[\\x00-\\xFF]*\"))+)*)@(?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+(?:(?:[A-Za-z0-9]*[A-Za-z][A-Za-z0-9]*)(?:[A-Za-z0-9-]*[A-Za-z0-9])?))";

    /**
     * 发送
     * @param hvEmail
     * @return
     */
    public static boolean sendMsg(HvEmail hvEmail) throws MessagingException {
        if (hvEmail == null){
            return false;
        }
        Properties prop = new Properties();
        prop.put("mail.host", hvEmail.getHost());
        prop.put("mail.transport.protocol", hvEmail.getTransportProtocol());
        prop.put("mail.smtp.auth", hvEmail.getSmtpAuth());
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        prop.put("mail.smtp.socketFactory.fallback", "false");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.connectiontimeout", 2000);
        prop.put("mail.smtp.timeout", 8000);
        //使用JavaMail发送邮件的5个步骤
        //1、创建session
        Session session = Session.getDefaultInstance(prop);
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug(true);
        //2、通过session得到transport对象
        Transport ts = session.getTransport();
        //3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
        ts.connect(hvEmail.getHost(), hvEmail.getUser(), hvEmail.getPassword());
        //4、创建邮件
        Message message = createSimpleMail(session, hvEmail);
        //5、发送邮件
        ts.sendMessage(message, message.getAllRecipients());
        ts.close();
        return true;
    }

    public static MimeMessage createSimpleMail(Session session, HvEmail hvEmail) throws MessagingException {
        //创建邮件对象
        MimeMessage message = new MimeMessage(session);
        //指明邮件的发件人
        message.setFrom(new InternetAddress(hvEmail.getFrom()));
        //指明邮件的收件人
        List<String> toList = hvEmail.getToList();
        if(toList == null || toList.isEmpty()){
            return null;
        }
        InternetAddress[] toAddress = getAddress(toList);
        message.addRecipients(Message.RecipientType.TO, toAddress);

        //指明抄送人
        List<String> ccList = hvEmail.getCcList();
        if(ccList != null && !ccList.isEmpty()){
            InternetAddress[] ccAddress = getAddress(ccList);
            message.addRecipients(Message.RecipientType.CC, ccAddress);
        }

        //邮件的标题
        if(StringUtils.isNotBlank(hvEmail.getSubject())){
            message.setSubject(hvEmail.getSubject());
        }
        //邮件的文本内容
        if(StringUtils.isNotBlank(hvEmail.getContent())){
            message.setContent(hvEmail.getContent(), "text/html;charset=UTF-8");
        }
        //返回创建好的邮件对象
        return message;
    }

    /**
     * 获取邮件地址
     * @param list
     * @return
     */
    private static InternetAddress[] getAddress(List<String> list) throws AddressException {
        InternetAddress[] ccAddress = null;
        if(list != null){
            ccAddress = new InternetAddress[list.size()];
            for(int i = 0; i < list.size(); i++){
                String emailAddress = list.get(i);
                new InternetAddress(emailAddress);
                ccAddress[i]=new InternetAddress(emailAddress);
            }
        }
        return ccAddress;
    }

    //demo
    public static void main(String[] args) {
        long decode = IdTypeHandler.decode("1kw99");
        System.out.println(decode);
        HvEmail hv = new HvEmail("","","");
        List<String> list = new ArrayList<>();
        List<String> cclist = new ArrayList<>();
        list.add("yarm.yang@handeson.com");
        cclist.add("yarm.yang@handeson.com");
        cclist.add("15902124763@163.com");
        //cclist.add("Panda.Peng@handeson.com");
        hv.setToList(list);
        hv.setCcList(cclist);
        //hv.setFrom("hdsalarm@handeson.com");
        hv.setSubject("标题");
        hv.setContent("hi 你好<br>呵呵");
        //hv.setUser("hdsalarm@handeson.com");
        //hv.setPassword("S229rQAy35");
        try {
            boolean b = HvSendEmailUtil.sendMsg(hv);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        System.out.println(hv);
    }
}