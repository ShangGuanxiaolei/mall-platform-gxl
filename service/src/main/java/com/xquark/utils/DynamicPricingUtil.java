package com.xquark.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Optional;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.model.ChangePriceItem;
import com.xquark.dal.model.DynamicPricing;
import com.xquark.dal.model.DynamicPricingItem;
import com.xquark.dal.model.User;
import com.xquark.dal.vo.IdentityVO;

import java.math.BigDecimal;
import java.util.function.Consumer;

/**
 * @author wangxinhua.
 * @date 2018/11/11
 */
public class DynamicPricingUtil {

    private static final String IDENTITY_KEY = "login_return_";

    /**
     * 根据用户身份重新计算包含可计算单位的Item
     */
    public static void rePricing(DynamicPricingItem target, User user) {
        rePricing(target, user, null);
    }

    /**
     * 根据用户身份重新计算包含可计算单位的Item
     */
    public static void rePricing(DynamicPricingItem target, User user, BigDecimal cutDown) {
        DynamicPricing item = target.getDynamicPricing();
        rePricing(item, user, cutDown);
        // 把算好的价格同时赋给外层的对象
        //    target.setPrice(item.getPrice());
    }

    /**
     * 获取缓存中的身份信息
     */
    public static IdentityVO getIdentityVO(Long cpId) {
        if (cpId != null) {
            RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
            String json = redisUtils.get("login_return_" + cpId);
            if (json != null) {
                // 兼容原来的login_return代码
                JSONObject jsonObject = JSONObject.parseObject(json);
                boolean isProxy = jsonObject.getBoolean("isProxy");
                boolean isMember = jsonObject.getBoolean("isMember");
                return new IdentityVO(isProxy, isMember);
            }
        }
        return null;
    }

    /**
     * 根据身份计算立减值 *
     */
    public static BigDecimal calCutDown(Long cpId, DynamicPricing pricing) {
        IdentityVO identityVO = getIdentityVO(cpId);
        boolean isProxy = identityVO.getIsProxy();
        BigDecimal point = pricing.getReduction();
        BigDecimal val = BigDecimal.ZERO;
        if (point != null) {
            BigDecimal proxyDiscount = BigDecimal.ZERO;
            if (isProxy) {
                proxyDiscount = pricing.getServerAmt();
            }
            val = point.add(proxyDiscount);
        }
        return val;
    }

    /**
     * 根据身份计算立减值
     */
    public static BigDecimal calCutDown(Long cpId, DynamicPricingItem item) {
        BigDecimal bAmount = BigDecimal.valueOf(item.getAmount());
        return calCutDown(cpId, item.getDynamicPricing()).multiply(bAmount);
    }

    public static void rePricing(DynamicPricing target, User user) {
        rePricing(target, user, null);
    }

    public static Consumer<? super DynamicPricing> rePricing(User user) {
        return c -> rePricing(c, user);
    }

    public static BigDecimal genChangePrice(ChangePriceItem changePriceItem, User user) {
        Long cpId = user.getCpId();
        // 是否为代理
        boolean isProxy = false;
        // 是否为会员
        boolean isMember = false;
        IdentityVO identityVO = getIdentityVO(cpId);
        if (identityVO != null) {
            isProxy = identityVO.getIsProxy();
            isMember = identityVO.getIsMember();
        }

        BigDecimal price = changePriceItem.getPrice();
        BigDecimal reduction = changePriceItem.getReduction();
        BigDecimal serverAmt = changePriceItem.getServerAmt();
        BigDecimal conversionPrice = changePriceItem.getConversionPrice();

        // 总立减 - 兑换价德分部分 + 服务费
        BigDecimal subtractValue = reduction.add(serverAmt);
        // 会员价 总价减德分
        BigDecimal changePrice = BigDecimal.ZERO;
        // 如果普通价格已经为0则没必要再继续计算
        if (price.signum() > 0) {
            if (isProxy) {
                changePrice = conversionPrice.subtract(subtractValue);
            } else if (isMember) {
                changePrice = conversionPrice.subtract(reduction);
            } else {
                changePrice = conversionPrice;
            }
        }
        return changePrice;
    }

    /**
     * 根据用户身份重新计算价格
     *
     * @param cutDown 上下文计算的立减价, 如果传了该值则以传的值为准
     */
    public static void rePricing(DynamicPricing target, User user, BigDecimal cutDown) {
        if (target == null || user == null) {
            return;
        }
        Long cpId = user.getCpId();
        // 是否为代理
        boolean isProxy = user.getIsProxy();
        // 是否为会员
        boolean isMember = user.getIsMember();
        IdentityVO identityVO = getIdentityVO(cpId);
        if (identityVO != null) {
            isProxy = identityVO.getIsProxy();
            isMember = identityVO.getIsMember();
        }

        // 总立减 - 兑换价德分部分 + 服务费
        BigDecimal subtractValue = target.getReduction().add(target.getServerAmt());
        // 代理价 - 总价减立减
        BigDecimal proxyPrice = target.getPrice().subtract(subtractValue);
        target.setProxyPrice(proxyPrice);

        // 兑换价
        BigDecimal conversionPrice = target.getConversionPrice();
        BigDecimal memberPrice = target.getPrice().subtract(target.getReduction());
        // 会员价 总价减德分
        target.setMemberPrice(memberPrice);

        BigDecimal changePrice = BigDecimal.ZERO;
        // 已减
        BigDecimal reducedPrice = BigDecimal.ZERO;
        // 如果普通价格已经为0则没必要再继续计算
        BigDecimal price = Optional.fromNullable(target.getPrice()).or(BigDecimal.ZERO);
        if (price.signum() > 0) {
            if (isProxy) {
                changePrice = conversionPrice.subtract(subtractValue);
                reducedPrice = subtractValue;
            } else if (isMember) {
                changePrice = conversionPrice.subtract(target.getReduction());
                reducedPrice = target.getReduction();
            } else {
                changePrice = conversionPrice;
            }
        }
        // 上面的代码只是对原来的逻辑的兼容, 实际应该按传入的立减来算
        if (cutDown != null) {
            reducedPrice = cutDown;
        }
        price = price.subtract(reducedPrice);
        target.setChangePrice(changePrice);
        target.setReducedPrice(reducedPrice);
        //    target.setPrice(price);
    }
}
