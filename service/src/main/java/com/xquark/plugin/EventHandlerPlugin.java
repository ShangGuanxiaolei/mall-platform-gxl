package com.xquark.plugin;

import com.xquark.dal.model.Order;

public interface EventHandlerPlugin {

  void execute(Order order);

}
