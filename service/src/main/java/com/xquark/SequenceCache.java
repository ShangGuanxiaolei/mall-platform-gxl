package com.xquark;

import java.util.concurrent.atomic.AtomicLong;

/**
 * created by
 *
 * @author wangxinhua at 18-5-31 下午2:17
 */
public class SequenceCache {

  private final AtomicLong curr;

  private final AtomicLong max;

  public SequenceCache(AtomicLong curr, AtomicLong max) {
    this.curr = curr;
    this.max = max;
  }

  public AtomicLong getCurr() {
    return curr;
  }

  public AtomicLong getMax() {
    return max;
  }

  public boolean needRefresh() {
    return getCurr().get() >= getMax().get();
  }

}
