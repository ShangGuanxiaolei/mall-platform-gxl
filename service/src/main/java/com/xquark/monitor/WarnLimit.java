package com.xquark.monitor;

import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author jim
 * @description WarnLimit
 * @date 2019-01-29、12:59
 * 相同异常限制次数
 * 限制环境，只发prod
 *
 */
public class WarnLimit {
    /**
     * 是否可以发送预警
     *
     * @param warnEntity
     * @return true可以发送，false不发送
     */
    public static boolean confirm(WarnEntity warnEntity) {
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);

        final String uriFlag = redisUtils.get(warnEntity.getUriKey());
        if (uriFlag != null || !"".equals(uriFlag) ) {
            return false;
        }

        final String exceptionFlag = redisUtils.get(warnEntity.getExceptionKey());
        if (exceptionFlag != null && Long.valueOf(exceptionFlag) >= 3) {
            return false;
        }

        return true;
    }

    /**
     * 预警存入缓存
     * @param warnEntity
     * @param expire 在缓存中保存多久,单位：分钟
     */
    public static void recordWarn(WarnEntity warnEntity,Long expire) {
        RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class);
        redisUtils.setAndExpire(warnEntity.getUriKey(),"T_T",expire, TimeUnit.MINUTES);
        redisUtils.increment(warnEntity.getExceptionKey(),1L);
        redisUtils.expire(warnEntity.getExceptionKey(),expire,TimeUnit.MINUTES);
    }

}
