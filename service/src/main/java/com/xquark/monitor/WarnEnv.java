package com.xquark.monitor;

/**
 * @author jim
 * @description WarnEnv
 * @date 2019-01-30、11:38
 */
public enum WarnEnv {
    prod,dev,sit,uat,uat2,test,stage,innertest
}
