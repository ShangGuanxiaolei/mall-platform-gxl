package com.xquark.monitor;

/**
 * @author jim
 * @description WarnSystemName
 * @date 2019-01-29、16:18
 */
public enum WarnSystemName {
    REST_API,WEB,PC,TASK,BOS,SMS
}
