package com.xquark.monitor.drive.channel;

import com.xquark.monitor.WarnEntity;
import com.xquark.monitor.WarnLimit;
import com.xquark.monitor.drive.MonitorWarn;
import com.xquark.utils.Email.HvSendEmailUtil;
import com.xquark.utils.Email.vo.HvEmail;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;


/**
 * @author jim
 * @description EmailWarn
 * @date 2019-01-28、13:56
 */
public class EmailWarn extends MonitorWarn {

    private static final Log logger = LogFactory.getLog(EmailWarn.class);

    //sender info
    private final String senderAccount = "hdsalarm@handeson.com";
    private final String senderPassword = "S229rQAy35";
    private final String senderServer = "smtp.qiye.aliyun.com";
    private final String senderPort = "645";

    //recipient info
    private final String[] recipients = new String[]{
            "jim.yin@handeson.com" //jim
            , "ginger.liu@handeson.com" //ginger
            , "damon.song@handeson.com" //damon

    };

    @Override
    public void sendMonitorMsg(WarnEntity warnEntity) {
        /*~~信息概要整理~~*/

        /*缓存判断是否满足发送条件*/
        if(!WarnLimit.confirm(warnEntity)) {
            logger.info("此次异常，不满足邮件通知频次要求");
            return;
        }
        /*详细信息整理*/
        warnEntity.generateContent();
        /*发送邮件*/
        this.sendEmailFromUtil(warnEntity);
        WarnLimit.recordWarn(warnEntity,super.expire);
    }

    private boolean sendEmailFromUtil(WarnEntity warnEntity) {
        HvEmail hv = new HvEmail("","","");
        List toList = Arrays.asList(recipients);
        hv.setToList(toList);
        hv.setSubject("【** env:" + warnEntity.getSystemEnv() + " **】[hv-mall system warn]汉薇商城系统异常邮件预警");//标题
        hv.setContent(warnEntity.getContent());//内容

        boolean flag = false;
        try {
            flag = HvSendEmailUtil.sendMsg(hv);
        } catch (MessagingException e) {
            logger.error("发送邮件异常");
        }
        return flag;
    }

    private void sendEmail(WarnEntity warnEntity) {
        try {
            Properties properties = new Properties();
            properties.put("mail.transport.protocol", "smtp");// 连接协议
            properties.put("mail.smtp.host", senderServer);// 主机名
            properties.put("mail.smtp.port", senderPort);// 端口号
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.socketFactory.port", senderPort);
            properties.put("mail.smtp.connectiontimeout", 2000);
            properties.put("mail.smtp.timeout", 8000);


            Session session = Session.getInstance(properties);

            Message message = new MimeMessage(session);
            // 设置发件人邮箱地址
            message.setFrom(new InternetAddress(senderAccount));

            // 设置收件人邮箱地址
            InternetAddress[] internetAddressList = new InternetAddress[recipients.length];
            for ( int i = 0 ; i < recipients.length ; i++ ) {
                internetAddressList[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, internetAddressList);

            // 设置邮件标题
            message.setSubject("【** env:" + warnEntity.getSystemEnv() + " **】[hv-mall system warn]汉薇商城系统异常邮件预警");
            // 设置邮件内容
            message.setText(warnEntity.getContent());
            // 得到邮差对象
            Transport transport = session.getTransport();
            // 连接自己的邮箱账户
            transport.connect(senderAccount, senderPassword);// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
            // 发送邮件
            transport.sendMessage(message, message.getAllRecipients());
            logger.info("warning mail has been sent");
            transport.close();
        } catch (AddressException e) {
            logger.error(e);
        } catch (NoSuchProviderException e) {
            logger.error(e);
        } catch (MessagingException e) {
            logger.error(e);
        }
    }


}
