package com.xquark.monitor.drive;

import com.xquark.monitor.WarnEntity;
import com.xquark.monitor.drive.channel.EmailWarn;

/**
 * @author
 * @description MonitorWarn
 * @date 2019-01-28、11:49
 */
public abstract class MonitorWarn {

    //过期时间，单位分钟
    protected Long expire = 60L;

    /**
     * 推送监控信息
     * @param warnEntity
     */
    public abstract void sendMonitorMsg(WarnEntity warnEntity);

    public static MonitorWarn getInstance() {
        /**
         * 当前只发邮件，新增其他途径后，再添加路由
         */
        return new EmailWarn();
    }

    public Long getExpire() {
        return expire;
    }

    public void setExpire(Long expire) {
        this.expire = expire;
    }
}
