package com.xquark.monitor;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author jim
 * @description WarnEntity
 * @date 2019-01-28、11:51
 */
public class WarnEntity {
    private WarnSystemName systemName;
    private Exception exception;
    private HttpServletRequest request;
    private String content;
    private String uriKey;
    private String exceptionKey;
    private WarnEnv systemEnv;


    /**
     * 除了content在确定需要预警时生成，其他在构造方法中全部生成
     * @param systemName
     * @param exception
     * @param request
     */
    public WarnEntity(WarnEnv systemEnv,WarnSystemName systemName,Exception exception , HttpServletRequest request) {
        this.systemEnv = systemEnv;
        this.systemName = systemName;
        this.exception = exception;
        this.request = request;
        this.generateURIKey();
        this.generateExceptionKey();
    }


    /**
     * 生成URI为维度的cache key
     */
    private void generateURIKey() {
        StringBuffer key = new StringBuffer();

        //systemname
        if (this.systemName != null) {
            key.append(this.systemName);
        } else {
            key.append("undefinded-sys");
        }
        key.append("_");

        //uri
        key.append("_");
        if (this.request != null ) {
            key.append(this.request.getRequestURI());
        } else {
            key.append("uri-null");
        }

        //exception class name
        key.append("_");
        if (this.exception != null ) {
            key.append(this.exception.getClass().getName());
        } else {
            key.append("exception-null");
        }

        this.uriKey = key.toString();
    }

    /**
     * 生成异常为维度的cache key
     */
    private void generateExceptionKey() {
        StringBuffer key = new StringBuffer();
        //systemname
        if (this.systemName != null) {
            key.append(this.systemName);
        } else {
            key.append("undefinded-sys");
        }

        if (this.exception != null ) {
            key.append(this.exception.getClass().getName());
        } else {
            key.append("exception-null");
        }
        this.exceptionKey = key.toString();
    }

    /**
     * 生成 content
     */
    public void generateContent() {
        if(this.request == null || this.exception == null || this.systemEnv == null) {
            this.setContent("i'm so sorry T_T, request || exception || env is null");
            return;
        }

        StringBuffer content = new StringBuffer();

        if (WarnEnv.prod.equals(this.systemEnv)) {
            content.append("<font size =\"5\"><b>========Exception from prod!!!========</b></font>");
            content.append("\n");
            content.append("\n");
            content.append("\n");
        }
        content.append("[Time]:"+ new Date(System.currentTimeMillis()));
        content.append("\n");
        content.append("[Request URL]:" + this.request.getRequestURL());
        content.append("\n");
        content.append("[Parameters]:" + JSONObject.toJSONString(this.request.getParameterMap()));
        content.append("\n");
        content.append("[Exception Class]:" + this.exception.getClass());
        content.append("\n");
        content.append("[Exception Message]:" + this.exception.getMessage());
        content.append("\n");
        content.append("========[Stack Trace]:========\n" + getStackMsg(this.exception.getStackTrace()));
        content.append("\n");
        if (this.exception.getCause() != null) {
            content.append("========[Cause]:========\n" + getStackMsg(this.exception.getCause().getStackTrace()));
        }
        this.setContent(content.toString());
    }


    /**
     * 堆栈信息 toString
     * @param stackArray
     * @return
     */
    private String getStackMsg(StackTraceElement[] stackArray) {
        if (stackArray == null || stackArray.length == 0) return "i'm so sorry T_T, this stack trace is null";

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < stackArray.length; i++) {
            StackTraceElement element = stackArray[i];
            sb.append(element.toString() + "\n");
        }
        return sb.toString();
    }

    public String getUriKey() {
        return uriKey;
    }

    public void setUriKey(String uriKey) {
        this.uriKey = uriKey;
    }

    public String getExceptionKey() {
        return exceptionKey;
    }

    public void setExceptionKey(String exceptionKey) {
        this.exceptionKey = exceptionKey;
    }

    public WarnSystemName getSystemName() {
        return systemName;
    }

    public void setSystemName(WarnSystemName systemName) {
        this.systemName = systemName;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public WarnEnv getSystemEnv() {
        return systemEnv;
    }

    public void setSystemEnv(WarnEnv systemEnv) {
        this.systemEnv = systemEnv;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
