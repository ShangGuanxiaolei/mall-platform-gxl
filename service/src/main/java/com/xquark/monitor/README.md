#捕获异常，通知相关人员

```
不按照老的项目结构进行编码
包结构如下：
```
「package   
「**class**  
「*comment*  
> com.xquark~~.utils~~
>> monitor
>>> drive 
>>>> **MonitorWarn** *(interface 抽象异常处理动作)* 
>>>> 
>>>> channel  
>>>>> **EmailWarn** *(邮件提醒实现)*  
>>> 
>>> **WarnEntity** *(异常的抽象类)*  


```
ver.0.1
create by Jim
Jan.28.2018
```

- 仅捕获Exception,如果需要处理error在 ErrorController(在ExceptionHandler同一个包里)里添加  
- 主要告知信息详见WarnEntity  
- **Rule**  
   - system+uri+exception 一小时一次预警  
   - system+exception 一小时三次预警
- 推送方式
   - 目前只以邮件方式推送，即monitor.drive.channel  
- 发件信箱及收件人均hardcode  

```
ver.0.2
@auth jim
Jan.28.2018
```
从util移动到service  
util中没有jedis配置。人懒，复用一下service的
