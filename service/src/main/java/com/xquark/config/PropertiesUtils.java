package com.xquark.config;

import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;

/**
 * 属性properties值获取工具
 *
 * @author Appleyk
 * @date 2018年1月12日:上午9:12:29
 */
@Component //@Component注解声明为Spring bean
public class PropertiesUtils implements EmbeddedValueResolverAware {

  private StringValueResolver stringValueResolver;

  public void setEmbeddedValueResolver(StringValueResolver resolver) {
    stringValueResolver = resolver;
  }

  public String getPropertiesValue(String name) {
    return stringValueResolver.resolveStringValue("${" + name + "}");

  }
}