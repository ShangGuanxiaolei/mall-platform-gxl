package com.xquark.config;

import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xquark.cache.RedisUtilFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/** Created by wangxinhua on 17-4-15. DESC: 缓存配置类 */
@Configuration
@ComponentScan(basePackages = "com.xquark.cache")
public class CachingConfig implements InitializingBean {

  @Value("${spring.redis.database}")
  private Integer database;

  @Value("${spring.redis.host}")
  private String host;

  @Value("${spring.redis.port}")
  private Integer port;

  @Value("${spring.redis.password}")
  private String password;

  @Value("${spring.redis.pool.max-wait}")
  private long maxWait;

  @Value("${spring.redis.pool.max-active}")
  private Integer maxActive;

  // 最大空闲连接
  @Value("${spring.redis.pool.max-idle}")
  private Integer maxIdle;

  @Value("${spring.redis.pool.min-idle}")
  private Integer minIdle;

  @Value("${spring.redis.timeout}")
  private Integer timeout;

  @Autowired
  private JedisPoolConfig jedisPoolConfig;

  private static final StringRedisSerializer STRING_REDIS_SERIALIZER = new StringRedisSerializer();

  @Bean(name = "jsonRedisTemplate")
  public RedisTemplate<String, Object> jsonTemplate() {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory());
    RedisSerializer<Object> objectJackson2JsonRedisSerializer = new GenericFastJsonRedisSerializer();
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setHashKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(objectJackson2JsonRedisSerializer);
    template.setHashValueSerializer(objectJackson2JsonRedisSerializer);
    return template;
  }

  @Bean(name = "redisTemplate")
  public RedisTemplate<String, Object> redisTemplate() {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory());
    JdkSerializationRedisSerializer jdkSerializationRedisSerializer =
        new JdkSerializationRedisSerializer();
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setHashKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(jdkSerializationRedisSerializer);
    template.setHashValueSerializer(jdkSerializationRedisSerializer);
    return template;
  }

  @Bean(name = "stringRedisTemplate")
  public RedisTemplate<String, String> stringRedisTemplate() {
    RedisTemplate<String, String> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory());
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(STRING_REDIS_SERIALIZER);
    return template;
  }

  @Bean(name = "longRedisTemplate")
  public RedisTemplate<String, Long> longRedisTemplate() {
    RedisTemplate<String, Long> template = new RedisTemplate<>();
    // 设置连接工厂
    template.setConnectionFactory(redisConnectionFactory());
    // 设置序列化转换器
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Long.class));
    return template;
  }

  @Bean(name = "doubleRedisTemplate")
  public RedisTemplate<String, Double> decimalRedisTemplate() {
    RedisTemplate<String, Double> template = new RedisTemplate<>();
    // 设置连接工厂
    template.setConnectionFactory(redisConnectionFactory());
    // 设置序列化转换器
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Double.class));
    return template;
  }

  /** 配置redis连接池 */
  @Bean
  @ActiveContext("v2")
  public JedisPoolConfig poolConfigForV2() {
    return jedisPoolConfigBuilder(maxIdle, minIdle, maxActive);
  }

  @Bean
  @ActiveContext("sellerpc")
  public JedisPoolConfig poolConfigForSellerPc() {
    return jedisPoolConfigBuilder(maxIdle / 2, minIdle / 2, maxActive / 2);
  }

  @Bean
  @ActiveContext("web")
  public JedisPoolConfig poolConfigForWeb() {
    return jedisPoolConfigBuilder(maxIdle / 2, minIdle / 2, maxActive / 2);
  }

  @Bean
  @ActiveContext("task")
  public JedisPoolConfig poolConfigForTask() {
    return jedisPoolConfigBuilder(maxIdle / 2, minIdle / 4,
        maxActive / 2);
  }


  private JedisPoolConfig jedisPoolConfigBuilder(int maxIdle, int minIdle, int maxActive) {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxIdle(maxIdle);
    poolConfig.setMinIdle(minIdle);
    poolConfig.setMaxTotal(maxActive);
    poolConfig.setMaxWaitMillis(maxWait);
    poolConfig.setTestOnBorrow(true);
    return poolConfig;
  }

  public JedisPool getJedisPool() {
    JedisPoolConfig config = jedisPoolConfigBuilder(maxIdle / 2, minIdle / 2, maxActive / 2);
    JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout);
    return jedisPool;
  }

  /** redis连接工厂 */
  @Bean(name = "redisConnectionFactory")
  public RedisConnectionFactory redisConnectionFactory() {
    JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
    jedisConnectionFactory.setDatabase(database);
    jedisConnectionFactory.setHostName(host);
    jedisConnectionFactory.setPort(port);
    jedisConnectionFactory.setPassword(password);
    jedisConnectionFactory.setPoolConfig(jedisPoolConfig);
    return jedisConnectionFactory;
  }

  @Bean(name = "redisCacheManager")
  public RedisCacheManager redisCacheManager() {
    final RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate());
    // 默认过期时间为60秒
    redisCacheManager.setDefaultExpiration(60);
    return redisCacheManager;
  }

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper();
  }

  @Override
  public void afterPropertiesSet() {
    RedisUtilFactory.initialize(redisConnectionFactory());
  }
}
