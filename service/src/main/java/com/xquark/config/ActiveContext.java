package com.xquark.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Conditional;

/**
 * @author wangxinhua.
 * @date 2019/1/7
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Conditional(PoolConfigCondition.class)
public @interface ActiveContext {

  String[] value();

}
