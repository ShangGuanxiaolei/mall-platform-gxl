package com.xquark.config;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.MultiValueMap;

/**
 * @author wangxinhua.
 * @date 2019/1/7
 */
public class PoolConfigCondition implements Condition {

  private static final String CONTEXT_FILE = "/context.identify";

  @Override
  public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
    String realContext;
    try {
      realContext =
          IOUtils.resourceToString(CONTEXT_FILE, Charset.forName(StandardCharsets.UTF_8.name()));
    } catch (IOException e) {
      throw new RuntimeException("环境配置加载失败", e);
    }
    MultiValueMap<String, Object> allAnnotationAttributes =
        metadata.getAllAnnotationAttributes(ActiveContext.class.getName());
    if (allAnnotationAttributes != null) {
      String val = ((String[]) allAnnotationAttributes.get("value").get(0))[0];
      return val.contains(realContext);
    }
    return false;
  }
}
