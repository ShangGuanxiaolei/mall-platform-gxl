package com.xquark.listener.handler.notify;

import com.xquark.dal.model.Order;

public interface OrderNotifyHandler {

  public void onSubmit(Order order);

  public void onCancel(Order order);

  public void onPay(Order order);

  public void onShip(Order order);

  public void onSign(Order order);

  public void onRefund(Order order);

  public void onRequestRefund(Order order);

  public void onAcceptRefund(Order order);

  public void onRejectRefund(Order order);

}
