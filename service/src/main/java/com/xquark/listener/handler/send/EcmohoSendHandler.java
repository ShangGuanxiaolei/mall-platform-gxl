package com.xquark.listener.handler.send;


import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.xquark.dal.model.Order;
import com.xquark.dal.type.SendErpDest;
import com.xquark.dal.type.SendErpStatus;
import com.xquark.service.ecomho.EcmohoErpService;
import com.xquark.service.order.OrderService;


@Component("ecmohoSendHandler")
public class EcmohoSendHandler implements OrderSendErpHandler {

  @Autowired
  private EcmohoErpService ecmohoErpService;

  @Autowired
  private OrderService orderService;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public void onSend(Order order) {

    log.info("begin on ecmoho send transaction [" + order.getOrderNo()
        + "]  event");

    boolean isSuccess = ecmohoErpService.sendOrder(order);
    Date now = new Date();
    if (isSuccess) {
      orderService.updateOrderSendErpStatus(order.getId(),
          now, SendErpStatus.SUCCESS, SendErpDest.ECMOHO,
          "SUCCESS");
    } else {
      orderService.updateOrderSendErpStatus(order.getId(),
          now, SendErpStatus.FAIL, SendErpDest.ECMOHO,
          "fail");
    }
    log.info("end   on ecmho  send transaction [" + order.getOrderNo()
        + "]  event");
  }


}

//	public static void main(String args[]){
//		System.out.println("test");
//		StringBuffer sb = new StringBuffer();
//		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
//				.append("<Response>\n  <isSuccess>true</isSuccess>\n" +
//						"  <errorCode></errorCode>\n" +
//						"  <errorNumber></errorNumber>\n" +
//						"  <errorMsg></errorMsg>\n" +
//						" </Response>");
//		SAXBuilder builder = new SAXBuilder(false);
//		ByteArrayInputStream bi = new ByteArrayInputStream(sb.toString().getBytes());
//		InputSource inputSource = new InputSource(bi);
//		Document doc = null;
//		try{
//			doc = builder.build(inputSource);
//			Element root = doc.getRootElement();//获得根节点
//			Element result = (Element) XPath.selectSingleNode(root, "/Response/isSuccess");
//			String resultText = result.getText();
//			if(resultText!=null&&resultText.equals("true")) {
//				System.out.println("success");
//			}else{
//				System.out.println("fail");
//			}
//		}catch (Exception e){
//			e.printStackTrace();
//
//		}
//		DefaultHttpClient httpclient = new DefaultHttpClient();
//		HttpPost httpPost = new HttpPost("http://51shop.mobi/v2/");
//        String u="18602724066";
//		String p="602121";
//		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//		nvps.add(new BasicNameValuePair("u", u));
//		nvps.add(new BasicNameValuePair("p", p));
//		System.out.println("111111");
//		try {
//			UrlEncodedFormEntity entitys = new UrlEncodedFormEntity(nvps,
//					"UTF-8");
//			httpPost.setEntity(entitys);
//			HttpResponse response = httpclient.execute(httpPost);
//			System.out.println("222222222");
//			HttpEntity entity = response.getEntity();
//			System.out.println("333333333");
//			if (entity != null) {
//				long len = entity.getContentLength();
//				if (len != -1 && len < 2048) {
//					String resultXml = EntityUtils
//							.toString(entity, "UTF-8");
//					System.out.println("resultXml"+resultXml);
//				}
//			}
//		} catch (Exception e) {
//                    e.printStackTrace();
//		}
//
//	}


