package com.xquark.listener.handler.transaction;

import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.model.MainOrder;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.type.OrderActionType;
import com.xquark.dal.type.OrderSingleType;
import com.xquark.dal.vo.MainOrderVO;
import com.xquark.dal.vo.OrderVO;
import com.xquark.event.OrderActionEvent;
import com.xquark.service.cart.CartService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;


@Component("danbaoMainOrderTransactionHandler")
public class DanbaoMainOrderTransactionHandler implements MainOrderTransactionHandler {

  @Autowired
  private MainOrderService mainOrderService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private CartService cartService;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private ApplicationContext applicationContext;

  private Logger log = LoggerFactory.getLogger(getClass());


  @Override
  public void onSubmit(MainOrder order) {

    log.info("begin on main order transaction [" + order.getOrderNo()
        + "] submit event");
    MainOrderVO mainOrderVO = mainOrderService.loadByOrderNo(order.getOrderNo());
    List<OrderVO> orderVOs = mainOrderVO.getOrders();

    //根据主订单下单方式清空购物车-立即购买不清
    if (order.getOrderSingleType() != OrderSingleType.SUBMIT) {
      for (OrderVO ordervo : orderVOs) {
        List<OrderItem> orderItems = ordervo.getOrderItems();
        for (OrderItem orderItem : orderItems) {
          // 跳过赠品
          if (!orderItem.getProduct().getGift()) {
            cartService.removeBySku(orderItem.getSkuId());
          }
        }
      }
    }

    //分发事件到子订单
    for (OrderVO ordervo : orderVOs) {
      applicationContext.publishEvent(new OrderActionEvent(
          OrderActionType.SUBMIT, ordervo));
      log.info("main order transaction submit event subOrderNo:[" + ordervo.getOrderNo()
          + "] ");
    }
    log.info("end on main order transaction [" + order.getOrderNo()
        + "] submit event");
  }

  @Override
  public void onCancel(MainOrder order) {

    log.info("begin on main order transaction [" + order.getOrderNo()
        + "] cancel event");
    //TODO  cancel main order logic. sub order cancel logic will be not need.
    // 1. restore product stock and status

    // 2. cancel commission

    log.info("end on main order transaction [" + order.getOrderNo()
        + "] cancel event");
  }

  @Override
  public void onPay(MainOrder order) {
    log.info("begin on main order transaction [" + order.getOrderNo()
        + "] pay event");
    log.info("end on main order transaction [" + order.getOrderNo()
        + "] pay event");
  }

  @Override
  public void onFinish(MainOrder order) {

  }

  @Override
  public void onRequestRefund(MainOrder order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptRefund(MainOrder order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectRefund(MainOrder order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRefund(MainOrder order) {

    log.info("begin on order transaction [" + order.getOrderNo()
        + "] refund event");

    log.info("end on order transaction [" + order.getOrderNo()
        + "] refund event");
  }


}
