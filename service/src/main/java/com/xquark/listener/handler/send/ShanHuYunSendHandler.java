package com.xquark.listener.handler.send;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.xml.sax.InputSource;

import com.xquark.dal.mapper.SkuMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Product;
import com.xquark.dal.model.Sku;
import com.xquark.dal.model.Zone;
import com.xquark.dal.type.SendErpDest;
import com.xquark.dal.type.SendErpStatus;
import com.xquark.service.order.OrderAddressService;
import com.xquark.service.order.OrderService;
import com.xquark.service.order.vo.OrderForErpVO;
import com.xquark.service.order.vo.OrderItemForErpVO;
import com.xquark.service.product.ProductService;
import com.xquark.service.zone.ZoneService;

@Component("shanHuYunSendHandler")
public class ShanHuYunSendHandler implements OrderSendErpHandler {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public void onSend(Order order) {
    log.info("begin on shanhuyun send erp log  [" + order.getOrderNo()
        + "] event");
    log.info("end   on shanhuyun  send epr log  [" + order.getOrderNo()
        + "] event");
  }


}
