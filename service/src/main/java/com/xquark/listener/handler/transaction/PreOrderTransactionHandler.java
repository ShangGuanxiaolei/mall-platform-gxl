package com.xquark.listener.handler.transaction;

import org.springframework.stereotype.Component;

import com.xquark.dal.model.Order;

@Component("preOrderTransactionHandler")
public class PreOrderTransactionHandler implements OrderTransactionHandler {

  @Override
  public void onSubmit(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCancel(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onPay(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onShip(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onSign(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRequestRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onAcceptRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRejectRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestReissue(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptReissue(Order order) {
// TODO Auto-generated method stub
  }

  @Override
  public void onRejectReissue(Order order) {
// TODO Auto-generated method stub
  }

  @Override
  public void onRefund(Order order) {
    // TODO Auto-generated method stub

  }

}
