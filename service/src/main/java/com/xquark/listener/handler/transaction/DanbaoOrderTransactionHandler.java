package com.xquark.listener.handler.transaction;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.hds.xquark.PointContextInitialize;
import com.hds.xquark.dal.constrant.GradeCodeConstrants;
import com.hds.xquark.dal.type.PlatformType;
import com.hds.xquark.dal.type.Trancd;
import com.hds.xquark.service.point.PointCommOperationResult;
import com.hds.xquark.service.point.PointCommService;
import com.xquark.cache.RedisLock;
import com.xquark.cache.RedisUtilFactory;
import com.xquark.cache.RedisUtils;
import com.xquark.dal.consts.PointConstants;
import com.xquark.dal.consts.PromotionConstants;
import com.xquark.dal.mapper.*;
import com.xquark.dal.model.*;
import com.xquark.dal.model.mypiece.PGMemberInfoVo;
import com.xquark.dal.model.mypiece.PromotionOrderDetail;
import com.xquark.dal.model.mypiece.StockCheckBean;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.*;
import com.xquark.dal.type.*;
import com.xquark.dal.util.SpringContextUtil;
import com.xquark.dal.vo.PrivilegeCodeVO;
import com.xquark.dal.vo.PromotionSkuVo;
import com.xquark.event.MainOrderActionEvent;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.cantuan.PieceCacheService;
import com.xquark.service.coupon.UserCouponService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.error.ThirdRefundData;
import com.xquark.service.freshman.FreshManFlagService;
import com.xquark.service.freshman.FreshManService;
import com.xquark.service.freshman.pojo.FreshManToast;
import com.xquark.service.groupon.ActivityGrouponService;
import com.xquark.service.mypiece.StockCheckService;
import com.xquark.service.order.MainOrderService;
import com.xquark.service.order.OrderRefundService;
import com.xquark.service.order.OrderService;
import com.xquark.service.orderHeader.OrderHeaderService;
import com.xquark.service.outpay.PaymentRequestVO;
import com.xquark.service.outpay.ThirdPaymentQueryService;
import com.xquark.service.outpay.impl.AliPaymentImpl;
import com.xquark.service.outpay.impl.TenPaymentImpl;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.pintuan.PromotionPgService;
import com.xquark.service.platform.CustomerProfileService;
import com.xquark.service.pointgift.PointGiftService;
import com.xquark.service.pointgift.dto.PointSendType;
import com.xquark.service.pricing.PromotionService;
import com.xquark.service.pricing.base.PieceStateException;
import com.xquark.service.pricing.impl.pricing.impl.PgPromotionServiceAdapter;
import com.xquark.service.pricing.impl.pricing.impl.PiecePrice;
import com.xquark.service.privilege.PrivilegeCodeService;
import com.xquark.service.product.ProductCombineService;
import com.xquark.service.product.ProductService;
import com.xquark.service.product.vo.ProductSkuVO;
import com.xquark.service.promotion.PromotionBaseInfoService;
import com.xquark.service.promotion.PromotionConfigService;
import com.xquark.service.promotion.PromotionOrderSubject;
import com.xquark.service.promotion.PromotionSkusService;
import com.xquark.service.reserve.PromotionReserveService;
import com.xquark.service.shop.ShopService;
import com.xquark.service.shopTree.ShopTreeService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.UserService;
import com.xquark.utils.DateUtils;
import com.xquark.utils.http.PoolingHttpClients;
import com.xquark.utils.unionpay.HttpClient;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.hds.xquark.dal.type.TotalAuditType.API;

@Component("danbaoOrderTransactionHandler")
public class DanbaoOrderTransactionHandler implements OrderTransactionHandler {

  @Autowired
  private UserService userService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private UnionService unionService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private ShopTreeService shopTreeService;

  @Autowired
  private TenPaymentImpl tenPayment;

  @Autowired
  private AliPaymentImpl aliPayment;

  @Autowired
  private ActivityGrouponService activityGrouponService;

  @Autowired
  private PromotionOrderSubject promotionOrderSubject;

  @Autowired
  private PrivilegeCodeService privilegeCodeService;

  @Autowired
  private CustomerProfileService customerProfileService;

  @Autowired
  private OrderHeaderService orderHeaderService;

  private PointCommService pointCommService;

  private OrderRefundService orderRefundService;

  @Autowired
  private ProductCombineService productCombineService;

  @Autowired
  private RedisTemplate redisTemplate;

  @Autowired
  private StockCheckService stockCheckService;

  @Autowired
  private PromotionConfigService promotionConfigService;

  @Autowired
  private PgPromotionServiceAdapter pgPromotionServiceAdapter;

  @Autowired
  private PieceCacheService pieceCacheService;

  @Autowired
  private PointGiftService pointGiftService;

  @Autowired
  private PromotionReserveService reserveService;

  @Autowired
  SkuMapper skuMapper;

  @Autowired
  private ServMessageMapper servMessageMapper;

  @Autowired
  private SkuCombineMapper skuCombineMapper;

  @Autowired
  private PromotionBaseInfoService promotionBaseInfoService;

  @Autowired
  private PromotionService promotionService;

  @Autowired
  private PromotionToConserveMapper promotionToConserveMapper;

  @Autowired
  private PromotionToVipMapper promotionToVipMapper;

  @Autowired
  private FreshManService freshManService;

  @Autowired
  private FreshManFlagService freshManFlagService;

  @Value("${balance.center.url}")
  private String host;

  @Autowired
  private MemberInfoMapper memberInfoMapper;

  @Autowired
  private ThirdPaymentQueryService thirdPaymentQueryService;

  @Autowired
  public void setOrderRefundService(OrderRefundService orderRefundService) {
    this.orderRefundService = orderRefundService;
  }

  @Autowired
  public void setPointCommService(PointContextInitialize pointContextInitialize) {
    this.pointCommService = pointContextInitialize.getPointService();
  }

  private Logger log = LoggerFactory.getLogger(getClass());

  private static final Long COMPANY_CPID = 2000002L;

  /**
   * 新人团标识
   */
  private static final String PURE_WHITE = "PW";

  /**
   * 内部员工分润开关
   */
  @Value("${personnel.commission.on-off}")
  private boolean personnelCommissonOn;

  /**
   * 内部员工想去分润比
   */
  @Value("${personnel.commission.xiangqu.rate}")
  private String commissionRateXQ;

  @Value("${paymode.user.id.xiangqu}")
  private String xiangquUserId;

  @Autowired
  private PromotionSkusService promotionSkusService;

  @Autowired
  private CustomerModifyLinkDetailMapper customerModifyLinkDetailMapper;

  @Autowired
  private WeakLinkMapper weakLinkMapper;

  @Autowired
  private ConsumerToConsumerMapper consumerToConsumerMapper;

  @Autowired
  private ConsumerToCustomerMapper consumerToCustomerMapper;

  @Autowired
  private PromotionPgService promotionPgService;

  @Autowired
  private PromotionOrderDetailMapper orderDetailMapper;

  private void lockDB(Order order, OrderItem orderItem, Sku sku) {
    String buyerId = order.getBuyerId();
    String promotionId = order.getPromotionId();
    List<PromotionSkuVo> promotionSkuVos = promotionSkusService.getPromotionSkus(sku.getSkuCode());
    //
    if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
      modifyMeetingAmount(buyerId, promotionId, orderItem.getAmount(), orderItem.getSkuId());
      //大会活动针对于套装商品的库存处理 2018-12-17 lxl
      //判断是否是套装
      boolean ismaster = skuCombineMapper.isMaster(orderItem.getSkuId());
      if (ismaster) {
        minusStock(orderItem.getProductId(), orderItem.getSkuId(), orderItem.getAmount());
      }
    } else {
      ProductSkuVO productSkuVO =
          productService.load(orderItem.getProductId(), orderItem.getSkuId());
      if (productSkuVO.isDistributed()) {
        productService.updateSkuStock(productSkuVO.getSourceSku().getId(), orderItem.getAmount());
        productService.updateStock(productSkuVO.getSourceProduct().getId(), orderItem.getAmount());
        // 将商品上面的库存调整为所有sku库存中的最大值
        productService.updateProductAmountByMaxSkuAmount(productSkuVO.getSourceProduct().getId());
      } else {
        minusStock(orderItem.getProductId(), orderItem.getSkuId(), orderItem.getAmount());
      }
    }
  }

  @Override
  public void onSubmit(Order order) {
    List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
    log.info("begin on order transaction [" + order.getOrderNo() + "] submit event");


    Long cpId = userService.selectCpIdByUserId(order.getBuyerId());
    //如果是拼团活动就判断活动优惠就尝试增加享受活动的次数
    if (OrderSortType.isPiece(order.getOrderType())) {
      PromotionOrderDetail pieceOrderDetail = pgPromotionServiceAdapter.loadPieceDetailByOrderNo(order.getOrderNo());
      String memberCode = pieceOrderDetail.getPiece_group_detail_code();
      PGMemberInfoVo memberInfo = pgPromotionServiceAdapter.loadMemberInfo(memberCode);
      if (memberInfo.getIsGroupHead() > 0) {
        tryIncrementPromotionPriceNum(cpId, pieceOrderDetail);
      }
    }

    // 1. create danbao deal
    // dealService.createDanbaoPayDealByOrder(order);

    // FIXME 临时写死抢购商品不扣减db库存 只能适应单商品单活动
    // 2. update product stock
    for (OrderItem orderItem : orderItems) {
      final String skuId = orderItem.getSkuId();
      if (PromotionType.FLASHSALE == orderItem.getPromotionType()) {
        String buyerId = order.getBuyerId();
        String promotionId = orderItem.getPromotionId();
        modifyCacheAmount(buyerId, promotionId, orderItem.getSkuId(), orderItem.getAmount());
      } else if (PromotionType.RESERVE == orderItem.getPromotionType()) {
        final String promotionId = order.getPromotionId();
        if (!reserveService.modifyStock(promotionId, skuId, orderItem.getAmount())) {
          throw new BizException(GlobalErrorCode.RESERVE_PROMOTION_AMOUNT_LIMIT);
        }
      } else {
        if (OrderSortType.MEETING == order.getOrderType()) {
          PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
          String lock_type = "";
          if (null == lock_type_config) {
            lock_type = "DB";
          }
          lock_type = lock_type_config.getConfigValue();

          Sku sku = skuMapper.selectByPrimaryKey(orderItem.getSkuId());
          if ("DB".equals(lock_type)) {
            this.lockDB(order, orderItem, sku);

          } else {
            String buyerId = order.getBuyerId();
//                        PromotionConfig mp_config = promotionConfigService.selectByConfigType("meeting_pid");
//                        String promotionId = "";
//                        if (null == mp_config) {
//                            promotionId = "meeting";
//                        }
//                        promotionId = mp_config.getConfigValue();
            List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectEffectiveBySkuCode(sku.getSkuCode());
            String promotionId = "meeting";
            if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
              promotionId = promotionBaseInfos.get(0).getpCode();
            }

            this.modifyMoreCacheAmount(
                buyerId, promotionId, sku.getSkuCode(), orderItem.getAmount(), orderItem);
          }
        } else if (OrderSortType.isPiece(order.getOrderType())) {
          // 拼团订单下单不扣减库存, 在所有用户都支付后才扣减库存
        } else {
          ProductSkuVO productSkuVO =
              productService.load(orderItem.getProductId(), orderItem.getSkuId());
          if (productSkuVO.isDistributed()) {
            productService.updateSkuStock(
                productSkuVO.getSourceSku().getId(), orderItem.getAmount());
            productService.updateStock(
                productSkuVO.getSourceProduct().getId(), orderItem.getAmount());
            // 将商品上面的库存调整为所有sku库存中的最大值
            productService.updateProductAmountByMaxSkuAmount(
                productSkuVO.getSourceProduct().getId());
          } else {
            minusStock(orderItem.getProductId(), orderItem.getSkuId(), orderItem.getAmount());
          }
        }
      }
    }

    // 更新活动库存
    //    promotionOrderSubject.onSubmit(order);

    // 3. locking coupon
    if (StringUtils.isNotBlank(order.getCouponId())) {
      UserCoupon userCoupon = userCouponService.load(order.getCouponId());
      userCoupon.setStatus(CouponStatus.LOCKED);
      userCouponService.updateUserCouponById(userCoupon);
    }

    // 3. create union record
    // 调整到订单支付成功处理
    // unionService.calc(order.getId());

    //        yundouAmountService.modifyAmount(YundouOperationType.FINISH_ORDER, order.getId());
    //        // 全积分兑换订单，直接调用付款
    //        if (order.getFullYundou()) {
    //            orderService.executeBySystem(order.getId(), OrderActionType.PAY, null);
    //        }

    // 特权活动商品提交后，找到用户可使用的特权码，记录订单与特权码关系，同时特权码可用数量减1
    if (order.getOrderType() == OrderSortType.PRIVILEGE) {
      // 先记录特权码与订单的关系
      PrivilegeCodeVO privilegeCodeVO =
          privilegeCodeService.selectValidByUserId(order.getBuyerId());
      privilegeCodeService.addCodeOrder(privilegeCodeVO.getId(), order.getId());
      // 将特权码的可用数量减1
      privilegeCodeService.subQty(privilegeCodeVO.getId());
    }

    try {
      orderService.saveIdentityLogOnSubmit(order.getOrderNo(), cpId);
    } catch (Exception e) {
      log.warn("订单 {} 下单写入身份日志错误", order.getOrderNo(), e);
    }

    log.info("end on order transaction [" + order.getOrderNo() + "] submit event");
  }

  /**
   * 团购订单提交,取消,退款时的库存，销量等逻辑
   */
  private void checkGroupon(Order order, OrderStatus status) {
    if (order.getOrderType() == OrderSortType.GROUPON) {
      int amount = 0;
      List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
      for (OrderItem orderItem : orderItems) {
        amount = amount + orderItem.getAmount();
      }
      if (status == OrderStatus.SUBMITTED) {
        activityGrouponService.updateStock(order.getId(), amount);
      } else if (status == OrderStatus.CANCELLED) {
        activityGrouponService.updateStock(order.getId(), -amount);
      }
    }
  }

  @Override
  public void onCancel(Order order) {
    List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
    log.info("begin on order transaction [" + order.getOrderNo() + "] cancel event");

    // 1. restore product stock and status
    for (OrderItem orderItem : orderItems) {
      updateStockOnCancel(order, orderItem);
    }
    //释放拼主价优惠名额次数
    // 如果是拼团订单取消, 则要检查名额是否被占用, 被占用的名额要释放
    if (OrderSortType.isPiece(order.getOrderType())) {
      String orderNo = order.getOrderNo();
      PromotionOrderDetail detail = pgPromotionServiceAdapter.loadPieceDetailByOrderNo(orderNo);
      String tranCode = detail.getPiece_group_tran_code();
      pieceCacheService.incrementTranRestMember(tranCode);
      PGMemberInfoVo memberInfo = pgPromotionServiceAdapter.loadMemberInfo(detail.getPiece_group_detail_code());
      //尝试释放活动优惠次数
      if (memberInfo.getIsGroupHead() > 0) {
        tryReleasePromotionPriceNum(order);
      }
    }

    // 恢复活动库存
    //    promotionOrderSubject.onCancel(order);

    // cancel commission
    unionService.onCancel(order.getId());

    // coupon valid
    if (StringUtils.isNotBlank(order.getCouponId())) {
      UserCoupon userCoupon = userCouponService.load(order.getCouponId());
      if (userCoupon != null) {
        userCoupon.setStatus(CouponStatus.VALID);
        userCouponService.updateUserCouponById(userCoupon);
      }
    }

    // 特权订单取消时，删除特权码与订单关系，同时还原特权码可用数量
    if (order.getOrderType() == OrderSortType.PRIVILEGE) {
      PrivilegeCodeOrder codeOrder = privilegeCodeService.selectCodeOrder(order.getId());
      // 先删除特权码与订单关系
      privilegeCodeService.delCodeOrder(order.getId());
      // 将还原特权码可用数量
      privilegeCodeService.addQty(codeOrder.getCodeId());
    }

    log.info("end on order transaction [" + order.getOrderNo() + "] cancel event");
   
  }

  /**
   * 尝试释放活动的优惠次数
   *
   * @param order
   */
  private void tryReleasePromotionPriceNum(Order order) {
    PromotionOrderDetail detail = pgPromotionServiceAdapter.loadPieceDetailByOrderNo(order.getOrderNo());
    if (detail == null) {
      return;
    }
    final BigDecimal price = pgPromotionServiceAdapter.showOpenMemberPrice(detail.getP_detail_code());
    final PGMemberInfoVo memberInfo = pgPromotionServiceAdapter.loadMemberInfo(detail.getPiece_group_detail_code());

    //如果是这个活动的团主
    if (memberInfo.getIsGroupHead() > 0 && price != null) {
      final Long cpId = userService.selectCpIdByUserId(order.getBuyerId());
      pieceCacheService.decrementPromotionPriceNum(String.valueOf(cpId), detail.getP_detail_code());
    }
  }

  @Override
  public void onPay(Order order) {
    log.info("begin on order transaction [" + order.getOrderNo() + "] pay event");
    if (order.getPaidAt() != null && order.getPaidStatus() == PayStatus.SUCCESS) {
      log.info("order {} has paid", order.getOrderNo());
      return;
    }

    if (order.getOrderType() != OrderSortType.PURCHASE) {
      // fixme 注释掉佣金逻辑，避免报错
      payRequestBuyer2Seller(order);
    }

    int orderInMainSize = orderMapper.selectOrderCountByMainOrderId(order.getId(), null);
    int orderInMainFinishSize = orderMapper.selectOrderCountByMainOrderId(order.getId(), "SUCCESS");
    if (orderInMainSize == orderInMainFinishSize) {
      SpringContextUtil.getContext()
          .publishEvent(new MainOrderActionEvent(MainOrderActionType.FINISH, order));
    }

    String buyerId = order.getBuyerId();
    Long cpId = userService.selectCpIdByUserId(buyerId);
    // 下单时根据订单积分换购扣除用户对应的积分
    BigDecimal usingPoint = order.getPaidPoint();
    // 红包消费
    BigDecimal usingPointPacket = order.getPaidPointPacket();
    BigDecimal usingCommission = order.getPaidCommission();

    // 消费德分积分
    consumePoint(order, buyerId, cpId, usingPoint, usingCommission);

    if (usingPointPacket != null && usingPointPacket.signum() != 0) {
      pointGiftService.modifyWithDetail(cpId, usingPointPacket.negate(), order.getOrderNo(),
          PointSendType.CONSUMPTION);
    }

    // 保存分享人关系
    if (cpId != null) {

      saveWeekConnection(cpId, order);

      if (order.getLogisticDiscountType() == LogisticDiscountType.FREE_FIRST_PAY) {
        try {
          // 付款后如果用户已经使用了免邮资格则更新状态为已使用
          User user = new User();
          user.setCpId(cpId);
          user.setFreeLogisticUsed(Boolean.TRUE);
          userService.updateByCpIdSelective(user);
        } catch (Exception e) {
          log.error("订单 {} 更新用户免邮资格错误", order.getOrderNo(), e);
        }
      }

      //升级为强关系操作
      toStrong(order, cpId);

      try {
        final CareerLevelType levelType = customerProfileService.getLevelType(cpId);
        orderService.updateIdentityLogOnPay(order.getOrderNo(), levelType);
      } catch (Exception e) {
        log.warn("订单 {} 付款写入身份日志错误", order.getOrderNo(), e);
      }

      // 拼团订单处理
      if (OrderSortType.isPiece(order.getOrderType())) {
        String orderNo = order.getOrderNo();
        PromotionOrderDetail pieceOrderDetail =
            pgPromotionServiceAdapter.loadPieceDetailByOrderNo(order.getOrderNo());
        String memberCode = pieceOrderDetail.getPiece_group_detail_code();
        PGMemberInfoVo memberInfo = pgPromotionServiceAdapter.loadMemberInfo(memberCode);
        if (memberInfo == null) {
          log.error("订单 {} 拼团参团信息异常", orderNo);
        } else {
          boolean isGroupHead = memberInfo.getIsGroupHead() > 0;
          String tranCode = memberInfo.getPieceGroupTranCode();
          String pCode = pieceOrderDetail.getP_code();
          String detailCode = memberInfo.getPieceGroupDetailCode();
          boolean isPieceFullBeforeSelf = false;
          if (isGroupHead) {
            // 拼团主支付, 拼团表置为开团
            try {
              pgPromotionServiceAdapter.batchUpdateTranAndMember(tranCode, PieceStatus.OPENED);
              final PromotionBaseInfo baseInfo = pgPromotionServiceAdapter.loadBaseInfo(pCode);
              // TODO 过期时间要设置为 (团结束的时间+一天)
              // 初始化redis中的参团人数信息
              pieceCacheService.initTranMember(
                  tranCode,
                  pieceOrderDetail.getP_detail_code(),
                  DateUtils.addDay(baseInfo.getEffectTo(), 1));
            } catch (Exception e) {
              log.error("拼团主 {} 修改为开团状态失败");
            }
          } else {
            try {
              // 自己参团前是否已经满团
              isPieceFullBeforeSelf = pgPromotionServiceAdapter.isPieceFull(pieceOrderDetail);
              // 不更新状态
            } catch (PieceStateException e) {
              log.error("拼团数据错误", e);
            }
            // 参团人支付, 参团表置为开团
            pgPromotionServiceAdapter.updateMemberInfoStatus(memberCode, PieceStatus.OPENED);
            // 三期适应需求,在支付回调时处理扣减参团名额
            pieceCacheService.decrementTranRestMember(tranCode);

            // 由于允许插队拼团，如果团在自己参团前已经满了，则只扣减自己部分的库存
            if (isPieceFullBeforeSelf) {
              if (!modifyPieceDetailStock(order, tranCode, detailCode, pCode, cpId)) {
                return;
              }
            } else {
              // 扣除整团的库存
              if (!modifyPieceTranStock(
                  order, pieceOrderDetail, tranCode, pCode, detailCode, cpId)) {
                return; // 扣减失败则返回
              }
            }
            pieceNotify(tranCode);
          }
        }
      }
    }
    log.info("end on order transaction [" + order.getOrderNo() + "] pay event");
  }

  private void consumePoint(Order order, String buyerId, Long cpId, BigDecimal usingPoint,
      BigDecimal usingCommission) {
    if (usingPoint != null && usingPoint.signum() != 0) {
      PointCommOperationResult pointRet =
          pointCommService.modifyPoint(
              cpId,
              order.getOrderNo(),
              PointConstants.DEDUCT_POINT_CODE,
              PlatformType.E,
              usingPoint,
              Trancd.DEDUCT_P,
              API);
      log.info("用户 {} 下单扣减德分 {}", buyerId, usingPoint);
      log.info("扣减明细: {}", pointRet);
    }

    if (usingCommission != null && usingCommission.signum() != 0) {
      PointCommOperationResult commRet =
          pointCommService.modifyCommission(
              cpId,
              order.getOrderNo(),
              PointConstants.DEDUCT_COMM_CODE,
              PlatformType.E,
              usingCommission,
              Trancd.DEDUCT_C,
              API);
      log.info("用户 {} 下单扣减积分 {}", buyerId, usingCommission);
      log.info("扣减明细: {}", commRet);
    }
  }

  /**
   * 参团扣减自己的库存
   */
  private boolean modifyPieceDetailStock(Order order, String tranCode,
                                         String tranDetailCode, String pCode, Long cpId) {
    String orderNo = order.getOrderNo();
    String orderId = order.getId();
    if (pgPromotionServiceAdapter.isDetailPieceStockEnough(pCode, tranDetailCode, orderId, tranCode)) {
      // 当前插队库存充足
      pgPromotionServiceAdapter.updatePgOrderToPaidByOrderNo(orderNo);
      boolean b = pgPromotionServiceAdapter.modifyDetailPieceStock(tranCode, pCode, tranDetailCode, orderId);
      if (!b) {//扣减库存失败
        // 把插队订单设置为库存不足取消，在定时任务中退款
        pgPromotionServiceAdapter.updateDetailMember(
            tranDetailCode, PieceStatus.STOCK_CANCELED, PieceStatus.OPENED, PieceStatus.GROUPED);
        log.error("库存不足直接返回，不执行后续逻辑,tranCode", tranCode);
        return false;
      }
    } else {
      // 把插队订单设置为库存不足取消，在定时任务中退款
      pgPromotionServiceAdapter.updateDetailMember(
          tranDetailCode, PieceStatus.STOCK_CANCELED, PieceStatus.OPENED, PieceStatus.GROUPED);
      log.error("库存不足直接返回，不执行后续逻辑,tranCode", tranCode);
      return false;
    }
    log.info("订单 {} 插队拼团成功, tranCode: {}", order.getOrderNo(), tranCode);
    return true;
  }

  /**
   * 成团批量扣减团库存
   */
  private boolean modifyPieceTranStock(Order order, PromotionOrderDetail pieceOrderDetail, String tranCode,
                                       String pCode, String detailCode, Long cpId) {
    // 极端设置1人团只有拼团主支付也是有可能满团的
    // 自己参团后是否满团
    Boolean isPieceFullAfterSelf = null;
    try {
      isPieceFullAfterSelf = pgPromotionServiceAdapter.isPieceFull(pieceOrderDetail);
    } catch (PieceStateException e) {
      // 不更新状态
      log.error("拼团数据错误", e);
    }
    // 异常状态则为null
    if (isPieceFullAfterSelf != null && isPieceFullAfterSelf) {
      // 库存充足, 执行正常逻辑
      String orderId = order.getId();
      // 当次订单需要扣减的库存也要算
      if (pgPromotionServiceAdapter.isTranPieceStockEnough(tranCode, orderId, pCode)) {
        //先修改修改库存
        boolean b = pgPromotionServiceAdapter.modifyFullPieceStock(tranCode, orderId, pCode);
        log.info("更新库存结果：" + b);
        if (!b) {//有库存更新失败
          // 订单设置为库存不足取消，在定时任务中退款
          pgPromotionServiceAdapter.updateDetailMember(
              detailCode, PieceStatus.STOCK_CANCELED, PieceStatus.OPENED, PieceStatus.GROUPED);
          log.error("库存不足执行结束拼团处理完成，不执行后续逻辑,tranCode", tranCode);
          return false;
        }
        // 已满, 把已支付未成团的更新为成团, 并且把对应的订单状态都改为PAID
        pgPromotionServiceAdapter.batchUpdateGroupedTran(
            tranCode, PieceStatus.GROUPED, PieceStatus.OPENED);


        //拼团三期需求：白人开团成团次数达到一定数量，开团人升级为vip
        updateOrderHeaderJoinType(order.getOrderNo(), pCode, tranCode);
        //新人专区 拼团单处理
        List<String> toFreshMan = orderDetailMapper.selectTranOrderIds(tranCode);
        if(CollectionUtils.isNotEmpty(toFreshMan)){
          for (String id : toFreshMan) {
            String encodeId = IdTypeHandler.encode(Long.valueOf(id));
            Optional<Order> orderOptional = Optional.fromNullable(orderService.load(encodeId));
            //新人满200元送2000德分
            freshManService.freshManPieceOrderGrantPoint(orderOptional.get());
            //新人满500 升级vip
            freshManService.freshManPieceOrderToVip(orderOptional.get());
          }
        }
        log.info("团 {} 拼团成功, ");
        return true;
      } else {
        // 库存不足, 置为库存不足取消, 订单状态不变, 在定时任务中处理
        pgPromotionServiceAdapter.batchUpdateTranAndMember(
            tranCode, PieceStatus.STOCK_CANCELED, PieceStatus.OPENED);
        return false;
      }
    }
    return false;
  }

  private void pieceNotify(String tranCode) {
    try {
      List<String> memberList = servMessageMapper.memberList(tranCode);
      for (int i = 0; i < memberList.size(); i++) {
        String orderNoList = servMessageMapper.selectOrderNo(memberList.get(i), tranCode);
        Integer push = servMessageMapper.message(ServMessageStatus.SUCCESSPIECECONTENT.getWord(orderNoList), ServMessageStatus.TYPEMSG.getWord(), ServMessageStatus.STATUS.getWord(), memberList.get(i), ServMessageStatus.TYPESYSTEM.getWord());
        if (push < 1) {
          log.error("成团消息推送错误，用户id为{}", memberList.get(i));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error("拼团{}成团消息推送错误！", tranCode);
    }
  }

  /**
   * 更新order_header表加入类型
   *
   * @param orderNo
   * @param tranCode tranCode
   */
  private void updateOrderHeaderJoinType(String orderNo, String pCode, String tranCode) {
    //查询当前团拼主cpId
    String groupHeadMemberId = pgPromotionServiceAdapter.selectGroupHeadMemberId(tranCode);
    if (groupHeadMemberId == null) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "根据tranCode查询拼主cpId失败，tranCode：" + tranCode);
    }

    boolean hasIdentity = customerProfileService.hasIdentity(Long.valueOf(groupHeadMemberId));
    if (hasIdentity) {
      return;//有身份直接返回，不需要升级了
    }

    //根据升级VIP类型查询对应的配置
    PromotionToVip promotionToVip = promotionToVipMapper.selectByType(PromotionToVipType.PIECE_TO_VIP);
    if (null == promotionToVip || null == promotionToVip.getToNumber()) {
      //没有查询到直接返回，表明配置不存在或未启用
      return;
    }
    Integer toNumber = promotionToVip.getToNumber();
    Date effectiveBegin = promotionToVip.getEffectiveBegin();
    if (null == effectiveBegin) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "升级vip有效期时间配置不能为空");
    }
    Date effectiveEnd = promotionToVip.getEffectiveEnd();
    if (null == effectiveEnd) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "升级vip有效期时间配置不能为空");
    }
    if (!DateUtils.isEffectiveDate(new Date(), effectiveBegin, effectiveEnd)) {
      //不在有效期内，直接返回
      return;
    }

    //判断当前团是否为新人团，新人团才需要累加次数
    PromotionPg promotionPg = promotionPgService.selectPromotionPgBypCode(pCode);
    if (promotionPg == null || null == promotionPg.getJoinGroupQua()) {
      throw new BizException(GlobalErrorCode.ERROR_PARAM, "拼团活动不存在，团编号：" + pCode);
    }
    if (PURE_WHITE.equals(promotionPg.getJoinGroupQua())) {
      //拼团三期需求：白人开团成团后记录次数
      updatePgNum(groupHeadMemberId);
      //查询当前团拼主开团次数是否达到了规定次数
      Integer pgNum = promotionToConserveMapper.selectByCpId(groupHeadMemberId);
      if (pgNum != null && pgNum.equals(toNumber)) {
        //处理拼团达到规定次数升级VIP逻辑
        if (orderNo == null) {
          throw new BizException(GlobalErrorCode.ERROR_PARAM, "订单编号不存在");
        }
        if (!(orderHeaderService.updateJoinTypeByOrderNoForPiece(orderNo, groupHeadMemberId) > 0)) {
          throw new BizException(GlobalErrorCode.ERROR_PARAM, "更新中台订单参与类型失败，订单编号：" + orderNo);
        }
        FreshManToast freshManToast = new FreshManToast();
        freshManToast.setType(4); //消费VIP弹层
        freshManToast.setVipType(3);
        freshManToast.setCpId(Long.valueOf(groupHeadMemberId));
        freshManToast.setRecordTime(DateUtils.addSeconds(new Date(), 1800));//加上半小时
        freshManFlagService.updateFlag4Toast(freshManToast);

      }
    }
  }

  /**
   * 累加次数
   *
   * @param groupHeadMemberId 开团人cpId
   */
  private void updatePgNum(String groupHeadMemberId) {
    PromotionToConserve promotionToConserve = new PromotionToConserve();
    Integer pgNum = promotionToConserveMapper.selectByCpId(groupHeadMemberId);
    if (null != pgNum) {
      promotionToConserve.setCpId(Long.valueOf(groupHeadMemberId));
      promotionToConserve.setPgNum(++pgNum);//增加一
      promotionToConserveMapper.updateByPrimaryKeySelective(promotionToConserve);
    } else {
      promotionToConserveMapper.insert(groupHeadMemberId);
    }
  }

  private void toStrong(Order order, long cpId) {
    //TODO 判断订单类型是否为vip升级单，如果是需要记录关系变更轨迹
    if (order.getOrderType() != OrderSortType.VIP_RIGHT) {
      log.info("非VIP商品，跳过强关系绑定");
      return;
    }
    boolean hasUpline = customerProfileService.hasUplineCpId(cpId);
    if (hasUpline) {//有强关系直接返回
      log.info("已有强关系，跳过轨迹记录");
      return;
    }

    Long shareCpId = order.getShareCpId();
    // 修复强关系变更关系
    if(null != shareCpId && shareCpId.equals(cpId)){
      shareCpId = customerProfileService.getUpline(cpId, shareCpId);
    }
    Long fromConsumerCpId = consumerToConsumerMapper.selectUplineConsumerCpId(cpId);
    Long customerCpId = consumerToCustomerMapper.selectCustomerIdByConsumeId(cpId);
    Long weakShareCpId = weakLinkMapper.selectShareCpId(cpId);

    CustomerModifyLinkDetail customerModifyLinkDetail = new CustomerModifyLinkDetail();
    customerModifyLinkDetail.setCpid(cpId);
    if (fromConsumerCpId == null && customerCpId == null) {
      //原关系为弱关系，升级为强关系
      if (weakShareCpId != null && weakShareCpId != 0) {// shareCpId1有效
        customerModifyLinkDetail.setFromCpid(weakShareCpId);//原上级cpid
        customerModifyLinkDetail.setLinkDirection("WEAK_TO_STRONG");//弱关系->强关系
        //FIXME 填入当前分享人cpid，如果为空，取原关系上级
        customerModifyLinkDetail.setToCpid(shareCpId == null ? weakShareCpId : shareCpId);
        customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);

        //调用安畅接口，记录用户关系变更
        addCustomerRelationship(cpId,customerModifyLinkDetail.getToCpid(),3);
        log.info("弱关系->强关系,轨迹记录成功");
        return;
      } else {//弱关系都没有,传入公司作为记录
        customerModifyLinkDetail.setFromCpid(COMPANY_CPID);//原上级cpid
        customerModifyLinkDetail.setLinkDirection("WEAK_TO_STRONG");//弱关系->强关系
        //FIXME 填入当前分享人cpid，如果为空，取原关系上级
        customerModifyLinkDetail.setToCpid(shareCpId == null ? COMPANY_CPID : shareCpId);
        customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);

        //调用安畅接口，记录用户关系变更
        addCustomerRelationship(cpId,customerModifyLinkDetail.getToCpid(),3);
        log.info("弱关系->强关系,轨迹记录成功");
      }
    }
    //原关系为次强关系，升级为强关系
    Long upCpId = fromConsumerCpId != null ? fromConsumerCpId : customerCpId;//次强关系上级
    customerModifyLinkDetail.setFromCpid(upCpId);//原上级cpid
    customerModifyLinkDetail.setLinkDirection("SUBSTRONG_TO_STRONG");//次强关系->强关系
    //FIXME 填入当前分享人cpid，如果为空，取原关系上级
    customerModifyLinkDetail.setToCpid(shareCpId == null ? upCpId : shareCpId);
    customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);

    //调用安畅接口，记录用户关系变更
    addCustomerRelationship(cpId,customerModifyLinkDetail.getToCpid(),3);
    log.info("次强关系->强关系，轨迹记录成功");
  }

  /**
   * 尝试增加活动优惠次数
   *
   * @param cpId             cpId
   * @param pieceOrderDetail pieceOrderDetail
   */
  private void tryIncrementPromotionPriceNum(Long cpId, PromotionOrderDetail pieceOrderDetail) {
    final String pDetailCode = pieceOrderDetail.getP_detail_code();
    PiecePrice piecePrice = pgPromotionServiceAdapter.getPiecePrice(pDetailCode, cpId);
    BigDecimal oPenMemberPrice = piecePrice.getMemberPrice();
    Integer priceLimit = piecePrice.getLimit();
    // 如果享受了拼主价就增加次数
    if (isCanObtainOpenMemberPrice(cpId, pDetailCode, oPenMemberPrice, priceLimit)) {
      pieceCacheService.incrementPromotionPriceNum(String.valueOf(cpId), pDetailCode);
    }
  }

  /**
   * 能否享受拼主价
   *
   * @param cpId       cpId
   * @param detailCode detailCode
   * @param price      price
   * @param priceLimit priceLimit
   * @return true or false
   */
  private boolean isCanObtainOpenMemberPrice(Long cpId, String detailCode, BigDecimal price, Integer priceLimit) {
    return price != null && pieceCacheService.isCanObtainOpenMemberPrice(String.valueOf(cpId), detailCode, priceLimit);
  }

  /**
   * 绑定弱关系
   */
  private void saveWeekConnection(Long cpId, Order order) {
    if (cpId == null || order == null) {
      return;
    }
    boolean hasUpline = customerProfileService.hasUplineCpId(cpId);
    Long boundCpId = customerProfileService.getUplineWeekIgnoreIdentity(cpId);
    // 身份跟白人表都没有绑定过弱关系 且自己没有强关系
    if (boundCpId == null && !hasUpline) {
      Long fromCpId = orderHeaderService.loadFromCpId(order.getOrderNo());
      // 如果fromCpId为空, 则要么是没有分享, 要么是已经有强关系, 都不需要继续绑定弱关系
      if (fromCpId == null) {
        return;
      }

      //TODO 原有弱关系绑定变更为次强关系，记录身份变更轨迹
      Long weakShareCpId = weakLinkMapper.selectShareCpId(cpId);
      CustomerModifyLinkDetail customerModifyLinkDetail = new CustomerModifyLinkDetail();
      customerModifyLinkDetail.setCpid(cpId);
      //FIXME 设置原上级cpid，如果为空，则取公司
      customerModifyLinkDetail.setFromCpid(weakShareCpId == null ? COMPANY_CPID : weakShareCpId);
      customerModifyLinkDetail.setLinkDirection("WEAK_TO_SUBSTRONG");//弱关系->次强关系

      Long shareCpId = order.getShareCpId();
      //防止自己绑定自己
      if(null != shareCpId && cpId.equals(shareCpId)){
        shareCpId = customerProfileService.getUpline(cpId, fromCpId);
      }
      // 如果分享人跟当前下单人都是白人, 则保存到白人弱关系表
      if (shareCpId != null
          && !customerProfileService.hasUplineCpId(shareCpId)
          && !customerProfileService.hasUplineCpId(cpId)) {
        // 分享人白人上面的第一个有身份的人
        Long firstIdentityCpId = customerProfileService.getFirstIdentityUpline(shareCpId);
        customerProfileService.saveConsumerToConsumer(
            new ConsumerToConsumer(cpId, shareCpId, firstIdentityCpId));

        customerModifyLinkDetail.setToCpid(shareCpId);//设置当前分享人的cpid
        customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);

        //调用安畅接口，记录用户关系变更
        addCustomerRelationship(cpId,shareCpId,1);
        log.info("弱关系->次强关系(分享人也是白人)，关系变更轨迹记录成功");
        return;
      }
      if (fromCpId != 0) {// fromCpId有效, 则保存有身份的弱关系
        ConsumerToCustomer consumerToCustomer = new ConsumerToCustomer(cpId, fromCpId);
        customerProfileService.saveConsumerToCustomer(consumerToCustomer);

        //FIXME 设置当前分享人的cpid,如果为空，取原上级
        if (Objects.isNull(weakShareCpId)) {
          customerModifyLinkDetail.setToCpid(shareCpId == null ? fromCpId : shareCpId);
        } else {
          customerModifyLinkDetail.setToCpid(shareCpId == null ? weakShareCpId : shareCpId);
        }
        customerModifyLinkDetailMapper.insertSelective(customerModifyLinkDetail);

        //调用安畅接口，记录用户关系变更
        addCustomerRelationship(cpId,customerModifyLinkDetail.getToCpid(),1);
        log.info("弱关系->次强关系(分享人为有身份的人)，关系变更轨迹记录成功");
      }
    }
  }

  private boolean payRequestBuyer2Seller(Order order) {
    SubAccount fromAccount, toAccount;
    if (order.getStatus() == OrderStatus.PAID) {
      return true;
    }

    // 从买家消费账户转到卖家的担保账户
    fromAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    // 由于微商系统没有自营的概念 订单的资金流 分销 店铺 -> root shop
    //		toAccount = accountApiService.findSubAccountByUserId(
    //				order.getSellerId(), AccountType.DANBAO);
    User user = userService.load(order.getSellerId());
    String rootShopId = shopService.loadRootShop().getId();
    String rootUserId = shopService.load(rootShopId).getOwnerId();
    toAccount = accountApiService.findSubAccountByUserId(rootUserId, AccountType.DANBAO);
    //        PayRequest request = new PayRequest(order.getPayNo(),
    //                order.getOrderNo(), PayRequestBizType.ORDER,
    //                PayRequestPayType.DANBAO, order.getTotalFee().add(
    //                order.getDiscountFee()), fromAccount.getId(),
    //                toAccount.getId(), null);
    // fixme 订单金额退款问题，不能加优惠金额getDiscountFee
    PayRequest request =
        new PayRequest(
            order.getPayNo(),
            order.getOrderNo(),
            PayRequestBizType.ORDER,
            PayRequestPayType.DANBAO,
            order.getTotalFee(),
            fromAccount.getId(),
            toAccount.getId(),
            null);
    return payRequestApiService.payRequest(request);
  }

  @Override
  public void onShip(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onSign(Order order) {
    log.info("begin on order transaction [" + order.getOrderNo() + "] sign event");

    danbaoDone(order);
    // 查找对应main order下所有order是否完成

    log.info("end on order transaction [" + order.getOrderNo() + "] sign event");
  }

  @Override
  public void onRequestRefund(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptRefund(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectRefund(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onChange(Order order) {
    // 换货业务，待处理逻辑
  }

  @Override
  public void onRequestChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestReissue(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptReissue(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectReissue(Order order) {
    // TODO Auto-generated method stub
  }

  private static final BigDecimal ZERO_FEE = new BigDecimal("0.00");

  private static final String REFUND_REASON_QUALITY2 = "未收到货";

  @Override
  public void onRefund(Order order) {
    // 拼团订单退款，使用抽出的方法,支付状态必须为PAIDNOSTOCK
    if (OrderSortType.isPiece(order.getOrderType())
        && OrderStatus.PAIDNOSTOCK.equals(order.getStatus())) {
      refundForPg(order);
      return;
    }

    log.info("begin on order transaction [" + order.getOrderNo() + "] refund event");
    // 订单退款时恢复库存 modify by chh 2016-08-26
    List<OrderRefund> orderRefunds = orderRefundService.listByOrderId(order.getId());
    boolean refunding =
        order.getStatus().equals(OrderStatus.REFUNDING) && CollectionUtils.isNotEmpty(orderRefunds);
    boolean qualityProblem = false;
    if (refunding) {
      java.util.Optional<OrderRefund> refund = java.util.Optional.ofNullable(orderRefunds.get(0));
      qualityProblem = refund.map(input -> {
        String r = input.getRefundReason();
        if (StringUtils.isBlank(r)) {
          return false;
        } else {
          return r.equals(REFUND_REASON_QUALITY2);
        }
      }).orElse(false);
    }

    if (qualityProblem) { // 未收到货,不执行库存修改
      List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
      for (OrderItem orderItem : orderItems) {
        productService.updateStock(orderItem.getProductId(), -orderItem.getAmount()); // 只修改销量
      }
      refundFeeAndCommission(order); // 退款和退积分
      log.info("订单：" + order.getOrderNo() + "执行了未收到货的处理，库存没有增加，销量减少，积分退回");
    } else {
      List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
      for (OrderItem orderItem : orderItems) {
        updateStockOnCancel(order, orderItem); // 修改库存和销量
      }
      refundFeeAndCommission(order);
      log.info("订单：" + order.getOrderNo() + "执行退货入仓，库存增加，销量减少，积分退回============");
    }
  }

  @Value("${pay.callback.third_refund_url}")
  private String thirdRefundCallbackUrl;

  /**
   * 退款+退积分操作封装
   */
  private void refundFeeAndCommission(Order order) {
    // 订单退款时分佣全部设置为CLOSE状态 modify by chh 2016-08-26
    unionService.onCancel(order.getId());

    PayRequest searchPayRequest = new PayRequest();
    searchPayRequest.setBizId(order.getOrderNo());
    List<PayRequest> danbaoRequests = payRequestApiService.loadDanbaoRequest(searchPayRequest);
    if (danbaoRequests.isEmpty() && order.getPayNo() != null) {
      searchPayRequest.setPayNo(order.getPayNo());
      searchPayRequest.setBizId(null);
      danbaoRequests = payRequestApiService.loadDanbaoRequest(searchPayRequest);
    }

    if (danbaoRequests == null || danbaoRequests.size() == 0) {
      throw new BizException(
          GlobalErrorCode.INTERNAL_ERROR,
          "danbao account is empty orderNo=[" + order.getOrderNo() + "]");
    }

    log.info("danbao pay request data = {}", danbaoRequests.toString());

    // 目前不考虑预付款
    PayRequest danbaoRequest = danbaoRequests.get(0);

    // 现在最多能退多少钱
    searchPayRequest = new PayRequest();
    searchPayRequest.setBizId(order.getOrderNo());

    boolean refundable = payBack4Refund(danbaoRequest, order);
    if (!refundable) {
      refundCommission(order);
      refundFirstOrderLogisticRight(order);
      return;
    }
    danbaoDone(order);

    PaymentMode mode = order.getPayType();
    BigDecimal refundFee = order.getRefundFee();
    if (!refundFee.equals(ZERO_FEE)) {
      if (mode == PaymentMode.ALIPAY) {
        PaymentRequestVO paymentRequestVO = new PaymentRequestVO();
        paymentRequestVO.setOutTradeNo(order.getPayNo());
        paymentRequestVO.setTotalFee(order.getRefundFee());
        paymentRequestVO.setOutRequestNo(order.getOrderNo());
        try {
          aliPayment.payRefund(paymentRequestVO, order);
        } catch (Exception e) {
          log.error("alipay refund error:", e);
        }
      } else if (mode == PaymentMode.WEIXIN_APP) {
        tenPayment.refund(order);
      } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
        tenPayment.refund(order);
      }
    }

    promotionOrderSubject.onCancel(order);
    // 退换积分德分
    refundCommission(order);
    // 处理订单的首单免邮
    refundFirstOrderLogisticRight(order);
  }


  /**
   * 向支付平台推送退款信息
   * @param order
   */
  private void doRefund(Order order){
    PaymentMode mode = order.getPayType();
    BigDecimal refundFee = order.getRefundFee();
    if (!refundFee.equals(ZERO_FEE)) {
      ThirdRefundData data = new ThirdRefundData();
      data.setFromSystem(PayChannelConst.DEFAULT_FROM_SYSTEM);
      if (mode == PaymentMode.ALIPAY) {
        data.setPayType(PayTypeEnum.ALIPAY_APP.getPayType());
        data.setPayChannel(PayChannelConst.ALIPAY);
      } else if (mode == PaymentMode.WEIXIN_APP) {
        data.setPayType(PayTypeEnum.WECHAT_APP.getPayType());
        data.setPayChannel(PayChannelConst.WECHAT);
      } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
        data.setPayType(PayTypeEnum.WECHAT_MINIAPP.getPayType());
        data.setPayChannel(PayChannelConst.WECHAT);
      }

      data.setAmount(order.getRefundFee().multiply(new BigDecimal(100)).intValue());
      data.setTotalAmount(order.getTotalFee().multiply(new BigDecimal(100)).intValue());
      data.setOno(order.getOrderNo());
      data.setTradeNo(order.getPayNo());
      data.setCallbackUrl(thirdRefundCallbackUrl);
      thirdPaymentQueryService.doThirdRefund(data);
    }
  }

  private static final String REFUND_REASON_QUALITY = "商品质量问题";

  /**
   * 处理订单的首单免邮
   */
  private void refundFirstOrderLogisticRight(Order order) {
    String buyerId = order.getBuyerId();
    Long cpId = userService.selectCpIdByUserId(buyerId);
    LogisticDiscountType logisticDiscountType = order.getLogisticDiscountType();
    // 子单为首单免邮则总单下的其他单也应该为首单免邮(拆单复制属性)
    if (logisticDiscountType == LogisticDiscountType.FREE_FIRST_PAY) {
      // 若其他子单都为已退款状态, 则返还首单免邮资格
      try {
        String mainOrderId = order.getMainOrderId();
        List<Order> orders = orderService.selectStatusByMainOrderId(mainOrderId);
        if (orders.size() == 1) {
          // 没有拆单, 只需要判断自己
          userService.updateFreeLogistic(cpId, false);
        } else {
          // 是否所有其他子单都已经退款
          boolean allRestRefunded = true;
          for (Order inner : orders) {
            // 跳过自己
            if (StringUtils.equals(inner.getId(), order.getId())) {
              continue;
            }
            if (inner.getStatus() != OrderStatus.CLOSED) {
              allRestRefunded = false;
            }
          }
          if (allRestRefunded) {
            userService.updateFreeLogistic(cpId, false);
          }
        }
      } catch (Exception e) {
        log.error("订单 {} 取消后返还首单免邮资格失败", order.getOrderNo(), e);
      }
    }
  }

  /**
   * 退积分德分
   */
  private void refundCommission(Order order) {
    String buyerId = order.getBuyerId();
    Long cpId = userService.selectCpIdByUserId(buyerId);
    BigDecimal usingPoint = order.getPaidPoint();
    BigDecimal usingPointPacket = order.getPaidPointPacket();
    BigDecimal usingCommission = order.getPaidCommission();

    List<OrderRefund> orderRefunds = orderRefundService.listByOrderId(order.getId());
    boolean refunding =
        order.getStatus().equals(OrderStatus.REFUNDING) && CollectionUtils.isNotEmpty(orderRefunds);
    boolean qualityProblem = false;
    if (refunding) {
      Optional<OrderRefund> refund = Optional.fromNullable(orderRefunds.get(0));
      qualityProblem =
          refund
              .transform(
                  new Function<OrderRefund, Boolean>() {
                    @Override
                    public Boolean apply(OrderRefund input) {
                      String r = input.getRefundReason();
                      if (StringUtils.isBlank(r)) {
                        return false;
                      } else {
                        return r.equals(REFUND_REASON_QUALITY);
                      }
                    }
                  })
              .or(false);
    }
    try {
      if (null != usingPoint
          && usingPoint.signum() != 0
          && (order.isInOneHour() || (refunding && qualityProblem))) {
        PointCommOperationResult pointRet =
            pointCommService.modifyPoint(
                cpId,
                order.getOrderNo(),
                GradeCodeConstrants.RETURN_POINT_CODE,
                PlatformType.E,
                usingPoint,
                Trancd.DEDUCT_P,
                API);
        log.info("用户 {} 德分返还 {}", buyerId, usingPoint);
        log.info("返还明细: {}", pointRet);
      }
    } catch (Exception e) {
      log.error("用户 {} 返还德分失败", e);
    }

    try {
      if (null != usingCommission && usingCommission.signum() != 0) {
        PointCommOperationResult commRet =
            pointCommService.modifyCommission(
                cpId,
                order.getOrderNo(),
                GradeCodeConstrants.RETURN_COMMISSION_CODE,
                PlatformType.E,
                usingCommission,
                Trancd.DEDUCT_C,
                API);
        log.info("用户 {} 返还积分 {}", buyerId, usingCommission);
        log.info("返还明细: {}", commRet);
      }
    } catch (Exception e) {
      log.error("用户 {} 下单返还德分失败", e);
    }
    try {
      if (usingPointPacket != null && usingPointPacket.signum() != 0
          && order.isInOneHour() || (refunding && qualityProblem)) {
        pointGiftService.modifyWithDetail(cpId, usingPointPacket, order.getOrderNo(),
            PointSendType.ORDER_REFUND);
      }
    } catch (Exception e) {
      log.error("用户 {} 下单扣减红包德分失败", cpId);
    }
  }

  /**
   * 拼团退款退积分、德分操作的封装
   *
   * @param order
   */
  private void refundForPg(Order order) {
    unionService.onCancel(order.getId());
    // 现在最多能退多少钱
    PayRequest searchPayRequest = new PayRequest();
    searchPayRequest.setBizId(order.getOrderNo());
    BigDecimal maxRefundFee = payRequestApiService.findMaxRefundAmount(searchPayRequest);
    if (maxRefundFee.compareTo(BigDecimal.ZERO) == 0) {
      searchPayRequest.setPayNo(order.getPayNo());
      searchPayRequest.setBizId(null);
      maxRefundFee = payRequestApiService.findMaxRefundAmount(searchPayRequest);
    }

    if (order.getRefundFee().compareTo(maxRefundFee) > 0) {
      throw new BizException(
          GlobalErrorCode.INTERNAL_ERROR,
          "refundFee is more than orderNo=[" + order.getOrderNo() + "]");
    }

    if (order.getRefundFee().compareTo(maxRefundFee) < 0) {
      danbaoDone(order);
    }

    PaymentMode mode = order.getPayType();
    BigDecimal refundFee = order.getRefundFee();
    if (!refundFee.equals(ZERO_FEE)) {
      if (mode == PaymentMode.ALIPAY) {
        PaymentRequestVO paymentRequestVO = new PaymentRequestVO();
        paymentRequestVO.setOutTradeNo(order.getPayNo());
        paymentRequestVO.setTotalFee(order.getRefundFee());
        paymentRequestVO.setOutRequestNo(order.getOrderNo());
        try {
          aliPayment.payRefund(paymentRequestVO, order);
        } catch (Exception e) {
          log.error("alipay refund error:", e);
        }
      } else if (mode == PaymentMode.WEIXIN_APP) {
        tenPayment.refund(order);
      } else if (mode == PaymentMode.WEIXIN_MINIPROGRAM) {
        tenPayment.refund(order);
      }
    }

    promotionOrderSubject.onCancel(order);

    // 退换积分德分
    String buyerId = order.getBuyerId();
    Long cpId = userService.selectCpIdByUserId(buyerId);
    BigDecimal usingPoint = order.getPaidPoint();
    BigDecimal usingCommission = order.getPaidCommission();

    try {
      if (null != usingPoint && usingPoint.signum() != 0) {
        PointCommOperationResult pointRet =
            pointCommService.modifyPoint(
                cpId,
                order.getOrderNo(),
                GradeCodeConstrants.RETURN_POINT_CODE,
                PlatformType.E,
                usingPoint,
                Trancd.DEDUCT_P,
                API);
        log.info("用户 {} 德分返还 {}", buyerId, usingPoint);
        log.info("返还明细: {}", pointRet);
      }
    } catch (Exception e) {
      log.error("用户 {} 返还德分失败", e);
    }

    try {
      if (null != usingCommission && usingCommission.signum() != 0) {
        PointCommOperationResult commRet =
            pointCommService.modifyCommission(
                cpId,
                order.getOrderNo(),
                GradeCodeConstrants.RETURN_COMMISSION_CODE,
                PlatformType.E,
                usingCommission,
                Trancd.DEDUCT_C,
                API);
        log.info("用户 {} 返还积分 {}", buyerId, usingCommission);
        log.info("返还明细: {}", commRet);
      }
    } catch (Exception e) {
      log.error("用户 {} 下单返还德分失败", e);
    }
    // 处理订单的首单免邮
    refundFirstOrderLogisticRight(order);
  }

  // 确认收货前不能退
  private boolean payBack4Refund(PayRequest danbaoRequest, Order order) {

    String payNo = danbaoRequest.getPayNo();

    // 本笔支付已退了多少钱（合并支付已考虑在内）
    PayRequest seachRequest = new PayRequest();
    seachRequest.setPayNo(danbaoRequest.getPayNo());
    List<PayRequest> refundedRequests =
        payRequestApiService.loadRefundRequest(seachRequest); // 该支付号中担保账户退到消费账户的记录
    // 该支付号中担保账户中已经退了多少钱
    // BigDecimal refundedFee = BigDecimal.ZERO;
    // for(PayRequest refundedRequest : refundedRequests){
    // if(refundedRequest.getPayType() == PayRequestPayType.DANBAO2CONSUME)
    // refundedFee = refundedFee.add(refundedRequest.getAmount());
    // }
    // refundedFee = refundedFee.setScale(2, BigDecimal.ROUND_DOWN);

    // 从担保账户退到可消费账户（只退退款金额）
    PayRequest refundRequest =
        new PayRequest(
            payNo,
            order.getOrderNo(),
            PayRequestBizType.REFUND,
            PayRequestPayType.DANBAO2CONSUME,
            order.getRefundFee(),
            danbaoRequest.getToSubAccountId(),
            danbaoRequest.getFromSubAccountId(),
            danbaoRequest.getId());
    payRequestApiService.payRequest(refundRequest);

    // 可消费账户原路返回
    List<PayRequest> consumeRequests = payRequestApiService.loadConsumeReqeustByPayNo(payNo);
    if (consumeRequests == null || consumeRequests.size() == 0) {
      log.info("consume account is empty bizNo=[{}]", order.getOrderNo());
      return false;
    }

    // 排序，先退AVAILABLE可用账户里的钱，数据库时取出来已排序
    // consumeRequests = sort(consumeRequests);

    // 如果是退全款，则可能多笔交易，则直接根据payRequest记录的金额退回
    // 如果是部分退款，则只能是一笔担保交易，直接根据order.getRefundFee()的金额退回
    BigDecimal refundFee = order.getRefundFee(); // 本次共须退多少钱

    BigDecimal refundPlatformFee = BigDecimal.ZERO; // 退给平台的钱
    BigDecimal refundUserFee = BigDecimal.ZERO; // 退给买家的钱

    for (PayRequest consumeRequest : consumeRequests) {
      if (refundFee.compareTo(BigDecimal.ZERO) < 1) {
        break;
      }

      // 已退过款的，并退完的continue;
      boolean exists = false;
      BigDecimal reqRefundFee = BigDecimal.ZERO; // 该消费账户共能多少钱减去已退的钱
      for (PayRequest refundedRequest : refundedRequests) {
        if (consumeRequest.getId().equals(refundedRequest.getForRequestId())) {
          reqRefundFee = consumeRequest.getAmount().subtract(refundedRequest.getAmount());
          exists = true;
          break;
        }
      }
      if (exists && reqRefundFee.compareTo(BigDecimal.ZERO) == 0) {
        continue;
      }

      reqRefundFee =
          reqRefundFee.compareTo(BigDecimal.ZERO) > 0 ? reqRefundFee : consumeRequest.getAmount();

      // 消费账户退多少钱
      // BigDecimal reqRefundFee = BigDecimal.ZERO;

      // 该消费账户可退多少钱
      // BigDecimal canRefundFee = reqRefundFee.subtract(refundedFee);
      // 如果消费账户的钱大于等于须退款的钱
      if (reqRefundFee.compareTo(refundFee) > -1) { //
        reqRefundFee = refundFee;
      } /*
       * else{ reqRefundFee = canRefundFee; }
       */

      refundFee = refundFee.subtract(reqRefundFee); // 本次退款剩余退多少钱

      if (consumeRequest.getPayType() == PayRequestPayType.AVAILABLE2CONSUME) {

        refundUserFee = refundUserFee.add(reqRefundFee);

        // 消费账户退到可用余额
        refundRequest =
            new PayRequest(
                payNo,
                order.getOrderNo(),
                PayRequestBizType.REFUND,
                PayRequestPayType.CONSUME2AVAILABLE,
                reqRefundFee,
                consumeRequest.getToSubAccountId(),
                consumeRequest.getFromSubAccountId(),
                consumeRequest.getId());
        payRequestApiService.payRequest(refundRequest);

        // 可用余额再退到冻结账户
        SubAccount subAccount =
            accountApiService.findSubAccountBySubAccountId(consumeRequest.getFromSubAccountId());
        refundRequest =
            new PayRequest(
                payNo,
                order.getOrderNo(),
                PayRequestBizType.REFUND,
                PayRequestPayType.INSTANT,
                reqRefundFee,
                consumeRequest.getFromSubAccountId(),
                accountApiService
                    .findSubAccountByAccountId(subAccount.getAccountId(), AccountType.FROZEN)
                    .getId(),
                refundRequest.getId());
        payRequestApiService.payRequest(refundRequest);

      } else if (consumeRequest.getPayType() == PayRequestPayType.HONGBAO2CONSUME) {

        refundPlatformFee = refundPlatformFee.add(reqRefundFee);

        // 消费账户退到红包账户(买家的红包账户)
        refundRequest =
            new PayRequest(
                payNo,
                order.getOrderNo(),
                PayRequestBizType.REFUND,
                PayRequestPayType.CONSUME2HONGBAO,
                reqRefundFee,
                consumeRequest.getToSubAccountId(),
                consumeRequest.getFromSubAccountId(),
                consumeRequest.getId());
        payRequestApiService.payRequest(refundRequest);

        // 再返回
        //				if (StringUtils.isNotBlank(consumeRequest.getForRequestId())) {
        //					// TODO 由于充值那里的账户不规范， 现在都是想去,这里先写死
        //					String toAccount = accountApiService
        //							.findSubAccountByPayMode(PaymentMode.XIANGQU,
        //									AccountType.DEPOSIT).getId();
        //
        //					PayRequest fromRequest = payRequestApiService
        //							.loadByRequestId(consumeRequest.getForRequestId());
        //					PayRequest request = new PayRequest(payNo,
        //							order.getOrderNo(), PayRequestBizType.REFUND,
        //							PayRequestPayType.INSTANT, reqRefundFee,
        //							fromRequest.getToSubAccountId(), toAccount,
        //							refundRequest.getId());
        //					payRequestApiService.payRequest(request);
        //				}
      }
    }
    Order updateOrder = new Order();
    updateOrder.setId(order.getId());
    updateOrder.setRefundFee(refundUserFee);
    updateOrder.setRefundPlatformFee(refundPlatformFee);
    orderService.update(updateOrder);
    return true;
  }

  private boolean danbaoDone(Order order) {

    unionService.calcWechatCommission(order.getId());

    SubAccount fromAccount, toAccount;
    String payNo = payRequestApiService.generatePayNo();

    PayRequest request;

    // 分给卖家的钱
    BigDecimal sellerFee =
        order.getTotalFee().add(order.getDiscountFee()).subtract(order.getRefundFee());

    User user = userService.load(order.getSellerId());
    String rootShopId = shopTreeService.selectRootShopByShopId(user.getShopId()).getRootShopId();
    String rootUserId = shopService.load(rootShopId).getOwnerId();
    fromAccount = accountApiService.findSubAccountByUserId(rootUserId, AccountType.DANBAO);
    toAccount = accountApiService.findSubAccountByUserId(rootUserId, AccountType.AVAILABLE);

    request =
        new PayRequest(
            payNo,
            order.getOrderNo(),
            PayRequestBizType.ORDER,
            PayRequestPayType.INSTANT,
            sellerFee,
            fromAccount.getId(),
            toAccount.getId(),
            null);

    payRequestApiService.payRequest(request);

    BigDecimal logisticsFee = order.getLogisticsFee().setScale(2, BigDecimal.ROUND_DOWN);
    sellerFee = sellerFee.setScale(2, BigDecimal.ROUND_DOWN);
    // 只收运费，相当于全额退款，不分佣
    if (logisticsFee.compareTo(sellerFee) == 0) {
      return true;
    }

    // //计算佣金的费用，订单额-运费+平台优惠
    // BigDecimal cmCalculateFee = sellerFee.subtract(logisticsFee);
    // if(cmCalculateFee.compareTo(BigDecimal.ZERO) < 1 ){
    // return true; //只退运费
    // }

    // 再从卖家的可用余额转到佣金账户
    List<Commission> commissions = unionService.listByOrderId(order.getId());
    BigDecimal cmFee = BigDecimal.ZERO;
    for (Commission cm : commissions) {
      cmFee = cmFee.add(cm.getFee());
    }

    if (cmFee.compareTo(sellerFee) > 0) {
      log.info("danbao account is empty orderNo=[" + order.getOrderNo() + "]");
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "剩余的金额不足推广金额");
    }

    for (Commission cm : commissions) {
      // 从卖家的可消费账户里的钱转到 广告主的佣金账户
      /** if source owner id is not null that means it's a disbributed order */
      /** 微商系统只有自营商品 */
      //			if(StringUtils.isNotBlank(getDisbributedOrderSourceOwnerId(order))){
      //				fromAccount = accountApiService.findSubAccountByUserId(
      //						getDisbributedOrderSourceOwnerId(order), AccountType.AVAILABLE);
      //				toAccount = accountApiService.findSubAccountByUserId(
      //						cm.getUserId(), AccountType.COMMISSION);
      //			}else {
      //				fromAccount = accountApiService.findSubAccountByUserId(
      //						order.getSellerId(), AccountType.AVAILABLE);
      //				toAccount = accountApiService.findSubAccountByUserId(
      //						cm.getUserId(), AccountType.COMMISSION);
      //			}
      fromAccount = accountApiService.findSubAccountByUserId(rootUserId, AccountType.AVAILABLE);
      toAccount = accountApiService.findSubAccountByUserId(cm.getUserId(), AccountType.COMMISSION);

      request =
          new PayRequest(
              payNo,
              order.getOrderNo(),
              PayRequestBizType.ORDER,
              PayRequestPayType.INSTANT,
              cm.getFee(),
              fromAccount.getId(),
              toAccount.getId(),
              null);
      payRequestApiService.payRequest(request);
    }

    // 扣手续费
    BigDecimal techServiceFee = orderService.loadTechServiceFee(order);
    if (techServiceFee.compareTo(BigDecimal.ZERO) == 1) {
      fromAccount = accountApiService.findSubAccountByUserId(rootUserId, AccountType.AVAILABLE);
      toAccount =
          accountApiService.findSubAccountByUserId(
              userService.loadKkkdUserId(), AccountType.AVAILABLE);
      request =
          new PayRequest(
              payNo,
              order.getOrderNo(),
              PayRequestBizType.ORDER,
              PayRequestPayType.INSTANT,
              techServiceFee,
              fromAccount.getId(),
              toAccount.getId(),
              null);
      payRequestApiService.payRequest(request);
    }

    unionService.onSuccess(order.getId());
    return true;
  }

  /**
   * 修改DB中的库存
   */
  private void modifyMeetingAmount(
      String buyerId, String promotionId, Integer amount, String skuId) {

    // 拿RedisUtil实例
    //    String lockKey = promotionId + skuId;
    //    RedisUtils<String> redisUtils = RedisUtilFactory.getInstance(String.class,1000, 2000);
    //    try {
    //      //拿锁
    //      boolean isOk = redisUtils.lock(lockKey);
    //      int times =3;
    //      //库存锁定失败则重试三次
    //      if (!isOk) {
    //        while (times > 0) {
    //          isOk = redisUtils.lock(lockKey);
    //          if (isOk) {
    //            break;
    //          } else {
    //            times--;
    //          }
    //        }
    //      }
    // 库存锁定失败则退出
    //      if (!isOk) {
    //        return;
    //      }
    // 拿锁成功，则库存扣减
    Sku sku = skuMapper.selectByPrimaryKey(skuId);
    StockCheckBean stockCheckBean = new StockCheckBean();
    stockCheckBean.setResume_sku_num(amount);
    stockCheckBean.setP_code(promotionId);
    stockCheckBean.setSku_code(sku.getSkuCode());
    stockCheckService.deduceStock(stockCheckBean);
    // 库存锁定失败则退出
    //      if (!isOk) {
    //        return;
    //      }
    //    } catch (InterruptedException e) {
    //      log.warn("get stock lock failed,", e);
    //      return;
    //    } finally {
    //      redisUtils.unlock(lockKey);
    //    }
  }

  /**
   * 修改redis中的库存
   */
  private void modifyMoreCacheAmount(
      String buyerId, String promotionId, String skuCode, Integer amount, OrderItem orderItem) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String totalKey = PromotionConstants.getCommonAmountKey(promotionId, skuCode);
    List<PromotionSkuVo> promotionSkus = promotionSkusService.getPromotionSkus(skuCode);
    if (CollectionUtils.isEmpty(promotionSkus)) {
      return;
    }
    //如果是套装先扣套装库存
    boolean ismaster = skuCombineMapper.isMaster(orderItem.getSkuId());
    if (ismaster) {
      minusStock(orderItem.getProductId(), orderItem.getSkuId(), amount);
    }
    RedisLock lock =
        new RedisLock(redisTemplate, PromotionConstants.DISTRIBUTED_LOCKER, 10000, 20000);
    try {
      if (lock.lock()) {
        // 加用户购买数量, 减总库存数量
        redisUtils.increment(totalKey, -amount);
        if (redisUtils.get(totalKey) < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存不足");
        }
      }
    } catch (InterruptedException e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存修改失败", e);
    } finally {
      lock.unlock();
    }
    System.out.println(skuCode + " redis库存剩余：" + (long) redisUtils.get(totalKey));

  }

  /**
   * 修改redis中的库存
   */
  private void modifyCacheAmount(String buyerId, String promotionId, String skuId, Integer amount) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    String userKey = PromotionConstants.getUserAmountKey(buyerId, promotionId, skuId);
    String totalKey = PromotionConstants.getFlashSaleAmountKey(promotionId, skuId);

    RedisLock lock =
        new RedisLock(redisTemplate, PromotionConstants.DISTRIBUTED_LOCKER, 10000, 20000);

    try {
      if (lock.lock()) {
        // 加用户购买数量, 减总库存数量
        Integer amt = redisUtils.get(totalKey);
        if (amount > 0 && amount > amt) {
            // 扣减库存操作，且库存不足
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您来晚了, 商品库存不足");
        }
        Long after = redisUtils.increment(totalKey, -amount);
        if (after < 0) {
          // 二次校验后库存已被别人抢走
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "您来完了，商品库存不足");
        }
        redisUtils.increment(userKey, amount);
      }
    } catch (InterruptedException e) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存修改失败", e);
    } finally {
      lock.unlock();
    }
  }

  /**
   * add by hs
   * 一元购商品统一批次只能购一次
   * key=批次号:cpid:unionId
   */
  private void saleZoneLotLimit(String buyerId, String promotionId) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    User user = userService.load(buyerId);
    //查询promotion批次号
    Promotion promotion = promotionService.load(promotionId);
    String saleZoneLotKey = String.
        join(":", promotion.getBatchNo(),
            String.valueOf(user.getCpId()), user.getUnionId());

    //该用户是否第一次购买
    if (!redisUtils.hasKey(saleZoneLotKey)) {
      redisUtils.set(saleZoneLotKey, 1);
      long timeOutSetting = promotion.getValidTo().toInstant().plus(Duration.ofDays(1)).toEpochMilli()
          - System.currentTimeMillis();
      //设置过期时间活动结束 +1天
      redisUtils.expire(saleZoneLotKey, timeOutSetting, TimeUnit.MILLISECONDS);
    } else {
      redisUtils.increment(saleZoneLotKey, 1);
    }
  }

  /**
   * add by hs
   * 一元购商品统一批次只能购一次
   * 退款、退货
   */
  private void saleZoneLotLimitCancel(String buyerId, String promotionId) {
    RedisUtils<Integer> redisUtils = RedisUtilFactory.getInstance(Integer.class);
    User user = userService.load(buyerId);
    //查询promotion批次号
    Promotion promotion = promotionService.load(promotionId);
    String saleZoneLotKey = String.
        join(":", promotion.getBatchNo(),
            String.valueOf(user.getCpId()), user.getUnionId());
    redisUtils.increment(saleZoneLotKey, -1);
  }

  /**
   * 取消、退款时更新库存 如果为活动商品, 则更新redis库存, 不直接改商品库存
   */
  private void updateStockOnCancel(Order order, OrderItem orderItem) {
    try {
      final String skuId = orderItem.getSkuId();
      if (PromotionType.FLASHSALE == orderItem.getPromotionType()) {
        String buyerId = order.getBuyerId();
        String promotionId = orderItem.getPromotionId();
        modifyCacheAmount(buyerId, promotionId, orderItem.getSkuId(), -orderItem.getAmount());
      } else if (PromotionType.RESERVE == orderItem.getPromotionType()) {
        final String promotionId = order.getPromotionId();
        if (!reserveService.modifyStock(promotionId, skuId, -orderItem.getAmount())) {
          throw new BizException(GlobalErrorCode.RESERVE_PROMOTION_AMOUNT_LIMIT);
        }
      } else if (PromotionType.MEETING_GIFT == orderItem.getPromotionType()
          || PromotionType.MEETING == orderItem.getPromotionType()) {
        String buyerId = order.getBuyerId();

        PromotionConfig lock_type_config = promotionConfigService.selectByConfigType("lock_type");
        String lock_type = "";
        if (null == lock_type_config) {
          lock_type = "DB";
        }
        lock_type = lock_type_config.getConfigValue();
        Sku sku = skuMapper.selectByPrimaryKey(orderItem.getSkuId());
        if ("DB".equals(lock_type)) {
          List<PromotionSkuVo> promotionSkuVos =
              promotionSkusService.getPromotionSkus(sku.getSkuCode());
          //
          final String promotionId = order.getPromotionId();
          if (CollectionUtils.isNotEmpty(promotionSkuVos)) {
            modifyMeetingAmount(buyerId, promotionId, -orderItem.getAmount(), orderItem.getSkuId());
            //判断是否是套装
            boolean ismaster = skuCombineMapper.isMaster(orderItem.getSkuId());
            if (ismaster) {
              minusStock(orderItem.getProductId(), orderItem.getSkuId(), -orderItem.getAmount());
            }
          } else {
            ProductSkuVO productSkuVO =
                productService.load(orderItem.getProductId(), orderItem.getSkuId());
            if (productSkuVO.isDistributed()) {
              productService.updateSkuStock(
                  productSkuVO.getSourceSku().getId(), -orderItem.getAmount());
              productService.updateStock(
                  productSkuVO.getSourceProduct().getId(), -orderItem.getAmount());
              // 将商品上面的库存调整为所有sku库存中的最大值
              productService.updateProductAmountByMaxSkuAmount(
                  productSkuVO.getSourceProduct().getId());
            } else {
              minusStock(orderItem.getProductId(), orderItem.getSkuId(), -orderItem.getAmount());
            }
          }
        } else {
//                    PromotionConfig mp_config = promotionConfigService.selectByConfigType("meeting_pid");
//                    String promotionId = "";
//                    if (null == mp_config) {
//                        promotionId = "meeting";
//                    } else {
//                        promotionId = mp_config.getConfigValue();
//                    }
          List<PromotionBaseInfo> promotionBaseInfos = promotionBaseInfoService.selectEffectiveBySkuCode(sku.getSkuCode());
          String promotionId = "meeting";
          if (CollectionUtils.isNotEmpty(promotionBaseInfos)) {
            promotionId = promotionBaseInfos.get(0).getpCode();
          }

          // 对每个订单行的库存回退
          modifyMoreCacheAmount(buyerId, promotionId, sku.getSkuCode(), -orderItem.getAmount(), orderItem);
        }
      } else {
        minusStock(orderItem.getProductId(), orderItem.getSkuId(), -orderItem.getAmount());
      }

    } catch (Exception e) {
      log.debug(
          "order cancel failed, skuId:"
              + orderItem.getSkuId()
              + " should reduce amount:"
              + orderItem.getAmount()
              + " "
              + e.toString());
    }
  }

  /**
   * 修改库存, 如果是套装商品则会同步扣减套装商品库存 增加/扣减库存操作方向与父库存 传入的amount一致
   */
  private void minusStock(String productId, String skuId, Integer amount) {
    // 促销活动增加  生成的product 需要更新 源product和sku的库存 TODO 没有做套装的处理
    Product product = productService.findProductById(productId);
    if(null!=product && null!=product.getSourceId()){
      productService.updateStock(IdTypeHandler.encode(product.getSourceId()), amount);
      Sku sku = skuMapper.selectByPrimaryKey(skuId);
      if(null!= sku && StringUtils.isNotBlank(sku.getSourceSkuCode())){
        Sku bySkuCode = skuMapper.selectBySkuCode(sku.getSourceSkuCode());
        productService.updateSkuStock(bySkuCode.getId(), amount);
      }
      productService.updateProductAmountByMaxSkuAmount(IdTypeHandler.encode(product.getSourceId()));
    }

    productService.updateSkuStock(skuId, amount);
    productService.updateStock(productId, amount);
    // 将商品上面的库存调整为所有sku库存中的最大值
    productService.updateProductAmountByMaxSkuAmount(productId);
    boolean isCombineProduct = productCombineService.isCombineProduct(productId);
    if (!isCombineProduct) {
      return;
    }
    // 判断是套装商品则同时修改套装子商品库存
    List<SkuCombineExtra> slaveIds = productCombineService.listSlaveBySkuId(skuId);
    for (SkuCombineExtra slave : slaveIds) {
      String slaveId = slave.getSlaveId();
      Sku slaveSku = productService.loadSku(slaveId);
      if (slaveSku == null) {
        log.error("套装商品 {} 缺少子sku", productId);
        continue;
      }
      // 子商品扣减件数应该为父商品件数乘以子商品套装件数
      Integer slaveAmount = slave.getAmount() * amount;
      minusStock(slaveSku.getProductId(), slaveSku.getId(), slaveAmount);
    }
  }


  private boolean addCustomerRelationship(Long cpId,Long upline,Integer type) {
    if (Objects.isNull(cpId)) {
      log.info("方法入参空");
      return false;
    }
    //调用安畅接口，记录用户关系变更
    String path = "/v1/api/addCustomerRelationship";
    log.info("进入调用安畅关系绑定接口：" + cpId + ",upline=" + upline);
    String value = memberInfoMapper.selectSystemConfig();
    if (value == "1" || value.equals("1")) {
      Map<String, Object> mapTag = new HashMap<>();

      HttpResponse response = null; //接口返回
      String entity = null;//接口返回值
      mapTag.put("method", "addCustomerRelationship");
      mapTag.put("cpId", cpId);

      //url
      String repUrl =
              new StringBuffer().append(host).append(path)
                      .toString();
      ImmutableMap<String,Long> reParam = ImmutableMap.of("cpid", cpId
              , "upline", upline, "type", type.longValue(), "source", 3L);
      mapTag.put("请求参数", reParam);
      mapTag.put("请求URL", repUrl);
      log.info("关系升级调用安畅接口接口参数：" + mapTag);

      try {
        response = PoolingHttpClients.postJsonResponse(repUrl, reParam);
      } catch (Exception e) {
        log.error("关系升级调用安畅接口调用失败" + mapTag + e.getMessage());
        return false;
      }

      if (response == null) {
        log.error("关系升级调用安畅接口调用失败,返回null" + mapTag);
        return false;
      }

      try {
        entity = EntityUtils.toString(response.getEntity(), "utf-8");
      } catch (IOException e) {
        log.error("关系升级调用安畅接口解析返回体数据io异常" + e.getMessage());
        return false;
      }

      JSONObject json = JSON.parseObject(entity);
      mapTag.put("返回状态码", response.getStatusLine());
      mapTag.put("返回entity", json);

      int res_code = json.getIntValue("res_code");
      if(res_code == 10){
        log.info("关系升级调用安畅接口成功" + mapTag);
        return true;
      }
      log.info("关系升级调用安畅接口失败" + mapTag);
      return false;

    }else{
      log.info("关系升级调用安畅接口未开启！");
    }
    return false;
  }

}
