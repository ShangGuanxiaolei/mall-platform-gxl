package com.xquark.listener.handler.send;

import com.xquark.dal.model.Order;

public interface OrderSendErpHandler {

  public abstract void onSend(Order order);

}
