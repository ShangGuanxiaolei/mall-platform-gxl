package com.xquark.listener.handler.transaction;

import com.xquark.dal.model.Order;

public interface OrderTransactionHandler {

  public abstract void onSubmit(Order order);

  public abstract void onCancel(Order order);

  public abstract void onPay(Order order);

  public abstract void onShip(Order order);

  public abstract void onSign(Order order);

  public abstract void onRefund(Order order);

  public abstract void onRequestRefund(Order order);

  public abstract void onAcceptRefund(Order order);

  public abstract void onRejectRefund(Order order);

  public abstract void onChange(Order order);

  public abstract void onRequestChange(Order order);

  public abstract void onAcceptChange(Order order);

  public abstract void onRejectChange(Order order);

  public abstract void onRequestReissue(Order order);

  public abstract void onAcceptReissue(Order order);

  public abstract void onRejectReissue(Order order);

}
