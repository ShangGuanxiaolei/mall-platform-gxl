package com.xquark.listener.handler.transaction;

import com.xquark.dal.model.MainOrder;

public interface MainOrderTransactionHandler {

  public abstract void onSubmit(MainOrder order);

  public abstract void onCancel(MainOrder order);

  public abstract void onPay(MainOrder order);

  public abstract void onFinish(MainOrder order);

  public abstract void onRefund(MainOrder order);

  public abstract void onRequestRefund(MainOrder order);

  public abstract void onAcceptRefund(MainOrder order);

  public abstract void onRejectRefund(MainOrder order);

}
