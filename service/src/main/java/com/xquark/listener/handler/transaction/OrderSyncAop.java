package com.xquark.listener.handler.transaction;

import com.xquark.dal.model.Order;
import com.xquark.service.orderHeader.OrderHeaderService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * created by
 *
 * @author wangxinhua at 18-6-30 下午3:23 同步订单状态到三方表
 */
@Component
@Aspect
public class OrderSyncAop {

  private final OrderHeaderService orderHeaderService;

  @Autowired
  public OrderSyncAop(OrderHeaderService orderHeaderService) {
    this.orderHeaderService = orderHeaderService;
  }

  @Pointcut(value = "execution(* com.xquark.listener.handler.transaction."
      + "DanbaoOrderTransactionHandler.on*(..)) && args(order)", argNames = "order")
  public void onLayer(Order order) {
  }

  /**
   * 订单状态改变后同步到第三方
   */
  @AfterReturning(value = "onLayer(order)", argNames = "joinPoint,order")
  public void onStatusChange(JoinPoint joinPoint, Order order) {
    orderHeaderService.syncOrderHeaderStatus(order);
  }

}
