package com.xquark.listener.handler.notify;

import org.springframework.stereotype.Component;

import com.xquark.dal.model.Order;

@Component("codOrderNotifyHandler")
public class CodOrderNotifyHandler implements OrderNotifyHandler {

  @Override
  public void onSubmit(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onCancel(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onPay(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onShip(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onSign(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRequestRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onAcceptRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRejectRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRefund(Order order) {
    // TODO Auto-generated method stub

  }

}
