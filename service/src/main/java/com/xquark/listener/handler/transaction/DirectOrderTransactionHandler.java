package com.xquark.listener.handler.transaction;

import com.xquark.dal.model.*;
import com.xquark.dal.type.*;
import com.xquark.service.account.AccountApiService;
import com.xquark.service.account.AccountService;
import com.xquark.service.cart.CartService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.order.OrderService;
import com.xquark.service.pay.PayRequestApiService;
import com.xquark.service.personnel.PersonnelService;
import com.xquark.service.product.ProductService;
import com.xquark.service.union.UnionService;
import com.xquark.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component("directOrderTransactionHandler")
public class DirectOrderTransactionHandler implements OrderTransactionHandler {

  @Autowired
  private UserService userService;

  @Autowired
  private AccountApiService accountApiService;

  @Autowired
  private CartService cartService;

  @Autowired
  private OrderService orderService;

  @Autowired
  private ProductService productService;

  @Autowired
  private PayRequestApiService payRequestApiService;

  @Autowired
  private UnionService unionService;

  @Autowired
  protected AccountService accountService;

  @Autowired
  private PersonnelService personnelService;

  /**
   * 内部员工分润开关
   */
  @Value("${personnel.commission.on-off}")
  private boolean personnelCommissonOn;

  /**
   * 内部员工想去分润比
   */
  @Value("${personnel.commission.xiangqu.rate}")
  private String commissionRateXQ;

  @Value("${paymode.user.id.xiangqu}")
  private String xiangquUserId;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public void onSubmit(Order order) {
    List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
    log.info("begin on order[" + order.getOrderNo() + "] submit event");

    // 1. create direct deal
    //dealService.createDirectPayDealByOrder(order);

    // 2. update product stock
    for (OrderItem orderItem : orderItems) {
      // TODO 减库存的同步处理
      productService.updateSkuStock(orderItem.getSkuId(), orderItem.getAmount());
      productService.updateStock(orderItem.getProductId(), orderItem.getAmount());
      productService.updateProductAmountByMaxSkuAmount(orderItem.getProductId());
    }

    // 3. clear shopping cart
    if (order.getOrderSingleType() != OrderSingleType.SUBMIT) {
      for (OrderItem orderItem : orderItems) {
        cartService.removeBySku(orderItem.getSkuId());
      }
    }

    // 3. create union record
    // 调整到订单支付成功处理
//		unionService.calc(order.getId());

    log.info("end on order[" + order.getOrderNo() + "] submit event");
  }

  @Override
  public void onCancel(Order order) {
    List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
    log.info("begin on order[" + order.getOrderNo() + "] cancel event");

    // 1. restore product stock and status
    for (OrderItem orderItem : orderItems) {
      productService.updateSkuStock(orderItem.getSkuId(), -orderItem.getAmount());
    }

    //2. cancel commission
    unionService.onCancel(order.getId());

    log.info("end on order[" + order.getOrderNo() + "] cancel event");
  }

  @Override
  public void onPay(Order order) {
    log.info("begin on order[" + order.getOrderNo() + "] pay event");

    boolean f = false;
    unionService.calc(order.getId());

    //买家的可用余额到卖家的可用余额
    f = payRequestBuyer2Seller(order);
    if (!f) {
      log.info("payRequest buyer2Seller order[" + order.getOrderNo() + "] pay event");
      throw new BizException(GlobalErrorCode.UNKNOWN, "payRequest buyer2Seller occurs error");
    }

    unionService.onSuccess(order.getId());

    log.info("end on order[" + order.getOrderNo() + "] pay event");
  }

  /**
   * 红包充值
   */
	/*private PayRequest discountRecharge(Order order) {
		String payNo = payRequestApiService.generatePayNo();
		SubAccount fromAccount = accountApiService.findSubAccountByUserId(userService.loadKkkdUserId(), AccountType.HONGBAO);
		SubAccount toAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.HONGBAO);
		PayRequest request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER, PayRequestPayType.RECHARGE, order.getDiscountFee(), fromAccount.getId(), toAccount.getId(), null);
		boolean f = payRequestApiService.payRequest(request);
		if(!f){
			throw new BizException(GlobalErrorCode.UNKNOWN, "discountRecharge error");
		}
			
		return request;
    }*/

//    private PayRequest payRequestRecharge(Order order) {
//		// 2. craete deposit pay request
//		String payNo = payRequestApiService.generatePayNo();
//		SubAccount fromAccount = accountApiService.findSubAccountByPayMode(PaymentMode.ALIPAY, AccountType.AVAILABLE);
//		SubAccount toAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.AVAILABLE);
//		PayRequest request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER, PayRequestPayType.RECHARGE, order.getTotalFee(), fromAccount.getId(), toAccount.getId(), null);
//		boolean f = payRequestApiService.payRequest(request);
//		if(!f){
//			log.info("payRequest recharge order[" + order.getOrderNo() + "] pay event");
//			throw new BizException(GlobalErrorCode.UNKNOWN, "payRequest recharge error");
//		}
//			
//		return request;
//	}
//	
//    private boolean payRequestBuyer2Seller(Order order, List<String> forRequestIds) {
//        boolean result = true;
//        for (String requestId : forRequestIds) {
//            boolean r = payRequestBuyer2Seller(order, requestId);
//            result = result && r;
//        }
//        return result;
//    }
//    
  private boolean payRequestBuyer2Seller(Order order) {
    String payNo = payRequestApiService.generatePayNo();

    boolean f = false;
    SubAccount fromAccount = null, toAccount = null;
    PayRequest request = null;

    //把剩余的钱分到卖家的可用账户
    fromAccount = accountApiService.findSubAccountByUserId(order.getBuyerId(), AccountType.CONSUME);
    toAccount = accountApiService
        .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
    request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
        PayRequestPayType.INSTANT, order.getTotalFee().add(order.getDiscountFee()),
        fromAccount.getId(), toAccount.getId(), null);
    f = payRequestApiService.payRequest(request);

    List<Commission> commissions = unionService.listByOrderId(order.getId());
//		//分给卖家的钱
//		BigDecimal sellerFee = order.getTotalFee().add(order.getDiscountFee());
//		sellerFee = sellerFee.subtract(cmFee);

    for (Commission cm : commissions) {
      //从卖家的可用余额里的钱转到 广告主的佣金账户
      fromAccount = accountApiService
          .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
      toAccount = accountApiService.findSubAccountByUserId(cm.getUserId(), AccountType.COMMISSION);
      request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
          PayRequestPayType.INSTANT, cm.getFee(), fromAccount.getId(), toAccount.getId(), null);
      f = payRequestApiService.payRequest(request);
      if (!f) {
        return f;
      }
    }

    //扣手续费
    BigDecimal techServiceFee = orderService.loadTechServiceFee(order);
    if (techServiceFee.compareTo(BigDecimal.ZERO) == 1) {
      fromAccount = accountApiService
          .findSubAccountByUserId(order.getSellerId(), AccountType.AVAILABLE);
      toAccount = accountApiService
          .findSubAccountByUserId(userService.loadKkkdUserId(), AccountType.AVAILABLE);
      request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
          PayRequestPayType.INSTANT, techServiceFee, fromAccount.getId(), toAccount.getId(), null);
      payRequestApiService.payRequest(request);
    }

    //内部员工返回分润
    try {
      BigDecimal rateXQ = new BigDecimal(commissionRateXQ);
      if (personnelCommissonOn) {
        String buyerId = order.getBuyerId();
        if (order.getPartnerType() == UserPartnerType.XIANGQU
            || rateXQ.compareTo(BigDecimal.ZERO) > 0) {
          Personnel pXQ = personnelService
              .loadByExtUserIdAndPartner(buyerId, UserPartnerType.XIANGQU);
          if (pXQ != null) {
            Personnel pKKKD = personnelService
                .loadByInnerIdAndPartner(pXQ.getInnerId(), UserPartnerType.KKKD);
            if (pKKKD != null) {
              BigDecimal amount = BigDecimal.ZERO;
              for (Commission cm : commissions) {
                //从想去佣金中按比例返到员工可用余额
                if (cm.getUserId().equals(xiangquUserId)) {
                  amount = amount.add(cm.getFee());
                }
              }
              amount = amount.multiply(rateXQ).setScale(2, RoundingMode.DOWN);
              if (amount.compareTo(BigDecimal.ZERO) > 0) {
                fromAccount = accountApiService
                    .findSubAccountByUserId(xiangquUserId, AccountType.COMMISSION);
                toAccount = accountApiService
                    .findSubAccountByUserId(pKKKD.getExtUserId(), AccountType.AVAILABLE);
                request = new PayRequest(payNo, order.getOrderNo(), PayRequestBizType.ORDER,
                    PayRequestPayType.INSTANT, amount, fromAccount.getId(), toAccount.getId(),
                    null);
                payRequestApiService.payRequest(request);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      log.error("内部员工分润异常：orderNO=" + order.getOrderNo());
    }

    return f;
  }

  @Override
  public void onShip(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onSign(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onAcceptRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRejectRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRejectChange(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onRequestReissue(Order order) {
    // TODO Auto-generated method stub
  }

  @Override
  public void onAcceptReissue(Order order) {
// TODO Auto-generated method stub
  }

  @Override
  public void onRejectReissue(Order order) {
// TODO Auto-generated method stub
  }

  @Override
  public void onRefund(Order order) {
    // TODO Auto-generated method stub
    List<OrderItem> orderItems = orderService.listOrderItems(order.getId());
    for (OrderItem orderItem : orderItems) {
      productService.updateSkuStock(orderItem.getSkuId(), -orderItem.getAmount());
    }
  }

}
