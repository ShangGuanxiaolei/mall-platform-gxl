package com.xquark.listener.handler.notify;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Formatter;

import org.parboiled.common.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.xquark.dal.model.Message;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.User;
import com.xquark.dal.mybatis.IdTypeHandler;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.PushMsgId;
import com.xquark.dal.type.PushMsgType;
import com.xquark.dal.type.UserPartnerType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.event.MessageNotifyEvent;
import com.xquark.event.XQMessageNotifyEvent;
import com.xquark.service.account.AccountService;
import com.xquark.service.cashier.CashierService;
import com.xquark.service.msg.MessageService;
import com.xquark.service.order.OrderMessageService;
import com.xquark.service.order.OrderService;
import com.xquark.service.outpay.ThirdPartyPayment;
import com.xquark.service.user.UserService;

@Component("directOrderNotifyHandler")
public class DirectOrderNotifyHandler implements OrderNotifyHandler {

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private UserService userService;

  @Autowired
  private OrderService orderService;

  @Autowired
  protected AccountService accountService;

  @Autowired
  private OrderMessageService orderMessageService;

  @Autowired
  private MessageService messageService;

  @Autowired
  private CashierService cashierService;

  @Autowired
  private ThirdPartyPayment aliPayment;

  @Value("${tech.serviceFee.standard}")
  private String serviceFeethreshold;

  private Logger log = LoggerFactory.getLogger(getClass());

  private Boolean isKDUser(User user) {
    if (user != null && !user.getLoginname().substring(0, 2).equalsIgnoreCase("CID") && StringUtils
        .isEmpty(user.getExtUserId())) {
      return true;
    }
    return false;
  }

  private Boolean isXQUser(User user) {
    if (user != null && user.getPartner() != null && user.getPartner().equalsIgnoreCase("xiangqu")
        && StringUtils.isNotEmpty(user.getExtUserId())) {
      return true;
    }
    return false;
  }

  private void pushXQMsg(Long msgId, String baiduTag, Integer type, String relatId) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    XQMessageNotifyEvent event = new XQMessageNotifyEvent(message, baiduTag, null, type, relatId);
    applicationContext.publishEvent(event);
  }

  private String modifyContent(String content, Long msgId) {
    if (msgId.equals(PushMsgId.OrderHandFee.getId())) {
      Formatter fmt = new Formatter();
      try {
        fmt.format(content,
            StringUtils.isEmpty(serviceFeethreshold) ? "3000" : serviceFeethreshold);
        content = fmt.toString();
      } finally {
        fmt.close();
      }
    }
    return content;
  }

  private void pushMessage(Long msgId, String baiduTag, String url, Integer type, String relatId) {
    Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
    message.setContent(modifyContent(message.getContent(), msgId));
    MessageNotifyEvent event = new MessageNotifyEvent(message, baiduTag, url, type, relatId);
    applicationContext.publishEvent(event);
  }

  @Override
  public void onSubmit(Order order) {
    log.info("begin on order notify [" + order.getOrderNo() + "] submit event");
    // 4. sendding message
    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());
    OrderVO orderVO = orderService.loadVO(order.getId());

    //TODO 待修复
    if (order.getPartnerType() == UserPartnerType.KKKD) {
      orderMessageService.sendBuyerSMSOrderSubmitted(orderVO, seller, buyer);
    }

    // 5. if it's the first order, notify the seller
    if (orderService.countFirstStatus4Seller(order, OrderStatus.SUBMITTED)) {
      // "喜讯 - 有宝贝被购买啦"
      pushMessage(PushMsgId.FirstSelled.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_ORDER_FIRST_S.getValue(), null);
    }

    if (isKDUser(userService.load(order.getSellerId()))) {
      pushMessage(PushMsgId.NewOrder.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_ORDER_NEW_S.getValue(),
          order.getId());
      if (!orderService.loadTechServiceFee(orderVO).equals(BigDecimal.ZERO)) {
        pushMessage(PushMsgId.OrderHandFee.getId(), order.getSellerId(),
            Long.toString(new Date().getTime()), PushMsgType.MSG_ORDER_HANDFEE.getValue(),
            order.getId());
      }
    }

    User buyerUser = userService.load(order.getBuyerId());
    if (isXQUser(buyerUser)) {
      pushXQMsg(PushMsgId.NewOrder_XQ.getId(), buyerUser.getExtUserId(),
          PushMsgType.MSG_ORDER_NEW_B.getValue(), order.getId());
    }

    log.info("end on order notify [" + order.getOrderNo() + "] submit event");
  }

  @Override
  public void onCancel(Order order) {
    log.info("begin on order notify [" + order.getOrderNo() + "] cancel event");
    // 3. send sms mesage
// 		OrderVO orderVO = orderService.loadVO(order.getId());
// 		User seller = userService.load(order.getSellerId());
// 		User buyer = userService.load(order.getBuyerId());

// 		orderMessageService.sendBuyerSMSOrderCancelled(orderVO, seller, buyer);
// 		orderMessageService.sendSellerSMSOrderCancelled(orderVO, seller, buyer);

    if (isKDUser(userService.load(order.getSellerId()))) {
      pushMessage(PushMsgId.OrderCancel.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_ORDER_CLOSED_S.getValue(),
          order.getId());
    }

    User aBuyerUser = userService.load(order.getBuyerId());
    if (isXQUser(aBuyerUser)) {
      pushXQMsg(PushMsgId.OrderCancel_XQ.getId(), aBuyerUser.getExtUserId(),
          PushMsgType.MSG_ORDER_CLOSED_B.getValue(), order.getId());
    }

    //如果支付是选择支付宝的，调用支付宝接口取消订单，不论成功失败
    cashierService.closeTrade(order.getPayNo(), aBuyerUser.getId(), order.getOrderNo());

    log.info("end on order notify [" + order.getOrderNo() + "] cancel event");
  }

  @Override
  public void onPay(Order order) {
    log.info("begin on order notify [" + order.getOrderNo() + "] pay event");
    //send sms messsage
    OrderVO orderVO = orderService.loadVO(order.getId());
    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());

    orderMessageService.sendBuyerSMSOrderPaied(orderVO, seller, buyer);
    orderMessageService.sendSellerSMSOrderPaied(orderVO, seller, buyer);

    // if it's the first deal, notify the seller
    if (orderService.countFirstStatus4Seller(order, OrderStatus.PAID)) {
      // "喜讯 - 交易完成可以收款啦"
      pushMessage(PushMsgId.FirstIncome.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_INCOME_FIRST_S.getValue(), null);
    }

    if (isKDUser(userService.load(order.getSellerId()))) {
      pushMessage(PushMsgId.OrderPay.getId(), order.getSellerId(),
          Long.toString(new Date().getTime()), PushMsgType.MSG_ORDER_NEW_S.getValue(),
          order.getId());
    }

    User buyerUser = userService.load(order.getBuyerId());
    if (isXQUser(buyerUser)) {
      pushXQMsg(PushMsgId.OrderPay_XQ.getId(), buyerUser.getExtUserId(),
          PushMsgType.MSG_ORDER_PAYED_B.getValue(), order.getId());
    }

    log.info("end on order notify [" + order.getOrderNo() + "] pay event");
  }

  @Override
  public void onShip(Order order) {
    log.info("begin on order notify [" + order.getOrderNo() + "] ship event");

    OrderVO orderVO = orderService.loadVO(order.getId());
    User seller = userService.load(order.getSellerId());
    User buyer = userService.load(order.getBuyerId());
    orderMessageService.sendBuyerSMSOrderShipped(orderVO, seller, buyer);

    User aUser = userService.load(order.getBuyerId());
    if (isXQUser(aUser)) {
      pushXQMsg(PushMsgId.OrderShip_XQ.getId(), aUser.getExtUserId(),
          PushMsgType.MSG_ORDER_SHIPPED_B.getValue(), order.getId());
    }
    log.info("end on order notify [" + order.getOrderNo() + "] ship event");
  }

  @Override
  public void onSign(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRequestRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onAcceptRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRejectRefund(Order order) {
    // TODO Auto-generated method stub

  }

  @Override
  public void onRefund(Order order) {
    // TODO Auto-generated method stub

  }

}
