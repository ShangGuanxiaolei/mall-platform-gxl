package com.xquark.listener.handler.notify;

import com.xquark.dal.model.MainOrder;


public interface MainOrderNotifyHandler {

  public void onSubmit(MainOrder order);

  public void onCancel(MainOrder order);

  public void onPay(MainOrder order);

  public void onFinish(MainOrder order);

  public void onRefund(MainOrder order);

  public void onRequestRefund(MainOrder order);

  public void onAcceptRefund(MainOrder order);

  public void onRejectRefund(MainOrder order);

}
