package com.xquark.listener.handler.send;

import com.xquark.dal.model.Order;
import com.xquark.dal.type.SendErpDest;
import com.xquark.dal.type.SendErpStatus;
import com.xquark.service.order.OrderService;
import com.xquark.service.senmiao.SenMiaoErpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("senMiaoSendHandler")
public class SenMiaoSendHandler implements OrderSendErpHandler {

  @Autowired
  private SenMiaoErpService senMiaoErpService;

  @Autowired
  private OrderService orderService;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public void onSend(Order order) {
    log.info("begin on senmiao send transaction [" + order.getOrderNo()
        + "]  event");

    boolean isSuccess = senMiaoErpService.sendOrder(order);
    Date now = new Date();
    if (isSuccess) {
      orderService.updateOrderSendErpStatus(order.getId(),
          now, SendErpStatus.SUCCESS, SendErpDest.ECMOHO,
          "SUCCESS");
    } else {
      orderService.updateOrderSendErpStatus(order.getId(),
          now, SendErpStatus.FAIL, SendErpDest.ECMOHO,
          "fail");
    }
    log.info("end on senmiao send transaction [" + order.getOrderNo()
        + "]  event");
  }

}
