package com.xquark.listener;

import com.xquark.dal.model.Order;
import java.lang.String;
import com.xquark.event.OrderActionEvent;
import com.xquark.listener.handler.send.EcmohoSendHandler;
import com.xquark.listener.handler.send.OrderSendErpHandler;
import com.xquark.listener.handler.send.SenMiaoSendHandler;
import com.xquark.listener.handler.send.ShanHuYunSendHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class OrderSendListener implements SmartApplicationListener {

  @Autowired
  private EcmohoSendHandler ecmohoSendHandler;

  @Autowired
  private SenMiaoSendHandler senMiaoSendHandler;

  @Autowired
  private ShanHuYunSendHandler shanHuYunSendHandler;

  @Value("${order.send.erp.on-off}")
  private boolean sendErpSwitchOn;

  private Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public void onApplicationEvent(ApplicationEvent ev) {
    if (sendErpSwitchOn) {
      OrderActionEvent event = (OrderActionEvent) ev;
      Order order = (Order) event.getSource();

      OrderSendErpHandler handler = getOrderSendErpHandler(order);
      if (handler != null) {
        switch (event.getType()) {
          case PAY:
            handler.onSend(order);
            break;
          default:
            break;
        }
      } else {
        log.info("order send no order send to any erp!");
      }
    }
  }

  @Override
  public int getOrder() {
    return OrderActionEvent.secondStep;
  }

  @Override
  public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
    return eventType == OrderActionEvent.class;
  }

  @Override
  public boolean supportsSourceType(Class<?> sourceType) {
    return sourceType == Order.class;
  }

  private OrderSendErpHandler getOrderSendErpHandler(Order order) {
    String dest = order.getDest();
    log.info("order send dest order_no=" + order.getOrderNo());
    if (dest != null) {
      switch (dest) {
        case "ECMOHO_WANGDIAN":
          return ecmohoSendHandler;
        case "SENMIAO_WANGDIAN":
          return senMiaoSendHandler;
        case "SENMIAO_SHANHUYUN":
          return shanHuYunSendHandler;
        default:
          break;
      }
    } else {
      log.info("order send no dest!");
    }
    return null;
  }
}
