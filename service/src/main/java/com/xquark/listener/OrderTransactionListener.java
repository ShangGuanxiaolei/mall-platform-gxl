package com.xquark.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

import com.xquark.dal.model.Order;
import com.xquark.dal.type.OrderType;
import com.xquark.event.OrderActionEvent;
import com.xquark.listener.handler.transaction.CodOrderTransactionHandler;
import com.xquark.listener.handler.transaction.DanbaoOrderTransactionHandler;
import com.xquark.listener.handler.transaction.DirectOrderTransactionHandler;
import com.xquark.listener.handler.transaction.OrderTransactionHandler;
import com.xquark.listener.handler.transaction.PreOrderTransactionHandler;

@Component
public class OrderTransactionListener implements SmartApplicationListener {

  @Autowired
  private DirectOrderTransactionHandler directOrderTransactionHandler;

  @Autowired
  private DanbaoOrderTransactionHandler danbaoOrderTransactionHandler;

  @Autowired
  private PreOrderTransactionHandler preOrderTransactionHandler;

  @Autowired
  private CodOrderTransactionHandler codOrderTransactionHandler;

  @Override
  public void onApplicationEvent(ApplicationEvent ev) {
    OrderActionEvent event = (OrderActionEvent) ev;
    Order order = (Order) event.getSource();
    OrderTransactionHandler handler = null;
    if (order.getType().equals(OrderType.PIECE)) {
       handler = getOrderTransactionHandler(OrderType.DANBAO, order);
    } else {
       handler = getOrderTransactionHandler(order.getType(), order);
    }

    switch (event.getType()) {
      case SUBMIT:
        handler.onSubmit(order);
        break;
      case CANCEL:
        handler.onCancel(order);
        break;
      case PAY:
        handler.onPay(order);

        break;
      case SHIP:
        handler.onShip(order);
        break;
      case SIGN:
        handler.onSign(order);
        break;
      case REFUND:
        handler.onRefund(order);
        break;
      case REQUEST_REFUND:
        handler.onRequestRefund(order);
        break;
      case ACCEPT_REFUND:
        handler.onAcceptRefund(order);
        break;
      case REJECT_REFUND:
        handler.onRejectRefund(order);
        break;
      default:
        break;
    }
  }

  @Override
  public int getOrder() {
    return OrderActionEvent.firstStep;
  }

  @Override
  public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
    return eventType == OrderActionEvent.class;
  }

  @Override
  public boolean supportsSourceType(Class<?> sourceType) {
    return sourceType == Order.class || sourceType.getGenericSuperclass() == Order.class;
  }

  private OrderTransactionHandler getOrderTransactionHandler(OrderType type, Order order) {
    switch (type) {
      case DIRECT:
        return directOrderTransactionHandler;
      case DANBAO:
        return danbaoOrderTransactionHandler;
      case PREORDER:
        return preOrderTransactionHandler;
      case COD:
        return codOrderTransactionHandler;
      default:
        return null;
    }
  }
}
