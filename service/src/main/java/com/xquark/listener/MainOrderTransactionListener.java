package com.xquark.listener;

import com.xquark.dal.model.MainOrder;
import com.xquark.dal.type.OrderType;
import com.xquark.event.MainOrderActionEvent;
import com.xquark.listener.handler.transaction.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.SmartApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MainOrderTransactionListener implements SmartApplicationListener {

  @Autowired
  private DanbaoMainOrderTransactionHandler danbaoMainOrderTransactionHandler;

  @Override
  public void onApplicationEvent(ApplicationEvent ev) {
    MainOrderActionEvent event = (MainOrderActionEvent) ev;
    MainOrder order = (MainOrder) event.getSource();

    MainOrderTransactionHandler handler = getMainOrderTransactionHandler(order.getType());

    switch (event.getType()) {
      case SUBMIT:
        handler.onSubmit(order);
        break;
      case CANCEL:
        handler.onCancel(order);
        break;
      case PAY:
        handler.onPay(order);
        break;
      case FINISH:
        handler.onFinish(order);
        break;
      case REFUND:
        handler.onRefund(order);
        break;
      case REQUEST_REFUND:
        handler.onRequestRefund(order);
        break;
      case ACCEPT_REFUND:
        handler.onAcceptRefund(order);
        break;
      case REJECT_REFUND:
        handler.onRejectRefund(order);
        break;
      default:
        break;
    }
  }

  @Override
  public int getOrder() {
    return MainOrderActionEvent.firstStep;
  }

  @Override
  public boolean supportsEventType(Class<? extends ApplicationEvent> eventType) {
    return eventType == MainOrderActionEvent.class;
  }

  @Override
  public boolean supportsSourceType(Class<?> sourceType) {
    return sourceType == MainOrder.class;
  }

  private MainOrderTransactionHandler getMainOrderTransactionHandler(OrderType type) {
    switch (type) {

      case DANBAO:
        return danbaoMainOrderTransactionHandler;
      default:
        return null;
    }
  }
}
