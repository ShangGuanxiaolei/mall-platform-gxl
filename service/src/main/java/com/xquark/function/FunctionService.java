package com.xquark.function;

import com.xquark.dal.model.Function;
import com.xquark.dal.vo.FunctionVO;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

/**
 * Created by wangxinhua on 17-4-22. DESC:
 */
public interface FunctionService {

  Boolean save(Function function);

  Boolean delete(String id);

  Boolean deleteByHtmlId(String htmlId);

  Boolean update(Function function);

  Function load(String id);

  Function loadByHtmlId(String htmlId);

  List<Function> loadAll();

  List<FunctionVO> loadFunctionList();

  List<FunctionVO> loadFunctionVOList(String order, Pageable page, Direction direction);

  List<FunctionVO> loadFunctionVOList(String moduleId, String order, Pageable page,
      Direction direction);

  List<String> loadRoles(String functionId);

  List<String> loadEnableRoles(String functionId);

  List<String> loadVisibleRoles(String functionId);

  Boolean modifyVisibleRoles(String functionId, String[] rolesToSave, String[] rolesToRemove);

  Boolean modifyEnableRoles(String functionId, String[] rolesToSave, String[] rolesToRemove);

  Integer count();

  Integer count(String moduleId);


}
