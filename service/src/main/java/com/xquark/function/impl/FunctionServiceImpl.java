package com.xquark.function.impl;

import com.xquark.dal.mapper.FunctionMapper;
import com.xquark.dal.mapper.RoleFunctionMapper;
import com.xquark.dal.model.Function;
import com.xquark.dal.model.RoleFunction;
import com.xquark.dal.vo.FunctionVO;
import com.xquark.function.FunctionService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

/**
 * Created by wangxinhua on 17-4-22. DESC:
 */
@Service
public class FunctionServiceImpl implements FunctionService {


  private final FunctionMapper functionMapper;

  private final RoleFunctionMapper roleFunctionMapper;

  @Autowired
  public FunctionServiceImpl(FunctionMapper functionMapper, RoleFunctionMapper roleFunctionMapper) {
    this.functionMapper = functionMapper;
    this.roleFunctionMapper = roleFunctionMapper;
  }

  @Override
  public Boolean save(Function function) {
    Function exists = functionMapper.selectByHtmlId(function.getHtmlId());
    if (exists != null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要添加的按钮已存在");
    }
    return functionMapper.insert(function) > 0;
  }

  @Override
  public Boolean delete(String id) {
    Function exists = functionMapper.selectOne(id);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要删除的按钮不存在");
    }
    roleFunctionMapper.deleteByFunctionId(id);
    return functionMapper.deleteById(id) > 0;
  }

  @Override
  public Boolean deleteByHtmlId(String htmlId) {
    Function exists = functionMapper.selectByHtmlId(htmlId);
    if (exists == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "要删除的按钮不存在");
    }
    return functionMapper.deleteByHtmlId(htmlId) > 0;
  }

  @Override
  public Boolean update(Function function) {
    return functionMapper.update(function) > 0;
  }

  @Override
  public Function load(String id) {
    return functionMapper.selectOne(id);
  }

  @Override
  public Function loadByHtmlId(String htmlId) {
    return functionMapper.selectByHtmlId(htmlId);
  }

  @Override
  public List<Function> loadAll() {
    return functionMapper.listAll();
  }

  @Override
  public List<FunctionVO> loadFunctionList() {
    return functionMapper.listFunctionVO(null, null, null, null);
  }

  @Override
  public List<FunctionVO> loadFunctionVOList(String order, Pageable page, Direction direction) {
    return functionMapper.listFunctionVO(null, order, page, direction);
  }

  @Override
  public List<FunctionVO> loadFunctionVOList(String moduleId, String order, Pageable page,
      Direction direction) {
    return functionMapper.listFunctionVO(moduleId, order, page, direction);
  }

  @Override
  public List<String> loadRoles(String functionId) {
    Function function = functionMapper.selectOne(functionId);
    if (function == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "按钮不存在");
    }
    return functionMapper.listRoles(functionId);
  }

  @Override
  public List<String> loadEnableRoles(String functionId) {
    Function function = functionMapper.selectOne(functionId);
    if (function == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "按钮不存在");
    }
    return functionMapper.listEnableRoles(functionId);
  }

  @Override
  public List<String> loadVisibleRoles(String functionId) {
    Function function = functionMapper.selectOne(functionId);
    if (function == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "按钮不存在");
    }
    return functionMapper.listVisibleRoles(functionId);
  }

  @Override
  public Boolean modifyVisibleRoles(String functionId, String[] rolesToSave,
      String[] rolesToRemove) {

    if (rolesToSave != null) {
      for (String roleId : rolesToSave) {
        RoleFunction roleFunction = roleFunctionMapper.selectUnique(functionId, roleId);
        // 如果该条记录已存在则修改为可见, 不存在则新建记录并设置为可见
        if (roleFunction != null) {
          roleFunction.setIsShow(true);
          roleFunctionMapper.updateUnique(roleFunction);
        } else {
          roleFunction = new RoleFunction(functionId, roleId);
          roleFunction.setIsShow(true);
          roleFunction.setIsEnable(false);
          roleFunction.setArchive(false);
          roleFunctionMapper.insert(roleFunction);
        }
      }
    }

    if (rolesToRemove != null) {
      roleFunctionMapper.removeShow(functionId, rolesToRemove);
    }
    return true;
  }

  @Override
  public Boolean modifyEnableRoles(String functionId, String[] rolesToSave,
      String[] rolesToRemove) {
    if (rolesToSave != null) {
      for (String roleId : rolesToSave) {
        // 检查按钮是否已经跟该权限绑定过
        RoleFunction roleFunction = roleFunctionMapper.selectUnique(functionId, roleId);
        // 如果该条记录已存在则修改为可用, 不存在则新建记录并设置为可见
        if (roleFunction != null) {
          roleFunction.setIsEnable(true);
          roleFunctionMapper.updateUnique(roleFunction);
        } else {
          // 创建新记录
          roleFunction = new RoleFunction(functionId, roleId);
          roleFunction.setIsEnable(true);
          roleFunction.setIsShow(false);
          roleFunction.setArchive(false);
          roleFunctionMapper.insert(roleFunction);
        }
      }
    }

    if (rolesToRemove != null) {
      roleFunctionMapper.removeEnable(functionId, rolesToRemove);
    }
    return true;

  }

  @Override
  public Integer count() {
    return functionMapper.count();
  }

  @Override
  public Integer count(String moduleId) {
    return functionMapper.countByModuleId(moduleId);
  }
}
