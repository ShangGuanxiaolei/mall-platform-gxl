package com.xquark.openApiService.zone.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.xquark.dal.mapper.ZoneMapper;
import com.xquark.dal.model.Zone;
import com.xquark.openApiService.zone.OpenApiZoneService;

public class OpenApiZoneServiceImpl implements OpenApiZoneService {

  @Autowired
  private ZoneMapper zoneMapper;

  @Override
  public List<Zone> listParents(String zoneId) {
    List<Zone> zones = new ArrayList<Zone>();
    Zone zone = zoneMapper.selectByPrimaryKey(zoneId);
    if (zone == null) {
      return zones;
    }
    String path = zone.getPath();
    String[] zoneIds = path.split(">");

    for (String zId : zoneIds) {
      if (StringUtils.isEmpty(zId)) {
        continue;
      }
      zones.add(zoneMapper.selectByPrimaryKey(zId));
    }
    zones.add(zone);
    return zones;
  }


}
