package com.xquark.openApiService.zone;

import java.util.List;

import com.xquark.dal.model.Zone;

public interface OpenApiZoneService {

  List<Zone> listParents(String zoneId);
}
