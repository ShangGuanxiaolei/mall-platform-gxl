package com.xquark.openApiService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.xquark.dal.model.User;
import com.xquark.service.user.UserService;

public class BaseOpenApiServiceImpl implements BaseOpenApiService {

  protected Logger log = LoggerFactory.getLogger(getClass());


  @Autowired
  protected UserService userService;

  @Override
  public User getUserByPartnerAndExtuserid(String domain, String extUid) {
    return userService.loadExtUser(domain, extUid);
  }

}
