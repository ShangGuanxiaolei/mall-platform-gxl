package com.xquark.openApiService.order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xquark.dal.model.Order;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.vo.OrderVO;
import com.xquark.openApiService.BaseOpenApiService;

public interface OpenApiOrderService extends BaseOpenApiService {

  List<OrderVO> listByStatus4Buyer(String domain, String extUid, OrderStatus status,
      Pageable pageable);

  Long countBuyerOrdersByStatus(String domain, String extUid, OrderStatus status, Date periodStart,
      Date periodEnd);

  BigDecimal sumBuyerOrdersPaidFee(String domain, String extUid, Date periodStart, Date periodEnd);

  OrderVO loadVO(String orderId);

  void sign(String id, String domain, String extUid); // by zzd

  Order cancel(String id, String domain, String extUid); // by zzd

  void delaySign(String id, String domain, String extUid); // by zzd

  void checkPrivileges(String orderId, String domain, String extUid); // by zzd

  Boolean deleteByBuyer(String domain, String extUid, String orderId);

}
