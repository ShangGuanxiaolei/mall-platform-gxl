package com.xquark.openApiService.order.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xquark.dal.mapper.OrderAddressMapper;
import com.xquark.dal.mapper.OrderItemMapper;
import com.xquark.dal.mapper.OrderMapper;
import com.xquark.dal.mapper.ShopMapper;
import com.xquark.dal.mapper.UserMapper;
import com.xquark.dal.model.Order;
import com.xquark.dal.model.OrderAddress;
import com.xquark.dal.model.OrderItem;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.User;
import com.xquark.dal.model.Zone;
import com.xquark.dal.status.OrderStatus;
import com.xquark.dal.type.OrderActionType;
import com.xquark.dal.vo.OrderVO;
import com.xquark.openApiService.BaseOpenApiServiceImpl;
import com.xquark.openApiService.order.OpenApiOrderService;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.msg.MessageService;
import com.xquark.service.order.OrderService;
import com.xquark.service.zone.ZoneService;

@Service("openApiOrderService")
public class OpenApiOrderServiceImpl extends BaseOpenApiServiceImpl implements
    OpenApiOrderService {


  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private OrderItemMapper orderItemMapper;

  @Autowired
  private UserMapper userMapper;

  @Autowired
  private ShopMapper shopMapper;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private OrderAddressMapper orderAddressMapper;

  @Value("${order.delaysign.date}")
  private String delayDay;


  @Autowired
  private MessageService messageService;

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private OrderService orderService;

  @Override
  public List<OrderVO> listByStatus4Buyer(String domain, String extUid,
      OrderStatus status, Pageable pageable) {
    User user = getUserByPartnerAndExtuserid(domain, extUid);
    if (user == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "user.not.found");
    }

    List<OrderVO> orders = orderMapper.selectByBuyerAndStatus(user.getId(),
        status, pageable);
    Shop shop = null;
    User seller = null;
    for (OrderVO order : orders) {
      order.setOrderItems(orderItemMapper.selectByOrderId(order.getId()));
      seller = userMapper.selectByPKAndAdmin(order.getSellerId());
      order.setSellerPhone(seller.getPhone());
      shop = shopMapper.selectByPrimaryKey(seller.getShopId());
      order.setShopName(shop == null ? "" : shop.getName());
      order.setDefDelayDate(Integer.parseInt(delayDay));
    }
    return orders;
  }

  @Override
  public BigDecimal sumBuyerOrdersPaidFee(String domain, String extUid, Date periodStart,
      Date periodEnd) {
    User user = getUserByPartnerAndExtuserid(domain, extUid);
    BigDecimal result = new BigDecimal(0);
    return orderMapper.sumByBuyerAndStatus(user.getId(), periodStart, periodEnd);
  }

  @Override
  public Long countBuyerOrdersByStatus(String domain, String extUid, OrderStatus status,
      Date periodStart, Date periodEnd) {
    User user = getUserByPartnerAndExtuserid(domain, extUid);
    if (user == null) {
      return new Long(0);
    }
    if (status != null && status.equals(OrderStatus.CLOSED)) {
      Long cnt = 0L;
      cnt += orderMapper
          .countByBuyerAndStatus(user.getId(), OrderStatus.CANCELLED, periodStart, periodEnd);
      cnt += orderMapper
          .countByBuyerAndStatus(user.getId(), OrderStatus.SUCCESS, periodStart, periodEnd);
      cnt += orderMapper
          .countByBuyerAndStatus(user.getId(), OrderStatus.CLOSED, periodStart, periodEnd);
      return cnt;
    }
    return orderMapper.countByBuyerAndStatus(user.getId(), status, periodStart, periodEnd);
  }

  @Override
  public OrderVO loadVO(String orderId) {
    Order order = orderMapper.selectByPrimaryKey(orderId);
    if (order == null) {
      return null;
      // throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "非法订单编号");
    }
    OrderVO vo = new OrderVO();
    BeanUtils.copyProperties(order, vo);

    List<OrderItem> orderItems = orderItemMapper.selectByOrderId(orderId);
    vo.setOrderItems(orderItems);

    OrderAddress orderAddress = orderAddressMapper.selectByOrderId(orderId);
    vo.setOrderAddress(orderAddress);

    List<Zone> zoneList = zoneService.listParents(orderAddress.getZoneId());
    String addressDetails = "";
    for (Zone zone : zoneList) {
      addressDetails += zone.getName();
    }
    addressDetails += orderAddress.getStreet();
    vo.setAddressDetails(addressDetails);

    User seller = userService.load(order.getSellerId());
    if (seller != null) {
      vo.setSellerPhone(seller.getPhone());
      vo.setShopId(vo.getShopId());
    }
    vo.setDefDelayDate(Integer.parseInt(delayDay));

    return vo;
  }

  @Override
  public void sign(String orderId, String domain, String extUid) {
    checkPrivileges(orderId, domain, extUid);
    orderService.executeBySystem(orderId, OrderActionType.SIGN, null);
  }

  @Override
  @Transactional
  public Order cancel(String orderId, String domain, String extUid) {
    checkPrivileges(orderId, domain, extUid);
    orderService.executeBySystem(orderId, OrderActionType.CANCEL, null);
    Order order = loadVO(orderId);
    return order;
  }

  @Override
  public void delaySign(String orderId, String domain, String extUid) {
    checkPrivileges(orderId, domain, extUid);
    orderService.executeBySystem(orderId, OrderActionType.DELAYSIGN, null);
  }
	
	/*@SuppressWarnings("resource")
	private void pushMessage(Long msgId, String baiduTag, String url, Integer type, String relatId) {
		if (msgId == null) {
			log.warn("pushMessage msgId is null, type:" + type + ", relatId:" + relatId);
			return ;
		}
		Message message = messageService.loadMessage(IdTypeHandler.encode(msgId));
		if (msgId.equals(PushMsgId.OrderDelaySign.getId())) {
			Formatter fmt = new Formatter();
			try {
				fmt.format(message.getContent(), delayDay);
			} finally {
				message.setContent(fmt.toString());
			}
		} else if (msgId.equals(PushMsgId.OrderDelaySignRemind.getId()) ||
					 msgId.equals(PushMsgId.OrderRemindShip.getId())) {
			Formatter fmt = new Formatter();
			try {
				//Order order = load(relatId);
				Order order =loadVO(relatId);
				if (order != null) {
					fmt.format(message.getContent(), order.getOrderNo());
				} else {
					fmt.format(message.getContent(), "");
				}
			} finally {
				message.setContent(fmt.toString());
			}
		}
		MessageNotifyEvent event = new MessageNotifyEvent(message, baiduTag, url, type, relatId);
		applicationContext.publishEvent(event);
	}*/

  @Override
  public void checkPrivileges(String orderId, String domain, String extUid) {
    Order order = loadVO(orderId);
    User user = userService.loadExtUser(domain, extUid);
    if (user == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "取消订单的用户不存在");
    }

    if (!order.getBuyerId().equals(user.getId())) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "用户无权限操作");
    }

  }

  @Override
  public Boolean deleteByBuyer(String domain, String extUid, String orderId) {
    checkPrivileges(orderId, domain, extUid);
    return orderService.deleteByBuyer(orderId);
  }


}
