package com.xquark.userFamily;

import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.FamilySetting;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserFamily;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.service.ArchivableEntityService;

import java.math.BigDecimal;
import java.util.List;


public interface FamilyCardSettingService extends ArchivableEntityService<FamilyCardSetting> {

  /**
   * 创建配置信息
   */
  int create(FamilyCardSetting familyCardSetting);

  /**
   * 更新配置信息
   */
  int update(FamilyCardSetting familyCardSetting);

  /**
   * 通过 id 查找配置信息
   */
  FamilyCardSetting load(String id);

  /**
   * 通过 shopId 查找配置信息
   */
  List<FamilyCardSetting> loadDefaultList(String shopId);

//    /**
//     * 通过 用户id 查找配置
//     * @param userId
//     * @return
//     */
//    FamilyCardSetting findByUser(String userId);

//    /**
//     * 获取当前登录用户的配置信息
//     * @return
//     */
//    FamilyCardSetting mine();

  /**
   * 通过 shopId 查找sliver配置信息
   */
  FamilyCardSetting loadSilverCardSetting(String shopId);


  FamilyCardSetting selectCardByShopIdAndFee(String shopId, BigDecimal amountDemand);
}
