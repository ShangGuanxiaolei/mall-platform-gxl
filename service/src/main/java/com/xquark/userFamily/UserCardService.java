package com.xquark.userFamily;

import com.xquark.dal.model.UserCard;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.service.ArchivableEntityService;

import java.util.List;


public interface UserCardService extends ArchivableEntityService<UserCard> {

  /**
   * 创建userCard信息
   */
  int create(UserCard userCard);

  /**
   * 更新userCard信息
   */
  int update(UserCard userCard);

  /**
   * 通过 id 查找userCard信息
   */
  UserCard load(String id);

  /**
   * 通过 familyCardStatus 查找
   */
  List<UserCard> selectUserCardsByStatus(FamilyCardStatus familyCardStatus);

  UserCard selectUserCardsByUserIdAndShopId(String userId, String shopId);

  UserCard selectUserCardsByUserId(String userId);

  List<UserCard> listUserCardsByShopId(String shopId);
}
