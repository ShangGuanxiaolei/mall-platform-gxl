package com.xquark.userFamily;

import com.xquark.dal.model.FamilySetting;
import com.xquark.service.ArchivableEntityService;


public interface FamilySettingService extends ArchivableEntityService<FamilySetting> {

  /**
   * 创建配置信息
   */
  int create(FamilySetting familySetting);

  /**
   * 更新配置信息
   */
  int update(FamilySetting familySetting);

  /**
   * 通过 id 查找配置信息
   */
  FamilySetting load(String id);

//    /**
//     * 通过 用户id 查找配置
//     * @param userId
//     * @return
//     */
//    FamilySetting findByUser(String userId);
//
//    /**
//     * 获取当前登录用户的配置信息
//     * @return
//     */
//    FamilySetting mine();
}
