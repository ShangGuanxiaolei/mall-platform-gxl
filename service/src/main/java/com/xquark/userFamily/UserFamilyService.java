package com.xquark.userFamily;

import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserFamily;
import com.xquark.service.ArchivableEntityService;


public interface UserFamilyService extends ArchivableEntityService<UserFamily> {

  /**
   * 创建配置信息
   */
  int create(UserFamily userFamily);

  /**
   * 更新配置信息
   */
  int update(UserFamily userFamily);

  /**
   * 通过 id 查找配置信息
   */
  UserFamily load(String id);

//    /**
//     * 通过 用户id 查找配置
//     * @param userId
//     * @return
//     */
//    UserCard findByUser(String userId);
//
//    /**
//     * 获取当前登录用户的配置信息
//     * @return
//     */
//    UserCard mine();
}
