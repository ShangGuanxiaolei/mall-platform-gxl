package com.xquark.userFamily.impl;


import com.xquark.dal.mapper.FamilyCardSettingMapper;
import com.xquark.dal.mapper.ShopWechatSettingMapper;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.ShopWechatSetting;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wechat.ShopWechatSettingService;
import com.xquark.userFamily.FamilyCardSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


@Service("familyCardSettingService")
public class FamilyCardSettingServiceImpl extends BaseServiceImpl implements
    FamilyCardSettingService {

  @Autowired
  private FamilyCardSettingMapper familyCardSettingMapper;


  @Override
  public int create(FamilyCardSetting familyCardSetting) {
    return familyCardSettingMapper.insert(familyCardSetting);
  }

  @Override
  public int update(FamilyCardSetting familyCardSetting) {
    return familyCardSettingMapper.updateByPrimaryKey(familyCardSetting);
  }

  @Override
  public int insert(FamilyCardSetting familyCardSetting) {
    return familyCardSettingMapper.insert(familyCardSetting);
  }

  @Override
  public int insertOrder(FamilyCardSetting familyCardSetting) {
    return familyCardSettingMapper.insert(familyCardSetting);
  }

  @Override
  public FamilyCardSetting load(String id) {
    return familyCardSettingMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<FamilyCardSetting> loadDefaultList(String shopId) {
    return familyCardSettingMapper.selectByShopId(shopId);
  }

  @Override
  public FamilyCardSetting loadSilverCardSetting(String shopId) {
    return familyCardSettingMapper.selectSilverCardByShopId(shopId);
  }

  @Override
  public FamilyCardSetting selectCardByShopIdAndFee(String shopId, BigDecimal amountDemand) {
    List<FamilyCardSetting> listCardSetting = familyCardSettingMapper.selectByShopId(shopId);
    for (FamilyCardSetting familyCardSetting : listCardSetting) {
      Integer feeDemand = familyCardSetting.getAmountDemand();
      double personCmRate = familyCardSetting.getPersonalCmRate();
      double subordinateCmRate = familyCardSetting.getSubordinateCmRate();
      if (personCmRate != 0.00 && subordinateCmRate != 0.00 && feeDemand != 0) {
        if (amountDemand.compareTo(new BigDecimal(feeDemand)) == 1) {
          return familyCardSetting;
        }
      }
    }

    return null;
  }

//	@Override
//	public FamilyCardSetting findByUser(String userId) {
//		return familyCardSettingMapper.selectByUserId(userId);
//	}
//
//	@Override
//	public FamilyCardSetting mine() {
//		return familyCardSettingMapper.selectByUserId(getCurrentUser().getId());
//	}

  @Override
  public int delete(String id) {
    return familyCardSettingMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return familyCardSettingMapper.unDeleteByPrimaryKey(id);
  }
}
