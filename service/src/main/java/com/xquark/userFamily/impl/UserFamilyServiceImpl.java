package com.xquark.userFamily.impl;


import com.xquark.dal.mapper.UserCardMapper;
import com.xquark.dal.mapper.UserFamilyMapper;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.model.UserFamily;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.userFamily.UserCardService;
import com.xquark.userFamily.UserFamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("userFamilyService")
public class UserFamilyServiceImpl extends BaseServiceImpl implements UserFamilyService {

  @Autowired
  private UserFamilyMapper userFamilyMapper;


  @Override
  public int create(UserFamily userCard) {
    return userFamilyMapper.insert(userCard);
  }

  @Override
  public int update(UserFamily userCard) {
    return userFamilyMapper.updateByPrimaryKey(userCard);
  }

  @Override
  public int insert(UserFamily userCard) {
    return userFamilyMapper.insert(userCard);
  }

  @Override
  public int insertOrder(UserFamily userFamily) {
    return userFamilyMapper.insert(userFamily);
  }

  @Override
  public UserFamily load(String id) {
    return userFamilyMapper.selectByPrimaryKey(id);
  }

//	@Override
//	public FamilyCardSetting findByUser(String userId) {
//		return familyCardSettingMapper.selectByUserId(userId);
//	}
//
//	@Override
//	public FamilyCardSetting mine() {
//		return familyCardSettingMapper.selectByUserId(getCurrentUser().getId());
//	}

  @Override
  public int delete(String id) {
    return userFamilyMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userFamilyMapper.unDeleteByPrimaryKey(id);
  }
}
