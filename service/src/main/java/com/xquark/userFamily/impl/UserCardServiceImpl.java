package com.xquark.userFamily.impl;


import com.xquark.dal.mapper.UserCardMapper;
import com.xquark.dal.model.UserCard;
import com.xquark.dal.status.FamilyCardStatus;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.userFamily.UserCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("userCardService")
public class UserCardServiceImpl extends BaseServiceImpl implements UserCardService {

  @Autowired
  private UserCardMapper userCardMapper;


  @Override
  public int create(UserCard userCard) {
    return userCardMapper.insert(userCard);
  }

  @Override
  public int update(UserCard userCard) {
    return userCardMapper.updateByPrimaryKey(userCard);
  }

  @Override
  public int insert(UserCard userCard) {
    return userCardMapper.insert(userCard);
  }

  @Override
  public int insertOrder(UserCard userCard) {
    return userCardMapper.insert(userCard);
  }

  @Override
  public UserCard load(String id) {
    return userCardMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<UserCard> selectUserCardsByStatus(FamilyCardStatus status) {
    return userCardMapper.selectUserCardsByStatus(status);

  }

  @Override
  public UserCard selectUserCardsByUserIdAndShopId(String userId, String shopId) {
    return userCardMapper.selectUserCardsByUserIdAndShopId(userId, shopId);
  }

  @Override
  public UserCard selectUserCardsByUserId(String userId) {
    return userCardMapper.selectUserCardsByUserId(userId);
  }

  @Override
  public List<UserCard> listUserCardsByShopId(String shopId) {
    return userCardMapper.listUserCardsByShopId(shopId);
  }

//	@Override
//	public FamilyCardSetting findByUser(String userId) {
//		return familyCardSettingMapper.selectByUserId(userId);
//	}
//
//	@Override
//	public FamilyCardSetting mine() {
//		return familyCardSettingMapper.selectByUserId(getCurrentUser().getId());
//	}

  @Override
  public int delete(String id) {
    return userCardMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return userCardMapper.unDeleteByPrimaryKey(id);
  }
}
