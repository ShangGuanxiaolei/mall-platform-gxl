package com.xquark.userFamily.impl;


import com.xquark.dal.mapper.FamilyCardSettingMapper;
import com.xquark.dal.mapper.FamilySettingMapper;
import com.xquark.dal.model.FamilyCardSetting;
import com.xquark.dal.model.FamilySetting;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.userFamily.FamilyCardSettingService;
import com.xquark.userFamily.FamilySettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("familySettingService")
public class FamilySettingServiceImpl extends BaseServiceImpl implements FamilySettingService {

  @Autowired
  private FamilySettingMapper familySettingMapper;


  @Override
  public int create(FamilySetting familySetting) {
    return familySettingMapper.insert(familySetting);
  }

  @Override
  public int update(FamilySetting familySetting) {
    return familySettingMapper.updateByPrimaryKey(familySetting);
  }

  @Override
  public int insert(FamilySetting familySetting) {
    return familySettingMapper.insert(familySetting);
  }

  @Override
  public int insertOrder(FamilySetting familySetting) {
    return familySettingMapper.insert(familySetting);
  }

  @Override
  public FamilySetting load(String id) {
    return familySettingMapper.selectByPrimaryKey(id);
  }

//	@Override
//	public FamilyCardSetting findByUser(String userId) {
//		return familyCardSettingMapper.selectByUserId(userId);
//	}
//
//	@Override
//	public FamilyCardSetting mine() {
//		return familyCardSettingMapper.selectByUserId(getCurrentUser().getId());
//	}

  @Override
  public int delete(String id) {
    return familySettingMapper.deleteByPrimaryKey(id);
  }

  @Override
  public int undelete(String id) {
    return familySettingMapper.unDeleteByPrimaryKey(id);
  }
}
