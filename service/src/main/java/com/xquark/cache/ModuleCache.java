package com.xquark.cache;

import com.xquark.dal.IUser;
import com.xquark.dal.vo.ModuleVO;
import com.xquark.service.module.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by wangxinhua on 17-4-15. DESC:
 */
@Component
public class ModuleCache extends InitCache {

  public static String KEY = "userModules";

  private static Logger logger = LoggerFactory.getLogger(ModuleCache.class);

//
//    @Autowired
//    private MerchantService merchantService;

  @Autowired
  private ModuleService moduleService;

  @Autowired
  private RedisTemplate redisTemplate;

  @Override
  public void refresh() {
    // 初始化时不刷新，已修改为登陆时刷新
//        Map<String, List<ModuleVO>> cachedModule = new HashMap<String, List<ModuleVO>>();
//        List<Merchant> merchants = merchantService.listAll();
//        if (merchants != null && merchants.size() > 0) {
//            for (Merchant merchant : merchants) {
//                List<MerchantRole> roles = roleService.listRoleByMerchantId(merchant.getId());
//                List<String> strRoles;
//                if (roles != null && roles.size() > 0) {
//                    strRoles = new ArrayList<String>();
//                    for (MerchantRole role : roles) strRoles.add(role.getRoleName());
//                    // TODO wangxh 兼容旧属性
//                } else strRoles = Arrays.asList(merchant.getRoles().split(","));
//
//                if (strRoles.size() > 0) {
//                    List<ModuleVO> moduleVOS = moduleService.listTree(strRoles);
//                    String hashKey = merchant.getLoginname();
//                    cachedModule.put(hashKey, moduleVOS);
//                }
//
//            }
//        }
//
//        logger.info("刷新菜单缓存----------------");
//        redisTemplate.opsForHash().putAll(KEY, cachedModule);

  }

  public void refreshByUser(IUser user) {
    Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
    List<String> strRoles = new ArrayList<String>();
    List<ModuleVO> moduleVOS;
    boolean hasRoot = false;
    if (authorities != null) {
      for (GrantedAuthority authority : authorities) {
        String role = StringUtils.substringAfter(authority.getAuthority(), "_");
        if ("ROOT".equals(role)) {
          hasRoot = true;
        }
        strRoles.add(role);
      }
    }
    // 如果为ROOT用户则加载所有菜单
    if (strRoles.size() > 0) {
      if (hasRoot) {
        moduleVOS = moduleService.treeMenuListAll();
      } else {
        moduleVOS = moduleService.listTree(strRoles);
      }
    } else {
      moduleVOS = new ArrayList<ModuleVO>();
    }
    logger.info("刷新用户 {} 的菜单缓存", user.getUsername());
    redisTemplate.opsForHash().put(KEY, user.getUsername(), moduleVOS);
  }

  @Override
  public boolean isCached() {
    return redisTemplate.hasKey(KEY) && redisTemplate.type(KEY) == DataType.HASH;
  }

  @Override
  public void remove() {
    redisTemplate.delete(KEY);
  }

  @Override
  public Object load(String loginName) {
    return redisTemplate.opsForHash().get(KEY, loginName);
  }

  @Override
  public boolean contains(String hashKey) {
    return redisTemplate.opsForHash().hasKey(KEY, hashKey);
  }

}
