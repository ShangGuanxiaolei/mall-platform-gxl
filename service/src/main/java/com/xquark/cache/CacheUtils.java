package com.xquark.cache;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xquark.dal.model.IndexAble;
import com.xquark.dal.util.SpringContextUtil;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.util.CollectionUtils;

/**
 * Created by wangxinhua on 17-11-15. DESC:
 */
public class CacheUtils {

  private static final Logger logger = LoggerFactory.getLogger(CacheUtils.class);

  @SuppressWarnings("unchecked")
  private static final RedisTemplate<String, String> stringRedisTemplate = SpringContextUtil
      .getBean("stringRedisTemplate", RedisTemplate.class);

  @SuppressWarnings("unchecked")
  private static final RedisTemplate<String, Object> redisTemplate = SpringContextUtil
      .getBean("redisTemplate", RedisTemplate.class);

  private static final ObjectMapper objectMapper = SpringContextUtil
      .getBean("objectMapper", ObjectMapper.class);

  /**
   * 删除缓存<br> 根据key精确匹配删除
   */
  @SuppressWarnings("unchecked")
  public static void del(String... key) {
    if (key != null && key.length > 0) {
      if (key.length == 1) {
        redisTemplate.delete(key[0]);
      } else {
        redisTemplate.delete(CollectionUtils.arrayToList(key));
      }
    }
  }

  /**
   * 批量删除<br> （该操作会执行模糊查询，请尽量不要使用，以免影响性能或误删）
   */
  public static void batchDel(String... pattern) {
    for (String kp : pattern) {
      redisTemplate.delete(redisTemplate.keys(kp + "*"));
    }
  }

  /**
   * 取得缓存（int型）
   */
  public static Integer getInt(String key) {
    String value = stringRedisTemplate.boundValueOps(key).get();
    if (StringUtils.isNotBlank(value)) {
      return Integer.valueOf(value);
    }
    return null;
  }

  /**
   * 设置缓存
   *
   * @param key 键
   * @param obj 值
   */
  public static void setObj(String key, Object obj) {
    redisTemplate.boundValueOps(key).set(obj);
  }

  /**
   * 取得缓存（字符串类型）
   */
  public static String getStr(String key) {
    return stringRedisTemplate.boundValueOps(key).get();
  }

  /**
   * 取得缓存（字符串类型）
   */
  public static String getStr(String key, boolean retain) {
    String value = stringRedisTemplate.boundValueOps(key).get();
    if (!retain) {
      redisTemplate.delete(key);
    }
    return value;
  }

  /**
   * 获取缓存<br> 注：基本数据类型(Character除外)，请直接使用get(String key, Class<T> clazz)取值
   */
  public static Object getObj(String key) {
    return redisTemplate.boundValueOps(key).get();
  }

  /**
   * 获取缓存<br> 注：java 8种基本类型的数据请直接使用get(String key, Class<T> clazz)取值
   *
   * @param retain 是否保留
   */
  public static Object getObj(String key, boolean retain) {
    Object obj = redisTemplate.boundValueOps(key).get();
    if (!retain) {
      redisTemplate.delete(key);
    }
    return obj;
  }

  /**
   * 获取缓存<br> 注：该方法暂不支持Character数据类型
   *
   * @param key key
   * @param clazz 类型
   */
  @SuppressWarnings("unchecked")
  public static <T> T get(String key, Class<T> clazz) {
    return (T) redisTemplate.boundValueOps(key).get();
  }

  /**
   * 将value对象写入缓存
   *
   * @param time 失效时间(秒)
   */
  public static void set(String key, Object value, long time) {
    if (value.getClass().equals(String.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Integer.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Double.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Float.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Short.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Long.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else if (value.getClass().equals(Boolean.class)) {
      stringRedisTemplate.opsForValue().set(key, value.toString());
    } else {
      redisTemplate.opsForValue().set(key, value);
    }
    if (time > 0) {
      redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }
  }

  /**
   * 将value写入缓存
   *
   * @param key 键
   * @param value 值
   */
  public static void set(String key, Object value) {
    set(key, value, 0);
  }

  /**
   * 递减操作
   */
  public static double decr(String key, double by) {
    return redisTemplate.opsForValue().increment(key, -by);
  }

  /**
   * 递增操作
   */
  public static double incr(String key, double by) {
    return redisTemplate.opsForValue().increment(key, by);
  }

  /**
   * 获取double类型值
   */
  public static double getDouble(String key) {
    String value = stringRedisTemplate.boundValueOps(key).get();
    if (StringUtils.isNotBlank(value)) {
      return Double.valueOf(value);
    }
    return 0d;
  }

  /**
   * 设置double类型值
   *
   * @param time 失效时间(秒)
   */
  public static void setDouble(String key, double value, long time) {
    stringRedisTemplate.opsForValue().set(key, String.valueOf(value));
    if (time > 0) {
      stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
    }
  }

  /**
   * 设置double类型值
   *
   * @param time 失效时间(秒)
   */
  public static void setInt(String key, int value, long time) {
    stringRedisTemplate.opsForValue().set(key, String.valueOf(value));
    if (time > 0) {
      stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
    }
  }

  /**
   * 将map写入缓存
   */
  public static <T> void setMap(String key, Map<String, T> map) {
    redisTemplate.opsForHash().putAll(key, map);
  }

  /**
   * 向key对应的map中添加缓存对象
   */
  public static <T> void addMap(String key, Map<String, T> map) {
    redisTemplate.opsForHash().putAll(key, map);
  }

  /**
   * 向key对应的map中添加缓存对象
   *
   * @param key cache对象key
   * @param field map对应的key
   * @param value 值
   */
  public static void addMap(String key, String field, String value) {
    redisTemplate.opsForHash().put(key, field, value);
  }

  /**
   * 向key对应的map中添加缓存对象
   *
   * @param key cache对象key
   * @param field map对应的key
   * @param obj 对象
   */
  public static <T> void addMap(String key, String field, T obj) {
    redisTemplate.opsForHash().put(key, field, obj);
  }

  /**
   * 获取map缓存
   */
  public static <T> Map<String, T> mget(String key) {
    BoundHashOperations<String, String, T> boundHashOperations = redisTemplate
        .boundHashOps(key);
    return boundHashOperations.entries();
  }

  /**
   * 获取map缓存中的某个对象
   */
  @SuppressWarnings("unchecked")
  public static <T> T getMapField(String key, String field, Class<T> clazz) {
    return (T) redisTemplate.boundHashOps(key).get(field);
  }

  /**
   * 删除map中的某个对象
   *
   * @param key map对应的key
   * @param field map中该对象的key
   * @author lh
   * @date 2016年8月10日
   */
  public void delMapField(String key, String... field) {
    BoundHashOperations<String, String, ?> boundHashOperations = redisTemplate
        .boundHashOps(key);
    boundHashOperations.delete(field);
  }

  /**
   * 指定缓存的失效时间
   *
   * @param key 缓存KEY
   * @param time 失效时间(秒)
   * @author FangJun
   * @date 2016年8月14日
   */
  public static void expire(String key, long time) {
    if (time > 0) {
      redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }
  }

  /**
   * 添加set
   */
  public static void sadd(String key, String... value) {
    redisTemplate.boundSetOps(key).add(value);
  }

  /**
   * 删除set集合中的对象
   */
  public static void srem(String key, String... value) {
    redisTemplate.boundSetOps(key).remove(value);
  }

  /**
   * set重命名
   */
  public static void srename(String oldkey, String newkey) {
    redisTemplate.boundSetOps(oldkey).rename(newkey);
  }

  /**
   * 模糊查询keys
   */
  public static Set<String> keys(String pattern) {
    return redisTemplate.keys(pattern);
  }

  /**
   * 向缓存中添加带排序号的值
   *
   * @param key 键
   * @param value 值
   * @param score 顺序
   */
  public static void zAddString(String key, String value, int score) {
    stringRedisTemplate.opsForZSet().add(key, value, score);
  }

  /**
   * 向缓存中添加带排序号的值, 先序列化为JSON字符串
   *
   * @param key 键
   * @param obj 对象
   * @param score 顺序
   */
  public static void zAdd(String key, Object obj, int score) throws JsonProcessingException {
    zAddString(key, objectMapper.writeValueAsString(obj), score);
  }

  /**
   * 向缓存中添加带排序号的值, 先序列化为JSON字符串
   *
   * @param key 键
   * @param obj 对象
   */
  public static void zAdd(String key, Object obj) throws JsonProcessingException {
    // 若对象支持索引排序，则获取索引
    int index = 0;
    if (obj instanceof IndexAble) {
      index = ((IndexAble) obj).getScore();
    }
    zAdd(key, obj, index);
  }

  public static void zRemove(String key, Object obj) throws JsonProcessingException {
    stringRedisTemplate.opsForZSet().remove(key, objectMapper.writeValueAsString(obj));
  }

  /**
   * 根据关键字匹配删除 通过遍历删除，效率较低
   *
   * @param key 键
   * @param match 关键字
   * @param <T> 类型
   */
  public static <T> void zRemoveMatch(String key, String match) throws JsonProcessingException {
    @SuppressWarnings("unchecked")
    Cursor<TypedTuple<String>> cursor = stringRedisTemplate.opsForZSet()
        .scan(key, ScanOptions.scanOptions()
            .match("*".concat(match).concat("*"))
            .build());
    while (cursor.hasNext()) {
      TypedTuple<String> property = cursor.next();
      zRemove(key, property.getValue());
    }
  }

  /**
   * 更新zSet中的对象
   *
   * @param key zSet键
   * @param oldValue 根据该值查找
   * @param newValue 新对象
   */
  public static void zUpdate(String key, String oldValue, String newValue)
      throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, JsonProcessingException {
    zRemove(key, oldValue);
    zAdd(key, newValue);
  }

  /**
   * 根据match查找zSet
   *
   * @param key zSet键
   * @param match 查询参数
   * @param clazz 返回参数Class
   * @param <T> 返回参数类型
   * @return 有序结果集
   */
  public static <T> Set<T> zScan(String key, String match, Class<T> clazz) throws IOException {
    @SuppressWarnings("unchecked")
    Cursor<TypedTuple<String>> cursor = stringRedisTemplate.opsForZSet()
        .scan(key, ScanOptions.scanOptions()
            .match("*".concat(match).concat("*"))
            .build());
    Set<T> result = new TreeSet<>();
    while (cursor.hasNext()) {
      TypedTuple<String> json = cursor.next();
      result.add(objectMapper.readValue(json.getValue(), clazz));
    }
    return result;
  }


}
