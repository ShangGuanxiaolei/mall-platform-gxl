package com.xquark.cache;

import com.xquark.dal.util.SpringContextUtil;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Created by wangxinhua on 17-11-19. DESC: redisTemplate工厂类， 确保每种类型只存在单个实例
 *
 * @deprecated 使用 {@link RedisUtilFactory}
 */
@Deprecated
public class RedisTemplateFactory {

  private static final ConcurrentHashMap<Class<?>, RedisTemplate<?, ?>> TEMPLATE_MAP = new ConcurrentHashMap<>();

  private static final StringRedisSerializer STRING_REDIS_SERIALIZER = new StringRedisSerializer();

  private static final RedisConnectionFactory CONNECTION_FACTORY = SpringContextUtil
      .getBean("redisConnectionFactory", RedisConnectionFactory.class);

  /**
   * 获取redisTemplate实例
   *
   * @param clazz redisTemplate 的value类型
   * @param <K> redisTemplate 的key类型
   * @param <V> redisTemplate 的value类型
   * @return {@link RedisTemplate} 实例
   */
  public static <K, V> RedisTemplate<K, V> getInstance(Class<V> clazz) {
    @SuppressWarnings("unchecked")
    RedisTemplate<K, V> template = (RedisTemplate<K, V>) TEMPLATE_MAP.get(clazz);
    if (template == null) {
      TEMPLATE_MAP.putIfAbsent(clazz, createTemplate(clazz));
    }
    return template;
  }

  /**
   * 根据clazz的类型构建一个redisTemplate
   *
   * @param clazz redis的value类型
   * @param <K> key的泛型类型
   * @param <V> value的泛型类型
   * @return {@link RedisTemplate} 的实例
   */
  private static <K, V> RedisTemplate<K, V> createTemplate(Class<V> clazz) {
    RedisTemplate<K, V> template = new RedisTemplate<>();
    template.setConnectionFactory(CONNECTION_FACTORY);
    // 设置序列化转换器
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setHashKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(clazz));
    template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    return template;
  }
}
