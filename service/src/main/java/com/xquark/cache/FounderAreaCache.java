package com.xquark.cache;

import com.xquark.dal.model.Zone;
import com.xquark.dal.vo.FunctionVO;
import com.xquark.dal.vo.RoleFunctionVO;
import com.xquark.dal.vo.UserAgentVO;
import com.xquark.function.FunctionService;
import com.xquark.service.agent.UserAgentService;
import com.xquark.service.zone.ZoneService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chh on 17-6-7. DESC: 缓存代理联合创始人对应的封地区域
 */
@Component
public class FounderAreaCache extends InitCache {

  private static Logger logger = LoggerFactory.getLogger(FounderAreaCache.class);
  public static final String KEY = "founderArea";

  @Autowired
  private UserAgentService userAgentService;

  @Autowired
  private ZoneService zoneService;

  @Autowired
  private RedisTemplate<String, Object> redisTemplate;

  @Override
  public void refresh() {
    // 查询所有联合创始人代理信息
    List<UserAgentVO> founders = userAgentService.getAllFounder();
    Map<String, List<Zone>> founderCache = new HashMap<String, List<Zone>>();
    if (founders != null) {
      for (UserAgentVO userAgentVO : founders) {
        String userId = userAgentVO.getUserId();
        String area = userAgentVO.getArea();
        if (StringUtils.isNotEmpty(area)) {
          List<Zone> zones = zoneService.listAllChildrenIncludeSelf(area, "%>" + area + ">%");
          founderCache.put(userId, zones);
        }
      }
      logger.info("刷新联合创始人封地区域缓存");
      redisTemplate.opsForHash().putAll(KEY, founderCache);
    }
  }

  @Override
  public Object load(String hashKey) {
    return redisTemplate.opsForHash().get(KEY, hashKey);
  }

  @Override
  public boolean isCached() {
    return redisTemplate.hasKey(KEY);
  }

  @Override
  public void remove() {
    redisTemplate.delete(KEY);
  }

  @Override
  public boolean contains(String hashKey) {
    return redisTemplate.opsForHash().hasKey(KEY, hashKey);
  }

  public Map<Object, Object> entries() {
    return redisTemplate.opsForHash().entries(KEY);
  }
}
