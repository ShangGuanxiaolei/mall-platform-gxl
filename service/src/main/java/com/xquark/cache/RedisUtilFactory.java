package com.xquark.cache;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Preconditions.checkNotNull;

/** Created by wangxinhua on 17-11-19. DESC: redisTemplate工厂类， 确保每种类型只存在单个实例 */
public class RedisUtilFactory {

  private static ConcurrentHashMap<Class<?>, RedisUtils<?>> UTIL_MAP;

  private static final StringRedisSerializer STRING_REDIS_SERIALIZER = new StringRedisSerializer();

  private static RedisConnectionFactory CONNECTION_FACTORY;

  public static void initialize(RedisConnectionFactory factory) {
    CONNECTION_FACTORY = factory;
    UTIL_MAP =
        new ConcurrentHashMap<Class<?>, RedisUtils<?>>() {
          {
            put(BigDecimal.class, createTemplate(BigDecimal.class));
            put(String.class, createTemplate(String.class));
            put(Integer.class, createTemplate(Integer.class));
            put(Set.class, createTemplate(Set.class));
            put(Long.class, createTemplate(Long.class));
            put(Map.class, createTemplate(Map.class));
            put(Boolean.class, createTemplate(Boolean.class));
            put(List.class, createTemplate(List.class));
            put(Object.class, createTemplate(Object.class));
          }
        };
  }

  /**
   * 获取redisUtils实例 非线程安全
   *
   * @param vClass redis value class对象
   * @param <V> value 类型
   * @return redisUtils实例
   */
  public static <V> RedisUtils<V> getInstance(Class<V> vClass) {
    @SuppressWarnings("unchecked")
    RedisUtils<V> util = (RedisUtils<V>) UTIL_MAP.get(vClass);
    return util;
  }

  /**
   * 获取redisUtils实例7JYou9xh%on- 非线程安全
   *
   * @param vClass redis value class对象
   * @param <V> value 类型
   * @return redisUtils实例
   */
  public static <V> RedisUtils<V> getInstance(Class<V> vClass, int timeoutMsecs, int expireMsecs) {
    @SuppressWarnings("unchecked")
    RedisUtils<V> util = (RedisUtils<V>) UTIL_MAP.get(vClass);
    checkNotNull(util, "redis template未初始化");
    return util;
  }

  private static <V> RedisUtils<V> createTemplate(Class<V> vClass) {
    RedisTemplate<String, V> template = creatTemplate(vClass);
    return new RedisUtils<>(template);
  }

  private static <V> RedisUtils<V> createTemplate(
      Class<V> vClass, int timeoutMsecs, int expireMsecs) {
    RedisTemplate<String, V> template = creatTemplate(vClass);
    return new RedisUtils<>(template, timeoutMsecs, expireMsecs);
  }

  private static <V> RedisTemplate<String, V> creatTemplate(Class<V> vClass) {
    RedisTemplate<String, V> template = new RedisTemplate<>();
    // 设置连接工厂
    template.setConnectionFactory(CONNECTION_FACTORY);
    // 设置序列化转换器
    template.setKeySerializer(STRING_REDIS_SERIALIZER);
    template.setHashKeySerializer(STRING_REDIS_SERIALIZER);
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(vClass));
    template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.afterPropertiesSet();
    return template;
  }
}
