package com.xquark.cache;

import java.io.Serializable;

/**
 * Created by wangxinhua on 17-4-15. DESC:
 */
public abstract class InitCache implements Serializable {

  public abstract void refresh();

  public abstract Object load(String hashKey);

  public abstract boolean isCached();

  public abstract void remove();

  public abstract boolean contains(String hashKey);

  protected String generateHashKey(String prefix, String key) {
    StringBuilder builder = new StringBuilder();
    return builder.append(prefix).append(key).toString();
  }

}
