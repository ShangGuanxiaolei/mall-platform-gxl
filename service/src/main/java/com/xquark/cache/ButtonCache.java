package com.xquark.cache;

import com.xquark.dal.vo.FunctionVO;
import com.xquark.dal.vo.RoleFunctionVO;
import com.xquark.function.FunctionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangxinhua on 17-4-15. DESC:
 */
@Component
public class ButtonCache extends InitCache {

  private static Logger logger = LoggerFactory.getLogger(ButtonCache.class);
  public static final String KEY = "urlFunctions";

  @Autowired
  private FunctionService functionService;

  @Autowired
  private RedisTemplate<String, Object> redisTemplate;

  @Override
  public void refresh() {
    // 缓存格式为Map<String, List<RoleFunctionVO>> url => 多个按钮
    List<FunctionVO> moduleFunctionVOS = functionService.loadFunctionList();
    String prefix = "ROLE_";
    Map<String, List<RoleFunctionVO>> buttonCache = new HashMap<String, List<RoleFunctionVO>>();
    if (moduleFunctionVOS != null) {
      for (FunctionVO functionVO : moduleFunctionVOS) {
        String url = functionVO.getModuleUrl();
        String htmlId = functionVO.getHtmlId();
        String name = functionVO.getName();
        String visRoles = functionVO.getVisRoles(prefix);
        String avaRoles = functionVO.getAvaRoles(prefix);

        RoleFunctionVO roleFunctionVO = new RoleFunctionVO();
        roleFunctionVO.setHtmlId(htmlId);
        roleFunctionVO.setName(name);
        roleFunctionVO.setAvaRoles(avaRoles);
        roleFunctionVO.setVisRoles(visRoles);

        List<RoleFunctionVO> voList = buttonCache.get(url);
        // 已存在则添加，不存在则新建
        if (voList != null && voList.size() > 0) {
          voList.add(roleFunctionVO);
        } else {
          voList = new ArrayList<RoleFunctionVO>();
          voList.add(roleFunctionVO);
          buttonCache.put(url, voList);
        }
      }
      logger.info("刷新按钮缓存");
      redisTemplate.opsForHash().putAll(KEY, buttonCache);
    }
  }

  @Override
  public Object load(String hashKey) {
    return redisTemplate.opsForHash().get(KEY, hashKey);
  }

  @Override
  public boolean isCached() {
    return redisTemplate.hasKey(KEY);
  }

  @Override
  public void remove() {
    redisTemplate.delete(KEY);
  }

  @Override
  public boolean contains(String hashKey) {
    return redisTemplate.opsForHash().hasKey(KEY, hashKey);
  }
}
