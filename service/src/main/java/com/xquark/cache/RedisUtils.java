package com.xquark.cache;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by wangxinhua on 17-11-19. DESC: 类型安全的redis工具类
 */
public class RedisUtils<T> {

  private static Logger logger = LoggerFactory.getLogger(RedisUtils.class);

  private static final int DEFAULT_ACQUIRY_RESOLUTION_MILLIS = 100;

  /**
   * 锁超时时间，防止线程在入锁以后，无限的执行等待
   */
  private int expireMsecs = 60 * 1000;

  /**
   * 锁等待时间，防止线程饥饿
   */
  private int timeoutMsecs = 10 * 1000;

  private volatile boolean locked = false;


  private final RedisTemplate<String, T> redisTemplate;

  /**
   * 包级别构造器,不允许cache包外实例化
   *
   * @param template 封装的 {@link RedisTemplate} 实例
   */
  RedisUtils(RedisTemplate<String, T> template) {
    redisTemplate = template;
  }

  /**
   * 包级别构造器,不允许cache包外实例化
   *
   * @param template 封装的 {@link RedisTemplate} 实例
   */
  RedisUtils(RedisTemplate<String, T> template, int timeoutMsecs, int expireMsecs) {
    redisTemplate = template;
    this.timeoutMsecs = timeoutMsecs;
    this.expireMsecs = expireMsecs;
  }

  /**
   * 封装redis的get操作
   *
   * @param key redis键
   * @return T 类型实例
   */
  public T get(String key) {
    return redisTemplate.opsForValue().get(key);
  }

  /**
   * 批量查询
   * @param keys 多个key
   * @return 查询到的结果
   */
  public List<T> multiGet(Collection<String> keys) {
    return redisTemplate.opsForValue().multiGet(keys)
            .stream().filter(Objects::nonNull).collect(Collectors.toList());
  }

  /**
   * 封装redis的set操作
   *
   * @param key 键名
   * @param t 要保存的实例
   */
  public void set(String key, T t) {
    redisTemplate.opsForValue().set(key, t,timeoutMsecs,TimeUnit.DAYS);
  }

  /**
   * 封装setIfAbsent操作
   * @param key 键名
   * @param t 要保存的实例
   */
  public boolean setIfAbsent(String key, T t) {
    return redisTemplate.opsForValue().setIfAbsent(key, t);
  }

  /**
   * 封装redis的过期操作
   *
   * @param key 键名
   * @param time 过期时间
   * @param unit 时间单位
   */
  public void expire(String key, long time, TimeUnit unit) {
    redisTemplate.expire(key, time, unit);
  }

  /**
   * 设置键同时设置过期时间
   *
   * @param key 键名
   * @param t 值
   * @param time 过期时间
   * @param unit 时间单位
   */
  public void setAndExpire(String key, T t, long time, TimeUnit unit) {
    set(key, t);
    expire(key, time, unit);
  }

  /**
   * 封装redis的set操作
   *  @param key 键名
   * @param t 要保存的实例
   * @param time 过期时间
   * @param unit 过期时间单位
   */
  public void set(String key, T t, long time, TimeUnit unit) {
    redisTemplate.opsForValue().set(key, t, time, unit);
  }

  /**
   * 封装redis的delete操作
   *
   * @param key 删除的键名
   */
  public void del(String key) {
    redisTemplate.delete(key);
  }

  /**
   * 封装redis的hasKey操作
   *
   * @param key 键名
   * @return has key or not
   */
  public boolean hasKey(String key) {
    return redisTemplate.hasKey(key);
  }

  /**
   * 封装redis的hGetAll命令
   *
   * @param key hashKey
   * @return key 包含的所有 field -> value 键值对
   */
  public Map<Object, Object> hGetAll(String key) {
    return redisTemplate.boundHashOps(key).entries();
  }

  /**
   * 封装redis的hSet操作
   *
   * @param key mapKey
   * @param field fieldkey
   * @param t value
   */
  public void hSet(String key, String field, T t) {
    redisTemplate.boundHashOps(key).put(field, t);
  }

  /**
   * 封装redis的hGet命令
   *
   * @param key 键名
   * @param field 属性名
   * @return key -> field -> value
   */
  public Object hGet(String key, String field) {
    return redisTemplate.boundHashOps(key).get(field);
  }

  /**
   * 封装 leftPush操作
   *
   * @param key 集合key
   * @param t push值
   * @return push后的索引
   */
  public long lPush(String key, T t) {
    return redisTemplate.boundListOps(key).leftPush(t);
  }

  /**
   * 封装leftPop操作
   *
   * @param key 键
   * @return T 结果
   */
  public T lPop(String key) {
    return redisTemplate.boundListOps(key).leftPop();
  }

  public List<T> lPopWhile(String key, Predicate<? super T> predicate) {
      final List<T> ret = new ArrayList<>();
      T t = peek(key);
      while (t != null && predicate.test(t)) {
        ret.add(lPop(key));
        t = peek(key);
      }
      return ret;
  }

  /**
   * 封装lRange操作
   */
  public List<T> lRange(String key, int start, int end) {
    return redisTemplate.boundListOps(key).range(start, end);
  }

  public T peek(String key) {
    final List<T> ts = lRange(key, 0, 0);
    if (CollectionUtils.isEmpty(ts)) {
      return null;
    }
    return ts.get(0);
  }

  public List<T> lRangeAll(String key) {
    final BoundListOperations<String, T> listOperations = redisTemplate.boundListOps(key);
     return redisTemplate.boundListOps(key).range(0, listOperations.size());
  }

  /**
   * 封装rightPop操作
   *
   * @param key 键
   * @return T 结果
   */
  public T rPop(String key) {
    return redisTemplate.boundListOps(key).rightPop();
  }

  /**
   * 封装LLEN  list队列大小
   * @param key
   * @return
   */
  public Long lSize(String key) {
    return redisTemplate.opsForList().size(key);
  }

  /**
   * 封装sAdd命令
   *
   * @param key 键
   * @param t 要保存的对象实例
   * @return 保存后的索引至
   */
  public long sAdd(String key, T... t) {
    return redisTemplate.boundSetOps(key).add(t);
  }

  /**
   * 封装zAdd命令
   *
   * @param key 键
   * @param score 分数 / 排名
   * @param t 要保存的对象实例
   * @return 保存是否成功
   */
  public boolean zAdd(String key, double score, T t) {
    return redisTemplate.boundZSetOps(key).add(t, score);
  }

  /**
   * 原子增加
   */
  public Long increment(String key, long delta) {
    return redisTemplate.opsForValue().increment(key, delta);
  }

  public Long leftPushAll(String key,T... values){
     return redisTemplate.opsForList().leftPushAll(key, values);
  }

  @SuppressWarnings("unchecked")
  public Long leftPushAllList(String key, Collection<? extends T> values) {
    return redisTemplate.opsForList().leftPushAll(key, (Collection<T>) values);
  }


  /**
   * 原子自增
   */
  public Long incrementOne(String key) {
    return increment(key, 1L);
  }

  /**
   *
   * @param key
   * @return
   */
  private String getX(final String key) {
    Object obj = null;
    try {
      obj = redisTemplate.execute(new RedisCallback<Object>() {
        @Override
        public Object doInRedis(RedisConnection connection) throws DataAccessException {
          StringRedisSerializer serializer = new StringRedisSerializer();
          byte[] data = connection.get(serializer.serialize(key));
          connection.close();
          if (data == null) {
            return null;
          }
          return serializer.deserialize(data);
        }
      });
    } catch (Exception e) {
      logger.error("get redis error, key : {}", key);
    }
    return obj != null ? obj.toString() : null;
  }

  private boolean setNX(final String key, final String value) {
    Object obj = null;
    try {
      obj = redisTemplate.execute(new RedisCallback<Object>() {
        @Override
        public Object doInRedis(RedisConnection connection) throws DataAccessException {
          StringRedisSerializer serializer = new StringRedisSerializer();
          Boolean success = connection.setNX(serializer.serialize(key), serializer.serialize(value));
          connection.close();
          return success;
        }
      });
    } catch (Exception e) {
      logger.error("setNX redis error, key : {}", key);
    }
    return obj != null ? (Boolean) obj : false;
  }
  private String getSet(final String key, final String value) {
    Object obj = null;
    try {
      obj = redisTemplate.execute(new RedisCallback<Object>() {
        @Override
        public Object doInRedis(RedisConnection connection) throws DataAccessException {
          StringRedisSerializer serializer = new StringRedisSerializer();
          byte[] ret = connection.getSet(serializer.serialize(key), serializer.serialize(value));
          connection.close();
          return serializer.deserialize(ret);
        }
      });
    } catch (Exception e) {
      logger.error("setNX redis error, key : {}", key);
    }
    return obj != null ? (String) obj : null;
  }

  /**
   * 获得 lock.
   * 实现思路: 主要是使用了redis 的setnx命令,缓存了锁.
   * reids缓存的key是锁的key,所有的共享, value是锁的到期时间(注意:这里把过期时间放在value了,没有时间上设置其超时时间)
   * 执行过程:
   * 1.通过setnx尝试设置某个key的值,成功(当前没有这个锁)则返回,成功获得锁
   * 2.锁已经存在则获取锁的到期时间,和当前时间比较,超时的话,则设置新的值
   *
   * @return true if lock is acquired, false acquire timeouted
   * @throws InterruptedException in case of thread interruption
   */
  public synchronized boolean lock(String lockKey) throws InterruptedException {
    int timeout = timeoutMsecs;
    while (timeout >= 0) {
      long expires = System.currentTimeMillis() + expireMsecs + 1;
      String expiresStr = String.valueOf(expires); //锁到期时间
      if (this.setNX(lockKey, expiresStr)) {
        // lock acquired
        locked = true;
        return true;
      }

      String currentValueStr = this.getX(lockKey); //redis里的时间
      if (currentValueStr != null && Long.parseLong(currentValueStr) < System.currentTimeMillis()) {
        //判断是否为空，不为空的情况下，如果被其他线程设置了值，则第二个条件判断是过不去的
        // lock is expired

        String oldValueStr = this.getSet(lockKey, expiresStr);
        //获取上一个锁到期时间，并设置现在的锁到期时间，
        //只有一个线程才能获取上一个线上的设置时间，因为jedis.getSet是同步的
        if (oldValueStr != null && oldValueStr.equals(currentValueStr)) {
          //防止误删（覆盖，因为key是相同的）了他人的锁——这里达不到效果，这里值会被覆盖，但是因为什么相差了很少的时间，所以可以接受

          //[分布式的情况下]:如过这个时候，多个线程恰好都到了这里，但是只有一个线程的设置值和当前值相同，他才有权利获取锁
          // lock acquired
          locked = true;
          return true;
        }
      }
      timeout -= DEFAULT_ACQUIRY_RESOLUTION_MILLIS;

            /*
                延迟100 毫秒,  这里使用随机时间可能会好一点,可以防止饥饿进程的出现,即,当同时到达多个进程,
                只会有一个进程获得锁,其他的都用同样的频率进行尝试,后面有来了一些进行,也以同样的频率申请锁,这将可能导致前面来的锁得不到满足.
                使用随机的等待时间可以一定程度上保证公平性
             */
      Thread.sleep(DEFAULT_ACQUIRY_RESOLUTION_MILLIS);
    }
    return false;
  }


  /**
   * Acqurired lock release.
   */
  public synchronized void unlock(String lockKey) {
    if (locked) {
      redisTemplate.delete(lockKey);
      locked = false;
    }
  }

}