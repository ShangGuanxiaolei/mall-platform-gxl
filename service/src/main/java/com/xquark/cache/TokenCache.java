package com.xquark.cache;

import com.xquark.dal.vo.FunctionVO;
import com.xquark.dal.vo.RoleFunctionVO;
import com.xquark.function.FunctionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chh on 17-5-4. DESC:供服务端储存token使用，由于token跨越了web和v2,没法使用session共享，只能使用redis存储
 */
@Component
public class TokenCache {

  private static Logger logger = LoggerFactory.getLogger(TokenCache.class);

  @Autowired
  private RedisTemplate redisTemplate;

  public void put(String token) {
    redisTemplate.opsForValue().set(token, token);
  }

  public void remove(String token) {
    redisTemplate.delete(token);
  }

  public Object load(String token) {
    return redisTemplate.opsForValue().get(token);
  }

}
