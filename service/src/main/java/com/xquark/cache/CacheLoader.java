package com.xquark.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by wangxinhua on 17-4-15. DESC: 缓存类
 */
public class CacheLoader {

  @Autowired
  private ButtonCache buttonCache;

  @Autowired
  private FounderAreaCache founderAreaCache;

  /**
   * 初始化缓存
   */
//  @PostConstruct
  public void init() {
//        moduleCache.refresh();
    buttonCache.refresh();
//        roleCache.refresh();
    founderAreaCache.refresh();
  }
}
