package com.xquark.cache;

import com.xquark.service.module.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: wangxh 创建日期: 17-4-18 简介:
 */
@Component
public class UrlRoleCache extends InitCache {

  private static Logger logger = LoggerFactory.getLogger(UrlRoleCache.class);
  public static String KEY = "urlRoles";

  @Autowired
  private ModuleService moduleService;

  @Autowired
  private RedisTemplate<String, Object> redisTemplate;

  @Override
  public void refresh() {
    Map<String, List<String>> urlRoleMap = moduleService.loadUrlRoleMap();
    if (urlRoleMap == null) {
      urlRoleMap = new HashMap<String, List<String>>();
    }
    logger.info("刷新URL-菜单缓存--------------");
    redisTemplate.opsForHash().putAll(KEY, urlRoleMap);
  }

  @Override
  public Object load(String hashKey) {
    return redisTemplate.opsForHash().get(KEY, hashKey);
  }

  @Override
  public boolean isCached() {
    return redisTemplate.hasKey(KEY) && redisTemplate.type(KEY) == DataType.HASH;
  }

  @Override
  public void remove() {
    redisTemplate.delete(KEY);
  }

  @Override
  public boolean contains(String hashKey) {
    return redisTemplate.opsForHash().hasKey(KEY, hashKey);
  }
}
