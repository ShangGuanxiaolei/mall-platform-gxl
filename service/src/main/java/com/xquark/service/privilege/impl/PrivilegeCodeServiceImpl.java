package com.xquark.service.privilege.impl;

import com.xquark.dal.mapper.*;
import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.model.PrivilegeCodeOrder;
import com.xquark.dal.model.PrivilegeCodeUser;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.status.AuditType;
import com.xquark.dal.vo.PrivilegeCodeVO;
import com.xquark.service.auditRule.AuditRuleService;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.privilege.PrivilegeCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 特权码相关Service
 *
 * @author chh 2018-01-16
 */
@Service("privilegeCodeService")
public class PrivilegeCodeServiceImpl extends BaseServiceImpl implements PrivilegeCodeService {

  @Autowired
  private PrivilegeCodeMapper privilegeCodeMapper;

  @Autowired
  private PrivilegeCodeUserMapper privilegeCodeUserMapper;

  @Autowired
  private PrivilegeCodeOrderMapper privilegeCodeOrderMapper;

  @Override
  public PrivilegeCode selectByPrimaryKey(String id) {
    return privilegeCodeMapper.selectByPrimaryKey(id);
  }

  @Override
  public int insert(PrivilegeCode role) {
    return privilegeCodeMapper.insert(role);
  }

  @Override
  public int modify(PrivilegeCode role) {
    return privilegeCodeMapper.modify(role);
  }

  @Override
  public int delete(String id) {
    return privilegeCodeMapper.delete(id);
  }

  /**
   * 列表查询页面
   */
  @Override
  public List<PrivilegeCode> list(Pageable page, Map<String, Object> params) {
    return privilegeCodeMapper.list(page, params);
  }

  /**
   * 对分页页面列表接口取总数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return privilegeCodeMapper.selectCnt(params);
  }


  /**
   * 查找某个用户所有的特权码
   */
  @Override
  public List<PrivilegeCodeVO> selectByUserId(String userId) {
    return privilegeCodeMapper.selectByUserId(userId);
  }

  /**
   * 根据编码查询特权码
   */
  @Override
  public PrivilegeCode selectByCode(String code) {
    return privilegeCodeMapper.selectByCode(code);
  }

  /**
   * 查询某个特权码是否已经被用户领用
   */
  @Override
  public boolean codeExist(String code) {
    PrivilegeCodeUser codeUser = privilegeCodeUserMapper.selectByCode(code);
    return codeUser != null;
  }

  /**
   * 兑换特权码
   */
  @Override
  public boolean acquireCode(String code, String userId) {
    PrivilegeCodeUser codeUser = new PrivilegeCodeUser();
    PrivilegeCode codeV = privilegeCodeMapper.selectByCode(code);
    codeUser.setUserId(userId);
    codeUser.setArchive(false);
    codeUser.setCodeId(codeV.getId());
    int result = privilegeCodeUserMapper.insert(codeUser);
    return result > 0;
  }

  /**
   * 返回一个用户可使用的特权码
   */
  @Override
  public PrivilegeCodeVO selectValidByUserId(String userId) {
    return privilegeCodeMapper.selectValidByUserId(userId);
  }

  /**
   * 记录特权码与订单的关系
   */
  @Override
  public void addCodeOrder(String codeId, String orderId) {
    PrivilegeCodeOrder codeOrder = new PrivilegeCodeOrder();
    codeOrder.setCodeId(codeId);
    codeOrder.setOrderId(orderId);
    privilegeCodeOrderMapper.insert(codeOrder);
  }

  /**
   * 删除特权码与订单的关系
   */
  @Override
  public void delCodeOrder(String orderId) {
    privilegeCodeOrderMapper.deleteByOrderId(orderId);
  }

  /**
   * 将特权码的可用数量减1
   */
  @Override
  public void subQty(String codeId) {
    privilegeCodeMapper.subQty(codeId);
  }

  /**
   * 将特权码的可用数量加1
   */
  @Override
  public void addQty(String codeId) {
    privilegeCodeMapper.addQty(codeId);
  }

  /**
   * 查找某个订单对应的特权码
   */
  @Override
  public PrivilegeCodeOrder selectCodeOrder(String orderId) {
    return privilegeCodeOrderMapper.selectByOrderId(orderId);
  }

}
