package com.xquark.service.privilege;

import com.xquark.dal.model.AuditRule;
import com.xquark.dal.model.PrivilegeCode;
import com.xquark.dal.model.PrivilegeCodeOrder;
import com.xquark.dal.status.AgentType;
import com.xquark.dal.status.AuditRuleType;
import com.xquark.dal.vo.PrivilegeCodeVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


/**
 * 特权码相关Service
 *
 * @author chh 2018-01-16
 */
public interface PrivilegeCodeService {


  PrivilegeCode selectByPrimaryKey(String id);

  int insert(PrivilegeCode role);

  int modify(PrivilegeCode role);

  int delete(String id);

  /**
   * 列表查询页面
   */
  List<PrivilegeCode> list(Pageable page, Map<String, Object> params);

  /**
   * 对分页页面列表接口取总数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查找某个用户所有的特权码
   */
  List<PrivilegeCodeVO> selectByUserId(String userId);

  /**
   * 根据编码查询特权码
   */
  PrivilegeCode selectByCode(String code);

  /**
   * 查询某个特权码是否已经被用户领用
   */
  boolean codeExist(String code);

  /**
   * 兑换特权码
   */
  boolean acquireCode(String code, String userId);

  /**
   * 返回一个用户可使用的特权码
   */
  PrivilegeCodeVO selectValidByUserId(String userId);

  /**
   * 记录特权码与订单的关系
   */
  void addCodeOrder(String codeId, String orderId);

  /**
   * 删除特权码与订单的关系
   */
  void delCodeOrder(String orderId);

  /**
   * 将特权码的可用数量减1
   */
  void subQty(String codeId);

  /**
   * 将特权码的可用数量加1
   */
  void addQty(String codeId);

  /**
   * 查找某个订单对应的特权码
   */
  PrivilegeCodeOrder selectCodeOrder(String orderId);
}
