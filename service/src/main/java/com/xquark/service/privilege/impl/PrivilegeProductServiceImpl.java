package com.xquark.service.privilege.impl;

import com.xquark.dal.mapper.PrivilegeProductMapper;
import com.xquark.dal.mapper.ProductTopMapper;
import com.xquark.dal.model.PrivilegeProduct;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.vo.PrivilegeProductVO;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.privilege.PrivilegeProductService;
import com.xquark.service.product.ProductTopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("PrivilegeProductService")
public class PrivilegeProductServiceImpl extends BaseServiceImpl implements
    PrivilegeProductService {

  @Autowired
  PrivilegeProductMapper privilegeProductMapper;

  @Override
  public int insert(PrivilegeProduct productTop) {
    return privilegeProductMapper.insert(productTop);
  }

  @Override
  public int insertOrder(PrivilegeProduct privilegeProduct) {
    return privilegeProductMapper.insert(privilegeProduct);
  }

  @Override
  public PrivilegeProductVO load(String id) {
    return privilegeProductMapper.selectByPrimaryKey(id);
  }


  @Override
  public int deleteForArchive(String id) {
    return privilegeProductMapper.updateForArchive(id);
  }

  @Override
  public int update(PrivilegeProduct record) {
    return privilegeProductMapper.updateByPrimaryKeySelective(record);
  }

  /**
   * 服务端分页查询数据
   */
  @Override
  public List<PrivilegeProductVO> list(Pageable pager, Map<String, Object> params) {
    return privilegeProductMapper.list(pager, params);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(Map<String, Object> params) {
    return privilegeProductMapper.selectCnt(params);
  }

  /**
   * 查询是否已经有该商品的分佣设置
   */
  @Override
  public Long selectByProductId(String shopId, String productId, String id) {
    return privilegeProductMapper.selectByProductId(shopId, productId, id);
  }

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  @Override
  public List<PrivilegeProductVO> getHomeForApp(Pageable pager, String shopId) {
    return privilegeProductMapper.getHomeForApp(pager, shopId);
  }

  @Override
  public long getCount() {
    return privilegeProductMapper.getCount();
  }

}
