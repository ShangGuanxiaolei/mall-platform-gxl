package com.xquark.service.privilege;

import com.xquark.dal.model.PrivilegeProduct;
import com.xquark.dal.model.ProductTop;
import com.xquark.dal.vo.PrivilegeProductVO;
import com.xquark.dal.vo.ProductTopVO;
import com.xquark.service.BaseEntityService;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


public interface PrivilegeProductService extends BaseEntityService<PrivilegeProduct> {

  PrivilegeProductVO load(String id);

  int insert(PrivilegeProduct record);

  int deleteForArchive(String id);

  int update(PrivilegeProduct record);

  /**
   * 服务端分页查询数据
   */
  List<PrivilegeProductVO> list(Pageable pager, Map<String, Object> params);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(Map<String, Object> params);

  /**
   * 查询是否已经有该商品的分佣设置
   */
  Long selectByProductId(String shopId, String productId, String id);

  /**
   * 供客户端调用，获取首页爆款推荐商品
   */
  List<PrivilegeProductVO> getHomeForApp(Pageable pager, String shopId);

  long getCount();

}

