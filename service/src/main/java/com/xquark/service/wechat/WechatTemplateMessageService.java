package com.xquark.service.wechat;

import com.xquark.dal.model.wechat.TemplateMessage;
import com.xquark.dal.model.wechat.TemplateMessageType;

import java.util.List;

/**
 * 商家微信公众号模板消息通知设置
 */
public interface WechatTemplateMessageService {

  List<TemplateMessage> list(String shopId);

  int saveTemplateId(TemplateMessageType[] templateMessageNames, String[] templateIds);

  TemplateMessage loadByTmpTypeAndShopId(TemplateMessageType templateMessageType, String shopId);

}
