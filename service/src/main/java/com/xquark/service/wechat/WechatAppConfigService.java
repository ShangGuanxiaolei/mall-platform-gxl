package com.xquark.service.wechat;

import com.xquark.dal.model.wechat.WechatAppConfig;

import java.util.List;
import java.util.Map;

/**
 * Created by dongsongjie on 16/6/9.
 */
public interface WechatAppConfigService {

  int save(WechatAppConfig appConfig);

  WechatAppConfig load(String shopId);

  WechatAppConfig load();

  List<WechatAppConfig> selectByUrlBegin(String url);

  // 没有微信配置记录时新增证书
  int insertCert(Map map);

  // 有微信配置记录时更新证书
  int updateCert(Map map);

  // 没有微信App配置记录时新增证书
  int insertAppCert(Map map);

  // 有微信App配置记录时更新证书
  int updateAppCert(Map map);

}
