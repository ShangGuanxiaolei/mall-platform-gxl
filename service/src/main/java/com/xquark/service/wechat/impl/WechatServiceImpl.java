package com.xquark.service.wechat.impl;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.mapper.WechatCustomizedMenuMapper;
import com.xquark.dal.model.wechat.*;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.shop.ShopService;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.WechatAutoReplyService;
import com.xquark.service.wechat.WechatService;
import com.xquark.service.wechat.WechatTemplateMessageService;
import com.xquark.service.wechat.WechatUtils;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.event.EventType;
import com.xquark.wechat.exception.WeChatException;
import com.xquark.wechat.menu.Menu;
import com.xquark.wechat.menu.MenuButton;
import com.xquark.wechat.menu.MenuManager;
import com.xquark.wechat.message.Article;
import com.xquark.wechat.message.TemplateMsg;
import com.xquark.wechat.message.template.TemplateMsgBody;
import com.xquark.wechat.message.template.TemplateMsgData;
import com.xquark.wechat.oauth.OAuthManager;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenRequest;
import com.xquark.wechat.oauth.protocol.get_access_token.GetAccessTokenResponse;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoRequest;
import com.xquark.wechat.token.AccessToken;
import com.xquark.wechat.token.Ticket;
import com.xquark.wechat.token.TicketType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 服务实现
 */
@Service("wechatService")
public class WechatServiceImpl extends BaseServiceImpl implements WechatService {

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Autowired
  private WechatAppConfigServiceImpl wechatAppConfigService;

  @Autowired
  private WechatAutoReplyService wechatAutoReplyService;

  @Autowired
  private WechatCustomizedMenuMapper wechatCustomizedMenuMapper;

  @Autowired
  private WechatTemplateMessageService wechatTemplateMessageService;

  @Autowired
  private UserService userService;

  @Autowired
  private ShopService shopService;

  @Autowired
  private PasswordEncoder pwdEncoder;

  @Value("${profiles.active}")
  private String profile;

  @Value("${site.web.host.name}")
  private String hostName;

  @Override
  public String onEventDispatch(HttpServletRequest request) {
    XquarkWechatSupport xquarkWechatSupport = new XquarkWechatSupport(wechatAppConfigMapper,
        wechatAutoReplyService, this, userService, wechatCustomizedMenuMapper);
    return xquarkWechatSupport.onEventDispatch(request);
  }

  @Override
  public boolean sendTemplateMessage(TemplateMessageType templateMessageType, String shopId,
      String url, List<TemplateMsgData> data, IUser toUser) {
    Preconditions.checkNotNull(templateMessageType);
    Preconditions.checkNotNull(shopId);
    Preconditions.checkNotNull(toUser);

    boolean ret = false;
    TemplateMessage templateMessage = wechatTemplateMessageService
        .loadByTmpTypeAndShopId(templateMessageType, shopId);
    if (templateMessage == null) {
      log.error("模板消息未找到," + "消息类型:" + templateMessageType + ",店铺id:" + shopId);
      return false;
    }

    WechatAppConfig appConfig = wechatAppConfigMapper.selectByShopId(shopId);
    TemplateMsgBody postData = new TemplateMsgBody();
    postData.setTouser(toUser.getUsername());
    postData.setTemplateId(templateMessage.getTemplateId());
    postData.setUrl(StringUtils.trimToEmpty(url));
    postData.setData(new ArrayList<TemplateMsgData>(2));

    String accessToken = obtainAccessToken(appConfig);

    TemplateMsg templateMsg = new TemplateMsg(accessToken);
    ret = templateMsg.send(postData) != null ? true : false;
    return ret;
  }

  @Override
  public WechatAppConfig load(IUser user) {
    return null;
  }

  @Override
  @Transactional
  public boolean updateMenus(WechatAppConfig wechatAppConfig, CustomizedMenu customizedMenu) {
    boolean ret = true;
    try {
      Menu menu = convertToMenu(customizedMenu, wechatAppConfig);
      String accessToken = obtainAccessToken(wechatAppConfig);
      MenuManager menuManager = new MenuManager(accessToken);
      menuManager.create(menu);
      wechatCustomizedMenuMapper
          .updateRenewable(customizedMenu.getItems().get(0).getShopId(), false);
    } catch (WeChatException we) {
      log.error("更新菜单项失败:", we);
      ret = false;
    } catch (Exception e) {
      log.error("更新菜单项失败", e);
    }

    return ret;
  }

  @Override
  @Transactional
  public String obtainAccessToken(WechatAppConfig wechatAppConfig) {
    Date now = new Date();
    Date accessTokenExpiredAt = wechatAppConfig.getAccessTokenExpiredAt();
    if (accessTokenExpiredAt != null && accessTokenExpiredAt.after(now)) {
      log.info("token无需更新");
      return wechatAppConfig.getAccessToken();
    } else if (accessTokenExpiredAt == null) { // 尚未执行task获取token
      log.info("token未初始化");
      return renewAccessToken(wechatAppConfig);
    } else if (accessTokenExpiredAt.before(now)) { // token过期
      log.info("token过期");
      return renewAccessToken(wechatAppConfig);
    }
    return null;
  }

  @Override
  @Transactional
  public String renewAccessToken(WechatAppConfig wechatAppConfig) {
    AccessToken accessToken = new AccessToken();
    Config config = WechatUtils.convertWechatAppConfig(wechatAppConfig);
    accessToken.request(config);

    wechatAppConfig.setAccessToken(accessToken.getToken());
    wechatAppConfig.setAccessTokenExpiredAt(
        new Date(System.currentTimeMillis() + accessToken.getExpiresIn() * 1000)); //getExpiresIn 秒
    wechatAppConfig.setAccessTokenRenewCounter(wechatAppConfig.getAccessTokenRenewCounter() + 1);

    if (wechatAppConfigMapper.updateByPrimaryKeySelective(wechatAppConfig) == 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新token失败");
    }
    return wechatAppConfig.getAccessToken();
  }

  @Override
  @Transactional
  public String obtainJsapiTicket(WechatAppConfig wechatAppConfig) {
    Date now = new Date();
    Date jsapiTicketExpiredAt = wechatAppConfig.getJsapiTicketExpiredAt();
    if (jsapiTicketExpiredAt != null && jsapiTicketExpiredAt.after(now)) {
      log.info("jsapi ticket无需更新");
      return wechatAppConfig.getJsapiTicket();
    } else if (jsapiTicketExpiredAt == null) { // 尚未执行task获取token
      log.info("jsapi ticket未初始化");
      return renewJsapiTicket(wechatAppConfig);
    } else if (jsapiTicketExpiredAt.before(now)) { // token过期
      log.info("jsapi ticket过期");
      return renewJsapiTicket(wechatAppConfig);
    }
    return null;
  }

  /**
   * 获取jsapiticket
   */
  @Override
  public String obtainJsapiTicket() {
    WechatAppConfig wechatAppConfig = wechatAppConfigMapper
        .selectByShopId(shopService.loadRootShop().getId());
    return obtainJsapiTicket(wechatAppConfig);
  }

  @Override
  @Transactional
  public String renewJsapiTicket(WechatAppConfig wechatAppConfig) {
    Ticket ticket = new Ticket(TicketType.jsapi);
    Config config = WechatUtils.convertWechatAppConfig(wechatAppConfig);
    ticket.request(config);
    wechatAppConfig.setJsapiTicket(ticket.getToken());
    wechatAppConfig.setJsapiTicketExpiredAt(
        new Date(System.currentTimeMillis() + ticket.getExpiresIn() * 1000)); // expiresIn 秒
    wechatAppConfig.setJsapiTicketRenewCounter(wechatAppConfig.getJsapiTicketRenewCounter() + 1);
    if (wechatAppConfigMapper.updateByPrimaryKeySelective(wechatAppConfig) == 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新jsapi token失败");
    }
    return wechatAppConfig.getJsapiTicket();
  }

  private Menu convertToMenu(CustomizedMenu customizedMenu, WechatAppConfig wechatAppConfig) {
    Menu ret = new Menu();
    List<MenuButton> menuButtons = new ArrayList<MenuButton>(3);
    CustomizedMenuItem[][] items = customizedMenu.getMapping();

    for (int i = 0; i < items.length; i++) {
      CustomizedMenuItem[] buttons = items[i];
      if (buttons != null && buttons.length > 0) {
        for (int j = 0; j < buttons.length; j++) {
          CustomizedMenuItem item = buttons[j];
          MenuButton menuButton = new MenuButton();
          if (item != null) {
            menuButton.setName(item.getName());
            if (j == 0) {
              menuButtons.add(menuButton);
              if (item.getType().equals(ButtonType.HAS_SUB_BUTTON)) {
                menuButton.setSubButton(new ArrayList<MenuButton>(5));//初始化子菜单列表
              } else if (item.getType().equals(ButtonType.CLICK_RETURN_TEXT)) {
                menuButton.setKey(item.getKey());
                menuButton.setType(EventType.click);
              } else if (item.getType().equals(ButtonType.CLICK_RETURN_MEDIA_ID)) {
                menuButton.setType(EventType.view_limited);
                menuButton.setMediaId(item.getMediaId());
              } else if (item.getType().equals(ButtonType.CLICK_REDIRECT_VIEW)) {
                menuButton.setType(EventType.view);
                menuButton
                    .setUrl(generateOauthUrl(wechatAppConfig, item, OAuthManager.SCOPE_USERINFO));
              }
              log.info("add root level button:" + JSON.toJSONString(menuButton));
            } else {
              if (item.getType().equals(ButtonType.CLICK_RETURN_TEXT)) {
                menuButton.setType(EventType.click);
                menuButton.setKey(item.getKey());
              } else if (item.getType().equals(ButtonType.CLICK_RETURN_MEDIA_ID)) {
                menuButton.setType(EventType.view_limited);
                menuButton.setMediaId(item.getMediaId());
              } else if (item.getType().equals(ButtonType.CLICK_REDIRECT_VIEW)) {
                menuButton.setType(EventType.view);
                menuButton
                    .setUrl(generateOauthUrl(wechatAppConfig, item, OAuthManager.SCOPE_USERINFO));
              }
              log.info("add sub level button:" + JSON.toJSONString(menuButton));
              menuButtons.get(i).getSubButton().add(menuButton);
            }
          }
        }
      }
    }
    log.info("buttons prepare to add:" + JSON.toJSONString(menuButtons));
    ret.setButton(menuButtons);
    return ret;
  }

  /**
   * <b>state 参数格式变化请同步更新getShopIdFromState方法</b>
   */
  private String generateOauthUrl(WechatAppConfig wechatAppConfig, CustomizedMenuItem item,
      String scope) {
    if (!item.getUrl().contains(hostName)) { //不对非系统域名的url生成oauth链接
      return item.getUrl();
    }

    return OAuthManager.generateRedirectURI(WechatUtils.convertWechatAppConfig(wechatAppConfig),
        item.getUrl(), scope,
        OAuthManager.STATE_FLAG + wechatAppConfig.getId() + OAuthManager.STATE_FLAG
            + wechatAppConfig.getShopId());
  }

  @Override
  public String getShopIdFromState(String state) {
    String[] params = state.split(OAuthManager.STATE_FLAG);
    if (params.length == 3) {
      return params[2];
    }
    return null;
  }

  @Override
  public boolean autoReply(ConditionType conditionType, String input) {
    return false;
  }

  @Override
  public Article findMeterial(String mediaId) {
    return null;
  }

  /**
   * 通过code取微信用户信息
   */
  @Override
  public WechatUserBean getWechatUserInfoByCode(String code) {
    WechatAppConfig appConfig = wechatAppConfigService.load();
    Config config = new Config("", "", "", appConfig.getAppAppId(),
        appConfig.getAppAppSecret(), "", "", "", "");
    GetAccessTokenRequest getAccessTokenRequest = new GetAccessTokenRequest(config,
        code);
    try {
      GetAccessTokenResponse res = OAuthManager.getAccessToken(getAccessTokenRequest);
      GetUserinfoRequest userInfo = new GetUserinfoRequest(config, res.getAccess_token(),
          res.getOpenid());
      OAuthManager.getUserinfo(userInfo);
      return OAuthManager.getUserinfo(userInfo);
    } catch (Exception e) {
      throw new BizException(GlobalErrorCode.WECHAT_OAUTH_CODE_NOT_VAILD, "微信登录失败: ", e);
    }
  }





  public static void main(String[] args) {
    String state = "123wx" + OAuthManager.STATE_FLAG + "fkdsajg";
    String[] params = state.split(OAuthManager.STATE_FLAG);
    String[] params1 = StringUtils.split(state, OAuthManager.STATE_FLAG);
    System.out.println(params[0] + "," + params[1]);
    System.out.println(params1[0] + "," + params1[1]);
  }

}
