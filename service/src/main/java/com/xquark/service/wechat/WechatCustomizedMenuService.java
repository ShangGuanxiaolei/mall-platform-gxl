package com.xquark.service.wechat;

import com.xquark.dal.model.wechat.ButtonType;
import com.xquark.dal.model.wechat.CustomizedMenu;

/**
 * 商家微信公众号自定义菜单服务
 */
public interface WechatCustomizedMenuService {

  int modifyButtonInfo(String id, String name, ButtonType buttonType, String contentOrMediaId,
      String url);

  int modifyMenusOrder(String[] ids, Integer[] newIndexes);

  String addButton(String parentId, Integer index, String name, ButtonType buttonType,
      String contentOrMediaId, String url);

  CustomizedMenu menuList(String shopId);
}
