package com.xquark.service.wechat.impl;

import com.xquark.dal.mapper.WechatAutoReplyMapper;
import com.xquark.dal.model.wechat.AutoReply;
import com.xquark.dal.model.wechat.ConditionType;
import com.xquark.dal.model.wechat.MatchType;
import com.xquark.dal.model.wechat.ReplyType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wechat.WechatAutoReplyService;
import com.xquark.wechat.event.EventType;
import com.xquark.wechat.message.Article;
import com.xquark.wechat.response.ArticleResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("WechatAutoReplyService")
public class WechatAutoReplyServiceImpl extends BaseServiceImpl implements WechatAutoReplyService {

  @Autowired
  private WechatAutoReplyMapper wechatAutoReplyMapper;

  @Override
  public AutoReply find(String input, String shopId) {
    AutoReply ret = null;

    List<AutoReply> autoReplies = wechatAutoReplyMapper.list(shopId);
    if (autoReplies != null && autoReplies.size() > 0) {
      for (AutoReply autoReply : autoReplies) {
        if (autoReply.getConditionType().equals(ConditionType.REQUEST_TEXT)) {
          if (autoReply.getMatchType().equals(MatchType.AUTO) || (
              autoReply.getMatchType().equals(MatchType.LITERAL)
                  && StringUtils.equals(autoReply.getCondition(), input)) ||
              ((autoReply.getMatchType().equals(MatchType.LIKE)
                  && StringUtils.contains(input, autoReply.getCondition())))) {
            ret = autoReply;
            break;
          }
        }
      }
    }
    // 如果没有匹配到关键词回复，则判断是否有消息自动回复，有则回复自动回复内容
    if (ret == null) {
      ret = wechatAutoReplyMapper.getAutoBack();
    }

    return ret;
  }

  @Override
  public AutoReply find(EventType input, String shopId) {
    return null;
  }

  @Override
  public int update(AutoReply autoReply) {
    return wechatAutoReplyMapper.updateByPrimaryKeySelective(autoReply);
  }

  @Override
  public int delete(String id) {
    return wechatAutoReplyMapper.updateForArchive(id);
  }

  @Override
  public int undelete(String id) {
    return wechatAutoReplyMapper.updateForUnArchive(id);
  }

  @Override
  public int insert(AutoReply autoReply) {
    return wechatAutoReplyMapper.insert(autoReply);
  }

  @Override
  public int insertOrder(AutoReply autoReply) {
    return wechatAutoReplyMapper.insert(autoReply);
  }

  @Override
  public AutoReply load(String id) {
    return wechatAutoReplyMapper.selectByPrimaryKey(id);
  }


  /**
   * 服务端分页查询数据
   */
  @Override
  public List<AutoReply> list(String shopId) {
    return wechatAutoReplyMapper.list(shopId);
  }

  /**
   * 分页查询数据总条数
   */
  @Override
  public Long selectCnt(String shopId) {
    return wechatAutoReplyMapper.selectCnt(shopId);
  }


  /**
   * 获取消息自动回复
   */
  @Override
  public AutoReply getAutoBack() {
    return wechatAutoReplyMapper.getAutoBack();
  }

  /**
   * 获取被添加自动回复
   */
  @Override
  public AutoReply getAddBack() {
    return wechatAutoReplyMapper.getAddBack();
  }

}
