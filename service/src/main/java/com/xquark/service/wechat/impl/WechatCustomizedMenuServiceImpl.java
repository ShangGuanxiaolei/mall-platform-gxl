package com.xquark.service.wechat.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.WechatCustomizedMenuMapper;
import com.xquark.dal.model.wechat.ButtonType;
import com.xquark.dal.model.wechat.CustomizedMenu;
import com.xquark.dal.model.wechat.CustomizedMenuItem;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.error.BizException;
import com.xquark.service.error.GlobalErrorCode;
import com.xquark.service.wechat.WechatCustomizedMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 服务实现
 */
@Service("WechatCustomizedMenuService")
public class WechatCustomizedMenuServiceImpl extends BaseServiceImpl implements
    WechatCustomizedMenuService {

  @Autowired
  private WechatCustomizedMenuMapper wechatCustomizedMenuMapper;

  @Override
  @Transactional
  public int modifyButtonInfo(String id, String name, ButtonType buttonType,
      String contentOrMediaId, String url) {
    Preconditions.checkArgument(name != null, "name can not be null.");
    Preconditions.checkArgument(buttonType != null, "buttonType can not be null.");

    CustomizedMenuItem customizedMenuItem = wechatCustomizedMenuMapper.selectByPrimaryKey(id);
    if (customizedMenuItem == null) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "找不到对应的菜单项");
    }
    if (!StringUtils.equals(customizedMenuItem.getShopId(), getCurrentUser().getShopId())) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "无权修改该菜单项");
    }

    customizedMenuItem.setName(name);
    setTypeBasedContent(buttonType, contentOrMediaId, url, customizedMenuItem);

    return wechatCustomizedMenuMapper.updateByPrimaryKeySelective(customizedMenuItem);
  }

  @Override
  @Transactional
  public int modifyMenusOrder(String[] ids, Integer[] newIndexes) {
    Preconditions.checkNotNull(ids, "ids can not be null");
    Preconditions.checkNotNull(newIndexes, "new indexes can not be null");

    int ret = 0;
    for (int i = 0; i < ids.length; i++) {
      CustomizedMenuItem customizedMenuItem = wechatCustomizedMenuMapper.selectByPrimaryKey(ids[i]);
      if (customizedMenuItem == null) {
        throw new BizException(GlobalErrorCode.NOT_FOUND, "找不到对应的菜单项");
      }
      if (!StringUtils.equals(customizedMenuItem.getShopId(), getCurrentUser().getShopId())) {
        throw new BizException(GlobalErrorCode.UNAUTHORIZED, "无权修改该菜单项");
      }
      customizedMenuItem.setIndex(newIndexes[i]);
      generateItemKey(customizedMenuItem.getParentId(), newIndexes[i], getCurrentUser(),
          customizedMenuItem);
      wechatCustomizedMenuMapper.updateByPrimaryKeySelective(customizedMenuItem);
      ret++;
    }

    return ret;
  }

  @Override
  @Transactional
  public String addButton(String parentId, Integer index, String name, ButtonType buttonType,
      String contentOrMediaId, String url) {
    Preconditions.checkNotNull(index, "button index can not be null.");
    Preconditions.checkNotNull(name, "name can not be null.");
    Preconditions.checkNotNull(buttonType, "buttonType can not be null.");

    IUser user = getCurrentUser();
    CustomizedMenuItem customizedMenuItem = new CustomizedMenuItem();
    customizedMenuItem.setParentId(StringUtils.isNotEmpty(parentId) ? parentId : null);
    customizedMenuItem.setIndex(index);
    customizedMenuItem.setName(name);
    customizedMenuItem.setType(buttonType);
    customizedMenuItem.setShopId(user.getShopId());

    setTypeBasedContent(buttonType, contentOrMediaId, url, customizedMenuItem);
    generateItemKey(parentId, index, user, customizedMenuItem);

    wechatCustomizedMenuMapper.insert(customizedMenuItem);
    return customizedMenuItem.getId();
  }

  private void generateItemKey(String parentId, Integer index, IUser user,
      CustomizedMenuItem customizedMenuItem) {
    // 生成菜单项的唯一key值, 作为我们与对接的按钮唯一标示符
    Integer level = 0;
    Integer parentIndex = 0;
    if (StringUtils.isNotEmpty(parentId)) {
      level = 1;
      parentIndex = wechatCustomizedMenuMapper.selectByPrimaryKey(parentId).getIndex();
    }
    customizedMenuItem.setKey(user.getShopId() + level + parentIndex + index);
  }

  @Override
  public CustomizedMenu menuList(String shopId) {
    CustomizedMenu ret = new CustomizedMenu();
    List<CustomizedMenuItem> items = wechatCustomizedMenuMapper.list(shopId, null);
    ret.setItems(items);
    return ret;
  }


  /**
   * 根据按钮类型保存按钮内容
   */
  private void setTypeBasedContent(ButtonType buttonType, String contentOrMediaId, String url,
      CustomizedMenuItem customizedMenuItem) {
    //根据事件类型保存内容
    customizedMenuItem.setType(buttonType);
    if (buttonType.equals(ButtonType.CLICK_RETURN_TEXT)) { // 点击事件,推送纯文本
      customizedMenuItem.setContent(contentOrMediaId);
    } else if (buttonType.equals(ButtonType.CLICK_RETURN_MEDIA_ID)) { // 点击事件,直接推送永久图文素材
      customizedMenuItem.setMediaId(contentOrMediaId);
    } else if (buttonType.equals(ButtonType.CLICK_REDIRECT_VIEW)) { // 点击跳转到url
      customizedMenuItem.setUrl(url);
    }
  }
}
