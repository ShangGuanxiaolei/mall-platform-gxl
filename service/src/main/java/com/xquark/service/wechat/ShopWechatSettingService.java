package com.xquark.service.wechat;

import com.xquark.dal.model.*;
import com.xquark.service.ArchivableEntityService;

public interface ShopWechatSettingService extends ArchivableEntityService<ShopWechatSetting> {

  /**
   * 创建配置信息
   */
  int create(ShopWechatSetting shopWechatSetting);

  /**
   * 更新配置信息
   */
  int update(ShopWechatSetting shopWechatSetting);

  /**
   * 通过 id 查找配置信息
   */
  ShopWechatSetting load(String id);

  /**
   * 通过 用户id 查找配置
   */
  ShopWechatSetting findByUser(String userId);

  /**
   * 获取当前登录用户的配置信息
   */
  ShopWechatSetting mine();


}
