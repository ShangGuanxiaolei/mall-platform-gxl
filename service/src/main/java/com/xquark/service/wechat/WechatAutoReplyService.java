package com.xquark.service.wechat;

import com.xquark.dal.model.wechat.AutoReply;
import com.xquark.dal.model.wechat.MatchType;
import com.xquark.service.ArchivableEntityService;
import com.xquark.wechat.event.EventType;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * 商家微信公众号自定义回复服务
 */
public interface WechatAutoReplyService extends ArchivableEntityService<AutoReply> {

  AutoReply find(String input, String shopId);

  AutoReply find(EventType input, String shopId);

  int update(AutoReply autoReply);

  /**
   * 服务端分页查询数据
   */
  List<AutoReply> list(String shopId);

  /**
   * 分页查询数据总条数
   */
  Long selectCnt(String shopId);

  /**
   * 获取消息自动回复
   */
  AutoReply getAutoBack();

  /**
   * 获取被添加自动回复
   */
  AutoReply getAddBack();
}
