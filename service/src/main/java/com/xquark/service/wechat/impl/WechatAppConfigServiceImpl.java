package com.xquark.service.wechat.impl;

import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.model.Shop;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.shop.ShopService;
import com.xquark.service.wechat.WechatAppConfigService;
import com.xquark.utils.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("WechatAppConfigService")
public class WechatAppConfigServiceImpl extends BaseServiceImpl implements WechatAppConfigService {

  /**
   * 启动时缓存微信配置
   */
  private volatile WechatAppConfig appConfig;

  @Autowired
  private WechatAppConfigMapper wechatAppConfigMapper;

  @Autowired
  private ShopService shopService;

  @Value("${profiles.active}")
  private String profile;

  @Value("${wechat.api.url}")
  private String apiUrl;

  @Override
  @Transactional
  public int save(WechatAppConfig appConfig) {
    int ret = 0;
    if (StringUtils.isEmpty(appConfig.getId())) {
      wechatAppConfigMapper.insert(appConfig.getAppName(), appConfig.getAppId(),
          appConfig.getAppSecret(), appConfig.getMchId(), appConfig.getMchKey(),
          profile, getCurrentUser().getShopId(), "", calApiToken(appConfig.getAppId()),
          appConfig.getCert_password());
    } else {
      ret = wechatAppConfigMapper.updateByPrimaryKeySelective(appConfig);
    }

    return ret;
  }

  @Override
  public WechatAppConfig load(String shopId) {
    return wechatAppConfigMapper.selectByShopId(shopId);
  }

  @Override
  public WechatAppConfig load() {
    Shop rootShop = shopService.loadRootShop();
    return load(rootShop.getId());
  }

  @Override
  public List<WechatAppConfig> selectByUrlBegin(String url) {
    return wechatAppConfigMapper.selectByUrlBegin(url);
  }

  private String calApiToken(String appId) {
    long salt = System.currentTimeMillis();
    String appIdCode = MD5Util.MD5Encode(appId + salt, "UTF-8");
    return StringUtils.substring(appIdCode, 0, 16);
  }

  // 没有微信配置记录时新增证书
  @Override
  public int insertCert(Map map) {
    return wechatAppConfigMapper.insertCert(map);
  }

  // 有微信配置记录时更新证书
  @Override
  public int updateCert(Map map) {
    return wechatAppConfigMapper.updateCert(map);
  }

  // 没有微信App配置记录时新增证书
  @Override
  public int insertAppCert(Map map) {
    return wechatAppConfigMapper.insertAppCert(map);
  }

  // 有微信App配置记录时更新证书
  @Override
  public int updateAppCert(Map map) {
    return wechatAppConfigMapper.updateAppCert(map);
  }

}
