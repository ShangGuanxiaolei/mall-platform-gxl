package com.xquark.service.wechat;

import com.xquark.dal.consts.WechatConstants;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.wechat.common.Config;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * wechat的工具集
 */
public class WechatUtils {

  public static Config convertWechatAppConfig(WechatAppConfig wechatAppConfig) {
    return new Config(wechatAppConfig.getApiUrl(), wechatAppConfig.getEncodingAESKey(),
        wechatAppConfig.getApiVerifyToken(), wechatAppConfig.getAppId(),
        wechatAppConfig.getAppSecret(), wechatAppConfig.getMchId(), wechatAppConfig.getMchKey(),
        wechatAppConfig.getCertFileContent(), wechatAppConfig.getAccessToken());
  }

  /**
   *
   * @param request
   * @return
   */
  public static boolean isFromWechatApp(HttpServletRequest request) {
    boolean ret = false;
    String userAgent = request.getHeader("User-Agent");
    if (StringUtils.isNotEmpty(userAgent)) {
      return StringUtils.contains(userAgent.toLowerCase(), "micromessenger")
          && getSessionId(request) == null;
    }
    return ret;
  }

  public static String getSessionId(HttpServletRequest request) {
    assert request != null;
    return request.getHeader(WechatConstants.WECHAT_MINI_PRPGRAM_THIRD_SESSION_KEY);
  }
}
