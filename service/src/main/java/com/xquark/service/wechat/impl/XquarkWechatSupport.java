package com.xquark.service.wechat.impl;

import com.alibaba.fastjson.JSON;
import com.xquark.dal.IUser;
import com.xquark.dal.mapper.WechatAppConfigMapper;
import com.xquark.dal.mapper.WechatCustomizedMenuMapper;
import com.xquark.dal.model.wechat.*;
import com.xquark.service.user.UserService;
import com.xquark.service.wechat.WechatAutoReplyService;
import com.xquark.service.wechat.WechatService;
import com.xquark.service.wechat.WechatUtils;
import com.xquark.utils.MD5Util;
import com.xquark.wechat.WechatSupport;
import com.xquark.wechat.common.Config;
import com.xquark.wechat.message.Article;
import com.xquark.wechat.response.ArticleResponse;
import com.xquark.wechat.user.User;
import com.xquark.wechat.user.UserManager;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dongsongjie on 16/6/7.
 */
public class XquarkWechatSupport extends WechatSupport {

  private WechatCustomizedMenuMapper wechatCustomizedMenuMapper;

  private WechatAppConfigMapper wechatAppConfigMapper;

  private WechatAutoReplyService wechatAutoReplyService;

  private WechatService wechatService;

  private UserService userService;

  public XquarkWechatSupport(WechatAppConfigMapper wechatAppConfigMapper,
      WechatAutoReplyService wechatAutoReplyService, WechatService wechatService,
      UserService userService, WechatCustomizedMenuMapper wechatCustomizedMenuMapper) {
    this.wechatAppConfigMapper = wechatAppConfigMapper;
    this.wechatAutoReplyService = wechatAutoReplyService;
    this.wechatService = wechatService;
    this.userService = userService;
    this.wechatCustomizedMenuMapper = wechatCustomizedMenuMapper;
  }

  public String onEventDispatch(HttpServletRequest request) {
    String ret = null;

    Config config = getConfig(request);

    //将上下文传入事件处理并拿到处理结果
    ret = execute(request, config);

    if (StringUtils.isEmpty(ret)) {
      logger
          .info("response with empty xml, wechat request:" + JSON.toJSONString(this.wechatRequest));
    }

    return ret;
  }

  private Config getConfig(HttpServletRequest request) {
    String requestUrl = request.getRequestURL().toString();
    WechatAppConfig appConfig = wechatAppConfigMapper.selectByApiUrl(requestUrl);
    return WechatUtils.convertWechatAppConfig(appConfig);

  }

  private Config getConfig(String appName) {
    WechatAppConfig appConfig = wechatAppConfigMapper.selectByAppName(appName);
    return new Config(appConfig.getApiUrl(), appConfig.getEncodingAESKey(),
        appConfig.getApiVerifyToken(), appConfig.getAppId(),
        appConfig.getAppSecret(), appConfig.getMchId(), appConfig.getMchKey(),
        appConfig.getCertFileContent(), appConfig.getAccessToken());
  }

  @Override
  protected void onText() {
    String input = this.wechatRequest.getContent();
    String appName = this.wechatRequest.getToUserName(); //公众号名称
    WechatAppConfig appConfig = wechatAppConfigMapper.selectByAppName(appName);
    if (appConfig == null) {
      return;
    }

    if (StringUtils.startsWith(input, "tlp ")) { // 模板消息测试
      String[] msg = StringUtils.split(input, " ");
      IUser user = userService
          .loadByWechatParam(this.wechatRequest.getFromUserName(), appName, false);
      wechatService.sendTemplateMessage(TemplateMessageType.find(msg[1]), appConfig.getShopId(),
          "http://www.baidu.com", null, user);
    }

    AutoReply autoReply = wechatAutoReplyService.find(input, appConfig.getShopId());

    if (autoReply != null && autoReply.getReplyType().equals(ReplyType.TEXT)) {
      responseText(autoReply.getReplyContent());
    } else if (autoReply != null && autoReply.getReplyType().equals(ReplyType.WECHAT_MEDIA)) {
      //FIXME dongsongjie
      ArticleResponse articleResponse = new ArticleResponse();
      Article article = wechatService.findMeterial(autoReply.getReplyContent());
      articleResponse.setTitle(article.getTitle());
      articleResponse.setDescription(article.getDigest());
      if (article.isShowCover()) {
        articleResponse.setPicUrl(article.getThumbUrl());
      }
      articleResponse.setUrl(article.getUrl());
      responseNews(articleResponse);
    } else {
      logger.error("no implement of unknown reply type for input:" + input);
    }
  }

  /**
   * 参考:http://mp.weixin.qq.com/wiki/14/bb5031008f1494a59c6f71fa0f319c66.html
   * 每个用户对每个公众号的OpenID是唯一的,对于不同的公众号,同一用户的OpenID不同,此时需要UnionID识别用户,UnionID必须开放平台授权才能读取
   * 在用户关注时拿到用户的注册信息,如OPENID,参考: http://mp.weixin.qq.com/wiki/2/5baf56ce4947d35003b86a9805634b1e.html#.E5.85.B3.E6.B3.A8.2F.E5.8F.96.E6.B6.88.E5.85.B3.E6.B3.A8.E4.BA.8B.E4.BB.B6
   */
  @Override
  protected void subscribe() {
    //关注时间, 整型, 用于处理重复消息问题, 微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次
    //关于重试的消息排重，推荐使用FromUserName + CreateTime 排重
    //获取用户信息
    String appName = this.wechatRequest.getToUserName(); //公众号名称
    WechatAppConfig appConfig = wechatAppConfigMapper.selectByAppName(appName);
    String accessToken = wechatService.obtainAccessToken(appConfig);

    User wechatUser = UserManager.getUserInfo(this.wechatRequest.getFromUserName(), accessToken);
    com.xquark.dal.model.User user = userService
        .loadByWechatParam(wechatUser.getOpenId(), appName, null);

    if (user == null) {
      user = new com.xquark.dal.model.User();
      user.setName(wechatUser.getNickName());
      user.setLoginname(wechatUser.getOpenId());
      user.setPassword(MD5Util.MD5Encode(wechatUser.getOpenId(), "UTF-8"));
      user.setAvatar(wechatUser.getHeadimgUrl());
      user.setWeixinCode(appName);
      user.setWithdrawType(3); //微信提现
      userService.insert(user);

      // 如果配置了关注回复消息，则发送消息
      AutoReply autoReply = wechatAutoReplyService.getAddBack();
      if (autoReply != null) {
        responseText(autoReply.getReplyContent());
      }
    } else {
      user.setName(wechatUser.getNickName());
      user.setArchive(false);
      user.setAvatar(wechatUser.getHeadimgUrl());
      userService.syncWechatUserInfo(user);
    }
  }

  @Override
  protected void unSubscribe() {
    String appName = this.wechatRequest.getToUserName(); //公众号名称
    String fromUserName = this.wechatRequest.getFromUserName(); //退订用户的OPENID
    com.xquark.dal.model.User user = userService.loadByWechatParam(fromUserName, appName, false);

    if (user != null) {
      user.setArchive(true);
      userService.syncWechatUserInfo(user);
    } else {
      logger.error(
          "unsubscribe user not found. appName:" + appName + " ,fromUserName:" + fromUserName);
    }
  }

  @Override
  protected void click() {
    String buttonKey = this.wechatRequest.getEventKey();
    CustomizedMenuItem button = wechatCustomizedMenuMapper.selectByButtonKey(buttonKey);
    if (button != null && StringUtils.isNotEmpty(button.getContent())) {
      responseText(button.getContent());
    } else {
      responseText("未知点击");
    }
  }

  @Override
  protected void templateMsgCallback() {
    responseText("通知成功");
  }

  @Override
  protected void scan() {
    //ignore
  }

  @Override
  protected void location() {
    //ignore
  }

  @Override
  protected void view() {
    //ignore
  }

  @Override
  protected void scanCodePush() {
    //ignore
  }

  @Override
  protected void scanCodeWaitMsg() {
    //ignore
  }

  @Override
  protected void picSysPhoto() {
    //ignore
  }

  @Override
  protected void picPhotoOrAlbum() {
    //ignore
  }

  @Override
  protected void picWeixin() {
    //ignore
  }

  @Override
  protected void locationSelect() {
    //ignore
  }

  @Override
  protected void kfCreateSession() {
    //ignore
  }

  @Override
  protected void kfCloseSession() {
    //ignore
  }

  @Override
  protected void kfSwitchSession() {
    //ignore
  }

  @Override
  protected void onImage() {
    //ignore
  }

  @Override
  protected void onVoice() {
    //ignore
  }

  @Override
  protected void onVideo() {
    //ignore
  }

  @Override
  protected void onShortVideo() {
    //ignore
  }

  @Override
  protected void onLocation() {
    //ignore
  }

  @Override
  protected void onLink() {
    //ignore
  }

  @Override
  protected void onUnknown() {
    //ignore
  }
}
