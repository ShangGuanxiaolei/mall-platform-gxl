package com.xquark.service.wechat.impl;

import com.google.common.base.Preconditions;
import com.xquark.dal.mapper.WechatTemplateMessageMapper;
import com.xquark.dal.model.wechat.TemplateMessage;
import com.xquark.dal.model.wechat.TemplateMessageType;
import com.xquark.service.common.BaseServiceImpl;
import com.xquark.service.wechat.WechatTemplateMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("WechatTemplateMessageService")
public class WechatTemplateMessageServiceImpl extends BaseServiceImpl implements
    WechatTemplateMessageService {

  @Autowired
  private WechatTemplateMessageMapper wechatTemplateMessageMapper;

  @Override
  public List<TemplateMessage> list(String shopId) {
    Preconditions.checkNotNull(shopId, "shopId can not be null");
    List<TemplateMessage> templateMessages = wechatTemplateMessageMapper.list(shopId, null);

    return templateMessages;
  }

  @Override
  @Transactional
  public int saveTemplateId(TemplateMessageType[] templateMessageNames, String[] templateIds) {
    int ret = 0;
    Preconditions.checkNotNull(templateMessageNames, "templateMessageNames can not be null");
    Preconditions.checkNotNull(templateIds, "templateIds can not be null");

    for (int i = 0; i < templateMessageNames.length; i++) {
      TemplateMessage templateMessage = wechatTemplateMessageMapper
          .selectByTemplateName(templateMessageNames[i], getCurrentUser().getShopId());
      if (templateMessage == null) {
        templateMessage = new TemplateMessage();
        templateMessage.setShopId(getCurrentUser().getShopId());
        templateMessage.setTemplateId(templateIds[i]);
        templateMessage.setTemplateName(templateMessageNames[i].toString());
        templateMessage.setTemplateNo(templateMessageNames[i].getTemplateMessageNo());
        ret = wechatTemplateMessageMapper.insert(templateMessage);
      } else {
        templateMessage.setTemplateId(templateIds[i]);
        ret = wechatTemplateMessageMapper.updateTemplateId(templateMessage);
      }
    }

    return ret;
  }

  @Override
  public TemplateMessage loadByTmpTypeAndShopId(TemplateMessageType templateMessageType,
      String shopId) {
    Preconditions.checkNotNull(templateMessageType, "templateMessageType can not be null");
    Preconditions.checkNotNull(shopId, "shopId can not be null");
    return wechatTemplateMessageMapper.selectByTemplateName(templateMessageType, shopId);
  }
}
