package com.xquark.service.wechat;

import com.xquark.dal.IUser;
import com.xquark.dal.model.wechat.ConditionType;
import com.xquark.dal.model.wechat.CustomizedMenu;
import com.xquark.dal.model.wechat.TemplateMessageType;
import com.xquark.dal.model.wechat.WechatAppConfig;
import com.xquark.wechat.message.Article;
import com.xquark.wechat.message.template.TemplateMsgData;
import com.xquark.wechat.oauth.protocol.WechatUserBean;
import com.xquark.wechat.oauth.protocol.get_userinfo.GetUserinfoResponse;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by dongsongjie on 16/4/6.
 */
public interface WechatService {

  /**
   * 微信API事件处理入口
   */
  String onEventDispatch(HttpServletRequest request);

  /**
   * 发送模板消息到用户
   */
  boolean sendTemplateMessage(TemplateMessageType templateMessageType, String shopId, String url,
      List<TemplateMsgData> data, IUser toUser);

  /**
   * 获取当前用户的微信公众号配置信息, 包括支付信息等等
   */
  WechatAppConfig load(IUser user);

  /**
   * 更新某公众号菜单项
   */
  boolean updateMenus(WechatAppConfig wechatAppConfig, CustomizedMenu customizedMenu);

  /**
   * 自动回复信息
   */
  boolean autoReply(ConditionType conditionType, String input);


  /**
   * 获得accessToken
   */
  public String obtainAccessToken(WechatAppConfig wechatAppConfig);

  /**
   * 强制更新accessToken
   */
  public String renewAccessToken(WechatAppConfig wechatAppConfig);

  /**
   * 获取jsapi ticket
   */
  public String obtainJsapiTicket(WechatAppConfig wechatAppConfig);

  String obtainJsapiTicket();

  /**
   * 强制更新jsapi ticket
   */
  public String renewJsapiTicket(WechatAppConfig wechatAppConfig);


  Article findMeterial(String mediaId);

  /**
   * 从Oauth请求链接中的state参数读取店铺id
   */
  String getShopIdFromState(String state);

  WechatUserBean getWechatUserInfoByCode(String code);
}
